MPLAB XC16 ASSEMBLY Listing:   			page 1


   1              	.file "/home/user/Documents/FST/Programming/project_sub-zero/lib/lib_pic33e/trap.c"
   2              	.section .debug_abbrev,info
   3                 	.Ldebug_abbrev0:
   4                 	.section .debug_info,info
   5                 	.Ldebug_info0:
   6                 	.section .debug_line,info
   7                 	.Ldebug_line0:
   8 0000 FF 00 00 00 	.section .text,code
   8      02 00 C4 00 
   8      00 00 01 01 
   8      FB 0E 0A 00 
   8      01 01 01 01 
   8      00 00 00 01 
   8      6C 69 62 2F 
   8      6C 69 62 5F 
   8      70 69 63 33 
   9              	.Ltext0:
  10              	.section .text,code
  11              	.align 2
  12              	.weak _trap_handler
  13              	.type _trap_handler,@function
  14              	_trap_handler:
  15              	.LFB0:
  16              	.file 1 "lib/lib_pic33e/trap.c"
   1:lib/lib_pic33e/trap.c **** #ifndef NOTRAP
   2:lib/lib_pic33e/trap.c **** 
   3:lib/lib_pic33e/trap.c **** #include "trap.h"
   4:lib/lib_pic33e/trap.c **** #include <xc.h>
   5:lib/lib_pic33e/trap.c **** 
   6:lib/lib_pic33e/trap.c **** /*
   7:lib/lib_pic33e/trap.c ****  * The handler is defined as weak so it can be defined outside the library
   8:lib/lib_pic33e/trap.c ****  */
   9:lib/lib_pic33e/trap.c **** void __attribute__((weak)) trap_handler(TrapType type) {
  17              	.loc 1 9 0
  18              	.set ___PA___,1
  19 000000  02 00 FA 	lnk #2
  20              	.LCFI0:
  21 000002  00 0F 78 	mov w0,[w14]
  10:lib/lib_pic33e/trap.c **** 
  11:lib/lib_pic33e/trap.c **** 
  12:lib/lib_pic33e/trap.c **** #if 0
  13:lib/lib_pic33e/trap.c ****     switch(type){
  14:lib/lib_pic33e/trap.c ****     case HARD_TRAP_OSCILATOR_FAIL:
  15:lib/lib_pic33e/trap.c ****         // Do stuff
  16:lib/lib_pic33e/trap.c ****         break;
  17:lib/lib_pic33e/trap.c ****     case HARD_TRAP_ADDRESS_ERROR:
  18:lib/lib_pic33e/trap.c ****         // Do stuff
  19:lib/lib_pic33e/trap.c ****         break;
  20:lib/lib_pic33e/trap.c ****         .
  21:lib/lib_pic33e/trap.c ****         .
  22:lib/lib_pic33e/trap.c ****         .
  23:lib/lib_pic33e/trap.c ****     }
  24:lib/lib_pic33e/trap.c **** #endif
  25:lib/lib_pic33e/trap.c **** 
  26:lib/lib_pic33e/trap.c **** 	return;
  27:lib/lib_pic33e/trap.c **** }
  22              	.loc 1 27 0
MPLAB XC16 ASSEMBLY Listing:   			page 2


  23 000004  8E 07 78 	mov w14,w15
  24 000006  4F 07 78 	mov [--w15],w14
  25 000008  00 40 A9 	bclr CORCON,#2
  26 00000a  00 00 06 	return 
  27              	.set ___PA___,0
  28              	.LFE0:
  29              	.size _trap_handler,.-_trap_handler
  30              	.section .isr.text,code,keep
  31              	.align 2
  32              	.global __OscillatorFail
  33              	.type __OscillatorFail,@function
  34              	__OscillatorFail:
  35              	.section .isr.text,code,keep
  36              	.LFB1:
  37              	.section .isr.text,code,keep
  28:lib/lib_pic33e/trap.c **** 
  29:lib/lib_pic33e/trap.c **** void __attribute__((interrupt,no_auto_psv)) _OscillatorFail(void){
  38              	.loc 1 29 0
  39              	.set ___PA___,0
  40 000000  00 00 F8 	push _RCOUNT
  41              	.LCFI1:
  42 000002  80 9F BE 	mov.d w0,[w15++]
  43              	.LCFI2:
  44 000004  82 9F BE 	mov.d w2,[w15++]
  45              	.LCFI3:
  46 000006  84 9F BE 	mov.d w4,[w15++]
  47              	.LCFI4:
  48 000008  86 9F BE 	mov.d w6,[w15++]
  49              	.LCFI5:
  50 00000a  00 00 FA 	lnk #0
  51              	.LCFI6:
  52              	.section .isr.text,code,keep
  30:lib/lib_pic33e/trap.c **** 
  31:lib/lib_pic33e/trap.c ****     trap_handler(HARD_TRAP_OSCILATOR_FAIL);
  53              	.loc 1 31 0
  54 00000c  00 00 EB 	clr w0
  55 00000e  00 00 07 	rcall _trap_handler
  56              	.section .isr.text,code,keep
  32:lib/lib_pic33e/trap.c **** 
  33:lib/lib_pic33e/trap.c ****     INTCON1bits.OSCFAIL = 0;            /*clear trap flag*/ 
  57              	.loc 1 33 0
  58 000010  00 20 A9 	bclr.b _INTCON1bits,#1
  59              	.section .isr.text,code,keep
  34:lib/lib_pic33e/trap.c **** 
  35:lib/lib_pic33e/trap.c ****     RESET();
  60              	.loc 1 35 0
  61              	
  62 000012  00 00 FE 	reset
  63              	.section .isr.text,code,keep
  36:lib/lib_pic33e/trap.c ****     return;
  37:lib/lib_pic33e/trap.c **** 
  38:lib/lib_pic33e/trap.c **** }
  64              	.loc 1 38 0
  65 000014  8E 07 78 	mov w14,w15
  66 000016  4F 07 78 	mov [--w15],w14
  67 000018  00 40 A9 	bclr CORCON,#2
  68 00001a  80 1F 78 	mov w0,[w15++]
MPLAB XC16 ASSEMBLY Listing:   			page 3


  69 00001c  00 0E 20 	mov #224,w0
  70 00001e  00 20 B7 	ior _SR
  71 000020  4F 00 78 	mov [--w15],w0
  72 000022  4F 03 BE 	mov.d [--w15],w6
  73 000024  4F 02 BE 	mov.d [--w15],w4
  74 000026  4F 01 BE 	mov.d [--w15],w2
  75 000028  4F 00 BE 	mov.d [--w15],w0
  76 00002a  00 00 F9 	pop _RCOUNT
  77 00002c  00 40 06 	retfie 
  78              	.set ___PA___,0
  79              	.LFE1:
  80              	.size __OscillatorFail,.-__OscillatorFail
  81              	.section .isr.text,code,keep
  82              	.align 2
  83              	.global __AddressError
  84              	.type __AddressError,@function
  85              	__AddressError:
  86              	.section .isr.text,code,keep
  87              	.LFB2:
  88              	.section .isr.text,code,keep
  39:lib/lib_pic33e/trap.c **** 
  40:lib/lib_pic33e/trap.c **** void __attribute__((interrupt,no_auto_psv)) _AddressError(void){
  89              	.loc 1 40 0
  90              	.set ___PA___,0
  91 00002e  00 00 F8 	push _RCOUNT
  92              	.LCFI7:
  93 000030  80 9F BE 	mov.d w0,[w15++]
  94              	.LCFI8:
  95 000032  82 9F BE 	mov.d w2,[w15++]
  96              	.LCFI9:
  97 000034  84 9F BE 	mov.d w4,[w15++]
  98              	.LCFI10:
  99 000036  86 9F BE 	mov.d w6,[w15++]
 100              	.LCFI11:
 101 000038  00 00 FA 	lnk #0
 102              	.LCFI12:
 103              	.section .isr.text,code,keep
  41:lib/lib_pic33e/trap.c **** 
  42:lib/lib_pic33e/trap.c ****     trap_handler(HARD_TRAP_ADDRESS_ERROR);
 104              	.loc 1 42 0
 105 00003a  10 00 20 	mov #1,w0
 106 00003c  00 00 07 	rcall _trap_handler
 107              	.section .isr.text,code,keep
  43:lib/lib_pic33e/trap.c **** 
  44:lib/lib_pic33e/trap.c ****     INTCON1bits.ADDRERR = 0;            /*clear trap flag*/ 
 108              	.loc 1 44 0
 109 00003e  00 60 A9 	bclr.b _INTCON1bits,#3
 110              	.section .isr.text,code,keep
  45:lib/lib_pic33e/trap.c **** 
  46:lib/lib_pic33e/trap.c ****     RESET();
 111              	.loc 1 46 0
 112              	
 113 000040  00 00 FE 	reset
 114              	.section .isr.text,code,keep
  47:lib/lib_pic33e/trap.c ****     return;
  48:lib/lib_pic33e/trap.c **** 
  49:lib/lib_pic33e/trap.c **** }
MPLAB XC16 ASSEMBLY Listing:   			page 4


 115              	.loc 1 49 0
 116 000042  8E 07 78 	mov w14,w15
 117 000044  4F 07 78 	mov [--w15],w14
 118 000046  00 40 A9 	bclr CORCON,#2
 119 000048  80 1F 78 	mov w0,[w15++]
 120 00004a  00 0E 20 	mov #224,w0
 121 00004c  00 20 B7 	ior _SR
 122 00004e  4F 00 78 	mov [--w15],w0
 123 000050  4F 03 BE 	mov.d [--w15],w6
 124 000052  4F 02 BE 	mov.d [--w15],w4
 125 000054  4F 01 BE 	mov.d [--w15],w2
 126 000056  4F 00 BE 	mov.d [--w15],w0
 127 000058  00 00 F9 	pop _RCOUNT
 128 00005a  00 40 06 	retfie 
 129              	.set ___PA___,0
 130              	.LFE2:
 131              	.size __AddressError,.-__AddressError
 132              	.section .isr.text,code,keep
 133              	.align 2
 134              	.global __StackError
 135              	.type __StackError,@function
 136              	__StackError:
 137              	.section .isr.text,code,keep
 138              	.LFB3:
 139              	.section .isr.text,code,keep
  50:lib/lib_pic33e/trap.c **** 
  51:lib/lib_pic33e/trap.c **** void __attribute__((interrupt,no_auto_psv)) _StackError(void){
 140              	.loc 1 51 0
 141              	.set ___PA___,0
 142 00005c  00 00 F8 	push _RCOUNT
 143              	.LCFI13:
 144 00005e  80 9F BE 	mov.d w0,[w15++]
 145              	.LCFI14:
 146 000060  82 9F BE 	mov.d w2,[w15++]
 147              	.LCFI15:
 148 000062  84 9F BE 	mov.d w4,[w15++]
 149              	.LCFI16:
 150 000064  86 9F BE 	mov.d w6,[w15++]
 151              	.LCFI17:
 152 000066  00 00 FA 	lnk #0
 153              	.LCFI18:
 154              	.section .isr.text,code,keep
  52:lib/lib_pic33e/trap.c **** 
  53:lib/lib_pic33e/trap.c ****     trap_handler(HARD_TRAP_STACK_ERROR);
 155              	.loc 1 53 0
 156 000068  20 00 20 	mov #2,w0
 157 00006a  00 00 07 	rcall _trap_handler
 158              	.section .isr.text,code,keep
  54:lib/lib_pic33e/trap.c **** 
  55:lib/lib_pic33e/trap.c ****     INTCON1bits.STKERR = 0;            /*clear trap flag*/ 
 159              	.loc 1 55 0
 160 00006c  00 40 A9 	bclr.b _INTCON1bits,#2
 161              	.section .isr.text,code,keep
  56:lib/lib_pic33e/trap.c **** 
  57:lib/lib_pic33e/trap.c ****     RESET();
 162              	.loc 1 57 0
 163              	
MPLAB XC16 ASSEMBLY Listing:   			page 5


 164 00006e  00 00 FE 	reset
 165              	.section .isr.text,code,keep
  58:lib/lib_pic33e/trap.c ****     return;
  59:lib/lib_pic33e/trap.c **** 
  60:lib/lib_pic33e/trap.c **** }
 166              	.loc 1 60 0
 167 000070  8E 07 78 	mov w14,w15
 168 000072  4F 07 78 	mov [--w15],w14
 169 000074  00 40 A9 	bclr CORCON,#2
 170 000076  80 1F 78 	mov w0,[w15++]
 171 000078  00 0E 20 	mov #224,w0
 172 00007a  00 20 B7 	ior _SR
 173 00007c  4F 00 78 	mov [--w15],w0
 174 00007e  4F 03 BE 	mov.d [--w15],w6
 175 000080  4F 02 BE 	mov.d [--w15],w4
 176 000082  4F 01 BE 	mov.d [--w15],w2
 177 000084  4F 00 BE 	mov.d [--w15],w0
 178 000086  00 00 F9 	pop _RCOUNT
 179 000088  00 40 06 	retfie 
 180              	.set ___PA___,0
 181              	.LFE3:
 182              	.size __StackError,.-__StackError
 183              	.section .isr.text,code,keep
 184              	.align 2
 185              	.global __MathError
 186              	.type __MathError,@function
 187              	__MathError:
 188              	.section .isr.text,code,keep
 189              	.LFB4:
 190              	.section .isr.text,code,keep
  61:lib/lib_pic33e/trap.c **** 
  62:lib/lib_pic33e/trap.c **** void __attribute__((interrupt,no_auto_psv)) _MathError(void){
 191              	.loc 1 62 0
 192              	.set ___PA___,0
 193 00008a  00 00 F8 	push _RCOUNT
 194              	.LCFI19:
 195 00008c  80 9F BE 	mov.d w0,[w15++]
 196              	.LCFI20:
 197 00008e  82 9F BE 	mov.d w2,[w15++]
 198              	.LCFI21:
 199 000090  84 9F BE 	mov.d w4,[w15++]
 200              	.LCFI22:
 201 000092  86 9F BE 	mov.d w6,[w15++]
 202              	.LCFI23:
 203 000094  00 00 FA 	lnk #0
 204              	.LCFI24:
 205              	.section .isr.text,code,keep
  63:lib/lib_pic33e/trap.c **** 
  64:lib/lib_pic33e/trap.c ****     trap_handler(HARD_TRAP_MATH_ERROR);
 206              	.loc 1 64 0
 207 000096  30 00 20 	mov #3,w0
 208 000098  00 00 07 	rcall _trap_handler
 209              	.section .isr.text,code,keep
  65:lib/lib_pic33e/trap.c **** 
  66:lib/lib_pic33e/trap.c ****     INTCON1bits.MATHERR = 0;            /*clear trap flag*/   
 210              	.loc 1 66 0
 211 00009a  00 80 A9 	bclr.b _INTCON1bits,#4
MPLAB XC16 ASSEMBLY Listing:   			page 6


 212              	.section .isr.text,code,keep
  67:lib/lib_pic33e/trap.c **** 
  68:lib/lib_pic33e/trap.c ****     RESET();
 213              	.loc 1 68 0
 214              	
 215 00009c  00 00 FE 	reset
 216              	.section .isr.text,code,keep
  69:lib/lib_pic33e/trap.c ****     return;
  70:lib/lib_pic33e/trap.c **** 
  71:lib/lib_pic33e/trap.c **** }
 217              	.loc 1 71 0
 218 00009e  8E 07 78 	mov w14,w15
 219 0000a0  4F 07 78 	mov [--w15],w14
 220 0000a2  00 40 A9 	bclr CORCON,#2
 221 0000a4  80 1F 78 	mov w0,[w15++]
 222 0000a6  00 0E 20 	mov #224,w0
 223 0000a8  00 20 B7 	ior _SR
 224 0000aa  4F 00 78 	mov [--w15],w0
 225 0000ac  4F 03 BE 	mov.d [--w15],w6
 226 0000ae  4F 02 BE 	mov.d [--w15],w4
 227 0000b0  4F 01 BE 	mov.d [--w15],w2
 228 0000b2  4F 00 BE 	mov.d [--w15],w0
 229 0000b4  00 00 F9 	pop _RCOUNT
 230 0000b6  00 40 06 	retfie 
 231              	.set ___PA___,0
 232              	.LFE4:
 233              	.size __MathError,.-__MathError
 234              	.section .debug_frame,info
 235                 	.Lframe0:
 236 0000 10 00 00 00 	.4byte .LECIE0-.LSCIE0
 237                 	.LSCIE0:
 238 0004 FF FF FF FF 	.4byte 0xffffffff
 239 0008 01          	.byte 0x1
 240 0009 00          	.byte 0
 241 000a 01          	.uleb128 0x1
 242 000b 02          	.sleb128 2
 243 000c 25          	.byte 0x25
 244 000d 12          	.byte 0x12
 245 000e 0F          	.uleb128 0xf
 246 000f 7E          	.sleb128 -2
 247 0010 09          	.byte 0x9
 248 0011 25          	.uleb128 0x25
 249 0012 0F          	.uleb128 0xf
 250 0013 00          	.align 4
 251                 	.LECIE0:
 252                 	.LSFDE0:
 253 0014 18 00 00 00 	.4byte .LEFDE0-.LASFDE0
 254                 	.LASFDE0:
 255 0018 00 00 00 00 	.4byte .Lframe0
 256 001c 00 00 00 00 	.4byte .LFB0
 257 0020 0C 00 00 00 	.4byte .LFE0-.LFB0
 258 0024 04          	.byte 0x4
 259 0025 02 00 00 00 	.4byte .LCFI0-.LFB0
 260 0029 13          	.byte 0x13
 261 002a 7D          	.sleb128 -3
 262 002b 0D          	.byte 0xd
 263 002c 0E          	.uleb128 0xe
MPLAB XC16 ASSEMBLY Listing:   			page 7


 264 002d 8E          	.byte 0x8e
 265 002e 02          	.uleb128 0x2
 266 002f 00          	.align 4
 267                 	.LEFDE0:
 268                 	.LSFDE2:
 269 0030 3C 00 00 00 	.4byte .LEFDE2-.LASFDE2
 270                 	.LASFDE2:
 271 0034 00 00 00 00 	.4byte .Lframe0
 272 0038 00 00 00 00 	.4byte .LFB1
 273 003c 2E 00 00 00 	.4byte .LFE1-.LFB1
 274 0040 04          	.byte 0x4
 275 0041 04 00 00 00 	.4byte .LCFI2-.LFB1
 276 0045 13          	.byte 0x13
 277 0046 7B          	.sleb128 -5
 278 0047 04          	.byte 0x4
 279 0048 02 00 00 00 	.4byte .LCFI3-.LCFI2
 280 004c 13          	.byte 0x13
 281 004d 79          	.sleb128 -7
 282 004e 04          	.byte 0x4
 283 004f 02 00 00 00 	.4byte .LCFI4-.LCFI3
 284 0053 13          	.byte 0x13
 285 0054 77          	.sleb128 -9
 286 0055 04          	.byte 0x4
 287 0056 02 00 00 00 	.4byte .LCFI5-.LCFI4
 288 005a 13          	.byte 0x13
 289 005b 75          	.sleb128 -11
 290 005c 04          	.byte 0x4
 291 005d 02 00 00 00 	.4byte .LCFI6-.LCFI5
 292 0061 13          	.byte 0x13
 293 0062 74          	.sleb128 -12
 294 0063 0D          	.byte 0xd
 295 0064 0E          	.uleb128 0xe
 296 0065 8E          	.byte 0x8e
 297 0066 0B          	.uleb128 0xb
 298 0067 86          	.byte 0x86
 299 0068 09          	.uleb128 0x9
 300 0069 84          	.byte 0x84
 301 006a 07          	.uleb128 0x7
 302 006b 82          	.byte 0x82
 303 006c 05          	.uleb128 0x5
 304 006d 80          	.byte 0x80
 305 006e 03          	.uleb128 0x3
 306 006f 00          	.align 4
 307                 	.LEFDE2:
 308                 	.LSFDE4:
 309 0070 3C 00 00 00 	.4byte .LEFDE4-.LASFDE4
 310                 	.LASFDE4:
 311 0074 00 00 00 00 	.4byte .Lframe0
 312 0078 00 00 00 00 	.4byte .LFB2
 313 007c 2E 00 00 00 	.4byte .LFE2-.LFB2
 314 0080 04          	.byte 0x4
 315 0081 04 00 00 00 	.4byte .LCFI8-.LFB2
 316 0085 13          	.byte 0x13
 317 0086 7B          	.sleb128 -5
 318 0087 04          	.byte 0x4
 319 0088 02 00 00 00 	.4byte .LCFI9-.LCFI8
 320 008c 13          	.byte 0x13
MPLAB XC16 ASSEMBLY Listing:   			page 8


 321 008d 79          	.sleb128 -7
 322 008e 04          	.byte 0x4
 323 008f 02 00 00 00 	.4byte .LCFI10-.LCFI9
 324 0093 13          	.byte 0x13
 325 0094 77          	.sleb128 -9
 326 0095 04          	.byte 0x4
 327 0096 02 00 00 00 	.4byte .LCFI11-.LCFI10
 328 009a 13          	.byte 0x13
 329 009b 75          	.sleb128 -11
 330 009c 04          	.byte 0x4
 331 009d 02 00 00 00 	.4byte .LCFI12-.LCFI11
 332 00a1 13          	.byte 0x13
 333 00a2 74          	.sleb128 -12
 334 00a3 0D          	.byte 0xd
 335 00a4 0E          	.uleb128 0xe
 336 00a5 8E          	.byte 0x8e
 337 00a6 0B          	.uleb128 0xb
 338 00a7 86          	.byte 0x86
 339 00a8 09          	.uleb128 0x9
 340 00a9 84          	.byte 0x84
 341 00aa 07          	.uleb128 0x7
 342 00ab 82          	.byte 0x82
 343 00ac 05          	.uleb128 0x5
 344 00ad 80          	.byte 0x80
 345 00ae 03          	.uleb128 0x3
 346 00af 00          	.align 4
 347                 	.LEFDE4:
 348                 	.LSFDE6:
 349 00b0 3C 00 00 00 	.4byte .LEFDE6-.LASFDE6
 350                 	.LASFDE6:
 351 00b4 00 00 00 00 	.4byte .Lframe0
 352 00b8 00 00 00 00 	.4byte .LFB3
 353 00bc 2E 00 00 00 	.4byte .LFE3-.LFB3
 354 00c0 04          	.byte 0x4
 355 00c1 04 00 00 00 	.4byte .LCFI14-.LFB3
 356 00c5 13          	.byte 0x13
 357 00c6 7B          	.sleb128 -5
 358 00c7 04          	.byte 0x4
 359 00c8 02 00 00 00 	.4byte .LCFI15-.LCFI14
 360 00cc 13          	.byte 0x13
 361 00cd 79          	.sleb128 -7
 362 00ce 04          	.byte 0x4
 363 00cf 02 00 00 00 	.4byte .LCFI16-.LCFI15
 364 00d3 13          	.byte 0x13
 365 00d4 77          	.sleb128 -9
 366 00d5 04          	.byte 0x4
 367 00d6 02 00 00 00 	.4byte .LCFI17-.LCFI16
 368 00da 13          	.byte 0x13
 369 00db 75          	.sleb128 -11
 370 00dc 04          	.byte 0x4
 371 00dd 02 00 00 00 	.4byte .LCFI18-.LCFI17
 372 00e1 13          	.byte 0x13
 373 00e2 74          	.sleb128 -12
 374 00e3 0D          	.byte 0xd
 375 00e4 0E          	.uleb128 0xe
 376 00e5 8E          	.byte 0x8e
 377 00e6 0B          	.uleb128 0xb
MPLAB XC16 ASSEMBLY Listing:   			page 9


 378 00e7 86          	.byte 0x86
 379 00e8 09          	.uleb128 0x9
 380 00e9 84          	.byte 0x84
 381 00ea 07          	.uleb128 0x7
 382 00eb 82          	.byte 0x82
 383 00ec 05          	.uleb128 0x5
 384 00ed 80          	.byte 0x80
 385 00ee 03          	.uleb128 0x3
 386 00ef 00          	.align 4
 387                 	.LEFDE6:
 388                 	.LSFDE8:
 389 00f0 3C 00 00 00 	.4byte .LEFDE8-.LASFDE8
 390                 	.LASFDE8:
 391 00f4 00 00 00 00 	.4byte .Lframe0
 392 00f8 00 00 00 00 	.4byte .LFB4
 393 00fc 2E 00 00 00 	.4byte .LFE4-.LFB4
 394 0100 04          	.byte 0x4
 395 0101 04 00 00 00 	.4byte .LCFI20-.LFB4
 396 0105 13          	.byte 0x13
 397 0106 7B          	.sleb128 -5
 398 0107 04          	.byte 0x4
 399 0108 02 00 00 00 	.4byte .LCFI21-.LCFI20
 400 010c 13          	.byte 0x13
 401 010d 79          	.sleb128 -7
 402 010e 04          	.byte 0x4
 403 010f 02 00 00 00 	.4byte .LCFI22-.LCFI21
 404 0113 13          	.byte 0x13
 405 0114 77          	.sleb128 -9
 406 0115 04          	.byte 0x4
 407 0116 02 00 00 00 	.4byte .LCFI23-.LCFI22
 408 011a 13          	.byte 0x13
 409 011b 75          	.sleb128 -11
 410 011c 04          	.byte 0x4
 411 011d 02 00 00 00 	.4byte .LCFI24-.LCFI23
 412 0121 13          	.byte 0x13
 413 0122 74          	.sleb128 -12
 414 0123 0D          	.byte 0xd
 415 0124 0E          	.uleb128 0xe
 416 0125 8E          	.byte 0x8e
 417 0126 0B          	.uleb128 0xb
 418 0127 86          	.byte 0x86
 419 0128 09          	.uleb128 0x9
 420 0129 84          	.byte 0x84
 421 012a 07          	.uleb128 0x7
 422 012b 82          	.byte 0x82
 423 012c 05          	.uleb128 0x5
 424 012d 80          	.byte 0x80
 425 012e 03          	.uleb128 0x3
 426 012f 00          	.align 4
 427                 	.LEFDE8:
 428                 	.section .text,code
 429              	.Letext0:
 430              	.file 2 "lib/lib_pic33e/trap.h"
 431              	.file 3 "/opt/microchip/xc16/v1.36/bin/bin/../../include/lega-c/stdint.h"
 432              	.file 4 "/opt/microchip/xc16/v1.36/bin/bin/../../support/dsPIC33E/h/p33EP256MU806.h"
 433              	.section .debug_info,info
 434 0000 07 04 00 00 	.4byte 0x407
MPLAB XC16 ASSEMBLY Listing:   			page 10


 435 0004 02 00       	.2byte 0x2
 436 0006 00 00 00 00 	.4byte .Ldebug_abbrev0
 437 000a 04          	.byte 0x4
 438 000b 01          	.uleb128 0x1
 439 000c 47 4E 55 20 	.asciz "GNU C 4.5.1 (XC16, Microchip v1.36) (A) Build date: Dec  5 2018"
 439      43 20 34 2E 
 439      35 2E 31 20 
 439      28 58 43 31 
 439      36 2C 20 4D 
 439      69 63 72 6F 
 439      63 68 69 70 
 439      20 76 31 2E 
 439      33 36 29 20 
 440 004c 01          	.byte 0x1
 441 004d 6C 69 62 2F 	.asciz "lib/lib_pic33e/trap.c"
 441      6C 69 62 5F 
 441      70 69 63 33 
 441      33 65 2F 74 
 441      72 61 70 2E 
 441      63 00 
 442 0063 2F 68 6F 6D 	.asciz "/home/user/Documents/FST/Programming/project_sub-zero"
 442      65 2F 75 73 
 442      65 72 2F 44 
 442      6F 63 75 6D 
 442      65 6E 74 73 
 442      2F 46 53 54 
 442      2F 50 72 6F 
 442      67 72 61 6D 
 442      6D 69 6E 67 
 443 0099 00 00 00 00 	.4byte .Ltext0
 444 009d 00 00 00 00 	.4byte .Letext0
 445 00a1 00 00 00 00 	.4byte .Ldebug_line0
 446 00a5 02          	.uleb128 0x2
 447 00a6 74 72 61 70 	.asciz "trap_types"
 447      5F 74 79 70 
 447      65 73 00 
 448 00b1 02          	.byte 0x2
 449 00b2 02          	.byte 0x2
 450 00b3 08          	.byte 0x8
 451 00b4 37 01 00 00 	.4byte 0x137
 452 00b8 03          	.uleb128 0x3
 453 00b9 48 41 52 44 	.asciz "HARD_TRAP_OSCILATOR_FAIL"
 453      5F 54 52 41 
 453      50 5F 4F 53 
 453      43 49 4C 41 
 453      54 4F 52 5F 
 453      46 41 49 4C 
 453      00 
 454 00d2 00          	.sleb128 0
 455 00d3 03          	.uleb128 0x3
 456 00d4 48 41 52 44 	.asciz "HARD_TRAP_ADDRESS_ERROR"
 456      5F 54 52 41 
 456      50 5F 41 44 
 456      44 52 45 53 
 456      53 5F 45 52 
 456      52 4F 52 00 
 457 00ec 01          	.sleb128 1
MPLAB XC16 ASSEMBLY Listing:   			page 11


 458 00ed 03          	.uleb128 0x3
 459 00ee 48 41 52 44 	.asciz "HARD_TRAP_STACK_ERROR"
 459      5F 54 52 41 
 459      50 5F 53 54 
 459      41 43 4B 5F 
 459      45 52 52 4F 
 459      52 00 
 460 0104 02          	.sleb128 2
 461 0105 03          	.uleb128 0x3
 462 0106 48 41 52 44 	.asciz "HARD_TRAP_MATH_ERROR"
 462      5F 54 52 41 
 462      50 5F 4D 41 
 462      54 48 5F 45 
 462      52 52 4F 52 
 462      00 
 463 011b 03          	.sleb128 3
 464 011c 03          	.uleb128 0x3
 465 011d 43 55 53 54 	.asciz "CUSTOM_TRAP_PARSE_ERROR"
 465      4F 4D 5F 54 
 465      52 41 50 5F 
 465      50 41 52 53 
 465      45 5F 45 52 
 465      52 4F 52 00 
 466 0135 04          	.sleb128 4
 467 0136 00          	.byte 0x0
 468 0137 04          	.uleb128 0x4
 469 0138 54 72 61 70 	.asciz "TrapType"
 469      54 79 70 65 
 469      00 
 470 0141 02          	.byte 0x2
 471 0142 0E          	.byte 0xe
 472 0143 A5 00 00 00 	.4byte 0xa5
 473 0147 05          	.uleb128 0x5
 474 0148 01          	.byte 0x1
 475 0149 06          	.byte 0x6
 476 014a 73 69 67 6E 	.asciz "signed char"
 476      65 64 20 63 
 476      68 61 72 00 
 477 0156 05          	.uleb128 0x5
 478 0157 02          	.byte 0x2
 479 0158 05          	.byte 0x5
 480 0159 69 6E 74 00 	.asciz "int"
 481 015d 05          	.uleb128 0x5
 482 015e 04          	.byte 0x4
 483 015f 05          	.byte 0x5
 484 0160 6C 6F 6E 67 	.asciz "long int"
 484      20 69 6E 74 
 484      00 
 485 0169 05          	.uleb128 0x5
 486 016a 08          	.byte 0x8
 487 016b 05          	.byte 0x5
 488 016c 6C 6F 6E 67 	.asciz "long long int"
 488      20 6C 6F 6E 
 488      67 20 69 6E 
 488      74 00 
 489 017a 05          	.uleb128 0x5
 490 017b 01          	.byte 0x1
MPLAB XC16 ASSEMBLY Listing:   			page 12


 491 017c 08          	.byte 0x8
 492 017d 75 6E 73 69 	.asciz "unsigned char"
 492      67 6E 65 64 
 492      20 63 68 61 
 492      72 00 
 493 018b 04          	.uleb128 0x4
 494 018c 75 69 6E 74 	.asciz "uint16_t"
 494      31 36 5F 74 
 494      00 
 495 0195 03          	.byte 0x3
 496 0196 31          	.byte 0x31
 497 0197 9B 01 00 00 	.4byte 0x19b
 498 019b 05          	.uleb128 0x5
 499 019c 02          	.byte 0x2
 500 019d 07          	.byte 0x7
 501 019e 75 6E 73 69 	.asciz "unsigned int"
 501      67 6E 65 64 
 501      20 69 6E 74 
 501      00 
 502 01ab 05          	.uleb128 0x5
 503 01ac 04          	.byte 0x4
 504 01ad 07          	.byte 0x7
 505 01ae 6C 6F 6E 67 	.asciz "long unsigned int"
 505      20 75 6E 73 
 505      69 67 6E 65 
 505      64 20 69 6E 
 505      74 00 
 506 01c0 05          	.uleb128 0x5
 507 01c1 08          	.byte 0x8
 508 01c2 07          	.byte 0x7
 509 01c3 6C 6F 6E 67 	.asciz "long long unsigned int"
 509      20 6C 6F 6E 
 509      67 20 75 6E 
 509      73 69 67 6E 
 509      65 64 20 69 
 509      6E 74 00 
 510 01da 06          	.uleb128 0x6
 511 01db 74 61 67 49 	.asciz "tagINTCON1BITS"
 511      4E 54 43 4F 
 511      4E 31 42 49 
 511      54 53 00 
 512 01ea 02          	.byte 0x2
 513 01eb 04          	.byte 0x4
 514 01ec 3A 2A       	.2byte 0x2a3a
 515 01ee 34 03 00 00 	.4byte 0x334
 516 01f2 07          	.uleb128 0x7
 517 01f3 4F 53 43 46 	.asciz "OSCFAIL"
 517      41 49 4C 00 
 518 01fb 04          	.byte 0x4
 519 01fc 3C 2A       	.2byte 0x2a3c
 520 01fe 8B 01 00 00 	.4byte 0x18b
 521 0202 02          	.byte 0x2
 522 0203 01          	.byte 0x1
 523 0204 0E          	.byte 0xe
 524 0205 02          	.byte 0x2
 525 0206 23          	.byte 0x23
 526 0207 00          	.uleb128 0x0
MPLAB XC16 ASSEMBLY Listing:   			page 13


 527 0208 07          	.uleb128 0x7
 528 0209 53 54 4B 45 	.asciz "STKERR"
 528      52 52 00 
 529 0210 04          	.byte 0x4
 530 0211 3D 2A       	.2byte 0x2a3d
 531 0213 8B 01 00 00 	.4byte 0x18b
 532 0217 02          	.byte 0x2
 533 0218 01          	.byte 0x1
 534 0219 0D          	.byte 0xd
 535 021a 02          	.byte 0x2
 536 021b 23          	.byte 0x23
 537 021c 00          	.uleb128 0x0
 538 021d 07          	.uleb128 0x7
 539 021e 41 44 44 52 	.asciz "ADDRERR"
 539      45 52 52 00 
 540 0226 04          	.byte 0x4
 541 0227 3E 2A       	.2byte 0x2a3e
 542 0229 8B 01 00 00 	.4byte 0x18b
 543 022d 02          	.byte 0x2
 544 022e 01          	.byte 0x1
 545 022f 0C          	.byte 0xc
 546 0230 02          	.byte 0x2
 547 0231 23          	.byte 0x23
 548 0232 00          	.uleb128 0x0
 549 0233 07          	.uleb128 0x7
 550 0234 4D 41 54 48 	.asciz "MATHERR"
 550      45 52 52 00 
 551 023c 04          	.byte 0x4
 552 023d 3F 2A       	.2byte 0x2a3f
 553 023f 8B 01 00 00 	.4byte 0x18b
 554 0243 02          	.byte 0x2
 555 0244 01          	.byte 0x1
 556 0245 0B          	.byte 0xb
 557 0246 02          	.byte 0x2
 558 0247 23          	.byte 0x23
 559 0248 00          	.uleb128 0x0
 560 0249 07          	.uleb128 0x7
 561 024a 44 4D 41 43 	.asciz "DMACERR"
 561      45 52 52 00 
 562 0252 04          	.byte 0x4
 563 0253 40 2A       	.2byte 0x2a40
 564 0255 8B 01 00 00 	.4byte 0x18b
 565 0259 02          	.byte 0x2
 566 025a 01          	.byte 0x1
 567 025b 0A          	.byte 0xa
 568 025c 02          	.byte 0x2
 569 025d 23          	.byte 0x23
 570 025e 00          	.uleb128 0x0
 571 025f 07          	.uleb128 0x7
 572 0260 44 49 56 30 	.asciz "DIV0ERR"
 572      45 52 52 00 
 573 0268 04          	.byte 0x4
 574 0269 41 2A       	.2byte 0x2a41
 575 026b 8B 01 00 00 	.4byte 0x18b
 576 026f 02          	.byte 0x2
 577 0270 01          	.byte 0x1
 578 0271 09          	.byte 0x9
MPLAB XC16 ASSEMBLY Listing:   			page 14


 579 0272 02          	.byte 0x2
 580 0273 23          	.byte 0x23
 581 0274 00          	.uleb128 0x0
 582 0275 07          	.uleb128 0x7
 583 0276 53 46 54 41 	.asciz "SFTACERR"
 583      43 45 52 52 
 583      00 
 584 027f 04          	.byte 0x4
 585 0280 42 2A       	.2byte 0x2a42
 586 0282 8B 01 00 00 	.4byte 0x18b
 587 0286 02          	.byte 0x2
 588 0287 01          	.byte 0x1
 589 0288 08          	.byte 0x8
 590 0289 02          	.byte 0x2
 591 028a 23          	.byte 0x23
 592 028b 00          	.uleb128 0x0
 593 028c 07          	.uleb128 0x7
 594 028d 43 4F 56 54 	.asciz "COVTE"
 594      45 00 
 595 0293 04          	.byte 0x4
 596 0294 43 2A       	.2byte 0x2a43
 597 0296 8B 01 00 00 	.4byte 0x18b
 598 029a 02          	.byte 0x2
 599 029b 01          	.byte 0x1
 600 029c 07          	.byte 0x7
 601 029d 02          	.byte 0x2
 602 029e 23          	.byte 0x23
 603 029f 00          	.uleb128 0x0
 604 02a0 07          	.uleb128 0x7
 605 02a1 4F 56 42 54 	.asciz "OVBTE"
 605      45 00 
 606 02a7 04          	.byte 0x4
 607 02a8 44 2A       	.2byte 0x2a44
 608 02aa 8B 01 00 00 	.4byte 0x18b
 609 02ae 02          	.byte 0x2
 610 02af 01          	.byte 0x1
 611 02b0 06          	.byte 0x6
 612 02b1 02          	.byte 0x2
 613 02b2 23          	.byte 0x23
 614 02b3 00          	.uleb128 0x0
 615 02b4 07          	.uleb128 0x7
 616 02b5 4F 56 41 54 	.asciz "OVATE"
 616      45 00 
 617 02bb 04          	.byte 0x4
 618 02bc 45 2A       	.2byte 0x2a45
 619 02be 8B 01 00 00 	.4byte 0x18b
 620 02c2 02          	.byte 0x2
 621 02c3 01          	.byte 0x1
 622 02c4 05          	.byte 0x5
 623 02c5 02          	.byte 0x2
 624 02c6 23          	.byte 0x23
 625 02c7 00          	.uleb128 0x0
 626 02c8 07          	.uleb128 0x7
 627 02c9 43 4F 56 42 	.asciz "COVBERR"
 627      45 52 52 00 
 628 02d1 04          	.byte 0x4
 629 02d2 46 2A       	.2byte 0x2a46
MPLAB XC16 ASSEMBLY Listing:   			page 15


 630 02d4 8B 01 00 00 	.4byte 0x18b
 631 02d8 02          	.byte 0x2
 632 02d9 01          	.byte 0x1
 633 02da 04          	.byte 0x4
 634 02db 02          	.byte 0x2
 635 02dc 23          	.byte 0x23
 636 02dd 00          	.uleb128 0x0
 637 02de 07          	.uleb128 0x7
 638 02df 43 4F 56 41 	.asciz "COVAERR"
 638      45 52 52 00 
 639 02e7 04          	.byte 0x4
 640 02e8 47 2A       	.2byte 0x2a47
 641 02ea 8B 01 00 00 	.4byte 0x18b
 642 02ee 02          	.byte 0x2
 643 02ef 01          	.byte 0x1
 644 02f0 03          	.byte 0x3
 645 02f1 02          	.byte 0x2
 646 02f2 23          	.byte 0x23
 647 02f3 00          	.uleb128 0x0
 648 02f4 07          	.uleb128 0x7
 649 02f5 4F 56 42 45 	.asciz "OVBERR"
 649      52 52 00 
 650 02fc 04          	.byte 0x4
 651 02fd 48 2A       	.2byte 0x2a48
 652 02ff 8B 01 00 00 	.4byte 0x18b
 653 0303 02          	.byte 0x2
 654 0304 01          	.byte 0x1
 655 0305 02          	.byte 0x2
 656 0306 02          	.byte 0x2
 657 0307 23          	.byte 0x23
 658 0308 00          	.uleb128 0x0
 659 0309 07          	.uleb128 0x7
 660 030a 4F 56 41 45 	.asciz "OVAERR"
 660      52 52 00 
 661 0311 04          	.byte 0x4
 662 0312 49 2A       	.2byte 0x2a49
 663 0314 8B 01 00 00 	.4byte 0x18b
 664 0318 02          	.byte 0x2
 665 0319 01          	.byte 0x1
 666 031a 01          	.byte 0x1
 667 031b 02          	.byte 0x2
 668 031c 23          	.byte 0x23
 669 031d 00          	.uleb128 0x0
 670 031e 07          	.uleb128 0x7
 671 031f 4E 53 54 44 	.asciz "NSTDIS"
 671      49 53 00 
 672 0326 04          	.byte 0x4
 673 0327 4A 2A       	.2byte 0x2a4a
 674 0329 8B 01 00 00 	.4byte 0x18b
 675 032d 02          	.byte 0x2
 676 032e 01          	.byte 0x1
 677 032f 10          	.byte 0x10
 678 0330 02          	.byte 0x2
 679 0331 23          	.byte 0x23
 680 0332 00          	.uleb128 0x0
 681 0333 00          	.byte 0x0
 682 0334 08          	.uleb128 0x8
MPLAB XC16 ASSEMBLY Listing:   			page 16


 683 0335 49 4E 54 43 	.asciz "INTCON1BITS"
 683      4F 4E 31 42 
 683      49 54 53 00 
 684 0341 04          	.byte 0x4
 685 0342 4B 2A       	.2byte 0x2a4b
 686 0344 DA 01 00 00 	.4byte 0x1da
 687 0348 09          	.uleb128 0x9
 688 0349 01          	.byte 0x1
 689 034a 74 72 61 70 	.asciz "trap_handler"
 689      5F 68 61 6E 
 689      64 6C 65 72 
 689      00 
 690 0357 01          	.byte 0x1
 691 0358 09          	.byte 0x9
 692 0359 01          	.byte 0x1
 693 035a 00 00 00 00 	.4byte .LFB0
 694 035e 00 00 00 00 	.4byte .LFE0
 695 0362 01          	.byte 0x1
 696 0363 5E          	.byte 0x5e
 697 0364 78 03 00 00 	.4byte 0x378
 698 0368 0A          	.uleb128 0xa
 699 0369 74 79 70 65 	.asciz "type"
 699      00 
 700 036e 01          	.byte 0x1
 701 036f 09          	.byte 0x9
 702 0370 37 01 00 00 	.4byte 0x137
 703 0374 02          	.byte 0x2
 704 0375 7E          	.byte 0x7e
 705 0376 00          	.sleb128 0
 706 0377 00          	.byte 0x0
 707 0378 0B          	.uleb128 0xb
 708 0379 01          	.byte 0x1
 709 037a 5F 4F 73 63 	.asciz "_OscillatorFail"
 709      69 6C 6C 61 
 709      74 6F 72 46 
 709      61 69 6C 00 
 710 038a 01          	.byte 0x1
 711 038b 1D          	.byte 0x1d
 712 038c 01          	.byte 0x1
 713 038d 00 00 00 00 	.4byte .LFB1
 714 0391 00 00 00 00 	.4byte .LFE1
 715 0395 01          	.byte 0x1
 716 0396 5E          	.byte 0x5e
 717 0397 0B          	.uleb128 0xb
 718 0398 01          	.byte 0x1
 719 0399 5F 41 64 64 	.asciz "_AddressError"
 719      72 65 73 73 
 719      45 72 72 6F 
 719      72 00 
 720 03a7 01          	.byte 0x1
 721 03a8 28          	.byte 0x28
 722 03a9 01          	.byte 0x1
 723 03aa 00 00 00 00 	.4byte .LFB2
 724 03ae 00 00 00 00 	.4byte .LFE2
 725 03b2 01          	.byte 0x1
 726 03b3 5E          	.byte 0x5e
 727 03b4 0B          	.uleb128 0xb
MPLAB XC16 ASSEMBLY Listing:   			page 17


 728 03b5 01          	.byte 0x1
 729 03b6 5F 53 74 61 	.asciz "_StackError"
 729      63 6B 45 72 
 729      72 6F 72 00 
 730 03c2 01          	.byte 0x1
 731 03c3 33          	.byte 0x33
 732 03c4 01          	.byte 0x1
 733 03c5 00 00 00 00 	.4byte .LFB3
 734 03c9 00 00 00 00 	.4byte .LFE3
 735 03cd 01          	.byte 0x1
 736 03ce 5E          	.byte 0x5e
 737 03cf 0B          	.uleb128 0xb
 738 03d0 01          	.byte 0x1
 739 03d1 5F 4D 61 74 	.asciz "_MathError"
 739      68 45 72 72 
 739      6F 72 00 
 740 03dc 01          	.byte 0x1
 741 03dd 3E          	.byte 0x3e
 742 03de 01          	.byte 0x1
 743 03df 00 00 00 00 	.4byte .LFB4
 744 03e3 00 00 00 00 	.4byte .LFE4
 745 03e7 01          	.byte 0x1
 746 03e8 5E          	.byte 0x5e
 747 03e9 0C          	.uleb128 0xc
 748 03ea 00 00 00 00 	.4byte .LASF0
 749 03ee 04          	.byte 0x4
 750 03ef 4C 2A       	.2byte 0x2a4c
 751 03f1 F7 03 00 00 	.4byte 0x3f7
 752 03f5 01          	.byte 0x1
 753 03f6 01          	.byte 0x1
 754 03f7 0D          	.uleb128 0xd
 755 03f8 34 03 00 00 	.4byte 0x334
 756 03fc 0C          	.uleb128 0xc
 757 03fd 00 00 00 00 	.4byte .LASF0
 758 0401 04          	.byte 0x4
 759 0402 4C 2A       	.2byte 0x2a4c
 760 0404 F7 03 00 00 	.4byte 0x3f7
 761 0408 01          	.byte 0x1
 762 0409 01          	.byte 0x1
 763 040a 00          	.byte 0x0
 764                 	.section .debug_abbrev,info
 765 0000 01          	.uleb128 0x1
 766 0001 11          	.uleb128 0x11
 767 0002 01          	.byte 0x1
 768 0003 25          	.uleb128 0x25
 769 0004 08          	.uleb128 0x8
 770 0005 13          	.uleb128 0x13
 771 0006 0B          	.uleb128 0xb
 772 0007 03          	.uleb128 0x3
 773 0008 08          	.uleb128 0x8
 774 0009 1B          	.uleb128 0x1b
 775 000a 08          	.uleb128 0x8
 776 000b 11          	.uleb128 0x11
 777 000c 01          	.uleb128 0x1
 778 000d 12          	.uleb128 0x12
 779 000e 01          	.uleb128 0x1
 780 000f 10          	.uleb128 0x10
MPLAB XC16 ASSEMBLY Listing:   			page 18


 781 0010 06          	.uleb128 0x6
 782 0011 00          	.byte 0x0
 783 0012 00          	.byte 0x0
 784 0013 02          	.uleb128 0x2
 785 0014 04          	.uleb128 0x4
 786 0015 01          	.byte 0x1
 787 0016 03          	.uleb128 0x3
 788 0017 08          	.uleb128 0x8
 789 0018 0B          	.uleb128 0xb
 790 0019 0B          	.uleb128 0xb
 791 001a 3A          	.uleb128 0x3a
 792 001b 0B          	.uleb128 0xb
 793 001c 3B          	.uleb128 0x3b
 794 001d 0B          	.uleb128 0xb
 795 001e 01          	.uleb128 0x1
 796 001f 13          	.uleb128 0x13
 797 0020 00          	.byte 0x0
 798 0021 00          	.byte 0x0
 799 0022 03          	.uleb128 0x3
 800 0023 28          	.uleb128 0x28
 801 0024 00          	.byte 0x0
 802 0025 03          	.uleb128 0x3
 803 0026 08          	.uleb128 0x8
 804 0027 1C          	.uleb128 0x1c
 805 0028 0D          	.uleb128 0xd
 806 0029 00          	.byte 0x0
 807 002a 00          	.byte 0x0
 808 002b 04          	.uleb128 0x4
 809 002c 16          	.uleb128 0x16
 810 002d 00          	.byte 0x0
 811 002e 03          	.uleb128 0x3
 812 002f 08          	.uleb128 0x8
 813 0030 3A          	.uleb128 0x3a
 814 0031 0B          	.uleb128 0xb
 815 0032 3B          	.uleb128 0x3b
 816 0033 0B          	.uleb128 0xb
 817 0034 49          	.uleb128 0x49
 818 0035 13          	.uleb128 0x13
 819 0036 00          	.byte 0x0
 820 0037 00          	.byte 0x0
 821 0038 05          	.uleb128 0x5
 822 0039 24          	.uleb128 0x24
 823 003a 00          	.byte 0x0
 824 003b 0B          	.uleb128 0xb
 825 003c 0B          	.uleb128 0xb
 826 003d 3E          	.uleb128 0x3e
 827 003e 0B          	.uleb128 0xb
 828 003f 03          	.uleb128 0x3
 829 0040 08          	.uleb128 0x8
 830 0041 00          	.byte 0x0
 831 0042 00          	.byte 0x0
 832 0043 06          	.uleb128 0x6
 833 0044 13          	.uleb128 0x13
 834 0045 01          	.byte 0x1
 835 0046 03          	.uleb128 0x3
 836 0047 08          	.uleb128 0x8
 837 0048 0B          	.uleb128 0xb
MPLAB XC16 ASSEMBLY Listing:   			page 19


 838 0049 0B          	.uleb128 0xb
 839 004a 3A          	.uleb128 0x3a
 840 004b 0B          	.uleb128 0xb
 841 004c 3B          	.uleb128 0x3b
 842 004d 05          	.uleb128 0x5
 843 004e 01          	.uleb128 0x1
 844 004f 13          	.uleb128 0x13
 845 0050 00          	.byte 0x0
 846 0051 00          	.byte 0x0
 847 0052 07          	.uleb128 0x7
 848 0053 0D          	.uleb128 0xd
 849 0054 00          	.byte 0x0
 850 0055 03          	.uleb128 0x3
 851 0056 08          	.uleb128 0x8
 852 0057 3A          	.uleb128 0x3a
 853 0058 0B          	.uleb128 0xb
 854 0059 3B          	.uleb128 0x3b
 855 005a 05          	.uleb128 0x5
 856 005b 49          	.uleb128 0x49
 857 005c 13          	.uleb128 0x13
 858 005d 0B          	.uleb128 0xb
 859 005e 0B          	.uleb128 0xb
 860 005f 0D          	.uleb128 0xd
 861 0060 0B          	.uleb128 0xb
 862 0061 0C          	.uleb128 0xc
 863 0062 0B          	.uleb128 0xb
 864 0063 38          	.uleb128 0x38
 865 0064 0A          	.uleb128 0xa
 866 0065 00          	.byte 0x0
 867 0066 00          	.byte 0x0
 868 0067 08          	.uleb128 0x8
 869 0068 16          	.uleb128 0x16
 870 0069 00          	.byte 0x0
 871 006a 03          	.uleb128 0x3
 872 006b 08          	.uleb128 0x8
 873 006c 3A          	.uleb128 0x3a
 874 006d 0B          	.uleb128 0xb
 875 006e 3B          	.uleb128 0x3b
 876 006f 05          	.uleb128 0x5
 877 0070 49          	.uleb128 0x49
 878 0071 13          	.uleb128 0x13
 879 0072 00          	.byte 0x0
 880 0073 00          	.byte 0x0
 881 0074 09          	.uleb128 0x9
 882 0075 2E          	.uleb128 0x2e
 883 0076 01          	.byte 0x1
 884 0077 3F          	.uleb128 0x3f
 885 0078 0C          	.uleb128 0xc
 886 0079 03          	.uleb128 0x3
 887 007a 08          	.uleb128 0x8
 888 007b 3A          	.uleb128 0x3a
 889 007c 0B          	.uleb128 0xb
 890 007d 3B          	.uleb128 0x3b
 891 007e 0B          	.uleb128 0xb
 892 007f 27          	.uleb128 0x27
 893 0080 0C          	.uleb128 0xc
 894 0081 11          	.uleb128 0x11
MPLAB XC16 ASSEMBLY Listing:   			page 20


 895 0082 01          	.uleb128 0x1
 896 0083 12          	.uleb128 0x12
 897 0084 01          	.uleb128 0x1
 898 0085 40          	.uleb128 0x40
 899 0086 0A          	.uleb128 0xa
 900 0087 01          	.uleb128 0x1
 901 0088 13          	.uleb128 0x13
 902 0089 00          	.byte 0x0
 903 008a 00          	.byte 0x0
 904 008b 0A          	.uleb128 0xa
 905 008c 05          	.uleb128 0x5
 906 008d 00          	.byte 0x0
 907 008e 03          	.uleb128 0x3
 908 008f 08          	.uleb128 0x8
 909 0090 3A          	.uleb128 0x3a
 910 0091 0B          	.uleb128 0xb
 911 0092 3B          	.uleb128 0x3b
 912 0093 0B          	.uleb128 0xb
 913 0094 49          	.uleb128 0x49
 914 0095 13          	.uleb128 0x13
 915 0096 02          	.uleb128 0x2
 916 0097 0A          	.uleb128 0xa
 917 0098 00          	.byte 0x0
 918 0099 00          	.byte 0x0
 919 009a 0B          	.uleb128 0xb
 920 009b 2E          	.uleb128 0x2e
 921 009c 00          	.byte 0x0
 922 009d 3F          	.uleb128 0x3f
 923 009e 0C          	.uleb128 0xc
 924 009f 03          	.uleb128 0x3
 925 00a0 08          	.uleb128 0x8
 926 00a1 3A          	.uleb128 0x3a
 927 00a2 0B          	.uleb128 0xb
 928 00a3 3B          	.uleb128 0x3b
 929 00a4 0B          	.uleb128 0xb
 930 00a5 27          	.uleb128 0x27
 931 00a6 0C          	.uleb128 0xc
 932 00a7 11          	.uleb128 0x11
 933 00a8 01          	.uleb128 0x1
 934 00a9 12          	.uleb128 0x12
 935 00aa 01          	.uleb128 0x1
 936 00ab 40          	.uleb128 0x40
 937 00ac 0A          	.uleb128 0xa
 938 00ad 00          	.byte 0x0
 939 00ae 00          	.byte 0x0
 940 00af 0C          	.uleb128 0xc
 941 00b0 34          	.uleb128 0x34
 942 00b1 00          	.byte 0x0
 943 00b2 03          	.uleb128 0x3
 944 00b3 0E          	.uleb128 0xe
 945 00b4 3A          	.uleb128 0x3a
 946 00b5 0B          	.uleb128 0xb
 947 00b6 3B          	.uleb128 0x3b
 948 00b7 05          	.uleb128 0x5
 949 00b8 49          	.uleb128 0x49
 950 00b9 13          	.uleb128 0x13
 951 00ba 3F          	.uleb128 0x3f
MPLAB XC16 ASSEMBLY Listing:   			page 21


 952 00bb 0C          	.uleb128 0xc
 953 00bc 3C          	.uleb128 0x3c
 954 00bd 0C          	.uleb128 0xc
 955 00be 00          	.byte 0x0
 956 00bf 00          	.byte 0x0
 957 00c0 0D          	.uleb128 0xd
 958 00c1 35          	.uleb128 0x35
 959 00c2 00          	.byte 0x0
 960 00c3 49          	.uleb128 0x49
 961 00c4 13          	.uleb128 0x13
 962 00c5 00          	.byte 0x0
 963 00c6 00          	.byte 0x0
 964 00c7 00          	.byte 0x0
 965                 	.section .debug_pubnames,info
 966 0000 64 00 00 00 	.4byte 0x64
 967 0004 02 00       	.2byte 0x2
 968 0006 00 00 00 00 	.4byte .Ldebug_info0
 969 000a 0B 04 00 00 	.4byte 0x40b
 970 000e 48 03 00 00 	.4byte 0x348
 971 0012 74 72 61 70 	.asciz "trap_handler"
 971      5F 68 61 6E 
 971      64 6C 65 72 
 971      00 
 972 001f 78 03 00 00 	.4byte 0x378
 973 0023 5F 4F 73 63 	.asciz "_OscillatorFail"
 973      69 6C 6C 61 
 973      74 6F 72 46 
 973      61 69 6C 00 
 974 0033 97 03 00 00 	.4byte 0x397
 975 0037 5F 41 64 64 	.asciz "_AddressError"
 975      72 65 73 73 
 975      45 72 72 6F 
 975      72 00 
 976 0045 B4 03 00 00 	.4byte 0x3b4
 977 0049 5F 53 74 61 	.asciz "_StackError"
 977      63 6B 45 72 
 977      72 6F 72 00 
 978 0055 CF 03 00 00 	.4byte 0x3cf
 979 0059 5F 4D 61 74 	.asciz "_MathError"
 979      68 45 72 72 
 979      6F 72 00 
 980 0064 00 00 00 00 	.4byte 0x0
 981                 	.section .debug_pubtypes,info
 982 0000 5A 00 00 00 	.4byte 0x5a
 983 0004 02 00       	.2byte 0x2
 984 0006 00 00 00 00 	.4byte .Ldebug_info0
 985 000a 0B 04 00 00 	.4byte 0x40b
 986 000e A5 00 00 00 	.4byte 0xa5
 987 0012 74 72 61 70 	.asciz "trap_types"
 987      5F 74 79 70 
 987      65 73 00 
 988 001d 37 01 00 00 	.4byte 0x137
 989 0021 54 72 61 70 	.asciz "TrapType"
 989      54 79 70 65 
 989      00 
 990 002a 8B 01 00 00 	.4byte 0x18b
 991 002e 75 69 6E 74 	.asciz "uint16_t"
MPLAB XC16 ASSEMBLY Listing:   			page 22


 991      31 36 5F 74 
 991      00 
 992 0037 DA 01 00 00 	.4byte 0x1da
 993 003b 74 61 67 49 	.asciz "tagINTCON1BITS"
 993      4E 54 43 4F 
 993      4E 31 42 49 
 993      54 53 00 
 994 004a 34 03 00 00 	.4byte 0x334
 995 004e 49 4E 54 43 	.asciz "INTCON1BITS"
 995      4F 4E 31 42 
 995      49 54 53 00 
 996 005a 00 00 00 00 	.4byte 0x0
 997                 	.section .debug_aranges,info
 998 0000 14 00 00 00 	.4byte 0x14
 999 0004 02 00       	.2byte 0x2
 1000 0006 00 00 00 00 	.4byte .Ldebug_info0
 1001 000a 04          	.byte 0x4
 1002 000b 00          	.byte 0x0
 1003 000c 00 00       	.2byte 0x0
 1004 000e 00 00       	.2byte 0x0
 1005 0010 00 00 00 00 	.4byte 0x0
 1006 0014 00 00 00 00 	.4byte 0x0
 1007                 	.section .debug_str,info
 1008                 	.LASF0:
 1009 0000 49 4E 54 43 	.asciz "INTCON1bits"
 1009      4F 4E 31 62 
 1009      69 74 73 00 
 1010                 	.section .text,code
 1011              	
 1012              	
 1013              	
 1014              	.section __c30_info,info,bss
 1015                 	__psv_trap_errata:
 1016                 	
 1017                 	.section __c30_signature,info,data
 1018 0000 01 00       	.word 0x0001
 1019 0002 00 00       	.word 0x0000
 1020 0004 00 00       	.word 0x0000
 1021                 	
 1022                 	
 1023                 	
 1024                 	.set ___PA___,0
 1025                 	.end
MPLAB XC16 ASSEMBLY Listing:   			page 23


DEFINED SYMBOLS
                            *ABS*:00000000 /home/user/Documents/FST/Programming/project_sub-zero/lib/lib_pic33e/trap.c
                            *ABS*:00000001 __33EP256MU806
                            *ABS*:00000001 __dsPIC33E
                            *ABS*:00000001 __HAS_DSP
                            *ABS*:00000001 __HAS_DMAV2
                            *ABS*:00000001 __HAS_CODEGUARD
                            *ABS*:00000001 __HAS_PMP_ENHANCED
                            *ABS*:00000001 __HAS_EDS
                            *ABS*:00000001 __HAS_EP
                            *ABS*:00000011 __TARGET_DIVIDE_CYCLES
                            *ABS*:00000001 __C30ELF
    {standard input}:14     .text:00000000 _trap_handler
    {standard input}:18     *ABS*:00000000 ___PA___
                            *ABS*:00000040 __ext_attr_.isr.text
    {standard input}:34     .isr.text:00000000 __OscillatorFail
    {standard input}:85     .isr.text:0000002e __AddressError
    {standard input}:136    .isr.text:0000005c __StackError
    {standard input}:187    .isr.text:0000008a __MathError
    {standard input}:1015   __c30_info:00000000 __psv_trap_errata
    {standard input}:40     .isr.text:00000000 .L0
    {standard input}:19     .text:00000000 .L0
                    .debug_abbrev:00000000 .Ldebug_abbrev0
                            .text:00000000 .Ltext0
                            .text:0000000c .Letext0
                      .debug_line:00000000 .Ldebug_line0
                            .text:00000000 .LFB0
                            .text:0000000c .LFE0
                        .isr.text:00000000 .LFB1
                        .isr.text:0000002e .LFE1
                        .isr.text:0000002e .LFB2
                        .isr.text:0000005c .LFE2
                        .isr.text:0000005c .LFB3
                        .isr.text:0000008a .LFE3
                        .isr.text:0000008a .LFB4
                        .isr.text:000000b8 .LFE4
                       .debug_str:00000000 .LASF0
                     .debug_frame:00000000 .Lframe0
                      .debug_info:00000000 .Ldebug_info0

UNDEFINED SYMBOLS
CORCON
_RCOUNT
_INTCON1bits
_SR

EQUATE SYMBOLS
/home/user/Documents/FST/Programming/project_sub-zero/lib/lib_pic33e/trap.c = 0x0
                  __33EP256MU806 = 0x1
                      __dsPIC33E = 0x1
                       __HAS_DSP = 0x1
                     __HAS_DMAV2 = 0x1
                 __HAS_CODEGUARD = 0x1
              __HAS_PMP_ENHANCED = 0x1
                       __HAS_EDS = 0x1
                        __HAS_EP = 0x1
          __TARGET_DIVIDE_CYCLES = 0x11
                        __C30ELF = 0x1
                        ___PA___ = 0x0
            __ext_attr_.isr.text = 0x40
