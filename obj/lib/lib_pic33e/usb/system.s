MPLAB XC16 ASSEMBLY Listing:   			page 1


   1              	.file "/home/user/Documents/FST/Programming/project_sub-zero/lib/lib_pic33e/usb/system.c"
   2              	.section .debug_abbrev,info
   3                 	.Ldebug_abbrev0:
   4                 	.section .debug_info,info
   5                 	.Ldebug_info0:
   6                 	.section .debug_line,info
   7                 	.Ldebug_line0:
   8 0000 6C 00 00 00 	.section .text,code
   8      02 00 3B 00 
   8      00 00 01 01 
   8      FB 0E 0A 00 
   8      01 01 01 01 
   8      00 00 00 01 
   8      6C 69 62 2F 
   8      6C 69 62 5F 
   8      70 69 63 33 
   9              	.Ltext0:
  10              	.section .text,code
  11              	.align 2
  12              	.global _SYSTEM_Initialize_USB
  13              	.type _SYSTEM_Initialize_USB,@function
  14              	_SYSTEM_Initialize_USB:
  15              	.LFB0:
  16              	.file 1 "lib/lib_pic33e/usb/system.c"
   1:lib/lib_pic33e/usb/system.c **** #ifndef NOUSB
   2:lib/lib_pic33e/usb/system.c **** /*******************************************************************************
   3:lib/lib_pic33e/usb/system.c **** Copyright 2016 Microchip Technology Inc. (www.microchip.com)
   4:lib/lib_pic33e/usb/system.c **** 
   5:lib/lib_pic33e/usb/system.c **** Licensed under the Apache License, Version 2.0 (the "License");
   6:lib/lib_pic33e/usb/system.c **** you may not use this file except in compliance with the License.
   7:lib/lib_pic33e/usb/system.c **** You may obtain a copy of the License at
   8:lib/lib_pic33e/usb/system.c **** 
   9:lib/lib_pic33e/usb/system.c ****     http://www.apache.org/licenses/LICENSE-2.0
  10:lib/lib_pic33e/usb/system.c **** 
  11:lib/lib_pic33e/usb/system.c **** Unless required by applicable law or agreed to in writing, software
  12:lib/lib_pic33e/usb/system.c **** distributed under the License is distributed on an "AS IS" BASIS,
  13:lib/lib_pic33e/usb/system.c **** WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  14:lib/lib_pic33e/usb/system.c **** See the License for the specific language governing permissions and
  15:lib/lib_pic33e/usb/system.c **** limitations under the License.
  16:lib/lib_pic33e/usb/system.c **** 
  17:lib/lib_pic33e/usb/system.c **** To request to license the code under the MLA license (www.microchip.com/mla_license), 
  18:lib/lib_pic33e/usb/system.c **** please contact mla_licensing@microchip.com
  19:lib/lib_pic33e/usb/system.c **** *******************************************************************************/
  20:lib/lib_pic33e/usb/system.c **** 
  21:lib/lib_pic33e/usb/system.c **** #include <xc.h>
  22:lib/lib_pic33e/usb/system.c **** 
  23:lib/lib_pic33e/usb/system.c **** //Had to include this file due to the USBSleepOnSuspend() function.
  24:lib/lib_pic33e/usb/system.c **** #include "usb_hal_dspic33e.h"
  25:lib/lib_pic33e/usb/system.c **** #include "system.h"
  26:lib/lib_pic33e/usb/system.c **** #include "usb.h"
  27:lib/lib_pic33e/usb/system.c **** #include "app_device_cdc_basic.h"
  28:lib/lib_pic33e/usb/system.c **** 
  29:lib/lib_pic33e/usb/system.c **** 
  30:lib/lib_pic33e/usb/system.c **** 
  31:lib/lib_pic33e/usb/system.c **** 
  32:lib/lib_pic33e/usb/system.c **** 
  33:lib/lib_pic33e/usb/system.c **** /*********************************************************************
MPLAB XC16 ASSEMBLY Listing:   			page 2


  34:lib/lib_pic33e/usb/system.c **** * Function: void SYSTEM_Initialize_USB( SYSTEM_STATE state )
  35:lib/lib_pic33e/usb/system.c **** *
  36:lib/lib_pic33e/usb/system.c **** * Overview: Initializes the system.
  37:lib/lib_pic33e/usb/system.c **** *
  38:lib/lib_pic33e/usb/system.c **** * PreCondition: None
  39:lib/lib_pic33e/usb/system.c **** *
  40:lib/lib_pic33e/usb/system.c **** * Input:  SYSTEM_STATE - the state to initialize the system into
  41:lib/lib_pic33e/usb/system.c **** *
  42:lib/lib_pic33e/usb/system.c **** * Output: None
  43:lib/lib_pic33e/usb/system.c **** *
  44:lib/lib_pic33e/usb/system.c **** ********************************************************************/
  45:lib/lib_pic33e/usb/system.c **** void SYSTEM_Initialize_USB( SYSTEM_STATE state )
  46:lib/lib_pic33e/usb/system.c **** {
  17              	.loc 1 46 0
  18              	.set ___PA___,1
  19 000000  02 00 FA 	lnk #2
  20              	.LCFI0:
  21              	.loc 1 46 0
  22 000002  00 0F 78 	mov w0,[w14]
  47:lib/lib_pic33e/usb/system.c ****     switch(state)
  23              	.loc 1 47 0
  24 000004  1E 00 78 	mov [w14],w0
  25 000006  E1 0F 50 	sub w0,#1,[w15]
  26              	.set ___BP___,0
  27 000008  00 00 3A 	bra nz,.L7
  28              	.L4:
  48:lib/lib_pic33e/usb/system.c ****     {
  49:lib/lib_pic33e/usb/system.c ****         case SYSTEM_STATE_USB_START:
  50:lib/lib_pic33e/usb/system.c ****             //Do nothing, clock configuration is already being done in timing.h
  51:lib/lib_pic33e/usb/system.c ****             break;
  52:lib/lib_pic33e/usb/system.c **** 
  53:lib/lib_pic33e/usb/system.c ****         case SYSTEM_STATE_USB_SUSPEND:
  54:lib/lib_pic33e/usb/system.c ****             //If developing a bus powered USB device that needs to be USB compliant,
  55:lib/lib_pic33e/usb/system.c ****             //insert code here to reduce the I/O pin and microcontroller power consumption,
  56:lib/lib_pic33e/usb/system.c ****             //so that the total current is <2.5mA from the USB host's VBUS supply.
  57:lib/lib_pic33e/usb/system.c ****             //If developing a self powered application (or a bus powered device where
  58:lib/lib_pic33e/usb/system.c ****             //official USB compliance isn't critical), nothing strictly needs
  59:lib/lib_pic33e/usb/system.c ****             //to be done during USB suspend.
  60:lib/lib_pic33e/usb/system.c **** 
  61:lib/lib_pic33e/usb/system.c ****             USBSleepOnSuspend();
  29              	.loc 1 61 0
  30 00000a  00 00 07 	rcall _USBSleepOnSuspend
  62:lib/lib_pic33e/usb/system.c ****             break;
  31              	.loc 1 62 0
  32 00000c  00 00 37 	bra .L1
  33              	.L7:
  34              	.L1:
  63:lib/lib_pic33e/usb/system.c **** 
  64:lib/lib_pic33e/usb/system.c ****         case SYSTEM_STATE_USB_RESUME:
  65:lib/lib_pic33e/usb/system.c ****             //Here would be a good place to restore I/O pins and application states
  66:lib/lib_pic33e/usb/system.c ****             //back to their original values that may have been saved/changed at the 
  67:lib/lib_pic33e/usb/system.c ****             //start of the suspend event (ex: when the SYSTEM_Initialize_USB(SYSTEM_STATE_USB_SUSPE
  68:lib/lib_pic33e/usb/system.c ****             //callback was called.
  69:lib/lib_pic33e/usb/system.c ****             
  70:lib/lib_pic33e/usb/system.c ****             //If you didn't change any I/O pin states prior to entry into suspend,
  71:lib/lib_pic33e/usb/system.c ****             //then nothing explicitly needs to be done here.  However, by the time
  72:lib/lib_pic33e/usb/system.c ****             //the firmware returns from this function, the full application should
MPLAB XC16 ASSEMBLY Listing:   			page 3


  73:lib/lib_pic33e/usb/system.c ****             //be restored into effectively exactly the same state as the application
  74:lib/lib_pic33e/usb/system.c ****             //was in, prior to entering USB suspend.
  75:lib/lib_pic33e/usb/system.c ****             
  76:lib/lib_pic33e/usb/system.c ****             //Additionally, before returning from this function, make sure the microcontroller's
  77:lib/lib_pic33e/usb/system.c ****             //currently active clock settings are compatible with USB operation, including
  78:lib/lib_pic33e/usb/system.c ****             //taking into consideration all possible microcontroller features/effects
  79:lib/lib_pic33e/usb/system.c ****             //that can affect the oscillator settings (such as internal/external 
  80:lib/lib_pic33e/usb/system.c ****             //switchover (two speed start up), fail-safe clock monitor, PLL lock time,
  81:lib/lib_pic33e/usb/system.c ****             //external crystal/resonator startup time (if using a crystal/resonator),
  82:lib/lib_pic33e/usb/system.c ****             //etc.
  83:lib/lib_pic33e/usb/system.c **** 
  84:lib/lib_pic33e/usb/system.c ****             //Additionally, the official USB specifications dictate that USB devices
  85:lib/lib_pic33e/usb/system.c ****             //must become fully operational and ready for new host requests/normal 
  86:lib/lib_pic33e/usb/system.c ****             //USB communication after a 10ms "resume recovery interval" has elapsed.
  87:lib/lib_pic33e/usb/system.c ****             //In order to meet this timing requirement and avoid possible issues,
  88:lib/lib_pic33e/usb/system.c ****             //make sure that all necessary oscillator startup is complete and this
  89:lib/lib_pic33e/usb/system.c ****             //function has returned within less than this 10ms interval.
  90:lib/lib_pic33e/usb/system.c **** 
  91:lib/lib_pic33e/usb/system.c ****             break;
  92:lib/lib_pic33e/usb/system.c **** 
  93:lib/lib_pic33e/usb/system.c ****         default:
  94:lib/lib_pic33e/usb/system.c ****             break;
  95:lib/lib_pic33e/usb/system.c ****     }
  96:lib/lib_pic33e/usb/system.c **** }
  35              	.loc 1 96 0
  36 00000e  8E 07 78 	mov w14,w15
  37 000010  4F 07 78 	mov [--w15],w14
  38 000012  00 40 A9 	bclr CORCON,#2
  39 000014  00 00 06 	return 
  40              	.set ___PA___,0
  41              	.LFE0:
  42              	.size _SYSTEM_Initialize_USB,.-_SYSTEM_Initialize_USB
  43              	.section .isr.text,code,keep
  44              	.align 2
  45              	.global __USB1Interrupt
  46              	.type __USB1Interrupt,@function
  47              	__USB1Interrupt:
  48              	.section .isr.text,code,keep
  49              	.LFB1:
  50              	.section .isr.text,code,keep
  97:lib/lib_pic33e/usb/system.c **** 
  98:lib/lib_pic33e/usb/system.c **** #if defined(USB_INTERRUPT)
  99:lib/lib_pic33e/usb/system.c **** void __attribute__((interrupt,auto_psv)) _USB1Interrupt()
 100:lib/lib_pic33e/usb/system.c **** {
  51              	.loc 1 100 0
  52              	.set ___PA___,1
  53 000000  00 00 F8 	push _RCOUNT
  54              	.LCFI1:
  55 000002  80 9F BE 	mov.d w0,[w15++]
  56              	.LCFI2:
  57 000004  82 9F BE 	mov.d w2,[w15++]
  58              	.LCFI3:
  59 000006  84 9F BE 	mov.d w4,[w15++]
  60              	.LCFI4:
  61 000008  86 9F BE 	mov.d w6,[w15++]
  62              	.LCFI5:
  63 00000a  00 00 F8 	push _DSRPAG
MPLAB XC16 ASSEMBLY Listing:   			page 4


  64              	.LCFI6:
  65 00000c  00 00 F8 	push _DSWPAG
  66              	.LCFI7:
  67 00000e  10 00 20 	mov #1,w0
  68 000010  00 00 88 	mov w0,_DSWPAG
  69 000012  00 00 20 	mov #__const_psvpage,w0
  70 000014  00 00 88 	mov w0,_DSRPAG
  71 000016  00 00 00 	nop 
  72 000018  00 00 FA 	lnk #0
  73              	.LCFI8:
  74              	.section .isr.text,code,keep
 101:lib/lib_pic33e/usb/system.c ****     USBDeviceTasks();
  75              	.loc 1 101 0
  76 00001a  00 00 07 	rcall _USBDeviceTasks
  77              	.section .isr.text,code,keep
 102:lib/lib_pic33e/usb/system.c **** 
 103:lib/lib_pic33e/usb/system.c **** }
  78              	.loc 1 103 0
  79 00001c  8E 07 78 	mov w14,w15
  80 00001e  4F 07 78 	mov [--w15],w14
  81 000020  00 40 A9 	bclr CORCON,#2
  82 000022  00 00 F9 	pop _DSWPAG
  83 000024  00 00 F9 	pop _DSRPAG
  84 000026  80 1F 78 	mov w0,[w15++]
  85 000028  00 0E 20 	mov #224,w0
  86 00002a  00 20 B7 	ior _SR
  87 00002c  4F 00 78 	mov [--w15],w0
  88 00002e  4F 03 BE 	mov.d [--w15],w6
  89 000030  4F 02 BE 	mov.d [--w15],w4
  90 000032  4F 01 BE 	mov.d [--w15],w2
  91 000034  4F 00 BE 	mov.d [--w15],w0
  92 000036  00 00 F9 	pop _RCOUNT
  93 000038  00 40 06 	retfie 
  94              	.set ___PA___,0
  95              	.LFE1:
  96              	.size __USB1Interrupt,.-__USB1Interrupt
  97              	.section .debug_frame,info
  98                 	.Lframe0:
  99 0000 10 00 00 00 	.4byte .LECIE0-.LSCIE0
 100                 	.LSCIE0:
 101 0004 FF FF FF FF 	.4byte 0xffffffff
 102 0008 01          	.byte 0x1
 103 0009 00          	.byte 0
 104 000a 01          	.uleb128 0x1
 105 000b 02          	.sleb128 2
 106 000c 25          	.byte 0x25
 107 000d 12          	.byte 0x12
 108 000e 0F          	.uleb128 0xf
 109 000f 7E          	.sleb128 -2
 110 0010 09          	.byte 0x9
 111 0011 25          	.uleb128 0x25
 112 0012 0F          	.uleb128 0xf
 113 0013 00          	.align 4
 114                 	.LECIE0:
 115                 	.LSFDE0:
 116 0014 18 00 00 00 	.4byte .LEFDE0-.LASFDE0
 117                 	.LASFDE0:
MPLAB XC16 ASSEMBLY Listing:   			page 5


 118 0018 00 00 00 00 	.4byte .Lframe0
 119 001c 00 00 00 00 	.4byte .LFB0
 120 0020 16 00 00 00 	.4byte .LFE0-.LFB0
 121 0024 04          	.byte 0x4
 122 0025 02 00 00 00 	.4byte .LCFI0-.LFB0
 123 0029 13          	.byte 0x13
 124 002a 7D          	.sleb128 -3
 125 002b 0D          	.byte 0xd
 126 002c 0E          	.uleb128 0xe
 127 002d 8E          	.byte 0x8e
 128 002e 02          	.uleb128 0x2
 129 002f 00          	.align 4
 130                 	.LEFDE0:
 131                 	.LSFDE2:
 132 0030 40 00 00 00 	.4byte .LEFDE2-.LASFDE2
 133                 	.LASFDE2:
 134 0034 00 00 00 00 	.4byte .Lframe0
 135 0038 00 00 00 00 	.4byte .LFB1
 136 003c 3A 00 00 00 	.4byte .LFE1-.LFB1
 137 0040 04          	.byte 0x4
 138 0041 04 00 00 00 	.4byte .LCFI2-.LFB1
 139 0045 13          	.byte 0x13
 140 0046 7B          	.sleb128 -5
 141 0047 04          	.byte 0x4
 142 0048 02 00 00 00 	.4byte .LCFI3-.LCFI2
 143 004c 13          	.byte 0x13
 144 004d 79          	.sleb128 -7
 145 004e 04          	.byte 0x4
 146 004f 02 00 00 00 	.4byte .LCFI4-.LCFI3
 147 0053 13          	.byte 0x13
 148 0054 77          	.sleb128 -9
 149 0055 04          	.byte 0x4
 150 0056 02 00 00 00 	.4byte .LCFI5-.LCFI4
 151 005a 13          	.byte 0x13
 152 005b 75          	.sleb128 -11
 153 005c 04          	.byte 0x4
 154 005d 04 00 00 00 	.4byte .LCFI7-.LCFI5
 155 0061 86          	.byte 0x86
 156 0062 09          	.uleb128 0x9
 157 0063 84          	.byte 0x84
 158 0064 07          	.uleb128 0x7
 159 0065 82          	.byte 0x82
 160 0066 05          	.uleb128 0x5
 161 0067 80          	.byte 0x80
 162 0068 03          	.uleb128 0x3
 163 0069 04          	.byte 0x4
 164 006a 0C 00 00 00 	.4byte .LCFI8-.LCFI7
 165 006e 13          	.byte 0x13
 166 006f 72          	.sleb128 -14
 167 0070 0D          	.byte 0xd
 168 0071 0E          	.uleb128 0xe
 169 0072 8E          	.byte 0x8e
 170 0073 0D          	.uleb128 0xd
 171                 	.align 4
 172                 	.LEFDE2:
 173                 	.section .text,code
 174              	.Letext0:
MPLAB XC16 ASSEMBLY Listing:   			page 6


 175              	.file 2 "lib/lib_pic33e/usb/system.h"
 176              	.section .debug_info,info
 177 0000 0C 02 00 00 	.4byte 0x20c
 178 0004 02 00       	.2byte 0x2
 179 0006 00 00 00 00 	.4byte .Ldebug_abbrev0
 180 000a 04          	.byte 0x4
 181 000b 01          	.uleb128 0x1
 182 000c 47 4E 55 20 	.asciz "GNU C 4.5.1 (XC16, Microchip v1.36) (A) Build date: Dec  5 2018"
 182      43 20 34 2E 
 182      35 2E 31 20 
 182      28 58 43 31 
 182      36 2C 20 4D 
 182      69 63 72 6F 
 182      63 68 69 70 
 182      20 76 31 2E 
 182      33 36 29 20 
 183 004c 01          	.byte 0x1
 184 004d 6C 69 62 2F 	.asciz "lib/lib_pic33e/usb/system.c"
 184      6C 69 62 5F 
 184      70 69 63 33 
 184      33 65 2F 75 
 184      73 62 2F 73 
 184      79 73 74 65 
 184      6D 2E 63 00 
 185 0069 2F 68 6F 6D 	.asciz "/home/user/Documents/FST/Programming/project_sub-zero"
 185      65 2F 75 73 
 185      65 72 2F 44 
 185      6F 63 75 6D 
 185      65 6E 74 73 
 185      2F 46 53 54 
 185      2F 50 72 6F 
 185      67 72 61 6D 
 185      6D 69 6E 67 
 186 009f 00 00 00 00 	.4byte .Ltext0
 187 00a3 00 00 00 00 	.4byte .Letext0
 188 00a7 00 00 00 00 	.4byte .Ldebug_line0
 189 00ab 02          	.uleb128 0x2
 190 00ac 01          	.byte 0x1
 191 00ad 06          	.byte 0x6
 192 00ae 73 69 67 6E 	.asciz "signed char"
 192      65 64 20 63 
 192      68 61 72 00 
 193 00ba 02          	.uleb128 0x2
 194 00bb 02          	.byte 0x2
 195 00bc 05          	.byte 0x5
 196 00bd 69 6E 74 00 	.asciz "int"
 197 00c1 02          	.uleb128 0x2
 198 00c2 04          	.byte 0x4
 199 00c3 05          	.byte 0x5
 200 00c4 6C 6F 6E 67 	.asciz "long int"
 200      20 69 6E 74 
 200      00 
 201 00cd 02          	.uleb128 0x2
 202 00ce 08          	.byte 0x8
 203 00cf 05          	.byte 0x5
 204 00d0 6C 6F 6E 67 	.asciz "long long int"
 204      20 6C 6F 6E 
MPLAB XC16 ASSEMBLY Listing:   			page 7


 204      67 20 69 6E 
 204      74 00 
 205 00de 02          	.uleb128 0x2
 206 00df 01          	.byte 0x1
 207 00e0 08          	.byte 0x8
 208 00e1 75 6E 73 69 	.asciz "unsigned char"
 208      67 6E 65 64 
 208      20 63 68 61 
 208      72 00 
 209 00ef 02          	.uleb128 0x2
 210 00f0 02          	.byte 0x2
 211 00f1 07          	.byte 0x7
 212 00f2 75 6E 73 69 	.asciz "unsigned int"
 212      67 6E 65 64 
 212      20 69 6E 74 
 212      00 
 213 00ff 02          	.uleb128 0x2
 214 0100 04          	.byte 0x4
 215 0101 07          	.byte 0x7
 216 0102 6C 6F 6E 67 	.asciz "long unsigned int"
 216      20 75 6E 73 
 216      69 67 6E 65 
 216      64 20 69 6E 
 216      74 00 
 217 0114 02          	.uleb128 0x2
 218 0115 08          	.byte 0x8
 219 0116 07          	.byte 0x7
 220 0117 6C 6F 6E 67 	.asciz "long long unsigned int"
 220      20 6C 6F 6E 
 220      67 20 75 6E 
 220      73 69 67 6E 
 220      65 64 20 69 
 220      6E 74 00 
 221 012e 03          	.uleb128 0x3
 222 012f 02          	.byte 0x2
 223 0130 02          	.byte 0x2
 224 0131 20          	.byte 0x20
 225 0132 85 01 00 00 	.4byte 0x185
 226 0136 04          	.uleb128 0x4
 227 0137 53 59 53 54 	.asciz "SYSTEM_STATE_USB_START"
 227      45 4D 5F 53 
 227      54 41 54 45 
 227      5F 55 53 42 
 227      5F 53 54 41 
 227      52 54 00 
 228 014e 00          	.sleb128 0
 229 014f 04          	.uleb128 0x4
 230 0150 53 59 53 54 	.asciz "SYSTEM_STATE_USB_SUSPEND"
 230      45 4D 5F 53 
 230      54 41 54 45 
 230      5F 55 53 42 
 230      5F 53 55 53 
 230      50 45 4E 44 
 230      00 
 231 0169 01          	.sleb128 1
 232 016a 04          	.uleb128 0x4
 233 016b 53 59 53 54 	.asciz "SYSTEM_STATE_USB_RESUME"
MPLAB XC16 ASSEMBLY Listing:   			page 8


 233      45 4D 5F 53 
 233      54 41 54 45 
 233      5F 55 53 42 
 233      5F 52 45 53 
 233      55 4D 45 00 
 234 0183 02          	.sleb128 2
 235 0184 00          	.byte 0x0
 236 0185 05          	.uleb128 0x5
 237 0186 53 59 53 54 	.asciz "SYSTEM_STATE"
 237      45 4D 5F 53 
 237      54 41 54 45 
 237      00 
 238 0193 02          	.byte 0x2
 239 0194 24          	.byte 0x24
 240 0195 2E 01 00 00 	.4byte 0x12e
 241 0199 02          	.uleb128 0x2
 242 019a 01          	.byte 0x1
 243 019b 02          	.byte 0x2
 244 019c 5F 42 6F 6F 	.asciz "_Bool"
 244      6C 00 
 245 01a2 02          	.uleb128 0x2
 246 01a3 02          	.byte 0x2
 247 01a4 07          	.byte 0x7
 248 01a5 73 68 6F 72 	.asciz "short unsigned int"
 248      74 20 75 6E 
 248      73 69 67 6E 
 248      65 64 20 69 
 248      6E 74 00 
 249 01b8 06          	.uleb128 0x6
 250 01b9 01          	.byte 0x1
 251 01ba 53 59 53 54 	.asciz "SYSTEM_Initialize_USB"
 251      45 4D 5F 49 
 251      6E 69 74 69 
 251      61 6C 69 7A 
 251      65 5F 55 53 
 251      42 00 
 252 01d0 01          	.byte 0x1
 253 01d1 2D          	.byte 0x2d
 254 01d2 01          	.byte 0x1
 255 01d3 00 00 00 00 	.4byte .LFB0
 256 01d7 00 00 00 00 	.4byte .LFE0
 257 01db 01          	.byte 0x1
 258 01dc 5E          	.byte 0x5e
 259 01dd F2 01 00 00 	.4byte 0x1f2
 260 01e1 07          	.uleb128 0x7
 261 01e2 73 74 61 74 	.asciz "state"
 261      65 00 
 262 01e8 01          	.byte 0x1
 263 01e9 2D          	.byte 0x2d
 264 01ea 85 01 00 00 	.4byte 0x185
 265 01ee 02          	.byte 0x2
 266 01ef 7E          	.byte 0x7e
 267 01f0 00          	.sleb128 0
 268 01f1 00          	.byte 0x0
 269 01f2 08          	.uleb128 0x8
 270 01f3 01          	.byte 0x1
 271 01f4 5F 55 53 42 	.asciz "_USB1Interrupt"
MPLAB XC16 ASSEMBLY Listing:   			page 9


 271      31 49 6E 74 
 271      65 72 72 75 
 271      70 74 00 
 272 0203 01          	.byte 0x1
 273 0204 63          	.byte 0x63
 274 0205 00 00 00 00 	.4byte .LFB1
 275 0209 00 00 00 00 	.4byte .LFE1
 276 020d 01          	.byte 0x1
 277 020e 5E          	.byte 0x5e
 278 020f 00          	.byte 0x0
 279                 	.section .debug_abbrev,info
 280 0000 01          	.uleb128 0x1
 281 0001 11          	.uleb128 0x11
 282 0002 01          	.byte 0x1
 283 0003 25          	.uleb128 0x25
 284 0004 08          	.uleb128 0x8
 285 0005 13          	.uleb128 0x13
 286 0006 0B          	.uleb128 0xb
 287 0007 03          	.uleb128 0x3
 288 0008 08          	.uleb128 0x8
 289 0009 1B          	.uleb128 0x1b
 290 000a 08          	.uleb128 0x8
 291 000b 11          	.uleb128 0x11
 292 000c 01          	.uleb128 0x1
 293 000d 12          	.uleb128 0x12
 294 000e 01          	.uleb128 0x1
 295 000f 10          	.uleb128 0x10
 296 0010 06          	.uleb128 0x6
 297 0011 00          	.byte 0x0
 298 0012 00          	.byte 0x0
 299 0013 02          	.uleb128 0x2
 300 0014 24          	.uleb128 0x24
 301 0015 00          	.byte 0x0
 302 0016 0B          	.uleb128 0xb
 303 0017 0B          	.uleb128 0xb
 304 0018 3E          	.uleb128 0x3e
 305 0019 0B          	.uleb128 0xb
 306 001a 03          	.uleb128 0x3
 307 001b 08          	.uleb128 0x8
 308 001c 00          	.byte 0x0
 309 001d 00          	.byte 0x0
 310 001e 03          	.uleb128 0x3
 311 001f 04          	.uleb128 0x4
 312 0020 01          	.byte 0x1
 313 0021 0B          	.uleb128 0xb
 314 0022 0B          	.uleb128 0xb
 315 0023 3A          	.uleb128 0x3a
 316 0024 0B          	.uleb128 0xb
 317 0025 3B          	.uleb128 0x3b
 318 0026 0B          	.uleb128 0xb
 319 0027 01          	.uleb128 0x1
 320 0028 13          	.uleb128 0x13
 321 0029 00          	.byte 0x0
 322 002a 00          	.byte 0x0
 323 002b 04          	.uleb128 0x4
 324 002c 28          	.uleb128 0x28
 325 002d 00          	.byte 0x0
MPLAB XC16 ASSEMBLY Listing:   			page 10


 326 002e 03          	.uleb128 0x3
 327 002f 08          	.uleb128 0x8
 328 0030 1C          	.uleb128 0x1c
 329 0031 0D          	.uleb128 0xd
 330 0032 00          	.byte 0x0
 331 0033 00          	.byte 0x0
 332 0034 05          	.uleb128 0x5
 333 0035 16          	.uleb128 0x16
 334 0036 00          	.byte 0x0
 335 0037 03          	.uleb128 0x3
 336 0038 08          	.uleb128 0x8
 337 0039 3A          	.uleb128 0x3a
 338 003a 0B          	.uleb128 0xb
 339 003b 3B          	.uleb128 0x3b
 340 003c 0B          	.uleb128 0xb
 341 003d 49          	.uleb128 0x49
 342 003e 13          	.uleb128 0x13
 343 003f 00          	.byte 0x0
 344 0040 00          	.byte 0x0
 345 0041 06          	.uleb128 0x6
 346 0042 2E          	.uleb128 0x2e
 347 0043 01          	.byte 0x1
 348 0044 3F          	.uleb128 0x3f
 349 0045 0C          	.uleb128 0xc
 350 0046 03          	.uleb128 0x3
 351 0047 08          	.uleb128 0x8
 352 0048 3A          	.uleb128 0x3a
 353 0049 0B          	.uleb128 0xb
 354 004a 3B          	.uleb128 0x3b
 355 004b 0B          	.uleb128 0xb
 356 004c 27          	.uleb128 0x27
 357 004d 0C          	.uleb128 0xc
 358 004e 11          	.uleb128 0x11
 359 004f 01          	.uleb128 0x1
 360 0050 12          	.uleb128 0x12
 361 0051 01          	.uleb128 0x1
 362 0052 40          	.uleb128 0x40
 363 0053 0A          	.uleb128 0xa
 364 0054 01          	.uleb128 0x1
 365 0055 13          	.uleb128 0x13
 366 0056 00          	.byte 0x0
 367 0057 00          	.byte 0x0
 368 0058 07          	.uleb128 0x7
 369 0059 05          	.uleb128 0x5
 370 005a 00          	.byte 0x0
 371 005b 03          	.uleb128 0x3
 372 005c 08          	.uleb128 0x8
 373 005d 3A          	.uleb128 0x3a
 374 005e 0B          	.uleb128 0xb
 375 005f 3B          	.uleb128 0x3b
 376 0060 0B          	.uleb128 0xb
 377 0061 49          	.uleb128 0x49
 378 0062 13          	.uleb128 0x13
 379 0063 02          	.uleb128 0x2
 380 0064 0A          	.uleb128 0xa
 381 0065 00          	.byte 0x0
 382 0066 00          	.byte 0x0
MPLAB XC16 ASSEMBLY Listing:   			page 11


 383 0067 08          	.uleb128 0x8
 384 0068 2E          	.uleb128 0x2e
 385 0069 00          	.byte 0x0
 386 006a 3F          	.uleb128 0x3f
 387 006b 0C          	.uleb128 0xc
 388 006c 03          	.uleb128 0x3
 389 006d 08          	.uleb128 0x8
 390 006e 3A          	.uleb128 0x3a
 391 006f 0B          	.uleb128 0xb
 392 0070 3B          	.uleb128 0x3b
 393 0071 0B          	.uleb128 0xb
 394 0072 11          	.uleb128 0x11
 395 0073 01          	.uleb128 0x1
 396 0074 12          	.uleb128 0x12
 397 0075 01          	.uleb128 0x1
 398 0076 40          	.uleb128 0x40
 399 0077 0A          	.uleb128 0xa
 400 0078 00          	.byte 0x0
 401 0079 00          	.byte 0x0
 402 007a 00          	.byte 0x0
 403                 	.section .debug_pubnames,info
 404 0000 3B 00 00 00 	.4byte 0x3b
 405 0004 02 00       	.2byte 0x2
 406 0006 00 00 00 00 	.4byte .Ldebug_info0
 407 000a 10 02 00 00 	.4byte 0x210
 408 000e B8 01 00 00 	.4byte 0x1b8
 409 0012 53 59 53 54 	.asciz "SYSTEM_Initialize_USB"
 409      45 4D 5F 49 
 409      6E 69 74 69 
 409      61 6C 69 7A 
 409      65 5F 55 53 
 409      42 00 
 410 0028 F2 01 00 00 	.4byte 0x1f2
 411 002c 5F 55 53 42 	.asciz "_USB1Interrupt"
 411      31 49 6E 74 
 411      65 72 72 75 
 411      70 74 00 
 412 003b 00 00 00 00 	.4byte 0x0
 413                 	.section .debug_pubtypes,info
 414 0000 1F 00 00 00 	.4byte 0x1f
 415 0004 02 00       	.2byte 0x2
 416 0006 00 00 00 00 	.4byte .Ldebug_info0
 417 000a 10 02 00 00 	.4byte 0x210
 418 000e 85 01 00 00 	.4byte 0x185
 419 0012 53 59 53 54 	.asciz "SYSTEM_STATE"
 419      45 4D 5F 53 
 419      54 41 54 45 
 419      00 
 420 001f 00 00 00 00 	.4byte 0x0
 421                 	.section .debug_aranges,info
 422 0000 14 00 00 00 	.4byte 0x14
 423 0004 02 00       	.2byte 0x2
 424 0006 00 00 00 00 	.4byte .Ldebug_info0
 425 000a 04          	.byte 0x4
 426 000b 00          	.byte 0x0
 427 000c 00 00       	.2byte 0x0
 428 000e 00 00       	.2byte 0x0
MPLAB XC16 ASSEMBLY Listing:   			page 12


 429 0010 00 00 00 00 	.4byte 0x0
 430 0014 00 00 00 00 	.4byte 0x0
 431                 	.section .debug_str,info
 432                 	.section .text,code
 433              	
 434              	
 435              	
 436              	.section __c30_info,info,bss
 437                 	__psv_trap_errata:
 438                 	
 439                 	.section __c30_signature,info,data
 440 0000 01 00       	.word 0x0001
 441 0002 00 00       	.word 0x0000
 442 0004 00 00       	.word 0x0000
 443                 	
 444                 	
 445                 	
 446                 	.set ___PA___,0
 447                 	.end
MPLAB XC16 ASSEMBLY Listing:   			page 13


DEFINED SYMBOLS
                            *ABS*:00000000 /home/user/Documents/FST/Programming/project_sub-zero/lib/lib_pic33e/usb/system.c
                            *ABS*:00000001 __33EP256MU806
                            *ABS*:00000001 __dsPIC33E
                            *ABS*:00000001 __HAS_DSP
                            *ABS*:00000001 __HAS_DMAV2
                            *ABS*:00000001 __HAS_CODEGUARD
                            *ABS*:00000001 __HAS_PMP_ENHANCED
                            *ABS*:00000001 __HAS_EDS
                            *ABS*:00000001 __HAS_EP
                            *ABS*:00000011 __TARGET_DIVIDE_CYCLES
                            *ABS*:00000001 __C30ELF
    {standard input}:14     .text:00000000 _SYSTEM_Initialize_USB
    {standard input}:18     *ABS*:00000000 ___PA___
    {standard input}:26     *ABS*:00000000 ___BP___
                            *ABS*:00000040 __ext_attr_.isr.text
    {standard input}:47     .isr.text:00000000 __USB1Interrupt
    {standard input}:437    __c30_info:00000000 __psv_trap_errata
    {standard input}:53     .isr.text:00000000 .L0
    {standard input}:19     .text:00000000 .L0
                            .text:0000000e .L7
                            .text:0000000e .L1
                    .debug_abbrev:00000000 .Ldebug_abbrev0
                            .text:00000000 .Ltext0
                            .text:00000016 .Letext0
                      .debug_line:00000000 .Ldebug_line0
                            .text:00000000 .LFB0
                            .text:00000016 .LFE0
                        .isr.text:00000000 .LFB1
                        .isr.text:0000003a .LFE1
                     .debug_frame:00000000 .Lframe0
                      .debug_info:00000000 .Ldebug_info0

UNDEFINED SYMBOLS
_USBSleepOnSuspend
CORCON
_RCOUNT
_DSRPAG
_DSWPAG
__const_psvpage
_USBDeviceTasks
_SR

EQUATE SYMBOLS
/home/user/Documents/FST/Programming/project_sub-zero/lib/lib_pic33e/usb/system.c = 0x0
                  __33EP256MU806 = 0x1
                      __dsPIC33E = 0x1
                       __HAS_DSP = 0x1
                     __HAS_DMAV2 = 0x1
                 __HAS_CODEGUARD = 0x1
              __HAS_PMP_ENHANCED = 0x1
                       __HAS_EDS = 0x1
                        __HAS_EP = 0x1
          __TARGET_DIVIDE_CYCLES = 0x11
                        __C30ELF = 0x1
                        ___PA___ = 0x0
                        ___BP___ = 0x0
            __ext_attr_.isr.text = 0x40
