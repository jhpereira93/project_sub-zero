MPLAB XC16 ASSEMBLY Listing:   			page 1


   1              	.file "/home/user/Documents/FST/Programming/project_sub-zero/lib/lib_pic33e/usb/usb_device.c"
   2              	.section .debug_abbrev,info
   3                 	.Ldebug_abbrev0:
   4                 	.section .debug_info,info
   5                 	.Ldebug_info0:
   6                 	.section .debug_line,info
   7                 	.Ldebug_line0:
   8 0000 74 05 00 00 	.section .text,code
   8      02 00 1D 01 
   8      00 00 01 01 
   8      FB 0E 0A 00 
   8      01 01 01 01 
   8      00 00 00 01 
   8      6C 69 62 2F 
   8      6C 69 62 5F 
   8      70 69 63 33 
   9              	.Ltext0:
  10              	.section .nbss,bss,near
  11                 	.type _USBDeviceState,@object
  12                 	.global _USBDeviceState
  13                 	.align 2
  14 0000 00 00       	_USBDeviceState:.space 2
  15                 	.type _USBActiveConfiguration,@object
  16                 	.global _USBActiveConfiguration
  17 0002 00          	_USBActiveConfiguration:.space 1
  18                 	.section .bss,bss
  19                 	.type _USBAlternateInterface,@object
  20                 	.global _USBAlternateInterface
  21 0000 00 00       	_USBAlternateInterface:.space 2
  22                 	.section .nbss,bss,near
  23                 	.type _pBDTEntryEP0OutCurrent,@object
  24                 	.global _pBDTEntryEP0OutCurrent
  25 0003 00          	.align 2
  26 0004 00 00       	_pBDTEntryEP0OutCurrent:.space 2
  27                 	.type _pBDTEntryEP0OutNext,@object
  28                 	.global _pBDTEntryEP0OutNext
  29                 	.align 2
  30 0006 00 00       	_pBDTEntryEP0OutNext:.space 2
  31                 	.section .bss,bss
  32                 	.type _pBDTEntryOut,@object
  33                 	.global _pBDTEntryOut
  34                 	.align 2
  35 0002 00 00 00 00 	_pBDTEntryOut:.space 10
  35      00 00 00 00 
  35      00 00 
  36                 	.type _pBDTEntryIn,@object
  37                 	.global _pBDTEntryIn
  38                 	.align 2
  39 000c 00 00 00 00 	_pBDTEntryIn:.space 10
  39      00 00 00 00 
  39      00 00 
  40                 	.section .nbss,bss,near
  41                 	.type _shortPacketStatus,@object
  42                 	.global _shortPacketStatus
  43 0008 00          	_shortPacketStatus:.space 1
  44                 	.type _controlTransferState,@object
  45                 	.global _controlTransferState
MPLAB XC16 ASSEMBLY Listing:   			page 2


  46 0009 00          	_controlTransferState:.space 1
  47                 	.section .bss,bss
  48                 	.type _inPipes,@object
  49                 	.global _inPipes
  50                 	.align 2
  51 0016 00 00 00 00 	_inPipes:.space 6
  51      00 00 
  52                 	.type _outPipes,@object
  53                 	.global _outPipes
  54 001c 00 00 00 00 	_outPipes:.space 7
  54      00 00 00 
  55                 	.section .nbss,bss,near
  56                 	.type _pDst,@object
  57                 	.global _pDst
  58                 	.align 2
  59 000a 00 00       	_pDst:.space 2
  60                 	.type _RemoteWakeup,@object
  61                 	.global _RemoteWakeup
  62 000c 00          	_RemoteWakeup:.space 1
  63                 	.type _USBBusIsSuspended,@object
  64                 	.global _USBBusIsSuspended
  65 000d 00          	_USBBusIsSuspended:.space 1
  66                 	.type _USTATcopy,@object
  67                 	.global _USTATcopy
  68 000e 00          	_USTATcopy:.space 1
  69                 	.type _endpoint_number,@object
  70                 	.global _endpoint_number
  71 000f 00          	_endpoint_number:.space 1
  72                 	.type _BothEP0OutUOWNsSet,@object
  73                 	.global _BothEP0OutUOWNsSet
  74 0010 00          	_BothEP0OutUOWNsSet:.space 1
  75                 	.section .bss,bss
  76                 	.type _ep_data_in,@object
  77                 	.global _ep_data_in
  78 0023 00 00 00 00 	_ep_data_in:.space 5
  78      00 
  79                 	.type _ep_data_out,@object
  80                 	.global _ep_data_out
  81 0028 00 00 00 00 	_ep_data_out:.space 5
  81      00 
  82                 	.section .nbss,bss,near
  83                 	.type _USBStatusStageTimeoutCounter,@object
  84                 	.global _USBStatusStageTimeoutCounter
  85 0011 00          	_USBStatusStageTimeoutCounter:.space 1
  86                 	.type _USBDeferStatusStagePacket,@object
  87                 	.global _USBDeferStatusStagePacket
  88 0012 00          	_USBDeferStatusStagePacket:.space 1
  89                 	.type _USBStatusStageEnabledFlag1,@object
  90                 	.global _USBStatusStageEnabledFlag1
  91 0013 00          	_USBStatusStageEnabledFlag1:.space 1
  92                 	.type _USBStatusStageEnabledFlag2,@object
  93                 	.global _USBStatusStageEnabledFlag2
  94 0014 00          	_USBStatusStageEnabledFlag2:.space 1
  95                 	.type _USBDeferINDataStagePackets,@object
  96                 	.global _USBDeferINDataStagePackets
  97 0015 00          	_USBDeferINDataStagePackets:.space 1
  98                 	.type _USBDeferOUTDataStagePackets,@object
MPLAB XC16 ASSEMBLY Listing:   			page 3


  99                 	.global _USBDeferOUTDataStagePackets
 100 0016 00          	_USBDeferOUTDataStagePackets:.space 1
 101                 	.type _USB1msTickCount,@object
 102                 	.global _USB1msTickCount
 103 0017 00          	.align 2
 104 0018 00 00 00 00 	_USB1msTickCount:.space 4
 105                 	.type _USBTicksSinceSuspendEnd,@object
 106                 	.global _USBTicksSinceSuspendEnd
 107 001c 00          	_USBTicksSinceSuspendEnd:.space 1
 108 001d 00          	.section *_0xf61649585d16559e,bss
 109                 	.type _BDT,@object
 110                 	.global _BDT
 111                 	.align 512
 112 0000 00 00 00 00 	_BDT:.space 160
 112      00 00 00 00 
 112      00 00 00 00 
 112      00 00 00 00 
 112      00 00 00 00 
 112      00 00 00 00 
 112      00 00 00 00 
 112      00 00 00 00 
 112      00 00 00 00 
 113                 	.section .bss,bss
 114                 	.type _SetupPkt,@object
 115                 	.global _SetupPkt
 116 002d 00 00 00 00 	_SetupPkt:.space 8
 116      00 00 00 00 
 117                 	.type _CtrlTrfData,@object
 118                 	.global _CtrlTrfData
 119 0035 00 00 00 00 	_CtrlTrfData:.space 8
 119      00 00 00 00 
 120 003d 00          	.section .text,code
 121              	.align 2
 122              	.global _USBDeviceInit
 123              	.type _USBDeviceInit,@function
 124              	_USBDeviceInit:
 125              	.LFB0:
 126              	.file 1 "lib/lib_pic33e/usb/usb_device.c"
   1:lib/lib_pic33e/usb/usb_device.c **** #ifndef NOUSB
   2:lib/lib_pic33e/usb/usb_device.c **** // DOM-IGNORE-BEGIN
   3:lib/lib_pic33e/usb/usb_device.c **** /*******************************************************************************
   4:lib/lib_pic33e/usb/usb_device.c **** Copyright 2015 Microchip Technology Inc. (www.microchip.com)
   5:lib/lib_pic33e/usb/usb_device.c **** 
   6:lib/lib_pic33e/usb/usb_device.c **** Licensed under the Apache License, Version 2.0 (the "License");
   7:lib/lib_pic33e/usb/usb_device.c **** you may not use this file except in compliance with the License.
   8:lib/lib_pic33e/usb/usb_device.c **** You may obtain a copy of the License at
   9:lib/lib_pic33e/usb/usb_device.c **** 
  10:lib/lib_pic33e/usb/usb_device.c ****     http://www.apache.org/licenses/LICENSE-2.0
  11:lib/lib_pic33e/usb/usb_device.c **** 
  12:lib/lib_pic33e/usb/usb_device.c **** Unless required by applicable law or agreed to in writing, software
  13:lib/lib_pic33e/usb/usb_device.c **** distributed under the License is distributed on an "AS IS" BASIS,
  14:lib/lib_pic33e/usb/usb_device.c **** WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  15:lib/lib_pic33e/usb/usb_device.c **** See the License for the specific language governing permissions and
  16:lib/lib_pic33e/usb/usb_device.c **** limitations under the License.
  17:lib/lib_pic33e/usb/usb_device.c **** 
  18:lib/lib_pic33e/usb/usb_device.c **** To request to license the code under the MLA license (www.microchip.com/mla_license), 
  19:lib/lib_pic33e/usb/usb_device.c **** please contact mla_licensing@microchip.com
MPLAB XC16 ASSEMBLY Listing:   			page 4


  20:lib/lib_pic33e/usb/usb_device.c **** *******************************************************************************/
  21:lib/lib_pic33e/usb/usb_device.c **** //DOM-IGNORE-END
  22:lib/lib_pic33e/usb/usb_device.c **** 
  23:lib/lib_pic33e/usb/usb_device.c **** /*******************************************************************************
  24:lib/lib_pic33e/usb/usb_device.c ****   USB Device Layer
  25:lib/lib_pic33e/usb/usb_device.c **** 
  26:lib/lib_pic33e/usb/usb_device.c ****   Company:
  27:lib/lib_pic33e/usb/usb_device.c ****     Microchip Technology Inc.
  28:lib/lib_pic33e/usb/usb_device.c **** 
  29:lib/lib_pic33e/usb/usb_device.c ****   File Name:
  30:lib/lib_pic33e/usb/usb_device.c ****     usb_device.c
  31:lib/lib_pic33e/usb/usb_device.c **** 
  32:lib/lib_pic33e/usb/usb_device.c ****   Summary:
  33:lib/lib_pic33e/usb/usb_device.c ****    Provides basic USB device functionality, including enumeration and USB
  34:lib/lib_pic33e/usb/usb_device.c ****    chapter 9 required behavior.
  35:lib/lib_pic33e/usb/usb_device.c **** 
  36:lib/lib_pic33e/usb/usb_device.c ****   Description:
  37:lib/lib_pic33e/usb/usb_device.c ****    Provides basic USB device functionality, including enumeration and USB
  38:lib/lib_pic33e/usb/usb_device.c ****    chapter 9 required behavior.
  39:lib/lib_pic33e/usb/usb_device.c **** *******************************************************************************/
  40:lib/lib_pic33e/usb/usb_device.c **** 
  41:lib/lib_pic33e/usb/usb_device.c **** // *****************************************************************************
  42:lib/lib_pic33e/usb/usb_device.c **** // *****************************************************************************
  43:lib/lib_pic33e/usb/usb_device.c **** // Section: Included Files
  44:lib/lib_pic33e/usb/usb_device.c **** // *****************************************************************************
  45:lib/lib_pic33e/usb/usb_device.c **** // *****************************************************************************
  46:lib/lib_pic33e/usb/usb_device.c **** #include <xc.h>
  47:lib/lib_pic33e/usb/usb_device.c **** 
  48:lib/lib_pic33e/usb/usb_device.c **** #include <stdint.h>
  49:lib/lib_pic33e/usb/usb_device.c **** #include <stddef.h>
  50:lib/lib_pic33e/usb/usb_device.c **** #include <string.h>
  51:lib/lib_pic33e/usb/usb_device.c **** 
  52:lib/lib_pic33e/usb/usb_device.c **** #include "usb_config.h"
  53:lib/lib_pic33e/usb/usb_device.c **** 
  54:lib/lib_pic33e/usb/usb_device.c **** #include "usb.h"
  55:lib/lib_pic33e/usb/usb_device.c **** #include "usb_ch9.h"
  56:lib/lib_pic33e/usb/usb_device.c **** #include "usb_device.h"
  57:lib/lib_pic33e/usb/usb_device.c **** #include "usb_device_local.h"
  58:lib/lib_pic33e/usb/usb_device.c **** 
  59:lib/lib_pic33e/usb/usb_device.c **** #ifndef uintptr_t
  60:lib/lib_pic33e/usb/usb_device.c ****     #if  defined(__XC8__) || defined(__XC16__)
  61:lib/lib_pic33e/usb/usb_device.c ****         #define uintptr_t uint16_t
  62:lib/lib_pic33e/usb/usb_device.c ****     #elif defined (__XC32__)
  63:lib/lib_pic33e/usb/usb_device.c ****         #define uintptr_t uint32_t
  64:lib/lib_pic33e/usb/usb_device.c ****     #endif
  65:lib/lib_pic33e/usb/usb_device.c **** #endif
  66:lib/lib_pic33e/usb/usb_device.c **** 
  67:lib/lib_pic33e/usb/usb_device.c **** #if defined(USB_USE_MSD)
  68:lib/lib_pic33e/usb/usb_device.c ****     #include "usb_device_msd.h"
  69:lib/lib_pic33e/usb/usb_device.c **** #endif
  70:lib/lib_pic33e/usb/usb_device.c **** 
  71:lib/lib_pic33e/usb/usb_device.c **** // *****************************************************************************
  72:lib/lib_pic33e/usb/usb_device.c **** // *****************************************************************************
  73:lib/lib_pic33e/usb/usb_device.c **** // Section: File Scope or Global Constants
  74:lib/lib_pic33e/usb/usb_device.c **** // *****************************************************************************
  75:lib/lib_pic33e/usb/usb_device.c **** // *****************************************************************************
  76:lib/lib_pic33e/usb/usb_device.c **** #if !defined(USE_USB_BUS_SENSE_IO)
MPLAB XC16 ASSEMBLY Listing:   			page 5


  77:lib/lib_pic33e/usb/usb_device.c ****     //Assume the +5V VBUS is always present (like it would be in a bus powered
  78:lib/lib_pic33e/usb/usb_device.c ****     //only application), unless USE_USB_BUS_SENSE_IO and USB_BUS_SENSE have
  79:lib/lib_pic33e/usb/usb_device.c ****     //been properly defined elsewhere in the project.
  80:lib/lib_pic33e/usb/usb_device.c ****     #undef USB_BUS_SENSE
  81:lib/lib_pic33e/usb/usb_device.c ****     #define USB_BUS_SENSE 1
  82:lib/lib_pic33e/usb/usb_device.c **** #endif
  83:lib/lib_pic33e/usb/usb_device.c **** 
  84:lib/lib_pic33e/usb/usb_device.c **** #if defined(USB_DEVICE_DISABLE_DTS_CHECKING)
  85:lib/lib_pic33e/usb/usb_device.c ****     #define _DTS_CHECKING_ENABLED 0
  86:lib/lib_pic33e/usb/usb_device.c **** #else
  87:lib/lib_pic33e/usb/usb_device.c ****     #define _DTS_CHECKING_ENABLED _DTSEN
  88:lib/lib_pic33e/usb/usb_device.c **** #endif
  89:lib/lib_pic33e/usb/usb_device.c **** 
  90:lib/lib_pic33e/usb/usb_device.c **** #if !defined(self_power)
  91:lib/lib_pic33e/usb/usb_device.c ****     //Assume the application is always bus powered, unless self_power has been
  92:lib/lib_pic33e/usb/usb_device.c ****     //defined elsewhere in the project
  93:lib/lib_pic33e/usb/usb_device.c ****     #define self_power 0    //0 = bus powered
  94:lib/lib_pic33e/usb/usb_device.c **** #endif
  95:lib/lib_pic33e/usb/usb_device.c **** 
  96:lib/lib_pic33e/usb/usb_device.c **** #if !defined(USB_MAX_NUM_CONFIG_DSC)
  97:lib/lib_pic33e/usb/usb_device.c ****     //Assume the application only implements one configuration descriptor,
  98:lib/lib_pic33e/usb/usb_device.c ****     //unless otherwise specified elsewhere in the project
  99:lib/lib_pic33e/usb/usb_device.c ****     #define USB_MAX_NUM_CONFIG_DSC      1
 100:lib/lib_pic33e/usb/usb_device.c **** #endif
 101:lib/lib_pic33e/usb/usb_device.c **** 
 102:lib/lib_pic33e/usb/usb_device.c **** #if defined(__XC8)
 103:lib/lib_pic33e/usb/usb_device.c ****     //Suppress expected/harmless compiler warning message about unused RAM variables
 104:lib/lib_pic33e/usb/usb_device.c ****     //and certain function pointer usage.
 105:lib/lib_pic33e/usb/usb_device.c ****     //Certain variables and function pointers are not used if you don't use all
 106:lib/lib_pic33e/usb/usb_device.c ****     //of the USB stack APIs.  However, these variables should not be
 107:lib/lib_pic33e/usb/usb_device.c ****     //removed (since they are still used/needed in some applications, and this
 108:lib/lib_pic33e/usb/usb_device.c ****     //is a common file shared by many projects, some of which rely on the "unused"
 109:lib/lib_pic33e/usb/usb_device.c ****     //variables/function pointers).
 110:lib/lib_pic33e/usb/usb_device.c ****     #pragma warning disable 1090
 111:lib/lib_pic33e/usb/usb_device.c ****     #if __XC8_VERSION > 1300
 112:lib/lib_pic33e/usb/usb_device.c ****         #pragma warning disable 1471
 113:lib/lib_pic33e/usb/usb_device.c ****     #endif
 114:lib/lib_pic33e/usb/usb_device.c **** #endif
 115:lib/lib_pic33e/usb/usb_device.c **** 
 116:lib/lib_pic33e/usb/usb_device.c **** // *****************************************************************************
 117:lib/lib_pic33e/usb/usb_device.c **** // *****************************************************************************
 118:lib/lib_pic33e/usb/usb_device.c **** // Section: File Scope Data Types
 119:lib/lib_pic33e/usb/usb_device.c **** // *****************************************************************************
 120:lib/lib_pic33e/usb/usb_device.c **** // *****************************************************************************
 121:lib/lib_pic33e/usb/usb_device.c **** typedef union
 122:lib/lib_pic33e/usb/usb_device.c **** {
 123:lib/lib_pic33e/usb/usb_device.c ****     uint8_t Val;
 124:lib/lib_pic33e/usb/usb_device.c ****     struct __PACKED
 125:lib/lib_pic33e/usb/usb_device.c ****     {
 126:lib/lib_pic33e/usb/usb_device.c ****         unsigned b0:1;
 127:lib/lib_pic33e/usb/usb_device.c ****         unsigned b1:1;
 128:lib/lib_pic33e/usb/usb_device.c ****         unsigned b2:1;
 129:lib/lib_pic33e/usb/usb_device.c ****         unsigned b3:1;
 130:lib/lib_pic33e/usb/usb_device.c ****         unsigned b4:1;
 131:lib/lib_pic33e/usb/usb_device.c ****         unsigned b5:1;
 132:lib/lib_pic33e/usb/usb_device.c ****         unsigned b6:1;
 133:lib/lib_pic33e/usb/usb_device.c ****         unsigned b7:1;
MPLAB XC16 ASSEMBLY Listing:   			page 6


 134:lib/lib_pic33e/usb/usb_device.c ****     } bits;
 135:lib/lib_pic33e/usb/usb_device.c **** } uint8_t_VAL, uint8_t_BITS;
 136:lib/lib_pic33e/usb/usb_device.c **** 
 137:lib/lib_pic33e/usb/usb_device.c **** // *****************************************************************************
 138:lib/lib_pic33e/usb/usb_device.c **** // *****************************************************************************
 139:lib/lib_pic33e/usb/usb_device.c **** // Section: Variables
 140:lib/lib_pic33e/usb/usb_device.c **** // *****************************************************************************
 141:lib/lib_pic33e/usb/usb_device.c **** // *****************************************************************************
 142:lib/lib_pic33e/usb/usb_device.c **** USB_VOLATILE USB_DEVICE_STATE USBDeviceState;
 143:lib/lib_pic33e/usb/usb_device.c **** USB_VOLATILE uint8_t USBActiveConfiguration;
 144:lib/lib_pic33e/usb/usb_device.c **** USB_VOLATILE uint8_t USBAlternateInterface[USB_MAX_NUM_INT];
 145:lib/lib_pic33e/usb/usb_device.c **** volatile BDT_ENTRY *pBDTEntryEP0OutCurrent;
 146:lib/lib_pic33e/usb/usb_device.c **** volatile BDT_ENTRY *pBDTEntryEP0OutNext;
 147:lib/lib_pic33e/usb/usb_device.c **** volatile BDT_ENTRY *pBDTEntryOut[USB_MAX_EP_NUMBER+1];
 148:lib/lib_pic33e/usb/usb_device.c **** volatile BDT_ENTRY *pBDTEntryIn[USB_MAX_EP_NUMBER+1];
 149:lib/lib_pic33e/usb/usb_device.c **** USB_VOLATILE uint8_t shortPacketStatus;
 150:lib/lib_pic33e/usb/usb_device.c **** USB_VOLATILE uint8_t controlTransferState;
 151:lib/lib_pic33e/usb/usb_device.c **** USB_VOLATILE IN_PIPE inPipes[1];
 152:lib/lib_pic33e/usb/usb_device.c **** //had to replace usb_volatile by volatile, since it is declared as simply volatile on usb_hal_dspic
 153:lib/lib_pic33e/usb/usb_device.c **** volatile OUT_PIPE outPipes[1];
 154:lib/lib_pic33e/usb/usb_device.c **** USB_VOLATILE uint8_t *pDst;
 155:lib/lib_pic33e/usb/usb_device.c **** USB_VOLATILE bool RemoteWakeup;
 156:lib/lib_pic33e/usb/usb_device.c **** USB_VOLATILE bool USBBusIsSuspended;
 157:lib/lib_pic33e/usb/usb_device.c **** USB_VOLATILE USTAT_FIELDS USTATcopy;
 158:lib/lib_pic33e/usb/usb_device.c **** USB_VOLATILE uint8_t endpoint_number;
 159:lib/lib_pic33e/usb/usb_device.c **** USB_VOLATILE bool BothEP0OutUOWNsSet;
 160:lib/lib_pic33e/usb/usb_device.c **** USB_VOLATILE EP_STATUS ep_data_in[USB_MAX_EP_NUMBER+1];
 161:lib/lib_pic33e/usb/usb_device.c **** USB_VOLATILE EP_STATUS ep_data_out[USB_MAX_EP_NUMBER+1];
 162:lib/lib_pic33e/usb/usb_device.c **** USB_VOLATILE uint8_t USBStatusStageTimeoutCounter;
 163:lib/lib_pic33e/usb/usb_device.c **** volatile bool USBDeferStatusStagePacket;
 164:lib/lib_pic33e/usb/usb_device.c **** volatile bool USBStatusStageEnabledFlag1;
 165:lib/lib_pic33e/usb/usb_device.c **** volatile bool USBStatusStageEnabledFlag2;
 166:lib/lib_pic33e/usb/usb_device.c **** volatile bool USBDeferINDataStagePackets;
 167:lib/lib_pic33e/usb/usb_device.c **** volatile bool USBDeferOUTDataStagePackets;
 168:lib/lib_pic33e/usb/usb_device.c **** USB_VOLATILE uint32_t USB1msTickCount;
 169:lib/lib_pic33e/usb/usb_device.c **** USB_VOLATILE uint8_t USBTicksSinceSuspendEnd;
 170:lib/lib_pic33e/usb/usb_device.c **** 
 171:lib/lib_pic33e/usb/usb_device.c **** /** USB FIXED LOCATION VARIABLES ***********************************/
 172:lib/lib_pic33e/usb/usb_device.c **** #if defined(COMPILER_MPLAB_C18)
 173:lib/lib_pic33e/usb/usb_device.c ****     #pragma udata USB_BDT=USB_BDT_ADDRESS
 174:lib/lib_pic33e/usb/usb_device.c **** #endif
 175:lib/lib_pic33e/usb/usb_device.c **** 
 176:lib/lib_pic33e/usb/usb_device.c **** volatile BDT_ENTRY BDT[BDT_NUM_ENTRIES] BDT_BASE_ADDR_TAG;
 177:lib/lib_pic33e/usb/usb_device.c **** 
 178:lib/lib_pic33e/usb/usb_device.c **** /********************************************************************
 179:lib/lib_pic33e/usb/usb_device.c ****  * EP0 Buffer Space
 180:lib/lib_pic33e/usb/usb_device.c ****  *******************************************************************/
 181:lib/lib_pic33e/usb/usb_device.c **** volatile CTRL_TRF_SETUP SetupPkt CTRL_TRF_SETUP_ADDR_TAG;
 182:lib/lib_pic33e/usb/usb_device.c **** volatile uint8_t CtrlTrfData[USB_EP0_BUFF_SIZE] CTRL_TRF_DATA_ADDR_TAG;
 183:lib/lib_pic33e/usb/usb_device.c **** 
 184:lib/lib_pic33e/usb/usb_device.c **** /********************************************************************
 185:lib/lib_pic33e/usb/usb_device.c ****  * non-EP0 Buffer Space
 186:lib/lib_pic33e/usb/usb_device.c ****  *******************************************************************/
 187:lib/lib_pic33e/usb/usb_device.c **** #if defined(USB_USE_MSD)
 188:lib/lib_pic33e/usb/usb_device.c ****     //Check if the MSD application specific USB endpoint buffer placement address 
 189:lib/lib_pic33e/usb/usb_device.c ****     //macros have already been defined or not (ex: in a processor specific header)
 190:lib/lib_pic33e/usb/usb_device.c ****     //The msd_cbw and msd_csw buffers must be USB module accessible (and therefore
MPLAB XC16 ASSEMBLY Listing:   			page 7


 191:lib/lib_pic33e/usb/usb_device.c ****     //must be at a certain address range on certain microcontrollers).
 192:lib/lib_pic33e/usb/usb_device.c ****     #if !defined(MSD_CBW_ADDR_TAG)
 193:lib/lib_pic33e/usb/usb_device.c ****         //Not previously defined.  Assume in this case all microcontroller RAM is
 194:lib/lib_pic33e/usb/usb_device.c ****         //USB module accessible, and therefore, no specific address tag value is needed.
 195:lib/lib_pic33e/usb/usb_device.c ****         #define MSD_CBW_ADDR_TAG
 196:lib/lib_pic33e/usb/usb_device.c ****         #define MSD_CSW_ADDR_TAG
 197:lib/lib_pic33e/usb/usb_device.c ****     #endif
 198:lib/lib_pic33e/usb/usb_device.c **** 	volatile USB_MSD_CBW msd_cbw MSD_CBW_ADDR_TAG;  //Must be located in USB module accessible RAM
 199:lib/lib_pic33e/usb/usb_device.c **** 	volatile USB_MSD_CSW msd_csw MSD_CSW_ADDR_TAG;  //Must be located in USB module accessible RAM
 200:lib/lib_pic33e/usb/usb_device.c **** 
 201:lib/lib_pic33e/usb/usb_device.c ****     #if defined(__18CXX) || defined(__XC8)
 202:lib/lib_pic33e/usb/usb_device.c ****         volatile char msd_buffer[512] @ MSD_BUFFER_ADDRESS;
 203:lib/lib_pic33e/usb/usb_device.c ****     #else
 204:lib/lib_pic33e/usb/usb_device.c ****         volatile char msd_buffer[512];
 205:lib/lib_pic33e/usb/usb_device.c **** 	#endif
 206:lib/lib_pic33e/usb/usb_device.c **** #endif
 207:lib/lib_pic33e/usb/usb_device.c **** 
 208:lib/lib_pic33e/usb/usb_device.c **** //Depricated in v2.2 - will be removed in a future revision
 209:lib/lib_pic33e/usb/usb_device.c **** #if !defined(USB_USER_DEVICE_DESCRIPTOR)
 210:lib/lib_pic33e/usb/usb_device.c ****     //Device descriptor
 211:lib/lib_pic33e/usb/usb_device.c ****     extern const USB_DEVICE_DESCRIPTOR device_dsc;
 212:lib/lib_pic33e/usb/usb_device.c **** #else
 213:lib/lib_pic33e/usb/usb_device.c ****     USB_USER_DEVICE_DESCRIPTOR_INCLUDE;
 214:lib/lib_pic33e/usb/usb_device.c **** #endif
 215:lib/lib_pic33e/usb/usb_device.c **** 
 216:lib/lib_pic33e/usb/usb_device.c **** #if !defined(USB_USER_CONFIG_DESCRIPTOR)
 217:lib/lib_pic33e/usb/usb_device.c ****     //Array of configuration descriptors
 218:lib/lib_pic33e/usb/usb_device.c ****     extern const uint8_t *const USB_CD_Ptr[];
 219:lib/lib_pic33e/usb/usb_device.c **** #else
 220:lib/lib_pic33e/usb/usb_device.c ****     USB_USER_CONFIG_DESCRIPTOR_INCLUDE;
 221:lib/lib_pic33e/usb/usb_device.c **** #endif
 222:lib/lib_pic33e/usb/usb_device.c **** 
 223:lib/lib_pic33e/usb/usb_device.c **** extern const uint8_t *const USB_SD_Ptr[];
 224:lib/lib_pic33e/usb/usb_device.c **** 
 225:lib/lib_pic33e/usb/usb_device.c **** 
 226:lib/lib_pic33e/usb/usb_device.c **** // *****************************************************************************
 227:lib/lib_pic33e/usb/usb_device.c **** // *****************************************************************************
 228:lib/lib_pic33e/usb/usb_device.c **** // Section: Private and External Prototypes
 229:lib/lib_pic33e/usb/usb_device.c **** // *****************************************************************************
 230:lib/lib_pic33e/usb/usb_device.c **** // *****************************************************************************
 231:lib/lib_pic33e/usb/usb_device.c **** extern bool USER_USB_CALLBACK_EVENT_HANDLER(USB_EVENT event, void *pdata, uint16_t size);
 232:lib/lib_pic33e/usb/usb_device.c **** 
 233:lib/lib_pic33e/usb/usb_device.c **** static void USBCtrlEPService(void);
 234:lib/lib_pic33e/usb/usb_device.c **** static void USBCtrlTrfSetupHandler(void);
 235:lib/lib_pic33e/usb/usb_device.c **** static void USBCtrlTrfInHandler(void);
 236:lib/lib_pic33e/usb/usb_device.c **** static void USBCheckStdRequest(void);
 237:lib/lib_pic33e/usb/usb_device.c **** static void USBStdGetDscHandler(void);
 238:lib/lib_pic33e/usb/usb_device.c **** static void USBCtrlEPServiceComplete(void);
 239:lib/lib_pic33e/usb/usb_device.c **** static void USBCtrlTrfTxService(void);
 240:lib/lib_pic33e/usb/usb_device.c **** static void USBCtrlTrfRxService(void);
 241:lib/lib_pic33e/usb/usb_device.c **** static void USBStdSetCfgHandler(void);
 242:lib/lib_pic33e/usb/usb_device.c **** static void USBStdGetStatusHandler(void);
 243:lib/lib_pic33e/usb/usb_device.c **** static void USBStdFeatureReqHandler(void);
 244:lib/lib_pic33e/usb/usb_device.c **** static void USBCtrlTrfOutHandler(void);
 245:lib/lib_pic33e/usb/usb_device.c **** static void USBConfigureEndpoint(uint8_t EPNum, uint8_t direction);
 246:lib/lib_pic33e/usb/usb_device.c **** static void USBWakeFromSuspend(void);
 247:lib/lib_pic33e/usb/usb_device.c **** static void USBSuspend(void);
MPLAB XC16 ASSEMBLY Listing:   			page 8


 248:lib/lib_pic33e/usb/usb_device.c **** static void USBStallHandler(void);
 249:lib/lib_pic33e/usb/usb_device.c **** 
 250:lib/lib_pic33e/usb/usb_device.c **** // *****************************************************************************
 251:lib/lib_pic33e/usb/usb_device.c **** // *****************************************************************************
 252:lib/lib_pic33e/usb/usb_device.c **** // Section: Macros or Functions
 253:lib/lib_pic33e/usb/usb_device.c **** // *****************************************************************************
 254:lib/lib_pic33e/usb/usb_device.c **** // *****************************************************************************
 255:lib/lib_pic33e/usb/usb_device.c **** 
 256:lib/lib_pic33e/usb/usb_device.c **** /**************************************************************************
 257:lib/lib_pic33e/usb/usb_device.c ****     Function:
 258:lib/lib_pic33e/usb/usb_device.c ****         void USBDeviceInit(void)
 259:lib/lib_pic33e/usb/usb_device.c ****     
 260:lib/lib_pic33e/usb/usb_device.c ****     Description:
 261:lib/lib_pic33e/usb/usb_device.c ****         This function initializes the device stack it in the default state. The
 262:lib/lib_pic33e/usb/usb_device.c ****         USB module will be completely reset including all of the internal
 263:lib/lib_pic33e/usb/usb_device.c ****         variables, registers, and interrupt flags.
 264:lib/lib_pic33e/usb/usb_device.c ****                 
 265:lib/lib_pic33e/usb/usb_device.c ****     Precondition:
 266:lib/lib_pic33e/usb/usb_device.c ****         This function must be called before any of the other USB Device
 267:lib/lib_pic33e/usb/usb_device.c ****         functions can be called, including USBDeviceTasks().
 268:lib/lib_pic33e/usb/usb_device.c ****         
 269:lib/lib_pic33e/usb/usb_device.c ****     Parameters:
 270:lib/lib_pic33e/usb/usb_device.c ****         None
 271:lib/lib_pic33e/usb/usb_device.c ****      
 272:lib/lib_pic33e/usb/usb_device.c ****     Return Values:
 273:lib/lib_pic33e/usb/usb_device.c ****         None
 274:lib/lib_pic33e/usb/usb_device.c ****         
 275:lib/lib_pic33e/usb/usb_device.c ****     Remarks:
 276:lib/lib_pic33e/usb/usb_device.c ****         None
 277:lib/lib_pic33e/usb/usb_device.c ****                                                           
 278:lib/lib_pic33e/usb/usb_device.c ****   ***************************************************************************/
 279:lib/lib_pic33e/usb/usb_device.c **** void USBDeviceInit(void)
 280:lib/lib_pic33e/usb/usb_device.c **** {
 127              	.loc 1 280 0
 128              	.set ___PA___,1
 129 000000  02 00 FA 	lnk #2
 130              	.LCFI0:
 131 000002  88 1F 78 	mov w8,[w15++]
 132              	.LCFI1:
 281:lib/lib_pic33e/usb/usb_device.c ****     uint8_t i;
 282:lib/lib_pic33e/usb/usb_device.c **** 
 283:lib/lib_pic33e/usb/usb_device.c ****     USBDisableInterrupts();
 133              	.loc 1 283 0
 134 000004  00 C0 A9 	bclr.b _IEC5bits,#6
 284:lib/lib_pic33e/usb/usb_device.c **** 
 285:lib/lib_pic33e/usb/usb_device.c ****     //Make sure that if a GPIO output driver exists on VBUS, that it is 
 286:lib/lib_pic33e/usb/usb_device.c ****     //tri-stated to avoid potential contention with the host
 287:lib/lib_pic33e/usb/usb_device.c ****     USB_HAL_VBUSTristate();
 288:lib/lib_pic33e/usb/usb_device.c ****     
 289:lib/lib_pic33e/usb/usb_device.c ****     // Clear all USB error flags
 290:lib/lib_pic33e/usb/usb_device.c ****     USBClearInterruptRegister(U1EIR);  
 135              	.loc 1 290 0
 136 000006  80 80 EB 	setm w1
 291:lib/lib_pic33e/usb/usb_device.c ****        
 292:lib/lib_pic33e/usb/usb_device.c ****     // Clears all USB interrupts          
 293:lib/lib_pic33e/usb/usb_device.c ****     USBClearInterruptRegister(U1IR); 
 137              	.loc 1 293 0
MPLAB XC16 ASSEMBLY Listing:   			page 9


 138 000008  00 80 EB 	setm w0
 139              	.loc 1 290 0
 140 00000a  01 00 88 	mov w1,_U1EIR
 294:lib/lib_pic33e/usb/usb_device.c **** 
 295:lib/lib_pic33e/usb/usb_device.c ****     //Clear all of the endpoint control registers
 296:lib/lib_pic33e/usb/usb_device.c ****     U1EP0 = 0;
 297:lib/lib_pic33e/usb/usb_device.c ****     
 298:lib/lib_pic33e/usb/usb_device.c ****     DisableNonZeroEndpoints(USB_MAX_EP_NUMBER);
 141              	.loc 1 298 0
 142 00000c  82 00 20 	mov #8,w2
 143              	.loc 1 293 0
 144 00000e  00 00 88 	mov w0,_U1IR
 145              	.loc 1 298 0
 146 000010  80 00 EB 	clr w1
 147              	.loc 1 296 0
 148 000012  00 20 EF 	clr _U1EP0
 149              	.loc 1 298 0
 150 000014  00 00 20 	mov #_U1EP1,w0
 151 000016  00 00 07 	rcall _memset
 299:lib/lib_pic33e/usb/usb_device.c **** 
 300:lib/lib_pic33e/usb/usb_device.c ****     SetConfigurationOptions();
 152              	.loc 1 300 0
 153 000018  00 20 EF 	clr _U1CNFG1
 154 00001a  F2 09 20 	mov #159,w2
 155 00001c  00 20 EF 	clr _U1CNFG2
 156 00001e  F1 09 20 	mov #159,w1
 157 000020  00 40 A9 	bclr.b _U1OTGCONbits,#2
 301:lib/lib_pic33e/usb/usb_device.c **** 
 302:lib/lib_pic33e/usb/usb_device.c ****     //power up the module (if not already powered)
 303:lib/lib_pic33e/usb/usb_device.c ****     USBPowerModule();
 304:lib/lib_pic33e/usb/usb_device.c **** 
 305:lib/lib_pic33e/usb/usb_device.c ****     //set the address of the BDT (if applicable)
 306:lib/lib_pic33e/usb/usb_device.c ****     USBSetBDTAddress(BDT);
 158              	.loc 1 306 0
 159 000022  00 00 20 	mov #_BDT,w0
 160              	.loc 1 300 0
 161 000024  02 00 88 	mov w2,_U1EIE
 162              	.loc 1 306 0
 163 000026  48 00 DE 	lsr w0,#8,w0
 164              	.loc 1 300 0
 165 000028  01 00 88 	mov w1,_U1IE
 307:lib/lib_pic33e/usb/usb_device.c **** 
 308:lib/lib_pic33e/usb/usb_device.c ****     //Clear all of the BDT entries
 309:lib/lib_pic33e/usb/usb_device.c ****     for(i = 0; i < (sizeof(BDT)/sizeof(BDT_ENTRY)); i++)
 166              	.loc 1 309 0
 167 00002a  80 40 EB 	clr.b w1
 168              	.loc 1 300 0
 169 00002c  00 C0 A8 	bset.b _U1OTGIEbits,#6
 170              	.loc 1 309 0
 171 00002e  01 4F 78 	mov.b w1,[w14]
 172              	.loc 1 303 0
 173 000030  00 00 A8 	bset.b _U1PWRCbits,#0
 174              	.loc 1 306 0
 175 000032  00 00 88 	mov w0,_U1BDTP1
 176              	.loc 1 309 0
 177 000034  00 00 37 	bra .L2
 178              	.L3:
MPLAB XC16 ASSEMBLY Listing:   			page 10


 310:lib/lib_pic33e/usb/usb_device.c ****     {
 311:lib/lib_pic33e/usb/usb_device.c ****         BDT[i].Val = 0x00;
 179              	.loc 1 311 0
 180 000036  1E 80 FB 	ze [w14],w0
 181 000038  02 00 20 	mov #_BDT,w2
 182 00003a  C3 01 DD 	sl w0,#3,w3
 183 00003c  60 00 B8 	mul.uu w0,#0,w0
 184 00003e  02 81 41 	add w3,w2,w2
 185              	.loc 1 309 0
 186 000040  1E 4F E8 	inc.b [w14],[w14]
 187              	.loc 1 311 0
 188 000042  00 89 BE 	mov.d w0,[w2]
 189              	.L2:
 190              	.loc 1 309 0
 191 000044  1E 40 78 	mov.b [w14],w0
 192 000046  F3 4F 50 	sub.b w0,#19,[w15]
 193              	.set ___BP___,0
 194 000048  00 00 36 	bra leu,.L3
 312:lib/lib_pic33e/usb/usb_device.c ****     }
 313:lib/lib_pic33e/usb/usb_device.c **** 
 314:lib/lib_pic33e/usb/usb_device.c ****     // Assert reset request to all of the Ping Pong buffer pointers
 315:lib/lib_pic33e/usb/usb_device.c ****     USBPingPongBufferReset = 1;                    
 195              	.loc 1 315 0
 196 00004a  00 20 A8 	bset.b _U1CONbits,#1
 316:lib/lib_pic33e/usb/usb_device.c **** 
 317:lib/lib_pic33e/usb/usb_device.c ****     // Reset to default address
 318:lib/lib_pic33e/usb/usb_device.c ****     U1ADDR = 0x00;                   
 197              	.loc 1 318 0
 198 00004c  00 20 EF 	clr _U1ADDR
 319:lib/lib_pic33e/usb/usb_device.c **** 
 320:lib/lib_pic33e/usb/usb_device.c ****     // Make sure packet processing is enabled
 321:lib/lib_pic33e/usb/usb_device.c ****     USBPacketDisable = 0;           
 199              	.loc 1 321 0
 200 00004e  00 A0 A9 	bclr.b _U1CONbits,#5
 322:lib/lib_pic33e/usb/usb_device.c **** 
 323:lib/lib_pic33e/usb/usb_device.c ****     //Stop trying to reset ping pong buffer pointers
 324:lib/lib_pic33e/usb/usb_device.c ****     USBPingPongBufferReset = 0;
 201              	.loc 1 324 0
 202 000050  00 20 A9 	bclr.b _U1CONbits,#1
 203              	.L4:
 325:lib/lib_pic33e/usb/usb_device.c **** 
 326:lib/lib_pic33e/usb/usb_device.c ****     // Flush any pending transactions
 327:lib/lib_pic33e/usb/usb_device.c ****     do
 328:lib/lib_pic33e/usb/usb_device.c ****     {
 329:lib/lib_pic33e/usb/usb_device.c ****         USBClearInterruptFlag(USBTransactionCompleteIFReg,USBTransactionCompleteIFBitNum);
 330:lib/lib_pic33e/usb/usb_device.c ****         //Initialize USB stack software state variables
 331:lib/lib_pic33e/usb/usb_device.c ****         inPipes[0].info.Val = 0;
 332:lib/lib_pic33e/usb/usb_device.c ****         outPipes[0].info.Val = 0;
 333:lib/lib_pic33e/usb/usb_device.c ****         outPipes[0].wCount.Val = 0;
 204              	.loc 1 333 0
 205 000052  31 00 20 	mov #_outPipes+3,w1
 206 000054  40 00 20 	mov #_outPipes+4,w0
 207              	.loc 1 329 0
 208 000056  82 00 20 	mov #8,w2
 209              	.loc 1 331 0
 210 000058  23 00 20 	mov #_inPipes+2,w3
 211              	.loc 1 329 0
MPLAB XC16 ASSEMBLY Listing:   			page 11


 212 00005a  02 00 88 	mov w2,_U1IR
 213              	.loc 1 331 0
 214 00005c  00 42 EB 	clr.b w4
 215              	.loc 1 332 0
 216 00005e  22 00 20 	mov #_outPipes+2,w2
 217              	.loc 1 331 0
 218 000060  84 49 78 	mov.b w4,[w3]
 219              	.loc 1 332 0
 220 000062  80 41 EB 	clr.b w3
 221 000064  03 49 78 	mov.b w3,[w2]
 222              	.loc 1 333 0
 223 000066  11 41 78 	mov.b [w1],w2
 224 000068  60 41 61 	and.b w2,#0,w2
 225 00006a  82 48 78 	mov.b w2,[w1]
 226 00006c  90 40 78 	mov.b [w0],w1
 227 00006e  E0 C0 60 	and.b w1,#0,w1
 228 000070  01 48 78 	mov.b w1,[w0]
 334:lib/lib_pic33e/usb/usb_device.c ****     }while(USBTransactionCompleteIF == 1);
 229              	.loc 1 334 0
 230 000072  00 00 80 	mov _U1IRbits,w0
 231 000074  68 00 60 	and w0,#8,w0
 232 000076  00 00 E0 	cp0 w0
 233              	.set ___BP___,0
 234 000078  00 00 3A 	bra nz,.L4
 335:lib/lib_pic33e/usb/usb_device.c **** 
 336:lib/lib_pic33e/usb/usb_device.c ****     //Set flags to true, so the USBCtrlEPAllowStatusStage() function knows not to
 337:lib/lib_pic33e/usb/usb_device.c ****     //try and arm a status stage, even before the first control transfer starts.
 338:lib/lib_pic33e/usb/usb_device.c ****     USBStatusStageEnabledFlag1 = true;
 235              	.loc 1 338 0
 236 00007a  10 C0 B3 	mov.b #1,w0
 339:lib/lib_pic33e/usb/usb_device.c ****     USBStatusStageEnabledFlag2 = true;
 237              	.loc 1 339 0
 238 00007c  11 C0 B3 	mov.b #1,w1
 239              	.loc 1 338 0
 240 00007e  00 E0 B7 	mov.b WREG,_USBStatusStageEnabledFlag1
 340:lib/lib_pic33e/usb/usb_device.c ****     //Initialize other flags
 341:lib/lib_pic33e/usb/usb_device.c ****     USBDeferINDataStagePackets = false;
 342:lib/lib_pic33e/usb/usb_device.c ****     USBDeferOUTDataStagePackets = false;
 343:lib/lib_pic33e/usb/usb_device.c ****     USBBusIsSuspended = false;
 344:lib/lib_pic33e/usb/usb_device.c **** 
 345:lib/lib_pic33e/usb/usb_device.c ****     //Initialize all pBDTEntryIn[] and pBDTEntryOut[]
 346:lib/lib_pic33e/usb/usb_device.c ****     //pointers to NULL, so they don't get used inadvertently.
 347:lib/lib_pic33e/usb/usb_device.c ****     for(i = 0; i < (uint8_t)(USB_MAX_EP_NUMBER+1u); i++)
 241              	.loc 1 347 0
 242 000080  00 40 EB 	clr.b w0
 243              	.loc 1 339 0
 244 000082  81 41 78 	mov.b w1,w3
 245 000084  02 00 20 	mov #_USBStatusStageEnabledFlag2,w2
 246 000086  03 49 78 	mov.b w3,[w2]
 247              	.loc 1 347 0
 248 000088  00 4F 78 	mov.b w0,[w14]
 249              	.loc 1 341 0
 250 00008a  00 60 EF 	clr.b _USBDeferINDataStagePackets
 251              	.loc 1 342 0
 252 00008c  00 60 EF 	clr.b _USBDeferOUTDataStagePackets
 253              	.loc 1 343 0
 254 00008e  00 60 EF 	clr.b _USBBusIsSuspended
MPLAB XC16 ASSEMBLY Listing:   			page 12


 255              	.loc 1 347 0
 256 000090  00 00 37 	bra .L5
 257              	.L6:
 348:lib/lib_pic33e/usb/usb_device.c ****     {
 349:lib/lib_pic33e/usb/usb_device.c ****         pBDTEntryIn[i] = 0u;
 258              	.loc 1 349 0
 259 000092  9E 80 FB 	ze [w14],w1
 350:lib/lib_pic33e/usb/usb_device.c ****         pBDTEntryOut[i] = 0u;
 260              	.loc 1 350 0
 261 000094  1E 80 FB 	ze [w14],w0
 351:lib/lib_pic33e/usb/usb_device.c ****         ep_data_in[i].Val = 0u;
 262              	.loc 1 351 0
 263 000096  1E 84 FB 	ze [w14],w8
 352:lib/lib_pic33e/usb/usb_device.c ****         ep_data_out[i].Val = 0u;
 264              	.loc 1 352 0
 265 000098  1E 83 FB 	ze [w14],w6
 266              	.loc 1 347 0
 267 00009a  1E 4F E8 	inc.b [w14],[w14]
 268              	.loc 1 349 0
 269 00009c  01 81 40 	add w1,w1,w2
 270 00009e  01 00 20 	mov #_pBDTEntryIn,w1
 271 0000a0  80 02 EB 	clr w5
 272 0000a2  01 02 41 	add w2,w1,w4
 273              	.loc 1 350 0
 274 0000a4  80 00 40 	add w0,w0,w1
 275 0000a6  00 00 20 	mov #_pBDTEntryOut,w0
 276 0000a8  80 01 EB 	clr w3
 277 0000aa  00 81 40 	add w1,w0,w2
 278              	.loc 1 351 0
 279 0000ac  00 00 20 	mov #_ep_data_in,w0
 280 0000ae  80 43 EB 	clr.b w7
 281 0000b0  88 00 40 	add w0,w8,w1
 282              	.loc 1 352 0
 283 0000b2  00 00 20 	mov #_ep_data_out,w0
 284              	.loc 1 351 0
 285 0000b4  87 48 78 	mov.b w7,[w1]
 286              	.loc 1 352 0
 287 0000b6  06 00 40 	add w0,w6,w0
 288 0000b8  80 40 EB 	clr.b w1
 289              	.loc 1 349 0
 290 0000ba  05 0A 78 	mov w5,[w4]
 291              	.loc 1 350 0
 292 0000bc  03 09 78 	mov w3,[w2]
 293              	.loc 1 352 0
 294 0000be  01 48 78 	mov.b w1,[w0]
 295              	.L5:
 296              	.loc 1 347 0
 297 0000c0  1E 40 78 	mov.b [w14],w0
 298 0000c2  E4 4F 50 	sub.b w0,#4,[w15]
 299              	.set ___BP___,0
 300 0000c4  00 00 36 	bra leu,.L6
 353:lib/lib_pic33e/usb/usb_device.c ****     }
 354:lib/lib_pic33e/usb/usb_device.c **** 
 355:lib/lib_pic33e/usb/usb_device.c ****     //Get ready for the first packet
 356:lib/lib_pic33e/usb/usb_device.c ****     pBDTEntryIn[0] = (volatile BDT_ENTRY*)&BDT[EP0_IN_EVEN];
 301              	.loc 1 356 0
 302 0000c6  03 01 20 	mov #_BDT+16,w3
MPLAB XC16 ASSEMBLY Listing:   			page 13


 357:lib/lib_pic33e/usb/usb_device.c ****     // Initialize EP0 as a Ctrl EP
 358:lib/lib_pic33e/usb/usb_device.c ****     U1EP0 = EP_CTRL|USB_HANDSHAKE_ENABLED;        
 303              	.loc 1 358 0
 304 0000c8  D1 00 20 	mov #13,w1
 359:lib/lib_pic33e/usb/usb_device.c **** 	//Prepare for the first SETUP on EP0 OUT
 360:lib/lib_pic33e/usb/usb_device.c ****     BDT[EP0_OUT_EVEN].ADR = ConvertToPhysicalAddress(&SetupPkt);
 305              	.loc 1 360 0
 306 0000ca  00 00 20 	mov #_SetupPkt,w0
 307              	.loc 1 358 0
 308 0000cc  01 00 88 	mov w1,_U1EP0
 361:lib/lib_pic33e/usb/usb_device.c ****     BDT[EP0_OUT_EVEN].CNT = USB_EP0_BUFF_SIZE;
 309              	.loc 1 361 0
 310 0000ce  01 C0 2F 	mov #-1024,w1
 311              	.loc 1 360 0
 312 0000d0  20 00 88 	mov w0,_BDT+4
 362:lib/lib_pic33e/usb/usb_device.c ****     BDT[EP0_OUT_EVEN].STAT.Val = _DAT0|_BSTALL;
 313              	.loc 1 362 0
 314 0000d2  40 00 20 	mov #4,w0
 315              	.loc 1 361 0
 316 0000d4  12 00 80 	mov _BDT+2,w2
 317              	.loc 1 356 0
 318 0000d6  03 00 88 	mov w3,_pBDTEntryIn
 319              	.loc 1 361 0
 320 0000d8  81 00 61 	and w2,w1,w1
 321 0000da  01 30 A0 	bset w1,#3
 322 0000dc  11 00 88 	mov w1,_BDT+2
 323              	.loc 1 362 0
 324 0000de  00 00 88 	mov w0,_BDT
 363:lib/lib_pic33e/usb/usb_device.c ****     BDT[EP0_OUT_EVEN].STAT.Val |= _USIE;
 325              	.loc 1 363 0
 326 0000e0  00 00 80 	mov _BDT,w0
 327 0000e2  00 70 A0 	bset w0,#7
 328 0000e4  00 00 88 	mov w0,_BDT
 364:lib/lib_pic33e/usb/usb_device.c **** 
 365:lib/lib_pic33e/usb/usb_device.c ****     // Clear active configuration
 366:lib/lib_pic33e/usb/usb_device.c ****     USBActiveConfiguration = 0;     
 329              	.loc 1 366 0
 330 0000e6  00 60 EF 	clr.b _USBActiveConfiguration
 367:lib/lib_pic33e/usb/usb_device.c **** 
 368:lib/lib_pic33e/usb/usb_device.c ****     USB1msTickCount = 0;            //Keeps track of total number of milliseconds since calling USB
 331              	.loc 1 368 0
 332 0000e8  00 20 EF 	clr _USB1msTickCount
 333 0000ea  02 20 EF 	clr _USB1msTickCount+2
 369:lib/lib_pic33e/usb/usb_device.c ****     USBTicksSinceSuspendEnd = 0;    //Keeps track of the number of milliseconds since a suspend con
 334              	.loc 1 369 0
 335 0000ec  00 60 EF 	clr.b _USBTicksSinceSuspendEnd
 370:lib/lib_pic33e/usb/usb_device.c **** 
 371:lib/lib_pic33e/usb/usb_device.c ****     //Indicate that we are now in the detached state
 372:lib/lib_pic33e/usb/usb_device.c ****     USBDeviceState = DETACHED_STATE;
 336              	.loc 1 372 0
 337 0000ee  00 20 EF 	clr _USBDeviceState
 373:lib/lib_pic33e/usb/usb_device.c **** }
 338              	.loc 1 373 0
 339 0000f0  4F 04 78 	mov [--w15],w8
 340 0000f2  8E 07 78 	mov w14,w15
 341 0000f4  4F 07 78 	mov [--w15],w14
 342 0000f6  00 40 A9 	bclr CORCON,#2
MPLAB XC16 ASSEMBLY Listing:   			page 14


 343 0000f8  00 00 06 	return 
 344              	.set ___PA___,0
 345              	.LFE0:
 346              	.size _USBDeviceInit,.-_USBDeviceInit
 347              	.align 2
 348              	.global _USBDeviceTasks
 349              	.type _USBDeviceTasks,@function
 350              	_USBDeviceTasks:
 351              	.LFB1:
 374:lib/lib_pic33e/usb/usb_device.c **** 
 375:lib/lib_pic33e/usb/usb_device.c **** 
 376:lib/lib_pic33e/usb/usb_device.c **** 
 377:lib/lib_pic33e/usb/usb_device.c **** /**************************************************************************
 378:lib/lib_pic33e/usb/usb_device.c ****   Function:
 379:lib/lib_pic33e/usb/usb_device.c ****         void USBDeviceTasks(void)
 380:lib/lib_pic33e/usb/usb_device.c ****     
 381:lib/lib_pic33e/usb/usb_device.c ****   Summary:
 382:lib/lib_pic33e/usb/usb_device.c ****     This function is the main state machine/transaction handler of the USB 
 383:lib/lib_pic33e/usb/usb_device.c ****     device side stack.  When the USB stack is operated in "USB_POLLING" mode 
 384:lib/lib_pic33e/usb/usb_device.c ****     (usb_config.h user option) the USBDeviceTasks() function should be called 
 385:lib/lib_pic33e/usb/usb_device.c ****     periodically to receive and transmit packets through the stack. This 
 386:lib/lib_pic33e/usb/usb_device.c ****     function also takes care of control transfers associated with the USB 
 387:lib/lib_pic33e/usb/usb_device.c ****     enumeration process, and detecting various USB events (such as suspend).  
 388:lib/lib_pic33e/usb/usb_device.c ****     This function should be called at least once every 1.8ms during the USB 
 389:lib/lib_pic33e/usb/usb_device.c ****     enumeration process. After the enumeration process is complete (which can 
 390:lib/lib_pic33e/usb/usb_device.c ****     be determined when USBGetDeviceState() returns CONFIGURED_STATE), the 
 391:lib/lib_pic33e/usb/usb_device.c ****     USBDeviceTasks() handler may be called the faster of: either once 
 392:lib/lib_pic33e/usb/usb_device.c ****     every 9.8ms, or as often as needed to make sure that the hardware USTAT 
 393:lib/lib_pic33e/usb/usb_device.c ****     FIFO never gets full.  A good rule of thumb is to call USBDeviceTasks() at
 394:lib/lib_pic33e/usb/usb_device.c ****     a minimum rate of either the frequency that USBTransferOnePacket() gets 
 395:lib/lib_pic33e/usb/usb_device.c ****     called, or, once/1.8ms, whichever is faster.  See the inline code comments 
 396:lib/lib_pic33e/usb/usb_device.c ****     near the top of usb_device.c for more details about minimum timing 
 397:lib/lib_pic33e/usb/usb_device.c ****     requirements when calling USBDeviceTasks().
 398:lib/lib_pic33e/usb/usb_device.c ****     
 399:lib/lib_pic33e/usb/usb_device.c ****     When the USB stack is operated in "USB_INTERRUPT" mode, it is not necessary
 400:lib/lib_pic33e/usb/usb_device.c ****     to call USBDeviceTasks() from the main loop context.  In the USB_INTERRUPT
 401:lib/lib_pic33e/usb/usb_device.c ****     mode, the USBDeviceTasks() handler only needs to execute when a USB 
 402:lib/lib_pic33e/usb/usb_device.c ****     interrupt occurs, and therefore only needs to be called from the interrupt 
 403:lib/lib_pic33e/usb/usb_device.c ****     context.
 404:lib/lib_pic33e/usb/usb_device.c **** 
 405:lib/lib_pic33e/usb/usb_device.c ****   Description:
 406:lib/lib_pic33e/usb/usb_device.c ****     This function is the main state machine/transaction handler of the USB 
 407:lib/lib_pic33e/usb/usb_device.c ****     device side stack.  When the USB stack is operated in "USB_POLLING" mode 
 408:lib/lib_pic33e/usb/usb_device.c ****     (usb_config.h user option) the USBDeviceTasks() function should be called 
 409:lib/lib_pic33e/usb/usb_device.c ****     periodically to receive and transmit packets through the stack. This 
 410:lib/lib_pic33e/usb/usb_device.c ****     function also takes care of control transfers associated with the USB 
 411:lib/lib_pic33e/usb/usb_device.c ****     enumeration process, and detecting various USB events (such as suspend).  
 412:lib/lib_pic33e/usb/usb_device.c ****     This function should be called at least once every 1.8ms during the USB 
 413:lib/lib_pic33e/usb/usb_device.c ****     enumeration process. After the enumeration process is complete (which can 
 414:lib/lib_pic33e/usb/usb_device.c ****     be determined when USBGetDeviceState() returns CONFIGURED_STATE), the 
 415:lib/lib_pic33e/usb/usb_device.c ****     USBDeviceTasks() handler may be called the faster of: either once 
 416:lib/lib_pic33e/usb/usb_device.c ****     every 9.8ms, or as often as needed to make sure that the hardware USTAT 
 417:lib/lib_pic33e/usb/usb_device.c ****     FIFO never gets full.  A good rule of thumb is to call USBDeviceTasks() at
 418:lib/lib_pic33e/usb/usb_device.c ****     a minimum rate of either the frequency that USBTransferOnePacket() gets 
 419:lib/lib_pic33e/usb/usb_device.c ****     called, or, once/1.8ms, whichever is faster.  See the inline code comments 
 420:lib/lib_pic33e/usb/usb_device.c ****     near the top of usb_device.c for more details about minimum timing 
 421:lib/lib_pic33e/usb/usb_device.c ****     requirements when calling USBDeviceTasks().
MPLAB XC16 ASSEMBLY Listing:   			page 15


 422:lib/lib_pic33e/usb/usb_device.c ****     
 423:lib/lib_pic33e/usb/usb_device.c ****     When the USB stack is operated in "USB_INTERRUPT" mode, it is not necessary
 424:lib/lib_pic33e/usb/usb_device.c ****     to call USBDeviceTasks() from the main loop context.  In the USB_INTERRUPT
 425:lib/lib_pic33e/usb/usb_device.c ****     mode, the USBDeviceTasks() handler only needs to execute when a USB 
 426:lib/lib_pic33e/usb/usb_device.c ****     interrupt occurs, and therefore only needs to be called from the interrupt 
 427:lib/lib_pic33e/usb/usb_device.c ****     context.
 428:lib/lib_pic33e/usb/usb_device.c **** 
 429:lib/lib_pic33e/usb/usb_device.c ****     Typical usage:
 430:lib/lib_pic33e/usb/usb_device.c ****     <code>
 431:lib/lib_pic33e/usb/usb_device.c ****     void main(void)
 432:lib/lib_pic33e/usb/usb_device.c ****     {
 433:lib/lib_pic33e/usb/usb_device.c ****         USBDeviceInit();
 434:lib/lib_pic33e/usb/usb_device.c ****         while(1)
 435:lib/lib_pic33e/usb/usb_device.c ****         {
 436:lib/lib_pic33e/usb/usb_device.c ****             USBDeviceTasks(); //Takes care of enumeration and other USB events
 437:lib/lib_pic33e/usb/usb_device.c ****             if((USBGetDeviceState() \< CONFIGURED_STATE) ||
 438:lib/lib_pic33e/usb/usb_device.c ****                (USBIsDeviceSuspended() == true))
 439:lib/lib_pic33e/usb/usb_device.c ****             {
 440:lib/lib_pic33e/usb/usb_device.c ****                 //Either the device is not configured or we are suspended,
 441:lib/lib_pic33e/usb/usb_device.c ****                 // so we don't want to execute any USB related application code
 442:lib/lib_pic33e/usb/usb_device.c ****                 continue;   //go back to the top of the while loop
 443:lib/lib_pic33e/usb/usb_device.c ****             }
 444:lib/lib_pic33e/usb/usb_device.c ****             else
 445:lib/lib_pic33e/usb/usb_device.c ****             {
 446:lib/lib_pic33e/usb/usb_device.c ****                 //Otherwise we are free to run USB and non-USB related user 
 447:lib/lib_pic33e/usb/usb_device.c ****                 //application code.
 448:lib/lib_pic33e/usb/usb_device.c ****                 UserApplication();
 449:lib/lib_pic33e/usb/usb_device.c ****             }
 450:lib/lib_pic33e/usb/usb_device.c ****         }
 451:lib/lib_pic33e/usb/usb_device.c ****     }
 452:lib/lib_pic33e/usb/usb_device.c ****     </code>
 453:lib/lib_pic33e/usb/usb_device.c **** 
 454:lib/lib_pic33e/usb/usb_device.c ****   Precondition:
 455:lib/lib_pic33e/usb/usb_device.c ****     Make sure the USBDeviceInit() function has been called prior to calling
 456:lib/lib_pic33e/usb/usb_device.c ****     USBDeviceTasks() for the first time.
 457:lib/lib_pic33e/usb/usb_device.c ****   Remarks:
 458:lib/lib_pic33e/usb/usb_device.c ****     USBDeviceTasks() does not need to be called while in the USB suspend mode, 
 459:lib/lib_pic33e/usb/usb_device.c ****     if the user application firmware in the USBCBSuspend() callback function
 460:lib/lib_pic33e/usb/usb_device.c ****     enables the ACTVIF USB interrupt source and put the microcontroller into 
 461:lib/lib_pic33e/usb/usb_device.c ****     sleep mode.  If the application firmware decides not to sleep the 
 462:lib/lib_pic33e/usb/usb_device.c ****     microcontroller core during USB suspend (ex: continues running at full 
 463:lib/lib_pic33e/usb/usb_device.c ****     frequency, or clock switches to a lower frequency), then the USBDeviceTasks()
 464:lib/lib_pic33e/usb/usb_device.c ****     function must still be called periodically, at a rate frequent enough to 
 465:lib/lib_pic33e/usb/usb_device.c ****     ensure the 10ms resume recovery interval USB specification is met.  Assuming
 466:lib/lib_pic33e/usb/usb_device.c ****     a worst case primary oscillator and PLL start up time of less than 5ms, then
 467:lib/lib_pic33e/usb/usb_device.c ****     USBDeviceTasks() should be called once every 5ms in this scenario.
 468:lib/lib_pic33e/usb/usb_device.c ****    
 469:lib/lib_pic33e/usb/usb_device.c ****     When the USB cable is detached, or the USB host is not actively powering 
 470:lib/lib_pic33e/usb/usb_device.c ****     the VBUS line to +5V nominal, the application firmware does not always have 
 471:lib/lib_pic33e/usb/usb_device.c ****     to call USBDeviceTasks() frequently, as no USB activity will be taking 
 472:lib/lib_pic33e/usb/usb_device.c ****     place.  However, if USBDeviceTasks() is not called regularly, some 
 473:lib/lib_pic33e/usb/usb_device.c ****     alternative means of promptly detecting when VBUS is powered (indicating 
 474:lib/lib_pic33e/usb/usb_device.c ****     host attachment), or not powered (host powered down or USB cable unplugged)
 475:lib/lib_pic33e/usb/usb_device.c ****     is still needed.  For self or dual self/bus powered USB applications, see 
 476:lib/lib_pic33e/usb/usb_device.c ****     the USBDeviceAttach() and USBDeviceDetach() API documentation for additional 
 477:lib/lib_pic33e/usb/usb_device.c ****     considerations.
 478:lib/lib_pic33e/usb/usb_device.c ****     ***************************************************************************/
MPLAB XC16 ASSEMBLY Listing:   			page 16


 479:lib/lib_pic33e/usb/usb_device.c **** void USBDeviceTasks(void)
 480:lib/lib_pic33e/usb/usb_device.c **** {
 352              	.loc 1 480 0
 353              	.set ___PA___,1
 354 0000fa  02 00 FA 	lnk #2
 355              	.LCFI2:
 481:lib/lib_pic33e/usb/usb_device.c ****     uint8_t i;
 482:lib/lib_pic33e/usb/usb_device.c **** 
 483:lib/lib_pic33e/usb/usb_device.c ****     #ifdef USB_SUPPORT_OTG
 484:lib/lib_pic33e/usb/usb_device.c ****         //SRP Time Out Check
 485:lib/lib_pic33e/usb/usb_device.c ****         if (USBOTGSRPIsReady())
 486:lib/lib_pic33e/usb/usb_device.c ****         {
 487:lib/lib_pic33e/usb/usb_device.c ****             if (USBT1MSECIF && USBT1MSECIE)
 488:lib/lib_pic33e/usb/usb_device.c ****             {
 489:lib/lib_pic33e/usb/usb_device.c ****                 if (USBOTGGetSRPTimeOutFlag())
 490:lib/lib_pic33e/usb/usb_device.c ****                 {
 491:lib/lib_pic33e/usb/usb_device.c ****                     if (USBOTGIsSRPTimeOutExpired())
 492:lib/lib_pic33e/usb/usb_device.c ****                     {
 493:lib/lib_pic33e/usb/usb_device.c ****                         USB_OTGEventHandler(0,OTG_EVENT_SRP_FAILED,0,0);
 494:lib/lib_pic33e/usb/usb_device.c ****                     }
 495:lib/lib_pic33e/usb/usb_device.c ****                 }
 496:lib/lib_pic33e/usb/usb_device.c **** 
 497:lib/lib_pic33e/usb/usb_device.c ****                 //Clear Interrupt Flag
 498:lib/lib_pic33e/usb/usb_device.c ****                 USBClearInterruptFlag(USBT1MSECIFReg,USBT1MSECIFBitNum);
 499:lib/lib_pic33e/usb/usb_device.c ****             }
 500:lib/lib_pic33e/usb/usb_device.c ****         }
 501:lib/lib_pic33e/usb/usb_device.c ****     #endif
 502:lib/lib_pic33e/usb/usb_device.c **** 
 503:lib/lib_pic33e/usb/usb_device.c ****     #if defined(USB_POLLING)
 504:lib/lib_pic33e/usb/usb_device.c ****     //If the interrupt option is selected then the customer is required
 505:lib/lib_pic33e/usb/usb_device.c ****     //  to notify the stack when the device is attached or removed from the
 506:lib/lib_pic33e/usb/usb_device.c ****     //  bus by calling the USBDeviceAttach() and USBDeviceDetach() functions.
 507:lib/lib_pic33e/usb/usb_device.c ****     if (USB_BUS_SENSE != 1)
 508:lib/lib_pic33e/usb/usb_device.c ****     {
 509:lib/lib_pic33e/usb/usb_device.c ****          // Disable module & detach from bus
 510:lib/lib_pic33e/usb/usb_device.c ****          U1CON = 0;             
 511:lib/lib_pic33e/usb/usb_device.c **** 
 512:lib/lib_pic33e/usb/usb_device.c ****          // Mask all USB interrupts              
 513:lib/lib_pic33e/usb/usb_device.c ****          U1IE = 0;          
 514:lib/lib_pic33e/usb/usb_device.c **** 
 515:lib/lib_pic33e/usb/usb_device.c ****          //Move to the detached state                  
 516:lib/lib_pic33e/usb/usb_device.c ****          USBDeviceState = DETACHED_STATE;
 517:lib/lib_pic33e/usb/usb_device.c **** 
 518:lib/lib_pic33e/usb/usb_device.c ****          #ifdef  USB_SUPPORT_OTG    
 519:lib/lib_pic33e/usb/usb_device.c ****              //Disable D+ Pullup
 520:lib/lib_pic33e/usb/usb_device.c ****              U1OTGCONbits.DPPULUP = 0;
 521:lib/lib_pic33e/usb/usb_device.c **** 
 522:lib/lib_pic33e/usb/usb_device.c ****              //Disable HNP
 523:lib/lib_pic33e/usb/usb_device.c ****              USBOTGDisableHnp();
 524:lib/lib_pic33e/usb/usb_device.c **** 
 525:lib/lib_pic33e/usb/usb_device.c ****              //Deactivate HNP
 526:lib/lib_pic33e/usb/usb_device.c ****              USBOTGDeactivateHnp();
 527:lib/lib_pic33e/usb/usb_device.c ****              
 528:lib/lib_pic33e/usb/usb_device.c ****              //If ID Pin Changed State
 529:lib/lib_pic33e/usb/usb_device.c ****              if (USBIDIF && USBIDIE)
 530:lib/lib_pic33e/usb/usb_device.c ****              {  
 531:lib/lib_pic33e/usb/usb_device.c ****                  //Re-detect & Initialize
MPLAB XC16 ASSEMBLY Listing:   			page 17


 532:lib/lib_pic33e/usb/usb_device.c ****                   USBOTGInitialize();
 533:lib/lib_pic33e/usb/usb_device.c **** 
 534:lib/lib_pic33e/usb/usb_device.c ****                   //Clear ID Interrupt Flag
 535:lib/lib_pic33e/usb/usb_device.c ****                   USBClearInterruptFlag(USBIDIFReg,USBIDIFBitNum);
 536:lib/lib_pic33e/usb/usb_device.c ****              }
 537:lib/lib_pic33e/usb/usb_device.c ****          #endif
 538:lib/lib_pic33e/usb/usb_device.c **** 
 539:lib/lib_pic33e/usb/usb_device.c ****          #if defined __C30__ || defined __XC16__
 540:lib/lib_pic33e/usb/usb_device.c ****              //USBClearInterruptFlag(U1OTGIR, 3); 
 541:lib/lib_pic33e/usb/usb_device.c ****          #endif
 542:lib/lib_pic33e/usb/usb_device.c ****             //return so that we don't go through the rest of 
 543:lib/lib_pic33e/usb/usb_device.c ****             //the state machine
 544:lib/lib_pic33e/usb/usb_device.c ****          USBClearUSBInterrupt();
 545:lib/lib_pic33e/usb/usb_device.c ****          return;
 546:lib/lib_pic33e/usb/usb_device.c ****     }
 547:lib/lib_pic33e/usb/usb_device.c **** 
 548:lib/lib_pic33e/usb/usb_device.c **** 	#ifdef USB_SUPPORT_OTG
 549:lib/lib_pic33e/usb/usb_device.c ****     //If Session Is Started Then
 550:lib/lib_pic33e/usb/usb_device.c ****     else
 551:lib/lib_pic33e/usb/usb_device.c **** 	{
 552:lib/lib_pic33e/usb/usb_device.c ****         //If SRP Is Ready
 553:lib/lib_pic33e/usb/usb_device.c ****         if (USBOTGSRPIsReady())
 554:lib/lib_pic33e/usb/usb_device.c ****         {   
 555:lib/lib_pic33e/usb/usb_device.c ****             //Clear SRPReady
 556:lib/lib_pic33e/usb/usb_device.c ****             USBOTGClearSRPReady();
 557:lib/lib_pic33e/usb/usb_device.c **** 
 558:lib/lib_pic33e/usb/usb_device.c ****             //Clear SRP Timeout Flag
 559:lib/lib_pic33e/usb/usb_device.c ****             USBOTGClearSRPTimeOutFlag();
 560:lib/lib_pic33e/usb/usb_device.c **** 
 561:lib/lib_pic33e/usb/usb_device.c ****             //Indicate Session Started
 562:lib/lib_pic33e/usb/usb_device.c ****             UART2PrintString( "\r\n***** USB OTG B Event - Session Started  *****\r\n" );
 563:lib/lib_pic33e/usb/usb_device.c ****         }
 564:lib/lib_pic33e/usb/usb_device.c ****     }
 565:lib/lib_pic33e/usb/usb_device.c **** 	#endif	//#ifdef USB_SUPPORT_OTG
 566:lib/lib_pic33e/usb/usb_device.c **** 
 567:lib/lib_pic33e/usb/usb_device.c ****     //if we are in the detached state
 568:lib/lib_pic33e/usb/usb_device.c ****     if(USBDeviceState == DETACHED_STATE)
 569:lib/lib_pic33e/usb/usb_device.c ****     {
 570:lib/lib_pic33e/usb/usb_device.c **** 	    //Initialize register to known value
 571:lib/lib_pic33e/usb/usb_device.c ****         U1CON = 0;                          
 572:lib/lib_pic33e/usb/usb_device.c **** 
 573:lib/lib_pic33e/usb/usb_device.c ****         // Mask all USB interrupts
 574:lib/lib_pic33e/usb/usb_device.c ****         U1IE = 0;                                
 575:lib/lib_pic33e/usb/usb_device.c **** 
 576:lib/lib_pic33e/usb/usb_device.c ****         //Enable/set things like: pull ups, full/low-speed mode, 
 577:lib/lib_pic33e/usb/usb_device.c ****         //set the ping pong mode, and set internal transceiver
 578:lib/lib_pic33e/usb/usb_device.c ****         SetConfigurationOptions();
 579:lib/lib_pic33e/usb/usb_device.c **** 
 580:lib/lib_pic33e/usb/usb_device.c ****         // Enable module & attach to bus
 581:lib/lib_pic33e/usb/usb_device.c ****         while(!U1CONbits.USBEN){U1CONbits.USBEN = 1;}
 582:lib/lib_pic33e/usb/usb_device.c **** 
 583:lib/lib_pic33e/usb/usb_device.c ****         //moved to the attached state
 584:lib/lib_pic33e/usb/usb_device.c ****         USBDeviceState = ATTACHED_STATE;
 585:lib/lib_pic33e/usb/usb_device.c **** 
 586:lib/lib_pic33e/usb/usb_device.c ****         #ifdef  USB_SUPPORT_OTG
 587:lib/lib_pic33e/usb/usb_device.c ****             U1OTGCON |= USB_OTG_DPLUS_ENABLE | USB_OTG_ENABLE;  
 588:lib/lib_pic33e/usb/usb_device.c ****         #endif
MPLAB XC16 ASSEMBLY Listing:   			page 18


 589:lib/lib_pic33e/usb/usb_device.c ****     }
 590:lib/lib_pic33e/usb/usb_device.c **** 	#endif  //#if defined(USB_POLLING)
 591:lib/lib_pic33e/usb/usb_device.c **** 
 592:lib/lib_pic33e/usb/usb_device.c ****     if(USBDeviceState == ATTACHED_STATE)
 356              	.loc 1 592 0
 357 0000fc  00 00 80 	mov _USBDeviceState,w0
 358 0000fe  E1 0F 50 	sub w0,#1,[w15]
 359              	.set ___BP___,0
 360 000100  00 00 3A 	bra nz,.L8
 593:lib/lib_pic33e/usb/usb_device.c ****     {
 594:lib/lib_pic33e/usb/usb_device.c ****         /*
 595:lib/lib_pic33e/usb/usb_device.c ****          * After enabling the USB module, it takes some time for the
 596:lib/lib_pic33e/usb/usb_device.c ****          * voltage on the D+ or D- line to rise high enough to get out
 597:lib/lib_pic33e/usb/usb_device.c ****          * of the SE0 condition. The USB Reset interrupt should not be
 598:lib/lib_pic33e/usb/usb_device.c ****          * unmasked until the SE0 condition is cleared. This helps
 599:lib/lib_pic33e/usb/usb_device.c ****          * prevent the firmware from misinterpreting this unique event
 600:lib/lib_pic33e/usb/usb_device.c ****          * as a USB bus reset from the USB host.
 601:lib/lib_pic33e/usb/usb_device.c ****          */
 602:lib/lib_pic33e/usb/usb_device.c **** 
 603:lib/lib_pic33e/usb/usb_device.c ****         if(!USBSE0Event)
 361              	.loc 1 603 0
 362 000102  01 00 80 	mov _U1CONbits,w1
 363 000104  00 04 20 	mov #64,w0
 364 000106  00 80 60 	and w1,w0,w0
 365 000108  00 00 E0 	cp0 w0
 366              	.set ___BP___,0
 367 00010a  00 00 3A 	bra nz,.L8
 604:lib/lib_pic33e/usb/usb_device.c ****         {
 605:lib/lib_pic33e/usb/usb_device.c ****             //We recently attached, make sure we are in a clean state
 606:lib/lib_pic33e/usb/usb_device.c ****             #if defined(__dsPIC33E__) || defined(_PIC24E__) || defined(__PIC32MM__)
 607:lib/lib_pic33e/usb/usb_device.c ****                 U1IR = 0xFFEF;  //Preserve IDLEIF info, so we can detect suspend
 368              	.loc 1 607 0
 369 00010c  F1 FE 2F 	mov #-17,w1
 608:lib/lib_pic33e/usb/usb_device.c ****                                 //during attach de-bounce interval
 609:lib/lib_pic33e/usb/usb_device.c ****             #else
 610:lib/lib_pic33e/usb/usb_device.c ****                 USBClearInterruptRegister(U1IR);
 611:lib/lib_pic33e/usb/usb_device.c ****             #endif
 612:lib/lib_pic33e/usb/usb_device.c **** 
 613:lib/lib_pic33e/usb/usb_device.c ****             #if defined(USB_POLLING)
 614:lib/lib_pic33e/usb/usb_device.c ****                 U1IE=0;                        // Mask all USB interrupts
 615:lib/lib_pic33e/usb/usb_device.c ****             #endif
 616:lib/lib_pic33e/usb/usb_device.c ****             USBResetIE = 1;             // Unmask RESET interrupt
 617:lib/lib_pic33e/usb/usb_device.c ****             USBIdleIE = 1;             // Unmask IDLE interrupt
 618:lib/lib_pic33e/usb/usb_device.c ****             USBDeviceState = POWERED_STATE;
 370              	.loc 1 618 0
 371 00010e  20 00 20 	mov #2,w0
 372              	.loc 1 607 0
 373 000110  01 00 88 	mov w1,_U1IR
 374              	.loc 1 616 0
 375 000112  00 00 A8 	bset.b _U1IEbits,#0
 376              	.loc 1 617 0
 377 000114  00 80 A8 	bset.b _U1IEbits,#4
 378              	.loc 1 618 0
 379 000116  00 00 88 	mov w0,_USBDeviceState
 380              	.L8:
 619:lib/lib_pic33e/usb/usb_device.c ****         }
 620:lib/lib_pic33e/usb/usb_device.c ****     }
MPLAB XC16 ASSEMBLY Listing:   			page 19


 621:lib/lib_pic33e/usb/usb_device.c **** 
 622:lib/lib_pic33e/usb/usb_device.c ****     #ifdef  USB_SUPPORT_OTG
 623:lib/lib_pic33e/usb/usb_device.c ****         //If ID Pin Changed State
 624:lib/lib_pic33e/usb/usb_device.c ****         if (USBIDIF && USBIDIE)
 625:lib/lib_pic33e/usb/usb_device.c ****         {  
 626:lib/lib_pic33e/usb/usb_device.c ****             //Re-detect & Initialize
 627:lib/lib_pic33e/usb/usb_device.c ****             USBOTGInitialize();
 628:lib/lib_pic33e/usb/usb_device.c **** 
 629:lib/lib_pic33e/usb/usb_device.c ****             USBClearInterruptFlag(USBIDIFReg,USBIDIFBitNum);
 630:lib/lib_pic33e/usb/usb_device.c ****         }
 631:lib/lib_pic33e/usb/usb_device.c ****     #endif
 632:lib/lib_pic33e/usb/usb_device.c **** 
 633:lib/lib_pic33e/usb/usb_device.c ****     /*
 634:lib/lib_pic33e/usb/usb_device.c ****      * Task A: Service USB Activity Interrupt
 635:lib/lib_pic33e/usb/usb_device.c ****      */
 636:lib/lib_pic33e/usb/usb_device.c ****     if(USBActivityIF && USBActivityIE)
 381              	.loc 1 636 0
 382 000118  00 00 80 	mov _U1OTGIRbits,w0
 383 00011a  70 00 60 	and w0,#16,w0
 384 00011c  00 00 E0 	cp0 w0
 385              	.set ___BP___,0
 386 00011e  00 00 32 	bra z,.L9
 387 000120  00 00 80 	mov _U1OTGIEbits,w0
 388 000122  70 00 60 	and w0,#16,w0
 389 000124  00 00 E0 	cp0 w0
 390              	.set ___BP___,0
 391 000126  00 00 32 	bra z,.L9
 637:lib/lib_pic33e/usb/usb_device.c ****     {
 638:lib/lib_pic33e/usb/usb_device.c ****         USBClearInterruptFlag(USBActivityIFReg,USBActivityIFBitNum);
 392              	.loc 1 638 0
 393 000128  00 01 20 	mov #16,w0
 394 00012a  00 00 88 	mov w0,_U1OTGIR
 639:lib/lib_pic33e/usb/usb_device.c ****         #if defined(USB_SUPPORT_OTG)
 640:lib/lib_pic33e/usb/usb_device.c ****             U1OTGIR = 0x10;        
 641:lib/lib_pic33e/usb/usb_device.c ****         #else
 642:lib/lib_pic33e/usb/usb_device.c ****             USBWakeFromSuspend();
 395              	.loc 1 642 0
 396 00012c  00 00 07 	rcall _USBWakeFromSuspend
 397              	.L9:
 643:lib/lib_pic33e/usb/usb_device.c ****         #endif
 644:lib/lib_pic33e/usb/usb_device.c ****     }
 645:lib/lib_pic33e/usb/usb_device.c **** 
 646:lib/lib_pic33e/usb/usb_device.c ****     /*
 647:lib/lib_pic33e/usb/usb_device.c ****      * Pointless to continue servicing if the device is in suspend mode.
 648:lib/lib_pic33e/usb/usb_device.c ****      */
 649:lib/lib_pic33e/usb/usb_device.c ****     if(USBSuspendControl==1)
 398              	.loc 1 649 0
 399 00012e  00 00 80 	mov _U1PWRCbits,w0
 400 000130  62 00 60 	and w0,#2,w0
 401 000132  00 00 E0 	cp0 w0
 402              	.set ___BP___,0
 403 000134  00 00 32 	bra z,.L10
 650:lib/lib_pic33e/usb/usb_device.c ****     {
 651:lib/lib_pic33e/usb/usb_device.c ****         USBClearUSBInterrupt();
 404              	.loc 1 651 0
 405 000136  00 C0 A9 	bclr.b _IFS5bits,#6
 652:lib/lib_pic33e/usb/usb_device.c ****         return;
MPLAB XC16 ASSEMBLY Listing:   			page 20


 406              	.loc 1 652 0
 407 000138  00 00 37 	bra .L7
 408              	.L10:
 653:lib/lib_pic33e/usb/usb_device.c ****     }
 654:lib/lib_pic33e/usb/usb_device.c **** 
 655:lib/lib_pic33e/usb/usb_device.c ****     /*
 656:lib/lib_pic33e/usb/usb_device.c ****      * Task B: Service USB Bus Reset Interrupt.
 657:lib/lib_pic33e/usb/usb_device.c ****      * When bus reset is received during suspend, ACTVIF will be set first,
 658:lib/lib_pic33e/usb/usb_device.c ****      * once the UCONbits.SUSPND is clear, then the URSTIF bit will be asserted.
 659:lib/lib_pic33e/usb/usb_device.c ****      * This is why URSTIF is checked after ACTVIF.
 660:lib/lib_pic33e/usb/usb_device.c ****      *
 661:lib/lib_pic33e/usb/usb_device.c ****      * The USB reset flag is masked when the USB state is in
 662:lib/lib_pic33e/usb/usb_device.c ****      * DETACHED_STATE or ATTACHED_STATE, and therefore cannot
 663:lib/lib_pic33e/usb/usb_device.c ****      * cause a USB reset event during these two states.
 664:lib/lib_pic33e/usb/usb_device.c ****      */
 665:lib/lib_pic33e/usb/usb_device.c ****     if(USBResetIF && USBResetIE)
 409              	.loc 1 665 0
 410 00013a  00 00 80 	mov _U1IRbits,w0
 411 00013c  61 00 60 	and w0,#1,w0
 412 00013e  00 00 E0 	cp0 w0
 413              	.set ___BP___,0
 414 000140  00 00 32 	bra z,.L12
 415 000142  00 00 80 	mov _U1IEbits,w0
 416 000144  61 00 60 	and w0,#1,w0
 417 000146  00 00 E0 	cp0 w0
 418              	.set ___BP___,0
 419 000148  00 00 32 	bra z,.L12
 666:lib/lib_pic33e/usb/usb_device.c ****     {
 667:lib/lib_pic33e/usb/usb_device.c ****         USBDeviceInit();
 420              	.loc 1 667 0
 421 00014a  00 00 07 	rcall _USBDeviceInit
 668:lib/lib_pic33e/usb/usb_device.c **** 
 669:lib/lib_pic33e/usb/usb_device.c ****         //Re-enable the interrupts since the USBDeviceInit() function will
 670:lib/lib_pic33e/usb/usb_device.c ****         //  disable them.  This will do nothing in a polling setup
 671:lib/lib_pic33e/usb/usb_device.c ****         USBUnmaskInterrupts();
 422              	.loc 1 671 0
 423 00014c  00 C0 A8 	bset.b _IEC5bits,#6
 672:lib/lib_pic33e/usb/usb_device.c **** 
 673:lib/lib_pic33e/usb/usb_device.c ****         USBDeviceState = DEFAULT_STATE;
 424              	.loc 1 673 0
 425 00014e  41 00 20 	mov #4,w1
 674:lib/lib_pic33e/usb/usb_device.c **** 
 675:lib/lib_pic33e/usb/usb_device.c ****         #ifdef USB_SUPPORT_OTG
 676:lib/lib_pic33e/usb/usb_device.c ****              //Disable HNP
 677:lib/lib_pic33e/usb/usb_device.c ****              USBOTGDisableHnp();
 678:lib/lib_pic33e/usb/usb_device.c **** 
 679:lib/lib_pic33e/usb/usb_device.c ****              //Deactivate HNP
 680:lib/lib_pic33e/usb/usb_device.c ****              USBOTGDeactivateHnp();
 681:lib/lib_pic33e/usb/usb_device.c ****         #endif
 682:lib/lib_pic33e/usb/usb_device.c **** 
 683:lib/lib_pic33e/usb/usb_device.c ****         USBClearInterruptFlag(USBResetIFReg,USBResetIFBitNum);
 426              	.loc 1 683 0
 427 000150  10 00 20 	mov #1,w0
 428              	.loc 1 673 0
 429 000152  01 00 88 	mov w1,_USBDeviceState
 430              	.loc 1 683 0
 431 000154  00 00 88 	mov w0,_U1IR
MPLAB XC16 ASSEMBLY Listing:   			page 21


 432              	.L12:
 684:lib/lib_pic33e/usb/usb_device.c ****     }
 685:lib/lib_pic33e/usb/usb_device.c **** 
 686:lib/lib_pic33e/usb/usb_device.c ****     /*
 687:lib/lib_pic33e/usb/usb_device.c ****      * Task C: Service other USB interrupts
 688:lib/lib_pic33e/usb/usb_device.c ****      */
 689:lib/lib_pic33e/usb/usb_device.c ****     if(USBIdleIF && USBIdleIE)
 433              	.loc 1 689 0
 434 000156  00 00 80 	mov _U1IRbits,w0
 435 000158  70 00 60 	and w0,#16,w0
 436 00015a  00 00 E0 	cp0 w0
 437              	.set ___BP___,0
 438 00015c  00 00 32 	bra z,.L13
 439 00015e  00 00 80 	mov _U1IEbits,w0
 440 000160  70 00 60 	and w0,#16,w0
 441 000162  00 00 E0 	cp0 w0
 442              	.set ___BP___,0
 443 000164  00 00 32 	bra z,.L13
 690:lib/lib_pic33e/usb/usb_device.c ****     { 
 691:lib/lib_pic33e/usb/usb_device.c ****         #ifdef  USB_SUPPORT_OTG 
 692:lib/lib_pic33e/usb/usb_device.c ****             //If Suspended, Try to switch to Host
 693:lib/lib_pic33e/usb/usb_device.c ****             USBOTGSelectRole(ROLE_HOST);
 694:lib/lib_pic33e/usb/usb_device.c ****             USBClearInterruptFlag(USBIdleIFReg,USBIdleIFBitNum);
 695:lib/lib_pic33e/usb/usb_device.c ****         #else
 696:lib/lib_pic33e/usb/usb_device.c ****             USBSuspend();
 444              	.loc 1 696 0
 445 000166  00 00 07 	rcall _USBSuspend
 446              	.L13:
 697:lib/lib_pic33e/usb/usb_device.c ****         #endif
 698:lib/lib_pic33e/usb/usb_device.c ****     }
 699:lib/lib_pic33e/usb/usb_device.c **** 
 700:lib/lib_pic33e/usb/usb_device.c ****     #if defined(__XC16__) || defined(__C30__) || defined(__XC32__)
 701:lib/lib_pic33e/usb/usb_device.c ****         //Check if a 1ms interval has elapsed.	
 702:lib/lib_pic33e/usb/usb_device.c ****         if(USBT1MSECIF)
 447              	.loc 1 702 0
 448 000168  01 00 80 	mov _U1OTGIRbits,w1
 449 00016a  00 04 20 	mov #64,w0
 450 00016c  00 80 60 	and w1,w0,w0
 451 00016e  00 00 E0 	cp0 w0
 452              	.set ___BP___,0
 453 000170  00 00 32 	bra z,.L14
 703:lib/lib_pic33e/usb/usb_device.c ****         {
 704:lib/lib_pic33e/usb/usb_device.c ****             USBClearInterruptFlag(USBT1MSECIFReg, USBT1MSECIFBitNum);
 454              	.loc 1 704 0
 455 000172  00 04 20 	mov #64,w0
 456 000174  00 00 88 	mov w0,_U1OTGIR
 705:lib/lib_pic33e/usb/usb_device.c ****             USBIncrement1msInternalTimers();
 457              	.loc 1 705 0
 458 000176  00 00 07 	rcall _USBIncrement1msInternalTimers
 459              	.L14:
 706:lib/lib_pic33e/usb/usb_device.c ****         }
 707:lib/lib_pic33e/usb/usb_device.c ****     #endif
 708:lib/lib_pic33e/usb/usb_device.c **** 
 709:lib/lib_pic33e/usb/usb_device.c ****     //Start-of-Frame Interrupt
 710:lib/lib_pic33e/usb/usb_device.c ****     if(USBSOFIF)
 460              	.loc 1 710 0
 461 000178  00 00 80 	mov _U1IRbits,w0
MPLAB XC16 ASSEMBLY Listing:   			page 22


 462 00017a  64 00 60 	and w0,#4,w0
 463 00017c  00 00 E0 	cp0 w0
 464              	.set ___BP___,0
 465 00017e  00 00 32 	bra z,.L15
 711:lib/lib_pic33e/usb/usb_device.c ****     {
 712:lib/lib_pic33e/usb/usb_device.c ****         //Call the user SOF event callback if enabled.
 713:lib/lib_pic33e/usb/usb_device.c ****         if(USBSOFIE)
 466              	.loc 1 713 0
 467 000180  00 00 80 	mov _U1IEbits,w0
 468 000182  64 00 60 	and w0,#4,w0
 469 000184  00 00 E0 	cp0 w0
 470              	.set ___BP___,0
 471 000186  00 00 32 	bra z,.L16
 714:lib/lib_pic33e/usb/usb_device.c ****         {
 715:lib/lib_pic33e/usb/usb_device.c ****             USB_SOF_HANDLER(EVENT_SOF,0,1);
 472              	.loc 1 715 0
 473 000188  12 00 20 	mov #1,w2
 474 00018a  80 00 EB 	clr w1
 475 00018c  30 07 20 	mov #115,w0
 476 00018e  00 00 07 	rcall _USER_USB_CALLBACK_EVENT_HANDLER
 477              	.L16:
 716:lib/lib_pic33e/usb/usb_device.c ****         }    
 717:lib/lib_pic33e/usb/usb_device.c ****         USBClearInterruptFlag(USBSOFIFReg,USBSOFIFBitNum);
 478              	.loc 1 717 0
 479 000190  40 00 20 	mov #4,w0
 480 000192  00 00 88 	mov w0,_U1IR
 718:lib/lib_pic33e/usb/usb_device.c **** 
 719:lib/lib_pic33e/usb/usb_device.c ****         #if defined(__XC8__) || defined(__C18__)
 720:lib/lib_pic33e/usb/usb_device.c ****             USBIncrement1msInternalTimers();
 721:lib/lib_pic33e/usb/usb_device.c ****         #endif
 722:lib/lib_pic33e/usb/usb_device.c **** 
 723:lib/lib_pic33e/usb/usb_device.c ****         #if defined(USB_ENABLE_STATUS_STAGE_TIMEOUTS)
 724:lib/lib_pic33e/usb/usb_device.c ****             //Supporting this feature requires a 1ms time base for keeping track of the timeout int
 725:lib/lib_pic33e/usb/usb_device.c ****             #if(USB_SPEED_OPTION == USB_LOW_SPEED)
 726:lib/lib_pic33e/usb/usb_device.c ****                 #warning "Double click this message.  See inline code comments."
 727:lib/lib_pic33e/usb/usb_device.c ****                 //The "USB_ENABLE_STATUS_STAGE_TIMEOUTS" feature is optional and is
 728:lib/lib_pic33e/usb/usb_device.c ****                 //not strictly needed in all applications (ex: those that never call 
 729:lib/lib_pic33e/usb/usb_device.c ****                 //USBDeferStatusStage() and don't use host to device (OUT) control
 730:lib/lib_pic33e/usb/usb_device.c ****                 //transfers with data stage).  
 731:lib/lib_pic33e/usb/usb_device.c ****                 //However, if this feature is enabled and used in a low speed application,
 732:lib/lib_pic33e/usb/usb_device.c ****                 //it is required for the application code to periodically call the
 733:lib/lib_pic33e/usb/usb_device.c ****                 //USBIncrement1msInternalTimers() function at a nominally 1ms rate.
 734:lib/lib_pic33e/usb/usb_device.c ****             #endif
 735:lib/lib_pic33e/usb/usb_device.c ****             
 736:lib/lib_pic33e/usb/usb_device.c ****             //Decrement our status stage counter.
 737:lib/lib_pic33e/usb/usb_device.c ****             if(USBStatusStageTimeoutCounter != 0u)
 481              	.loc 1 737 0
 482 000194  00 C0 BF 	mov.b _USBStatusStageTimeoutCounter,WREG
 483 000196  00 04 E0 	cp0.b w0
 484              	.set ___BP___,0
 485 000198  00 00 32 	bra z,.L17
 738:lib/lib_pic33e/usb/usb_device.c ****             {
 739:lib/lib_pic33e/usb/usb_device.c ****                 USBStatusStageTimeoutCounter--;
 486              	.loc 1 739 0
 487 00019a  00 C0 BF 	mov.b _USBStatusStageTimeoutCounter,WREG
 488 00019c  00 40 E9 	dec.b w0,w0
 489 00019e  00 E0 B7 	mov.b WREG,_USBStatusStageTimeoutCounter
MPLAB XC16 ASSEMBLY Listing:   			page 23


 490              	.L17:
 740:lib/lib_pic33e/usb/usb_device.c ****             }
 741:lib/lib_pic33e/usb/usb_device.c ****             //Check if too much time has elapsed since progress was made in 
 742:lib/lib_pic33e/usb/usb_device.c ****             //processing the control transfer, without arming the status stage.  
 743:lib/lib_pic33e/usb/usb_device.c ****             //If so, auto-arm the status stage to ensure that the control 
 744:lib/lib_pic33e/usb/usb_device.c ****             //transfer can [eventually] complete, within the timing limits
 745:lib/lib_pic33e/usb/usb_device.c ****             //dictated by section 9.2.6 of the official USB 2.0 specifications.
 746:lib/lib_pic33e/usb/usb_device.c ****             if(USBStatusStageTimeoutCounter == 0)
 491              	.loc 1 746 0
 492 0001a0  00 C0 BF 	mov.b _USBStatusStageTimeoutCounter,WREG
 493 0001a2  00 04 E0 	cp0.b w0
 494              	.set ___BP___,0
 495 0001a4  00 00 3A 	bra nz,.L15
 747:lib/lib_pic33e/usb/usb_device.c ****             {
 748:lib/lib_pic33e/usb/usb_device.c ****                 USBCtrlEPAllowStatusStage();    //Does nothing if the status stage was already arme
 496              	.loc 1 748 0
 497 0001a6  00 00 07 	rcall _USBCtrlEPAllowStatusStage
 498              	.L15:
 749:lib/lib_pic33e/usb/usb_device.c ****             } 
 750:lib/lib_pic33e/usb/usb_device.c ****         #endif
 751:lib/lib_pic33e/usb/usb_device.c ****     }
 752:lib/lib_pic33e/usb/usb_device.c **** 
 753:lib/lib_pic33e/usb/usb_device.c ****     if(USBStallIF && USBStallIE)
 499              	.loc 1 753 0
 500 0001a8  01 00 80 	mov _U1IRbits,w1
 501 0001aa  00 08 20 	mov #128,w0
 502 0001ac  00 80 60 	and w1,w0,w0
 503 0001ae  00 00 E0 	cp0 w0
 504              	.set ___BP___,0
 505 0001b0  00 00 32 	bra z,.L18
 506 0001b2  01 00 80 	mov _U1IEbits,w1
 507 0001b4  00 08 20 	mov #128,w0
 508 0001b6  00 80 60 	and w1,w0,w0
 509 0001b8  00 00 E0 	cp0 w0
 510              	.set ___BP___,0
 511 0001ba  00 00 32 	bra z,.L18
 754:lib/lib_pic33e/usb/usb_device.c ****     {
 755:lib/lib_pic33e/usb/usb_device.c ****         USBStallHandler();
 512              	.loc 1 755 0
 513 0001bc  00 00 07 	rcall _USBStallHandler
 514              	.L18:
 756:lib/lib_pic33e/usb/usb_device.c ****     }
 757:lib/lib_pic33e/usb/usb_device.c **** 
 758:lib/lib_pic33e/usb/usb_device.c ****     if(USBErrorIF && USBErrorIE)
 515              	.loc 1 758 0
 516 0001be  00 00 80 	mov _U1IRbits,w0
 517 0001c0  62 00 60 	and w0,#2,w0
 518 0001c2  00 00 E0 	cp0 w0
 519              	.set ___BP___,0
 520 0001c4  00 00 32 	bra z,.L19
 521 0001c6  00 00 80 	mov _U1IEbits,w0
 522 0001c8  62 00 60 	and w0,#2,w0
 523 0001ca  00 00 E0 	cp0 w0
 524              	.set ___BP___,0
 525 0001cc  00 00 32 	bra z,.L19
 759:lib/lib_pic33e/usb/usb_device.c ****     {
 760:lib/lib_pic33e/usb/usb_device.c ****         USB_ERROR_HANDLER(EVENT_BUS_ERROR,0,1);
MPLAB XC16 ASSEMBLY Listing:   			page 24


 526              	.loc 1 760 0
 527 0001ce  12 00 20 	mov #1,w2
 528 0001d0  80 00 EB 	clr w1
 529 0001d2  F0 FF 27 	mov #32767,w0
 530 0001d4  00 00 07 	rcall _USER_USB_CALLBACK_EVENT_HANDLER
 761:lib/lib_pic33e/usb/usb_device.c ****         USBClearInterruptRegister(U1EIR);               // This clears UERRIF
 531              	.loc 1 761 0
 532 0001d6  80 80 EB 	setm w1
 762:lib/lib_pic33e/usb/usb_device.c **** 
 763:lib/lib_pic33e/usb/usb_device.c ****         //On PIC18, clearing the source of the error will automatically clear
 764:lib/lib_pic33e/usb/usb_device.c ****         //  the interrupt flag.  On other devices the interrupt flag must be 
 765:lib/lib_pic33e/usb/usb_device.c ****         //  manually cleared. 
 766:lib/lib_pic33e/usb/usb_device.c ****         #if defined(__C32__) || defined(__C30__) || defined __XC16__
 767:lib/lib_pic33e/usb/usb_device.c ****             USBClearInterruptFlag( USBErrorIFReg, USBErrorIFBitNum );
 533              	.loc 1 767 0
 534 0001d8  20 00 20 	mov #2,w0
 535              	.loc 1 761 0
 536 0001da  01 00 88 	mov w1,_U1EIR
 537              	.loc 1 767 0
 538 0001dc  00 00 88 	mov w0,_U1IR
 539              	.L19:
 768:lib/lib_pic33e/usb/usb_device.c ****         #endif
 769:lib/lib_pic33e/usb/usb_device.c ****     }
 770:lib/lib_pic33e/usb/usb_device.c **** 
 771:lib/lib_pic33e/usb/usb_device.c ****     /*
 772:lib/lib_pic33e/usb/usb_device.c ****      * Pointless to continue servicing if the host has not sent a bus reset.
 773:lib/lib_pic33e/usb/usb_device.c ****      * Once bus reset is received, the device transitions into the DEFAULT
 774:lib/lib_pic33e/usb/usb_device.c ****      * state and is ready for communication.
 775:lib/lib_pic33e/usb/usb_device.c ****      */
 776:lib/lib_pic33e/usb/usb_device.c ****     if(USBDeviceState < DEFAULT_STATE)
 540              	.loc 1 776 0
 541 0001de  00 00 80 	mov _USBDeviceState,w0
 542 0001e0  E3 0F 50 	sub w0,#3,[w15]
 543              	.set ___BP___,0
 544 0001e2  00 00 3E 	bra gtu,.L20
 777:lib/lib_pic33e/usb/usb_device.c ****     {
 778:lib/lib_pic33e/usb/usb_device.c ****         USBClearUSBInterrupt();
 545              	.loc 1 778 0
 546 0001e4  00 C0 A9 	bclr.b _IFS5bits,#6
 779:lib/lib_pic33e/usb/usb_device.c ****         return;
 547              	.loc 1 779 0
 548 0001e6  00 00 37 	bra .L7
 549              	.L20:
 780:lib/lib_pic33e/usb/usb_device.c ****     }  
 781:lib/lib_pic33e/usb/usb_device.c **** 
 782:lib/lib_pic33e/usb/usb_device.c ****     /*
 783:lib/lib_pic33e/usb/usb_device.c ****      * Task D: Servicing USB Transaction Complete Interrupt
 784:lib/lib_pic33e/usb/usb_device.c ****      */
 785:lib/lib_pic33e/usb/usb_device.c ****     if(USBTransactionCompleteIE)
 550              	.loc 1 785 0
 551 0001e8  00 00 80 	mov _U1IEbits,w0
 552 0001ea  68 00 60 	and w0,#8,w0
 553 0001ec  00 00 E0 	cp0 w0
 554              	.set ___BP___,0
 555 0001ee  00 00 32 	bra z,.L21
 786:lib/lib_pic33e/usb/usb_device.c ****     {
 787:lib/lib_pic33e/usb/usb_device.c ****         for(i = 0; i < 4u; i++)	//Drain or deplete the USAT FIFO entries.  If the USB FIFO ever get
MPLAB XC16 ASSEMBLY Listing:   			page 25


 556              	.loc 1 787 0
 557 0001f0  00 40 EB 	clr.b w0
 558 0001f2  00 4F 78 	mov.b w0,[w14]
 559 0001f4  00 00 37 	bra .L22
 560              	.L28:
 788:lib/lib_pic33e/usb/usb_device.c ****         {						//utilization can be compromised, and the device won't be able to receive SETUP pack
 789:lib/lib_pic33e/usb/usb_device.c ****             if(USBTransactionCompleteIF)
 561              	.loc 1 789 0
 562 0001f6  00 00 80 	mov _U1IRbits,w0
 563 0001f8  68 00 60 	and w0,#8,w0
 564 0001fa  00 00 E0 	cp0 w0
 565              	.set ___BP___,0
 566 0001fc  00 00 32 	bra z,.L29
 790:lib/lib_pic33e/usb/usb_device.c ****             {
 791:lib/lib_pic33e/usb/usb_device.c ****                 //Save and extract USTAT register info.  Will use this info later.
 792:lib/lib_pic33e/usb/usb_device.c ****                 USTATcopy.Val = U1STAT;
 567              	.loc 1 792 0
 568 0001fe  01 00 80 	mov _U1STAT,w1
 793:lib/lib_pic33e/usb/usb_device.c ****                 endpoint_number = USBHALGetLastEndpoint(USTATcopy);
 794:lib/lib_pic33e/usb/usb_device.c **** 
 795:lib/lib_pic33e/usb/usb_device.c ****                 USBClearInterruptFlag(USBTransactionCompleteIFReg,USBTransactionCompleteIFBitNum);
 569              	.loc 1 795 0
 570 000200  80 00 20 	mov #8,w0
 571              	.loc 1 792 0
 572 000202  81 40 78 	mov.b w1,w1
 573 000204  81 41 78 	mov.b w1,w3
 574 000206  02 00 20 	mov #_USTATcopy,w2
 575 000208  03 49 78 	mov.b w3,[w2]
 576              	.loc 1 793 0
 577 00020a  01 00 20 	mov #_USTATcopy,w1
 578 00020c  00 00 00 	nop
 579 00020e  91 40 78 	mov.b [w1],w1
 580 000210  81 80 FB 	ze w1,w1
 581 000212  C4 08 DE 	lsr w1,#4,w1
 582 000214  81 40 78 	mov.b w1,w1
 583 000216  81 41 78 	mov.b w1,w3
 584 000218  02 00 20 	mov #_endpoint_number,w2
 585 00021a  03 49 78 	mov.b w3,[w2]
 586              	.loc 1 795 0
 587 00021c  00 00 88 	mov w0,_U1IR
 796:lib/lib_pic33e/usb/usb_device.c **** 
 797:lib/lib_pic33e/usb/usb_device.c ****                 //Keep track of the hardware ping pong state for endpoints other
 798:lib/lib_pic33e/usb/usb_device.c ****                 //than EP0, if ping pong buffering is enabled.
 799:lib/lib_pic33e/usb/usb_device.c ****                 #if (USB_PING_PONG_MODE == USB_PING_PONG__ALL_BUT_EP0) || (USB_PING_PONG_MODE == US
 800:lib/lib_pic33e/usb/usb_device.c ****                 if(USBHALGetLastDirection(USTATcopy) == OUT_FROM_HOST)
 588              	.loc 1 800 0
 589 00021e  00 C0 BF 	mov.b _USTATcopy,WREG
 590 000220  68 40 60 	and.b w0,#8,w0
 591 000222  00 04 E0 	cp0.b w0
 592              	.set ___BP___,0
 593 000224  00 00 3A 	bra nz,.L24
 801:lib/lib_pic33e/usb/usb_device.c ****                 {
 802:lib/lib_pic33e/usb/usb_device.c ****                     ep_data_out[endpoint_number].bits.ping_pong_state ^= 1;
 594              	.loc 1 802 0
 595 000226  01 00 20 	mov #_endpoint_number,w1
 596 000228  00 00 00 	nop
 597 00022a  91 40 78 	mov.b [w1],w1
MPLAB XC16 ASSEMBLY Listing:   			page 26


 598 00022c  00 00 20 	mov #_ep_data_out,w0
 599 00022e  01 81 FB 	ze w1,w2
 600 000230  00 00 41 	add w2,w0,w0
 601 000232  01 81 FB 	ze w1,w2
 602 000234  01 00 20 	mov #_ep_data_out,w1
 603 000236  81 00 41 	add w2,w1,w1
 604 000238  00 00 00 	nop 
 605 00023a  11 41 78 	mov.b [w1],w2
 606 00023c  E1 40 61 	and.b w2,#1,w1
 607 00023e  10 41 78 	mov.b [w0],w2
 608 000240  02 04 A1 	bclr.b w2,#0
 609 000242  01 04 A2 	btg.b w1,#0
 610 000244  E1 C0 60 	and.b w1,#1,w1
 611 000246  E1 C0 60 	and.b w1,#1,w1
 612 000248  81 40 71 	ior.b w2,w1,w1
 613 00024a  01 48 78 	mov.b w1,[w0]
 614 00024c  00 00 37 	bra .L25
 615              	.L24:
 803:lib/lib_pic33e/usb/usb_device.c ****                 }
 804:lib/lib_pic33e/usb/usb_device.c ****                 else
 805:lib/lib_pic33e/usb/usb_device.c ****                 {
 806:lib/lib_pic33e/usb/usb_device.c ****                     ep_data_in[endpoint_number].bits.ping_pong_state ^= 1;
 616              	.loc 1 806 0
 617 00024e  01 00 20 	mov #_endpoint_number,w1
 618 000250  00 00 00 	nop
 619 000252  91 40 78 	mov.b [w1],w1
 620 000254  00 00 20 	mov #_ep_data_in,w0
 621 000256  01 81 FB 	ze w1,w2
 622 000258  00 00 41 	add w2,w0,w0
 623 00025a  01 81 FB 	ze w1,w2
 624 00025c  01 00 20 	mov #_ep_data_in,w1
 625 00025e  81 00 41 	add w2,w1,w1
 626 000260  00 00 00 	nop 
 627 000262  91 41 78 	mov.b [w1],w3
 628 000264  E1 C0 61 	and.b w3,#1,w1
 629 000266  10 41 78 	mov.b [w0],w2
 630 000268  02 04 A1 	bclr.b w2,#0
 631 00026a  01 04 A2 	btg.b w1,#0
 632 00026c  E1 C0 60 	and.b w1,#1,w1
 633 00026e  E1 C0 60 	and.b w1,#1,w1
 634 000270  81 40 71 	ior.b w2,w1,w1
 635 000272  01 48 78 	mov.b w1,[w0]
 636              	.L25:
 807:lib/lib_pic33e/usb/usb_device.c ****                 }
 808:lib/lib_pic33e/usb/usb_device.c ****                 #endif
 809:lib/lib_pic33e/usb/usb_device.c **** 
 810:lib/lib_pic33e/usb/usb_device.c ****                 //USBCtrlEPService only services transactions over EP0.
 811:lib/lib_pic33e/usb/usb_device.c ****                 //It ignores all other EP transactions.
 812:lib/lib_pic33e/usb/usb_device.c ****                 if(endpoint_number == 0)
 637              	.loc 1 812 0
 638 000274  00 C0 BF 	mov.b _endpoint_number,WREG
 639 000276  00 04 E0 	cp0.b w0
 640              	.set ___BP___,0
 641 000278  00 00 3A 	bra nz,.L26
 813:lib/lib_pic33e/usb/usb_device.c ****                 {
 814:lib/lib_pic33e/usb/usb_device.c ****                     USBCtrlEPService();
 642              	.loc 1 814 0
MPLAB XC16 ASSEMBLY Listing:   			page 27


 643 00027a  00 00 07 	rcall _USBCtrlEPService
 644 00027c  00 00 37 	bra .L27
 645              	.L26:
 815:lib/lib_pic33e/usb/usb_device.c ****                 }
 816:lib/lib_pic33e/usb/usb_device.c ****                 else
 817:lib/lib_pic33e/usb/usb_device.c ****                 {
 818:lib/lib_pic33e/usb/usb_device.c ****                     USB_TRANSFER_COMPLETE_HANDLER(EVENT_TRANSFER, (uint8_t*)&USTATcopy.Val, 0);
 646              	.loc 1 818 0
 647 00027e  00 01 EB 	clr w2
 648 000280  01 00 20 	mov #_USTATcopy,w1
 649 000282  20 07 20 	mov #114,w0
 650 000284  00 00 07 	rcall _USER_USB_CALLBACK_EVENT_HANDLER
 651              	.L27:
 652              	.loc 1 787 0
 653 000286  1E 4F E8 	inc.b [w14],[w14]
 654              	.L22:
 655 000288  1E 40 78 	mov.b [w14],w0
 656 00028a  E3 4F 50 	sub.b w0,#3,[w15]
 657              	.set ___BP___,0
 658 00028c  00 00 36 	bra leu,.L28
 659 00028e  00 00 37 	bra .L21
 660              	.L29:
 661              	.L21:
 819:lib/lib_pic33e/usb/usb_device.c ****                 }
 820:lib/lib_pic33e/usb/usb_device.c ****             }//end if(USBTransactionCompleteIF)
 821:lib/lib_pic33e/usb/usb_device.c ****             else
 822:lib/lib_pic33e/usb/usb_device.c ****             {
 823:lib/lib_pic33e/usb/usb_device.c ****                 break;	//USTAT FIFO must be empty.
 824:lib/lib_pic33e/usb/usb_device.c ****             }
 825:lib/lib_pic33e/usb/usb_device.c ****         }//end for()
 826:lib/lib_pic33e/usb/usb_device.c ****     }//end if(USBTransactionCompleteIE)
 827:lib/lib_pic33e/usb/usb_device.c **** 
 828:lib/lib_pic33e/usb/usb_device.c ****     USBClearUSBInterrupt();
 662              	.loc 1 828 0
 663 000290  00 C0 A9 	bclr.b _IFS5bits,#6
 664              	.L7:
 829:lib/lib_pic33e/usb/usb_device.c **** }//end of USBDeviceTasks()
 665              	.loc 1 829 0
 666 000292  8E 07 78 	mov w14,w15
 667 000294  4F 07 78 	mov [--w15],w14
 668 000296  00 40 A9 	bclr CORCON,#2
 669 000298  00 00 06 	return 
 670              	.set ___PA___,0
 671              	.LFE1:
 672              	.size _USBDeviceTasks,.-_USBDeviceTasks
 673              	.align 2
 674              	.global _USBEnableEndpoint
 675              	.type _USBEnableEndpoint,@function
 676              	_USBEnableEndpoint:
 677              	.LFB2:
 830:lib/lib_pic33e/usb/usb_device.c **** 
 831:lib/lib_pic33e/usb/usb_device.c **** /*******************************************************************************
 832:lib/lib_pic33e/usb/usb_device.c ****   Function:
 833:lib/lib_pic33e/usb/usb_device.c ****         void USBEnableEndpoint(uint8_t ep, uint8_t options)
 834:lib/lib_pic33e/usb/usb_device.c ****     
 835:lib/lib_pic33e/usb/usb_device.c ****   Summary:
 836:lib/lib_pic33e/usb/usb_device.c ****     This function will enable the specified endpoint with the specified
MPLAB XC16 ASSEMBLY Listing:   			page 28


 837:lib/lib_pic33e/usb/usb_device.c ****     options
 838:lib/lib_pic33e/usb/usb_device.c ****   Description:
 839:lib/lib_pic33e/usb/usb_device.c ****     This function will enable the specified endpoint with the specified
 840:lib/lib_pic33e/usb/usb_device.c ****     options.
 841:lib/lib_pic33e/usb/usb_device.c ****     
 842:lib/lib_pic33e/usb/usb_device.c ****     Typical Usage:
 843:lib/lib_pic33e/usb/usb_device.c ****     <code>
 844:lib/lib_pic33e/usb/usb_device.c ****     void USBCBInitEP(void)
 845:lib/lib_pic33e/usb/usb_device.c ****     {
 846:lib/lib_pic33e/usb/usb_device.c ****         USBEnableEndpoint(MSD_DATA_IN_EP,USB_IN_ENABLED|USB_OUT_ENABLED|USB_HANDSHAKE_ENABLED|USB_D
 847:lib/lib_pic33e/usb/usb_device.c ****         USBMSDInit();
 848:lib/lib_pic33e/usb/usb_device.c ****     }
 849:lib/lib_pic33e/usb/usb_device.c ****     </code>
 850:lib/lib_pic33e/usb/usb_device.c ****     
 851:lib/lib_pic33e/usb/usb_device.c ****     In the above example endpoint number MSD_DATA_IN_EP is being configured
 852:lib/lib_pic33e/usb/usb_device.c ****     for both IN and OUT traffic with handshaking enabled. Also since
 853:lib/lib_pic33e/usb/usb_device.c ****     MSD_DATA_IN_EP is not endpoint 0 (MSD does not allow this), then we can
 854:lib/lib_pic33e/usb/usb_device.c ****     explicitly disable SETUP packets on this endpoint.
 855:lib/lib_pic33e/usb/usb_device.c ****   Conditions:
 856:lib/lib_pic33e/usb/usb_device.c ****     None
 857:lib/lib_pic33e/usb/usb_device.c ****   Input:
 858:lib/lib_pic33e/usb/usb_device.c ****     uint8_t ep -       the endpoint to be configured
 859:lib/lib_pic33e/usb/usb_device.c ****     uint8_t options -  optional settings for the endpoint. The options should
 860:lib/lib_pic33e/usb/usb_device.c ****                     be ORed together to form a single options string. The
 861:lib/lib_pic33e/usb/usb_device.c ****                     available optional settings for the endpoint. The
 862:lib/lib_pic33e/usb/usb_device.c ****                     options should be ORed together to form a single options
 863:lib/lib_pic33e/usb/usb_device.c ****                     string. The available options are the following\:
 864:lib/lib_pic33e/usb/usb_device.c ****                     * USB_HANDSHAKE_ENABLED enables USB handshaking (ACK,
 865:lib/lib_pic33e/usb/usb_device.c ****                       NAK)
 866:lib/lib_pic33e/usb/usb_device.c ****                     * USB_HANDSHAKE_DISABLED disables USB handshaking (ACK,
 867:lib/lib_pic33e/usb/usb_device.c ****                       NAK)
 868:lib/lib_pic33e/usb/usb_device.c ****                     * USB_OUT_ENABLED enables the out direction
 869:lib/lib_pic33e/usb/usb_device.c ****                     * USB_OUT_DISABLED disables the out direction
 870:lib/lib_pic33e/usb/usb_device.c ****                     * USB_IN_ENABLED enables the in direction
 871:lib/lib_pic33e/usb/usb_device.c ****                     * USB_IN_DISABLED disables the in direction
 872:lib/lib_pic33e/usb/usb_device.c ****                     * USB_ALLOW_SETUP enables control transfers
 873:lib/lib_pic33e/usb/usb_device.c ****                     * USB_DISALLOW_SETUP disables control transfers
 874:lib/lib_pic33e/usb/usb_device.c ****                     * USB_STALL_ENDPOINT STALLs this endpoint
 875:lib/lib_pic33e/usb/usb_device.c ****   Return:
 876:lib/lib_pic33e/usb/usb_device.c ****     None
 877:lib/lib_pic33e/usb/usb_device.c ****   Remarks:
 878:lib/lib_pic33e/usb/usb_device.c ****     None                                                                                           
 879:lib/lib_pic33e/usb/usb_device.c ****   *****************************************************************************/
 880:lib/lib_pic33e/usb/usb_device.c **** void USBEnableEndpoint(uint8_t ep, uint8_t options)
 881:lib/lib_pic33e/usb/usb_device.c **** {
 678              	.loc 1 881 0
 679              	.set ___PA___,1
 680 00029a  04 00 FA 	lnk #4
 681              	.LCFI3:
 682              	.loc 1 881 0
 683 00029c  20 47 98 	mov.b w0,[w14+2]
 684 00029e  31 47 98 	mov.b w1,[w14+3]
 882:lib/lib_pic33e/usb/usb_device.c ****     unsigned char* p;
 883:lib/lib_pic33e/usb/usb_device.c ****         
 884:lib/lib_pic33e/usb/usb_device.c ****     //Use USBConfigureEndpoint() to set up the pBDTEntryIn/Out[ep] pointer and 
 885:lib/lib_pic33e/usb/usb_device.c ****     //starting DTS state in the BDT entry.
 886:lib/lib_pic33e/usb/usb_device.c ****     if(options & USB_OUT_ENABLED)
MPLAB XC16 ASSEMBLY Listing:   			page 29


 685              	.loc 1 886 0
 686 0002a0  3E 40 90 	mov.b [w14+3],w0
 687 0002a2  00 80 FB 	ze w0,w0
 688 0002a4  68 00 60 	and w0,#8,w0
 689 0002a6  00 00 E0 	cp0 w0
 690              	.set ___BP___,0
 691 0002a8  00 00 32 	bra z,.L31
 887:lib/lib_pic33e/usb/usb_device.c ****     {
 888:lib/lib_pic33e/usb/usb_device.c ****         USBConfigureEndpoint(ep, OUT_FROM_HOST);
 692              	.loc 1 888 0
 693 0002aa  80 40 EB 	clr.b w1
 694 0002ac  2E 40 90 	mov.b [w14+2],w0
 695 0002ae  00 00 07 	rcall _USBConfigureEndpoint
 696              	.L31:
 889:lib/lib_pic33e/usb/usb_device.c ****     }
 890:lib/lib_pic33e/usb/usb_device.c ****     if(options & USB_IN_ENABLED)
 697              	.loc 1 890 0
 698 0002b0  00 00 00 	nop 
 699 0002b2  3E 40 90 	mov.b [w14+3],w0
 700 0002b4  00 80 FB 	ze w0,w0
 701 0002b6  64 00 60 	and w0,#4,w0
 702 0002b8  00 00 E0 	cp0 w0
 703              	.set ___BP___,0
 704 0002ba  00 00 32 	bra z,.L32
 891:lib/lib_pic33e/usb/usb_device.c ****     {
 892:lib/lib_pic33e/usb/usb_device.c ****         USBConfigureEndpoint(ep, IN_TO_HOST);
 705              	.loc 1 892 0
 706 0002bc  11 C0 B3 	mov.b #1,w1
 707 0002be  2E 40 90 	mov.b [w14+2],w0
 708 0002c0  00 00 07 	rcall _USBConfigureEndpoint
 709              	.L32:
 893:lib/lib_pic33e/usb/usb_device.c ****     }
 894:lib/lib_pic33e/usb/usb_device.c **** 
 895:lib/lib_pic33e/usb/usb_device.c ****     //Update the relevant UEPx register to actually enable the endpoint with
 896:lib/lib_pic33e/usb/usb_device.c ****     //the specified options (ex: handshaking enabled, control transfers allowed,
 897:lib/lib_pic33e/usb/usb_device.c ****     //etc.)
 898:lib/lib_pic33e/usb/usb_device.c ****     #if defined(__C32__)
 899:lib/lib_pic33e/usb/usb_device.c ****         p = (unsigned char*)(&U1EP0+(4*ep));
 900:lib/lib_pic33e/usb/usb_device.c ****     #else
 901:lib/lib_pic33e/usb/usb_device.c ****         p = (unsigned char*)(&U1EP0+ep);
 710              	.loc 1 901 0
 711 0002c2  00 00 00 	nop 
 712 0002c4  2E 40 90 	mov.b [w14+2],w0
 713 0002c6  01 00 20 	mov #_U1EP0,w1
 714 0002c8  00 80 FB 	ze w0,w0
 715 0002ca  00 00 40 	add w0,w0,w0
 716 0002cc  00 8F 40 	add w1,w0,[w14]
 902:lib/lib_pic33e/usb/usb_device.c ****     #endif
 903:lib/lib_pic33e/usb/usb_device.c ****     *p = options;
 717              	.loc 1 903 0
 718 0002ce  1E 00 78 	mov [w14],w0
 719 0002d0  BE 40 90 	mov.b [w14+3],w1
 720 0002d2  01 48 78 	mov.b w1,[w0]
 904:lib/lib_pic33e/usb/usb_device.c **** }
 721              	.loc 1 904 0
 722 0002d4  8E 07 78 	mov w14,w15
 723 0002d6  4F 07 78 	mov [--w15],w14
MPLAB XC16 ASSEMBLY Listing:   			page 30


 724 0002d8  00 40 A9 	bclr CORCON,#2
 725 0002da  00 00 06 	return 
 726              	.set ___PA___,0
 727              	.LFE2:
 728              	.size _USBEnableEndpoint,.-_USBEnableEndpoint
 729              	.align 2
 730              	.global _USBTransferOnePacket
 731              	.type _USBTransferOnePacket,@function
 732              	_USBTransferOnePacket:
 733              	.LFB3:
 905:lib/lib_pic33e/usb/usb_device.c **** 
 906:lib/lib_pic33e/usb/usb_device.c **** 
 907:lib/lib_pic33e/usb/usb_device.c **** /*************************************************************************
 908:lib/lib_pic33e/usb/usb_device.c ****   Function:
 909:lib/lib_pic33e/usb/usb_device.c ****     USB_HANDLE USBTransferOnePacket(uint8_t ep, uint8_t dir, uint8_t* data, uint8_t len)
 910:lib/lib_pic33e/usb/usb_device.c ****     
 911:lib/lib_pic33e/usb/usb_device.c ****   Summary:
 912:lib/lib_pic33e/usb/usb_device.c ****     Transfers a single packet (one transaction) of data on the USB bus.
 913:lib/lib_pic33e/usb/usb_device.c **** 
 914:lib/lib_pic33e/usb/usb_device.c ****   Description:
 915:lib/lib_pic33e/usb/usb_device.c ****     The USBTransferOnePacket() function prepares a USB endpoint
 916:lib/lib_pic33e/usb/usb_device.c ****     so that it may send data to the host (an IN transaction), or 
 917:lib/lib_pic33e/usb/usb_device.c ****     receive data from the host (an OUT transaction).  The 
 918:lib/lib_pic33e/usb/usb_device.c ****     USBTransferOnePacket() function can be used both to receive	and 
 919:lib/lib_pic33e/usb/usb_device.c ****     send data to the host.  This function is the primary API function 
 920:lib/lib_pic33e/usb/usb_device.c ****     provided by the USB stack firmware for sending or receiving application 
 921:lib/lib_pic33e/usb/usb_device.c ****     data over the USB port.  
 922:lib/lib_pic33e/usb/usb_device.c **** 
 923:lib/lib_pic33e/usb/usb_device.c ****     The USBTransferOnePacket() is intended for use with all application 
 924:lib/lib_pic33e/usb/usb_device.c ****     endpoints.  It is not used for sending or receiving application data 
 925:lib/lib_pic33e/usb/usb_device.c ****     through endpoint 0 by using control transfers.  Separate API 
 926:lib/lib_pic33e/usb/usb_device.c ****     functions, such as USBEP0Receive(), USBEP0SendRAMPtr(), and
 927:lib/lib_pic33e/usb/usb_device.c ****     USBEP0SendROMPtr() are provided for this purpose.
 928:lib/lib_pic33e/usb/usb_device.c **** 
 929:lib/lib_pic33e/usb/usb_device.c ****     The	USBTransferOnePacket() writes to the Buffer Descriptor Table (BDT)
 930:lib/lib_pic33e/usb/usb_device.c ****     entry associated with an endpoint buffer, and sets the UOWN bit, which 
 931:lib/lib_pic33e/usb/usb_device.c ****     prepares the USB hardware to allow the transaction to complete.  The 
 932:lib/lib_pic33e/usb/usb_device.c ****     application firmware can use the USBHandleBusy() macro to check the 
 933:lib/lib_pic33e/usb/usb_device.c ****     status of the transaction, to see if the data has been successfully 
 934:lib/lib_pic33e/usb/usb_device.c ****     transmitted yet.
 935:lib/lib_pic33e/usb/usb_device.c **** 
 936:lib/lib_pic33e/usb/usb_device.c **** 
 937:lib/lib_pic33e/usb/usb_device.c ****     Typical Usage
 938:lib/lib_pic33e/usb/usb_device.c ****     <code>
 939:lib/lib_pic33e/usb/usb_device.c ****     //make sure that the we are in the configured state
 940:lib/lib_pic33e/usb/usb_device.c ****     if(USBGetDeviceState() == CONFIGURED_STATE)
 941:lib/lib_pic33e/usb/usb_device.c ****     {
 942:lib/lib_pic33e/usb/usb_device.c ****         //make sure that the last transaction isn't busy by checking the handle
 943:lib/lib_pic33e/usb/usb_device.c ****         if(!USBHandleBusy(USBInHandle))
 944:lib/lib_pic33e/usb/usb_device.c ****         {
 945:lib/lib_pic33e/usb/usb_device.c **** 	        //Write the new data that we wish to send to the host to the INPacket[] array
 946:lib/lib_pic33e/usb/usb_device.c **** 	        INPacket[0] = USEFUL_APPLICATION_VALUE1;
 947:lib/lib_pic33e/usb/usb_device.c **** 	        INPacket[1] = USEFUL_APPLICATION_VALUE2;
 948:lib/lib_pic33e/usb/usb_device.c **** 	        //INPacket[2] = ... (fill in the rest of the packet data)
 949:lib/lib_pic33e/usb/usb_device.c **** 	      
 950:lib/lib_pic33e/usb/usb_device.c ****             //Send the data contained in the INPacket[] array through endpoint "EP_NUM"
 951:lib/lib_pic33e/usb/usb_device.c ****             USBInHandle = USBTransferOnePacket(EP_NUM,IN_TO_HOST,(uint8_t*)&INPacket[0],sizeof(INPa
MPLAB XC16 ASSEMBLY Listing:   			page 31


 952:lib/lib_pic33e/usb/usb_device.c ****         }
 953:lib/lib_pic33e/usb/usb_device.c ****     }
 954:lib/lib_pic33e/usb/usb_device.c ****     </code>
 955:lib/lib_pic33e/usb/usb_device.c **** 
 956:lib/lib_pic33e/usb/usb_device.c ****   Conditions:
 957:lib/lib_pic33e/usb/usb_device.c ****     Before calling USBTransferOnePacket(), the following should be true.
 958:lib/lib_pic33e/usb/usb_device.c ****     1.  The USB stack has already been initialized (USBDeviceInit() was called).
 959:lib/lib_pic33e/usb/usb_device.c ****     2.  A transaction is not already pending on the specified endpoint.  This
 960:lib/lib_pic33e/usb/usb_device.c ****         is done by checking the previous request using the USBHandleBusy() 
 961:lib/lib_pic33e/usb/usb_device.c ****         macro (see the typical usage example).
 962:lib/lib_pic33e/usb/usb_device.c ****     3.  The host has already sent a set configuration request and the 
 963:lib/lib_pic33e/usb/usb_device.c ****         enumeration process is complete.
 964:lib/lib_pic33e/usb/usb_device.c ****         This can be checked by verifying that the USBGetDeviceState() 
 965:lib/lib_pic33e/usb/usb_device.c ****         macro returns "CONFIGURED_STATE", prior to calling 
 966:lib/lib_pic33e/usb/usb_device.c ****         USBTransferOnePacket().
 967:lib/lib_pic33e/usb/usb_device.c ****  					
 968:lib/lib_pic33e/usb/usb_device.c ****   Input:
 969:lib/lib_pic33e/usb/usb_device.c ****     uint8_t ep - The endpoint number that the data will be transmitted or
 970:lib/lib_pic33e/usb/usb_device.c **** 	          received on
 971:lib/lib_pic33e/usb/usb_device.c ****     uint8_t dir - The direction of the transfer
 972:lib/lib_pic33e/usb/usb_device.c ****                This value is either OUT_FROM_HOST or IN_TO_HOST
 973:lib/lib_pic33e/usb/usb_device.c ****     uint8_t* data - For IN transactions: pointer to the RAM buffer containing
 974:lib/lib_pic33e/usb/usb_device.c ****                  the data to be sent to the host.  For OUT transactions: pointer
 975:lib/lib_pic33e/usb/usb_device.c ****                  to the RAM buffer that the received data should get written to.
 976:lib/lib_pic33e/usb/usb_device.c ****    uint8_t len - Length of the data needing to be sent (for IN transactions).
 977:lib/lib_pic33e/usb/usb_device.c ****               For OUT transactions, the len parameter should normally be set
 978:lib/lib_pic33e/usb/usb_device.c ****               to the endpoint size specified in the endpoint descriptor.    
 979:lib/lib_pic33e/usb/usb_device.c **** 
 980:lib/lib_pic33e/usb/usb_device.c ****   Return Values:
 981:lib/lib_pic33e/usb/usb_device.c ****     USB_HANDLE - handle to the transfer.  The handle is a pointer to 
 982:lib/lib_pic33e/usb/usb_device.c ****                  the BDT entry associated with this transaction.  The
 983:lib/lib_pic33e/usb/usb_device.c ****                  status of the transaction (ex: if it is complete or still
 984:lib/lib_pic33e/usb/usb_device.c ****                  pending) can be checked using the USBHandleBusy() macro
 985:lib/lib_pic33e/usb/usb_device.c ****                  and supplying the USB_HANDLE provided by
 986:lib/lib_pic33e/usb/usb_device.c ****                  USBTransferOnePacket().
 987:lib/lib_pic33e/usb/usb_device.c **** 
 988:lib/lib_pic33e/usb/usb_device.c ****   Remarks:
 989:lib/lib_pic33e/usb/usb_device.c ****     If calling the USBTransferOnePacket() function from within the USBCBInitEP()
 990:lib/lib_pic33e/usb/usb_device.c ****     callback function, the set configuration is still being processed and the
 991:lib/lib_pic33e/usb/usb_device.c ****     USBDeviceState may not be == CONFIGURED_STATE yet.  In this	special case, 
 992:lib/lib_pic33e/usb/usb_device.c ****     the USBTransferOnePacket() may still be called, but make sure that the 
 993:lib/lib_pic33e/usb/usb_device.c ****     endpoint has been enabled and initialized by the USBEnableEndpoint() 
 994:lib/lib_pic33e/usb/usb_device.c ****     function first.  
 995:lib/lib_pic33e/usb/usb_device.c ****     
 996:lib/lib_pic33e/usb/usb_device.c ****   *************************************************************************/
 997:lib/lib_pic33e/usb/usb_device.c **** USB_HANDLE USBTransferOnePacket(uint8_t ep,uint8_t dir,uint8_t* data,uint8_t len)
 998:lib/lib_pic33e/usb/usb_device.c **** {
 734              	.loc 1 998 0
 735              	.set ___PA___,1
 736 0002dc  08 00 FA 	lnk #8
 737              	.LCFI4:
 738              	.loc 1 998 0
 739 0002de  20 47 98 	mov.b w0,[w14+2]
 740 0002e0  31 47 98 	mov.b w1,[w14+3]
 741 0002e2  22 07 98 	mov w2,[w14+4]
 742 0002e4  63 47 98 	mov.b w3,[w14+6]
 999:lib/lib_pic33e/usb/usb_device.c ****     volatile BDT_ENTRY* handle;
MPLAB XC16 ASSEMBLY Listing:   			page 32


1000:lib/lib_pic33e/usb/usb_device.c **** 
1001:lib/lib_pic33e/usb/usb_device.c ****     //If the direction is IN
1002:lib/lib_pic33e/usb/usb_device.c ****     if(dir != 0)
 743              	.loc 1 1002 0
 744 0002e6  3E 40 90 	mov.b [w14+3],w0
 745 0002e8  00 04 E0 	cp0.b w0
 746              	.set ___BP___,0
 747 0002ea  00 00 32 	bra z,.L34
1003:lib/lib_pic33e/usb/usb_device.c ****     {
1004:lib/lib_pic33e/usb/usb_device.c ****         //point to the IN BDT of the specified endpoint
1005:lib/lib_pic33e/usb/usb_device.c ****         handle = pBDTEntryIn[ep];
 748              	.loc 1 1005 0
 749 0002ec  2E 40 90 	mov.b [w14+2],w0
 750 0002ee  01 00 20 	mov #_pBDTEntryIn,w1
 751 0002f0  00 80 FB 	ze w0,w0
 752 0002f2  00 00 40 	add w0,w0,w0
 753 0002f4  01 00 40 	add w0,w1,w0
 754 0002f6  10 0F 78 	mov [w0],[w14]
 755 0002f8  00 00 37 	bra .L35
 756              	.L34:
1006:lib/lib_pic33e/usb/usb_device.c ****     }
1007:lib/lib_pic33e/usb/usb_device.c ****     else
1008:lib/lib_pic33e/usb/usb_device.c ****     {
1009:lib/lib_pic33e/usb/usb_device.c ****         //else point to the OUT BDT of the specified endpoint
1010:lib/lib_pic33e/usb/usb_device.c ****         handle = pBDTEntryOut[ep];
 757              	.loc 1 1010 0
 758 0002fa  2E 40 90 	mov.b [w14+2],w0
 759 0002fc  01 00 20 	mov #_pBDTEntryOut,w1
 760 0002fe  00 80 FB 	ze w0,w0
 761 000300  00 00 40 	add w0,w0,w0
 762 000302  01 00 40 	add w0,w1,w0
 763 000304  10 0F 78 	mov [w0],[w14]
 764              	.L35:
1011:lib/lib_pic33e/usb/usb_device.c ****     }
1012:lib/lib_pic33e/usb/usb_device.c ****     
1013:lib/lib_pic33e/usb/usb_device.c ****     //Error checking code.  Make sure the handle (pBDTEntryIn[ep] or
1014:lib/lib_pic33e/usb/usb_device.c ****     //pBDTEntryOut[ep]) is initialized before using it.
1015:lib/lib_pic33e/usb/usb_device.c ****     if(handle == 0)
 765              	.loc 1 1015 0
 766 000306  1E 00 78 	mov [w14],w0
 767 000308  00 00 E0 	cp0 w0
 768              	.set ___BP___,0
 769 00030a  00 00 3A 	bra nz,.L36
1016:lib/lib_pic33e/usb/usb_device.c ****     {
1017:lib/lib_pic33e/usb/usb_device.c ****         return 0;
 770              	.loc 1 1017 0
 771 00030c  00 00 EB 	clr w0
 772 00030e  00 00 37 	bra .L37
 773              	.L36:
1018:lib/lib_pic33e/usb/usb_device.c ****     }
1019:lib/lib_pic33e/usb/usb_device.c **** 
1020:lib/lib_pic33e/usb/usb_device.c ****     //Toggle the DTS bit if required
1021:lib/lib_pic33e/usb/usb_device.c ****     #if (USB_PING_PONG_MODE == USB_PING_PONG__NO_PING_PONG)
1022:lib/lib_pic33e/usb/usb_device.c ****         handle->STAT.Val ^= _DTSMASK;
1023:lib/lib_pic33e/usb/usb_device.c ****     #elif (USB_PING_PONG_MODE == USB_PING_PONG__EP0_OUT_ONLY)
1024:lib/lib_pic33e/usb/usb_device.c ****         if(ep != 0)
1025:lib/lib_pic33e/usb/usb_device.c ****         {
MPLAB XC16 ASSEMBLY Listing:   			page 33


1026:lib/lib_pic33e/usb/usb_device.c ****             handle->STAT.Val ^= _DTSMASK;
1027:lib/lib_pic33e/usb/usb_device.c ****         }
1028:lib/lib_pic33e/usb/usb_device.c ****     #endif
1029:lib/lib_pic33e/usb/usb_device.c **** 
1030:lib/lib_pic33e/usb/usb_device.c ****     //Set the data pointer, data length, and enable the endpoint
1031:lib/lib_pic33e/usb/usb_device.c ****     handle->ADR = ConvertToPhysicalAddress(data);
 774              	.loc 1 1031 0
 775 000310  1E 00 78 	mov [w14],w0
 776 000312  AE 00 90 	mov [w14+4],w1
 777 000314  C0 41 90 	mov.b [w0+4],w3
 778 000316  F2 0F 20 	mov #255,w2
 779 000318  E0 C1 61 	and.b w3,#0,w3
 780 00031a  02 81 60 	and w1,w2,w2
 781 00031c  C8 08 DE 	lsr w1,#8,w1
 782 00031e  02 C1 71 	ior.b w3,w2,w2
 783 000320  42 40 98 	mov.b w2,[w0+4]
 784 000322  50 41 90 	mov.b [w0+5],w2
 785 000324  60 41 61 	and.b w2,#0,w2
 786 000326  81 40 71 	ior.b w2,w1,w1
 787 000328  51 40 98 	mov.b w1,[w0+5]
1032:lib/lib_pic33e/usb/usb_device.c ****     handle->CNT = len;
 788              	.loc 1 1032 0
 789 00032a  1E 00 78 	mov [w14],w0
 790 00032c  EE 40 90 	mov.b [w14+6],w1
 791 00032e  A0 41 90 	mov.b [w0+2],w3
 792 000330  81 80 FB 	ze w1,w1
 793 000332  F4 3F 20 	mov #1023,w4
 794 000334  F2 0F 20 	mov #255,w2
 795 000336  84 80 60 	and w1,w4,w1
 796 000338  E0 C1 61 	and.b w3,#0,w3
 797 00033a  02 81 60 	and w1,w2,w2
 798 00033c  C8 08 DE 	lsr w1,#8,w1
 799 00033e  02 C1 71 	ior.b w3,w2,w2
 800 000340  E3 80 60 	and w1,#3,w1
 801 000342  22 40 98 	mov.b w2,[w0+2]
 802 000344  E3 C0 60 	and.b w1,#3,w1
 803 000346  B0 41 90 	mov.b [w0+3],w3
 804 000348  C2 CF B3 	mov.b #-4,w2
 805 00034a  02 C1 61 	and.b w3,w2,w2
 806 00034c  81 40 71 	ior.b w2,w1,w1
 807 00034e  31 40 98 	mov.b w1,[w0+3]
1033:lib/lib_pic33e/usb/usb_device.c ****     handle->STAT.Val &= _DTSMASK;
 808              	.loc 1 1033 0
 809 000350  9E 00 78 	mov [w14],w1
 810 000352  1E 00 78 	mov [w14],w0
 811 000354  91 82 FB 	ze [w1],w5
 812 000356  04 04 20 	mov #64,w4
 813 000358  91 40 90 	mov.b [w1+1],w1
 814 00035a  F2 0F 20 	mov #255,w2
 815 00035c  90 41 78 	mov.b [w0],w3
 816 00035e  81 80 FB 	ze w1,w1
 817 000360  E0 C1 61 	and.b w3,#0,w3
 818 000362  C8 08 DD 	sl w1,#8,w1
 819 000364  81 80 72 	ior w5,w1,w1
 820 000366  84 80 60 	and w1,w4,w1
 821 000368  02 81 60 	and w1,w2,w2
 822 00036a  02 C1 71 	ior.b w3,w2,w2
MPLAB XC16 ASSEMBLY Listing:   			page 34


 823 00036c  02 48 78 	mov.b w2,[w0]
 824 00036e  C8 08 DE 	lsr w1,#8,w1
 825 000370  10 41 90 	mov.b [w0+1],w2
 826 000372  60 41 61 	and.b w2,#0,w2
 827 000374  81 40 71 	ior.b w2,w1,w1
 828 000376  11 40 98 	mov.b w1,[w0+1]
1034:lib/lib_pic33e/usb/usb_device.c ****     handle->STAT.Val |= (_DTSEN & _DTS_CHECKING_ENABLED);
 829              	.loc 1 1034 0
 830 000378  9E 00 78 	mov [w14],w1
 831 00037a  1E 00 78 	mov [w14],w0
 832 00037c  11 82 FB 	ze [w1],w4
 833 00037e  F2 0F 20 	mov #255,w2
 834 000380  91 40 90 	mov.b [w1+1],w1
 835 000382  90 41 78 	mov.b [w0],w3
 836 000384  81 80 FB 	ze w1,w1
 837 000386  E0 C1 61 	and.b w3,#0,w3
 838 000388  C8 08 DD 	sl w1,#8,w1
 839 00038a  81 00 72 	ior w4,w1,w1
 840 00038c  01 30 A0 	bset w1,#3
 841 00038e  02 81 60 	and w1,w2,w2
 842 000390  02 C1 71 	ior.b w3,w2,w2
 843 000392  02 48 78 	mov.b w2,[w0]
 844 000394  C8 08 DE 	lsr w1,#8,w1
 845 000396  10 41 90 	mov.b [w0+1],w2
 846 000398  60 41 61 	and.b w2,#0,w2
 847 00039a  81 40 71 	ior.b w2,w1,w1
 848 00039c  11 40 98 	mov.b w1,[w0+1]
1035:lib/lib_pic33e/usb/usb_device.c ****     handle->STAT.Val |= _USIE;
 849              	.loc 1 1035 0
 850 00039e  1E 00 78 	mov [w14],w0
 851 0003a0  9E 00 78 	mov [w14],w1
 852 0003a2  F2 0F 20 	mov #255,w2
 853 0003a4  11 82 FB 	ze [w1],w4
 854 0003a6  91 40 90 	mov.b [w1+1],w1
 855 0003a8  90 41 78 	mov.b [w0],w3
 856 0003aa  81 80 FB 	ze w1,w1
 857 0003ac  E0 C1 61 	and.b w3,#0,w3
 858 0003ae  C8 08 DD 	sl w1,#8,w1
 859 0003b0  81 00 72 	ior w4,w1,w1
 860 0003b2  01 70 A0 	bset w1,#7
 861 0003b4  02 81 60 	and w1,w2,w2
 862 0003b6  C8 08 DE 	lsr w1,#8,w1
 863 0003b8  02 C1 71 	ior.b w3,w2,w2
 864 0003ba  02 48 78 	mov.b w2,[w0]
 865 0003bc  10 41 90 	mov.b [w0+1],w2
 866 0003be  60 41 61 	and.b w2,#0,w2
 867 0003c0  81 40 71 	ior.b w2,w1,w1
 868 0003c2  11 40 98 	mov.b w1,[w0+1]
1036:lib/lib_pic33e/usb/usb_device.c **** 
1037:lib/lib_pic33e/usb/usb_device.c ****     //Point to the next buffer for ping pong purposes.
1038:lib/lib_pic33e/usb/usb_device.c ****     if(dir != OUT_FROM_HOST)
 869              	.loc 1 1038 0
 870 0003c4  3E 40 90 	mov.b [w14+3],w0
 871 0003c6  00 04 E0 	cp0.b w0
 872              	.set ___BP___,0
 873 0003c8  00 00 32 	bra z,.L38
1039:lib/lib_pic33e/usb/usb_device.c ****     {
MPLAB XC16 ASSEMBLY Listing:   			page 35


1040:lib/lib_pic33e/usb/usb_device.c ****         //toggle over the to the next buffer for an IN endpoint
1041:lib/lib_pic33e/usb/usb_device.c ****         pBDTEntryIn[ep] = (BDT_ENTRY*)(((uintptr_t)pBDTEntryIn[ep]) ^ USB_NEXT_PING_PONG);
 874              	.loc 1 1041 0
 875 0003ca  2E 40 90 	mov.b [w14+2],w0
 876 0003cc  01 00 20 	mov #_pBDTEntryIn,w1
 877 0003ce  00 80 FB 	ze w0,w0
 878 0003d0  00 00 40 	add w0,w0,w0
 879 0003d2  81 00 40 	add w0,w1,w1
 880 0003d4  2E 40 90 	mov.b [w14+2],w0
 881 0003d6  00 00 00 	nop 
 882 0003d8  91 00 78 	mov [w1],w1
 883 0003da  00 80 FB 	ze w0,w0
 884 0003dc  00 01 40 	add w0,w0,w2
 885 0003de  01 30 A2 	btg w1,#3
 886 0003e0  00 00 20 	mov #_pBDTEntryIn,w0
 887 0003e2  00 00 41 	add w2,w0,w0
 888 0003e4  01 08 78 	mov w1,[w0]
 889 0003e6  00 00 37 	bra .L39
 890              	.L38:
1042:lib/lib_pic33e/usb/usb_device.c ****     }
1043:lib/lib_pic33e/usb/usb_device.c ****     else
1044:lib/lib_pic33e/usb/usb_device.c ****     {
1045:lib/lib_pic33e/usb/usb_device.c ****         //toggle over the to the next buffer for an OUT endpoint
1046:lib/lib_pic33e/usb/usb_device.c ****         pBDTEntryOut[ep] = (BDT_ENTRY*)(((uintptr_t)pBDTEntryOut[ep]) ^ USB_NEXT_PING_PONG);
 891              	.loc 1 1046 0
 892 0003e8  00 00 00 	nop 
 893 0003ea  2E 40 90 	mov.b [w14+2],w0
 894 0003ec  01 00 20 	mov #_pBDTEntryOut,w1
 895 0003ee  00 80 FB 	ze w0,w0
 896 0003f0  00 00 40 	add w0,w0,w0
 897 0003f2  81 00 40 	add w0,w1,w1
 898 0003f4  2E 40 90 	mov.b [w14+2],w0
 899 0003f6  00 00 00 	nop 
 900 0003f8  91 00 78 	mov [w1],w1
 901 0003fa  00 80 FB 	ze w0,w0
 902 0003fc  00 01 40 	add w0,w0,w2
 903 0003fe  01 30 A2 	btg w1,#3
 904 000400  00 00 20 	mov #_pBDTEntryOut,w0
 905 000402  00 00 41 	add w2,w0,w0
 906 000404  01 08 78 	mov w1,[w0]
 907              	.L39:
1047:lib/lib_pic33e/usb/usb_device.c ****     }
1048:lib/lib_pic33e/usb/usb_device.c ****     return (USB_HANDLE)handle;
 908              	.loc 1 1048 0
 909 000406  00 00 00 	nop 
 910 000408  1E 00 78 	mov [w14],w0
 911              	.L37:
1049:lib/lib_pic33e/usb/usb_device.c **** }
 912              	.loc 1 1049 0
 913 00040a  8E 07 78 	mov w14,w15
 914 00040c  4F 07 78 	mov [--w15],w14
 915 00040e  00 40 A9 	bclr CORCON,#2
 916 000410  00 00 06 	return 
 917              	.set ___PA___,0
 918              	.LFE3:
 919              	.size _USBTransferOnePacket,.-_USBTransferOnePacket
 920              	.align 2
MPLAB XC16 ASSEMBLY Listing:   			page 36


 921              	.global _USBStallEndpoint
 922              	.type _USBStallEndpoint,@function
 923              	_USBStallEndpoint:
 924              	.LFB4:
1050:lib/lib_pic33e/usb/usb_device.c **** 
1051:lib/lib_pic33e/usb/usb_device.c **** 
1052:lib/lib_pic33e/usb/usb_device.c **** /********************************************************************
1053:lib/lib_pic33e/usb/usb_device.c ****     Function:
1054:lib/lib_pic33e/usb/usb_device.c ****         void USBStallEndpoint(uint8_t ep, uint8_t dir)
1055:lib/lib_pic33e/usb/usb_device.c ****         
1056:lib/lib_pic33e/usb/usb_device.c ****     Summary:
1057:lib/lib_pic33e/usb/usb_device.c ****          Configures the specified endpoint to send STALL to the host, the next
1058:lib/lib_pic33e/usb/usb_device.c ****          time the host tries to access the endpoint.
1059:lib/lib_pic33e/usb/usb_device.c ****     
1060:lib/lib_pic33e/usb/usb_device.c ****     PreCondition:
1061:lib/lib_pic33e/usb/usb_device.c ****         None
1062:lib/lib_pic33e/usb/usb_device.c ****         
1063:lib/lib_pic33e/usb/usb_device.c ****     Parameters:
1064:lib/lib_pic33e/usb/usb_device.c ****         uint8_t ep - The endpoint number that should be configured to send STALL.
1065:lib/lib_pic33e/usb/usb_device.c ****         uint8_t dir - The direction of the endpoint to STALL, either
1066:lib/lib_pic33e/usb/usb_device.c ****                    IN_TO_HOST or OUT_FROM_HOST.
1067:lib/lib_pic33e/usb/usb_device.c ****         
1068:lib/lib_pic33e/usb/usb_device.c ****     Return Values:
1069:lib/lib_pic33e/usb/usb_device.c ****         None
1070:lib/lib_pic33e/usb/usb_device.c ****         
1071:lib/lib_pic33e/usb/usb_device.c ****     Remarks:
1072:lib/lib_pic33e/usb/usb_device.c ****         None
1073:lib/lib_pic33e/usb/usb_device.c **** 
1074:lib/lib_pic33e/usb/usb_device.c ****  *******************************************************************/
1075:lib/lib_pic33e/usb/usb_device.c **** void USBStallEndpoint(uint8_t ep, uint8_t dir)
1076:lib/lib_pic33e/usb/usb_device.c **** {
 925              	.loc 1 1076 0
 926              	.set ___PA___,1
 927 000412  04 00 FA 	lnk #4
 928              	.LCFI5:
 929              	.loc 1 1076 0
 930 000414  20 47 98 	mov.b w0,[w14+2]
 931 000416  31 47 98 	mov.b w1,[w14+3]
1077:lib/lib_pic33e/usb/usb_device.c ****     BDT_ENTRY *p;
1078:lib/lib_pic33e/usb/usb_device.c **** 
1079:lib/lib_pic33e/usb/usb_device.c ****     if(ep == 0)
 932              	.loc 1 1079 0
 933 000418  2E 40 90 	mov.b [w14+2],w0
 934 00041a  00 04 E0 	cp0.b w0
 935              	.set ___BP___,0
 936 00041c  00 00 3A 	bra nz,.L41
1080:lib/lib_pic33e/usb/usb_device.c ****     {
1081:lib/lib_pic33e/usb/usb_device.c ****         //For control endpoints (ex: EP0), we need to STALL both IN and OUT
1082:lib/lib_pic33e/usb/usb_device.c ****         //endpoints.  EP0 OUT must also be prepared to receive the next SETUP 
1083:lib/lib_pic33e/usb/usb_device.c ****         //packet that will arrive.
1084:lib/lib_pic33e/usb/usb_device.c ****         pBDTEntryEP0OutNext->CNT = USB_EP0_BUFF_SIZE;
 937              	.loc 1 1084 0
 938 00041e  00 00 80 	mov _pBDTEntryEP0OutNext,w0
 939 000420  C1 CF B3 	mov.b #-4,w1
 940 000422  20 41 90 	mov.b [w0+2],w2
 941 000424  60 41 61 	and.b w2,#0,w2
 942 000426  02 34 A0 	bset.b w2,#3
MPLAB XC16 ASSEMBLY Listing:   			page 37


 943 000428  22 40 98 	mov.b w2,[w0+2]
 944 00042a  30 41 90 	mov.b [w0+3],w2
 945 00042c  81 40 61 	and.b w2,w1,w1
 946 00042e  31 40 98 	mov.b w1,[w0+3]
1085:lib/lib_pic33e/usb/usb_device.c ****         pBDTEntryEP0OutNext->ADR = ConvertToPhysicalAddress(&SetupPkt);
 947              	.loc 1 1085 0
 948 000430  00 00 80 	mov _pBDTEntryEP0OutNext,w0
 949 000432  01 00 20 	mov #_SetupPkt,w1
 950 000434  C0 41 90 	mov.b [w0+4],w3
 951 000436  F2 0F 20 	mov #255,w2
 952 000438  E0 C1 61 	and.b w3,#0,w3
 953 00043a  02 81 60 	and w1,w2,w2
 954 00043c  C8 08 DE 	lsr w1,#8,w1
 955 00043e  02 C1 71 	ior.b w3,w2,w2
 956 000440  42 40 98 	mov.b w2,[w0+4]
 957 000442  50 41 90 	mov.b [w0+5],w2
 958 000444  60 41 61 	and.b w2,#0,w2
 959 000446  81 40 71 	ior.b w2,w1,w1
 960 000448  51 40 98 	mov.b w1,[w0+5]
1086:lib/lib_pic33e/usb/usb_device.c ****         pBDTEntryEP0OutNext->STAT.Val = _DAT0|(_DTSEN & _DTS_CHECKING_ENABLED)|_BSTALL;
 961              	.loc 1 1086 0
 962 00044a  00 00 80 	mov _pBDTEntryEP0OutNext,w0
1087:lib/lib_pic33e/usb/usb_device.c ****         pBDTEntryEP0OutNext->STAT.Val |= _USIE;
 963              	.loc 1 1087 0
 964 00044c  F3 0F 20 	mov #255,w3
 965              	.loc 1 1086 0
 966 00044e  90 40 78 	mov.b [w0],w1
 967 000450  E0 C0 60 	and.b w1,#0,w1
 968 000452  C1 40 B3 	ior.b #12,w1
 969 000454  01 48 78 	mov.b w1,[w0]
1088:lib/lib_pic33e/usb/usb_device.c ****         pBDTEntryIn[0]->STAT.Val = _BSTALL; 
1089:lib/lib_pic33e/usb/usb_device.c ****         pBDTEntryIn[0]->STAT.Val |= _USIE;
 970              	.loc 1 1089 0
 971 000456  F2 0F 20 	mov #255,w2
 972              	.loc 1 1086 0
 973 000458  90 40 90 	mov.b [w0+1],w1
 974 00045a  E0 C0 60 	and.b w1,#0,w1
 975 00045c  11 40 98 	mov.b w1,[w0+1]
 976              	.loc 1 1087 0
 977 00045e  00 00 80 	mov _pBDTEntryEP0OutNext,w0
 978 000460  01 00 80 	mov _pBDTEntryEP0OutNext,w1
 979 000462  91 82 FB 	ze [w1],w5
 980 000464  00 00 00 	nop 
 981 000466  91 40 90 	mov.b [w1+1],w1
 982 000468  10 42 78 	mov.b [w0],w4
 983 00046a  81 80 FB 	ze w1,w1
 984 00046c  60 42 62 	and.b w4,#0,w4
 985 00046e  C8 08 DD 	sl w1,#8,w1
 986 000470  81 80 72 	ior w5,w1,w1
 987 000472  01 70 A0 	bset w1,#7
 988 000474  83 81 60 	and w1,w3,w3
 989 000476  83 41 72 	ior.b w4,w3,w3
 990 000478  03 48 78 	mov.b w3,[w0]
 991 00047a  C8 08 DE 	lsr w1,#8,w1
 992 00047c  90 41 90 	mov.b [w0+1],w3
 993 00047e  E0 C1 61 	and.b w3,#0,w3
 994 000480  81 C0 71 	ior.b w3,w1,w1
MPLAB XC16 ASSEMBLY Listing:   			page 38


 995 000482  11 40 98 	mov.b w1,[w0+1]
 996              	.loc 1 1088 0
 997 000484  00 00 80 	mov _pBDTEntryIn,w0
 998 000486  90 40 78 	mov.b [w0],w1
 999 000488  E0 C0 60 	and.b w1,#0,w1
 1000 00048a  01 24 A0 	bset.b w1,#2
 1001 00048c  01 48 78 	mov.b w1,[w0]
 1002 00048e  90 40 90 	mov.b [w0+1],w1
 1003 000490  E0 C0 60 	and.b w1,#0,w1
 1004 000492  11 40 98 	mov.b w1,[w0+1]
 1005              	.loc 1 1089 0
 1006 000494  00 00 80 	mov _pBDTEntryIn,w0
 1007 000496  01 00 80 	mov _pBDTEntryIn,w1
 1008 000498  11 82 FB 	ze [w1],w4
 1009 00049a  00 00 00 	nop 
 1010 00049c  91 40 90 	mov.b [w1+1],w1
 1011 00049e  90 41 78 	mov.b [w0],w3
 1012 0004a0  81 80 FB 	ze w1,w1
 1013 0004a2  E0 C1 61 	and.b w3,#0,w3
 1014 0004a4  C8 08 DD 	sl w1,#8,w1
 1015 0004a6  81 00 72 	ior w4,w1,w1
 1016 0004a8  01 70 A0 	bset w1,#7
 1017 0004aa  02 81 60 	and w1,w2,w2
 1018 0004ac  C8 08 DE 	lsr w1,#8,w1
 1019 0004ae  02 C1 71 	ior.b w3,w2,w2
 1020 0004b0  02 48 78 	mov.b w2,[w0]
 1021 0004b2  10 41 90 	mov.b [w0+1],w2
 1022 0004b4  60 41 61 	and.b w2,#0,w2
 1023 0004b6  81 40 71 	ior.b w2,w1,w1
 1024 0004b8  11 40 98 	mov.b w1,[w0+1]
 1025 0004ba  00 00 37 	bra .L40
 1026              	.L41:
1090:lib/lib_pic33e/usb/usb_device.c ****                
1091:lib/lib_pic33e/usb/usb_device.c ****     }
1092:lib/lib_pic33e/usb/usb_device.c ****     else
1093:lib/lib_pic33e/usb/usb_device.c ****     {
1094:lib/lib_pic33e/usb/usb_device.c ****         p = (BDT_ENTRY*)(&BDT[EP(ep,dir,0)]);
 1027              	.loc 1 1094 0
 1028 0004bc  2E 40 90 	mov.b [w14+2],w0
 1029 0004be  BE 40 90 	mov.b [w14+3],w1
 1030 0004c0  00 80 FB 	ze w0,w0
 1031 0004c2  81 80 FB 	ze w1,w1
 1032 0004c4  00 01 40 	add w0,w0,w2
 1033 0004c6  00 00 20 	mov #_BDT,w0
 1034 0004c8  81 00 41 	add w2,w1,w1
 1035 0004ca  C4 08 DD 	sl w1,#4,w1
 1036 0004cc  00 8F 40 	add w1,w0,[w14]
1095:lib/lib_pic33e/usb/usb_device.c ****         p->STAT.Val |= _BSTALL;
 1037              	.loc 1 1095 0
 1038 0004ce  F2 0F 20 	mov #255,w2
 1039 0004d0  9E 00 78 	mov [w14],w1
 1040 0004d2  1E 00 78 	mov [w14],w0
 1041 0004d4  91 82 FB 	ze [w1],w5
 1042 0004d6  91 40 90 	mov.b [w1+1],w1
 1043 0004d8  10 42 78 	mov.b [w0],w4
 1044 0004da  90 41 90 	mov.b [w0+1],w3
 1045 0004dc  81 80 FB 	ze w1,w1
MPLAB XC16 ASSEMBLY Listing:   			page 39


 1046 0004de  60 42 62 	and.b w4,#0,w4
 1047 0004e0  C8 08 DD 	sl w1,#8,w1
 1048 0004e2  E0 C1 61 	and.b w3,#0,w3
 1049 0004e4  81 80 72 	ior w5,w1,w1
 1050 0004e6  01 20 A0 	bset w1,#2
 1051 0004e8  02 81 60 	and w1,w2,w2
 1052 0004ea  C8 08 DE 	lsr w1,#8,w1
 1053 0004ec  02 41 72 	ior.b w4,w2,w2
 1054 0004ee  81 C0 71 	ior.b w3,w1,w1
 1055 0004f0  02 48 78 	mov.b w2,[w0]
 1056 0004f2  11 40 98 	mov.b w1,[w0+1]
1096:lib/lib_pic33e/usb/usb_device.c ****         p->STAT.Val |= _USIE;
 1057              	.loc 1 1096 0
 1058 0004f4  9E 00 78 	mov [w14],w1
 1059 0004f6  1E 00 78 	mov [w14],w0
 1060 0004f8  11 82 FB 	ze [w1],w4
 1061 0004fa  91 40 90 	mov.b [w1+1],w1
 1062 0004fc  90 42 78 	mov.b [w0],w5
 1063 0004fe  10 41 90 	mov.b [w0+1],w2
 1064 000500  81 80 FB 	ze w1,w1
 1065 000502  F3 0F 20 	mov #255,w3
 1066 000504  C8 08 DD 	sl w1,#8,w1
 1067 000506  E0 C2 62 	and.b w5,#0,w5
 1068 000508  81 00 72 	ior w4,w1,w1
 1069 00050a  60 42 61 	and.b w2,#0,w4
1097:lib/lib_pic33e/usb/usb_device.c ****     
1098:lib/lib_pic33e/usb/usb_device.c ****         //If the device is in FULL or ALL_BUT_EP0 ping pong modes
1099:lib/lib_pic33e/usb/usb_device.c ****         //then stall that entry as well
1100:lib/lib_pic33e/usb/usb_device.c ****         #if (USB_PING_PONG_MODE == USB_PING_PONG__FULL_PING_PONG) || (USB_PING_PONG_MODE == USB_PIN
1101:lib/lib_pic33e/usb/usb_device.c ****         p = (BDT_ENTRY*)(&BDT[EP(ep,dir,1)]);
 1070              	.loc 1 1101 0
 1071 00050c  02 00 20 	mov #_BDT,w2
 1072              	.loc 1 1096 0
 1073 00050e  01 70 A0 	bset w1,#7
 1074 000510  83 81 60 	and w1,w3,w3
 1075 000512  C8 08 DE 	lsr w1,#8,w1
 1076 000514  83 C1 72 	ior.b w5,w3,w3
 1077 000516  81 40 72 	ior.b w4,w1,w1
 1078 000518  03 48 78 	mov.b w3,[w0]
 1079 00051a  11 40 98 	mov.b w1,[w0+1]
 1080              	.loc 1 1101 0
 1081 00051c  2E 40 90 	mov.b [w14+2],w0
 1082 00051e  BE 40 90 	mov.b [w14+3],w1
 1083 000520  00 80 FB 	ze w0,w0
 1084 000522  81 80 FB 	ze w1,w1
 1085 000524  00 00 40 	add w0,w0,w0
 1086 000526  01 00 40 	add w0,w1,w0
 1087 000528  44 00 DD 	sl w0,#4,w0
 1088 00052a  68 00 40 	add w0,#8,w0
 1089 00052c  02 0F 40 	add w0,w2,[w14]
1102:lib/lib_pic33e/usb/usb_device.c ****         p->STAT.Val |= _BSTALL;
 1090              	.loc 1 1102 0
 1091 00052e  F2 0F 20 	mov #255,w2
 1092 000530  9E 00 78 	mov [w14],w1
 1093 000532  1E 00 78 	mov [w14],w0
 1094 000534  91 82 FB 	ze [w1],w5
 1095 000536  91 40 90 	mov.b [w1+1],w1
MPLAB XC16 ASSEMBLY Listing:   			page 40


 1096 000538  10 42 78 	mov.b [w0],w4
 1097 00053a  90 41 90 	mov.b [w0+1],w3
 1098 00053c  81 80 FB 	ze w1,w1
 1099 00053e  60 42 62 	and.b w4,#0,w4
 1100 000540  C8 08 DD 	sl w1,#8,w1
 1101 000542  E0 C1 61 	and.b w3,#0,w3
 1102 000544  81 80 72 	ior w5,w1,w1
 1103 000546  01 20 A0 	bset w1,#2
 1104 000548  02 81 60 	and w1,w2,w2
 1105 00054a  C8 08 DE 	lsr w1,#8,w1
 1106 00054c  02 41 72 	ior.b w4,w2,w2
 1107 00054e  81 C0 71 	ior.b w3,w1,w1
 1108 000550  02 48 78 	mov.b w2,[w0]
 1109 000552  11 40 98 	mov.b w1,[w0+1]
1103:lib/lib_pic33e/usb/usb_device.c ****         p->STAT.Val |= _USIE;
 1110              	.loc 1 1103 0
 1111 000554  1E 00 78 	mov [w14],w0
 1112 000556  9E 00 78 	mov [w14],w1
 1113 000558  90 41 78 	mov.b [w0],w3
 1114 00055a  11 82 FB 	ze [w1],w4
 1115 00055c  91 40 90 	mov.b [w1+1],w1
 1116 00055e  F2 0F 20 	mov #255,w2
 1117 000560  81 80 FB 	ze w1,w1
 1118 000562  E0 C1 61 	and.b w3,#0,w3
 1119 000564  C8 08 DD 	sl w1,#8,w1
 1120 000566  81 00 72 	ior w4,w1,w1
 1121 000568  01 70 A0 	bset w1,#7
 1122 00056a  02 81 60 	and w1,w2,w2
 1123 00056c  C8 08 DE 	lsr w1,#8,w1
 1124 00056e  02 C1 71 	ior.b w3,w2,w2
 1125 000570  02 48 78 	mov.b w2,[w0]
 1126 000572  10 41 90 	mov.b [w0+1],w2
 1127 000574  60 41 61 	and.b w2,#0,w2
 1128 000576  81 40 71 	ior.b w2,w1,w1
 1129 000578  11 40 98 	mov.b w1,[w0+1]
 1130              	.L40:
1104:lib/lib_pic33e/usb/usb_device.c ****         #endif
1105:lib/lib_pic33e/usb/usb_device.c ****     }
1106:lib/lib_pic33e/usb/usb_device.c **** }
 1131              	.loc 1 1106 0
 1132 00057a  8E 07 78 	mov w14,w15
 1133 00057c  4F 07 78 	mov [--w15],w14
 1134 00057e  00 40 A9 	bclr CORCON,#2
 1135 000580  00 00 06 	return 
 1136              	.set ___PA___,0
 1137              	.LFE4:
 1138              	.size _USBStallEndpoint,.-_USBStallEndpoint
 1139              	.align 2
 1140              	.global _USBCancelIO
 1141              	.type _USBCancelIO,@function
 1142              	_USBCancelIO:
 1143              	.LFB5:
1107:lib/lib_pic33e/usb/usb_device.c **** 
1108:lib/lib_pic33e/usb/usb_device.c **** /**************************************************************************
1109:lib/lib_pic33e/usb/usb_device.c ****     Function:
1110:lib/lib_pic33e/usb/usb_device.c ****         void USBCancelIO(uint8_t endpoint)
1111:lib/lib_pic33e/usb/usb_device.c ****     
MPLAB XC16 ASSEMBLY Listing:   			page 41


1112:lib/lib_pic33e/usb/usb_device.c ****     Description:
1113:lib/lib_pic33e/usb/usb_device.c ****         This function cancels the transfers pending on the specified endpoint.
1114:lib/lib_pic33e/usb/usb_device.c ****         This function can only be used after a SETUP packet is received and 
1115:lib/lib_pic33e/usb/usb_device.c ****         before that setup packet is handled.  This is the time period in which
1116:lib/lib_pic33e/usb/usb_device.c ****         the EVENT_EP0_REQUEST is thrown, before the event handler function
1117:lib/lib_pic33e/usb/usb_device.c ****         returns to the stack.
1118:lib/lib_pic33e/usb/usb_device.c **** 
1119:lib/lib_pic33e/usb/usb_device.c ****     Precondition:
1120:lib/lib_pic33e/usb/usb_device.c ****   
1121:lib/lib_pic33e/usb/usb_device.c ****     Parameters:
1122:lib/lib_pic33e/usb/usb_device.c ****         uint8_t endpoint - the endpoint number you wish to cancel the transfers for
1123:lib/lib_pic33e/usb/usb_device.c ****      
1124:lib/lib_pic33e/usb/usb_device.c ****     Return Values:
1125:lib/lib_pic33e/usb/usb_device.c ****         None
1126:lib/lib_pic33e/usb/usb_device.c ****         
1127:lib/lib_pic33e/usb/usb_device.c ****     Remarks:
1128:lib/lib_pic33e/usb/usb_device.c ****         None
1129:lib/lib_pic33e/usb/usb_device.c ****                                                           
1130:lib/lib_pic33e/usb/usb_device.c ****   **************************************************************************/
1131:lib/lib_pic33e/usb/usb_device.c **** void USBCancelIO(uint8_t endpoint)
1132:lib/lib_pic33e/usb/usb_device.c **** {
 1144              	.loc 1 1132 0
 1145              	.set ___PA___,1
 1146 000582  02 00 FA 	lnk #2
 1147              	.LCFI6:
 1148 000584  88 9F BE 	mov.d w8,[w15++]
 1149              	.LCFI7:
 1150 000586  8A 1F 78 	mov w10,[w15++]
 1151              	.LCFI8:
 1152              	.loc 1 1132 0
 1153 000588  00 4F 78 	mov.b w0,[w14]
1133:lib/lib_pic33e/usb/usb_device.c ****     if(USBPacketDisable == 1)
 1154              	.loc 1 1133 0
 1155 00058a  00 02 20 	mov #32,w0
 1156 00058c  01 00 80 	mov _U1CONbits,w1
 1157 00058e  00 80 60 	and w1,w0,w0
 1158 000590  00 00 E0 	cp0 w0
 1159              	.set ___BP___,0
 1160 000592  00 00 32 	bra z,.L43
1134:lib/lib_pic33e/usb/usb_device.c ****     {
1135:lib/lib_pic33e/usb/usb_device.c ****     	//The PKTDIS bit is currently set right now.  It is therefore "safe"
1136:lib/lib_pic33e/usb/usb_device.c ****     	//to mess with the BDT right now.
1137:lib/lib_pic33e/usb/usb_device.c ****     	pBDTEntryIn[endpoint]->Val &= _DTSMASK;	//Makes UOWN = 0 (_UCPU mode).  Deactivates endpoint. 
 1161              	.loc 1 1137 0
 1162 000594  9E 80 FB 	ze [w14],w1
 1163 000596  1E 80 FB 	ze [w14],w0
 1164 000598  81 81 40 	add w1,w1,w3
 1165 00059a  01 00 20 	mov #_pBDTEntryIn,w1
 1166 00059c  00 01 40 	add w0,w0,w2
 1167 00059e  81 80 41 	add w3,w1,w1
 1168 0005a0  00 00 20 	mov #_pBDTEntryIn,w0
 1169 0005a2  00 00 41 	add w2,w0,w0
 1170 0005a4  11 01 78 	mov [w1],w2
 1171 0005a6  90 01 78 	mov [w0],w3
 1172 0005a8  04 04 20 	mov #64,w4
 1173 0005aa  05 00 20 	mov #0,w5
 1174 0005ac  93 84 FB 	ze [w3],w9
MPLAB XC16 ASSEMBLY Listing:   			page 42


 1175 0005ae  13 43 90 	mov.b [w3+1],w6
 1176 0005b0  84 03 78 	mov w4,w7
 1177 0005b2  23 44 90 	mov.b [w3+2],w8
 1178 0005b4  06 83 FB 	ze w6,w6
 1179 0005b6  B3 41 90 	mov.b [w3+3],w3
 1180 0005b8  48 35 DD 	sl w6,#8,w10
 1181 0005ba  12 43 78 	mov.b [w2],w6
 1182 0005bc  8A 84 74 	ior w9,w10,w9
 1183 0005be  08 84 FB 	ze w8,w8
 1184 0005c0  09 00 78 	mov w9,w0
 1185 0005c2  83 81 FB 	ze w3,w3
 1186 0005c4  05 02 78 	mov w5,w4
 1187 0005c6  C8 1A DD 	sl w3,#8,w5
 1188 0005c8  F3 0F 20 	mov #255,w3
 1189 0005ca  85 02 74 	ior w8,w5,w5
 1190 0005cc  60 43 63 	and.b w6,#0,w6
 1191 0005ce  85 00 78 	mov w5,w1
 1192 0005d0  80 02 78 	mov w0,w5
 1193 0005d2  01 00 78 	mov w1,w0
 1194 0005d4  87 83 62 	and w5,w7,w7
 1195 0005d6  04 00 60 	and w0,w4,w0
 1196 0005d8  61 02 B8 	mul.uu w0,#1,w4
 1197 0005da  C0 22 DD 	sl w4,#0,w5
 1198 0005dc  04 00 20 	mov #0,w4
 1199 0005de  04 00 BE 	mov.d w4,w0
 1200 0005e0  61 3A B8 	mul.uu w7,#1,w4
 1201 0005e2  04 00 70 	ior w0,w4,w0
 1202 0005e4  85 80 70 	ior w1,w5,w1
 1203 0005e6  83 01 60 	and w0,w3,w3
 1204 0005e8  83 41 73 	ior.b w6,w3,w3
 1205 0005ea  03 49 78 	mov.b w3,[w2]
 1206 0005ec  C8 01 DE 	lsr w0,#8,w3
 1207 0005ee  12 42 90 	mov.b [w2+1],w4
 1208 0005f0  F5 0F 20 	mov #255,w5
 1209 0005f2  60 42 62 	and.b w4,#0,w4
 1210 0005f4  85 82 60 	and w1,w5,w5
 1211 0005f6  83 41 72 	ior.b w4,w3,w3
 1212 0005f8  48 08 DE 	lsr w1,#8,w0
 1213 0005fa  13 41 98 	mov.b w3,[w2+1]
1138:lib/lib_pic33e/usb/usb_device.c ****     	pBDTEntryIn[endpoint]->Val ^= _DTSMASK;	//Toggle the DTS bit.  This packet didn't get sent yet
 1214              	.loc 1 1138 0
 1215 0005fc  04 00 20 	mov #_pBDTEntryIn,w4
 1216              	.loc 1 1137 0
 1217 0005fe  A2 40 90 	mov.b [w2+2],w1
 1218              	.loc 1 1138 0
 1219 000600  03 00 20 	mov #_pBDTEntryIn,w3
 1220              	.loc 1 1137 0
 1221 000602  E0 C0 60 	and.b w1,#0,w1
 1222 000604  85 C0 70 	ior.b w1,w5,w1
 1223 000606  21 41 98 	mov.b w1,[w2+2]
 1224 000608  B2 40 90 	mov.b [w2+3],w1
 1225 00060a  E0 C0 60 	and.b w1,#0,w1
 1226 00060c  00 C0 70 	ior.b w1,w0,w0
 1227 00060e  30 41 98 	mov.b w0,[w2+3]
 1228              	.loc 1 1138 0
 1229 000610  9E 80 FB 	ze [w14],w1
 1230 000612  1E 80 FB 	ze [w14],w0
MPLAB XC16 ASSEMBLY Listing:   			page 43


 1231 000614  01 81 40 	add w1,w1,w2
 1232 000616  80 00 40 	add w0,w0,w1
 1233 000618  04 00 41 	add w2,w4,w0
 1234 00061a  83 80 40 	add w1,w3,w1
 1235 00061c  10 00 78 	mov [w0],w0
 1236 00061e  00 00 00 	nop 
 1237 000620  91 00 78 	mov [w1],w1
 1238 000622  04 04 20 	mov #64,w4
 1239 000624  05 00 20 	mov #0,w5
 1240 000626  91 84 FB 	ze [w1],w9
 1241 000628  F7 0F 20 	mov #255,w7
 1242 00062a  11 43 90 	mov.b [w1+1],w6
 1243 00062c  21 45 90 	mov.b [w1+2],w10
 1244 00062e  06 83 FB 	ze w6,w6
 1245 000630  31 44 90 	mov.b [w1+3],w8
 1246 000632  C8 30 DD 	sl w6,#8,w1
 1247 000634  10 43 78 	mov.b [w0],w6
 1248 000636  81 84 74 	ior w9,w1,w9
 1249 000638  8A 80 FB 	ze w10,w1
 1250 00063a  09 01 78 	mov w9,w2
 1251 00063c  08 84 FB 	ze w8,w8
 1252 00063e  60 43 63 	and.b w6,#0,w6
 1253 000640  48 44 DD 	sl w8,#8,w8
 1254 000642  88 80 70 	ior w1,w8,w1
 1255 000644  81 01 78 	mov w1,w3
 1256 000646  04 01 69 	xor w2,w4,w2
 1257 000648  85 81 69 	xor w3,w5,w3
 1258 00064a  87 00 61 	and w2,w7,w1
 1259 00064c  81 40 73 	ior.b w6,w1,w1
 1260 00064e  01 48 78 	mov.b w1,[w0]
 1261 000650  C8 10 DE 	lsr w2,#8,w1
 1262 000652  90 42 90 	mov.b [w0+1],w5
 1263 000654  F4 0F 20 	mov #255,w4
 1264 000656  E0 C2 62 	and.b w5,#0,w5
 1265 000658  04 82 61 	and w3,w4,w4
 1266 00065a  81 C0 72 	ior.b w5,w1,w1
 1267 00065c  48 19 DE 	lsr w3,#8,w2
 1268 00065e  11 40 98 	mov.b w1,[w0+1]
1139:lib/lib_pic33e/usb/usb_device.c ****     	
1140:lib/lib_pic33e/usb/usb_device.c ****     	//Need to do additional handling if ping-pong buffering is being used
1141:lib/lib_pic33e/usb/usb_device.c ****         #if ((USB_PING_PONG_MODE == USB_PING_PONG__FULL_PING_PONG) || (USB_PING_PONG_MODE == USB_PI
1142:lib/lib_pic33e/usb/usb_device.c ****         //Point to the next buffer for ping pong purposes.  UOWN getting cleared
1143:lib/lib_pic33e/usb/usb_device.c ****         //(either due to SIE clearing it after a transaction, or the firmware
1144:lib/lib_pic33e/usb/usb_device.c ****         //clearing it) makes hardware ping pong pointer advance.
1145:lib/lib_pic33e/usb/usb_device.c ****         pBDTEntryIn[endpoint] = (BDT_ENTRY*)(((uintptr_t)pBDTEntryIn[endpoint]) ^ USB_NEXT_PING_PON
 1269              	.loc 1 1145 0
 1270 000660  01 00 20 	mov #_pBDTEntryIn,w1
 1271              	.loc 1 1138 0
 1272 000662  A0 41 90 	mov.b [w0+2],w3
 1273 000664  E0 C1 61 	and.b w3,#0,w3
 1274 000666  84 C1 71 	ior.b w3,w4,w3
 1275 000668  23 40 98 	mov.b w3,[w0+2]
 1276 00066a  B0 41 90 	mov.b [w0+3],w3
 1277 00066c  E0 C1 61 	and.b w3,#0,w3
 1278 00066e  02 C1 71 	ior.b w3,w2,w2
 1279 000670  32 40 98 	mov.b w2,[w0+3]
 1280              	.loc 1 1145 0
MPLAB XC16 ASSEMBLY Listing:   			page 44


 1281 000672  1E 80 FB 	ze [w14],w0
 1282 000674  00 00 40 	add w0,w0,w0
 1283 000676  81 00 40 	add w0,w1,w1
 1284 000678  1E 80 FB 	ze [w14],w0
 1285 00067a  00 00 00 	nop 
 1286 00067c  91 00 78 	mov [w1],w1
 1287 00067e  00 01 40 	add w0,w0,w2
 1288 000680  00 00 20 	mov #_pBDTEntryIn,w0
 1289 000682  01 30 A2 	btg w1,#3
 1290 000684  00 00 41 	add w2,w0,w0
1146:lib/lib_pic33e/usb/usb_device.c ****         
1147:lib/lib_pic33e/usb/usb_device.c ****     	pBDTEntryIn[endpoint]->STAT.Val &= _DTSMASK;
 1291              	.loc 1 1147 0
 1292 000686  03 00 20 	mov #_pBDTEntryIn,w3
 1293              	.loc 1 1145 0
 1294 000688  01 08 78 	mov w1,[w0]
 1295              	.loc 1 1147 0
 1296 00068a  02 00 20 	mov #_pBDTEntryIn,w2
 1297 00068c  9E 80 FB 	ze [w14],w1
 1298 00068e  1E 80 FB 	ze [w14],w0
 1299 000690  01 82 40 	add w1,w1,w4
 1300 000692  80 00 40 	add w0,w0,w1
 1301 000694  03 00 42 	add w4,w3,w0
 1302 000696  82 80 40 	add w1,w2,w1
 1303 000698  10 00 78 	mov [w0],w0
 1304 00069a  00 00 00 	nop 
 1305 00069c  91 00 78 	mov [w1],w1
 1306 00069e  04 04 20 	mov #64,w4
 1307 0006a0  91 82 FB 	ze [w1],w5
 1308 0006a2  F2 0F 20 	mov #255,w2
 1309 0006a4  91 40 90 	mov.b [w1+1],w1
 1310 0006a6  90 41 78 	mov.b [w0],w3
 1311 0006a8  81 80 FB 	ze w1,w1
 1312 0006aa  E0 C1 61 	and.b w3,#0,w3
 1313 0006ac  C8 08 DD 	sl w1,#8,w1
 1314 0006ae  81 80 72 	ior w5,w1,w1
 1315 0006b0  84 80 60 	and w1,w4,w1
 1316 0006b2  02 81 60 	and w1,w2,w2
 1317 0006b4  02 C1 71 	ior.b w3,w2,w2
 1318 0006b6  02 48 78 	mov.b w2,[w0]
 1319 0006b8  C8 08 DE 	lsr w1,#8,w1
 1320 0006ba  10 41 90 	mov.b [w0+1],w2
1148:lib/lib_pic33e/usb/usb_device.c ****     	pBDTEntryIn[endpoint]->STAT.Val ^= _DTSMASK;
 1321              	.loc 1 1148 0
 1322 0006bc  03 00 20 	mov #_pBDTEntryIn,w3
 1323              	.loc 1 1147 0
 1324 0006be  60 42 61 	and.b w2,#0,w4
 1325              	.loc 1 1148 0
 1326 0006c0  02 00 20 	mov #_pBDTEntryIn,w2
 1327              	.loc 1 1147 0
 1328 0006c2  81 40 72 	ior.b w4,w1,w1
 1329 0006c4  11 40 98 	mov.b w1,[w0+1]
 1330              	.loc 1 1148 0
 1331 0006c6  9E 80 FB 	ze [w14],w1
 1332 0006c8  1E 80 FB 	ze [w14],w0
 1333 0006ca  01 82 40 	add w1,w1,w4
 1334 0006cc  80 00 40 	add w0,w0,w1
MPLAB XC16 ASSEMBLY Listing:   			page 45


 1335 0006ce  03 00 42 	add w4,w3,w0
 1336 0006d0  82 80 40 	add w1,w2,w1
 1337 0006d2  10 00 78 	mov [w0],w0
 1338 0006d4  00 00 00 	nop 
 1339 0006d6  91 00 78 	mov [w1],w1
 1340 0006d8  04 04 20 	mov #64,w4
 1341 0006da  91 82 FB 	ze [w1],w5
 1342 0006dc  F2 0F 20 	mov #255,w2
 1343 0006de  91 40 90 	mov.b [w1+1],w1
 1344 0006e0  90 41 78 	mov.b [w0],w3
 1345 0006e2  81 80 FB 	ze w1,w1
 1346 0006e4  E0 C1 61 	and.b w3,#0,w3
 1347 0006e6  C8 08 DD 	sl w1,#8,w1
 1348 0006e8  81 80 72 	ior w5,w1,w1
 1349 0006ea  84 80 68 	xor w1,w4,w1
 1350 0006ec  02 81 60 	and w1,w2,w2
 1351 0006ee  02 C1 71 	ior.b w3,w2,w2
 1352 0006f0  02 48 78 	mov.b w2,[w0]
 1353 0006f2  C8 08 DE 	lsr w1,#8,w1
 1354 0006f4  10 41 90 	mov.b [w0+1],w2
 1355 0006f6  60 41 61 	and.b w2,#0,w2
 1356 0006f8  81 40 71 	ior.b w2,w1,w1
 1357 0006fa  11 40 98 	mov.b w1,[w0+1]
 1358              	.L43:
1149:lib/lib_pic33e/usb/usb_device.c ****         #endif
1150:lib/lib_pic33e/usb/usb_device.c ****     }
1151:lib/lib_pic33e/usb/usb_device.c **** }
 1359              	.loc 1 1151 0
 1360 0006fc  4F 05 78 	mov [--w15],w10
 1361 0006fe  4F 04 BE 	mov.d [--w15],w8
 1362 000700  8E 07 78 	mov w14,w15
 1363 000702  4F 07 78 	mov [--w15],w14
 1364 000704  00 40 A9 	bclr CORCON,#2
 1365 000706  00 00 06 	return 
 1366              	.set ___PA___,0
 1367              	.LFE5:
 1368              	.size _USBCancelIO,.-_USBCancelIO
 1369              	.align 2
 1370              	.global _USBDeviceDetach
 1371              	.type _USBDeviceDetach,@function
 1372              	_USBDeviceDetach:
 1373              	.LFB6:
1152:lib/lib_pic33e/usb/usb_device.c **** 
1153:lib/lib_pic33e/usb/usb_device.c **** /**************************************************************************
1154:lib/lib_pic33e/usb/usb_device.c ****     Function:
1155:lib/lib_pic33e/usb/usb_device.c ****         void USBDeviceDetach(void)
1156:lib/lib_pic33e/usb/usb_device.c ****    
1157:lib/lib_pic33e/usb/usb_device.c ****     Summary:
1158:lib/lib_pic33e/usb/usb_device.c ****         This function configures the USB module to "soft detach" itself from
1159:lib/lib_pic33e/usb/usb_device.c ****         the USB host.
1160:lib/lib_pic33e/usb/usb_device.c ****         
1161:lib/lib_pic33e/usb/usb_device.c ****     Description:
1162:lib/lib_pic33e/usb/usb_device.c ****         This function configures the USB module to perform a "soft detach"
1163:lib/lib_pic33e/usb/usb_device.c ****         operation, by disabling the D+ (or D-) ~1.5k pull up resistor, which
1164:lib/lib_pic33e/usb/usb_device.c ****         lets the host know the device is present and attached.  This will make
1165:lib/lib_pic33e/usb/usb_device.c ****         the host think that the device has been unplugged.  This is potentially
1166:lib/lib_pic33e/usb/usb_device.c ****         useful, as it allows the USB device to force the host to re-enumerate
MPLAB XC16 ASSEMBLY Listing:   			page 46


1167:lib/lib_pic33e/usb/usb_device.c ****         the device (on the firmware has re-enabled the USB module/pull up, by
1168:lib/lib_pic33e/usb/usb_device.c ****         calling USBDeviceAttach(), to "soft re-attach" to the host).
1169:lib/lib_pic33e/usb/usb_device.c ****         
1170:lib/lib_pic33e/usb/usb_device.c ****     Precondition:
1171:lib/lib_pic33e/usb/usb_device.c ****         Should only be called when USB_INTERRUPT is defined.  See remarks
1172:lib/lib_pic33e/usb/usb_device.c ****         section if USB_POLLING mode option is being used (usb_config.h option).
1173:lib/lib_pic33e/usb/usb_device.c **** 
1174:lib/lib_pic33e/usb/usb_device.c ****         Additionally, this function should only be called from the main() loop 
1175:lib/lib_pic33e/usb/usb_device.c ****         context.  Do not call this function from within an interrupt handler, as 
1176:lib/lib_pic33e/usb/usb_device.c ****         this function may modify global interrupt enable bits and settings.
1177:lib/lib_pic33e/usb/usb_device.c ****         
1178:lib/lib_pic33e/usb/usb_device.c ****     Parameters:
1179:lib/lib_pic33e/usb/usb_device.c ****         None
1180:lib/lib_pic33e/usb/usb_device.c ****      
1181:lib/lib_pic33e/usb/usb_device.c ****     Return Values:
1182:lib/lib_pic33e/usb/usb_device.c ****         None
1183:lib/lib_pic33e/usb/usb_device.c ****         
1184:lib/lib_pic33e/usb/usb_device.c ****     Remarks:
1185:lib/lib_pic33e/usb/usb_device.c ****         If the application firmware calls USBDeviceDetach(), it is strongly
1186:lib/lib_pic33e/usb/usb_device.c ****         recommended that the firmware wait at least >= 80ms before calling
1187:lib/lib_pic33e/usb/usb_device.c ****         USBDeviceAttach().  If the firmware performs a soft detach, and then
1188:lib/lib_pic33e/usb/usb_device.c ****         re-attaches too soon (ex: after a few micro seconds for instance), some
1189:lib/lib_pic33e/usb/usb_device.c ****         hosts may interpret this as an unexpected "glitch" rather than as a
1190:lib/lib_pic33e/usb/usb_device.c ****         physical removal/re-attachment of the USB device.  In this case the host
1191:lib/lib_pic33e/usb/usb_device.c ****         may simply ignore the event without re-enumerating the device.  To 
1192:lib/lib_pic33e/usb/usb_device.c ****         ensure that the host properly detects and processes the device soft
1193:lib/lib_pic33e/usb/usb_device.c ****         detach/re-attach, it is recommended to make sure the device remains 
1194:lib/lib_pic33e/usb/usb_device.c ****         detached long enough to mimic a real human controlled USB 
1195:lib/lib_pic33e/usb/usb_device.c ****         unplug/re-attach event (ex: after calling USBDeviceDetach(), do not
1196:lib/lib_pic33e/usb/usb_device.c ****         call USBDeviceAttach() for at least 80+ms, preferably longer.
1197:lib/lib_pic33e/usb/usb_device.c ****         
1198:lib/lib_pic33e/usb/usb_device.c ****         Neither the USBDeviceDetach() or USBDeviceAttach() functions are blocking
1199:lib/lib_pic33e/usb/usb_device.c ****         or take long to execute.  It is the application firmwares 
1200:lib/lib_pic33e/usb/usb_device.c ****         responsibility for adding the 80+ms delay, when using these API 
1201:lib/lib_pic33e/usb/usb_device.c ****         functions.
1202:lib/lib_pic33e/usb/usb_device.c ****         
1203:lib/lib_pic33e/usb/usb_device.c ****         Note: The Windows plug and play event handler processing is fairly 
1204:lib/lib_pic33e/usb/usb_device.c ****         slow, especially in certain versions of Windows, and for certain USB
1205:lib/lib_pic33e/usb/usb_device.c ****         device classes.  It has been observed that some device classes need to
1206:lib/lib_pic33e/usb/usb_device.c ****         provide even more USB detach dwell interval (before calling 
1207:lib/lib_pic33e/usb/usb_device.c ****         USBDeviceAttach()), in order to work correctly after re-enumeration.
1208:lib/lib_pic33e/usb/usb_device.c ****         If the USB device is a CDC class device, it is recommended to wait
1209:lib/lib_pic33e/usb/usb_device.c ****         at least 1.5 seconds or longer, before soft re-attaching to the host,
1210:lib/lib_pic33e/usb/usb_device.c ****         to provide the plug and play event handler enough time to finish 
1211:lib/lib_pic33e/usb/usb_device.c ****         processing the removal event, before the re-attach occurs.
1212:lib/lib_pic33e/usb/usb_device.c ****         
1213:lib/lib_pic33e/usb/usb_device.c ****         If the application is using the USB_POLLING mode option, then the 
1214:lib/lib_pic33e/usb/usb_device.c ****         USBDeviceDetach() and USBDeviceAttach() functions are not available.  
1215:lib/lib_pic33e/usb/usb_device.c ****         In this mode, the USB stack relies on the "#define USE_USB_BUS_SENSE_IO" 
1216:lib/lib_pic33e/usb/usb_device.c ****         and "#define USB_BUS_SENSE" options in the 
1217:lib/lib_pic33e/usb/usb_device.c ****         HardwareProfile – [platform name].h file. 
1218:lib/lib_pic33e/usb/usb_device.c **** 
1219:lib/lib_pic33e/usb/usb_device.c ****         When using the USB_POLLING mode option, and the 
1220:lib/lib_pic33e/usb/usb_device.c ****         "#define USE_USB_BUS_SENSE_IO" definition has been commented out, then 
1221:lib/lib_pic33e/usb/usb_device.c ****         the USB stack assumes that it should always enable the USB module at 
1222:lib/lib_pic33e/usb/usb_device.c ****         pretty much all times.  Basically, anytime the application firmware 
1223:lib/lib_pic33e/usb/usb_device.c ****         calls USBDeviceTasks(), the firmware will automatically enable the USB 
MPLAB XC16 ASSEMBLY Listing:   			page 47


1224:lib/lib_pic33e/usb/usb_device.c ****         module.  This mode would typically be selected if the application was 
1225:lib/lib_pic33e/usb/usb_device.c ****         designed to be a purely bus powered device.  In this case, the 
1226:lib/lib_pic33e/usb/usb_device.c ****         application is powered from the +5V VBUS supply from the USB port, so 
1227:lib/lib_pic33e/usb/usb_device.c ****         it is correct and sensible in this type of application to power up and 
1228:lib/lib_pic33e/usb/usb_device.c ****         turn on the USB module, at anytime that the microcontroller is 
1229:lib/lib_pic33e/usb/usb_device.c ****         powered (which implies the USB cable is attached and the host is also 
1230:lib/lib_pic33e/usb/usb_device.c ****         powered).
1231:lib/lib_pic33e/usb/usb_device.c **** 
1232:lib/lib_pic33e/usb/usb_device.c ****         In a self powered application, the USB stack is designed with the 
1233:lib/lib_pic33e/usb/usb_device.c ****         intention that the user will enable the "#define USE_USB_BUS_SENSE_IO" 
1234:lib/lib_pic33e/usb/usb_device.c ****         option in the HardwareProfile – [platform name].h file.  When this 
1235:lib/lib_pic33e/usb/usb_device.c ****         option is defined, then the USBDeviceTasks() function will automatically 
1236:lib/lib_pic33e/usb/usb_device.c ****         check the I/O pin port value of the designated pin (based on the 
1237:lib/lib_pic33e/usb/usb_device.c ****         #define USB_BUS_SENSE option in the HardwareProfile – [platform name].h 
1238:lib/lib_pic33e/usb/usb_device.c ****         file), every time the application calls USBDeviceTasks().  If the 
1239:lib/lib_pic33e/usb/usb_device.c ****         USBDeviceTasks() function is executed and finds that the pin defined by 
1240:lib/lib_pic33e/usb/usb_device.c ****         the #define USB_BUS_SENSE is in a logic low state, then it will 
1241:lib/lib_pic33e/usb/usb_device.c ****         automatically disable the USB module and tri-state the D+ and D- pins.  
1242:lib/lib_pic33e/usb/usb_device.c ****         If however the USBDeviceTasks() function is executed and finds the pin 
1243:lib/lib_pic33e/usb/usb_device.c ****         defined by the #define USB_BUS_SENSE is in a logic high state, then it 
1244:lib/lib_pic33e/usb/usb_device.c ****         will automatically enable the USB module, if it has not already been 
1245:lib/lib_pic33e/usb/usb_device.c ****         enabled.        
1246:lib/lib_pic33e/usb/usb_device.c ****                                                           
1247:lib/lib_pic33e/usb/usb_device.c ****   **************************************************************************/
1248:lib/lib_pic33e/usb/usb_device.c **** #if defined(USB_INTERRUPT)
1249:lib/lib_pic33e/usb/usb_device.c **** void USBDeviceDetach(void)
1250:lib/lib_pic33e/usb/usb_device.c **** {
 1374              	.loc 1 1250 0
 1375              	.set ___PA___,1
 1376 000708  00 00 FA 	lnk #0
 1377              	.LCFI9:
1251:lib/lib_pic33e/usb/usb_device.c **** 
1252:lib/lib_pic33e/usb/usb_device.c ****     //If the interrupt option is selected then the customer is required
1253:lib/lib_pic33e/usb/usb_device.c ****     //  to notify the stack when the device is attached or removed from the
1254:lib/lib_pic33e/usb/usb_device.c ****     //  bus by calling the USBDeviceAttach() and USBDeviceDetach() functions.
1255:lib/lib_pic33e/usb/usb_device.c **** #ifdef USB_SUPPORT_OTG
1256:lib/lib_pic33e/usb/usb_device.c ****     if (USB_BUS_SENSE != 1)
1257:lib/lib_pic33e/usb/usb_device.c **** #endif
1258:lib/lib_pic33e/usb/usb_device.c ****     {
1259:lib/lib_pic33e/usb/usb_device.c ****          // Disable module & detach from bus
1260:lib/lib_pic33e/usb/usb_device.c ****          U1CON = 0;             
 1378              	.loc 1 1260 0
 1379 00070a  00 20 EF 	clr _U1CON
1261:lib/lib_pic33e/usb/usb_device.c **** 
1262:lib/lib_pic33e/usb/usb_device.c ****          // Mask all USB interrupts              
1263:lib/lib_pic33e/usb/usb_device.c ****          U1IE = 0;          
 1380              	.loc 1 1263 0
 1381 00070c  00 20 EF 	clr _U1IE
1264:lib/lib_pic33e/usb/usb_device.c **** 
1265:lib/lib_pic33e/usb/usb_device.c ****          //Move to the detached state                  
1266:lib/lib_pic33e/usb/usb_device.c ****          USBDeviceState = DETACHED_STATE;
 1382              	.loc 1 1266 0
 1383 00070e  00 20 EF 	clr _USBDeviceState
1267:lib/lib_pic33e/usb/usb_device.c **** 
1268:lib/lib_pic33e/usb/usb_device.c ****          #ifdef  USB_SUPPORT_OTG    
1269:lib/lib_pic33e/usb/usb_device.c ****              //Disable D+ Pull-up
1270:lib/lib_pic33e/usb/usb_device.c ****              U1OTGCONbits.DPPULUP = 0;
MPLAB XC16 ASSEMBLY Listing:   			page 48


1271:lib/lib_pic33e/usb/usb_device.c **** 
1272:lib/lib_pic33e/usb/usb_device.c ****              //Disable HNP
1273:lib/lib_pic33e/usb/usb_device.c ****              USBOTGDisableHnp();
1274:lib/lib_pic33e/usb/usb_device.c **** 
1275:lib/lib_pic33e/usb/usb_device.c ****              //Deactivate HNP
1276:lib/lib_pic33e/usb/usb_device.c ****              USBOTGDeactivateHnp();
1277:lib/lib_pic33e/usb/usb_device.c ****              
1278:lib/lib_pic33e/usb/usb_device.c ****              //If ID Pin Changed State
1279:lib/lib_pic33e/usb/usb_device.c ****              if (USBIDIF && USBIDIE)
1280:lib/lib_pic33e/usb/usb_device.c ****              {  
1281:lib/lib_pic33e/usb/usb_device.c ****                  //Re-detect & Initialize
1282:lib/lib_pic33e/usb/usb_device.c ****                   USBOTGInitialize();
1283:lib/lib_pic33e/usb/usb_device.c **** 
1284:lib/lib_pic33e/usb/usb_device.c ****                   //Clear ID Interrupt Flag
1285:lib/lib_pic33e/usb/usb_device.c ****                   USBClearInterruptFlag(USBIDIFReg,USBIDIFBitNum);
1286:lib/lib_pic33e/usb/usb_device.c ****              }
1287:lib/lib_pic33e/usb/usb_device.c ****          #endif
1288:lib/lib_pic33e/usb/usb_device.c **** 
1289:lib/lib_pic33e/usb/usb_device.c ****          #if defined __C30__ || defined __XC16__
1290:lib/lib_pic33e/usb/usb_device.c ****              //USBClearInterruptFlag(U1OTGIR, 3); 
1291:lib/lib_pic33e/usb/usb_device.c ****          #endif
1292:lib/lib_pic33e/usb/usb_device.c ****             //return so that we don't go through the rest of 
1293:lib/lib_pic33e/usb/usb_device.c ****             //the state machine
1294:lib/lib_pic33e/usb/usb_device.c ****           return;
1295:lib/lib_pic33e/usb/usb_device.c ****     }
1296:lib/lib_pic33e/usb/usb_device.c **** 
1297:lib/lib_pic33e/usb/usb_device.c **** #ifdef USB_SUPPORT_OTG
1298:lib/lib_pic33e/usb/usb_device.c ****     //If Session Is Started Then
1299:lib/lib_pic33e/usb/usb_device.c ****    else
1300:lib/lib_pic33e/usb/usb_device.c ****    {
1301:lib/lib_pic33e/usb/usb_device.c ****         //If SRP Is Ready
1302:lib/lib_pic33e/usb/usb_device.c ****         if (USBOTGSRPIsReady())
1303:lib/lib_pic33e/usb/usb_device.c ****         {   
1304:lib/lib_pic33e/usb/usb_device.c ****             //Clear SRPReady
1305:lib/lib_pic33e/usb/usb_device.c ****             USBOTGClearSRPReady();
1306:lib/lib_pic33e/usb/usb_device.c **** 
1307:lib/lib_pic33e/usb/usb_device.c ****             //Clear SRP Timeout Flag
1308:lib/lib_pic33e/usb/usb_device.c ****             USBOTGClearSRPTimeOutFlag();
1309:lib/lib_pic33e/usb/usb_device.c **** 
1310:lib/lib_pic33e/usb/usb_device.c ****             //Indicate Session Started
1311:lib/lib_pic33e/usb/usb_device.c ****             UART2PrintString( "\r\n***** USB OTG B Event - Session Started  *****\r\n" );
1312:lib/lib_pic33e/usb/usb_device.c ****         }
1313:lib/lib_pic33e/usb/usb_device.c ****     }
1314:lib/lib_pic33e/usb/usb_device.c **** #endif
1315:lib/lib_pic33e/usb/usb_device.c **** }
 1384              	.loc 1 1315 0
 1385 000710  8E 07 78 	mov w14,w15
 1386 000712  4F 07 78 	mov [--w15],w14
 1387 000714  00 40 A9 	bclr CORCON,#2
 1388 000716  00 00 06 	return 
 1389              	.set ___PA___,0
 1390              	.LFE6:
 1391              	.size _USBDeviceDetach,.-_USBDeviceDetach
 1392              	.align 2
 1393              	.global _USBDeviceAttach
 1394              	.type _USBDeviceAttach,@function
 1395              	_USBDeviceAttach:
MPLAB XC16 ASSEMBLY Listing:   			page 49


 1396              	.LFB7:
1316:lib/lib_pic33e/usb/usb_device.c **** #endif  //#if defined(USB_INTERRUPT)
1317:lib/lib_pic33e/usb/usb_device.c **** /**************************************************************************
1318:lib/lib_pic33e/usb/usb_device.c ****     Function:
1319:lib/lib_pic33e/usb/usb_device.c ****         void USBDeviceAttach(void)
1320:lib/lib_pic33e/usb/usb_device.c ****     
1321:lib/lib_pic33e/usb/usb_device.c ****     Summary:
1322:lib/lib_pic33e/usb/usb_device.c ****         Checks if VBUS is present, and that the USB module is not already 
1323:lib/lib_pic33e/usb/usb_device.c ****         initialized, and if so, enables the USB module so as to signal device 
1324:lib/lib_pic33e/usb/usb_device.c ****         attachment to the USB host.   
1325:lib/lib_pic33e/usb/usb_device.c **** 
1326:lib/lib_pic33e/usb/usb_device.c ****     Description:
1327:lib/lib_pic33e/usb/usb_device.c ****         This function indicates to the USB host that the USB device has been
1328:lib/lib_pic33e/usb/usb_device.c ****         attached to the bus.  This function needs to be called in order for the
1329:lib/lib_pic33e/usb/usb_device.c ****         device to start to enumerate on the bus.
1330:lib/lib_pic33e/usb/usb_device.c ****                 
1331:lib/lib_pic33e/usb/usb_device.c ****     Precondition:
1332:lib/lib_pic33e/usb/usb_device.c ****         Should only be called when USB_INTERRUPT is defined.  Also, should only 
1333:lib/lib_pic33e/usb/usb_device.c ****         be called from the main() loop context.  Do not call USBDeviceAttach()
1334:lib/lib_pic33e/usb/usb_device.c ****         from within an interrupt handler, as the USBDeviceAttach() function
1335:lib/lib_pic33e/usb/usb_device.c ****         may modify global interrupt enable bits and settings.
1336:lib/lib_pic33e/usb/usb_device.c **** 
1337:lib/lib_pic33e/usb/usb_device.c ****         For normal USB devices:
1338:lib/lib_pic33e/usb/usb_device.c ****         Make sure that if the module was previously on, that it has been turned off 
1339:lib/lib_pic33e/usb/usb_device.c ****         for a long time (ex: 100ms+) before calling this function to re-enable the module.
1340:lib/lib_pic33e/usb/usb_device.c ****         If the device turns off the D+ (for full speed) or D- (for low speed) ~1.5k ohm
1341:lib/lib_pic33e/usb/usb_device.c ****         pull up resistor, and then turns it back on very quickly, common hosts will sometimes 
1342:lib/lib_pic33e/usb/usb_device.c ****         reject this event, since no human could ever unplug and re-attach a USB device in a 
1343:lib/lib_pic33e/usb/usb_device.c ****         microseconds (or nanoseconds) timescale.  The host could simply treat this as some kind 
1344:lib/lib_pic33e/usb/usb_device.c ****         of glitch and ignore the event altogether.  
1345:lib/lib_pic33e/usb/usb_device.c ****     Parameters:
1346:lib/lib_pic33e/usb/usb_device.c ****         None
1347:lib/lib_pic33e/usb/usb_device.c ****      
1348:lib/lib_pic33e/usb/usb_device.c ****     Return Values:
1349:lib/lib_pic33e/usb/usb_device.c ****         None       
1350:lib/lib_pic33e/usb/usb_device.c ****     
1351:lib/lib_pic33e/usb/usb_device.c ****     Remarks: 
1352:lib/lib_pic33e/usb/usb_device.c **** 		See also the USBDeviceDetach() API function documentation.                                       
1353:lib/lib_pic33e/usb/usb_device.c **** ****************************************************************************/
1354:lib/lib_pic33e/usb/usb_device.c **** #if defined(USB_INTERRUPT)
1355:lib/lib_pic33e/usb/usb_device.c **** void USBDeviceAttach(void)
1356:lib/lib_pic33e/usb/usb_device.c **** {
 1397              	.loc 1 1356 0
 1398              	.set ___PA___,1
 1399 000718  00 00 FA 	lnk #0
 1400              	.LCFI10:
1357:lib/lib_pic33e/usb/usb_device.c **** 
1358:lib/lib_pic33e/usb/usb_device.c ****     //if we are in the detached state
1359:lib/lib_pic33e/usb/usb_device.c ****     if(USBDeviceState == DETACHED_STATE)
 1401              	.loc 1 1359 0
 1402 00071a  00 00 80 	mov _USBDeviceState,w0
 1403 00071c  00 00 E0 	cp0 w0
 1404              	.set ___BP___,0
 1405 00071e  00 00 3A 	bra nz,.L46
1360:lib/lib_pic33e/usb/usb_device.c ****     {
1361:lib/lib_pic33e/usb/usb_device.c ****         if(USB_BUS_SENSE == 1)
1362:lib/lib_pic33e/usb/usb_device.c ****         {
MPLAB XC16 ASSEMBLY Listing:   			page 50


1363:lib/lib_pic33e/usb/usb_device.c ****     	    //Initialize registers to known states.
1364:lib/lib_pic33e/usb/usb_device.c ****             U1CON = 0;          
 1406              	.loc 1 1364 0
 1407 000720  00 20 EF 	clr _U1CON
1365:lib/lib_pic33e/usb/usb_device.c ****     
1366:lib/lib_pic33e/usb/usb_device.c ****             // Mask all USB interrupts
1367:lib/lib_pic33e/usb/usb_device.c ****             U1IE = 0;                                
1368:lib/lib_pic33e/usb/usb_device.c ****     
1369:lib/lib_pic33e/usb/usb_device.c ****             //Configure things like: pull ups, full/low-speed mode, 
1370:lib/lib_pic33e/usb/usb_device.c ****             //set the ping pong mode, and set internal transceiver
1371:lib/lib_pic33e/usb/usb_device.c ****             SetConfigurationOptions();
 1408              	.loc 1 1371 0
 1409 000722  F2 09 20 	mov #159,w2
 1410              	.loc 1 1367 0
 1411 000724  00 20 EF 	clr _U1IE
 1412              	.loc 1 1371 0
 1413 000726  F1 09 20 	mov #159,w1
 1414 000728  00 20 EF 	clr _U1CNFG1
1372:lib/lib_pic33e/usb/usb_device.c ****     
1373:lib/lib_pic33e/usb/usb_device.c ****             USBEnableInterrupts();  //Modifies global interrupt settings
 1415              	.loc 1 1373 0
 1416 00072a  F0 8F 2F 	mov #-1793,w0
 1417              	.loc 1 1371 0
 1418 00072c  00 20 EF 	clr _U1CNFG2
 1419 00072e  00 40 A9 	bclr.b _U1OTGCONbits,#2
 1420 000730  02 00 88 	mov w2,_U1EIE
 1421 000732  01 00 88 	mov w1,_U1IE
 1422 000734  00 C0 A8 	bset.b _U1OTGIEbits,#6
 1423              	.loc 1 1373 0
 1424 000736  00 C0 A8 	bset.b _IEC5bits,#6
 1425 000738  01 00 80 	mov _IPC21bits,w1
 1426 00073a  00 80 60 	and w1,w0,w0
 1427 00073c  00 A0 A0 	bset w0,#10
 1428 00073e  00 00 88 	mov w0,_IPC21bits
1374:lib/lib_pic33e/usb/usb_device.c ****     
1375:lib/lib_pic33e/usb/usb_device.c ****             // Enable module & attach to bus
1376:lib/lib_pic33e/usb/usb_device.c ****             while(!U1CONbits.USBEN){U1CONbits.USBEN = 1;}
 1429              	.loc 1 1376 0
 1430 000740  00 00 37 	bra .L48
 1431              	.L49:
 1432 000742  00 00 A8 	bset.b _U1CONbits,#0
 1433              	.L48:
 1434 000744  00 00 80 	mov _U1CONbits,w0
 1435 000746  61 00 60 	and w0,#1,w0
 1436 000748  00 00 E0 	cp0 w0
 1437              	.set ___BP___,0
 1438 00074a  00 00 32 	bra z,.L49
1377:lib/lib_pic33e/usb/usb_device.c ****     
1378:lib/lib_pic33e/usb/usb_device.c ****             //moved to the attached state
1379:lib/lib_pic33e/usb/usb_device.c ****             USBDeviceState = ATTACHED_STATE;
 1439              	.loc 1 1379 0
 1440 00074c  10 00 20 	mov #1,w0
 1441 00074e  00 00 88 	mov w0,_USBDeviceState
 1442              	.L46:
1380:lib/lib_pic33e/usb/usb_device.c ****     
1381:lib/lib_pic33e/usb/usb_device.c ****             #ifdef  USB_SUPPORT_OTG
1382:lib/lib_pic33e/usb/usb_device.c ****                 U1OTGCON = USB_OTG_DPLUS_ENABLE | USB_OTG_ENABLE;  
MPLAB XC16 ASSEMBLY Listing:   			page 51


1383:lib/lib_pic33e/usb/usb_device.c ****             #endif
1384:lib/lib_pic33e/usb/usb_device.c ****         }
1385:lib/lib_pic33e/usb/usb_device.c ****     }
1386:lib/lib_pic33e/usb/usb_device.c **** }
 1443              	.loc 1 1386 0
 1444 000750  8E 07 78 	mov w14,w15
 1445 000752  4F 07 78 	mov [--w15],w14
 1446 000754  00 40 A9 	bclr CORCON,#2
 1447 000756  00 00 06 	return 
 1448              	.set ___PA___,0
 1449              	.LFE7:
 1450              	.size _USBDeviceAttach,.-_USBDeviceAttach
 1451              	.align 2
 1452              	.global _USBCtrlEPAllowStatusStage
 1453              	.type _USBCtrlEPAllowStatusStage,@function
 1454              	_USBCtrlEPAllowStatusStage:
 1455              	.LFB8:
1387:lib/lib_pic33e/usb/usb_device.c **** #endif  //#if defined(USB_INTERRUPT)
1388:lib/lib_pic33e/usb/usb_device.c **** 
1389:lib/lib_pic33e/usb/usb_device.c **** 
1390:lib/lib_pic33e/usb/usb_device.c **** /*******************************************************************************
1391:lib/lib_pic33e/usb/usb_device.c ****   Function: void USBCtrlEPAllowStatusStage(void);
1392:lib/lib_pic33e/usb/usb_device.c **** 
1393:lib/lib_pic33e/usb/usb_device.c ****   Summary: This function prepares the proper endpoint 0 IN or endpoint 0 OUT
1394:lib/lib_pic33e/usb/usb_device.c ****             (based on the controlTransferState) to allow the status stage packet
1395:lib/lib_pic33e/usb/usb_device.c ****             of a control transfer to complete.  This function gets used
1396:lib/lib_pic33e/usb/usb_device.c ****             internally by the USB stack itself, but it may also be called from
1397:lib/lib_pic33e/usb/usb_device.c ****             the application firmware, IF the application firmware called
1398:lib/lib_pic33e/usb/usb_device.c ****             the USBDeferStatusStage() function during the initial processing
1399:lib/lib_pic33e/usb/usb_device.c ****             of the control transfer request.  In this case, the application
1400:lib/lib_pic33e/usb/usb_device.c ****             must call the USBCtrlEPAllowStatusStage() once, after it has fully
1401:lib/lib_pic33e/usb/usb_device.c ****             completed processing and handling the data stage portion of the
1402:lib/lib_pic33e/usb/usb_device.c ****             request.
1403:lib/lib_pic33e/usb/usb_device.c **** 
1404:lib/lib_pic33e/usb/usb_device.c ****             If the application firmware has no need for delaying control
1405:lib/lib_pic33e/usb/usb_device.c ****             transfers, and therefore never calls USBDeferStatusStage(), then the
1406:lib/lib_pic33e/usb/usb_device.c ****             application firmware should not call USBCtrlEPAllowStatusStage().
1407:lib/lib_pic33e/usb/usb_device.c **** 
1408:lib/lib_pic33e/usb/usb_device.c ****   Description:
1409:lib/lib_pic33e/usb/usb_device.c **** 
1410:lib/lib_pic33e/usb/usb_device.c ****   Conditions:
1411:lib/lib_pic33e/usb/usb_device.c ****     None
1412:lib/lib_pic33e/usb/usb_device.c **** 
1413:lib/lib_pic33e/usb/usb_device.c ****   Input:
1414:lib/lib_pic33e/usb/usb_device.c **** 
1415:lib/lib_pic33e/usb/usb_device.c ****   Return:
1416:lib/lib_pic33e/usb/usb_device.c **** 
1417:lib/lib_pic33e/usb/usb_device.c ****   Remarks:
1418:lib/lib_pic33e/usb/usb_device.c ****     None
1419:lib/lib_pic33e/usb/usb_device.c ****   *****************************************************************************/
1420:lib/lib_pic33e/usb/usb_device.c **** void USBCtrlEPAllowStatusStage(void)
1421:lib/lib_pic33e/usb/usb_device.c **** {
 1456              	.loc 1 1421 0
 1457              	.set ___PA___,1
 1458 000758  00 00 FA 	lnk #0
 1459              	.LCFI11:
1422:lib/lib_pic33e/usb/usb_device.c ****     //Check and set two flags, prior to actually modifying any BDT entries.
MPLAB XC16 ASSEMBLY Listing:   			page 52


1423:lib/lib_pic33e/usb/usb_device.c ****     //This double checking is necessary to make certain that 
1424:lib/lib_pic33e/usb/usb_device.c ****     //USBCtrlEPAllowStatusStage() can be called twice simultaneously (ex: once 
1425:lib/lib_pic33e/usb/usb_device.c ****     //in main loop context, while simultaneously getting an interrupt which 
1426:lib/lib_pic33e/usb/usb_device.c ****     //tries to call USBCtrlEPAllowStatusStage() again, at the same time).
1427:lib/lib_pic33e/usb/usb_device.c ****     if(USBStatusStageEnabledFlag1 == false)
 1460              	.loc 1 1427 0
 1461 00075a  00 C0 BF 	mov.b _USBStatusStageEnabledFlag1,WREG
 1462 00075c  00 04 A2 	btg.b w0,#0
 1463 00075e  00 04 E0 	cp0.b w0
 1464              	.set ___BP___,0
 1465 000760  00 00 32 	bra z,.L50
1428:lib/lib_pic33e/usb/usb_device.c ****     {
1429:lib/lib_pic33e/usb/usb_device.c ****         USBStatusStageEnabledFlag1 = true;
 1466              	.loc 1 1429 0
 1467 000762  10 C0 B3 	mov.b #1,w0
 1468 000764  00 E0 B7 	mov.b WREG,_USBStatusStageEnabledFlag1
1430:lib/lib_pic33e/usb/usb_device.c ****         if(USBStatusStageEnabledFlag2 == false)
 1469              	.loc 1 1430 0
 1470 000766  00 C0 BF 	mov.b _USBStatusStageEnabledFlag2,WREG
 1471 000768  00 04 A2 	btg.b w0,#0
 1472 00076a  00 04 E0 	cp0.b w0
 1473              	.set ___BP___,0
 1474 00076c  00 00 32 	bra z,.L50
1431:lib/lib_pic33e/usb/usb_device.c ****         {
1432:lib/lib_pic33e/usb/usb_device.c ****             USBStatusStageEnabledFlag2 = true;
 1475              	.loc 1 1432 0
 1476 00076e  10 C0 B3 	mov.b #1,w0
 1477 000770  00 E0 B7 	mov.b WREG,_USBStatusStageEnabledFlag2
1433:lib/lib_pic33e/usb/usb_device.c ****         
1434:lib/lib_pic33e/usb/usb_device.c ****             //Determine which endpoints (EP0 IN or OUT needs arming for the status
1435:lib/lib_pic33e/usb/usb_device.c ****             //stage), based on the type of control transfer currently pending.
1436:lib/lib_pic33e/usb/usb_device.c ****             if(controlTransferState == CTRL_TRF_RX)
 1478              	.loc 1 1436 0
 1479 000772  00 C0 BF 	mov.b _controlTransferState,WREG
 1480 000774  E2 4F 50 	sub.b w0,#2,[w15]
 1481              	.set ___BP___,0
 1482 000776  00 00 3A 	bra nz,.L52
1437:lib/lib_pic33e/usb/usb_device.c ****             {
1438:lib/lib_pic33e/usb/usb_device.c ****                 pBDTEntryIn[0]->CNT = 0;
 1483              	.loc 1 1438 0
 1484 000778  00 00 80 	mov _pBDTEntryIn,w0
 1485 00077a  C1 CF B3 	mov.b #-4,w1
 1486 00077c  20 41 90 	mov.b [w0+2],w2
 1487 00077e  60 41 61 	and.b w2,#0,w2
 1488 000780  22 40 98 	mov.b w2,[w0+2]
 1489 000782  30 41 90 	mov.b [w0+3],w2
 1490 000784  81 40 61 	and.b w2,w1,w1
 1491 000786  31 40 98 	mov.b w1,[w0+3]
1439:lib/lib_pic33e/usb/usb_device.c ****                 pBDTEntryIn[0]->STAT.Val = _DAT1|(_DTSEN & _DTS_CHECKING_ENABLED);        
 1492              	.loc 1 1439 0
 1493 000788  00 00 80 	mov _pBDTEntryIn,w0
 1494 00078a  81 C4 B3 	mov.b #72,w1
 1495 00078c  10 41 78 	mov.b [w0],w2
 1496 00078e  60 41 61 	and.b w2,#0,w2
 1497 000790  81 40 71 	ior.b w2,w1,w1
 1498 000792  01 48 78 	mov.b w1,[w0]
1440:lib/lib_pic33e/usb/usb_device.c ****                 pBDTEntryIn[0]->STAT.Val |= _USIE;
MPLAB XC16 ASSEMBLY Listing:   			page 53


 1499              	.loc 1 1440 0
 1500 000794  F2 0F 20 	mov #255,w2
 1501              	.loc 1 1439 0
 1502 000796  90 40 90 	mov.b [w0+1],w1
 1503 000798  E0 C0 60 	and.b w1,#0,w1
 1504 00079a  11 40 98 	mov.b w1,[w0+1]
 1505              	.loc 1 1440 0
 1506 00079c  00 00 80 	mov _pBDTEntryIn,w0
 1507 00079e  01 00 80 	mov _pBDTEntryIn,w1
 1508 0007a0  11 82 FB 	ze [w1],w4
 1509 0007a2  00 00 00 	nop 
 1510 0007a4  91 40 90 	mov.b [w1+1],w1
 1511 0007a6  90 41 78 	mov.b [w0],w3
 1512 0007a8  81 80 FB 	ze w1,w1
 1513 0007aa  E0 C1 61 	and.b w3,#0,w3
 1514 0007ac  C8 08 DD 	sl w1,#8,w1
 1515 0007ae  81 00 72 	ior w4,w1,w1
 1516 0007b0  01 70 A0 	bset w1,#7
 1517 0007b2  02 81 60 	and w1,w2,w2
 1518 0007b4  02 C1 71 	ior.b w3,w2,w2
 1519 0007b6  02 48 78 	mov.b w2,[w0]
 1520 0007b8  C8 08 DE 	lsr w1,#8,w1
 1521 0007ba  10 41 90 	mov.b [w0+1],w2
 1522 0007bc  60 41 61 	and.b w2,#0,w2
 1523 0007be  81 40 71 	ior.b w2,w1,w1
 1524 0007c0  11 40 98 	mov.b w1,[w0+1]
 1525 0007c2  00 00 37 	bra .L50
 1526              	.L52:
1441:lib/lib_pic33e/usb/usb_device.c ****             }
1442:lib/lib_pic33e/usb/usb_device.c ****             else if(controlTransferState == CTRL_TRF_TX)
 1527              	.loc 1 1442 0
 1528 0007c4  00 C0 BF 	mov.b _controlTransferState,WREG
 1529 0007c6  E1 4F 50 	sub.b w0,#1,[w15]
 1530              	.set ___BP___,0
 1531 0007c8  00 00 3A 	bra nz,.L50
1443:lib/lib_pic33e/usb/usb_device.c ****             {
1444:lib/lib_pic33e/usb/usb_device.c ****                 BothEP0OutUOWNsSet = false;	//Indicator flag used in USBCtrlTrfOutHandler()
1445:lib/lib_pic33e/usb/usb_device.c **** 
1446:lib/lib_pic33e/usb/usb_device.c ****                 //This buffer (when ping pong buffering is enabled on EP0 OUT) receives the
1447:lib/lib_pic33e/usb/usb_device.c ****                 //next SETUP packet.
1448:lib/lib_pic33e/usb/usb_device.c ****                 #if((USB_PING_PONG_MODE == USB_PING_PONG__EP0_OUT_ONLY) || (USB_PING_PONG_MODE == U
1449:lib/lib_pic33e/usb/usb_device.c ****                     pBDTEntryEP0OutCurrent->CNT = USB_EP0_BUFF_SIZE;
 1532              	.loc 1 1449 0
 1533 0007ca  00 00 80 	mov _pBDTEntryEP0OutCurrent,w0
 1534              	.loc 1 1444 0
 1535 0007cc  00 60 EF 	clr.b _BothEP0OutUOWNsSet
 1536              	.loc 1 1449 0
 1537 0007ce  C1 CF B3 	mov.b #-4,w1
 1538 0007d0  20 41 90 	mov.b [w0+2],w2
 1539 0007d2  60 41 61 	and.b w2,#0,w2
 1540 0007d4  02 34 A0 	bset.b w2,#3
 1541 0007d6  22 40 98 	mov.b w2,[w0+2]
 1542 0007d8  30 41 90 	mov.b [w0+3],w2
 1543 0007da  81 40 61 	and.b w2,w1,w1
 1544 0007dc  31 40 98 	mov.b w1,[w0+3]
1450:lib/lib_pic33e/usb/usb_device.c ****                     pBDTEntryEP0OutCurrent->ADR = ConvertToPhysicalAddress(&SetupPkt);
 1545              	.loc 1 1450 0
MPLAB XC16 ASSEMBLY Listing:   			page 54


 1546 0007de  00 00 80 	mov _pBDTEntryEP0OutCurrent,w0
 1547 0007e0  01 00 20 	mov #_SetupPkt,w1
 1548 0007e2  C0 41 90 	mov.b [w0+4],w3
 1549 0007e4  F2 0F 20 	mov #255,w2
 1550 0007e6  E0 C1 61 	and.b w3,#0,w3
 1551 0007e8  02 81 60 	and w1,w2,w2
 1552 0007ea  C8 08 DE 	lsr w1,#8,w1
 1553 0007ec  02 C1 71 	ior.b w3,w2,w2
 1554 0007ee  42 40 98 	mov.b w2,[w0+4]
 1555 0007f0  50 41 90 	mov.b [w0+5],w2
 1556 0007f2  60 41 61 	and.b w2,#0,w2
 1557 0007f4  81 40 71 	ior.b w2,w1,w1
 1558 0007f6  51 40 98 	mov.b w1,[w0+5]
1451:lib/lib_pic33e/usb/usb_device.c ****                     pBDTEntryEP0OutCurrent->STAT.Val = _BSTALL; //Prepare endpoint to accept a SETU
 1559              	.loc 1 1451 0
 1560 0007f8  00 00 80 	mov _pBDTEntryEP0OutCurrent,w0
1452:lib/lib_pic33e/usb/usb_device.c ****                     pBDTEntryEP0OutCurrent->STAT.Val |= _USIE;
 1561              	.loc 1 1452 0
 1562 0007fa  F4 0F 20 	mov #255,w4
 1563              	.loc 1 1451 0
 1564 0007fc  90 40 78 	mov.b [w0],w1
 1565 0007fe  E0 C0 60 	and.b w1,#0,w1
 1566 000800  01 24 A0 	bset.b w1,#2
 1567 000802  01 48 78 	mov.b w1,[w0]
1453:lib/lib_pic33e/usb/usb_device.c ****                     BothEP0OutUOWNsSet = true;	//Indicator flag used in USBCtrlTrfOutHandler()
 1568              	.loc 1 1453 0
 1569 000804  13 C0 B3 	mov.b #1,w3
 1570              	.loc 1 1451 0
 1571 000806  90 40 90 	mov.b [w0+1],w1
 1572 000808  E0 C0 60 	and.b w1,#0,w1
 1573 00080a  11 40 98 	mov.b w1,[w0+1]
 1574              	.loc 1 1452 0
 1575 00080c  00 00 80 	mov _pBDTEntryEP0OutCurrent,w0
 1576 00080e  01 00 80 	mov _pBDTEntryEP0OutCurrent,w1
1454:lib/lib_pic33e/usb/usb_device.c ****                 #endif
1455:lib/lib_pic33e/usb/usb_device.c **** 
1456:lib/lib_pic33e/usb/usb_device.c ****                 //This EP0 OUT buffer receives the 0-byte OUT status stage packet.
1457:lib/lib_pic33e/usb/usb_device.c ****                 pBDTEntryEP0OutNext->CNT = USB_EP0_BUFF_SIZE;
 1577              	.loc 1 1457 0
 1578 000810  C2 CF B3 	mov.b #-4,w2
 1579              	.loc 1 1452 0
 1580 000812  11 83 FB 	ze [w1],w6
 1581 000814  91 40 90 	mov.b [w1+1],w1
 1582 000816  90 42 78 	mov.b [w0],w5
 1583 000818  81 80 FB 	ze w1,w1
 1584 00081a  E0 C2 62 	and.b w5,#0,w5
 1585 00081c  C8 08 DD 	sl w1,#8,w1
 1586 00081e  81 00 73 	ior w6,w1,w1
 1587 000820  01 70 A0 	bset w1,#7
 1588 000822  04 82 60 	and w1,w4,w4
 1589 000824  04 C2 72 	ior.b w5,w4,w4
 1590 000826  04 48 78 	mov.b w4,[w0]
 1591 000828  C8 08 DE 	lsr w1,#8,w1
 1592 00082a  10 42 90 	mov.b [w0+1],w4
 1593 00082c  60 42 62 	and.b w4,#0,w4
 1594 00082e  81 40 72 	ior.b w4,w1,w1
 1595 000830  11 40 98 	mov.b w1,[w0+1]
MPLAB XC16 ASSEMBLY Listing:   			page 55


 1596              	.loc 1 1457 0
 1597 000832  00 00 80 	mov _pBDTEntryEP0OutNext,w0
 1598              	.loc 1 1453 0
 1599 000834  03 42 78 	mov.b w3,w4
 1600 000836  01 00 20 	mov #_BothEP0OutUOWNsSet,w1
 1601 000838  84 48 78 	mov.b w4,[w1]
 1602              	.loc 1 1457 0
 1603 00083a  A0 40 90 	mov.b [w0+2],w1
 1604 00083c  E0 C0 60 	and.b w1,#0,w1
 1605 00083e  01 34 A0 	bset.b w1,#3
 1606 000840  21 40 98 	mov.b w1,[w0+2]
 1607 000842  B0 40 90 	mov.b [w0+3],w1
 1608 000844  82 C0 60 	and.b w1,w2,w1
 1609 000846  31 40 98 	mov.b w1,[w0+3]
1458:lib/lib_pic33e/usb/usb_device.c ****                 pBDTEntryEP0OutNext->ADR = ConvertToPhysicalAddress(&SetupPkt);
 1610              	.loc 1 1458 0
 1611 000848  00 00 80 	mov _pBDTEntryEP0OutNext,w0
 1612 00084a  01 00 20 	mov #_SetupPkt,w1
 1613 00084c  C0 41 90 	mov.b [w0+4],w3
 1614 00084e  F2 0F 20 	mov #255,w2
 1615 000850  E0 C1 61 	and.b w3,#0,w3
 1616 000852  02 81 60 	and w1,w2,w2
 1617 000854  C8 08 DE 	lsr w1,#8,w1
 1618 000856  02 C1 71 	ior.b w3,w2,w2
 1619 000858  42 40 98 	mov.b w2,[w0+4]
 1620 00085a  50 41 90 	mov.b [w0+5],w2
 1621 00085c  60 41 61 	and.b w2,#0,w2
 1622 00085e  81 40 71 	ior.b w2,w1,w1
 1623 000860  51 40 98 	mov.b w1,[w0+5]
1459:lib/lib_pic33e/usb/usb_device.c ****                 pBDTEntryEP0OutNext->STAT.Val = _USIE;           // Note: DTSEN is 0
 1624              	.loc 1 1459 0
 1625 000862  00 00 80 	mov _pBDTEntryEP0OutNext,w0
 1626 000864  90 40 78 	mov.b [w0],w1
 1627 000866  E0 C0 60 	and.b w1,#0,w1
 1628 000868  01 74 A0 	bset.b w1,#7
 1629 00086a  01 48 78 	mov.b w1,[w0]
 1630 00086c  90 40 90 	mov.b [w0+1],w1
 1631 00086e  E0 C0 60 	and.b w1,#0,w1
 1632 000870  11 40 98 	mov.b w1,[w0+1]
 1633              	.L50:
1460:lib/lib_pic33e/usb/usb_device.c ****             }
1461:lib/lib_pic33e/usb/usb_device.c ****         }    
1462:lib/lib_pic33e/usb/usb_device.c ****     }
1463:lib/lib_pic33e/usb/usb_device.c **** }   
 1634              	.loc 1 1463 0
 1635 000872  8E 07 78 	mov w14,w15
 1636 000874  4F 07 78 	mov [--w15],w14
 1637 000876  00 40 A9 	bclr CORCON,#2
 1638 000878  00 00 06 	return 
 1639              	.set ___PA___,0
 1640              	.LFE8:
 1641              	.size _USBCtrlEPAllowStatusStage,.-_USBCtrlEPAllowStatusStage
 1642              	.align 2
 1643              	.global _USBCtrlEPAllowDataStage
 1644              	.type _USBCtrlEPAllowDataStage,@function
 1645              	_USBCtrlEPAllowDataStage:
 1646              	.LFB9:
MPLAB XC16 ASSEMBLY Listing:   			page 56


1464:lib/lib_pic33e/usb/usb_device.c **** 
1465:lib/lib_pic33e/usb/usb_device.c **** 
1466:lib/lib_pic33e/usb/usb_device.c **** /*******************************************************************************
1467:lib/lib_pic33e/usb/usb_device.c ****   Function: void USBCtrlEPAllowDataStage(void);
1468:lib/lib_pic33e/usb/usb_device.c ****     
1469:lib/lib_pic33e/usb/usb_device.c ****   Summary: This function allows the data stage of either a host-to-device or
1470:lib/lib_pic33e/usb/usb_device.c ****             device-to-host control transfer (with data stage) to complete.
1471:lib/lib_pic33e/usb/usb_device.c ****             This function is meant to be used in conjunction with either the
1472:lib/lib_pic33e/usb/usb_device.c ****             USBDeferOUTDataStage() or USBDeferINDataStage().  If the firmware
1473:lib/lib_pic33e/usb/usb_device.c ****             does not call either USBDeferOUTDataStage() or USBDeferINDataStage(),
1474:lib/lib_pic33e/usb/usb_device.c ****             then the firmware does not need to manually call 
1475:lib/lib_pic33e/usb/usb_device.c ****             USBCtrlEPAllowDataStage(), as the USB stack will call this function
1476:lib/lib_pic33e/usb/usb_device.c ****             instead.
1477:lib/lib_pic33e/usb/usb_device.c ****      
1478:lib/lib_pic33e/usb/usb_device.c ****   Description:
1479:lib/lib_pic33e/usb/usb_device.c ****     
1480:lib/lib_pic33e/usb/usb_device.c ****   Conditions: A control transfer (with data stage) should already be pending, 
1481:lib/lib_pic33e/usb/usb_device.c ****                 if the firmware calls this function.  Additionally, the firmware
1482:lib/lib_pic33e/usb/usb_device.c ****                 should have called either USBDeferOUTDataStage() or 
1483:lib/lib_pic33e/usb/usb_device.c ****                 USBDeferINDataStage() at the start of the control transfer, if
1484:lib/lib_pic33e/usb/usb_device.c ****                 the firmware will be calling this function manually.
1485:lib/lib_pic33e/usb/usb_device.c **** 
1486:lib/lib_pic33e/usb/usb_device.c ****   Input:
1487:lib/lib_pic33e/usb/usb_device.c **** 
1488:lib/lib_pic33e/usb/usb_device.c ****   Return:
1489:lib/lib_pic33e/usb/usb_device.c **** 
1490:lib/lib_pic33e/usb/usb_device.c ****   Remarks: 
1491:lib/lib_pic33e/usb/usb_device.c ****   *****************************************************************************/
1492:lib/lib_pic33e/usb/usb_device.c **** void USBCtrlEPAllowDataStage(void)
1493:lib/lib_pic33e/usb/usb_device.c **** {
 1647              	.loc 1 1493 0
 1648              	.set ___PA___,1
 1649 00087a  00 00 FA 	lnk #0
 1650              	.LCFI12:
1494:lib/lib_pic33e/usb/usb_device.c ****     USBDeferINDataStagePackets = false;
 1651              	.loc 1 1494 0
 1652 00087c  00 60 EF 	clr.b _USBDeferINDataStagePackets
1495:lib/lib_pic33e/usb/usb_device.c ****     USBDeferOUTDataStagePackets = false;
 1653              	.loc 1 1495 0
 1654 00087e  00 60 EF 	clr.b _USBDeferOUTDataStagePackets
1496:lib/lib_pic33e/usb/usb_device.c **** 
1497:lib/lib_pic33e/usb/usb_device.c ****     if(controlTransferState == CTRL_TRF_RX) //(<setup><out><out>...<out><in>)
 1655              	.loc 1 1497 0
 1656 000880  00 C0 BF 	mov.b _controlTransferState,WREG
 1657 000882  E2 4F 50 	sub.b w0,#2,[w15]
 1658              	.set ___BP___,0
 1659 000884  00 00 3A 	bra nz,.L54
1498:lib/lib_pic33e/usb/usb_device.c ****     {
1499:lib/lib_pic33e/usb/usb_device.c ****         //Prepare EP0 OUT to receive the first OUT data packet in the data stage sequence.
1500:lib/lib_pic33e/usb/usb_device.c ****         pBDTEntryEP0OutNext->CNT = USB_EP0_BUFF_SIZE;
 1660              	.loc 1 1500 0
 1661 000886  00 00 80 	mov _pBDTEntryEP0OutNext,w0
 1662 000888  C1 CF B3 	mov.b #-4,w1
 1663 00088a  20 41 90 	mov.b [w0+2],w2
 1664 00088c  60 41 61 	and.b w2,#0,w2
 1665 00088e  02 34 A0 	bset.b w2,#3
 1666 000890  22 40 98 	mov.b w2,[w0+2]
MPLAB XC16 ASSEMBLY Listing:   			page 57


 1667 000892  30 41 90 	mov.b [w0+3],w2
 1668 000894  81 40 61 	and.b w2,w1,w1
 1669 000896  31 40 98 	mov.b w1,[w0+3]
1501:lib/lib_pic33e/usb/usb_device.c ****         pBDTEntryEP0OutNext->ADR = ConvertToPhysicalAddress(&CtrlTrfData);
 1670              	.loc 1 1501 0
 1671 000898  00 00 80 	mov _pBDTEntryEP0OutNext,w0
 1672 00089a  01 00 20 	mov #_CtrlTrfData,w1
 1673 00089c  C0 41 90 	mov.b [w0+4],w3
 1674 00089e  F2 0F 20 	mov #255,w2
 1675 0008a0  E0 C1 61 	and.b w3,#0,w3
 1676 0008a2  02 81 60 	and w1,w2,w2
 1677 0008a4  C8 08 DE 	lsr w1,#8,w1
 1678 0008a6  02 C1 71 	ior.b w3,w2,w2
 1679 0008a8  42 40 98 	mov.b w2,[w0+4]
 1680 0008aa  50 41 90 	mov.b [w0+5],w2
 1681 0008ac  60 41 61 	and.b w2,#0,w2
 1682 0008ae  81 40 71 	ior.b w2,w1,w1
 1683 0008b0  51 40 98 	mov.b w1,[w0+5]
1502:lib/lib_pic33e/usb/usb_device.c ****         pBDTEntryEP0OutNext->STAT.Val = _DAT1|(_DTSEN & _DTS_CHECKING_ENABLED);
 1684              	.loc 1 1502 0
 1685 0008b2  00 00 80 	mov _pBDTEntryEP0OutNext,w0
 1686 0008b4  81 C4 B3 	mov.b #72,w1
 1687 0008b6  10 41 78 	mov.b [w0],w2
 1688 0008b8  60 41 61 	and.b w2,#0,w2
 1689 0008ba  81 40 71 	ior.b w2,w1,w1
 1690 0008bc  01 48 78 	mov.b w1,[w0]
1503:lib/lib_pic33e/usb/usb_device.c ****         pBDTEntryEP0OutNext->STAT.Val |= _USIE;
 1691              	.loc 1 1503 0
 1692 0008be  F2 0F 20 	mov #255,w2
 1693              	.loc 1 1502 0
 1694 0008c0  90 40 90 	mov.b [w0+1],w1
 1695 0008c2  E0 C0 60 	and.b w1,#0,w1
 1696 0008c4  11 40 98 	mov.b w1,[w0+1]
 1697              	.loc 1 1503 0
 1698 0008c6  00 00 80 	mov _pBDTEntryEP0OutNext,w0
 1699 0008c8  01 00 80 	mov _pBDTEntryEP0OutNext,w1
 1700 0008ca  11 82 FB 	ze [w1],w4
 1701 0008cc  00 00 00 	nop 
 1702 0008ce  91 40 90 	mov.b [w1+1],w1
 1703 0008d0  90 41 78 	mov.b [w0],w3
 1704 0008d2  81 80 FB 	ze w1,w1
 1705 0008d4  E0 C1 61 	and.b w3,#0,w3
 1706 0008d6  C8 08 DD 	sl w1,#8,w1
 1707 0008d8  81 00 72 	ior w4,w1,w1
 1708 0008da  01 70 A0 	bset w1,#7
 1709 0008dc  02 81 60 	and w1,w2,w2
 1710 0008de  02 C1 71 	ior.b w3,w2,w2
 1711 0008e0  02 48 78 	mov.b w2,[w0]
 1712 0008e2  C8 08 DE 	lsr w1,#8,w1
 1713 0008e4  10 41 90 	mov.b [w0+1],w2
 1714 0008e6  60 41 61 	and.b w2,#0,w2
 1715 0008e8  81 40 71 	ior.b w2,w1,w1
 1716 0008ea  11 40 98 	mov.b w1,[w0+1]
 1717 0008ec  00 00 37 	bra .L53
 1718              	.L54:
1504:lib/lib_pic33e/usb/usb_device.c ****     }   
1505:lib/lib_pic33e/usb/usb_device.c ****     else    //else must be controlTransferState == CTRL_TRF_TX (<setup><in><in>...<in><out>)
MPLAB XC16 ASSEMBLY Listing:   			page 58


1506:lib/lib_pic33e/usb/usb_device.c ****     {
1507:lib/lib_pic33e/usb/usb_device.c ****         //Error check the data stage byte count.  Make sure the user specified
1508:lib/lib_pic33e/usb/usb_device.c ****         //value was no greater than the number of bytes the host requested.
1509:lib/lib_pic33e/usb/usb_device.c **** 		if(SetupPkt.wLength < inPipes[0].wCount.Val)
 1719              	.loc 1 1509 0
 1720 0008ee  61 00 20 	mov #_SetupPkt+6,w1
 1721 0008f0  70 00 20 	mov #_SetupPkt+7,w0
 1722 0008f2  91 80 FB 	ze [w1],w1
 1723 0008f4  10 81 FB 	ze [w0],w2
 1724 0008f6  20 00 80 	mov _inPipes+4,w0
 1725 0008f8  48 11 DD 	sl w2,#8,w2
 1726 0008fa  82 80 70 	ior w1,w2,w1
 1727 0008fc  80 8F 50 	sub w1,w0,[w15]
 1728              	.set ___BP___,0
 1729 0008fe  00 00 31 	bra geu,.L56
1510:lib/lib_pic33e/usb/usb_device.c **** 		{
1511:lib/lib_pic33e/usb/usb_device.c **** 			inPipes[0].wCount.Val = SetupPkt.wLength;
 1730              	.loc 1 1511 0
 1731 000900  60 00 20 	mov #_SetupPkt+6,w0
 1732 000902  71 00 20 	mov #_SetupPkt+7,w1
 1733 000904  10 80 FB 	ze [w0],w0
 1734 000906  91 80 FB 	ze [w1],w1
 1735 000908  C8 08 DD 	sl w1,#8,w1
 1736 00090a  01 00 70 	ior w0,w1,w0
 1737 00090c  20 00 88 	mov w0,_inPipes+4
 1738              	.L56:
1512:lib/lib_pic33e/usb/usb_device.c **** 		}
1513:lib/lib_pic33e/usb/usb_device.c **** 		USBCtrlTrfTxService();  //Copies one IN data packet worth of data from application buffer
 1739              	.loc 1 1513 0
 1740 00090e  00 00 07 	rcall _USBCtrlTrfTxService
1514:lib/lib_pic33e/usb/usb_device.c **** 		                        //to CtrlTrfData buffer.  Also keeps track of how many bytes remaining.
1515:lib/lib_pic33e/usb/usb_device.c **** 
1516:lib/lib_pic33e/usb/usb_device.c **** 	    //Cnt should have been initialized by responsible request owner (ex: by
1517:lib/lib_pic33e/usb/usb_device.c **** 	    //using the USBEP0SendRAMPtr() or USBEP0SendROMPtr() API function).
1518:lib/lib_pic33e/usb/usb_device.c **** 		pBDTEntryIn[0]->ADR = ConvertToPhysicalAddress(&CtrlTrfData);
 1741              	.loc 1 1518 0
 1742 000910  00 00 80 	mov _pBDTEntryIn,w0
 1743 000912  01 00 20 	mov #_CtrlTrfData,w1
 1744 000914  C0 41 90 	mov.b [w0+4],w3
 1745 000916  F2 0F 20 	mov #255,w2
 1746 000918  E0 C1 61 	and.b w3,#0,w3
 1747 00091a  02 81 60 	and w1,w2,w2
 1748 00091c  C8 08 DE 	lsr w1,#8,w1
 1749 00091e  02 C1 71 	ior.b w3,w2,w2
 1750 000920  42 40 98 	mov.b w2,[w0+4]
 1751 000922  50 41 90 	mov.b [w0+5],w2
 1752 000924  60 41 61 	and.b w2,#0,w2
 1753 000926  81 40 71 	ior.b w2,w1,w1
 1754 000928  51 40 98 	mov.b w1,[w0+5]
1519:lib/lib_pic33e/usb/usb_device.c **** 		pBDTEntryIn[0]->STAT.Val = _DAT1|(_DTSEN & _DTS_CHECKING_ENABLED);
 1755              	.loc 1 1519 0
 1756 00092a  00 00 80 	mov _pBDTEntryIn,w0
 1757 00092c  81 C4 B3 	mov.b #72,w1
 1758 00092e  10 41 78 	mov.b [w0],w2
 1759 000930  60 41 61 	and.b w2,#0,w2
 1760 000932  81 40 71 	ior.b w2,w1,w1
 1761 000934  01 48 78 	mov.b w1,[w0]
MPLAB XC16 ASSEMBLY Listing:   			page 59


1520:lib/lib_pic33e/usb/usb_device.c ****         pBDTEntryIn[0]->STAT.Val |= _USIE;
 1762              	.loc 1 1520 0
 1763 000936  F2 0F 20 	mov #255,w2
 1764              	.loc 1 1519 0
 1765 000938  90 40 90 	mov.b [w0+1],w1
 1766 00093a  E0 C0 60 	and.b w1,#0,w1
 1767 00093c  11 40 98 	mov.b w1,[w0+1]
 1768              	.loc 1 1520 0
 1769 00093e  00 00 80 	mov _pBDTEntryIn,w0
 1770 000940  01 00 80 	mov _pBDTEntryIn,w1
 1771 000942  11 82 FB 	ze [w1],w4
 1772 000944  00 00 00 	nop 
 1773 000946  91 40 90 	mov.b [w1+1],w1
 1774 000948  90 41 78 	mov.b [w0],w3
 1775 00094a  81 80 FB 	ze w1,w1
 1776 00094c  E0 C1 61 	and.b w3,#0,w3
 1777 00094e  C8 08 DD 	sl w1,#8,w1
 1778 000950  81 00 72 	ior w4,w1,w1
 1779 000952  01 70 A0 	bset w1,#7
 1780 000954  02 81 60 	and w1,w2,w2
 1781 000956  02 C1 71 	ior.b w3,w2,w2
 1782 000958  02 48 78 	mov.b w2,[w0]
 1783 00095a  C8 08 DE 	lsr w1,#8,w1
 1784 00095c  10 41 90 	mov.b [w0+1],w2
 1785 00095e  60 41 61 	and.b w2,#0,w2
 1786 000960  81 40 71 	ior.b w2,w1,w1
 1787 000962  11 40 98 	mov.b w1,[w0+1]
 1788              	.L53:
1521:lib/lib_pic33e/usb/usb_device.c ****     }     
1522:lib/lib_pic33e/usb/usb_device.c **** }    
 1789              	.loc 1 1522 0
 1790 000964  8E 07 78 	mov w14,w15
 1791 000966  4F 07 78 	mov [--w15],w14
 1792 000968  00 40 A9 	bclr CORCON,#2
 1793 00096a  00 00 06 	return 
 1794              	.set ___PA___,0
 1795              	.LFE9:
 1796              	.size _USBCtrlEPAllowDataStage,.-_USBCtrlEPAllowDataStage
 1797              	.align 2
 1798              	.type _USBConfigureEndpoint,@function
 1799              	_USBConfigureEndpoint:
 1800              	.LFB10:
1523:lib/lib_pic33e/usb/usb_device.c **** 
1524:lib/lib_pic33e/usb/usb_device.c **** 
1525:lib/lib_pic33e/usb/usb_device.c **** /******************************************************************************/
1526:lib/lib_pic33e/usb/usb_device.c **** /** Internal Functions *********************************************************/
1527:lib/lib_pic33e/usb/usb_device.c **** /******************************************************************************/
1528:lib/lib_pic33e/usb/usb_device.c **** 
1529:lib/lib_pic33e/usb/usb_device.c **** /********************************************************************
1530:lib/lib_pic33e/usb/usb_device.c ****  * Function:        void USBConfigureEndpoint(uint8_t EPNum, uint8_t direction)
1531:lib/lib_pic33e/usb/usb_device.c ****  *
1532:lib/lib_pic33e/usb/usb_device.c ****  * PreCondition:    None
1533:lib/lib_pic33e/usb/usb_device.c ****  *
1534:lib/lib_pic33e/usb/usb_device.c ****  * Input:           uint8_t EPNum - the endpoint to be configured
1535:lib/lib_pic33e/usb/usb_device.c ****  *                  uint8_t direction - the direction to be configured
1536:lib/lib_pic33e/usb/usb_device.c ****  *                                   (either OUT_FROM_HOST or IN_TO_HOST)
1537:lib/lib_pic33e/usb/usb_device.c ****  *
MPLAB XC16 ASSEMBLY Listing:   			page 60


1538:lib/lib_pic33e/usb/usb_device.c ****  * Output:          None
1539:lib/lib_pic33e/usb/usb_device.c ****  *
1540:lib/lib_pic33e/usb/usb_device.c ****  * Side Effects:    None
1541:lib/lib_pic33e/usb/usb_device.c ****  *
1542:lib/lib_pic33e/usb/usb_device.c ****  * Overview:        This function will configure the specified 
1543:lib/lib_pic33e/usb/usb_device.c ****  *                  endpoint
1544:lib/lib_pic33e/usb/usb_device.c ****  *
1545:lib/lib_pic33e/usb/usb_device.c ****  * Note:            None
1546:lib/lib_pic33e/usb/usb_device.c ****  *******************************************************************/
1547:lib/lib_pic33e/usb/usb_device.c **** static void USBConfigureEndpoint(uint8_t EPNum, uint8_t direction)
1548:lib/lib_pic33e/usb/usb_device.c **** {
 1801              	.loc 1 1548 0
 1802              	.set ___PA___,1
 1803 00096c  04 00 FA 	lnk #4
 1804              	.LCFI13:
1549:lib/lib_pic33e/usb/usb_device.c ****     volatile BDT_ENTRY* handle;
1550:lib/lib_pic33e/usb/usb_device.c **** 
1551:lib/lib_pic33e/usb/usb_device.c ****     //Compute a pointer to the even BDT entry corresponding to the
1552:lib/lib_pic33e/usb/usb_device.c ****     //EPNum and direction values passed to this function.
1553:lib/lib_pic33e/usb/usb_device.c ****     handle = (volatile BDT_ENTRY*)&BDT[EP0_OUT_EVEN]; //Get address of start of BDT
 1805              	.loc 1 1553 0
 1806 00096e  02 00 20 	mov #_BDT,w2
 1807 000970  02 0F 78 	mov w2,[w14]
 1808              	.loc 1 1548 0
 1809 000972  20 47 98 	mov.b w0,[w14+2]
 1810 000974  31 47 98 	mov.b w1,[w14+3]
1554:lib/lib_pic33e/usb/usb_device.c ****     handle += EP(EPNum,direction,0u);     //Add in offset to the BDT of interest
 1811              	.loc 1 1554 0
 1812 000976  2E 40 90 	mov.b [w14+2],w0
 1813 000978  BE 40 90 	mov.b [w14+3],w1
 1814 00097a  00 80 FB 	ze w0,w0
 1815 00097c  81 80 FB 	ze w1,w1
 1816 00097e  00 00 40 	add w0,w0,w0
 1817 000980  01 00 40 	add w0,w1,w0
 1818 000982  44 00 DD 	sl w0,#4,w0
 1819 000984  1E 0F 40 	add w0,[w14],[w14]
1555:lib/lib_pic33e/usb/usb_device.c ****     
1556:lib/lib_pic33e/usb/usb_device.c ****     handle->STAT.UOWN = 0;  //mostly redundant, since USBStdSetCfgHandler() 
 1820              	.loc 1 1556 0
 1821 000986  1E 00 78 	mov [w14],w0
 1822 000988  90 40 78 	mov.b [w0],w1
 1823 00098a  01 74 A1 	bclr.b w1,#7
 1824 00098c  01 48 78 	mov.b w1,[w0]
1557:lib/lib_pic33e/usb/usb_device.c ****     //already cleared the entire BDT table
1558:lib/lib_pic33e/usb/usb_device.c **** 
1559:lib/lib_pic33e/usb/usb_device.c ****     //Make sure our pBDTEntryIn/Out[] pointer is initialized.  Needed later
1560:lib/lib_pic33e/usb/usb_device.c ****     //for USBTransferOnePacket() API calls.
1561:lib/lib_pic33e/usb/usb_device.c ****     if(direction == OUT_FROM_HOST)
 1825              	.loc 1 1561 0
 1826 00098e  3E 40 90 	mov.b [w14+3],w0
 1827 000990  00 04 E0 	cp0.b w0
 1828              	.set ___BP___,0
 1829 000992  00 00 3A 	bra nz,.L58
1562:lib/lib_pic33e/usb/usb_device.c ****     {
1563:lib/lib_pic33e/usb/usb_device.c ****         pBDTEntryOut[EPNum] = handle;
 1830              	.loc 1 1563 0
 1831 000994  2E 40 90 	mov.b [w14+2],w0
MPLAB XC16 ASSEMBLY Listing:   			page 61


 1832 000996  01 00 20 	mov #_pBDTEntryOut,w1
 1833 000998  00 80 FB 	ze w0,w0
 1834 00099a  00 00 40 	add w0,w0,w0
 1835 00099c  01 00 40 	add w0,w1,w0
 1836 00099e  1E 08 78 	mov [w14],[w0]
 1837 0009a0  00 00 37 	bra .L59
 1838              	.L58:
1564:lib/lib_pic33e/usb/usb_device.c ****     }
1565:lib/lib_pic33e/usb/usb_device.c ****     else
1566:lib/lib_pic33e/usb/usb_device.c ****     {
1567:lib/lib_pic33e/usb/usb_device.c ****         pBDTEntryIn[EPNum] = handle;
 1839              	.loc 1 1567 0
 1840 0009a2  00 00 00 	nop 
 1841 0009a4  2E 40 90 	mov.b [w14+2],w0
 1842 0009a6  01 00 20 	mov #_pBDTEntryIn,w1
 1843 0009a8  00 80 FB 	ze w0,w0
 1844 0009aa  00 00 40 	add w0,w0,w0
 1845 0009ac  01 00 40 	add w0,w1,w0
 1846 0009ae  1E 08 78 	mov [w14],[w0]
 1847              	.L59:
1568:lib/lib_pic33e/usb/usb_device.c ****     }
1569:lib/lib_pic33e/usb/usb_device.c **** 
1570:lib/lib_pic33e/usb/usb_device.c ****     #if (USB_PING_PONG_MODE == USB_PING_PONG__FULL_PING_PONG)
1571:lib/lib_pic33e/usb/usb_device.c ****         handle->STAT.DTS = 0;
 1848              	.loc 1 1571 0
 1849 0009b0  00 00 00 	nop 
 1850 0009b2  1E 00 78 	mov [w14],w0
 1851 0009b4  00 00 00 	nop 
 1852 0009b6  90 40 78 	mov.b [w0],w1
 1853 0009b8  01 64 A1 	bclr.b w1,#6
 1854 0009ba  01 48 78 	mov.b w1,[w0]
1572:lib/lib_pic33e/usb/usb_device.c ****         (handle+1)->STAT.DTS = 1;
 1855              	.loc 1 1572 0
 1856 0009bc  9E 00 78 	mov [w14],w1
 1857 0009be  68 80 40 	add w1,#8,w0
 1858 0009c0  90 40 78 	mov.b [w0],w1
 1859 0009c2  01 64 A0 	bset.b w1,#6
 1860 0009c4  01 48 78 	mov.b w1,[w0]
1573:lib/lib_pic33e/usb/usb_device.c ****     #elif (USB_PING_PONG_MODE == USB_PING_PONG__NO_PING_PONG)
1574:lib/lib_pic33e/usb/usb_device.c ****         //Set DTS to one because the first thing we will do
1575:lib/lib_pic33e/usb/usb_device.c ****         //when transmitting is toggle the bit
1576:lib/lib_pic33e/usb/usb_device.c ****         handle->STAT.DTS = 1;
1577:lib/lib_pic33e/usb/usb_device.c ****     #elif (USB_PING_PONG_MODE == USB_PING_PONG__EP0_OUT_ONLY)
1578:lib/lib_pic33e/usb/usb_device.c ****         if(EPNum != 0)
1579:lib/lib_pic33e/usb/usb_device.c ****         {
1580:lib/lib_pic33e/usb/usb_device.c ****             handle->STAT.DTS = 1;
1581:lib/lib_pic33e/usb/usb_device.c ****         }
1582:lib/lib_pic33e/usb/usb_device.c ****     #elif (USB_PING_PONG_MODE == USB_PING_PONG__ALL_BUT_EP0)    
1583:lib/lib_pic33e/usb/usb_device.c ****         if(EPNum != 0)
1584:lib/lib_pic33e/usb/usb_device.c ****         {
1585:lib/lib_pic33e/usb/usb_device.c ****             handle->STAT.DTS = 0;
1586:lib/lib_pic33e/usb/usb_device.c ****             (handle+1)->STAT.DTS = 1;
1587:lib/lib_pic33e/usb/usb_device.c ****         }
1588:lib/lib_pic33e/usb/usb_device.c ****     #endif
1589:lib/lib_pic33e/usb/usb_device.c **** }
 1861              	.loc 1 1589 0
 1862 0009c6  8E 07 78 	mov w14,w15
MPLAB XC16 ASSEMBLY Listing:   			page 62


 1863 0009c8  4F 07 78 	mov [--w15],w14
 1864 0009ca  00 40 A9 	bclr CORCON,#2
 1865 0009cc  00 00 06 	return 
 1866              	.set ___PA___,0
 1867              	.LFE10:
 1868              	.size _USBConfigureEndpoint,.-_USBConfigureEndpoint
 1869              	.align 2
 1870              	.type _USBCtrlEPServiceComplete,@function
 1871              	_USBCtrlEPServiceComplete:
 1872              	.LFB11:
1590:lib/lib_pic33e/usb/usb_device.c **** 
1591:lib/lib_pic33e/usb/usb_device.c **** 
1592:lib/lib_pic33e/usb/usb_device.c **** /******************************************************************************
1593:lib/lib_pic33e/usb/usb_device.c ****  * Function:        void USBCtrlEPServiceComplete(void)
1594:lib/lib_pic33e/usb/usb_device.c ****  *
1595:lib/lib_pic33e/usb/usb_device.c ****  * PreCondition:    None
1596:lib/lib_pic33e/usb/usb_device.c ****  *
1597:lib/lib_pic33e/usb/usb_device.c ****  * Input:           None
1598:lib/lib_pic33e/usb/usb_device.c ****  *
1599:lib/lib_pic33e/usb/usb_device.c ****  * Output:          None
1600:lib/lib_pic33e/usb/usb_device.c ****  *
1601:lib/lib_pic33e/usb/usb_device.c ****  * Side Effects:    None
1602:lib/lib_pic33e/usb/usb_device.c ****  *
1603:lib/lib_pic33e/usb/usb_device.c ****  * Overview:        This routine wrap up the remaining tasks in servicing
1604:lib/lib_pic33e/usb/usb_device.c ****  *                  a Setup Request. Its main task is to set the endpoint
1605:lib/lib_pic33e/usb/usb_device.c ****  *                  controls appropriately for a given situation. See code
1606:lib/lib_pic33e/usb/usb_device.c ****  *                  below.
1607:lib/lib_pic33e/usb/usb_device.c ****  *                  There are three main scenarios:
1608:lib/lib_pic33e/usb/usb_device.c ****  *                  a) There was no handler for the Request, in this case
1609:lib/lib_pic33e/usb/usb_device.c ****  *                     a STALL should be sent out.
1610:lib/lib_pic33e/usb/usb_device.c ****  *                  b) The host has requested a read control transfer,
1611:lib/lib_pic33e/usb/usb_device.c ****  *                     endpoints are required to be setup in a specific way.
1612:lib/lib_pic33e/usb/usb_device.c ****  *                  c) The host has requested a write control transfer, or
1613:lib/lib_pic33e/usb/usb_device.c ****  *                     a control data stage is not required, endpoints are
1614:lib/lib_pic33e/usb/usb_device.c ****  *                     required to be setup in a specific way.
1615:lib/lib_pic33e/usb/usb_device.c ****  *
1616:lib/lib_pic33e/usb/usb_device.c ****  *                  Packet processing is resumed by clearing PKTDIS bit.
1617:lib/lib_pic33e/usb/usb_device.c ****  *
1618:lib/lib_pic33e/usb/usb_device.c ****  * Note:            None
1619:lib/lib_pic33e/usb/usb_device.c ****  *****************************************************************************/
1620:lib/lib_pic33e/usb/usb_device.c **** static void USBCtrlEPServiceComplete(void)
1621:lib/lib_pic33e/usb/usb_device.c **** {
 1873              	.loc 1 1621 0
 1874              	.set ___PA___,1
 1875 0009ce  00 00 FA 	lnk #0
 1876              	.LCFI14:
1622:lib/lib_pic33e/usb/usb_device.c ****     /*
1623:lib/lib_pic33e/usb/usb_device.c ****      * PKTDIS bit is set when a Setup Transaction is received.
1624:lib/lib_pic33e/usb/usb_device.c ****      * Clear to resume packet processing.
1625:lib/lib_pic33e/usb/usb_device.c ****      */
1626:lib/lib_pic33e/usb/usb_device.c ****     USBPacketDisable = 0;
 1877              	.loc 1 1626 0
 1878 0009d0  00 A0 A9 	bclr.b _U1CONbits,#5
1627:lib/lib_pic33e/usb/usb_device.c **** 
1628:lib/lib_pic33e/usb/usb_device.c **** 	//Check the busy bits and the SetupPtk.DataDir variables to determine what type of
1629:lib/lib_pic33e/usb/usb_device.c **** 	//control transfer is currently in progress.  We need to know the type of control
1630:lib/lib_pic33e/usb/usb_device.c **** 	//transfer that is currently pending, in order to know how to properly arm the 
MPLAB XC16 ASSEMBLY Listing:   			page 63


1631:lib/lib_pic33e/usb/usb_device.c **** 	//EP0 IN and EP0 OUT endpoints.
1632:lib/lib_pic33e/usb/usb_device.c ****     if(inPipes[0].info.bits.busy == 0)
 1879              	.loc 1 1632 0
 1880 0009d2  00 08 20 	mov #128,w0
 1881 0009d4  11 00 80 	mov _inPipes+2,w1
 1882 0009d6  00 80 60 	and w1,w0,w0
 1883 0009d8  00 00 E0 	cp0 w0
 1884              	.set ___BP___,0
 1885 0009da  00 00 3A 	bra nz,.L61
1633:lib/lib_pic33e/usb/usb_device.c ****     {
1634:lib/lib_pic33e/usb/usb_device.c ****         if(outPipes[0].info.bits.busy == 1)
 1886              	.loc 1 1634 0
 1887 0009dc  21 00 20 	mov #_outPipes+2,w1
 1888 0009de  00 C8 B3 	mov.b #-128,w0
 1889 0009e0  91 40 78 	mov.b [w1],w1
 1890 0009e2  00 C0 60 	and.b w1,w0,w0
 1891 0009e4  00 04 E0 	cp0.b w0
 1892              	.set ___BP___,0
 1893 0009e6  00 00 32 	bra z,.L62
1635:lib/lib_pic33e/usb/usb_device.c ****         {
1636:lib/lib_pic33e/usb/usb_device.c ****             controlTransferState = CTRL_TRF_RX;
 1894              	.loc 1 1636 0
 1895 0009e8  20 C0 B3 	mov.b #2,w0
 1896 0009ea  00 E0 B7 	mov.b WREG,_controlTransferState
1637:lib/lib_pic33e/usb/usb_device.c ****             /*
1638:lib/lib_pic33e/usb/usb_device.c ****              * Control Write:
1639:lib/lib_pic33e/usb/usb_device.c ****              * <SETUP[0]><OUT[1]><OUT[0]>...<IN[1]> | <SETUP[0]>
1640:lib/lib_pic33e/usb/usb_device.c ****              */
1641:lib/lib_pic33e/usb/usb_device.c **** 
1642:lib/lib_pic33e/usb/usb_device.c ****             //1. Prepare OUT EP to receive data, unless a USB class request handler
1643:lib/lib_pic33e/usb/usb_device.c ****             //   function decided to defer the data stage (ex: because the intended
1644:lib/lib_pic33e/usb/usb_device.c ****             //   RAM buffer wasn't available yet) by calling USBDeferDataStage().
1645:lib/lib_pic33e/usb/usb_device.c ****             //   If it did so, it is then responsible for calling USBCtrlEPAllowDataStage(),
1646:lib/lib_pic33e/usb/usb_device.c ****             //   once it is ready to begin receiving the data.
1647:lib/lib_pic33e/usb/usb_device.c ****             if(USBDeferOUTDataStagePackets == false)
 1897              	.loc 1 1647 0
 1898 0009ec  00 C0 BF 	mov.b _USBDeferOUTDataStagePackets,WREG
 1899 0009ee  00 04 A2 	btg.b w0,#0
 1900 0009f0  00 04 E0 	cp0.b w0
 1901              	.set ___BP___,0
 1902 0009f2  00 00 32 	bra z,.L63
1648:lib/lib_pic33e/usb/usb_device.c ****             {
1649:lib/lib_pic33e/usb/usb_device.c ****                 USBCtrlEPAllowDataStage();
 1903              	.loc 1 1649 0
 1904 0009f4  00 00 07 	rcall _USBCtrlEPAllowDataStage
 1905              	.L63:
1650:lib/lib_pic33e/usb/usb_device.c ****             }
1651:lib/lib_pic33e/usb/usb_device.c ****             
1652:lib/lib_pic33e/usb/usb_device.c ****             //2.  IN endpoint 0 status stage will be armed by USBCtrlEPAllowStatusStage() 
1653:lib/lib_pic33e/usb/usb_device.c ****             //after all of the OUT data has been received and consumed, or if a timeout occurs.
1654:lib/lib_pic33e/usb/usb_device.c ****             USBStatusStageEnabledFlag2 = false;
 1906              	.loc 1 1654 0
 1907 0009f6  00 60 EF 	clr.b _USBStatusStageEnabledFlag2
1655:lib/lib_pic33e/usb/usb_device.c ****             USBStatusStageEnabledFlag1 = false;
 1908              	.loc 1 1655 0
 1909 0009f8  00 60 EF 	clr.b _USBStatusStageEnabledFlag1
 1910 0009fa  00 00 37 	bra .L60
MPLAB XC16 ASSEMBLY Listing:   			page 64


 1911              	.L62:
1656:lib/lib_pic33e/usb/usb_device.c ****         }
1657:lib/lib_pic33e/usb/usb_device.c ****         else
1658:lib/lib_pic33e/usb/usb_device.c ****         {
1659:lib/lib_pic33e/usb/usb_device.c ****             /*
1660:lib/lib_pic33e/usb/usb_device.c ****              * If no one knows how to service this request then stall.
1661:lib/lib_pic33e/usb/usb_device.c ****              * Must also prepare EP0 to receive the next SETUP transaction.
1662:lib/lib_pic33e/usb/usb_device.c ****              */
1663:lib/lib_pic33e/usb/usb_device.c ****             pBDTEntryEP0OutNext->CNT = USB_EP0_BUFF_SIZE;
 1912              	.loc 1 1663 0
 1913 0009fc  00 00 80 	mov _pBDTEntryEP0OutNext,w0
 1914 0009fe  C1 CF B3 	mov.b #-4,w1
 1915 000a00  20 41 90 	mov.b [w0+2],w2
 1916 000a02  60 41 61 	and.b w2,#0,w2
 1917 000a04  02 34 A0 	bset.b w2,#3
 1918 000a06  22 40 98 	mov.b w2,[w0+2]
 1919 000a08  30 41 90 	mov.b [w0+3],w2
 1920 000a0a  81 40 61 	and.b w2,w1,w1
 1921 000a0c  31 40 98 	mov.b w1,[w0+3]
1664:lib/lib_pic33e/usb/usb_device.c ****             pBDTEntryEP0OutNext->ADR = ConvertToPhysicalAddress(&SetupPkt);
 1922              	.loc 1 1664 0
 1923 000a0e  00 00 80 	mov _pBDTEntryEP0OutNext,w0
 1924 000a10  01 00 20 	mov #_SetupPkt,w1
 1925 000a12  C0 41 90 	mov.b [w0+4],w3
 1926 000a14  F2 0F 20 	mov #255,w2
 1927 000a16  E0 C1 61 	and.b w3,#0,w3
 1928 000a18  02 81 60 	and w1,w2,w2
 1929 000a1a  C8 08 DE 	lsr w1,#8,w1
 1930 000a1c  02 C1 71 	ior.b w3,w2,w2
 1931 000a1e  42 40 98 	mov.b w2,[w0+4]
 1932 000a20  50 41 90 	mov.b [w0+5],w2
 1933 000a22  60 41 61 	and.b w2,#0,w2
 1934 000a24  81 40 71 	ior.b w2,w1,w1
 1935 000a26  51 40 98 	mov.b w1,[w0+5]
1665:lib/lib_pic33e/usb/usb_device.c ****             pBDTEntryEP0OutNext->STAT.Val = _DAT0|(_DTSEN & _DTS_CHECKING_ENABLED)|_BSTALL;
 1936              	.loc 1 1665 0
 1937 000a28  00 00 80 	mov _pBDTEntryEP0OutNext,w0
1666:lib/lib_pic33e/usb/usb_device.c ****             pBDTEntryEP0OutNext->STAT.Val |= _USIE;
 1938              	.loc 1 1666 0
 1939 000a2a  F3 0F 20 	mov #255,w3
 1940              	.loc 1 1665 0
 1941 000a2c  90 40 78 	mov.b [w0],w1
 1942 000a2e  E0 C0 60 	and.b w1,#0,w1
 1943 000a30  C1 40 B3 	ior.b #12,w1
 1944 000a32  01 48 78 	mov.b w1,[w0]
1667:lib/lib_pic33e/usb/usb_device.c ****             pBDTEntryIn[0]->STAT.Val = _BSTALL;
1668:lib/lib_pic33e/usb/usb_device.c ****             pBDTEntryIn[0]->STAT.Val |= _USIE;
 1945              	.loc 1 1668 0
 1946 000a34  F2 0F 20 	mov #255,w2
 1947              	.loc 1 1665 0
 1948 000a36  90 40 90 	mov.b [w0+1],w1
 1949 000a38  E0 C0 60 	and.b w1,#0,w1
 1950 000a3a  11 40 98 	mov.b w1,[w0+1]
 1951              	.loc 1 1666 0
 1952 000a3c  00 00 80 	mov _pBDTEntryEP0OutNext,w0
 1953 000a3e  01 00 80 	mov _pBDTEntryEP0OutNext,w1
 1954 000a40  91 82 FB 	ze [w1],w5
MPLAB XC16 ASSEMBLY Listing:   			page 65


 1955 000a42  00 00 00 	nop 
 1956 000a44  91 40 90 	mov.b [w1+1],w1
 1957 000a46  10 42 78 	mov.b [w0],w4
 1958 000a48  81 80 FB 	ze w1,w1
 1959 000a4a  60 42 62 	and.b w4,#0,w4
 1960 000a4c  C8 08 DD 	sl w1,#8,w1
 1961 000a4e  81 80 72 	ior w5,w1,w1
 1962 000a50  01 70 A0 	bset w1,#7
 1963 000a52  83 81 60 	and w1,w3,w3
 1964 000a54  83 41 72 	ior.b w4,w3,w3
 1965 000a56  03 48 78 	mov.b w3,[w0]
 1966 000a58  C8 08 DE 	lsr w1,#8,w1
 1967 000a5a  90 41 90 	mov.b [w0+1],w3
 1968 000a5c  E0 C1 61 	and.b w3,#0,w3
 1969 000a5e  81 C0 71 	ior.b w3,w1,w1
 1970 000a60  11 40 98 	mov.b w1,[w0+1]
 1971              	.loc 1 1667 0
 1972 000a62  00 00 80 	mov _pBDTEntryIn,w0
 1973 000a64  90 40 78 	mov.b [w0],w1
 1974 000a66  E0 C0 60 	and.b w1,#0,w1
 1975 000a68  01 24 A0 	bset.b w1,#2
 1976 000a6a  01 48 78 	mov.b w1,[w0]
 1977 000a6c  90 40 90 	mov.b [w0+1],w1
 1978 000a6e  E0 C0 60 	and.b w1,#0,w1
 1979 000a70  11 40 98 	mov.b w1,[w0+1]
 1980              	.loc 1 1668 0
 1981 000a72  00 00 80 	mov _pBDTEntryIn,w0
 1982 000a74  01 00 80 	mov _pBDTEntryIn,w1
 1983 000a76  11 82 FB 	ze [w1],w4
 1984 000a78  00 00 00 	nop 
 1985 000a7a  91 40 90 	mov.b [w1+1],w1
 1986 000a7c  90 41 78 	mov.b [w0],w3
 1987 000a7e  81 80 FB 	ze w1,w1
 1988 000a80  E0 C1 61 	and.b w3,#0,w3
 1989 000a82  C8 08 DD 	sl w1,#8,w1
 1990 000a84  81 00 72 	ior w4,w1,w1
 1991 000a86  01 70 A0 	bset w1,#7
 1992 000a88  02 81 60 	and w1,w2,w2
 1993 000a8a  C8 08 DE 	lsr w1,#8,w1
 1994 000a8c  02 C1 71 	ior.b w3,w2,w2
 1995 000a8e  02 48 78 	mov.b w2,[w0]
 1996 000a90  10 41 90 	mov.b [w0+1],w2
 1997 000a92  60 41 61 	and.b w2,#0,w2
 1998 000a94  81 40 71 	ior.b w2,w1,w1
 1999 000a96  11 40 98 	mov.b w1,[w0+1]
 2000 000a98  00 00 37 	bra .L60
 2001              	.L61:
1669:lib/lib_pic33e/usb/usb_device.c ****         }
1670:lib/lib_pic33e/usb/usb_device.c ****     }
1671:lib/lib_pic33e/usb/usb_device.c ****     else    // A module has claimed ownership of the control transfer session.
1672:lib/lib_pic33e/usb/usb_device.c ****     {
1673:lib/lib_pic33e/usb/usb_device.c **** 		if(SetupPkt.DataDir == USB_SETUP_DEVICE_TO_HOST_BITFIELD)
 2002              	.loc 1 1673 0
 2003 000a9a  01 00 20 	mov #_SetupPkt,w1
 2004 000a9c  00 C8 B3 	mov.b #-128,w0
 2005 000a9e  91 40 78 	mov.b [w1],w1
 2006 000aa0  00 C0 60 	and.b w1,w0,w0
MPLAB XC16 ASSEMBLY Listing:   			page 66


 2007 000aa2  00 04 E0 	cp0.b w0
 2008              	.set ___BP___,0
 2009 000aa4  00 00 32 	bra z,.L65
1674:lib/lib_pic33e/usb/usb_device.c **** 		{
1675:lib/lib_pic33e/usb/usb_device.c **** 			controlTransferState = CTRL_TRF_TX;
 2010              	.loc 1 1675 0
 2011 000aa6  10 C0 B3 	mov.b #1,w0
 2012 000aa8  00 E0 B7 	mov.b WREG,_controlTransferState
1676:lib/lib_pic33e/usb/usb_device.c **** 			/*
1677:lib/lib_pic33e/usb/usb_device.c **** 			 * Control Read:
1678:lib/lib_pic33e/usb/usb_device.c **** 			 * <SETUP[0]><IN[1]><IN[0]>...<OUT[1]> | <SETUP[0]>
1679:lib/lib_pic33e/usb/usb_device.c **** 			 *
1680:lib/lib_pic33e/usb/usb_device.c **** 			 * 1. Prepare IN EP to transfer data to the host.  If however the data
1681:lib/lib_pic33e/usb/usb_device.c **** 			 *    wasn't ready yet (ex: because the firmware needs to go and read it from
1682:lib/lib_pic33e/usb/usb_device.c **** 			 *    some slow/currently unavailable resource, such as an external I2C EEPconst),
1683:lib/lib_pic33e/usb/usb_device.c **** 			 *    Then the class request handler responsible should call the USBDeferDataStage()
1684:lib/lib_pic33e/usb/usb_device.c **** 			 *    macro.  In this case, the firmware may wait up to 500ms, before it is required
1685:lib/lib_pic33e/usb/usb_device.c **** 			 *    to transmit the first IN data packet.  Once the data is ready, and the firmware
1686:lib/lib_pic33e/usb/usb_device.c **** 			 *    is ready to begin sending the data, it should then call the 
1687:lib/lib_pic33e/usb/usb_device.c **** 			 *    USBCtrlEPAllowDataStage() function to start the data stage.
1688:lib/lib_pic33e/usb/usb_device.c **** 			 */
1689:lib/lib_pic33e/usb/usb_device.c **** 			if(USBDeferINDataStagePackets == false)
 2013              	.loc 1 1689 0
 2014 000aaa  00 C0 BF 	mov.b _USBDeferINDataStagePackets,WREG
 2015 000aac  00 04 A2 	btg.b w0,#0
 2016 000aae  00 04 E0 	cp0.b w0
 2017              	.set ___BP___,0
 2018 000ab0  00 00 32 	bra z,.L66
1690:lib/lib_pic33e/usb/usb_device.c ****             {
1691:lib/lib_pic33e/usb/usb_device.c ****                 USBCtrlEPAllowDataStage();
 2019              	.loc 1 1691 0
 2020 000ab2  00 00 07 	rcall _USBCtrlEPAllowDataStage
 2021              	.L66:
1692:lib/lib_pic33e/usb/usb_device.c **** 			}
1693:lib/lib_pic33e/usb/usb_device.c **** 
1694:lib/lib_pic33e/usb/usb_device.c ****             // 2. (Optionally) allow the status stage now, to prepare for early termination.
1695:lib/lib_pic33e/usb/usb_device.c ****             //    Note: If a class request handler decided to set USBDeferStatusStagePacket == true
1696:lib/lib_pic33e/usb/usb_device.c ****             //    then it is responsible for eventually calling USBCtrlEPAllowStatusStage() once it
1697:lib/lib_pic33e/usb/usb_device.c ****             //    is ready.  If the class request handler does this, it needs to be careful to
1698:lib/lib_pic33e/usb/usb_device.c ****             //    be written so that it can handle the early termination scenario.
1699:lib/lib_pic33e/usb/usb_device.c ****             //    Ex: It should call USBCtrlEPAllowStatusStage() when any of the following occurs:
1700:lib/lib_pic33e/usb/usb_device.c ****             //    1.  The desired total number of bytes were sent to the host.
1701:lib/lib_pic33e/usb/usb_device.c ****             //    2.  The number of bytes that the host originally requested (in the SETUP packet t
1702:lib/lib_pic33e/usb/usb_device.c ****             //        started the control transfer) has been reached.
1703:lib/lib_pic33e/usb/usb_device.c ****             //    3.  Or, if a timeout occurs (ex: <50ms since the last successful EP0 IN transacti
1704:lib/lib_pic33e/usb/usb_device.c ****             //        of how many bytes have actually been sent.  This is necessary to prevent a de
1705:lib/lib_pic33e/usb/usb_device.c ****             //        (where the control transfer can't complete, due to continuous NAK on status s
1706:lib/lib_pic33e/usb/usb_device.c ****             //        host performs early termination.  If enabled, the USB_ENABLE_STATUS_STAGE_TIM
1707:lib/lib_pic33e/usb/usb_device.c ****             //        option can take care of this for you.
1708:lib/lib_pic33e/usb/usb_device.c ****             //    Note: For this type of control transfer, there is normally no harm in simply armi
1709:lib/lib_pic33e/usb/usb_device.c ****             //    status stage packet right now, even if the IN data is not ready yet.  This allows
1710:lib/lib_pic33e/usb/usb_device.c ****             //    immediate early termination, without adding unnecessary delay.  Therefore, it is 
1711:lib/lib_pic33e/usb/usb_device.c ****             //    recommended for the USB class handler firmware to call USBDeferStatusStage(), for
1712:lib/lib_pic33e/usb/usb_device.c ****             //    type of control transfer.  If the USB class handler firmware needs more time to f
1713:lib/lib_pic33e/usb/usb_device.c ****             //    data that needs to be sent to the host, it should instead use the USBDeferDataSta
1714:lib/lib_pic33e/usb/usb_device.c ****             USBStatusStageEnabledFlag2 = false;
 2022              	.loc 1 1714 0
MPLAB XC16 ASSEMBLY Listing:   			page 67


 2023 000ab4  00 60 EF 	clr.b _USBStatusStageEnabledFlag2
1715:lib/lib_pic33e/usb/usb_device.c ****             USBStatusStageEnabledFlag1 = false;
 2024              	.loc 1 1715 0
 2025 000ab6  00 60 EF 	clr.b _USBStatusStageEnabledFlag1
1716:lib/lib_pic33e/usb/usb_device.c ****             if(USBDeferStatusStagePacket == false)
 2026              	.loc 1 1716 0
 2027 000ab8  00 C0 BF 	mov.b _USBDeferStatusStagePacket,WREG
 2028 000aba  00 04 A2 	btg.b w0,#0
 2029 000abc  00 04 E0 	cp0.b w0
 2030              	.set ___BP___,0
 2031 000abe  00 00 32 	bra z,.L60
1717:lib/lib_pic33e/usb/usb_device.c ****             {
1718:lib/lib_pic33e/usb/usb_device.c ****                 USBCtrlEPAllowStatusStage();
 2032              	.loc 1 1718 0
 2033 000ac0  00 00 07 	rcall _USBCtrlEPAllowStatusStage
 2034 000ac2  00 00 37 	bra .L60
 2035              	.L65:
1719:lib/lib_pic33e/usb/usb_device.c ****             } 
1720:lib/lib_pic33e/usb/usb_device.c **** 		}
1721:lib/lib_pic33e/usb/usb_device.c **** 		else   // (SetupPkt.DataDir == USB_SETUP_DIRECTION_HOST_TO_DEVICE)
1722:lib/lib_pic33e/usb/usb_device.c **** 		{
1723:lib/lib_pic33e/usb/usb_device.c **** 			//This situation occurs for special types of control transfers,
1724:lib/lib_pic33e/usb/usb_device.c **** 			//such as that which occurs when the host sends a SET_ADDRESS
1725:lib/lib_pic33e/usb/usb_device.c **** 			//control transfer.  Ex:
1726:lib/lib_pic33e/usb/usb_device.c **** 			//
1727:lib/lib_pic33e/usb/usb_device.c **** 			//<SETUP[0]><IN[1]> | <SETUP[0]>
1728:lib/lib_pic33e/usb/usb_device.c **** 				
1729:lib/lib_pic33e/usb/usb_device.c **** 			//Although the data direction is HOST_TO_DEVICE, there is no data stage
1730:lib/lib_pic33e/usb/usb_device.c **** 			//(hence: outPipes[0].info.bits.busy == 0).  There is however still
1731:lib/lib_pic33e/usb/usb_device.c **** 			//an IN status stage.
1732:lib/lib_pic33e/usb/usb_device.c **** 
1733:lib/lib_pic33e/usb/usb_device.c **** 			controlTransferState = CTRL_TRF_RX;     //Since this is a HOST_TO_DEVICE control transfer
1734:lib/lib_pic33e/usb/usb_device.c **** 			
1735:lib/lib_pic33e/usb/usb_device.c **** 			//1. Prepare OUT EP to receive the next SETUP packet.
1736:lib/lib_pic33e/usb/usb_device.c **** 			pBDTEntryEP0OutNext->CNT = USB_EP0_BUFF_SIZE;
 2036              	.loc 1 1736 0
 2037 000ac4  00 00 80 	mov _pBDTEntryEP0OutNext,w0
 2038              	.loc 1 1733 0
 2039 000ac6  22 C0 B3 	mov.b #2,w2
 2040              	.loc 1 1736 0
 2041 000ac8  C1 CF B3 	mov.b #-4,w1
 2042              	.loc 1 1733 0
 2043 000aca  00 00 00 	nop 
 2044 000acc  02 42 78 	mov.b w2,w4
 2045 000ace  03 00 20 	mov #_controlTransferState,w3
 2046 000ad0  84 49 78 	mov.b w4,[w3]
 2047              	.loc 1 1736 0
 2048 000ad2  20 41 90 	mov.b [w0+2],w2
 2049 000ad4  60 41 61 	and.b w2,#0,w2
 2050 000ad6  02 34 A0 	bset.b w2,#3
 2051 000ad8  22 40 98 	mov.b w2,[w0+2]
 2052 000ada  30 41 90 	mov.b [w0+3],w2
 2053 000adc  81 40 61 	and.b w2,w1,w1
 2054 000ade  31 40 98 	mov.b w1,[w0+3]
1737:lib/lib_pic33e/usb/usb_device.c **** 			pBDTEntryEP0OutNext->ADR = ConvertToPhysicalAddress(&SetupPkt);
 2055              	.loc 1 1737 0
 2056 000ae0  00 00 80 	mov _pBDTEntryEP0OutNext,w0
MPLAB XC16 ASSEMBLY Listing:   			page 68


 2057 000ae2  01 00 20 	mov #_SetupPkt,w1
 2058 000ae4  C0 41 90 	mov.b [w0+4],w3
 2059 000ae6  F2 0F 20 	mov #255,w2
 2060 000ae8  E0 C1 61 	and.b w3,#0,w3
 2061 000aea  02 81 60 	and w1,w2,w2
 2062 000aec  C8 08 DE 	lsr w1,#8,w1
 2063 000aee  02 C1 71 	ior.b w3,w2,w2
 2064 000af0  42 40 98 	mov.b w2,[w0+4]
 2065 000af2  50 41 90 	mov.b [w0+5],w2
 2066 000af4  60 41 61 	and.b w2,#0,w2
 2067 000af6  81 40 71 	ior.b w2,w1,w1
 2068 000af8  51 40 98 	mov.b w1,[w0+5]
1738:lib/lib_pic33e/usb/usb_device.c **** 			pBDTEntryEP0OutNext->STAT.Val = _BSTALL;
 2069              	.loc 1 1738 0
 2070 000afa  00 00 80 	mov _pBDTEntryEP0OutNext,w0
1739:lib/lib_pic33e/usb/usb_device.c ****             pBDTEntryEP0OutNext->STAT.Val |= _USIE;
 2071              	.loc 1 1739 0
 2072 000afc  F2 0F 20 	mov #255,w2
 2073              	.loc 1 1738 0
 2074 000afe  90 40 78 	mov.b [w0],w1
 2075 000b00  E0 C0 60 	and.b w1,#0,w1
 2076 000b02  01 24 A0 	bset.b w1,#2
 2077 000b04  01 48 78 	mov.b w1,[w0]
 2078 000b06  90 40 90 	mov.b [w0+1],w1
 2079 000b08  E0 C0 60 	and.b w1,#0,w1
 2080 000b0a  11 40 98 	mov.b w1,[w0+1]
 2081              	.loc 1 1739 0
 2082 000b0c  00 00 80 	mov _pBDTEntryEP0OutNext,w0
 2083 000b0e  01 00 80 	mov _pBDTEntryEP0OutNext,w1
 2084 000b10  11 82 FB 	ze [w1],w4
 2085 000b12  00 00 00 	nop 
 2086 000b14  91 40 90 	mov.b [w1+1],w1
 2087 000b16  90 41 78 	mov.b [w0],w3
 2088 000b18  81 80 FB 	ze w1,w1
 2089 000b1a  E0 C1 61 	and.b w3,#0,w3
 2090 000b1c  C8 08 DD 	sl w1,#8,w1
 2091 000b1e  81 00 72 	ior w4,w1,w1
 2092 000b20  01 70 A0 	bset w1,#7
 2093 000b22  02 81 60 	and w1,w2,w2
 2094 000b24  02 C1 71 	ior.b w3,w2,w2
 2095 000b26  02 48 78 	mov.b w2,[w0]
 2096 000b28  C8 08 DE 	lsr w1,#8,w1
 2097 000b2a  10 41 90 	mov.b [w0+1],w2
 2098 000b2c  60 41 61 	and.b w2,#0,w2
 2099 000b2e  81 40 71 	ior.b w2,w1,w1
 2100 000b30  11 40 98 	mov.b w1,[w0+1]
1740:lib/lib_pic33e/usb/usb_device.c **** 				
1741:lib/lib_pic33e/usb/usb_device.c **** 			//2. Prepare for IN status stage of the control transfer
1742:lib/lib_pic33e/usb/usb_device.c ****             USBStatusStageEnabledFlag2 = false;
 2101              	.loc 1 1742 0
 2102 000b32  00 60 EF 	clr.b _USBStatusStageEnabledFlag2
1743:lib/lib_pic33e/usb/usb_device.c ****             USBStatusStageEnabledFlag1 = false;
 2103              	.loc 1 1743 0
 2104 000b34  00 60 EF 	clr.b _USBStatusStageEnabledFlag1
1744:lib/lib_pic33e/usb/usb_device.c **** 			if(USBDeferStatusStagePacket == false)
 2105              	.loc 1 1744 0
 2106 000b36  00 C0 BF 	mov.b _USBDeferStatusStagePacket,WREG
MPLAB XC16 ASSEMBLY Listing:   			page 69


 2107 000b38  00 04 A2 	btg.b w0,#0
 2108 000b3a  00 04 E0 	cp0.b w0
 2109              	.set ___BP___,0
 2110 000b3c  00 00 32 	bra z,.L60
1745:lib/lib_pic33e/usb/usb_device.c ****             {
1746:lib/lib_pic33e/usb/usb_device.c ****                 USBCtrlEPAllowStatusStage();
 2111              	.loc 1 1746 0
 2112 000b3e  00 00 07 	rcall _USBCtrlEPAllowStatusStage
 2113              	.L60:
1747:lib/lib_pic33e/usb/usb_device.c ****             } 
1748:lib/lib_pic33e/usb/usb_device.c **** 		}
1749:lib/lib_pic33e/usb/usb_device.c **** 
1750:lib/lib_pic33e/usb/usb_device.c ****     }//end if(ctrl_trf_session_owner == MUID_NULL)
1751:lib/lib_pic33e/usb/usb_device.c **** 
1752:lib/lib_pic33e/usb/usb_device.c **** }//end USBCtrlEPServiceComplete
 2114              	.loc 1 1752 0
 2115 000b40  8E 07 78 	mov w14,w15
 2116 000b42  4F 07 78 	mov [--w15],w14
 2117 000b44  00 40 A9 	bclr CORCON,#2
 2118 000b46  00 00 06 	return 
 2119              	.set ___PA___,0
 2120              	.LFE11:
 2121              	.size _USBCtrlEPServiceComplete,.-_USBCtrlEPServiceComplete
 2122              	.align 2
 2123              	.type _USBCtrlTrfTxService,@function
 2124              	_USBCtrlTrfTxService:
 2125              	.LFB12:
1753:lib/lib_pic33e/usb/usb_device.c **** 
1754:lib/lib_pic33e/usb/usb_device.c **** 
1755:lib/lib_pic33e/usb/usb_device.c **** /******************************************************************************
1756:lib/lib_pic33e/usb/usb_device.c ****  * Function:        void USBCtrlTrfTxService(void)
1757:lib/lib_pic33e/usb/usb_device.c ****  *
1758:lib/lib_pic33e/usb/usb_device.c ****  * PreCondition:    pSrc, wCount, and usb_stat.ctrl_trf_mem are setup properly.
1759:lib/lib_pic33e/usb/usb_device.c ****  *
1760:lib/lib_pic33e/usb/usb_device.c ****  * Input:           None
1761:lib/lib_pic33e/usb/usb_device.c ****  *
1762:lib/lib_pic33e/usb/usb_device.c ****  * Output:          None
1763:lib/lib_pic33e/usb/usb_device.c ****  *
1764:lib/lib_pic33e/usb/usb_device.c ****  * Side Effects:    None
1765:lib/lib_pic33e/usb/usb_device.c ****  *
1766:lib/lib_pic33e/usb/usb_device.c ****  * Overview:        This routine is used for device to host control transfers 
1767:lib/lib_pic33e/usb/usb_device.c ****  *					(IN transactions).  This function takes care of managing a
1768:lib/lib_pic33e/usb/usb_device.c ****  *                  transfer over multiple USB transactions.
1769:lib/lib_pic33e/usb/usb_device.c ****  *					This routine should be called from only two places.
1770:lib/lib_pic33e/usb/usb_device.c ****  *                  One from USBCtrlEPServiceComplete() and one from
1771:lib/lib_pic33e/usb/usb_device.c ****  *                  USBCtrlTrfInHandler().
1772:lib/lib_pic33e/usb/usb_device.c ****  *
1773:lib/lib_pic33e/usb/usb_device.c ****  * Note:            
1774:lib/lib_pic33e/usb/usb_device.c ****  *****************************************************************************/
1775:lib/lib_pic33e/usb/usb_device.c **** static void USBCtrlTrfTxService(void)
1776:lib/lib_pic33e/usb/usb_device.c **** {
 2126              	.loc 1 1776 0
 2127              	.set ___PA___,1
 2128 000b48  02 00 FA 	lnk #2
 2129              	.LCFI15:
1777:lib/lib_pic33e/usb/usb_device.c ****     uint8_t byteToSend;
1778:lib/lib_pic33e/usb/usb_device.c **** 
MPLAB XC16 ASSEMBLY Listing:   			page 70


1779:lib/lib_pic33e/usb/usb_device.c ****     //Figure out how many bytes of data to send in the next IN transaction.
1780:lib/lib_pic33e/usb/usb_device.c ****     //Assume a full size packet, unless otherwise determined below.
1781:lib/lib_pic33e/usb/usb_device.c ****     byteToSend = USB_EP0_BUFF_SIZE;         
 2130              	.loc 1 1781 0
 2131 000b4a  81 C0 B3 	mov.b #8,w1
1782:lib/lib_pic33e/usb/usb_device.c ****     if(inPipes[0].wCount.Val < (uint8_t)USB_EP0_BUFF_SIZE)
 2132              	.loc 1 1782 0
 2133 000b4c  20 00 80 	mov _inPipes+4,w0
 2134              	.loc 1 1781 0
 2135 000b4e  01 4F 78 	mov.b w1,[w14]
 2136              	.loc 1 1782 0
 2137 000b50  E7 0F 50 	sub w0,#7,[w15]
 2138              	.set ___BP___,0
 2139 000b52  00 00 3E 	bra gtu,.L68
1783:lib/lib_pic33e/usb/usb_device.c ****     {
1784:lib/lib_pic33e/usb/usb_device.c ****         byteToSend = inPipes[0].wCount.Val;
 2140              	.loc 1 1784 0
 2141 000b54  21 00 80 	mov _inPipes+4,w1
1785:lib/lib_pic33e/usb/usb_device.c **** 
1786:lib/lib_pic33e/usb/usb_device.c ****         //Keep track of whether or not we have sent a "short packet" yet.
1787:lib/lib_pic33e/usb/usb_device.c ****         //This is useful so that later on, we can configure EP0 IN to STALL,
1788:lib/lib_pic33e/usb/usb_device.c ****         //after we have sent all of the intended data.  This makes sure the
1789:lib/lib_pic33e/usb/usb_device.c ****         //hardware STALLs if the host erroneously tries to send more IN token 
1790:lib/lib_pic33e/usb/usb_device.c ****         //packets, requesting more data than intended in the control transfer.
1791:lib/lib_pic33e/usb/usb_device.c ****         if(shortPacketStatus == SHORT_PKT_NOT_USED)
 2142              	.loc 1 1791 0
 2143 000b56  00 C0 BF 	mov.b _shortPacketStatus,WREG
 2144              	.loc 1 1784 0
 2145 000b58  01 4F 78 	mov.b w1,[w14]
 2146              	.loc 1 1791 0
 2147 000b5a  00 04 E0 	cp0.b w0
 2148              	.set ___BP___,0
 2149 000b5c  00 00 3A 	bra nz,.L69
1792:lib/lib_pic33e/usb/usb_device.c ****         {
1793:lib/lib_pic33e/usb/usb_device.c ****             shortPacketStatus = SHORT_PKT_PENDING;
 2150              	.loc 1 1793 0
 2151 000b5e  10 C0 B3 	mov.b #1,w0
 2152 000b60  00 E0 B7 	mov.b WREG,_shortPacketStatus
 2153 000b62  00 00 37 	bra .L68
 2154              	.L69:
1794:lib/lib_pic33e/usb/usb_device.c ****         }
1795:lib/lib_pic33e/usb/usb_device.c ****         else if(shortPacketStatus == SHORT_PKT_PENDING)
 2155              	.loc 1 1795 0
 2156 000b64  00 C0 BF 	mov.b _shortPacketStatus,WREG
 2157 000b66  E1 4F 50 	sub.b w0,#1,[w15]
 2158              	.set ___BP___,0
 2159 000b68  00 00 3A 	bra nz,.L68
1796:lib/lib_pic33e/usb/usb_device.c ****         {
1797:lib/lib_pic33e/usb/usb_device.c ****             shortPacketStatus = SHORT_PKT_SENT;
 2160              	.loc 1 1797 0
 2161 000b6a  20 C0 B3 	mov.b #2,w0
 2162 000b6c  00 E0 B7 	mov.b WREG,_shortPacketStatus
 2163              	.L68:
1798:lib/lib_pic33e/usb/usb_device.c ****         }
1799:lib/lib_pic33e/usb/usb_device.c ****     }
1800:lib/lib_pic33e/usb/usb_device.c **** 
1801:lib/lib_pic33e/usb/usb_device.c ****     //Keep track of how many bytes remain to be sent in the transfer, by
MPLAB XC16 ASSEMBLY Listing:   			page 71


1802:lib/lib_pic33e/usb/usb_device.c ****     //subtracting the number of bytes about to be sent from the total.
1803:lib/lib_pic33e/usb/usb_device.c ****     inPipes[0].wCount.Val -= byteToSend;
1804:lib/lib_pic33e/usb/usb_device.c ****     
1805:lib/lib_pic33e/usb/usb_device.c ****     //Next, load the number of bytes to send to BC7..0 in buffer descriptor.
1806:lib/lib_pic33e/usb/usb_device.c ****     //Note: Control endpoints may never have a max packet size of > 64 bytes.
1807:lib/lib_pic33e/usb/usb_device.c ****     //Therefore, the BC8 and BC9 bits should always be maintained clear.
1808:lib/lib_pic33e/usb/usb_device.c ****     pBDTEntryIn[0]->CNT = byteToSend;
 2164              	.loc 1 1808 0
 2165 000b6e  00 00 80 	mov _pBDTEntryIn,w0
 2166              	.loc 1 1803 0
 2167 000b70  23 00 80 	mov _inPipes+4,w3
 2168 000b72  1E 81 FB 	ze [w14],w2
 2169              	.loc 1 1808 0
 2170 000b74  9E 80 FB 	ze [w14],w1
 2171              	.loc 1 1803 0
 2172 000b76  82 81 51 	sub w3,w2,w3
 2173              	.loc 1 1808 0
 2174 000b78  F2 3F 20 	mov #1023,w2
 2175              	.loc 1 1803 0
 2176 000b7a  23 00 88 	mov w3,_inPipes+4
 2177              	.loc 1 1808 0
 2178 000b7c  82 80 60 	and w1,w2,w1
 2179 000b7e  A0 41 90 	mov.b [w0+2],w3
 2180 000b80  F2 0F 20 	mov #255,w2
 2181 000b82  E0 C1 61 	and.b w3,#0,w3
 2182 000b84  02 81 60 	and w1,w2,w2
 2183 000b86  C8 08 DE 	lsr w1,#8,w1
 2184 000b88  02 C1 71 	ior.b w3,w2,w2
 2185 000b8a  E3 80 60 	and w1,#3,w1
 2186 000b8c  22 40 98 	mov.b w2,[w0+2]
 2187 000b8e  63 C1 60 	and.b w1,#3,w2
 2188 000b90  30 42 90 	mov.b [w0+3],w4
 2189 000b92  C3 CF B3 	mov.b #-4,w3
1809:lib/lib_pic33e/usb/usb_device.c **** 
1810:lib/lib_pic33e/usb/usb_device.c ****     //Now copy the data from the source location, to the CtrlTrfData[] buffer,
1811:lib/lib_pic33e/usb/usb_device.c ****     //which we will send to the host.
1812:lib/lib_pic33e/usb/usb_device.c ****     pDst = (USB_VOLATILE uint8_t*)CtrlTrfData;                // Set destination pointer
 2190              	.loc 1 1812 0
 2191 000b94  01 00 20 	mov #_CtrlTrfData,w1
 2192              	.loc 1 1808 0
 2193 000b96  83 41 62 	and.b w4,w3,w3
 2194 000b98  02 C1 71 	ior.b w3,w2,w2
 2195 000b9a  32 40 98 	mov.b w2,[w0+3]
 2196              	.loc 1 1812 0
 2197 000b9c  01 00 88 	mov w1,_pDst
1813:lib/lib_pic33e/usb/usb_device.c ****     if(inPipes[0].info.bits.ctrl_trf_mem == USB_EP0_ROM)   // Determine type of memory source
 2198              	.loc 1 1813 0
 2199 000b9e  10 00 80 	mov _inPipes+2,w0
 2200 000ba0  61 00 60 	and w0,#1,w0
 2201 000ba2  00 00 E0 	cp0 w0
 2202              	.set ___BP___,0
 2203 000ba4  00 00 3A 	bra nz,.L74
1814:lib/lib_pic33e/usb/usb_device.c ****     {
1815:lib/lib_pic33e/usb/usb_device.c ****         while(byteToSend)
 2204              	.loc 1 1815 0
 2205 000ba6  00 00 37 	bra .L71
 2206              	.L72:
MPLAB XC16 ASSEMBLY Listing:   			page 72


1816:lib/lib_pic33e/usb/usb_device.c ****         {
1817:lib/lib_pic33e/usb/usb_device.c ****             *pDst++ = *inPipes[0].pSrc.bRom++;
 2207              	.loc 1 1817 0
 2208 000ba8  00 00 80 	mov _inPipes,w0
 2209 000baa  01 00 80 	mov _pDst,w1
 2210 000bac  10 41 78 	mov.b [w0],w2
 2211 000bae  82 48 78 	mov.b w2,[w1]
1818:lib/lib_pic33e/usb/usb_device.c ****             byteToSend--;
 2212              	.loc 1 1818 0
 2213 000bb0  1E 4F E9 	dec.b [w14],[w14]
 2214              	.loc 1 1817 0
 2215 000bb2  81 00 E8 	inc w1,w1
 2216 000bb4  00 00 E8 	inc w0,w0
 2217 000bb6  01 00 88 	mov w1,_pDst
 2218 000bb8  00 00 88 	mov w0,_inPipes
 2219              	.L71:
 2220              	.loc 1 1815 0
 2221 000bba  1E 40 78 	mov.b [w14],w0
 2222 000bbc  00 04 E0 	cp0.b w0
 2223              	.set ___BP___,0
 2224 000bbe  00 00 3A 	bra nz,.L72
 2225 000bc0  00 00 37 	bra .L67
 2226              	.L75:
1819:lib/lib_pic33e/usb/usb_device.c ****         }//end while(byte_to_send.Val)
1820:lib/lib_pic33e/usb/usb_device.c ****     }
1821:lib/lib_pic33e/usb/usb_device.c ****     else  // RAM
1822:lib/lib_pic33e/usb/usb_device.c ****     {
1823:lib/lib_pic33e/usb/usb_device.c ****         while(byteToSend)
1824:lib/lib_pic33e/usb/usb_device.c ****         {
1825:lib/lib_pic33e/usb/usb_device.c ****             *pDst++ = *inPipes[0].pSrc.bRam++;
 2227              	.loc 1 1825 0
 2228 000bc2  00 00 80 	mov _inPipes,w0
 2229 000bc4  01 00 80 	mov _pDst,w1
 2230 000bc6  10 41 78 	mov.b [w0],w2
 2231 000bc8  82 48 78 	mov.b w2,[w1]
1826:lib/lib_pic33e/usb/usb_device.c ****             byteToSend--;
 2232              	.loc 1 1826 0
 2233 000bca  1E 4F E9 	dec.b [w14],[w14]
 2234              	.loc 1 1825 0
 2235 000bcc  81 00 E8 	inc w1,w1
 2236 000bce  00 00 E8 	inc w0,w0
 2237 000bd0  01 00 88 	mov w1,_pDst
 2238 000bd2  00 00 88 	mov w0,_inPipes
 2239              	.L74:
 2240              	.loc 1 1823 0
 2241 000bd4  1E 40 78 	mov.b [w14],w0
 2242 000bd6  00 04 E0 	cp0.b w0
 2243              	.set ___BP___,0
 2244 000bd8  00 00 3A 	bra nz,.L75
 2245              	.L67:
1827:lib/lib_pic33e/usb/usb_device.c ****         }//end while(byte_to_send.Val)
1828:lib/lib_pic33e/usb/usb_device.c ****     }//end if(usb_stat.ctrl_trf_mem == _const)
1829:lib/lib_pic33e/usb/usb_device.c **** }//end USBCtrlTrfTxService
 2246              	.loc 1 1829 0
 2247 000bda  8E 07 78 	mov w14,w15
 2248 000bdc  4F 07 78 	mov [--w15],w14
 2249 000bde  00 40 A9 	bclr CORCON,#2
MPLAB XC16 ASSEMBLY Listing:   			page 73


 2250 000be0  00 00 06 	return 
 2251              	.set ___PA___,0
 2252              	.LFE12:
 2253              	.size _USBCtrlTrfTxService,.-_USBCtrlTrfTxService
 2254              	.align 2
 2255              	.type _USBCtrlTrfRxService,@function
 2256              	_USBCtrlTrfRxService:
 2257              	.LFB13:
1830:lib/lib_pic33e/usb/usb_device.c **** 
1831:lib/lib_pic33e/usb/usb_device.c **** /******************************************************************************
1832:lib/lib_pic33e/usb/usb_device.c ****  * Function:        void USBCtrlTrfRxService(void)
1833:lib/lib_pic33e/usb/usb_device.c ****  *
1834:lib/lib_pic33e/usb/usb_device.c ****  * PreCondition:    pDst and wCount are setup properly.
1835:lib/lib_pic33e/usb/usb_device.c ****  *                  pSrc is always &CtrlTrfData
1836:lib/lib_pic33e/usb/usb_device.c ****  *                  usb_stat.ctrl_trf_mem is always USB_EP0_RAM.
1837:lib/lib_pic33e/usb/usb_device.c ****  *                  wCount should be set to 0 at the start of each control
1838:lib/lib_pic33e/usb/usb_device.c ****  *                  transfer.
1839:lib/lib_pic33e/usb/usb_device.c ****  *
1840:lib/lib_pic33e/usb/usb_device.c ****  * Input:           None
1841:lib/lib_pic33e/usb/usb_device.c ****  *
1842:lib/lib_pic33e/usb/usb_device.c ****  * Output:          None
1843:lib/lib_pic33e/usb/usb_device.c ****  *
1844:lib/lib_pic33e/usb/usb_device.c ****  * Side Effects:    None
1845:lib/lib_pic33e/usb/usb_device.c ****  *
1846:lib/lib_pic33e/usb/usb_device.c ****  * Overview:        This routine is used for host to device control transfers
1847:lib/lib_pic33e/usb/usb_device.c ****  *					(uses OUT transactions).  This function receives the data that arrives
1848:lib/lib_pic33e/usb/usb_device.c ****  *					on EP0 OUT, and copies it into the appropriate outPipes[0].pDst.bRam
1849:lib/lib_pic33e/usb/usb_device.c ****  *					buffer.  Once the host has sent all the data it was intending
1850:lib/lib_pic33e/usb/usb_device.c ****  *					to send, this function will call the appropriate outPipes[0].pFunc()
1851:lib/lib_pic33e/usb/usb_device.c ****  *					handler (unless it is NULL), so that it can be used by the
1852:lib/lib_pic33e/usb/usb_device.c ****  *					intended target firmware.
1853:lib/lib_pic33e/usb/usb_device.c ****  *
1854:lib/lib_pic33e/usb/usb_device.c ****  * Note:            None
1855:lib/lib_pic33e/usb/usb_device.c ****  *****************************************************************************/
1856:lib/lib_pic33e/usb/usb_device.c **** static void USBCtrlTrfRxService(void)
1857:lib/lib_pic33e/usb/usb_device.c **** {
 2258              	.loc 1 1857 0
 2259              	.set ___PA___,1
 2260 000be2  02 00 FA 	lnk #2
 2261              	.LCFI16:
1858:lib/lib_pic33e/usb/usb_device.c ****     uint8_t byteToRead;
1859:lib/lib_pic33e/usb/usb_device.c ****     uint8_t i;
1860:lib/lib_pic33e/usb/usb_device.c **** 
1861:lib/lib_pic33e/usb/usb_device.c ****     //Load byteToRead with the number of bytes the host just sent us in the 
1862:lib/lib_pic33e/usb/usb_device.c ****     //last OUT transaction.
1863:lib/lib_pic33e/usb/usb_device.c ****     byteToRead = pBDTEntryEP0OutCurrent->CNT;   
 2262              	.loc 1 1863 0
 2263 000be4  00 00 80 	mov _pBDTEntryEP0OutCurrent,w0
1864:lib/lib_pic33e/usb/usb_device.c **** 
1865:lib/lib_pic33e/usb/usb_device.c ****     //Update the "outPipes[0].wCount.Val", which keeps track of the total number
1866:lib/lib_pic33e/usb/usb_device.c ****     //of remaining bytes expected to be received from the host, in the control
1867:lib/lib_pic33e/usb/usb_device.c ****     //transfer.  First check to see if the host sent us more bytes than the
1868:lib/lib_pic33e/usb/usb_device.c ****     //application firmware was expecting to receive.
1869:lib/lib_pic33e/usb/usb_device.c ****     if(byteToRead > outPipes[0].wCount.Val)
 2264              	.loc 1 1869 0
 2265 000be6  31 00 20 	mov #_outPipes+3,w1
 2266              	.loc 1 1863 0
MPLAB XC16 ASSEMBLY Listing:   			page 74


 2267 000be8  20 42 90 	mov.b [w0+2],w4
 2268              	.loc 1 1869 0
 2269 000bea  42 00 20 	mov #_outPipes+4,w2
 2270              	.loc 1 1863 0
 2271 000bec  B0 41 90 	mov.b [w0+3],w3
 2272 000bee  04 80 FB 	ze w4,w0
 2273 000bf0  83 81 FB 	ze w3,w3
 2274 000bf2  E3 81 61 	and w3,#3,w3
 2275 000bf4  C8 19 DD 	sl w3,#8,w3
 2276 000bf6  03 00 70 	ior w0,w3,w0
 2277 000bf8  00 4F 78 	mov.b w0,[w14]
 2278              	.loc 1 1869 0
 2279 000bfa  11 80 FB 	ze [w1],w0
 2280 000bfc  9E 80 FB 	ze [w14],w1
 2281 000bfe  12 81 FB 	ze [w2],w2
 2282 000c00  48 11 DD 	sl w2,#8,w2
 2283 000c02  02 00 70 	ior w0,w2,w0
 2284 000c04  80 8F 50 	sub w1,w0,[w15]
 2285              	.set ___BP___,0
 2286 000c06  00 00 36 	bra leu,.L77
1870:lib/lib_pic33e/usb/usb_device.c ****     {
1871:lib/lib_pic33e/usb/usb_device.c ****         byteToRead = outPipes[0].wCount.Val;
 2287              	.loc 1 1871 0
 2288 000c08  30 00 20 	mov #_outPipes+3,w0
 2289 000c0a  41 00 20 	mov #_outPipes+4,w1
 2290 000c0c  10 80 FB 	ze [w0],w0
 2291 000c0e  91 80 FB 	ze [w1],w1
 2292 000c10  C8 08 DD 	sl w1,#8,w1
 2293 000c12  01 00 70 	ior w0,w1,w0
 2294 000c14  00 4F 78 	mov.b w0,[w14]
 2295              	.L77:
1872:lib/lib_pic33e/usb/usb_device.c ****     }	
1873:lib/lib_pic33e/usb/usb_device.c ****     //Reduce the number of remaining bytes by the number we just received.
1874:lib/lib_pic33e/usb/usb_device.c ****     outPipes[0].wCount.Val -= byteToRead;
 2296              	.loc 1 1874 0
 2297 000c16  31 00 20 	mov #_outPipes+3,w1
 2298 000c18  40 00 20 	mov #_outPipes+4,w0
 2299 000c1a  32 00 20 	mov #_outPipes+3,w2
 2300 000c1c  43 00 20 	mov #_outPipes+4,w3
 2301 000c1e  92 82 FB 	ze [w2],w5
 2302 000c20  1E 81 FB 	ze [w14],w2
 2303 000c22  13 83 FB 	ze [w3],w6
 2304 000c24  F3 0F 20 	mov #255,w3
 2305 000c26  11 42 78 	mov.b [w1],w4
 2306 000c28  48 33 DD 	sl w6,#8,w6
 2307 000c2a  60 42 62 	and.b w4,#0,w4
 2308 000c2c  86 82 72 	ior w5,w6,w5
1875:lib/lib_pic33e/usb/usb_device.c **** 
1876:lib/lib_pic33e/usb/usb_device.c ****     //Copy the OUT DATAx packet bytes that we just received from the host,
1877:lib/lib_pic33e/usb/usb_device.c ****     //into the user application buffer space.
1878:lib/lib_pic33e/usb/usb_device.c ****     for(i=0;i<byteToRead;i++)
 2309              	.loc 1 1878 0
 2310 000c2e  00 43 EB 	clr.b w6
 2311 000c30  16 47 98 	mov.b w6,[w14+1]
 2312              	.loc 1 1874 0
 2313 000c32  02 81 52 	sub w5,w2,w2
 2314 000c34  83 01 61 	and w2,w3,w3
MPLAB XC16 ASSEMBLY Listing:   			page 75


 2315 000c36  48 11 DE 	lsr w2,#8,w2
 2316 000c38  83 41 72 	ior.b w4,w3,w3
 2317 000c3a  83 48 78 	mov.b w3,[w1]
 2318 000c3c  90 40 78 	mov.b [w0],w1
 2319 000c3e  E0 C0 60 	and.b w1,#0,w1
 2320 000c40  82 C0 70 	ior.b w1,w2,w1
 2321 000c42  01 48 78 	mov.b w1,[w0]
 2322              	.loc 1 1878 0
 2323 000c44  00 00 37 	bra .L78
 2324              	.L79:
1879:lib/lib_pic33e/usb/usb_device.c ****     {
1880:lib/lib_pic33e/usb/usb_device.c ****         *outPipes[0].pDst.bRam++ = CtrlTrfData[i];
 2325              	.loc 1 1880 0
 2326 000c46  00 00 00 	nop 
 2327 000c48  1E 40 90 	mov.b [w14+1],w0
 2328 000c4a  02 00 20 	mov #_outPipes,w2
 2329 000c4c  14 00 20 	mov #_outPipes+1,w4
 2330 000c4e  00 80 FB 	ze w0,w0
 2331 000c50  01 00 20 	mov #_CtrlTrfData,w1
 2332 000c52  80 81 40 	add w1,w0,w3
 2333 000c54  01 00 20 	mov #_outPipes,w1
 2334 000c56  10 00 20 	mov #_outPipes+1,w0
 2335 000c58  12 81 FB 	ze [w2],w2
 2336 000c5a  14 82 FB 	ze [w4],w4
 2337 000c5c  93 41 78 	mov.b [w3],w3
 2338 000c5e  48 22 DD 	sl w4,#8,w4
 2339 000c60  04 01 71 	ior w2,w4,w2
 2340 000c62  03 49 78 	mov.b w3,[w2]
 2341              	.loc 1 1878 0
 2342 000c64  9E 41 90 	mov.b [w14+1],w3
 2343 000c66  83 41 E8 	inc.b w3,w3
 2344 000c68  13 47 98 	mov.b w3,[w14+1]
 2345              	.loc 1 1880 0
 2346 000c6a  11 42 78 	mov.b [w1],w4
 2347 000c6c  02 01 E8 	inc w2,w2
 2348 000c6e  F3 0F 20 	mov #255,w3
 2349 000c70  60 42 62 	and.b w4,#0,w4
 2350 000c72  83 01 61 	and w2,w3,w3
 2351 000c74  48 11 DE 	lsr w2,#8,w2
 2352 000c76  83 41 72 	ior.b w4,w3,w3
 2353 000c78  83 48 78 	mov.b w3,[w1]
 2354 000c7a  90 40 78 	mov.b [w0],w1
 2355 000c7c  E0 C0 60 	and.b w1,#0,w1
 2356 000c7e  82 C0 70 	ior.b w1,w2,w1
 2357 000c80  01 48 78 	mov.b w1,[w0]
 2358              	.L78:
 2359              	.loc 1 1878 0
 2360 000c82  00 00 00 	nop 
 2361 000c84  1E 40 90 	mov.b [w14+1],w0
 2362 000c86  00 00 00 	nop 
 2363 000c88  9E 4F 50 	sub.b w0,[w14],[w15]
 2364              	.set ___BP___,0
 2365 000c8a  00 00 39 	bra ltu,.L79
1881:lib/lib_pic33e/usb/usb_device.c ****     }//end while(byteToRead.Val)
1882:lib/lib_pic33e/usb/usb_device.c **** 
1883:lib/lib_pic33e/usb/usb_device.c ****     //If there is more data to receive, prepare EP0 OUT so that it can receive 
1884:lib/lib_pic33e/usb/usb_device.c **** 	//the next packet in the sequence.
MPLAB XC16 ASSEMBLY Listing:   			page 76


1885:lib/lib_pic33e/usb/usb_device.c ****     if(outPipes[0].wCount.Val > 0)
 2366              	.loc 1 1885 0
 2367 000c8c  30 00 20 	mov #_outPipes+3,w0
 2368 000c8e  41 00 20 	mov #_outPipes+4,w1
 2369 000c90  10 80 FB 	ze [w0],w0
 2370 000c92  91 80 FB 	ze [w1],w1
 2371 000c94  C8 08 DD 	sl w1,#8,w1
 2372 000c96  01 00 70 	ior w0,w1,w0
 2373 000c98  00 00 E0 	cp0 w0
 2374              	.set ___BP___,0
 2375 000c9a  00 00 32 	bra z,.L80
1886:lib/lib_pic33e/usb/usb_device.c ****     {
1887:lib/lib_pic33e/usb/usb_device.c ****         pBDTEntryEP0OutNext->CNT = USB_EP0_BUFF_SIZE;
 2376              	.loc 1 1887 0
 2377 000c9c  00 00 80 	mov _pBDTEntryEP0OutNext,w0
 2378 000c9e  C1 CF B3 	mov.b #-4,w1
 2379 000ca0  20 41 90 	mov.b [w0+2],w2
 2380 000ca2  60 41 61 	and.b w2,#0,w2
 2381 000ca4  02 34 A0 	bset.b w2,#3
 2382 000ca6  22 40 98 	mov.b w2,[w0+2]
 2383 000ca8  30 41 90 	mov.b [w0+3],w2
 2384 000caa  81 40 61 	and.b w2,w1,w1
 2385 000cac  31 40 98 	mov.b w1,[w0+3]
1888:lib/lib_pic33e/usb/usb_device.c ****         pBDTEntryEP0OutNext->ADR = ConvertToPhysicalAddress(&CtrlTrfData);
 2386              	.loc 1 1888 0
 2387 000cae  00 00 80 	mov _pBDTEntryEP0OutNext,w0
 2388 000cb0  01 00 20 	mov #_CtrlTrfData,w1
 2389 000cb2  C0 41 90 	mov.b [w0+4],w3
 2390 000cb4  F2 0F 20 	mov #255,w2
 2391 000cb6  E0 C1 61 	and.b w3,#0,w3
 2392 000cb8  02 81 60 	and w1,w2,w2
 2393 000cba  C8 08 DE 	lsr w1,#8,w1
 2394 000cbc  02 C1 71 	ior.b w3,w2,w2
 2395 000cbe  42 40 98 	mov.b w2,[w0+4]
 2396 000cc0  50 41 90 	mov.b [w0+5],w2
 2397 000cc2  60 41 61 	and.b w2,#0,w2
 2398 000cc4  81 40 71 	ior.b w2,w1,w1
 2399 000cc6  51 40 98 	mov.b w1,[w0+5]
1889:lib/lib_pic33e/usb/usb_device.c ****         if(pBDTEntryEP0OutCurrent->STAT.DTS == 0)
 2400              	.loc 1 1889 0
 2401 000cc8  00 00 80 	mov _pBDTEntryEP0OutCurrent,w0
 2402 000cca  10 40 78 	mov.b [w0],w0
 2403 000ccc  00 44 B2 	and.b #64,w0
 2404 000cce  00 04 E0 	cp0.b w0
 2405              	.set ___BP___,0
 2406 000cd0  00 00 3A 	bra nz,.L81
1890:lib/lib_pic33e/usb/usb_device.c ****         {
1891:lib/lib_pic33e/usb/usb_device.c ****             pBDTEntryEP0OutNext->STAT.Val = _DAT1|(_DTSEN & _DTS_CHECKING_ENABLED);
 2407              	.loc 1 1891 0
 2408 000cd2  00 00 80 	mov _pBDTEntryEP0OutNext,w0
 2409 000cd4  81 C4 B3 	mov.b #72,w1
 2410 000cd6  10 41 78 	mov.b [w0],w2
 2411 000cd8  60 41 61 	and.b w2,#0,w2
 2412 000cda  81 40 71 	ior.b w2,w1,w1
 2413 000cdc  01 48 78 	mov.b w1,[w0]
1892:lib/lib_pic33e/usb/usb_device.c ****             pBDTEntryEP0OutNext->STAT.Val |= _USIE;
 2414              	.loc 1 1892 0
MPLAB XC16 ASSEMBLY Listing:   			page 77


 2415 000cde  F2 0F 20 	mov #255,w2
 2416              	.loc 1 1891 0
 2417 000ce0  90 40 90 	mov.b [w0+1],w1
 2418 000ce2  E0 C0 60 	and.b w1,#0,w1
 2419 000ce4  11 40 98 	mov.b w1,[w0+1]
 2420              	.loc 1 1892 0
 2421 000ce6  00 00 80 	mov _pBDTEntryEP0OutNext,w0
 2422 000ce8  01 00 80 	mov _pBDTEntryEP0OutNext,w1
 2423 000cea  11 82 FB 	ze [w1],w4
 2424 000cec  00 00 00 	nop 
 2425 000cee  91 40 90 	mov.b [w1+1],w1
 2426 000cf0  90 41 78 	mov.b [w0],w3
 2427 000cf2  81 80 FB 	ze w1,w1
 2428 000cf4  E0 C1 61 	and.b w3,#0,w3
 2429 000cf6  C8 08 DD 	sl w1,#8,w1
 2430 000cf8  81 00 72 	ior w4,w1,w1
 2431 000cfa  01 70 A0 	bset w1,#7
 2432 000cfc  02 81 60 	and w1,w2,w2
 2433 000cfe  02 C1 71 	ior.b w3,w2,w2
 2434 000d00  02 48 78 	mov.b w2,[w0]
 2435 000d02  C8 08 DE 	lsr w1,#8,w1
 2436 000d04  10 41 90 	mov.b [w0+1],w2
 2437 000d06  60 41 61 	and.b w2,#0,w2
 2438 000d08  81 40 71 	ior.b w2,w1,w1
 2439 000d0a  11 40 98 	mov.b w1,[w0+1]
 2440 000d0c  00 00 37 	bra .L76
 2441              	.L81:
1893:lib/lib_pic33e/usb/usb_device.c ****         }
1894:lib/lib_pic33e/usb/usb_device.c ****         else
1895:lib/lib_pic33e/usb/usb_device.c ****         {
1896:lib/lib_pic33e/usb/usb_device.c ****             pBDTEntryEP0OutNext->STAT.Val = _DAT0|(_DTSEN & _DTS_CHECKING_ENABLED);
 2442              	.loc 1 1896 0
 2443 000d0e  00 00 80 	mov _pBDTEntryEP0OutNext,w0
1897:lib/lib_pic33e/usb/usb_device.c ****             pBDTEntryEP0OutNext->STAT.Val |= _USIE;
 2444              	.loc 1 1897 0
 2445 000d10  F2 0F 20 	mov #255,w2
 2446              	.loc 1 1896 0
 2447 000d12  90 40 78 	mov.b [w0],w1
 2448 000d14  E0 C0 60 	and.b w1,#0,w1
 2449 000d16  01 34 A0 	bset.b w1,#3
 2450 000d18  01 48 78 	mov.b w1,[w0]
 2451 000d1a  90 40 90 	mov.b [w0+1],w1
 2452 000d1c  E0 C0 60 	and.b w1,#0,w1
 2453 000d1e  11 40 98 	mov.b w1,[w0+1]
 2454              	.loc 1 1897 0
 2455 000d20  00 00 80 	mov _pBDTEntryEP0OutNext,w0
 2456 000d22  01 00 80 	mov _pBDTEntryEP0OutNext,w1
 2457 000d24  11 82 FB 	ze [w1],w4
 2458 000d26  00 00 00 	nop 
 2459 000d28  91 40 90 	mov.b [w1+1],w1
 2460 000d2a  90 41 78 	mov.b [w0],w3
 2461 000d2c  81 80 FB 	ze w1,w1
 2462 000d2e  E0 C1 61 	and.b w3,#0,w3
 2463 000d30  C8 08 DD 	sl w1,#8,w1
 2464 000d32  81 00 72 	ior w4,w1,w1
 2465 000d34  01 70 A0 	bset w1,#7
 2466 000d36  02 81 60 	and w1,w2,w2
MPLAB XC16 ASSEMBLY Listing:   			page 78


 2467 000d38  02 C1 71 	ior.b w3,w2,w2
 2468 000d3a  02 48 78 	mov.b w2,[w0]
 2469 000d3c  C8 08 DE 	lsr w1,#8,w1
 2470 000d3e  10 41 90 	mov.b [w0+1],w2
 2471 000d40  60 41 61 	and.b w2,#0,w2
 2472 000d42  81 40 71 	ior.b w2,w1,w1
 2473 000d44  11 40 98 	mov.b w1,[w0+1]
 2474 000d46  00 00 37 	bra .L76
 2475              	.L80:
1898:lib/lib_pic33e/usb/usb_device.c ****         }
1899:lib/lib_pic33e/usb/usb_device.c ****     }
1900:lib/lib_pic33e/usb/usb_device.c ****     else
1901:lib/lib_pic33e/usb/usb_device.c ****     {
1902:lib/lib_pic33e/usb/usb_device.c **** 	    //We have received all OUT packets that we were expecting to
1903:lib/lib_pic33e/usb/usb_device.c **** 	    //receive for the control transfer.  Prepare EP0 OUT to receive
1904:lib/lib_pic33e/usb/usb_device.c **** 		//the next SETUP transaction that may arrive.
1905:lib/lib_pic33e/usb/usb_device.c ****         pBDTEntryEP0OutNext->CNT = USB_EP0_BUFF_SIZE;
 2476              	.loc 1 1905 0
 2477 000d48  00 00 80 	mov _pBDTEntryEP0OutNext,w0
 2478 000d4a  C1 CF B3 	mov.b #-4,w1
 2479 000d4c  20 41 90 	mov.b [w0+2],w2
 2480 000d4e  60 41 61 	and.b w2,#0,w2
 2481 000d50  02 34 A0 	bset.b w2,#3
 2482 000d52  22 40 98 	mov.b w2,[w0+2]
 2483 000d54  30 41 90 	mov.b [w0+3],w2
 2484 000d56  81 40 61 	and.b w2,w1,w1
 2485 000d58  31 40 98 	mov.b w1,[w0+3]
1906:lib/lib_pic33e/usb/usb_device.c ****         pBDTEntryEP0OutNext->ADR = ConvertToPhysicalAddress(&SetupPkt);
 2486              	.loc 1 1906 0
 2487 000d5a  00 00 80 	mov _pBDTEntryEP0OutNext,w0
 2488 000d5c  01 00 20 	mov #_SetupPkt,w1
 2489 000d5e  C0 41 90 	mov.b [w0+4],w3
 2490 000d60  F2 0F 20 	mov #255,w2
 2491 000d62  E0 C1 61 	and.b w3,#0,w3
 2492 000d64  02 81 60 	and w1,w2,w2
 2493 000d66  C8 08 DE 	lsr w1,#8,w1
 2494 000d68  02 C1 71 	ior.b w3,w2,w2
 2495 000d6a  42 40 98 	mov.b w2,[w0+4]
 2496 000d6c  50 41 90 	mov.b [w0+5],w2
 2497 000d6e  60 41 61 	and.b w2,#0,w2
 2498 000d70  81 40 71 	ior.b w2,w1,w1
 2499 000d72  51 40 98 	mov.b w1,[w0+5]
1907:lib/lib_pic33e/usb/usb_device.c ****         //Configure EP0 OUT to receive the next SETUP transaction for any future
1908:lib/lib_pic33e/usb/usb_device.c ****         //control transfers.  However, set BSTALL in case the host tries to send
1909:lib/lib_pic33e/usb/usb_device.c ****         //more data than it claims it was going to send.
1910:lib/lib_pic33e/usb/usb_device.c ****         pBDTEntryEP0OutNext->STAT.Val = _BSTALL;
 2500              	.loc 1 1910 0
 2501 000d74  00 00 80 	mov _pBDTEntryEP0OutNext,w0
1911:lib/lib_pic33e/usb/usb_device.c ****         pBDTEntryEP0OutNext->STAT.Val |= _USIE;
 2502              	.loc 1 1911 0
 2503 000d76  F4 0F 20 	mov #255,w4
 2504              	.loc 1 1910 0
 2505 000d78  90 40 78 	mov.b [w0],w1
 2506 000d7a  E0 C0 60 	and.b w1,#0,w1
 2507 000d7c  01 24 A0 	bset.b w1,#2
 2508 000d7e  01 48 78 	mov.b w1,[w0]
1912:lib/lib_pic33e/usb/usb_device.c **** 
MPLAB XC16 ASSEMBLY Listing:   			page 79


1913:lib/lib_pic33e/usb/usb_device.c **** 		//All data bytes for the host to device control write (OUT) have now been
1914:lib/lib_pic33e/usb/usb_device.c **** 		//received successfully.
1915:lib/lib_pic33e/usb/usb_device.c **** 		//Go ahead and call the user specified callback function, to use/consume
1916:lib/lib_pic33e/usb/usb_device.c **** 		//the control transfer data (ex: if the "void (*function)" parameter 
1917:lib/lib_pic33e/usb/usb_device.c **** 		//was non-NULL when USBEP0Receive() was called).
1918:lib/lib_pic33e/usb/usb_device.c ****         if(outPipes[0].pFunc != NULL)
 2509              	.loc 1 1918 0
 2510 000d80  53 00 20 	mov #_outPipes+5,w3
 2511              	.loc 1 1910 0
 2512 000d82  90 40 90 	mov.b [w0+1],w1
 2513 000d84  E0 C0 60 	and.b w1,#0,w1
 2514 000d86  11 40 98 	mov.b w1,[w0+1]
 2515              	.loc 1 1911 0
 2516 000d88  00 00 80 	mov _pBDTEntryEP0OutNext,w0
 2517 000d8a  01 00 80 	mov _pBDTEntryEP0OutNext,w1
 2518              	.loc 1 1918 0
 2519 000d8c  62 00 20 	mov #_outPipes+6,w2
 2520              	.loc 1 1911 0
 2521 000d8e  11 83 FB 	ze [w1],w6
 2522 000d90  91 40 90 	mov.b [w1+1],w1
 2523 000d92  90 42 78 	mov.b [w0],w5
 2524 000d94  81 80 FB 	ze w1,w1
 2525 000d96  E0 C2 62 	and.b w5,#0,w5
 2526 000d98  C8 08 DD 	sl w1,#8,w1
 2527 000d9a  81 00 73 	ior w6,w1,w1
 2528 000d9c  01 70 A0 	bset w1,#7
 2529 000d9e  04 82 60 	and w1,w4,w4
 2530 000da0  04 C2 72 	ior.b w5,w4,w4
 2531 000da2  04 48 78 	mov.b w4,[w0]
 2532 000da4  C8 08 DE 	lsr w1,#8,w1
 2533 000da6  10 42 90 	mov.b [w0+1],w4
 2534 000da8  60 42 62 	and.b w4,#0,w4
 2535 000daa  81 40 72 	ior.b w4,w1,w1
 2536 000dac  11 40 98 	mov.b w1,[w0+1]
 2537              	.loc 1 1918 0
 2538 000dae  13 80 FB 	ze [w3],w0
 2539 000db0  92 80 FB 	ze [w2],w1
 2540 000db2  C8 08 DD 	sl w1,#8,w1
 2541 000db4  01 00 70 	ior w0,w1,w0
 2542 000db6  00 00 E0 	cp0 w0
 2543              	.set ___BP___,0
 2544 000db8  00 00 32 	bra z,.L83
1919:lib/lib_pic33e/usb/usb_device.c ****         {
1920:lib/lib_pic33e/usb/usb_device.c ****             #if defined(__XC8)
1921:lib/lib_pic33e/usb/usb_device.c ****                 //Special pragmas to suppress an expected/harmless warning
1922:lib/lib_pic33e/usb/usb_device.c ****                 //message when building with the XC8 compiler
1923:lib/lib_pic33e/usb/usb_device.c ****                 #pragma warning push
1924:lib/lib_pic33e/usb/usb_device.c ****                 #pragma warning disable 1088
1925:lib/lib_pic33e/usb/usb_device.c ****                 outPipes[0].pFunc();    //Call the user's callback function
1926:lib/lib_pic33e/usb/usb_device.c ****                 #pragma warning pop
1927:lib/lib_pic33e/usb/usb_device.c ****             #else
1928:lib/lib_pic33e/usb/usb_device.c ****                 outPipes[0].pFunc();    //Call the user's callback function
 2545              	.loc 1 1928 0
 2546 000dba  50 00 20 	mov #_outPipes+5,w0
 2547 000dbc  61 00 20 	mov #_outPipes+6,w1
 2548 000dbe  10 80 FB 	ze [w0],w0
 2549 000dc0  91 80 FB 	ze [w1],w1
MPLAB XC16 ASSEMBLY Listing:   			page 80


 2550 000dc2  C8 08 DD 	sl w1,#8,w1
 2551 000dc4  01 00 70 	ior w0,w1,w0
 2552 000dc6  00 00 01 	call w0
 2553              	.L83:
1929:lib/lib_pic33e/usb/usb_device.c ****             #endif
1930:lib/lib_pic33e/usb/usb_device.c ****         }
1931:lib/lib_pic33e/usb/usb_device.c ****         outPipes[0].info.bits.busy = 0;    
 2554              	.loc 1 1931 0
 2555 000dc8  20 00 20 	mov #_outPipes+2,w0
 2556 000dca  90 40 78 	mov.b [w0],w1
 2557 000dcc  01 74 A1 	bclr.b w1,#7
 2558 000dce  01 48 78 	mov.b w1,[w0]
1932:lib/lib_pic33e/usb/usb_device.c **** 
1933:lib/lib_pic33e/usb/usb_device.c ****         //Ready to arm status stage IN transaction now, if the application
1934:lib/lib_pic33e/usb/usb_device.c ****         //firmware has completed processing the request.  If it is still busy
1935:lib/lib_pic33e/usb/usb_device.c ****         //and needs more time to finish handling the request, then the user
1936:lib/lib_pic33e/usb/usb_device.c ****         //callback (the one called by the outPipes[0].pFunc();) should set the
1937:lib/lib_pic33e/usb/usb_device.c ****         //USBDeferStatusStagePacket to true (by calling USBDeferStatusStage()).  In
1938:lib/lib_pic33e/usb/usb_device.c ****         //this case, it is the application's firmware responsibility to call 
1939:lib/lib_pic33e/usb/usb_device.c ****         //the USBCtrlEPAllowStatusStage() function, once it is fully done handling the request.
1940:lib/lib_pic33e/usb/usb_device.c ****         //Note: The application firmware must process the request and call
1941:lib/lib_pic33e/usb/usb_device.c ****         //USBCtrlEPAllowStatusStage() in a semi-timely fashion.  "Semi-timely"
1942:lib/lib_pic33e/usb/usb_device.c ****         //means either 50ms, 500ms, or 5 seconds, depending on the type of
1943:lib/lib_pic33e/usb/usb_device.c ****         //control transfer.  See the USB 2.0 specification section 9.2.6 for
1944:lib/lib_pic33e/usb/usb_device.c ****         //more details.
1945:lib/lib_pic33e/usb/usb_device.c ****         if(USBDeferStatusStagePacket == false)
 2559              	.loc 1 1945 0
 2560 000dd0  00 C0 BF 	mov.b _USBDeferStatusStagePacket,WREG
 2561 000dd2  00 04 A2 	btg.b w0,#0
 2562 000dd4  00 04 E0 	cp0.b w0
 2563              	.set ___BP___,0
 2564 000dd6  00 00 32 	bra z,.L76
1946:lib/lib_pic33e/usb/usb_device.c ****         {
1947:lib/lib_pic33e/usb/usb_device.c ****             USBCtrlEPAllowStatusStage();
 2565              	.loc 1 1947 0
 2566 000dd8  00 00 07 	rcall _USBCtrlEPAllowStatusStage
 2567              	.L76:
1948:lib/lib_pic33e/usb/usb_device.c ****         }            
1949:lib/lib_pic33e/usb/usb_device.c ****     }    
1950:lib/lib_pic33e/usb/usb_device.c **** 
1951:lib/lib_pic33e/usb/usb_device.c **** }//end USBCtrlTrfRxService
 2568              	.loc 1 1951 0
 2569 000dda  8E 07 78 	mov w14,w15
 2570 000ddc  4F 07 78 	mov [--w15],w14
 2571 000dde  00 40 A9 	bclr CORCON,#2
 2572 000de0  00 00 06 	return 
 2573              	.set ___PA___,0
 2574              	.LFE13:
 2575              	.size _USBCtrlTrfRxService,.-_USBCtrlTrfRxService
 2576              	.align 2
 2577              	.type _USBStdSetCfgHandler,@function
 2578              	_USBStdSetCfgHandler:
 2579              	.LFB14:
1952:lib/lib_pic33e/usb/usb_device.c **** 
1953:lib/lib_pic33e/usb/usb_device.c **** 
1954:lib/lib_pic33e/usb/usb_device.c **** /********************************************************************
1955:lib/lib_pic33e/usb/usb_device.c ****  * Function:        void USBStdSetCfgHandler(void)
MPLAB XC16 ASSEMBLY Listing:   			page 81


1956:lib/lib_pic33e/usb/usb_device.c ****  *
1957:lib/lib_pic33e/usb/usb_device.c ****  * PreCondition:    None
1958:lib/lib_pic33e/usb/usb_device.c ****  *
1959:lib/lib_pic33e/usb/usb_device.c ****  * Input:           None
1960:lib/lib_pic33e/usb/usb_device.c ****  *
1961:lib/lib_pic33e/usb/usb_device.c ****  * Output:          None
1962:lib/lib_pic33e/usb/usb_device.c ****  *
1963:lib/lib_pic33e/usb/usb_device.c ****  * Side Effects:    None
1964:lib/lib_pic33e/usb/usb_device.c ****  *
1965:lib/lib_pic33e/usb/usb_device.c ****  * Overview:        This routine first disables all endpoints by
1966:lib/lib_pic33e/usb/usb_device.c ****  *                  clearing UEP registers. It then configures
1967:lib/lib_pic33e/usb/usb_device.c ****  *                  (initializes) endpoints by calling the callback
1968:lib/lib_pic33e/usb/usb_device.c ****  *                  function USBCBInitEP().
1969:lib/lib_pic33e/usb/usb_device.c ****  *
1970:lib/lib_pic33e/usb/usb_device.c ****  * Note:            None
1971:lib/lib_pic33e/usb/usb_device.c ****  *******************************************************************/
1972:lib/lib_pic33e/usb/usb_device.c **** static void USBStdSetCfgHandler(void)
1973:lib/lib_pic33e/usb/usb_device.c **** {
 2580              	.loc 1 1973 0
 2581              	.set ___PA___,1
 2582 000de2  02 00 FA 	lnk #2
 2583              	.LCFI17:
1974:lib/lib_pic33e/usb/usb_device.c ****     uint8_t i;
1975:lib/lib_pic33e/usb/usb_device.c **** 
1976:lib/lib_pic33e/usb/usb_device.c ****     // This will generate a zero length packet
1977:lib/lib_pic33e/usb/usb_device.c ****     inPipes[0].info.bits.busy = 1;            
 2584              	.loc 1 1977 0
 2585 000de4  10 00 80 	mov _inPipes+2,w0
1978:lib/lib_pic33e/usb/usb_device.c **** 
1979:lib/lib_pic33e/usb/usb_device.c ****     //Clear all of the endpoint control registers
1980:lib/lib_pic33e/usb/usb_device.c ****     DisableNonZeroEndpoints(USB_MAX_EP_NUMBER);
 2586              	.loc 1 1980 0
 2587 000de6  82 00 20 	mov #8,w2
 2588              	.loc 1 1977 0
 2589 000de8  00 70 A0 	bset w0,#7
 2590              	.loc 1 1980 0
 2591 000dea  80 00 EB 	clr w1
 2592              	.loc 1 1977 0
 2593 000dec  10 00 88 	mov w0,_inPipes+2
 2594              	.loc 1 1980 0
 2595 000dee  00 00 20 	mov #_U1EP1,w0
 2596 000df0  00 00 07 	rcall _memset
1981:lib/lib_pic33e/usb/usb_device.c **** 
1982:lib/lib_pic33e/usb/usb_device.c ****     //Clear all of the BDT entries
1983:lib/lib_pic33e/usb/usb_device.c ****     memset((void*)&BDT[0], 0x00, sizeof(BDT));
 2597              	.loc 1 1983 0
 2598 000df2  02 0A 20 	mov #160,w2
 2599 000df4  80 00 EB 	clr w1
 2600 000df6  00 00 20 	mov #_BDT,w0
 2601 000df8  00 00 07 	rcall _memset
1984:lib/lib_pic33e/usb/usb_device.c **** 
1985:lib/lib_pic33e/usb/usb_device.c ****     // Assert reset request to all of the Ping Pong buffer pointers
1986:lib/lib_pic33e/usb/usb_device.c ****     USBPingPongBufferReset = 1;                                   
1987:lib/lib_pic33e/usb/usb_device.c **** 
1988:lib/lib_pic33e/usb/usb_device.c **** 	//Re-Initialize all ping pong software state bits to 0 (which corresponds to
1989:lib/lib_pic33e/usb/usb_device.c **** 	//the EVEN buffer being the next one that will be used), since we are also 
1990:lib/lib_pic33e/usb/usb_device.c **** 	//doing a hardware ping pong pointer reset above.
MPLAB XC16 ASSEMBLY Listing:   			page 82


1991:lib/lib_pic33e/usb/usb_device.c **** 	for(i = 0; i < (uint8_t)(USB_MAX_EP_NUMBER+1u); i++)
 2602              	.loc 1 1991 0
 2603 000dfa  00 40 EB 	clr.b w0
 2604              	.loc 1 1986 0
 2605 000dfc  00 20 A8 	bset.b _U1CONbits,#1
 2606              	.loc 1 1991 0
 2607 000dfe  00 4F 78 	mov.b w0,[w14]
 2608 000e00  00 00 37 	bra .L85
 2609              	.L86:
1992:lib/lib_pic33e/usb/usb_device.c **** 	{
1993:lib/lib_pic33e/usb/usb_device.c **** 		ep_data_in[i].Val = 0u;
 2610              	.loc 1 1993 0
 2611 000e02  9E 80 FB 	ze [w14],w1
1994:lib/lib_pic33e/usb/usb_device.c ****         ep_data_out[i].Val = 0u;
 2612              	.loc 1 1994 0
 2613 000e04  1E 80 FB 	ze [w14],w0
 2614              	.loc 1 1991 0
 2615 000e06  1E 4F E8 	inc.b [w14],[w14]
 2616              	.loc 1 1993 0
 2617 000e08  02 00 20 	mov #_ep_data_in,w2
 2618 000e0a  80 41 EB 	clr.b w3
 2619 000e0c  01 01 41 	add w2,w1,w2
 2620              	.loc 1 1994 0
 2621 000e0e  01 00 20 	mov #_ep_data_out,w1
 2622              	.loc 1 1993 0
 2623 000e10  03 49 78 	mov.b w3,[w2]
 2624              	.loc 1 1994 0
 2625 000e12  00 80 40 	add w1,w0,w0
 2626 000e14  80 40 EB 	clr.b w1
 2627 000e16  01 48 78 	mov.b w1,[w0]
 2628              	.L85:
 2629              	.loc 1 1991 0
 2630 000e18  1E 40 78 	mov.b [w14],w0
 2631 000e1a  E4 4F 50 	sub.b w0,#4,[w15]
 2632              	.set ___BP___,0
 2633 000e1c  00 00 36 	bra leu,.L86
1995:lib/lib_pic33e/usb/usb_device.c **** 	}
1996:lib/lib_pic33e/usb/usb_device.c **** 
1997:lib/lib_pic33e/usb/usb_device.c ****     //clear the alternate interface settings
1998:lib/lib_pic33e/usb/usb_device.c ****     memset((void*)&USBAlternateInterface,0x00,USB_MAX_NUM_INT);
 2634              	.loc 1 1998 0
 2635 000e1e  22 00 20 	mov #2,w2
 2636 000e20  80 00 EB 	clr w1
 2637 000e22  00 00 20 	mov #_USBAlternateInterface,w0
 2638 000e24  00 00 07 	rcall _memset
1999:lib/lib_pic33e/usb/usb_device.c **** 
2000:lib/lib_pic33e/usb/usb_device.c ****     //Stop trying to reset ping pong buffer pointers
2001:lib/lib_pic33e/usb/usb_device.c ****     USBPingPongBufferReset = 0;
2002:lib/lib_pic33e/usb/usb_device.c **** 
2003:lib/lib_pic33e/usb/usb_device.c ****     pBDTEntryIn[0] = (volatile BDT_ENTRY*)&BDT[EP0_IN_EVEN];
2004:lib/lib_pic33e/usb/usb_device.c **** 
2005:lib/lib_pic33e/usb/usb_device.c **** 	//Set the next out to the current out packet
2006:lib/lib_pic33e/usb/usb_device.c ****     pBDTEntryEP0OutCurrent = (volatile BDT_ENTRY*)&BDT[EP0_OUT_EVEN];
2007:lib/lib_pic33e/usb/usb_device.c ****     pBDTEntryEP0OutNext = pBDTEntryEP0OutCurrent;
2008:lib/lib_pic33e/usb/usb_device.c **** 
2009:lib/lib_pic33e/usb/usb_device.c ****     //set the current configuration
2010:lib/lib_pic33e/usb/usb_device.c ****     USBActiveConfiguration = SetupPkt.bConfigurationValue;
MPLAB XC16 ASSEMBLY Listing:   			page 83


 2639              	.loc 1 2010 0
 2640 000e26  21 00 20 	mov #_SetupPkt+2,w1
 2641              	.loc 1 2001 0
 2642 000e28  00 20 A9 	bclr.b _U1CONbits,#1
 2643              	.loc 1 2003 0
 2644 000e2a  00 01 20 	mov #_BDT+16,w0
 2645              	.loc 1 2010 0
 2646 000e2c  11 41 78 	mov.b [w1],w2
 2647              	.loc 1 2006 0
 2648 000e2e  01 00 20 	mov #_BDT,w1
 2649              	.loc 1 2010 0
 2650 000e30  00 00 00 	nop 
 2651 000e32  02 42 78 	mov.b w2,w4
 2652 000e34  03 00 20 	mov #_USBActiveConfiguration,w3
 2653 000e36  84 49 78 	mov.b w4,[w3]
 2654              	.loc 1 2006 0
 2655 000e38  01 00 88 	mov w1,_pBDTEntryEP0OutCurrent
 2656              	.loc 1 2003 0
 2657 000e3a  00 00 88 	mov w0,_pBDTEntryIn
 2658              	.loc 1 2007 0
 2659 000e3c  01 00 80 	mov _pBDTEntryEP0OutCurrent,w1
2011:lib/lib_pic33e/usb/usb_device.c **** 
2012:lib/lib_pic33e/usb/usb_device.c ****     //if the configuration value == 0
2013:lib/lib_pic33e/usb/usb_device.c ****     if(USBActiveConfiguration == 0)
 2660              	.loc 1 2013 0
 2661 000e3e  00 C0 BF 	mov.b _USBActiveConfiguration,WREG
 2662              	.loc 1 2007 0
 2663 000e40  01 00 88 	mov w1,_pBDTEntryEP0OutNext
 2664              	.loc 1 2013 0
 2665 000e42  00 04 E0 	cp0.b w0
 2666              	.set ___BP___,0
 2667 000e44  00 00 3A 	bra nz,.L87
2014:lib/lib_pic33e/usb/usb_device.c ****     {
2015:lib/lib_pic33e/usb/usb_device.c ****         //Go back to the addressed state
2016:lib/lib_pic33e/usb/usb_device.c ****         USBDeviceState = ADDRESS_STATE;
 2668              	.loc 1 2016 0
 2669 000e46  00 01 20 	mov #16,w0
 2670 000e48  00 00 88 	mov w0,_USBDeviceState
 2671 000e4a  00 00 37 	bra .L84
 2672              	.L87:
2017:lib/lib_pic33e/usb/usb_device.c ****     }
2018:lib/lib_pic33e/usb/usb_device.c ****     else
2019:lib/lib_pic33e/usb/usb_device.c ****     {
2020:lib/lib_pic33e/usb/usb_device.c ****         //initialize the required endpoints
2021:lib/lib_pic33e/usb/usb_device.c ****         USB_SET_CONFIGURATION_HANDLER(EVENT_CONFIGURED,(void*)&USBActiveConfiguration,1);
 2673              	.loc 1 2021 0
 2674 000e4c  12 00 20 	mov #1,w2
 2675 000e4e  01 00 20 	mov #_USBActiveConfiguration,w1
 2676 000e50  10 00 20 	mov #1,w0
 2677 000e52  00 00 07 	rcall _USER_USB_CALLBACK_EVENT_HANDLER
2022:lib/lib_pic33e/usb/usb_device.c **** 
2023:lib/lib_pic33e/usb/usb_device.c ****         //Otherwise go to the configured state.  Update the state variable last,
2024:lib/lib_pic33e/usb/usb_device.c ****         //after performing all of the set configuration related initialization
2025:lib/lib_pic33e/usb/usb_device.c ****         //tasks.
2026:lib/lib_pic33e/usb/usb_device.c ****         USBDeviceState = CONFIGURED_STATE;		
 2678              	.loc 1 2026 0
 2679 000e54  00 02 20 	mov #32,w0
MPLAB XC16 ASSEMBLY Listing:   			page 84


 2680 000e56  00 00 88 	mov w0,_USBDeviceState
 2681              	.L84:
2027:lib/lib_pic33e/usb/usb_device.c ****     }//end if(SetupPkt.bConfigurationValue == 0)
2028:lib/lib_pic33e/usb/usb_device.c **** }//end USBStdSetCfgHandler
 2682              	.loc 1 2028 0
 2683 000e58  8E 07 78 	mov w14,w15
 2684 000e5a  4F 07 78 	mov [--w15],w14
 2685 000e5c  00 40 A9 	bclr CORCON,#2
 2686 000e5e  00 00 06 	return 
 2687              	.set ___PA___,0
 2688              	.LFE14:
 2689              	.size _USBStdSetCfgHandler,.-_USBStdSetCfgHandler
 2690              	.align 2
 2691              	.type _USBStdGetDscHandler,@function
 2692              	_USBStdGetDscHandler:
 2693              	.LFB15:
2029:lib/lib_pic33e/usb/usb_device.c **** 
2030:lib/lib_pic33e/usb/usb_device.c **** 
2031:lib/lib_pic33e/usb/usb_device.c **** /********************************************************************
2032:lib/lib_pic33e/usb/usb_device.c ****  * Function:        void USBStdGetDscHandler(void)
2033:lib/lib_pic33e/usb/usb_device.c ****  *
2034:lib/lib_pic33e/usb/usb_device.c ****  * PreCondition:    None
2035:lib/lib_pic33e/usb/usb_device.c ****  *
2036:lib/lib_pic33e/usb/usb_device.c ****  * Input:           None
2037:lib/lib_pic33e/usb/usb_device.c ****  *
2038:lib/lib_pic33e/usb/usb_device.c ****  * Output:          None
2039:lib/lib_pic33e/usb/usb_device.c ****  *
2040:lib/lib_pic33e/usb/usb_device.c ****  * Side Effects:    None
2041:lib/lib_pic33e/usb/usb_device.c ****  *
2042:lib/lib_pic33e/usb/usb_device.c ****  * Overview:        This routine handles the standard GET_DESCRIPTOR
2043:lib/lib_pic33e/usb/usb_device.c ****  *                  request.
2044:lib/lib_pic33e/usb/usb_device.c ****  *
2045:lib/lib_pic33e/usb/usb_device.c ****  * Note:            None
2046:lib/lib_pic33e/usb/usb_device.c ****  *******************************************************************/
2047:lib/lib_pic33e/usb/usb_device.c **** static void USBStdGetDscHandler(void)
2048:lib/lib_pic33e/usb/usb_device.c **** {
 2694              	.loc 1 2048 0
 2695              	.set ___PA___,1
 2696 000e60  00 00 FA 	lnk #0
 2697              	.LCFI18:
2049:lib/lib_pic33e/usb/usb_device.c ****     if(SetupPkt.bmRequestType == 0x80)
 2698              	.loc 1 2049 0
 2699 000e62  01 00 20 	mov #_SetupPkt,w1
 2700 000e64  00 C8 B3 	mov.b #-128,w0
 2701 000e66  91 40 78 	mov.b [w1],w1
 2702 000e68  80 CF 50 	sub.b w1,w0,[w15]
 2703              	.set ___BP___,0
 2704 000e6a  00 00 3A 	bra nz,.L89
2050:lib/lib_pic33e/usb/usb_device.c ****     {
2051:lib/lib_pic33e/usb/usb_device.c ****         inPipes[0].info.Val = USB_EP0_ROM | USB_EP0_BUSY | USB_EP0_INCLUDE_ZERO;
2052:lib/lib_pic33e/usb/usb_device.c **** 
2053:lib/lib_pic33e/usb/usb_device.c ****         switch(SetupPkt.bDescriptorType)
 2705              	.loc 1 2053 0
 2706 000e6c  30 00 20 	mov #_SetupPkt+3,w0
 2707              	.loc 1 2051 0
 2708 000e6e  21 00 20 	mov #_inPipes+2,w1
 2709 000e70  02 CC B3 	mov.b #-64,w2
MPLAB XC16 ASSEMBLY Listing:   			page 85


 2710 000e72  82 48 78 	mov.b w2,[w1]
 2711              	.loc 1 2053 0
 2712 000e74  10 40 78 	mov.b [w0],w0
 2713 000e76  00 80 FB 	ze w0,w0
 2714 000e78  E2 0F 50 	sub w0,#2,[w15]
 2715              	.set ___BP___,0
 2716 000e7a  00 00 32 	bra z,.L93
 2717 000e7c  E3 0F 50 	sub w0,#3,[w15]
 2718              	.set ___BP___,0
 2719 000e7e  00 00 32 	bra z,.L94
 2720 000e80  E1 0F 50 	sub w0,#1,[w15]
 2721              	.set ___BP___,0
 2722 000e82  00 00 3A 	bra nz,.L99
 2723              	.L92:
2054:lib/lib_pic33e/usb/usb_device.c ****         {
2055:lib/lib_pic33e/usb/usb_device.c ****             case USB_DESCRIPTOR_DEVICE:
2056:lib/lib_pic33e/usb/usb_device.c ****                 #if !defined(USB_USER_DEVICE_DESCRIPTOR)
2057:lib/lib_pic33e/usb/usb_device.c ****                     inPipes[0].pSrc.bRom = (const uint8_t*)&device_dsc;
2058:lib/lib_pic33e/usb/usb_device.c ****                 #else
2059:lib/lib_pic33e/usb/usb_device.c ****                     inPipes[0].pSrc.bRom = (const uint8_t*)USB_USER_DEVICE_DESCRIPTOR;
 2724              	.loc 1 2059 0
 2725 000e84  01 00 20 	mov #_device_dsc,w1
2060:lib/lib_pic33e/usb/usb_device.c ****                 #endif
2061:lib/lib_pic33e/usb/usb_device.c ****                 inPipes[0].wCount.Val = sizeof(device_dsc);
 2726              	.loc 1 2061 0
 2727 000e86  20 01 20 	mov #18,w0
 2728              	.loc 1 2059 0
 2729 000e88  01 00 88 	mov w1,_inPipes
 2730              	.loc 1 2061 0
 2731 000e8a  20 00 88 	mov w0,_inPipes+4
2062:lib/lib_pic33e/usb/usb_device.c ****                 break;
 2732              	.loc 1 2062 0
 2733 000e8c  00 00 37 	bra .L89
 2734              	.L93:
2063:lib/lib_pic33e/usb/usb_device.c ****             case USB_DESCRIPTOR_CONFIGURATION:
2064:lib/lib_pic33e/usb/usb_device.c ****                 //First perform error case check, to make sure the host is requesting a 
2065:lib/lib_pic33e/usb/usb_device.c ****                 //legal descriptor index.  If the request index is illegal, don't do 
2066:lib/lib_pic33e/usb/usb_device.c ****                 //anything (so that the default STALL response will be sent).
2067:lib/lib_pic33e/usb/usb_device.c ****                 if(SetupPkt.bDscIndex < USB_MAX_NUM_CONFIG_DSC)
 2735              	.loc 1 2067 0
 2736 000e8e  20 00 20 	mov #_SetupPkt+2,w0
 2737 000e90  10 40 78 	mov.b [w0],w0
 2738 000e92  00 04 E0 	cp0.b w0
 2739              	.set ___BP___,0
 2740 000e94  00 00 3A 	bra nz,.L95
2068:lib/lib_pic33e/usb/usb_device.c ****                 {
2069:lib/lib_pic33e/usb/usb_device.c ****                     #if !defined(USB_USER_CONFIG_DESCRIPTOR)
2070:lib/lib_pic33e/usb/usb_device.c ****                         inPipes[0].pSrc.bRom = *(USB_CD_Ptr+SetupPkt.bDscIndex);
2071:lib/lib_pic33e/usb/usb_device.c ****                     #else
2072:lib/lib_pic33e/usb/usb_device.c ****                         inPipes[0].pSrc.bRom = *(USB_USER_CONFIG_DESCRIPTOR+SetupPkt.bDscIndex);
 2741              	.loc 1 2072 0
 2742 000e96  20 00 20 	mov #_SetupPkt+2,w0
 2743 000e98  01 00 20 	mov #_USB_CD_Ptr,w1
 2744 000e9a  10 40 78 	mov.b [w0],w0
 2745 000e9c  00 80 FB 	ze w0,w0
 2746 000e9e  00 00 40 	add w0,w0,w0
 2747 000ea0  01 00 40 	add w0,w1,w0
MPLAB XC16 ASSEMBLY Listing:   			page 86


2073:lib/lib_pic33e/usb/usb_device.c ****                     #endif
2074:lib/lib_pic33e/usb/usb_device.c **** 
2075:lib/lib_pic33e/usb/usb_device.c ****                     //This must be loaded using byte addressing.  The source pointer
2076:lib/lib_pic33e/usb/usb_device.c ****                     //  may not be word aligned for the 16 or 32 bit machines resulting
2077:lib/lib_pic33e/usb/usb_device.c ****                     //  in an address error on the dereference.
2078:lib/lib_pic33e/usb/usb_device.c ****                     inPipes[0].wCount.byte.LB = *(inPipes[0].pSrc.bRom+2);
 2748              	.loc 1 2078 0
 2749 000ea2  41 00 20 	mov #_inPipes+4,w1
 2750              	.loc 1 2072 0
 2751 000ea4  10 00 78 	mov [w0],w0
 2752 000ea6  00 00 88 	mov w0,_inPipes
 2753              	.loc 1 2078 0
 2754 000ea8  00 00 80 	mov _inPipes,w0
 2755 000eaa  00 81 E8 	inc2 w0,w2
2079:lib/lib_pic33e/usb/usb_device.c ****                     inPipes[0].wCount.byte.HB = *(inPipes[0].pSrc.bRom+3);
 2756              	.loc 1 2079 0
 2757 000eac  50 00 20 	mov #_inPipes+5,w0
 2758              	.loc 1 2078 0
 2759 000eae  12 41 78 	mov.b [w2],w2
 2760 000eb0  82 48 78 	mov.b w2,[w1]
 2761              	.loc 1 2079 0
 2762 000eb2  01 00 80 	mov _inPipes,w1
 2763 000eb4  E3 80 40 	add w1,#3,w1
 2764 000eb6  00 00 00 	nop 
 2765 000eb8  91 40 78 	mov.b [w1],w1
 2766 000eba  01 48 78 	mov.b w1,[w0]
2080:lib/lib_pic33e/usb/usb_device.c ****                 }
2081:lib/lib_pic33e/usb/usb_device.c **** 				else
2082:lib/lib_pic33e/usb/usb_device.c **** 				{
2083:lib/lib_pic33e/usb/usb_device.c **** 					inPipes[0].info.Val = 0;
2084:lib/lib_pic33e/usb/usb_device.c **** 				}
2085:lib/lib_pic33e/usb/usb_device.c ****                 break;
 2767              	.loc 1 2085 0
 2768 000ebc  00 00 37 	bra .L89
 2769              	.L95:
 2770              	.loc 1 2083 0
 2771 000ebe  20 00 20 	mov #_inPipes+2,w0
 2772 000ec0  80 40 EB 	clr.b w1
 2773 000ec2  01 48 78 	mov.b w1,[w0]
 2774              	.loc 1 2085 0
 2775 000ec4  00 00 37 	bra .L89
 2776              	.L94:
2086:lib/lib_pic33e/usb/usb_device.c ****             case USB_DESCRIPTOR_STRING:
2087:lib/lib_pic33e/usb/usb_device.c ****                 //USB_NUM_STRING_DESCRIPTORS was introduced as optional in release v2.3.  In v2.4 a
2088:lib/lib_pic33e/usb/usb_device.c ****                 //  later it is now mandatory.  This should be defined in usb_config.h and should
2089:lib/lib_pic33e/usb/usb_device.c ****                 //  indicate the number of string descriptors.
2090:lib/lib_pic33e/usb/usb_device.c ****                 if(SetupPkt.bDscIndex<USB_NUM_STRING_DESCRIPTORS)
 2777              	.loc 1 2090 0
 2778 000ec6  20 00 20 	mov #_SetupPkt+2,w0
 2779 000ec8  10 40 78 	mov.b [w0],w0
 2780 000eca  E2 4F 50 	sub.b w0,#2,[w15]
 2781              	.set ___BP___,0
 2782 000ecc  00 00 3E 	bra gtu,.L97
2091:lib/lib_pic33e/usb/usb_device.c ****                 {
2092:lib/lib_pic33e/usb/usb_device.c ****                     //Get a pointer to the String descriptor requested
2093:lib/lib_pic33e/usb/usb_device.c ****                     inPipes[0].pSrc.bRom = *(USB_SD_Ptr+SetupPkt.bDscIndex);
 2783              	.loc 1 2093 0
MPLAB XC16 ASSEMBLY Listing:   			page 87


 2784 000ece  20 00 20 	mov #_SetupPkt+2,w0
 2785 000ed0  01 00 20 	mov #_USB_SD_Ptr,w1
 2786 000ed2  10 40 78 	mov.b [w0],w0
 2787 000ed4  00 80 FB 	ze w0,w0
 2788 000ed6  00 00 40 	add w0,w0,w0
 2789 000ed8  01 00 40 	add w0,w1,w0
 2790 000eda  10 00 78 	mov [w0],w0
 2791 000edc  00 00 88 	mov w0,_inPipes
2094:lib/lib_pic33e/usb/usb_device.c ****                     // Set data count
2095:lib/lib_pic33e/usb/usb_device.c ****                     inPipes[0].wCount.Val = *inPipes[0].pSrc.bRom;                    
 2792              	.loc 1 2095 0
 2793 000ede  00 00 80 	mov _inPipes,w0
 2794 000ee0  10 40 78 	mov.b [w0],w0
 2795 000ee2  00 80 FB 	ze w0,w0
 2796 000ee4  20 00 88 	mov w0,_inPipes+4
2096:lib/lib_pic33e/usb/usb_device.c ****                 }
2097:lib/lib_pic33e/usb/usb_device.c ****                 #if defined(IMPLEMENT_MICROSOFT_OS_DESCRIPTOR)
2098:lib/lib_pic33e/usb/usb_device.c ****                 else if(SetupPkt.bDscIndex == MICROSOFT_OS_DESCRIPTOR_INDEX)
2099:lib/lib_pic33e/usb/usb_device.c ****                 {
2100:lib/lib_pic33e/usb/usb_device.c ****                     //Get a pointer to the special MS OS string descriptor requested
2101:lib/lib_pic33e/usb/usb_device.c ****                     inPipes[0].pSrc.bRom = (const uint8_t*)&MSOSDescriptor;
2102:lib/lib_pic33e/usb/usb_device.c ****                     // Set data count
2103:lib/lib_pic33e/usb/usb_device.c ****                     inPipes[0].wCount.Val = *inPipes[0].pSrc.bRom;                    
2104:lib/lib_pic33e/usb/usb_device.c ****                 }    
2105:lib/lib_pic33e/usb/usb_device.c ****                 #endif
2106:lib/lib_pic33e/usb/usb_device.c ****                 else
2107:lib/lib_pic33e/usb/usb_device.c ****                 {
2108:lib/lib_pic33e/usb/usb_device.c ****                     inPipes[0].info.Val = 0;
2109:lib/lib_pic33e/usb/usb_device.c ****                 }
2110:lib/lib_pic33e/usb/usb_device.c ****                 break;
 2797              	.loc 1 2110 0
 2798 000ee6  00 00 37 	bra .L89
 2799              	.L97:
 2800              	.loc 1 2108 0
 2801 000ee8  20 00 20 	mov #_inPipes+2,w0
 2802 000eea  80 40 EB 	clr.b w1
 2803 000eec  01 48 78 	mov.b w1,[w0]
 2804              	.loc 1 2110 0
 2805 000eee  00 00 37 	bra .L89
 2806              	.L99:
2111:lib/lib_pic33e/usb/usb_device.c ****             default:
2112:lib/lib_pic33e/usb/usb_device.c ****                 inPipes[0].info.Val = 0;
 2807              	.loc 1 2112 0
 2808 000ef0  20 00 20 	mov #_inPipes+2,w0
 2809 000ef2  80 40 EB 	clr.b w1
 2810 000ef4  01 48 78 	mov.b w1,[w0]
 2811              	.L89:
2113:lib/lib_pic33e/usb/usb_device.c ****                 break;
2114:lib/lib_pic33e/usb/usb_device.c ****         }//end switch
2115:lib/lib_pic33e/usb/usb_device.c ****     }//end if
2116:lib/lib_pic33e/usb/usb_device.c **** }//end USBStdGetDscHandler
 2812              	.loc 1 2116 0
 2813 000ef6  8E 07 78 	mov w14,w15
 2814 000ef8  4F 07 78 	mov [--w15],w14
 2815 000efa  00 40 A9 	bclr CORCON,#2
 2816 000efc  00 00 06 	return 
 2817              	.set ___PA___,0
MPLAB XC16 ASSEMBLY Listing:   			page 88


 2818              	.LFE15:
 2819              	.size _USBStdGetDscHandler,.-_USBStdGetDscHandler
 2820              	.align 2
 2821              	.type _USBStdGetStatusHandler,@function
 2822              	_USBStdGetStatusHandler:
 2823              	.LFB16:
2117:lib/lib_pic33e/usb/usb_device.c **** 
2118:lib/lib_pic33e/usb/usb_device.c **** /********************************************************************
2119:lib/lib_pic33e/usb/usb_device.c ****  * Function:        void USBStdGetStatusHandler(void)
2120:lib/lib_pic33e/usb/usb_device.c ****  *
2121:lib/lib_pic33e/usb/usb_device.c ****  * PreCondition:    None
2122:lib/lib_pic33e/usb/usb_device.c ****  *
2123:lib/lib_pic33e/usb/usb_device.c ****  * Input:           None
2124:lib/lib_pic33e/usb/usb_device.c ****  *
2125:lib/lib_pic33e/usb/usb_device.c ****  * Output:          None
2126:lib/lib_pic33e/usb/usb_device.c ****  *
2127:lib/lib_pic33e/usb/usb_device.c ****  * Side Effects:    None
2128:lib/lib_pic33e/usb/usb_device.c ****  *
2129:lib/lib_pic33e/usb/usb_device.c ****  * Overview:        This routine handles the standard GET_STATUS request
2130:lib/lib_pic33e/usb/usb_device.c ****  *
2131:lib/lib_pic33e/usb/usb_device.c ****  * Note:            None
2132:lib/lib_pic33e/usb/usb_device.c ****  *******************************************************************/
2133:lib/lib_pic33e/usb/usb_device.c **** static void USBStdGetStatusHandler(void)
2134:lib/lib_pic33e/usb/usb_device.c **** {
 2824              	.loc 1 2134 0
 2825              	.set ___PA___,1
 2826 000efe  02 00 FA 	lnk #2
 2827              	.LCFI19:
2135:lib/lib_pic33e/usb/usb_device.c ****     CtrlTrfData[0] = 0;                 // Initialize content
 2828              	.loc 1 2135 0
 2829 000f00  00 00 20 	mov #_CtrlTrfData,w0
 2830 000f02  00 41 EB 	clr.b w2
2136:lib/lib_pic33e/usb/usb_device.c ****     CtrlTrfData[1] = 0;
 2831              	.loc 1 2136 0
 2832 000f04  11 00 20 	mov #_CtrlTrfData+1,w1
 2833              	.loc 1 2135 0
 2834 000f06  02 48 78 	mov.b w2,[w0]
 2835              	.loc 1 2136 0
 2836 000f08  00 41 EB 	clr.b w2
2137:lib/lib_pic33e/usb/usb_device.c **** 
2138:lib/lib_pic33e/usb/usb_device.c ****     switch(SetupPkt.Recipient)
 2837              	.loc 1 2138 0
 2838 000f0a  00 00 20 	mov #_SetupPkt,w0
 2839              	.loc 1 2136 0
 2840 000f0c  82 48 78 	mov.b w2,[w1]
 2841              	.loc 1 2138 0
 2842 000f0e  90 40 78 	mov.b [w0],w1
 2843 000f10  7F C0 60 	and.b w1,#31,w0
 2844 000f12  00 80 FB 	ze w0,w0
 2845 000f14  E1 0F 50 	sub w0,#1,[w15]
 2846              	.set ___BP___,0
 2847 000f16  00 00 32 	bra z,.L103
 2848 000f18  E2 0F 50 	sub w0,#2,[w15]
 2849              	.set ___BP___,0
 2850 000f1a  00 00 32 	bra z,.L104
 2851 000f1c  00 00 E0 	cp0 w0
 2852              	.set ___BP___,0
MPLAB XC16 ASSEMBLY Listing:   			page 89


 2853 000f1e  00 00 3A 	bra nz,.L101
 2854              	.L102:
2139:lib/lib_pic33e/usb/usb_device.c ****     {
2140:lib/lib_pic33e/usb/usb_device.c ****         case USB_SETUP_RECIPIENT_DEVICE_BITFIELD:
2141:lib/lib_pic33e/usb/usb_device.c ****             inPipes[0].info.bits.busy = 1;
 2855              	.loc 1 2141 0
 2856 000f20  10 00 80 	mov _inPipes+2,w0
 2857 000f22  00 70 A0 	bset w0,#7
 2858 000f24  10 00 88 	mov w0,_inPipes+2
2142:lib/lib_pic33e/usb/usb_device.c ****             /*
2143:lib/lib_pic33e/usb/usb_device.c ****              * [0]: bit0: Self-Powered Status [0] Bus-Powered [1] Self-Powered
2144:lib/lib_pic33e/usb/usb_device.c ****              *      bit1: RemoteWakeup        [0] Disabled    [1] Enabled
2145:lib/lib_pic33e/usb/usb_device.c ****              */
2146:lib/lib_pic33e/usb/usb_device.c ****             if(self_power == 1) // self_power is defined in HardwareProfile.h
2147:lib/lib_pic33e/usb/usb_device.c ****             {
2148:lib/lib_pic33e/usb/usb_device.c ****                 CtrlTrfData[0]|=0x01;
2149:lib/lib_pic33e/usb/usb_device.c ****             }
2150:lib/lib_pic33e/usb/usb_device.c **** 
2151:lib/lib_pic33e/usb/usb_device.c ****             if(RemoteWakeup == true)
 2859              	.loc 1 2151 0
 2860 000f26  00 C0 BF 	mov.b _RemoteWakeup,WREG
 2861 000f28  00 04 E0 	cp0.b w0
 2862              	.set ___BP___,0
 2863 000f2a  00 00 32 	bra z,.L110
2152:lib/lib_pic33e/usb/usb_device.c ****             {
2153:lib/lib_pic33e/usb/usb_device.c ****                 CtrlTrfData[0]|=0x02;
 2864              	.loc 1 2153 0
 2865 000f2c  01 00 20 	mov #_CtrlTrfData,w1
 2866 000f2e  00 00 20 	mov #_CtrlTrfData,w0
 2867 000f30  91 40 78 	mov.b [w1],w1
 2868 000f32  01 14 A0 	bset.b w1,#1
 2869 000f34  01 48 78 	mov.b w1,[w0]
2154:lib/lib_pic33e/usb/usb_device.c ****             }
2155:lib/lib_pic33e/usb/usb_device.c ****             break;
 2870              	.loc 1 2155 0
 2871 000f36  00 00 37 	bra .L101
 2872              	.L103:
2156:lib/lib_pic33e/usb/usb_device.c ****         case USB_SETUP_RECIPIENT_INTERFACE_BITFIELD:
2157:lib/lib_pic33e/usb/usb_device.c ****             inPipes[0].info.bits.busy = 1;     // No data to update
 2873              	.loc 1 2157 0
 2874 000f38  10 00 80 	mov _inPipes+2,w0
 2875 000f3a  00 70 A0 	bset w0,#7
 2876 000f3c  10 00 88 	mov w0,_inPipes+2
2158:lib/lib_pic33e/usb/usb_device.c ****             break;
 2877              	.loc 1 2158 0
 2878 000f3e  00 00 37 	bra .L101
 2879              	.L104:
 2880              	.LBB2:
2159:lib/lib_pic33e/usb/usb_device.c ****         case USB_SETUP_RECIPIENT_ENDPOINT_BITFIELD:
2160:lib/lib_pic33e/usb/usb_device.c ****             inPipes[0].info.bits.busy = 1;
2161:lib/lib_pic33e/usb/usb_device.c ****             /*
2162:lib/lib_pic33e/usb/usb_device.c ****              * [0]: bit0: Halt Status [0] Not Halted [1] Halted
2163:lib/lib_pic33e/usb/usb_device.c ****              */
2164:lib/lib_pic33e/usb/usb_device.c ****             {
2165:lib/lib_pic33e/usb/usb_device.c ****                 BDT_ENTRY *p;
2166:lib/lib_pic33e/usb/usb_device.c **** 
2167:lib/lib_pic33e/usb/usb_device.c ****                 if(SetupPkt.EPDir == 0)
MPLAB XC16 ASSEMBLY Listing:   			page 90


 2881              	.loc 1 2167 0
 2882 000f40  41 00 20 	mov #_SetupPkt+4,w1
 2883              	.LBE2:
 2884              	.loc 1 2160 0
 2885 000f42  12 00 80 	mov _inPipes+2,w2
 2886              	.LBB3:
 2887              	.loc 1 2167 0
 2888 000f44  00 C8 B3 	mov.b #-128,w0
 2889              	.LBE3:
 2890              	.loc 1 2160 0
 2891 000f46  02 70 A0 	bset w2,#7
 2892 000f48  12 00 88 	mov w2,_inPipes+2
 2893              	.LBB4:
 2894              	.loc 1 2167 0
 2895 000f4a  91 40 78 	mov.b [w1],w1
 2896 000f4c  00 C0 60 	and.b w1,w0,w0
 2897 000f4e  00 04 E0 	cp0.b w0
 2898              	.set ___BP___,0
 2899 000f50  00 00 3A 	bra nz,.L106
2168:lib/lib_pic33e/usb/usb_device.c ****                 {
2169:lib/lib_pic33e/usb/usb_device.c ****                     p = (BDT_ENTRY*)pBDTEntryOut[SetupPkt.EPNum];
 2900              	.loc 1 2169 0
 2901 000f52  40 00 20 	mov #_SetupPkt+4,w0
 2902 000f54  01 00 20 	mov #_pBDTEntryOut,w1
 2903 000f56  10 41 78 	mov.b [w0],w2
 2904 000f58  6F 40 61 	and.b w2,#15,w0
 2905 000f5a  00 80 FB 	ze w0,w0
 2906 000f5c  00 00 40 	add w0,w0,w0
 2907 000f5e  01 00 40 	add w0,w1,w0
 2908 000f60  10 0F 78 	mov [w0],[w14]
 2909 000f62  00 00 37 	bra .L107
 2910              	.L106:
2170:lib/lib_pic33e/usb/usb_device.c ****                 }
2171:lib/lib_pic33e/usb/usb_device.c ****                 else
2172:lib/lib_pic33e/usb/usb_device.c ****                 {
2173:lib/lib_pic33e/usb/usb_device.c ****                     p = (BDT_ENTRY*)pBDTEntryIn[SetupPkt.EPNum];
 2911              	.loc 1 2173 0
 2912 000f64  40 00 20 	mov #_SetupPkt+4,w0
 2913 000f66  01 00 20 	mov #_pBDTEntryIn,w1
 2914 000f68  10 41 78 	mov.b [w0],w2
 2915 000f6a  6F 40 61 	and.b w2,#15,w0
 2916 000f6c  00 80 FB 	ze w0,w0
 2917 000f6e  00 00 40 	add w0,w0,w0
 2918 000f70  01 00 40 	add w0,w1,w0
 2919 000f72  10 0F 78 	mov [w0],[w14]
 2920              	.L107:
2174:lib/lib_pic33e/usb/usb_device.c ****                 }
2175:lib/lib_pic33e/usb/usb_device.c **** 
2176:lib/lib_pic33e/usb/usb_device.c ****                 if((p->STAT.UOWN == 1) && (p->STAT.BSTALL == 1))
 2921              	.loc 1 2176 0
 2922 000f74  9E 00 78 	mov [w14],w1
 2923 000f76  00 C8 B3 	mov.b #-128,w0
 2924 000f78  91 40 78 	mov.b [w1],w1
 2925 000f7a  00 C0 60 	and.b w1,w0,w0
 2926 000f7c  00 04 E0 	cp0.b w0
 2927              	.set ___BP___,0
 2928 000f7e  00 00 32 	bra z,.L111
MPLAB XC16 ASSEMBLY Listing:   			page 91


 2929 000f80  1E 00 78 	mov [w14],w0
 2930 000f82  10 40 78 	mov.b [w0],w0
 2931 000f84  64 40 60 	and.b w0,#4,w0
 2932 000f86  00 04 E0 	cp0.b w0
 2933              	.set ___BP___,0
 2934 000f88  00 00 32 	bra z,.L112
2177:lib/lib_pic33e/usb/usb_device.c ****                     CtrlTrfData[0]=0x01;    // Set bit0
 2935              	.loc 1 2177 0
 2936 000f8a  00 00 20 	mov #_CtrlTrfData,w0
 2937 000f8c  11 C0 B3 	mov.b #1,w1
 2938 000f8e  01 48 78 	mov.b w1,[w0]
2178:lib/lib_pic33e/usb/usb_device.c ****                 break;
 2939              	.loc 1 2178 0
 2940 000f90  00 00 37 	bra .L101
 2941              	.L110:
 2942 000f92  00 00 37 	bra .L101
 2943              	.L111:
 2944 000f94  00 00 37 	bra .L101
 2945              	.L112:
 2946              	.L101:
 2947              	.LBE4:
2179:lib/lib_pic33e/usb/usb_device.c ****             }
2180:lib/lib_pic33e/usb/usb_device.c ****     }//end switch
2181:lib/lib_pic33e/usb/usb_device.c **** 
2182:lib/lib_pic33e/usb/usb_device.c ****     if(inPipes[0].info.bits.busy == 1)
 2948              	.loc 1 2182 0
 2949 000f96  11 00 80 	mov _inPipes+2,w1
 2950 000f98  00 08 20 	mov #128,w0
 2951 000f9a  00 80 60 	and w1,w0,w0
 2952 000f9c  00 00 E0 	cp0 w0
 2953              	.set ___BP___,0
 2954 000f9e  00 00 32 	bra z,.L100
2183:lib/lib_pic33e/usb/usb_device.c ****     {
2184:lib/lib_pic33e/usb/usb_device.c ****         inPipes[0].pSrc.bRam = (uint8_t*)&CtrlTrfData;        // Set Source
 2955              	.loc 1 2184 0
 2956 000fa0  01 00 20 	mov #_CtrlTrfData,w1
2185:lib/lib_pic33e/usb/usb_device.c ****         inPipes[0].info.bits.ctrl_trf_mem = USB_EP0_RAM;      // Set memory type
2186:lib/lib_pic33e/usb/usb_device.c ****         inPipes[0].wCount.v[0] = 2;                           // Set data count
 2957              	.loc 1 2186 0
 2958 000fa2  40 00 20 	mov #_inPipes+4,w0
 2959              	.loc 1 2184 0
 2960 000fa4  01 00 88 	mov w1,_inPipes
 2961              	.loc 1 2186 0
 2962 000fa6  21 C0 B3 	mov.b #2,w1
 2963              	.loc 1 2185 0
 2964 000fa8  12 00 80 	mov _inPipes+2,w2
 2965 000faa  02 00 A0 	bset w2,#0
 2966 000fac  12 00 88 	mov w2,_inPipes+2
 2967              	.loc 1 2186 0
 2968 000fae  01 48 78 	mov.b w1,[w0]
 2969              	.L100:
2187:lib/lib_pic33e/usb/usb_device.c ****     }//end if(...)
2188:lib/lib_pic33e/usb/usb_device.c **** }//end USBStdGetStatusHandler
 2970              	.loc 1 2188 0
 2971 000fb0  8E 07 78 	mov w14,w15
 2972 000fb2  4F 07 78 	mov [--w15],w14
 2973 000fb4  00 40 A9 	bclr CORCON,#2
MPLAB XC16 ASSEMBLY Listing:   			page 92


 2974 000fb6  00 00 06 	return 
 2975              	.set ___PA___,0
 2976              	.LFE16:
 2977              	.size _USBStdGetStatusHandler,.-_USBStdGetStatusHandler
 2978              	.align 2
 2979              	.type _USBStallHandler,@function
 2980              	_USBStallHandler:
 2981              	.LFB17:
2189:lib/lib_pic33e/usb/usb_device.c **** 
2190:lib/lib_pic33e/usb/usb_device.c **** /********************************************************************
2191:lib/lib_pic33e/usb/usb_device.c ****  * Function:        void USBStallHandler(void)
2192:lib/lib_pic33e/usb/usb_device.c ****  *
2193:lib/lib_pic33e/usb/usb_device.c ****  * PreCondition:    None
2194:lib/lib_pic33e/usb/usb_device.c ****  *
2195:lib/lib_pic33e/usb/usb_device.c ****  * Input:           None
2196:lib/lib_pic33e/usb/usb_device.c ****  *
2197:lib/lib_pic33e/usb/usb_device.c ****  * Output:          None
2198:lib/lib_pic33e/usb/usb_device.c ****  *
2199:lib/lib_pic33e/usb/usb_device.c ****  * Side Effects:    
2200:lib/lib_pic33e/usb/usb_device.c ****  *
2201:lib/lib_pic33e/usb/usb_device.c ****  * Overview:        This function handles the event of a STALL 
2202:lib/lib_pic33e/usb/usb_device.c ****  *                  occurring on the bus
2203:lib/lib_pic33e/usb/usb_device.c ****  *
2204:lib/lib_pic33e/usb/usb_device.c ****  * Note:            None
2205:lib/lib_pic33e/usb/usb_device.c ****  *******************************************************************/
2206:lib/lib_pic33e/usb/usb_device.c **** static void USBStallHandler(void)
2207:lib/lib_pic33e/usb/usb_device.c **** {
 2982              	.loc 1 2207 0
 2983              	.set ___PA___,1
 2984 000fb8  00 00 FA 	lnk #0
 2985              	.LCFI20:
2208:lib/lib_pic33e/usb/usb_device.c ****     /*
2209:lib/lib_pic33e/usb/usb_device.c ****      * Does not really have to do anything here,
2210:lib/lib_pic33e/usb/usb_device.c ****      * even for the control endpoint.
2211:lib/lib_pic33e/usb/usb_device.c ****      * All BDs of Endpoint 0 are owned by SIE right now,
2212:lib/lib_pic33e/usb/usb_device.c ****      * but once a Setup Transaction is received, the ownership
2213:lib/lib_pic33e/usb/usb_device.c ****      * for EP0_OUT will be returned to CPU.
2214:lib/lib_pic33e/usb/usb_device.c ****      * When the Setup Transaction is serviced, the ownership
2215:lib/lib_pic33e/usb/usb_device.c ****      * for EP0_IN will then be forced back to CPU by firmware.
2216:lib/lib_pic33e/usb/usb_device.c ****      */
2217:lib/lib_pic33e/usb/usb_device.c **** 
2218:lib/lib_pic33e/usb/usb_device.c ****     if(U1EP0bits.EPSTALL == 1)
 2986              	.loc 1 2218 0
 2987 000fba  00 00 80 	mov _U1EP0bits,w0
 2988 000fbc  62 00 60 	and w0,#2,w0
 2989 000fbe  00 00 E0 	cp0 w0
 2990              	.set ___BP___,0
 2991 000fc0  00 00 32 	bra z,.L114
2219:lib/lib_pic33e/usb/usb_device.c ****     {
2220:lib/lib_pic33e/usb/usb_device.c ****         // UOWN - if 0, owned by CPU, if 1, owned by SIE
2221:lib/lib_pic33e/usb/usb_device.c ****         if((pBDTEntryEP0OutCurrent->STAT.Val == _USIE) && (pBDTEntryIn[0]->STAT.Val == (_USIE|_BSTA
 2992              	.loc 1 2221 0
 2993 000fc2  00 00 80 	mov _pBDTEntryEP0OutCurrent,w0
 2994 000fc4  01 08 20 	mov #128,w1
 2995 000fc6  10 81 FB 	ze [w0],w2
 2996 000fc8  10 40 90 	mov.b [w0+1],w0
 2997 000fca  00 80 FB 	ze w0,w0
MPLAB XC16 ASSEMBLY Listing:   			page 93


 2998 000fcc  48 00 DD 	sl w0,#8,w0
 2999 000fce  00 00 71 	ior w2,w0,w0
 3000 000fd0  81 0F 50 	sub w0,w1,[w15]
 3001              	.set ___BP___,0
 3002 000fd2  00 00 3A 	bra nz,.L115
 3003 000fd4  00 00 80 	mov _pBDTEntryIn,w0
 3004 000fd6  41 08 20 	mov #132,w1
 3005 000fd8  10 81 FB 	ze [w0],w2
 3006 000fda  10 40 90 	mov.b [w0+1],w0
 3007 000fdc  00 80 FB 	ze w0,w0
 3008 000fde  48 00 DD 	sl w0,#8,w0
 3009 000fe0  00 00 71 	ior w2,w0,w0
 3010 000fe2  81 0F 50 	sub w0,w1,[w15]
 3011              	.set ___BP___,0
 3012 000fe4  00 00 3A 	bra nz,.L115
2222:lib/lib_pic33e/usb/usb_device.c ****         {
2223:lib/lib_pic33e/usb/usb_device.c ****             // Set ep0Bo to stall also
2224:lib/lib_pic33e/usb/usb_device.c ****             pBDTEntryEP0OutCurrent->STAT.Val = _DAT0|(_DTSEN & _DTS_CHECKING_ENABLED)|_BSTALL;
 3013              	.loc 1 2224 0
 3014 000fe6  00 00 80 	mov _pBDTEntryEP0OutCurrent,w0
2225:lib/lib_pic33e/usb/usb_device.c ****             pBDTEntryEP0OutCurrent->STAT.Val |= _USIE;
 3015              	.loc 1 2225 0
 3016 000fe8  F2 0F 20 	mov #255,w2
 3017              	.loc 1 2224 0
 3018 000fea  90 40 78 	mov.b [w0],w1
 3019 000fec  E0 C0 60 	and.b w1,#0,w1
 3020 000fee  C1 40 B3 	ior.b #12,w1
 3021 000ff0  01 48 78 	mov.b w1,[w0]
 3022 000ff2  90 40 90 	mov.b [w0+1],w1
 3023 000ff4  E0 C0 60 	and.b w1,#0,w1
 3024 000ff6  11 40 98 	mov.b w1,[w0+1]
 3025              	.loc 1 2225 0
 3026 000ff8  00 00 80 	mov _pBDTEntryEP0OutCurrent,w0
 3027 000ffa  01 00 80 	mov _pBDTEntryEP0OutCurrent,w1
 3028 000ffc  11 82 FB 	ze [w1],w4
 3029 000ffe  00 00 00 	nop 
 3030 001000  91 40 90 	mov.b [w1+1],w1
 3031 001002  90 41 78 	mov.b [w0],w3
 3032 001004  81 80 FB 	ze w1,w1
 3033 001006  E0 C1 61 	and.b w3,#0,w3
 3034 001008  C8 08 DD 	sl w1,#8,w1
 3035 00100a  81 00 72 	ior w4,w1,w1
 3036 00100c  01 70 A0 	bset w1,#7
 3037 00100e  02 81 60 	and w1,w2,w2
 3038 001010  02 C1 71 	ior.b w3,w2,w2
 3039 001012  02 48 78 	mov.b w2,[w0]
 3040 001014  C8 08 DE 	lsr w1,#8,w1
 3041 001016  10 41 90 	mov.b [w0+1],w2
 3042 001018  60 41 61 	and.b w2,#0,w2
 3043 00101a  81 40 71 	ior.b w2,w1,w1
 3044 00101c  11 40 98 	mov.b w1,[w0+1]
 3045              	.L115:
2226:lib/lib_pic33e/usb/usb_device.c ****         }//end if
2227:lib/lib_pic33e/usb/usb_device.c ****         U1EP0bits.EPSTALL = 0;               // Clear stall status
 3046              	.loc 1 2227 0
 3047 00101e  00 20 A9 	bclr.b _U1EP0bits,#1
 3048              	.L114:
MPLAB XC16 ASSEMBLY Listing:   			page 94


2228:lib/lib_pic33e/usb/usb_device.c ****     }//end if
2229:lib/lib_pic33e/usb/usb_device.c **** 
2230:lib/lib_pic33e/usb/usb_device.c ****     USBClearInterruptFlag(USBStallIFReg,USBStallIFBitNum);
 3049              	.loc 1 2230 0
 3050 001020  00 08 20 	mov #128,w0
 3051 001022  00 00 88 	mov w0,_U1IR
2231:lib/lib_pic33e/usb/usb_device.c **** }
 3052              	.loc 1 2231 0
 3053 001024  8E 07 78 	mov w14,w15
 3054 001026  4F 07 78 	mov [--w15],w14
 3055 001028  00 40 A9 	bclr CORCON,#2
 3056 00102a  00 00 06 	return 
 3057              	.set ___PA___,0
 3058              	.LFE17:
 3059              	.size _USBStallHandler,.-_USBStallHandler
 3060              	.align 2
 3061              	.type _USBSuspend,@function
 3062              	_USBSuspend:
 3063              	.LFB18:
2232:lib/lib_pic33e/usb/usb_device.c **** 
2233:lib/lib_pic33e/usb/usb_device.c **** /********************************************************************
2234:lib/lib_pic33e/usb/usb_device.c ****  * Function:        void USBSuspend(void)
2235:lib/lib_pic33e/usb/usb_device.c ****  *
2236:lib/lib_pic33e/usb/usb_device.c ****  * PreCondition:    None
2237:lib/lib_pic33e/usb/usb_device.c ****  *
2238:lib/lib_pic33e/usb/usb_device.c ****  * Input:           None
2239:lib/lib_pic33e/usb/usb_device.c ****  *
2240:lib/lib_pic33e/usb/usb_device.c ****  * Output:          None
2241:lib/lib_pic33e/usb/usb_device.c ****  *
2242:lib/lib_pic33e/usb/usb_device.c ****  * Side Effects:    
2243:lib/lib_pic33e/usb/usb_device.c ****  *
2244:lib/lib_pic33e/usb/usb_device.c ****  * Overview:        This function handles if the host tries to 
2245:lib/lib_pic33e/usb/usb_device.c ****  *                  suspend the device
2246:lib/lib_pic33e/usb/usb_device.c ****  *
2247:lib/lib_pic33e/usb/usb_device.c ****  * Note:            None
2248:lib/lib_pic33e/usb/usb_device.c ****  *******************************************************************/
2249:lib/lib_pic33e/usb/usb_device.c **** static void USBSuspend(void)
2250:lib/lib_pic33e/usb/usb_device.c **** {
 3064              	.loc 1 2250 0
 3065              	.set ___PA___,1
 3066 00102c  00 00 FA 	lnk #0
 3067              	.LCFI21:
2251:lib/lib_pic33e/usb/usb_device.c ****     /*
2252:lib/lib_pic33e/usb/usb_device.c ****      * NOTE: Do not clear UIRbits.ACTVIF here!
2253:lib/lib_pic33e/usb/usb_device.c ****      * Reason:
2254:lib/lib_pic33e/usb/usb_device.c ****      * ACTVIF is only generated once an IDLEIF has been generated.
2255:lib/lib_pic33e/usb/usb_device.c ****      * This is a 1:1 ratio interrupt generation.
2256:lib/lib_pic33e/usb/usb_device.c ****      * For every IDLEIF, there will be only one ACTVIF regardless of
2257:lib/lib_pic33e/usb/usb_device.c ****      * the number of subsequent bus transitions.
2258:lib/lib_pic33e/usb/usb_device.c ****      *
2259:lib/lib_pic33e/usb/usb_device.c ****      * If the ACTIF is cleared here, a problem could occur when:
2260:lib/lib_pic33e/usb/usb_device.c ****      * [       IDLE       ][bus activity ->
2261:lib/lib_pic33e/usb/usb_device.c ****      * <--- 3 ms ----->     ^
2262:lib/lib_pic33e/usb/usb_device.c ****      *                ^     ACTVIF=1
2263:lib/lib_pic33e/usb/usb_device.c ****      *                IDLEIF=1
2264:lib/lib_pic33e/usb/usb_device.c ****      *  #           #           #           #   (#=Program polling flags)
2265:lib/lib_pic33e/usb/usb_device.c ****      *                          ^
MPLAB XC16 ASSEMBLY Listing:   			page 95


2266:lib/lib_pic33e/usb/usb_device.c ****      *                          This polling loop will see both
2267:lib/lib_pic33e/usb/usb_device.c ****      *                          IDLEIF=1 and ACTVIF=1.
2268:lib/lib_pic33e/usb/usb_device.c ****      *                          However, the program services IDLEIF first
2269:lib/lib_pic33e/usb/usb_device.c ****      *                          because ACTIVIE=0.
2270:lib/lib_pic33e/usb/usb_device.c ****      *                          If this routine clears the only ACTIVIF,
2271:lib/lib_pic33e/usb/usb_device.c ****      *                          then it can never get out of the suspend
2272:lib/lib_pic33e/usb/usb_device.c ****      *                          mode.
2273:lib/lib_pic33e/usb/usb_device.c ****      */
2274:lib/lib_pic33e/usb/usb_device.c ****     USBActivityIE = 1;                     // Enable bus activity interrupt
 3068              	.loc 1 2274 0
 3069 00102e  00 80 A8 	bset.b _U1OTGIEbits,#4
2275:lib/lib_pic33e/usb/usb_device.c ****     USBClearInterruptFlag(USBIdleIFReg,USBIdleIFBitNum);
 3070              	.loc 1 2275 0
 3071 001030  01 01 20 	mov #16,w1
2276:lib/lib_pic33e/usb/usb_device.c **** 
2277:lib/lib_pic33e/usb/usb_device.c ****     #if defined(__18CXX) || defined(_PIC14E) || defined(__XC8)
2278:lib/lib_pic33e/usb/usb_device.c ****         U1CONbits.SUSPND = 1;                   // Put USB module in power conserve
2279:lib/lib_pic33e/usb/usb_device.c ****                                                 // mode, SIE clock inactive
2280:lib/lib_pic33e/usb/usb_device.c ****     #endif
2281:lib/lib_pic33e/usb/usb_device.c ****     USBBusIsSuspended = true;
 3072              	.loc 1 2281 0
 3073 001032  10 C0 B3 	mov.b #1,w0
 3074              	.loc 1 2275 0
 3075 001034  01 00 88 	mov w1,_U1IR
 3076              	.loc 1 2281 0
 3077 001036  00 E0 B7 	mov.b WREG,_USBBusIsSuspended
2282:lib/lib_pic33e/usb/usb_device.c ****     USBTicksSinceSuspendEnd = 0;
 3078              	.loc 1 2282 0
 3079 001038  00 60 EF 	clr.b _USBTicksSinceSuspendEnd
2283:lib/lib_pic33e/usb/usb_device.c ****  
2284:lib/lib_pic33e/usb/usb_device.c ****     /*
2285:lib/lib_pic33e/usb/usb_device.c ****      * At this point the PIC can go into sleep,idle, or
2286:lib/lib_pic33e/usb/usb_device.c ****      * switch to a slower clock, etc.  This should be done in the
2287:lib/lib_pic33e/usb/usb_device.c ****      * USBCBSuspend() if necessary.
2288:lib/lib_pic33e/usb/usb_device.c ****      */
2289:lib/lib_pic33e/usb/usb_device.c ****     USB_SUSPEND_HANDLER(EVENT_SUSPEND,0,0);
 3080              	.loc 1 2289 0
 3081 00103a  00 01 EB 	clr w2
 3082 00103c  80 00 EB 	clr w1
 3083 00103e  50 07 20 	mov #117,w0
 3084 001040  00 00 07 	rcall _USER_USB_CALLBACK_EVENT_HANDLER
2290:lib/lib_pic33e/usb/usb_device.c **** }
 3085              	.loc 1 2290 0
 3086 001042  8E 07 78 	mov w14,w15
 3087 001044  4F 07 78 	mov [--w15],w14
 3088 001046  00 40 A9 	bclr CORCON,#2
 3089 001048  00 00 06 	return 
 3090              	.set ___PA___,0
 3091              	.LFE18:
 3092              	.size _USBSuspend,.-_USBSuspend
 3093              	.align 2
 3094              	.type _USBWakeFromSuspend,@function
 3095              	_USBWakeFromSuspend:
 3096              	.LFB19:
2291:lib/lib_pic33e/usb/usb_device.c **** 
2292:lib/lib_pic33e/usb/usb_device.c **** /********************************************************************
2293:lib/lib_pic33e/usb/usb_device.c ****  * Function:        void USBWakeFromSuspend(void)
MPLAB XC16 ASSEMBLY Listing:   			page 96


2294:lib/lib_pic33e/usb/usb_device.c ****  *
2295:lib/lib_pic33e/usb/usb_device.c ****  * PreCondition:    None
2296:lib/lib_pic33e/usb/usb_device.c ****  *
2297:lib/lib_pic33e/usb/usb_device.c ****  * Input:           None
2298:lib/lib_pic33e/usb/usb_device.c ****  *
2299:lib/lib_pic33e/usb/usb_device.c ****  * Output:          None
2300:lib/lib_pic33e/usb/usb_device.c ****  *
2301:lib/lib_pic33e/usb/usb_device.c ****  * Side Effects:    None
2302:lib/lib_pic33e/usb/usb_device.c ****  *
2303:lib/lib_pic33e/usb/usb_device.c ****  * Overview:
2304:lib/lib_pic33e/usb/usb_device.c ****  *
2305:lib/lib_pic33e/usb/usb_device.c ****  * Note:            None
2306:lib/lib_pic33e/usb/usb_device.c ****  *******************************************************************/
2307:lib/lib_pic33e/usb/usb_device.c **** static void USBWakeFromSuspend(void)
2308:lib/lib_pic33e/usb/usb_device.c **** {
 3097              	.loc 1 2308 0
 3098              	.set ___PA___,1
 3099 00104a  00 00 FA 	lnk #0
 3100              	.LCFI22:
2309:lib/lib_pic33e/usb/usb_device.c ****     USBBusIsSuspended = false;
 3101              	.loc 1 2309 0
 3102 00104c  00 60 EF 	clr.b _USBBusIsSuspended
2310:lib/lib_pic33e/usb/usb_device.c **** 
2311:lib/lib_pic33e/usb/usb_device.c ****     /*
2312:lib/lib_pic33e/usb/usb_device.c ****      * If using clock switching, the place to restore the original
2313:lib/lib_pic33e/usb/usb_device.c ****      * microcontroller core clock frequency is in the USBCBWakeFromSuspend() callback
2314:lib/lib_pic33e/usb/usb_device.c ****      */
2315:lib/lib_pic33e/usb/usb_device.c ****     USB_WAKEUP_FROM_SUSPEND_HANDLER(EVENT_RESUME,0,0);
 3103              	.loc 1 2315 0
 3104 00104e  00 01 EB 	clr w2
 3105 001050  80 00 EB 	clr w1
 3106 001052  40 07 20 	mov #116,w0
 3107 001054  00 00 07 	rcall _USER_USB_CALLBACK_EVENT_HANDLER
2316:lib/lib_pic33e/usb/usb_device.c **** 
2317:lib/lib_pic33e/usb/usb_device.c ****     #if defined(__18CXX) || defined(_PIC14E) || defined(__XC8)
2318:lib/lib_pic33e/usb/usb_device.c ****         //To avoid improperly clocking the USB module, make sure the oscillator
2319:lib/lib_pic33e/usb/usb_device.c ****         //settings are consistent with USB operation before clearing the SUSPND bit.
2320:lib/lib_pic33e/usb/usb_device.c ****         //Make sure the correct oscillator settings are selected in the 
2321:lib/lib_pic33e/usb/usb_device.c ****         //"USB_WAKEUP_FROM_SUSPEND_HANDLER(EVENT_RESUME,0,0)" handler.
2322:lib/lib_pic33e/usb/usb_device.c ****         U1CONbits.SUSPND = 0;   // Bring USB module out of power conserve
2323:lib/lib_pic33e/usb/usb_device.c ****                                 // mode.
2324:lib/lib_pic33e/usb/usb_device.c ****     #endif
2325:lib/lib_pic33e/usb/usb_device.c **** 
2326:lib/lib_pic33e/usb/usb_device.c **** 
2327:lib/lib_pic33e/usb/usb_device.c ****     USBActivityIE = 0;
 3108              	.loc 1 2327 0
 3109 001056  00 80 A9 	bclr.b _U1OTGIEbits,#4
2328:lib/lib_pic33e/usb/usb_device.c **** 
2329:lib/lib_pic33e/usb/usb_device.c ****     /********************************************************************
2330:lib/lib_pic33e/usb/usb_device.c ****     Bug Fix: Feb 26, 2007 v2.1
2331:lib/lib_pic33e/usb/usb_device.c ****     *********************************************************************
2332:lib/lib_pic33e/usb/usb_device.c ****     The ACTVIF bit cannot be cleared immediately after the USB module wakes
2333:lib/lib_pic33e/usb/usb_device.c ****     up from Suspend or while the USB module is suspended. A few clock cycles
2334:lib/lib_pic33e/usb/usb_device.c ****     are required to synchronize the internal hardware state machine before
2335:lib/lib_pic33e/usb/usb_device.c ****     the ACTIVIF bit can be cleared by firmware. Clearing the ACTVIF bit
2336:lib/lib_pic33e/usb/usb_device.c ****     before the internal hardware is synchronized may not have an effect on
2337:lib/lib_pic33e/usb/usb_device.c ****     the value of ACTVIF. Additionally, if the USB module uses the clock from
MPLAB XC16 ASSEMBLY Listing:   			page 97


2338:lib/lib_pic33e/usb/usb_device.c ****     the 96 MHz PLL source, then after clearing the SUSPND bit, the USB
2339:lib/lib_pic33e/usb/usb_device.c ****     module may not be immediately operational while waiting for the 96 MHz
2340:lib/lib_pic33e/usb/usb_device.c ****     PLL to lock.
2341:lib/lib_pic33e/usb/usb_device.c ****     ********************************************************************/
2342:lib/lib_pic33e/usb/usb_device.c **** 
2343:lib/lib_pic33e/usb/usb_device.c ****     // UIRbits.ACTVIF = 0;                      // Removed
2344:lib/lib_pic33e/usb/usb_device.c ****     #if defined(__18CXX) || defined(__XC8)
2345:lib/lib_pic33e/usb/usb_device.c ****     while(USBActivityIF)
2346:lib/lib_pic33e/usb/usb_device.c ****     #endif
2347:lib/lib_pic33e/usb/usb_device.c ****     {
2348:lib/lib_pic33e/usb/usb_device.c ****         USBClearInterruptFlag(USBActivityIFReg,USBActivityIFBitNum);
 3110              	.loc 1 2348 0
 3111 001058  00 01 20 	mov #16,w0
 3112 00105a  00 00 88 	mov w0,_U1OTGIR
2349:lib/lib_pic33e/usb/usb_device.c ****     }  // Added
2350:lib/lib_pic33e/usb/usb_device.c **** 
2351:lib/lib_pic33e/usb/usb_device.c ****     USBTicksSinceSuspendEnd = 0;
 3113              	.loc 1 2351 0
 3114 00105c  00 60 EF 	clr.b _USBTicksSinceSuspendEnd
2352:lib/lib_pic33e/usb/usb_device.c **** 
2353:lib/lib_pic33e/usb/usb_device.c **** }//end USBWakeFromSuspend
 3115              	.loc 1 2353 0
 3116 00105e  8E 07 78 	mov w14,w15
 3117 001060  4F 07 78 	mov [--w15],w14
 3118 001062  00 40 A9 	bclr CORCON,#2
 3119 001064  00 00 06 	return 
 3120              	.set ___PA___,0
 3121              	.LFE19:
 3122              	.size _USBWakeFromSuspend,.-_USBWakeFromSuspend
 3123              	.align 2
 3124              	.type _USBCtrlEPService,@function
 3125              	_USBCtrlEPService:
 3126              	.LFB20:
2354:lib/lib_pic33e/usb/usb_device.c **** 
2355:lib/lib_pic33e/usb/usb_device.c **** /********************************************************************
2356:lib/lib_pic33e/usb/usb_device.c ****  * Function:        void USBCtrlEPService(void)
2357:lib/lib_pic33e/usb/usb_device.c ****  *
2358:lib/lib_pic33e/usb/usb_device.c ****  * PreCondition:    USTAT is loaded with a valid endpoint address.
2359:lib/lib_pic33e/usb/usb_device.c ****  *
2360:lib/lib_pic33e/usb/usb_device.c ****  * Input:           None
2361:lib/lib_pic33e/usb/usb_device.c ****  *
2362:lib/lib_pic33e/usb/usb_device.c ****  * Output:          None
2363:lib/lib_pic33e/usb/usb_device.c ****  *
2364:lib/lib_pic33e/usb/usb_device.c ****  * Side Effects:    None
2365:lib/lib_pic33e/usb/usb_device.c ****  *
2366:lib/lib_pic33e/usb/usb_device.c ****  * Overview:        USBCtrlEPService checks for three transaction
2367:lib/lib_pic33e/usb/usb_device.c ****  *                  types that it knows how to service and services
2368:lib/lib_pic33e/usb/usb_device.c ****  *                  them:
2369:lib/lib_pic33e/usb/usb_device.c ****  *                  1. EP0 SETUP
2370:lib/lib_pic33e/usb/usb_device.c ****  *                  2. EP0 OUT
2371:lib/lib_pic33e/usb/usb_device.c ****  *                  3. EP0 IN
2372:lib/lib_pic33e/usb/usb_device.c ****  *                  It ignores all other types (i.e. EP1, EP2, etc.)
2373:lib/lib_pic33e/usb/usb_device.c ****  *
2374:lib/lib_pic33e/usb/usb_device.c ****  * Note:            None
2375:lib/lib_pic33e/usb/usb_device.c ****  *******************************************************************/
2376:lib/lib_pic33e/usb/usb_device.c **** static void USBCtrlEPService(void)
2377:lib/lib_pic33e/usb/usb_device.c **** {
MPLAB XC16 ASSEMBLY Listing:   			page 98


 3127              	.loc 1 2377 0
 3128              	.set ___PA___,1
 3129 001066  00 00 FA 	lnk #0
 3130              	.LCFI23:
2378:lib/lib_pic33e/usb/usb_device.c ****     //If we get to here, that means a successful transaction has just occurred 
2379:lib/lib_pic33e/usb/usb_device.c ****     //on EP0.  This means "progress" has occurred in the currently pending 
2380:lib/lib_pic33e/usb/usb_device.c ****     //control transfer, so we should re-initialize our timeout counter.
2381:lib/lib_pic33e/usb/usb_device.c ****     #if defined(USB_ENABLE_STATUS_STAGE_TIMEOUTS)
2382:lib/lib_pic33e/usb/usb_device.c ****         USBStatusStageTimeoutCounter = USB_STATUS_STAGE_TIMEOUT;
 3131              	.loc 1 2382 0
 3132 001068  D0 C2 B3 	mov.b #45,w0
 3133 00106a  00 E0 B7 	mov.b WREG,_USBStatusStageTimeoutCounter
2383:lib/lib_pic33e/usb/usb_device.c ****     #endif
2384:lib/lib_pic33e/usb/usb_device.c **** 	
2385:lib/lib_pic33e/usb/usb_device.c **** 	//Check if the last transaction was on EP0 OUT endpoint (of any kind, to either the even or odd bu
2386:lib/lib_pic33e/usb/usb_device.c ****     if((USTATcopy.Val & USTAT_EP0_PP_MASK) == USTAT_EP0_OUT_EVEN)
 3134              	.loc 1 2386 0
 3135 00106c  00 C0 BF 	mov.b _USTATcopy,WREG
 3136 00106e  00 80 FB 	ze w0,w0
 3137 001070  00 20 A1 	bclr w0,#2
 3138 001072  00 00 E0 	cp0 w0
 3139              	.set ___BP___,0
 3140 001074  00 00 3A 	bra nz,.L119
2387:lib/lib_pic33e/usb/usb_device.c ****     {
2388:lib/lib_pic33e/usb/usb_device.c **** 		//Point to the EP0 OUT buffer of the buffer that arrived
2389:lib/lib_pic33e/usb/usb_device.c ****         #if defined (_PIC14E) || defined(__18CXX) || defined(__XC8)
2390:lib/lib_pic33e/usb/usb_device.c ****             pBDTEntryEP0OutCurrent = (volatile BDT_ENTRY*)&BDT[(USTATcopy.Val & USTAT_EP_MASK)>>1];
2391:lib/lib_pic33e/usb/usb_device.c ****         #elif defined(__C30__) || defined(__C32__) || defined __XC16__
2392:lib/lib_pic33e/usb/usb_device.c ****             pBDTEntryEP0OutCurrent = (volatile BDT_ENTRY*)&BDT[(USTATcopy.Val & USTAT_EP_MASK)>>2];
 3141              	.loc 1 2392 0
 3142 001076  01 00 20 	mov #_USTATcopy,w1
 3143 001078  00 00 00 	nop
 3144 00107a  91 40 78 	mov.b [w1],w1
 3145 00107c  00 00 20 	mov #_BDT,w0
 3146 00107e  81 80 FB 	ze w1,w1
 3147 001080  C2 08 DE 	lsr w1,#2,w1
 3148 001082  81 40 78 	mov.b w1,w1
 3149 001084  81 80 FB 	ze w1,w1
 3150 001086  C3 08 DD 	sl w1,#3,w1
 3151 001088  00 80 40 	add w1,w0,w0
 3152 00108a  00 00 88 	mov w0,_pBDTEntryEP0OutCurrent
2393:lib/lib_pic33e/usb/usb_device.c ****         #else
2394:lib/lib_pic33e/usb/usb_device.c ****             #error "unimplemented"
2395:lib/lib_pic33e/usb/usb_device.c ****         #endif
2396:lib/lib_pic33e/usb/usb_device.c **** 
2397:lib/lib_pic33e/usb/usb_device.c **** 		//Set the next out to the current out packet
2398:lib/lib_pic33e/usb/usb_device.c ****         pBDTEntryEP0OutNext = pBDTEntryEP0OutCurrent;
2399:lib/lib_pic33e/usb/usb_device.c **** 		//Toggle it to the next ping pong buffer (if applicable)
2400:lib/lib_pic33e/usb/usb_device.c ****         pBDTEntryEP0OutNext = (volatile BDT_ENTRY*)(((uintptr_t)pBDTEntryEP0OutNext) ^ USB_NEXT_EP0
2401:lib/lib_pic33e/usb/usb_device.c **** 
2402:lib/lib_pic33e/usb/usb_device.c **** 		//If the current EP0 OUT buffer has a SETUP packet
2403:lib/lib_pic33e/usb/usb_device.c ****         if(pBDTEntryEP0OutCurrent->STAT.PID == PID_SETUP)
 3153              	.loc 1 2403 0
 3154 00108c  01 00 80 	mov _pBDTEntryEP0OutCurrent,w1
 3155              	.loc 1 2398 0
 3156 00108e  02 00 80 	mov _pBDTEntryEP0OutCurrent,w2
 3157              	.loc 1 2403 0
MPLAB XC16 ASSEMBLY Listing:   			page 99


 3158 001090  40 C3 B3 	mov.b #52,w0
 3159              	.loc 1 2398 0
 3160 001092  02 00 88 	mov w2,_pBDTEntryEP0OutNext
 3161              	.loc 1 2400 0
 3162 001094  02 00 80 	mov _pBDTEntryEP0OutNext,w2
 3163 001096  02 30 A2 	btg w2,#3
 3164 001098  02 00 88 	mov w2,_pBDTEntryEP0OutNext
 3165              	.loc 1 2403 0
 3166 00109a  91 40 78 	mov.b [w1],w1
 3167 00109c  C1 43 B2 	and.b #60,w1
 3168 00109e  80 CF 50 	sub.b w1,w0,[w15]
 3169              	.set ___BP___,0
 3170 0010a0  00 00 3A 	bra nz,.L120
2404:lib/lib_pic33e/usb/usb_device.c ****         {
2405:lib/lib_pic33e/usb/usb_device.c **** 	        //The SETUP transaction data may have gone into the the CtrlTrfData 
2406:lib/lib_pic33e/usb/usb_device.c **** 	        //buffer, or elsewhere, depending upon how the BDT was prepared
2407:lib/lib_pic33e/usb/usb_device.c **** 	        //before the transaction.  Therefore, we should copy the data to the 
2408:lib/lib_pic33e/usb/usb_device.c **** 	        //SetupPkt buffer so it can be processed correctly by USBCtrlTrfSetupHandler().		    
2409:lib/lib_pic33e/usb/usb_device.c ****             memcpy((uint8_t*)&SetupPkt, (uint8_t*)ConvertToVirtualAddress(pBDTEntryEP0OutCurrent->A
 3171              	.loc 1 2409 0
 3172 0010a2  00 00 80 	mov _pBDTEntryEP0OutCurrent,w0
 3173 0010a4  40 41 90 	mov.b [w0+4],w2
 3174 0010a6  D0 40 90 	mov.b [w0+5],w1
 3175 0010a8  02 80 FB 	ze w2,w0
 3176 0010aa  81 80 FB 	ze w1,w1
 3177 0010ac  C8 08 DD 	sl w1,#8,w1
 3178 0010ae  01 00 70 	ior w0,w1,w0
 3179 0010b0  82 00 20 	mov #8,w2
 3180 0010b2  80 00 78 	mov w0,w1
 3181 0010b4  00 00 20 	mov #_SetupPkt,w0
 3182 0010b6  00 00 07 	rcall _memcpy
2410:lib/lib_pic33e/usb/usb_device.c **** 
2411:lib/lib_pic33e/usb/usb_device.c **** 			//Handle the control transfer (parse the 8-byte SETUP command and figure out what to do)
2412:lib/lib_pic33e/usb/usb_device.c ****             USBCtrlTrfSetupHandler();
 3183              	.loc 1 2412 0
 3184 0010b8  00 00 07 	rcall _USBCtrlTrfSetupHandler
 3185 0010ba  00 00 37 	bra .L118
 3186              	.L120:
2413:lib/lib_pic33e/usb/usb_device.c ****         }
2414:lib/lib_pic33e/usb/usb_device.c ****         else
2415:lib/lib_pic33e/usb/usb_device.c ****         {
2416:lib/lib_pic33e/usb/usb_device.c **** 			//Handle the DATA transfer
2417:lib/lib_pic33e/usb/usb_device.c ****             USBCtrlTrfOutHandler();
 3187              	.loc 1 2417 0
 3188 0010bc  00 00 07 	rcall _USBCtrlTrfOutHandler
 3189 0010be  00 00 37 	bra .L118
 3190              	.L119:
2418:lib/lib_pic33e/usb/usb_device.c ****         }
2419:lib/lib_pic33e/usb/usb_device.c ****     }
2420:lib/lib_pic33e/usb/usb_device.c ****     else if((USTATcopy.Val & USTAT_EP0_PP_MASK) == USTAT_EP0_IN)
 3191              	.loc 1 2420 0
 3192 0010c0  00 C0 BF 	mov.b _USTATcopy,WREG
 3193 0010c2  00 80 FB 	ze w0,w0
 3194 0010c4  00 20 A1 	bclr w0,#2
 3195 0010c6  E8 0F 50 	sub w0,#8,[w15]
 3196              	.set ___BP___,0
 3197 0010c8  00 00 3A 	bra nz,.L118
MPLAB XC16 ASSEMBLY Listing:   			page 100


2421:lib/lib_pic33e/usb/usb_device.c ****     {
2422:lib/lib_pic33e/usb/usb_device.c **** 		//Otherwise the transmission was and EP0 IN
2423:lib/lib_pic33e/usb/usb_device.c **** 		//  so take care of the IN transfer
2424:lib/lib_pic33e/usb/usb_device.c ****         USBCtrlTrfInHandler();
 3198              	.loc 1 2424 0
 3199 0010ca  00 00 07 	rcall _USBCtrlTrfInHandler
 3200              	.L118:
2425:lib/lib_pic33e/usb/usb_device.c ****     }
2426:lib/lib_pic33e/usb/usb_device.c **** 
2427:lib/lib_pic33e/usb/usb_device.c **** }//end USBCtrlEPService
 3201              	.loc 1 2427 0
 3202 0010cc  8E 07 78 	mov w14,w15
 3203 0010ce  4F 07 78 	mov [--w15],w14
 3204 0010d0  00 40 A9 	bclr CORCON,#2
 3205 0010d2  00 00 06 	return 
 3206              	.set ___PA___,0
 3207              	.LFE20:
 3208              	.size _USBCtrlEPService,.-_USBCtrlEPService
 3209              	.align 2
 3210              	.type _USBCtrlTrfSetupHandler,@function
 3211              	_USBCtrlTrfSetupHandler:
 3212              	.LFB21:
2428:lib/lib_pic33e/usb/usb_device.c **** 
2429:lib/lib_pic33e/usb/usb_device.c **** /********************************************************************
2430:lib/lib_pic33e/usb/usb_device.c ****  * Function:        void USBCtrlTrfSetupHandler(void)
2431:lib/lib_pic33e/usb/usb_device.c ****  *
2432:lib/lib_pic33e/usb/usb_device.c ****  * PreCondition:    SetupPkt buffer is loaded with valid USB Setup Data
2433:lib/lib_pic33e/usb/usb_device.c ****  *
2434:lib/lib_pic33e/usb/usb_device.c ****  * Input:           None
2435:lib/lib_pic33e/usb/usb_device.c ****  *
2436:lib/lib_pic33e/usb/usb_device.c ****  * Output:          None
2437:lib/lib_pic33e/usb/usb_device.c ****  *
2438:lib/lib_pic33e/usb/usb_device.c ****  * Side Effects:    None
2439:lib/lib_pic33e/usb/usb_device.c ****  *
2440:lib/lib_pic33e/usb/usb_device.c ****  * Overview:        This routine is a task dispatcher and has 3 stages.
2441:lib/lib_pic33e/usb/usb_device.c ****  *                  1. It initializes the control transfer state machine.
2442:lib/lib_pic33e/usb/usb_device.c ****  *                  2. It calls on each of the module that may know how to
2443:lib/lib_pic33e/usb/usb_device.c ****  *                     service the Setup Request from the host.
2444:lib/lib_pic33e/usb/usb_device.c ****  *                     Module Example: USBD, HID, CDC, MSD, ...
2445:lib/lib_pic33e/usb/usb_device.c ****  *                     A callback function, USBCBCheckOtherReq(),
2446:lib/lib_pic33e/usb/usb_device.c ****  *                     is required to call other module handlers.
2447:lib/lib_pic33e/usb/usb_device.c ****  *                  3. Once each of the modules has had a chance to check if
2448:lib/lib_pic33e/usb/usb_device.c ****  *                     it is responsible for servicing the request, stage 3
2449:lib/lib_pic33e/usb/usb_device.c ****  *                     then checks direction of the transfer to determine how
2450:lib/lib_pic33e/usb/usb_device.c ****  *                     to prepare EP0 for the control transfer.
2451:lib/lib_pic33e/usb/usb_device.c ****  *                     Refer to USBCtrlEPServiceComplete() for more details.
2452:lib/lib_pic33e/usb/usb_device.c ****  *
2453:lib/lib_pic33e/usb/usb_device.c ****  * Note:            Microchip USB Firmware has three different states for
2454:lib/lib_pic33e/usb/usb_device.c ****  *                  the control transfer state machine:
2455:lib/lib_pic33e/usb/usb_device.c ****  *                  1. WAIT_SETUP
2456:lib/lib_pic33e/usb/usb_device.c ****  *                  2. CTRL_TRF_TX (device sends data to host through IN transactions)
2457:lib/lib_pic33e/usb/usb_device.c ****  *                  3. CTRL_TRF_RX (device receives data from host through OUT transactions)
2458:lib/lib_pic33e/usb/usb_device.c ****  *                  Refer to firmware manual to find out how one state
2459:lib/lib_pic33e/usb/usb_device.c ****  *                  is transitioned to another.
2460:lib/lib_pic33e/usb/usb_device.c ****  *
2461:lib/lib_pic33e/usb/usb_device.c ****  *                  A Control Transfer is composed of many USB transactions.
2462:lib/lib_pic33e/usb/usb_device.c ****  *                  When transferring data over multiple transactions,
MPLAB XC16 ASSEMBLY Listing:   			page 101


2463:lib/lib_pic33e/usb/usb_device.c ****  *                  it is important to keep track of data source, data
2464:lib/lib_pic33e/usb/usb_device.c ****  *                  destination, and data count. These three parameters are
2465:lib/lib_pic33e/usb/usb_device.c ****  *                  stored in pSrc,pDst, and wCount. A flag is used to
2466:lib/lib_pic33e/usb/usb_device.c ****  *                  note if the data source is from const or RAM.
2467:lib/lib_pic33e/usb/usb_device.c ****  *
2468:lib/lib_pic33e/usb/usb_device.c ****  *******************************************************************/
2469:lib/lib_pic33e/usb/usb_device.c **** static void USBCtrlTrfSetupHandler(void)
2470:lib/lib_pic33e/usb/usb_device.c **** {
 3213              	.loc 1 2470 0
 3214              	.set ___PA___,1
 3215 0010d4  00 00 FA 	lnk #0
 3216              	.LCFI24:
2471:lib/lib_pic33e/usb/usb_device.c ****     //--------------------------------------------------------------------------
2472:lib/lib_pic33e/usb/usb_device.c ****     //1. Re-initialize state tracking variables related to control transfers.
2473:lib/lib_pic33e/usb/usb_device.c ****     //--------------------------------------------------------------------------
2474:lib/lib_pic33e/usb/usb_device.c ****     shortPacketStatus = SHORT_PKT_NOT_USED;  
2475:lib/lib_pic33e/usb/usb_device.c ****     USBDeferStatusStagePacket = false;
2476:lib/lib_pic33e/usb/usb_device.c ****     USBDeferINDataStagePackets = false;
2477:lib/lib_pic33e/usb/usb_device.c ****     USBDeferOUTDataStagePackets = false;
2478:lib/lib_pic33e/usb/usb_device.c ****     BothEP0OutUOWNsSet = false;
2479:lib/lib_pic33e/usb/usb_device.c ****     controlTransferState = WAIT_SETUP;
2480:lib/lib_pic33e/usb/usb_device.c **** 
2481:lib/lib_pic33e/usb/usb_device.c ****     //Abandon any previous control transfers that might have been using EP0.
2482:lib/lib_pic33e/usb/usb_device.c ****     //Ordinarily, nothing actually needs abandoning, since the previous control
2483:lib/lib_pic33e/usb/usb_device.c ****     //transfer would have completed successfully prior to the host sending the next
2484:lib/lib_pic33e/usb/usb_device.c ****     //SETUP packet.  However, in a timeout error case, or after an EP0 STALL event,
2485:lib/lib_pic33e/usb/usb_device.c ****     //one or more UOWN bits might still be set.  If so, we should clear the UOWN bits,
2486:lib/lib_pic33e/usb/usb_device.c ****     //so the EP0 IN/OUT endpoints are in a known inactive state, ready for re-arming
2487:lib/lib_pic33e/usb/usb_device.c ****     //by the class request handler that will be called next.
2488:lib/lib_pic33e/usb/usb_device.c ****     pBDTEntryIn[0]->STAT.Val &= ~(_USIE);  
 3217              	.loc 1 2488 0
 3218 0010d6  00 00 80 	mov _pBDTEntryIn,w0
 3219 0010d8  01 00 80 	mov _pBDTEntryIn,w1
 3220              	.loc 1 2474 0
 3221 0010da  00 60 EF 	clr.b _shortPacketStatus
 3222              	.loc 1 2488 0
 3223 0010dc  F2 0F 20 	mov #255,w2
 3224              	.loc 1 2475 0
 3225 0010de  00 60 EF 	clr.b _USBDeferStatusStagePacket
 3226              	.loc 1 2476 0
 3227 0010e0  00 60 EF 	clr.b _USBDeferINDataStagePackets
 3228              	.loc 1 2477 0
 3229 0010e2  00 60 EF 	clr.b _USBDeferOUTDataStagePackets
 3230              	.loc 1 2478 0
 3231 0010e4  00 60 EF 	clr.b _BothEP0OutUOWNsSet
 3232              	.loc 1 2479 0
 3233 0010e6  00 60 EF 	clr.b _controlTransferState
 3234              	.loc 1 2488 0
 3235 0010e8  11 82 FB 	ze [w1],w4
 3236 0010ea  91 40 90 	mov.b [w1+1],w1
 3237 0010ec  90 41 78 	mov.b [w0],w3
 3238 0010ee  81 80 FB 	ze w1,w1
 3239 0010f0  E0 C1 61 	and.b w3,#0,w3
 3240 0010f2  C8 08 DD 	sl w1,#8,w1
 3241 0010f4  81 00 72 	ior w4,w1,w1
 3242 0010f6  01 70 A1 	bclr w1,#7
 3243 0010f8  02 81 60 	and w1,w2,w2
MPLAB XC16 ASSEMBLY Listing:   			page 102


 3244 0010fa  02 C1 71 	ior.b w3,w2,w2
 3245 0010fc  02 48 78 	mov.b w2,[w0]
 3246 0010fe  C8 08 DE 	lsr w1,#8,w1
 3247 001100  10 41 90 	mov.b [w0+1],w2
 3248 001102  60 41 61 	and.b w2,#0,w2
 3249 001104  81 40 71 	ior.b w2,w1,w1
 3250 001106  11 40 98 	mov.b w1,[w0+1]
2489:lib/lib_pic33e/usb/usb_device.c ****     
2490:lib/lib_pic33e/usb/usb_device.c ****     pBDTEntryIn[0] = (volatile BDT_ENTRY*)(((uintptr_t)pBDTEntryIn[0]) ^ USB_NEXT_EP0_IN_PING_PONG)
 3251              	.loc 1 2490 0
 3252 001108  00 00 80 	mov _pBDTEntryIn,w0
 3253 00110a  00 30 A2 	btg w0,#3
 3254 00110c  00 00 88 	mov w0,_pBDTEntryIn
2491:lib/lib_pic33e/usb/usb_device.c ****     pBDTEntryIn[0]->STAT.Val &= ~(_USIE);      
 3255              	.loc 1 2491 0
 3256 00110e  00 00 80 	mov _pBDTEntryIn,w0
 3257 001110  01 00 80 	mov _pBDTEntryIn,w1
 3258 001112  F2 0F 20 	mov #255,w2
 3259 001114  11 82 FB 	ze [w1],w4
 3260 001116  91 40 90 	mov.b [w1+1],w1
 3261 001118  90 41 78 	mov.b [w0],w3
 3262 00111a  81 80 FB 	ze w1,w1
 3263 00111c  E0 C1 61 	and.b w3,#0,w3
 3264 00111e  C8 08 DD 	sl w1,#8,w1
 3265 001120  81 00 72 	ior w4,w1,w1
 3266 001122  01 70 A1 	bclr w1,#7
 3267 001124  02 81 60 	and w1,w2,w2
 3268 001126  02 C1 71 	ior.b w3,w2,w2
 3269 001128  02 48 78 	mov.b w2,[w0]
 3270 00112a  C8 08 DE 	lsr w1,#8,w1
 3271 00112c  10 41 90 	mov.b [w0+1],w2
 3272 00112e  60 41 61 	and.b w2,#0,w2
 3273 001130  81 40 71 	ior.b w2,w1,w1
 3274 001132  11 40 98 	mov.b w1,[w0+1]
2492:lib/lib_pic33e/usb/usb_device.c ****     pBDTEntryIn[0] = (volatile BDT_ENTRY*)(((uintptr_t)pBDTEntryIn[0]) ^ USB_NEXT_EP0_IN_PING_PONG)
2493:lib/lib_pic33e/usb/usb_device.c ****     pBDTEntryEP0OutNext->STAT.Val &= ~(_USIE);         
 3275              	.loc 1 2493 0
 3276 001134  00 00 80 	mov _pBDTEntryEP0OutNext,w0
 3277 001136  03 00 80 	mov _pBDTEntryEP0OutNext,w3
 3278              	.loc 1 2492 0
 3279 001138  04 00 80 	mov _pBDTEntryIn,w4
2494:lib/lib_pic33e/usb/usb_device.c **** 
2495:lib/lib_pic33e/usb/usb_device.c ****     inPipes[0].info.Val = 0;
2496:lib/lib_pic33e/usb/usb_device.c ****     inPipes[0].wCount.Val = 0;
2497:lib/lib_pic33e/usb/usb_device.c ****     outPipes[0].info.Val = 0;
2498:lib/lib_pic33e/usb/usb_device.c ****     outPipes[0].wCount.Val = 0;
 3280              	.loc 1 2498 0
 3281 00113a  32 00 20 	mov #_outPipes+3,w2
 3282 00113c  41 00 20 	mov #_outPipes+4,w1
 3283              	.loc 1 2492 0
 3284 00113e  84 02 78 	mov w4,w5
 3285              	.loc 1 2493 0
 3286 001140  F4 0F 20 	mov #255,w4
 3287              	.loc 1 2492 0
 3288 001142  05 30 A2 	btg w5,#3
 3289 001144  05 00 88 	mov w5,_pBDTEntryIn
 3290              	.loc 1 2493 0
MPLAB XC16 ASSEMBLY Listing:   			page 103


 3291 001146  13 83 FB 	ze [w3],w6
 3292 001148  93 41 90 	mov.b [w3+1],w3
 3293 00114a  90 42 78 	mov.b [w0],w5
 3294 00114c  83 81 FB 	ze w3,w3
 3295 00114e  E0 C2 62 	and.b w5,#0,w5
 3296 001150  C8 19 DD 	sl w3,#8,w3
 3297 001152  83 01 73 	ior w6,w3,w3
 3298 001154  03 70 A1 	bclr w3,#7
 3299 001156  04 82 61 	and w3,w4,w4
 3300 001158  04 C2 72 	ior.b w5,w4,w4
 3301 00115a  04 48 78 	mov.b w4,[w0]
 3302 00115c  48 1A DE 	lsr w3,#8,w4
 3303 00115e  90 42 90 	mov.b [w0+1],w5
 3304              	.loc 1 2495 0
 3305 001160  23 00 20 	mov #_inPipes+2,w3
 3306              	.loc 1 2493 0
 3307 001162  60 C3 62 	and.b w5,#0,w6
 3308              	.loc 1 2495 0
 3309 001164  80 42 EB 	clr.b w5
 3310              	.loc 1 2493 0
 3311 001166  04 43 73 	ior.b w6,w4,w6
 3312              	.loc 1 2496 0
 3313 001168  00 02 EB 	clr w4
 3314              	.loc 1 2493 0
 3315 00116a  16 40 98 	mov.b w6,[w0+1]
 3316              	.loc 1 2497 0
 3317 00116c  20 00 20 	mov #_outPipes+2,w0
 3318              	.loc 1 2495 0
 3319 00116e  85 49 78 	mov.b w5,[w3]
 3320              	.loc 1 2497 0
 3321 001170  80 41 EB 	clr.b w3
 3322              	.loc 1 2496 0
 3323 001172  24 00 88 	mov w4,_inPipes+4
 3324              	.loc 1 2497 0
 3325 001174  03 48 78 	mov.b w3,[w0]
 3326              	.loc 1 2498 0
 3327 001176  12 40 78 	mov.b [w2],w0
 3328 001178  60 40 60 	and.b w0,#0,w0
 3329 00117a  00 49 78 	mov.b w0,[w2]
 3330 00117c  11 40 78 	mov.b [w1],w0
 3331 00117e  60 40 60 	and.b w0,#0,w0
 3332 001180  80 48 78 	mov.b w0,[w1]
2499:lib/lib_pic33e/usb/usb_device.c ****     
2500:lib/lib_pic33e/usb/usb_device.c **** 
2501:lib/lib_pic33e/usb/usb_device.c ****     //--------------------------------------------------------------------------
2502:lib/lib_pic33e/usb/usb_device.c ****     //2. Now find out what was in the SETUP packet, and begin handling the request.
2503:lib/lib_pic33e/usb/usb_device.c ****     //--------------------------------------------------------------------------
2504:lib/lib_pic33e/usb/usb_device.c ****     USBCheckStdRequest();                                               //Check for standard USB "C
 3333              	.loc 1 2504 0
 3334 001182  00 00 07 	rcall _USBCheckStdRequest
2505:lib/lib_pic33e/usb/usb_device.c ****     USB_NONSTANDARD_EP0_REQUEST_HANDLER(EVENT_EP0_REQUEST,0,0); //Check for USB device class specif
 3335              	.loc 1 2505 0
 3336 001184  00 01 EB 	clr w2
 3337 001186  80 00 EB 	clr w1
 3338 001188  30 00 20 	mov #3,w0
 3339 00118a  00 00 07 	rcall _USER_USB_CALLBACK_EVENT_HANDLER
2506:lib/lib_pic33e/usb/usb_device.c **** 
MPLAB XC16 ASSEMBLY Listing:   			page 104


2507:lib/lib_pic33e/usb/usb_device.c **** 
2508:lib/lib_pic33e/usb/usb_device.c ****     //--------------------------------------------------------------------------
2509:lib/lib_pic33e/usb/usb_device.c ****     //3. Re-arm EP0 IN and EP0 OUT endpoints, based on the control transfer in 
2510:lib/lib_pic33e/usb/usb_device.c ****     //   progress.  If one of the above handlers (in step 2) knew how to process
2511:lib/lib_pic33e/usb/usb_device.c ****     //   the request, it will have set one of the inPipes[0].info.bits.busy or
2512:lib/lib_pic33e/usb/usb_device.c ****     //   outPipes[0].info.bits.busy flags = 1.  This lets the
2513:lib/lib_pic33e/usb/usb_device.c ****     //   USBCtrlEPServiceComplete() function know how and which endpoints to 
2514:lib/lib_pic33e/usb/usb_device.c ****     //   arm.  If both info.bits.busy flags are = 0, then no one knew how to
2515:lib/lib_pic33e/usb/usb_device.c ****     //   process the request.  In this case, the default behavior will be to
2516:lib/lib_pic33e/usb/usb_device.c ****     //   perform protocol STALL on EP0.
2517:lib/lib_pic33e/usb/usb_device.c ****     //-------------------------------------------------------------------------- 
2518:lib/lib_pic33e/usb/usb_device.c ****     USBCtrlEPServiceComplete();
 3340              	.loc 1 2518 0
 3341 00118c  00 00 07 	rcall _USBCtrlEPServiceComplete
2519:lib/lib_pic33e/usb/usb_device.c **** }//end USBCtrlTrfSetupHandler
 3342              	.loc 1 2519 0
 3343 00118e  8E 07 78 	mov w14,w15
 3344 001190  4F 07 78 	mov [--w15],w14
 3345 001192  00 40 A9 	bclr CORCON,#2
 3346 001194  00 00 06 	return 
 3347              	.set ___PA___,0
 3348              	.LFE21:
 3349              	.size _USBCtrlTrfSetupHandler,.-_USBCtrlTrfSetupHandler
 3350              	.align 2
 3351              	.type _USBCtrlTrfOutHandler,@function
 3352              	_USBCtrlTrfOutHandler:
 3353              	.LFB22:
2520:lib/lib_pic33e/usb/usb_device.c **** 
2521:lib/lib_pic33e/usb/usb_device.c **** 
2522:lib/lib_pic33e/usb/usb_device.c **** /******************************************************************************
2523:lib/lib_pic33e/usb/usb_device.c ****  * Function:        void USBCtrlTrfOutHandler(void)
2524:lib/lib_pic33e/usb/usb_device.c ****  *
2525:lib/lib_pic33e/usb/usb_device.c ****  * PreCondition:    None
2526:lib/lib_pic33e/usb/usb_device.c ****  *
2527:lib/lib_pic33e/usb/usb_device.c ****  * Input:           None
2528:lib/lib_pic33e/usb/usb_device.c ****  *
2529:lib/lib_pic33e/usb/usb_device.c ****  * Output:          None
2530:lib/lib_pic33e/usb/usb_device.c ****  *
2531:lib/lib_pic33e/usb/usb_device.c ****  * Side Effects:    None
2532:lib/lib_pic33e/usb/usb_device.c ****  *
2533:lib/lib_pic33e/usb/usb_device.c ****  * Overview:        This routine handles an OUT transaction according to
2534:lib/lib_pic33e/usb/usb_device.c ****  *                  which control transfer state is currently active.
2535:lib/lib_pic33e/usb/usb_device.c ****  *
2536:lib/lib_pic33e/usb/usb_device.c ****  * Note:            Note that if the the control transfer was from
2537:lib/lib_pic33e/usb/usb_device.c ****  *                  host to device, the session owner should be notified
2538:lib/lib_pic33e/usb/usb_device.c ****  *                  at the end of each OUT transaction to service the
2539:lib/lib_pic33e/usb/usb_device.c ****  *                  received data.
2540:lib/lib_pic33e/usb/usb_device.c ****  *
2541:lib/lib_pic33e/usb/usb_device.c ****  *****************************************************************************/
2542:lib/lib_pic33e/usb/usb_device.c **** static void USBCtrlTrfOutHandler(void)
2543:lib/lib_pic33e/usb/usb_device.c **** {
 3354              	.loc 1 2543 0
 3355              	.set ___PA___,1
 3356 001196  00 00 FA 	lnk #0
 3357              	.LCFI25:
2544:lib/lib_pic33e/usb/usb_device.c ****     if(controlTransferState == CTRL_TRF_RX)
 3358              	.loc 1 2544 0
MPLAB XC16 ASSEMBLY Listing:   			page 105


 3359 001198  00 C0 BF 	mov.b _controlTransferState,WREG
 3360 00119a  E2 4F 50 	sub.b w0,#2,[w15]
 3361              	.set ___BP___,0
 3362 00119c  00 00 3A 	bra nz,.L124
2545:lib/lib_pic33e/usb/usb_device.c ****     {
2546:lib/lib_pic33e/usb/usb_device.c ****         USBCtrlTrfRxService();	//Copies the newly received data into the appropriate buffer and con
 3363              	.loc 1 2546 0
 3364 00119e  00 00 07 	rcall _USBCtrlTrfRxService
 3365 0011a0  00 00 37 	bra .L123
 3366              	.L124:
2547:lib/lib_pic33e/usb/usb_device.c ****     }
2548:lib/lib_pic33e/usb/usb_device.c ****     else //In this case the last OUT transaction must have been a status stage of a CTRL_TRF_TX (<s
2549:lib/lib_pic33e/usb/usb_device.c ****     {
2550:lib/lib_pic33e/usb/usb_device.c ****         //If the status stage is complete, this means we are done with the 
2551:lib/lib_pic33e/usb/usb_device.c ****         //control transfer.  Go back to the idle "WAIT_SETUP" state.
2552:lib/lib_pic33e/usb/usb_device.c ****         controlTransferState = WAIT_SETUP;
 3367              	.loc 1 2552 0
 3368 0011a2  00 60 EF 	clr.b _controlTransferState
2553:lib/lib_pic33e/usb/usb_device.c **** 
2554:lib/lib_pic33e/usb/usb_device.c ****         //Prepare EP0 OUT for the next SETUP transaction, however, it may have
2555:lib/lib_pic33e/usb/usb_device.c ****         //already been prepared if ping-pong buffering was enabled on EP0 OUT,
2556:lib/lib_pic33e/usb/usb_device.c ****         //and the last control transfer was of direction: device to host, see
2557:lib/lib_pic33e/usb/usb_device.c ****         //USBCtrlEPServiceComplete().  If it was already prepared, do not want
2558:lib/lib_pic33e/usb/usb_device.c ****         //to do anything to the BDT.
2559:lib/lib_pic33e/usb/usb_device.c ****         if(BothEP0OutUOWNsSet == false)
 3369              	.loc 1 2559 0
 3370 0011a4  00 C0 BF 	mov.b _BothEP0OutUOWNsSet,WREG
 3371 0011a6  00 04 A2 	btg.b w0,#0
 3372 0011a8  00 04 E0 	cp0.b w0
 3373              	.set ___BP___,0
 3374 0011aa  00 00 32 	bra z,.L126
2560:lib/lib_pic33e/usb/usb_device.c ****         {
2561:lib/lib_pic33e/usb/usb_device.c ****             pBDTEntryEP0OutNext->CNT = USB_EP0_BUFF_SIZE;
 3375              	.loc 1 2561 0
 3376 0011ac  00 00 80 	mov _pBDTEntryEP0OutNext,w0
 3377 0011ae  C1 CF B3 	mov.b #-4,w1
 3378 0011b0  20 41 90 	mov.b [w0+2],w2
 3379 0011b2  60 41 61 	and.b w2,#0,w2
 3380 0011b4  02 34 A0 	bset.b w2,#3
 3381 0011b6  22 40 98 	mov.b w2,[w0+2]
 3382 0011b8  30 41 90 	mov.b [w0+3],w2
 3383 0011ba  81 40 61 	and.b w2,w1,w1
 3384 0011bc  31 40 98 	mov.b w1,[w0+3]
2562:lib/lib_pic33e/usb/usb_device.c ****             pBDTEntryEP0OutNext->ADR = ConvertToPhysicalAddress(&SetupPkt);
 3385              	.loc 1 2562 0
 3386 0011be  00 00 80 	mov _pBDTEntryEP0OutNext,w0
 3387 0011c0  01 00 20 	mov #_SetupPkt,w1
 3388 0011c2  C0 41 90 	mov.b [w0+4],w3
 3389 0011c4  F2 0F 20 	mov #255,w2
 3390 0011c6  E0 C1 61 	and.b w3,#0,w3
 3391 0011c8  02 81 60 	and w1,w2,w2
 3392 0011ca  C8 08 DE 	lsr w1,#8,w1
 3393 0011cc  02 C1 71 	ior.b w3,w2,w2
 3394 0011ce  42 40 98 	mov.b w2,[w0+4]
 3395 0011d0  50 41 90 	mov.b [w0+5],w2
 3396 0011d2  60 41 61 	and.b w2,#0,w2
 3397 0011d4  81 40 71 	ior.b w2,w1,w1
MPLAB XC16 ASSEMBLY Listing:   			page 106


 3398 0011d6  51 40 98 	mov.b w1,[w0+5]
2563:lib/lib_pic33e/usb/usb_device.c ****             pBDTEntryEP0OutNext->STAT.Val = _DAT0|(_DTSEN & _DTS_CHECKING_ENABLED)|_BSTALL;
 3399              	.loc 1 2563 0
 3400 0011d8  00 00 80 	mov _pBDTEntryEP0OutNext,w0
2564:lib/lib_pic33e/usb/usb_device.c ****             pBDTEntryEP0OutNext->STAT.Val |= _USIE;
 3401              	.loc 1 2564 0
 3402 0011da  F2 0F 20 	mov #255,w2
 3403              	.loc 1 2563 0
 3404 0011dc  90 40 78 	mov.b [w0],w1
 3405 0011de  E0 C0 60 	and.b w1,#0,w1
 3406 0011e0  C1 40 B3 	ior.b #12,w1
 3407 0011e2  01 48 78 	mov.b w1,[w0]
 3408 0011e4  90 40 90 	mov.b [w0+1],w1
 3409 0011e6  E0 C0 60 	and.b w1,#0,w1
 3410 0011e8  11 40 98 	mov.b w1,[w0+1]
 3411              	.loc 1 2564 0
 3412 0011ea  00 00 80 	mov _pBDTEntryEP0OutNext,w0
 3413 0011ec  01 00 80 	mov _pBDTEntryEP0OutNext,w1
 3414 0011ee  11 82 FB 	ze [w1],w4
 3415 0011f0  00 00 00 	nop 
 3416 0011f2  91 40 90 	mov.b [w1+1],w1
 3417 0011f4  90 41 78 	mov.b [w0],w3
 3418 0011f6  81 80 FB 	ze w1,w1
 3419 0011f8  E0 C1 61 	and.b w3,#0,w3
 3420 0011fa  C8 08 DD 	sl w1,#8,w1
 3421 0011fc  81 00 72 	ior w4,w1,w1
 3422 0011fe  01 70 A0 	bset w1,#7
 3423 001200  02 81 60 	and w1,w2,w2
 3424 001202  02 C1 71 	ior.b w3,w2,w2
 3425 001204  02 48 78 	mov.b w2,[w0]
 3426 001206  C8 08 DE 	lsr w1,#8,w1
 3427 001208  10 41 90 	mov.b [w0+1],w2
 3428 00120a  60 41 61 	and.b w2,#0,w2
 3429 00120c  81 40 71 	ior.b w2,w1,w1
 3430 00120e  11 40 98 	mov.b w1,[w0+1]
 3431 001210  00 00 37 	bra .L123
 3432              	.L126:
2565:lib/lib_pic33e/usb/usb_device.c ****         }
2566:lib/lib_pic33e/usb/usb_device.c ****         else
2567:lib/lib_pic33e/usb/usb_device.c ****         {
2568:lib/lib_pic33e/usb/usb_device.c ****                 BothEP0OutUOWNsSet = false;
 3433              	.loc 1 2568 0
 3434 001212  00 60 EF 	clr.b _BothEP0OutUOWNsSet
 3435              	.L123:
2569:lib/lib_pic33e/usb/usb_device.c ****         }
2570:lib/lib_pic33e/usb/usb_device.c ****     }
2571:lib/lib_pic33e/usb/usb_device.c **** }
 3436              	.loc 1 2571 0
 3437 001214  8E 07 78 	mov w14,w15
 3438 001216  4F 07 78 	mov [--w15],w14
 3439 001218  00 40 A9 	bclr CORCON,#2
 3440 00121a  00 00 06 	return 
 3441              	.set ___PA___,0
 3442              	.LFE22:
 3443              	.size _USBCtrlTrfOutHandler,.-_USBCtrlTrfOutHandler
 3444              	.align 2
 3445              	.type _USBCtrlTrfInHandler,@function
MPLAB XC16 ASSEMBLY Listing:   			page 107


 3446              	_USBCtrlTrfInHandler:
 3447              	.LFB23:
2572:lib/lib_pic33e/usb/usb_device.c **** 
2573:lib/lib_pic33e/usb/usb_device.c **** /******************************************************************************
2574:lib/lib_pic33e/usb/usb_device.c ****  * Function:        void USBCtrlTrfInHandler(void)
2575:lib/lib_pic33e/usb/usb_device.c ****  *
2576:lib/lib_pic33e/usb/usb_device.c ****  * PreCondition:    None
2577:lib/lib_pic33e/usb/usb_device.c ****  *
2578:lib/lib_pic33e/usb/usb_device.c ****  * Input:           None
2579:lib/lib_pic33e/usb/usb_device.c ****  *
2580:lib/lib_pic33e/usb/usb_device.c ****  * Output:          None
2581:lib/lib_pic33e/usb/usb_device.c ****  *
2582:lib/lib_pic33e/usb/usb_device.c ****  * Side Effects:    None
2583:lib/lib_pic33e/usb/usb_device.c ****  *
2584:lib/lib_pic33e/usb/usb_device.c ****  * Overview:        This routine handles an IN transaction according to
2585:lib/lib_pic33e/usb/usb_device.c ****  *                  which control transfer state is currently active.
2586:lib/lib_pic33e/usb/usb_device.c ****  *
2587:lib/lib_pic33e/usb/usb_device.c ****  * Note:            A Set Address Request must not change the actual address
2588:lib/lib_pic33e/usb/usb_device.c ****  *                  of the device until the completion of the control
2589:lib/lib_pic33e/usb/usb_device.c ****  *                  transfer. The end of the control transfer for Set Address
2590:lib/lib_pic33e/usb/usb_device.c ****  *                  Request is an IN transaction. Therefore it is necessary
2591:lib/lib_pic33e/usb/usb_device.c ****  *                  to service this unique situation when the condition is
2592:lib/lib_pic33e/usb/usb_device.c ****  *                  right. Macro mUSBCheckAdrPendingState is defined in
2593:lib/lib_pic33e/usb/usb_device.c ****  *                  usb9.h and its function is to specifically service this
2594:lib/lib_pic33e/usb/usb_device.c ****  *                  event.
2595:lib/lib_pic33e/usb/usb_device.c ****  *****************************************************************************/
2596:lib/lib_pic33e/usb/usb_device.c **** static void USBCtrlTrfInHandler(void)
2597:lib/lib_pic33e/usb/usb_device.c **** {
 3448              	.loc 1 2597 0
 3449              	.set ___PA___,1
 3450 00121c  02 00 FA 	lnk #2
 3451              	.LCFI26:
2598:lib/lib_pic33e/usb/usb_device.c ****     uint8_t lastDTS;
2599:lib/lib_pic33e/usb/usb_device.c **** 
2600:lib/lib_pic33e/usb/usb_device.c ****     lastDTS = pBDTEntryIn[0]->STAT.DTS;
 3452              	.loc 1 2600 0
 3453 00121e  01 00 80 	mov _pBDTEntryIn,w1
2601:lib/lib_pic33e/usb/usb_device.c **** 
2602:lib/lib_pic33e/usb/usb_device.c ****     //switch to the next ping pong buffer
2603:lib/lib_pic33e/usb/usb_device.c ****     pBDTEntryIn[0] = (volatile BDT_ENTRY*)(((uintptr_t)pBDTEntryIn[0]) ^ USB_NEXT_EP0_IN_PING_PONG)
 3454              	.loc 1 2603 0
 3455 001220  00 00 80 	mov _pBDTEntryIn,w0
 3456              	.loc 1 2600 0
 3457 001222  91 80 FB 	ze [w1],w1
 3458 001224  C6 08 DE 	lsr w1,#6,w1
 3459              	.loc 1 2603 0
 3460 001226  00 30 A2 	btg w0,#3
 3461              	.loc 1 2600 0
 3462 001228  E1 C0 60 	and.b w1,#1,w1
 3463 00122a  01 4F 78 	mov.b w1,[w14]
 3464              	.loc 1 2603 0
 3465 00122c  00 00 88 	mov w0,_pBDTEntryIn
2604:lib/lib_pic33e/usb/usb_device.c **** 
2605:lib/lib_pic33e/usb/usb_device.c ****     //Must check if in ADR_PENDING_STATE.  If so, we need to update the address
2606:lib/lib_pic33e/usb/usb_device.c ****     //now, since the IN status stage of the (set address) control transfer has 
2607:lib/lib_pic33e/usb/usb_device.c ****     //evidently completed successfully.
2608:lib/lib_pic33e/usb/usb_device.c ****     if(USBDeviceState == ADR_PENDING_STATE)
MPLAB XC16 ASSEMBLY Listing:   			page 108


 3466              	.loc 1 2608 0
 3467 00122e  00 00 80 	mov _USBDeviceState,w0
 3468 001230  E8 0F 50 	sub w0,#8,[w15]
 3469              	.set ___BP___,0
 3470 001232  00 00 3A 	bra nz,.L128
2609:lib/lib_pic33e/usb/usb_device.c ****     {
2610:lib/lib_pic33e/usb/usb_device.c ****         U1ADDR = (SetupPkt.bDevADR & 0x7F);
 3471              	.loc 1 2610 0
 3472 001234  21 00 20 	mov #_SetupPkt+2,w1
 3473 001236  F0 07 20 	mov #127,w0
 3474 001238  91 40 78 	mov.b [w1],w1
 3475 00123a  81 80 FB 	ze w1,w1
 3476 00123c  00 80 60 	and w1,w0,w0
 3477 00123e  00 00 88 	mov w0,_U1ADDR
2611:lib/lib_pic33e/usb/usb_device.c ****         if(U1ADDR != 0u)
 3478              	.loc 1 2611 0
 3479 001240  00 00 80 	mov _U1ADDR,w0
 3480 001242  00 00 E0 	cp0 w0
 3481              	.set ___BP___,0
 3482 001244  00 00 32 	bra z,.L129
2612:lib/lib_pic33e/usb/usb_device.c ****         {
2613:lib/lib_pic33e/usb/usb_device.c ****             USBDeviceState=ADDRESS_STATE;
 3483              	.loc 1 2613 0
 3484 001246  00 01 20 	mov #16,w0
 3485 001248  00 00 88 	mov w0,_USBDeviceState
 3486 00124a  00 00 37 	bra .L128
 3487              	.L129:
2614:lib/lib_pic33e/usb/usb_device.c ****         }
2615:lib/lib_pic33e/usb/usb_device.c ****         else
2616:lib/lib_pic33e/usb/usb_device.c ****         {
2617:lib/lib_pic33e/usb/usb_device.c ****             USBDeviceState=DEFAULT_STATE;
 3488              	.loc 1 2617 0
 3489 00124c  40 00 20 	mov #4,w0
 3490 00124e  00 00 88 	mov w0,_USBDeviceState
 3491              	.L128:
2618:lib/lib_pic33e/usb/usb_device.c ****         }
2619:lib/lib_pic33e/usb/usb_device.c ****     }//end if
2620:lib/lib_pic33e/usb/usb_device.c **** 
2621:lib/lib_pic33e/usb/usb_device.c **** 
2622:lib/lib_pic33e/usb/usb_device.c ****     if(controlTransferState == CTRL_TRF_TX)
 3492              	.loc 1 2622 0
 3493 001250  00 C0 BF 	mov.b _controlTransferState,WREG
 3494 001252  E1 4F 50 	sub.b w0,#1,[w15]
 3495              	.set ___BP___,0
 3496 001254  00 00 3A 	bra nz,.L130
2623:lib/lib_pic33e/usb/usb_device.c ****     {
2624:lib/lib_pic33e/usb/usb_device.c ****         pBDTEntryIn[0]->ADR = ConvertToPhysicalAddress(CtrlTrfData);
 3497              	.loc 1 2624 0
 3498 001256  00 00 80 	mov _pBDTEntryIn,w0
 3499 001258  01 00 20 	mov #_CtrlTrfData,w1
 3500 00125a  C0 41 90 	mov.b [w0+4],w3
 3501 00125c  48 09 DE 	lsr w1,#8,w2
 3502 00125e  E0 C1 61 	and.b w3,#0,w3
 3503 001260  F4 0F 20 	mov #255,w4
 3504 001262  84 80 60 	and w1,w4,w1
 3505 001264  81 C0 71 	ior.b w3,w1,w1
 3506 001266  41 40 98 	mov.b w1,[w0+4]
MPLAB XC16 ASSEMBLY Listing:   			page 109


 3507 001268  D0 40 90 	mov.b [w0+5],w1
 3508 00126a  E0 C0 60 	and.b w1,#0,w1
 3509 00126c  82 C0 70 	ior.b w1,w2,w1
 3510 00126e  51 40 98 	mov.b w1,[w0+5]
2625:lib/lib_pic33e/usb/usb_device.c ****         USBCtrlTrfTxService();
 3511              	.loc 1 2625 0
 3512 001270  00 00 07 	rcall _USBCtrlTrfTxService
2626:lib/lib_pic33e/usb/usb_device.c **** 
2627:lib/lib_pic33e/usb/usb_device.c ****         //Check if we have already sent a short packet.  If so, configure
2628:lib/lib_pic33e/usb/usb_device.c ****         //the endpoint to STALL in response to any further IN tokens (in the
2629:lib/lib_pic33e/usb/usb_device.c ****         //case that the host erroneously tries to receive more data than it
2630:lib/lib_pic33e/usb/usb_device.c ****         //should).
2631:lib/lib_pic33e/usb/usb_device.c ****         if(shortPacketStatus == SHORT_PKT_SENT)
 3513              	.loc 1 2631 0
 3514 001272  00 C0 BF 	mov.b _shortPacketStatus,WREG
 3515 001274  E2 4F 50 	sub.b w0,#2,[w15]
 3516              	.set ___BP___,0
 3517 001276  00 00 3A 	bra nz,.L131
2632:lib/lib_pic33e/usb/usb_device.c ****         {
2633:lib/lib_pic33e/usb/usb_device.c ****             // If a short packet has been sent, don't want to send any more,
2634:lib/lib_pic33e/usb/usb_device.c ****             // stall next time if host is still trying to read.
2635:lib/lib_pic33e/usb/usb_device.c ****             pBDTEntryIn[0]->STAT.Val = _BSTALL;
 3518              	.loc 1 2635 0
 3519 001278  00 00 80 	mov _pBDTEntryIn,w0
2636:lib/lib_pic33e/usb/usb_device.c ****             pBDTEntryIn[0]->STAT.Val |= _USIE;
 3520              	.loc 1 2636 0
 3521 00127a  F2 0F 20 	mov #255,w2
 3522              	.loc 1 2635 0
 3523 00127c  90 40 78 	mov.b [w0],w1
 3524 00127e  E0 C0 60 	and.b w1,#0,w1
 3525 001280  01 24 A0 	bset.b w1,#2
 3526 001282  01 48 78 	mov.b w1,[w0]
 3527 001284  90 40 90 	mov.b [w0+1],w1
 3528 001286  E0 C0 60 	and.b w1,#0,w1
 3529 001288  11 40 98 	mov.b w1,[w0+1]
 3530              	.loc 1 2636 0
 3531 00128a  00 00 80 	mov _pBDTEntryIn,w0
 3532 00128c  01 00 80 	mov _pBDTEntryIn,w1
 3533 00128e  11 82 FB 	ze [w1],w4
 3534 001290  00 00 00 	nop 
 3535 001292  91 40 90 	mov.b [w1+1],w1
 3536 001294  90 41 78 	mov.b [w0],w3
 3537 001296  81 80 FB 	ze w1,w1
 3538 001298  E0 C1 61 	and.b w3,#0,w3
 3539 00129a  C8 08 DD 	sl w1,#8,w1
 3540 00129c  81 00 72 	ior w4,w1,w1
 3541 00129e  01 70 A0 	bset w1,#7
 3542 0012a0  02 81 60 	and w1,w2,w2
 3543 0012a2  02 C1 71 	ior.b w3,w2,w2
 3544 0012a4  02 48 78 	mov.b w2,[w0]
 3545 0012a6  C8 08 DE 	lsr w1,#8,w1
 3546 0012a8  10 41 90 	mov.b [w0+1],w2
 3547 0012aa  60 41 61 	and.b w2,#0,w2
 3548 0012ac  81 40 71 	ior.b w2,w1,w1
 3549 0012ae  11 40 98 	mov.b w1,[w0+1]
 3550 0012b0  00 00 37 	bra .L127
 3551              	.L131:
MPLAB XC16 ASSEMBLY Listing:   			page 110


2637:lib/lib_pic33e/usb/usb_device.c ****         }
2638:lib/lib_pic33e/usb/usb_device.c ****         else
2639:lib/lib_pic33e/usb/usb_device.c ****         {
2640:lib/lib_pic33e/usb/usb_device.c ****             if(lastDTS == 0)
 3552              	.loc 1 2640 0
 3553 0012b2  1E 40 78 	mov.b [w14],w0
 3554 0012b4  00 04 E0 	cp0.b w0
 3555              	.set ___BP___,0
 3556 0012b6  00 00 3A 	bra nz,.L133
2641:lib/lib_pic33e/usb/usb_device.c ****             {
2642:lib/lib_pic33e/usb/usb_device.c ****                 pBDTEntryIn[0]->STAT.Val = _DAT1|(_DTSEN & _DTS_CHECKING_ENABLED);
 3557              	.loc 1 2642 0
 3558 0012b8  00 00 80 	mov _pBDTEntryIn,w0
 3559 0012ba  81 C4 B3 	mov.b #72,w1
 3560 0012bc  10 41 78 	mov.b [w0],w2
 3561 0012be  60 41 61 	and.b w2,#0,w2
 3562 0012c0  81 40 71 	ior.b w2,w1,w1
 3563 0012c2  01 48 78 	mov.b w1,[w0]
2643:lib/lib_pic33e/usb/usb_device.c ****                 pBDTEntryIn[0]->STAT.Val |= _USIE;
 3564              	.loc 1 2643 0
 3565 0012c4  F2 0F 20 	mov #255,w2
 3566              	.loc 1 2642 0
 3567 0012c6  90 40 90 	mov.b [w0+1],w1
 3568 0012c8  E0 C0 60 	and.b w1,#0,w1
 3569 0012ca  11 40 98 	mov.b w1,[w0+1]
 3570              	.loc 1 2643 0
 3571 0012cc  00 00 80 	mov _pBDTEntryIn,w0
 3572 0012ce  01 00 80 	mov _pBDTEntryIn,w1
 3573 0012d0  11 82 FB 	ze [w1],w4
 3574 0012d2  00 00 00 	nop 
 3575 0012d4  91 40 90 	mov.b [w1+1],w1
 3576 0012d6  90 41 78 	mov.b [w0],w3
 3577 0012d8  81 80 FB 	ze w1,w1
 3578 0012da  E0 C1 61 	and.b w3,#0,w3
 3579 0012dc  C8 08 DD 	sl w1,#8,w1
 3580 0012de  81 00 72 	ior w4,w1,w1
 3581 0012e0  01 70 A0 	bset w1,#7
 3582 0012e2  02 81 60 	and w1,w2,w2
 3583 0012e4  02 C1 71 	ior.b w3,w2,w2
 3584 0012e6  02 48 78 	mov.b w2,[w0]
 3585 0012e8  C8 08 DE 	lsr w1,#8,w1
 3586 0012ea  10 41 90 	mov.b [w0+1],w2
 3587 0012ec  60 41 61 	and.b w2,#0,w2
 3588 0012ee  81 40 71 	ior.b w2,w1,w1
 3589 0012f0  11 40 98 	mov.b w1,[w0+1]
 3590 0012f2  00 00 37 	bra .L127
 3591              	.L133:
2644:lib/lib_pic33e/usb/usb_device.c ****             }
2645:lib/lib_pic33e/usb/usb_device.c ****             else
2646:lib/lib_pic33e/usb/usb_device.c ****             {
2647:lib/lib_pic33e/usb/usb_device.c ****                 pBDTEntryIn[0]->STAT.Val = _DAT0|(_DTSEN & _DTS_CHECKING_ENABLED);
 3592              	.loc 1 2647 0
 3593 0012f4  00 00 80 	mov _pBDTEntryIn,w0
2648:lib/lib_pic33e/usb/usb_device.c ****                 pBDTEntryIn[0]->STAT.Val |= _USIE;
 3594              	.loc 1 2648 0
 3595 0012f6  F2 0F 20 	mov #255,w2
 3596              	.loc 1 2647 0
MPLAB XC16 ASSEMBLY Listing:   			page 111


 3597 0012f8  90 40 78 	mov.b [w0],w1
 3598 0012fa  E0 C0 60 	and.b w1,#0,w1
 3599 0012fc  01 34 A0 	bset.b w1,#3
 3600 0012fe  01 48 78 	mov.b w1,[w0]
 3601 001300  90 40 90 	mov.b [w0+1],w1
 3602 001302  E0 C0 60 	and.b w1,#0,w1
 3603 001304  11 40 98 	mov.b w1,[w0+1]
 3604              	.loc 1 2648 0
 3605 001306  00 00 80 	mov _pBDTEntryIn,w0
 3606 001308  01 00 80 	mov _pBDTEntryIn,w1
 3607 00130a  11 82 FB 	ze [w1],w4
 3608 00130c  00 00 00 	nop 
 3609 00130e  91 40 90 	mov.b [w1+1],w1
 3610 001310  90 41 78 	mov.b [w0],w3
 3611 001312  81 80 FB 	ze w1,w1
 3612 001314  E0 C1 61 	and.b w3,#0,w3
 3613 001316  C8 08 DD 	sl w1,#8,w1
 3614 001318  81 00 72 	ior w4,w1,w1
 3615 00131a  01 70 A0 	bset w1,#7
 3616 00131c  02 81 60 	and w1,w2,w2
 3617 00131e  02 C1 71 	ior.b w3,w2,w2
 3618 001320  02 48 78 	mov.b w2,[w0]
 3619 001322  C8 08 DE 	lsr w1,#8,w1
 3620 001324  10 41 90 	mov.b [w0+1],w2
 3621 001326  60 41 61 	and.b w2,#0,w2
 3622 001328  81 40 71 	ior.b w2,w1,w1
 3623 00132a  11 40 98 	mov.b w1,[w0+1]
 3624 00132c  00 00 37 	bra .L127
 3625              	.L130:
2649:lib/lib_pic33e/usb/usb_device.c ****             }
2650:lib/lib_pic33e/usb/usb_device.c ****         }//end if(...)else
2651:lib/lib_pic33e/usb/usb_device.c ****     }
2652:lib/lib_pic33e/usb/usb_device.c **** 	else // must have been a CTRL_TRF_RX status stage IN packet (<setup><out><out>...<IN>  <-- this la
2653:lib/lib_pic33e/usb/usb_device.c **** 	{
2654:lib/lib_pic33e/usb/usb_device.c ****         //if someone is still expecting data from the control transfer
2655:lib/lib_pic33e/usb/usb_device.c ****         //  then make sure to terminate that request and let them know that
2656:lib/lib_pic33e/usb/usb_device.c ****         //  they are done
2657:lib/lib_pic33e/usb/usb_device.c ****         if(outPipes[0].info.bits.busy == 1)
 3626              	.loc 1 2657 0
 3627 00132e  21 00 20 	mov #_outPipes+2,w1
 3628 001330  00 C8 B3 	mov.b #-128,w0
 3629 001332  91 40 78 	mov.b [w1],w1
 3630 001334  00 C0 60 	and.b w1,w0,w0
 3631 001336  00 04 E0 	cp0.b w0
 3632              	.set ___BP___,0
 3633 001338  00 00 32 	bra z,.L134
2658:lib/lib_pic33e/usb/usb_device.c ****         {
2659:lib/lib_pic33e/usb/usb_device.c ****             if(outPipes[0].pFunc != NULL)
 3634              	.loc 1 2659 0
 3635 00133a  50 00 20 	mov #_outPipes+5,w0
 3636 00133c  61 00 20 	mov #_outPipes+6,w1
 3637 00133e  00 00 00 	nop 
 3638 001340  10 80 FB 	ze [w0],w0
 3639 001342  00 00 00 	nop 
 3640 001344  91 80 FB 	ze [w1],w1
 3641 001346  C8 08 DD 	sl w1,#8,w1
 3642 001348  01 00 70 	ior w0,w1,w0
MPLAB XC16 ASSEMBLY Listing:   			page 112


 3643 00134a  00 00 E0 	cp0 w0
 3644              	.set ___BP___,0
 3645 00134c  00 00 32 	bra z,.L135
2660:lib/lib_pic33e/usb/usb_device.c ****             {
2661:lib/lib_pic33e/usb/usb_device.c ****                 outPipes[0].pFunc();
 3646              	.loc 1 2661 0
 3647 00134e  50 00 20 	mov #_outPipes+5,w0
 3648 001350  61 00 20 	mov #_outPipes+6,w1
 3649 001352  00 00 00 	nop 
 3650 001354  10 80 FB 	ze [w0],w0
 3651 001356  00 00 00 	nop 
 3652 001358  91 80 FB 	ze [w1],w1
 3653 00135a  C8 08 DD 	sl w1,#8,w1
 3654 00135c  01 00 70 	ior w0,w1,w0
 3655 00135e  00 00 01 	call w0
 3656              	.L135:
2662:lib/lib_pic33e/usb/usb_device.c ****             }
2663:lib/lib_pic33e/usb/usb_device.c ****             outPipes[0].info.bits.busy = 0;
 3657              	.loc 1 2663 0
 3658 001360  20 00 20 	mov #_outPipes+2,w0
 3659 001362  90 40 78 	mov.b [w0],w1
 3660 001364  01 74 A1 	bclr.b w1,#7
 3661 001366  01 48 78 	mov.b w1,[w0]
 3662              	.L134:
2664:lib/lib_pic33e/usb/usb_device.c ****         }
2665:lib/lib_pic33e/usb/usb_device.c ****     	
2666:lib/lib_pic33e/usb/usb_device.c ****         controlTransferState = WAIT_SETUP;
 3663              	.loc 1 2666 0
 3664 001368  00 60 EF 	clr.b _controlTransferState
 3665              	.L127:
2667:lib/lib_pic33e/usb/usb_device.c ****         //Don't need to arm EP0 OUT here.  It was already armed by the last <out> that
2668:lib/lib_pic33e/usb/usb_device.c ****         //got processed by the USBCtrlTrfRxService() handler.
2669:lib/lib_pic33e/usb/usb_device.c **** 	}	
2670:lib/lib_pic33e/usb/usb_device.c **** 
2671:lib/lib_pic33e/usb/usb_device.c **** }
 3666              	.loc 1 2671 0
 3667 00136a  8E 07 78 	mov w14,w15
 3668 00136c  4F 07 78 	mov [--w15],w14
 3669 00136e  00 40 A9 	bclr CORCON,#2
 3670 001370  00 00 06 	return 
 3671              	.set ___PA___,0
 3672              	.LFE23:
 3673              	.size _USBCtrlTrfInHandler,.-_USBCtrlTrfInHandler
 3674              	.align 2
 3675              	.type _USBCheckStdRequest,@function
 3676              	_USBCheckStdRequest:
 3677              	.LFB24:
2672:lib/lib_pic33e/usb/usb_device.c **** 
2673:lib/lib_pic33e/usb/usb_device.c **** 
2674:lib/lib_pic33e/usb/usb_device.c **** /********************************************************************
2675:lib/lib_pic33e/usb/usb_device.c ****  * Function:        void USBCheckStdRequest(void)
2676:lib/lib_pic33e/usb/usb_device.c ****  *
2677:lib/lib_pic33e/usb/usb_device.c ****  * PreCondition:    None
2678:lib/lib_pic33e/usb/usb_device.c ****  *
2679:lib/lib_pic33e/usb/usb_device.c ****  * Input:           None
2680:lib/lib_pic33e/usb/usb_device.c ****  *
2681:lib/lib_pic33e/usb/usb_device.c ****  * Output:          None
MPLAB XC16 ASSEMBLY Listing:   			page 113


2682:lib/lib_pic33e/usb/usb_device.c ****  *
2683:lib/lib_pic33e/usb/usb_device.c ****  * Side Effects:    None
2684:lib/lib_pic33e/usb/usb_device.c ****  *
2685:lib/lib_pic33e/usb/usb_device.c ****  * Overview:        This routine checks the setup data packet to see
2686:lib/lib_pic33e/usb/usb_device.c ****  *                  if it knows how to handle it
2687:lib/lib_pic33e/usb/usb_device.c ****  *
2688:lib/lib_pic33e/usb/usb_device.c ****  * Note:            None
2689:lib/lib_pic33e/usb/usb_device.c ****  *******************************************************************/
2690:lib/lib_pic33e/usb/usb_device.c **** static void USBCheckStdRequest(void)
2691:lib/lib_pic33e/usb/usb_device.c **** {
 3678              	.loc 1 2691 0
 3679              	.set ___PA___,1
 3680 001372  00 00 FA 	lnk #0
 3681              	.LCFI27:
2692:lib/lib_pic33e/usb/usb_device.c ****     if(SetupPkt.RequestType != USB_SETUP_TYPE_STANDARD_BITFIELD) return;
 3682              	.loc 1 2692 0
 3683 001374  00 00 20 	mov #_SetupPkt,w0
 3684 001376  10 40 78 	mov.b [w0],w0
 3685 001378  00 46 B2 	and.b #96,w0
 3686 00137a  00 04 E0 	cp0.b w0
 3687              	.set ___BP___,0
 3688 00137c  00 00 3A 	bra nz,.L150
 3689              	.L137:
2693:lib/lib_pic33e/usb/usb_device.c **** 
2694:lib/lib_pic33e/usb/usb_device.c ****     switch(SetupPkt.bRequest)
 3690              	.loc 1 2694 0
 3691 00137e  10 00 20 	mov #_SetupPkt+1,w0
 3692 001380  B2 00 20 	mov #11,w2
 3693 001382  03 00 20 	mov #0,w3
 3694 001384  10 40 78 	mov.b [w0],w0
 3695 001386  00 80 FB 	ze w0,w0
 3696 001388  CF 80 DE 	asr w0,#15,w1
 3697 00138a  82 0F 50 	sub w0,w2,[w15]
 3698 00138c  83 8F 58 	subb w1,w3,[w15]
 3699              	.set ___BP___,0
 3700 00138e  00 00 3E 	bra gtu,.L151
 3701 001390  00 06 01 	bra w0
 3702              	.align 2
 3703              	.set ___PA___,0
 3704              	.L149:
 3705 001392  00 00 37 	bra .L140
 3706 001394  00 00 37 	bra .L141
 3707 001396  00 00 37 	bra .L152
 3708 001398  00 00 37 	bra .L141
 3709 00139a  00 00 37 	bra .L152
 3710 00139c  00 00 37 	bra .L142
 3711 00139e  00 00 37 	bra .L143
 3712 0013a0  00 00 37 	bra .L144
 3713 0013a2  00 00 37 	bra .L145
 3714 0013a4  00 00 37 	bra .L146
 3715 0013a6  00 00 37 	bra .L147
 3716 0013a8  00 00 37 	bra .L148
 3717              	.set ___PA___,1
 3718              	.L142:
2695:lib/lib_pic33e/usb/usb_device.c ****     {
2696:lib/lib_pic33e/usb/usb_device.c ****         case USB_REQUEST_SET_ADDRESS:
2697:lib/lib_pic33e/usb/usb_device.c ****             inPipes[0].info.bits.busy = 1;            // This will generate a zero length packet
MPLAB XC16 ASSEMBLY Listing:   			page 114


 3719              	.loc 1 2697 0
 3720 0013aa  11 00 80 	mov _inPipes+2,w1
2698:lib/lib_pic33e/usb/usb_device.c ****             USBDeviceState = ADR_PENDING_STATE;       // Update state only
 3721              	.loc 1 2698 0
 3722 0013ac  80 00 20 	mov #8,w0
 3723              	.loc 1 2697 0
 3724 0013ae  01 70 A0 	bset w1,#7
 3725 0013b0  11 00 88 	mov w1,_inPipes+2
 3726              	.loc 1 2698 0
 3727 0013b2  00 00 88 	mov w0,_USBDeviceState
2699:lib/lib_pic33e/usb/usb_device.c ****             /* See USBCtrlTrfInHandler() for the next step */
2700:lib/lib_pic33e/usb/usb_device.c ****             break;
 3728              	.loc 1 2700 0
 3729 0013b4  00 00 37 	bra .L136
 3730              	.L143:
2701:lib/lib_pic33e/usb/usb_device.c ****         case USB_REQUEST_GET_DESCRIPTOR:
2702:lib/lib_pic33e/usb/usb_device.c ****             USBStdGetDscHandler();
 3731              	.loc 1 2702 0
 3732 0013b6  00 00 07 	rcall _USBStdGetDscHandler
2703:lib/lib_pic33e/usb/usb_device.c ****             break;
 3733              	.loc 1 2703 0
 3734 0013b8  00 00 37 	bra .L136
 3735              	.L146:
2704:lib/lib_pic33e/usb/usb_device.c ****         case USB_REQUEST_SET_CONFIGURATION:
2705:lib/lib_pic33e/usb/usb_device.c ****             USBStdSetCfgHandler();
 3736              	.loc 1 2705 0
 3737 0013ba  00 00 07 	rcall _USBStdSetCfgHandler
2706:lib/lib_pic33e/usb/usb_device.c ****             break;
 3738              	.loc 1 2706 0
 3739 0013bc  00 00 37 	bra .L136
 3740              	.L145:
2707:lib/lib_pic33e/usb/usb_device.c ****         case USB_REQUEST_GET_CONFIGURATION:
2708:lib/lib_pic33e/usb/usb_device.c ****             inPipes[0].pSrc.bRam = (uint8_t*)&USBActiveConfiguration;         // Set Source
 3741              	.loc 1 2708 0
 3742 0013be  01 00 20 	mov #_USBActiveConfiguration,w1
2709:lib/lib_pic33e/usb/usb_device.c ****             inPipes[0].info.bits.ctrl_trf_mem = USB_EP0_RAM;               // Set memory type
2710:lib/lib_pic33e/usb/usb_device.c ****             inPipes[0].wCount.v[0] = 1;                         // Set data count
 3743              	.loc 1 2710 0
 3744 0013c0  40 00 20 	mov #_inPipes+4,w0
 3745              	.loc 1 2708 0
 3746 0013c2  01 00 88 	mov w1,_inPipes
 3747              	.loc 1 2710 0
 3748 0013c4  11 C0 B3 	mov.b #1,w1
 3749              	.loc 1 2709 0
 3750 0013c6  12 00 80 	mov _inPipes+2,w2
 3751 0013c8  02 00 A0 	bset w2,#0
 3752 0013ca  12 00 88 	mov w2,_inPipes+2
 3753              	.loc 1 2710 0
 3754 0013cc  01 48 78 	mov.b w1,[w0]
2711:lib/lib_pic33e/usb/usb_device.c ****             inPipes[0].info.bits.busy = 1;
 3755              	.loc 1 2711 0
 3756 0013ce  10 00 80 	mov _inPipes+2,w0
 3757 0013d0  00 70 A0 	bset w0,#7
 3758 0013d2  10 00 88 	mov w0,_inPipes+2
2712:lib/lib_pic33e/usb/usb_device.c ****             break;
 3759              	.loc 1 2712 0
 3760 0013d4  00 00 37 	bra .L136
MPLAB XC16 ASSEMBLY Listing:   			page 115


 3761              	.L140:
2713:lib/lib_pic33e/usb/usb_device.c ****         case USB_REQUEST_GET_STATUS:
2714:lib/lib_pic33e/usb/usb_device.c ****             USBStdGetStatusHandler();
 3762              	.loc 1 2714 0
 3763 0013d6  00 00 07 	rcall _USBStdGetStatusHandler
2715:lib/lib_pic33e/usb/usb_device.c ****             break;
 3764              	.loc 1 2715 0
 3765 0013d8  00 00 37 	bra .L136
 3766              	.L141:
2716:lib/lib_pic33e/usb/usb_device.c ****         case USB_REQUEST_CLEAR_FEATURE:
2717:lib/lib_pic33e/usb/usb_device.c ****         case USB_REQUEST_SET_FEATURE:
2718:lib/lib_pic33e/usb/usb_device.c ****             USBStdFeatureReqHandler();
 3767              	.loc 1 2718 0
 3768 0013da  00 00 07 	rcall _USBStdFeatureReqHandler
2719:lib/lib_pic33e/usb/usb_device.c ****             break;
 3769              	.loc 1 2719 0
 3770 0013dc  00 00 37 	bra .L136
 3771              	.L147:
2720:lib/lib_pic33e/usb/usb_device.c ****         case USB_REQUEST_GET_INTERFACE:
2721:lib/lib_pic33e/usb/usb_device.c ****             inPipes[0].pSrc.bRam = (uint8_t*)&USBAlternateInterface[SetupPkt.bIntfID];  // Set sour
 3772              	.loc 1 2721 0
 3773 0013de  40 00 20 	mov #_SetupPkt+4,w0
 3774 0013e0  02 00 20 	mov #_USBAlternateInterface,w2
 3775 0013e2  90 40 78 	mov.b [w0],w1
2722:lib/lib_pic33e/usb/usb_device.c ****             inPipes[0].info.bits.ctrl_trf_mem = USB_EP0_RAM;               // Set memory type
2723:lib/lib_pic33e/usb/usb_device.c ****             inPipes[0].wCount.v[0] = 1;                         // Set data count
 3776              	.loc 1 2723 0
 3777 0013e4  40 00 20 	mov #_inPipes+4,w0
 3778              	.loc 1 2721 0
 3779 0013e6  81 81 FB 	ze w1,w3
 3780              	.loc 1 2723 0
 3781 0013e8  11 C0 B3 	mov.b #1,w1
 3782              	.loc 1 2721 0
 3783 0013ea  02 81 41 	add w3,w2,w2
 3784 0013ec  02 00 88 	mov w2,_inPipes
 3785              	.loc 1 2722 0
 3786 0013ee  12 00 80 	mov _inPipes+2,w2
 3787 0013f0  02 00 A0 	bset w2,#0
 3788 0013f2  12 00 88 	mov w2,_inPipes+2
 3789              	.loc 1 2723 0
 3790 0013f4  01 48 78 	mov.b w1,[w0]
2724:lib/lib_pic33e/usb/usb_device.c ****             inPipes[0].info.bits.busy = 1;
 3791              	.loc 1 2724 0
 3792 0013f6  10 00 80 	mov _inPipes+2,w0
 3793 0013f8  00 70 A0 	bset w0,#7
 3794 0013fa  10 00 88 	mov w0,_inPipes+2
2725:lib/lib_pic33e/usb/usb_device.c ****             break;
 3795              	.loc 1 2725 0
 3796 0013fc  00 00 37 	bra .L136
 3797              	.L148:
2726:lib/lib_pic33e/usb/usb_device.c ****         case USB_REQUEST_SET_INTERFACE:
2727:lib/lib_pic33e/usb/usb_device.c ****             inPipes[0].info.bits.busy = 1;
2728:lib/lib_pic33e/usb/usb_device.c ****             USBAlternateInterface[SetupPkt.bIntfID] = SetupPkt.bAltID;
 3798              	.loc 1 2728 0
 3799 0013fe  40 00 20 	mov #_SetupPkt+4,w0
 3800 001400  21 00 20 	mov #_SetupPkt+2,w1
 3801              	.loc 1 2727 0
MPLAB XC16 ASSEMBLY Listing:   			page 116


 3802 001402  13 00 80 	mov _inPipes+2,w3
 3803              	.loc 1 2728 0
 3804 001404  02 00 20 	mov #_USBAlternateInterface,w2
 3805              	.loc 1 2727 0
 3806 001406  03 70 A0 	bset w3,#7
 3807 001408  13 00 88 	mov w3,_inPipes+2
 3808              	.loc 1 2728 0
 3809 00140a  10 40 78 	mov.b [w0],w0
 3810 00140c  91 40 78 	mov.b [w1],w1
 3811 00140e  00 80 FB 	ze w0,w0
 3812 001410  00 00 41 	add w2,w0,w0
 3813 001412  01 48 78 	mov.b w1,[w0]
2729:lib/lib_pic33e/usb/usb_device.c ****             break;
 3814              	.loc 1 2729 0
 3815 001414  00 00 37 	bra .L136
 3816              	.L144:
2730:lib/lib_pic33e/usb/usb_device.c ****         case USB_REQUEST_SET_DESCRIPTOR:
2731:lib/lib_pic33e/usb/usb_device.c ****             USB_SET_DESCRIPTOR_HANDLER(EVENT_SET_DESCRIPTOR,0,0);
 3817              	.loc 1 2731 0
 3818 001416  00 01 EB 	clr w2
 3819 001418  80 00 EB 	clr w1
 3820 00141a  20 00 20 	mov #2,w0
 3821 00141c  00 00 07 	rcall _USER_USB_CALLBACK_EVENT_HANDLER
2732:lib/lib_pic33e/usb/usb_device.c ****             break;
 3822              	.loc 1 2732 0
 3823 00141e  00 00 37 	bra .L136
 3824              	.L150:
 3825 001420  00 00 37 	bra .L136
 3826              	.L151:
 3827 001422  00 00 37 	bra .L136
 3828              	.L152:
 3829              	.L136:
2733:lib/lib_pic33e/usb/usb_device.c ****         case USB_REQUEST_SYNCH_FRAME:
2734:lib/lib_pic33e/usb/usb_device.c ****         default:
2735:lib/lib_pic33e/usb/usb_device.c ****             break;
2736:lib/lib_pic33e/usb/usb_device.c ****     }//end switch
2737:lib/lib_pic33e/usb/usb_device.c **** }//end USBCheckStdRequest
 3830              	.loc 1 2737 0
 3831 001424  8E 07 78 	mov w14,w15
 3832 001426  4F 07 78 	mov [--w15],w14
 3833 001428  00 40 A9 	bclr CORCON,#2
 3834 00142a  00 00 06 	return 
 3835              	.set ___PA___,0
 3836              	.LFE24:
 3837              	.size _USBCheckStdRequest,.-_USBCheckStdRequest
 3838              	.align 2
 3839              	.type _USBStdFeatureReqHandler,@function
 3840              	_USBStdFeatureReqHandler:
 3841              	.LFB25:
2738:lib/lib_pic33e/usb/usb_device.c **** 
2739:lib/lib_pic33e/usb/usb_device.c **** /********************************************************************
2740:lib/lib_pic33e/usb/usb_device.c ****  * Function:        void USBStdFeatureReqHandler(void)
2741:lib/lib_pic33e/usb/usb_device.c ****  *
2742:lib/lib_pic33e/usb/usb_device.c ****  * PreCondition:    None
2743:lib/lib_pic33e/usb/usb_device.c ****  *
2744:lib/lib_pic33e/usb/usb_device.c ****  * Input:           None
2745:lib/lib_pic33e/usb/usb_device.c ****  *
MPLAB XC16 ASSEMBLY Listing:   			page 117


2746:lib/lib_pic33e/usb/usb_device.c ****  * Output:          Can alter BDT entries.  Can also modify USB stack
2747:lib/lib_pic33e/usb/usb_device.c ****  *                  Maintained variables.
2748:lib/lib_pic33e/usb/usb_device.c ****  *
2749:lib/lib_pic33e/usb/usb_device.c ****  * Side Effects:    None
2750:lib/lib_pic33e/usb/usb_device.c ****  *
2751:lib/lib_pic33e/usb/usb_device.c ****  * Overview:        This routine handles the standard SET & CLEAR
2752:lib/lib_pic33e/usb/usb_device.c ****  *                  FEATURES requests
2753:lib/lib_pic33e/usb/usb_device.c ****  *
2754:lib/lib_pic33e/usb/usb_device.c ****  * Note:            This is a private function, intended for internal 
2755:lib/lib_pic33e/usb/usb_device.c ****  *                  use by the USB stack, when processing SET/CLEAR
2756:lib/lib_pic33e/usb/usb_device.c ****  *                  feature requests.  
2757:lib/lib_pic33e/usb/usb_device.c ****  *******************************************************************/
2758:lib/lib_pic33e/usb/usb_device.c **** static void USBStdFeatureReqHandler(void)
2759:lib/lib_pic33e/usb/usb_device.c **** {
 3842              	.loc 1 2759 0
 3843              	.set ___PA___,1
 3844 00142c  06 00 FA 	lnk #6
 3845              	.LCFI28:
2760:lib/lib_pic33e/usb/usb_device.c ****     BDT_ENTRY *p;
2761:lib/lib_pic33e/usb/usb_device.c ****     EP_STATUS current_ep_data;
2762:lib/lib_pic33e/usb/usb_device.c ****     #if defined(__C32__)
2763:lib/lib_pic33e/usb/usb_device.c ****         uint32_t* pUEP;
2764:lib/lib_pic33e/usb/usb_device.c ****     #else
2765:lib/lib_pic33e/usb/usb_device.c ****         unsigned char* pUEP;             
2766:lib/lib_pic33e/usb/usb_device.c ****     #endif
2767:lib/lib_pic33e/usb/usb_device.c ****     
2768:lib/lib_pic33e/usb/usb_device.c **** 
2769:lib/lib_pic33e/usb/usb_device.c ****     #ifdef	USB_SUPPORT_OTG
2770:lib/lib_pic33e/usb/usb_device.c ****     //Check for USB On-The-Go (OTG) specific requests
2771:lib/lib_pic33e/usb/usb_device.c ****     if ((SetupPkt.bFeature == OTG_FEATURE_B_HNP_ENABLE)&&
2772:lib/lib_pic33e/usb/usb_device.c ****         (SetupPkt.Recipient == USB_SETUP_RECIPIENT_DEVICE_BITFIELD))
2773:lib/lib_pic33e/usb/usb_device.c ****     {  
2774:lib/lib_pic33e/usb/usb_device.c ****         inPipes[0].info.bits.busy = 1;
2775:lib/lib_pic33e/usb/usb_device.c ****         if(SetupPkt.bRequest == USB_REQUEST_SET_FEATURE)
2776:lib/lib_pic33e/usb/usb_device.c ****             USBOTGEnableHnp();
2777:lib/lib_pic33e/usb/usb_device.c ****         else
2778:lib/lib_pic33e/usb/usb_device.c ****             USBOTGDisableHnp();
2779:lib/lib_pic33e/usb/usb_device.c ****     }
2780:lib/lib_pic33e/usb/usb_device.c **** 
2781:lib/lib_pic33e/usb/usb_device.c ****     if ((SetupPkt.bFeature == OTG_FEATURE_A_HNP_SUPPORT)&&
2782:lib/lib_pic33e/usb/usb_device.c ****         (SetupPkt.Recipient == USB_SETUP_RECIPIENT_DEVICE_BITFIELD))
2783:lib/lib_pic33e/usb/usb_device.c ****     {
2784:lib/lib_pic33e/usb/usb_device.c ****         inPipes[0].info.bits.busy = 1;
2785:lib/lib_pic33e/usb/usb_device.c ****         if(SetupPkt.bRequest == USB_REQUEST_SET_FEATURE)
2786:lib/lib_pic33e/usb/usb_device.c ****             USBOTGEnableSupportHnp();
2787:lib/lib_pic33e/usb/usb_device.c ****         else
2788:lib/lib_pic33e/usb/usb_device.c ****             USBOTGDisableSupportHnp();
2789:lib/lib_pic33e/usb/usb_device.c ****     }
2790:lib/lib_pic33e/usb/usb_device.c **** 
2791:lib/lib_pic33e/usb/usb_device.c ****     if ((SetupPkt.bFeature == OTG_FEATURE_A_ALT_HNP_SUPPORT)&&
2792:lib/lib_pic33e/usb/usb_device.c ****         (SetupPkt.Recipient == USB_SETUP_RECIPIENT_DEVICE_BITFIELD))
2793:lib/lib_pic33e/usb/usb_device.c ****     {
2794:lib/lib_pic33e/usb/usb_device.c ****         inPipes[0].info.bits.busy = 1;
2795:lib/lib_pic33e/usb/usb_device.c ****         if(SetupPkt.bRequest == USB_REQUEST_SET_FEATURE)
2796:lib/lib_pic33e/usb/usb_device.c ****             USBOTGEnableAltHnp();
2797:lib/lib_pic33e/usb/usb_device.c ****         else
2798:lib/lib_pic33e/usb/usb_device.c ****             USBOTGDisableAltHnp();
MPLAB XC16 ASSEMBLY Listing:   			page 118


2799:lib/lib_pic33e/usb/usb_device.c ****     }
2800:lib/lib_pic33e/usb/usb_device.c ****     #endif   //#ifdef USB_SUPPORT_OTG 
2801:lib/lib_pic33e/usb/usb_device.c **** 
2802:lib/lib_pic33e/usb/usb_device.c ****     //Check if the host sent a valid SET or CLEAR feature (remote wakeup) request.
2803:lib/lib_pic33e/usb/usb_device.c ****     if((SetupPkt.bFeature == USB_FEATURE_DEVICE_REMOTE_WAKEUP)&&
 3846              	.loc 1 2803 0
 3847 00142e  20 00 20 	mov #_SetupPkt+2,w0
 3848 001430  10 40 78 	mov.b [w0],w0
 3849 001432  E1 4F 50 	sub.b w0,#1,[w15]
 3850              	.set ___BP___,0
 3851 001434  00 00 3A 	bra nz,.L154
2804:lib/lib_pic33e/usb/usb_device.c ****        (SetupPkt.Recipient == USB_SETUP_RECIPIENT_DEVICE_BITFIELD))
 3852              	.loc 1 2804 0
 3853 001436  00 00 20 	mov #_SetupPkt,w0
 3854 001438  10 40 78 	mov.b [w0],w0
 3855 00143a  7F 40 60 	and.b w0,#31,w0
 3856              	.loc 1 2803 0
 3857 00143c  00 04 E0 	cp0.b w0
 3858              	.set ___BP___,0
 3859 00143e  00 00 3A 	bra nz,.L154
2805:lib/lib_pic33e/usb/usb_device.c ****     {
2806:lib/lib_pic33e/usb/usb_device.c ****         inPipes[0].info.bits.busy = 1;
2807:lib/lib_pic33e/usb/usb_device.c ****         if(SetupPkt.bRequest == USB_REQUEST_SET_FEATURE)
 3860              	.loc 1 2807 0
 3861 001440  10 00 20 	mov #_SetupPkt+1,w0
 3862              	.loc 1 2806 0
 3863 001442  11 00 80 	mov _inPipes+2,w1
 3864 001444  01 70 A0 	bset w1,#7
 3865 001446  11 00 88 	mov w1,_inPipes+2
 3866              	.loc 1 2807 0
 3867 001448  10 40 78 	mov.b [w0],w0
 3868 00144a  E3 4F 50 	sub.b w0,#3,[w15]
 3869              	.set ___BP___,0
 3870 00144c  00 00 3A 	bra nz,.L155
2808:lib/lib_pic33e/usb/usb_device.c ****             RemoteWakeup = true;
 3871              	.loc 1 2808 0
 3872 00144e  10 C0 B3 	mov.b #1,w0
 3873 001450  00 E0 B7 	mov.b WREG,_RemoteWakeup
 3874 001452  00 00 37 	bra .L154
 3875              	.L155:
2809:lib/lib_pic33e/usb/usb_device.c ****         else
2810:lib/lib_pic33e/usb/usb_device.c ****             RemoteWakeup = false;
 3876              	.loc 1 2810 0
 3877 001454  00 60 EF 	clr.b _RemoteWakeup
 3878              	.L154:
2811:lib/lib_pic33e/usb/usb_device.c ****     }//end if
2812:lib/lib_pic33e/usb/usb_device.c **** 
2813:lib/lib_pic33e/usb/usb_device.c ****     //Check if the host sent a valid SET or CLEAR endpoint halt request.
2814:lib/lib_pic33e/usb/usb_device.c ****     if((SetupPkt.bFeature == USB_FEATURE_ENDPOINT_HALT)&&
 3879              	.loc 1 2814 0
 3880 001456  20 00 20 	mov #_SetupPkt+2,w0
 3881 001458  10 40 78 	mov.b [w0],w0
 3882 00145a  00 04 E0 	cp0.b w0
 3883              	.set ___BP___,0
 3884 00145c  00 00 3A 	bra nz,.L153
2815:lib/lib_pic33e/usb/usb_device.c ****        (SetupPkt.Recipient == USB_SETUP_RECIPIENT_ENDPOINT_BITFIELD)&&
 3885              	.loc 1 2815 0
MPLAB XC16 ASSEMBLY Listing:   			page 119


 3886 00145e  00 00 20 	mov #_SetupPkt,w0
 3887 001460  10 40 78 	mov.b [w0],w0
 3888 001462  7F 40 60 	and.b w0,#31,w0
 3889              	.loc 1 2814 0
 3890 001464  E2 4F 50 	sub.b w0,#2,[w15]
 3891              	.set ___BP___,0
 3892 001466  00 00 3A 	bra nz,.L153
2816:lib/lib_pic33e/usb/usb_device.c ****        (SetupPkt.EPNum != 0) && (SetupPkt.EPNum <= USB_MAX_EP_NUMBER)&&
 3893              	.loc 1 2816 0
 3894 001468  40 00 20 	mov #_SetupPkt+4,w0
 3895 00146a  10 40 78 	mov.b [w0],w0
 3896 00146c  6F 40 60 	and.b w0,#15,w0
 3897              	.loc 1 2815 0
 3898 00146e  00 04 E0 	cp0.b w0
 3899              	.set ___BP___,0
 3900 001470  00 00 32 	bra z,.L153
 3901              	.loc 1 2816 0
 3902 001472  40 00 20 	mov #_SetupPkt+4,w0
 3903 001474  90 40 78 	mov.b [w0],w1
 3904 001476  6F C0 60 	and.b w1,#15,w0
 3905 001478  00 80 FB 	ze w0,w0
 3906 00147a  E4 0F 50 	sub w0,#4,[w15]
 3907              	.set ___BP___,0
 3908 00147c  00 00 3C 	bra gt,.L153
2817:lib/lib_pic33e/usb/usb_device.c ****        (USBDeviceState == CONFIGURED_STATE))
 3909              	.loc 1 2817 0
 3910 00147e  01 00 80 	mov _USBDeviceState,w1
 3911              	.loc 1 2816 0
 3912 001480  00 02 20 	mov #32,w0
 3913 001482  80 8F 50 	sub w1,w0,[w15]
 3914              	.set ___BP___,0
 3915 001484  00 00 3A 	bra nz,.L153
2818:lib/lib_pic33e/usb/usb_device.c ****     {
2819:lib/lib_pic33e/usb/usb_device.c **** 		//The request was valid.  Take control of the control transfer and
2820:lib/lib_pic33e/usb/usb_device.c **** 		//perform the host requested action.
2821:lib/lib_pic33e/usb/usb_device.c **** 		inPipes[0].info.bits.busy = 1;
2822:lib/lib_pic33e/usb/usb_device.c **** 
2823:lib/lib_pic33e/usb/usb_device.c ****         //Fetch a pointer to the BDT that the host wants to SET/CLEAR halt on.
2824:lib/lib_pic33e/usb/usb_device.c ****         if(SetupPkt.EPDir == OUT_FROM_HOST)
 3916              	.loc 1 2824 0
 3917 001486  41 00 20 	mov #_SetupPkt+4,w1
 3918              	.loc 1 2821 0
 3919 001488  12 00 80 	mov _inPipes+2,w2
 3920              	.loc 1 2824 0
 3921 00148a  00 C8 B3 	mov.b #-128,w0
 3922              	.loc 1 2821 0
 3923 00148c  02 70 A0 	bset w2,#7
 3924 00148e  12 00 88 	mov w2,_inPipes+2
 3925              	.loc 1 2824 0
 3926 001490  91 40 78 	mov.b [w1],w1
 3927 001492  00 C0 60 	and.b w1,w0,w0
 3928 001494  00 04 E0 	cp0.b w0
 3929              	.set ___BP___,0
 3930 001496  00 00 3A 	bra nz,.L157
2825:lib/lib_pic33e/usb/usb_device.c ****         {
2826:lib/lib_pic33e/usb/usb_device.c ****             p = (BDT_ENTRY*)pBDTEntryOut[SetupPkt.EPNum];
 3931              	.loc 1 2826 0
MPLAB XC16 ASSEMBLY Listing:   			page 120


 3932 001498  40 00 20 	mov #_SetupPkt+4,w0
 3933 00149a  01 00 20 	mov #_pBDTEntryOut,w1
 3934 00149c  10 41 78 	mov.b [w0],w2
 3935 00149e  6F 40 61 	and.b w2,#15,w0
2827:lib/lib_pic33e/usb/usb_device.c ****             current_ep_data.Val = ep_data_out[SetupPkt.EPNum].Val;
 3936              	.loc 1 2827 0
 3937 0014a0  42 00 20 	mov #_SetupPkt+4,w2
 3938              	.loc 1 2826 0
 3939 0014a2  00 80 FB 	ze w0,w0
 3940              	.loc 1 2827 0
 3941 0014a4  12 42 78 	mov.b [w2],w4
 3942 0014a6  EF 41 62 	and.b w4,#15,w3
 3943              	.loc 1 2826 0
 3944 0014a8  00 01 40 	add w0,w0,w2
 3945              	.loc 1 2827 0
 3946 0014aa  03 80 FB 	ze w3,w0
 3947              	.loc 1 2826 0
 3948 0014ac  81 00 41 	add w2,w1,w1
 3949              	.loc 1 2827 0
 3950 0014ae  02 00 20 	mov #_ep_data_out,w2
 3951 0014b0  00 00 41 	add w2,w0,w0
 3952              	.loc 1 2826 0
 3953 0014b2  11 0F 78 	mov [w1],[w14]
 3954              	.loc 1 2827 0
 3955 0014b4  10 40 78 	mov.b [w0],w0
 3956 0014b6  40 47 98 	mov.b w0,[w14+4]
 3957 0014b8  00 00 37 	bra .L158
 3958              	.L157:
2828:lib/lib_pic33e/usb/usb_device.c ****         }
2829:lib/lib_pic33e/usb/usb_device.c ****         else
2830:lib/lib_pic33e/usb/usb_device.c ****         {
2831:lib/lib_pic33e/usb/usb_device.c ****             p = (BDT_ENTRY*)pBDTEntryIn[SetupPkt.EPNum];
 3959              	.loc 1 2831 0
 3960 0014ba  40 00 20 	mov #_SetupPkt+4,w0
 3961 0014bc  01 00 20 	mov #_pBDTEntryIn,w1
 3962 0014be  10 41 78 	mov.b [w0],w2
 3963 0014c0  6F 40 61 	and.b w2,#15,w0
2832:lib/lib_pic33e/usb/usb_device.c ****             current_ep_data.Val = ep_data_in[SetupPkt.EPNum].Val;
 3964              	.loc 1 2832 0
 3965 0014c2  42 00 20 	mov #_SetupPkt+4,w2
 3966              	.loc 1 2831 0
 3967 0014c4  00 80 FB 	ze w0,w0
 3968              	.loc 1 2832 0
 3969 0014c6  12 42 78 	mov.b [w2],w4
 3970 0014c8  EF 41 62 	and.b w4,#15,w3
 3971              	.loc 1 2831 0
 3972 0014ca  00 01 40 	add w0,w0,w2
 3973              	.loc 1 2832 0
 3974 0014cc  03 80 FB 	ze w3,w0
 3975              	.loc 1 2831 0
 3976 0014ce  81 00 41 	add w2,w1,w1
 3977              	.loc 1 2832 0
 3978 0014d0  02 00 20 	mov #_ep_data_in,w2
 3979 0014d2  00 00 41 	add w2,w0,w0
 3980              	.loc 1 2831 0
 3981 0014d4  11 0F 78 	mov [w1],[w14]
 3982              	.loc 1 2832 0
MPLAB XC16 ASSEMBLY Listing:   			page 121


 3983 0014d6  10 40 78 	mov.b [w0],w0
 3984 0014d8  40 47 98 	mov.b w0,[w14+4]
 3985              	.L158:
2833:lib/lib_pic33e/usb/usb_device.c ****         }
2834:lib/lib_pic33e/usb/usb_device.c **** 
2835:lib/lib_pic33e/usb/usb_device.c ****         //If ping pong buffering is enabled on the requested endpoint, need 
2836:lib/lib_pic33e/usb/usb_device.c ****         //to point to the one that is the active BDT entry which the SIE will 
2837:lib/lib_pic33e/usb/usb_device.c ****         //use for the next attempted transaction on that EP number.
2838:lib/lib_pic33e/usb/usb_device.c ****         #if (USB_PING_PONG_MODE == USB_PING_PONG__ALL_BUT_EP0) || (USB_PING_PONG_MODE == USB_PING_P
2839:lib/lib_pic33e/usb/usb_device.c ****             if(current_ep_data.bits.ping_pong_state == 0) //Check if even
 3986              	.loc 1 2839 0
 3987 0014da  4E 40 90 	mov.b [w14+4],w0
 3988 0014dc  61 40 60 	and.b w0,#1,w0
 3989 0014de  00 04 E0 	cp0.b w0
 3990              	.set ___BP___,0
 3991 0014e0  00 00 3A 	bra nz,.L159
2840:lib/lib_pic33e/usb/usb_device.c ****             {
2841:lib/lib_pic33e/usb/usb_device.c ****                 p = (BDT_ENTRY*)(((uintptr_t)p) & (~USB_NEXT_PING_PONG));
 3992              	.loc 1 2841 0
 3993 0014e2  1E 00 78 	mov [w14],w0
 3994 0014e4  00 30 A1 	bclr w0,#3
 3995 0014e6  00 0F 78 	mov w0,[w14]
 3996 0014e8  00 00 37 	bra .L160
 3997              	.L159:
2842:lib/lib_pic33e/usb/usb_device.c ****             }
2843:lib/lib_pic33e/usb/usb_device.c ****             else //else must have been odd
2844:lib/lib_pic33e/usb/usb_device.c ****             {
2845:lib/lib_pic33e/usb/usb_device.c ****                 p = (BDT_ENTRY*)(((uintptr_t)p) | USB_NEXT_PING_PONG);
 3998              	.loc 1 2845 0
 3999 0014ea  1E 00 78 	mov [w14],w0
 4000 0014ec  00 30 A0 	bset w0,#3
 4001 0014ee  00 0F 78 	mov w0,[w14]
 4002              	.L160:
2846:lib/lib_pic33e/usb/usb_device.c ****             }
2847:lib/lib_pic33e/usb/usb_device.c ****         #endif
2848:lib/lib_pic33e/usb/usb_device.c ****         
2849:lib/lib_pic33e/usb/usb_device.c ****         //Update the BDT pointers with the new, next entry based on the feature
2850:lib/lib_pic33e/usb/usb_device.c ****         //  request
2851:lib/lib_pic33e/usb/usb_device.c ****         if(SetupPkt.EPDir == OUT_FROM_HOST)
 4003              	.loc 1 2851 0
 4004 0014f0  41 00 20 	mov #_SetupPkt+4,w1
 4005 0014f2  00 C8 B3 	mov.b #-128,w0
 4006 0014f4  91 40 78 	mov.b [w1],w1
 4007 0014f6  00 C0 60 	and.b w1,w0,w0
 4008 0014f8  00 04 E0 	cp0.b w0
 4009              	.set ___BP___,0
 4010 0014fa  00 00 3A 	bra nz,.L161
2852:lib/lib_pic33e/usb/usb_device.c ****         {
2853:lib/lib_pic33e/usb/usb_device.c ****             pBDTEntryOut[SetupPkt.EPNum] = (volatile BDT_ENTRY *)p;
 4011              	.loc 1 2853 0
 4012 0014fc  40 00 20 	mov #_SetupPkt+4,w0
 4013 0014fe  9E 00 78 	mov [w14],w1
 4014 001500  10 41 78 	mov.b [w0],w2
 4015 001502  6F 40 61 	and.b w2,#15,w0
 4016 001504  02 00 20 	mov #_pBDTEntryOut,w2
 4017 001506  00 80 FB 	ze w0,w0
 4018 001508  00 00 40 	add w0,w0,w0
MPLAB XC16 ASSEMBLY Listing:   			page 122


 4019 00150a  02 00 40 	add w0,w2,w0
 4020 00150c  01 08 78 	mov w1,[w0]
 4021 00150e  00 00 37 	bra .L162
 4022              	.L161:
2854:lib/lib_pic33e/usb/usb_device.c ****         }
2855:lib/lib_pic33e/usb/usb_device.c ****         else
2856:lib/lib_pic33e/usb/usb_device.c ****         {
2857:lib/lib_pic33e/usb/usb_device.c ****             pBDTEntryIn[SetupPkt.EPNum] = (volatile BDT_ENTRY *)p;
 4023              	.loc 1 2857 0
 4024 001510  40 00 20 	mov #_SetupPkt+4,w0
 4025 001512  9E 00 78 	mov [w14],w1
 4026 001514  10 42 78 	mov.b [w0],w4
 4027 001516  6F 40 62 	and.b w4,#15,w0
 4028 001518  02 00 20 	mov #_pBDTEntryIn,w2
 4029 00151a  00 80 FB 	ze w0,w0
 4030 00151c  00 00 40 	add w0,w0,w0
 4031 00151e  02 00 40 	add w0,w2,w0
 4032 001520  01 08 78 	mov w1,[w0]
 4033              	.L162:
2858:lib/lib_pic33e/usb/usb_device.c ****         }
2859:lib/lib_pic33e/usb/usb_device.c **** 
2860:lib/lib_pic33e/usb/usb_device.c **** 		//Check if it was a SET_FEATURE endpoint halt request
2861:lib/lib_pic33e/usb/usb_device.c ****         if(SetupPkt.bRequest == USB_REQUEST_SET_FEATURE)
 4034              	.loc 1 2861 0
 4035 001522  10 00 20 	mov #_SetupPkt+1,w0
 4036 001524  10 40 78 	mov.b [w0],w0
 4037 001526  E3 4F 50 	sub.b w0,#3,[w15]
 4038              	.set ___BP___,0
 4039 001528  00 00 3A 	bra nz,.L163
2862:lib/lib_pic33e/usb/usb_device.c ****         {
2863:lib/lib_pic33e/usb/usb_device.c ****             if(p->STAT.UOWN == 1)
 4040              	.loc 1 2863 0
 4041 00152a  9E 00 78 	mov [w14],w1
 4042 00152c  00 C8 B3 	mov.b #-128,w0
 4043 00152e  91 40 78 	mov.b [w1],w1
 4044 001530  00 C0 60 	and.b w1,w0,w0
 4045 001532  00 04 E0 	cp0.b w0
 4046              	.set ___BP___,0
 4047 001534  00 00 32 	bra z,.L164
2864:lib/lib_pic33e/usb/usb_device.c ****             {
2865:lib/lib_pic33e/usb/usb_device.c ****                 //Mark that we are terminating this transfer and that the user
2866:lib/lib_pic33e/usb/usb_device.c ****                 //  needs to be notified later
2867:lib/lib_pic33e/usb/usb_device.c ****                 if(SetupPkt.EPDir == OUT_FROM_HOST)
 4048              	.loc 1 2867 0
 4049 001536  41 00 20 	mov #_SetupPkt+4,w1
 4050 001538  00 C8 B3 	mov.b #-128,w0
 4051 00153a  91 40 78 	mov.b [w1],w1
 4052 00153c  00 C0 60 	and.b w1,w0,w0
 4053 00153e  00 04 E0 	cp0.b w0
 4054              	.set ___BP___,0
 4055 001540  00 00 3A 	bra nz,.L165
2868:lib/lib_pic33e/usb/usb_device.c ****                 {
2869:lib/lib_pic33e/usb/usb_device.c ****                     ep_data_out[SetupPkt.EPNum].bits.transfer_terminated = 1;
 4056              	.loc 1 2869 0
 4057 001542  41 00 20 	mov #_SetupPkt+4,w1
 4058 001544  00 00 20 	mov #_ep_data_out,w0
 4059 001546  11 41 78 	mov.b [w1],w2
MPLAB XC16 ASSEMBLY Listing:   			page 123


 4060 001548  EF 40 61 	and.b w2,#15,w1
 4061 00154a  81 80 FB 	ze w1,w1
 4062 00154c  00 80 40 	add w1,w0,w0
 4063 00154e  90 40 78 	mov.b [w0],w1
 4064 001550  01 14 A0 	bset.b w1,#1
 4065 001552  01 48 78 	mov.b w1,[w0]
 4066 001554  00 00 37 	bra .L164
 4067              	.L165:
2870:lib/lib_pic33e/usb/usb_device.c ****                 }
2871:lib/lib_pic33e/usb/usb_device.c ****                 else
2872:lib/lib_pic33e/usb/usb_device.c ****                 {
2873:lib/lib_pic33e/usb/usb_device.c ****                     ep_data_in[SetupPkt.EPNum].bits.transfer_terminated = 1;
 4068              	.loc 1 2873 0
 4069 001556  41 00 20 	mov #_SetupPkt+4,w1
 4070 001558  00 00 20 	mov #_ep_data_in,w0
 4071 00155a  11 42 78 	mov.b [w1],w4
 4072 00155c  EF 40 62 	and.b w4,#15,w1
 4073 00155e  81 80 FB 	ze w1,w1
 4074 001560  00 80 40 	add w1,w0,w0
 4075 001562  90 40 78 	mov.b [w0],w1
 4076 001564  01 14 A0 	bset.b w1,#1
 4077 001566  01 48 78 	mov.b w1,[w0]
 4078              	.L164:
2874:lib/lib_pic33e/usb/usb_device.c ****                 }
2875:lib/lib_pic33e/usb/usb_device.c ****             }
2876:lib/lib_pic33e/usb/usb_device.c **** 
2877:lib/lib_pic33e/usb/usb_device.c **** 			//Then STALL the endpoint
2878:lib/lib_pic33e/usb/usb_device.c ****             p->STAT.Val |= _BSTALL;
 4079              	.loc 1 2878 0
 4080 001568  9E 00 78 	mov [w14],w1
 4081 00156a  1E 00 78 	mov [w14],w0
 4082 00156c  91 82 FB 	ze [w1],w5
 4083 00156e  91 40 90 	mov.b [w1+1],w1
 4084 001570  10 42 78 	mov.b [w0],w4
 4085 001572  90 41 90 	mov.b [w0+1],w3
 4086 001574  81 80 FB 	ze w1,w1
 4087 001576  F2 0F 20 	mov #255,w2
 4088 001578  C8 08 DD 	sl w1,#8,w1
 4089 00157a  60 42 62 	and.b w4,#0,w4
 4090 00157c  81 80 72 	ior w5,w1,w1
 4091 00157e  E0 C1 61 	and.b w3,#0,w3
 4092 001580  01 20 A0 	bset w1,#2
 4093 001582  02 81 60 	and w1,w2,w2
 4094 001584  C8 08 DE 	lsr w1,#8,w1
 4095 001586  02 41 72 	ior.b w4,w2,w2
 4096 001588  81 C0 71 	ior.b w3,w1,w1
 4097 00158a  02 48 78 	mov.b w2,[w0]
 4098 00158c  11 40 98 	mov.b w1,[w0+1]
2879:lib/lib_pic33e/usb/usb_device.c ****             p->STAT.Val |= _USIE;
 4099              	.loc 1 2879 0
 4100 00158e  9E 00 78 	mov [w14],w1
 4101 001590  1E 00 78 	mov [w14],w0
 4102 001592  91 82 FB 	ze [w1],w5
 4103 001594  91 40 90 	mov.b [w1+1],w1
 4104 001596  10 42 78 	mov.b [w0],w4
 4105 001598  90 41 90 	mov.b [w0+1],w3
 4106 00159a  81 80 FB 	ze w1,w1
MPLAB XC16 ASSEMBLY Listing:   			page 124


 4107 00159c  F2 0F 20 	mov #255,w2
 4108 00159e  C8 08 DD 	sl w1,#8,w1
 4109 0015a0  60 42 62 	and.b w4,#0,w4
 4110 0015a2  81 80 72 	ior w5,w1,w1
 4111 0015a4  E0 C1 61 	and.b w3,#0,w3
 4112 0015a6  01 70 A0 	bset w1,#7
 4113 0015a8  02 81 60 	and w1,w2,w2
 4114 0015aa  C8 08 DE 	lsr w1,#8,w1
 4115 0015ac  02 41 72 	ior.b w4,w2,w2
 4116 0015ae  81 C0 71 	ior.b w3,w1,w1
 4117 0015b0  02 48 78 	mov.b w2,[w0]
 4118 0015b2  11 40 98 	mov.b w1,[w0+1]
 4119 0015b4  00 00 37 	bra .L153
 4120              	.L163:
2880:lib/lib_pic33e/usb/usb_device.c ****         }//if(SetupPkt.bRequest == USB_REQUEST_SET_FEATURE)
2881:lib/lib_pic33e/usb/usb_device.c ****         else
2882:lib/lib_pic33e/usb/usb_device.c ****         {
2883:lib/lib_pic33e/usb/usb_device.c **** 			//Else the request must have been a CLEAR_FEATURE endpoint halt.
2884:lib/lib_pic33e/usb/usb_device.c ****             #if (USB_PING_PONG_MODE == USB_PING_PONG__ALL_BUT_EP0) || (USB_PING_PONG_MODE == USB_PI
2885:lib/lib_pic33e/usb/usb_device.c ****                 //toggle over the to the non-active BDT
2886:lib/lib_pic33e/usb/usb_device.c ****                 p = (BDT_ENTRY*)(((uintptr_t)p) ^ USB_NEXT_PING_PONG);  
 4121              	.loc 1 2886 0
 4122 0015b6  1E 00 78 	mov [w14],w0
 4123 0015b8  00 30 A2 	btg w0,#3
 4124 0015ba  00 0F 78 	mov w0,[w14]
2887:lib/lib_pic33e/usb/usb_device.c **** 
2888:lib/lib_pic33e/usb/usb_device.c ****                 if(p->STAT.UOWN == 1)
 4125              	.loc 1 2888 0
 4126 0015bc  00 C8 B3 	mov.b #-128,w0
 4127 0015be  9E 00 78 	mov [w14],w1
 4128 0015c0  91 40 78 	mov.b [w1],w1
 4129 0015c2  00 C0 60 	and.b w1,w0,w0
 4130 0015c4  00 04 E0 	cp0.b w0
 4131              	.set ___BP___,0
 4132 0015c6  00 00 32 	bra z,.L166
2889:lib/lib_pic33e/usb/usb_device.c ****                 {
2890:lib/lib_pic33e/usb/usb_device.c ****                     //Clear UOWN and set DTS state so it will be correct the next time
2891:lib/lib_pic33e/usb/usb_device.c ****                     //the application firmware uses USBTransferOnePacket() on the EP.
2892:lib/lib_pic33e/usb/usb_device.c ****                     p->STAT.Val &= (~_USIE);    //Clear UOWN bit
 4133              	.loc 1 2892 0
 4134 0015c8  9E 00 78 	mov [w14],w1
 4135 0015ca  1E 00 78 	mov [w14],w0
 4136 0015cc  11 41 90 	mov.b [w1+1],w2
 4137 0015ce  91 80 FB 	ze [w1],w1
 4138 0015d0  10 42 78 	mov.b [w0],w4
 4139 0015d2  90 41 90 	mov.b [w0+1],w3
 4140 0015d4  02 81 FB 	ze w2,w2
 4141 0015d6  60 42 62 	and.b w4,#0,w4
 4142 0015d8  48 11 DD 	sl w2,#8,w2
 4143 0015da  E0 C1 61 	and.b w3,#0,w3
 4144 0015dc  82 80 70 	ior w1,w2,w1
 4145 0015de  F2 0F 20 	mov #255,w2
 4146 0015e0  01 70 A1 	bclr w1,#7
 4147 0015e2  02 81 60 	and w1,w2,w2
 4148 0015e4  C8 08 DE 	lsr w1,#8,w1
 4149 0015e6  02 41 72 	ior.b w4,w2,w2
 4150 0015e8  81 C0 71 	ior.b w3,w1,w1
MPLAB XC16 ASSEMBLY Listing:   			page 125


 4151 0015ea  02 48 78 	mov.b w2,[w0]
 4152 0015ec  11 40 98 	mov.b w1,[w0+1]
2893:lib/lib_pic33e/usb/usb_device.c ****                     p->STAT.Val |= _DAT1;       //Set DTS to DATA1
 4153              	.loc 1 2893 0
 4154 0015ee  9E 00 78 	mov [w14],w1
 4155 0015f0  1E 00 78 	mov [w14],w0
 4156 0015f2  11 41 90 	mov.b [w1+1],w2
 4157 0015f4  91 80 FB 	ze [w1],w1
 4158 0015f6  10 42 78 	mov.b [w0],w4
 4159 0015f8  90 41 90 	mov.b [w0+1],w3
 4160 0015fa  02 81 FB 	ze w2,w2
 4161 0015fc  60 42 62 	and.b w4,#0,w4
 4162 0015fe  48 11 DD 	sl w2,#8,w2
 4163 001600  E0 C1 61 	and.b w3,#0,w3
 4164 001602  82 80 70 	ior w1,w2,w1
 4165 001604  F2 0F 20 	mov #255,w2
 4166 001606  01 60 A0 	bset w1,#6
 4167 001608  02 81 60 	and w1,w2,w2
 4168 00160a  C8 08 DE 	lsr w1,#8,w1
 4169 00160c  02 41 72 	ior.b w4,w2,w2
 4170 00160e  81 C0 71 	ior.b w3,w1,w1
 4171 001610  02 48 78 	mov.b w2,[w0]
 4172 001612  11 40 98 	mov.b w1,[w0+1]
2894:lib/lib_pic33e/usb/usb_device.c ****                     USB_TRANSFER_TERMINATED_HANDLER(EVENT_TRANSFER_TERMINATED,p,sizeof(p));
 4173              	.loc 1 2894 0
 4174 001614  22 00 20 	mov #2,w2
 4175 001616  9E 00 78 	mov [w14],w1
 4176 001618  50 00 20 	mov #5,w0
 4177 00161a  00 00 07 	rcall _USER_USB_CALLBACK_EVENT_HANDLER
 4178 00161c  00 00 37 	bra .L167
 4179              	.L166:
2895:lib/lib_pic33e/usb/usb_device.c ****                 }
2896:lib/lib_pic33e/usb/usb_device.c ****                 else
2897:lib/lib_pic33e/usb/usb_device.c ****                 {
2898:lib/lib_pic33e/usb/usb_device.c ****                     //UOWN already clear, but still need to set DTS to DATA1     
2899:lib/lib_pic33e/usb/usb_device.c **** 					p->STAT.Val |= _DAT1;
 4180              	.loc 1 2899 0
 4181 00161e  00 00 00 	nop 
 4182 001620  9E 00 78 	mov [w14],w1
 4183 001622  00 00 00 	nop 
 4184 001624  1E 00 78 	mov [w14],w0
 4185 001626  00 00 00 	nop 
 4186 001628  11 81 FB 	ze [w1],w2
 4187 00162a  00 00 00 	nop 
 4188 00162c  91 40 90 	mov.b [w1+1],w1
 4189 00162e  00 00 00 	nop 
 4190 001630  90 41 90 	mov.b [w0+1],w3
 4191 001632  81 80 FB 	ze w1,w1
 4192 001634  E0 C1 61 	and.b w3,#0,w3
 4193 001636  C8 08 DD 	sl w1,#8,w1
 4194 001638  81 00 71 	ior w2,w1,w1
 4195 00163a  01 60 A0 	bset w1,#6
 4196 00163c  48 09 DE 	lsr w1,#8,w2
 4197 00163e  02 C1 71 	ior.b w3,w2,w2
 4198 001640  12 40 98 	mov.b w2,[w0+1]
 4199 001642  10 41 78 	mov.b [w0],w2
 4200 001644  F3 0F 20 	mov #255,w3
MPLAB XC16 ASSEMBLY Listing:   			page 126


 4201 001646  60 41 61 	and.b w2,#0,w2
 4202 001648  83 80 60 	and w1,w3,w1
 4203 00164a  81 40 71 	ior.b w2,w1,w1
 4204 00164c  01 48 78 	mov.b w1,[w0]
 4205              	.L167:
2900:lib/lib_pic33e/usb/usb_device.c ****                 }
2901:lib/lib_pic33e/usb/usb_device.c **** 
2902:lib/lib_pic33e/usb/usb_device.c ****                 //toggle back to the active BDT (the one the SIE is currently looking at
2903:lib/lib_pic33e/usb/usb_device.c ****                 //and will use for the next successful transaction to take place on the EP
2904:lib/lib_pic33e/usb/usb_device.c ****                 p = (BDT_ENTRY*)(((uintptr_t)p) ^ USB_NEXT_PING_PONG);
 4206              	.loc 1 2904 0
 4207 00164e  9E 00 78 	mov [w14],w1
2905:lib/lib_pic33e/usb/usb_device.c ****                 
2906:lib/lib_pic33e/usb/usb_device.c ****                 //Check if we are currently terminating, or have previously terminated
2907:lib/lib_pic33e/usb/usb_device.c ****                 //a transaction on the given endpoint.  If so, need to clear UOWN,
2908:lib/lib_pic33e/usb/usb_device.c ****                 //set DTS to the proper state, and call the application callback
2909:lib/lib_pic33e/usb/usb_device.c ****                 //function.
2910:lib/lib_pic33e/usb/usb_device.c ****                 if((current_ep_data.bits.transfer_terminated != 0) || (p->STAT.UOWN == 1))
 4208              	.loc 1 2910 0
 4209 001650  4E 40 90 	mov.b [w14+4],w0
 4210              	.loc 1 2904 0
 4211 001652  01 30 A2 	btg w1,#3
 4212              	.loc 1 2910 0
 4213 001654  62 40 60 	and.b w0,#2,w0
 4214              	.loc 1 2904 0
 4215 001656  01 0F 78 	mov w1,[w14]
 4216              	.loc 1 2910 0
 4217 001658  00 04 E0 	cp0.b w0
 4218              	.set ___BP___,0
 4219 00165a  00 00 3A 	bra nz,.L168
 4220 00165c  9E 00 78 	mov [w14],w1
 4221 00165e  00 C8 B3 	mov.b #-128,w0
 4222 001660  91 40 78 	mov.b [w1],w1
 4223 001662  00 C0 60 	and.b w1,w0,w0
 4224 001664  00 04 E0 	cp0.b w0
 4225              	.set ___BP___,0
 4226 001666  00 00 32 	bra z,.L169
 4227              	.L168:
2911:lib/lib_pic33e/usb/usb_device.c ****                 {
2912:lib/lib_pic33e/usb/usb_device.c ****                     if(SetupPkt.EPDir == OUT_FROM_HOST)
 4228              	.loc 1 2912 0
 4229 001668  41 00 20 	mov #_SetupPkt+4,w1
 4230 00166a  00 C8 B3 	mov.b #-128,w0
 4231 00166c  91 40 78 	mov.b [w1],w1
 4232 00166e  00 C0 60 	and.b w1,w0,w0
 4233 001670  00 04 E0 	cp0.b w0
 4234              	.set ___BP___,0
 4235 001672  00 00 3A 	bra nz,.L170
2913:lib/lib_pic33e/usb/usb_device.c ****                     {
2914:lib/lib_pic33e/usb/usb_device.c ****                         ep_data_out[SetupPkt.EPNum].bits.transfer_terminated = 0;
 4236              	.loc 1 2914 0
 4237 001674  41 00 20 	mov #_SetupPkt+4,w1
 4238 001676  00 00 20 	mov #_ep_data_out,w0
 4239 001678  11 41 78 	mov.b [w1],w2
 4240 00167a  EF 40 61 	and.b w2,#15,w1
 4241 00167c  81 80 FB 	ze w1,w1
 4242 00167e  00 80 40 	add w1,w0,w0
MPLAB XC16 ASSEMBLY Listing:   			page 127


 4243 001680  90 40 78 	mov.b [w0],w1
 4244 001682  01 14 A1 	bclr.b w1,#1
 4245 001684  01 48 78 	mov.b w1,[w0]
 4246 001686  00 00 37 	bra .L171
 4247              	.L170:
2915:lib/lib_pic33e/usb/usb_device.c ****                     }
2916:lib/lib_pic33e/usb/usb_device.c ****                     else
2917:lib/lib_pic33e/usb/usb_device.c ****                     {
2918:lib/lib_pic33e/usb/usb_device.c ****                         ep_data_in[SetupPkt.EPNum].bits.transfer_terminated = 0;
 4248              	.loc 1 2918 0
 4249 001688  41 00 20 	mov #_SetupPkt+4,w1
 4250 00168a  00 00 20 	mov #_ep_data_in,w0
 4251 00168c  11 42 78 	mov.b [w1],w4
 4252 00168e  EF 40 62 	and.b w4,#15,w1
 4253 001690  81 80 FB 	ze w1,w1
 4254 001692  00 80 40 	add w1,w0,w0
 4255 001694  90 40 78 	mov.b [w0],w1
 4256 001696  01 14 A1 	bclr.b w1,#1
 4257 001698  01 48 78 	mov.b w1,[w0]
 4258              	.L171:
2919:lib/lib_pic33e/usb/usb_device.c ****                     }
2920:lib/lib_pic33e/usb/usb_device.c ****                     //clear UOWN, clear DTS to DATA0, and finally remove the STALL condition     
2921:lib/lib_pic33e/usb/usb_device.c ****                     p->STAT.Val &= ~(_USIE | _DAT1 | _BSTALL);  
 4259              	.loc 1 2921 0
 4260 00169a  9E 00 78 	mov [w14],w1
 4261 00169c  1E 00 78 	mov [w14],w0
 4262 00169e  11 41 90 	mov.b [w1+1],w2
 4263 0016a0  91 80 FB 	ze [w1],w1
 4264 0016a2  10 42 78 	mov.b [w0],w4
 4265 0016a4  90 41 90 	mov.b [w0+1],w3
 4266 0016a6  02 81 FB 	ze w2,w2
 4267 0016a8  60 42 62 	and.b w4,#0,w4
 4268 0016aa  48 11 DD 	sl w2,#8,w2
 4269 0016ac  E0 C1 61 	and.b w3,#0,w3
 4270 0016ae  02 81 70 	ior w1,w2,w2
 4271 0016b0  B1 F3 2F 	mov #-197,w1
 4272 0016b2  82 02 78 	mov w2,w5
 4273 0016b4  F2 0F 20 	mov #255,w2
 4274 0016b6  81 80 62 	and w5,w1,w1
 4275 0016b8  02 81 60 	and w1,w2,w2
 4276 0016ba  C8 08 DE 	lsr w1,#8,w1
 4277 0016bc  02 41 72 	ior.b w4,w2,w2
 4278 0016be  81 C0 71 	ior.b w3,w1,w1
 4279 0016c0  02 48 78 	mov.b w2,[w0]
 4280 0016c2  11 40 98 	mov.b w1,[w0+1]
2922:lib/lib_pic33e/usb/usb_device.c ****                     //Call the application event handler callback function, so it can 
2923:lib/lib_pic33e/usb/usb_device.c **** 					//decide if the endpoint should get re-armed again or not.
2924:lib/lib_pic33e/usb/usb_device.c ****                     USB_TRANSFER_TERMINATED_HANDLER(EVENT_TRANSFER_TERMINATED,p,sizeof(p));
 4281              	.loc 1 2924 0
 4282 0016c4  22 00 20 	mov #2,w2
 4283 0016c6  9E 00 78 	mov [w14],w1
 4284 0016c8  50 00 20 	mov #5,w0
 4285 0016ca  00 00 07 	rcall _USER_USB_CALLBACK_EVENT_HANDLER
 4286 0016cc  00 00 37 	bra .L172
 4287              	.L169:
2925:lib/lib_pic33e/usb/usb_device.c ****                 }
2926:lib/lib_pic33e/usb/usb_device.c ****                 else
MPLAB XC16 ASSEMBLY Listing:   			page 128


2927:lib/lib_pic33e/usb/usb_device.c ****                 {
2928:lib/lib_pic33e/usb/usb_device.c ****                     //clear UOWN, clear DTS to DATA0, and finally remove the STALL condition     
2929:lib/lib_pic33e/usb/usb_device.c ****                     p->STAT.Val &= ~(_USIE | _DAT1 | _BSTALL); 
 4288              	.loc 1 2929 0
 4289 0016ce  00 00 00 	nop 
 4290 0016d0  9E 00 78 	mov [w14],w1
 4291 0016d2  00 00 00 	nop 
 4292 0016d4  1E 00 78 	mov [w14],w0
 4293 0016d6  00 00 00 	nop 
 4294 0016d8  11 81 FB 	ze [w1],w2
 4295 0016da  00 00 00 	nop 
 4296 0016dc  91 40 90 	mov.b [w1+1],w1
 4297 0016de  00 00 00 	nop 
 4298 0016e0  90 41 90 	mov.b [w0+1],w3
 4299 0016e2  01 82 FB 	ze w1,w4
 4300 0016e4  B1 F3 2F 	mov #-197,w1
 4301 0016e6  48 22 DD 	sl w4,#8,w4
 4302 0016e8  E0 C1 61 	and.b w3,#0,w3
 4303 0016ea  04 01 71 	ior w2,w4,w2
 4304 0016ec  81 00 61 	and w2,w1,w1
 4305 0016ee  48 09 DE 	lsr w1,#8,w2
 4306 0016f0  02 C1 71 	ior.b w3,w2,w2
 4307 0016f2  12 40 98 	mov.b w2,[w0+1]
 4308 0016f4  10 41 78 	mov.b [w0],w2
 4309 0016f6  F3 0F 20 	mov #255,w3
 4310 0016f8  60 41 61 	and.b w2,#0,w2
 4311 0016fa  83 80 60 	and w1,w3,w1
 4312 0016fc  81 40 71 	ior.b w2,w1,w1
 4313 0016fe  01 48 78 	mov.b w1,[w0]
 4314              	.L172:
2930:lib/lib_pic33e/usb/usb_device.c ****                 } 
2931:lib/lib_pic33e/usb/usb_device.c ****             #else //else we must not be using ping-pong buffering on the requested endpoint
2932:lib/lib_pic33e/usb/usb_device.c ****                 //Check if we need to call the user transfer terminated event callback function.
2933:lib/lib_pic33e/usb/usb_device.c ****                 //We should call the callback, if the endpoint was previously terminated,
2934:lib/lib_pic33e/usb/usb_device.c ****                 //or the endpoint is currently armed, and the host is performing clear
2935:lib/lib_pic33e/usb/usb_device.c ****                 //endpoint halt, even though the endpoint wasn't stalled.
2936:lib/lib_pic33e/usb/usb_device.c ****                 if((current_ep_data.bits.transfer_terminated != 0) || (p->STAT.UOWN == 1))
2937:lib/lib_pic33e/usb/usb_device.c ****                 {
2938:lib/lib_pic33e/usb/usb_device.c ****                     //We are going to call the user transfer terminated callback.
2939:lib/lib_pic33e/usb/usb_device.c ****                     //Clear the flag so we know we took care of it and don't need
2940:lib/lib_pic33e/usb/usb_device.c ****                     //to call it again later.
2941:lib/lib_pic33e/usb/usb_device.c ****                     if(SetupPkt.EPDir == OUT_FROM_HOST)
2942:lib/lib_pic33e/usb/usb_device.c ****                     {
2943:lib/lib_pic33e/usb/usb_device.c ****                         ep_data_out[SetupPkt.EPNum].bits.transfer_terminated = 0;
2944:lib/lib_pic33e/usb/usb_device.c ****                     }
2945:lib/lib_pic33e/usb/usb_device.c ****                     else
2946:lib/lib_pic33e/usb/usb_device.c ****                     {
2947:lib/lib_pic33e/usb/usb_device.c ****                         ep_data_in[SetupPkt.EPNum].bits.transfer_terminated = 0;
2948:lib/lib_pic33e/usb/usb_device.c ****                     }
2949:lib/lib_pic33e/usb/usb_device.c ****  
2950:lib/lib_pic33e/usb/usb_device.c ****                     //Clear UOWN and remove the STALL condition.   
2951:lib/lib_pic33e/usb/usb_device.c ****                     //  In this case we also need to set the DTS bit to 1 so that
2952:lib/lib_pic33e/usb/usb_device.c ****                     //  it toggles to DATA0 the next time the application firmware
2953:lib/lib_pic33e/usb/usb_device.c ****                     //  calls USBTransferOnePacket() (or equivalent macro).  
2954:lib/lib_pic33e/usb/usb_device.c ****                     p->STAT.Val &= ~(_USIE | _BSTALL);  
2955:lib/lib_pic33e/usb/usb_device.c ****                     p->STAT.Val |= _DAT1;
2956:lib/lib_pic33e/usb/usb_device.c ****                     //Let the application firmware know a transaction just
MPLAB XC16 ASSEMBLY Listing:   			page 129


2957:lib/lib_pic33e/usb/usb_device.c ****                     //got terminated by the host, and that it is now free to
2958:lib/lib_pic33e/usb/usb_device.c ****                     //re-arm the endpoint or do other tasks if desired.                            
2959:lib/lib_pic33e/usb/usb_device.c ****                     USB_TRANSFER_TERMINATED_HANDLER(EVENT_TRANSFER_TERMINATED,p,sizeof(p));
2960:lib/lib_pic33e/usb/usb_device.c ****                 }
2961:lib/lib_pic33e/usb/usb_device.c ****                 else
2962:lib/lib_pic33e/usb/usb_device.c ****                 {
2963:lib/lib_pic33e/usb/usb_device.c ****                     //Clear UOWN and remove the STALL condition.   
2964:lib/lib_pic33e/usb/usb_device.c ****                     //  In this case we also need to set the DTS bit to 1 so that
2965:lib/lib_pic33e/usb/usb_device.c ****                     //  it toggles to DATA0 the next time the application firmware
2966:lib/lib_pic33e/usb/usb_device.c ****                     //  calls USBTransferOnePacket() (or equivalent macro).  
2967:lib/lib_pic33e/usb/usb_device.c ****                     p->STAT.Val &= ~(_USIE | _BSTALL);  
2968:lib/lib_pic33e/usb/usb_device.c ****                     p->STAT.Val |= _DAT1;
2969:lib/lib_pic33e/usb/usb_device.c ****                 } 
2970:lib/lib_pic33e/usb/usb_device.c ****             #endif //end of #if (USB_PING_PONG_MODE == USB_PING_PONG__ALL_BUT_EP0) || (USB_PING_PON
2971:lib/lib_pic33e/usb/usb_device.c ****             
2972:lib/lib_pic33e/usb/usb_device.c **** 			//Get a pointer to the appropriate UEPn register
2973:lib/lib_pic33e/usb/usb_device.c ****             #if defined(__C32__)
2974:lib/lib_pic33e/usb/usb_device.c ****                 pUEP = (uint32_t*)(&U1EP0);
2975:lib/lib_pic33e/usb/usb_device.c ****                 pUEP += (SetupPkt.EPNum*4);
2976:lib/lib_pic33e/usb/usb_device.c ****             #else
2977:lib/lib_pic33e/usb/usb_device.c ****                 pUEP = (unsigned char*)(&U1EP0+SetupPkt.EPNum);
 4315              	.loc 1 2977 0
 4316 001700  01 00 20 	mov #_U1EP0,w1
 4317 001702  40 00 20 	mov #_SetupPkt+4,w0
 4318 001704  10 41 78 	mov.b [w0],w2
 4319 001706  6F 40 61 	and.b w2,#15,w0
 4320 001708  00 80 FB 	ze w0,w0
 4321 00170a  00 00 40 	add w0,w0,w0
 4322 00170c  00 80 40 	add w1,w0,w0
 4323 00170e  10 07 98 	mov w0,[w14+2]
2978:lib/lib_pic33e/usb/usb_device.c ****             #endif
2979:lib/lib_pic33e/usb/usb_device.c **** 
2980:lib/lib_pic33e/usb/usb_device.c **** 			//Clear the STALL bit in the UEP register
2981:lib/lib_pic33e/usb/usb_device.c ****             *pUEP &= ~UEP_STALL;            
 4324              	.loc 1 2981 0
 4325 001710  9E 00 90 	mov [w14+2],w1
 4326 001712  1E 00 90 	mov [w14+2],w0
 4327 001714  91 40 78 	mov.b [w1],w1
 4328 001716  01 14 A1 	bclr.b w1,#1
 4329 001718  01 48 78 	mov.b w1,[w0]
 4330              	.L153:
2982:lib/lib_pic33e/usb/usb_device.c ****         }//end if(SetupPkt.bRequest == USB_REQUEST_SET_FEATURE)
2983:lib/lib_pic33e/usb/usb_device.c ****     }//end if (lots of checks for set/clear endpoint halt)
2984:lib/lib_pic33e/usb/usb_device.c **** }//end USBStdFeatureReqHandler
 4331              	.loc 1 2984 0
 4332 00171a  8E 07 78 	mov w14,w15
 4333 00171c  4F 07 78 	mov [--w15],w14
 4334 00171e  00 40 A9 	bclr CORCON,#2
 4335 001720  00 00 06 	return 
 4336              	.set ___PA___,0
 4337              	.LFE25:
 4338              	.size _USBStdFeatureReqHandler,.-_USBStdFeatureReqHandler
 4339              	.align 2
 4340              	.global _USBIncrement1msInternalTimers
 4341              	.type _USBIncrement1msInternalTimers,@function
 4342              	_USBIncrement1msInternalTimers:
 4343              	.LFB26:
MPLAB XC16 ASSEMBLY Listing:   			page 130


2985:lib/lib_pic33e/usb/usb_device.c **** 
2986:lib/lib_pic33e/usb/usb_device.c **** 
2987:lib/lib_pic33e/usb/usb_device.c **** 
2988:lib/lib_pic33e/usb/usb_device.c **** 
2989:lib/lib_pic33e/usb/usb_device.c **** /**************************************************************************
2990:lib/lib_pic33e/usb/usb_device.c ****     Function:
2991:lib/lib_pic33e/usb/usb_device.c ****         void USBIncrement1msInternalTimers(void)
2992:lib/lib_pic33e/usb/usb_device.c **** 
2993:lib/lib_pic33e/usb/usb_device.c ****     Description:
2994:lib/lib_pic33e/usb/usb_device.c ****         This function increments internal 1ms time base counters, which are
2995:lib/lib_pic33e/usb/usb_device.c ****         useful for application code (that can use a 1ms time base/counter), and
2996:lib/lib_pic33e/usb/usb_device.c ****         for certain USB event timing specific purposes.
2997:lib/lib_pic33e/usb/usb_device.c **** 
2998:lib/lib_pic33e/usb/usb_device.c ****         In USB full speed applications, the application code does not need to (and should
2999:lib/lib_pic33e/usb/usb_device.c ****         not) explicitly call this function, as the USBDeviceTasks() function will
3000:lib/lib_pic33e/usb/usb_device.c ****         automatically call this function whenever a 1ms time interval has elapsed
3001:lib/lib_pic33e/usb/usb_device.c ****         (assuming the code is calling USBDeviceTasks() frequently enough in USB_POLLING
3002:lib/lib_pic33e/usb/usb_device.c ****         mode, or that USB interrupts aren't being masked for more than 1ms at a time
3003:lib/lib_pic33e/usb/usb_device.c ****         in USB_INTERRUPT mode).
3004:lib/lib_pic33e/usb/usb_device.c **** 
3005:lib/lib_pic33e/usb/usb_device.c ****         In USB low speed applications, the application firmware is responsible for
3006:lib/lib_pic33e/usb/usb_device.c ****         periodically calling this function at a ~1ms rate.  This can be done using
3007:lib/lib_pic33e/usb/usb_device.c ****         a general purpose microcontroller timer set to interrupt every 1ms for example.
3008:lib/lib_pic33e/usb/usb_device.c ****         If the low speed application code does not call this function, the internal timers
3009:lib/lib_pic33e/usb/usb_device.c ****         will not increment, and the USBGet1msTickCount() API function will not be available.
3010:lib/lib_pic33e/usb/usb_device.c ****         Additionally, certain USB stack operations (like control transfer timeouts)
3011:lib/lib_pic33e/usb/usb_device.c ****         may be unavailable.
3012:lib/lib_pic33e/usb/usb_device.c **** 
3013:lib/lib_pic33e/usb/usb_device.c ****     Precondition:
3014:lib/lib_pic33e/usb/usb_device.c ****         This function should be called only after USBDeviceInit() has been
3015:lib/lib_pic33e/usb/usb_device.c ****         called (at least once at the start of the application).  Ordinarily,
3016:lib/lib_pic33e/usb/usb_device.c ****         application code should never call this function, unless it is a low speed
3017:lib/lib_pic33e/usb/usb_device.c ****         USB device.
3018:lib/lib_pic33e/usb/usb_device.c **** 
3019:lib/lib_pic33e/usb/usb_device.c ****     Parameters:
3020:lib/lib_pic33e/usb/usb_device.c ****         None
3021:lib/lib_pic33e/usb/usb_device.c **** 
3022:lib/lib_pic33e/usb/usb_device.c ****     Return Values:
3023:lib/lib_pic33e/usb/usb_device.c ****         None
3024:lib/lib_pic33e/usb/usb_device.c **** 
3025:lib/lib_pic33e/usb/usb_device.c ****     Remarks:
3026:lib/lib_pic33e/usb/usb_device.c ****         This function does not need to be called during USB suspend conditions, when
3027:lib/lib_pic33e/usb/usb_device.c ****         the USB module/stack is disabled, or when the USB cable is detached from the host.
3028:lib/lib_pic33e/usb/usb_device.c ****   ***************************************************************************/
3029:lib/lib_pic33e/usb/usb_device.c **** void USBIncrement1msInternalTimers(void)
3030:lib/lib_pic33e/usb/usb_device.c **** {
 4344              	.loc 1 3030 0
 4345              	.set ___PA___,1
 4346 001722  00 00 FA 	lnk #0
 4347              	.LCFI29:
3031:lib/lib_pic33e/usb/usb_device.c ****     #if(USB_SPEED_OPTION == USB_LOW_SPEED)
3032:lib/lib_pic33e/usb/usb_device.c ****         #warning "For low speed USB applications, read the function comments for the USBIncrement1m
3033:lib/lib_pic33e/usb/usb_device.c ****     #endif
3034:lib/lib_pic33e/usb/usb_device.c **** 
3035:lib/lib_pic33e/usb/usb_device.c ****     //Increment timekeeping 1ms tick counters.  Useful for other APIs/code
3036:lib/lib_pic33e/usb/usb_device.c ****     //that needs a 1ms time base that is active during USB non-suspended operation.
3037:lib/lib_pic33e/usb/usb_device.c ****     USB1msTickCount++;
MPLAB XC16 ASSEMBLY Listing:   			page 131


 4348              	.loc 1 3037 0
 4349 001724  00 00 80 	mov _USB1msTickCount,w0
 4350 001726  11 00 80 	mov _USB1msTickCount+2,w1
 4351 001728  61 00 40 	add w0,#1,w0
 4352 00172a  E0 80 48 	addc w1,#0,w1
 4353 00172c  00 00 88 	mov w0,_USB1msTickCount
 4354 00172e  11 00 88 	mov w1,_USB1msTickCount+2
3038:lib/lib_pic33e/usb/usb_device.c ****     if(USBIsBusSuspended() == false)
 4355              	.loc 1 3038 0
 4356 001730  00 C0 BF 	mov.b _USBBusIsSuspended,WREG
 4357 001732  00 04 A2 	btg.b w0,#0
 4358 001734  00 04 E0 	cp0.b w0
 4359              	.set ___BP___,0
 4360 001736  00 00 32 	bra z,.L173
3039:lib/lib_pic33e/usb/usb_device.c ****     {
3040:lib/lib_pic33e/usb/usb_device.c ****         USBTicksSinceSuspendEnd++;
 4361              	.loc 1 3040 0
 4362 001738  00 C0 BF 	mov.b _USBTicksSinceSuspendEnd,WREG
 4363 00173a  00 40 E8 	inc.b w0,w0
 4364 00173c  00 E0 B7 	mov.b WREG,_USBTicksSinceSuspendEnd
3041:lib/lib_pic33e/usb/usb_device.c ****         //Check for 8-bit wraparound.  If so, force it to saturate at 255.
3042:lib/lib_pic33e/usb/usb_device.c ****         if(USBTicksSinceSuspendEnd == 0)
 4365              	.loc 1 3042 0
 4366 00173e  00 C0 BF 	mov.b _USBTicksSinceSuspendEnd,WREG
 4367 001740  00 04 E0 	cp0.b w0
 4368              	.set ___BP___,0
 4369 001742  00 00 3A 	bra nz,.L173
3043:lib/lib_pic33e/usb/usb_device.c ****         {
3044:lib/lib_pic33e/usb/usb_device.c ****             USBTicksSinceSuspendEnd = 255;
 4370              	.loc 1 3044 0
 4371 001744  00 C0 EB 	setm.b w0
 4372 001746  00 E0 B7 	mov.b WREG,_USBTicksSinceSuspendEnd
 4373              	.L173:
3045:lib/lib_pic33e/usb/usb_device.c ****         }
3046:lib/lib_pic33e/usb/usb_device.c ****     }
3047:lib/lib_pic33e/usb/usb_device.c **** }
 4374              	.loc 1 3047 0
 4375 001748  8E 07 78 	mov w14,w15
 4376 00174a  4F 07 78 	mov [--w15],w14
 4377 00174c  00 40 A9 	bclr CORCON,#2
 4378 00174e  00 00 06 	return 
 4379              	.set ___PA___,0
 4380              	.LFE26:
 4381              	.size _USBIncrement1msInternalTimers,.-_USBIncrement1msInternalTimers
 4382              	.align 2
 4383              	.global _USBGet1msTickCount
 4384              	.type _USBGet1msTickCount,@function
 4385              	_USBGet1msTickCount:
 4386              	.LFB27:
3048:lib/lib_pic33e/usb/usb_device.c **** 
3049:lib/lib_pic33e/usb/usb_device.c **** 
3050:lib/lib_pic33e/usb/usb_device.c **** 
3051:lib/lib_pic33e/usb/usb_device.c **** 
3052:lib/lib_pic33e/usb/usb_device.c **** /**************************************************************************
3053:lib/lib_pic33e/usb/usb_device.c ****     Function:
3054:lib/lib_pic33e/usb/usb_device.c ****         uint32_t USBGet1msTickCount(void)
3055:lib/lib_pic33e/usb/usb_device.c **** 
MPLAB XC16 ASSEMBLY Listing:   			page 132


3056:lib/lib_pic33e/usb/usb_device.c ****     Description:
3057:lib/lib_pic33e/usb/usb_device.c ****         This function retrieves a 32-bit unsigned integer that normally increments by
3058:lib/lib_pic33e/usb/usb_device.c ****         one every one millisecond.  The count value starts from zero when the
3059:lib/lib_pic33e/usb/usb_device.c ****         USBDeviceInit() function is first called.  See the remarks section for
3060:lib/lib_pic33e/usb/usb_device.c ****         details on special circumstances where the tick count will not increment.
3061:lib/lib_pic33e/usb/usb_device.c **** 
3062:lib/lib_pic33e/usb/usb_device.c ****     Precondition:
3063:lib/lib_pic33e/usb/usb_device.c ****         This function should be called only after USBDeviceInit() has been
3064:lib/lib_pic33e/usb/usb_device.c ****         called (at least once at the start of the application).
3065:lib/lib_pic33e/usb/usb_device.c **** 
3066:lib/lib_pic33e/usb/usb_device.c ****     Parameters:
3067:lib/lib_pic33e/usb/usb_device.c ****         None
3068:lib/lib_pic33e/usb/usb_device.c **** 
3069:lib/lib_pic33e/usb/usb_device.c ****     Return Values:
3070:lib/lib_pic33e/usb/usb_device.c ****         uint32_t representing the approximate millisecond count, since the time the
3071:lib/lib_pic33e/usb/usb_device.c ****         USBDeviceInit() function was first called.
3072:lib/lib_pic33e/usb/usb_device.c **** 
3073:lib/lib_pic33e/usb/usb_device.c ****     Remarks:
3074:lib/lib_pic33e/usb/usb_device.c ****         On 8-bit USB full speed devices, the internal counter is incremented on
3075:lib/lib_pic33e/usb/usb_device.c ****         every SOF packet detected.  Therefore, it will not increment during suspend
3076:lib/lib_pic33e/usb/usb_device.c ****         or when the USB cable is detached.  However, on 16-bit devices, the T1MSECIF
3077:lib/lib_pic33e/usb/usb_device.c ****         hardware interrupt source is used to increment the internal counter.  Therefore,
3078:lib/lib_pic33e/usb/usb_device.c ****         on 16-bit devices, the count continue to increment during USB suspend or
3079:lib/lib_pic33e/usb/usb_device.c ****         detach events, so long as the application code has not put the microcontroller
3080:lib/lib_pic33e/usb/usb_device.c ****         to sleep during these events, and the application firmware is regularly
3081:lib/lib_pic33e/usb/usb_device.c ****         calling the USBDeviceTasks() function (or allowing it to execute, if using
3082:lib/lib_pic33e/usb/usb_device.c ****         USB_INTERRUPT mode operation).
3083:lib/lib_pic33e/usb/usb_device.c **** 
3084:lib/lib_pic33e/usb/usb_device.c ****         In USB low speed applications, the host does not broadcast SOF packets to
3085:lib/lib_pic33e/usb/usb_device.c ****         the device, so the application firmware becomes responsible for calling
3086:lib/lib_pic33e/usb/usb_device.c ****         USBIncrement1msInternalTimers() periodically (ex: from a general purpose
3087:lib/lib_pic33e/usb/usb_device.c ****         timer interrupt handler), or else the returned value from this function will
3088:lib/lib_pic33e/usb/usb_device.c ****         not increment.
3089:lib/lib_pic33e/usb/usb_device.c ****         
3090:lib/lib_pic33e/usb/usb_device.c ****         Prior to calling USBDeviceInit() for the first time the returned value will
3091:lib/lib_pic33e/usb/usb_device.c ****         be unpredictable.
3092:lib/lib_pic33e/usb/usb_device.c **** 
3093:lib/lib_pic33e/usb/usb_device.c ****         This function is USB_INTERRUPT mode safe and may be called from main loop
3094:lib/lib_pic33e/usb/usb_device.c ****         code without risk of retrieving a partially updated 32-bit number.
3095:lib/lib_pic33e/usb/usb_device.c **** 
3096:lib/lib_pic33e/usb/usb_device.c ****         However, this value only increments when the USBDeviceTasks() function is allowed
3097:lib/lib_pic33e/usb/usb_device.c ****         to execute.  If USB_INTERRUPT mode is used, it is allowable to block on this
3098:lib/lib_pic33e/usb/usb_device.c ****         function.  If however USB_POLLING mode is used, one must not block on this
3099:lib/lib_pic33e/usb/usb_device.c ****         function without also calling USBDeviceTasks() continuously for the blocking
3100:lib/lib_pic33e/usb/usb_device.c ****         duration (since the USB stack must still be allowed to execute, and the USB
3101:lib/lib_pic33e/usb/usb_device.c ****         stack is also responsible for updating the tick counter internally).
3102:lib/lib_pic33e/usb/usb_device.c **** 
3103:lib/lib_pic33e/usb/usb_device.c ****         If the application is operating in USB_POLLING mode, this function should
3104:lib/lib_pic33e/usb/usb_device.c ****         only be called from the main loop context, and not from an interrupt handler,
3105:lib/lib_pic33e/usb/usb_device.c ****         as the returned value could be incorrect, if the main loop context code was in
3106:lib/lib_pic33e/usb/usb_device.c ****         the process of updating the internal count at the moment of the interrupt event.
3107:lib/lib_pic33e/usb/usb_device.c ****    ***************************************************************************/
3108:lib/lib_pic33e/usb/usb_device.c **** uint32_t USBGet1msTickCount(void)
3109:lib/lib_pic33e/usb/usb_device.c **** {
 4387              	.loc 1 3109 0
 4388              	.set ___PA___,1
 4389 001750  04 00 FA 	lnk #4
MPLAB XC16 ASSEMBLY Listing:   			page 133


 4390              	.LCFI30:
 4391              	.L176:
3110:lib/lib_pic33e/usb/usb_device.c ****     #if defined (USB_INTERRUPT)
3111:lib/lib_pic33e/usb/usb_device.c ****         uint32_t localContextValue;
3112:lib/lib_pic33e/usb/usb_device.c **** 
3113:lib/lib_pic33e/usb/usb_device.c ****         //Repeatedly read the interrupt context variable, until we get a stable/unchanging
3114:lib/lib_pic33e/usb/usb_device.c ****         //value.  This ensures that the complete 32-bit value got read without
3115:lib/lib_pic33e/usb/usb_device.c ****         //getting interrupted in between bytes.
3116:lib/lib_pic33e/usb/usb_device.c ****         do
3117:lib/lib_pic33e/usb/usb_device.c ****         {
3118:lib/lib_pic33e/usb/usb_device.c ****             localContextValue = USB1msTickCount;
 4392              	.loc 1 3118 0
 4393 001752  02 00 80 	mov _USB1msTickCount,w2
 4394 001754  13 00 80 	mov _USB1msTickCount+2,w3
 4395 001756  02 8F BE 	mov.d w2,[w14]
3119:lib/lib_pic33e/usb/usb_device.c ****         }while(localContextValue != USB1msTickCount);
 4396              	.loc 1 3119 0
 4397 001758  00 00 80 	mov _USB1msTickCount,w0
 4398 00175a  11 00 80 	mov _USB1msTickCount+2,w1
 4399 00175c  1E 01 78 	mov [w14],w2
 4400 00175e  9E 01 90 	mov [w14+2],w3
 4401 001760  80 0F 51 	sub w2,w0,[w15]
 4402 001762  81 8F 59 	subb w3,w1,[w15]
 4403              	.set ___BP___,0
 4404 001764  00 00 3A 	bra nz,.L176
3120:lib/lib_pic33e/usb/usb_device.c **** 
3121:lib/lib_pic33e/usb/usb_device.c ****         return localContextValue;    
 4405              	.loc 1 3121 0
 4406 001766  1E 00 78 	mov [w14],w0
 4407 001768  9E 00 90 	mov [w14+2],w1
3122:lib/lib_pic33e/usb/usb_device.c ****     
3123:lib/lib_pic33e/usb/usb_device.c ****     #else
3124:lib/lib_pic33e/usb/usb_device.c ****         return USB1msTickCount;
3125:lib/lib_pic33e/usb/usb_device.c ****     #endif
3126:lib/lib_pic33e/usb/usb_device.c **** }
 4408              	.loc 1 3126 0
 4409 00176a  8E 07 78 	mov w14,w15
 4410 00176c  4F 07 78 	mov [--w15],w14
 4411 00176e  00 40 A9 	bclr CORCON,#2
 4412 001770  00 00 06 	return 
 4413              	.set ___PA___,0
 4414              	.LFE27:
 4415              	.size _USBGet1msTickCount,.-_USBGet1msTickCount
 4416              	.section .debug_frame,info
 4417                 	.Lframe0:
 4418 0000 10 00 00 00 	.4byte .LECIE0-.LSCIE0
 4419                 	.LSCIE0:
 4420 0004 FF FF FF FF 	.4byte 0xffffffff
 4421 0008 01          	.byte 0x1
 4422 0009 00          	.byte 0
 4423 000a 01          	.uleb128 0x1
 4424 000b 02          	.sleb128 2
 4425 000c 25          	.byte 0x25
 4426 000d 12          	.byte 0x12
 4427 000e 0F          	.uleb128 0xf
 4428 000f 7E          	.sleb128 -2
 4429 0010 09          	.byte 0x9
MPLAB XC16 ASSEMBLY Listing:   			page 134


 4430 0011 25          	.uleb128 0x25
 4431 0012 0F          	.uleb128 0xf
 4432 0013 00          	.align 4
 4433                 	.LECIE0:
 4434                 	.LSFDE0:
 4435 0014 1E 00 00 00 	.4byte .LEFDE0-.LASFDE0
 4436                 	.LASFDE0:
 4437 0018 00 00 00 00 	.4byte .Lframe0
 4438 001c 00 00 00 00 	.4byte .LFB0
 4439 0020 FA 00 00 00 	.4byte .LFE0-.LFB0
 4440 0024 04          	.byte 0x4
 4441 0025 02 00 00 00 	.4byte .LCFI0-.LFB0
 4442 0029 13          	.byte 0x13
 4443 002a 7D          	.sleb128 -3
 4444 002b 0D          	.byte 0xd
 4445 002c 0E          	.uleb128 0xe
 4446 002d 8E          	.byte 0x8e
 4447 002e 02          	.uleb128 0x2
 4448 002f 04          	.byte 0x4
 4449 0030 02 00 00 00 	.4byte .LCFI1-.LCFI0
 4450 0034 88          	.byte 0x88
 4451 0035 04          	.uleb128 0x4
 4452                 	.align 4
 4453                 	.LEFDE0:
 4454                 	.LSFDE2:
 4455 0036 18 00 00 00 	.4byte .LEFDE2-.LASFDE2
 4456                 	.LASFDE2:
 4457 003a 00 00 00 00 	.4byte .Lframe0
 4458 003e 00 00 00 00 	.4byte .LFB1
 4459 0042 A0 01 00 00 	.4byte .LFE1-.LFB1
 4460 0046 04          	.byte 0x4
 4461 0047 02 00 00 00 	.4byte .LCFI2-.LFB1
 4462 004b 13          	.byte 0x13
 4463 004c 7D          	.sleb128 -3
 4464 004d 0D          	.byte 0xd
 4465 004e 0E          	.uleb128 0xe
 4466 004f 8E          	.byte 0x8e
 4467 0050 02          	.uleb128 0x2
 4468 0051 00          	.align 4
 4469                 	.LEFDE2:
 4470                 	.LSFDE4:
 4471 0052 18 00 00 00 	.4byte .LEFDE4-.LASFDE4
 4472                 	.LASFDE4:
 4473 0056 00 00 00 00 	.4byte .Lframe0
 4474 005a 00 00 00 00 	.4byte .LFB2
 4475 005e 42 00 00 00 	.4byte .LFE2-.LFB2
 4476 0062 04          	.byte 0x4
 4477 0063 02 00 00 00 	.4byte .LCFI3-.LFB2
 4478 0067 13          	.byte 0x13
 4479 0068 7D          	.sleb128 -3
 4480 0069 0D          	.byte 0xd
 4481 006a 0E          	.uleb128 0xe
 4482 006b 8E          	.byte 0x8e
 4483 006c 02          	.uleb128 0x2
 4484 006d 00          	.align 4
 4485                 	.LEFDE4:
 4486                 	.LSFDE6:
MPLAB XC16 ASSEMBLY Listing:   			page 135


 4487 006e 18 00 00 00 	.4byte .LEFDE6-.LASFDE6
 4488                 	.LASFDE6:
 4489 0072 00 00 00 00 	.4byte .Lframe0
 4490 0076 00 00 00 00 	.4byte .LFB3
 4491 007a 36 01 00 00 	.4byte .LFE3-.LFB3
 4492 007e 04          	.byte 0x4
 4493 007f 02 00 00 00 	.4byte .LCFI4-.LFB3
 4494 0083 13          	.byte 0x13
 4495 0084 7D          	.sleb128 -3
 4496 0085 0D          	.byte 0xd
 4497 0086 0E          	.uleb128 0xe
 4498 0087 8E          	.byte 0x8e
 4499 0088 02          	.uleb128 0x2
 4500 0089 00          	.align 4
 4501                 	.LEFDE6:
 4502                 	.LSFDE8:
 4503 008a 18 00 00 00 	.4byte .LEFDE8-.LASFDE8
 4504                 	.LASFDE8:
 4505 008e 00 00 00 00 	.4byte .Lframe0
 4506 0092 00 00 00 00 	.4byte .LFB4
 4507 0096 70 01 00 00 	.4byte .LFE4-.LFB4
 4508 009a 04          	.byte 0x4
 4509 009b 02 00 00 00 	.4byte .LCFI5-.LFB4
 4510 009f 13          	.byte 0x13
 4511 00a0 7D          	.sleb128 -3
 4512 00a1 0D          	.byte 0xd
 4513 00a2 0E          	.uleb128 0xe
 4514 00a3 8E          	.byte 0x8e
 4515 00a4 02          	.uleb128 0x2
 4516 00a5 00          	.align 4
 4517                 	.LEFDE8:
 4518                 	.LSFDE10:
 4519 00a6 20 00 00 00 	.4byte .LEFDE10-.LASFDE10
 4520                 	.LASFDE10:
 4521 00aa 00 00 00 00 	.4byte .Lframe0
 4522 00ae 00 00 00 00 	.4byte .LFB5
 4523 00b2 86 01 00 00 	.4byte .LFE5-.LFB5
 4524 00b6 04          	.byte 0x4
 4525 00b7 02 00 00 00 	.4byte .LCFI6-.LFB5
 4526 00bb 13          	.byte 0x13
 4527 00bc 7D          	.sleb128 -3
 4528 00bd 0D          	.byte 0xd
 4529 00be 0E          	.uleb128 0xe
 4530 00bf 8E          	.byte 0x8e
 4531 00c0 02          	.uleb128 0x2
 4532 00c1 04          	.byte 0x4
 4533 00c2 04 00 00 00 	.4byte .LCFI8-.LCFI6
 4534 00c6 8A          	.byte 0x8a
 4535 00c7 06          	.uleb128 0x6
 4536 00c8 88          	.byte 0x88
 4537 00c9 04          	.uleb128 0x4
 4538                 	.align 4
 4539                 	.LEFDE10:
 4540                 	.LSFDE12:
 4541 00ca 18 00 00 00 	.4byte .LEFDE12-.LASFDE12
 4542                 	.LASFDE12:
 4543 00ce 00 00 00 00 	.4byte .Lframe0
MPLAB XC16 ASSEMBLY Listing:   			page 136


 4544 00d2 00 00 00 00 	.4byte .LFB6
 4545 00d6 10 00 00 00 	.4byte .LFE6-.LFB6
 4546 00da 04          	.byte 0x4
 4547 00db 02 00 00 00 	.4byte .LCFI9-.LFB6
 4548 00df 13          	.byte 0x13
 4549 00e0 7D          	.sleb128 -3
 4550 00e1 0D          	.byte 0xd
 4551 00e2 0E          	.uleb128 0xe
 4552 00e3 8E          	.byte 0x8e
 4553 00e4 02          	.uleb128 0x2
 4554 00e5 00          	.align 4
 4555                 	.LEFDE12:
 4556                 	.LSFDE14:
 4557 00e6 18 00 00 00 	.4byte .LEFDE14-.LASFDE14
 4558                 	.LASFDE14:
 4559 00ea 00 00 00 00 	.4byte .Lframe0
 4560 00ee 00 00 00 00 	.4byte .LFB7
 4561 00f2 40 00 00 00 	.4byte .LFE7-.LFB7
 4562 00f6 04          	.byte 0x4
 4563 00f7 02 00 00 00 	.4byte .LCFI10-.LFB7
 4564 00fb 13          	.byte 0x13
 4565 00fc 7D          	.sleb128 -3
 4566 00fd 0D          	.byte 0xd
 4567 00fe 0E          	.uleb128 0xe
 4568 00ff 8E          	.byte 0x8e
 4569 0100 02          	.uleb128 0x2
 4570 0101 00          	.align 4
 4571                 	.LEFDE14:
 4572                 	.LSFDE16:
 4573 0102 18 00 00 00 	.4byte .LEFDE16-.LASFDE16
 4574                 	.LASFDE16:
 4575 0106 00 00 00 00 	.4byte .Lframe0
 4576 010a 00 00 00 00 	.4byte .LFB8
 4577 010e 22 01 00 00 	.4byte .LFE8-.LFB8
 4578 0112 04          	.byte 0x4
 4579 0113 02 00 00 00 	.4byte .LCFI11-.LFB8
 4580 0117 13          	.byte 0x13
 4581 0118 7D          	.sleb128 -3
 4582 0119 0D          	.byte 0xd
 4583 011a 0E          	.uleb128 0xe
 4584 011b 8E          	.byte 0x8e
 4585 011c 02          	.uleb128 0x2
 4586 011d 00          	.align 4
 4587                 	.LEFDE16:
 4588                 	.LSFDE18:
 4589 011e 18 00 00 00 	.4byte .LEFDE18-.LASFDE18
 4590                 	.LASFDE18:
 4591 0122 00 00 00 00 	.4byte .Lframe0
 4592 0126 00 00 00 00 	.4byte .LFB9
 4593 012a F2 00 00 00 	.4byte .LFE9-.LFB9
 4594 012e 04          	.byte 0x4
 4595 012f 02 00 00 00 	.4byte .LCFI12-.LFB9
 4596 0133 13          	.byte 0x13
 4597 0134 7D          	.sleb128 -3
 4598 0135 0D          	.byte 0xd
 4599 0136 0E          	.uleb128 0xe
 4600 0137 8E          	.byte 0x8e
MPLAB XC16 ASSEMBLY Listing:   			page 137


 4601 0138 02          	.uleb128 0x2
 4602 0139 00          	.align 4
 4603                 	.LEFDE18:
 4604                 	.LSFDE20:
 4605 013a 18 00 00 00 	.4byte .LEFDE20-.LASFDE20
 4606                 	.LASFDE20:
 4607 013e 00 00 00 00 	.4byte .Lframe0
 4608 0142 00 00 00 00 	.4byte .LFB10
 4609 0146 62 00 00 00 	.4byte .LFE10-.LFB10
 4610 014a 04          	.byte 0x4
 4611 014b 02 00 00 00 	.4byte .LCFI13-.LFB10
 4612 014f 13          	.byte 0x13
 4613 0150 7D          	.sleb128 -3
 4614 0151 0D          	.byte 0xd
 4615 0152 0E          	.uleb128 0xe
 4616 0153 8E          	.byte 0x8e
 4617 0154 02          	.uleb128 0x2
 4618 0155 00          	.align 4
 4619                 	.LEFDE20:
 4620                 	.LSFDE22:
 4621 0156 18 00 00 00 	.4byte .LEFDE22-.LASFDE22
 4622                 	.LASFDE22:
 4623 015a 00 00 00 00 	.4byte .Lframe0
 4624 015e 00 00 00 00 	.4byte .LFB11
 4625 0162 7A 01 00 00 	.4byte .LFE11-.LFB11
 4626 0166 04          	.byte 0x4
 4627 0167 02 00 00 00 	.4byte .LCFI14-.LFB11
 4628 016b 13          	.byte 0x13
 4629 016c 7D          	.sleb128 -3
 4630 016d 0D          	.byte 0xd
 4631 016e 0E          	.uleb128 0xe
 4632 016f 8E          	.byte 0x8e
 4633 0170 02          	.uleb128 0x2
 4634 0171 00          	.align 4
 4635                 	.LEFDE22:
 4636                 	.LSFDE24:
 4637 0172 18 00 00 00 	.4byte .LEFDE24-.LASFDE24
 4638                 	.LASFDE24:
 4639 0176 00 00 00 00 	.4byte .Lframe0
 4640 017a 00 00 00 00 	.4byte .LFB12
 4641 017e 9A 00 00 00 	.4byte .LFE12-.LFB12
 4642 0182 04          	.byte 0x4
 4643 0183 02 00 00 00 	.4byte .LCFI15-.LFB12
 4644 0187 13          	.byte 0x13
 4645 0188 7D          	.sleb128 -3
 4646 0189 0D          	.byte 0xd
 4647 018a 0E          	.uleb128 0xe
 4648 018b 8E          	.byte 0x8e
 4649 018c 02          	.uleb128 0x2
 4650 018d 00          	.align 4
 4651                 	.LEFDE24:
 4652                 	.LSFDE26:
 4653 018e 18 00 00 00 	.4byte .LEFDE26-.LASFDE26
 4654                 	.LASFDE26:
 4655 0192 00 00 00 00 	.4byte .Lframe0
 4656 0196 00 00 00 00 	.4byte .LFB13
 4657 019a 00 02 00 00 	.4byte .LFE13-.LFB13
MPLAB XC16 ASSEMBLY Listing:   			page 138


 4658 019e 04          	.byte 0x4
 4659 019f 02 00 00 00 	.4byte .LCFI16-.LFB13
 4660 01a3 13          	.byte 0x13
 4661 01a4 7D          	.sleb128 -3
 4662 01a5 0D          	.byte 0xd
 4663 01a6 0E          	.uleb128 0xe
 4664 01a7 8E          	.byte 0x8e
 4665 01a8 02          	.uleb128 0x2
 4666 01a9 00          	.align 4
 4667                 	.LEFDE26:
 4668                 	.LSFDE28:
 4669 01aa 18 00 00 00 	.4byte .LEFDE28-.LASFDE28
 4670                 	.LASFDE28:
 4671 01ae 00 00 00 00 	.4byte .Lframe0
 4672 01b2 00 00 00 00 	.4byte .LFB14
 4673 01b6 7E 00 00 00 	.4byte .LFE14-.LFB14
 4674 01ba 04          	.byte 0x4
 4675 01bb 02 00 00 00 	.4byte .LCFI17-.LFB14
 4676 01bf 13          	.byte 0x13
 4677 01c0 7D          	.sleb128 -3
 4678 01c1 0D          	.byte 0xd
 4679 01c2 0E          	.uleb128 0xe
 4680 01c3 8E          	.byte 0x8e
 4681 01c4 02          	.uleb128 0x2
 4682 01c5 00          	.align 4
 4683                 	.LEFDE28:
 4684                 	.LSFDE30:
 4685 01c6 18 00 00 00 	.4byte .LEFDE30-.LASFDE30
 4686                 	.LASFDE30:
 4687 01ca 00 00 00 00 	.4byte .Lframe0
 4688 01ce 00 00 00 00 	.4byte .LFB15
 4689 01d2 9E 00 00 00 	.4byte .LFE15-.LFB15
 4690 01d6 04          	.byte 0x4
 4691 01d7 02 00 00 00 	.4byte .LCFI18-.LFB15
 4692 01db 13          	.byte 0x13
 4693 01dc 7D          	.sleb128 -3
 4694 01dd 0D          	.byte 0xd
 4695 01de 0E          	.uleb128 0xe
 4696 01df 8E          	.byte 0x8e
 4697 01e0 02          	.uleb128 0x2
 4698 01e1 00          	.align 4
 4699                 	.LEFDE30:
 4700                 	.LSFDE32:
 4701 01e2 18 00 00 00 	.4byte .LEFDE32-.LASFDE32
 4702                 	.LASFDE32:
 4703 01e6 00 00 00 00 	.4byte .Lframe0
 4704 01ea 00 00 00 00 	.4byte .LFB16
 4705 01ee BA 00 00 00 	.4byte .LFE16-.LFB16
 4706 01f2 04          	.byte 0x4
 4707 01f3 02 00 00 00 	.4byte .LCFI19-.LFB16
 4708 01f7 13          	.byte 0x13
 4709 01f8 7D          	.sleb128 -3
 4710 01f9 0D          	.byte 0xd
 4711 01fa 0E          	.uleb128 0xe
 4712 01fb 8E          	.byte 0x8e
 4713 01fc 02          	.uleb128 0x2
 4714 01fd 00          	.align 4
MPLAB XC16 ASSEMBLY Listing:   			page 139


 4715                 	.LEFDE32:
 4716                 	.LSFDE34:
 4717 01fe 18 00 00 00 	.4byte .LEFDE34-.LASFDE34
 4718                 	.LASFDE34:
 4719 0202 00 00 00 00 	.4byte .Lframe0
 4720 0206 00 00 00 00 	.4byte .LFB17
 4721 020a 74 00 00 00 	.4byte .LFE17-.LFB17
 4722 020e 04          	.byte 0x4
 4723 020f 02 00 00 00 	.4byte .LCFI20-.LFB17
 4724 0213 13          	.byte 0x13
 4725 0214 7D          	.sleb128 -3
 4726 0215 0D          	.byte 0xd
 4727 0216 0E          	.uleb128 0xe
 4728 0217 8E          	.byte 0x8e
 4729 0218 02          	.uleb128 0x2
 4730 0219 00          	.align 4
 4731                 	.LEFDE34:
 4732                 	.LSFDE36:
 4733 021a 18 00 00 00 	.4byte .LEFDE36-.LASFDE36
 4734                 	.LASFDE36:
 4735 021e 00 00 00 00 	.4byte .Lframe0
 4736 0222 00 00 00 00 	.4byte .LFB18
 4737 0226 1E 00 00 00 	.4byte .LFE18-.LFB18
 4738 022a 04          	.byte 0x4
 4739 022b 02 00 00 00 	.4byte .LCFI21-.LFB18
 4740 022f 13          	.byte 0x13
 4741 0230 7D          	.sleb128 -3
 4742 0231 0D          	.byte 0xd
 4743 0232 0E          	.uleb128 0xe
 4744 0233 8E          	.byte 0x8e
 4745 0234 02          	.uleb128 0x2
 4746 0235 00          	.align 4
 4747                 	.LEFDE36:
 4748                 	.LSFDE38:
 4749 0236 18 00 00 00 	.4byte .LEFDE38-.LASFDE38
 4750                 	.LASFDE38:
 4751 023a 00 00 00 00 	.4byte .Lframe0
 4752 023e 00 00 00 00 	.4byte .LFB19
 4753 0242 1C 00 00 00 	.4byte .LFE19-.LFB19
 4754 0246 04          	.byte 0x4
 4755 0247 02 00 00 00 	.4byte .LCFI22-.LFB19
 4756 024b 13          	.byte 0x13
 4757 024c 7D          	.sleb128 -3
 4758 024d 0D          	.byte 0xd
 4759 024e 0E          	.uleb128 0xe
 4760 024f 8E          	.byte 0x8e
 4761 0250 02          	.uleb128 0x2
 4762 0251 00          	.align 4
 4763                 	.LEFDE38:
 4764                 	.LSFDE40:
 4765 0252 18 00 00 00 	.4byte .LEFDE40-.LASFDE40
 4766                 	.LASFDE40:
 4767 0256 00 00 00 00 	.4byte .Lframe0
 4768 025a 00 00 00 00 	.4byte .LFB20
 4769 025e 6E 00 00 00 	.4byte .LFE20-.LFB20
 4770 0262 04          	.byte 0x4
 4771 0263 02 00 00 00 	.4byte .LCFI23-.LFB20
MPLAB XC16 ASSEMBLY Listing:   			page 140


 4772 0267 13          	.byte 0x13
 4773 0268 7D          	.sleb128 -3
 4774 0269 0D          	.byte 0xd
 4775 026a 0E          	.uleb128 0xe
 4776 026b 8E          	.byte 0x8e
 4777 026c 02          	.uleb128 0x2
 4778 026d 00          	.align 4
 4779                 	.LEFDE40:
 4780                 	.LSFDE42:
 4781 026e 18 00 00 00 	.4byte .LEFDE42-.LASFDE42
 4782                 	.LASFDE42:
 4783 0272 00 00 00 00 	.4byte .Lframe0
 4784 0276 00 00 00 00 	.4byte .LFB21
 4785 027a C2 00 00 00 	.4byte .LFE21-.LFB21
 4786 027e 04          	.byte 0x4
 4787 027f 02 00 00 00 	.4byte .LCFI24-.LFB21
 4788 0283 13          	.byte 0x13
 4789 0284 7D          	.sleb128 -3
 4790 0285 0D          	.byte 0xd
 4791 0286 0E          	.uleb128 0xe
 4792 0287 8E          	.byte 0x8e
 4793 0288 02          	.uleb128 0x2
 4794 0289 00          	.align 4
 4795                 	.LEFDE42:
 4796                 	.LSFDE44:
 4797 028a 18 00 00 00 	.4byte .LEFDE44-.LASFDE44
 4798                 	.LASFDE44:
 4799 028e 00 00 00 00 	.4byte .Lframe0
 4800 0292 00 00 00 00 	.4byte .LFB22
 4801 0296 86 00 00 00 	.4byte .LFE22-.LFB22
 4802 029a 04          	.byte 0x4
 4803 029b 02 00 00 00 	.4byte .LCFI25-.LFB22
 4804 029f 13          	.byte 0x13
 4805 02a0 7D          	.sleb128 -3
 4806 02a1 0D          	.byte 0xd
 4807 02a2 0E          	.uleb128 0xe
 4808 02a3 8E          	.byte 0x8e
 4809 02a4 02          	.uleb128 0x2
 4810 02a5 00          	.align 4
 4811                 	.LEFDE44:
 4812                 	.LSFDE46:
 4813 02a6 18 00 00 00 	.4byte .LEFDE46-.LASFDE46
 4814                 	.LASFDE46:
 4815 02aa 00 00 00 00 	.4byte .Lframe0
 4816 02ae 00 00 00 00 	.4byte .LFB23
 4817 02b2 56 01 00 00 	.4byte .LFE23-.LFB23
 4818 02b6 04          	.byte 0x4
 4819 02b7 02 00 00 00 	.4byte .LCFI26-.LFB23
 4820 02bb 13          	.byte 0x13
 4821 02bc 7D          	.sleb128 -3
 4822 02bd 0D          	.byte 0xd
 4823 02be 0E          	.uleb128 0xe
 4824 02bf 8E          	.byte 0x8e
 4825 02c0 02          	.uleb128 0x2
 4826 02c1 00          	.align 4
 4827                 	.LEFDE46:
 4828                 	.LSFDE48:
MPLAB XC16 ASSEMBLY Listing:   			page 141


 4829 02c2 18 00 00 00 	.4byte .LEFDE48-.LASFDE48
 4830                 	.LASFDE48:
 4831 02c6 00 00 00 00 	.4byte .Lframe0
 4832 02ca 00 00 00 00 	.4byte .LFB24
 4833 02ce BA 00 00 00 	.4byte .LFE24-.LFB24
 4834 02d2 04          	.byte 0x4
 4835 02d3 02 00 00 00 	.4byte .LCFI27-.LFB24
 4836 02d7 13          	.byte 0x13
 4837 02d8 7D          	.sleb128 -3
 4838 02d9 0D          	.byte 0xd
 4839 02da 0E          	.uleb128 0xe
 4840 02db 8E          	.byte 0x8e
 4841 02dc 02          	.uleb128 0x2
 4842 02dd 00          	.align 4
 4843                 	.LEFDE48:
 4844                 	.LSFDE50:
 4845 02de 18 00 00 00 	.4byte .LEFDE50-.LASFDE50
 4846                 	.LASFDE50:
 4847 02e2 00 00 00 00 	.4byte .Lframe0
 4848 02e6 00 00 00 00 	.4byte .LFB25
 4849 02ea F6 02 00 00 	.4byte .LFE25-.LFB25
 4850 02ee 04          	.byte 0x4
 4851 02ef 02 00 00 00 	.4byte .LCFI28-.LFB25
 4852 02f3 13          	.byte 0x13
 4853 02f4 7D          	.sleb128 -3
 4854 02f5 0D          	.byte 0xd
 4855 02f6 0E          	.uleb128 0xe
 4856 02f7 8E          	.byte 0x8e
 4857 02f8 02          	.uleb128 0x2
 4858 02f9 00          	.align 4
 4859                 	.LEFDE50:
 4860                 	.LSFDE52:
 4861 02fa 18 00 00 00 	.4byte .LEFDE52-.LASFDE52
 4862                 	.LASFDE52:
 4863 02fe 00 00 00 00 	.4byte .Lframe0
 4864 0302 00 00 00 00 	.4byte .LFB26
 4865 0306 2E 00 00 00 	.4byte .LFE26-.LFB26
 4866 030a 04          	.byte 0x4
 4867 030b 02 00 00 00 	.4byte .LCFI29-.LFB26
 4868 030f 13          	.byte 0x13
 4869 0310 7D          	.sleb128 -3
 4870 0311 0D          	.byte 0xd
 4871 0312 0E          	.uleb128 0xe
 4872 0313 8E          	.byte 0x8e
 4873 0314 02          	.uleb128 0x2
 4874 0315 00          	.align 4
 4875                 	.LEFDE52:
 4876                 	.LSFDE54:
 4877 0316 18 00 00 00 	.4byte .LEFDE54-.LASFDE54
 4878                 	.LASFDE54:
 4879 031a 00 00 00 00 	.4byte .Lframe0
 4880 031e 00 00 00 00 	.4byte .LFB27
 4881 0322 22 00 00 00 	.4byte .LFE27-.LFB27
 4882 0326 04          	.byte 0x4
 4883 0327 02 00 00 00 	.4byte .LCFI30-.LFB27
 4884 032b 13          	.byte 0x13
 4885 032c 7D          	.sleb128 -3
MPLAB XC16 ASSEMBLY Listing:   			page 142


 4886 032d 0D          	.byte 0xd
 4887 032e 0E          	.uleb128 0xe
 4888 032f 8E          	.byte 0x8e
 4889 0330 02          	.uleb128 0x2
 4890 0331 00          	.align 4
 4891                 	.LEFDE54:
 4892                 	.section .text,code
 4893              	.Letext0:
 4894              	.file 2 "/opt/microchip/xc16/v1.36/bin/bin/../../include/lega-c/stdint.h"
 4895              	.file 3 "/opt/microchip/xc16/v1.36/bin/bin/../../support/dsPIC33E/h/p33EP256MU806.h"
 4896              	.file 4 "lib/lib_pic33e/usb/usb_ch9.h"
 4897              	.file 5 "lib/lib_pic33e/usb/usb_common.h"
 4898              	.file 6 "lib/lib_pic33e/usb/usb_device.h"
 4899              	.file 7 "lib/lib_pic33e/usb/usb_hal_dspic33e.h"
 4900              	.file 8 "lib/lib_pic33e/usb/usb_device_local.h"
 4901              	.section .debug_info,info
 4902 0000 45 2A 00 00 	.4byte 0x2a45
 4903 0004 02 00       	.2byte 0x2
 4904 0006 00 00 00 00 	.4byte .Ldebug_abbrev0
 4905 000a 04          	.byte 0x4
 4906 000b 01          	.uleb128 0x1
 4907 000c 47 4E 55 20 	.asciz "GNU C 4.5.1 (XC16, Microchip v1.36) (A) Build date: Dec  5 2018"
 4907      43 20 34 2E 
 4907      35 2E 31 20 
 4907      28 58 43 31 
 4907      36 2C 20 4D 
 4907      69 63 72 6F 
 4907      63 68 69 70 
 4907      20 76 31 2E 
 4907      33 36 29 20 
 4908 004c 01          	.byte 0x1
 4909 004d 6C 69 62 2F 	.asciz "lib/lib_pic33e/usb/usb_device.c"
 4909      6C 69 62 5F 
 4909      70 69 63 33 
 4909      33 65 2F 75 
 4909      73 62 2F 75 
 4909      73 62 5F 64 
 4909      65 76 69 63 
 4909      65 2E 63 00 
 4910 006d 2F 68 6F 6D 	.asciz "/home/user/Documents/FST/Programming/project_sub-zero"
 4910      65 2F 75 73 
 4910      65 72 2F 44 
 4910      6F 63 75 6D 
 4910      65 6E 74 73 
 4910      2F 46 53 54 
 4910      2F 50 72 6F 
 4910      67 72 61 6D 
 4910      6D 69 6E 67 
 4911 00a3 00 00 00 00 	.4byte .Ltext0
 4912 00a7 00 00 00 00 	.4byte .Letext0
 4913 00ab 00 00 00 00 	.4byte .Ldebug_line0
 4914 00af 02          	.uleb128 0x2
 4915 00b0 01          	.byte 0x1
 4916 00b1 06          	.byte 0x6
 4917 00b2 73 69 67 6E 	.asciz "signed char"
 4917      65 64 20 63 
 4917      68 61 72 00 
MPLAB XC16 ASSEMBLY Listing:   			page 143


 4918 00be 02          	.uleb128 0x2
 4919 00bf 02          	.byte 0x2
 4920 00c0 05          	.byte 0x5
 4921 00c1 69 6E 74 00 	.asciz "int"
 4922 00c5 02          	.uleb128 0x2
 4923 00c6 04          	.byte 0x4
 4924 00c7 05          	.byte 0x5
 4925 00c8 6C 6F 6E 67 	.asciz "long int"
 4925      20 69 6E 74 
 4925      00 
 4926 00d1 02          	.uleb128 0x2
 4927 00d2 08          	.byte 0x8
 4928 00d3 05          	.byte 0x5
 4929 00d4 6C 6F 6E 67 	.asciz "long long int"
 4929      20 6C 6F 6E 
 4929      67 20 69 6E 
 4929      74 00 
 4930 00e2 03          	.uleb128 0x3
 4931 00e3 75 69 6E 74 	.asciz "uint8_t"
 4931      38 5F 74 00 
 4932 00eb 02          	.byte 0x2
 4933 00ec 2B          	.byte 0x2b
 4934 00ed F1 00 00 00 	.4byte 0xf1
 4935 00f1 02          	.uleb128 0x2
 4936 00f2 01          	.byte 0x1
 4937 00f3 08          	.byte 0x8
 4938 00f4 75 6E 73 69 	.asciz "unsigned char"
 4938      67 6E 65 64 
 4938      20 63 68 61 
 4938      72 00 
 4939 0102 03          	.uleb128 0x3
 4940 0103 75 69 6E 74 	.asciz "uint16_t"
 4940      31 36 5F 74 
 4940      00 
 4941 010c 02          	.byte 0x2
 4942 010d 31          	.byte 0x31
 4943 010e 12 01 00 00 	.4byte 0x112
 4944 0112 02          	.uleb128 0x2
 4945 0113 02          	.byte 0x2
 4946 0114 07          	.byte 0x7
 4947 0115 75 6E 73 69 	.asciz "unsigned int"
 4947      67 6E 65 64 
 4947      20 69 6E 74 
 4947      00 
 4948 0122 03          	.uleb128 0x3
 4949 0123 75 69 6E 74 	.asciz "uint32_t"
 4949      33 32 5F 74 
 4949      00 
 4950 012c 02          	.byte 0x2
 4951 012d 37          	.byte 0x37
 4952 012e 32 01 00 00 	.4byte 0x132
 4953 0132 02          	.uleb128 0x2
 4954 0133 04          	.byte 0x4
 4955 0134 07          	.byte 0x7
 4956 0135 6C 6F 6E 67 	.asciz "long unsigned int"
 4956      20 75 6E 73 
 4956      69 67 6E 65 
MPLAB XC16 ASSEMBLY Listing:   			page 144


 4956      64 20 69 6E 
 4956      74 00 
 4957 0147 02          	.uleb128 0x2
 4958 0148 08          	.byte 0x8
 4959 0149 07          	.byte 0x7
 4960 014a 6C 6F 6E 67 	.asciz "long long unsigned int"
 4960      20 6C 6F 6E 
 4960      67 20 75 6E 
 4960      73 69 67 6E 
 4960      65 64 20 69 
 4960      6E 74 00 
 4961 0161 04          	.uleb128 0x4
 4962 0162 74 61 67 55 	.asciz "tagU1OTGIRBITS"
 4962      31 4F 54 47 
 4962      49 52 42 49 
 4962      54 53 00 
 4963 0171 02          	.byte 0x2
 4964 0172 03          	.byte 0x3
 4965 0173 53 12       	.2byte 0x1253
 4966 0175 14 02 00 00 	.4byte 0x214
 4967 0179 05          	.uleb128 0x5
 4968 017a 56 42 55 53 	.asciz "VBUSVDIF"
 4968      56 44 49 46 
 4968      00 
 4969 0183 03          	.byte 0x3
 4970 0184 54 12       	.2byte 0x1254
 4971 0186 02 01 00 00 	.4byte 0x102
 4972 018a 02          	.byte 0x2
 4973 018b 01          	.byte 0x1
 4974 018c 0F          	.byte 0xf
 4975 018d 02          	.byte 0x2
 4976 018e 23          	.byte 0x23
 4977 018f 00          	.uleb128 0x0
 4978 0190 05          	.uleb128 0x5
 4979 0191 53 45 53 45 	.asciz "SESENDIF"
 4979      4E 44 49 46 
 4979      00 
 4980 019a 03          	.byte 0x3
 4981 019b 56 12       	.2byte 0x1256
 4982 019d 02 01 00 00 	.4byte 0x102
 4983 01a1 02          	.byte 0x2
 4984 01a2 01          	.byte 0x1
 4985 01a3 0D          	.byte 0xd
 4986 01a4 02          	.byte 0x2
 4987 01a5 23          	.byte 0x23
 4988 01a6 00          	.uleb128 0x0
 4989 01a7 05          	.uleb128 0x5
 4990 01a8 53 45 53 56 	.asciz "SESVDIF"
 4990      44 49 46 00 
 4991 01b0 03          	.byte 0x3
 4992 01b1 57 12       	.2byte 0x1257
 4993 01b3 02 01 00 00 	.4byte 0x102
 4994 01b7 02          	.byte 0x2
 4995 01b8 01          	.byte 0x1
 4996 01b9 0C          	.byte 0xc
 4997 01ba 02          	.byte 0x2
 4998 01bb 23          	.byte 0x23
MPLAB XC16 ASSEMBLY Listing:   			page 145


 4999 01bc 00          	.uleb128 0x0
 5000 01bd 05          	.uleb128 0x5
 5001 01be 41 43 54 56 	.asciz "ACTVIF"
 5001      49 46 00 
 5002 01c5 03          	.byte 0x3
 5003 01c6 58 12       	.2byte 0x1258
 5004 01c8 02 01 00 00 	.4byte 0x102
 5005 01cc 02          	.byte 0x2
 5006 01cd 01          	.byte 0x1
 5007 01ce 0B          	.byte 0xb
 5008 01cf 02          	.byte 0x2
 5009 01d0 23          	.byte 0x23
 5010 01d1 00          	.uleb128 0x0
 5011 01d2 05          	.uleb128 0x5
 5012 01d3 4C 53 54 41 	.asciz "LSTATEIF"
 5012      54 45 49 46 
 5012      00 
 5013 01dc 03          	.byte 0x3
 5014 01dd 59 12       	.2byte 0x1259
 5015 01df 02 01 00 00 	.4byte 0x102
 5016 01e3 02          	.byte 0x2
 5017 01e4 01          	.byte 0x1
 5018 01e5 0A          	.byte 0xa
 5019 01e6 02          	.byte 0x2
 5020 01e7 23          	.byte 0x23
 5021 01e8 00          	.uleb128 0x0
 5022 01e9 05          	.uleb128 0x5
 5023 01ea 54 31 4D 53 	.asciz "T1MSECIF"
 5023      45 43 49 46 
 5023      00 
 5024 01f3 03          	.byte 0x3
 5025 01f4 5A 12       	.2byte 0x125a
 5026 01f6 02 01 00 00 	.4byte 0x102
 5027 01fa 02          	.byte 0x2
 5028 01fb 01          	.byte 0x1
 5029 01fc 09          	.byte 0x9
 5030 01fd 02          	.byte 0x2
 5031 01fe 23          	.byte 0x23
 5032 01ff 00          	.uleb128 0x0
 5033 0200 05          	.uleb128 0x5
 5034 0201 49 44 49 46 	.asciz "IDIF"
 5034      00 
 5035 0206 03          	.byte 0x3
 5036 0207 5B 12       	.2byte 0x125b
 5037 0209 02 01 00 00 	.4byte 0x102
 5038 020d 02          	.byte 0x2
 5039 020e 01          	.byte 0x1
 5040 020f 08          	.byte 0x8
 5041 0210 02          	.byte 0x2
 5042 0211 23          	.byte 0x23
 5043 0212 00          	.uleb128 0x0
 5044 0213 00          	.byte 0x0
 5045 0214 06          	.uleb128 0x6
 5046 0215 55 31 4F 54 	.asciz "U1OTGIRBITS"
 5046      47 49 52 42 
 5046      49 54 53 00 
 5047 0221 03          	.byte 0x3
MPLAB XC16 ASSEMBLY Listing:   			page 146


 5048 0222 5C 12       	.2byte 0x125c
 5049 0224 61 01 00 00 	.4byte 0x161
 5050 0228 04          	.uleb128 0x4
 5051 0229 74 61 67 55 	.asciz "tagU1OTGIEBITS"
 5051      31 4F 54 47 
 5051      49 45 42 49 
 5051      54 53 00 
 5052 0238 02          	.byte 0x2
 5053 0239 03          	.byte 0x3
 5054 023a 61 12       	.2byte 0x1261
 5055 023c DB 02 00 00 	.4byte 0x2db
 5056 0240 05          	.uleb128 0x5
 5057 0241 56 42 55 53 	.asciz "VBUSVDIE"
 5057      56 44 49 45 
 5057      00 
 5058 024a 03          	.byte 0x3
 5059 024b 62 12       	.2byte 0x1262
 5060 024d 02 01 00 00 	.4byte 0x102
 5061 0251 02          	.byte 0x2
 5062 0252 01          	.byte 0x1
 5063 0253 0F          	.byte 0xf
 5064 0254 02          	.byte 0x2
 5065 0255 23          	.byte 0x23
 5066 0256 00          	.uleb128 0x0
 5067 0257 05          	.uleb128 0x5
 5068 0258 53 45 53 45 	.asciz "SESENDIE"
 5068      4E 44 49 45 
 5068      00 
 5069 0261 03          	.byte 0x3
 5070 0262 64 12       	.2byte 0x1264
 5071 0264 02 01 00 00 	.4byte 0x102
 5072 0268 02          	.byte 0x2
 5073 0269 01          	.byte 0x1
 5074 026a 0D          	.byte 0xd
 5075 026b 02          	.byte 0x2
 5076 026c 23          	.byte 0x23
 5077 026d 00          	.uleb128 0x0
 5078 026e 05          	.uleb128 0x5
 5079 026f 53 45 53 56 	.asciz "SESVDIE"
 5079      44 49 45 00 
 5080 0277 03          	.byte 0x3
 5081 0278 65 12       	.2byte 0x1265
 5082 027a 02 01 00 00 	.4byte 0x102
 5083 027e 02          	.byte 0x2
 5084 027f 01          	.byte 0x1
 5085 0280 0C          	.byte 0xc
 5086 0281 02          	.byte 0x2
 5087 0282 23          	.byte 0x23
 5088 0283 00          	.uleb128 0x0
 5089 0284 05          	.uleb128 0x5
 5090 0285 41 43 54 56 	.asciz "ACTVIE"
 5090      49 45 00 
 5091 028c 03          	.byte 0x3
 5092 028d 66 12       	.2byte 0x1266
 5093 028f 02 01 00 00 	.4byte 0x102
 5094 0293 02          	.byte 0x2
 5095 0294 01          	.byte 0x1
MPLAB XC16 ASSEMBLY Listing:   			page 147


 5096 0295 0B          	.byte 0xb
 5097 0296 02          	.byte 0x2
 5098 0297 23          	.byte 0x23
 5099 0298 00          	.uleb128 0x0
 5100 0299 05          	.uleb128 0x5
 5101 029a 4C 53 54 41 	.asciz "LSTATEIE"
 5101      54 45 49 45 
 5101      00 
 5102 02a3 03          	.byte 0x3
 5103 02a4 67 12       	.2byte 0x1267
 5104 02a6 02 01 00 00 	.4byte 0x102
 5105 02aa 02          	.byte 0x2
 5106 02ab 01          	.byte 0x1
 5107 02ac 0A          	.byte 0xa
 5108 02ad 02          	.byte 0x2
 5109 02ae 23          	.byte 0x23
 5110 02af 00          	.uleb128 0x0
 5111 02b0 05          	.uleb128 0x5
 5112 02b1 54 31 4D 53 	.asciz "T1MSECIE"
 5112      45 43 49 45 
 5112      00 
 5113 02ba 03          	.byte 0x3
 5114 02bb 68 12       	.2byte 0x1268
 5115 02bd 02 01 00 00 	.4byte 0x102
 5116 02c1 02          	.byte 0x2
 5117 02c2 01          	.byte 0x1
 5118 02c3 09          	.byte 0x9
 5119 02c4 02          	.byte 0x2
 5120 02c5 23          	.byte 0x23
 5121 02c6 00          	.uleb128 0x0
 5122 02c7 05          	.uleb128 0x5
 5123 02c8 49 44 49 45 	.asciz "IDIE"
 5123      00 
 5124 02cd 03          	.byte 0x3
 5125 02ce 69 12       	.2byte 0x1269
 5126 02d0 02 01 00 00 	.4byte 0x102
 5127 02d4 02          	.byte 0x2
 5128 02d5 01          	.byte 0x1
 5129 02d6 08          	.byte 0x8
 5130 02d7 02          	.byte 0x2
 5131 02d8 23          	.byte 0x23
 5132 02d9 00          	.uleb128 0x0
 5133 02da 00          	.byte 0x0
 5134 02db 06          	.uleb128 0x6
 5135 02dc 55 31 4F 54 	.asciz "U1OTGIEBITS"
 5135      47 49 45 42 
 5135      49 54 53 00 
 5136 02e8 03          	.byte 0x3
 5137 02e9 6A 12       	.2byte 0x126a
 5138 02eb 28 02 00 00 	.4byte 0x228
 5139 02ef 04          	.uleb128 0x4
 5140 02f0 74 61 67 55 	.asciz "tagU1OTGCONBITS"
 5140      31 4F 54 47 
 5140      43 4F 4E 42 
 5140      49 54 53 00 
 5141 0300 02          	.byte 0x2
 5142 0301 03          	.byte 0x3
MPLAB XC16 ASSEMBLY Listing:   			page 148


 5143 0302 7D 12       	.2byte 0x127d
 5144 0304 B8 03 00 00 	.4byte 0x3b8
 5145 0308 05          	.uleb128 0x5
 5146 0309 56 42 55 53 	.asciz "VBUSDIS"
 5146      44 49 53 00 
 5147 0311 03          	.byte 0x3
 5148 0312 7E 12       	.2byte 0x127e
 5149 0314 02 01 00 00 	.4byte 0x102
 5150 0318 02          	.byte 0x2
 5151 0319 01          	.byte 0x1
 5152 031a 0F          	.byte 0xf
 5153 031b 02          	.byte 0x2
 5154 031c 23          	.byte 0x23
 5155 031d 00          	.uleb128 0x0
 5156 031e 05          	.uleb128 0x5
 5157 031f 56 42 55 53 	.asciz "VBUSCHG"
 5157      43 48 47 00 
 5158 0327 03          	.byte 0x3
 5159 0328 7F 12       	.2byte 0x127f
 5160 032a 02 01 00 00 	.4byte 0x102
 5161 032e 02          	.byte 0x2
 5162 032f 01          	.byte 0x1
 5163 0330 0E          	.byte 0xe
 5164 0331 02          	.byte 0x2
 5165 0332 23          	.byte 0x23
 5166 0333 00          	.uleb128 0x0
 5167 0334 05          	.uleb128 0x5
 5168 0335 4F 54 47 45 	.asciz "OTGEN"
 5168      4E 00 
 5169 033b 03          	.byte 0x3
 5170 033c 80 12       	.2byte 0x1280
 5171 033e 02 01 00 00 	.4byte 0x102
 5172 0342 02          	.byte 0x2
 5173 0343 01          	.byte 0x1
 5174 0344 0D          	.byte 0xd
 5175 0345 02          	.byte 0x2
 5176 0346 23          	.byte 0x23
 5177 0347 00          	.uleb128 0x0
 5178 0348 05          	.uleb128 0x5
 5179 0349 56 42 55 53 	.asciz "VBUSON"
 5179      4F 4E 00 
 5180 0350 03          	.byte 0x3
 5181 0351 81 12       	.2byte 0x1281
 5182 0353 02 01 00 00 	.4byte 0x102
 5183 0357 02          	.byte 0x2
 5184 0358 01          	.byte 0x1
 5185 0359 0C          	.byte 0xc
 5186 035a 02          	.byte 0x2
 5187 035b 23          	.byte 0x23
 5188 035c 00          	.uleb128 0x0
 5189 035d 05          	.uleb128 0x5
 5190 035e 44 4D 50 55 	.asciz "DMPULDWN"
 5190      4C 44 57 4E 
 5190      00 
 5191 0367 03          	.byte 0x3
 5192 0368 82 12       	.2byte 0x1282
 5193 036a 02 01 00 00 	.4byte 0x102
MPLAB XC16 ASSEMBLY Listing:   			page 149


 5194 036e 02          	.byte 0x2
 5195 036f 01          	.byte 0x1
 5196 0370 0B          	.byte 0xb
 5197 0371 02          	.byte 0x2
 5198 0372 23          	.byte 0x23
 5199 0373 00          	.uleb128 0x0
 5200 0374 05          	.uleb128 0x5
 5201 0375 44 50 50 55 	.asciz "DPPULDWN"
 5201      4C 44 57 4E 
 5201      00 
 5202 037e 03          	.byte 0x3
 5203 037f 83 12       	.2byte 0x1283
 5204 0381 02 01 00 00 	.4byte 0x102
 5205 0385 02          	.byte 0x2
 5206 0386 01          	.byte 0x1
 5207 0387 0A          	.byte 0xa
 5208 0388 02          	.byte 0x2
 5209 0389 23          	.byte 0x23
 5210 038a 00          	.uleb128 0x0
 5211 038b 05          	.uleb128 0x5
 5212 038c 44 4D 50 55 	.asciz "DMPULUP"
 5212      4C 55 50 00 
 5213 0394 03          	.byte 0x3
 5214 0395 84 12       	.2byte 0x1284
 5215 0397 02 01 00 00 	.4byte 0x102
 5216 039b 02          	.byte 0x2
 5217 039c 01          	.byte 0x1
 5218 039d 09          	.byte 0x9
 5219 039e 02          	.byte 0x2
 5220 039f 23          	.byte 0x23
 5221 03a0 00          	.uleb128 0x0
 5222 03a1 05          	.uleb128 0x5
 5223 03a2 44 50 50 55 	.asciz "DPPULUP"
 5223      4C 55 50 00 
 5224 03aa 03          	.byte 0x3
 5225 03ab 85 12       	.2byte 0x1285
 5226 03ad 02 01 00 00 	.4byte 0x102
 5227 03b1 02          	.byte 0x2
 5228 03b2 01          	.byte 0x1
 5229 03b3 08          	.byte 0x8
 5230 03b4 02          	.byte 0x2
 5231 03b5 23          	.byte 0x23
 5232 03b6 00          	.uleb128 0x0
 5233 03b7 00          	.byte 0x0
 5234 03b8 06          	.uleb128 0x6
 5235 03b9 55 31 4F 54 	.asciz "U1OTGCONBITS"
 5235      47 43 4F 4E 
 5235      42 49 54 53 
 5235      00 
 5236 03c6 03          	.byte 0x3
 5237 03c7 86 12       	.2byte 0x1286
 5238 03c9 EF 02 00 00 	.4byte 0x2ef
 5239 03cd 07          	.uleb128 0x7
 5240 03ce 02          	.byte 0x2
 5241 03cf 03          	.byte 0x3
 5242 03d0 8D 12       	.2byte 0x128d
 5243 03d2 2E 04 00 00 	.4byte 0x42e
MPLAB XC16 ASSEMBLY Listing:   			page 150


 5244 03d6 05          	.uleb128 0x5
 5245 03d7 55 53 42 50 	.asciz "USBPWR"
 5245      57 52 00 
 5246 03de 03          	.byte 0x3
 5247 03df 8E 12       	.2byte 0x128e
 5248 03e1 02 01 00 00 	.4byte 0x102
 5249 03e5 02          	.byte 0x2
 5250 03e6 01          	.byte 0x1
 5251 03e7 0F          	.byte 0xf
 5252 03e8 02          	.byte 0x2
 5253 03e9 23          	.byte 0x23
 5254 03ea 00          	.uleb128 0x0
 5255 03eb 05          	.uleb128 0x5
 5256 03ec 55 53 55 53 	.asciz "USUSPND"
 5256      50 4E 44 00 
 5257 03f4 03          	.byte 0x3
 5258 03f5 8F 12       	.2byte 0x128f
 5259 03f7 02 01 00 00 	.4byte 0x102
 5260 03fb 02          	.byte 0x2
 5261 03fc 01          	.byte 0x1
 5262 03fd 0E          	.byte 0xe
 5263 03fe 02          	.byte 0x2
 5264 03ff 23          	.byte 0x23
 5265 0400 00          	.uleb128 0x0
 5266 0401 05          	.uleb128 0x5
 5267 0402 55 53 4C 50 	.asciz "USLPGRD"
 5267      47 52 44 00 
 5268 040a 03          	.byte 0x3
 5269 040b 91 12       	.2byte 0x1291
 5270 040d 02 01 00 00 	.4byte 0x102
 5271 0411 02          	.byte 0x2
 5272 0412 01          	.byte 0x1
 5273 0413 0B          	.byte 0xb
 5274 0414 02          	.byte 0x2
 5275 0415 23          	.byte 0x23
 5276 0416 00          	.uleb128 0x0
 5277 0417 05          	.uleb128 0x5
 5278 0418 55 41 43 54 	.asciz "UACTPND"
 5278      50 4E 44 00 
 5279 0420 03          	.byte 0x3
 5280 0421 93 12       	.2byte 0x1293
 5281 0423 02 01 00 00 	.4byte 0x102
 5282 0427 02          	.byte 0x2
 5283 0428 01          	.byte 0x1
 5284 0429 08          	.byte 0x8
 5285 042a 02          	.byte 0x2
 5286 042b 23          	.byte 0x23
 5287 042c 00          	.uleb128 0x0
 5288 042d 00          	.byte 0x0
 5289 042e 07          	.uleb128 0x7
 5290 042f 02          	.byte 0x2
 5291 0430 03          	.byte 0x3
 5292 0431 95 12       	.2byte 0x1295
 5293 0433 4F 04 00 00 	.4byte 0x44f
 5294 0437 05          	.uleb128 0x5
 5295 0438 55 53 55 53 	.asciz "USUSPEND"
 5295      50 45 4E 44 
MPLAB XC16 ASSEMBLY Listing:   			page 151


 5295      00 
 5296 0441 03          	.byte 0x3
 5297 0442 97 12       	.2byte 0x1297
 5298 0444 02 01 00 00 	.4byte 0x102
 5299 0448 02          	.byte 0x2
 5300 0449 01          	.byte 0x1
 5301 044a 0E          	.byte 0xe
 5302 044b 02          	.byte 0x2
 5303 044c 23          	.byte 0x23
 5304 044d 00          	.uleb128 0x0
 5305 044e 00          	.byte 0x0
 5306 044f 08          	.uleb128 0x8
 5307 0450 02          	.byte 0x2
 5308 0451 03          	.byte 0x3
 5309 0452 8C 12       	.2byte 0x128c
 5310 0454 63 04 00 00 	.4byte 0x463
 5311 0458 09          	.uleb128 0x9
 5312 0459 CD 03 00 00 	.4byte 0x3cd
 5313 045d 09          	.uleb128 0x9
 5314 045e 2E 04 00 00 	.4byte 0x42e
 5315 0462 00          	.byte 0x0
 5316 0463 04          	.uleb128 0x4
 5317 0464 74 61 67 55 	.asciz "tagU1PWRCBITS"
 5317      31 50 57 52 
 5317      43 42 49 54 
 5317      53 00 
 5318 0472 02          	.byte 0x2
 5319 0473 03          	.byte 0x3
 5320 0474 8B 12       	.2byte 0x128b
 5321 0476 83 04 00 00 	.4byte 0x483
 5322 047a 0A          	.uleb128 0xa
 5323 047b 4F 04 00 00 	.4byte 0x44f
 5324 047f 02          	.byte 0x2
 5325 0480 23          	.byte 0x23
 5326 0481 00          	.uleb128 0x0
 5327 0482 00          	.byte 0x0
 5328 0483 06          	.uleb128 0x6
 5329 0484 55 31 50 57 	.asciz "U1PWRCBITS"
 5329      52 43 42 49 
 5329      54 53 00 
 5330 048f 03          	.byte 0x3
 5331 0490 9A 12       	.2byte 0x129a
 5332 0492 63 04 00 00 	.4byte 0x463
 5333 0496 07          	.uleb128 0x7
 5334 0497 02          	.byte 0x2
 5335 0498 03          	.byte 0x3
 5336 0499 A1 12       	.2byte 0x12a1
 5337 049b 34 05 00 00 	.4byte 0x534
 5338 049f 05          	.uleb128 0x5
 5339 04a0 55 52 53 54 	.asciz "URSTIF"
 5339      49 46 00 
 5340 04a7 03          	.byte 0x3
 5341 04a8 A2 12       	.2byte 0x12a2
 5342 04aa 02 01 00 00 	.4byte 0x102
 5343 04ae 02          	.byte 0x2
 5344 04af 01          	.byte 0x1
 5345 04b0 0F          	.byte 0xf
MPLAB XC16 ASSEMBLY Listing:   			page 152


 5346 04b1 02          	.byte 0x2
 5347 04b2 23          	.byte 0x23
 5348 04b3 00          	.uleb128 0x0
 5349 04b4 05          	.uleb128 0x5
 5350 04b5 55 45 52 52 	.asciz "UERRIF"
 5350      49 46 00 
 5351 04bc 03          	.byte 0x3
 5352 04bd A3 12       	.2byte 0x12a3
 5353 04bf 02 01 00 00 	.4byte 0x102
 5354 04c3 02          	.byte 0x2
 5355 04c4 01          	.byte 0x1
 5356 04c5 0E          	.byte 0xe
 5357 04c6 02          	.byte 0x2
 5358 04c7 23          	.byte 0x23
 5359 04c8 00          	.uleb128 0x0
 5360 04c9 05          	.uleb128 0x5
 5361 04ca 53 4F 46 49 	.asciz "SOFIF"
 5361      46 00 
 5362 04d0 03          	.byte 0x3
 5363 04d1 A4 12       	.2byte 0x12a4
 5364 04d3 02 01 00 00 	.4byte 0x102
 5365 04d7 02          	.byte 0x2
 5366 04d8 01          	.byte 0x1
 5367 04d9 0D          	.byte 0xd
 5368 04da 02          	.byte 0x2
 5369 04db 23          	.byte 0x23
 5370 04dc 00          	.uleb128 0x0
 5371 04dd 05          	.uleb128 0x5
 5372 04de 54 52 4E 49 	.asciz "TRNIF"
 5372      46 00 
 5373 04e4 03          	.byte 0x3
 5374 04e5 A5 12       	.2byte 0x12a5
 5375 04e7 02 01 00 00 	.4byte 0x102
 5376 04eb 02          	.byte 0x2
 5377 04ec 01          	.byte 0x1
 5378 04ed 0C          	.byte 0xc
 5379 04ee 02          	.byte 0x2
 5380 04ef 23          	.byte 0x23
 5381 04f0 00          	.uleb128 0x0
 5382 04f1 05          	.uleb128 0x5
 5383 04f2 49 44 4C 45 	.asciz "IDLEIF"
 5383      49 46 00 
 5384 04f9 03          	.byte 0x3
 5385 04fa A6 12       	.2byte 0x12a6
 5386 04fc 02 01 00 00 	.4byte 0x102
 5387 0500 02          	.byte 0x2
 5388 0501 01          	.byte 0x1
 5389 0502 0B          	.byte 0xb
 5390 0503 02          	.byte 0x2
 5391 0504 23          	.byte 0x23
 5392 0505 00          	.uleb128 0x0
 5393 0506 05          	.uleb128 0x5
 5394 0507 52 45 53 55 	.asciz "RESUMEIF"
 5394      4D 45 49 46 
 5394      00 
 5395 0510 03          	.byte 0x3
 5396 0511 A7 12       	.2byte 0x12a7
MPLAB XC16 ASSEMBLY Listing:   			page 153


 5397 0513 02 01 00 00 	.4byte 0x102
 5398 0517 02          	.byte 0x2
 5399 0518 01          	.byte 0x1
 5400 0519 0A          	.byte 0xa
 5401 051a 02          	.byte 0x2
 5402 051b 23          	.byte 0x23
 5403 051c 00          	.uleb128 0x0
 5404 051d 05          	.uleb128 0x5
 5405 051e 53 54 41 4C 	.asciz "STALLIF"
 5405      4C 49 46 00 
 5406 0526 03          	.byte 0x3
 5407 0527 A9 12       	.2byte 0x12a9
 5408 0529 02 01 00 00 	.4byte 0x102
 5409 052d 02          	.byte 0x2
 5410 052e 01          	.byte 0x1
 5411 052f 08          	.byte 0x8
 5412 0530 02          	.byte 0x2
 5413 0531 23          	.byte 0x23
 5414 0532 00          	.uleb128 0x0
 5415 0533 00          	.byte 0x0
 5416 0534 07          	.uleb128 0x7
 5417 0535 02          	.byte 0x2
 5418 0536 03          	.byte 0x3
 5419 0537 AB 12       	.2byte 0x12ab
 5420 0539 6C 05 00 00 	.4byte 0x56c
 5421 053d 05          	.uleb128 0x5
 5422 053e 44 45 54 41 	.asciz "DETACHIF"
 5422      43 48 49 46 
 5422      00 
 5423 0547 03          	.byte 0x3
 5424 0548 AC 12       	.2byte 0x12ac
 5425 054a 02 01 00 00 	.4byte 0x102
 5426 054e 02          	.byte 0x2
 5427 054f 01          	.byte 0x1
 5428 0550 0F          	.byte 0xf
 5429 0551 02          	.byte 0x2
 5430 0552 23          	.byte 0x23
 5431 0553 00          	.uleb128 0x0
 5432 0554 05          	.uleb128 0x5
 5433 0555 41 54 54 41 	.asciz "ATTACHIF"
 5433      43 48 49 46 
 5433      00 
 5434 055e 03          	.byte 0x3
 5435 055f AE 12       	.2byte 0x12ae
 5436 0561 02 01 00 00 	.4byte 0x102
 5437 0565 02          	.byte 0x2
 5438 0566 01          	.byte 0x1
 5439 0567 09          	.byte 0x9
 5440 0568 02          	.byte 0x2
 5441 0569 23          	.byte 0x23
 5442 056a 00          	.uleb128 0x0
 5443 056b 00          	.byte 0x0
 5444 056c 08          	.uleb128 0x8
 5445 056d 02          	.byte 0x2
 5446 056e 03          	.byte 0x3
 5447 056f A0 12       	.2byte 0x12a0
 5448 0571 80 05 00 00 	.4byte 0x580
MPLAB XC16 ASSEMBLY Listing:   			page 154


 5449 0575 09          	.uleb128 0x9
 5450 0576 96 04 00 00 	.4byte 0x496
 5451 057a 09          	.uleb128 0x9
 5452 057b 34 05 00 00 	.4byte 0x534
 5453 057f 00          	.byte 0x0
 5454 0580 04          	.uleb128 0x4
 5455 0581 74 61 67 55 	.asciz "tagU1IRBITS"
 5455      31 49 52 42 
 5455      49 54 53 00 
 5456 058d 02          	.byte 0x2
 5457 058e 03          	.byte 0x3
 5458 058f 9F 12       	.2byte 0x129f
 5459 0591 9E 05 00 00 	.4byte 0x59e
 5460 0595 0A          	.uleb128 0xa
 5461 0596 6C 05 00 00 	.4byte 0x56c
 5462 059a 02          	.byte 0x2
 5463 059b 23          	.byte 0x23
 5464 059c 00          	.uleb128 0x0
 5465 059d 00          	.byte 0x0
 5466 059e 06          	.uleb128 0x6
 5467 059f 55 31 49 52 	.asciz "U1IRBITS"
 5467      42 49 54 53 
 5467      00 
 5468 05a8 03          	.byte 0x3
 5469 05a9 B1 12       	.2byte 0x12b1
 5470 05ab 80 05 00 00 	.4byte 0x580
 5471 05af 07          	.uleb128 0x7
 5472 05b0 02          	.byte 0x2
 5473 05b1 03          	.byte 0x3
 5474 05b2 B8 12       	.2byte 0x12b8
 5475 05b4 4D 06 00 00 	.4byte 0x64d
 5476 05b8 05          	.uleb128 0x5
 5477 05b9 55 52 53 54 	.asciz "URSTIE"
 5477      49 45 00 
 5478 05c0 03          	.byte 0x3
 5479 05c1 B9 12       	.2byte 0x12b9
 5480 05c3 02 01 00 00 	.4byte 0x102
 5481 05c7 02          	.byte 0x2
 5482 05c8 01          	.byte 0x1
 5483 05c9 0F          	.byte 0xf
 5484 05ca 02          	.byte 0x2
 5485 05cb 23          	.byte 0x23
 5486 05cc 00          	.uleb128 0x0
 5487 05cd 05          	.uleb128 0x5
 5488 05ce 55 45 52 52 	.asciz "UERRIE"
 5488      49 45 00 
 5489 05d5 03          	.byte 0x3
 5490 05d6 BA 12       	.2byte 0x12ba
 5491 05d8 02 01 00 00 	.4byte 0x102
 5492 05dc 02          	.byte 0x2
 5493 05dd 01          	.byte 0x1
 5494 05de 0E          	.byte 0xe
 5495 05df 02          	.byte 0x2
 5496 05e0 23          	.byte 0x23
 5497 05e1 00          	.uleb128 0x0
 5498 05e2 05          	.uleb128 0x5
 5499 05e3 53 4F 46 49 	.asciz "SOFIE"
MPLAB XC16 ASSEMBLY Listing:   			page 155


 5499      45 00 
 5500 05e9 03          	.byte 0x3
 5501 05ea BB 12       	.2byte 0x12bb
 5502 05ec 02 01 00 00 	.4byte 0x102
 5503 05f0 02          	.byte 0x2
 5504 05f1 01          	.byte 0x1
 5505 05f2 0D          	.byte 0xd
 5506 05f3 02          	.byte 0x2
 5507 05f4 23          	.byte 0x23
 5508 05f5 00          	.uleb128 0x0
 5509 05f6 05          	.uleb128 0x5
 5510 05f7 54 52 4E 49 	.asciz "TRNIE"
 5510      45 00 
 5511 05fd 03          	.byte 0x3
 5512 05fe BC 12       	.2byte 0x12bc
 5513 0600 02 01 00 00 	.4byte 0x102
 5514 0604 02          	.byte 0x2
 5515 0605 01          	.byte 0x1
 5516 0606 0C          	.byte 0xc
 5517 0607 02          	.byte 0x2
 5518 0608 23          	.byte 0x23
 5519 0609 00          	.uleb128 0x0
 5520 060a 05          	.uleb128 0x5
 5521 060b 49 44 4C 45 	.asciz "IDLEIE"
 5521      49 45 00 
 5522 0612 03          	.byte 0x3
 5523 0613 BD 12       	.2byte 0x12bd
 5524 0615 02 01 00 00 	.4byte 0x102
 5525 0619 02          	.byte 0x2
 5526 061a 01          	.byte 0x1
 5527 061b 0B          	.byte 0xb
 5528 061c 02          	.byte 0x2
 5529 061d 23          	.byte 0x23
 5530 061e 00          	.uleb128 0x0
 5531 061f 05          	.uleb128 0x5
 5532 0620 52 45 53 55 	.asciz "RESUMEIE"
 5532      4D 45 49 45 
 5532      00 
 5533 0629 03          	.byte 0x3
 5534 062a BE 12       	.2byte 0x12be
 5535 062c 02 01 00 00 	.4byte 0x102
 5536 0630 02          	.byte 0x2
 5537 0631 01          	.byte 0x1
 5538 0632 0A          	.byte 0xa
 5539 0633 02          	.byte 0x2
 5540 0634 23          	.byte 0x23
 5541 0635 00          	.uleb128 0x0
 5542 0636 05          	.uleb128 0x5
 5543 0637 53 54 41 4C 	.asciz "STALLIE"
 5543      4C 49 45 00 
 5544 063f 03          	.byte 0x3
 5545 0640 C0 12       	.2byte 0x12c0
 5546 0642 02 01 00 00 	.4byte 0x102
 5547 0646 02          	.byte 0x2
 5548 0647 01          	.byte 0x1
 5549 0648 08          	.byte 0x8
 5550 0649 02          	.byte 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 156


 5551 064a 23          	.byte 0x23
 5552 064b 00          	.uleb128 0x0
 5553 064c 00          	.byte 0x0
 5554 064d 07          	.uleb128 0x7
 5555 064e 02          	.byte 0x2
 5556 064f 03          	.byte 0x3
 5557 0650 C2 12       	.2byte 0x12c2
 5558 0652 85 06 00 00 	.4byte 0x685
 5559 0656 05          	.uleb128 0x5
 5560 0657 44 45 54 41 	.asciz "DETACHIE"
 5560      43 48 49 45 
 5560      00 
 5561 0660 03          	.byte 0x3
 5562 0661 C3 12       	.2byte 0x12c3
 5563 0663 02 01 00 00 	.4byte 0x102
 5564 0667 02          	.byte 0x2
 5565 0668 01          	.byte 0x1
 5566 0669 0F          	.byte 0xf
 5567 066a 02          	.byte 0x2
 5568 066b 23          	.byte 0x23
 5569 066c 00          	.uleb128 0x0
 5570 066d 05          	.uleb128 0x5
 5571 066e 41 54 54 41 	.asciz "ATTACHIE"
 5571      43 48 49 45 
 5571      00 
 5572 0677 03          	.byte 0x3
 5573 0678 C5 12       	.2byte 0x12c5
 5574 067a 02 01 00 00 	.4byte 0x102
 5575 067e 02          	.byte 0x2
 5576 067f 01          	.byte 0x1
 5577 0680 09          	.byte 0x9
 5578 0681 02          	.byte 0x2
 5579 0682 23          	.byte 0x23
 5580 0683 00          	.uleb128 0x0
 5581 0684 00          	.byte 0x0
 5582 0685 08          	.uleb128 0x8
 5583 0686 02          	.byte 0x2
 5584 0687 03          	.byte 0x3
 5585 0688 B7 12       	.2byte 0x12b7
 5586 068a 99 06 00 00 	.4byte 0x699
 5587 068e 09          	.uleb128 0x9
 5588 068f AF 05 00 00 	.4byte 0x5af
 5589 0693 09          	.uleb128 0x9
 5590 0694 4D 06 00 00 	.4byte 0x64d
 5591 0698 00          	.byte 0x0
 5592 0699 04          	.uleb128 0x4
 5593 069a 74 61 67 55 	.asciz "tagU1IEBITS"
 5593      31 49 45 42 
 5593      49 54 53 00 
 5594 06a6 02          	.byte 0x2
 5595 06a7 03          	.byte 0x3
 5596 06a8 B6 12       	.2byte 0x12b6
 5597 06aa B7 06 00 00 	.4byte 0x6b7
 5598 06ae 0A          	.uleb128 0xa
 5599 06af 85 06 00 00 	.4byte 0x685
 5600 06b3 02          	.byte 0x2
 5601 06b4 23          	.byte 0x23
MPLAB XC16 ASSEMBLY Listing:   			page 157


 5602 06b5 00          	.uleb128 0x0
 5603 06b6 00          	.byte 0x0
 5604 06b7 06          	.uleb128 0x6
 5605 06b8 55 31 49 45 	.asciz "U1IEBITS"
 5605      42 49 54 53 
 5605      00 
 5606 06c1 03          	.byte 0x3
 5607 06c2 C8 12       	.2byte 0x12c8
 5608 06c4 99 06 00 00 	.4byte 0x699
 5609 06c8 07          	.uleb128 0x7
 5610 06c9 02          	.byte 0x2
 5611 06ca 03          	.byte 0x3
 5612 06cb 10 13       	.2byte 0x1310
 5613 06cd 4C 07 00 00 	.4byte 0x74c
 5614 06d1 05          	.uleb128 0x5
 5615 06d2 55 53 42 45 	.asciz "USBEN"
 5615      4E 00 
 5616 06d8 03          	.byte 0x3
 5617 06d9 11 13       	.2byte 0x1311
 5618 06db 02 01 00 00 	.4byte 0x102
 5619 06df 02          	.byte 0x2
 5620 06e0 01          	.byte 0x1
 5621 06e1 0F          	.byte 0xf
 5622 06e2 02          	.byte 0x2
 5623 06e3 23          	.byte 0x23
 5624 06e4 00          	.uleb128 0x0
 5625 06e5 05          	.uleb128 0x5
 5626 06e6 50 50 42 52 	.asciz "PPBRST"
 5626      53 54 00 
 5627 06ed 03          	.byte 0x3
 5628 06ee 12 13       	.2byte 0x1312
 5629 06f0 02 01 00 00 	.4byte 0x102
 5630 06f4 02          	.byte 0x2
 5631 06f5 01          	.byte 0x1
 5632 06f6 0E          	.byte 0xe
 5633 06f7 02          	.byte 0x2
 5634 06f8 23          	.byte 0x23
 5635 06f9 00          	.uleb128 0x0
 5636 06fa 05          	.uleb128 0x5
 5637 06fb 52 45 53 55 	.asciz "RESUME"
 5637      4D 45 00 
 5638 0702 03          	.byte 0x3
 5639 0703 13 13       	.2byte 0x1313
 5640 0705 02 01 00 00 	.4byte 0x102
 5641 0709 02          	.byte 0x2
 5642 070a 01          	.byte 0x1
 5643 070b 0D          	.byte 0xd
 5644 070c 02          	.byte 0x2
 5645 070d 23          	.byte 0x23
 5646 070e 00          	.uleb128 0x0
 5647 070f 05          	.uleb128 0x5
 5648 0710 48 4F 53 54 	.asciz "HOSTEN"
 5648      45 4E 00 
 5649 0717 03          	.byte 0x3
 5650 0718 14 13       	.2byte 0x1314
 5651 071a 02 01 00 00 	.4byte 0x102
 5652 071e 02          	.byte 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 158


 5653 071f 01          	.byte 0x1
 5654 0720 0C          	.byte 0xc
 5655 0721 02          	.byte 0x2
 5656 0722 23          	.byte 0x23
 5657 0723 00          	.uleb128 0x0
 5658 0724 05          	.uleb128 0x5
 5659 0725 50 4B 54 44 	.asciz "PKTDIS"
 5659      49 53 00 
 5660 072c 03          	.byte 0x3
 5661 072d 16 13       	.2byte 0x1316
 5662 072f 02 01 00 00 	.4byte 0x102
 5663 0733 02          	.byte 0x2
 5664 0734 01          	.byte 0x1
 5665 0735 0A          	.byte 0xa
 5666 0736 02          	.byte 0x2
 5667 0737 23          	.byte 0x23
 5668 0738 00          	.uleb128 0x0
 5669 0739 05          	.uleb128 0x5
 5670 073a 53 45 30 00 	.asciz "SE0"
 5671 073e 03          	.byte 0x3
 5672 073f 17 13       	.2byte 0x1317
 5673 0741 02 01 00 00 	.4byte 0x102
 5674 0745 02          	.byte 0x2
 5675 0746 01          	.byte 0x1
 5676 0747 09          	.byte 0x9
 5677 0748 02          	.byte 0x2
 5678 0749 23          	.byte 0x23
 5679 074a 00          	.uleb128 0x0
 5680 074b 00          	.byte 0x0
 5681 074c 07          	.uleb128 0x7
 5682 074d 02          	.byte 0x2
 5683 074e 03          	.byte 0x3
 5684 074f 19 13       	.2byte 0x1319
 5685 0751 AA 07 00 00 	.4byte 0x7aa
 5686 0755 05          	.uleb128 0x5
 5687 0756 53 4F 46 45 	.asciz "SOFEN"
 5687      4E 00 
 5688 075c 03          	.byte 0x3
 5689 075d 1A 13       	.2byte 0x131a
 5690 075f 02 01 00 00 	.4byte 0x102
 5691 0763 02          	.byte 0x2
 5692 0764 01          	.byte 0x1
 5693 0765 0F          	.byte 0xf
 5694 0766 02          	.byte 0x2
 5695 0767 23          	.byte 0x23
 5696 0768 00          	.uleb128 0x0
 5697 0769 05          	.uleb128 0x5
 5698 076a 55 53 42 52 	.asciz "USBRST"
 5698      53 54 00 
 5699 0771 03          	.byte 0x3
 5700 0772 1C 13       	.2byte 0x131c
 5701 0774 02 01 00 00 	.4byte 0x102
 5702 0778 02          	.byte 0x2
 5703 0779 01          	.byte 0x1
 5704 077a 0B          	.byte 0xb
 5705 077b 02          	.byte 0x2
 5706 077c 23          	.byte 0x23
MPLAB XC16 ASSEMBLY Listing:   			page 159


 5707 077d 00          	.uleb128 0x0
 5708 077e 05          	.uleb128 0x5
 5709 077f 54 4F 4B 42 	.asciz "TOKBUSY"
 5709      55 53 59 00 
 5710 0787 03          	.byte 0x3
 5711 0788 1D 13       	.2byte 0x131d
 5712 078a 02 01 00 00 	.4byte 0x102
 5713 078e 02          	.byte 0x2
 5714 078f 01          	.byte 0x1
 5715 0790 0A          	.byte 0xa
 5716 0791 02          	.byte 0x2
 5717 0792 23          	.byte 0x23
 5718 0793 00          	.uleb128 0x0
 5719 0794 05          	.uleb128 0x5
 5720 0795 4A 53 54 41 	.asciz "JSTATE"
 5720      54 45 00 
 5721 079c 03          	.byte 0x3
 5722 079d 1F 13       	.2byte 0x131f
 5723 079f 02 01 00 00 	.4byte 0x102
 5724 07a3 02          	.byte 0x2
 5725 07a4 01          	.byte 0x1
 5726 07a5 08          	.byte 0x8
 5727 07a6 02          	.byte 0x2
 5728 07a7 23          	.byte 0x23
 5729 07a8 00          	.uleb128 0x0
 5730 07a9 00          	.byte 0x0
 5731 07aa 07          	.uleb128 0x7
 5732 07ab 02          	.byte 0x2
 5733 07ac 03          	.byte 0x3
 5734 07ad 21 13       	.2byte 0x1321
 5735 07af C8 07 00 00 	.4byte 0x7c8
 5736 07b3 05          	.uleb128 0x5
 5737 07b4 52 45 53 45 	.asciz "RESET"
 5737      54 00 
 5738 07ba 03          	.byte 0x3
 5739 07bb 23 13       	.2byte 0x1323
 5740 07bd 02 01 00 00 	.4byte 0x102
 5741 07c1 02          	.byte 0x2
 5742 07c2 01          	.byte 0x1
 5743 07c3 0B          	.byte 0xb
 5744 07c4 02          	.byte 0x2
 5745 07c5 23          	.byte 0x23
 5746 07c6 00          	.uleb128 0x0
 5747 07c7 00          	.byte 0x0
 5748 07c8 08          	.uleb128 0x8
 5749 07c9 02          	.byte 0x2
 5750 07ca 03          	.byte 0x3
 5751 07cb 0F 13       	.2byte 0x130f
 5752 07cd E1 07 00 00 	.4byte 0x7e1
 5753 07d1 09          	.uleb128 0x9
 5754 07d2 C8 06 00 00 	.4byte 0x6c8
 5755 07d6 09          	.uleb128 0x9
 5756 07d7 4C 07 00 00 	.4byte 0x74c
 5757 07db 09          	.uleb128 0x9
 5758 07dc AA 07 00 00 	.4byte 0x7aa
 5759 07e0 00          	.byte 0x0
 5760 07e1 04          	.uleb128 0x4
MPLAB XC16 ASSEMBLY Listing:   			page 160


 5761 07e2 74 61 67 55 	.asciz "tagU1CONBITS"
 5761      31 43 4F 4E 
 5761      42 49 54 53 
 5761      00 
 5762 07ef 02          	.byte 0x2
 5763 07f0 03          	.byte 0x3
 5764 07f1 0E 13       	.2byte 0x130e
 5765 07f3 00 08 00 00 	.4byte 0x800
 5766 07f7 0A          	.uleb128 0xa
 5767 07f8 C8 07 00 00 	.4byte 0x7c8
 5768 07fc 02          	.byte 0x2
 5769 07fd 23          	.byte 0x23
 5770 07fe 00          	.uleb128 0x0
 5771 07ff 00          	.byte 0x0
 5772 0800 06          	.uleb128 0x6
 5773 0801 55 31 43 4F 	.asciz "U1CONBITS"
 5773      4E 42 49 54 
 5773      53 00 
 5774 080b 03          	.byte 0x3
 5775 080c 26 13       	.2byte 0x1326
 5776 080e E1 07 00 00 	.4byte 0x7e1
 5777 0812 07          	.uleb128 0x7
 5778 0813 02          	.byte 0x2
 5779 0814 03          	.byte 0x3
 5780 0815 A4 13       	.2byte 0x13a4
 5781 0817 B2 08 00 00 	.4byte 0x8b2
 5782 081b 05          	.uleb128 0x5
 5783 081c 45 50 48 53 	.asciz "EPHSHK"
 5783      48 4B 00 
 5784 0823 03          	.byte 0x3
 5785 0824 A5 13       	.2byte 0x13a5
 5786 0826 02 01 00 00 	.4byte 0x102
 5787 082a 02          	.byte 0x2
 5788 082b 01          	.byte 0x1
 5789 082c 0F          	.byte 0xf
 5790 082d 02          	.byte 0x2
 5791 082e 23          	.byte 0x23
 5792 082f 00          	.uleb128 0x0
 5793 0830 05          	.uleb128 0x5
 5794 0831 45 50 53 54 	.asciz "EPSTALL"
 5794      41 4C 4C 00 
 5795 0839 03          	.byte 0x3
 5796 083a A6 13       	.2byte 0x13a6
 5797 083c 02 01 00 00 	.4byte 0x102
 5798 0840 02          	.byte 0x2
 5799 0841 01          	.byte 0x1
 5800 0842 0E          	.byte 0xe
 5801 0843 02          	.byte 0x2
 5802 0844 23          	.byte 0x23
 5803 0845 00          	.uleb128 0x0
 5804 0846 05          	.uleb128 0x5
 5805 0847 45 50 54 58 	.asciz "EPTXEN"
 5805      45 4E 00 
 5806 084e 03          	.byte 0x3
 5807 084f A7 13       	.2byte 0x13a7
 5808 0851 02 01 00 00 	.4byte 0x102
 5809 0855 02          	.byte 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 161


 5810 0856 01          	.byte 0x1
 5811 0857 0D          	.byte 0xd
 5812 0858 02          	.byte 0x2
 5813 0859 23          	.byte 0x23
 5814 085a 00          	.uleb128 0x0
 5815 085b 05          	.uleb128 0x5
 5816 085c 45 50 52 58 	.asciz "EPRXEN"
 5816      45 4E 00 
 5817 0863 03          	.byte 0x3
 5818 0864 A8 13       	.2byte 0x13a8
 5819 0866 02 01 00 00 	.4byte 0x102
 5820 086a 02          	.byte 0x2
 5821 086b 01          	.byte 0x1
 5822 086c 0C          	.byte 0xc
 5823 086d 02          	.byte 0x2
 5824 086e 23          	.byte 0x23
 5825 086f 00          	.uleb128 0x0
 5826 0870 05          	.uleb128 0x5
 5827 0871 45 50 43 4F 	.asciz "EPCONDIS"
 5827      4E 44 49 53 
 5827      00 
 5828 087a 03          	.byte 0x3
 5829 087b A9 13       	.2byte 0x13a9
 5830 087d 02 01 00 00 	.4byte 0x102
 5831 0881 02          	.byte 0x2
 5832 0882 01          	.byte 0x1
 5833 0883 0B          	.byte 0xb
 5834 0884 02          	.byte 0x2
 5835 0885 23          	.byte 0x23
 5836 0886 00          	.uleb128 0x0
 5837 0887 05          	.uleb128 0x5
 5838 0888 52 45 54 52 	.asciz "RETRYDIS"
 5838      59 44 49 53 
 5838      00 
 5839 0891 03          	.byte 0x3
 5840 0892 AB 13       	.2byte 0x13ab
 5841 0894 02 01 00 00 	.4byte 0x102
 5842 0898 02          	.byte 0x2
 5843 0899 01          	.byte 0x1
 5844 089a 09          	.byte 0x9
 5845 089b 02          	.byte 0x2
 5846 089c 23          	.byte 0x23
 5847 089d 00          	.uleb128 0x0
 5848 089e 05          	.uleb128 0x5
 5849 089f 4C 53 50 44 	.asciz "LSPD"
 5849      00 
 5850 08a4 03          	.byte 0x3
 5851 08a5 AC 13       	.2byte 0x13ac
 5852 08a7 02 01 00 00 	.4byte 0x102
 5853 08ab 02          	.byte 0x2
 5854 08ac 01          	.byte 0x1
 5855 08ad 08          	.byte 0x8
 5856 08ae 02          	.byte 0x2
 5857 08af 23          	.byte 0x23
 5858 08b0 00          	.uleb128 0x0
 5859 08b1 00          	.byte 0x0
 5860 08b2 07          	.uleb128 0x7
MPLAB XC16 ASSEMBLY Listing:   			page 162


 5861 08b3 02          	.byte 0x2
 5862 08b4 03          	.byte 0x3
 5863 08b5 AE 13       	.2byte 0x13ae
 5864 08b7 FC 08 00 00 	.4byte 0x8fc
 5865 08bb 05          	.uleb128 0x5
 5866 08bc 45 50 49 4E 	.asciz "EPINEN"
 5866      45 4E 00 
 5867 08c3 03          	.byte 0x3
 5868 08c4 B0 13       	.2byte 0x13b0
 5869 08c6 02 01 00 00 	.4byte 0x102
 5870 08ca 02          	.byte 0x2
 5871 08cb 01          	.byte 0x1
 5872 08cc 0D          	.byte 0xd
 5873 08cd 02          	.byte 0x2
 5874 08ce 23          	.byte 0x23
 5875 08cf 00          	.uleb128 0x0
 5876 08d0 05          	.uleb128 0x5
 5877 08d1 45 50 4F 55 	.asciz "EPOUTEN"
 5877      54 45 4E 00 
 5878 08d9 03          	.byte 0x3
 5879 08da B1 13       	.2byte 0x13b1
 5880 08dc 02 01 00 00 	.4byte 0x102
 5881 08e0 02          	.byte 0x2
 5882 08e1 01          	.byte 0x1
 5883 08e2 0C          	.byte 0xc
 5884 08e3 02          	.byte 0x2
 5885 08e4 23          	.byte 0x23
 5886 08e5 00          	.uleb128 0x0
 5887 08e6 05          	.uleb128 0x5
 5888 08e7 4C 4F 57 53 	.asciz "LOWSPD"
 5888      50 44 00 
 5889 08ee 03          	.byte 0x3
 5890 08ef B3 13       	.2byte 0x13b3
 5891 08f1 02 01 00 00 	.4byte 0x102
 5892 08f5 02          	.byte 0x2
 5893 08f6 01          	.byte 0x1
 5894 08f7 08          	.byte 0x8
 5895 08f8 02          	.byte 0x2
 5896 08f9 23          	.byte 0x23
 5897 08fa 00          	.uleb128 0x0
 5898 08fb 00          	.byte 0x0
 5899 08fc 08          	.uleb128 0x8
 5900 08fd 02          	.byte 0x2
 5901 08fe 03          	.byte 0x3
 5902 08ff A3 13       	.2byte 0x13a3
 5903 0901 10 09 00 00 	.4byte 0x910
 5904 0905 09          	.uleb128 0x9
 5905 0906 12 08 00 00 	.4byte 0x812
 5906 090a 09          	.uleb128 0x9
 5907 090b B2 08 00 00 	.4byte 0x8b2
 5908 090f 00          	.byte 0x0
 5909 0910 04          	.uleb128 0x4
 5910 0911 74 61 67 55 	.asciz "tagU1EP0BITS"
 5910      31 45 50 30 
 5910      42 49 54 53 
 5910      00 
 5911 091e 02          	.byte 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 163


 5912 091f 03          	.byte 0x3
 5913 0920 A2 13       	.2byte 0x13a2
 5914 0922 2F 09 00 00 	.4byte 0x92f
 5915 0926 0A          	.uleb128 0xa
 5916 0927 FC 08 00 00 	.4byte 0x8fc
 5917 092b 02          	.byte 0x2
 5918 092c 23          	.byte 0x23
 5919 092d 00          	.uleb128 0x0
 5920 092e 00          	.byte 0x0
 5921 092f 06          	.uleb128 0x6
 5922 0930 55 31 45 50 	.asciz "U1EP0BITS"
 5922      30 42 49 54 
 5922      53 00 
 5923 093a 03          	.byte 0x3
 5924 093b B6 13       	.2byte 0x13b6
 5925 093d 10 09 00 00 	.4byte 0x910
 5926 0941 04          	.uleb128 0x4
 5927 0942 74 61 67 49 	.asciz "tagIFS5BITS"
 5927      46 53 35 42 
 5927      49 54 53 00 
 5928 094e 02          	.byte 0x2
 5929 094f 03          	.byte 0x3
 5930 0950 71 25       	.2byte 0x2571
 5931 0952 65 0A 00 00 	.4byte 0xa65
 5932 0956 05          	.uleb128 0x5
 5933 0957 55 33 45 49 	.asciz "U3EIF"
 5933      46 00 
 5934 095d 03          	.byte 0x3
 5935 095e 73 25       	.2byte 0x2573
 5936 0960 02 01 00 00 	.4byte 0x102
 5937 0964 02          	.byte 0x2
 5938 0965 01          	.byte 0x1
 5939 0966 0E          	.byte 0xe
 5940 0967 02          	.byte 0x2
 5941 0968 23          	.byte 0x23
 5942 0969 00          	.uleb128 0x0
 5943 096a 05          	.uleb128 0x5
 5944 096b 55 33 52 58 	.asciz "U3RXIF"
 5944      49 46 00 
 5945 0972 03          	.byte 0x3
 5946 0973 74 25       	.2byte 0x2574
 5947 0975 02 01 00 00 	.4byte 0x102
 5948 0979 02          	.byte 0x2
 5949 097a 01          	.byte 0x1
 5950 097b 0D          	.byte 0xd
 5951 097c 02          	.byte 0x2
 5952 097d 23          	.byte 0x23
 5953 097e 00          	.uleb128 0x0
 5954 097f 05          	.uleb128 0x5
 5955 0980 55 33 54 58 	.asciz "U3TXIF"
 5955      49 46 00 
 5956 0987 03          	.byte 0x3
 5957 0988 75 25       	.2byte 0x2575
 5958 098a 02 01 00 00 	.4byte 0x102
 5959 098e 02          	.byte 0x2
 5960 098f 01          	.byte 0x1
 5961 0990 0C          	.byte 0xc
MPLAB XC16 ASSEMBLY Listing:   			page 164


 5962 0991 02          	.byte 0x2
 5963 0992 23          	.byte 0x23
 5964 0993 00          	.uleb128 0x0
 5965 0994 05          	.uleb128 0x5
 5966 0995 55 53 42 31 	.asciz "USB1IF"
 5966      49 46 00 
 5967 099c 03          	.byte 0x3
 5968 099d 77 25       	.2byte 0x2577
 5969 099f 02 01 00 00 	.4byte 0x102
 5970 09a3 02          	.byte 0x2
 5971 09a4 01          	.byte 0x1
 5972 09a5 09          	.byte 0x9
 5973 09a6 02          	.byte 0x2
 5974 09a7 23          	.byte 0x23
 5975 09a8 00          	.uleb128 0x0
 5976 09a9 05          	.uleb128 0x5
 5977 09aa 55 34 45 49 	.asciz "U4EIF"
 5977      46 00 
 5978 09b0 03          	.byte 0x3
 5979 09b1 78 25       	.2byte 0x2578
 5980 09b3 02 01 00 00 	.4byte 0x102
 5981 09b7 02          	.byte 0x2
 5982 09b8 01          	.byte 0x1
 5983 09b9 08          	.byte 0x8
 5984 09ba 02          	.byte 0x2
 5985 09bb 23          	.byte 0x23
 5986 09bc 00          	.uleb128 0x0
 5987 09bd 05          	.uleb128 0x5
 5988 09be 55 34 52 58 	.asciz "U4RXIF"
 5988      49 46 00 
 5989 09c5 03          	.byte 0x3
 5990 09c6 79 25       	.2byte 0x2579
 5991 09c8 02 01 00 00 	.4byte 0x102
 5992 09cc 02          	.byte 0x2
 5993 09cd 01          	.byte 0x1
 5994 09ce 07          	.byte 0x7
 5995 09cf 02          	.byte 0x2
 5996 09d0 23          	.byte 0x23
 5997 09d1 00          	.uleb128 0x0
 5998 09d2 05          	.uleb128 0x5
 5999 09d3 55 34 54 58 	.asciz "U4TXIF"
 5999      49 46 00 
 6000 09da 03          	.byte 0x3
 6001 09db 7A 25       	.2byte 0x257a
 6002 09dd 02 01 00 00 	.4byte 0x102
 6003 09e1 02          	.byte 0x2
 6004 09e2 01          	.byte 0x1
 6005 09e3 06          	.byte 0x6
 6006 09e4 02          	.byte 0x2
 6007 09e5 23          	.byte 0x23
 6008 09e6 00          	.uleb128 0x0
 6009 09e7 05          	.uleb128 0x5
 6010 09e8 53 50 49 33 	.asciz "SPI3EIF"
 6010      45 49 46 00 
 6011 09f0 03          	.byte 0x3
 6012 09f1 7B 25       	.2byte 0x257b
 6013 09f3 02 01 00 00 	.4byte 0x102
MPLAB XC16 ASSEMBLY Listing:   			page 165


 6014 09f7 02          	.byte 0x2
 6015 09f8 01          	.byte 0x1
 6016 09f9 05          	.byte 0x5
 6017 09fa 02          	.byte 0x2
 6018 09fb 23          	.byte 0x23
 6019 09fc 00          	.uleb128 0x0
 6020 09fd 05          	.uleb128 0x5
 6021 09fe 53 50 49 33 	.asciz "SPI3IF"
 6021      49 46 00 
 6022 0a05 03          	.byte 0x3
 6023 0a06 7C 25       	.2byte 0x257c
 6024 0a08 02 01 00 00 	.4byte 0x102
 6025 0a0c 02          	.byte 0x2
 6026 0a0d 01          	.byte 0x1
 6027 0a0e 04          	.byte 0x4
 6028 0a0f 02          	.byte 0x2
 6029 0a10 23          	.byte 0x23
 6030 0a11 00          	.uleb128 0x0
 6031 0a12 05          	.uleb128 0x5
 6032 0a13 4F 43 39 49 	.asciz "OC9IF"
 6032      46 00 
 6033 0a19 03          	.byte 0x3
 6034 0a1a 7D 25       	.2byte 0x257d
 6035 0a1c 02 01 00 00 	.4byte 0x102
 6036 0a20 02          	.byte 0x2
 6037 0a21 01          	.byte 0x1
 6038 0a22 03          	.byte 0x3
 6039 0a23 02          	.byte 0x2
 6040 0a24 23          	.byte 0x23
 6041 0a25 00          	.uleb128 0x0
 6042 0a26 05          	.uleb128 0x5
 6043 0a27 49 43 39 49 	.asciz "IC9IF"
 6043      46 00 
 6044 0a2d 03          	.byte 0x3
 6045 0a2e 7E 25       	.2byte 0x257e
 6046 0a30 02 01 00 00 	.4byte 0x102
 6047 0a34 02          	.byte 0x2
 6048 0a35 01          	.byte 0x1
 6049 0a36 02          	.byte 0x2
 6050 0a37 02          	.byte 0x2
 6051 0a38 23          	.byte 0x23
 6052 0a39 00          	.uleb128 0x0
 6053 0a3a 05          	.uleb128 0x5
 6054 0a3b 50 57 4D 31 	.asciz "PWM1IF"
 6054      49 46 00 
 6055 0a42 03          	.byte 0x3
 6056 0a43 7F 25       	.2byte 0x257f
 6057 0a45 02 01 00 00 	.4byte 0x102
 6058 0a49 02          	.byte 0x2
 6059 0a4a 01          	.byte 0x1
 6060 0a4b 01          	.byte 0x1
 6061 0a4c 02          	.byte 0x2
 6062 0a4d 23          	.byte 0x23
 6063 0a4e 00          	.uleb128 0x0
 6064 0a4f 05          	.uleb128 0x5
 6065 0a50 50 57 4D 32 	.asciz "PWM2IF"
 6065      49 46 00 
MPLAB XC16 ASSEMBLY Listing:   			page 166


 6066 0a57 03          	.byte 0x3
 6067 0a58 80 25       	.2byte 0x2580
 6068 0a5a 02 01 00 00 	.4byte 0x102
 6069 0a5e 02          	.byte 0x2
 6070 0a5f 01          	.byte 0x1
 6071 0a60 10          	.byte 0x10
 6072 0a61 02          	.byte 0x2
 6073 0a62 23          	.byte 0x23
 6074 0a63 00          	.uleb128 0x0
 6075 0a64 00          	.byte 0x0
 6076 0a65 06          	.uleb128 0x6
 6077 0a66 49 46 53 35 	.asciz "IFS5BITS"
 6077      42 49 54 53 
 6077      00 
 6078 0a6f 03          	.byte 0x3
 6079 0a70 81 25       	.2byte 0x2581
 6080 0a72 41 09 00 00 	.4byte 0x941
 6081 0a76 04          	.uleb128 0x4
 6082 0a77 74 61 67 49 	.asciz "tagIEC5BITS"
 6082      45 43 35 42 
 6082      49 54 53 00 
 6083 0a83 02          	.byte 0x2
 6084 0a84 03          	.byte 0x3
 6085 0a85 1D 26       	.2byte 0x261d
 6086 0a87 9A 0B 00 00 	.4byte 0xb9a
 6087 0a8b 05          	.uleb128 0x5
 6088 0a8c 55 33 45 49 	.asciz "U3EIE"
 6088      45 00 
 6089 0a92 03          	.byte 0x3
 6090 0a93 1F 26       	.2byte 0x261f
 6091 0a95 02 01 00 00 	.4byte 0x102
 6092 0a99 02          	.byte 0x2
 6093 0a9a 01          	.byte 0x1
 6094 0a9b 0E          	.byte 0xe
 6095 0a9c 02          	.byte 0x2
 6096 0a9d 23          	.byte 0x23
 6097 0a9e 00          	.uleb128 0x0
 6098 0a9f 05          	.uleb128 0x5
 6099 0aa0 55 33 52 58 	.asciz "U3RXIE"
 6099      49 45 00 
 6100 0aa7 03          	.byte 0x3
 6101 0aa8 20 26       	.2byte 0x2620
 6102 0aaa 02 01 00 00 	.4byte 0x102
 6103 0aae 02          	.byte 0x2
 6104 0aaf 01          	.byte 0x1
 6105 0ab0 0D          	.byte 0xd
 6106 0ab1 02          	.byte 0x2
 6107 0ab2 23          	.byte 0x23
 6108 0ab3 00          	.uleb128 0x0
 6109 0ab4 05          	.uleb128 0x5
 6110 0ab5 55 33 54 58 	.asciz "U3TXIE"
 6110      49 45 00 
 6111 0abc 03          	.byte 0x3
 6112 0abd 21 26       	.2byte 0x2621
 6113 0abf 02 01 00 00 	.4byte 0x102
 6114 0ac3 02          	.byte 0x2
 6115 0ac4 01          	.byte 0x1
MPLAB XC16 ASSEMBLY Listing:   			page 167


 6116 0ac5 0C          	.byte 0xc
 6117 0ac6 02          	.byte 0x2
 6118 0ac7 23          	.byte 0x23
 6119 0ac8 00          	.uleb128 0x0
 6120 0ac9 05          	.uleb128 0x5
 6121 0aca 55 53 42 31 	.asciz "USB1IE"
 6121      49 45 00 
 6122 0ad1 03          	.byte 0x3
 6123 0ad2 23 26       	.2byte 0x2623
 6124 0ad4 02 01 00 00 	.4byte 0x102
 6125 0ad8 02          	.byte 0x2
 6126 0ad9 01          	.byte 0x1
 6127 0ada 09          	.byte 0x9
 6128 0adb 02          	.byte 0x2
 6129 0adc 23          	.byte 0x23
 6130 0add 00          	.uleb128 0x0
 6131 0ade 05          	.uleb128 0x5
 6132 0adf 55 34 45 49 	.asciz "U4EIE"
 6132      45 00 
 6133 0ae5 03          	.byte 0x3
 6134 0ae6 24 26       	.2byte 0x2624
 6135 0ae8 02 01 00 00 	.4byte 0x102
 6136 0aec 02          	.byte 0x2
 6137 0aed 01          	.byte 0x1
 6138 0aee 08          	.byte 0x8
 6139 0aef 02          	.byte 0x2
 6140 0af0 23          	.byte 0x23
 6141 0af1 00          	.uleb128 0x0
 6142 0af2 05          	.uleb128 0x5
 6143 0af3 55 34 52 58 	.asciz "U4RXIE"
 6143      49 45 00 
 6144 0afa 03          	.byte 0x3
 6145 0afb 25 26       	.2byte 0x2625
 6146 0afd 02 01 00 00 	.4byte 0x102
 6147 0b01 02          	.byte 0x2
 6148 0b02 01          	.byte 0x1
 6149 0b03 07          	.byte 0x7
 6150 0b04 02          	.byte 0x2
 6151 0b05 23          	.byte 0x23
 6152 0b06 00          	.uleb128 0x0
 6153 0b07 05          	.uleb128 0x5
 6154 0b08 55 34 54 58 	.asciz "U4TXIE"
 6154      49 45 00 
 6155 0b0f 03          	.byte 0x3
 6156 0b10 26 26       	.2byte 0x2626
 6157 0b12 02 01 00 00 	.4byte 0x102
 6158 0b16 02          	.byte 0x2
 6159 0b17 01          	.byte 0x1
 6160 0b18 06          	.byte 0x6
 6161 0b19 02          	.byte 0x2
 6162 0b1a 23          	.byte 0x23
 6163 0b1b 00          	.uleb128 0x0
 6164 0b1c 05          	.uleb128 0x5
 6165 0b1d 53 50 49 33 	.asciz "SPI3EIE"
 6165      45 49 45 00 
 6166 0b25 03          	.byte 0x3
 6167 0b26 27 26       	.2byte 0x2627
MPLAB XC16 ASSEMBLY Listing:   			page 168


 6168 0b28 02 01 00 00 	.4byte 0x102
 6169 0b2c 02          	.byte 0x2
 6170 0b2d 01          	.byte 0x1
 6171 0b2e 05          	.byte 0x5
 6172 0b2f 02          	.byte 0x2
 6173 0b30 23          	.byte 0x23
 6174 0b31 00          	.uleb128 0x0
 6175 0b32 05          	.uleb128 0x5
 6176 0b33 53 50 49 33 	.asciz "SPI3IE"
 6176      49 45 00 
 6177 0b3a 03          	.byte 0x3
 6178 0b3b 28 26       	.2byte 0x2628
 6179 0b3d 02 01 00 00 	.4byte 0x102
 6180 0b41 02          	.byte 0x2
 6181 0b42 01          	.byte 0x1
 6182 0b43 04          	.byte 0x4
 6183 0b44 02          	.byte 0x2
 6184 0b45 23          	.byte 0x23
 6185 0b46 00          	.uleb128 0x0
 6186 0b47 05          	.uleb128 0x5
 6187 0b48 4F 43 39 49 	.asciz "OC9IE"
 6187      45 00 
 6188 0b4e 03          	.byte 0x3
 6189 0b4f 29 26       	.2byte 0x2629
 6190 0b51 02 01 00 00 	.4byte 0x102
 6191 0b55 02          	.byte 0x2
 6192 0b56 01          	.byte 0x1
 6193 0b57 03          	.byte 0x3
 6194 0b58 02          	.byte 0x2
 6195 0b59 23          	.byte 0x23
 6196 0b5a 00          	.uleb128 0x0
 6197 0b5b 05          	.uleb128 0x5
 6198 0b5c 49 43 39 49 	.asciz "IC9IE"
 6198      45 00 
 6199 0b62 03          	.byte 0x3
 6200 0b63 2A 26       	.2byte 0x262a
 6201 0b65 02 01 00 00 	.4byte 0x102
 6202 0b69 02          	.byte 0x2
 6203 0b6a 01          	.byte 0x1
 6204 0b6b 02          	.byte 0x2
 6205 0b6c 02          	.byte 0x2
 6206 0b6d 23          	.byte 0x23
 6207 0b6e 00          	.uleb128 0x0
 6208 0b6f 05          	.uleb128 0x5
 6209 0b70 50 57 4D 31 	.asciz "PWM1IE"
 6209      49 45 00 
 6210 0b77 03          	.byte 0x3
 6211 0b78 2B 26       	.2byte 0x262b
 6212 0b7a 02 01 00 00 	.4byte 0x102
 6213 0b7e 02          	.byte 0x2
 6214 0b7f 01          	.byte 0x1
 6215 0b80 01          	.byte 0x1
 6216 0b81 02          	.byte 0x2
 6217 0b82 23          	.byte 0x23
 6218 0b83 00          	.uleb128 0x0
 6219 0b84 05          	.uleb128 0x5
 6220 0b85 50 57 4D 32 	.asciz "PWM2IE"
MPLAB XC16 ASSEMBLY Listing:   			page 169


 6220      49 45 00 
 6221 0b8c 03          	.byte 0x3
 6222 0b8d 2C 26       	.2byte 0x262c
 6223 0b8f 02 01 00 00 	.4byte 0x102
 6224 0b93 02          	.byte 0x2
 6225 0b94 01          	.byte 0x1
 6226 0b95 10          	.byte 0x10
 6227 0b96 02          	.byte 0x2
 6228 0b97 23          	.byte 0x23
 6229 0b98 00          	.uleb128 0x0
 6230 0b99 00          	.byte 0x0
 6231 0b9a 06          	.uleb128 0x6
 6232 0b9b 49 45 43 35 	.asciz "IEC5BITS"
 6232      42 49 54 53 
 6232      00 
 6233 0ba4 03          	.byte 0x3
 6234 0ba5 2D 26       	.2byte 0x262d
 6235 0ba7 76 0A 00 00 	.4byte 0xa76
 6236 0bab 07          	.uleb128 0x7
 6237 0bac 02          	.byte 0x2
 6238 0bad 03          	.byte 0x3
 6239 0bae F2 28       	.2byte 0x28f2
 6240 0bb0 DE 0B 00 00 	.4byte 0xbde
 6241 0bb4 05          	.uleb128 0x5
 6242 0bb5 55 53 42 31 	.asciz "USB1IP"
 6242      49 50 00 
 6243 0bbc 03          	.byte 0x3
 6244 0bbd F4 28       	.2byte 0x28f4
 6245 0bbf 02 01 00 00 	.4byte 0x102
 6246 0bc3 02          	.byte 0x2
 6247 0bc4 03          	.byte 0x3
 6248 0bc5 05          	.byte 0x5
 6249 0bc6 02          	.byte 0x2
 6250 0bc7 23          	.byte 0x23
 6251 0bc8 00          	.uleb128 0x0
 6252 0bc9 05          	.uleb128 0x5
 6253 0bca 55 34 45 49 	.asciz "U4EIP"
 6253      50 00 
 6254 0bd0 03          	.byte 0x3
 6255 0bd1 F6 28       	.2byte 0x28f6
 6256 0bd3 02 01 00 00 	.4byte 0x102
 6257 0bd7 02          	.byte 0x2
 6258 0bd8 03          	.byte 0x3
 6259 0bd9 01          	.byte 0x1
 6260 0bda 02          	.byte 0x2
 6261 0bdb 23          	.byte 0x23
 6262 0bdc 00          	.uleb128 0x0
 6263 0bdd 00          	.byte 0x0
 6264 0bde 07          	.uleb128 0x7
 6265 0bdf 02          	.byte 0x2
 6266 0be0 03          	.byte 0x3
 6267 0be1 F8 28       	.2byte 0x28f8
 6268 0be3 69 0C 00 00 	.4byte 0xc69
 6269 0be7 05          	.uleb128 0x5
 6270 0be8 55 53 42 31 	.asciz "USB1IP0"
 6270      49 50 30 00 
 6271 0bf0 03          	.byte 0x3
MPLAB XC16 ASSEMBLY Listing:   			page 170


 6272 0bf1 FA 28       	.2byte 0x28fa
 6273 0bf3 02 01 00 00 	.4byte 0x102
 6274 0bf7 02          	.byte 0x2
 6275 0bf8 01          	.byte 0x1
 6276 0bf9 07          	.byte 0x7
 6277 0bfa 02          	.byte 0x2
 6278 0bfb 23          	.byte 0x23
 6279 0bfc 00          	.uleb128 0x0
 6280 0bfd 05          	.uleb128 0x5
 6281 0bfe 55 53 42 31 	.asciz "USB1IP1"
 6281      49 50 31 00 
 6282 0c06 03          	.byte 0x3
 6283 0c07 FB 28       	.2byte 0x28fb
 6284 0c09 02 01 00 00 	.4byte 0x102
 6285 0c0d 02          	.byte 0x2
 6286 0c0e 01          	.byte 0x1
 6287 0c0f 06          	.byte 0x6
 6288 0c10 02          	.byte 0x2
 6289 0c11 23          	.byte 0x23
 6290 0c12 00          	.uleb128 0x0
 6291 0c13 05          	.uleb128 0x5
 6292 0c14 55 53 42 31 	.asciz "USB1IP2"
 6292      49 50 32 00 
 6293 0c1c 03          	.byte 0x3
 6294 0c1d FC 28       	.2byte 0x28fc
 6295 0c1f 02 01 00 00 	.4byte 0x102
 6296 0c23 02          	.byte 0x2
 6297 0c24 01          	.byte 0x1
 6298 0c25 05          	.byte 0x5
 6299 0c26 02          	.byte 0x2
 6300 0c27 23          	.byte 0x23
 6301 0c28 00          	.uleb128 0x0
 6302 0c29 05          	.uleb128 0x5
 6303 0c2a 55 34 45 49 	.asciz "U4EIP0"
 6303      50 30 00 
 6304 0c31 03          	.byte 0x3
 6305 0c32 FE 28       	.2byte 0x28fe
 6306 0c34 02 01 00 00 	.4byte 0x102
 6307 0c38 02          	.byte 0x2
 6308 0c39 01          	.byte 0x1
 6309 0c3a 03          	.byte 0x3
 6310 0c3b 02          	.byte 0x2
 6311 0c3c 23          	.byte 0x23
 6312 0c3d 00          	.uleb128 0x0
 6313 0c3e 05          	.uleb128 0x5
 6314 0c3f 55 34 45 49 	.asciz "U4EIP1"
 6314      50 31 00 
 6315 0c46 03          	.byte 0x3
 6316 0c47 FF 28       	.2byte 0x28ff
 6317 0c49 02 01 00 00 	.4byte 0x102
 6318 0c4d 02          	.byte 0x2
 6319 0c4e 01          	.byte 0x1
 6320 0c4f 02          	.byte 0x2
 6321 0c50 02          	.byte 0x2
 6322 0c51 23          	.byte 0x23
 6323 0c52 00          	.uleb128 0x0
 6324 0c53 05          	.uleb128 0x5
MPLAB XC16 ASSEMBLY Listing:   			page 171


 6325 0c54 55 34 45 49 	.asciz "U4EIP2"
 6325      50 32 00 
 6326 0c5b 03          	.byte 0x3
 6327 0c5c 00 29       	.2byte 0x2900
 6328 0c5e 02 01 00 00 	.4byte 0x102
 6329 0c62 02          	.byte 0x2
 6330 0c63 01          	.byte 0x1
 6331 0c64 01          	.byte 0x1
 6332 0c65 02          	.byte 0x2
 6333 0c66 23          	.byte 0x23
 6334 0c67 00          	.uleb128 0x0
 6335 0c68 00          	.byte 0x0
 6336 0c69 08          	.uleb128 0x8
 6337 0c6a 02          	.byte 0x2
 6338 0c6b 03          	.byte 0x3
 6339 0c6c F1 28       	.2byte 0x28f1
 6340 0c6e 7D 0C 00 00 	.4byte 0xc7d
 6341 0c72 09          	.uleb128 0x9
 6342 0c73 AB 0B 00 00 	.4byte 0xbab
 6343 0c77 09          	.uleb128 0x9
 6344 0c78 DE 0B 00 00 	.4byte 0xbde
 6345 0c7c 00          	.byte 0x0
 6346 0c7d 04          	.uleb128 0x4
 6347 0c7e 74 61 67 49 	.asciz "tagIPC21BITS"
 6347      50 43 32 31 
 6347      42 49 54 53 
 6347      00 
 6348 0c8b 02          	.byte 0x2
 6349 0c8c 03          	.byte 0x3
 6350 0c8d F0 28       	.2byte 0x28f0
 6351 0c8f 9C 0C 00 00 	.4byte 0xc9c
 6352 0c93 0A          	.uleb128 0xa
 6353 0c94 69 0C 00 00 	.4byte 0xc69
 6354 0c98 02          	.byte 0x2
 6355 0c99 23          	.byte 0x23
 6356 0c9a 00          	.uleb128 0x0
 6357 0c9b 00          	.byte 0x0
 6358 0c9c 06          	.uleb128 0x6
 6359 0c9d 49 50 43 32 	.asciz "IPC21BITS"
 6359      31 42 49 54 
 6359      53 00 
 6360 0ca7 03          	.byte 0x3
 6361 0ca8 03 29       	.2byte 0x2903
 6362 0caa 7D 0C 00 00 	.4byte 0xc7d
 6363 0cae 0B          	.uleb128 0xb
 6364 0caf 02          	.byte 0x2
 6365 0cb0 02          	.uleb128 0x2
 6366 0cb1 02          	.byte 0x2
 6367 0cb2 07          	.byte 0x7
 6368 0cb3 73 68 6F 72 	.asciz "short unsigned int"
 6368      74 20 75 6E 
 6368      73 69 67 6E 
 6368      65 64 20 69 
 6368      6E 74 00 
 6369 0cc6 0C          	.uleb128 0xc
 6370 0cc7 02          	.byte 0x2
 6371 0cc8 05          	.byte 0x5
MPLAB XC16 ASSEMBLY Listing:   			page 172


 6372 0cc9 CF          	.byte 0xcf
 6373 0cca F4 0F 00 00 	.4byte 0xff4
 6374 0cce 0D          	.uleb128 0xd
 6375 0ccf 45 56 45 4E 	.asciz "EVENT_NONE"
 6375      54 5F 4E 4F 
 6375      4E 45 00 
 6376 0cda 00          	.sleb128 0
 6377 0cdb 0D          	.uleb128 0xd
 6378 0cdc 45 56 45 4E 	.asciz "EVENT_DEVICE_STACK_BASE"
 6378      54 5F 44 45 
 6378      56 49 43 45 
 6378      5F 53 54 41 
 6378      43 4B 5F 42 
 6378      41 53 45 00 
 6379 0cf4 01          	.sleb128 1
 6380 0cf5 0D          	.uleb128 0xd
 6381 0cf6 45 56 45 4E 	.asciz "EVENT_HOST_STACK_BASE"
 6381      54 5F 48 4F 
 6381      53 54 5F 53 
 6381      54 41 43 4B 
 6381      5F 42 41 53 
 6381      45 00 
 6382 0d0c E4 00       	.sleb128 100
 6383 0d0e 0D          	.uleb128 0xd
 6384 0d0f 45 56 45 4E 	.asciz "EVENT_HUB_ATTACH"
 6384      54 5F 48 55 
 6384      42 5F 41 54 
 6384      54 41 43 48 
 6384      00 
 6385 0d20 E5 00       	.sleb128 101
 6386 0d22 0D          	.uleb128 0xd
 6387 0d23 45 56 45 4E 	.asciz "EVENT_STALL"
 6387      54 5F 53 54 
 6387      41 4C 4C 00 
 6388 0d2f E6 00       	.sleb128 102
 6389 0d31 0D          	.uleb128 0xd
 6390 0d32 45 56 45 4E 	.asciz "EVENT_VBUS_SES_REQUEST"
 6390      54 5F 56 42 
 6390      55 53 5F 53 
 6390      45 53 5F 52 
 6390      45 51 55 45 
 6390      53 54 00 
 6391 0d49 E7 00       	.sleb128 103
 6392 0d4b 0D          	.uleb128 0xd
 6393 0d4c 45 56 45 4E 	.asciz "EVENT_VBUS_OVERCURRENT"
 6393      54 5F 56 42 
 6393      55 53 5F 4F 
 6393      56 45 52 43 
 6393      55 52 52 45 
 6393      4E 54 00 
 6394 0d63 E8 00       	.sleb128 104
 6395 0d65 0D          	.uleb128 0xd
 6396 0d66 45 56 45 4E 	.asciz "EVENT_VBUS_REQUEST_POWER"
 6396      54 5F 56 42 
 6396      55 53 5F 52 
 6396      45 51 55 45 
 6396      53 54 5F 50 
MPLAB XC16 ASSEMBLY Listing:   			page 173


 6396      4F 57 45 52 
 6396      00 
 6397 0d7f E9 00       	.sleb128 105
 6398 0d81 0D          	.uleb128 0xd
 6399 0d82 45 56 45 4E 	.asciz "EVENT_VBUS_RELEASE_POWER"
 6399      54 5F 56 42 
 6399      55 53 5F 52 
 6399      45 4C 45 41 
 6399      53 45 5F 50 
 6399      4F 57 45 52 
 6399      00 
 6400 0d9b EA 00       	.sleb128 106
 6401 0d9d 0D          	.uleb128 0xd
 6402 0d9e 45 56 45 4E 	.asciz "EVENT_VBUS_POWER_AVAILABLE"
 6402      54 5F 56 42 
 6402      55 53 5F 50 
 6402      4F 57 45 52 
 6402      5F 41 56 41 
 6402      49 4C 41 42 
 6402      4C 45 00 
 6403 0db9 EB 00       	.sleb128 107
 6404 0dbb 0D          	.uleb128 0xd
 6405 0dbc 45 56 45 4E 	.asciz "EVENT_UNSUPPORTED_DEVICE"
 6405      54 5F 55 4E 
 6405      53 55 50 50 
 6405      4F 52 54 45 
 6405      44 5F 44 45 
 6405      56 49 43 45 
 6405      00 
 6406 0dd5 EC 00       	.sleb128 108
 6407 0dd7 0D          	.uleb128 0xd
 6408 0dd8 45 56 45 4E 	.asciz "EVENT_CANNOT_ENUMERATE"
 6408      54 5F 43 41 
 6408      4E 4E 4F 54 
 6408      5F 45 4E 55 
 6408      4D 45 52 41 
 6408      54 45 00 
 6409 0def ED 00       	.sleb128 109
 6410 0df1 0D          	.uleb128 0xd
 6411 0df2 45 56 45 4E 	.asciz "EVENT_CLIENT_INIT_ERROR"
 6411      54 5F 43 4C 
 6411      49 45 4E 54 
 6411      5F 49 4E 49 
 6411      54 5F 45 52 
 6411      52 4F 52 00 
 6412 0e0a EE 00       	.sleb128 110
 6413 0e0c 0D          	.uleb128 0xd
 6414 0e0d 45 56 45 4E 	.asciz "EVENT_OUT_OF_MEMORY"
 6414      54 5F 4F 55 
 6414      54 5F 4F 46 
 6414      5F 4D 45 4D 
 6414      4F 52 59 00 
 6415 0e21 EF 00       	.sleb128 111
 6416 0e23 0D          	.uleb128 0xd
 6417 0e24 45 56 45 4E 	.asciz "EVENT_UNSPECIFIED_ERROR"
 6417      54 5F 55 4E 
 6417      53 50 45 43 
MPLAB XC16 ASSEMBLY Listing:   			page 174


 6417      49 46 49 45 
 6417      44 5F 45 52 
 6417      52 4F 52 00 
 6418 0e3c F0 00       	.sleb128 112
 6419 0e3e 0D          	.uleb128 0xd
 6420 0e3f 45 56 45 4E 	.asciz "EVENT_DETACH"
 6420      54 5F 44 45 
 6420      54 41 43 48 
 6420      00 
 6421 0e4c F1 00       	.sleb128 113
 6422 0e4e 0D          	.uleb128 0xd
 6423 0e4f 45 56 45 4E 	.asciz "EVENT_TRANSFER"
 6423      54 5F 54 52 
 6423      41 4E 53 46 
 6423      45 52 00 
 6424 0e5e F2 00       	.sleb128 114
 6425 0e60 0D          	.uleb128 0xd
 6426 0e61 45 56 45 4E 	.asciz "EVENT_SOF"
 6426      54 5F 53 4F 
 6426      46 00 
 6427 0e6b F3 00       	.sleb128 115
 6428 0e6d 0D          	.uleb128 0xd
 6429 0e6e 45 56 45 4E 	.asciz "EVENT_RESUME"
 6429      54 5F 52 45 
 6429      53 55 4D 45 
 6429      00 
 6430 0e7b F4 00       	.sleb128 116
 6431 0e7d 0D          	.uleb128 0xd
 6432 0e7e 45 56 45 4E 	.asciz "EVENT_SUSPEND"
 6432      54 5F 53 55 
 6432      53 50 45 4E 
 6432      44 00 
 6433 0e8c F5 00       	.sleb128 117
 6434 0e8e 0D          	.uleb128 0xd
 6435 0e8f 45 56 45 4E 	.asciz "EVENT_RESET"
 6435      54 5F 52 45 
 6435      53 45 54 00 
 6436 0e9b F6 00       	.sleb128 118
 6437 0e9d 0D          	.uleb128 0xd
 6438 0e9e 45 56 45 4E 	.asciz "EVENT_DATA_ISOC_READ"
 6438      54 5F 44 41 
 6438      54 41 5F 49 
 6438      53 4F 43 5F 
 6438      52 45 41 44 
 6438      00 
 6439 0eb3 F7 00       	.sleb128 119
 6440 0eb5 0D          	.uleb128 0xd
 6441 0eb6 45 56 45 4E 	.asciz "EVENT_DATA_ISOC_WRITE"
 6441      54 5F 44 41 
 6441      54 41 5F 49 
 6441      53 4F 43 5F 
 6441      57 52 49 54 
 6441      45 00 
 6442 0ecc F8 00       	.sleb128 120
 6443 0ece 0D          	.uleb128 0xd
 6444 0ecf 45 56 45 4E 	.asciz "EVENT_OVERRIDE_CLIENT_DRIVER_SELECTION"
 6444      54 5F 4F 56 
MPLAB XC16 ASSEMBLY Listing:   			page 175


 6444      45 52 52 49 
 6444      44 45 5F 43 
 6444      4C 49 45 4E 
 6444      54 5F 44 52 
 6444      49 56 45 52 
 6444      5F 53 45 4C 
 6444      45 43 54 49 
 6445 0ef6 F9 00       	.sleb128 121
 6446 0ef8 0D          	.uleb128 0xd
 6447 0ef9 45 56 45 4E 	.asciz "EVENT_1MS"
 6447      54 5F 31 4D 
 6447      53 00 
 6448 0f03 FA 00       	.sleb128 122
 6449 0f05 0D          	.uleb128 0xd
 6450 0f06 45 56 45 4E 	.asciz "EVENT_ALT_INTERFACE"
 6450      54 5F 41 4C 
 6450      54 5F 49 4E 
 6450      54 45 52 46 
 6450      41 43 45 00 
 6451 0f1a FB 00       	.sleb128 123
 6452 0f1c 0D          	.uleb128 0xd
 6453 0f1d 45 56 45 4E 	.asciz "EVENT_HOLD_BEFORE_CONFIGURATION"
 6453      54 5F 48 4F 
 6453      4C 44 5F 42 
 6453      45 46 4F 52 
 6453      45 5F 43 4F 
 6453      4E 46 49 47 
 6453      55 52 41 54 
 6453      49 4F 4E 00 
 6454 0f3d FC 00       	.sleb128 124
 6455 0f3f 0D          	.uleb128 0xd
 6456 0f40 45 56 45 4E 	.asciz "EVENT_GENERIC_BASE"
 6456      54 5F 47 45 
 6456      4E 45 52 49 
 6456      43 5F 42 41 
 6456      53 45 00 
 6457 0f53 90 03       	.sleb128 400
 6458 0f55 0D          	.uleb128 0xd
 6459 0f56 45 56 45 4E 	.asciz "EVENT_MSD_BASE"
 6459      54 5F 4D 53 
 6459      44 5F 42 41 
 6459      53 45 00 
 6460 0f65 F4 03       	.sleb128 500
 6461 0f67 0D          	.uleb128 0xd
 6462 0f68 45 56 45 4E 	.asciz "EVENT_HID_BASE"
 6462      54 5F 48 49 
 6462      44 5F 42 41 
 6462      53 45 00 
 6463 0f77 D8 04       	.sleb128 600
 6464 0f79 0D          	.uleb128 0xd
 6465 0f7a 45 56 45 4E 	.asciz "EVENT_PRINTER_BASE"
 6465      54 5F 50 52 
 6465      49 4E 54 45 
 6465      52 5F 42 41 
 6465      53 45 00 
 6466 0f8d BC 05       	.sleb128 700
 6467 0f8f 0D          	.uleb128 0xd
MPLAB XC16 ASSEMBLY Listing:   			page 176


 6468 0f90 45 56 45 4E 	.asciz "EVENT_CDC_BASE"
 6468      54 5F 43 44 
 6468      43 5F 42 41 
 6468      53 45 00 
 6469 0f9f A0 06       	.sleb128 800
 6470 0fa1 0D          	.uleb128 0xd
 6471 0fa2 45 56 45 4E 	.asciz "EVENT_CHARGER_BASE"
 6471      54 5F 43 48 
 6471      41 52 47 45 
 6471      52 5F 42 41 
 6471      53 45 00 
 6472 0fb5 84 07       	.sleb128 900
 6473 0fb7 0D          	.uleb128 0xd
 6474 0fb8 45 56 45 4E 	.asciz "EVENT_AUDIO_BASE"
 6474      54 5F 41 55 
 6474      44 49 4F 5F 
 6474      42 41 53 45 
 6474      00 
 6475 0fc9 E8 07       	.sleb128 1000
 6476 0fcb 0D          	.uleb128 0xd
 6477 0fcc 45 56 45 4E 	.asciz "EVENT_USER_BASE"
 6477      54 5F 55 53 
 6477      45 52 5F 42 
 6477      41 53 45 00 
 6478 0fdc 90 CE 00    	.sleb128 10000
 6479 0fdf 0D          	.uleb128 0xd
 6480 0fe0 45 56 45 4E 	.asciz "EVENT_BUS_ERROR"
 6480      54 5F 42 55 
 6480      53 5F 45 52 
 6480      52 4F 52 00 
 6481 0ff0 FF FF 01    	.sleb128 32767
 6482 0ff3 00          	.byte 0x0
 6483 0ff4 02          	.uleb128 0x2
 6484 0ff5 01          	.byte 0x1
 6485 0ff6 02          	.byte 0x2
 6486 0ff7 5F 42 6F 6F 	.asciz "_Bool"
 6486      6C 00 
 6487 0ffd 0E          	.uleb128 0xe
 6488 0ffe 5F 55 53 42 	.asciz "_USB_DEVICE_DESCRIPTOR"
 6488      5F 44 45 56 
 6488      49 43 45 5F 
 6488      44 45 53 43 
 6488      52 49 50 54 
 6488      4F 52 00 
 6489 1015 12          	.byte 0x12
 6490 1016 04          	.byte 0x4
 6491 1017 45          	.byte 0x45
 6492 1018 4E 11 00 00 	.4byte 0x114e
 6493 101c 0F          	.uleb128 0xf
 6494 101d 62 4C 65 6E 	.asciz "bLength"
 6494      67 74 68 00 
 6495 1025 04          	.byte 0x4
 6496 1026 47          	.byte 0x47
 6497 1027 E2 00 00 00 	.4byte 0xe2
 6498 102b 02          	.byte 0x2
 6499 102c 23          	.byte 0x23
 6500 102d 00          	.uleb128 0x0
MPLAB XC16 ASSEMBLY Listing:   			page 177


 6501 102e 10          	.uleb128 0x10
 6502 102f 00 00 00 00 	.4byte .LASF0
 6503 1033 04          	.byte 0x4
 6504 1034 48          	.byte 0x48
 6505 1035 E2 00 00 00 	.4byte 0xe2
 6506 1039 02          	.byte 0x2
 6507 103a 23          	.byte 0x23
 6508 103b 01          	.uleb128 0x1
 6509 103c 0F          	.uleb128 0xf
 6510 103d 62 63 64 55 	.asciz "bcdUSB"
 6510      53 42 00 
 6511 1044 04          	.byte 0x4
 6512 1045 49          	.byte 0x49
 6513 1046 02 01 00 00 	.4byte 0x102
 6514 104a 02          	.byte 0x2
 6515 104b 23          	.byte 0x23
 6516 104c 02          	.uleb128 0x2
 6517 104d 0F          	.uleb128 0xf
 6518 104e 62 44 65 76 	.asciz "bDeviceClass"
 6518      69 63 65 43 
 6518      6C 61 73 73 
 6518      00 
 6519 105b 04          	.byte 0x4
 6520 105c 4A          	.byte 0x4a
 6521 105d E2 00 00 00 	.4byte 0xe2
 6522 1061 02          	.byte 0x2
 6523 1062 23          	.byte 0x23
 6524 1063 04          	.uleb128 0x4
 6525 1064 0F          	.uleb128 0xf
 6526 1065 62 44 65 76 	.asciz "bDeviceSubClass"
 6526      69 63 65 53 
 6526      75 62 43 6C 
 6526      61 73 73 00 
 6527 1075 04          	.byte 0x4
 6528 1076 4B          	.byte 0x4b
 6529 1077 E2 00 00 00 	.4byte 0xe2
 6530 107b 02          	.byte 0x2
 6531 107c 23          	.byte 0x23
 6532 107d 05          	.uleb128 0x5
 6533 107e 0F          	.uleb128 0xf
 6534 107f 62 44 65 76 	.asciz "bDeviceProtocol"
 6534      69 63 65 50 
 6534      72 6F 74 6F 
 6534      63 6F 6C 00 
 6535 108f 04          	.byte 0x4
 6536 1090 4C          	.byte 0x4c
 6537 1091 E2 00 00 00 	.4byte 0xe2
 6538 1095 02          	.byte 0x2
 6539 1096 23          	.byte 0x23
 6540 1097 06          	.uleb128 0x6
 6541 1098 0F          	.uleb128 0xf
 6542 1099 62 4D 61 78 	.asciz "bMaxPacketSize0"
 6542      50 61 63 6B 
 6542      65 74 53 69 
 6542      7A 65 30 00 
 6543 10a9 04          	.byte 0x4
 6544 10aa 4D          	.byte 0x4d
MPLAB XC16 ASSEMBLY Listing:   			page 178


 6545 10ab E2 00 00 00 	.4byte 0xe2
 6546 10af 02          	.byte 0x2
 6547 10b0 23          	.byte 0x23
 6548 10b1 07          	.uleb128 0x7
 6549 10b2 0F          	.uleb128 0xf
 6550 10b3 69 64 56 65 	.asciz "idVendor"
 6550      6E 64 6F 72 
 6550      00 
 6551 10bc 04          	.byte 0x4
 6552 10bd 4E          	.byte 0x4e
 6553 10be 02 01 00 00 	.4byte 0x102
 6554 10c2 02          	.byte 0x2
 6555 10c3 23          	.byte 0x23
 6556 10c4 08          	.uleb128 0x8
 6557 10c5 0F          	.uleb128 0xf
 6558 10c6 69 64 50 72 	.asciz "idProduct"
 6558      6F 64 75 63 
 6558      74 00 
 6559 10d0 04          	.byte 0x4
 6560 10d1 4F          	.byte 0x4f
 6561 10d2 02 01 00 00 	.4byte 0x102
 6562 10d6 02          	.byte 0x2
 6563 10d7 23          	.byte 0x23
 6564 10d8 0A          	.uleb128 0xa
 6565 10d9 0F          	.uleb128 0xf
 6566 10da 62 63 64 44 	.asciz "bcdDevice"
 6566      65 76 69 63 
 6566      65 00 
 6567 10e4 04          	.byte 0x4
 6568 10e5 50          	.byte 0x50
 6569 10e6 02 01 00 00 	.4byte 0x102
 6570 10ea 02          	.byte 0x2
 6571 10eb 23          	.byte 0x23
 6572 10ec 0C          	.uleb128 0xc
 6573 10ed 0F          	.uleb128 0xf
 6574 10ee 69 4D 61 6E 	.asciz "iManufacturer"
 6574      75 66 61 63 
 6574      74 75 72 65 
 6574      72 00 
 6575 10fc 04          	.byte 0x4
 6576 10fd 51          	.byte 0x51
 6577 10fe E2 00 00 00 	.4byte 0xe2
 6578 1102 02          	.byte 0x2
 6579 1103 23          	.byte 0x23
 6580 1104 0E          	.uleb128 0xe
 6581 1105 0F          	.uleb128 0xf
 6582 1106 69 50 72 6F 	.asciz "iProduct"
 6582      64 75 63 74 
 6582      00 
 6583 110f 04          	.byte 0x4
 6584 1110 52          	.byte 0x52
 6585 1111 E2 00 00 00 	.4byte 0xe2
 6586 1115 02          	.byte 0x2
 6587 1116 23          	.byte 0x23
 6588 1117 0F          	.uleb128 0xf
 6589 1118 0F          	.uleb128 0xf
 6590 1119 69 53 65 72 	.asciz "iSerialNumber"
MPLAB XC16 ASSEMBLY Listing:   			page 179


 6590      69 61 6C 4E 
 6590      75 6D 62 65 
 6590      72 00 
 6591 1127 04          	.byte 0x4
 6592 1128 53          	.byte 0x53
 6593 1129 E2 00 00 00 	.4byte 0xe2
 6594 112d 02          	.byte 0x2
 6595 112e 23          	.byte 0x23
 6596 112f 10          	.uleb128 0x10
 6597 1130 0F          	.uleb128 0xf
 6598 1131 62 4E 75 6D 	.asciz "bNumConfigurations"
 6598      43 6F 6E 66 
 6598      69 67 75 72 
 6598      61 74 69 6F 
 6598      6E 73 00 
 6599 1144 04          	.byte 0x4
 6600 1145 54          	.byte 0x54
 6601 1146 E2 00 00 00 	.4byte 0xe2
 6602 114a 02          	.byte 0x2
 6603 114b 23          	.byte 0x23
 6604 114c 11          	.uleb128 0x11
 6605 114d 00          	.byte 0x0
 6606 114e 03          	.uleb128 0x3
 6607 114f 55 53 42 5F 	.asciz "USB_DEVICE_DESCRIPTOR"
 6607      44 45 56 49 
 6607      43 45 5F 44 
 6607      45 53 43 52 
 6607      49 50 54 4F 
 6607      52 00 
 6608 1165 04          	.byte 0x4
 6609 1166 55          	.byte 0x55
 6610 1167 FD 0F 00 00 	.4byte 0xffd
 6611 116b 07          	.uleb128 0x7
 6612 116c 08          	.byte 0x8
 6613 116d 04          	.byte 0x4
 6614 116e 0D 01       	.2byte 0x10d
 6615 1170 CF 11 00 00 	.4byte 0x11cf
 6616 1174 11          	.uleb128 0x11
 6617 1175 00 00 00 00 	.4byte .LASF1
 6618 1179 04          	.byte 0x4
 6619 117a 0F 01       	.2byte 0x10f
 6620 117c E2 00 00 00 	.4byte 0xe2
 6621 1180 02          	.byte 0x2
 6622 1181 23          	.byte 0x23
 6623 1182 00          	.uleb128 0x0
 6624 1183 12          	.uleb128 0x12
 6625 1184 62 52 65 71 	.asciz "bRequest"
 6625      75 65 73 74 
 6625      00 
 6626 118d 04          	.byte 0x4
 6627 118e 10 01       	.2byte 0x110
 6628 1190 E2 00 00 00 	.4byte 0xe2
 6629 1194 02          	.byte 0x2
 6630 1195 23          	.byte 0x23
 6631 1196 01          	.uleb128 0x1
 6632 1197 12          	.uleb128 0x12
 6633 1198 77 56 61 6C 	.asciz "wValue"
MPLAB XC16 ASSEMBLY Listing:   			page 180


 6633      75 65 00 
 6634 119f 04          	.byte 0x4
 6635 11a0 11 01       	.2byte 0x111
 6636 11a2 02 01 00 00 	.4byte 0x102
 6637 11a6 02          	.byte 0x2
 6638 11a7 23          	.byte 0x23
 6639 11a8 02          	.uleb128 0x2
 6640 11a9 12          	.uleb128 0x12
 6641 11aa 77 49 6E 64 	.asciz "wIndex"
 6641      65 78 00 
 6642 11b1 04          	.byte 0x4
 6643 11b2 12 01       	.2byte 0x112
 6644 11b4 02 01 00 00 	.4byte 0x102
 6645 11b8 02          	.byte 0x2
 6646 11b9 23          	.byte 0x23
 6647 11ba 04          	.uleb128 0x4
 6648 11bb 12          	.uleb128 0x12
 6649 11bc 77 4C 65 6E 	.asciz "wLength"
 6649      67 74 68 00 
 6650 11c4 04          	.byte 0x4
 6651 11c5 13 01       	.2byte 0x113
 6652 11c7 02 01 00 00 	.4byte 0x102
 6653 11cb 02          	.byte 0x2
 6654 11cc 23          	.byte 0x23
 6655 11cd 06          	.uleb128 0x6
 6656 11ce 00          	.byte 0x0
 6657 11cf 07          	.uleb128 0x7
 6658 11d0 02          	.byte 0x2
 6659 11d1 04          	.byte 0x4
 6660 11d2 1D 01       	.2byte 0x11d
 6661 11d4 F5 11 00 00 	.4byte 0x11f5
 6662 11d8 12          	.uleb128 0x12
 6663 11d9 4C 42 00    	.asciz "LB"
 6664 11dc 04          	.byte 0x4
 6665 11dd 1F 01       	.2byte 0x11f
 6666 11df E2 00 00 00 	.4byte 0xe2
 6667 11e3 02          	.byte 0x2
 6668 11e4 23          	.byte 0x23
 6669 11e5 00          	.uleb128 0x0
 6670 11e6 12          	.uleb128 0x12
 6671 11e7 48 42 00    	.asciz "HB"
 6672 11ea 04          	.byte 0x4
 6673 11eb 20 01       	.2byte 0x120
 6674 11ed E2 00 00 00 	.4byte 0xe2
 6675 11f1 02          	.byte 0x2
 6676 11f2 23          	.byte 0x23
 6677 11f3 01          	.uleb128 0x1
 6678 11f4 00          	.byte 0x0
 6679 11f5 08          	.uleb128 0x8
 6680 11f6 02          	.byte 0x2
 6681 11f7 04          	.byte 0x4
 6682 11f8 19 01       	.2byte 0x119
 6683 11fa 22 12 00 00 	.4byte 0x1222
 6684 11fe 13          	.uleb128 0x13
 6685 11ff 56 61 6C 00 	.asciz "Val"
 6686 1203 04          	.byte 0x4
 6687 1204 1B 01       	.2byte 0x11b
MPLAB XC16 ASSEMBLY Listing:   			page 181


 6688 1206 02 01 00 00 	.4byte 0x102
 6689 120a 13          	.uleb128 0x13
 6690 120b 76 00       	.asciz "v"
 6691 120d 04          	.byte 0x4
 6692 120e 1C 01       	.2byte 0x11c
 6693 1210 22 12 00 00 	.4byte 0x1222
 6694 1214 13          	.uleb128 0x13
 6695 1215 62 79 74 65 	.asciz "byte"
 6695      00 
 6696 121a 04          	.byte 0x4
 6697 121b 21 01       	.2byte 0x121
 6698 121d CF 11 00 00 	.4byte 0x11cf
 6699 1221 00          	.byte 0x0
 6700 1222 14          	.uleb128 0x14
 6701 1223 E2 00 00 00 	.4byte 0xe2
 6702 1227 32 12 00 00 	.4byte 0x1232
 6703 122b 15          	.uleb128 0x15
 6704 122c 12 01 00 00 	.4byte 0x112
 6705 1230 01          	.byte 0x1
 6706 1231 00          	.byte 0x0
 6707 1232 07          	.uleb128 0x7
 6708 1233 02          	.byte 0x2
 6709 1234 04          	.byte 0x4
 6710 1235 28 01       	.2byte 0x128
 6711 1237 58 12 00 00 	.4byte 0x1258
 6712 123b 12          	.uleb128 0x12
 6713 123c 4C 42 00    	.asciz "LB"
 6714 123f 04          	.byte 0x4
 6715 1240 2A 01       	.2byte 0x12a
 6716 1242 E2 00 00 00 	.4byte 0xe2
 6717 1246 02          	.byte 0x2
 6718 1247 23          	.byte 0x23
 6719 1248 00          	.uleb128 0x0
 6720 1249 12          	.uleb128 0x12
 6721 124a 48 42 00    	.asciz "HB"
 6722 124d 04          	.byte 0x4
 6723 124e 2B 01       	.2byte 0x12b
 6724 1250 E2 00 00 00 	.4byte 0xe2
 6725 1254 02          	.byte 0x2
 6726 1255 23          	.byte 0x23
 6727 1256 01          	.uleb128 0x1
 6728 1257 00          	.byte 0x0
 6729 1258 08          	.uleb128 0x8
 6730 1259 02          	.byte 0x2
 6731 125a 04          	.byte 0x4
 6732 125b 24 01       	.2byte 0x124
 6733 125d 85 12 00 00 	.4byte 0x1285
 6734 1261 13          	.uleb128 0x13
 6735 1262 56 61 6C 00 	.asciz "Val"
 6736 1266 04          	.byte 0x4
 6737 1267 26 01       	.2byte 0x126
 6738 1269 02 01 00 00 	.4byte 0x102
 6739 126d 13          	.uleb128 0x13
 6740 126e 76 00       	.asciz "v"
 6741 1270 04          	.byte 0x4
 6742 1271 27 01       	.2byte 0x127
 6743 1273 22 12 00 00 	.4byte 0x1222
MPLAB XC16 ASSEMBLY Listing:   			page 182


 6744 1277 13          	.uleb128 0x13
 6745 1278 62 79 74 65 	.asciz "byte"
 6745      00 
 6746 127d 04          	.byte 0x4
 6747 127e 2C 01       	.2byte 0x12c
 6748 1280 32 12 00 00 	.4byte 0x1232
 6749 1284 00          	.byte 0x0
 6750 1285 07          	.uleb128 0x7
 6751 1286 02          	.byte 0x2
 6752 1287 04          	.byte 0x4
 6753 1288 33 01       	.2byte 0x133
 6754 128a AB 12 00 00 	.4byte 0x12ab
 6755 128e 12          	.uleb128 0x12
 6756 128f 4C 42 00    	.asciz "LB"
 6757 1292 04          	.byte 0x4
 6758 1293 35 01       	.2byte 0x135
 6759 1295 E2 00 00 00 	.4byte 0xe2
 6760 1299 02          	.byte 0x2
 6761 129a 23          	.byte 0x23
 6762 129b 00          	.uleb128 0x0
 6763 129c 12          	.uleb128 0x12
 6764 129d 48 42 00    	.asciz "HB"
 6765 12a0 04          	.byte 0x4
 6766 12a1 36 01       	.2byte 0x136
 6767 12a3 E2 00 00 00 	.4byte 0xe2
 6768 12a7 02          	.byte 0x2
 6769 12a8 23          	.byte 0x23
 6770 12a9 01          	.uleb128 0x1
 6771 12aa 00          	.byte 0x0
 6772 12ab 08          	.uleb128 0x8
 6773 12ac 02          	.byte 0x2
 6774 12ad 04          	.byte 0x4
 6775 12ae 2F 01       	.2byte 0x12f
 6776 12b0 D8 12 00 00 	.4byte 0x12d8
 6777 12b4 13          	.uleb128 0x13
 6778 12b5 56 61 6C 00 	.asciz "Val"
 6779 12b9 04          	.byte 0x4
 6780 12ba 31 01       	.2byte 0x131
 6781 12bc 02 01 00 00 	.4byte 0x102
 6782 12c0 13          	.uleb128 0x13
 6783 12c1 76 00       	.asciz "v"
 6784 12c3 04          	.byte 0x4
 6785 12c4 32 01       	.2byte 0x132
 6786 12c6 22 12 00 00 	.4byte 0x1222
 6787 12ca 13          	.uleb128 0x13
 6788 12cb 62 79 74 65 	.asciz "byte"
 6788      00 
 6789 12d0 04          	.byte 0x4
 6790 12d1 37 01       	.2byte 0x137
 6791 12d3 85 12 00 00 	.4byte 0x1285
 6792 12d7 00          	.byte 0x0
 6793 12d8 07          	.uleb128 0x7
 6794 12d9 08          	.byte 0x8
 6795 12da 04          	.byte 0x4
 6796 12db 15 01       	.2byte 0x115
 6797 12dd 1C 13 00 00 	.4byte 0x131c
 6798 12e1 12          	.uleb128 0x12
MPLAB XC16 ASSEMBLY Listing:   			page 183


 6799 12e2 57 5F 56 61 	.asciz "W_Value"
 6799      6C 75 65 00 
 6800 12ea 04          	.byte 0x4
 6801 12eb 22 01       	.2byte 0x122
 6802 12ed F5 11 00 00 	.4byte 0x11f5
 6803 12f1 02          	.byte 0x2
 6804 12f2 23          	.byte 0x23
 6805 12f3 02          	.uleb128 0x2
 6806 12f4 12          	.uleb128 0x12
 6807 12f5 57 5F 49 6E 	.asciz "W_Index"
 6807      64 65 78 00 
 6808 12fd 04          	.byte 0x4
 6809 12fe 2D 01       	.2byte 0x12d
 6810 1300 58 12 00 00 	.4byte 0x1258
 6811 1304 02          	.byte 0x2
 6812 1305 23          	.byte 0x23
 6813 1306 04          	.uleb128 0x4
 6814 1307 12          	.uleb128 0x12
 6815 1308 57 5F 4C 65 	.asciz "W_Length"
 6815      6E 67 74 68 
 6815      00 
 6816 1311 04          	.byte 0x4
 6817 1312 38 01       	.2byte 0x138
 6818 1314 AB 12 00 00 	.4byte 0x12ab
 6819 1318 02          	.byte 0x2
 6820 1319 23          	.byte 0x23
 6821 131a 06          	.uleb128 0x6
 6822 131b 00          	.byte 0x0
 6823 131c 07          	.uleb128 0x7
 6824 131d 08          	.byte 0x8
 6825 131e 04          	.byte 0x4
 6826 131f 3A 01       	.2byte 0x13a
 6827 1321 82 13 00 00 	.4byte 0x1382
 6828 1325 05          	.uleb128 0x5
 6829 1326 52 65 63 69 	.asciz "Recipient"
 6829      70 69 65 6E 
 6829      74 00 
 6830 1330 04          	.byte 0x4
 6831 1331 3C 01       	.2byte 0x13c
 6832 1333 12 01 00 00 	.4byte 0x112
 6833 1337 02          	.byte 0x2
 6834 1338 05          	.byte 0x5
 6835 1339 0B          	.byte 0xb
 6836 133a 02          	.byte 0x2
 6837 133b 23          	.byte 0x23
 6838 133c 00          	.uleb128 0x0
 6839 133d 05          	.uleb128 0x5
 6840 133e 52 65 71 75 	.asciz "RequestType"
 6840      65 73 74 54 
 6840      79 70 65 00 
 6841 134a 04          	.byte 0x4
 6842 134b 3D 01       	.2byte 0x13d
 6843 134d 12 01 00 00 	.4byte 0x112
 6844 1351 02          	.byte 0x2
 6845 1352 02          	.byte 0x2
 6846 1353 09          	.byte 0x9
 6847 1354 02          	.byte 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 184


 6848 1355 23          	.byte 0x23
 6849 1356 00          	.uleb128 0x0
 6850 1357 05          	.uleb128 0x5
 6851 1358 44 61 74 61 	.asciz "DataDir"
 6851      44 69 72 00 
 6852 1360 04          	.byte 0x4
 6853 1361 3E 01       	.2byte 0x13e
 6854 1363 12 01 00 00 	.4byte 0x112
 6855 1367 02          	.byte 0x2
 6856 1368 01          	.byte 0x1
 6857 1369 08          	.byte 0x8
 6858 136a 02          	.byte 0x2
 6859 136b 23          	.byte 0x23
 6860 136c 00          	.uleb128 0x0
 6861 136d 12          	.uleb128 0x12
 6862 136e 62 46 65 61 	.asciz "bFeature"
 6862      74 75 72 65 
 6862      00 
 6863 1377 04          	.byte 0x4
 6864 1378 40 01       	.2byte 0x140
 6865 137a E2 00 00 00 	.4byte 0xe2
 6866 137e 02          	.byte 0x2
 6867 137f 23          	.byte 0x23
 6868 1380 02          	.uleb128 0x2
 6869 1381 00          	.byte 0x0
 6870 1382 07          	.uleb128 0x7
 6871 1383 01          	.byte 0x1
 6872 1384 04          	.byte 0x4
 6873 1385 4C 01       	.2byte 0x14c
 6874 1387 C9 13 00 00 	.4byte 0x13c9
 6875 138b 05          	.uleb128 0x5
 6876 138c 72 65 63 69 	.asciz "recipient"
 6876      70 69 65 6E 
 6876      74 00 
 6877 1396 04          	.byte 0x4
 6878 1397 4E 01       	.2byte 0x14e
 6879 1399 E2 00 00 00 	.4byte 0xe2
 6880 139d 01          	.byte 0x1
 6881 139e 05          	.byte 0x5
 6882 139f 03          	.byte 0x3
 6883 13a0 02          	.byte 0x2
 6884 13a1 23          	.byte 0x23
 6885 13a2 00          	.uleb128 0x0
 6886 13a3 05          	.uleb128 0x5
 6887 13a4 74 79 70 65 	.asciz "type"
 6887      00 
 6888 13a9 04          	.byte 0x4
 6889 13aa 4F 01       	.2byte 0x14f
 6890 13ac E2 00 00 00 	.4byte 0xe2
 6891 13b0 01          	.byte 0x1
 6892 13b1 02          	.byte 0x2
 6893 13b2 01          	.byte 0x1
 6894 13b3 02          	.byte 0x2
 6895 13b4 23          	.byte 0x23
 6896 13b5 00          	.uleb128 0x0
 6897 13b6 16          	.uleb128 0x16
 6898 13b7 00 00 00 00 	.4byte .LASF2
MPLAB XC16 ASSEMBLY Listing:   			page 185


 6899 13bb 04          	.byte 0x4
 6900 13bc 50 01       	.2byte 0x150
 6901 13be E2 00 00 00 	.4byte 0xe2
 6902 13c2 01          	.byte 0x1
 6903 13c3 01          	.byte 0x1
 6904 13c4 08          	.byte 0x8
 6905 13c5 02          	.byte 0x2
 6906 13c6 23          	.byte 0x23
 6907 13c7 00          	.uleb128 0x0
 6908 13c8 00          	.byte 0x0
 6909 13c9 08          	.uleb128 0x8
 6910 13ca 01          	.byte 0x1
 6911 13cb 04          	.byte 0x4
 6912 13cc 49 01       	.2byte 0x149
 6913 13ce E4 13 00 00 	.4byte 0x13e4
 6914 13d2 17          	.uleb128 0x17
 6915 13d3 00 00 00 00 	.4byte .LASF1
 6916 13d7 04          	.byte 0x4
 6917 13d8 4B 01       	.2byte 0x14b
 6918 13da E2 00 00 00 	.4byte 0xe2
 6919 13de 09          	.uleb128 0x9
 6920 13df 82 13 00 00 	.4byte 0x1382
 6921 13e3 00          	.byte 0x0
 6922 13e4 07          	.uleb128 0x7
 6923 13e5 01          	.byte 0x1
 6924 13e6 04          	.byte 0x4
 6925 13e7 47 01       	.2byte 0x147
 6926 13e9 05 14 00 00 	.4byte 0x1405
 6927 13ed 12          	.uleb128 0x12
 6928 13ee 72 65 71 75 	.asciz "requestInfo"
 6928      65 73 74 49 
 6928      6E 66 6F 00 
 6929 13fa 04          	.byte 0x4
 6930 13fb 52 01       	.2byte 0x152
 6931 13fd C9 13 00 00 	.4byte 0x13c9
 6932 1401 02          	.byte 0x2
 6933 1402 23          	.byte 0x23
 6934 1403 00          	.uleb128 0x0
 6935 1404 00          	.byte 0x0
 6936 1405 07          	.uleb128 0x7
 6937 1406 08          	.byte 0x8
 6938 1407 04          	.byte 0x4
 6939 1408 54 01       	.2byte 0x154
 6940 140a 46 14 00 00 	.4byte 0x1446
 6941 140e 12          	.uleb128 0x12
 6942 140f 62 44 73 63 	.asciz "bDscIndex"
 6942      49 6E 64 65 
 6942      78 00 
 6943 1419 04          	.byte 0x4
 6944 141a 58 01       	.2byte 0x158
 6945 141c E2 00 00 00 	.4byte 0xe2
 6946 1420 02          	.byte 0x2
 6947 1421 23          	.byte 0x23
 6948 1422 02          	.uleb128 0x2
 6949 1423 11          	.uleb128 0x11
 6950 1424 00 00 00 00 	.4byte .LASF0
 6951 1428 04          	.byte 0x4
MPLAB XC16 ASSEMBLY Listing:   			page 186


 6952 1429 59 01       	.2byte 0x159
 6953 142b E2 00 00 00 	.4byte 0xe2
 6954 142f 02          	.byte 0x2
 6955 1430 23          	.byte 0x23
 6956 1431 03          	.uleb128 0x3
 6957 1432 12          	.uleb128 0x12
 6958 1433 77 4C 61 6E 	.asciz "wLangID"
 6958      67 49 44 00 
 6959 143b 04          	.byte 0x4
 6960 143c 5A 01       	.2byte 0x15a
 6961 143e 02 01 00 00 	.4byte 0x102
 6962 1442 02          	.byte 0x2
 6963 1443 23          	.byte 0x23
 6964 1444 04          	.uleb128 0x4
 6965 1445 00          	.byte 0x0
 6966 1446 07          	.uleb128 0x7
 6967 1447 08          	.byte 0x8
 6968 1448 04          	.byte 0x4
 6969 1449 5E 01       	.2byte 0x15e
 6970 144b 77 14 00 00 	.4byte 0x1477
 6971 144f 12          	.uleb128 0x12
 6972 1450 62 44 65 76 	.asciz "bDevADR"
 6972      41 44 52 00 
 6973 1458 04          	.byte 0x4
 6974 1459 62 01       	.2byte 0x162
 6975 145b E2 00 00 00 	.4byte 0xe2
 6976 145f 02          	.byte 0x2
 6977 1460 23          	.byte 0x23
 6978 1461 02          	.uleb128 0x2
 6979 1462 12          	.uleb128 0x12
 6980 1463 62 44 65 76 	.asciz "bDevADRH"
 6980      41 44 52 48 
 6980      00 
 6981 146c 04          	.byte 0x4
 6982 146d 63 01       	.2byte 0x163
 6983 146f E2 00 00 00 	.4byte 0xe2
 6984 1473 02          	.byte 0x2
 6985 1474 23          	.byte 0x23
 6986 1475 03          	.uleb128 0x3
 6987 1476 00          	.byte 0x0
 6988 1477 07          	.uleb128 0x7
 6989 1478 08          	.byte 0x8
 6990 1479 04          	.byte 0x4
 6991 147a 69 01       	.2byte 0x169
 6992 147c B3 14 00 00 	.4byte 0x14b3
 6993 1480 12          	.uleb128 0x12
 6994 1481 62 43 6F 6E 	.asciz "bConfigurationValue"
 6994      66 69 67 75 
 6994      72 61 74 69 
 6994      6F 6E 56 61 
 6994      6C 75 65 00 
 6995 1495 04          	.byte 0x4
 6996 1496 6D 01       	.2byte 0x16d
 6997 1498 E2 00 00 00 	.4byte 0xe2
 6998 149c 02          	.byte 0x2
 6999 149d 23          	.byte 0x23
 7000 149e 02          	.uleb128 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 187


 7001 149f 12          	.uleb128 0x12
 7002 14a0 62 43 66 67 	.asciz "bCfgRSD"
 7002      52 53 44 00 
 7003 14a8 04          	.byte 0x4
 7004 14a9 6E 01       	.2byte 0x16e
 7005 14ab E2 00 00 00 	.4byte 0xe2
 7006 14af 02          	.byte 0x2
 7007 14b0 23          	.byte 0x23
 7008 14b1 03          	.uleb128 0x3
 7009 14b2 00          	.byte 0x0
 7010 14b3 07          	.uleb128 0x7
 7011 14b4 08          	.byte 0x8
 7012 14b5 04          	.byte 0x4
 7013 14b6 74 01       	.2byte 0x174
 7014 14b8 0B 15 00 00 	.4byte 0x150b
 7015 14bc 12          	.uleb128 0x12
 7016 14bd 62 41 6C 74 	.asciz "bAltID"
 7016      49 44 00 
 7017 14c4 04          	.byte 0x4
 7018 14c5 78 01       	.2byte 0x178
 7019 14c7 E2 00 00 00 	.4byte 0xe2
 7020 14cb 02          	.byte 0x2
 7021 14cc 23          	.byte 0x23
 7022 14cd 02          	.uleb128 0x2
 7023 14ce 12          	.uleb128 0x12
 7024 14cf 62 41 6C 74 	.asciz "bAltID_H"
 7024      49 44 5F 48 
 7024      00 
 7025 14d8 04          	.byte 0x4
 7026 14d9 79 01       	.2byte 0x179
 7027 14db E2 00 00 00 	.4byte 0xe2
 7028 14df 02          	.byte 0x2
 7029 14e0 23          	.byte 0x23
 7030 14e1 03          	.uleb128 0x3
 7031 14e2 12          	.uleb128 0x12
 7032 14e3 62 49 6E 74 	.asciz "bIntfID"
 7032      66 49 44 00 
 7033 14eb 04          	.byte 0x4
 7034 14ec 7A 01       	.2byte 0x17a
 7035 14ee E2 00 00 00 	.4byte 0xe2
 7036 14f2 02          	.byte 0x2
 7037 14f3 23          	.byte 0x23
 7038 14f4 04          	.uleb128 0x4
 7039 14f5 12          	.uleb128 0x12
 7040 14f6 62 49 6E 74 	.asciz "bIntfID_H"
 7040      66 49 44 5F 
 7040      48 00 
 7041 1500 04          	.byte 0x4
 7042 1501 7B 01       	.2byte 0x17b
 7043 1503 E2 00 00 00 	.4byte 0xe2
 7044 1507 02          	.byte 0x2
 7045 1508 23          	.byte 0x23
 7046 1509 05          	.uleb128 0x5
 7047 150a 00          	.byte 0x0
 7048 150b 07          	.uleb128 0x7
 7049 150c 08          	.byte 0x8
 7050 150d 04          	.byte 0x4
MPLAB XC16 ASSEMBLY Listing:   			page 188


 7051 150e 7F 01       	.2byte 0x17f
 7052 1510 39 15 00 00 	.4byte 0x1539
 7053 1514 12          	.uleb128 0x12
 7054 1515 62 45 50 49 	.asciz "bEPID"
 7054      44 00 
 7055 151b 04          	.byte 0x4
 7056 151c 85 01       	.2byte 0x185
 7057 151e E2 00 00 00 	.4byte 0xe2
 7058 1522 02          	.byte 0x2
 7059 1523 23          	.byte 0x23
 7060 1524 04          	.uleb128 0x4
 7061 1525 12          	.uleb128 0x12
 7062 1526 62 45 50 49 	.asciz "bEPID_H"
 7062      44 5F 48 00 
 7063 152e 04          	.byte 0x4
 7064 152f 86 01       	.2byte 0x186
 7065 1531 E2 00 00 00 	.4byte 0xe2
 7066 1535 02          	.byte 0x2
 7067 1536 23          	.byte 0x23
 7068 1537 05          	.uleb128 0x5
 7069 1538 00          	.byte 0x0
 7070 1539 07          	.uleb128 0x7
 7071 153a 08          	.byte 0x8
 7072 153b 04          	.byte 0x4
 7073 153c 8A 01       	.2byte 0x18a
 7074 153e 6B 15 00 00 	.4byte 0x156b
 7075 1542 05          	.uleb128 0x5
 7076 1543 45 50 4E 75 	.asciz "EPNum"
 7076      6D 00 
 7077 1549 04          	.byte 0x4
 7078 154a 90 01       	.2byte 0x190
 7079 154c 12 01 00 00 	.4byte 0x112
 7080 1550 02          	.byte 0x2
 7081 1551 04          	.byte 0x4
 7082 1552 0C          	.byte 0xc
 7083 1553 02          	.byte 0x2
 7084 1554 23          	.byte 0x23
 7085 1555 04          	.uleb128 0x4
 7086 1556 05          	.uleb128 0x5
 7087 1557 45 50 44 69 	.asciz "EPDir"
 7087      72 00 
 7088 155d 04          	.byte 0x4
 7089 155e 92 01       	.2byte 0x192
 7090 1560 12 01 00 00 	.4byte 0x112
 7091 1564 02          	.byte 0x2
 7092 1565 01          	.byte 0x1
 7093 1566 08          	.byte 0x8
 7094 1567 02          	.byte 0x2
 7095 1568 23          	.byte 0x23
 7096 1569 04          	.uleb128 0x4
 7097 156a 00          	.byte 0x0
 7098 156b 08          	.uleb128 0x8
 7099 156c 08          	.byte 0x8
 7100 156d 04          	.byte 0x4
 7101 156e 0A 01       	.2byte 0x10a
 7102 1570 A7 15 00 00 	.4byte 0x15a7
 7103 1574 09          	.uleb128 0x9
MPLAB XC16 ASSEMBLY Listing:   			page 189


 7104 1575 6B 11 00 00 	.4byte 0x116b
 7105 1579 09          	.uleb128 0x9
 7106 157a D8 12 00 00 	.4byte 0x12d8
 7107 157e 09          	.uleb128 0x9
 7108 157f 1C 13 00 00 	.4byte 0x131c
 7109 1583 09          	.uleb128 0x9
 7110 1584 E4 13 00 00 	.4byte 0x13e4
 7111 1588 09          	.uleb128 0x9
 7112 1589 05 14 00 00 	.4byte 0x1405
 7113 158d 09          	.uleb128 0x9
 7114 158e 46 14 00 00 	.4byte 0x1446
 7115 1592 09          	.uleb128 0x9
 7116 1593 77 14 00 00 	.4byte 0x1477
 7117 1597 09          	.uleb128 0x9
 7118 1598 B3 14 00 00 	.4byte 0x14b3
 7119 159c 09          	.uleb128 0x9
 7120 159d 0B 15 00 00 	.4byte 0x150b
 7121 15a1 09          	.uleb128 0x9
 7122 15a2 39 15 00 00 	.4byte 0x1539
 7123 15a6 00          	.byte 0x0
 7124 15a7 06          	.uleb128 0x6
 7125 15a8 43 54 52 4C 	.asciz "CTRL_TRF_SETUP"
 7125      5F 54 52 46 
 7125      5F 53 45 54 
 7125      55 50 00 
 7126 15b7 04          	.byte 0x4
 7127 15b8 9A 01       	.2byte 0x19a
 7128 15ba 6B 15 00 00 	.4byte 0x156b
 7129 15be 0C          	.uleb128 0xc
 7130 15bf 02          	.byte 0x2
 7131 15c0 06          	.byte 0x6
 7132 15c1 4A          	.byte 0x4a
 7133 15c2 40 16 00 00 	.4byte 0x1640
 7134 15c6 0D          	.uleb128 0xd
 7135 15c7 44 45 54 41 	.asciz "DETACHED_STATE"
 7135      43 48 45 44 
 7135      5F 53 54 41 
 7135      54 45 00 
 7136 15d6 00          	.sleb128 0
 7137 15d7 0D          	.uleb128 0xd
 7138 15d8 41 54 54 41 	.asciz "ATTACHED_STATE"
 7138      43 48 45 44 
 7138      5F 53 54 41 
 7138      54 45 00 
 7139 15e7 01          	.sleb128 1
 7140 15e8 0D          	.uleb128 0xd
 7141 15e9 50 4F 57 45 	.asciz "POWERED_STATE"
 7141      52 45 44 5F 
 7141      53 54 41 54 
 7141      45 00 
 7142 15f7 02          	.sleb128 2
 7143 15f8 0D          	.uleb128 0xd
 7144 15f9 44 45 46 41 	.asciz "DEFAULT_STATE"
 7144      55 4C 54 5F 
 7144      53 54 41 54 
 7144      45 00 
 7145 1607 04          	.sleb128 4
MPLAB XC16 ASSEMBLY Listing:   			page 190


 7146 1608 0D          	.uleb128 0xd
 7147 1609 41 44 52 5F 	.asciz "ADR_PENDING_STATE"
 7147      50 45 4E 44 
 7147      49 4E 47 5F 
 7147      53 54 41 54 
 7147      45 00 
 7148 161b 08          	.sleb128 8
 7149 161c 0D          	.uleb128 0xd
 7150 161d 41 44 44 52 	.asciz "ADDRESS_STATE"
 7150      45 53 53 5F 
 7150      53 54 41 54 
 7150      45 00 
 7151 162b 10          	.sleb128 16
 7152 162c 0D          	.uleb128 0xd
 7153 162d 43 4F 4E 46 	.asciz "CONFIGURED_STATE"
 7153      49 47 55 52 
 7153      45 44 5F 53 
 7153      54 41 54 45 
 7153      00 
 7154 163e 20          	.sleb128 32
 7155 163f 00          	.byte 0x0
 7156 1640 03          	.uleb128 0x3
 7157 1641 55 53 42 5F 	.asciz "USB_DEVICE_STATE"
 7157      44 45 56 49 
 7157      43 45 5F 53 
 7157      54 41 54 45 
 7157      00 
 7158 1652 06          	.byte 0x6
 7159 1653 6E          	.byte 0x6e
 7160 1654 BE 15 00 00 	.4byte 0x15be
 7161 1658 0C          	.uleb128 0xc
 7162 1659 02          	.byte 0x2
 7163 165a 06          	.byte 0x6
 7164 165b 73          	.byte 0x73
 7165 165c CA 16 00 00 	.4byte 0x16ca
 7166 1660 0D          	.uleb128 0xd
 7167 1661 45 56 45 4E 	.asciz "EVENT_CONFIGURED"
 7167      54 5F 43 4F 
 7167      4E 46 49 47 
 7167      55 52 45 44 
 7167      00 
 7168 1672 01          	.sleb128 1
 7169 1673 0D          	.uleb128 0xd
 7170 1674 45 56 45 4E 	.asciz "EVENT_SET_DESCRIPTOR"
 7170      54 5F 53 45 
 7170      54 5F 44 45 
 7170      53 43 52 49 
 7170      50 54 4F 52 
 7170      00 
 7171 1689 02          	.sleb128 2
 7172 168a 0D          	.uleb128 0xd
 7173 168b 45 56 45 4E 	.asciz "EVENT_EP0_REQUEST"
 7173      54 5F 45 50 
 7173      30 5F 52 45 
 7173      51 55 45 53 
 7173      54 00 
 7174 169d 03          	.sleb128 3
MPLAB XC16 ASSEMBLY Listing:   			page 191


 7175 169e 0D          	.uleb128 0xd
 7176 169f 45 56 45 4E 	.asciz "EVENT_ATTACH"
 7176      54 5F 41 54 
 7176      54 41 43 48 
 7176      00 
 7177 16ac 04          	.sleb128 4
 7178 16ad 0D          	.uleb128 0xd
 7179 16ae 45 56 45 4E 	.asciz "EVENT_TRANSFER_TERMINATED"
 7179      54 5F 54 52 
 7179      41 4E 53 46 
 7179      45 52 5F 54 
 7179      45 52 4D 49 
 7179      4E 41 54 45 
 7179      44 00 
 7180 16c8 05          	.sleb128 5
 7181 16c9 00          	.byte 0x0
 7182 16ca 07          	.uleb128 0x7
 7183 16cb 02          	.byte 0x2
 7184 16cc 06          	.byte 0x6
 7185 16cd BC 07       	.2byte 0x7bc
 7186 16cf F0 16 00 00 	.4byte 0x16f0
 7187 16d3 12          	.uleb128 0x12
 7188 16d4 4C 42 00    	.asciz "LB"
 7189 16d7 06          	.byte 0x6
 7190 16d8 BE 07       	.2byte 0x7be
 7191 16da E2 00 00 00 	.4byte 0xe2
 7192 16de 02          	.byte 0x2
 7193 16df 23          	.byte 0x23
 7194 16e0 00          	.uleb128 0x0
 7195 16e1 12          	.uleb128 0x12
 7196 16e2 48 42 00    	.asciz "HB"
 7197 16e5 06          	.byte 0x6
 7198 16e6 BF 07       	.2byte 0x7bf
 7199 16e8 E2 00 00 00 	.4byte 0xe2
 7200 16ec 02          	.byte 0x2
 7201 16ed 23          	.byte 0x23
 7202 16ee 01          	.uleb128 0x1
 7203 16ef 00          	.byte 0x0
 7204 16f0 08          	.uleb128 0x8
 7205 16f1 02          	.byte 0x2
 7206 16f2 06          	.byte 0x6
 7207 16f3 B8 07       	.2byte 0x7b8
 7208 16f5 1D 17 00 00 	.4byte 0x171d
 7209 16f9 13          	.uleb128 0x13
 7210 16fa 56 61 6C 00 	.asciz "Val"
 7211 16fe 06          	.byte 0x6
 7212 16ff BA 07       	.2byte 0x7ba
 7213 1701 02 01 00 00 	.4byte 0x102
 7214 1705 13          	.uleb128 0x13
 7215 1706 76 00       	.asciz "v"
 7216 1708 06          	.byte 0x6
 7217 1709 BB 07       	.2byte 0x7bb
 7218 170b 22 12 00 00 	.4byte 0x1222
 7219 170f 13          	.uleb128 0x13
 7220 1710 62 79 74 65 	.asciz "byte"
 7220      00 
 7221 1715 06          	.byte 0x6
MPLAB XC16 ASSEMBLY Listing:   			page 192


 7222 1716 C0 07       	.2byte 0x7c0
 7223 1718 CA 16 00 00 	.4byte 0x16ca
 7224 171c 00          	.byte 0x0
 7225 171d 06          	.uleb128 0x6
 7226 171e 75 69 6E 74 	.asciz "uint16_t_VAL"
 7226      31 36 5F 74 
 7226      5F 56 41 4C 
 7226      00 
 7227 172b 06          	.byte 0x6
 7228 172c C1 07       	.2byte 0x7c1
 7229 172e F0 16 00 00 	.4byte 0x16f0
 7230 1732 08          	.uleb128 0x8
 7231 1733 02          	.byte 0x2
 7232 1734 06          	.byte 0x6
 7233 1735 C8 07       	.2byte 0x7c8
 7234 1737 70 17 00 00 	.4byte 0x1770
 7235 173b 13          	.uleb128 0x13
 7236 173c 62 52 61 6D 	.asciz "bRam"
 7236      00 
 7237 1741 06          	.byte 0x6
 7238 1742 CC 07       	.2byte 0x7cc
 7239 1744 70 17 00 00 	.4byte 0x1770
 7240 1748 13          	.uleb128 0x13
 7241 1749 62 52 6F 6D 	.asciz "bRom"
 7241      00 
 7242 174e 06          	.byte 0x6
 7243 174f CD 07       	.2byte 0x7cd
 7244 1751 76 17 00 00 	.4byte 0x1776
 7245 1755 13          	.uleb128 0x13
 7246 1756 77 52 61 6D 	.asciz "wRam"
 7246      00 
 7247 175b 06          	.byte 0x6
 7248 175c CE 07       	.2byte 0x7ce
 7249 175e 81 17 00 00 	.4byte 0x1781
 7250 1762 13          	.uleb128 0x13
 7251 1763 77 52 6F 6D 	.asciz "wRom"
 7251      00 
 7252 1768 06          	.byte 0x6
 7253 1769 CF 07       	.2byte 0x7cf
 7254 176b 87 17 00 00 	.4byte 0x1787
 7255 176f 00          	.byte 0x0
 7256 1770 18          	.uleb128 0x18
 7257 1771 02          	.byte 0x2
 7258 1772 E2 00 00 00 	.4byte 0xe2
 7259 1776 18          	.uleb128 0x18
 7260 1777 02          	.byte 0x2
 7261 1778 7C 17 00 00 	.4byte 0x177c
 7262 177c 19          	.uleb128 0x19
 7263 177d E2 00 00 00 	.4byte 0xe2
 7264 1781 18          	.uleb128 0x18
 7265 1782 02          	.byte 0x2
 7266 1783 02 01 00 00 	.4byte 0x102
 7267 1787 18          	.uleb128 0x18
 7268 1788 02          	.byte 0x2
 7269 1789 8D 17 00 00 	.4byte 0x178d
 7270 178d 19          	.uleb128 0x19
 7271 178e 02 01 00 00 	.4byte 0x102
MPLAB XC16 ASSEMBLY Listing:   			page 193


 7272 1792 07          	.uleb128 0x7
 7273 1793 01          	.byte 0x1
 7274 1794 06          	.byte 0x6
 7275 1795 D3 07       	.2byte 0x7d3
 7276 1797 F6 17 00 00 	.4byte 0x17f6
 7277 179b 05          	.uleb128 0x5
 7278 179c 63 74 72 6C 	.asciz "ctrl_trf_mem"
 7278      5F 74 72 66 
 7278      5F 6D 65 6D 
 7278      00 
 7279 17a9 06          	.byte 0x6
 7280 17aa D6 07       	.2byte 0x7d6
 7281 17ac E2 00 00 00 	.4byte 0xe2
 7282 17b0 01          	.byte 0x1
 7283 17b1 01          	.byte 0x1
 7284 17b2 07          	.byte 0x7
 7285 17b3 02          	.byte 0x2
 7286 17b4 23          	.byte 0x23
 7287 17b5 00          	.uleb128 0x0
 7288 17b6 16          	.uleb128 0x16
 7289 17b7 00 00 00 00 	.4byte .LASF3
 7290 17bb 06          	.byte 0x6
 7291 17bc D7 07       	.2byte 0x7d7
 7292 17be E2 00 00 00 	.4byte 0xe2
 7293 17c2 01          	.byte 0x1
 7294 17c3 05          	.byte 0x5
 7295 17c4 02          	.byte 0x2
 7296 17c5 02          	.byte 0x2
 7297 17c6 23          	.byte 0x23
 7298 17c7 00          	.uleb128 0x0
 7299 17c8 05          	.uleb128 0x5
 7300 17c9 69 6E 63 6C 	.asciz "includeZero"
 7300      75 64 65 5A 
 7300      65 72 6F 00 
 7301 17d5 06          	.byte 0x6
 7302 17d6 DA 07       	.2byte 0x7da
 7303 17d8 E2 00 00 00 	.4byte 0xe2
 7304 17dc 01          	.byte 0x1
 7305 17dd 01          	.byte 0x1
 7306 17de 01          	.byte 0x1
 7307 17df 02          	.byte 0x2
 7308 17e0 23          	.byte 0x23
 7309 17e1 00          	.uleb128 0x0
 7310 17e2 05          	.uleb128 0x5
 7311 17e3 62 75 73 79 	.asciz "busy"
 7311      00 
 7312 17e8 06          	.byte 0x6
 7313 17e9 DC 07       	.2byte 0x7dc
 7314 17eb E2 00 00 00 	.4byte 0xe2
 7315 17ef 01          	.byte 0x1
 7316 17f0 01          	.byte 0x1
 7317 17f1 08          	.byte 0x8
 7318 17f2 02          	.byte 0x2
 7319 17f3 23          	.byte 0x23
 7320 17f4 00          	.uleb128 0x0
 7321 17f5 00          	.byte 0x0
 7322 17f6 08          	.uleb128 0x8
MPLAB XC16 ASSEMBLY Listing:   			page 194


 7323 17f7 01          	.byte 0x1
 7324 17f8 06          	.byte 0x6
 7325 17f9 D1 07       	.2byte 0x7d1
 7326 17fb 19 18 00 00 	.4byte 0x1819
 7327 17ff 13          	.uleb128 0x13
 7328 1800 62 69 74 73 	.asciz "bits"
 7328      00 
 7329 1805 06          	.byte 0x6
 7330 1806 DD 07       	.2byte 0x7dd
 7331 1808 92 17 00 00 	.4byte 0x1792
 7332 180c 13          	.uleb128 0x13
 7333 180d 56 61 6C 00 	.asciz "Val"
 7334 1811 06          	.byte 0x6
 7335 1812 DE 07       	.2byte 0x7de
 7336 1814 E2 00 00 00 	.4byte 0xe2
 7337 1818 00          	.byte 0x0
 7338 1819 07          	.uleb128 0x7
 7339 181a 06          	.byte 0x6
 7340 181b 06          	.byte 0x6
 7341 181c C6 07       	.2byte 0x7c6
 7342 181e 55 18 00 00 	.4byte 0x1855
 7343 1822 12          	.uleb128 0x12
 7344 1823 70 53 72 63 	.asciz "pSrc"
 7344      00 
 7345 1828 06          	.byte 0x6
 7346 1829 D0 07       	.2byte 0x7d0
 7347 182b 32 17 00 00 	.4byte 0x1732
 7348 182f 02          	.byte 0x2
 7349 1830 23          	.byte 0x23
 7350 1831 00          	.uleb128 0x0
 7351 1832 12          	.uleb128 0x12
 7352 1833 69 6E 66 6F 	.asciz "info"
 7352      00 
 7353 1838 06          	.byte 0x6
 7354 1839 DF 07       	.2byte 0x7df
 7355 183b F6 17 00 00 	.4byte 0x17f6
 7356 183f 02          	.byte 0x2
 7357 1840 23          	.byte 0x23
 7358 1841 02          	.uleb128 0x2
 7359 1842 12          	.uleb128 0x12
 7360 1843 77 43 6F 75 	.asciz "wCount"
 7360      6E 74 00 
 7361 184a 06          	.byte 0x6
 7362 184b E0 07       	.2byte 0x7e0
 7363 184d 1D 17 00 00 	.4byte 0x171d
 7364 1851 02          	.byte 0x2
 7365 1852 23          	.byte 0x23
 7366 1853 04          	.uleb128 0x4
 7367 1854 00          	.byte 0x0
 7368 1855 06          	.uleb128 0x6
 7369 1856 49 4E 5F 50 	.asciz "IN_PIPE"
 7369      49 50 45 00 
 7370 185e 06          	.byte 0x6
 7371 185f E1 07       	.2byte 0x7e1
 7372 1861 19 18 00 00 	.4byte 0x1819
 7373 1865 08          	.uleb128 0x8
 7374 1866 02          	.byte 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 195


 7375 1867 06          	.byte 0x6
 7376 1868 E7 07       	.2byte 0x7e7
 7377 186a 89 18 00 00 	.4byte 0x1889
 7378 186e 13          	.uleb128 0x13
 7379 186f 62 52 61 6D 	.asciz "bRam"
 7379      00 
 7380 1874 06          	.byte 0x6
 7381 1875 EB 07       	.2byte 0x7eb
 7382 1877 70 17 00 00 	.4byte 0x1770
 7383 187b 13          	.uleb128 0x13
 7384 187c 77 52 61 6D 	.asciz "wRam"
 7384      00 
 7385 1881 06          	.byte 0x6
 7386 1882 EC 07       	.2byte 0x7ec
 7387 1884 81 17 00 00 	.4byte 0x1781
 7388 1888 00          	.byte 0x0
 7389 1889 07          	.uleb128 0x7
 7390 188a 01          	.byte 0x1
 7391 188b 06          	.byte 0x6
 7392 188c F0 07       	.2byte 0x7f0
 7393 188e B8 18 00 00 	.4byte 0x18b8
 7394 1892 16          	.uleb128 0x16
 7395 1893 00 00 00 00 	.4byte .LASF3
 7396 1897 06          	.byte 0x6
 7397 1898 F2 07       	.2byte 0x7f2
 7398 189a E2 00 00 00 	.4byte 0xe2
 7399 189e 01          	.byte 0x1
 7400 189f 07          	.byte 0x7
 7401 18a0 01          	.byte 0x1
 7402 18a1 02          	.byte 0x2
 7403 18a2 23          	.byte 0x23
 7404 18a3 00          	.uleb128 0x0
 7405 18a4 05          	.uleb128 0x5
 7406 18a5 62 75 73 79 	.asciz "busy"
 7406      00 
 7407 18aa 06          	.byte 0x6
 7408 18ab F4 07       	.2byte 0x7f4
 7409 18ad E2 00 00 00 	.4byte 0xe2
 7410 18b1 01          	.byte 0x1
 7411 18b2 01          	.byte 0x1
 7412 18b3 08          	.byte 0x8
 7413 18b4 02          	.byte 0x2
 7414 18b5 23          	.byte 0x23
 7415 18b6 00          	.uleb128 0x0
 7416 18b7 00          	.byte 0x0
 7417 18b8 08          	.uleb128 0x8
 7418 18b9 01          	.byte 0x1
 7419 18ba 06          	.byte 0x6
 7420 18bb EE 07       	.2byte 0x7ee
 7421 18bd DB 18 00 00 	.4byte 0x18db
 7422 18c1 13          	.uleb128 0x13
 7423 18c2 62 69 74 73 	.asciz "bits"
 7423      00 
 7424 18c7 06          	.byte 0x6
 7425 18c8 F5 07       	.2byte 0x7f5
 7426 18ca 89 18 00 00 	.4byte 0x1889
 7427 18ce 13          	.uleb128 0x13
MPLAB XC16 ASSEMBLY Listing:   			page 196


 7428 18cf 56 61 6C 00 	.asciz "Val"
 7429 18d3 06          	.byte 0x6
 7430 18d4 F6 07       	.2byte 0x7f6
 7431 18d6 E2 00 00 00 	.4byte 0xe2
 7432 18da 00          	.byte 0x0
 7433 18db 07          	.uleb128 0x7
 7434 18dc 07          	.byte 0x7
 7435 18dd 06          	.byte 0x6
 7436 18de E5 07       	.2byte 0x7e5
 7437 18e0 28 19 00 00 	.4byte 0x1928
 7438 18e4 12          	.uleb128 0x12
 7439 18e5 70 44 73 74 	.asciz "pDst"
 7439      00 
 7440 18ea 06          	.byte 0x6
 7441 18eb ED 07       	.2byte 0x7ed
 7442 18ed 65 18 00 00 	.4byte 0x1865
 7443 18f1 02          	.byte 0x2
 7444 18f2 23          	.byte 0x23
 7445 18f3 00          	.uleb128 0x0
 7446 18f4 12          	.uleb128 0x12
 7447 18f5 69 6E 66 6F 	.asciz "info"
 7447      00 
 7448 18fa 06          	.byte 0x6
 7449 18fb F7 07       	.2byte 0x7f7
 7450 18fd B8 18 00 00 	.4byte 0x18b8
 7451 1901 02          	.byte 0x2
 7452 1902 23          	.byte 0x23
 7453 1903 02          	.uleb128 0x2
 7454 1904 12          	.uleb128 0x12
 7455 1905 77 43 6F 75 	.asciz "wCount"
 7455      6E 74 00 
 7456 190c 06          	.byte 0x6
 7457 190d F8 07       	.2byte 0x7f8
 7458 190f 1D 17 00 00 	.4byte 0x171d
 7459 1913 02          	.byte 0x2
 7460 1914 23          	.byte 0x23
 7461 1915 03          	.uleb128 0x3
 7462 1916 12          	.uleb128 0x12
 7463 1917 70 46 75 6E 	.asciz "pFunc"
 7463      63 00 
 7464 191d 06          	.byte 0x6
 7465 191e F9 07       	.2byte 0x7f9
 7466 1920 2A 19 00 00 	.4byte 0x192a
 7467 1924 02          	.byte 0x2
 7468 1925 23          	.byte 0x23
 7469 1926 05          	.uleb128 0x5
 7470 1927 00          	.byte 0x0
 7471 1928 1A          	.uleb128 0x1a
 7472 1929 01          	.byte 0x1
 7473 192a 18          	.uleb128 0x18
 7474 192b 02          	.byte 0x2
 7475 192c 28 19 00 00 	.4byte 0x1928
 7476 1930 06          	.uleb128 0x6
 7477 1931 4F 55 54 5F 	.asciz "OUT_PIPE"
 7477      50 49 50 45 
 7477      00 
 7478 193a 06          	.byte 0x6
MPLAB XC16 ASSEMBLY Listing:   			page 197


 7479 193b FA 07       	.2byte 0x7fa
 7480 193d DB 18 00 00 	.4byte 0x18db
 7481 1941 1B          	.uleb128 0x1b
 7482 1942 01          	.byte 0x1
 7483 1943 07          	.byte 0x7
 7484 1944 D2          	.byte 0xd2
 7485 1945 94 19 00 00 	.4byte 0x1994
 7486 1949 1C          	.uleb128 0x1c
 7487 194a 42 53 54 41 	.asciz "BSTALL"
 7487      4C 4C 00 
 7488 1951 07          	.byte 0x7
 7489 1952 D4          	.byte 0xd4
 7490 1953 12 01 00 00 	.4byte 0x112
 7491 1957 02          	.byte 0x2
 7492 1958 01          	.byte 0x1
 7493 1959 0D          	.byte 0xd
 7494 195a 02          	.byte 0x2
 7495 195b 23          	.byte 0x23
 7496 195c 00          	.uleb128 0x0
 7497 195d 1C          	.uleb128 0x1c
 7498 195e 44 54 53 45 	.asciz "DTSEN"
 7498      4E 00 
 7499 1964 07          	.byte 0x7
 7500 1965 D5          	.byte 0xd5
 7501 1966 12 01 00 00 	.4byte 0x112
 7502 196a 02          	.byte 0x2
 7503 196b 01          	.byte 0x1
 7504 196c 0C          	.byte 0xc
 7505 196d 02          	.byte 0x2
 7506 196e 23          	.byte 0x23
 7507 196f 00          	.uleb128 0x0
 7508 1970 1C          	.uleb128 0x1c
 7509 1971 44 54 53 00 	.asciz "DTS"
 7510 1975 07          	.byte 0x7
 7511 1976 D7          	.byte 0xd7
 7512 1977 12 01 00 00 	.4byte 0x112
 7513 197b 02          	.byte 0x2
 7514 197c 01          	.byte 0x1
 7515 197d 09          	.byte 0x9
 7516 197e 02          	.byte 0x2
 7517 197f 23          	.byte 0x23
 7518 1980 00          	.uleb128 0x0
 7519 1981 1C          	.uleb128 0x1c
 7520 1982 55 4F 57 4E 	.asciz "UOWN"
 7520      00 
 7521 1987 07          	.byte 0x7
 7522 1988 D8          	.byte 0xd8
 7523 1989 12 01 00 00 	.4byte 0x112
 7524 198d 02          	.byte 0x2
 7525 198e 01          	.byte 0x1
 7526 198f 08          	.byte 0x8
 7527 1990 02          	.byte 0x2
 7528 1991 23          	.byte 0x23
 7529 1992 00          	.uleb128 0x0
 7530 1993 00          	.byte 0x0
 7531 1994 1B          	.uleb128 0x1b
 7532 1995 01          	.byte 0x1
MPLAB XC16 ASSEMBLY Listing:   			page 198


 7533 1996 07          	.byte 0x7
 7534 1997 DA          	.byte 0xda
 7535 1998 E5 19 00 00 	.4byte 0x19e5
 7536 199c 1C          	.uleb128 0x1c
 7537 199d 50 49 44 30 	.asciz "PID0"
 7537      00 
 7538 19a2 07          	.byte 0x7
 7539 19a3 DC          	.byte 0xdc
 7540 19a4 12 01 00 00 	.4byte 0x112
 7541 19a8 02          	.byte 0x2
 7542 19a9 01          	.byte 0x1
 7543 19aa 0D          	.byte 0xd
 7544 19ab 02          	.byte 0x2
 7545 19ac 23          	.byte 0x23
 7546 19ad 00          	.uleb128 0x0
 7547 19ae 1C          	.uleb128 0x1c
 7548 19af 50 49 44 31 	.asciz "PID1"
 7548      00 
 7549 19b4 07          	.byte 0x7
 7550 19b5 DD          	.byte 0xdd
 7551 19b6 12 01 00 00 	.4byte 0x112
 7552 19ba 02          	.byte 0x2
 7553 19bb 01          	.byte 0x1
 7554 19bc 0C          	.byte 0xc
 7555 19bd 02          	.byte 0x2
 7556 19be 23          	.byte 0x23
 7557 19bf 00          	.uleb128 0x0
 7558 19c0 1C          	.uleb128 0x1c
 7559 19c1 50 49 44 32 	.asciz "PID2"
 7559      00 
 7560 19c6 07          	.byte 0x7
 7561 19c7 DE          	.byte 0xde
 7562 19c8 12 01 00 00 	.4byte 0x112
 7563 19cc 02          	.byte 0x2
 7564 19cd 01          	.byte 0x1
 7565 19ce 0B          	.byte 0xb
 7566 19cf 02          	.byte 0x2
 7567 19d0 23          	.byte 0x23
 7568 19d1 00          	.uleb128 0x0
 7569 19d2 1C          	.uleb128 0x1c
 7570 19d3 50 49 44 33 	.asciz "PID3"
 7570      00 
 7571 19d8 07          	.byte 0x7
 7572 19d9 DF          	.byte 0xdf
 7573 19da 12 01 00 00 	.4byte 0x112
 7574 19de 02          	.byte 0x2
 7575 19df 01          	.byte 0x1
 7576 19e0 0A          	.byte 0xa
 7577 19e1 02          	.byte 0x2
 7578 19e2 23          	.byte 0x23
 7579 19e3 00          	.uleb128 0x0
 7580 19e4 00          	.byte 0x0
 7581 19e5 1B          	.uleb128 0x1b
 7582 19e6 01          	.byte 0x1
 7583 19e7 07          	.byte 0x7
 7584 19e8 E1          	.byte 0xe1
 7585 19e9 FF 19 00 00 	.4byte 0x19ff
MPLAB XC16 ASSEMBLY Listing:   			page 199


 7586 19ed 1C          	.uleb128 0x1c
 7587 19ee 50 49 44 00 	.asciz "PID"
 7588 19f2 07          	.byte 0x7
 7589 19f3 E3          	.byte 0xe3
 7590 19f4 12 01 00 00 	.4byte 0x112
 7591 19f8 02          	.byte 0x2
 7592 19f9 04          	.byte 0x4
 7593 19fa 0A          	.byte 0xa
 7594 19fb 02          	.byte 0x2
 7595 19fc 23          	.byte 0x23
 7596 19fd 00          	.uleb128 0x0
 7597 19fe 00          	.byte 0x0
 7598 19ff 1D          	.uleb128 0x1d
 7599 1a00 5F 42 44 5F 	.asciz "_BD_STAT"
 7599      53 54 41 54 
 7599      00 
 7600 1a09 02          	.byte 0x2
 7601 1a0a 07          	.byte 0x7
 7602 1a0b D0          	.byte 0xd0
 7603 1a0c 2B 1A 00 00 	.4byte 0x1a2b
 7604 1a10 09          	.uleb128 0x9
 7605 1a11 41 19 00 00 	.4byte 0x1941
 7606 1a15 09          	.uleb128 0x9
 7607 1a16 94 19 00 00 	.4byte 0x1994
 7608 1a1a 09          	.uleb128 0x9
 7609 1a1b E5 19 00 00 	.4byte 0x19e5
 7610 1a1f 1E          	.uleb128 0x1e
 7611 1a20 56 61 6C 00 	.asciz "Val"
 7612 1a24 07          	.byte 0x7
 7613 1a25 E5          	.byte 0xe5
 7614 1a26 02 01 00 00 	.4byte 0x102
 7615 1a2a 00          	.byte 0x0
 7616 1a2b 03          	.uleb128 0x3
 7617 1a2c 42 44 5F 53 	.asciz "BD_STAT"
 7617      54 41 54 00 
 7618 1a34 07          	.byte 0x7
 7619 1a35 E6          	.byte 0xe6
 7620 1a36 FF 19 00 00 	.4byte 0x19ff
 7621 1a3a 1B          	.uleb128 0x1b
 7622 1a3b 08          	.byte 0x8
 7623 1a3c 07          	.byte 0x7
 7624 1a3d ED          	.byte 0xed
 7625 1a3e 80 1A 00 00 	.4byte 0x1a80
 7626 1a42 0F          	.uleb128 0xf
 7627 1a43 53 54 41 54 	.asciz "STAT"
 7627      00 
 7628 1a48 07          	.byte 0x7
 7629 1a49 EF          	.byte 0xef
 7630 1a4a 2B 1A 00 00 	.4byte 0x1a2b
 7631 1a4e 02          	.byte 0x2
 7632 1a4f 23          	.byte 0x23
 7633 1a50 00          	.uleb128 0x0
 7634 1a51 1C          	.uleb128 0x1c
 7635 1a52 43 4E 54 00 	.asciz "CNT"
 7636 1a56 07          	.byte 0x7
 7637 1a57 F0          	.byte 0xf0
 7638 1a58 02 01 00 00 	.4byte 0x102
MPLAB XC16 ASSEMBLY Listing:   			page 200


 7639 1a5c 02          	.byte 0x2
 7640 1a5d 0A          	.byte 0xa
 7641 1a5e 06          	.byte 0x6
 7642 1a5f 02          	.byte 0x2
 7643 1a60 23          	.byte 0x23
 7644 1a61 02          	.uleb128 0x2
 7645 1a62 0F          	.uleb128 0xf
 7646 1a63 41 44 52 00 	.asciz "ADR"
 7647 1a67 07          	.byte 0x7
 7648 1a68 F1          	.byte 0xf1
 7649 1a69 02 01 00 00 	.4byte 0x102
 7650 1a6d 02          	.byte 0x2
 7651 1a6e 23          	.byte 0x23
 7652 1a6f 04          	.uleb128 0x4
 7653 1a70 0F          	.uleb128 0xf
 7654 1a71 41 44 52 48 	.asciz "ADRH"
 7654      00 
 7655 1a76 07          	.byte 0x7
 7656 1a77 F2          	.byte 0xf2
 7657 1a78 02 01 00 00 	.4byte 0x102
 7658 1a7c 02          	.byte 0x2
 7659 1a7d 23          	.byte 0x23
 7660 1a7e 06          	.uleb128 0x6
 7661 1a7f 00          	.byte 0x0
 7662 1a80 1B          	.uleb128 0x1b
 7663 1a81 04          	.byte 0x4
 7664 1a82 07          	.byte 0x7
 7665 1a83 F4          	.byte 0xf4
 7666 1a84 B1 1A 00 00 	.4byte 0x1ab1
 7667 1a88 1C          	.uleb128 0x1c
 7668 1a89 72 65 73 00 	.asciz "res"
 7669 1a8d 07          	.byte 0x7
 7670 1a8e F6          	.byte 0xf6
 7671 1a8f 22 01 00 00 	.4byte 0x122
 7672 1a93 04          	.byte 0x4
 7673 1a94 10          	.byte 0x10
 7674 1a95 10          	.byte 0x10
 7675 1a96 06          	.byte 0x6
 7676 1a97 23          	.byte 0x23
 7677 1a98 FE FF FF FF 	.uleb128 0xfffffffe
 7677      0F 
 7678 1a9d 1C          	.uleb128 0x1c
 7679 1a9e 63 6F 75 6E 	.asciz "count"
 7679      74 00 
 7680 1aa4 07          	.byte 0x7
 7681 1aa5 F7          	.byte 0xf7
 7682 1aa6 22 01 00 00 	.4byte 0x122
 7683 1aaa 04          	.byte 0x4
 7684 1aab 0A          	.byte 0xa
 7685 1aac 06          	.byte 0x6
 7686 1aad 02          	.byte 0x2
 7687 1aae 23          	.byte 0x23
 7688 1aaf 00          	.uleb128 0x0
 7689 1ab0 00          	.byte 0x0
 7690 1ab1 1F          	.uleb128 0x1f
 7691 1ab2 08          	.byte 0x8
 7692 1ab3 07          	.byte 0x7
MPLAB XC16 ASSEMBLY Listing:   			page 201


 7693 1ab4 EB          	.byte 0xeb
 7694 1ab5 C4 1A 00 00 	.4byte 0x1ac4
 7695 1ab9 09          	.uleb128 0x9
 7696 1aba 3A 1A 00 00 	.4byte 0x1a3a
 7697 1abe 09          	.uleb128 0x9
 7698 1abf 80 1A 00 00 	.4byte 0x1a80
 7699 1ac3 00          	.byte 0x0
 7700 1ac4 1D          	.uleb128 0x1d
 7701 1ac5 5F 5F 42 44 	.asciz "__BDT"
 7701      54 00 
 7702 1acb 08          	.byte 0x8
 7703 1acc 07          	.byte 0x7
 7704 1acd E9          	.byte 0xe9
 7705 1ace F5 1A 00 00 	.4byte 0x1af5
 7706 1ad2 09          	.uleb128 0x9
 7707 1ad3 B1 1A 00 00 	.4byte 0x1ab1
 7708 1ad7 1E          	.uleb128 0x1e
 7709 1ad8 56 61 6C 00 	.asciz "Val"
 7710 1adc 07          	.byte 0x7
 7711 1add FA          	.byte 0xfa
 7712 1ade 22 01 00 00 	.4byte 0x122
 7713 1ae2 1E          	.uleb128 0x1e
 7714 1ae3 76 00       	.asciz "v"
 7715 1ae5 07          	.byte 0x7
 7716 1ae6 FB          	.byte 0xfb
 7717 1ae7 F5 1A 00 00 	.4byte 0x1af5
 7718 1aeb 1E          	.uleb128 0x1e
 7719 1aec 77 00       	.asciz "w"
 7720 1aee 07          	.byte 0x7
 7721 1aef FC          	.byte 0xfc
 7722 1af0 05 1B 00 00 	.4byte 0x1b05
 7723 1af4 00          	.byte 0x0
 7724 1af5 14          	.uleb128 0x14
 7725 1af6 02 01 00 00 	.4byte 0x102
 7726 1afa 05 1B 00 00 	.4byte 0x1b05
 7727 1afe 15          	.uleb128 0x15
 7728 1aff 12 01 00 00 	.4byte 0x112
 7729 1b03 03          	.byte 0x3
 7730 1b04 00          	.byte 0x0
 7731 1b05 14          	.uleb128 0x14
 7732 1b06 22 01 00 00 	.4byte 0x122
 7733 1b0a 15 1B 00 00 	.4byte 0x1b15
 7734 1b0e 15          	.uleb128 0x15
 7735 1b0f 12 01 00 00 	.4byte 0x112
 7736 1b13 01          	.byte 0x1
 7737 1b14 00          	.byte 0x0
 7738 1b15 03          	.uleb128 0x3
 7739 1b16 42 44 54 5F 	.asciz "BDT_ENTRY"
 7739      45 4E 54 52 
 7739      59 00 
 7740 1b20 07          	.byte 0x7
 7741 1b21 FD          	.byte 0xfd
 7742 1b22 C4 1A 00 00 	.4byte 0x1ac4
 7743 1b26 07          	.uleb128 0x7
 7744 1b27 01          	.byte 0x1
 7745 1b28 07          	.byte 0x7
 7746 1b29 01 01       	.2byte 0x101
MPLAB XC16 ASSEMBLY Listing:   			page 202


 7747 1b2b 82 1B 00 00 	.4byte 0x1b82
 7748 1b2f 05          	.uleb128 0x5
 7749 1b30 66 69 6C 6C 	.asciz "filler1"
 7749      65 72 31 00 
 7750 1b38 07          	.byte 0x7
 7751 1b39 03 01       	.2byte 0x103
 7752 1b3b F1 00 00 00 	.4byte 0xf1
 7753 1b3f 01          	.byte 0x1
 7754 1b40 02          	.byte 0x2
 7755 1b41 06          	.byte 0x6
 7756 1b42 02          	.byte 0x2
 7757 1b43 23          	.byte 0x23
 7758 1b44 00          	.uleb128 0x0
 7759 1b45 05          	.uleb128 0x5
 7760 1b46 70 69 6E 67 	.asciz "ping_pong"
 7760      5F 70 6F 6E 
 7760      67 00 
 7761 1b50 07          	.byte 0x7
 7762 1b51 04 01       	.2byte 0x104
 7763 1b53 F1 00 00 00 	.4byte 0xf1
 7764 1b57 01          	.byte 0x1
 7765 1b58 01          	.byte 0x1
 7766 1b59 05          	.byte 0x5
 7767 1b5a 02          	.byte 0x2
 7768 1b5b 23          	.byte 0x23
 7769 1b5c 00          	.uleb128 0x0
 7770 1b5d 16          	.uleb128 0x16
 7771 1b5e 00 00 00 00 	.4byte .LASF2
 7772 1b62 07          	.byte 0x7
 7773 1b63 05 01       	.2byte 0x105
 7774 1b65 F1 00 00 00 	.4byte 0xf1
 7775 1b69 01          	.byte 0x1
 7776 1b6a 01          	.byte 0x1
 7777 1b6b 04          	.byte 0x4
 7778 1b6c 02          	.byte 0x2
 7779 1b6d 23          	.byte 0x23
 7780 1b6e 00          	.uleb128 0x0
 7781 1b6f 16          	.uleb128 0x16
 7782 1b70 00 00 00 00 	.4byte .LASF4
 7783 1b74 07          	.byte 0x7
 7784 1b75 06 01       	.2byte 0x106
 7785 1b77 F1 00 00 00 	.4byte 0xf1
 7786 1b7b 01          	.byte 0x1
 7787 1b7c 04          	.byte 0x4
 7788 1b7d 08          	.byte 0x8
 7789 1b7e 02          	.byte 0x2
 7790 1b7f 23          	.byte 0x23
 7791 1b80 00          	.uleb128 0x0
 7792 1b81 00          	.byte 0x0
 7793 1b82 1D          	.uleb128 0x1d
 7794 1b83 5F 5F 55 53 	.asciz "__USTAT"
 7794      54 41 54 00 
 7795 1b8b 01          	.byte 0x1
 7796 1b8c 07          	.byte 0x7
 7797 1b8d FF          	.byte 0xff
 7798 1b8e A4 1B 00 00 	.4byte 0x1ba4
 7799 1b92 09          	.uleb128 0x9
MPLAB XC16 ASSEMBLY Listing:   			page 203


 7800 1b93 26 1B 00 00 	.4byte 0x1b26
 7801 1b97 13          	.uleb128 0x13
 7802 1b98 56 61 6C 00 	.asciz "Val"
 7803 1b9c 07          	.byte 0x7
 7804 1b9d 08 01       	.2byte 0x108
 7805 1b9f E2 00 00 00 	.4byte 0xe2
 7806 1ba3 00          	.byte 0x0
 7807 1ba4 06          	.uleb128 0x6
 7808 1ba5 55 53 54 41 	.asciz "USTAT_FIELDS"
 7808      54 5F 46 49 
 7808      45 4C 44 53 
 7808      00 
 7809 1bb2 07          	.byte 0x7
 7810 1bb3 09 01       	.2byte 0x109
 7811 1bb5 82 1B 00 00 	.4byte 0x1b82
 7812 1bb9 1B          	.uleb128 0x1b
 7813 1bba 01          	.byte 0x1
 7814 1bbb 08          	.byte 0x8
 7815 1bbc 25          	.byte 0x25
 7816 1bbd 00 1C 00 00 	.4byte 0x1c00
 7817 1bc1 1C          	.uleb128 0x1c
 7818 1bc2 70 69 6E 67 	.asciz "ping_pong_state"
 7818      5F 70 6F 6E 
 7818      67 5F 73 74 
 7818      61 74 65 00 
 7819 1bd2 08          	.byte 0x8
 7820 1bd3 27          	.byte 0x27
 7821 1bd4 F1 00 00 00 	.4byte 0xf1
 7822 1bd8 01          	.byte 0x1
 7823 1bd9 01          	.byte 0x1
 7824 1bda 07          	.byte 0x7
 7825 1bdb 02          	.byte 0x2
 7826 1bdc 23          	.byte 0x23
 7827 1bdd 00          	.uleb128 0x0
 7828 1bde 1C          	.uleb128 0x1c
 7829 1bdf 74 72 61 6E 	.asciz "transfer_terminated"
 7829      73 66 65 72 
 7829      5F 74 65 72 
 7829      6D 69 6E 61 
 7829      74 65 64 00 
 7830 1bf3 08          	.byte 0x8
 7831 1bf4 28          	.byte 0x28
 7832 1bf5 F1 00 00 00 	.4byte 0xf1
 7833 1bf9 01          	.byte 0x1
 7834 1bfa 01          	.byte 0x1
 7835 1bfb 06          	.byte 0x6
 7836 1bfc 02          	.byte 0x2
 7837 1bfd 23          	.byte 0x23
 7838 1bfe 00          	.uleb128 0x0
 7839 1bff 00          	.byte 0x0
 7840 1c00 1F          	.uleb128 0x1f
 7841 1c01 01          	.byte 0x1
 7842 1c02 08          	.byte 0x8
 7843 1c03 23          	.byte 0x23
 7844 1c04 20 1C 00 00 	.4byte 0x1c20
 7845 1c08 1E          	.uleb128 0x1e
 7846 1c09 62 69 74 73 	.asciz "bits"
MPLAB XC16 ASSEMBLY Listing:   			page 204


 7846      00 
 7847 1c0e 08          	.byte 0x8
 7848 1c0f 29          	.byte 0x29
 7849 1c10 B9 1B 00 00 	.4byte 0x1bb9
 7850 1c14 1E          	.uleb128 0x1e
 7851 1c15 56 61 6C 00 	.asciz "Val"
 7852 1c19 08          	.byte 0x8
 7853 1c1a 2A          	.byte 0x2a
 7854 1c1b E2 00 00 00 	.4byte 0xe2
 7855 1c1f 00          	.byte 0x0
 7856 1c20 03          	.uleb128 0x3
 7857 1c21 45 50 5F 53 	.asciz "EP_STATUS"
 7857      54 41 54 55 
 7857      53 00 
 7858 1c2b 08          	.byte 0x8
 7859 1c2c 2B          	.byte 0x2b
 7860 1c2d 00 1C 00 00 	.4byte 0x1c00
 7861 1c31 20          	.uleb128 0x20
 7862 1c32 01          	.byte 0x1
 7863 1c33 55 53 42 44 	.asciz "USBDeviceInit"
 7863      65 76 69 63 
 7863      65 49 6E 69 
 7863      74 00 
 7864 1c41 01          	.byte 0x1
 7865 1c42 17 01       	.2byte 0x117
 7866 1c44 01          	.byte 0x1
 7867 1c45 00 00 00 00 	.4byte .LFB0
 7868 1c49 00 00 00 00 	.4byte .LFE0
 7869 1c4d 01          	.byte 0x1
 7870 1c4e 5E          	.byte 0x5e
 7871 1c4f 61 1C 00 00 	.4byte 0x1c61
 7872 1c53 21          	.uleb128 0x21
 7873 1c54 69 00       	.asciz "i"
 7874 1c56 01          	.byte 0x1
 7875 1c57 19 01       	.2byte 0x119
 7876 1c59 E2 00 00 00 	.4byte 0xe2
 7877 1c5d 02          	.byte 0x2
 7878 1c5e 7E          	.byte 0x7e
 7879 1c5f 00          	.sleb128 0
 7880 1c60 00          	.byte 0x0
 7881 1c61 20          	.uleb128 0x20
 7882 1c62 01          	.byte 0x1
 7883 1c63 55 53 42 44 	.asciz "USBDeviceTasks"
 7883      65 76 69 63 
 7883      65 54 61 73 
 7883      6B 73 00 
 7884 1c72 01          	.byte 0x1
 7885 1c73 DF 01       	.2byte 0x1df
 7886 1c75 01          	.byte 0x1
 7887 1c76 00 00 00 00 	.4byte .LFB1
 7888 1c7a 00 00 00 00 	.4byte .LFE1
 7889 1c7e 01          	.byte 0x1
 7890 1c7f 5E          	.byte 0x5e
 7891 1c80 92 1C 00 00 	.4byte 0x1c92
 7892 1c84 21          	.uleb128 0x21
 7893 1c85 69 00       	.asciz "i"
 7894 1c87 01          	.byte 0x1
MPLAB XC16 ASSEMBLY Listing:   			page 205


 7895 1c88 E1 01       	.2byte 0x1e1
 7896 1c8a E2 00 00 00 	.4byte 0xe2
 7897 1c8e 02          	.byte 0x2
 7898 1c8f 7E          	.byte 0x7e
 7899 1c90 00          	.sleb128 0
 7900 1c91 00          	.byte 0x0
 7901 1c92 20          	.uleb128 0x20
 7902 1c93 01          	.byte 0x1
 7903 1c94 55 53 42 45 	.asciz "USBEnableEndpoint"
 7903      6E 61 62 6C 
 7903      65 45 6E 64 
 7903      70 6F 69 6E 
 7903      74 00 
 7904 1ca6 01          	.byte 0x1
 7905 1ca7 70 03       	.2byte 0x370
 7906 1ca9 01          	.byte 0x1
 7907 1caa 00 00 00 00 	.4byte .LFB2
 7908 1cae 00 00 00 00 	.4byte .LFE2
 7909 1cb2 01          	.byte 0x1
 7910 1cb3 5E          	.byte 0x5e
 7911 1cb4 E7 1C 00 00 	.4byte 0x1ce7
 7912 1cb8 22          	.uleb128 0x22
 7913 1cb9 65 70 00    	.asciz "ep"
 7914 1cbc 01          	.byte 0x1
 7915 1cbd 70 03       	.2byte 0x370
 7916 1cbf E2 00 00 00 	.4byte 0xe2
 7917 1cc3 02          	.byte 0x2
 7918 1cc4 7E          	.byte 0x7e
 7919 1cc5 02          	.sleb128 2
 7920 1cc6 22          	.uleb128 0x22
 7921 1cc7 6F 70 74 69 	.asciz "options"
 7921      6F 6E 73 00 
 7922 1ccf 01          	.byte 0x1
 7923 1cd0 70 03       	.2byte 0x370
 7924 1cd2 E2 00 00 00 	.4byte 0xe2
 7925 1cd6 02          	.byte 0x2
 7926 1cd7 7E          	.byte 0x7e
 7927 1cd8 03          	.sleb128 3
 7928 1cd9 21          	.uleb128 0x21
 7929 1cda 70 00       	.asciz "p"
 7930 1cdc 01          	.byte 0x1
 7931 1cdd 72 03       	.2byte 0x372
 7932 1cdf E7 1C 00 00 	.4byte 0x1ce7
 7933 1ce3 02          	.byte 0x2
 7934 1ce4 7E          	.byte 0x7e
 7935 1ce5 00          	.sleb128 0
 7936 1ce6 00          	.byte 0x0
 7937 1ce7 18          	.uleb128 0x18
 7938 1ce8 02          	.byte 0x2
 7939 1ce9 F1 00 00 00 	.4byte 0xf1
 7940 1ced 23          	.uleb128 0x23
 7941 1cee 01          	.byte 0x1
 7942 1cef 55 53 42 54 	.asciz "USBTransferOnePacket"
 7942      72 61 6E 73 
 7942      66 65 72 4F 
 7942      6E 65 50 61 
 7942      63 6B 65 74 
MPLAB XC16 ASSEMBLY Listing:   			page 206


 7942      00 
 7943 1d04 01          	.byte 0x1
 7944 1d05 E5 03       	.2byte 0x3e5
 7945 1d07 01          	.byte 0x1
 7946 1d08 AE 0C 00 00 	.4byte 0xcae
 7947 1d0c 00 00 00 00 	.4byte .LFB3
 7948 1d10 00 00 00 00 	.4byte .LFE3
 7949 1d14 01          	.byte 0x1
 7950 1d15 5E          	.byte 0x5e
 7951 1d16 69 1D 00 00 	.4byte 0x1d69
 7952 1d1a 22          	.uleb128 0x22
 7953 1d1b 65 70 00    	.asciz "ep"
 7954 1d1e 01          	.byte 0x1
 7955 1d1f E5 03       	.2byte 0x3e5
 7956 1d21 E2 00 00 00 	.4byte 0xe2
 7957 1d25 02          	.byte 0x2
 7958 1d26 7E          	.byte 0x7e
 7959 1d27 02          	.sleb128 2
 7960 1d28 22          	.uleb128 0x22
 7961 1d29 64 69 72 00 	.asciz "dir"
 7962 1d2d 01          	.byte 0x1
 7963 1d2e E5 03       	.2byte 0x3e5
 7964 1d30 E2 00 00 00 	.4byte 0xe2
 7965 1d34 02          	.byte 0x2
 7966 1d35 7E          	.byte 0x7e
 7967 1d36 03          	.sleb128 3
 7968 1d37 22          	.uleb128 0x22
 7969 1d38 64 61 74 61 	.asciz "data"
 7969      00 
 7970 1d3d 01          	.byte 0x1
 7971 1d3e E5 03       	.2byte 0x3e5
 7972 1d40 70 17 00 00 	.4byte 0x1770
 7973 1d44 02          	.byte 0x2
 7974 1d45 7E          	.byte 0x7e
 7975 1d46 04          	.sleb128 4
 7976 1d47 22          	.uleb128 0x22
 7977 1d48 6C 65 6E 00 	.asciz "len"
 7978 1d4c 01          	.byte 0x1
 7979 1d4d E5 03       	.2byte 0x3e5
 7980 1d4f E2 00 00 00 	.4byte 0xe2
 7981 1d53 02          	.byte 0x2
 7982 1d54 7E          	.byte 0x7e
 7983 1d55 06          	.sleb128 6
 7984 1d56 21          	.uleb128 0x21
 7985 1d57 68 61 6E 64 	.asciz "handle"
 7985      6C 65 00 
 7986 1d5e 01          	.byte 0x1
 7987 1d5f E7 03       	.2byte 0x3e7
 7988 1d61 69 1D 00 00 	.4byte 0x1d69
 7989 1d65 02          	.byte 0x2
 7990 1d66 7E          	.byte 0x7e
 7991 1d67 00          	.sleb128 0
 7992 1d68 00          	.byte 0x0
 7993 1d69 18          	.uleb128 0x18
 7994 1d6a 02          	.byte 0x2
 7995 1d6b 6F 1D 00 00 	.4byte 0x1d6f
 7996 1d6f 24          	.uleb128 0x24
MPLAB XC16 ASSEMBLY Listing:   			page 207


 7997 1d70 15 1B 00 00 	.4byte 0x1b15
 7998 1d74 20          	.uleb128 0x20
 7999 1d75 01          	.byte 0x1
 8000 1d76 55 53 42 53 	.asciz "USBStallEndpoint"
 8000      74 61 6C 6C 
 8000      45 6E 64 70 
 8000      6F 69 6E 74 
 8000      00 
 8001 1d87 01          	.byte 0x1
 8002 1d88 33 04       	.2byte 0x433
 8003 1d8a 01          	.byte 0x1
 8004 1d8b 00 00 00 00 	.4byte .LFB4
 8005 1d8f 00 00 00 00 	.4byte .LFE4
 8006 1d93 01          	.byte 0x1
 8007 1d94 5E          	.byte 0x5e
 8008 1d95 C4 1D 00 00 	.4byte 0x1dc4
 8009 1d99 22          	.uleb128 0x22
 8010 1d9a 65 70 00    	.asciz "ep"
 8011 1d9d 01          	.byte 0x1
 8012 1d9e 33 04       	.2byte 0x433
 8013 1da0 E2 00 00 00 	.4byte 0xe2
 8014 1da4 02          	.byte 0x2
 8015 1da5 7E          	.byte 0x7e
 8016 1da6 02          	.sleb128 2
 8017 1da7 22          	.uleb128 0x22
 8018 1da8 64 69 72 00 	.asciz "dir"
 8019 1dac 01          	.byte 0x1
 8020 1dad 33 04       	.2byte 0x433
 8021 1daf E2 00 00 00 	.4byte 0xe2
 8022 1db3 02          	.byte 0x2
 8023 1db4 7E          	.byte 0x7e
 8024 1db5 03          	.sleb128 3
 8025 1db6 21          	.uleb128 0x21
 8026 1db7 70 00       	.asciz "p"
 8027 1db9 01          	.byte 0x1
 8028 1dba 35 04       	.2byte 0x435
 8029 1dbc C4 1D 00 00 	.4byte 0x1dc4
 8030 1dc0 02          	.byte 0x2
 8031 1dc1 7E          	.byte 0x7e
 8032 1dc2 00          	.sleb128 0
 8033 1dc3 00          	.byte 0x0
 8034 1dc4 18          	.uleb128 0x18
 8035 1dc5 02          	.byte 0x2
 8036 1dc6 15 1B 00 00 	.4byte 0x1b15
 8037 1dca 20          	.uleb128 0x20
 8038 1dcb 01          	.byte 0x1
 8039 1dcc 55 53 42 43 	.asciz "USBCancelIO"
 8039      61 6E 63 65 
 8039      6C 49 4F 00 
 8040 1dd8 01          	.byte 0x1
 8041 1dd9 6B 04       	.2byte 0x46b
 8042 1ddb 01          	.byte 0x1
 8043 1ddc 00 00 00 00 	.4byte .LFB5
 8044 1de0 00 00 00 00 	.4byte .LFE5
 8045 1de4 01          	.byte 0x1
 8046 1de5 5E          	.byte 0x5e
 8047 1de6 FF 1D 00 00 	.4byte 0x1dff
MPLAB XC16 ASSEMBLY Listing:   			page 208


 8048 1dea 22          	.uleb128 0x22
 8049 1deb 65 6E 64 70 	.asciz "endpoint"
 8049      6F 69 6E 74 
 8049      00 
 8050 1df4 01          	.byte 0x1
 8051 1df5 6B 04       	.2byte 0x46b
 8052 1df7 E2 00 00 00 	.4byte 0xe2
 8053 1dfb 02          	.byte 0x2
 8054 1dfc 7E          	.byte 0x7e
 8055 1dfd 00          	.sleb128 0
 8056 1dfe 00          	.byte 0x0
 8057 1dff 25          	.uleb128 0x25
 8058 1e00 01          	.byte 0x1
 8059 1e01 55 53 42 44 	.asciz "USBDeviceDetach"
 8059      65 76 69 63 
 8059      65 44 65 74 
 8059      61 63 68 00 
 8060 1e11 01          	.byte 0x1
 8061 1e12 E1 04       	.2byte 0x4e1
 8062 1e14 01          	.byte 0x1
 8063 1e15 00 00 00 00 	.4byte .LFB6
 8064 1e19 00 00 00 00 	.4byte .LFE6
 8065 1e1d 01          	.byte 0x1
 8066 1e1e 5E          	.byte 0x5e
 8067 1e1f 25          	.uleb128 0x25
 8068 1e20 01          	.byte 0x1
 8069 1e21 55 53 42 44 	.asciz "USBDeviceAttach"
 8069      65 76 69 63 
 8069      65 41 74 74 
 8069      61 63 68 00 
 8070 1e31 01          	.byte 0x1
 8071 1e32 4B 05       	.2byte 0x54b
 8072 1e34 01          	.byte 0x1
 8073 1e35 00 00 00 00 	.4byte .LFB7
 8074 1e39 00 00 00 00 	.4byte .LFE7
 8075 1e3d 01          	.byte 0x1
 8076 1e3e 5E          	.byte 0x5e
 8077 1e3f 25          	.uleb128 0x25
 8078 1e40 01          	.byte 0x1
 8079 1e41 55 53 42 43 	.asciz "USBCtrlEPAllowStatusStage"
 8079      74 72 6C 45 
 8079      50 41 6C 6C 
 8079      6F 77 53 74 
 8079      61 74 75 73 
 8079      53 74 61 67 
 8079      65 00 
 8080 1e5b 01          	.byte 0x1
 8081 1e5c 8C 05       	.2byte 0x58c
 8082 1e5e 01          	.byte 0x1
 8083 1e5f 00 00 00 00 	.4byte .LFB8
 8084 1e63 00 00 00 00 	.4byte .LFE8
 8085 1e67 01          	.byte 0x1
 8086 1e68 5E          	.byte 0x5e
 8087 1e69 25          	.uleb128 0x25
 8088 1e6a 01          	.byte 0x1
 8089 1e6b 55 53 42 43 	.asciz "USBCtrlEPAllowDataStage"
 8089      74 72 6C 45 
MPLAB XC16 ASSEMBLY Listing:   			page 209


 8089      50 41 6C 6C 
 8089      6F 77 44 61 
 8089      74 61 53 74 
 8089      61 67 65 00 
 8090 1e83 01          	.byte 0x1
 8091 1e84 D4 05       	.2byte 0x5d4
 8092 1e86 01          	.byte 0x1
 8093 1e87 00 00 00 00 	.4byte .LFB9
 8094 1e8b 00 00 00 00 	.4byte .LFE9
 8095 1e8f 01          	.byte 0x1
 8096 1e90 5E          	.byte 0x5e
 8097 1e91 26          	.uleb128 0x26
 8098 1e92 55 53 42 43 	.asciz "USBConfigureEndpoint"
 8098      6F 6E 66 69 
 8098      67 75 72 65 
 8098      45 6E 64 70 
 8098      6F 69 6E 74 
 8098      00 
 8099 1ea7 01          	.byte 0x1
 8100 1ea8 0B 06       	.2byte 0x60b
 8101 1eaa 01          	.byte 0x1
 8102 1eab 00 00 00 00 	.4byte .LFB10
 8103 1eaf 00 00 00 00 	.4byte .LFE10
 8104 1eb3 01          	.byte 0x1
 8105 1eb4 5E          	.byte 0x5e
 8106 1eb5 EC 1E 00 00 	.4byte 0x1eec
 8107 1eb9 22          	.uleb128 0x22
 8108 1eba 45 50 4E 75 	.asciz "EPNum"
 8108      6D 00 
 8109 1ec0 01          	.byte 0x1
 8110 1ec1 0B 06       	.2byte 0x60b
 8111 1ec3 E2 00 00 00 	.4byte 0xe2
 8112 1ec7 02          	.byte 0x2
 8113 1ec8 7E          	.byte 0x7e
 8114 1ec9 02          	.sleb128 2
 8115 1eca 27          	.uleb128 0x27
 8116 1ecb 00 00 00 00 	.4byte .LASF2
 8117 1ecf 01          	.byte 0x1
 8118 1ed0 0B 06       	.2byte 0x60b
 8119 1ed2 E2 00 00 00 	.4byte 0xe2
 8120 1ed6 02          	.byte 0x2
 8121 1ed7 7E          	.byte 0x7e
 8122 1ed8 03          	.sleb128 3
 8123 1ed9 21          	.uleb128 0x21
 8124 1eda 68 61 6E 64 	.asciz "handle"
 8124      6C 65 00 
 8125 1ee1 01          	.byte 0x1
 8126 1ee2 0D 06       	.2byte 0x60d
 8127 1ee4 69 1D 00 00 	.4byte 0x1d69
 8128 1ee8 02          	.byte 0x2
 8129 1ee9 7E          	.byte 0x7e
 8130 1eea 00          	.sleb128 0
 8131 1eeb 00          	.byte 0x0
 8132 1eec 28          	.uleb128 0x28
 8133 1eed 55 53 42 43 	.asciz "USBCtrlEPServiceComplete"
 8133      74 72 6C 45 
 8133      50 53 65 72 
MPLAB XC16 ASSEMBLY Listing:   			page 210


 8133      76 69 63 65 
 8133      43 6F 6D 70 
 8133      6C 65 74 65 
 8133      00 
 8134 1f06 01          	.byte 0x1
 8135 1f07 54 06       	.2byte 0x654
 8136 1f09 01          	.byte 0x1
 8137 1f0a 00 00 00 00 	.4byte .LFB11
 8138 1f0e 00 00 00 00 	.4byte .LFE11
 8139 1f12 01          	.byte 0x1
 8140 1f13 5E          	.byte 0x5e
 8141 1f14 26          	.uleb128 0x26
 8142 1f15 55 53 42 43 	.asciz "USBCtrlTrfTxService"
 8142      74 72 6C 54 
 8142      72 66 54 78 
 8142      53 65 72 76 
 8142      69 63 65 00 
 8143 1f29 01          	.byte 0x1
 8144 1f2a EF 06       	.2byte 0x6ef
 8145 1f2c 01          	.byte 0x1
 8146 1f2d 00 00 00 00 	.4byte .LFB12
 8147 1f31 00 00 00 00 	.4byte .LFE12
 8148 1f35 01          	.byte 0x1
 8149 1f36 5E          	.byte 0x5e
 8150 1f37 52 1F 00 00 	.4byte 0x1f52
 8151 1f3b 21          	.uleb128 0x21
 8152 1f3c 62 79 74 65 	.asciz "byteToSend"
 8152      54 6F 53 65 
 8152      6E 64 00 
 8153 1f47 01          	.byte 0x1
 8154 1f48 F1 06       	.2byte 0x6f1
 8155 1f4a E2 00 00 00 	.4byte 0xe2
 8156 1f4e 02          	.byte 0x2
 8157 1f4f 7E          	.byte 0x7e
 8158 1f50 00          	.sleb128 0
 8159 1f51 00          	.byte 0x0
 8160 1f52 26          	.uleb128 0x26
 8161 1f53 55 53 42 43 	.asciz "USBCtrlTrfRxService"
 8161      74 72 6C 54 
 8161      72 66 52 78 
 8161      53 65 72 76 
 8161      69 63 65 00 
 8162 1f67 01          	.byte 0x1
 8163 1f68 40 07       	.2byte 0x740
 8164 1f6a 01          	.byte 0x1
 8165 1f6b 00 00 00 00 	.4byte .LFB13
 8166 1f6f 00 00 00 00 	.4byte .LFE13
 8167 1f73 01          	.byte 0x1
 8168 1f74 5E          	.byte 0x5e
 8169 1f75 9D 1F 00 00 	.4byte 0x1f9d
 8170 1f79 21          	.uleb128 0x21
 8171 1f7a 62 79 74 65 	.asciz "byteToRead"
 8171      54 6F 52 65 
 8171      61 64 00 
 8172 1f85 01          	.byte 0x1
 8173 1f86 42 07       	.2byte 0x742
 8174 1f88 E2 00 00 00 	.4byte 0xe2
MPLAB XC16 ASSEMBLY Listing:   			page 211


 8175 1f8c 02          	.byte 0x2
 8176 1f8d 7E          	.byte 0x7e
 8177 1f8e 00          	.sleb128 0
 8178 1f8f 21          	.uleb128 0x21
 8179 1f90 69 00       	.asciz "i"
 8180 1f92 01          	.byte 0x1
 8181 1f93 43 07       	.2byte 0x743
 8182 1f95 E2 00 00 00 	.4byte 0xe2
 8183 1f99 02          	.byte 0x2
 8184 1f9a 7E          	.byte 0x7e
 8185 1f9b 01          	.sleb128 1
 8186 1f9c 00          	.byte 0x0
 8187 1f9d 26          	.uleb128 0x26
 8188 1f9e 55 53 42 53 	.asciz "USBStdSetCfgHandler"
 8188      74 64 53 65 
 8188      74 43 66 67 
 8188      48 61 6E 64 
 8188      6C 65 72 00 
 8189 1fb2 01          	.byte 0x1
 8190 1fb3 B4 07       	.2byte 0x7b4
 8191 1fb5 01          	.byte 0x1
 8192 1fb6 00 00 00 00 	.4byte .LFB14
 8193 1fba 00 00 00 00 	.4byte .LFE14
 8194 1fbe 01          	.byte 0x1
 8195 1fbf 5E          	.byte 0x5e
 8196 1fc0 D2 1F 00 00 	.4byte 0x1fd2
 8197 1fc4 21          	.uleb128 0x21
 8198 1fc5 69 00       	.asciz "i"
 8199 1fc7 01          	.byte 0x1
 8200 1fc8 B6 07       	.2byte 0x7b6
 8201 1fca E2 00 00 00 	.4byte 0xe2
 8202 1fce 02          	.byte 0x2
 8203 1fcf 7E          	.byte 0x7e
 8204 1fd0 00          	.sleb128 0
 8205 1fd1 00          	.byte 0x0
 8206 1fd2 28          	.uleb128 0x28
 8207 1fd3 55 53 42 53 	.asciz "USBStdGetDscHandler"
 8207      74 64 47 65 
 8207      74 44 73 63 
 8207      48 61 6E 64 
 8207      6C 65 72 00 
 8208 1fe7 01          	.byte 0x1
 8209 1fe8 FF 07       	.2byte 0x7ff
 8210 1fea 01          	.byte 0x1
 8211 1feb 00 00 00 00 	.4byte .LFB15
 8212 1fef 00 00 00 00 	.4byte .LFE15
 8213 1ff3 01          	.byte 0x1
 8214 1ff4 5E          	.byte 0x5e
 8215 1ff5 26          	.uleb128 0x26
 8216 1ff6 55 53 42 53 	.asciz "USBStdGetStatusHandler"
 8216      74 64 47 65 
 8216      74 53 74 61 
 8216      74 75 73 48 
 8216      61 6E 64 6C 
 8216      65 72 00 
 8217 200d 01          	.byte 0x1
 8218 200e 55 08       	.2byte 0x855
MPLAB XC16 ASSEMBLY Listing:   			page 212


 8219 2010 01          	.byte 0x1
 8220 2011 00 00 00 00 	.4byte .LFB16
 8221 2015 00 00 00 00 	.4byte .LFE16
 8222 2019 01          	.byte 0x1
 8223 201a 5E          	.byte 0x5e
 8224 201b 37 20 00 00 	.4byte 0x2037
 8225 201f 29          	.uleb128 0x29
 8226 2020 00 00 00 00 	.4byte .LBB2
 8227 2024 00 00 00 00 	.4byte .LBE2
 8228 2028 21          	.uleb128 0x21
 8229 2029 70 00       	.asciz "p"
 8230 202b 01          	.byte 0x1
 8231 202c 75 08       	.2byte 0x875
 8232 202e C4 1D 00 00 	.4byte 0x1dc4
 8233 2032 02          	.byte 0x2
 8234 2033 7E          	.byte 0x7e
 8235 2034 00          	.sleb128 0
 8236 2035 00          	.byte 0x0
 8237 2036 00          	.byte 0x0
 8238 2037 28          	.uleb128 0x28
 8239 2038 55 53 42 53 	.asciz "USBStallHandler"
 8239      74 61 6C 6C 
 8239      48 61 6E 64 
 8239      6C 65 72 00 
 8240 2048 01          	.byte 0x1
 8241 2049 9E 08       	.2byte 0x89e
 8242 204b 01          	.byte 0x1
 8243 204c 00 00 00 00 	.4byte .LFB17
 8244 2050 00 00 00 00 	.4byte .LFE17
 8245 2054 01          	.byte 0x1
 8246 2055 5E          	.byte 0x5e
 8247 2056 28          	.uleb128 0x28
 8248 2057 55 53 42 53 	.asciz "USBSuspend"
 8248      75 73 70 65 
 8248      6E 64 00 
 8249 2062 01          	.byte 0x1
 8250 2063 C9 08       	.2byte 0x8c9
 8251 2065 01          	.byte 0x1
 8252 2066 00 00 00 00 	.4byte .LFB18
 8253 206a 00 00 00 00 	.4byte .LFE18
 8254 206e 01          	.byte 0x1
 8255 206f 5E          	.byte 0x5e
 8256 2070 28          	.uleb128 0x28
 8257 2071 55 53 42 57 	.asciz "USBWakeFromSuspend"
 8257      61 6B 65 46 
 8257      72 6F 6D 53 
 8257      75 73 70 65 
 8257      6E 64 00 
 8258 2084 01          	.byte 0x1
 8259 2085 03 09       	.2byte 0x903
 8260 2087 01          	.byte 0x1
 8261 2088 00 00 00 00 	.4byte .LFB19
 8262 208c 00 00 00 00 	.4byte .LFE19
 8263 2090 01          	.byte 0x1
 8264 2091 5E          	.byte 0x5e
 8265 2092 28          	.uleb128 0x28
 8266 2093 55 53 42 43 	.asciz "USBCtrlEPService"
MPLAB XC16 ASSEMBLY Listing:   			page 213


 8266      74 72 6C 45 
 8266      50 53 65 72 
 8266      76 69 63 65 
 8266      00 
 8267 20a4 01          	.byte 0x1
 8268 20a5 48 09       	.2byte 0x948
 8269 20a7 01          	.byte 0x1
 8270 20a8 00 00 00 00 	.4byte .LFB20
 8271 20ac 00 00 00 00 	.4byte .LFE20
 8272 20b0 01          	.byte 0x1
 8273 20b1 5E          	.byte 0x5e
 8274 20b2 28          	.uleb128 0x28
 8275 20b3 55 53 42 43 	.asciz "USBCtrlTrfSetupHandler"
 8275      74 72 6C 54 
 8275      72 66 53 65 
 8275      74 75 70 48 
 8275      61 6E 64 6C 
 8275      65 72 00 
 8276 20ca 01          	.byte 0x1
 8277 20cb A5 09       	.2byte 0x9a5
 8278 20cd 01          	.byte 0x1
 8279 20ce 00 00 00 00 	.4byte .LFB21
 8280 20d2 00 00 00 00 	.4byte .LFE21
 8281 20d6 01          	.byte 0x1
 8282 20d7 5E          	.byte 0x5e
 8283 20d8 28          	.uleb128 0x28
 8284 20d9 55 53 42 43 	.asciz "USBCtrlTrfOutHandler"
 8284      74 72 6C 54 
 8284      72 66 4F 75 
 8284      74 48 61 6E 
 8284      64 6C 65 72 
 8284      00 
 8285 20ee 01          	.byte 0x1
 8286 20ef EE 09       	.2byte 0x9ee
 8287 20f1 01          	.byte 0x1
 8288 20f2 00 00 00 00 	.4byte .LFB22
 8289 20f6 00 00 00 00 	.4byte .LFE22
 8290 20fa 01          	.byte 0x1
 8291 20fb 5E          	.byte 0x5e
 8292 20fc 26          	.uleb128 0x26
 8293 20fd 55 53 42 43 	.asciz "USBCtrlTrfInHandler"
 8293      74 72 6C 54 
 8293      72 66 49 6E 
 8293      48 61 6E 64 
 8293      6C 65 72 00 
 8294 2111 01          	.byte 0x1
 8295 2112 24 0A       	.2byte 0xa24
 8296 2114 01          	.byte 0x1
 8297 2115 00 00 00 00 	.4byte .LFB23
 8298 2119 00 00 00 00 	.4byte .LFE23
 8299 211d 01          	.byte 0x1
 8300 211e 5E          	.byte 0x5e
 8301 211f 37 21 00 00 	.4byte 0x2137
 8302 2123 21          	.uleb128 0x21
 8303 2124 6C 61 73 74 	.asciz "lastDTS"
 8303      44 54 53 00 
 8304 212c 01          	.byte 0x1
MPLAB XC16 ASSEMBLY Listing:   			page 214


 8305 212d 26 0A       	.2byte 0xa26
 8306 212f E2 00 00 00 	.4byte 0xe2
 8307 2133 02          	.byte 0x2
 8308 2134 7E          	.byte 0x7e
 8309 2135 00          	.sleb128 0
 8310 2136 00          	.byte 0x0
 8311 2137 28          	.uleb128 0x28
 8312 2138 55 53 42 43 	.asciz "USBCheckStdRequest"
 8312      68 65 63 6B 
 8312      53 74 64 52 
 8312      65 71 75 65 
 8312      73 74 00 
 8313 214b 01          	.byte 0x1
 8314 214c 82 0A       	.2byte 0xa82
 8315 214e 01          	.byte 0x1
 8316 214f 00 00 00 00 	.4byte .LFB24
 8317 2153 00 00 00 00 	.4byte .LFE24
 8318 2157 01          	.byte 0x1
 8319 2158 5E          	.byte 0x5e
 8320 2159 26          	.uleb128 0x26
 8321 215a 55 53 42 53 	.asciz "USBStdFeatureReqHandler"
 8321      74 64 46 65 
 8321      61 74 75 72 
 8321      65 52 65 71 
 8321      48 61 6E 64 
 8321      6C 65 72 00 
 8322 2172 01          	.byte 0x1
 8323 2173 C6 0A       	.2byte 0xac6
 8324 2175 01          	.byte 0x1
 8325 2176 00 00 00 00 	.4byte .LFB25
 8326 217a 00 00 00 00 	.4byte .LFE25
 8327 217e 01          	.byte 0x1
 8328 217f 5E          	.byte 0x5e
 8329 2180 BD 21 00 00 	.4byte 0x21bd
 8330 2184 21          	.uleb128 0x21
 8331 2185 70 00       	.asciz "p"
 8332 2187 01          	.byte 0x1
 8333 2188 C8 0A       	.2byte 0xac8
 8334 218a C4 1D 00 00 	.4byte 0x1dc4
 8335 218e 02          	.byte 0x2
 8336 218f 7E          	.byte 0x7e
 8337 2190 00          	.sleb128 0
 8338 2191 21          	.uleb128 0x21
 8339 2192 63 75 72 72 	.asciz "current_ep_data"
 8339      65 6E 74 5F 
 8339      65 70 5F 64 
 8339      61 74 61 00 
 8340 21a2 01          	.byte 0x1
 8341 21a3 C9 0A       	.2byte 0xac9
 8342 21a5 20 1C 00 00 	.4byte 0x1c20
 8343 21a9 02          	.byte 0x2
 8344 21aa 7E          	.byte 0x7e
 8345 21ab 04          	.sleb128 4
 8346 21ac 21          	.uleb128 0x21
 8347 21ad 70 55 45 50 	.asciz "pUEP"
 8347      00 
 8348 21b2 01          	.byte 0x1
MPLAB XC16 ASSEMBLY Listing:   			page 215


 8349 21b3 CD 0A       	.2byte 0xacd
 8350 21b5 E7 1C 00 00 	.4byte 0x1ce7
 8351 21b9 02          	.byte 0x2
 8352 21ba 7E          	.byte 0x7e
 8353 21bb 02          	.sleb128 2
 8354 21bc 00          	.byte 0x0
 8355 21bd 25          	.uleb128 0x25
 8356 21be 01          	.byte 0x1
 8357 21bf 55 53 42 49 	.asciz "USBIncrement1msInternalTimers"
 8357      6E 63 72 65 
 8357      6D 65 6E 74 
 8357      31 6D 73 49 
 8357      6E 74 65 72 
 8357      6E 61 6C 54 
 8357      69 6D 65 72 
 8357      73 00 
 8358 21dd 01          	.byte 0x1
 8359 21de D5 0B       	.2byte 0xbd5
 8360 21e0 01          	.byte 0x1
 8361 21e1 00 00 00 00 	.4byte .LFB26
 8362 21e5 00 00 00 00 	.4byte .LFE26
 8363 21e9 01          	.byte 0x1
 8364 21ea 5E          	.byte 0x5e
 8365 21eb 23          	.uleb128 0x23
 8366 21ec 01          	.byte 0x1
 8367 21ed 55 53 42 47 	.asciz "USBGet1msTickCount"
 8367      65 74 31 6D 
 8367      73 54 69 63 
 8367      6B 43 6F 75 
 8367      6E 74 00 
 8368 2200 01          	.byte 0x1
 8369 2201 24 0C       	.2byte 0xc24
 8370 2203 01          	.byte 0x1
 8371 2204 22 01 00 00 	.4byte 0x122
 8372 2208 00 00 00 00 	.4byte .LFB27
 8373 220c 00 00 00 00 	.4byte .LFE27
 8374 2210 01          	.byte 0x1
 8375 2211 5E          	.byte 0x5e
 8376 2212 34 22 00 00 	.4byte 0x2234
 8377 2216 21          	.uleb128 0x21
 8378 2217 6C 6F 63 61 	.asciz "localContextValue"
 8378      6C 43 6F 6E 
 8378      74 65 78 74 
 8378      56 61 6C 75 
 8378      65 00 
 8379 2229 01          	.byte 0x1
 8380 222a 27 0C       	.2byte 0xc27
 8381 222c 22 01 00 00 	.4byte 0x122
 8382 2230 02          	.byte 0x2
 8383 2231 7E          	.byte 0x7e
 8384 2232 00          	.sleb128 0
 8385 2233 00          	.byte 0x0
 8386 2234 2A          	.uleb128 0x2a
 8387 2235 55 31 4F 54 	.asciz "U1OTGIR"
 8387      47 49 52 00 
 8388 223d 03          	.byte 0x3
 8389 223e 52 12       	.2byte 0x1252
MPLAB XC16 ASSEMBLY Listing:   			page 216


 8390 2240 46 22 00 00 	.4byte 0x2246
 8391 2244 01          	.byte 0x1
 8392 2245 01          	.byte 0x1
 8393 2246 24          	.uleb128 0x24
 8394 2247 02 01 00 00 	.4byte 0x102
 8395 224b 2B          	.uleb128 0x2b
 8396 224c 00 00 00 00 	.4byte .LASF5
 8397 2250 03          	.byte 0x3
 8398 2251 5D 12       	.2byte 0x125d
 8399 2253 59 22 00 00 	.4byte 0x2259
 8400 2257 01          	.byte 0x1
 8401 2258 01          	.byte 0x1
 8402 2259 24          	.uleb128 0x24
 8403 225a 14 02 00 00 	.4byte 0x214
 8404 225e 2B          	.uleb128 0x2b
 8405 225f 00 00 00 00 	.4byte .LASF6
 8406 2263 03          	.byte 0x3
 8407 2264 6B 12       	.2byte 0x126b
 8408 2266 6C 22 00 00 	.4byte 0x226c
 8409 226a 01          	.byte 0x1
 8410 226b 01          	.byte 0x1
 8411 226c 24          	.uleb128 0x24
 8412 226d DB 02 00 00 	.4byte 0x2db
 8413 2271 2B          	.uleb128 0x2b
 8414 2272 00 00 00 00 	.4byte .LASF7
 8415 2276 03          	.byte 0x3
 8416 2277 87 12       	.2byte 0x1287
 8417 2279 7F 22 00 00 	.4byte 0x227f
 8418 227d 01          	.byte 0x1
 8419 227e 01          	.byte 0x1
 8420 227f 24          	.uleb128 0x24
 8421 2280 B8 03 00 00 	.4byte 0x3b8
 8422 2284 2B          	.uleb128 0x2b
 8423 2285 00 00 00 00 	.4byte .LASF8
 8424 2289 03          	.byte 0x3
 8425 228a 9B 12       	.2byte 0x129b
 8426 228c 92 22 00 00 	.4byte 0x2292
 8427 2290 01          	.byte 0x1
 8428 2291 01          	.byte 0x1
 8429 2292 24          	.uleb128 0x24
 8430 2293 83 04 00 00 	.4byte 0x483
 8431 2297 2A          	.uleb128 0x2a
 8432 2298 55 31 49 52 	.asciz "U1IR"
 8432      00 
 8433 229d 03          	.byte 0x3
 8434 229e 9E 12       	.2byte 0x129e
 8435 22a0 46 22 00 00 	.4byte 0x2246
 8436 22a4 01          	.byte 0x1
 8437 22a5 01          	.byte 0x1
 8438 22a6 2B          	.uleb128 0x2b
 8439 22a7 00 00 00 00 	.4byte .LASF9
 8440 22ab 03          	.byte 0x3
 8441 22ac B2 12       	.2byte 0x12b2
 8442 22ae B4 22 00 00 	.4byte 0x22b4
 8443 22b2 01          	.byte 0x1
 8444 22b3 01          	.byte 0x1
 8445 22b4 24          	.uleb128 0x24
MPLAB XC16 ASSEMBLY Listing:   			page 217


 8446 22b5 9E 05 00 00 	.4byte 0x59e
 8447 22b9 2A          	.uleb128 0x2a
 8448 22ba 55 31 49 45 	.asciz "U1IE"
 8448      00 
 8449 22bf 03          	.byte 0x3
 8450 22c0 B5 12       	.2byte 0x12b5
 8451 22c2 46 22 00 00 	.4byte 0x2246
 8452 22c6 01          	.byte 0x1
 8453 22c7 01          	.byte 0x1
 8454 22c8 2B          	.uleb128 0x2b
 8455 22c9 00 00 00 00 	.4byte .LASF10
 8456 22cd 03          	.byte 0x3
 8457 22ce C9 12       	.2byte 0x12c9
 8458 22d0 D6 22 00 00 	.4byte 0x22d6
 8459 22d4 01          	.byte 0x1
 8460 22d5 01          	.byte 0x1
 8461 22d6 24          	.uleb128 0x24
 8462 22d7 B7 06 00 00 	.4byte 0x6b7
 8463 22db 2A          	.uleb128 0x2a
 8464 22dc 55 31 45 49 	.asciz "U1EIR"
 8464      52 00 
 8465 22e2 03          	.byte 0x3
 8466 22e3 CC 12       	.2byte 0x12cc
 8467 22e5 46 22 00 00 	.4byte 0x2246
 8468 22e9 01          	.byte 0x1
 8469 22ea 01          	.byte 0x1
 8470 22eb 2A          	.uleb128 0x2a
 8471 22ec 55 31 45 49 	.asciz "U1EIE"
 8471      45 00 
 8472 22f2 03          	.byte 0x3
 8473 22f3 E2 12       	.2byte 0x12e2
 8474 22f5 46 22 00 00 	.4byte 0x2246
 8475 22f9 01          	.byte 0x1
 8476 22fa 01          	.byte 0x1
 8477 22fb 2A          	.uleb128 0x2a
 8478 22fc 55 31 53 54 	.asciz "U1STAT"
 8478      41 54 00 
 8479 2303 03          	.byte 0x3
 8480 2304 F8 12       	.2byte 0x12f8
 8481 2306 46 22 00 00 	.4byte 0x2246
 8482 230a 01          	.byte 0x1
 8483 230b 01          	.byte 0x1
 8484 230c 2A          	.uleb128 0x2a
 8485 230d 55 31 43 4F 	.asciz "U1CON"
 8485      4E 00 
 8486 2313 03          	.byte 0x3
 8487 2314 0D 13       	.2byte 0x130d
 8488 2316 46 22 00 00 	.4byte 0x2246
 8489 231a 01          	.byte 0x1
 8490 231b 01          	.byte 0x1
 8491 231c 2B          	.uleb128 0x2b
 8492 231d 00 00 00 00 	.4byte .LASF11
 8493 2321 03          	.byte 0x3
 8494 2322 27 13       	.2byte 0x1327
 8495 2324 2A 23 00 00 	.4byte 0x232a
 8496 2328 01          	.byte 0x1
 8497 2329 01          	.byte 0x1
MPLAB XC16 ASSEMBLY Listing:   			page 218


 8498 232a 24          	.uleb128 0x24
 8499 232b 00 08 00 00 	.4byte 0x800
 8500 232f 2A          	.uleb128 0x2a
 8501 2330 55 31 41 44 	.asciz "U1ADDR"
 8501      44 52 00 
 8502 2337 03          	.byte 0x3
 8503 2338 2A 13       	.2byte 0x132a
 8504 233a 46 22 00 00 	.4byte 0x2246
 8505 233e 01          	.byte 0x1
 8506 233f 01          	.byte 0x1
 8507 2340 2A          	.uleb128 0x2a
 8508 2341 55 31 42 44 	.asciz "U1BDTP1"
 8508      54 50 31 00 
 8509 2349 03          	.byte 0x3
 8510 234a 40 13       	.2byte 0x1340
 8511 234c 46 22 00 00 	.4byte 0x2246
 8512 2350 01          	.byte 0x1
 8513 2351 01          	.byte 0x1
 8514 2352 2A          	.uleb128 0x2a
 8515 2353 55 31 43 4E 	.asciz "U1CNFG1"
 8515      46 47 31 00 
 8516 235b 03          	.byte 0x3
 8517 235c 8A 13       	.2byte 0x138a
 8518 235e 46 22 00 00 	.4byte 0x2246
 8519 2362 01          	.byte 0x1
 8520 2363 01          	.byte 0x1
 8521 2364 2A          	.uleb128 0x2a
 8522 2365 55 31 43 4E 	.asciz "U1CNFG2"
 8522      46 47 32 00 
 8523 236d 03          	.byte 0x3
 8524 236e 95 13       	.2byte 0x1395
 8525 2370 46 22 00 00 	.4byte 0x2246
 8526 2374 01          	.byte 0x1
 8527 2375 01          	.byte 0x1
 8528 2376 2A          	.uleb128 0x2a
 8529 2377 55 31 45 50 	.asciz "U1EP0"
 8529      30 00 
 8530 237d 03          	.byte 0x3
 8531 237e A1 13       	.2byte 0x13a1
 8532 2380 46 22 00 00 	.4byte 0x2246
 8533 2384 01          	.byte 0x1
 8534 2385 01          	.byte 0x1
 8535 2386 2B          	.uleb128 0x2b
 8536 2387 00 00 00 00 	.4byte .LASF12
 8537 238b 03          	.byte 0x3
 8538 238c B7 13       	.2byte 0x13b7
 8539 238e 94 23 00 00 	.4byte 0x2394
 8540 2392 01          	.byte 0x1
 8541 2393 01          	.byte 0x1
 8542 2394 24          	.uleb128 0x24
 8543 2395 2F 09 00 00 	.4byte 0x92f
 8544 2399 2A          	.uleb128 0x2a
 8545 239a 55 31 45 50 	.asciz "U1EP1"
 8545      31 00 
 8546 23a0 03          	.byte 0x3
 8547 23a1 BA 13       	.2byte 0x13ba
 8548 23a3 46 22 00 00 	.4byte 0x2246
MPLAB XC16 ASSEMBLY Listing:   			page 219


 8549 23a7 01          	.byte 0x1
 8550 23a8 01          	.byte 0x1
 8551 23a9 2B          	.uleb128 0x2b
 8552 23aa 00 00 00 00 	.4byte .LASF13
 8553 23ae 03          	.byte 0x3
 8554 23af 82 25       	.2byte 0x2582
 8555 23b1 B7 23 00 00 	.4byte 0x23b7
 8556 23b5 01          	.byte 0x1
 8557 23b6 01          	.byte 0x1
 8558 23b7 24          	.uleb128 0x24
 8559 23b8 65 0A 00 00 	.4byte 0xa65
 8560 23bc 2B          	.uleb128 0x2b
 8561 23bd 00 00 00 00 	.4byte .LASF14
 8562 23c1 03          	.byte 0x3
 8563 23c2 2E 26       	.2byte 0x262e
 8564 23c4 CA 23 00 00 	.4byte 0x23ca
 8565 23c8 01          	.byte 0x1
 8566 23c9 01          	.byte 0x1
 8567 23ca 24          	.uleb128 0x24
 8568 23cb 9A 0B 00 00 	.4byte 0xb9a
 8569 23cf 2B          	.uleb128 0x2b
 8570 23d0 00 00 00 00 	.4byte .LASF15
 8571 23d4 03          	.byte 0x3
 8572 23d5 04 29       	.2byte 0x2904
 8573 23d7 DD 23 00 00 	.4byte 0x23dd
 8574 23db 01          	.byte 0x1
 8575 23dc 01          	.byte 0x1
 8576 23dd 24          	.uleb128 0x24
 8577 23de 9C 0C 00 00 	.4byte 0xc9c
 8578 23e2 2B          	.uleb128 0x2b
 8579 23e3 00 00 00 00 	.4byte .LASF16
 8580 23e7 06          	.byte 0x6
 8581 23e8 0D 03       	.2byte 0x30d
 8582 23ea F0 23 00 00 	.4byte 0x23f0
 8583 23ee 01          	.byte 0x1
 8584 23ef 01          	.byte 0x1
 8585 23f0 24          	.uleb128 0x24
 8586 23f1 F4 0F 00 00 	.4byte 0xff4
 8587 23f5 2B          	.uleb128 0x2b
 8588 23f6 00 00 00 00 	.4byte .LASF17
 8589 23fa 06          	.byte 0x6
 8590 23fb 53 03       	.2byte 0x353
 8591 23fd F0 23 00 00 	.4byte 0x23f0
 8592 2401 01          	.byte 0x1
 8593 2402 01          	.byte 0x1
 8594 2403 2B          	.uleb128 0x2b
 8595 2404 00 00 00 00 	.4byte .LASF18
 8596 2408 06          	.byte 0x6
 8597 2409 DA 03       	.2byte 0x3da
 8598 240b F0 23 00 00 	.4byte 0x23f0
 8599 240f 01          	.byte 0x1
 8600 2410 01          	.byte 0x1
 8601 2411 14          	.uleb128 0x14
 8602 2412 55 18 00 00 	.4byte 0x1855
 8603 2416 21 24 00 00 	.4byte 0x2421
 8604 241a 15          	.uleb128 0x15
 8605 241b 12 01 00 00 	.4byte 0x112
MPLAB XC16 ASSEMBLY Listing:   			page 220


 8606 241f 00          	.byte 0x0
 8607 2420 00          	.byte 0x0
 8608 2421 2A          	.uleb128 0x2a
 8609 2422 69 6E 50 69 	.asciz "inPipes"
 8609      70 65 73 00 
 8610 242a 06          	.byte 0x6
 8611 242b E3 07       	.2byte 0x7e3
 8612 242d 33 24 00 00 	.4byte 0x2433
 8613 2431 01          	.byte 0x1
 8614 2432 01          	.byte 0x1
 8615 2433 24          	.uleb128 0x24
 8616 2434 11 24 00 00 	.4byte 0x2411
 8617 2438 2B          	.uleb128 0x2b
 8618 2439 00 00 00 00 	.4byte .LASF19
 8619 243d 06          	.byte 0x6
 8620 243e FC 07       	.2byte 0x7fc
 8621 2440 F0 23 00 00 	.4byte 0x23f0
 8622 2444 01          	.byte 0x1
 8623 2445 01          	.byte 0x1
 8624 2446 2B          	.uleb128 0x2b
 8625 2447 00 00 00 00 	.4byte .LASF20
 8626 244b 06          	.byte 0x6
 8627 244c FD 07       	.2byte 0x7fd
 8628 244e F0 23 00 00 	.4byte 0x23f0
 8629 2452 01          	.byte 0x1
 8630 2453 01          	.byte 0x1
 8631 2454 2B          	.uleb128 0x2b
 8632 2455 00 00 00 00 	.4byte .LASF21
 8633 2459 06          	.byte 0x6
 8634 245a FE 07       	.2byte 0x7fe
 8635 245c 62 24 00 00 	.4byte 0x2462
 8636 2460 01          	.byte 0x1
 8637 2461 01          	.byte 0x1
 8638 2462 24          	.uleb128 0x24
 8639 2463 40 16 00 00 	.4byte 0x1640
 8640 2467 2B          	.uleb128 0x2b
 8641 2468 00 00 00 00 	.4byte .LASF22
 8642 246c 06          	.byte 0x6
 8643 246d FF 07       	.2byte 0x7ff
 8644 246f 75 24 00 00 	.4byte 0x2475
 8645 2473 01          	.byte 0x1
 8646 2474 01          	.byte 0x1
 8647 2475 24          	.uleb128 0x24
 8648 2476 E2 00 00 00 	.4byte 0xe2
 8649 247a 2B          	.uleb128 0x2b
 8650 247b 00 00 00 00 	.4byte .LASF23
 8651 247f 06          	.byte 0x6
 8652 2480 00 08       	.2byte 0x800
 8653 2482 75 24 00 00 	.4byte 0x2475
 8654 2486 01          	.byte 0x1
 8655 2487 01          	.byte 0x1
 8656 2488 14          	.uleb128 0x14
 8657 2489 30 19 00 00 	.4byte 0x1930
 8658 248d 98 24 00 00 	.4byte 0x2498
 8659 2491 15          	.uleb128 0x15
 8660 2492 12 01 00 00 	.4byte 0x112
 8661 2496 00          	.byte 0x0
MPLAB XC16 ASSEMBLY Listing:   			page 221


 8662 2497 00          	.byte 0x0
 8663 2498 2B          	.uleb128 0x2b
 8664 2499 00 00 00 00 	.4byte .LASF24
 8665 249d 07          	.byte 0x7
 8666 249e CB 02       	.2byte 0x2cb
 8667 24a0 A6 24 00 00 	.4byte 0x24a6
 8668 24a4 01          	.byte 0x1
 8669 24a5 01          	.byte 0x1
 8670 24a6 24          	.uleb128 0x24
 8671 24a7 88 24 00 00 	.4byte 0x2488
 8672 24ab 14          	.uleb128 0x14
 8673 24ac 69 1D 00 00 	.4byte 0x1d69
 8674 24b0 BB 24 00 00 	.4byte 0x24bb
 8675 24b4 15          	.uleb128 0x15
 8676 24b5 12 01 00 00 	.4byte 0x112
 8677 24b9 04          	.byte 0x4
 8678 24ba 00          	.byte 0x0
 8679 24bb 2B          	.uleb128 0x2b
 8680 24bc 00 00 00 00 	.4byte .LASF25
 8681 24c0 07          	.byte 0x7
 8682 24c1 CC 02       	.2byte 0x2cc
 8683 24c3 AB 24 00 00 	.4byte 0x24ab
 8684 24c7 01          	.byte 0x1
 8685 24c8 01          	.byte 0x1
 8686 24c9 2B          	.uleb128 0x2b
 8687 24ca 00 00 00 00 	.4byte .LASF26
 8688 24ce 07          	.byte 0x7
 8689 24cf CD 02       	.2byte 0x2cd
 8690 24d1 AB 24 00 00 	.4byte 0x24ab
 8691 24d5 01          	.byte 0x1
 8692 24d6 01          	.byte 0x1
 8693 24d7 2C          	.uleb128 0x2c
 8694 24d8 00 00 00 00 	.4byte .LASF27
 8695 24dc 01          	.byte 0x1
 8696 24dd 90          	.byte 0x90
 8697 24de E4 24 00 00 	.4byte 0x24e4
 8698 24e2 01          	.byte 0x1
 8699 24e3 01          	.byte 0x1
 8700 24e4 24          	.uleb128 0x24
 8701 24e5 22 12 00 00 	.4byte 0x1222
 8702 24e9 2C          	.uleb128 0x2c
 8703 24ea 00 00 00 00 	.4byte .LASF28
 8704 24ee 01          	.byte 0x1
 8705 24ef 91          	.byte 0x91
 8706 24f0 69 1D 00 00 	.4byte 0x1d69
 8707 24f4 01          	.byte 0x1
 8708 24f5 01          	.byte 0x1
 8709 24f6 2C          	.uleb128 0x2c
 8710 24f7 00 00 00 00 	.4byte .LASF29
 8711 24fb 01          	.byte 0x1
 8712 24fc 92          	.byte 0x92
 8713 24fd 69 1D 00 00 	.4byte 0x1d69
 8714 2501 01          	.byte 0x1
 8715 2502 01          	.byte 0x1
 8716 2503 2C          	.uleb128 0x2c
 8717 2504 00 00 00 00 	.4byte .LASF30
 8718 2508 01          	.byte 0x1
MPLAB XC16 ASSEMBLY Listing:   			page 222


 8719 2509 95          	.byte 0x95
 8720 250a 75 24 00 00 	.4byte 0x2475
 8721 250e 01          	.byte 0x1
 8722 250f 01          	.byte 0x1
 8723 2510 2C          	.uleb128 0x2c
 8724 2511 00 00 00 00 	.4byte .LASF31
 8725 2515 01          	.byte 0x1
 8726 2516 96          	.byte 0x96
 8727 2517 75 24 00 00 	.4byte 0x2475
 8728 251b 01          	.byte 0x1
 8729 251c 01          	.byte 0x1
 8730 251d 2D          	.uleb128 0x2d
 8731 251e 70 44 73 74 	.asciz "pDst"
 8731      00 
 8732 2523 01          	.byte 0x1
 8733 2524 9A          	.byte 0x9a
 8734 2525 2B 25 00 00 	.4byte 0x252b
 8735 2529 01          	.byte 0x1
 8736 252a 01          	.byte 0x1
 8737 252b 18          	.uleb128 0x18
 8738 252c 02          	.byte 0x2
 8739 252d 75 24 00 00 	.4byte 0x2475
 8740 2531 2C          	.uleb128 0x2c
 8741 2532 00 00 00 00 	.4byte .LASF32
 8742 2536 01          	.byte 0x1
 8743 2537 9D          	.byte 0x9d
 8744 2538 3E 25 00 00 	.4byte 0x253e
 8745 253c 01          	.byte 0x1
 8746 253d 01          	.byte 0x1
 8747 253e 24          	.uleb128 0x24
 8748 253f A4 1B 00 00 	.4byte 0x1ba4
 8749 2543 2C          	.uleb128 0x2c
 8750 2544 00 00 00 00 	.4byte .LASF4
 8751 2548 01          	.byte 0x1
 8752 2549 9E          	.byte 0x9e
 8753 254a 75 24 00 00 	.4byte 0x2475
 8754 254e 01          	.byte 0x1
 8755 254f 01          	.byte 0x1
 8756 2550 2C          	.uleb128 0x2c
 8757 2551 00 00 00 00 	.4byte .LASF33
 8758 2555 01          	.byte 0x1
 8759 2556 9F          	.byte 0x9f
 8760 2557 F0 23 00 00 	.4byte 0x23f0
 8761 255b 01          	.byte 0x1
 8762 255c 01          	.byte 0x1
 8763 255d 14          	.uleb128 0x14
 8764 255e 20 1C 00 00 	.4byte 0x1c20
 8765 2562 6D 25 00 00 	.4byte 0x256d
 8766 2566 15          	.uleb128 0x15
 8767 2567 12 01 00 00 	.4byte 0x112
 8768 256b 04          	.byte 0x4
 8769 256c 00          	.byte 0x0
 8770 256d 2C          	.uleb128 0x2c
 8771 256e 00 00 00 00 	.4byte .LASF34
 8772 2572 01          	.byte 0x1
 8773 2573 A0          	.byte 0xa0
 8774 2574 7A 25 00 00 	.4byte 0x257a
MPLAB XC16 ASSEMBLY Listing:   			page 223


 8775 2578 01          	.byte 0x1
 8776 2579 01          	.byte 0x1
 8777 257a 24          	.uleb128 0x24
 8778 257b 5D 25 00 00 	.4byte 0x255d
 8779 257f 2C          	.uleb128 0x2c
 8780 2580 00 00 00 00 	.4byte .LASF35
 8781 2584 01          	.byte 0x1
 8782 2585 A1          	.byte 0xa1
 8783 2586 8C 25 00 00 	.4byte 0x258c
 8784 258a 01          	.byte 0x1
 8785 258b 01          	.byte 0x1
 8786 258c 24          	.uleb128 0x24
 8787 258d 5D 25 00 00 	.4byte 0x255d
 8788 2591 2C          	.uleb128 0x2c
 8789 2592 00 00 00 00 	.4byte .LASF36
 8790 2596 01          	.byte 0x1
 8791 2597 A2          	.byte 0xa2
 8792 2598 75 24 00 00 	.4byte 0x2475
 8793 259c 01          	.byte 0x1
 8794 259d 01          	.byte 0x1
 8795 259e 2C          	.uleb128 0x2c
 8796 259f 00 00 00 00 	.4byte .LASF37
 8797 25a3 01          	.byte 0x1
 8798 25a4 A4          	.byte 0xa4
 8799 25a5 F0 23 00 00 	.4byte 0x23f0
 8800 25a9 01          	.byte 0x1
 8801 25aa 01          	.byte 0x1
 8802 25ab 2C          	.uleb128 0x2c
 8803 25ac 00 00 00 00 	.4byte .LASF38
 8804 25b0 01          	.byte 0x1
 8805 25b1 A5          	.byte 0xa5
 8806 25b2 F0 23 00 00 	.4byte 0x23f0
 8807 25b6 01          	.byte 0x1
 8808 25b7 01          	.byte 0x1
 8809 25b8 2C          	.uleb128 0x2c
 8810 25b9 00 00 00 00 	.4byte .LASF39
 8811 25bd 01          	.byte 0x1
 8812 25be A8          	.byte 0xa8
 8813 25bf C5 25 00 00 	.4byte 0x25c5
 8814 25c3 01          	.byte 0x1
 8815 25c4 01          	.byte 0x1
 8816 25c5 24          	.uleb128 0x24
 8817 25c6 22 01 00 00 	.4byte 0x122
 8818 25ca 14          	.uleb128 0x14
 8819 25cb 15 1B 00 00 	.4byte 0x1b15
 8820 25cf DA 25 00 00 	.4byte 0x25da
 8821 25d3 15          	.uleb128 0x15
 8822 25d4 12 01 00 00 	.4byte 0x112
 8823 25d8 13          	.byte 0x13
 8824 25d9 00          	.byte 0x0
 8825 25da 2D          	.uleb128 0x2d
 8826 25db 42 44 54 00 	.asciz "BDT"
 8827 25df 01          	.byte 0x1
 8828 25e0 B0          	.byte 0xb0
 8829 25e1 E7 25 00 00 	.4byte 0x25e7
 8830 25e5 01          	.byte 0x1
 8831 25e6 01          	.byte 0x1
MPLAB XC16 ASSEMBLY Listing:   			page 224


 8832 25e7 24          	.uleb128 0x24
 8833 25e8 CA 25 00 00 	.4byte 0x25ca
 8834 25ec 2C          	.uleb128 0x2c
 8835 25ed 00 00 00 00 	.4byte .LASF40
 8836 25f1 01          	.byte 0x1
 8837 25f2 B5          	.byte 0xb5
 8838 25f3 F9 25 00 00 	.4byte 0x25f9
 8839 25f7 01          	.byte 0x1
 8840 25f8 01          	.byte 0x1
 8841 25f9 24          	.uleb128 0x24
 8842 25fa A7 15 00 00 	.4byte 0x15a7
 8843 25fe 14          	.uleb128 0x14
 8844 25ff E2 00 00 00 	.4byte 0xe2
 8845 2603 0E 26 00 00 	.4byte 0x260e
 8846 2607 15          	.uleb128 0x15
 8847 2608 12 01 00 00 	.4byte 0x112
 8848 260c 07          	.byte 0x7
 8849 260d 00          	.byte 0x0
 8850 260e 2C          	.uleb128 0x2c
 8851 260f 00 00 00 00 	.4byte .LASF41
 8852 2613 01          	.byte 0x1
 8853 2614 B6          	.byte 0xb6
 8854 2615 1B 26 00 00 	.4byte 0x261b
 8855 2619 01          	.byte 0x1
 8856 261a 01          	.byte 0x1
 8857 261b 24          	.uleb128 0x24
 8858 261c FE 25 00 00 	.4byte 0x25fe
 8859 2620 2C          	.uleb128 0x2c
 8860 2621 00 00 00 00 	.4byte .LASF42
 8861 2625 01          	.byte 0x1
 8862 2626 D5          	.byte 0xd5
 8863 2627 2D 26 00 00 	.4byte 0x262d
 8864 262b 01          	.byte 0x1
 8865 262c 01          	.byte 0x1
 8866 262d 19          	.uleb128 0x19
 8867 262e 4E 11 00 00 	.4byte 0x114e
 8868 2632 14          	.uleb128 0x14
 8869 2633 76 17 00 00 	.4byte 0x1776
 8870 2637 3D 26 00 00 	.4byte 0x263d
 8871 263b 2E          	.uleb128 0x2e
 8872 263c 00          	.byte 0x0
 8873 263d 2C          	.uleb128 0x2c
 8874 263e 00 00 00 00 	.4byte .LASF43
 8875 2642 01          	.byte 0x1
 8876 2643 DC          	.byte 0xdc
 8877 2644 4A 26 00 00 	.4byte 0x264a
 8878 2648 01          	.byte 0x1
 8879 2649 01          	.byte 0x1
 8880 264a 19          	.uleb128 0x19
 8881 264b 32 26 00 00 	.4byte 0x2632
 8882 264f 2C          	.uleb128 0x2c
 8883 2650 00 00 00 00 	.4byte .LASF44
 8884 2654 01          	.byte 0x1
 8885 2655 DF          	.byte 0xdf
 8886 2656 5C 26 00 00 	.4byte 0x265c
 8887 265a 01          	.byte 0x1
 8888 265b 01          	.byte 0x1
MPLAB XC16 ASSEMBLY Listing:   			page 225


 8889 265c 19          	.uleb128 0x19
 8890 265d 32 26 00 00 	.4byte 0x2632
 8891 2661 2A          	.uleb128 0x2a
 8892 2662 55 31 4F 54 	.asciz "U1OTGIR"
 8892      47 49 52 00 
 8893 266a 03          	.byte 0x3
 8894 266b 52 12       	.2byte 0x1252
 8895 266d 46 22 00 00 	.4byte 0x2246
 8896 2671 01          	.byte 0x1
 8897 2672 01          	.byte 0x1
 8898 2673 2B          	.uleb128 0x2b
 8899 2674 00 00 00 00 	.4byte .LASF5
 8900 2678 03          	.byte 0x3
 8901 2679 5D 12       	.2byte 0x125d
 8902 267b 59 22 00 00 	.4byte 0x2259
 8903 267f 01          	.byte 0x1
 8904 2680 01          	.byte 0x1
 8905 2681 2B          	.uleb128 0x2b
 8906 2682 00 00 00 00 	.4byte .LASF6
 8907 2686 03          	.byte 0x3
 8908 2687 6B 12       	.2byte 0x126b
 8909 2689 6C 22 00 00 	.4byte 0x226c
 8910 268d 01          	.byte 0x1
 8911 268e 01          	.byte 0x1
 8912 268f 2B          	.uleb128 0x2b
 8913 2690 00 00 00 00 	.4byte .LASF7
 8914 2694 03          	.byte 0x3
 8915 2695 87 12       	.2byte 0x1287
 8916 2697 7F 22 00 00 	.4byte 0x227f
 8917 269b 01          	.byte 0x1
 8918 269c 01          	.byte 0x1
 8919 269d 2B          	.uleb128 0x2b
 8920 269e 00 00 00 00 	.4byte .LASF8
 8921 26a2 03          	.byte 0x3
 8922 26a3 9B 12       	.2byte 0x129b
 8923 26a5 92 22 00 00 	.4byte 0x2292
 8924 26a9 01          	.byte 0x1
 8925 26aa 01          	.byte 0x1
 8926 26ab 2A          	.uleb128 0x2a
 8927 26ac 55 31 49 52 	.asciz "U1IR"
 8927      00 
 8928 26b1 03          	.byte 0x3
 8929 26b2 9E 12       	.2byte 0x129e
 8930 26b4 46 22 00 00 	.4byte 0x2246
 8931 26b8 01          	.byte 0x1
 8932 26b9 01          	.byte 0x1
 8933 26ba 2B          	.uleb128 0x2b
 8934 26bb 00 00 00 00 	.4byte .LASF9
 8935 26bf 03          	.byte 0x3
 8936 26c0 B2 12       	.2byte 0x12b2
 8937 26c2 B4 22 00 00 	.4byte 0x22b4
 8938 26c6 01          	.byte 0x1
 8939 26c7 01          	.byte 0x1
 8940 26c8 2A          	.uleb128 0x2a
 8941 26c9 55 31 49 45 	.asciz "U1IE"
 8941      00 
 8942 26ce 03          	.byte 0x3
MPLAB XC16 ASSEMBLY Listing:   			page 226


 8943 26cf B5 12       	.2byte 0x12b5
 8944 26d1 46 22 00 00 	.4byte 0x2246
 8945 26d5 01          	.byte 0x1
 8946 26d6 01          	.byte 0x1
 8947 26d7 2B          	.uleb128 0x2b
 8948 26d8 00 00 00 00 	.4byte .LASF10
 8949 26dc 03          	.byte 0x3
 8950 26dd C9 12       	.2byte 0x12c9
 8951 26df D6 22 00 00 	.4byte 0x22d6
 8952 26e3 01          	.byte 0x1
 8953 26e4 01          	.byte 0x1
 8954 26e5 2A          	.uleb128 0x2a
 8955 26e6 55 31 45 49 	.asciz "U1EIR"
 8955      52 00 
 8956 26ec 03          	.byte 0x3
 8957 26ed CC 12       	.2byte 0x12cc
 8958 26ef 46 22 00 00 	.4byte 0x2246
 8959 26f3 01          	.byte 0x1
 8960 26f4 01          	.byte 0x1
 8961 26f5 2A          	.uleb128 0x2a
 8962 26f6 55 31 45 49 	.asciz "U1EIE"
 8962      45 00 
 8963 26fc 03          	.byte 0x3
 8964 26fd E2 12       	.2byte 0x12e2
 8965 26ff 46 22 00 00 	.4byte 0x2246
 8966 2703 01          	.byte 0x1
 8967 2704 01          	.byte 0x1
 8968 2705 2A          	.uleb128 0x2a
 8969 2706 55 31 53 54 	.asciz "U1STAT"
 8969      41 54 00 
 8970 270d 03          	.byte 0x3
 8971 270e F8 12       	.2byte 0x12f8
 8972 2710 46 22 00 00 	.4byte 0x2246
 8973 2714 01          	.byte 0x1
 8974 2715 01          	.byte 0x1
 8975 2716 2A          	.uleb128 0x2a
 8976 2717 55 31 43 4F 	.asciz "U1CON"
 8976      4E 00 
 8977 271d 03          	.byte 0x3
 8978 271e 0D 13       	.2byte 0x130d
 8979 2720 46 22 00 00 	.4byte 0x2246
 8980 2724 01          	.byte 0x1
 8981 2725 01          	.byte 0x1
 8982 2726 2B          	.uleb128 0x2b
 8983 2727 00 00 00 00 	.4byte .LASF11
 8984 272b 03          	.byte 0x3
 8985 272c 27 13       	.2byte 0x1327
 8986 272e 2A 23 00 00 	.4byte 0x232a
 8987 2732 01          	.byte 0x1
 8988 2733 01          	.byte 0x1
 8989 2734 2A          	.uleb128 0x2a
 8990 2735 55 31 41 44 	.asciz "U1ADDR"
 8990      44 52 00 
 8991 273c 03          	.byte 0x3
 8992 273d 2A 13       	.2byte 0x132a
 8993 273f 46 22 00 00 	.4byte 0x2246
 8994 2743 01          	.byte 0x1
MPLAB XC16 ASSEMBLY Listing:   			page 227


 8995 2744 01          	.byte 0x1
 8996 2745 2A          	.uleb128 0x2a
 8997 2746 55 31 42 44 	.asciz "U1BDTP1"
 8997      54 50 31 00 
 8998 274e 03          	.byte 0x3
 8999 274f 40 13       	.2byte 0x1340
 9000 2751 46 22 00 00 	.4byte 0x2246
 9001 2755 01          	.byte 0x1
 9002 2756 01          	.byte 0x1
 9003 2757 2A          	.uleb128 0x2a
 9004 2758 55 31 43 4E 	.asciz "U1CNFG1"
 9004      46 47 31 00 
 9005 2760 03          	.byte 0x3
 9006 2761 8A 13       	.2byte 0x138a
 9007 2763 46 22 00 00 	.4byte 0x2246
 9008 2767 01          	.byte 0x1
 9009 2768 01          	.byte 0x1
 9010 2769 2A          	.uleb128 0x2a
 9011 276a 55 31 43 4E 	.asciz "U1CNFG2"
 9011      46 47 32 00 
 9012 2772 03          	.byte 0x3
 9013 2773 95 13       	.2byte 0x1395
 9014 2775 46 22 00 00 	.4byte 0x2246
 9015 2779 01          	.byte 0x1
 9016 277a 01          	.byte 0x1
 9017 277b 2A          	.uleb128 0x2a
 9018 277c 55 31 45 50 	.asciz "U1EP0"
 9018      30 00 
 9019 2782 03          	.byte 0x3
 9020 2783 A1 13       	.2byte 0x13a1
 9021 2785 46 22 00 00 	.4byte 0x2246
 9022 2789 01          	.byte 0x1
 9023 278a 01          	.byte 0x1
 9024 278b 2B          	.uleb128 0x2b
 9025 278c 00 00 00 00 	.4byte .LASF12
 9026 2790 03          	.byte 0x3
 9027 2791 B7 13       	.2byte 0x13b7
 9028 2793 94 23 00 00 	.4byte 0x2394
 9029 2797 01          	.byte 0x1
 9030 2798 01          	.byte 0x1
 9031 2799 2A          	.uleb128 0x2a
 9032 279a 55 31 45 50 	.asciz "U1EP1"
 9032      31 00 
 9033 27a0 03          	.byte 0x3
 9034 27a1 BA 13       	.2byte 0x13ba
 9035 27a3 46 22 00 00 	.4byte 0x2246
 9036 27a7 01          	.byte 0x1
 9037 27a8 01          	.byte 0x1
 9038 27a9 2B          	.uleb128 0x2b
 9039 27aa 00 00 00 00 	.4byte .LASF13
 9040 27ae 03          	.byte 0x3
 9041 27af 82 25       	.2byte 0x2582
 9042 27b1 B7 23 00 00 	.4byte 0x23b7
 9043 27b5 01          	.byte 0x1
 9044 27b6 01          	.byte 0x1
 9045 27b7 2B          	.uleb128 0x2b
 9046 27b8 00 00 00 00 	.4byte .LASF14
MPLAB XC16 ASSEMBLY Listing:   			page 228


 9047 27bc 03          	.byte 0x3
 9048 27bd 2E 26       	.2byte 0x262e
 9049 27bf CA 23 00 00 	.4byte 0x23ca
 9050 27c3 01          	.byte 0x1
 9051 27c4 01          	.byte 0x1
 9052 27c5 2B          	.uleb128 0x2b
 9053 27c6 00 00 00 00 	.4byte .LASF15
 9054 27ca 03          	.byte 0x3
 9055 27cb 04 29       	.2byte 0x2904
 9056 27cd DD 23 00 00 	.4byte 0x23dd
 9057 27d1 01          	.byte 0x1
 9058 27d2 01          	.byte 0x1
 9059 27d3 2F          	.uleb128 0x2f
 9060 27d4 00 00 00 00 	.4byte .LASF16
 9061 27d8 01          	.byte 0x1
 9062 27d9 A7          	.byte 0xa7
 9063 27da F0 23 00 00 	.4byte 0x23f0
 9064 27de 01          	.byte 0x1
 9065 27df 05          	.byte 0x5
 9066 27e0 03          	.byte 0x3
 9067 27e1 00 00 00 00 	.4byte _USBDeferOUTDataStagePackets
 9068 27e5 2F          	.uleb128 0x2f
 9069 27e6 00 00 00 00 	.4byte .LASF17
 9070 27ea 01          	.byte 0x1
 9071 27eb A3          	.byte 0xa3
 9072 27ec F0 23 00 00 	.4byte 0x23f0
 9073 27f0 01          	.byte 0x1
 9074 27f1 05          	.byte 0x5
 9075 27f2 03          	.byte 0x3
 9076 27f3 00 00 00 00 	.4byte _USBDeferStatusStagePacket
 9077 27f7 2F          	.uleb128 0x2f
 9078 27f8 00 00 00 00 	.4byte .LASF18
 9079 27fc 01          	.byte 0x1
 9080 27fd A6          	.byte 0xa6
 9081 27fe F0 23 00 00 	.4byte 0x23f0
 9082 2802 01          	.byte 0x1
 9083 2803 05          	.byte 0x5
 9084 2804 03          	.byte 0x3
 9085 2805 00 00 00 00 	.4byte _USBDeferINDataStagePackets
 9086 2809 30          	.uleb128 0x30
 9087 280a 69 6E 50 69 	.asciz "inPipes"
 9087      70 65 73 00 
 9088 2812 01          	.byte 0x1
 9089 2813 97          	.byte 0x97
 9090 2814 1F 28 00 00 	.4byte 0x281f
 9091 2818 01          	.byte 0x1
 9092 2819 05          	.byte 0x5
 9093 281a 03          	.byte 0x3
 9094 281b 00 00 00 00 	.4byte _inPipes
 9095 281f 24          	.uleb128 0x24
 9096 2820 11 24 00 00 	.4byte 0x2411
 9097 2824 2F          	.uleb128 0x2f
 9098 2825 00 00 00 00 	.4byte .LASF19
 9099 2829 01          	.byte 0x1
 9100 282a 9B          	.byte 0x9b
 9101 282b F0 23 00 00 	.4byte 0x23f0
 9102 282f 01          	.byte 0x1
MPLAB XC16 ASSEMBLY Listing:   			page 229


 9103 2830 05          	.byte 0x5
 9104 2831 03          	.byte 0x3
 9105 2832 00 00 00 00 	.4byte _RemoteWakeup
 9106 2836 2F          	.uleb128 0x2f
 9107 2837 00 00 00 00 	.4byte .LASF20
 9108 283b 01          	.byte 0x1
 9109 283c 9C          	.byte 0x9c
 9110 283d F0 23 00 00 	.4byte 0x23f0
 9111 2841 01          	.byte 0x1
 9112 2842 05          	.byte 0x5
 9113 2843 03          	.byte 0x3
 9114 2844 00 00 00 00 	.4byte _USBBusIsSuspended
 9115 2848 2F          	.uleb128 0x2f
 9116 2849 00 00 00 00 	.4byte .LASF21
 9117 284d 01          	.byte 0x1
 9118 284e 8E          	.byte 0x8e
 9119 284f 62 24 00 00 	.4byte 0x2462
 9120 2853 01          	.byte 0x1
 9121 2854 05          	.byte 0x5
 9122 2855 03          	.byte 0x3
 9123 2856 00 00 00 00 	.4byte _USBDeviceState
 9124 285a 2F          	.uleb128 0x2f
 9125 285b 00 00 00 00 	.4byte .LASF22
 9126 285f 01          	.byte 0x1
 9127 2860 8F          	.byte 0x8f
 9128 2861 75 24 00 00 	.4byte 0x2475
 9129 2865 01          	.byte 0x1
 9130 2866 05          	.byte 0x5
 9131 2867 03          	.byte 0x3
 9132 2868 00 00 00 00 	.4byte _USBActiveConfiguration
 9133 286c 2F          	.uleb128 0x2f
 9134 286d 00 00 00 00 	.4byte .LASF23
 9135 2871 01          	.byte 0x1
 9136 2872 A9          	.byte 0xa9
 9137 2873 75 24 00 00 	.4byte 0x2475
 9138 2877 01          	.byte 0x1
 9139 2878 05          	.byte 0x5
 9140 2879 03          	.byte 0x3
 9141 287a 00 00 00 00 	.4byte _USBTicksSinceSuspendEnd
 9142 287e 2F          	.uleb128 0x2f
 9143 287f 00 00 00 00 	.4byte .LASF24
 9144 2883 01          	.byte 0x1
 9145 2884 99          	.byte 0x99
 9146 2885 90 28 00 00 	.4byte 0x2890
 9147 2889 01          	.byte 0x1
 9148 288a 05          	.byte 0x5
 9149 288b 03          	.byte 0x3
 9150 288c 00 00 00 00 	.4byte _outPipes
 9151 2890 24          	.uleb128 0x24
 9152 2891 88 24 00 00 	.4byte 0x2488
 9153 2895 2F          	.uleb128 0x2f
 9154 2896 00 00 00 00 	.4byte .LASF25
 9155 289a 01          	.byte 0x1
 9156 289b 93          	.byte 0x93
 9157 289c AB 24 00 00 	.4byte 0x24ab
 9158 28a0 01          	.byte 0x1
 9159 28a1 05          	.byte 0x5
MPLAB XC16 ASSEMBLY Listing:   			page 230


 9160 28a2 03          	.byte 0x3
 9161 28a3 00 00 00 00 	.4byte _pBDTEntryOut
 9162 28a7 2F          	.uleb128 0x2f
 9163 28a8 00 00 00 00 	.4byte .LASF26
 9164 28ac 01          	.byte 0x1
 9165 28ad 94          	.byte 0x94
 9166 28ae AB 24 00 00 	.4byte 0x24ab
 9167 28b2 01          	.byte 0x1
 9168 28b3 05          	.byte 0x5
 9169 28b4 03          	.byte 0x3
 9170 28b5 00 00 00 00 	.4byte _pBDTEntryIn
 9171 28b9 2F          	.uleb128 0x2f
 9172 28ba 00 00 00 00 	.4byte .LASF27
 9173 28be 01          	.byte 0x1
 9174 28bf 90          	.byte 0x90
 9175 28c0 CB 28 00 00 	.4byte 0x28cb
 9176 28c4 01          	.byte 0x1
 9177 28c5 05          	.byte 0x5
 9178 28c6 03          	.byte 0x3
 9179 28c7 00 00 00 00 	.4byte _USBAlternateInterface
 9180 28cb 24          	.uleb128 0x24
 9181 28cc 22 12 00 00 	.4byte 0x1222
 9182 28d0 2F          	.uleb128 0x2f
 9183 28d1 00 00 00 00 	.4byte .LASF28
 9184 28d5 01          	.byte 0x1
 9185 28d6 91          	.byte 0x91
 9186 28d7 69 1D 00 00 	.4byte 0x1d69
 9187 28db 01          	.byte 0x1
 9188 28dc 05          	.byte 0x5
 9189 28dd 03          	.byte 0x3
 9190 28de 00 00 00 00 	.4byte _pBDTEntryEP0OutCurrent
 9191 28e2 2F          	.uleb128 0x2f
 9192 28e3 00 00 00 00 	.4byte .LASF29
 9193 28e7 01          	.byte 0x1
 9194 28e8 92          	.byte 0x92
 9195 28e9 69 1D 00 00 	.4byte 0x1d69
 9196 28ed 01          	.byte 0x1
 9197 28ee 05          	.byte 0x5
 9198 28ef 03          	.byte 0x3
 9199 28f0 00 00 00 00 	.4byte _pBDTEntryEP0OutNext
 9200 28f4 2F          	.uleb128 0x2f
 9201 28f5 00 00 00 00 	.4byte .LASF30
 9202 28f9 01          	.byte 0x1
 9203 28fa 95          	.byte 0x95
 9204 28fb 75 24 00 00 	.4byte 0x2475
 9205 28ff 01          	.byte 0x1
 9206 2900 05          	.byte 0x5
 9207 2901 03          	.byte 0x3
 9208 2902 00 00 00 00 	.4byte _shortPacketStatus
 9209 2906 2F          	.uleb128 0x2f
 9210 2907 00 00 00 00 	.4byte .LASF31
 9211 290b 01          	.byte 0x1
 9212 290c 96          	.byte 0x96
 9213 290d 75 24 00 00 	.4byte 0x2475
 9214 2911 01          	.byte 0x1
 9215 2912 05          	.byte 0x5
 9216 2913 03          	.byte 0x3
MPLAB XC16 ASSEMBLY Listing:   			page 231


 9217 2914 00 00 00 00 	.4byte _controlTransferState
 9218 2918 30          	.uleb128 0x30
 9219 2919 70 44 73 74 	.asciz "pDst"
 9219      00 
 9220 291e 01          	.byte 0x1
 9221 291f 9A          	.byte 0x9a
 9222 2920 2B 25 00 00 	.4byte 0x252b
 9223 2924 01          	.byte 0x1
 9224 2925 05          	.byte 0x5
 9225 2926 03          	.byte 0x3
 9226 2927 00 00 00 00 	.4byte _pDst
 9227 292b 2F          	.uleb128 0x2f
 9228 292c 00 00 00 00 	.4byte .LASF32
 9229 2930 01          	.byte 0x1
 9230 2931 9D          	.byte 0x9d
 9231 2932 3E 25 00 00 	.4byte 0x253e
 9232 2936 01          	.byte 0x1
 9233 2937 05          	.byte 0x5
 9234 2938 03          	.byte 0x3
 9235 2939 00 00 00 00 	.4byte _USTATcopy
 9236 293d 2F          	.uleb128 0x2f
 9237 293e 00 00 00 00 	.4byte .LASF4
 9238 2942 01          	.byte 0x1
 9239 2943 9E          	.byte 0x9e
 9240 2944 75 24 00 00 	.4byte 0x2475
 9241 2948 01          	.byte 0x1
 9242 2949 05          	.byte 0x5
 9243 294a 03          	.byte 0x3
 9244 294b 00 00 00 00 	.4byte _endpoint_number
 9245 294f 2F          	.uleb128 0x2f
 9246 2950 00 00 00 00 	.4byte .LASF33
 9247 2954 01          	.byte 0x1
 9248 2955 9F          	.byte 0x9f
 9249 2956 F0 23 00 00 	.4byte 0x23f0
 9250 295a 01          	.byte 0x1
 9251 295b 05          	.byte 0x5
 9252 295c 03          	.byte 0x3
 9253 295d 00 00 00 00 	.4byte _BothEP0OutUOWNsSet
 9254 2961 2F          	.uleb128 0x2f
 9255 2962 00 00 00 00 	.4byte .LASF34
 9256 2966 01          	.byte 0x1
 9257 2967 A0          	.byte 0xa0
 9258 2968 73 29 00 00 	.4byte 0x2973
 9259 296c 01          	.byte 0x1
 9260 296d 05          	.byte 0x5
 9261 296e 03          	.byte 0x3
 9262 296f 00 00 00 00 	.4byte _ep_data_in
 9263 2973 24          	.uleb128 0x24
 9264 2974 5D 25 00 00 	.4byte 0x255d
 9265 2978 2F          	.uleb128 0x2f
 9266 2979 00 00 00 00 	.4byte .LASF35
 9267 297d 01          	.byte 0x1
 9268 297e A1          	.byte 0xa1
 9269 297f 8A 29 00 00 	.4byte 0x298a
 9270 2983 01          	.byte 0x1
 9271 2984 05          	.byte 0x5
 9272 2985 03          	.byte 0x3
MPLAB XC16 ASSEMBLY Listing:   			page 232


 9273 2986 00 00 00 00 	.4byte _ep_data_out
 9274 298a 24          	.uleb128 0x24
 9275 298b 5D 25 00 00 	.4byte 0x255d
 9276 298f 2F          	.uleb128 0x2f
 9277 2990 00 00 00 00 	.4byte .LASF36
 9278 2994 01          	.byte 0x1
 9279 2995 A2          	.byte 0xa2
 9280 2996 75 24 00 00 	.4byte 0x2475
 9281 299a 01          	.byte 0x1
 9282 299b 05          	.byte 0x5
 9283 299c 03          	.byte 0x3
 9284 299d 00 00 00 00 	.4byte _USBStatusStageTimeoutCounter
 9285 29a1 2F          	.uleb128 0x2f
 9286 29a2 00 00 00 00 	.4byte .LASF37
 9287 29a6 01          	.byte 0x1
 9288 29a7 A4          	.byte 0xa4
 9289 29a8 F0 23 00 00 	.4byte 0x23f0
 9290 29ac 01          	.byte 0x1
 9291 29ad 05          	.byte 0x5
 9292 29ae 03          	.byte 0x3
 9293 29af 00 00 00 00 	.4byte _USBStatusStageEnabledFlag1
 9294 29b3 2F          	.uleb128 0x2f
 9295 29b4 00 00 00 00 	.4byte .LASF38
 9296 29b8 01          	.byte 0x1
 9297 29b9 A5          	.byte 0xa5
 9298 29ba F0 23 00 00 	.4byte 0x23f0
 9299 29be 01          	.byte 0x1
 9300 29bf 05          	.byte 0x5
 9301 29c0 03          	.byte 0x3
 9302 29c1 00 00 00 00 	.4byte _USBStatusStageEnabledFlag2
 9303 29c5 2F          	.uleb128 0x2f
 9304 29c6 00 00 00 00 	.4byte .LASF39
 9305 29ca 01          	.byte 0x1
 9306 29cb A8          	.byte 0xa8
 9307 29cc C5 25 00 00 	.4byte 0x25c5
 9308 29d0 01          	.byte 0x1
 9309 29d1 05          	.byte 0x5
 9310 29d2 03          	.byte 0x3
 9311 29d3 00 00 00 00 	.4byte _USB1msTickCount
 9312 29d7 30          	.uleb128 0x30
 9313 29d8 42 44 54 00 	.asciz "BDT"
 9314 29dc 01          	.byte 0x1
 9315 29dd B0          	.byte 0xb0
 9316 29de E9 29 00 00 	.4byte 0x29e9
 9317 29e2 01          	.byte 0x1
 9318 29e3 05          	.byte 0x5
 9319 29e4 03          	.byte 0x3
 9320 29e5 00 00 00 00 	.4byte _BDT
 9321 29e9 24          	.uleb128 0x24
 9322 29ea CA 25 00 00 	.4byte 0x25ca
 9323 29ee 2F          	.uleb128 0x2f
 9324 29ef 00 00 00 00 	.4byte .LASF40
 9325 29f3 01          	.byte 0x1
 9326 29f4 B5          	.byte 0xb5
 9327 29f5 F9 25 00 00 	.4byte 0x25f9
 9328 29f9 01          	.byte 0x1
 9329 29fa 05          	.byte 0x5
MPLAB XC16 ASSEMBLY Listing:   			page 233


 9330 29fb 03          	.byte 0x3
 9331 29fc 00 00 00 00 	.4byte _SetupPkt
 9332 2a00 2F          	.uleb128 0x2f
 9333 2a01 00 00 00 00 	.4byte .LASF41
 9334 2a05 01          	.byte 0x1
 9335 2a06 B6          	.byte 0xb6
 9336 2a07 12 2A 00 00 	.4byte 0x2a12
 9337 2a0b 01          	.byte 0x1
 9338 2a0c 05          	.byte 0x5
 9339 2a0d 03          	.byte 0x3
 9340 2a0e 00 00 00 00 	.4byte _CtrlTrfData
 9341 2a12 24          	.uleb128 0x24
 9342 2a13 FE 25 00 00 	.4byte 0x25fe
 9343 2a17 2C          	.uleb128 0x2c
 9344 2a18 00 00 00 00 	.4byte .LASF42
 9345 2a1c 01          	.byte 0x1
 9346 2a1d D5          	.byte 0xd5
 9347 2a1e 2D 26 00 00 	.4byte 0x262d
 9348 2a22 01          	.byte 0x1
 9349 2a23 01          	.byte 0x1
 9350 2a24 2C          	.uleb128 0x2c
 9351 2a25 00 00 00 00 	.4byte .LASF43
 9352 2a29 01          	.byte 0x1
 9353 2a2a DC          	.byte 0xdc
 9354 2a2b 31 2A 00 00 	.4byte 0x2a31
 9355 2a2f 01          	.byte 0x1
 9356 2a30 01          	.byte 0x1
 9357 2a31 19          	.uleb128 0x19
 9358 2a32 32 26 00 00 	.4byte 0x2632
 9359 2a36 2C          	.uleb128 0x2c
 9360 2a37 00 00 00 00 	.4byte .LASF44
 9361 2a3b 01          	.byte 0x1
 9362 2a3c DF          	.byte 0xdf
 9363 2a3d 43 2A 00 00 	.4byte 0x2a43
 9364 2a41 01          	.byte 0x1
 9365 2a42 01          	.byte 0x1
 9366 2a43 19          	.uleb128 0x19
 9367 2a44 32 26 00 00 	.4byte 0x2632
 9368 2a48 00          	.byte 0x0
 9369                 	.section .debug_abbrev,info
 9370 0000 01          	.uleb128 0x1
 9371 0001 11          	.uleb128 0x11
 9372 0002 01          	.byte 0x1
 9373 0003 25          	.uleb128 0x25
 9374 0004 08          	.uleb128 0x8
 9375 0005 13          	.uleb128 0x13
 9376 0006 0B          	.uleb128 0xb
 9377 0007 03          	.uleb128 0x3
 9378 0008 08          	.uleb128 0x8
 9379 0009 1B          	.uleb128 0x1b
 9380 000a 08          	.uleb128 0x8
 9381 000b 11          	.uleb128 0x11
 9382 000c 01          	.uleb128 0x1
 9383 000d 12          	.uleb128 0x12
 9384 000e 01          	.uleb128 0x1
 9385 000f 10          	.uleb128 0x10
 9386 0010 06          	.uleb128 0x6
MPLAB XC16 ASSEMBLY Listing:   			page 234


 9387 0011 00          	.byte 0x0
 9388 0012 00          	.byte 0x0
 9389 0013 02          	.uleb128 0x2
 9390 0014 24          	.uleb128 0x24
 9391 0015 00          	.byte 0x0
 9392 0016 0B          	.uleb128 0xb
 9393 0017 0B          	.uleb128 0xb
 9394 0018 3E          	.uleb128 0x3e
 9395 0019 0B          	.uleb128 0xb
 9396 001a 03          	.uleb128 0x3
 9397 001b 08          	.uleb128 0x8
 9398 001c 00          	.byte 0x0
 9399 001d 00          	.byte 0x0
 9400 001e 03          	.uleb128 0x3
 9401 001f 16          	.uleb128 0x16
 9402 0020 00          	.byte 0x0
 9403 0021 03          	.uleb128 0x3
 9404 0022 08          	.uleb128 0x8
 9405 0023 3A          	.uleb128 0x3a
 9406 0024 0B          	.uleb128 0xb
 9407 0025 3B          	.uleb128 0x3b
 9408 0026 0B          	.uleb128 0xb
 9409 0027 49          	.uleb128 0x49
 9410 0028 13          	.uleb128 0x13
 9411 0029 00          	.byte 0x0
 9412 002a 00          	.byte 0x0
 9413 002b 04          	.uleb128 0x4
 9414 002c 13          	.uleb128 0x13
 9415 002d 01          	.byte 0x1
 9416 002e 03          	.uleb128 0x3
 9417 002f 08          	.uleb128 0x8
 9418 0030 0B          	.uleb128 0xb
 9419 0031 0B          	.uleb128 0xb
 9420 0032 3A          	.uleb128 0x3a
 9421 0033 0B          	.uleb128 0xb
 9422 0034 3B          	.uleb128 0x3b
 9423 0035 05          	.uleb128 0x5
 9424 0036 01          	.uleb128 0x1
 9425 0037 13          	.uleb128 0x13
 9426 0038 00          	.byte 0x0
 9427 0039 00          	.byte 0x0
 9428 003a 05          	.uleb128 0x5
 9429 003b 0D          	.uleb128 0xd
 9430 003c 00          	.byte 0x0
 9431 003d 03          	.uleb128 0x3
 9432 003e 08          	.uleb128 0x8
 9433 003f 3A          	.uleb128 0x3a
 9434 0040 0B          	.uleb128 0xb
 9435 0041 3B          	.uleb128 0x3b
 9436 0042 05          	.uleb128 0x5
 9437 0043 49          	.uleb128 0x49
 9438 0044 13          	.uleb128 0x13
 9439 0045 0B          	.uleb128 0xb
 9440 0046 0B          	.uleb128 0xb
 9441 0047 0D          	.uleb128 0xd
 9442 0048 0B          	.uleb128 0xb
 9443 0049 0C          	.uleb128 0xc
MPLAB XC16 ASSEMBLY Listing:   			page 235


 9444 004a 0B          	.uleb128 0xb
 9445 004b 38          	.uleb128 0x38
 9446 004c 0A          	.uleb128 0xa
 9447 004d 00          	.byte 0x0
 9448 004e 00          	.byte 0x0
 9449 004f 06          	.uleb128 0x6
 9450 0050 16          	.uleb128 0x16
 9451 0051 00          	.byte 0x0
 9452 0052 03          	.uleb128 0x3
 9453 0053 08          	.uleb128 0x8
 9454 0054 3A          	.uleb128 0x3a
 9455 0055 0B          	.uleb128 0xb
 9456 0056 3B          	.uleb128 0x3b
 9457 0057 05          	.uleb128 0x5
 9458 0058 49          	.uleb128 0x49
 9459 0059 13          	.uleb128 0x13
 9460 005a 00          	.byte 0x0
 9461 005b 00          	.byte 0x0
 9462 005c 07          	.uleb128 0x7
 9463 005d 13          	.uleb128 0x13
 9464 005e 01          	.byte 0x1
 9465 005f 0B          	.uleb128 0xb
 9466 0060 0B          	.uleb128 0xb
 9467 0061 3A          	.uleb128 0x3a
 9468 0062 0B          	.uleb128 0xb
 9469 0063 3B          	.uleb128 0x3b
 9470 0064 05          	.uleb128 0x5
 9471 0065 01          	.uleb128 0x1
 9472 0066 13          	.uleb128 0x13
 9473 0067 00          	.byte 0x0
 9474 0068 00          	.byte 0x0
 9475 0069 08          	.uleb128 0x8
 9476 006a 17          	.uleb128 0x17
 9477 006b 01          	.byte 0x1
 9478 006c 0B          	.uleb128 0xb
 9479 006d 0B          	.uleb128 0xb
 9480 006e 3A          	.uleb128 0x3a
 9481 006f 0B          	.uleb128 0xb
 9482 0070 3B          	.uleb128 0x3b
 9483 0071 05          	.uleb128 0x5
 9484 0072 01          	.uleb128 0x1
 9485 0073 13          	.uleb128 0x13
 9486 0074 00          	.byte 0x0
 9487 0075 00          	.byte 0x0
 9488 0076 09          	.uleb128 0x9
 9489 0077 0D          	.uleb128 0xd
 9490 0078 00          	.byte 0x0
 9491 0079 49          	.uleb128 0x49
 9492 007a 13          	.uleb128 0x13
 9493 007b 00          	.byte 0x0
 9494 007c 00          	.byte 0x0
 9495 007d 0A          	.uleb128 0xa
 9496 007e 0D          	.uleb128 0xd
 9497 007f 00          	.byte 0x0
 9498 0080 49          	.uleb128 0x49
 9499 0081 13          	.uleb128 0x13
 9500 0082 38          	.uleb128 0x38
MPLAB XC16 ASSEMBLY Listing:   			page 236


 9501 0083 0A          	.uleb128 0xa
 9502 0084 00          	.byte 0x0
 9503 0085 00          	.byte 0x0
 9504 0086 0B          	.uleb128 0xb
 9505 0087 0F          	.uleb128 0xf
 9506 0088 00          	.byte 0x0
 9507 0089 0B          	.uleb128 0xb
 9508 008a 0B          	.uleb128 0xb
 9509 008b 00          	.byte 0x0
 9510 008c 00          	.byte 0x0
 9511 008d 0C          	.uleb128 0xc
 9512 008e 04          	.uleb128 0x4
 9513 008f 01          	.byte 0x1
 9514 0090 0B          	.uleb128 0xb
 9515 0091 0B          	.uleb128 0xb
 9516 0092 3A          	.uleb128 0x3a
 9517 0093 0B          	.uleb128 0xb
 9518 0094 3B          	.uleb128 0x3b
 9519 0095 0B          	.uleb128 0xb
 9520 0096 01          	.uleb128 0x1
 9521 0097 13          	.uleb128 0x13
 9522 0098 00          	.byte 0x0
 9523 0099 00          	.byte 0x0
 9524 009a 0D          	.uleb128 0xd
 9525 009b 28          	.uleb128 0x28
 9526 009c 00          	.byte 0x0
 9527 009d 03          	.uleb128 0x3
 9528 009e 08          	.uleb128 0x8
 9529 009f 1C          	.uleb128 0x1c
 9530 00a0 0D          	.uleb128 0xd
 9531 00a1 00          	.byte 0x0
 9532 00a2 00          	.byte 0x0
 9533 00a3 0E          	.uleb128 0xe
 9534 00a4 13          	.uleb128 0x13
 9535 00a5 01          	.byte 0x1
 9536 00a6 03          	.uleb128 0x3
 9537 00a7 08          	.uleb128 0x8
 9538 00a8 0B          	.uleb128 0xb
 9539 00a9 0B          	.uleb128 0xb
 9540 00aa 3A          	.uleb128 0x3a
 9541 00ab 0B          	.uleb128 0xb
 9542 00ac 3B          	.uleb128 0x3b
 9543 00ad 0B          	.uleb128 0xb
 9544 00ae 01          	.uleb128 0x1
 9545 00af 13          	.uleb128 0x13
 9546 00b0 00          	.byte 0x0
 9547 00b1 00          	.byte 0x0
 9548 00b2 0F          	.uleb128 0xf
 9549 00b3 0D          	.uleb128 0xd
 9550 00b4 00          	.byte 0x0
 9551 00b5 03          	.uleb128 0x3
 9552 00b6 08          	.uleb128 0x8
 9553 00b7 3A          	.uleb128 0x3a
 9554 00b8 0B          	.uleb128 0xb
 9555 00b9 3B          	.uleb128 0x3b
 9556 00ba 0B          	.uleb128 0xb
 9557 00bb 49          	.uleb128 0x49
MPLAB XC16 ASSEMBLY Listing:   			page 237


 9558 00bc 13          	.uleb128 0x13
 9559 00bd 38          	.uleb128 0x38
 9560 00be 0A          	.uleb128 0xa
 9561 00bf 00          	.byte 0x0
 9562 00c0 00          	.byte 0x0
 9563 00c1 10          	.uleb128 0x10
 9564 00c2 0D          	.uleb128 0xd
 9565 00c3 00          	.byte 0x0
 9566 00c4 03          	.uleb128 0x3
 9567 00c5 0E          	.uleb128 0xe
 9568 00c6 3A          	.uleb128 0x3a
 9569 00c7 0B          	.uleb128 0xb
 9570 00c8 3B          	.uleb128 0x3b
 9571 00c9 0B          	.uleb128 0xb
 9572 00ca 49          	.uleb128 0x49
 9573 00cb 13          	.uleb128 0x13
 9574 00cc 38          	.uleb128 0x38
 9575 00cd 0A          	.uleb128 0xa
 9576 00ce 00          	.byte 0x0
 9577 00cf 00          	.byte 0x0
 9578 00d0 11          	.uleb128 0x11
 9579 00d1 0D          	.uleb128 0xd
 9580 00d2 00          	.byte 0x0
 9581 00d3 03          	.uleb128 0x3
 9582 00d4 0E          	.uleb128 0xe
 9583 00d5 3A          	.uleb128 0x3a
 9584 00d6 0B          	.uleb128 0xb
 9585 00d7 3B          	.uleb128 0x3b
 9586 00d8 05          	.uleb128 0x5
 9587 00d9 49          	.uleb128 0x49
 9588 00da 13          	.uleb128 0x13
 9589 00db 38          	.uleb128 0x38
 9590 00dc 0A          	.uleb128 0xa
 9591 00dd 00          	.byte 0x0
 9592 00de 00          	.byte 0x0
 9593 00df 12          	.uleb128 0x12
 9594 00e0 0D          	.uleb128 0xd
 9595 00e1 00          	.byte 0x0
 9596 00e2 03          	.uleb128 0x3
 9597 00e3 08          	.uleb128 0x8
 9598 00e4 3A          	.uleb128 0x3a
 9599 00e5 0B          	.uleb128 0xb
 9600 00e6 3B          	.uleb128 0x3b
 9601 00e7 05          	.uleb128 0x5
 9602 00e8 49          	.uleb128 0x49
 9603 00e9 13          	.uleb128 0x13
 9604 00ea 38          	.uleb128 0x38
 9605 00eb 0A          	.uleb128 0xa
 9606 00ec 00          	.byte 0x0
 9607 00ed 00          	.byte 0x0
 9608 00ee 13          	.uleb128 0x13
 9609 00ef 0D          	.uleb128 0xd
 9610 00f0 00          	.byte 0x0
 9611 00f1 03          	.uleb128 0x3
 9612 00f2 08          	.uleb128 0x8
 9613 00f3 3A          	.uleb128 0x3a
 9614 00f4 0B          	.uleb128 0xb
MPLAB XC16 ASSEMBLY Listing:   			page 238


 9615 00f5 3B          	.uleb128 0x3b
 9616 00f6 05          	.uleb128 0x5
 9617 00f7 49          	.uleb128 0x49
 9618 00f8 13          	.uleb128 0x13
 9619 00f9 00          	.byte 0x0
 9620 00fa 00          	.byte 0x0
 9621 00fb 14          	.uleb128 0x14
 9622 00fc 01          	.uleb128 0x1
 9623 00fd 01          	.byte 0x1
 9624 00fe 49          	.uleb128 0x49
 9625 00ff 13          	.uleb128 0x13
 9626 0100 01          	.uleb128 0x1
 9627 0101 13          	.uleb128 0x13
 9628 0102 00          	.byte 0x0
 9629 0103 00          	.byte 0x0
 9630 0104 15          	.uleb128 0x15
 9631 0105 21          	.uleb128 0x21
 9632 0106 00          	.byte 0x0
 9633 0107 49          	.uleb128 0x49
 9634 0108 13          	.uleb128 0x13
 9635 0109 2F          	.uleb128 0x2f
 9636 010a 0B          	.uleb128 0xb
 9637 010b 00          	.byte 0x0
 9638 010c 00          	.byte 0x0
 9639 010d 16          	.uleb128 0x16
 9640 010e 0D          	.uleb128 0xd
 9641 010f 00          	.byte 0x0
 9642 0110 03          	.uleb128 0x3
 9643 0111 0E          	.uleb128 0xe
 9644 0112 3A          	.uleb128 0x3a
 9645 0113 0B          	.uleb128 0xb
 9646 0114 3B          	.uleb128 0x3b
 9647 0115 05          	.uleb128 0x5
 9648 0116 49          	.uleb128 0x49
 9649 0117 13          	.uleb128 0x13
 9650 0118 0B          	.uleb128 0xb
 9651 0119 0B          	.uleb128 0xb
 9652 011a 0D          	.uleb128 0xd
 9653 011b 0B          	.uleb128 0xb
 9654 011c 0C          	.uleb128 0xc
 9655 011d 0B          	.uleb128 0xb
 9656 011e 38          	.uleb128 0x38
 9657 011f 0A          	.uleb128 0xa
 9658 0120 00          	.byte 0x0
 9659 0121 00          	.byte 0x0
 9660 0122 17          	.uleb128 0x17
 9661 0123 0D          	.uleb128 0xd
 9662 0124 00          	.byte 0x0
 9663 0125 03          	.uleb128 0x3
 9664 0126 0E          	.uleb128 0xe
 9665 0127 3A          	.uleb128 0x3a
 9666 0128 0B          	.uleb128 0xb
 9667 0129 3B          	.uleb128 0x3b
 9668 012a 05          	.uleb128 0x5
 9669 012b 49          	.uleb128 0x49
 9670 012c 13          	.uleb128 0x13
 9671 012d 00          	.byte 0x0
MPLAB XC16 ASSEMBLY Listing:   			page 239


 9672 012e 00          	.byte 0x0
 9673 012f 18          	.uleb128 0x18
 9674 0130 0F          	.uleb128 0xf
 9675 0131 00          	.byte 0x0
 9676 0132 0B          	.uleb128 0xb
 9677 0133 0B          	.uleb128 0xb
 9678 0134 49          	.uleb128 0x49
 9679 0135 13          	.uleb128 0x13
 9680 0136 00          	.byte 0x0
 9681 0137 00          	.byte 0x0
 9682 0138 19          	.uleb128 0x19
 9683 0139 26          	.uleb128 0x26
 9684 013a 00          	.byte 0x0
 9685 013b 49          	.uleb128 0x49
 9686 013c 13          	.uleb128 0x13
 9687 013d 00          	.byte 0x0
 9688 013e 00          	.byte 0x0
 9689 013f 1A          	.uleb128 0x1a
 9690 0140 15          	.uleb128 0x15
 9691 0141 00          	.byte 0x0
 9692 0142 27          	.uleb128 0x27
 9693 0143 0C          	.uleb128 0xc
 9694 0144 00          	.byte 0x0
 9695 0145 00          	.byte 0x0
 9696 0146 1B          	.uleb128 0x1b
 9697 0147 13          	.uleb128 0x13
 9698 0148 01          	.byte 0x1
 9699 0149 0B          	.uleb128 0xb
 9700 014a 0B          	.uleb128 0xb
 9701 014b 3A          	.uleb128 0x3a
 9702 014c 0B          	.uleb128 0xb
 9703 014d 3B          	.uleb128 0x3b
 9704 014e 0B          	.uleb128 0xb
 9705 014f 01          	.uleb128 0x1
 9706 0150 13          	.uleb128 0x13
 9707 0151 00          	.byte 0x0
 9708 0152 00          	.byte 0x0
 9709 0153 1C          	.uleb128 0x1c
 9710 0154 0D          	.uleb128 0xd
 9711 0155 00          	.byte 0x0
 9712 0156 03          	.uleb128 0x3
 9713 0157 08          	.uleb128 0x8
 9714 0158 3A          	.uleb128 0x3a
 9715 0159 0B          	.uleb128 0xb
 9716 015a 3B          	.uleb128 0x3b
 9717 015b 0B          	.uleb128 0xb
 9718 015c 49          	.uleb128 0x49
 9719 015d 13          	.uleb128 0x13
 9720 015e 0B          	.uleb128 0xb
 9721 015f 0B          	.uleb128 0xb
 9722 0160 0D          	.uleb128 0xd
 9723 0161 0B          	.uleb128 0xb
 9724 0162 0C          	.uleb128 0xc
 9725 0163 0B          	.uleb128 0xb
 9726 0164 38          	.uleb128 0x38
 9727 0165 0A          	.uleb128 0xa
 9728 0166 00          	.byte 0x0
MPLAB XC16 ASSEMBLY Listing:   			page 240


 9729 0167 00          	.byte 0x0
 9730 0168 1D          	.uleb128 0x1d
 9731 0169 17          	.uleb128 0x17
 9732 016a 01          	.byte 0x1
 9733 016b 03          	.uleb128 0x3
 9734 016c 08          	.uleb128 0x8
 9735 016d 0B          	.uleb128 0xb
 9736 016e 0B          	.uleb128 0xb
 9737 016f 3A          	.uleb128 0x3a
 9738 0170 0B          	.uleb128 0xb
 9739 0171 3B          	.uleb128 0x3b
 9740 0172 0B          	.uleb128 0xb
 9741 0173 01          	.uleb128 0x1
 9742 0174 13          	.uleb128 0x13
 9743 0175 00          	.byte 0x0
 9744 0176 00          	.byte 0x0
 9745 0177 1E          	.uleb128 0x1e
 9746 0178 0D          	.uleb128 0xd
 9747 0179 00          	.byte 0x0
 9748 017a 03          	.uleb128 0x3
 9749 017b 08          	.uleb128 0x8
 9750 017c 3A          	.uleb128 0x3a
 9751 017d 0B          	.uleb128 0xb
 9752 017e 3B          	.uleb128 0x3b
 9753 017f 0B          	.uleb128 0xb
 9754 0180 49          	.uleb128 0x49
 9755 0181 13          	.uleb128 0x13
 9756 0182 00          	.byte 0x0
 9757 0183 00          	.byte 0x0
 9758 0184 1F          	.uleb128 0x1f
 9759 0185 17          	.uleb128 0x17
 9760 0186 01          	.byte 0x1
 9761 0187 0B          	.uleb128 0xb
 9762 0188 0B          	.uleb128 0xb
 9763 0189 3A          	.uleb128 0x3a
 9764 018a 0B          	.uleb128 0xb
 9765 018b 3B          	.uleb128 0x3b
 9766 018c 0B          	.uleb128 0xb
 9767 018d 01          	.uleb128 0x1
 9768 018e 13          	.uleb128 0x13
 9769 018f 00          	.byte 0x0
 9770 0190 00          	.byte 0x0
 9771 0191 20          	.uleb128 0x20
 9772 0192 2E          	.uleb128 0x2e
 9773 0193 01          	.byte 0x1
 9774 0194 3F          	.uleb128 0x3f
 9775 0195 0C          	.uleb128 0xc
 9776 0196 03          	.uleb128 0x3
 9777 0197 08          	.uleb128 0x8
 9778 0198 3A          	.uleb128 0x3a
 9779 0199 0B          	.uleb128 0xb
 9780 019a 3B          	.uleb128 0x3b
 9781 019b 05          	.uleb128 0x5
 9782 019c 27          	.uleb128 0x27
 9783 019d 0C          	.uleb128 0xc
 9784 019e 11          	.uleb128 0x11
 9785 019f 01          	.uleb128 0x1
MPLAB XC16 ASSEMBLY Listing:   			page 241


 9786 01a0 12          	.uleb128 0x12
 9787 01a1 01          	.uleb128 0x1
 9788 01a2 40          	.uleb128 0x40
 9789 01a3 0A          	.uleb128 0xa
 9790 01a4 01          	.uleb128 0x1
 9791 01a5 13          	.uleb128 0x13
 9792 01a6 00          	.byte 0x0
 9793 01a7 00          	.byte 0x0
 9794 01a8 21          	.uleb128 0x21
 9795 01a9 34          	.uleb128 0x34
 9796 01aa 00          	.byte 0x0
 9797 01ab 03          	.uleb128 0x3
 9798 01ac 08          	.uleb128 0x8
 9799 01ad 3A          	.uleb128 0x3a
 9800 01ae 0B          	.uleb128 0xb
 9801 01af 3B          	.uleb128 0x3b
 9802 01b0 05          	.uleb128 0x5
 9803 01b1 49          	.uleb128 0x49
 9804 01b2 13          	.uleb128 0x13
 9805 01b3 02          	.uleb128 0x2
 9806 01b4 0A          	.uleb128 0xa
 9807 01b5 00          	.byte 0x0
 9808 01b6 00          	.byte 0x0
 9809 01b7 22          	.uleb128 0x22
 9810 01b8 05          	.uleb128 0x5
 9811 01b9 00          	.byte 0x0
 9812 01ba 03          	.uleb128 0x3
 9813 01bb 08          	.uleb128 0x8
 9814 01bc 3A          	.uleb128 0x3a
 9815 01bd 0B          	.uleb128 0xb
 9816 01be 3B          	.uleb128 0x3b
 9817 01bf 05          	.uleb128 0x5
 9818 01c0 49          	.uleb128 0x49
 9819 01c1 13          	.uleb128 0x13
 9820 01c2 02          	.uleb128 0x2
 9821 01c3 0A          	.uleb128 0xa
 9822 01c4 00          	.byte 0x0
 9823 01c5 00          	.byte 0x0
 9824 01c6 23          	.uleb128 0x23
 9825 01c7 2E          	.uleb128 0x2e
 9826 01c8 01          	.byte 0x1
 9827 01c9 3F          	.uleb128 0x3f
 9828 01ca 0C          	.uleb128 0xc
 9829 01cb 03          	.uleb128 0x3
 9830 01cc 08          	.uleb128 0x8
 9831 01cd 3A          	.uleb128 0x3a
 9832 01ce 0B          	.uleb128 0xb
 9833 01cf 3B          	.uleb128 0x3b
 9834 01d0 05          	.uleb128 0x5
 9835 01d1 27          	.uleb128 0x27
 9836 01d2 0C          	.uleb128 0xc
 9837 01d3 49          	.uleb128 0x49
 9838 01d4 13          	.uleb128 0x13
 9839 01d5 11          	.uleb128 0x11
 9840 01d6 01          	.uleb128 0x1
 9841 01d7 12          	.uleb128 0x12
 9842 01d8 01          	.uleb128 0x1
MPLAB XC16 ASSEMBLY Listing:   			page 242


 9843 01d9 40          	.uleb128 0x40
 9844 01da 0A          	.uleb128 0xa
 9845 01db 01          	.uleb128 0x1
 9846 01dc 13          	.uleb128 0x13
 9847 01dd 00          	.byte 0x0
 9848 01de 00          	.byte 0x0
 9849 01df 24          	.uleb128 0x24
 9850 01e0 35          	.uleb128 0x35
 9851 01e1 00          	.byte 0x0
 9852 01e2 49          	.uleb128 0x49
 9853 01e3 13          	.uleb128 0x13
 9854 01e4 00          	.byte 0x0
 9855 01e5 00          	.byte 0x0
 9856 01e6 25          	.uleb128 0x25
 9857 01e7 2E          	.uleb128 0x2e
 9858 01e8 00          	.byte 0x0
 9859 01e9 3F          	.uleb128 0x3f
 9860 01ea 0C          	.uleb128 0xc
 9861 01eb 03          	.uleb128 0x3
 9862 01ec 08          	.uleb128 0x8
 9863 01ed 3A          	.uleb128 0x3a
 9864 01ee 0B          	.uleb128 0xb
 9865 01ef 3B          	.uleb128 0x3b
 9866 01f0 05          	.uleb128 0x5
 9867 01f1 27          	.uleb128 0x27
 9868 01f2 0C          	.uleb128 0xc
 9869 01f3 11          	.uleb128 0x11
 9870 01f4 01          	.uleb128 0x1
 9871 01f5 12          	.uleb128 0x12
 9872 01f6 01          	.uleb128 0x1
 9873 01f7 40          	.uleb128 0x40
 9874 01f8 0A          	.uleb128 0xa
 9875 01f9 00          	.byte 0x0
 9876 01fa 00          	.byte 0x0
 9877 01fb 26          	.uleb128 0x26
 9878 01fc 2E          	.uleb128 0x2e
 9879 01fd 01          	.byte 0x1
 9880 01fe 03          	.uleb128 0x3
 9881 01ff 08          	.uleb128 0x8
 9882 0200 3A          	.uleb128 0x3a
 9883 0201 0B          	.uleb128 0xb
 9884 0202 3B          	.uleb128 0x3b
 9885 0203 05          	.uleb128 0x5
 9886 0204 27          	.uleb128 0x27
 9887 0205 0C          	.uleb128 0xc
 9888 0206 11          	.uleb128 0x11
 9889 0207 01          	.uleb128 0x1
 9890 0208 12          	.uleb128 0x12
 9891 0209 01          	.uleb128 0x1
 9892 020a 40          	.uleb128 0x40
 9893 020b 0A          	.uleb128 0xa
 9894 020c 01          	.uleb128 0x1
 9895 020d 13          	.uleb128 0x13
 9896 020e 00          	.byte 0x0
 9897 020f 00          	.byte 0x0
 9898 0210 27          	.uleb128 0x27
 9899 0211 05          	.uleb128 0x5
MPLAB XC16 ASSEMBLY Listing:   			page 243


 9900 0212 00          	.byte 0x0
 9901 0213 03          	.uleb128 0x3
 9902 0214 0E          	.uleb128 0xe
 9903 0215 3A          	.uleb128 0x3a
 9904 0216 0B          	.uleb128 0xb
 9905 0217 3B          	.uleb128 0x3b
 9906 0218 05          	.uleb128 0x5
 9907 0219 49          	.uleb128 0x49
 9908 021a 13          	.uleb128 0x13
 9909 021b 02          	.uleb128 0x2
 9910 021c 0A          	.uleb128 0xa
 9911 021d 00          	.byte 0x0
 9912 021e 00          	.byte 0x0
 9913 021f 28          	.uleb128 0x28
 9914 0220 2E          	.uleb128 0x2e
 9915 0221 00          	.byte 0x0
 9916 0222 03          	.uleb128 0x3
 9917 0223 08          	.uleb128 0x8
 9918 0224 3A          	.uleb128 0x3a
 9919 0225 0B          	.uleb128 0xb
 9920 0226 3B          	.uleb128 0x3b
 9921 0227 05          	.uleb128 0x5
 9922 0228 27          	.uleb128 0x27
 9923 0229 0C          	.uleb128 0xc
 9924 022a 11          	.uleb128 0x11
 9925 022b 01          	.uleb128 0x1
 9926 022c 12          	.uleb128 0x12
 9927 022d 01          	.uleb128 0x1
 9928 022e 40          	.uleb128 0x40
 9929 022f 0A          	.uleb128 0xa
 9930 0230 00          	.byte 0x0
 9931 0231 00          	.byte 0x0
 9932 0232 29          	.uleb128 0x29
 9933 0233 0B          	.uleb128 0xb
 9934 0234 01          	.byte 0x1
 9935 0235 11          	.uleb128 0x11
 9936 0236 01          	.uleb128 0x1
 9937 0237 12          	.uleb128 0x12
 9938 0238 01          	.uleb128 0x1
 9939 0239 00          	.byte 0x0
 9940 023a 00          	.byte 0x0
 9941 023b 2A          	.uleb128 0x2a
 9942 023c 34          	.uleb128 0x34
 9943 023d 00          	.byte 0x0
 9944 023e 03          	.uleb128 0x3
 9945 023f 08          	.uleb128 0x8
 9946 0240 3A          	.uleb128 0x3a
 9947 0241 0B          	.uleb128 0xb
 9948 0242 3B          	.uleb128 0x3b
 9949 0243 05          	.uleb128 0x5
 9950 0244 49          	.uleb128 0x49
 9951 0245 13          	.uleb128 0x13
 9952 0246 3F          	.uleb128 0x3f
 9953 0247 0C          	.uleb128 0xc
 9954 0248 3C          	.uleb128 0x3c
 9955 0249 0C          	.uleb128 0xc
 9956 024a 00          	.byte 0x0
MPLAB XC16 ASSEMBLY Listing:   			page 244


 9957 024b 00          	.byte 0x0
 9958 024c 2B          	.uleb128 0x2b
 9959 024d 34          	.uleb128 0x34
 9960 024e 00          	.byte 0x0
 9961 024f 03          	.uleb128 0x3
 9962 0250 0E          	.uleb128 0xe
 9963 0251 3A          	.uleb128 0x3a
 9964 0252 0B          	.uleb128 0xb
 9965 0253 3B          	.uleb128 0x3b
 9966 0254 05          	.uleb128 0x5
 9967 0255 49          	.uleb128 0x49
 9968 0256 13          	.uleb128 0x13
 9969 0257 3F          	.uleb128 0x3f
 9970 0258 0C          	.uleb128 0xc
 9971 0259 3C          	.uleb128 0x3c
 9972 025a 0C          	.uleb128 0xc
 9973 025b 00          	.byte 0x0
 9974 025c 00          	.byte 0x0
 9975 025d 2C          	.uleb128 0x2c
 9976 025e 34          	.uleb128 0x34
 9977 025f 00          	.byte 0x0
 9978 0260 03          	.uleb128 0x3
 9979 0261 0E          	.uleb128 0xe
 9980 0262 3A          	.uleb128 0x3a
 9981 0263 0B          	.uleb128 0xb
 9982 0264 3B          	.uleb128 0x3b
 9983 0265 0B          	.uleb128 0xb
 9984 0266 49          	.uleb128 0x49
 9985 0267 13          	.uleb128 0x13
 9986 0268 3F          	.uleb128 0x3f
 9987 0269 0C          	.uleb128 0xc
 9988 026a 3C          	.uleb128 0x3c
 9989 026b 0C          	.uleb128 0xc
 9990 026c 00          	.byte 0x0
 9991 026d 00          	.byte 0x0
 9992 026e 2D          	.uleb128 0x2d
 9993 026f 34          	.uleb128 0x34
 9994 0270 00          	.byte 0x0
 9995 0271 03          	.uleb128 0x3
 9996 0272 08          	.uleb128 0x8
 9997 0273 3A          	.uleb128 0x3a
 9998 0274 0B          	.uleb128 0xb
 9999 0275 3B          	.uleb128 0x3b
 10000 0276 0B          	.uleb128 0xb
 10001 0277 49          	.uleb128 0x49
 10002 0278 13          	.uleb128 0x13
 10003 0279 3F          	.uleb128 0x3f
 10004 027a 0C          	.uleb128 0xc
 10005 027b 3C          	.uleb128 0x3c
 10006 027c 0C          	.uleb128 0xc
 10007 027d 00          	.byte 0x0
 10008 027e 00          	.byte 0x0
 10009 027f 2E          	.uleb128 0x2e
 10010 0280 21          	.uleb128 0x21
 10011 0281 00          	.byte 0x0
 10012 0282 00          	.byte 0x0
 10013 0283 00          	.byte 0x0
MPLAB XC16 ASSEMBLY Listing:   			page 245


 10014 0284 2F          	.uleb128 0x2f
 10015 0285 34          	.uleb128 0x34
 10016 0286 00          	.byte 0x0
 10017 0287 03          	.uleb128 0x3
 10018 0288 0E          	.uleb128 0xe
 10019 0289 3A          	.uleb128 0x3a
 10020 028a 0B          	.uleb128 0xb
 10021 028b 3B          	.uleb128 0x3b
 10022 028c 0B          	.uleb128 0xb
 10023 028d 49          	.uleb128 0x49
 10024 028e 13          	.uleb128 0x13
 10025 028f 3F          	.uleb128 0x3f
 10026 0290 0C          	.uleb128 0xc
 10027 0291 02          	.uleb128 0x2
 10028 0292 0A          	.uleb128 0xa
 10029 0293 00          	.byte 0x0
 10030 0294 00          	.byte 0x0
 10031 0295 30          	.uleb128 0x30
 10032 0296 34          	.uleb128 0x34
 10033 0297 00          	.byte 0x0
 10034 0298 03          	.uleb128 0x3
 10035 0299 08          	.uleb128 0x8
 10036 029a 3A          	.uleb128 0x3a
 10037 029b 0B          	.uleb128 0xb
 10038 029c 3B          	.uleb128 0x3b
 10039 029d 0B          	.uleb128 0xb
 10040 029e 49          	.uleb128 0x49
 10041 029f 13          	.uleb128 0x13
 10042 02a0 3F          	.uleb128 0x3f
 10043 02a1 0C          	.uleb128 0xc
 10044 02a2 02          	.uleb128 0x2
 10045 02a3 0A          	.uleb128 0xa
 10046 02a4 00          	.byte 0x0
 10047 02a5 00          	.byte 0x0
 10048 02a6 00          	.byte 0x0
 10049                 	.section .debug_pubnames,info
 10050 0000 9F 03 00 00 	.4byte 0x39f
 10051 0004 02 00       	.2byte 0x2
 10052 0006 00 00 00 00 	.4byte .Ldebug_info0
 10053 000a 49 2A 00 00 	.4byte 0x2a49
 10054 000e 31 1C 00 00 	.4byte 0x1c31
 10055 0012 55 53 42 44 	.asciz "USBDeviceInit"
 10055      65 76 69 63 
 10055      65 49 6E 69 
 10055      74 00 
 10056 0020 61 1C 00 00 	.4byte 0x1c61
 10057 0024 55 53 42 44 	.asciz "USBDeviceTasks"
 10057      65 76 69 63 
 10057      65 54 61 73 
 10057      6B 73 00 
 10058 0033 92 1C 00 00 	.4byte 0x1c92
 10059 0037 55 53 42 45 	.asciz "USBEnableEndpoint"
 10059      6E 61 62 6C 
 10059      65 45 6E 64 
 10059      70 6F 69 6E 
 10059      74 00 
 10060 0049 ED 1C 00 00 	.4byte 0x1ced
MPLAB XC16 ASSEMBLY Listing:   			page 246


 10061 004d 55 53 42 54 	.asciz "USBTransferOnePacket"
 10061      72 61 6E 73 
 10061      66 65 72 4F 
 10061      6E 65 50 61 
 10061      63 6B 65 74 
 10061      00 
 10062 0062 74 1D 00 00 	.4byte 0x1d74
 10063 0066 55 53 42 53 	.asciz "USBStallEndpoint"
 10063      74 61 6C 6C 
 10063      45 6E 64 70 
 10063      6F 69 6E 74 
 10063      00 
 10064 0077 CA 1D 00 00 	.4byte 0x1dca
 10065 007b 55 53 42 43 	.asciz "USBCancelIO"
 10065      61 6E 63 65 
 10065      6C 49 4F 00 
 10066 0087 FF 1D 00 00 	.4byte 0x1dff
 10067 008b 55 53 42 44 	.asciz "USBDeviceDetach"
 10067      65 76 69 63 
 10067      65 44 65 74 
 10067      61 63 68 00 
 10068 009b 1F 1E 00 00 	.4byte 0x1e1f
 10069 009f 55 53 42 44 	.asciz "USBDeviceAttach"
 10069      65 76 69 63 
 10069      65 41 74 74 
 10069      61 63 68 00 
 10070 00af 3F 1E 00 00 	.4byte 0x1e3f
 10071 00b3 55 53 42 43 	.asciz "USBCtrlEPAllowStatusStage"
 10071      74 72 6C 45 
 10071      50 41 6C 6C 
 10071      6F 77 53 74 
 10071      61 74 75 73 
 10071      53 74 61 67 
 10071      65 00 
 10072 00cd 69 1E 00 00 	.4byte 0x1e69
 10073 00d1 55 53 42 43 	.asciz "USBCtrlEPAllowDataStage"
 10073      74 72 6C 45 
 10073      50 41 6C 6C 
 10073      6F 77 44 61 
 10073      74 61 53 74 
 10073      61 67 65 00 
 10074 00e9 BD 21 00 00 	.4byte 0x21bd
 10075 00ed 55 53 42 49 	.asciz "USBIncrement1msInternalTimers"
 10075      6E 63 72 65 
 10075      6D 65 6E 74 
 10075      31 6D 73 49 
 10075      6E 74 65 72 
 10075      6E 61 6C 54 
 10075      69 6D 65 72 
 10075      73 00 
 10076 010b EB 21 00 00 	.4byte 0x21eb
 10077 010f 55 53 42 47 	.asciz "USBGet1msTickCount"
 10077      65 74 31 6D 
 10077      73 54 69 63 
 10077      6B 43 6F 75 
 10077      6E 74 00 
 10078 0122 D3 27 00 00 	.4byte 0x27d3
MPLAB XC16 ASSEMBLY Listing:   			page 247


 10079 0126 55 53 42 44 	.asciz "USBDeferOUTDataStagePackets"
 10079      65 66 65 72 
 10079      4F 55 54 44 
 10079      61 74 61 53 
 10079      74 61 67 65 
 10079      50 61 63 6B 
 10079      65 74 73 00 
 10080 0142 E5 27 00 00 	.4byte 0x27e5
 10081 0146 55 53 42 44 	.asciz "USBDeferStatusStagePacket"
 10081      65 66 65 72 
 10081      53 74 61 74 
 10081      75 73 53 74 
 10081      61 67 65 50 
 10081      61 63 6B 65 
 10081      74 00 
 10082 0160 F7 27 00 00 	.4byte 0x27f7
 10083 0164 55 53 42 44 	.asciz "USBDeferINDataStagePackets"
 10083      65 66 65 72 
 10083      49 4E 44 61 
 10083      74 61 53 74 
 10083      61 67 65 50 
 10083      61 63 6B 65 
 10083      74 73 00 
 10084 017f 09 28 00 00 	.4byte 0x2809
 10085 0183 69 6E 50 69 	.asciz "inPipes"
 10085      70 65 73 00 
 10086 018b 24 28 00 00 	.4byte 0x2824
 10087 018f 52 65 6D 6F 	.asciz "RemoteWakeup"
 10087      74 65 57 61 
 10087      6B 65 75 70 
 10087      00 
 10088 019c 36 28 00 00 	.4byte 0x2836
 10089 01a0 55 53 42 42 	.asciz "USBBusIsSuspended"
 10089      75 73 49 73 
 10089      53 75 73 70 
 10089      65 6E 64 65 
 10089      64 00 
 10090 01b2 48 28 00 00 	.4byte 0x2848
 10091 01b6 55 53 42 44 	.asciz "USBDeviceState"
 10091      65 76 69 63 
 10091      65 53 74 61 
 10091      74 65 00 
 10092 01c5 5A 28 00 00 	.4byte 0x285a
 10093 01c9 55 53 42 41 	.asciz "USBActiveConfiguration"
 10093      63 74 69 76 
 10093      65 43 6F 6E 
 10093      66 69 67 75 
 10093      72 61 74 69 
 10093      6F 6E 00 
 10094 01e0 6C 28 00 00 	.4byte 0x286c
 10095 01e4 55 53 42 54 	.asciz "USBTicksSinceSuspendEnd"
 10095      69 63 6B 73 
 10095      53 69 6E 63 
 10095      65 53 75 73 
 10095      70 65 6E 64 
 10095      45 6E 64 00 
 10096 01fc 7E 28 00 00 	.4byte 0x287e
MPLAB XC16 ASSEMBLY Listing:   			page 248


 10097 0200 6F 75 74 50 	.asciz "outPipes"
 10097      69 70 65 73 
 10097      00 
 10098 0209 95 28 00 00 	.4byte 0x2895
 10099 020d 70 42 44 54 	.asciz "pBDTEntryOut"
 10099      45 6E 74 72 
 10099      79 4F 75 74 
 10099      00 
 10100 021a A7 28 00 00 	.4byte 0x28a7
 10101 021e 70 42 44 54 	.asciz "pBDTEntryIn"
 10101      45 6E 74 72 
 10101      79 49 6E 00 
 10102 022a B9 28 00 00 	.4byte 0x28b9
 10103 022e 55 53 42 41 	.asciz "USBAlternateInterface"
 10103      6C 74 65 72 
 10103      6E 61 74 65 
 10103      49 6E 74 65 
 10103      72 66 61 63 
 10103      65 00 
 10104 0244 D0 28 00 00 	.4byte 0x28d0
 10105 0248 70 42 44 54 	.asciz "pBDTEntryEP0OutCurrent"
 10105      45 6E 74 72 
 10105      79 45 50 30 
 10105      4F 75 74 43 
 10105      75 72 72 65 
 10105      6E 74 00 
 10106 025f E2 28 00 00 	.4byte 0x28e2
 10107 0263 70 42 44 54 	.asciz "pBDTEntryEP0OutNext"
 10107      45 6E 74 72 
 10107      79 45 50 30 
 10107      4F 75 74 4E 
 10107      65 78 74 00 
 10108 0277 F4 28 00 00 	.4byte 0x28f4
 10109 027b 73 68 6F 72 	.asciz "shortPacketStatus"
 10109      74 50 61 63 
 10109      6B 65 74 53 
 10109      74 61 74 75 
 10109      73 00 
 10110 028d 06 29 00 00 	.4byte 0x2906
 10111 0291 63 6F 6E 74 	.asciz "controlTransferState"
 10111      72 6F 6C 54 
 10111      72 61 6E 73 
 10111      66 65 72 53 
 10111      74 61 74 65 
 10111      00 
 10112 02a6 18 29 00 00 	.4byte 0x2918
 10113 02aa 70 44 73 74 	.asciz "pDst"
 10113      00 
 10114 02af 2B 29 00 00 	.4byte 0x292b
 10115 02b3 55 53 54 41 	.asciz "USTATcopy"
 10115      54 63 6F 70 
 10115      79 00 
 10116 02bd 3D 29 00 00 	.4byte 0x293d
 10117 02c1 65 6E 64 70 	.asciz "endpoint_number"
 10117      6F 69 6E 74 
 10117      5F 6E 75 6D 
 10117      62 65 72 00 
MPLAB XC16 ASSEMBLY Listing:   			page 249


 10118 02d1 4F 29 00 00 	.4byte 0x294f
 10119 02d5 42 6F 74 68 	.asciz "BothEP0OutUOWNsSet"
 10119      45 50 30 4F 
 10119      75 74 55 4F 
 10119      57 4E 73 53 
 10119      65 74 00 
 10120 02e8 61 29 00 00 	.4byte 0x2961
 10121 02ec 65 70 5F 64 	.asciz "ep_data_in"
 10121      61 74 61 5F 
 10121      69 6E 00 
 10122 02f7 78 29 00 00 	.4byte 0x2978
 10123 02fb 65 70 5F 64 	.asciz "ep_data_out"
 10123      61 74 61 5F 
 10123      6F 75 74 00 
 10124 0307 8F 29 00 00 	.4byte 0x298f
 10125 030b 55 53 42 53 	.asciz "USBStatusStageTimeoutCounter"
 10125      74 61 74 75 
 10125      73 53 74 61 
 10125      67 65 54 69 
 10125      6D 65 6F 75 
 10125      74 43 6F 75 
 10125      6E 74 65 72 
 10125      00 
 10126 0328 A1 29 00 00 	.4byte 0x29a1
 10127 032c 55 53 42 53 	.asciz "USBStatusStageEnabledFlag1"
 10127      74 61 74 75 
 10127      73 53 74 61 
 10127      67 65 45 6E 
 10127      61 62 6C 65 
 10127      64 46 6C 61 
 10127      67 31 00 
 10128 0347 B3 29 00 00 	.4byte 0x29b3
 10129 034b 55 53 42 53 	.asciz "USBStatusStageEnabledFlag2"
 10129      74 61 74 75 
 10129      73 53 74 61 
 10129      67 65 45 6E 
 10129      61 62 6C 65 
 10129      64 46 6C 61 
 10129      67 32 00 
 10130 0366 C5 29 00 00 	.4byte 0x29c5
 10131 036a 55 53 42 31 	.asciz "USB1msTickCount"
 10131      6D 73 54 69 
 10131      63 6B 43 6F 
 10131      75 6E 74 00 
 10132 037a D7 29 00 00 	.4byte 0x29d7
 10133 037e 42 44 54 00 	.asciz "BDT"
 10134 0382 EE 29 00 00 	.4byte 0x29ee
 10135 0386 53 65 74 75 	.asciz "SetupPkt"
 10135      70 50 6B 74 
 10135      00 
 10136 038f 00 2A 00 00 	.4byte 0x2a00
 10137 0393 43 74 72 6C 	.asciz "CtrlTrfData"
 10137      54 72 66 44 
 10137      61 74 61 00 
 10138 039f 00 00 00 00 	.4byte 0x0
 10139                 	.section .debug_pubtypes,info
 10140 0000 74 02 00 00 	.4byte 0x274
MPLAB XC16 ASSEMBLY Listing:   			page 250


 10141 0004 02 00       	.2byte 0x2
 10142 0006 00 00 00 00 	.4byte .Ldebug_info0
 10143 000a 49 2A 00 00 	.4byte 0x2a49
 10144 000e E2 00 00 00 	.4byte 0xe2
 10145 0012 75 69 6E 74 	.asciz "uint8_t"
 10145      38 5F 74 00 
 10146 001a 02 01 00 00 	.4byte 0x102
 10147 001e 75 69 6E 74 	.asciz "uint16_t"
 10147      31 36 5F 74 
 10147      00 
 10148 0027 22 01 00 00 	.4byte 0x122
 10149 002b 75 69 6E 74 	.asciz "uint32_t"
 10149      33 32 5F 74 
 10149      00 
 10150 0034 61 01 00 00 	.4byte 0x161
 10151 0038 74 61 67 55 	.asciz "tagU1OTGIRBITS"
 10151      31 4F 54 47 
 10151      49 52 42 49 
 10151      54 53 00 
 10152 0047 14 02 00 00 	.4byte 0x214
 10153 004b 55 31 4F 54 	.asciz "U1OTGIRBITS"
 10153      47 49 52 42 
 10153      49 54 53 00 
 10154 0057 28 02 00 00 	.4byte 0x228
 10155 005b 74 61 67 55 	.asciz "tagU1OTGIEBITS"
 10155      31 4F 54 47 
 10155      49 45 42 49 
 10155      54 53 00 
 10156 006a DB 02 00 00 	.4byte 0x2db
 10157 006e 55 31 4F 54 	.asciz "U1OTGIEBITS"
 10157      47 49 45 42 
 10157      49 54 53 00 
 10158 007a EF 02 00 00 	.4byte 0x2ef
 10159 007e 74 61 67 55 	.asciz "tagU1OTGCONBITS"
 10159      31 4F 54 47 
 10159      43 4F 4E 42 
 10159      49 54 53 00 
 10160 008e B8 03 00 00 	.4byte 0x3b8
 10161 0092 55 31 4F 54 	.asciz "U1OTGCONBITS"
 10161      47 43 4F 4E 
 10161      42 49 54 53 
 10161      00 
 10162 009f 63 04 00 00 	.4byte 0x463
 10163 00a3 74 61 67 55 	.asciz "tagU1PWRCBITS"
 10163      31 50 57 52 
 10163      43 42 49 54 
 10163      53 00 
 10164 00b1 83 04 00 00 	.4byte 0x483
 10165 00b5 55 31 50 57 	.asciz "U1PWRCBITS"
 10165      52 43 42 49 
 10165      54 53 00 
 10166 00c0 80 05 00 00 	.4byte 0x580
 10167 00c4 74 61 67 55 	.asciz "tagU1IRBITS"
 10167      31 49 52 42 
 10167      49 54 53 00 
 10168 00d0 9E 05 00 00 	.4byte 0x59e
 10169 00d4 55 31 49 52 	.asciz "U1IRBITS"
MPLAB XC16 ASSEMBLY Listing:   			page 251


 10169      42 49 54 53 
 10169      00 
 10170 00dd 99 06 00 00 	.4byte 0x699
 10171 00e1 74 61 67 55 	.asciz "tagU1IEBITS"
 10171      31 49 45 42 
 10171      49 54 53 00 
 10172 00ed B7 06 00 00 	.4byte 0x6b7
 10173 00f1 55 31 49 45 	.asciz "U1IEBITS"
 10173      42 49 54 53 
 10173      00 
 10174 00fa E1 07 00 00 	.4byte 0x7e1
 10175 00fe 74 61 67 55 	.asciz "tagU1CONBITS"
 10175      31 43 4F 4E 
 10175      42 49 54 53 
 10175      00 
 10176 010b 00 08 00 00 	.4byte 0x800
 10177 010f 55 31 43 4F 	.asciz "U1CONBITS"
 10177      4E 42 49 54 
 10177      53 00 
 10178 0119 10 09 00 00 	.4byte 0x910
 10179 011d 74 61 67 55 	.asciz "tagU1EP0BITS"
 10179      31 45 50 30 
 10179      42 49 54 53 
 10179      00 
 10180 012a 2F 09 00 00 	.4byte 0x92f
 10181 012e 55 31 45 50 	.asciz "U1EP0BITS"
 10181      30 42 49 54 
 10181      53 00 
 10182 0138 41 09 00 00 	.4byte 0x941
 10183 013c 74 61 67 49 	.asciz "tagIFS5BITS"
 10183      46 53 35 42 
 10183      49 54 53 00 
 10184 0148 65 0A 00 00 	.4byte 0xa65
 10185 014c 49 46 53 35 	.asciz "IFS5BITS"
 10185      42 49 54 53 
 10185      00 
 10186 0155 76 0A 00 00 	.4byte 0xa76
 10187 0159 74 61 67 49 	.asciz "tagIEC5BITS"
 10187      45 43 35 42 
 10187      49 54 53 00 
 10188 0165 9A 0B 00 00 	.4byte 0xb9a
 10189 0169 49 45 43 35 	.asciz "IEC5BITS"
 10189      42 49 54 53 
 10189      00 
 10190 0172 7D 0C 00 00 	.4byte 0xc7d
 10191 0176 74 61 67 49 	.asciz "tagIPC21BITS"
 10191      50 43 32 31 
 10191      42 49 54 53 
 10191      00 
 10192 0183 9C 0C 00 00 	.4byte 0xc9c
 10193 0187 49 50 43 32 	.asciz "IPC21BITS"
 10193      31 42 49 54 
 10193      53 00 
 10194 0191 FD 0F 00 00 	.4byte 0xffd
 10195 0195 5F 55 53 42 	.asciz "_USB_DEVICE_DESCRIPTOR"
 10195      5F 44 45 56 
 10195      49 43 45 5F 
MPLAB XC16 ASSEMBLY Listing:   			page 252


 10195      44 45 53 43 
 10195      52 49 50 54 
 10195      4F 52 00 
 10196 01ac 4E 11 00 00 	.4byte 0x114e
 10197 01b0 55 53 42 5F 	.asciz "USB_DEVICE_DESCRIPTOR"
 10197      44 45 56 49 
 10197      43 45 5F 44 
 10197      45 53 43 52 
 10197      49 50 54 4F 
 10197      52 00 
 10198 01c6 A7 15 00 00 	.4byte 0x15a7
 10199 01ca 43 54 52 4C 	.asciz "CTRL_TRF_SETUP"
 10199      5F 54 52 46 
 10199      5F 53 45 54 
 10199      55 50 00 
 10200 01d9 40 16 00 00 	.4byte 0x1640
 10201 01dd 55 53 42 5F 	.asciz "USB_DEVICE_STATE"
 10201      44 45 56 49 
 10201      43 45 5F 53 
 10201      54 41 54 45 
 10201      00 
 10202 01ee 1D 17 00 00 	.4byte 0x171d
 10203 01f2 75 69 6E 74 	.asciz "uint16_t_VAL"
 10203      31 36 5F 74 
 10203      5F 56 41 4C 
 10203      00 
 10204 01ff 55 18 00 00 	.4byte 0x1855
 10205 0203 49 4E 5F 50 	.asciz "IN_PIPE"
 10205      49 50 45 00 
 10206 020b 30 19 00 00 	.4byte 0x1930
 10207 020f 4F 55 54 5F 	.asciz "OUT_PIPE"
 10207      50 49 50 45 
 10207      00 
 10208 0218 FF 19 00 00 	.4byte 0x19ff
 10209 021c 5F 42 44 5F 	.asciz "_BD_STAT"
 10209      53 54 41 54 
 10209      00 
 10210 0225 2B 1A 00 00 	.4byte 0x1a2b
 10211 0229 42 44 5F 53 	.asciz "BD_STAT"
 10211      54 41 54 00 
 10212 0231 C4 1A 00 00 	.4byte 0x1ac4
 10213 0235 5F 5F 42 44 	.asciz "__BDT"
 10213      54 00 
 10214 023b 15 1B 00 00 	.4byte 0x1b15
 10215 023f 42 44 54 5F 	.asciz "BDT_ENTRY"
 10215      45 4E 54 52 
 10215      59 00 
 10216 0249 82 1B 00 00 	.4byte 0x1b82
 10217 024d 5F 5F 55 53 	.asciz "__USTAT"
 10217      54 41 54 00 
 10218 0255 A4 1B 00 00 	.4byte 0x1ba4
 10219 0259 55 53 54 41 	.asciz "USTAT_FIELDS"
 10219      54 5F 46 49 
 10219      45 4C 44 53 
 10219      00 
 10220 0266 20 1C 00 00 	.4byte 0x1c20
 10221 026a 45 50 5F 53 	.asciz "EP_STATUS"
MPLAB XC16 ASSEMBLY Listing:   			page 253


 10221      54 41 54 55 
 10221      53 00 
 10222 0274 00 00 00 00 	.4byte 0x0
 10223                 	.section .debug_aranges,info
 10224 0000 14 00 00 00 	.4byte 0x14
 10225 0004 02 00       	.2byte 0x2
 10226 0006 00 00 00 00 	.4byte .Ldebug_info0
 10227 000a 04          	.byte 0x4
 10228 000b 00          	.byte 0x0
 10229 000c 00 00       	.2byte 0x0
 10230 000e 00 00       	.2byte 0x0
 10231 0010 00 00 00 00 	.4byte 0x0
 10232 0014 00 00 00 00 	.4byte 0x0
 10233                 	.section .debug_str,info
 10234                 	.LASF40:
 10235 0000 53 65 74 75 	.asciz "SetupPkt"
 10235      70 50 6B 74 
 10235      00 
 10236                 	.LASF38:
 10237 0009 55 53 42 53 	.asciz "USBStatusStageEnabledFlag2"
 10237      74 61 74 75 
 10237      73 53 74 61 
 10237      67 65 45 6E 
 10237      61 62 6C 65 
 10237      64 46 6C 61 
 10237      67 32 00 
 10238                 	.LASF29:
 10239 0024 70 42 44 54 	.asciz "pBDTEntryEP0OutNext"
 10239      45 6E 74 72 
 10239      79 45 50 30 
 10239      4F 75 74 4E 
 10239      65 78 74 00 
 10240                 	.LASF44:
 10241 0038 55 53 42 5F 	.asciz "USB_SD_Ptr"
 10241      53 44 5F 50 
 10241      74 72 00 
 10242                 	.LASF2:
 10243 0043 64 69 72 65 	.asciz "direction"
 10243      63 74 69 6F 
 10243      6E 00 
 10244                 	.LASF36:
 10245 004d 55 53 42 53 	.asciz "USBStatusStageTimeoutCounter"
 10245      74 61 74 75 
 10245      73 53 74 61 
 10245      67 65 54 69 
 10245      6D 65 6F 75 
 10245      74 43 6F 75 
 10245      6E 74 65 72 
 10245      00 
 10246                 	.LASF23:
 10247 006a 55 53 42 54 	.asciz "USBTicksSinceSuspendEnd"
 10247      69 63 6B 73 
 10247      53 69 6E 63 
 10247      65 53 75 73 
 10247      70 65 6E 64 
 10247      45 6E 64 00 
 10248                 	.LASF37:
MPLAB XC16 ASSEMBLY Listing:   			page 254


 10249 0082 55 53 42 53 	.asciz "USBStatusStageEnabledFlag1"
 10249      74 61 74 75 
 10249      73 53 74 61 
 10249      67 65 45 6E 
 10249      61 62 6C 65 
 10249      64 46 6C 61 
 10249      67 31 00 
 10250                 	.LASF22:
 10251 009d 55 53 42 41 	.asciz "USBActiveConfiguration"
 10251      63 74 69 76 
 10251      65 43 6F 6E 
 10251      66 69 67 75 
 10251      72 61 74 69 
 10251      6F 6E 00 
 10252                 	.LASF6:
 10253 00b4 55 31 4F 54 	.asciz "U1OTGIEbits"
 10253      47 49 45 62 
 10253      69 74 73 00 
 10254                 	.LASF28:
 10255 00c0 70 42 44 54 	.asciz "pBDTEntryEP0OutCurrent"
 10255      45 6E 74 72 
 10255      79 45 50 30 
 10255      4F 75 74 43 
 10255      75 72 72 65 
 10255      6E 74 00 
 10256                 	.LASF24:
 10257 00d7 6F 75 74 50 	.asciz "outPipes"
 10257      69 70 65 73 
 10257      00 
 10258                 	.LASF21:
 10259 00e0 55 53 42 44 	.asciz "USBDeviceState"
 10259      65 76 69 63 
 10259      65 53 74 61 
 10259      74 65 00 
 10260                 	.LASF5:
 10261 00ef 55 31 4F 54 	.asciz "U1OTGIRbits"
 10261      47 49 52 62 
 10261      69 74 73 00 
 10262                 	.LASF33:
 10263 00fb 42 6F 74 68 	.asciz "BothEP0OutUOWNsSet"
 10263      45 50 30 4F 
 10263      75 74 55 4F 
 10263      57 4E 73 53 
 10263      65 74 00 
 10264                 	.LASF41:
 10265 010e 43 74 72 6C 	.asciz "CtrlTrfData"
 10265      54 72 66 44 
 10265      61 74 61 00 
 10266                 	.LASF20:
 10267 011a 55 53 42 42 	.asciz "USBBusIsSuspended"
 10267      75 73 49 73 
 10267      53 75 73 70 
 10267      65 6E 64 65 
 10267      64 00 
 10268                 	.LASF34:
 10269 012c 65 70 5F 64 	.asciz "ep_data_in"
 10269      61 74 61 5F 
MPLAB XC16 ASSEMBLY Listing:   			page 255


 10269      69 6E 00 
 10270                 	.LASF11:
 10271 0137 55 31 43 4F 	.asciz "U1CONbits"
 10271      4E 62 69 74 
 10271      73 00 
 10272                 	.LASF12:
 10273 0141 55 31 45 50 	.asciz "U1EP0bits"
 10273      30 62 69 74 
 10273      73 00 
 10274                 	.LASF19:
 10275 014b 52 65 6D 6F 	.asciz "RemoteWakeup"
 10275      74 65 57 61 
 10275      6B 65 75 70 
 10275      00 
 10276                 	.LASF13:
 10277 0158 49 46 53 35 	.asciz "IFS5bits"
 10277      62 69 74 73 
 10277      00 
 10278                 	.LASF8:
 10279 0161 55 31 50 57 	.asciz "U1PWRCbits"
 10279      52 43 62 69 
 10279      74 73 00 
 10280                 	.LASF0:
 10281 016c 62 44 65 73 	.asciz "bDescriptorType"
 10281      63 72 69 70 
 10281      74 6F 72 54 
 10281      79 70 65 00 
 10282                 	.LASF30:
 10283 017c 73 68 6F 72 	.asciz "shortPacketStatus"
 10283      74 50 61 63 
 10283      6B 65 74 53 
 10283      74 61 74 75 
 10283      73 00 
 10284                 	.LASF35:
 10285 018e 65 70 5F 64 	.asciz "ep_data_out"
 10285      61 74 61 5F 
 10285      6F 75 74 00 
 10286                 	.LASF3:
 10287 019a 72 65 73 65 	.asciz "reserved"
 10287      72 76 65 64 
 10287      00 
 10288                 	.LASF25:
 10289 01a3 70 42 44 54 	.asciz "pBDTEntryOut"
 10289      45 6E 74 72 
 10289      79 4F 75 74 
 10289      00 
 10290                 	.LASF17:
 10291 01b0 55 53 42 44 	.asciz "USBDeferStatusStagePacket"
 10291      65 66 65 72 
 10291      53 74 61 74 
 10291      75 73 53 74 
 10291      61 67 65 50 
 10291      61 63 6B 65 
 10291      74 00 
 10292                 	.LASF1:
 10293 01ca 62 6D 52 65 	.asciz "bmRequestType"
 10293      71 75 65 73 
MPLAB XC16 ASSEMBLY Listing:   			page 256


 10293      74 54 79 70 
 10293      65 00 
 10294                 	.LASF31:
 10295 01d8 63 6F 6E 74 	.asciz "controlTransferState"
 10295      72 6F 6C 54 
 10295      72 61 6E 73 
 10295      66 65 72 53 
 10295      74 61 74 65 
 10295      00 
 10296                 	.LASF43:
 10297 01ed 55 53 42 5F 	.asciz "USB_CD_Ptr"
 10297      43 44 5F 50 
 10297      74 72 00 
 10298                 	.LASF16:
 10299 01f8 55 53 42 44 	.asciz "USBDeferOUTDataStagePackets"
 10299      65 66 65 72 
 10299      4F 55 54 44 
 10299      61 74 61 53 
 10299      74 61 67 65 
 10299      50 61 63 6B 
 10299      65 74 73 00 
 10300                 	.LASF15:
 10301 0214 49 50 43 32 	.asciz "IPC21bits"
 10301      31 62 69 74 
 10301      73 00 
 10302                 	.LASF10:
 10303 021e 55 31 49 45 	.asciz "U1IEbits"
 10303      62 69 74 73 
 10303      00 
 10304                 	.LASF27:
 10305 0227 55 53 42 41 	.asciz "USBAlternateInterface"
 10305      6C 74 65 72 
 10305      6E 61 74 65 
 10305      49 6E 74 65 
 10305      72 66 61 63 
 10305      65 00 
 10306                 	.LASF9:
 10307 023d 55 31 49 52 	.asciz "U1IRbits"
 10307      62 69 74 73 
 10307      00 
 10308                 	.LASF42:
 10309 0246 64 65 76 69 	.asciz "device_dsc"
 10309      63 65 5F 64 
 10309      73 63 00 
 10310                 	.LASF4:
 10311 0251 65 6E 64 70 	.asciz "endpoint_number"
 10311      6F 69 6E 74 
 10311      5F 6E 75 6D 
 10311      62 65 72 00 
 10312                 	.LASF7:
 10313 0261 55 31 4F 54 	.asciz "U1OTGCONbits"
 10313      47 43 4F 4E 
 10313      62 69 74 73 
 10313      00 
 10314                 	.LASF39:
 10315 026e 55 53 42 31 	.asciz "USB1msTickCount"
 10315      6D 73 54 69 
MPLAB XC16 ASSEMBLY Listing:   			page 257


 10315      63 6B 43 6F 
 10315      75 6E 74 00 
 10316                 	.LASF32:
 10317 027e 55 53 54 41 	.asciz "USTATcopy"
 10317      54 63 6F 70 
 10317      79 00 
 10318                 	.LASF26:
 10319 0288 70 42 44 54 	.asciz "pBDTEntryIn"
 10319      45 6E 74 72 
 10319      79 49 6E 00 
 10320                 	.LASF18:
 10321 0294 55 53 42 44 	.asciz "USBDeferINDataStagePackets"
 10321      65 66 65 72 
 10321      49 4E 44 61 
 10321      74 61 53 74 
 10321      61 67 65 50 
 10321      61 63 6B 65 
 10321      74 73 00 
 10322                 	.LASF14:
 10323 02af 49 45 43 35 	.asciz "IEC5bits"
 10323      62 69 74 73 
 10323      00 
 10324                 	.section .text,code
 10325              	
 10326              	
 10327              	
 10328              	.section __c30_info,info,bss
 10329                 	__psv_trap_errata:
 10330                 	
 10331                 	.section __c30_signature,info,data
 10332 0000 01 00       	.word 0x0001
 10333 0002 01 00       	.word 0x0001
 10334 0004 00 00       	.word 0x0000
 10335                 	
 10336                 	
 10337                 	
 10338                 	.set ___PA___,0
 10339                 	.end
MPLAB XC16 ASSEMBLY Listing:   			page 258


DEFINED SYMBOLS
                            *ABS*:00000000 /home/user/Documents/FST/Programming/project_sub-zero/lib/lib_pic33e/usb/usb_device.c
                            *ABS*:00000001 __33EP256MU806
                            *ABS*:00000001 __dsPIC33E
                            *ABS*:00000001 __HAS_DSP
                            *ABS*:00000001 __HAS_DMAV2
                            *ABS*:00000001 __HAS_CODEGUARD
                            *ABS*:00000001 __HAS_PMP_ENHANCED
                            *ABS*:00000001 __HAS_EDS
                            *ABS*:00000001 __HAS_EP
                            *ABS*:00000011 __TARGET_DIVIDE_CYCLES
                            *ABS*:00000001 __C30ELF
    {standard input}:14     .nbss:00000000 _USBDeviceState
    {standard input}:17     .nbss:00000002 _USBActiveConfiguration
    {standard input}:21     .bss:00000000 _USBAlternateInterface
    {standard input}:26     .nbss:00000004 _pBDTEntryEP0OutCurrent
    {standard input}:30     .nbss:00000006 _pBDTEntryEP0OutNext
    {standard input}:35     .bss:00000002 _pBDTEntryOut
    {standard input}:39     .bss:0000000c _pBDTEntryIn
    {standard input}:43     .nbss:00000008 _shortPacketStatus
    {standard input}:46     .nbss:00000009 _controlTransferState
    {standard input}:51     .bss:00000016 _inPipes
    {standard input}:54     .bss:0000001c _outPipes
    {standard input}:59     .nbss:0000000a _pDst
    {standard input}:62     .nbss:0000000c _RemoteWakeup
    {standard input}:65     .nbss:0000000d _USBBusIsSuspended
    {standard input}:68     .nbss:0000000e _USTATcopy
    {standard input}:71     .nbss:0000000f _endpoint_number
    {standard input}:74     .nbss:00000010 _BothEP0OutUOWNsSet
    {standard input}:78     .bss:00000023 _ep_data_in
    {standard input}:81     .bss:00000028 _ep_data_out
    {standard input}:85     .nbss:00000011 _USBStatusStageTimeoutCounter
    {standard input}:88     .nbss:00000012 _USBDeferStatusStagePacket
    {standard input}:91     .nbss:00000013 _USBStatusStageEnabledFlag1
    {standard input}:94     .nbss:00000014 _USBStatusStageEnabledFlag2
    {standard input}:97     .nbss:00000015 _USBDeferINDataStagePackets
    {standard input}:100    .nbss:00000016 _USBDeferOUTDataStagePackets
    {standard input}:104    .nbss:00000018 _USB1msTickCount
    {standard input}:107    .nbss:0000001c _USBTicksSinceSuspendEnd
    {standard input}:112    _0xf61649585d16559e:00000000 _BDT
    {standard input}:116    .bss:0000002d _SetupPkt
    {standard input}:119    .bss:00000035 _CtrlTrfData
    {standard input}:124    .text:00000000 _USBDeviceInit
    {standard input}:128    *ABS*:00000000 ___PA___
    {standard input}:193    *ABS*:00000000 ___BP___
    {standard input}:350    .text:000000fa _USBDeviceTasks
    {standard input}:3095   .text:0000104a _USBWakeFromSuspend
    {standard input}:3062   .text:0000102c _USBSuspend
    {standard input}:4342   .text:00001722 _USBIncrement1msInternalTimers
    {standard input}:1454   .text:00000758 _USBCtrlEPAllowStatusStage
    {standard input}:2980   .text:00000fb8 _USBStallHandler
    {standard input}:3125   .text:00001066 _USBCtrlEPService
    {standard input}:676    .text:0000029a _USBEnableEndpoint
    {standard input}:1799   .text:0000096c _USBConfigureEndpoint
    {standard input}:732    .text:000002dc _USBTransferOnePacket
    {standard input}:923    .text:00000412 _USBStallEndpoint
    {standard input}:1142   .text:00000582 _USBCancelIO
MPLAB XC16 ASSEMBLY Listing:   			page 259


    {standard input}:1372   .text:00000708 _USBDeviceDetach
    {standard input}:1395   .text:00000718 _USBDeviceAttach
    {standard input}:1645   .text:0000087a _USBCtrlEPAllowDataStage
    {standard input}:2124   .text:00000b48 _USBCtrlTrfTxService
    {standard input}:1871   .text:000009ce _USBCtrlEPServiceComplete
    {standard input}:2256   .text:00000be2 _USBCtrlTrfRxService
    {standard input}:2578   .text:00000de2 _USBStdSetCfgHandler
    {standard input}:2692   .text:00000e60 _USBStdGetDscHandler
    {standard input}:2822   .text:00000efe _USBStdGetStatusHandler
    {standard input}:3211   .text:000010d4 _USBCtrlTrfSetupHandler
    {standard input}:3352   .text:00001196 _USBCtrlTrfOutHandler
    {standard input}:3446   .text:0000121c _USBCtrlTrfInHandler
    {standard input}:3676   .text:00001372 _USBCheckStdRequest
    {standard input}:3840   .text:0000142c _USBStdFeatureReqHandler
    {standard input}:4385   .text:00001750 _USBGet1msTickCount
    {standard input}:10329  __c30_info:00000000 __psv_trap_errata
    {standard input}:129    .text:00000000 .L0
                            .text:00000044 .L2
                            .text:00000036 .L3
                            .text:00000052 .L4
                            .text:000000c0 .L5
                            .text:00000092 .L6
                            .text:00000118 .L8
                            .text:0000012e .L9
                            .text:0000013a .L10
                            .text:00000292 .L7
                            .text:00000156 .L12
                            .text:00000168 .L13
                            .text:00000178 .L14
                            .text:000001a8 .L15
                            .text:00000190 .L16
                            .text:000001a0 .L17
                            .text:000001be .L18
                            .text:000001de .L19
                            .text:000001e8 .L20
                            .text:00000290 .L21
                            .text:00000288 .L22
                            .text:00000290 .L29
                            .text:0000024e .L24
                            .text:00000274 .L25
                            .text:0000027e .L26
                            .text:00000286 .L27
                            .text:000001f6 .L28
                            .text:000002b0 .L31
                            .text:000002c2 .L32
                            .text:000002fa .L34
                            .text:00000306 .L35
                            .text:00000310 .L36
                            .text:0000040a .L37
                            .text:000003e8 .L38
                            .text:00000406 .L39
                            .text:000004bc .L41
                            .text:0000057a .L40
                            .text:000006fc .L43
                            .text:00000750 .L46
                            .text:00000744 .L48
                            .text:00000742 .L49
MPLAB XC16 ASSEMBLY Listing:   			page 260


                            .text:00000872 .L50
                            .text:000007c4 .L52
                            .text:000008ee .L54
                            .text:00000964 .L53
                            .text:0000090e .L56
                            .text:000009a2 .L58
                            .text:000009b0 .L59
                            .text:00000a9a .L61
                            .text:000009fc .L62
                            .text:000009f6 .L63
                            .text:00000b40 .L60
                            .text:00000ac4 .L65
                            .text:00000ab4 .L66
                            .text:00000b6e .L68
                            .text:00000b64 .L69
                            .text:00000bd4 .L74
                            .text:00000bba .L71
                            .text:00000ba8 .L72
                            .text:00000bda .L67
                            .text:00000bc2 .L75
                            .text:00000c16 .L77
                            .text:00000c82 .L78
                            .text:00000c46 .L79
                            .text:00000d48 .L80
                            .text:00000d0e .L81
                            .text:00000dda .L76
                            .text:00000dc8 .L83
                            .text:00000e18 .L85
                            .text:00000e02 .L86
                            .text:00000e4c .L87
                            .text:00000e58 .L84
                            .text:00000ef6 .L89
                            .text:00000e8e .L93
                            .text:00000ec6 .L94
                            .text:00000ef0 .L99
                            .text:00000ebe .L95
                            .text:00000ee8 .L97
                            .text:00000f38 .L103
                            .text:00000f40 .L104
                            .text:00000f96 .L101
                            .text:00000f92 .L110
                            .text:00000f64 .L106
                            .text:00000f74 .L107
                            .text:00000f94 .L111
                            .text:00000f96 .L112
                            .text:00000fb0 .L100
                            .text:00001020 .L114
                            .text:0000101e .L115
                            .text:000010c0 .L119
                            .text:000010bc .L120
                            .text:000010cc .L118
                            .text:000011a2 .L124
                            .text:00001214 .L123
                            .text:00001212 .L126
                            .text:00001250 .L128
                            .text:0000124c .L129
                            .text:0000132e .L130
MPLAB XC16 ASSEMBLY Listing:   			page 261


                            .text:000012b2 .L131
                            .text:0000136a .L127
                            .text:000012f4 .L133
                            .text:00001368 .L134
                            .text:00001360 .L135
                            .text:00001420 .L150
                            .text:00001422 .L151
                            .text:000013d6 .L140
                            .text:000013da .L141
                            .text:00001424 .L152
                            .text:000013aa .L142
                            .text:000013b6 .L143
                            .text:00001416 .L144
                            .text:000013be .L145
                            .text:000013ba .L146
                            .text:000013de .L147
                            .text:000013fe .L148
                            .text:00001424 .L136
                            .text:00001456 .L154
                            .text:00001454 .L155
                            .text:0000171a .L153
                            .text:000014ba .L157
                            .text:000014da .L158
                            .text:000014ea .L159
                            .text:000014f0 .L160
                            .text:00001510 .L161
                            .text:00001522 .L162
                            .text:000015b6 .L163
                            .text:00001568 .L164
                            .text:00001556 .L165
                            .text:0000161e .L166
                            .text:0000164e .L167
                            .text:00001668 .L168
                            .text:000016ce .L169
                            .text:00001688 .L170
                            .text:0000169a .L171
                            .text:00001700 .L172
                            .text:00001748 .L173
                            .text:00001752 .L176
                    .debug_abbrev:00000000 .Ldebug_abbrev0
                            .text:00000000 .Ltext0
                            .text:00001772 .Letext0
                      .debug_line:00000000 .Ldebug_line0
                       .debug_str:0000016c .LASF0
                       .debug_str:000001ca .LASF1
                       .debug_str:00000043 .LASF2
                       .debug_str:0000019a .LASF3
                       .debug_str:00000251 .LASF4
                            .text:00000000 .LFB0
                            .text:000000fa .LFE0
                            .text:000000fa .LFB1
                            .text:0000029a .LFE1
                            .text:0000029a .LFB2
                            .text:000002dc .LFE2
                            .text:000002dc .LFB3
                            .text:00000412 .LFE3
                            .text:00000412 .LFB4
MPLAB XC16 ASSEMBLY Listing:   			page 262


                            .text:00000582 .LFE4
                            .text:00000582 .LFB5
                            .text:00000708 .LFE5
                            .text:00000708 .LFB6
                            .text:00000718 .LFE6
                            .text:00000718 .LFB7
                            .text:00000758 .LFE7
                            .text:00000758 .LFB8
                            .text:0000087a .LFE8
                            .text:0000087a .LFB9
                            .text:0000096c .LFE9
                            .text:0000096c .LFB10
                            .text:000009ce .LFE10
                            .text:000009ce .LFB11
                            .text:00000b48 .LFE11
                            .text:00000b48 .LFB12
                            .text:00000be2 .LFE12
                            .text:00000be2 .LFB13
                            .text:00000de2 .LFE13
                            .text:00000de2 .LFB14
                            .text:00000e60 .LFE14
                            .text:00000e60 .LFB15
                            .text:00000efe .LFE15
                            .text:00000efe .LFB16
                            .text:00000fb8 .LFE16
                            .text:00000f40 .LBB2
                            .text:00000f42 .LBE2
                            .text:00000fb8 .LFB17
                            .text:0000102c .LFE17
                            .text:0000102c .LFB18
                            .text:0000104a .LFE18
                            .text:0000104a .LFB19
                            .text:00001066 .LFE19
                            .text:00001066 .LFB20
                            .text:000010d4 .LFE20
                            .text:000010d4 .LFB21
                            .text:00001196 .LFE21
                            .text:00001196 .LFB22
                            .text:0000121c .LFE22
                            .text:0000121c .LFB23
                            .text:00001372 .LFE23
                            .text:00001372 .LFB24
                            .text:0000142c .LFE24
                            .text:0000142c .LFB25
                            .text:00001722 .LFE25
                            .text:00001722 .LFB26
                            .text:00001750 .LFE26
                            .text:00001750 .LFB27
                            .text:00001772 .LFE27
                       .debug_str:000000ef .LASF5
                       .debug_str:000000b4 .LASF6
                       .debug_str:00000261 .LASF7
                       .debug_str:00000161 .LASF8
                       .debug_str:0000023d .LASF9
                       .debug_str:0000021e .LASF10
                       .debug_str:00000137 .LASF11
                       .debug_str:00000141 .LASF12
MPLAB XC16 ASSEMBLY Listing:   			page 263


                       .debug_str:00000158 .LASF13
                       .debug_str:000002af .LASF14
                       .debug_str:00000214 .LASF15
                       .debug_str:000001f8 .LASF16
                       .debug_str:000001b0 .LASF17
                       .debug_str:00000294 .LASF18
                       .debug_str:0000014b .LASF19
                       .debug_str:0000011a .LASF20
                       .debug_str:000000e0 .LASF21
                       .debug_str:0000009d .LASF22
                       .debug_str:0000006a .LASF23
                       .debug_str:000000d7 .LASF24
                       .debug_str:000001a3 .LASF25
                       .debug_str:00000288 .LASF26
                       .debug_str:00000227 .LASF27
                       .debug_str:000000c0 .LASF28
                       .debug_str:00000024 .LASF29
                       .debug_str:0000017c .LASF30
                       .debug_str:000001d8 .LASF31
                       .debug_str:0000027e .LASF32
                       .debug_str:000000fb .LASF33
                       .debug_str:0000012c .LASF34
                       .debug_str:0000018e .LASF35
                       .debug_str:0000004d .LASF36
                       .debug_str:00000082 .LASF37
                       .debug_str:00000009 .LASF38
                       .debug_str:0000026e .LASF39
                       .debug_str:00000000 .LASF40
                       .debug_str:0000010e .LASF41
                       .debug_str:00000246 .LASF42
                       .debug_str:000001ed .LASF43
                       .debug_str:00000038 .LASF44
                     .debug_frame:00000000 .Lframe0
                      .debug_info:00000000 .Ldebug_info0

UNDEFINED SYMBOLS
_IEC5bits
_U1EIR
_U1IR
_U1EP0
_U1EP1
_memset
_U1CNFG1
_U1CNFG2
_U1OTGCONbits
_U1EIE
_U1IE
_U1OTGIEbits
_U1PWRCbits
_U1BDTP1
_U1CONbits
_U1ADDR
_U1IRbits
CORCON
_U1IEbits
_U1OTGIRbits
_U1OTGIR
MPLAB XC16 ASSEMBLY Listing:   			page 264


_IFS5bits
_USER_USB_CALLBACK_EVENT_HANDLER
_U1STAT
_U1CON
_IPC21bits
_device_dsc
_USB_CD_Ptr
_USB_SD_Ptr
_U1EP0bits
_memcpy

EQUATE SYMBOLS
/home/user/Documents/FST/Programming/project_sub-zero/lib/lib_pic33e/usb/usb_device.c = 0x0
                  __33EP256MU806 = 0x1
                      __dsPIC33E = 0x1
                       __HAS_DSP = 0x1
                     __HAS_DMAV2 = 0x1
                 __HAS_CODEGUARD = 0x1
              __HAS_PMP_ENHANCED = 0x1
                       __HAS_EDS = 0x1
                        __HAS_EP = 0x1
          __TARGET_DIVIDE_CYCLES = 0x11
                        __C30ELF = 0x1
                        ___PA___ = 0x0
                        ___BP___ = 0x0
