MPLAB XC16 ASSEMBLY Listing:   			page 1


   1              	.file "/home/user/Documents/FST/Programming/project_sub-zero/lib/lib_pic33e/usb/app_device_cdc_basic.c"
   2              	.section .debug_abbrev,info
   3                 	.Ldebug_abbrev0:
   4                 	.section .debug_info,info
   5                 	.Ldebug_info0:
   6                 	.section .debug_line,info
   7                 	.Ldebug_line0:
   8 0000 8C 01 00 00 	.section .text,code
   8      02 00 3F 01 
   8      00 00 01 01 
   8      FB 0E 0A 00 
   8      01 01 01 01 
   8      00 00 00 01 
   8      6C 69 62 2F 
   8      6C 69 62 5F 
   8      70 69 63 33 
   9              	.Ltext0:
  10              	.section .bss,bss
  11                 	.type _readBufferUSB,@object
  12                 	.size _readBufferUSB,64
  13                 	_readBufferUSB:
  14 0000 00 00 00 00 	.skip 64
  14      00 00 00 00 
  14      00 00 00 00 
  14      00 00 00 00 
  14      00 00 00 00 
  14      00 00 00 00 
  14      00 00 00 00 
  14      00 00 00 00 
  14      00 00 00 00 
  15                 	.type _outBufferUSB,@object
  16                 	.global _outBufferUSB
  17 0040 00 00 00 00 	_outBufferUSB:.space 256
  17      00 00 00 00 
  17      00 00 00 00 
  17      00 00 00 00 
  17      00 00 00 00 
  17      00 00 00 00 
  17      00 00 00 00 
  17      00 00 00 00 
  17      00 00 00 00 
  18                 	.section .text,code
  19              	.align 2
  20              	.global _APP_DeviceCDCBasicDemoInitialize
  21              	.type _APP_DeviceCDCBasicDemoInitialize,@function
  22              	_APP_DeviceCDCBasicDemoInitialize:
  23              	.LFB0:
  24              	.file 1 "lib/lib_pic33e/usb/app_device_cdc_basic.c"
   1:lib/lib_pic33e/usb/app_device_cdc_basic.c **** #ifndef NOUSB
   2:lib/lib_pic33e/usb/app_device_cdc_basic.c **** /*******************************************************************************
   3:lib/lib_pic33e/usb/app_device_cdc_basic.c **** Copyright 2016 Microchip Technology Inc. (www.microchip.com)
   4:lib/lib_pic33e/usb/app_device_cdc_basic.c **** 
   5:lib/lib_pic33e/usb/app_device_cdc_basic.c **** Licensed under the Apache License, Version 2.0 (the "License");
   6:lib/lib_pic33e/usb/app_device_cdc_basic.c **** you may not use this file except in compliance with the License.
   7:lib/lib_pic33e/usb/app_device_cdc_basic.c **** You may obtain a copy of the License at
   8:lib/lib_pic33e/usb/app_device_cdc_basic.c **** 
   9:lib/lib_pic33e/usb/app_device_cdc_basic.c ****     http://www.apache.org/licenses/LICENSE-2.0
MPLAB XC16 ASSEMBLY Listing:   			page 2


  10:lib/lib_pic33e/usb/app_device_cdc_basic.c **** 
  11:lib/lib_pic33e/usb/app_device_cdc_basic.c **** Unless required by applicable law or agreed to in writing, software
  12:lib/lib_pic33e/usb/app_device_cdc_basic.c **** distributed under the License is distributed on an "AS IS" BASIS,
  13:lib/lib_pic33e/usb/app_device_cdc_basic.c **** WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  14:lib/lib_pic33e/usb/app_device_cdc_basic.c **** See the License for the specific language governing permissions and
  15:lib/lib_pic33e/usb/app_device_cdc_basic.c **** limitations under the License.
  16:lib/lib_pic33e/usb/app_device_cdc_basic.c **** 
  17:lib/lib_pic33e/usb/app_device_cdc_basic.c **** To request to license the code under the MLA license (www.microchip.com/mla_license), 
  18:lib/lib_pic33e/usb/app_device_cdc_basic.c **** please contact mla_licensing@microchip.com
  19:lib/lib_pic33e/usb/app_device_cdc_basic.c **** *******************************************************************************/
  20:lib/lib_pic33e/usb/app_device_cdc_basic.c **** 
  21:lib/lib_pic33e/usb/app_device_cdc_basic.c **** /** INCLUDES *******************************************************/
  22:lib/lib_pic33e/usb/app_device_cdc_basic.c **** #include "system.h"
  23:lib/lib_pic33e/usb/app_device_cdc_basic.c **** 
  24:lib/lib_pic33e/usb/app_device_cdc_basic.c **** #include <stdint.h>
  25:lib/lib_pic33e/usb/app_device_cdc_basic.c **** #include <string.h>
  26:lib/lib_pic33e/usb/app_device_cdc_basic.c **** #include <stddef.h>
  27:lib/lib_pic33e/usb/app_device_cdc_basic.c **** 
  28:lib/lib_pic33e/usb/app_device_cdc_basic.c **** #include "usb.h"
  29:lib/lib_pic33e/usb/app_device_cdc_basic.c **** 
  30:lib/lib_pic33e/usb/app_device_cdc_basic.c **** #include "app_device_cdc_basic.h"
  31:lib/lib_pic33e/usb/app_device_cdc_basic.c **** #include "usb_config.h"
  32:lib/lib_pic33e/usb/app_device_cdc_basic.c **** 
  33:lib/lib_pic33e/usb/app_device_cdc_basic.c **** #include "../usb_lib.h"
  34:lib/lib_pic33e/usb/app_device_cdc_basic.c **** 
  35:lib/lib_pic33e/usb/app_device_cdc_basic.c **** /** VARIABLES ******************************************************/
  36:lib/lib_pic33e/usb/app_device_cdc_basic.c **** 
  37:lib/lib_pic33e/usb/app_device_cdc_basic.c **** //static bool buttonPressed;
  38:lib/lib_pic33e/usb/app_device_cdc_basic.c **** //static char buttonMessage[] = "Button pressed.\r\n";
  39:lib/lib_pic33e/usb/app_device_cdc_basic.c **** static uint8_t readBufferUSB[CDC_DATA_OUT_EP_SIZE];
  40:lib/lib_pic33e/usb/app_device_cdc_basic.c **** uint8_t outBufferUSB[BULK_SIZE_USB];
  41:lib/lib_pic33e/usb/app_device_cdc_basic.c **** 
  42:lib/lib_pic33e/usb/app_device_cdc_basic.c **** 
  43:lib/lib_pic33e/usb/app_device_cdc_basic.c **** /*********************************************************************
  44:lib/lib_pic33e/usb/app_device_cdc_basic.c **** * Function: void APP_DeviceCDCBasicDemoInitialize(void);
  45:lib/lib_pic33e/usb/app_device_cdc_basic.c **** *
  46:lib/lib_pic33e/usb/app_device_cdc_basic.c **** * Overview: Initializes the demo code
  47:lib/lib_pic33e/usb/app_device_cdc_basic.c **** *
  48:lib/lib_pic33e/usb/app_device_cdc_basic.c **** * PreCondition: None
  49:lib/lib_pic33e/usb/app_device_cdc_basic.c **** *
  50:lib/lib_pic33e/usb/app_device_cdc_basic.c **** * Input: None
  51:lib/lib_pic33e/usb/app_device_cdc_basic.c **** *
  52:lib/lib_pic33e/usb/app_device_cdc_basic.c **** * Output: None
  53:lib/lib_pic33e/usb/app_device_cdc_basic.c **** *
  54:lib/lib_pic33e/usb/app_device_cdc_basic.c **** ********************************************************************/
  55:lib/lib_pic33e/usb/app_device_cdc_basic.c **** void APP_DeviceCDCBasicDemoInitialize()
  56:lib/lib_pic33e/usb/app_device_cdc_basic.c **** {   
  25              	.loc 1 56 0
  26              	.set ___PA___,1
  27 000000  00 00 FA 	lnk #0
  28              	.LCFI0:
  57:lib/lib_pic33e/usb/app_device_cdc_basic.c ****     line_coding.bCharFormat = 0;
  29              	.loc 1 57 0
  30 000002  46 00 20 	mov #_line_coding+4,w6
  31 000004  80 43 EB 	clr.b w7
  58:lib/lib_pic33e/usb/app_device_cdc_basic.c ****     line_coding.bDataBits = 8;
  32              	.loc 1 58 0
MPLAB XC16 ASSEMBLY Listing:   			page 3


  33 000006  64 00 20 	mov #_line_coding+6,w4
  34 000008  85 C0 B3 	mov.b #8,w5
  59:lib/lib_pic33e/usb/app_device_cdc_basic.c ****     line_coding.bParityType = 0;
  35              	.loc 1 59 0
  36 00000a  52 00 20 	mov #_line_coding+5,w2
  37 00000c  80 41 EB 	clr.b w3
  60:lib/lib_pic33e/usb/app_device_cdc_basic.c ****     line_coding.dwDTERate = 9600;
  38              	.loc 1 60 0
  39 00000e  00 58 22 	mov #9600,w0
  40 000010  01 00 20 	mov #0,w1
  41              	.loc 1 57 0
  42 000012  07 4B 78 	mov.b w7,[w6]
  43              	.loc 1 58 0
  44 000014  05 4A 78 	mov.b w5,[w4]
  45              	.loc 1 59 0
  46 000016  03 49 78 	mov.b w3,[w2]
  47              	.loc 1 60 0
  48 000018  00 00 88 	mov w0,_line_coding
  49 00001a  11 00 88 	mov w1,_line_coding+2
  61:lib/lib_pic33e/usb/app_device_cdc_basic.c **** }
  50              	.loc 1 61 0
  51 00001c  8E 07 78 	mov w14,w15
  52 00001e  4F 07 78 	mov [--w15],w14
  53 000020  00 40 A9 	bclr CORCON,#2
  54 000022  00 00 06 	return 
  55              	.set ___PA___,0
  56              	.LFE0:
  57              	.size _APP_DeviceCDCBasicDemoInitialize,.-_APP_DeviceCDCBasicDemoInitialize
  58              	.align 2
  59              	.global _APP_DeviceCDCBasicDemoTasks
  60              	.type _APP_DeviceCDCBasicDemoTasks,@function
  61              	_APP_DeviceCDCBasicDemoTasks:
  62              	.LFB1:
  62:lib/lib_pic33e/usb/app_device_cdc_basic.c **** 
  63:lib/lib_pic33e/usb/app_device_cdc_basic.c **** /*********************************************************************
  64:lib/lib_pic33e/usb/app_device_cdc_basic.c **** * Function: void APP_DeviceCDCBasicDemoTasks(void);
  65:lib/lib_pic33e/usb/app_device_cdc_basic.c **** *
  66:lib/lib_pic33e/usb/app_device_cdc_basic.c **** * Overview: Keeps the demo running.
  67:lib/lib_pic33e/usb/app_device_cdc_basic.c **** *
  68:lib/lib_pic33e/usb/app_device_cdc_basic.c **** * PreCondition:pThe demo should have been initialized and started via
  69:lib/lib_pic33e/usb/app_device_cdc_basic.c **** *   the APP_DeviceCDCBasicDemoInitialize() and APP_DeviceCDCBasicDemoStart() demos
  70:lib/lib_pic33e/usb/app_device_cdc_basic.c **** *   respectively.
  71:lib/lib_pic33e/usb/app_device_cdc_basic.c **** *
  72:lib/lib_pic33e/usb/app_device_cdc_basic.c **** * Input: None
  73:lib/lib_pic33e/usb/app_device_cdc_basic.c **** *
  74:lib/lib_pic33e/usb/app_device_cdc_basic.c **** * Output: None
  75:lib/lib_pic33e/usb/app_device_cdc_basic.c **** *
  76:lib/lib_pic33e/usb/app_device_cdc_basic.c **** ********************************************************************/
  77:lib/lib_pic33e/usb/app_device_cdc_basic.c **** void APP_DeviceCDCBasicDemoTasks()
  78:lib/lib_pic33e/usb/app_device_cdc_basic.c **** {
  63              	.loc 1 78 0
  64              	.set ___PA___,1
  65 000024  44 00 FA 	lnk #68
  66              	.LCFI1:
  79:lib/lib_pic33e/usb/app_device_cdc_basic.c **** 	unsigned int aux,i,j;
  80:lib/lib_pic33e/usb/app_device_cdc_basic.c ****     /* If the USB device isn't configured yet, we can't really do anything
  81:lib/lib_pic33e/usb/app_device_cdc_basic.c ****      * else since we don't have a host to talk to.  So jump back to the
MPLAB XC16 ASSEMBLY Listing:   			page 4


  82:lib/lib_pic33e/usb/app_device_cdc_basic.c ****      * top of the while loop. */
  83:lib/lib_pic33e/usb/app_device_cdc_basic.c ****     if( USBGetDeviceState() < CONFIGURED_STATE )
  67              	.loc 1 83 0
  68 000026  00 00 80 	mov _USBDeviceState,w0
  69 000028  FF 0F 50 	sub w0,#31,[w15]
  70              	.set ___BP___,0
  71 00002a  00 00 36 	bra leu,.L13
  72              	.L3:
  84:lib/lib_pic33e/usb/app_device_cdc_basic.c ****     {
  85:lib/lib_pic33e/usb/app_device_cdc_basic.c ****         return;
  86:lib/lib_pic33e/usb/app_device_cdc_basic.c ****     }
  87:lib/lib_pic33e/usb/app_device_cdc_basic.c **** 
  88:lib/lib_pic33e/usb/app_device_cdc_basic.c **** 
  89:lib/lib_pic33e/usb/app_device_cdc_basic.c ****     /* If we are currently suspended, then we need to see if we need to
  90:lib/lib_pic33e/usb/app_device_cdc_basic.c ****      * issue a remote wakeup.  In either case, we shouldn't process any
  91:lib/lib_pic33e/usb/app_device_cdc_basic.c ****      * keyboard commands since we aren't currently communicating to the host
  92:lib/lib_pic33e/usb/app_device_cdc_basic.c ****      * thus just continue back to the start of the while loop. */
  93:lib/lib_pic33e/usb/app_device_cdc_basic.c ****     if( USBIsDeviceSuspended() == true )
  73              	.loc 1 93 0
  74 00002c  00 00 80 	mov _U1PWRCbits,w0
  75 00002e  62 00 60 	and w0,#2,w0
  76 000030  00 00 E0 	cp0 w0
  77              	.set ___BP___,0
  78 000032  00 00 3A 	bra nz,.L14
  79              	.L5:
  94:lib/lib_pic33e/usb/app_device_cdc_basic.c ****     {
  95:lib/lib_pic33e/usb/app_device_cdc_basic.c ****         return;
  96:lib/lib_pic33e/usb/app_device_cdc_basic.c ****     }
  97:lib/lib_pic33e/usb/app_device_cdc_basic.c ****         
  98:lib/lib_pic33e/usb/app_device_cdc_basic.c ****     /*Deleted code that sent a message everytime a button was pressed, 
  99:lib/lib_pic33e/usb/app_device_cdc_basic.c ****     using functions mUSBUSARTIsTxTrfReady() and putrsUSBUSART(String message)*/
 100:lib/lib_pic33e/usb/app_device_cdc_basic.c **** 
 101:lib/lib_pic33e/usb/app_device_cdc_basic.c ****     /* Check to see if there is a transmission in progress, if there isn't, then
 102:lib/lib_pic33e/usb/app_device_cdc_basic.c ****      * we can see about performing an echo response to data received.
 103:lib/lib_pic33e/usb/app_device_cdc_basic.c ****      */
 104:lib/lib_pic33e/usb/app_device_cdc_basic.c ****     if(USBUSARTIsTxTrfReady())
  80              	.loc 1 104 0
  81 000034  00 C0 BF 	mov.b _cdc_trf_state,WREG
  82 000036  00 04 E0 	cp0.b w0
  83              	.set ___BP___,0
  84 000038  00 00 3A 	bra nz,.L6
  85              	.LBB2:
 105:lib/lib_pic33e/usb/app_device_cdc_basic.c ****     {
 106:lib/lib_pic33e/usb/app_device_cdc_basic.c ****         uint8_t numBytesRead=0;
  86              	.loc 1 106 0
  87 00003a  00 40 EB 	clr.b w0
 107:lib/lib_pic33e/usb/app_device_cdc_basic.c **** 
 108:lib/lib_pic33e/usb/app_device_cdc_basic.c ****         char readString[CDC_DATA_OUT_EP_SIZE];
 109:lib/lib_pic33e/usb/app_device_cdc_basic.c **** 
 110:lib/lib_pic33e/usb/app_device_cdc_basic.c ****         numBytesRead = getsUSBUSART(readBufferUSB, sizeof(readBufferUSB));
  88              	.loc 1 110 0
  89 00003c  01 C4 B3 	mov.b #64,w1
  90              	.loc 1 106 0
  91 00003e  20 47 98 	mov.b w0,[w14+2]
  92              	.loc 1 110 0
  93 000040  00 00 20 	mov #_readBufferUSB,w0
  94 000042  00 00 07 	rcall _getsUSBUSART
MPLAB XC16 ASSEMBLY Listing:   			page 5


 111:lib/lib_pic33e/usb/app_device_cdc_basic.c **** 
 112:lib/lib_pic33e/usb/app_device_cdc_basic.c ****         /* For every byte that was read... */
 113:lib/lib_pic33e/usb/app_device_cdc_basic.c ****         for(i=0; i<numBytesRead; i++){
  95              	.loc 1 113 0
  96 000044  80 00 EB 	clr w1
  97              	.loc 1 110 0
  98 000046  20 47 98 	mov.b w0,[w14+2]
  99              	.loc 1 113 0
 100 000048  01 0F 78 	mov w1,[w14]
 101 00004a  00 00 37 	bra .L7
 102              	.L8:
 114:lib/lib_pic33e/usb/app_device_cdc_basic.c ****             //echo everything that is read
 115:lib/lib_pic33e/usb/app_device_cdc_basic.c ****             readString[i] = readBufferUSB[i];        
 103              	.loc 1 115 0
 104 00004c  00 00 20 	mov #_readBufferUSB,w0
 105 00004e  9E 00 40 	add w0,[w14],w1
 106 000050  1E 01 78 	mov [w14],w2
 107 000052  63 00 41 	add w2,#3,w0
 108              	.loc 1 113 0
 109 000054  1E 0F E8 	inc [w14],[w14]
 110              	.loc 1 115 0
 111 000056  91 40 78 	mov.b [w1],w1
 112 000058  01 77 78 	mov.b w1,[w14+w0]
 113              	.L7:
 114              	.loc 1 113 0
 115 00005a  00 00 00 	nop 
 116 00005c  2E 40 90 	mov.b [w14+2],w0
 117 00005e  00 80 FB 	ze w0,w0
 118 000060  9E 0F 50 	sub w0,[w14],[w15]
 119              	.set ___BP___,0
 120 000062  00 00 3E 	bra gtu,.L8
 116:lib/lib_pic33e/usb/app_device_cdc_basic.c ****         }
 117:lib/lib_pic33e/usb/app_device_cdc_basic.c ****         readString[i]='\0';
 121              	.loc 1 117 0
 122 000064  1E 01 78 	mov [w14],w2
 123 000066  E3 00 41 	add w2,#3,w1
 124 000068  00 41 EB 	clr.b w2
 118:lib/lib_pic33e/usb/app_device_cdc_basic.c **** 
 119:lib/lib_pic33e/usb/app_device_cdc_basic.c ****         if(numBytesRead > 0){
 125              	.loc 1 119 0
 126 00006a  2E 40 90 	mov.b [w14+2],w0
 127              	.loc 1 117 0
 128 00006c  02 F7 78 	mov.b w2,[w14+w1]
 129              	.loc 1 119 0
 130 00006e  00 04 E0 	cp0.b w0
 131              	.set ___BP___,0
 132 000070  00 00 32 	bra z,.L9
 120:lib/lib_pic33e/usb/app_device_cdc_basic.c ****             /* After processing all of the received data, we need to send out
 121:lib/lib_pic33e/usb/app_device_cdc_basic.c ****                 * the "echo" data now.
 122:lib/lib_pic33e/usb/app_device_cdc_basic.c ****             */
 123:lib/lib_pic33e/usb/app_device_cdc_basic.c **** 
 124:lib/lib_pic33e/usb/app_device_cdc_basic.c ****             receive_handler_usb(readString);
 133              	.loc 1 124 0
 134 000072  63 00 47 	add w14,#3,w0
 135 000074  00 00 07 	rcall _receive_handler_usb
 125:lib/lib_pic33e/usb/app_device_cdc_basic.c **** 			return;
 136              	.loc 1 125 0
MPLAB XC16 ASSEMBLY Listing:   			page 6


 137 000076  00 00 37 	bra .L2
 138              	.L9:
 126:lib/lib_pic33e/usb/app_device_cdc_basic.c ****         }
 127:lib/lib_pic33e/usb/app_device_cdc_basic.c ****         //if nothing is read, send pending messages
 128:lib/lib_pic33e/usb/app_device_cdc_basic.c ****         else {
 129:lib/lib_pic33e/usb/app_device_cdc_basic.c **** 			/* If we are right behind the write pointer we might have something
 130:lib/lib_pic33e/usb/app_device_cdc_basic.c **** 			 * to read.
 131:lib/lib_pic33e/usb/app_device_cdc_basic.c **** 			 *
 132:lib/lib_pic33e/usb/app_device_cdc_basic.c **** 			 * If the inner_write_index_usb is 0 then nothing has been written.
 133:lib/lib_pic33e/usb/app_device_cdc_basic.c **** 			 *
 134:lib/lib_pic33e/usb/app_device_cdc_basic.c **** 			 * Else there is stuff to send, even if it is not a full package.
 135:lib/lib_pic33e/usb/app_device_cdc_basic.c **** 			 * The last position is pointed by the inner_write_index_usb.
 136:lib/lib_pic33e/usb/app_device_cdc_basic.c **** 			 *
 137:lib/lib_pic33e/usb/app_device_cdc_basic.c **** 			 * Otherwise there is a full package to send. And the last position
 138:lib/lib_pic33e/usb/app_device_cdc_basic.c **** 			 * is pointed by the gpacket_last_pos_usb[read_index_usb}
 139:lib/lib_pic33e/usb/app_device_cdc_basic.c **** 			 * */
 140:lib/lib_pic33e/usb/app_device_cdc_basic.c **** 			if ((read_index_usb+1)%BUFFER_SIZE_USB == write_index_usb) {
 139              	.loc 1 140 0
 140 000078  00 00 80 	mov _read_index_usb,w0
 141 00007a  F1 03 20 	mov #63,w1
 142 00007c  00 01 E8 	inc w0,w2
 143 00007e  00 00 80 	mov _write_index_usb,w0
 144 000080  81 00 61 	and w2,w1,w1
 145 000082  80 8F 50 	sub w1,w0,[w15]
 146              	.set ___BP___,0
 147 000084  00 00 3A 	bra nz,.L10
 141:lib/lib_pic33e/usb/app_device_cdc_basic.c **** 				if (inner_write_index_usb == 0) {
 148              	.loc 1 141 0
 149 000086  00 00 80 	mov _inner_write_index_usb,w0
 150 000088  00 00 E0 	cp0 w0
 151              	.set ___BP___,0
 152 00008a  00 00 32 	bra z,.L15
 153              	.L11:
 142:lib/lib_pic33e/usb/app_device_cdc_basic.c **** 					return;
 143:lib/lib_pic33e/usb/app_device_cdc_basic.c **** 				}
 144:lib/lib_pic33e/usb/app_device_cdc_basic.c **** 				else {
 145:lib/lib_pic33e/usb/app_device_cdc_basic.c **** 					memcpy(outBufferUSB, gpacket_buffer_usb[(read_index_usb+1)%BUFFER_SIZE_USB], BULK_SIZE_USB);
 154              	.loc 1 145 0
 155 00008c  01 00 80 	mov _read_index_usb,w1
 156 00008e  F0 03 20 	mov #63,w0
 157 000090  01 01 E8 	inc w1,w2
 158 000092  01 00 20 	mov #_gpacket_buffer_usb,w1
 159 000094  00 00 61 	and w2,w0,w0
 160 000096  02 10 20 	mov #256,w2
 161 000098  C8 01 DD 	sl w0,#8,w3
 162 00009a  00 00 20 	mov #_outBufferUSB,w0
 163 00009c  81 80 41 	add w3,w1,w1
 164 00009e  00 00 07 	rcall _memcpy
 146:lib/lib_pic33e/usb/app_device_cdc_basic.c **** 					i = inner_write_index_usb;
 165              	.loc 1 146 0
 166 0000a0  01 00 80 	mov _inner_write_index_usb,w1
 167 0000a2  01 0F 78 	mov w1,[w14]
 147:lib/lib_pic33e/usb/app_device_cdc_basic.c **** 					inner_write_index_usb = 0;
 168              	.loc 1 147 0
 169 0000a4  00 20 EF 	clr _inner_write_index_usb
 170 0000a6  00 00 37 	bra .L12
 171              	.L10:
MPLAB XC16 ASSEMBLY Listing:   			page 7


 148:lib/lib_pic33e/usb/app_device_cdc_basic.c **** 				}
 149:lib/lib_pic33e/usb/app_device_cdc_basic.c **** 			}
 150:lib/lib_pic33e/usb/app_device_cdc_basic.c **** 			else {
 151:lib/lib_pic33e/usb/app_device_cdc_basic.c **** 				memcpy(outBufferUSB, gpacket_buffer_usb[read_index_usb], BULK_SIZE_USB);
 172              	.loc 1 151 0
 173 0000a8  01 00 80 	mov _read_index_usb,w1
 174 0000aa  00 00 20 	mov #_gpacket_buffer_usb,w0
 175 0000ac  C8 08 DD 	sl w1,#8,w1
 176 0000ae  02 10 20 	mov #256,w2
 177 0000b0  80 80 40 	add w1,w0,w1
 178 0000b2  00 00 20 	mov #_outBufferUSB,w0
 179 0000b4  00 00 07 	rcall _memcpy
 152:lib/lib_pic33e/usb/app_device_cdc_basic.c **** 				i = gpacket_last_pos_usb[read_index_usb];
 180              	.loc 1 152 0
 181 0000b6  00 00 80 	mov _read_index_usb,w0
 182 0000b8  01 00 20 	mov #_gpacket_last_pos_usb,w1
 183 0000ba  00 80 40 	add w1,w0,w0
 153:lib/lib_pic33e/usb/app_device_cdc_basic.c **** 				read_index_usb = (read_index_usb + 1) % BUFFER_SIZE_USB;
 184              	.loc 1 153 0
 185 0000bc  01 00 80 	mov _read_index_usb,w1
 186              	.loc 1 152 0
 187 0000be  10 40 78 	mov.b [w0],w0
 188              	.loc 1 153 0
 189 0000c0  81 00 E8 	inc w1,w1
 190              	.loc 1 152 0
 191 0000c2  00 81 FB 	ze w0,w2
 192              	.loc 1 153 0
 193 0000c4  F0 03 20 	mov #63,w0
 194              	.loc 1 152 0
 195 0000c6  02 0F 78 	mov w2,[w14]
 196              	.loc 1 153 0
 197 0000c8  00 80 60 	and w1,w0,w0
 198 0000ca  00 00 88 	mov w0,_read_index_usb
 199              	.L12:
 154:lib/lib_pic33e/usb/app_device_cdc_basic.c **** 			}
 155:lib/lib_pic33e/usb/app_device_cdc_basic.c **** 
 156:lib/lib_pic33e/usb/app_device_cdc_basic.c **** 			putUSBUSART(outBufferUSB, i);
 200              	.loc 1 156 0
 201 0000cc  1E 00 78 	mov [w14],w0
 202 0000ce  00 40 78 	mov.b w0,w0
 203 0000d0  80 40 78 	mov.b w0,w1
 204 0000d2  00 00 20 	mov #_outBufferUSB,w0
 205 0000d4  00 00 07 	rcall _putUSBUSART
 206              	.L6:
 207              	.LBE2:
 157:lib/lib_pic33e/usb/app_device_cdc_basic.c **** 		}
 158:lib/lib_pic33e/usb/app_device_cdc_basic.c **** 	}
 159:lib/lib_pic33e/usb/app_device_cdc_basic.c ****     CDCTxService();    
 208              	.loc 1 159 0
 209 0000d6  00 00 07 	rcall _CDCTxService
 210 0000d8  00 00 37 	bra .L2
 211              	.L13:
 212 0000da  00 00 37 	bra .L2
 213              	.L14:
 214 0000dc  00 00 37 	bra .L2
 215              	.L15:
 216              	.L2:
MPLAB XC16 ASSEMBLY Listing:   			page 8


 160:lib/lib_pic33e/usb/app_device_cdc_basic.c **** }
 217              	.loc 1 160 0
 218 0000de  8E 07 78 	mov w14,w15
 219 0000e0  4F 07 78 	mov [--w15],w14
 220 0000e2  00 40 A9 	bclr CORCON,#2
 221 0000e4  00 00 06 	return 
 222              	.set ___PA___,0
 223              	.LFE1:
 224              	.size _APP_DeviceCDCBasicDemoTasks,.-_APP_DeviceCDCBasicDemoTasks
 225              	.section .debug_frame,info
 226                 	.Lframe0:
 227 0000 10 00 00 00 	.4byte .LECIE0-.LSCIE0
 228                 	.LSCIE0:
 229 0004 FF FF FF FF 	.4byte 0xffffffff
 230 0008 01          	.byte 0x1
 231 0009 00          	.byte 0
 232 000a 01          	.uleb128 0x1
 233 000b 02          	.sleb128 2
 234 000c 25          	.byte 0x25
 235 000d 12          	.byte 0x12
 236 000e 0F          	.uleb128 0xf
 237 000f 7E          	.sleb128 -2
 238 0010 09          	.byte 0x9
 239 0011 25          	.uleb128 0x25
 240 0012 0F          	.uleb128 0xf
 241 0013 00          	.align 4
 242                 	.LECIE0:
 243                 	.LSFDE0:
 244 0014 18 00 00 00 	.4byte .LEFDE0-.LASFDE0
 245                 	.LASFDE0:
 246 0018 00 00 00 00 	.4byte .Lframe0
 247 001c 00 00 00 00 	.4byte .LFB0
 248 0020 24 00 00 00 	.4byte .LFE0-.LFB0
 249 0024 04          	.byte 0x4
 250 0025 02 00 00 00 	.4byte .LCFI0-.LFB0
 251 0029 13          	.byte 0x13
 252 002a 7D          	.sleb128 -3
 253 002b 0D          	.byte 0xd
 254 002c 0E          	.uleb128 0xe
 255 002d 8E          	.byte 0x8e
 256 002e 02          	.uleb128 0x2
 257 002f 00          	.align 4
 258                 	.LEFDE0:
 259                 	.LSFDE2:
 260 0030 18 00 00 00 	.4byte .LEFDE2-.LASFDE2
 261                 	.LASFDE2:
 262 0034 00 00 00 00 	.4byte .Lframe0
 263 0038 00 00 00 00 	.4byte .LFB1
 264 003c C2 00 00 00 	.4byte .LFE1-.LFB1
 265 0040 04          	.byte 0x4
 266 0041 02 00 00 00 	.4byte .LCFI1-.LFB1
 267 0045 13          	.byte 0x13
 268 0046 7D          	.sleb128 -3
 269 0047 0D          	.byte 0xd
 270 0048 0E          	.uleb128 0xe
 271 0049 8E          	.byte 0x8e
 272 004a 02          	.uleb128 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 9


 273 004b 00          	.align 4
 274                 	.LEFDE2:
 275                 	.section .text,code
 276              	.Letext0:
 277              	.file 2 "/opt/microchip/xc16/v1.36/bin/bin/../../include/lega-c/stdint.h"
 278              	.file 3 "/opt/microchip/xc16/v1.36/bin/bin/../../support/dsPIC33E/h/p33EP256MU806.h"
 279              	.file 4 "lib/lib_pic33e/usb/usb_common.h"
 280              	.file 5 "lib/lib_pic33e/usb/usb_device.h"
 281              	.file 6 "lib/lib_pic33e/usb/usb_device_cdc.h"
 282              	.file 7 "lib/lib_pic33e/usb/app_device_cdc_basic.h"
 283              	.file 8 "lib/lib_pic33e/usb/../usb_lib.h"
 284              	.section .debug_info,info
 285 0000 17 09 00 00 	.4byte 0x917
 286 0004 02 00       	.2byte 0x2
 287 0006 00 00 00 00 	.4byte .Ldebug_abbrev0
 288 000a 04          	.byte 0x4
 289 000b 01          	.uleb128 0x1
 290 000c 47 4E 55 20 	.asciz "GNU C 4.5.1 (XC16, Microchip v1.36) (A) Build date: Dec  5 2018"
 290      43 20 34 2E 
 290      35 2E 31 20 
 290      28 58 43 31 
 290      36 2C 20 4D 
 290      69 63 72 6F 
 290      63 68 69 70 
 290      20 76 31 2E 
 290      33 36 29 20 
 291 004c 01          	.byte 0x1
 292 004d 6C 69 62 2F 	.asciz "lib/lib_pic33e/usb/app_device_cdc_basic.c"
 292      6C 69 62 5F 
 292      70 69 63 33 
 292      33 65 2F 75 
 292      73 62 2F 61 
 292      70 70 5F 64 
 292      65 76 69 63 
 292      65 5F 63 64 
 292      63 5F 62 61 
 293 0077 2F 68 6F 6D 	.asciz "/home/user/Documents/FST/Programming/project_sub-zero"
 293      65 2F 75 73 
 293      65 72 2F 44 
 293      6F 63 75 6D 
 293      65 6E 74 73 
 293      2F 46 53 54 
 293      2F 50 72 6F 
 293      67 72 61 6D 
 293      6D 69 6E 67 
 294 00ad 00 00 00 00 	.4byte .Ltext0
 295 00b1 00 00 00 00 	.4byte .Letext0
 296 00b5 00 00 00 00 	.4byte .Ldebug_line0
 297 00b9 02          	.uleb128 0x2
 298 00ba 01          	.byte 0x1
 299 00bb 06          	.byte 0x6
 300 00bc 73 69 67 6E 	.asciz "signed char"
 300      65 64 20 63 
 300      68 61 72 00 
 301 00c8 02          	.uleb128 0x2
 302 00c9 02          	.byte 0x2
 303 00ca 05          	.byte 0x5
MPLAB XC16 ASSEMBLY Listing:   			page 10


 304 00cb 69 6E 74 00 	.asciz "int"
 305 00cf 02          	.uleb128 0x2
 306 00d0 04          	.byte 0x4
 307 00d1 05          	.byte 0x5
 308 00d2 6C 6F 6E 67 	.asciz "long int"
 308      20 69 6E 74 
 308      00 
 309 00db 02          	.uleb128 0x2
 310 00dc 08          	.byte 0x8
 311 00dd 05          	.byte 0x5
 312 00de 6C 6F 6E 67 	.asciz "long long int"
 312      20 6C 6F 6E 
 312      67 20 69 6E 
 312      74 00 
 313 00ec 03          	.uleb128 0x3
 314 00ed 75 69 6E 74 	.asciz "uint8_t"
 314      38 5F 74 00 
 315 00f5 02          	.byte 0x2
 316 00f6 2B          	.byte 0x2b
 317 00f7 FB 00 00 00 	.4byte 0xfb
 318 00fb 02          	.uleb128 0x2
 319 00fc 01          	.byte 0x1
 320 00fd 08          	.byte 0x8
 321 00fe 75 6E 73 69 	.asciz "unsigned char"
 321      67 6E 65 64 
 321      20 63 68 61 
 321      72 00 
 322 010c 03          	.uleb128 0x3
 323 010d 75 69 6E 74 	.asciz "uint16_t"
 323      31 36 5F 74 
 323      00 
 324 0116 02          	.byte 0x2
 325 0117 31          	.byte 0x31
 326 0118 1C 01 00 00 	.4byte 0x11c
 327 011c 02          	.uleb128 0x2
 328 011d 02          	.byte 0x2
 329 011e 07          	.byte 0x7
 330 011f 75 6E 73 69 	.asciz "unsigned int"
 330      67 6E 65 64 
 330      20 69 6E 74 
 330      00 
 331 012c 03          	.uleb128 0x3
 332 012d 75 69 6E 74 	.asciz "uint32_t"
 332      33 32 5F 74 
 332      00 
 333 0136 02          	.byte 0x2
 334 0137 37          	.byte 0x37
 335 0138 3C 01 00 00 	.4byte 0x13c
 336 013c 02          	.uleb128 0x2
 337 013d 04          	.byte 0x4
 338 013e 07          	.byte 0x7
 339 013f 6C 6F 6E 67 	.asciz "long unsigned int"
 339      20 75 6E 73 
 339      69 67 6E 65 
 339      64 20 69 6E 
 339      74 00 
 340 0151 02          	.uleb128 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 11


 341 0152 08          	.byte 0x8
 342 0153 07          	.byte 0x7
 343 0154 6C 6F 6E 67 	.asciz "long long unsigned int"
 343      20 6C 6F 6E 
 343      67 20 75 6E 
 343      73 69 67 6E 
 343      65 64 20 69 
 343      6E 74 00 
 344 016b 04          	.uleb128 0x4
 345 016c 02          	.byte 0x2
 346 016d 03          	.byte 0x3
 347 016e 8D 12       	.2byte 0x128d
 348 0170 CC 01 00 00 	.4byte 0x1cc
 349 0174 05          	.uleb128 0x5
 350 0175 55 53 42 50 	.asciz "USBPWR"
 350      57 52 00 
 351 017c 03          	.byte 0x3
 352 017d 8E 12       	.2byte 0x128e
 353 017f 0C 01 00 00 	.4byte 0x10c
 354 0183 02          	.byte 0x2
 355 0184 01          	.byte 0x1
 356 0185 0F          	.byte 0xf
 357 0186 02          	.byte 0x2
 358 0187 23          	.byte 0x23
 359 0188 00          	.uleb128 0x0
 360 0189 05          	.uleb128 0x5
 361 018a 55 53 55 53 	.asciz "USUSPND"
 361      50 4E 44 00 
 362 0192 03          	.byte 0x3
 363 0193 8F 12       	.2byte 0x128f
 364 0195 0C 01 00 00 	.4byte 0x10c
 365 0199 02          	.byte 0x2
 366 019a 01          	.byte 0x1
 367 019b 0E          	.byte 0xe
 368 019c 02          	.byte 0x2
 369 019d 23          	.byte 0x23
 370 019e 00          	.uleb128 0x0
 371 019f 05          	.uleb128 0x5
 372 01a0 55 53 4C 50 	.asciz "USLPGRD"
 372      47 52 44 00 
 373 01a8 03          	.byte 0x3
 374 01a9 91 12       	.2byte 0x1291
 375 01ab 0C 01 00 00 	.4byte 0x10c
 376 01af 02          	.byte 0x2
 377 01b0 01          	.byte 0x1
 378 01b1 0B          	.byte 0xb
 379 01b2 02          	.byte 0x2
 380 01b3 23          	.byte 0x23
 381 01b4 00          	.uleb128 0x0
 382 01b5 05          	.uleb128 0x5
 383 01b6 55 41 43 54 	.asciz "UACTPND"
 383      50 4E 44 00 
 384 01be 03          	.byte 0x3
 385 01bf 93 12       	.2byte 0x1293
 386 01c1 0C 01 00 00 	.4byte 0x10c
 387 01c5 02          	.byte 0x2
 388 01c6 01          	.byte 0x1
MPLAB XC16 ASSEMBLY Listing:   			page 12


 389 01c7 08          	.byte 0x8
 390 01c8 02          	.byte 0x2
 391 01c9 23          	.byte 0x23
 392 01ca 00          	.uleb128 0x0
 393 01cb 00          	.byte 0x0
 394 01cc 04          	.uleb128 0x4
 395 01cd 02          	.byte 0x2
 396 01ce 03          	.byte 0x3
 397 01cf 95 12       	.2byte 0x1295
 398 01d1 ED 01 00 00 	.4byte 0x1ed
 399 01d5 05          	.uleb128 0x5
 400 01d6 55 53 55 53 	.asciz "USUSPEND"
 400      50 45 4E 44 
 400      00 
 401 01df 03          	.byte 0x3
 402 01e0 97 12       	.2byte 0x1297
 403 01e2 0C 01 00 00 	.4byte 0x10c
 404 01e6 02          	.byte 0x2
 405 01e7 01          	.byte 0x1
 406 01e8 0E          	.byte 0xe
 407 01e9 02          	.byte 0x2
 408 01ea 23          	.byte 0x23
 409 01eb 00          	.uleb128 0x0
 410 01ec 00          	.byte 0x0
 411 01ed 06          	.uleb128 0x6
 412 01ee 02          	.byte 0x2
 413 01ef 03          	.byte 0x3
 414 01f0 8C 12       	.2byte 0x128c
 415 01f2 01 02 00 00 	.4byte 0x201
 416 01f6 07          	.uleb128 0x7
 417 01f7 6B 01 00 00 	.4byte 0x16b
 418 01fb 07          	.uleb128 0x7
 419 01fc CC 01 00 00 	.4byte 0x1cc
 420 0200 00          	.byte 0x0
 421 0201 08          	.uleb128 0x8
 422 0202 74 61 67 55 	.asciz "tagU1PWRCBITS"
 422      31 50 57 52 
 422      43 42 49 54 
 422      53 00 
 423 0210 02          	.byte 0x2
 424 0211 03          	.byte 0x3
 425 0212 8B 12       	.2byte 0x128b
 426 0214 21 02 00 00 	.4byte 0x221
 427 0218 09          	.uleb128 0x9
 428 0219 ED 01 00 00 	.4byte 0x1ed
 429 021d 02          	.byte 0x2
 430 021e 23          	.byte 0x23
 431 021f 00          	.uleb128 0x0
 432 0220 00          	.byte 0x0
 433 0221 0A          	.uleb128 0xa
 434 0222 55 31 50 57 	.asciz "U1PWRCBITS"
 434      52 43 42 49 
 434      54 53 00 
 435 022d 03          	.byte 0x3
 436 022e 9A 12       	.2byte 0x129a
 437 0230 01 02 00 00 	.4byte 0x201
 438 0234 02          	.uleb128 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 13


 439 0235 02          	.byte 0x2
 440 0236 07          	.byte 0x7
 441 0237 73 68 6F 72 	.asciz "short unsigned int"
 441      74 20 75 6E 
 441      73 69 67 6E 
 441      65 64 20 69 
 441      6E 74 00 
 442 024a 0B          	.uleb128 0xb
 443 024b 02          	.byte 0x2
 444 024c 04          	.byte 0x4
 445 024d CF          	.byte 0xcf
 446 024e 78 05 00 00 	.4byte 0x578
 447 0252 0C          	.uleb128 0xc
 448 0253 45 56 45 4E 	.asciz "EVENT_NONE"
 448      54 5F 4E 4F 
 448      4E 45 00 
 449 025e 00          	.sleb128 0
 450 025f 0C          	.uleb128 0xc
 451 0260 45 56 45 4E 	.asciz "EVENT_DEVICE_STACK_BASE"
 451      54 5F 44 45 
 451      56 49 43 45 
 451      5F 53 54 41 
 451      43 4B 5F 42 
 451      41 53 45 00 
 452 0278 01          	.sleb128 1
 453 0279 0C          	.uleb128 0xc
 454 027a 45 56 45 4E 	.asciz "EVENT_HOST_STACK_BASE"
 454      54 5F 48 4F 
 454      53 54 5F 53 
 454      54 41 43 4B 
 454      5F 42 41 53 
 454      45 00 
 455 0290 E4 00       	.sleb128 100
 456 0292 0C          	.uleb128 0xc
 457 0293 45 56 45 4E 	.asciz "EVENT_HUB_ATTACH"
 457      54 5F 48 55 
 457      42 5F 41 54 
 457      54 41 43 48 
 457      00 
 458 02a4 E5 00       	.sleb128 101
 459 02a6 0C          	.uleb128 0xc
 460 02a7 45 56 45 4E 	.asciz "EVENT_STALL"
 460      54 5F 53 54 
 460      41 4C 4C 00 
 461 02b3 E6 00       	.sleb128 102
 462 02b5 0C          	.uleb128 0xc
 463 02b6 45 56 45 4E 	.asciz "EVENT_VBUS_SES_REQUEST"
 463      54 5F 56 42 
 463      55 53 5F 53 
 463      45 53 5F 52 
 463      45 51 55 45 
 463      53 54 00 
 464 02cd E7 00       	.sleb128 103
 465 02cf 0C          	.uleb128 0xc
 466 02d0 45 56 45 4E 	.asciz "EVENT_VBUS_OVERCURRENT"
 466      54 5F 56 42 
 466      55 53 5F 4F 
MPLAB XC16 ASSEMBLY Listing:   			page 14


 466      56 45 52 43 
 466      55 52 52 45 
 466      4E 54 00 
 467 02e7 E8 00       	.sleb128 104
 468 02e9 0C          	.uleb128 0xc
 469 02ea 45 56 45 4E 	.asciz "EVENT_VBUS_REQUEST_POWER"
 469      54 5F 56 42 
 469      55 53 5F 52 
 469      45 51 55 45 
 469      53 54 5F 50 
 469      4F 57 45 52 
 469      00 
 470 0303 E9 00       	.sleb128 105
 471 0305 0C          	.uleb128 0xc
 472 0306 45 56 45 4E 	.asciz "EVENT_VBUS_RELEASE_POWER"
 472      54 5F 56 42 
 472      55 53 5F 52 
 472      45 4C 45 41 
 472      53 45 5F 50 
 472      4F 57 45 52 
 472      00 
 473 031f EA 00       	.sleb128 106
 474 0321 0C          	.uleb128 0xc
 475 0322 45 56 45 4E 	.asciz "EVENT_VBUS_POWER_AVAILABLE"
 475      54 5F 56 42 
 475      55 53 5F 50 
 475      4F 57 45 52 
 475      5F 41 56 41 
 475      49 4C 41 42 
 475      4C 45 00 
 476 033d EB 00       	.sleb128 107
 477 033f 0C          	.uleb128 0xc
 478 0340 45 56 45 4E 	.asciz "EVENT_UNSUPPORTED_DEVICE"
 478      54 5F 55 4E 
 478      53 55 50 50 
 478      4F 52 54 45 
 478      44 5F 44 45 
 478      56 49 43 45 
 478      00 
 479 0359 EC 00       	.sleb128 108
 480 035b 0C          	.uleb128 0xc
 481 035c 45 56 45 4E 	.asciz "EVENT_CANNOT_ENUMERATE"
 481      54 5F 43 41 
 481      4E 4E 4F 54 
 481      5F 45 4E 55 
 481      4D 45 52 41 
 481      54 45 00 
 482 0373 ED 00       	.sleb128 109
 483 0375 0C          	.uleb128 0xc
 484 0376 45 56 45 4E 	.asciz "EVENT_CLIENT_INIT_ERROR"
 484      54 5F 43 4C 
 484      49 45 4E 54 
 484      5F 49 4E 49 
 484      54 5F 45 52 
 484      52 4F 52 00 
 485 038e EE 00       	.sleb128 110
 486 0390 0C          	.uleb128 0xc
MPLAB XC16 ASSEMBLY Listing:   			page 15


 487 0391 45 56 45 4E 	.asciz "EVENT_OUT_OF_MEMORY"
 487      54 5F 4F 55 
 487      54 5F 4F 46 
 487      5F 4D 45 4D 
 487      4F 52 59 00 
 488 03a5 EF 00       	.sleb128 111
 489 03a7 0C          	.uleb128 0xc
 490 03a8 45 56 45 4E 	.asciz "EVENT_UNSPECIFIED_ERROR"
 490      54 5F 55 4E 
 490      53 50 45 43 
 490      49 46 49 45 
 490      44 5F 45 52 
 490      52 4F 52 00 
 491 03c0 F0 00       	.sleb128 112
 492 03c2 0C          	.uleb128 0xc
 493 03c3 45 56 45 4E 	.asciz "EVENT_DETACH"
 493      54 5F 44 45 
 493      54 41 43 48 
 493      00 
 494 03d0 F1 00       	.sleb128 113
 495 03d2 0C          	.uleb128 0xc
 496 03d3 45 56 45 4E 	.asciz "EVENT_TRANSFER"
 496      54 5F 54 52 
 496      41 4E 53 46 
 496      45 52 00 
 497 03e2 F2 00       	.sleb128 114
 498 03e4 0C          	.uleb128 0xc
 499 03e5 45 56 45 4E 	.asciz "EVENT_SOF"
 499      54 5F 53 4F 
 499      46 00 
 500 03ef F3 00       	.sleb128 115
 501 03f1 0C          	.uleb128 0xc
 502 03f2 45 56 45 4E 	.asciz "EVENT_RESUME"
 502      54 5F 52 45 
 502      53 55 4D 45 
 502      00 
 503 03ff F4 00       	.sleb128 116
 504 0401 0C          	.uleb128 0xc
 505 0402 45 56 45 4E 	.asciz "EVENT_SUSPEND"
 505      54 5F 53 55 
 505      53 50 45 4E 
 505      44 00 
 506 0410 F5 00       	.sleb128 117
 507 0412 0C          	.uleb128 0xc
 508 0413 45 56 45 4E 	.asciz "EVENT_RESET"
 508      54 5F 52 45 
 508      53 45 54 00 
 509 041f F6 00       	.sleb128 118
 510 0421 0C          	.uleb128 0xc
 511 0422 45 56 45 4E 	.asciz "EVENT_DATA_ISOC_READ"
 511      54 5F 44 41 
 511      54 41 5F 49 
 511      53 4F 43 5F 
 511      52 45 41 44 
 511      00 
 512 0437 F7 00       	.sleb128 119
 513 0439 0C          	.uleb128 0xc
MPLAB XC16 ASSEMBLY Listing:   			page 16


 514 043a 45 56 45 4E 	.asciz "EVENT_DATA_ISOC_WRITE"
 514      54 5F 44 41 
 514      54 41 5F 49 
 514      53 4F 43 5F 
 514      57 52 49 54 
 514      45 00 
 515 0450 F8 00       	.sleb128 120
 516 0452 0C          	.uleb128 0xc
 517 0453 45 56 45 4E 	.asciz "EVENT_OVERRIDE_CLIENT_DRIVER_SELECTION"
 517      54 5F 4F 56 
 517      45 52 52 49 
 517      44 45 5F 43 
 517      4C 49 45 4E 
 517      54 5F 44 52 
 517      49 56 45 52 
 517      5F 53 45 4C 
 517      45 43 54 49 
 518 047a F9 00       	.sleb128 121
 519 047c 0C          	.uleb128 0xc
 520 047d 45 56 45 4E 	.asciz "EVENT_1MS"
 520      54 5F 31 4D 
 520      53 00 
 521 0487 FA 00       	.sleb128 122
 522 0489 0C          	.uleb128 0xc
 523 048a 45 56 45 4E 	.asciz "EVENT_ALT_INTERFACE"
 523      54 5F 41 4C 
 523      54 5F 49 4E 
 523      54 45 52 46 
 523      41 43 45 00 
 524 049e FB 00       	.sleb128 123
 525 04a0 0C          	.uleb128 0xc
 526 04a1 45 56 45 4E 	.asciz "EVENT_HOLD_BEFORE_CONFIGURATION"
 526      54 5F 48 4F 
 526      4C 44 5F 42 
 526      45 46 4F 52 
 526      45 5F 43 4F 
 526      4E 46 49 47 
 526      55 52 41 54 
 526      49 4F 4E 00 
 527 04c1 FC 00       	.sleb128 124
 528 04c3 0C          	.uleb128 0xc
 529 04c4 45 56 45 4E 	.asciz "EVENT_GENERIC_BASE"
 529      54 5F 47 45 
 529      4E 45 52 49 
 529      43 5F 42 41 
 529      53 45 00 
 530 04d7 90 03       	.sleb128 400
 531 04d9 0C          	.uleb128 0xc
 532 04da 45 56 45 4E 	.asciz "EVENT_MSD_BASE"
 532      54 5F 4D 53 
 532      44 5F 42 41 
 532      53 45 00 
 533 04e9 F4 03       	.sleb128 500
 534 04eb 0C          	.uleb128 0xc
 535 04ec 45 56 45 4E 	.asciz "EVENT_HID_BASE"
 535      54 5F 48 49 
 535      44 5F 42 41 
MPLAB XC16 ASSEMBLY Listing:   			page 17


 535      53 45 00 
 536 04fb D8 04       	.sleb128 600
 537 04fd 0C          	.uleb128 0xc
 538 04fe 45 56 45 4E 	.asciz "EVENT_PRINTER_BASE"
 538      54 5F 50 52 
 538      49 4E 54 45 
 538      52 5F 42 41 
 538      53 45 00 
 539 0511 BC 05       	.sleb128 700
 540 0513 0C          	.uleb128 0xc
 541 0514 45 56 45 4E 	.asciz "EVENT_CDC_BASE"
 541      54 5F 43 44 
 541      43 5F 42 41 
 541      53 45 00 
 542 0523 A0 06       	.sleb128 800
 543 0525 0C          	.uleb128 0xc
 544 0526 45 56 45 4E 	.asciz "EVENT_CHARGER_BASE"
 544      54 5F 43 48 
 544      41 52 47 45 
 544      52 5F 42 41 
 544      53 45 00 
 545 0539 84 07       	.sleb128 900
 546 053b 0C          	.uleb128 0xc
 547 053c 45 56 45 4E 	.asciz "EVENT_AUDIO_BASE"
 547      54 5F 41 55 
 547      44 49 4F 5F 
 547      42 41 53 45 
 547      00 
 548 054d E8 07       	.sleb128 1000
 549 054f 0C          	.uleb128 0xc
 550 0550 45 56 45 4E 	.asciz "EVENT_USER_BASE"
 550      54 5F 55 53 
 550      45 52 5F 42 
 550      41 53 45 00 
 551 0560 90 CE 00    	.sleb128 10000
 552 0563 0C          	.uleb128 0xc
 553 0564 45 56 45 4E 	.asciz "EVENT_BUS_ERROR"
 553      54 5F 42 55 
 553      53 5F 45 52 
 553      52 4F 52 00 
 554 0574 FF FF 01    	.sleb128 32767
 555 0577 00          	.byte 0x0
 556 0578 02          	.uleb128 0x2
 557 0579 01          	.byte 0x1
 558 057a 02          	.byte 0x2
 559 057b 5F 42 6F 6F 	.asciz "_Bool"
 559      6C 00 
 560 0581 0B          	.uleb128 0xb
 561 0582 02          	.byte 0x2
 562 0583 05          	.byte 0x5
 563 0584 4A          	.byte 0x4a
 564 0585 03 06 00 00 	.4byte 0x603
 565 0589 0C          	.uleb128 0xc
 566 058a 44 45 54 41 	.asciz "DETACHED_STATE"
 566      43 48 45 44 
 566      5F 53 54 41 
 566      54 45 00 
MPLAB XC16 ASSEMBLY Listing:   			page 18


 567 0599 00          	.sleb128 0
 568 059a 0C          	.uleb128 0xc
 569 059b 41 54 54 41 	.asciz "ATTACHED_STATE"
 569      43 48 45 44 
 569      5F 53 54 41 
 569      54 45 00 
 570 05aa 01          	.sleb128 1
 571 05ab 0C          	.uleb128 0xc
 572 05ac 50 4F 57 45 	.asciz "POWERED_STATE"
 572      52 45 44 5F 
 572      53 54 41 54 
 572      45 00 
 573 05ba 02          	.sleb128 2
 574 05bb 0C          	.uleb128 0xc
 575 05bc 44 45 46 41 	.asciz "DEFAULT_STATE"
 575      55 4C 54 5F 
 575      53 54 41 54 
 575      45 00 
 576 05ca 04          	.sleb128 4
 577 05cb 0C          	.uleb128 0xc
 578 05cc 41 44 52 5F 	.asciz "ADR_PENDING_STATE"
 578      50 45 4E 44 
 578      49 4E 47 5F 
 578      53 54 41 54 
 578      45 00 
 579 05de 08          	.sleb128 8
 580 05df 0C          	.uleb128 0xc
 581 05e0 41 44 44 52 	.asciz "ADDRESS_STATE"
 581      45 53 53 5F 
 581      53 54 41 54 
 581      45 00 
 582 05ee 10          	.sleb128 16
 583 05ef 0C          	.uleb128 0xc
 584 05f0 43 4F 4E 46 	.asciz "CONFIGURED_STATE"
 584      49 47 55 52 
 584      45 44 5F 53 
 584      54 41 54 45 
 584      00 
 585 0601 20          	.sleb128 32
 586 0602 00          	.byte 0x0
 587 0603 03          	.uleb128 0x3
 588 0604 55 53 42 5F 	.asciz "USB_DEVICE_STATE"
 588      44 45 56 49 
 588      43 45 5F 53 
 588      54 41 54 45 
 588      00 
 589 0615 05          	.byte 0x5
 590 0616 6E          	.byte 0x6e
 591 0617 81 05 00 00 	.4byte 0x581
 592 061b 04          	.uleb128 0x4
 593 061c 07          	.byte 0x7
 594 061d 06          	.byte 0x6
 595 061e 1E 03       	.2byte 0x31e
 596 0620 36 06 00 00 	.4byte 0x636
 597 0624 0D          	.uleb128 0xd
 598 0625 5F 62 79 74 	.asciz "_byte"
 598      65 00 
MPLAB XC16 ASSEMBLY Listing:   			page 19


 599 062b 06          	.byte 0x6
 600 062c 20 03       	.2byte 0x320
 601 062e 36 06 00 00 	.4byte 0x636
 602 0632 02          	.byte 0x2
 603 0633 23          	.byte 0x23
 604 0634 00          	.uleb128 0x0
 605 0635 00          	.byte 0x0
 606 0636 0E          	.uleb128 0xe
 607 0637 EC 00 00 00 	.4byte 0xec
 608 063b 46 06 00 00 	.4byte 0x646
 609 063f 0F          	.uleb128 0xf
 610 0640 1C 01 00 00 	.4byte 0x11c
 611 0644 06          	.byte 0x6
 612 0645 00          	.byte 0x0
 613 0646 04          	.uleb128 0x4
 614 0647 08          	.byte 0x8
 615 0648 06          	.byte 0x6
 616 0649 22 03       	.2byte 0x322
 617 064b A8 06 00 00 	.4byte 0x6a8
 618 064f 0D          	.uleb128 0xd
 619 0650 64 77 44 54 	.asciz "dwDTERate"
 619      45 52 61 74 
 619      65 00 
 620 065a 06          	.byte 0x6
 621 065b 24 03       	.2byte 0x324
 622 065d 2C 01 00 00 	.4byte 0x12c
 623 0661 02          	.byte 0x2
 624 0662 23          	.byte 0x23
 625 0663 00          	.uleb128 0x0
 626 0664 0D          	.uleb128 0xd
 627 0665 62 43 68 61 	.asciz "bCharFormat"
 627      72 46 6F 72 
 627      6D 61 74 00 
 628 0671 06          	.byte 0x6
 629 0672 25 03       	.2byte 0x325
 630 0674 EC 00 00 00 	.4byte 0xec
 631 0678 02          	.byte 0x2
 632 0679 23          	.byte 0x23
 633 067a 04          	.uleb128 0x4
 634 067b 0D          	.uleb128 0xd
 635 067c 62 50 61 72 	.asciz "bParityType"
 635      69 74 79 54 
 635      79 70 65 00 
 636 0688 06          	.byte 0x6
 637 0689 26 03       	.2byte 0x326
 638 068b EC 00 00 00 	.4byte 0xec
 639 068f 02          	.byte 0x2
 640 0690 23          	.byte 0x23
 641 0691 05          	.uleb128 0x5
 642 0692 0D          	.uleb128 0xd
 643 0693 62 44 61 74 	.asciz "bDataBits"
 643      61 42 69 74 
 643      73 00 
 644 069d 06          	.byte 0x6
 645 069e 27 03       	.2byte 0x327
 646 06a0 EC 00 00 00 	.4byte 0xec
 647 06a4 02          	.byte 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 20


 648 06a5 23          	.byte 0x23
 649 06a6 06          	.uleb128 0x6
 650 06a7 00          	.byte 0x0
 651 06a8 10          	.uleb128 0x10
 652 06a9 5F 4C 49 4E 	.asciz "_LINE_CODING"
 652      45 5F 43 4F 
 652      44 49 4E 47 
 652      00 
 653 06b6 08          	.byte 0x8
 654 06b7 06          	.byte 0x6
 655 06b8 1C 03       	.2byte 0x31c
 656 06ba C9 06 00 00 	.4byte 0x6c9
 657 06be 07          	.uleb128 0x7
 658 06bf 1B 06 00 00 	.4byte 0x61b
 659 06c3 07          	.uleb128 0x7
 660 06c4 46 06 00 00 	.4byte 0x646
 661 06c8 00          	.byte 0x0
 662 06c9 0A          	.uleb128 0xa
 663 06ca 4C 49 4E 45 	.asciz "LINE_CODING"
 663      5F 43 4F 44 
 663      49 4E 47 00 
 664 06d6 06          	.byte 0x6
 665 06d7 29 03       	.2byte 0x329
 666 06d9 A8 06 00 00 	.4byte 0x6a8
 667 06dd 11          	.uleb128 0x11
 668 06de 01          	.byte 0x1
 669 06df 41 50 50 5F 	.asciz "APP_DeviceCDCBasicDemoInitialize"
 669      44 65 76 69 
 669      63 65 43 44 
 669      43 42 61 73 
 669      69 63 44 65 
 669      6D 6F 49 6E 
 669      69 74 69 61 
 669      6C 69 7A 65 
 669      00 
 670 0700 01          	.byte 0x1
 671 0701 37          	.byte 0x37
 672 0702 00 00 00 00 	.4byte .LFB0
 673 0706 00 00 00 00 	.4byte .LFE0
 674 070a 01          	.byte 0x1
 675 070b 5E          	.byte 0x5e
 676 070c 12          	.uleb128 0x12
 677 070d 01          	.byte 0x1
 678 070e 41 50 50 5F 	.asciz "APP_DeviceCDCBasicDemoTasks"
 678      44 65 76 69 
 678      63 65 43 44 
 678      43 42 61 73 
 678      69 63 44 65 
 678      6D 6F 54 61 
 678      73 6B 73 00 
 679 072a 01          	.byte 0x1
 680 072b 4D          	.byte 0x4d
 681 072c 00 00 00 00 	.4byte .LFB1
 682 0730 00 00 00 00 	.4byte .LFE1
 683 0734 01          	.byte 0x1
 684 0735 5E          	.byte 0x5e
 685 0736 91 07 00 00 	.4byte 0x791
MPLAB XC16 ASSEMBLY Listing:   			page 21


 686 073a 13          	.uleb128 0x13
 687 073b 61 75 78 00 	.asciz "aux"
 688 073f 01          	.byte 0x1
 689 0740 4F          	.byte 0x4f
 690 0741 1C 01 00 00 	.4byte 0x11c
 691 0745 14          	.uleb128 0x14
 692 0746 69 00       	.asciz "i"
 693 0748 01          	.byte 0x1
 694 0749 4F          	.byte 0x4f
 695 074a 1C 01 00 00 	.4byte 0x11c
 696 074e 02          	.byte 0x2
 697 074f 7E          	.byte 0x7e
 698 0750 00          	.sleb128 0
 699 0751 13          	.uleb128 0x13
 700 0752 6A 00       	.asciz "j"
 701 0754 01          	.byte 0x1
 702 0755 4F          	.byte 0x4f
 703 0756 1C 01 00 00 	.4byte 0x11c
 704 075a 15          	.uleb128 0x15
 705 075b 00 00 00 00 	.4byte .LBB2
 706 075f 00 00 00 00 	.4byte .LBE2
 707 0763 14          	.uleb128 0x14
 708 0764 6E 75 6D 42 	.asciz "numBytesRead"
 708      79 74 65 73 
 708      52 65 61 64 
 708      00 
 709 0771 01          	.byte 0x1
 710 0772 6A          	.byte 0x6a
 711 0773 EC 00 00 00 	.4byte 0xec
 712 0777 02          	.byte 0x2
 713 0778 7E          	.byte 0x7e
 714 0779 02          	.sleb128 2
 715 077a 14          	.uleb128 0x14
 716 077b 72 65 61 64 	.asciz "readString"
 716      53 74 72 69 
 716      6E 67 00 
 717 0786 01          	.byte 0x1
 718 0787 6C          	.byte 0x6c
 719 0788 91 07 00 00 	.4byte 0x791
 720 078c 02          	.byte 0x2
 721 078d 7E          	.byte 0x7e
 722 078e 03          	.sleb128 3
 723 078f 00          	.byte 0x0
 724 0790 00          	.byte 0x0
 725 0791 0E          	.uleb128 0xe
 726 0792 A1 07 00 00 	.4byte 0x7a1
 727 0796 A1 07 00 00 	.4byte 0x7a1
 728 079a 0F          	.uleb128 0xf
 729 079b 1C 01 00 00 	.4byte 0x11c
 730 079f 3F          	.byte 0x3f
 731 07a0 00          	.byte 0x0
 732 07a1 02          	.uleb128 0x2
 733 07a2 01          	.byte 0x1
 734 07a3 06          	.byte 0x6
 735 07a4 63 68 61 72 	.asciz "char"
 735      00 
 736 07a9 16          	.uleb128 0x16
MPLAB XC16 ASSEMBLY Listing:   			page 22


 737 07aa 00 00 00 00 	.4byte .LASF0
 738 07ae 03          	.byte 0x3
 739 07af 9B 12       	.2byte 0x129b
 740 07b1 B7 07 00 00 	.4byte 0x7b7
 741 07b5 01          	.byte 0x1
 742 07b6 01          	.byte 0x1
 743 07b7 17          	.uleb128 0x17
 744 07b8 21 02 00 00 	.4byte 0x221
 745 07bc 16          	.uleb128 0x16
 746 07bd 00 00 00 00 	.4byte .LASF1
 747 07c1 05          	.byte 0x5
 748 07c2 FE 07       	.2byte 0x7fe
 749 07c4 CA 07 00 00 	.4byte 0x7ca
 750 07c8 01          	.byte 0x1
 751 07c9 01          	.byte 0x1
 752 07ca 17          	.uleb128 0x17
 753 07cb 03 06 00 00 	.4byte 0x603
 754 07cf 16          	.uleb128 0x16
 755 07d0 00 00 00 00 	.4byte .LASF2
 756 07d4 06          	.byte 0x6
 757 07d5 87 03       	.2byte 0x387
 758 07d7 EC 00 00 00 	.4byte 0xec
 759 07db 01          	.byte 0x1
 760 07dc 01          	.byte 0x1
 761 07dd 16          	.uleb128 0x16
 762 07de 00 00 00 00 	.4byte .LASF3
 763 07e2 06          	.byte 0x6
 764 07e3 8D 03       	.2byte 0x38d
 765 07e5 C9 06 00 00 	.4byte 0x6c9
 766 07e9 01          	.byte 0x1
 767 07ea 01          	.byte 0x1
 768 07eb 0E          	.uleb128 0xe
 769 07ec EC 00 00 00 	.4byte 0xec
 770 07f0 FB 07 00 00 	.4byte 0x7fb
 771 07f4 0F          	.uleb128 0xf
 772 07f5 1C 01 00 00 	.4byte 0x11c
 773 07f9 FF          	.byte 0xff
 774 07fa 00          	.byte 0x0
 775 07fb 18          	.uleb128 0x18
 776 07fc 00 00 00 00 	.4byte .LASF4
 777 0800 07          	.byte 0x7
 778 0801 20          	.byte 0x20
 779 0802 EB 07 00 00 	.4byte 0x7eb
 780 0806 01          	.byte 0x1
 781 0807 01          	.byte 0x1
 782 0808 0E          	.uleb128 0xe
 783 0809 EC 00 00 00 	.4byte 0xec
 784 080d 1E 08 00 00 	.4byte 0x81e
 785 0811 0F          	.uleb128 0xf
 786 0812 1C 01 00 00 	.4byte 0x11c
 787 0816 3F          	.byte 0x3f
 788 0817 0F          	.uleb128 0xf
 789 0818 1C 01 00 00 	.4byte 0x11c
 790 081c FF          	.byte 0xff
 791 081d 00          	.byte 0x0
 792 081e 18          	.uleb128 0x18
 793 081f 00 00 00 00 	.4byte .LASF5
MPLAB XC16 ASSEMBLY Listing:   			page 23


 794 0823 08          	.byte 0x8
 795 0824 12          	.byte 0x12
 796 0825 08 08 00 00 	.4byte 0x808
 797 0829 01          	.byte 0x1
 798 082a 01          	.byte 0x1
 799 082b 0E          	.uleb128 0xe
 800 082c EC 00 00 00 	.4byte 0xec
 801 0830 3B 08 00 00 	.4byte 0x83b
 802 0834 0F          	.uleb128 0xf
 803 0835 1C 01 00 00 	.4byte 0x11c
 804 0839 3F          	.byte 0x3f
 805 083a 00          	.byte 0x0
 806 083b 18          	.uleb128 0x18
 807 083c 00 00 00 00 	.4byte .LASF6
 808 0840 08          	.byte 0x8
 809 0841 13          	.byte 0x13
 810 0842 2B 08 00 00 	.4byte 0x82b
 811 0846 01          	.byte 0x1
 812 0847 01          	.byte 0x1
 813 0848 18          	.uleb128 0x18
 814 0849 00 00 00 00 	.4byte .LASF7
 815 084d 08          	.byte 0x8
 816 084e 14          	.byte 0x14
 817 084f 55 08 00 00 	.4byte 0x855
 818 0853 01          	.byte 0x1
 819 0854 01          	.byte 0x1
 820 0855 17          	.uleb128 0x17
 821 0856 1C 01 00 00 	.4byte 0x11c
 822 085a 18          	.uleb128 0x18
 823 085b 00 00 00 00 	.4byte .LASF8
 824 085f 08          	.byte 0x8
 825 0860 15          	.byte 0x15
 826 0861 55 08 00 00 	.4byte 0x855
 827 0865 01          	.byte 0x1
 828 0866 01          	.byte 0x1
 829 0867 18          	.uleb128 0x18
 830 0868 00 00 00 00 	.4byte .LASF9
 831 086c 08          	.byte 0x8
 832 086d 16          	.byte 0x16
 833 086e 55 08 00 00 	.4byte 0x855
 834 0872 01          	.byte 0x1
 835 0873 01          	.byte 0x1
 836 0874 14          	.uleb128 0x14
 837 0875 72 65 61 64 	.asciz "readBufferUSB"
 837      42 75 66 66 
 837      65 72 55 53 
 837      42 00 
 838 0883 01          	.byte 0x1
 839 0884 27          	.byte 0x27
 840 0885 2B 08 00 00 	.4byte 0x82b
 841 0889 05          	.byte 0x5
 842 088a 03          	.byte 0x3
 843 088b 00 00 00 00 	.4byte _readBufferUSB
 844 088f 16          	.uleb128 0x16
 845 0890 00 00 00 00 	.4byte .LASF0
 846 0894 03          	.byte 0x3
 847 0895 9B 12       	.2byte 0x129b
MPLAB XC16 ASSEMBLY Listing:   			page 24


 848 0897 B7 07 00 00 	.4byte 0x7b7
 849 089b 01          	.byte 0x1
 850 089c 01          	.byte 0x1
 851 089d 16          	.uleb128 0x16
 852 089e 00 00 00 00 	.4byte .LASF1
 853 08a2 05          	.byte 0x5
 854 08a3 FE 07       	.2byte 0x7fe
 855 08a5 CA 07 00 00 	.4byte 0x7ca
 856 08a9 01          	.byte 0x1
 857 08aa 01          	.byte 0x1
 858 08ab 16          	.uleb128 0x16
 859 08ac 00 00 00 00 	.4byte .LASF2
 860 08b0 06          	.byte 0x6
 861 08b1 87 03       	.2byte 0x387
 862 08b3 EC 00 00 00 	.4byte 0xec
 863 08b7 01          	.byte 0x1
 864 08b8 01          	.byte 0x1
 865 08b9 16          	.uleb128 0x16
 866 08ba 00 00 00 00 	.4byte .LASF3
 867 08be 06          	.byte 0x6
 868 08bf 8D 03       	.2byte 0x38d
 869 08c1 C9 06 00 00 	.4byte 0x6c9
 870 08c5 01          	.byte 0x1
 871 08c6 01          	.byte 0x1
 872 08c7 19          	.uleb128 0x19
 873 08c8 00 00 00 00 	.4byte .LASF4
 874 08cc 01          	.byte 0x1
 875 08cd 28          	.byte 0x28
 876 08ce EB 07 00 00 	.4byte 0x7eb
 877 08d2 01          	.byte 0x1
 878 08d3 05          	.byte 0x5
 879 08d4 03          	.byte 0x3
 880 08d5 00 00 00 00 	.4byte _outBufferUSB
 881 08d9 18          	.uleb128 0x18
 882 08da 00 00 00 00 	.4byte .LASF5
 883 08de 08          	.byte 0x8
 884 08df 12          	.byte 0x12
 885 08e0 08 08 00 00 	.4byte 0x808
 886 08e4 01          	.byte 0x1
 887 08e5 01          	.byte 0x1
 888 08e6 18          	.uleb128 0x18
 889 08e7 00 00 00 00 	.4byte .LASF6
 890 08eb 08          	.byte 0x8
 891 08ec 13          	.byte 0x13
 892 08ed 2B 08 00 00 	.4byte 0x82b
 893 08f1 01          	.byte 0x1
 894 08f2 01          	.byte 0x1
 895 08f3 18          	.uleb128 0x18
 896 08f4 00 00 00 00 	.4byte .LASF7
 897 08f8 08          	.byte 0x8
 898 08f9 14          	.byte 0x14
 899 08fa 55 08 00 00 	.4byte 0x855
 900 08fe 01          	.byte 0x1
 901 08ff 01          	.byte 0x1
 902 0900 18          	.uleb128 0x18
 903 0901 00 00 00 00 	.4byte .LASF8
 904 0905 08          	.byte 0x8
MPLAB XC16 ASSEMBLY Listing:   			page 25


 905 0906 15          	.byte 0x15
 906 0907 55 08 00 00 	.4byte 0x855
 907 090b 01          	.byte 0x1
 908 090c 01          	.byte 0x1
 909 090d 18          	.uleb128 0x18
 910 090e 00 00 00 00 	.4byte .LASF9
 911 0912 08          	.byte 0x8
 912 0913 16          	.byte 0x16
 913 0914 55 08 00 00 	.4byte 0x855
 914 0918 01          	.byte 0x1
 915 0919 01          	.byte 0x1
 916 091a 00          	.byte 0x0
 917                 	.section .debug_abbrev,info
 918 0000 01          	.uleb128 0x1
 919 0001 11          	.uleb128 0x11
 920 0002 01          	.byte 0x1
 921 0003 25          	.uleb128 0x25
 922 0004 08          	.uleb128 0x8
 923 0005 13          	.uleb128 0x13
 924 0006 0B          	.uleb128 0xb
 925 0007 03          	.uleb128 0x3
 926 0008 08          	.uleb128 0x8
 927 0009 1B          	.uleb128 0x1b
 928 000a 08          	.uleb128 0x8
 929 000b 11          	.uleb128 0x11
 930 000c 01          	.uleb128 0x1
 931 000d 12          	.uleb128 0x12
 932 000e 01          	.uleb128 0x1
 933 000f 10          	.uleb128 0x10
 934 0010 06          	.uleb128 0x6
 935 0011 00          	.byte 0x0
 936 0012 00          	.byte 0x0
 937 0013 02          	.uleb128 0x2
 938 0014 24          	.uleb128 0x24
 939 0015 00          	.byte 0x0
 940 0016 0B          	.uleb128 0xb
 941 0017 0B          	.uleb128 0xb
 942 0018 3E          	.uleb128 0x3e
 943 0019 0B          	.uleb128 0xb
 944 001a 03          	.uleb128 0x3
 945 001b 08          	.uleb128 0x8
 946 001c 00          	.byte 0x0
 947 001d 00          	.byte 0x0
 948 001e 03          	.uleb128 0x3
 949 001f 16          	.uleb128 0x16
 950 0020 00          	.byte 0x0
 951 0021 03          	.uleb128 0x3
 952 0022 08          	.uleb128 0x8
 953 0023 3A          	.uleb128 0x3a
 954 0024 0B          	.uleb128 0xb
 955 0025 3B          	.uleb128 0x3b
 956 0026 0B          	.uleb128 0xb
 957 0027 49          	.uleb128 0x49
 958 0028 13          	.uleb128 0x13
 959 0029 00          	.byte 0x0
 960 002a 00          	.byte 0x0
 961 002b 04          	.uleb128 0x4
MPLAB XC16 ASSEMBLY Listing:   			page 26


 962 002c 13          	.uleb128 0x13
 963 002d 01          	.byte 0x1
 964 002e 0B          	.uleb128 0xb
 965 002f 0B          	.uleb128 0xb
 966 0030 3A          	.uleb128 0x3a
 967 0031 0B          	.uleb128 0xb
 968 0032 3B          	.uleb128 0x3b
 969 0033 05          	.uleb128 0x5
 970 0034 01          	.uleb128 0x1
 971 0035 13          	.uleb128 0x13
 972 0036 00          	.byte 0x0
 973 0037 00          	.byte 0x0
 974 0038 05          	.uleb128 0x5
 975 0039 0D          	.uleb128 0xd
 976 003a 00          	.byte 0x0
 977 003b 03          	.uleb128 0x3
 978 003c 08          	.uleb128 0x8
 979 003d 3A          	.uleb128 0x3a
 980 003e 0B          	.uleb128 0xb
 981 003f 3B          	.uleb128 0x3b
 982 0040 05          	.uleb128 0x5
 983 0041 49          	.uleb128 0x49
 984 0042 13          	.uleb128 0x13
 985 0043 0B          	.uleb128 0xb
 986 0044 0B          	.uleb128 0xb
 987 0045 0D          	.uleb128 0xd
 988 0046 0B          	.uleb128 0xb
 989 0047 0C          	.uleb128 0xc
 990 0048 0B          	.uleb128 0xb
 991 0049 38          	.uleb128 0x38
 992 004a 0A          	.uleb128 0xa
 993 004b 00          	.byte 0x0
 994 004c 00          	.byte 0x0
 995 004d 06          	.uleb128 0x6
 996 004e 17          	.uleb128 0x17
 997 004f 01          	.byte 0x1
 998 0050 0B          	.uleb128 0xb
 999 0051 0B          	.uleb128 0xb
 1000 0052 3A          	.uleb128 0x3a
 1001 0053 0B          	.uleb128 0xb
 1002 0054 3B          	.uleb128 0x3b
 1003 0055 05          	.uleb128 0x5
 1004 0056 01          	.uleb128 0x1
 1005 0057 13          	.uleb128 0x13
 1006 0058 00          	.byte 0x0
 1007 0059 00          	.byte 0x0
 1008 005a 07          	.uleb128 0x7
 1009 005b 0D          	.uleb128 0xd
 1010 005c 00          	.byte 0x0
 1011 005d 49          	.uleb128 0x49
 1012 005e 13          	.uleb128 0x13
 1013 005f 00          	.byte 0x0
 1014 0060 00          	.byte 0x0
 1015 0061 08          	.uleb128 0x8
 1016 0062 13          	.uleb128 0x13
 1017 0063 01          	.byte 0x1
 1018 0064 03          	.uleb128 0x3
MPLAB XC16 ASSEMBLY Listing:   			page 27


 1019 0065 08          	.uleb128 0x8
 1020 0066 0B          	.uleb128 0xb
 1021 0067 0B          	.uleb128 0xb
 1022 0068 3A          	.uleb128 0x3a
 1023 0069 0B          	.uleb128 0xb
 1024 006a 3B          	.uleb128 0x3b
 1025 006b 05          	.uleb128 0x5
 1026 006c 01          	.uleb128 0x1
 1027 006d 13          	.uleb128 0x13
 1028 006e 00          	.byte 0x0
 1029 006f 00          	.byte 0x0
 1030 0070 09          	.uleb128 0x9
 1031 0071 0D          	.uleb128 0xd
 1032 0072 00          	.byte 0x0
 1033 0073 49          	.uleb128 0x49
 1034 0074 13          	.uleb128 0x13
 1035 0075 38          	.uleb128 0x38
 1036 0076 0A          	.uleb128 0xa
 1037 0077 00          	.byte 0x0
 1038 0078 00          	.byte 0x0
 1039 0079 0A          	.uleb128 0xa
 1040 007a 16          	.uleb128 0x16
 1041 007b 00          	.byte 0x0
 1042 007c 03          	.uleb128 0x3
 1043 007d 08          	.uleb128 0x8
 1044 007e 3A          	.uleb128 0x3a
 1045 007f 0B          	.uleb128 0xb
 1046 0080 3B          	.uleb128 0x3b
 1047 0081 05          	.uleb128 0x5
 1048 0082 49          	.uleb128 0x49
 1049 0083 13          	.uleb128 0x13
 1050 0084 00          	.byte 0x0
 1051 0085 00          	.byte 0x0
 1052 0086 0B          	.uleb128 0xb
 1053 0087 04          	.uleb128 0x4
 1054 0088 01          	.byte 0x1
 1055 0089 0B          	.uleb128 0xb
 1056 008a 0B          	.uleb128 0xb
 1057 008b 3A          	.uleb128 0x3a
 1058 008c 0B          	.uleb128 0xb
 1059 008d 3B          	.uleb128 0x3b
 1060 008e 0B          	.uleb128 0xb
 1061 008f 01          	.uleb128 0x1
 1062 0090 13          	.uleb128 0x13
 1063 0091 00          	.byte 0x0
 1064 0092 00          	.byte 0x0
 1065 0093 0C          	.uleb128 0xc
 1066 0094 28          	.uleb128 0x28
 1067 0095 00          	.byte 0x0
 1068 0096 03          	.uleb128 0x3
 1069 0097 08          	.uleb128 0x8
 1070 0098 1C          	.uleb128 0x1c
 1071 0099 0D          	.uleb128 0xd
 1072 009a 00          	.byte 0x0
 1073 009b 00          	.byte 0x0
 1074 009c 0D          	.uleb128 0xd
 1075 009d 0D          	.uleb128 0xd
MPLAB XC16 ASSEMBLY Listing:   			page 28


 1076 009e 00          	.byte 0x0
 1077 009f 03          	.uleb128 0x3
 1078 00a0 08          	.uleb128 0x8
 1079 00a1 3A          	.uleb128 0x3a
 1080 00a2 0B          	.uleb128 0xb
 1081 00a3 3B          	.uleb128 0x3b
 1082 00a4 05          	.uleb128 0x5
 1083 00a5 49          	.uleb128 0x49
 1084 00a6 13          	.uleb128 0x13
 1085 00a7 38          	.uleb128 0x38
 1086 00a8 0A          	.uleb128 0xa
 1087 00a9 00          	.byte 0x0
 1088 00aa 00          	.byte 0x0
 1089 00ab 0E          	.uleb128 0xe
 1090 00ac 01          	.uleb128 0x1
 1091 00ad 01          	.byte 0x1
 1092 00ae 49          	.uleb128 0x49
 1093 00af 13          	.uleb128 0x13
 1094 00b0 01          	.uleb128 0x1
 1095 00b1 13          	.uleb128 0x13
 1096 00b2 00          	.byte 0x0
 1097 00b3 00          	.byte 0x0
 1098 00b4 0F          	.uleb128 0xf
 1099 00b5 21          	.uleb128 0x21
 1100 00b6 00          	.byte 0x0
 1101 00b7 49          	.uleb128 0x49
 1102 00b8 13          	.uleb128 0x13
 1103 00b9 2F          	.uleb128 0x2f
 1104 00ba 0B          	.uleb128 0xb
 1105 00bb 00          	.byte 0x0
 1106 00bc 00          	.byte 0x0
 1107 00bd 10          	.uleb128 0x10
 1108 00be 17          	.uleb128 0x17
 1109 00bf 01          	.byte 0x1
 1110 00c0 03          	.uleb128 0x3
 1111 00c1 08          	.uleb128 0x8
 1112 00c2 0B          	.uleb128 0xb
 1113 00c3 0B          	.uleb128 0xb
 1114 00c4 3A          	.uleb128 0x3a
 1115 00c5 0B          	.uleb128 0xb
 1116 00c6 3B          	.uleb128 0x3b
 1117 00c7 05          	.uleb128 0x5
 1118 00c8 01          	.uleb128 0x1
 1119 00c9 13          	.uleb128 0x13
 1120 00ca 00          	.byte 0x0
 1121 00cb 00          	.byte 0x0
 1122 00cc 11          	.uleb128 0x11
 1123 00cd 2E          	.uleb128 0x2e
 1124 00ce 00          	.byte 0x0
 1125 00cf 3F          	.uleb128 0x3f
 1126 00d0 0C          	.uleb128 0xc
 1127 00d1 03          	.uleb128 0x3
 1128 00d2 08          	.uleb128 0x8
 1129 00d3 3A          	.uleb128 0x3a
 1130 00d4 0B          	.uleb128 0xb
 1131 00d5 3B          	.uleb128 0x3b
 1132 00d6 0B          	.uleb128 0xb
MPLAB XC16 ASSEMBLY Listing:   			page 29


 1133 00d7 11          	.uleb128 0x11
 1134 00d8 01          	.uleb128 0x1
 1135 00d9 12          	.uleb128 0x12
 1136 00da 01          	.uleb128 0x1
 1137 00db 40          	.uleb128 0x40
 1138 00dc 0A          	.uleb128 0xa
 1139 00dd 00          	.byte 0x0
 1140 00de 00          	.byte 0x0
 1141 00df 12          	.uleb128 0x12
 1142 00e0 2E          	.uleb128 0x2e
 1143 00e1 01          	.byte 0x1
 1144 00e2 3F          	.uleb128 0x3f
 1145 00e3 0C          	.uleb128 0xc
 1146 00e4 03          	.uleb128 0x3
 1147 00e5 08          	.uleb128 0x8
 1148 00e6 3A          	.uleb128 0x3a
 1149 00e7 0B          	.uleb128 0xb
 1150 00e8 3B          	.uleb128 0x3b
 1151 00e9 0B          	.uleb128 0xb
 1152 00ea 11          	.uleb128 0x11
 1153 00eb 01          	.uleb128 0x1
 1154 00ec 12          	.uleb128 0x12
 1155 00ed 01          	.uleb128 0x1
 1156 00ee 40          	.uleb128 0x40
 1157 00ef 0A          	.uleb128 0xa
 1158 00f0 01          	.uleb128 0x1
 1159 00f1 13          	.uleb128 0x13
 1160 00f2 00          	.byte 0x0
 1161 00f3 00          	.byte 0x0
 1162 00f4 13          	.uleb128 0x13
 1163 00f5 34          	.uleb128 0x34
 1164 00f6 00          	.byte 0x0
 1165 00f7 03          	.uleb128 0x3
 1166 00f8 08          	.uleb128 0x8
 1167 00f9 3A          	.uleb128 0x3a
 1168 00fa 0B          	.uleb128 0xb
 1169 00fb 3B          	.uleb128 0x3b
 1170 00fc 0B          	.uleb128 0xb
 1171 00fd 49          	.uleb128 0x49
 1172 00fe 13          	.uleb128 0x13
 1173 00ff 00          	.byte 0x0
 1174 0100 00          	.byte 0x0
 1175 0101 14          	.uleb128 0x14
 1176 0102 34          	.uleb128 0x34
 1177 0103 00          	.byte 0x0
 1178 0104 03          	.uleb128 0x3
 1179 0105 08          	.uleb128 0x8
 1180 0106 3A          	.uleb128 0x3a
 1181 0107 0B          	.uleb128 0xb
 1182 0108 3B          	.uleb128 0x3b
 1183 0109 0B          	.uleb128 0xb
 1184 010a 49          	.uleb128 0x49
 1185 010b 13          	.uleb128 0x13
 1186 010c 02          	.uleb128 0x2
 1187 010d 0A          	.uleb128 0xa
 1188 010e 00          	.byte 0x0
 1189 010f 00          	.byte 0x0
MPLAB XC16 ASSEMBLY Listing:   			page 30


 1190 0110 15          	.uleb128 0x15
 1191 0111 0B          	.uleb128 0xb
 1192 0112 01          	.byte 0x1
 1193 0113 11          	.uleb128 0x11
 1194 0114 01          	.uleb128 0x1
 1195 0115 12          	.uleb128 0x12
 1196 0116 01          	.uleb128 0x1
 1197 0117 00          	.byte 0x0
 1198 0118 00          	.byte 0x0
 1199 0119 16          	.uleb128 0x16
 1200 011a 34          	.uleb128 0x34
 1201 011b 00          	.byte 0x0
 1202 011c 03          	.uleb128 0x3
 1203 011d 0E          	.uleb128 0xe
 1204 011e 3A          	.uleb128 0x3a
 1205 011f 0B          	.uleb128 0xb
 1206 0120 3B          	.uleb128 0x3b
 1207 0121 05          	.uleb128 0x5
 1208 0122 49          	.uleb128 0x49
 1209 0123 13          	.uleb128 0x13
 1210 0124 3F          	.uleb128 0x3f
 1211 0125 0C          	.uleb128 0xc
 1212 0126 3C          	.uleb128 0x3c
 1213 0127 0C          	.uleb128 0xc
 1214 0128 00          	.byte 0x0
 1215 0129 00          	.byte 0x0
 1216 012a 17          	.uleb128 0x17
 1217 012b 35          	.uleb128 0x35
 1218 012c 00          	.byte 0x0
 1219 012d 49          	.uleb128 0x49
 1220 012e 13          	.uleb128 0x13
 1221 012f 00          	.byte 0x0
 1222 0130 00          	.byte 0x0
 1223 0131 18          	.uleb128 0x18
 1224 0132 34          	.uleb128 0x34
 1225 0133 00          	.byte 0x0
 1226 0134 03          	.uleb128 0x3
 1227 0135 0E          	.uleb128 0xe
 1228 0136 3A          	.uleb128 0x3a
 1229 0137 0B          	.uleb128 0xb
 1230 0138 3B          	.uleb128 0x3b
 1231 0139 0B          	.uleb128 0xb
 1232 013a 49          	.uleb128 0x49
 1233 013b 13          	.uleb128 0x13
 1234 013c 3F          	.uleb128 0x3f
 1235 013d 0C          	.uleb128 0xc
 1236 013e 3C          	.uleb128 0x3c
 1237 013f 0C          	.uleb128 0xc
 1238 0140 00          	.byte 0x0
 1239 0141 00          	.byte 0x0
 1240 0142 19          	.uleb128 0x19
 1241 0143 34          	.uleb128 0x34
 1242 0144 00          	.byte 0x0
 1243 0145 03          	.uleb128 0x3
 1244 0146 0E          	.uleb128 0xe
 1245 0147 3A          	.uleb128 0x3a
 1246 0148 0B          	.uleb128 0xb
MPLAB XC16 ASSEMBLY Listing:   			page 31


 1247 0149 3B          	.uleb128 0x3b
 1248 014a 0B          	.uleb128 0xb
 1249 014b 49          	.uleb128 0x49
 1250 014c 13          	.uleb128 0x13
 1251 014d 3F          	.uleb128 0x3f
 1252 014e 0C          	.uleb128 0xc
 1253 014f 02          	.uleb128 0x2
 1254 0150 0A          	.uleb128 0xa
 1255 0151 00          	.byte 0x0
 1256 0152 00          	.byte 0x0
 1257 0153 00          	.byte 0x0
 1258                 	.section .debug_pubnames,info
 1259 0000 64 00 00 00 	.4byte 0x64
 1260 0004 02 00       	.2byte 0x2
 1261 0006 00 00 00 00 	.4byte .Ldebug_info0
 1262 000a 1B 09 00 00 	.4byte 0x91b
 1263 000e DD 06 00 00 	.4byte 0x6dd
 1264 0012 41 50 50 5F 	.asciz "APP_DeviceCDCBasicDemoInitialize"
 1264      44 65 76 69 
 1264      63 65 43 44 
 1264      43 42 61 73 
 1264      69 63 44 65 
 1264      6D 6F 49 6E 
 1264      69 74 69 61 
 1264      6C 69 7A 65 
 1264      00 
 1265 0033 0C 07 00 00 	.4byte 0x70c
 1266 0037 41 50 50 5F 	.asciz "APP_DeviceCDCBasicDemoTasks"
 1266      44 65 76 69 
 1266      63 65 43 44 
 1266      43 42 61 73 
 1266      69 63 44 65 
 1266      6D 6F 54 61 
 1266      73 6B 73 00 
 1267 0053 C7 08 00 00 	.4byte 0x8c7
 1268 0057 6F 75 74 42 	.asciz "outBufferUSB"
 1268      75 66 66 65 
 1268      72 55 53 42 
 1268      00 
 1269 0064 00 00 00 00 	.4byte 0x0
 1270                 	.section .debug_pubtypes,info
 1271 0000 8B 00 00 00 	.4byte 0x8b
 1272 0004 02 00       	.2byte 0x2
 1273 0006 00 00 00 00 	.4byte .Ldebug_info0
 1274 000a 1B 09 00 00 	.4byte 0x91b
 1275 000e EC 00 00 00 	.4byte 0xec
 1276 0012 75 69 6E 74 	.asciz "uint8_t"
 1276      38 5F 74 00 
 1277 001a 0C 01 00 00 	.4byte 0x10c
 1278 001e 75 69 6E 74 	.asciz "uint16_t"
 1278      31 36 5F 74 
 1278      00 
 1279 0027 2C 01 00 00 	.4byte 0x12c
 1280 002b 75 69 6E 74 	.asciz "uint32_t"
 1280      33 32 5F 74 
 1280      00 
 1281 0034 01 02 00 00 	.4byte 0x201
MPLAB XC16 ASSEMBLY Listing:   			page 32


 1282 0038 74 61 67 55 	.asciz "tagU1PWRCBITS"
 1282      31 50 57 52 
 1282      43 42 49 54 
 1282      53 00 
 1283 0046 21 02 00 00 	.4byte 0x221
 1284 004a 55 31 50 57 	.asciz "U1PWRCBITS"
 1284      52 43 42 49 
 1284      54 53 00 
 1285 0055 03 06 00 00 	.4byte 0x603
 1286 0059 55 53 42 5F 	.asciz "USB_DEVICE_STATE"
 1286      44 45 56 49 
 1286      43 45 5F 53 
 1286      54 41 54 45 
 1286      00 
 1287 006a A8 06 00 00 	.4byte 0x6a8
 1288 006e 5F 4C 49 4E 	.asciz "_LINE_CODING"
 1288      45 5F 43 4F 
 1288      44 49 4E 47 
 1288      00 
 1289 007b C9 06 00 00 	.4byte 0x6c9
 1290 007f 4C 49 4E 45 	.asciz "LINE_CODING"
 1290      5F 43 4F 44 
 1290      49 4E 47 00 
 1291 008b 00 00 00 00 	.4byte 0x0
 1292                 	.section .debug_aranges,info
 1293 0000 14 00 00 00 	.4byte 0x14
 1294 0004 02 00       	.2byte 0x2
 1295 0006 00 00 00 00 	.4byte .Ldebug_info0
 1296 000a 04          	.byte 0x4
 1297 000b 00          	.byte 0x0
 1298 000c 00 00       	.2byte 0x0
 1299 000e 00 00       	.2byte 0x0
 1300 0010 00 00 00 00 	.4byte 0x0
 1301 0014 00 00 00 00 	.4byte 0x0
 1302                 	.section .debug_str,info
 1303                 	.LASF3:
 1304 0000 6C 69 6E 65 	.asciz "line_coding"
 1304      5F 63 6F 64 
 1304      69 6E 67 00 
 1305                 	.LASF4:
 1306 000c 6F 75 74 42 	.asciz "outBufferUSB"
 1306      75 66 66 65 
 1306      72 55 53 42 
 1306      00 
 1307                 	.LASF5:
 1308 0019 67 70 61 63 	.asciz "gpacket_buffer_usb"
 1308      6B 65 74 5F 
 1308      62 75 66 66 
 1308      65 72 5F 75 
 1308      73 62 00 
 1309                 	.LASF0:
 1310 002c 55 31 50 57 	.asciz "U1PWRCbits"
 1310      52 43 62 69 
 1310      74 73 00 
 1311                 	.LASF8:
 1312 0037 72 65 61 64 	.asciz "read_index_usb"
 1312      5F 69 6E 64 
MPLAB XC16 ASSEMBLY Listing:   			page 33


 1312      65 78 5F 75 
 1312      73 62 00 
 1313                 	.LASF1:
 1314 0046 55 53 42 44 	.asciz "USBDeviceState"
 1314      65 76 69 63 
 1314      65 53 74 61 
 1314      74 65 00 
 1315                 	.LASF6:
 1316 0055 67 70 61 63 	.asciz "gpacket_last_pos_usb"
 1316      6B 65 74 5F 
 1316      6C 61 73 74 
 1316      5F 70 6F 73 
 1316      5F 75 73 62 
 1316      00 
 1317                 	.LASF9:
 1318 006a 69 6E 6E 65 	.asciz "inner_write_index_usb"
 1318      72 5F 77 72 
 1318      69 74 65 5F 
 1318      69 6E 64 65 
 1318      78 5F 75 73 
 1318      62 00 
 1319                 	.LASF2:
 1320 0080 63 64 63 5F 	.asciz "cdc_trf_state"
 1320      74 72 66 5F 
 1320      73 74 61 74 
 1320      65 00 
 1321                 	.LASF7:
 1322 008e 77 72 69 74 	.asciz "write_index_usb"
 1322      65 5F 69 6E 
 1322      64 65 78 5F 
 1322      75 73 62 00 
 1323                 	.section .text,code
 1324              	
 1325              	
 1326              	
 1327              	.section __c30_info,info,bss
 1328                 	__psv_trap_errata:
 1329                 	
 1330                 	.section __c30_signature,info,data
 1331 0000 01 00       	.word 0x0001
 1332 0002 01 00       	.word 0x0001
 1333 0004 00 00       	.word 0x0000
 1334                 	
 1335                 	
 1336                 	
 1337                 	.set ___PA___,0
 1338                 	.end
MPLAB XC16 ASSEMBLY Listing:   			page 34


DEFINED SYMBOLS
                            *ABS*:00000000 /home/user/Documents/FST/Programming/project_sub-zero/lib/lib_pic33e/usb/app_device_cdc_basic.c
                            *ABS*:00000001 __33EP256MU806
                            *ABS*:00000001 __dsPIC33E
                            *ABS*:00000001 __HAS_DSP
                            *ABS*:00000001 __HAS_DMAV2
                            *ABS*:00000001 __HAS_CODEGUARD
                            *ABS*:00000001 __HAS_PMP_ENHANCED
                            *ABS*:00000001 __HAS_EDS
                            *ABS*:00000001 __HAS_EP
                            *ABS*:00000011 __TARGET_DIVIDE_CYCLES
                            *ABS*:00000001 __C30ELF
    {standard input}:13     .bss:00000000 _readBufferUSB
    {standard input}:17     .bss:00000040 _outBufferUSB
    {standard input}:22     .text:00000000 _APP_DeviceCDCBasicDemoInitialize
    {standard input}:26     *ABS*:00000000 ___PA___
    {standard input}:61     .text:00000024 _APP_DeviceCDCBasicDemoTasks
    {standard input}:70     *ABS*:00000000 ___BP___
    {standard input}:1328   __c30_info:00000000 __psv_trap_errata
    {standard input}:27     .text:00000000 .L0
                            .text:000000da .L13
                            .text:000000dc .L14
                            .text:000000d6 .L6
                            .text:0000005a .L7
                            .text:0000004c .L8
                            .text:00000078 .L9
                            .text:000000de .L2
                            .text:000000a8 .L10
                            .text:000000de .L15
                            .text:000000cc .L12
                    .debug_abbrev:00000000 .Ldebug_abbrev0
                            .text:00000000 .Ltext0
                            .text:000000e6 .Letext0
                      .debug_line:00000000 .Ldebug_line0
                            .text:00000000 .LFB0
                            .text:00000024 .LFE0
                            .text:00000024 .LFB1
                            .text:000000e6 .LFE1
                            .text:0000003a .LBB2
                            .text:000000d6 .LBE2
                       .debug_str:0000002c .LASF0
                       .debug_str:00000046 .LASF1
                       .debug_str:00000080 .LASF2
                       .debug_str:00000000 .LASF3
                       .debug_str:0000000c .LASF4
                       .debug_str:00000019 .LASF5
                       .debug_str:00000055 .LASF6
                       .debug_str:0000008e .LASF7
                       .debug_str:00000037 .LASF8
                       .debug_str:0000006a .LASF9
                     .debug_frame:00000000 .Lframe0
                      .debug_info:00000000 .Ldebug_info0

UNDEFINED SYMBOLS
_line_coding
CORCON
_USBDeviceState
MPLAB XC16 ASSEMBLY Listing:   			page 35


_U1PWRCbits
_cdc_trf_state
_getsUSBUSART
_receive_handler_usb
_read_index_usb
_write_index_usb
_inner_write_index_usb
_gpacket_buffer_usb
_memcpy
_gpacket_last_pos_usb
_putUSBUSART
_CDCTxService

EQUATE SYMBOLS
/home/user/Documents/FST/Programming/project_sub-zero/lib/lib_pic33e/usb/app_device_cdc_basic.c = 0x0
                  __33EP256MU806 = 0x1
                      __dsPIC33E = 0x1
                       __HAS_DSP = 0x1
                     __HAS_DMAV2 = 0x1
                 __HAS_CODEGUARD = 0x1
              __HAS_PMP_ENHANCED = 0x1
                       __HAS_EDS = 0x1
                        __HAS_EP = 0x1
          __TARGET_DIVIDE_CYCLES = 0x11
                        __C30ELF = 0x1
                        ___PA___ = 0x0
                        ___BP___ = 0x0
