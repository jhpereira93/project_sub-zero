MPLAB XC16 ASSEMBLY Listing:   			page 1


   1              	.file "/home/user/Documents/FST/Programming/project_sub-zero/lib/lib_pic33e/usb/usb_events.c"
   2              	.section .debug_abbrev,info
   3                 	.Ldebug_abbrev0:
   4                 	.section .debug_info,info
   5                 	.Ldebug_info0:
   6                 	.section .debug_line,info
   7                 	.Ldebug_line0:
   8 0000 D6 00 00 00 	.section .text,code
   8      02 00 A2 00 
   8      00 00 01 01 
   8      FB 0E 0A 00 
   8      01 01 01 01 
   8      00 00 00 01 
   8      6C 69 62 2F 
   8      6C 69 62 5F 
   8      70 69 63 33 
   9              	.Ltext0:
  10              	.section .text,code
  11              	.align 2
  12              	.global _USER_USB_CALLBACK_EVENT_HANDLER
  13              	.type _USER_USB_CALLBACK_EVENT_HANDLER,@function
  14              	_USER_USB_CALLBACK_EVENT_HANDLER:
  15              	.LFB0:
  16              	.file 1 "lib/lib_pic33e/usb/usb_events.c"
   1:lib/lib_pic33e/usb/usb_events.c **** #ifndef NOUSB
   2:lib/lib_pic33e/usb/usb_events.c **** /*******************************************************************************
   3:lib/lib_pic33e/usb/usb_events.c **** Copyright 2016 Microchip Technology Inc. (www.microchip.com)
   4:lib/lib_pic33e/usb/usb_events.c **** 
   5:lib/lib_pic33e/usb/usb_events.c **** Licensed under the Apache License, Version 2.0 (the "License");
   6:lib/lib_pic33e/usb/usb_events.c **** you may not use this file except in compliance with the License.
   7:lib/lib_pic33e/usb/usb_events.c **** You may obtain a copy of the License at
   8:lib/lib_pic33e/usb/usb_events.c **** 
   9:lib/lib_pic33e/usb/usb_events.c ****     http://www.apache.org/licenses/LICENSE-2.0
  10:lib/lib_pic33e/usb/usb_events.c **** 
  11:lib/lib_pic33e/usb/usb_events.c **** Unless required by applicable law or agreed to in writing, software
  12:lib/lib_pic33e/usb/usb_events.c **** distributed under the License is distributed on an "AS IS" BASIS,
  13:lib/lib_pic33e/usb/usb_events.c **** WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  14:lib/lib_pic33e/usb/usb_events.c **** See the License for the specific language governing permissions and
  15:lib/lib_pic33e/usb/usb_events.c **** limitations under the License.
  16:lib/lib_pic33e/usb/usb_events.c **** 
  17:lib/lib_pic33e/usb/usb_events.c **** To request to license the code under the MLA license (www.microchip.com/mla_license), 
  18:lib/lib_pic33e/usb/usb_events.c **** please contact mla_licensing@microchip.com
  19:lib/lib_pic33e/usb/usb_events.c **** *******************************************************************************/
  20:lib/lib_pic33e/usb/usb_events.c **** 
  21:lib/lib_pic33e/usb/usb_events.c **** /** INCLUDES *******************************************************/
  22:lib/lib_pic33e/usb/usb_events.c **** #include "system.h"
  23:lib/lib_pic33e/usb/usb_events.c **** 
  24:lib/lib_pic33e/usb/usb_events.c **** #include "app_device_cdc_basic.h"
  25:lib/lib_pic33e/usb/usb_events.c **** 
  26:lib/lib_pic33e/usb/usb_events.c **** #include "usb.h"
  27:lib/lib_pic33e/usb/usb_events.c **** #include "usb_device.h"
  28:lib/lib_pic33e/usb/usb_events.c **** #include "usb_device_cdc.h"
  29:lib/lib_pic33e/usb/usb_events.c **** 
  30:lib/lib_pic33e/usb/usb_events.c **** /*******************************************************************
  31:lib/lib_pic33e/usb/usb_events.c ****  * Function:        bool USER_USB_CALLBACK_EVENT_HANDLER(
  32:lib/lib_pic33e/usb/usb_events.c ****  *                        USB_EVENT event, void *pdata, uint16_t size)
  33:lib/lib_pic33e/usb/usb_events.c ****  *
MPLAB XC16 ASSEMBLY Listing:   			page 2


  34:lib/lib_pic33e/usb/usb_events.c ****  * PreCondition:    None
  35:lib/lib_pic33e/usb/usb_events.c ****  *
  36:lib/lib_pic33e/usb/usb_events.c ****  * Input:           USB_EVENT event - the type of event
  37:lib/lib_pic33e/usb/usb_events.c ****  *                  void *pdata - pointer to the event data
  38:lib/lib_pic33e/usb/usb_events.c ****  *                  uint16_t size - size of the event data
  39:lib/lib_pic33e/usb/usb_events.c ****  *
  40:lib/lib_pic33e/usb/usb_events.c ****  * Output:          None
  41:lib/lib_pic33e/usb/usb_events.c ****  *
  42:lib/lib_pic33e/usb/usb_events.c ****  * Side Effects:    None
  43:lib/lib_pic33e/usb/usb_events.c ****  *
  44:lib/lib_pic33e/usb/usb_events.c ****  * Overview:        This function is called from the USB stack to
  45:lib/lib_pic33e/usb/usb_events.c ****  *                  notify a user application that a USB event
  46:lib/lib_pic33e/usb/usb_events.c ****  *                  occured.  This callback is in interrupt context
  47:lib/lib_pic33e/usb/usb_events.c ****  *                  when the USB_INTERRUPT option is selected.
  48:lib/lib_pic33e/usb/usb_events.c ****  *
  49:lib/lib_pic33e/usb/usb_events.c ****  * Note:            None
  50:lib/lib_pic33e/usb/usb_events.c ****  *******************************************************************/
  51:lib/lib_pic33e/usb/usb_events.c **** bool USER_USB_CALLBACK_EVENT_HANDLER(USB_EVENT event, void *pdata, uint16_t size)
  52:lib/lib_pic33e/usb/usb_events.c **** {
  17              	.loc 1 52 0
  18              	.set ___PA___,1
  19 000000  06 00 FA 	lnk #6
  20              	.LCFI0:
  21              	.loc 1 52 0
  22 000002  00 0F 78 	mov w0,[w14]
  23 000004  11 07 98 	mov w1,[w14+2]
  24 000006  22 07 98 	mov w2,[w14+4]
  53:lib/lib_pic33e/usb/usb_events.c ****     switch( (int) event )
  25              	.loc 1 53 0
  26 000008  1E 00 78 	mov [w14],w0
  27 00000a  21 07 20 	mov #114,w1
  28 00000c  81 0F 50 	sub w0,w1,[w15]
  29              	.set ___BP___,0
  30 00000e  00 00 32 	bra z,.L15
  31 000010  21 07 20 	mov #114,w1
  32 000012  81 0F 50 	sub w0,w1,[w15]
  33              	.set ___BP___,0
  34 000014  00 00 3C 	bra gt,.L12
  35 000016  E2 0F 50 	sub w0,#2,[w15]
  36              	.set ___BP___,0
  37 000018  00 00 32 	bra z,.L16
  38 00001a  E2 0F 50 	sub w0,#2,[w15]
  39              	.set ___BP___,0
  40 00001c  00 00 3C 	bra gt,.L13
  41 00001e  E1 0F 50 	sub w0,#1,[w15]
  42              	.set ___BP___,0
  43 000020  00 00 32 	bra z,.L3
  54:lib/lib_pic33e/usb/usb_events.c ****     {
  55:lib/lib_pic33e/usb/usb_events.c ****         case EVENT_TRANSFER:
  56:lib/lib_pic33e/usb/usb_events.c ****             break;
  57:lib/lib_pic33e/usb/usb_events.c **** 
  58:lib/lib_pic33e/usb/usb_events.c ****         case EVENT_SOF:
  59:lib/lib_pic33e/usb/usb_events.c ****             /* We are using the SOF as a timer to time the LED indicator.  Call
  60:lib/lib_pic33e/usb/usb_events.c ****              * the LED update function here. */
  61:lib/lib_pic33e/usb/usb_events.c ****             //APP_LEDUpdateUSBStatus();
  62:lib/lib_pic33e/usb/usb_events.c ****             break;
  63:lib/lib_pic33e/usb/usb_events.c **** 
MPLAB XC16 ASSEMBLY Listing:   			page 3


  64:lib/lib_pic33e/usb/usb_events.c ****         case EVENT_SUSPEND:
  65:lib/lib_pic33e/usb/usb_events.c ****             /* Update the LED status for the suspend event. */
  66:lib/lib_pic33e/usb/usb_events.c ****             //APP_LEDUpdateUSBStatus();
  67:lib/lib_pic33e/usb/usb_events.c **** 
  68:lib/lib_pic33e/usb/usb_events.c ****             //Call the hardware platform specific handler for suspend events for
  69:lib/lib_pic33e/usb/usb_events.c ****             //possible further action (like optionally going reconfiguring the application
  70:lib/lib_pic33e/usb/usb_events.c ****             //for lower power states and going to sleep during the suspend event).  This
  71:lib/lib_pic33e/usb/usb_events.c ****             //would normally be done in USB compliant bus powered applications, although
  72:lib/lib_pic33e/usb/usb_events.c ****             //no further processing is needed for purely self powered applications that
  73:lib/lib_pic33e/usb/usb_events.c ****             //don't consume power from the host.
  74:lib/lib_pic33e/usb/usb_events.c ****             SYSTEM_Initialize_USB(SYSTEM_STATE_USB_SUSPEND);
  75:lib/lib_pic33e/usb/usb_events.c ****             break;
  76:lib/lib_pic33e/usb/usb_events.c **** 
  77:lib/lib_pic33e/usb/usb_events.c ****         case EVENT_RESUME:
  78:lib/lib_pic33e/usb/usb_events.c ****             /* Update the LED status for the resume event. */
  79:lib/lib_pic33e/usb/usb_events.c ****             //APP_LEDUpdateUSBStatus();
  80:lib/lib_pic33e/usb/usb_events.c **** 
  81:lib/lib_pic33e/usb/usb_events.c ****             //Call the hardware platform specific resume from suspend handler (ex: to
  82:lib/lib_pic33e/usb/usb_events.c ****             //restore I/O pins to higher power states if they were changed during the 
  83:lib/lib_pic33e/usb/usb_events.c ****             //preceding SYSTEM_Initialize_USB(SYSTEM_STATE_USB_SUSPEND) call at the start
  84:lib/lib_pic33e/usb/usb_events.c ****             //of the suspend condition.
  85:lib/lib_pic33e/usb/usb_events.c ****             //SYSTEM_Initialize_USB(SYSTEM_STATE_USB_RESUME);
  86:lib/lib_pic33e/usb/usb_events.c ****             break;
  87:lib/lib_pic33e/usb/usb_events.c **** 
  88:lib/lib_pic33e/usb/usb_events.c ****         case EVENT_CONFIGURED:
  89:lib/lib_pic33e/usb/usb_events.c ****             /* When the device is configured, we can (re)initialize the 
  90:lib/lib_pic33e/usb/usb_events.c ****              * demo code. */
  91:lib/lib_pic33e/usb/usb_events.c ****             CDCInitEP();
  92:lib/lib_pic33e/usb/usb_events.c ****             APP_DeviceCDCBasicDemoInitialize();
  93:lib/lib_pic33e/usb/usb_events.c ****             break;
  94:lib/lib_pic33e/usb/usb_events.c **** 
  95:lib/lib_pic33e/usb/usb_events.c ****         case EVENT_SET_DESCRIPTOR:
  96:lib/lib_pic33e/usb/usb_events.c ****             break;
  97:lib/lib_pic33e/usb/usb_events.c **** 
  98:lib/lib_pic33e/usb/usb_events.c ****         case EVENT_EP0_REQUEST:
  99:lib/lib_pic33e/usb/usb_events.c ****             /* We have received a non-standard USB request.  The HID driver
 100:lib/lib_pic33e/usb/usb_events.c ****              * needs to check to see if the request was for it. */
 101:lib/lib_pic33e/usb/usb_events.c ****             USBCheckCDCRequest();
 102:lib/lib_pic33e/usb/usb_events.c ****             break;
 103:lib/lib_pic33e/usb/usb_events.c **** 
 104:lib/lib_pic33e/usb/usb_events.c ****         case EVENT_BUS_ERROR:
 105:lib/lib_pic33e/usb/usb_events.c ****             break;
 106:lib/lib_pic33e/usb/usb_events.c **** 
 107:lib/lib_pic33e/usb/usb_events.c ****         case EVENT_TRANSFER_TERMINATED:
 108:lib/lib_pic33e/usb/usb_events.c ****             break;
 109:lib/lib_pic33e/usb/usb_events.c **** 
 110:lib/lib_pic33e/usb/usb_events.c ****         default:
 111:lib/lib_pic33e/usb/usb_events.c ****             break;
  44              	.loc 1 111 0
  45 000022  00 00 37 	bra .L14
  46              	.L13:
  47              	.loc 1 53 0
  48 000024  E3 0F 50 	sub w0,#3,[w15]
  49              	.set ___BP___,0
  50 000026  00 00 32 	bra z,.L5
  51              	.loc 1 108 0
  52 000028  00 00 37 	bra .L14
MPLAB XC16 ASSEMBLY Listing:   			page 4


  53              	.L12:
  54              	.loc 1 53 0
  55 00002a  41 07 20 	mov #116,w1
  56 00002c  81 0F 50 	sub w0,w1,[w15]
  57              	.set ___BP___,0
  58 00002e  00 00 32 	bra z,.L17
  59 000030  41 07 20 	mov #116,w1
  60 000032  81 0F 50 	sub w0,w1,[w15]
  61              	.set ___BP___,0
  62 000034  00 00 35 	bra lt,.L18
  63 000036  51 07 20 	mov #117,w1
  64 000038  81 0F 50 	sub w0,w1,[w15]
  65              	.set ___BP___,0
  66 00003a  00 00 3A 	bra nz,.L19
  67              	.L10:
  68              	.loc 1 74 0
  69 00003c  10 00 20 	mov #1,w0
  70 00003e  00 00 07 	rcall _SYSTEM_Initialize_USB
  71              	.loc 1 75 0
  72 000040  00 00 37 	bra .L14
  73              	.L3:
  74              	.loc 1 91 0
  75 000042  00 00 07 	rcall _CDCInitEP
  76              	.loc 1 92 0
  77 000044  00 00 07 	rcall _APP_DeviceCDCBasicDemoInitialize
  78              	.loc 1 93 0
  79 000046  00 00 37 	bra .L14
  80              	.L5:
  81              	.loc 1 101 0
  82 000048  00 00 07 	rcall _USBCheckCDCRequest
  83              	.loc 1 102 0
  84 00004a  00 00 37 	bra .L14
  85              	.L15:
  86 00004c  00 00 37 	bra .L14
  87              	.L16:
  88 00004e  00 00 37 	bra .L14
  89              	.L17:
  90 000050  00 00 37 	bra .L14
  91              	.L18:
  92 000052  00 00 37 	bra .L14
  93              	.L19:
  94              	.L14:
 112:lib/lib_pic33e/usb/usb_events.c ****     }
 113:lib/lib_pic33e/usb/usb_events.c ****     return true;
  95              	.loc 1 113 0
  96 000054  10 C0 B3 	mov.b #1,w0
 114:lib/lib_pic33e/usb/usb_events.c **** }
  97              	.loc 1 114 0
  98 000056  8E 07 78 	mov w14,w15
  99 000058  4F 07 78 	mov [--w15],w14
 100 00005a  00 40 A9 	bclr CORCON,#2
 101 00005c  00 00 06 	return 
 102              	.set ___PA___,0
 103              	.LFE0:
 104              	.size _USER_USB_CALLBACK_EVENT_HANDLER,.-_USER_USB_CALLBACK_EVENT_HANDLER
 105              	.section .debug_frame,info
 106                 	.Lframe0:
MPLAB XC16 ASSEMBLY Listing:   			page 5


 107 0000 10 00 00 00 	.4byte .LECIE0-.LSCIE0
 108                 	.LSCIE0:
 109 0004 FF FF FF FF 	.4byte 0xffffffff
 110 0008 01          	.byte 0x1
 111 0009 00          	.byte 0
 112 000a 01          	.uleb128 0x1
 113 000b 02          	.sleb128 2
 114 000c 25          	.byte 0x25
 115 000d 12          	.byte 0x12
 116 000e 0F          	.uleb128 0xf
 117 000f 7E          	.sleb128 -2
 118 0010 09          	.byte 0x9
 119 0011 25          	.uleb128 0x25
 120 0012 0F          	.uleb128 0xf
 121 0013 00          	.align 4
 122                 	.LECIE0:
 123                 	.LSFDE0:
 124 0014 18 00 00 00 	.4byte .LEFDE0-.LASFDE0
 125                 	.LASFDE0:
 126 0018 00 00 00 00 	.4byte .Lframe0
 127 001c 00 00 00 00 	.4byte .LFB0
 128 0020 5E 00 00 00 	.4byte .LFE0-.LFB0
 129 0024 04          	.byte 0x4
 130 0025 02 00 00 00 	.4byte .LCFI0-.LFB0
 131 0029 13          	.byte 0x13
 132 002a 7D          	.sleb128 -3
 133 002b 0D          	.byte 0xd
 134 002c 0E          	.uleb128 0xe
 135 002d 8E          	.byte 0x8e
 136 002e 02          	.uleb128 0x2
 137 002f 00          	.align 4
 138                 	.LEFDE0:
 139                 	.section .text,code
 140              	.Letext0:
 141              	.file 2 "lib/lib_pic33e/usb/system.h"
 142              	.file 3 "lib/lib_pic33e/usb/usb_common.h"
 143              	.file 4 "/opt/microchip/xc16/v1.36/bin/bin/../../include/lega-c/stdint.h"
 144              	.file 5 "lib/lib_pic33e/usb/usb_device.h"
 145              	.section .debug_info,info
 146 0000 CC 05 00 00 	.4byte 0x5cc
 147 0004 02 00       	.2byte 0x2
 148 0006 00 00 00 00 	.4byte .Ldebug_abbrev0
 149 000a 04          	.byte 0x4
 150 000b 01          	.uleb128 0x1
 151 000c 47 4E 55 20 	.asciz "GNU C 4.5.1 (XC16, Microchip v1.36) (A) Build date: Dec  5 2018"
 151      43 20 34 2E 
 151      35 2E 31 20 
 151      28 58 43 31 
 151      36 2C 20 4D 
 151      69 63 72 6F 
 151      63 68 69 70 
 151      20 76 31 2E 
 151      33 36 29 20 
 152 004c 01          	.byte 0x1
 153 004d 6C 69 62 2F 	.asciz "lib/lib_pic33e/usb/usb_events.c"
 153      6C 69 62 5F 
 153      70 69 63 33 
MPLAB XC16 ASSEMBLY Listing:   			page 6


 153      33 65 2F 75 
 153      73 62 2F 75 
 153      73 62 5F 65 
 153      76 65 6E 74 
 153      73 2E 63 00 
 154 006d 2F 68 6F 6D 	.asciz "/home/user/Documents/FST/Programming/project_sub-zero"
 154      65 2F 75 73 
 154      65 72 2F 44 
 154      6F 63 75 6D 
 154      65 6E 74 73 
 154      2F 46 53 54 
 154      2F 50 72 6F 
 154      67 72 61 6D 
 154      6D 69 6E 67 
 155 00a3 00 00 00 00 	.4byte .Ltext0
 156 00a7 00 00 00 00 	.4byte .Letext0
 157 00ab 00 00 00 00 	.4byte .Ldebug_line0
 158 00af 02          	.uleb128 0x2
 159 00b0 01          	.byte 0x1
 160 00b1 06          	.byte 0x6
 161 00b2 73 69 67 6E 	.asciz "signed char"
 161      65 64 20 63 
 161      68 61 72 00 
 162 00be 02          	.uleb128 0x2
 163 00bf 02          	.byte 0x2
 164 00c0 05          	.byte 0x5
 165 00c1 69 6E 74 00 	.asciz "int"
 166 00c5 02          	.uleb128 0x2
 167 00c6 04          	.byte 0x4
 168 00c7 05          	.byte 0x5
 169 00c8 6C 6F 6E 67 	.asciz "long int"
 169      20 69 6E 74 
 169      00 
 170 00d1 02          	.uleb128 0x2
 171 00d2 08          	.byte 0x8
 172 00d3 05          	.byte 0x5
 173 00d4 6C 6F 6E 67 	.asciz "long long int"
 173      20 6C 6F 6E 
 173      67 20 69 6E 
 173      74 00 
 174 00e2 02          	.uleb128 0x2
 175 00e3 01          	.byte 0x1
 176 00e4 08          	.byte 0x8
 177 00e5 75 6E 73 69 	.asciz "unsigned char"
 177      67 6E 65 64 
 177      20 63 68 61 
 177      72 00 
 178 00f3 03          	.uleb128 0x3
 179 00f4 75 69 6E 74 	.asciz "uint16_t"
 179      31 36 5F 74 
 179      00 
 180 00fd 04          	.byte 0x4
 181 00fe 31          	.byte 0x31
 182 00ff 03 01 00 00 	.4byte 0x103
 183 0103 02          	.uleb128 0x2
 184 0104 02          	.byte 0x2
 185 0105 07          	.byte 0x7
MPLAB XC16 ASSEMBLY Listing:   			page 7


 186 0106 75 6E 73 69 	.asciz "unsigned int"
 186      67 6E 65 64 
 186      20 69 6E 74 
 186      00 
 187 0113 02          	.uleb128 0x2
 188 0114 04          	.byte 0x4
 189 0115 07          	.byte 0x7
 190 0116 6C 6F 6E 67 	.asciz "long unsigned int"
 190      20 75 6E 73 
 190      69 67 6E 65 
 190      64 20 69 6E 
 190      74 00 
 191 0128 02          	.uleb128 0x2
 192 0129 08          	.byte 0x8
 193 012a 07          	.byte 0x7
 194 012b 6C 6F 6E 67 	.asciz "long long unsigned int"
 194      20 6C 6F 6E 
 194      67 20 75 6E 
 194      73 69 67 6E 
 194      65 64 20 69 
 194      6E 74 00 
 195 0142 04          	.uleb128 0x4
 196 0143 02          	.byte 0x2
 197 0144 02          	.byte 0x2
 198 0145 20          	.byte 0x20
 199 0146 99 01 00 00 	.4byte 0x199
 200 014a 05          	.uleb128 0x5
 201 014b 53 59 53 54 	.asciz "SYSTEM_STATE_USB_START"
 201      45 4D 5F 53 
 201      54 41 54 45 
 201      5F 55 53 42 
 201      5F 53 54 41 
 201      52 54 00 
 202 0162 00          	.sleb128 0
 203 0163 05          	.uleb128 0x5
 204 0164 53 59 53 54 	.asciz "SYSTEM_STATE_USB_SUSPEND"
 204      45 4D 5F 53 
 204      54 41 54 45 
 204      5F 55 53 42 
 204      5F 53 55 53 
 204      50 45 4E 44 
 204      00 
 205 017d 01          	.sleb128 1
 206 017e 05          	.uleb128 0x5
 207 017f 53 59 53 54 	.asciz "SYSTEM_STATE_USB_RESUME"
 207      45 4D 5F 53 
 207      54 41 54 45 
 207      5F 55 53 42 
 207      5F 52 45 53 
 207      55 4D 45 00 
 208 0197 02          	.sleb128 2
 209 0198 00          	.byte 0x0
 210 0199 06          	.uleb128 0x6
 211 019a 02          	.byte 0x2
 212 019b 02          	.uleb128 0x2
 213 019c 02          	.byte 0x2
 214 019d 07          	.byte 0x7
MPLAB XC16 ASSEMBLY Listing:   			page 8


 215 019e 73 68 6F 72 	.asciz "short unsigned int"
 215      74 20 75 6E 
 215      73 69 67 6E 
 215      65 64 20 69 
 215      6E 74 00 
 216 01b1 04          	.uleb128 0x4
 217 01b2 02          	.byte 0x2
 218 01b3 03          	.byte 0x3
 219 01b4 CF          	.byte 0xcf
 220 01b5 DF 04 00 00 	.4byte 0x4df
 221 01b9 05          	.uleb128 0x5
 222 01ba 45 56 45 4E 	.asciz "EVENT_NONE"
 222      54 5F 4E 4F 
 222      4E 45 00 
 223 01c5 00          	.sleb128 0
 224 01c6 05          	.uleb128 0x5
 225 01c7 45 56 45 4E 	.asciz "EVENT_DEVICE_STACK_BASE"
 225      54 5F 44 45 
 225      56 49 43 45 
 225      5F 53 54 41 
 225      43 4B 5F 42 
 225      41 53 45 00 
 226 01df 01          	.sleb128 1
 227 01e0 05          	.uleb128 0x5
 228 01e1 45 56 45 4E 	.asciz "EVENT_HOST_STACK_BASE"
 228      54 5F 48 4F 
 228      53 54 5F 53 
 228      54 41 43 4B 
 228      5F 42 41 53 
 228      45 00 
 229 01f7 E4 00       	.sleb128 100
 230 01f9 05          	.uleb128 0x5
 231 01fa 45 56 45 4E 	.asciz "EVENT_HUB_ATTACH"
 231      54 5F 48 55 
 231      42 5F 41 54 
 231      54 41 43 48 
 231      00 
 232 020b E5 00       	.sleb128 101
 233 020d 05          	.uleb128 0x5
 234 020e 45 56 45 4E 	.asciz "EVENT_STALL"
 234      54 5F 53 54 
 234      41 4C 4C 00 
 235 021a E6 00       	.sleb128 102
 236 021c 05          	.uleb128 0x5
 237 021d 45 56 45 4E 	.asciz "EVENT_VBUS_SES_REQUEST"
 237      54 5F 56 42 
 237      55 53 5F 53 
 237      45 53 5F 52 
 237      45 51 55 45 
 237      53 54 00 
 238 0234 E7 00       	.sleb128 103
 239 0236 05          	.uleb128 0x5
 240 0237 45 56 45 4E 	.asciz "EVENT_VBUS_OVERCURRENT"
 240      54 5F 56 42 
 240      55 53 5F 4F 
 240      56 45 52 43 
 240      55 52 52 45 
MPLAB XC16 ASSEMBLY Listing:   			page 9


 240      4E 54 00 
 241 024e E8 00       	.sleb128 104
 242 0250 05          	.uleb128 0x5
 243 0251 45 56 45 4E 	.asciz "EVENT_VBUS_REQUEST_POWER"
 243      54 5F 56 42 
 243      55 53 5F 52 
 243      45 51 55 45 
 243      53 54 5F 50 
 243      4F 57 45 52 
 243      00 
 244 026a E9 00       	.sleb128 105
 245 026c 05          	.uleb128 0x5
 246 026d 45 56 45 4E 	.asciz "EVENT_VBUS_RELEASE_POWER"
 246      54 5F 56 42 
 246      55 53 5F 52 
 246      45 4C 45 41 
 246      53 45 5F 50 
 246      4F 57 45 52 
 246      00 
 247 0286 EA 00       	.sleb128 106
 248 0288 05          	.uleb128 0x5
 249 0289 45 56 45 4E 	.asciz "EVENT_VBUS_POWER_AVAILABLE"
 249      54 5F 56 42 
 249      55 53 5F 50 
 249      4F 57 45 52 
 249      5F 41 56 41 
 249      49 4C 41 42 
 249      4C 45 00 
 250 02a4 EB 00       	.sleb128 107
 251 02a6 05          	.uleb128 0x5
 252 02a7 45 56 45 4E 	.asciz "EVENT_UNSUPPORTED_DEVICE"
 252      54 5F 55 4E 
 252      53 55 50 50 
 252      4F 52 54 45 
 252      44 5F 44 45 
 252      56 49 43 45 
 252      00 
 253 02c0 EC 00       	.sleb128 108
 254 02c2 05          	.uleb128 0x5
 255 02c3 45 56 45 4E 	.asciz "EVENT_CANNOT_ENUMERATE"
 255      54 5F 43 41 
 255      4E 4E 4F 54 
 255      5F 45 4E 55 
 255      4D 45 52 41 
 255      54 45 00 
 256 02da ED 00       	.sleb128 109
 257 02dc 05          	.uleb128 0x5
 258 02dd 45 56 45 4E 	.asciz "EVENT_CLIENT_INIT_ERROR"
 258      54 5F 43 4C 
 258      49 45 4E 54 
 258      5F 49 4E 49 
 258      54 5F 45 52 
 258      52 4F 52 00 
 259 02f5 EE 00       	.sleb128 110
 260 02f7 05          	.uleb128 0x5
 261 02f8 45 56 45 4E 	.asciz "EVENT_OUT_OF_MEMORY"
 261      54 5F 4F 55 
MPLAB XC16 ASSEMBLY Listing:   			page 10


 261      54 5F 4F 46 
 261      5F 4D 45 4D 
 261      4F 52 59 00 
 262 030c EF 00       	.sleb128 111
 263 030e 05          	.uleb128 0x5
 264 030f 45 56 45 4E 	.asciz "EVENT_UNSPECIFIED_ERROR"
 264      54 5F 55 4E 
 264      53 50 45 43 
 264      49 46 49 45 
 264      44 5F 45 52 
 264      52 4F 52 00 
 265 0327 F0 00       	.sleb128 112
 266 0329 05          	.uleb128 0x5
 267 032a 45 56 45 4E 	.asciz "EVENT_DETACH"
 267      54 5F 44 45 
 267      54 41 43 48 
 267      00 
 268 0337 F1 00       	.sleb128 113
 269 0339 05          	.uleb128 0x5
 270 033a 45 56 45 4E 	.asciz "EVENT_TRANSFER"
 270      54 5F 54 52 
 270      41 4E 53 46 
 270      45 52 00 
 271 0349 F2 00       	.sleb128 114
 272 034b 05          	.uleb128 0x5
 273 034c 45 56 45 4E 	.asciz "EVENT_SOF"
 273      54 5F 53 4F 
 273      46 00 
 274 0356 F3 00       	.sleb128 115
 275 0358 05          	.uleb128 0x5
 276 0359 45 56 45 4E 	.asciz "EVENT_RESUME"
 276      54 5F 52 45 
 276      53 55 4D 45 
 276      00 
 277 0366 F4 00       	.sleb128 116
 278 0368 05          	.uleb128 0x5
 279 0369 45 56 45 4E 	.asciz "EVENT_SUSPEND"
 279      54 5F 53 55 
 279      53 50 45 4E 
 279      44 00 
 280 0377 F5 00       	.sleb128 117
 281 0379 05          	.uleb128 0x5
 282 037a 45 56 45 4E 	.asciz "EVENT_RESET"
 282      54 5F 52 45 
 282      53 45 54 00 
 283 0386 F6 00       	.sleb128 118
 284 0388 05          	.uleb128 0x5
 285 0389 45 56 45 4E 	.asciz "EVENT_DATA_ISOC_READ"
 285      54 5F 44 41 
 285      54 41 5F 49 
 285      53 4F 43 5F 
 285      52 45 41 44 
 285      00 
 286 039e F7 00       	.sleb128 119
 287 03a0 05          	.uleb128 0x5
 288 03a1 45 56 45 4E 	.asciz "EVENT_DATA_ISOC_WRITE"
 288      54 5F 44 41 
MPLAB XC16 ASSEMBLY Listing:   			page 11


 288      54 41 5F 49 
 288      53 4F 43 5F 
 288      57 52 49 54 
 288      45 00 
 289 03b7 F8 00       	.sleb128 120
 290 03b9 05          	.uleb128 0x5
 291 03ba 45 56 45 4E 	.asciz "EVENT_OVERRIDE_CLIENT_DRIVER_SELECTION"
 291      54 5F 4F 56 
 291      45 52 52 49 
 291      44 45 5F 43 
 291      4C 49 45 4E 
 291      54 5F 44 52 
 291      49 56 45 52 
 291      5F 53 45 4C 
 291      45 43 54 49 
 292 03e1 F9 00       	.sleb128 121
 293 03e3 05          	.uleb128 0x5
 294 03e4 45 56 45 4E 	.asciz "EVENT_1MS"
 294      54 5F 31 4D 
 294      53 00 
 295 03ee FA 00       	.sleb128 122
 296 03f0 05          	.uleb128 0x5
 297 03f1 45 56 45 4E 	.asciz "EVENT_ALT_INTERFACE"
 297      54 5F 41 4C 
 297      54 5F 49 4E 
 297      54 45 52 46 
 297      41 43 45 00 
 298 0405 FB 00       	.sleb128 123
 299 0407 05          	.uleb128 0x5
 300 0408 45 56 45 4E 	.asciz "EVENT_HOLD_BEFORE_CONFIGURATION"
 300      54 5F 48 4F 
 300      4C 44 5F 42 
 300      45 46 4F 52 
 300      45 5F 43 4F 
 300      4E 46 49 47 
 300      55 52 41 54 
 300      49 4F 4E 00 
 301 0428 FC 00       	.sleb128 124
 302 042a 05          	.uleb128 0x5
 303 042b 45 56 45 4E 	.asciz "EVENT_GENERIC_BASE"
 303      54 5F 47 45 
 303      4E 45 52 49 
 303      43 5F 42 41 
 303      53 45 00 
 304 043e 90 03       	.sleb128 400
 305 0440 05          	.uleb128 0x5
 306 0441 45 56 45 4E 	.asciz "EVENT_MSD_BASE"
 306      54 5F 4D 53 
 306      44 5F 42 41 
 306      53 45 00 
 307 0450 F4 03       	.sleb128 500
 308 0452 05          	.uleb128 0x5
 309 0453 45 56 45 4E 	.asciz "EVENT_HID_BASE"
 309      54 5F 48 49 
 309      44 5F 42 41 
 309      53 45 00 
 310 0462 D8 04       	.sleb128 600
MPLAB XC16 ASSEMBLY Listing:   			page 12


 311 0464 05          	.uleb128 0x5
 312 0465 45 56 45 4E 	.asciz "EVENT_PRINTER_BASE"
 312      54 5F 50 52 
 312      49 4E 54 45 
 312      52 5F 42 41 
 312      53 45 00 
 313 0478 BC 05       	.sleb128 700
 314 047a 05          	.uleb128 0x5
 315 047b 45 56 45 4E 	.asciz "EVENT_CDC_BASE"
 315      54 5F 43 44 
 315      43 5F 42 41 
 315      53 45 00 
 316 048a A0 06       	.sleb128 800
 317 048c 05          	.uleb128 0x5
 318 048d 45 56 45 4E 	.asciz "EVENT_CHARGER_BASE"
 318      54 5F 43 48 
 318      41 52 47 45 
 318      52 5F 42 41 
 318      53 45 00 
 319 04a0 84 07       	.sleb128 900
 320 04a2 05          	.uleb128 0x5
 321 04a3 45 56 45 4E 	.asciz "EVENT_AUDIO_BASE"
 321      54 5F 41 55 
 321      44 49 4F 5F 
 321      42 41 53 45 
 321      00 
 322 04b4 E8 07       	.sleb128 1000
 323 04b6 05          	.uleb128 0x5
 324 04b7 45 56 45 4E 	.asciz "EVENT_USER_BASE"
 324      54 5F 55 53 
 324      45 52 5F 42 
 324      41 53 45 00 
 325 04c7 90 CE 00    	.sleb128 10000
 326 04ca 05          	.uleb128 0x5
 327 04cb 45 56 45 4E 	.asciz "EVENT_BUS_ERROR"
 327      54 5F 42 55 
 327      53 5F 45 52 
 327      52 4F 52 00 
 328 04db FF FF 01    	.sleb128 32767
 329 04de 00          	.byte 0x0
 330 04df 07          	.uleb128 0x7
 331 04e0 55 53 42 5F 	.asciz "USB_EVENT"
 331      45 56 45 4E 
 331      54 00 
 332 04ea 03          	.byte 0x3
 333 04eb 67 01       	.2byte 0x167
 334 04ed B1 01 00 00 	.4byte 0x1b1
 335 04f1 02          	.uleb128 0x2
 336 04f2 01          	.byte 0x1
 337 04f3 02          	.byte 0x2
 338 04f4 5F 42 6F 6F 	.asciz "_Bool"
 338      6C 00 
 339 04fa 04          	.uleb128 0x4
 340 04fb 02          	.byte 0x2
 341 04fc 05          	.byte 0x5
 342 04fd 73          	.byte 0x73
 343 04fe 6C 05 00 00 	.4byte 0x56c
MPLAB XC16 ASSEMBLY Listing:   			page 13


 344 0502 05          	.uleb128 0x5
 345 0503 45 56 45 4E 	.asciz "EVENT_CONFIGURED"
 345      54 5F 43 4F 
 345      4E 46 49 47 
 345      55 52 45 44 
 345      00 
 346 0514 01          	.sleb128 1
 347 0515 05          	.uleb128 0x5
 348 0516 45 56 45 4E 	.asciz "EVENT_SET_DESCRIPTOR"
 348      54 5F 53 45 
 348      54 5F 44 45 
 348      53 43 52 49 
 348      50 54 4F 52 
 348      00 
 349 052b 02          	.sleb128 2
 350 052c 05          	.uleb128 0x5
 351 052d 45 56 45 4E 	.asciz "EVENT_EP0_REQUEST"
 351      54 5F 45 50 
 351      30 5F 52 45 
 351      51 55 45 53 
 351      54 00 
 352 053f 03          	.sleb128 3
 353 0540 05          	.uleb128 0x5
 354 0541 45 56 45 4E 	.asciz "EVENT_ATTACH"
 354      54 5F 41 54 
 354      54 41 43 48 
 354      00 
 355 054e 04          	.sleb128 4
 356 054f 05          	.uleb128 0x5
 357 0550 45 56 45 4E 	.asciz "EVENT_TRANSFER_TERMINATED"
 357      54 5F 54 52 
 357      41 4E 53 46 
 357      45 52 5F 54 
 357      45 52 4D 49 
 357      4E 41 54 45 
 357      44 00 
 358 056a 05          	.sleb128 5
 359 056b 00          	.byte 0x0
 360 056c 08          	.uleb128 0x8
 361 056d 01          	.byte 0x1
 362 056e 55 53 45 52 	.asciz "USER_USB_CALLBACK_EVENT_HANDLER"
 362      5F 55 53 42 
 362      5F 43 41 4C 
 362      4C 42 41 43 
 362      4B 5F 45 56 
 362      45 4E 54 5F 
 362      48 41 4E 44 
 362      4C 45 52 00 
 363 058e 01          	.byte 0x1
 364 058f 33          	.byte 0x33
 365 0590 01          	.byte 0x1
 366 0591 F1 04 00 00 	.4byte 0x4f1
 367 0595 00 00 00 00 	.4byte .LFB0
 368 0599 00 00 00 00 	.4byte .LFE0
 369 059d 01          	.byte 0x1
 370 059e 5E          	.byte 0x5e
 371 059f 09          	.uleb128 0x9
MPLAB XC16 ASSEMBLY Listing:   			page 14


 372 05a0 65 76 65 6E 	.asciz "event"
 372      74 00 
 373 05a6 01          	.byte 0x1
 374 05a7 33          	.byte 0x33
 375 05a8 DF 04 00 00 	.4byte 0x4df
 376 05ac 02          	.byte 0x2
 377 05ad 7E          	.byte 0x7e
 378 05ae 00          	.sleb128 0
 379 05af 09          	.uleb128 0x9
 380 05b0 70 64 61 74 	.asciz "pdata"
 380      61 00 
 381 05b6 01          	.byte 0x1
 382 05b7 33          	.byte 0x33
 383 05b8 99 01 00 00 	.4byte 0x199
 384 05bc 02          	.byte 0x2
 385 05bd 7E          	.byte 0x7e
 386 05be 02          	.sleb128 2
 387 05bf 09          	.uleb128 0x9
 388 05c0 73 69 7A 65 	.asciz "size"
 388      00 
 389 05c5 01          	.byte 0x1
 390 05c6 33          	.byte 0x33
 391 05c7 F3 00 00 00 	.4byte 0xf3
 392 05cb 02          	.byte 0x2
 393 05cc 7E          	.byte 0x7e
 394 05cd 04          	.sleb128 4
 395 05ce 00          	.byte 0x0
 396 05cf 00          	.byte 0x0
 397                 	.section .debug_abbrev,info
 398 0000 01          	.uleb128 0x1
 399 0001 11          	.uleb128 0x11
 400 0002 01          	.byte 0x1
 401 0003 25          	.uleb128 0x25
 402 0004 08          	.uleb128 0x8
 403 0005 13          	.uleb128 0x13
 404 0006 0B          	.uleb128 0xb
 405 0007 03          	.uleb128 0x3
 406 0008 08          	.uleb128 0x8
 407 0009 1B          	.uleb128 0x1b
 408 000a 08          	.uleb128 0x8
 409 000b 11          	.uleb128 0x11
 410 000c 01          	.uleb128 0x1
 411 000d 12          	.uleb128 0x12
 412 000e 01          	.uleb128 0x1
 413 000f 10          	.uleb128 0x10
 414 0010 06          	.uleb128 0x6
 415 0011 00          	.byte 0x0
 416 0012 00          	.byte 0x0
 417 0013 02          	.uleb128 0x2
 418 0014 24          	.uleb128 0x24
 419 0015 00          	.byte 0x0
 420 0016 0B          	.uleb128 0xb
 421 0017 0B          	.uleb128 0xb
 422 0018 3E          	.uleb128 0x3e
 423 0019 0B          	.uleb128 0xb
 424 001a 03          	.uleb128 0x3
 425 001b 08          	.uleb128 0x8
MPLAB XC16 ASSEMBLY Listing:   			page 15


 426 001c 00          	.byte 0x0
 427 001d 00          	.byte 0x0
 428 001e 03          	.uleb128 0x3
 429 001f 16          	.uleb128 0x16
 430 0020 00          	.byte 0x0
 431 0021 03          	.uleb128 0x3
 432 0022 08          	.uleb128 0x8
 433 0023 3A          	.uleb128 0x3a
 434 0024 0B          	.uleb128 0xb
 435 0025 3B          	.uleb128 0x3b
 436 0026 0B          	.uleb128 0xb
 437 0027 49          	.uleb128 0x49
 438 0028 13          	.uleb128 0x13
 439 0029 00          	.byte 0x0
 440 002a 00          	.byte 0x0
 441 002b 04          	.uleb128 0x4
 442 002c 04          	.uleb128 0x4
 443 002d 01          	.byte 0x1
 444 002e 0B          	.uleb128 0xb
 445 002f 0B          	.uleb128 0xb
 446 0030 3A          	.uleb128 0x3a
 447 0031 0B          	.uleb128 0xb
 448 0032 3B          	.uleb128 0x3b
 449 0033 0B          	.uleb128 0xb
 450 0034 01          	.uleb128 0x1
 451 0035 13          	.uleb128 0x13
 452 0036 00          	.byte 0x0
 453 0037 00          	.byte 0x0
 454 0038 05          	.uleb128 0x5
 455 0039 28          	.uleb128 0x28
 456 003a 00          	.byte 0x0
 457 003b 03          	.uleb128 0x3
 458 003c 08          	.uleb128 0x8
 459 003d 1C          	.uleb128 0x1c
 460 003e 0D          	.uleb128 0xd
 461 003f 00          	.byte 0x0
 462 0040 00          	.byte 0x0
 463 0041 06          	.uleb128 0x6
 464 0042 0F          	.uleb128 0xf
 465 0043 00          	.byte 0x0
 466 0044 0B          	.uleb128 0xb
 467 0045 0B          	.uleb128 0xb
 468 0046 00          	.byte 0x0
 469 0047 00          	.byte 0x0
 470 0048 07          	.uleb128 0x7
 471 0049 16          	.uleb128 0x16
 472 004a 00          	.byte 0x0
 473 004b 03          	.uleb128 0x3
 474 004c 08          	.uleb128 0x8
 475 004d 3A          	.uleb128 0x3a
 476 004e 0B          	.uleb128 0xb
 477 004f 3B          	.uleb128 0x3b
 478 0050 05          	.uleb128 0x5
 479 0051 49          	.uleb128 0x49
 480 0052 13          	.uleb128 0x13
 481 0053 00          	.byte 0x0
 482 0054 00          	.byte 0x0
MPLAB XC16 ASSEMBLY Listing:   			page 16


 483 0055 08          	.uleb128 0x8
 484 0056 2E          	.uleb128 0x2e
 485 0057 01          	.byte 0x1
 486 0058 3F          	.uleb128 0x3f
 487 0059 0C          	.uleb128 0xc
 488 005a 03          	.uleb128 0x3
 489 005b 08          	.uleb128 0x8
 490 005c 3A          	.uleb128 0x3a
 491 005d 0B          	.uleb128 0xb
 492 005e 3B          	.uleb128 0x3b
 493 005f 0B          	.uleb128 0xb
 494 0060 27          	.uleb128 0x27
 495 0061 0C          	.uleb128 0xc
 496 0062 49          	.uleb128 0x49
 497 0063 13          	.uleb128 0x13
 498 0064 11          	.uleb128 0x11
 499 0065 01          	.uleb128 0x1
 500 0066 12          	.uleb128 0x12
 501 0067 01          	.uleb128 0x1
 502 0068 40          	.uleb128 0x40
 503 0069 0A          	.uleb128 0xa
 504 006a 00          	.byte 0x0
 505 006b 00          	.byte 0x0
 506 006c 09          	.uleb128 0x9
 507 006d 05          	.uleb128 0x5
 508 006e 00          	.byte 0x0
 509 006f 03          	.uleb128 0x3
 510 0070 08          	.uleb128 0x8
 511 0071 3A          	.uleb128 0x3a
 512 0072 0B          	.uleb128 0xb
 513 0073 3B          	.uleb128 0x3b
 514 0074 0B          	.uleb128 0xb
 515 0075 49          	.uleb128 0x49
 516 0076 13          	.uleb128 0x13
 517 0077 02          	.uleb128 0x2
 518 0078 0A          	.uleb128 0xa
 519 0079 00          	.byte 0x0
 520 007a 00          	.byte 0x0
 521 007b 00          	.byte 0x0
 522                 	.section .debug_pubnames,info
 523 0000 32 00 00 00 	.4byte 0x32
 524 0004 02 00       	.2byte 0x2
 525 0006 00 00 00 00 	.4byte .Ldebug_info0
 526 000a D0 05 00 00 	.4byte 0x5d0
 527 000e 6C 05 00 00 	.4byte 0x56c
 528 0012 55 53 45 52 	.asciz "USER_USB_CALLBACK_EVENT_HANDLER"
 528      5F 55 53 42 
 528      5F 43 41 4C 
 528      4C 42 41 43 
 528      4B 5F 45 56 
 528      45 4E 54 5F 
 528      48 41 4E 44 
 528      4C 45 52 00 
 529 0032 00 00 00 00 	.4byte 0x0
 530                 	.section .debug_pubtypes,info
 531 0000 29 00 00 00 	.4byte 0x29
 532 0004 02 00       	.2byte 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 17


 533 0006 00 00 00 00 	.4byte .Ldebug_info0
 534 000a D0 05 00 00 	.4byte 0x5d0
 535 000e F3 00 00 00 	.4byte 0xf3
 536 0012 75 69 6E 74 	.asciz "uint16_t"
 536      31 36 5F 74 
 536      00 
 537 001b DF 04 00 00 	.4byte 0x4df
 538 001f 55 53 42 5F 	.asciz "USB_EVENT"
 538      45 56 45 4E 
 538      54 00 
 539 0029 00 00 00 00 	.4byte 0x0
 540                 	.section .debug_aranges,info
 541 0000 14 00 00 00 	.4byte 0x14
 542 0004 02 00       	.2byte 0x2
 543 0006 00 00 00 00 	.4byte .Ldebug_info0
 544 000a 04          	.byte 0x4
 545 000b 00          	.byte 0x0
 546 000c 00 00       	.2byte 0x0
 547 000e 00 00       	.2byte 0x0
 548 0010 00 00 00 00 	.4byte 0x0
 549 0014 00 00 00 00 	.4byte 0x0
 550                 	.section .debug_str,info
 551                 	.section .text,code
 552              	
 553              	
 554              	
 555              	.section __c30_info,info,bss
 556                 	__psv_trap_errata:
 557                 	
 558                 	.section __c30_signature,info,data
 559 0000 01 00       	.word 0x0001
 560 0002 00 00       	.word 0x0000
 561 0004 00 00       	.word 0x0000
 562                 	
 563                 	
 564                 	
 565                 	.set ___PA___,0
 566                 	.end
MPLAB XC16 ASSEMBLY Listing:   			page 18


DEFINED SYMBOLS
                            *ABS*:00000000 /home/user/Documents/FST/Programming/project_sub-zero/lib/lib_pic33e/usb/usb_events.c
                            *ABS*:00000001 __33EP256MU806
                            *ABS*:00000001 __dsPIC33E
                            *ABS*:00000001 __HAS_DSP
                            *ABS*:00000001 __HAS_DMAV2
                            *ABS*:00000001 __HAS_CODEGUARD
                            *ABS*:00000001 __HAS_PMP_ENHANCED
                            *ABS*:00000001 __HAS_EDS
                            *ABS*:00000001 __HAS_EP
                            *ABS*:00000011 __TARGET_DIVIDE_CYCLES
                            *ABS*:00000001 __C30ELF
    {standard input}:14     .text:00000000 _USER_USB_CALLBACK_EVENT_HANDLER
    {standard input}:18     *ABS*:00000000 ___PA___
    {standard input}:29     *ABS*:00000000 ___BP___
    {standard input}:556    __c30_info:00000000 __psv_trap_errata
    {standard input}:19     .text:00000000 .L0
                            .text:0000004c .L15
                            .text:0000002a .L12
                            .text:0000004e .L16
                            .text:00000024 .L13
                            .text:00000042 .L3
                            .text:00000054 .L14
                            .text:00000048 .L5
                            .text:00000050 .L17
                            .text:00000052 .L18
                            .text:00000054 .L19
                    .debug_abbrev:00000000 .Ldebug_abbrev0
                            .text:00000000 .Ltext0
                            .text:0000005e .Letext0
                      .debug_line:00000000 .Ldebug_line0
                            .text:00000000 .LFB0
                            .text:0000005e .LFE0
                     .debug_frame:00000000 .Lframe0
                      .debug_info:00000000 .Ldebug_info0

UNDEFINED SYMBOLS
_SYSTEM_Initialize_USB
_CDCInitEP
_APP_DeviceCDCBasicDemoInitialize
_USBCheckCDCRequest
CORCON

EQUATE SYMBOLS
/home/user/Documents/FST/Programming/project_sub-zero/lib/lib_pic33e/usb/usb_events.c = 0x0
                  __33EP256MU806 = 0x1
                      __dsPIC33E = 0x1
                       __HAS_DSP = 0x1
                     __HAS_DMAV2 = 0x1
                 __HAS_CODEGUARD = 0x1
              __HAS_PMP_ENHANCED = 0x1
                       __HAS_EDS = 0x1
                        __HAS_EP = 0x1
          __TARGET_DIVIDE_CYCLES = 0x11
                        __C30ELF = 0x1
                        ___PA___ = 0x0
                        ___BP___ = 0x0
