MPLAB XC16 ASSEMBLY Listing:   			page 1


   1              	.file "/home/user/Documents/FST/Programming/project_sub-zero/lib/lib_pic33e/usb/usb_device_cdc.c"
   2              	.section .debug_abbrev,info
   3                 	.Ldebug_abbrev0:
   4                 	.section .debug_info,info
   5                 	.Ldebug_info0:
   6                 	.section .debug_line,info
   7                 	.Ldebug_line0:
   8 0000 17 02 00 00 	.section .text,code
   8      02 00 1F 01 
   8      00 00 01 01 
   8      FB 0E 0A 00 
   8      01 01 01 01 
   8      00 00 00 01 
   8      6C 69 62 2F 
   8      6C 69 62 5F 
   8      70 69 63 33 
   9              	.Ltext0:
  10              	.section .bss,bss
  11                 	.type _cdc_data_tx,@object
  12                 	.global _cdc_data_tx
  13 0000 00 00 00 00 	_cdc_data_tx:.space 64
  13      00 00 00 00 
  13      00 00 00 00 
  13      00 00 00 00 
  13      00 00 00 00 
  13      00 00 00 00 
  13      00 00 00 00 
  13      00 00 00 00 
  13      00 00 00 00 
  14                 	.type _cdc_data_rx,@object
  15                 	.global _cdc_data_rx
  16 0040 00 00 00 00 	_cdc_data_rx:.space 64
  16      00 00 00 00 
  16      00 00 00 00 
  16      00 00 00 00 
  16      00 00 00 00 
  16      00 00 00 00 
  16      00 00 00 00 
  16      00 00 00 00 
  16      00 00 00 00 
  17                 	.type _line_coding,@object
  18                 	.global _line_coding
  19                 	.align 2
  20 0080 00 00 00 00 	_line_coding:.space 8
  20      00 00 00 00 
  21                 	.type _cdc_notice,@object
  22                 	.global _cdc_notice
  23 0088 00 00 00 00 	_cdc_notice:.space 10
  23      00 00 00 00 
  23      00 00 
  24                 	.section .nbss,bss,near
  25                 	.type _cdc_rx_len,@object
  26                 	.global _cdc_rx_len
  27 0000 00          	_cdc_rx_len:.space 1
  28                 	.type _cdc_trf_state,@object
  29                 	.global _cdc_trf_state
  30 0001 00          	_cdc_trf_state:.space 1
MPLAB XC16 ASSEMBLY Listing:   			page 2


  31                 	.type _pCDCSrc,@object
  32                 	.global _pCDCSrc
  33                 	.align 2
  34 0002 00 00       	_pCDCSrc:.space 2
  35                 	.type _pCDCDst,@object
  36                 	.global _pCDCDst
  37                 	.align 2
  38 0004 00 00       	_pCDCDst:.space 2
  39                 	.type _cdc_tx_len,@object
  40                 	.global _cdc_tx_len
  41 0006 00          	_cdc_tx_len:.space 1
  42                 	.type _cdc_mem_type,@object
  43                 	.global _cdc_mem_type
  44 0007 00          	_cdc_mem_type:.space 1
  45                 	.type _CDCDataOutHandle,@object
  46                 	.global _CDCDataOutHandle
  47                 	.align 2
  48 0008 00 00       	_CDCDataOutHandle:.space 2
  49                 	.type _CDCDataInHandle,@object
  50                 	.global _CDCDataInHandle
  51                 	.align 2
  52 000a 00 00       	_CDCDataInHandle:.space 2
  53                 	.type _control_signal_bitmap,@object
  54                 	.global _control_signal_bitmap
  55                 	.align 2
  56 000c 00 00       	_control_signal_bitmap:.space 2
  57                 	.type _BaudRateGen,@object
  58                 	.global _BaudRateGen
  59                 	.align 2
  60 000e 00 00 00 00 	_BaudRateGen:.space 4
  61                 	.section .bss,bss
  62                 	.type _dummy_encapsulated_cmd_response,@object
  63                 	.global _dummy_encapsulated_cmd_response
  64 0092 00 00 00 00 	_dummy_encapsulated_cmd_response:.space 8
  64      00 00 00 00 
  65                 	.section .text,code
  66              	.align 2
  67              	.global _USBCheckCDCRequest
  68              	.type _USBCheckCDCRequest,@function
  69              	_USBCheckCDCRequest:
  70              	.LFB0:
  71              	.file 1 "lib/lib_pic33e/usb/usb_device_cdc.c"
   1:lib/lib_pic33e/usb/usb_device_cdc.c **** #ifndef NOUSB
   2:lib/lib_pic33e/usb/usb_device_cdc.c **** // DOM-IGNORE-BEGIN
   3:lib/lib_pic33e/usb/usb_device_cdc.c **** /*******************************************************************************
   4:lib/lib_pic33e/usb/usb_device_cdc.c **** Copyright 2015 Microchip Technology Inc. (www.microchip.com)
   5:lib/lib_pic33e/usb/usb_device_cdc.c **** 
   6:lib/lib_pic33e/usb/usb_device_cdc.c **** Licensed under the Apache License, Version 2.0 (the "License");
   7:lib/lib_pic33e/usb/usb_device_cdc.c **** you may not use this file except in compliance with the License.
   8:lib/lib_pic33e/usb/usb_device_cdc.c **** You may obtain a copy of the License at
   9:lib/lib_pic33e/usb/usb_device_cdc.c **** 
  10:lib/lib_pic33e/usb/usb_device_cdc.c ****     http://www.apache.org/licenses/LICENSE-2.0
  11:lib/lib_pic33e/usb/usb_device_cdc.c **** 
  12:lib/lib_pic33e/usb/usb_device_cdc.c **** Unless required by applicable law or agreed to in writing, software
  13:lib/lib_pic33e/usb/usb_device_cdc.c **** distributed under the License is distributed on an "AS IS" BASIS,
  14:lib/lib_pic33e/usb/usb_device_cdc.c **** WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  15:lib/lib_pic33e/usb/usb_device_cdc.c **** See the License for the specific language governing permissions and
MPLAB XC16 ASSEMBLY Listing:   			page 3


  16:lib/lib_pic33e/usb/usb_device_cdc.c **** limitations under the License.
  17:lib/lib_pic33e/usb/usb_device_cdc.c **** 
  18:lib/lib_pic33e/usb/usb_device_cdc.c **** To request to license the code under the MLA license (www.microchip.com/mla_license), 
  19:lib/lib_pic33e/usb/usb_device_cdc.c **** please contact mla_licensing@microchip.com
  20:lib/lib_pic33e/usb/usb_device_cdc.c **** *******************************************************************************/
  21:lib/lib_pic33e/usb/usb_device_cdc.c **** //DOM-IGNORE-END
  22:lib/lib_pic33e/usb/usb_device_cdc.c **** 
  23:lib/lib_pic33e/usb/usb_device_cdc.c **** /********************************************************************
  24:lib/lib_pic33e/usb/usb_device_cdc.c ****  Change History:
  25:lib/lib_pic33e/usb/usb_device_cdc.c ****   Rev    Description
  26:lib/lib_pic33e/usb/usb_device_cdc.c ****   ----   -----------
  27:lib/lib_pic33e/usb/usb_device_cdc.c ****   2.3    Deprecated the mUSBUSARTIsTxTrfReady() macro.  It is 
  28:lib/lib_pic33e/usb/usb_device_cdc.c ****          replaced by the USBUSARTIsTxTrfReady() function.
  29:lib/lib_pic33e/usb/usb_device_cdc.c **** 
  30:lib/lib_pic33e/usb/usb_device_cdc.c ****   2.6    Minor definition changes
  31:lib/lib_pic33e/usb/usb_device_cdc.c **** 
  32:lib/lib_pic33e/usb/usb_device_cdc.c ****   2.6a   No Changes
  33:lib/lib_pic33e/usb/usb_device_cdc.c **** 
  34:lib/lib_pic33e/usb/usb_device_cdc.c ****   2.7    Fixed error in the part support list of the variables section
  35:lib/lib_pic33e/usb/usb_device_cdc.c ****          where the address of the CDC variables are defined.  The 
  36:lib/lib_pic33e/usb/usb_device_cdc.c ****          PIC18F2553 was incorrectly named PIC18F2453 and the PIC18F4558
  37:lib/lib_pic33e/usb/usb_device_cdc.c ****          was incorrectly named PIC18F4458.
  38:lib/lib_pic33e/usb/usb_device_cdc.c **** 
  39:lib/lib_pic33e/usb/usb_device_cdc.c ****          http://www.microchip.com/forums/fb.aspx?m=487397
  40:lib/lib_pic33e/usb/usb_device_cdc.c **** 
  41:lib/lib_pic33e/usb/usb_device_cdc.c ****   2.8    Minor change to CDCInitEP() to enhance ruggedness in
  42:lib/lib_pic33e/usb/usb_device_cdc.c ****          multi0-threaded usage scenarios.
  43:lib/lib_pic33e/usb/usb_device_cdc.c ****   
  44:lib/lib_pic33e/usb/usb_device_cdc.c ****   2.9b   Updated to implement optional support for DTS reporting.
  45:lib/lib_pic33e/usb/usb_device_cdc.c **** 
  46:lib/lib_pic33e/usb/usb_device_cdc.c **** ********************************************************************/
  47:lib/lib_pic33e/usb/usb_device_cdc.c **** 
  48:lib/lib_pic33e/usb/usb_device_cdc.c **** /** I N C L U D E S **********************************************************/
  49:lib/lib_pic33e/usb/usb_device_cdc.c **** #include "system.h"
  50:lib/lib_pic33e/usb/usb_device_cdc.c **** #include "usb.h"
  51:lib/lib_pic33e/usb/usb_device_cdc.c **** #include "usb_device_cdc.h"
  52:lib/lib_pic33e/usb/usb_device_cdc.c **** 
  53:lib/lib_pic33e/usb/usb_device_cdc.c **** #ifdef USB_USE_CDC
  54:lib/lib_pic33e/usb/usb_device_cdc.c **** 
  55:lib/lib_pic33e/usb/usb_device_cdc.c **** #ifndef FIXED_ADDRESS_MEMORY
  56:lib/lib_pic33e/usb/usb_device_cdc.c ****     #define IN_DATA_BUFFER_ADDRESS_TAG
  57:lib/lib_pic33e/usb/usb_device_cdc.c ****     #define OUT_DATA_BUFFER_ADDRESS_TAG
  58:lib/lib_pic33e/usb/usb_device_cdc.c ****     #define CONTROL_BUFFER_ADDRESS_TAG
  59:lib/lib_pic33e/usb/usb_device_cdc.c **** #endif
  60:lib/lib_pic33e/usb/usb_device_cdc.c **** 
  61:lib/lib_pic33e/usb/usb_device_cdc.c **** #if !defined(IN_DATA_BUFFER_ADDRESS_TAG) || !defined(OUT_DATA_BUFFER_ADDRESS_TAG) || !defined(CONTR
  62:lib/lib_pic33e/usb/usb_device_cdc.c ****     #error "One of the fixed memory address definitions is not defined.  Please define the required
  63:lib/lib_pic33e/usb/usb_device_cdc.c **** #endif
  64:lib/lib_pic33e/usb/usb_device_cdc.c **** 
  65:lib/lib_pic33e/usb/usb_device_cdc.c **** /** V A R I A B L E S ********************************************************/
  66:lib/lib_pic33e/usb/usb_device_cdc.c **** volatile unsigned char cdc_data_tx[CDC_DATA_IN_EP_SIZE] IN_DATA_BUFFER_ADDRESS_TAG;
  67:lib/lib_pic33e/usb/usb_device_cdc.c **** volatile unsigned char cdc_data_rx[CDC_DATA_OUT_EP_SIZE] OUT_DATA_BUFFER_ADDRESS_TAG;
  68:lib/lib_pic33e/usb/usb_device_cdc.c **** 
  69:lib/lib_pic33e/usb/usb_device_cdc.c **** typedef union
  70:lib/lib_pic33e/usb/usb_device_cdc.c **** {
  71:lib/lib_pic33e/usb/usb_device_cdc.c ****     LINE_CODING lineCoding;
  72:lib/lib_pic33e/usb/usb_device_cdc.c ****     CDC_NOTICE cdcNotice;
MPLAB XC16 ASSEMBLY Listing:   			page 4


  73:lib/lib_pic33e/usb/usb_device_cdc.c **** } CONTROL_BUFFER;
  74:lib/lib_pic33e/usb/usb_device_cdc.c **** 
  75:lib/lib_pic33e/usb/usb_device_cdc.c **** //static CONTROL_BUFFER controlBuffer CONTROL_BUFFER_ADDRESS_TAG;
  76:lib/lib_pic33e/usb/usb_device_cdc.c **** 
  77:lib/lib_pic33e/usb/usb_device_cdc.c **** LINE_CODING line_coding;    // Buffer to store line coding information
  78:lib/lib_pic33e/usb/usb_device_cdc.c **** CDC_NOTICE cdc_notice;
  79:lib/lib_pic33e/usb/usb_device_cdc.c **** 
  80:lib/lib_pic33e/usb/usb_device_cdc.c **** #if defined(USB_CDC_SUPPORT_DSR_REPORTING)
  81:lib/lib_pic33e/usb/usb_device_cdc.c ****     SERIAL_STATE_NOTIFICATION SerialStatePacket;
  82:lib/lib_pic33e/usb/usb_device_cdc.c **** #endif
  83:lib/lib_pic33e/usb/usb_device_cdc.c **** 
  84:lib/lib_pic33e/usb/usb_device_cdc.c **** uint8_t cdc_rx_len;            // total rx length
  85:lib/lib_pic33e/usb/usb_device_cdc.c **** uint8_t cdc_trf_state;         // States are defined cdc.h
  86:lib/lib_pic33e/usb/usb_device_cdc.c **** POINTER pCDCSrc;            // Dedicated source pointer
  87:lib/lib_pic33e/usb/usb_device_cdc.c **** POINTER pCDCDst;            // Dedicated destination pointer
  88:lib/lib_pic33e/usb/usb_device_cdc.c **** uint8_t cdc_tx_len;            // total tx length
  89:lib/lib_pic33e/usb/usb_device_cdc.c **** uint8_t cdc_mem_type;          // _ROM, _RAM
  90:lib/lib_pic33e/usb/usb_device_cdc.c **** 
  91:lib/lib_pic33e/usb/usb_device_cdc.c **** USB_HANDLE CDCDataOutHandle;
  92:lib/lib_pic33e/usb/usb_device_cdc.c **** USB_HANDLE CDCDataInHandle;
  93:lib/lib_pic33e/usb/usb_device_cdc.c **** 
  94:lib/lib_pic33e/usb/usb_device_cdc.c **** 
  95:lib/lib_pic33e/usb/usb_device_cdc.c **** CONTROL_SIGNAL_BITMAP control_signal_bitmap;
  96:lib/lib_pic33e/usb/usb_device_cdc.c **** uint32_t BaudRateGen;			// BRG value calculated from baud rate
  97:lib/lib_pic33e/usb/usb_device_cdc.c **** 
  98:lib/lib_pic33e/usb/usb_device_cdc.c **** #if defined(USB_CDC_SUPPORT_DSR_REPORTING)
  99:lib/lib_pic33e/usb/usb_device_cdc.c ****     BM_SERIAL_STATE SerialStateBitmap;
 100:lib/lib_pic33e/usb/usb_device_cdc.c ****     BM_SERIAL_STATE OldSerialStateBitmap;
 101:lib/lib_pic33e/usb/usb_device_cdc.c ****     USB_HANDLE CDCNotificationInHandle;
 102:lib/lib_pic33e/usb/usb_device_cdc.c **** #endif
 103:lib/lib_pic33e/usb/usb_device_cdc.c **** 
 104:lib/lib_pic33e/usb/usb_device_cdc.c **** /**************************************************************************
 105:lib/lib_pic33e/usb/usb_device_cdc.c ****   SEND_ENCAPSULATED_COMMAND and GET_ENCAPSULATED_RESPONSE are required
 106:lib/lib_pic33e/usb/usb_device_cdc.c ****   requests according to the CDC specification.
 107:lib/lib_pic33e/usb/usb_device_cdc.c ****   However, it is not really being used here, therefore a dummy buffer is
 108:lib/lib_pic33e/usb/usb_device_cdc.c ****   used for conformance.
 109:lib/lib_pic33e/usb/usb_device_cdc.c ****  **************************************************************************/
 110:lib/lib_pic33e/usb/usb_device_cdc.c **** #define dummy_length    0x08
 111:lib/lib_pic33e/usb/usb_device_cdc.c **** uint8_t dummy_encapsulated_cmd_response[dummy_length];
 112:lib/lib_pic33e/usb/usb_device_cdc.c **** 
 113:lib/lib_pic33e/usb/usb_device_cdc.c **** #if defined(USB_CDC_SET_LINE_CODING_HANDLER)
 114:lib/lib_pic33e/usb/usb_device_cdc.c **** CTRL_TRF_RETURN USB_CDC_SET_LINE_CODING_HANDLER(CTRL_TRF_PARAMS);
 115:lib/lib_pic33e/usb/usb_device_cdc.c **** #endif
 116:lib/lib_pic33e/usb/usb_device_cdc.c **** 
 117:lib/lib_pic33e/usb/usb_device_cdc.c **** /** P R I V A T E  P R O T O T Y P E S ***************************************/
 118:lib/lib_pic33e/usb/usb_device_cdc.c **** void USBCDCSetLineCoding(void);
 119:lib/lib_pic33e/usb/usb_device_cdc.c **** 
 120:lib/lib_pic33e/usb/usb_device_cdc.c **** /** D E C L A R A T I O N S **************************************************/
 121:lib/lib_pic33e/usb/usb_device_cdc.c **** //#pragma code
 122:lib/lib_pic33e/usb/usb_device_cdc.c **** 
 123:lib/lib_pic33e/usb/usb_device_cdc.c **** /** C L A S S  S P E C I F I C  R E Q ****************************************/
 124:lib/lib_pic33e/usb/usb_device_cdc.c **** /******************************************************************************
 125:lib/lib_pic33e/usb/usb_device_cdc.c ****  	Function:
 126:lib/lib_pic33e/usb/usb_device_cdc.c ****  		void USBCheckCDCRequest(void)
 127:lib/lib_pic33e/usb/usb_device_cdc.c ****  
 128:lib/lib_pic33e/usb/usb_device_cdc.c ****  	Description:
 129:lib/lib_pic33e/usb/usb_device_cdc.c ****  		This routine checks the most recently received SETUP data packet to 
MPLAB XC16 ASSEMBLY Listing:   			page 5


 130:lib/lib_pic33e/usb/usb_device_cdc.c ****  		see if the request is specific to the CDC class.  If the request was
 131:lib/lib_pic33e/usb/usb_device_cdc.c ****  		a CDC specific request, this function will take care of handling the
 132:lib/lib_pic33e/usb/usb_device_cdc.c ****  		request and responding appropriately.
 133:lib/lib_pic33e/usb/usb_device_cdc.c ****  		
 134:lib/lib_pic33e/usb/usb_device_cdc.c ****  	PreCondition:
 135:lib/lib_pic33e/usb/usb_device_cdc.c ****  		This function should only be called after a control transfer SETUP
 136:lib/lib_pic33e/usb/usb_device_cdc.c ****  		packet has arrived from the host.
 137:lib/lib_pic33e/usb/usb_device_cdc.c **** 
 138:lib/lib_pic33e/usb/usb_device_cdc.c **** 	Parameters:
 139:lib/lib_pic33e/usb/usb_device_cdc.c **** 		None
 140:lib/lib_pic33e/usb/usb_device_cdc.c **** 		
 141:lib/lib_pic33e/usb/usb_device_cdc.c **** 	Return Values:
 142:lib/lib_pic33e/usb/usb_device_cdc.c **** 		None
 143:lib/lib_pic33e/usb/usb_device_cdc.c **** 		
 144:lib/lib_pic33e/usb/usb_device_cdc.c **** 	Remarks:
 145:lib/lib_pic33e/usb/usb_device_cdc.c **** 		This function does not change status or do anything if the SETUP packet
 146:lib/lib_pic33e/usb/usb_device_cdc.c **** 		did not contain a CDC class specific request.		 
 147:lib/lib_pic33e/usb/usb_device_cdc.c ****   *****************************************************************************/
 148:lib/lib_pic33e/usb/usb_device_cdc.c **** void USBCheckCDCRequest(void)
 149:lib/lib_pic33e/usb/usb_device_cdc.c **** {
  72              	.loc 1 149 0
  73              	.set ___PA___,1
  74 000000  00 00 FA 	lnk #0
  75              	.LCFI0:
  76 000002  88 9F BE 	mov.d w8,[w15++]
  77              	.LCFI1:
  78 000004  8A 9F BE 	mov.d w10,[w15++]
  79              	.LCFI2:
 150:lib/lib_pic33e/usb/usb_device_cdc.c ****     /*
 151:lib/lib_pic33e/usb/usb_device_cdc.c ****      * If request recipient is not an interface then return
 152:lib/lib_pic33e/usb/usb_device_cdc.c ****      */
 153:lib/lib_pic33e/usb/usb_device_cdc.c ****     if(SetupPkt.Recipient != USB_SETUP_RECIPIENT_INTERFACE_BITFIELD) return;
  80              	.loc 1 153 0
  81 000006  00 00 20 	mov #_SetupPkt,w0
  82 000008  10 40 78 	mov.b [w0],w0
  83 00000a  7F 40 60 	and.b w0,#31,w0
  84 00000c  E1 4F 50 	sub.b w0,#1,[w15]
  85              	.set ___BP___,0
  86 00000e  00 00 3A 	bra nz,.L13
  87              	.L2:
 154:lib/lib_pic33e/usb/usb_device_cdc.c **** 
 155:lib/lib_pic33e/usb/usb_device_cdc.c ****     /*
 156:lib/lib_pic33e/usb/usb_device_cdc.c ****      * If request type is not class-specific then return
 157:lib/lib_pic33e/usb/usb_device_cdc.c ****      */
 158:lib/lib_pic33e/usb/usb_device_cdc.c ****     if(SetupPkt.RequestType != USB_SETUP_TYPE_CLASS_BITFIELD) return;
  88              	.loc 1 158 0
  89 000010  01 00 20 	mov #_SetupPkt,w1
  90 000012  00 C2 B3 	mov.b #32,w0
  91 000014  91 40 78 	mov.b [w1],w1
  92 000016  01 46 B2 	and.b #96,w1
  93 000018  80 CF 50 	sub.b w1,w0,[w15]
  94              	.set ___BP___,0
  95 00001a  00 00 3A 	bra nz,.L14
  96              	.L4:
 159:lib/lib_pic33e/usb/usb_device_cdc.c **** 
 160:lib/lib_pic33e/usb/usb_device_cdc.c ****     /*
 161:lib/lib_pic33e/usb/usb_device_cdc.c ****      * Interface ID must match interface numbers associated with
MPLAB XC16 ASSEMBLY Listing:   			page 6


 162:lib/lib_pic33e/usb/usb_device_cdc.c ****      * CDC class, else return
 163:lib/lib_pic33e/usb/usb_device_cdc.c ****      */
 164:lib/lib_pic33e/usb/usb_device_cdc.c ****     if((SetupPkt.bIntfID != CDC_COMM_INTF_ID)&&
  97              	.loc 1 164 0
  98 00001c  40 00 20 	mov #_SetupPkt+4,w0
  99 00001e  10 40 78 	mov.b [w0],w0
 100 000020  00 04 E0 	cp0.b w0
 101              	.set ___BP___,0
 102 000022  00 00 32 	bra z,.L5
 165:lib/lib_pic33e/usb/usb_device_cdc.c ****        (SetupPkt.bIntfID != CDC_DATA_INTF_ID)) return;
 103              	.loc 1 165 0
 104 000024  40 00 20 	mov #_SetupPkt+4,w0
 105 000026  10 40 78 	mov.b [w0],w0
 106              	.loc 1 164 0
 107 000028  E1 4F 50 	sub.b w0,#1,[w15]
 108              	.set ___BP___,0
 109 00002a  00 00 3A 	bra nz,.L15
 110              	.L5:
 166:lib/lib_pic33e/usb/usb_device_cdc.c ****     
 167:lib/lib_pic33e/usb/usb_device_cdc.c ****     switch(SetupPkt.bRequest)
 111              	.loc 1 167 0
 112 00002c  10 00 20 	mov #_SetupPkt+1,w0
 113 00002e  01 02 20 	mov #32,w1
 114 000030  10 40 78 	mov.b [w0],w0
 115 000032  00 80 FB 	ze w0,w0
 116 000034  81 0F 50 	sub w0,w1,[w15]
 117              	.set ___BP___,0
 118 000036  00 00 32 	bra z,.L9
 119 000038  01 02 20 	mov #32,w1
 120 00003a  81 0F 50 	sub w0,w1,[w15]
 121              	.set ___BP___,0
 122 00003c  00 00 3C 	bra gt,.L12
 123 00003e  00 00 E0 	cp0 w0
 124              	.set ___BP___,0
 125 000040  00 00 32 	bra z,.L7
 126 000042  E1 0F 50 	sub w0,#1,[w15]
 127              	.set ___BP___,0
 128 000044  00 00 32 	bra z,.L8
 168:lib/lib_pic33e/usb/usb_device_cdc.c ****     {
 169:lib/lib_pic33e/usb/usb_device_cdc.c ****         //****** These commands are required ******//
 170:lib/lib_pic33e/usb/usb_device_cdc.c ****         case SEND_ENCAPSULATED_COMMAND:
 171:lib/lib_pic33e/usb/usb_device_cdc.c ****          //send the packet
 172:lib/lib_pic33e/usb/usb_device_cdc.c ****             inPipes[0].pSrc.bRam = (uint8_t*)&dummy_encapsulated_cmd_response;
 173:lib/lib_pic33e/usb/usb_device_cdc.c ****             inPipes[0].wCount.Val = dummy_length;
 174:lib/lib_pic33e/usb/usb_device_cdc.c ****             inPipes[0].info.bits.ctrl_trf_mem = USB_EP0_RAM;
 175:lib/lib_pic33e/usb/usb_device_cdc.c ****             inPipes[0].info.bits.busy = 1;
 176:lib/lib_pic33e/usb/usb_device_cdc.c ****             break;
 177:lib/lib_pic33e/usb/usb_device_cdc.c ****         case GET_ENCAPSULATED_RESPONSE:
 178:lib/lib_pic33e/usb/usb_device_cdc.c ****             // Populate dummy_encapsulated_cmd_response first.
 179:lib/lib_pic33e/usb/usb_device_cdc.c ****             inPipes[0].pSrc.bRam = (uint8_t*)&dummy_encapsulated_cmd_response;
 180:lib/lib_pic33e/usb/usb_device_cdc.c ****             inPipes[0].info.bits.busy = 1;
 181:lib/lib_pic33e/usb/usb_device_cdc.c ****             break;
 182:lib/lib_pic33e/usb/usb_device_cdc.c ****         //****** End of required commands ******//
 183:lib/lib_pic33e/usb/usb_device_cdc.c **** 
 184:lib/lib_pic33e/usb/usb_device_cdc.c ****         #if defined(USB_CDC_SUPPORT_ABSTRACT_CONTROL_MANAGEMENT_CAPABILITIES_D1)
 185:lib/lib_pic33e/usb/usb_device_cdc.c ****         case SET_LINE_CODING:
 186:lib/lib_pic33e/usb/usb_device_cdc.c ****             outPipes[0].wCount.Val = SetupPkt.wLength;
MPLAB XC16 ASSEMBLY Listing:   			page 7


 187:lib/lib_pic33e/usb/usb_device_cdc.c ****             outPipes[0].pDst.bRam = (uint8_t*)LINE_CODING_TARGET;
 188:lib/lib_pic33e/usb/usb_device_cdc.c ****             outPipes[0].pFunc = LINE_CODING_PFUNC;
 189:lib/lib_pic33e/usb/usb_device_cdc.c ****             outPipes[0].info.bits.busy = 1;
 190:lib/lib_pic33e/usb/usb_device_cdc.c ****             break;
 191:lib/lib_pic33e/usb/usb_device_cdc.c ****             
 192:lib/lib_pic33e/usb/usb_device_cdc.c ****         case GET_LINE_CODING:
 193:lib/lib_pic33e/usb/usb_device_cdc.c ****             USBEP0SendRAMPtr(
 194:lib/lib_pic33e/usb/usb_device_cdc.c ****                 (uint8_t*)&line_coding,
 195:lib/lib_pic33e/usb/usb_device_cdc.c ****                 LINE_CODING_LENGTH,
 196:lib/lib_pic33e/usb/usb_device_cdc.c ****                 USB_EP0_INCLUDE_ZERO);
 197:lib/lib_pic33e/usb/usb_device_cdc.c ****             break;
 198:lib/lib_pic33e/usb/usb_device_cdc.c **** 
 199:lib/lib_pic33e/usb/usb_device_cdc.c ****         case SET_CONTROL_LINE_STATE:
 200:lib/lib_pic33e/usb/usb_device_cdc.c ****             control_signal_bitmap._byte = (uint8_t)SetupPkt.wValue;
 201:lib/lib_pic33e/usb/usb_device_cdc.c ****             //------------------------------------------------------------------            
 202:lib/lib_pic33e/usb/usb_device_cdc.c ****             //One way to control the RTS pin is to allow the USB host to decide the value
 203:lib/lib_pic33e/usb/usb_device_cdc.c ****             //that should be output on the RTS pin.  Although RTS and CTS pin functions
 204:lib/lib_pic33e/usb/usb_device_cdc.c ****             //are technically intended for UART hardware based flow control, some legacy
 205:lib/lib_pic33e/usb/usb_device_cdc.c ****             //UART devices use the RTS pin like a "general purpose" output pin 
 206:lib/lib_pic33e/usb/usb_device_cdc.c ****             //from the PC host.  In this usage model, the RTS pin is not related
 207:lib/lib_pic33e/usb/usb_device_cdc.c ****             //to flow control for RX/TX.
 208:lib/lib_pic33e/usb/usb_device_cdc.c ****             //In this scenario, the USB host would want to be able to control the RTS
 209:lib/lib_pic33e/usb/usb_device_cdc.c ****             //pin, and the below line of code should be uncommented.
 210:lib/lib_pic33e/usb/usb_device_cdc.c ****             //However, if the intention is to implement true RTS/CTS flow control
 211:lib/lib_pic33e/usb/usb_device_cdc.c ****             //for the RX/TX pair, then this application firmware should override
 212:lib/lib_pic33e/usb/usb_device_cdc.c ****             //the USB host's setting for RTS, and instead generate a real RTS signal,
 213:lib/lib_pic33e/usb/usb_device_cdc.c ****             //based on the amount of remaining buffer space available for the 
 214:lib/lib_pic33e/usb/usb_device_cdc.c ****             //actual hardware UART of this microcontroller.  In this case, the 
 215:lib/lib_pic33e/usb/usb_device_cdc.c ****             //below code should be left commented out, but instead RTS should be 
 216:lib/lib_pic33e/usb/usb_device_cdc.c ****             //controlled in the application firmware responsible for operating the 
 217:lib/lib_pic33e/usb/usb_device_cdc.c ****             //hardware UART of this microcontroller.
 218:lib/lib_pic33e/usb/usb_device_cdc.c ****             //---------            
 219:lib/lib_pic33e/usb/usb_device_cdc.c ****             //CONFIGURE_RTS(control_signal_bitmap.CARRIER_CONTROL);  
 220:lib/lib_pic33e/usb/usb_device_cdc.c ****             //------------------------------------------------------------------            
 221:lib/lib_pic33e/usb/usb_device_cdc.c ****             
 222:lib/lib_pic33e/usb/usb_device_cdc.c ****             #if defined(USB_CDC_SUPPORT_DTR_SIGNALING)
 223:lib/lib_pic33e/usb/usb_device_cdc.c ****                 if(control_signal_bitmap.DTE_PRESENT == 1)
 224:lib/lib_pic33e/usb/usb_device_cdc.c ****                 {
 225:lib/lib_pic33e/usb/usb_device_cdc.c ****                     UART_DTR = USB_CDC_DTR_ACTIVE_LEVEL;
 226:lib/lib_pic33e/usb/usb_device_cdc.c ****                 }
 227:lib/lib_pic33e/usb/usb_device_cdc.c ****                 else
 228:lib/lib_pic33e/usb/usb_device_cdc.c ****                 {
 229:lib/lib_pic33e/usb/usb_device_cdc.c ****                     UART_DTR = (USB_CDC_DTR_ACTIVE_LEVEL ^ 1);
 230:lib/lib_pic33e/usb/usb_device_cdc.c ****                 }        
 231:lib/lib_pic33e/usb/usb_device_cdc.c ****             #endif
 232:lib/lib_pic33e/usb/usb_device_cdc.c ****             inPipes[0].info.bits.busy = 1;
 233:lib/lib_pic33e/usb/usb_device_cdc.c ****             break;
 234:lib/lib_pic33e/usb/usb_device_cdc.c ****         #endif
 235:lib/lib_pic33e/usb/usb_device_cdc.c **** 
 236:lib/lib_pic33e/usb/usb_device_cdc.c ****         #if defined(USB_CDC_SUPPORT_ABSTRACT_CONTROL_MANAGEMENT_CAPABILITIES_D2)
 237:lib/lib_pic33e/usb/usb_device_cdc.c ****         case SEND_BREAK:                        // Optional
 238:lib/lib_pic33e/usb/usb_device_cdc.c ****             inPipes[0].info.bits.busy = 1;
 239:lib/lib_pic33e/usb/usb_device_cdc.c **** 			if (SetupPkt.wValue == 0xFFFF)  //0xFFFF means send break indefinitely until a new SEND_BREAK co
 240:lib/lib_pic33e/usb/usb_device_cdc.c **** 			{
 241:lib/lib_pic33e/usb/usb_device_cdc.c **** 				UART_Tx = 0;       // Prepare to drive TX low (for break signaling)
 242:lib/lib_pic33e/usb/usb_device_cdc.c **** 				UART_TRISTx = 0;   // Make sure TX pin configured as an output
 243:lib/lib_pic33e/usb/usb_device_cdc.c **** 				UART_ENABLE = 0;   // Turn off USART (to relinquish TX pin control)
MPLAB XC16 ASSEMBLY Listing:   			page 8


 244:lib/lib_pic33e/usb/usb_device_cdc.c **** 			}
 245:lib/lib_pic33e/usb/usb_device_cdc.c **** 			else if (SetupPkt.wValue == 0x0000) //0x0000 means stop sending indefinite break 
 246:lib/lib_pic33e/usb/usb_device_cdc.c **** 			{
 247:lib/lib_pic33e/usb/usb_device_cdc.c ****     			UART_ENABLE = 1;   // turn on USART
 248:lib/lib_pic33e/usb/usb_device_cdc.c **** 				UART_TRISTx = 1;   // Make TX pin an input
 249:lib/lib_pic33e/usb/usb_device_cdc.c **** 			}
 250:lib/lib_pic33e/usb/usb_device_cdc.c **** 			else
 251:lib/lib_pic33e/usb/usb_device_cdc.c **** 			{
 252:lib/lib_pic33e/usb/usb_device_cdc.c ****                 //Send break signaling on the pin for (SetupPkt.wValue) milliseconds
 253:lib/lib_pic33e/usb/usb_device_cdc.c ****                 UART_SEND_BREAK();
 254:lib/lib_pic33e/usb/usb_device_cdc.c **** 			}
 255:lib/lib_pic33e/usb/usb_device_cdc.c ****             break;
 256:lib/lib_pic33e/usb/usb_device_cdc.c ****         #endif
 257:lib/lib_pic33e/usb/usb_device_cdc.c ****         default:
 258:lib/lib_pic33e/usb/usb_device_cdc.c ****             break;
 129              	.loc 1 258 0
 130 000046  00 00 37 	bra .L1
 131              	.L12:
 132              	.loc 1 167 0
 133 000048  11 02 20 	mov #33,w1
 134 00004a  81 0F 50 	sub w0,w1,[w15]
 135              	.set ___BP___,0
 136 00004c  00 00 32 	bra z,.L10
 137 00004e  21 02 20 	mov #34,w1
 138 000050  81 0F 50 	sub w0,w1,[w15]
 139              	.set ___BP___,0
 140 000052  00 00 32 	bra z,.L11
 141              	.loc 1 258 0
 142 000054  00 00 37 	bra .L1
 143              	.L7:
 144              	.loc 1 172 0
 145 000056  01 00 20 	mov #_dummy_encapsulated_cmd_response,w1
 146              	.loc 1 173 0
 147 000058  80 00 20 	mov #8,w0
 148              	.loc 1 172 0
 149 00005a  01 00 88 	mov w1,_inPipes
 150              	.loc 1 173 0
 151 00005c  20 00 88 	mov w0,_inPipes+4
 152              	.loc 1 174 0
 153 00005e  10 00 80 	mov _inPipes+2,w0
 154 000060  00 00 A0 	bset w0,#0
 155 000062  10 00 88 	mov w0,_inPipes+2
 156              	.loc 1 175 0
 157 000064  10 00 80 	mov _inPipes+2,w0
 158 000066  00 70 A0 	bset w0,#7
 159 000068  10 00 88 	mov w0,_inPipes+2
 160              	.loc 1 176 0
 161 00006a  00 00 37 	bra .L1
 162              	.L8:
 163              	.loc 1 179 0
 164 00006c  00 00 20 	mov #_dummy_encapsulated_cmd_response,w0
 165 00006e  00 00 88 	mov w0,_inPipes
 166              	.loc 1 180 0
 167 000070  10 00 80 	mov _inPipes+2,w0
 168 000072  00 70 A0 	bset w0,#7
 169 000074  10 00 88 	mov w0,_inPipes+2
 170              	.loc 1 181 0
MPLAB XC16 ASSEMBLY Listing:   			page 9


 171 000076  00 00 37 	bra .L1
 172              	.L9:
 173              	.loc 1 186 0
 174 000078  36 00 20 	mov #_outPipes+3,w6
 175 00007a  45 00 20 	mov #_outPipes+4,w5
 176              	.loc 1 187 0
 177 00007c  04 00 20 	mov #_outPipes,w4
 178 00007e  13 00 20 	mov #_outPipes+1,w3
 179              	.loc 1 188 0
 180 000080  52 00 20 	mov #_outPipes+5,w2
 181 000082  61 00 20 	mov #_outPipes+6,w1
 182              	.loc 1 189 0
 183 000084  20 00 20 	mov #_outPipes+2,w0
 184              	.loc 1 186 0
 185 000086  67 00 20 	mov #_SetupPkt+6,w7
 186 000088  79 00 20 	mov #_SetupPkt+7,w9
 187 00008a  17 84 FB 	ze [w7],w8
 188              	.loc 1 187 0
 189 00008c  07 00 20 	mov #_line_coding,w7
 190              	.loc 1 186 0
 191 00008e  99 84 FB 	ze [w9],w9
 192 000090  FA 0F 20 	mov #255,w10
 193 000092  96 45 78 	mov.b [w6],w11
 194 000094  C8 4C DD 	sl w9,#8,w9
 195 000096  E0 C5 65 	and.b w11,#0,w11
 196 000098  09 04 74 	ior w8,w9,w8
 197              	.loc 1 187 0
 198 00009a  F9 0F 20 	mov #255,w9
 199 00009c  89 84 63 	and w7,w9,w9
 200              	.loc 1 186 0
 201 00009e  0A 05 64 	and w8,w10,w10
 202 0000a0  48 44 DE 	lsr w8,#8,w8
 203 0000a2  0A C5 75 	ior.b w11,w10,w10
 204              	.loc 1 187 0
 205 0000a4  C8 3B DE 	lsr w7,#8,w7
 206              	.loc 1 186 0
 207 0000a6  0A 4B 78 	mov.b w10,[w6]
 208 0000a8  15 43 78 	mov.b [w5],w6
 209 0000aa  60 43 63 	and.b w6,#0,w6
 210 0000ac  08 43 73 	ior.b w6,w8,w6
 211 0000ae  86 4A 78 	mov.b w6,[w5]
 212              	.loc 1 187 0
 213 0000b0  94 42 78 	mov.b [w4],w5
 214 0000b2  E0 C2 62 	and.b w5,#0,w5
 215 0000b4  89 C2 72 	ior.b w5,w9,w5
 216 0000b6  05 4A 78 	mov.b w5,[w4]
 217 0000b8  13 42 78 	mov.b [w3],w4
 218 0000ba  60 42 62 	and.b w4,#0,w4
 219 0000bc  07 42 72 	ior.b w4,w7,w4
 220 0000be  84 49 78 	mov.b w4,[w3]
 221              	.loc 1 188 0
 222 0000c0  92 41 78 	mov.b [w2],w3
 223 0000c2  E0 C1 61 	and.b w3,#0,w3
 224 0000c4  03 49 78 	mov.b w3,[w2]
 225 0000c6  11 41 78 	mov.b [w1],w2
 226 0000c8  60 41 61 	and.b w2,#0,w2
 227 0000ca  82 48 78 	mov.b w2,[w1]
MPLAB XC16 ASSEMBLY Listing:   			page 10


 228              	.loc 1 189 0
 229 0000cc  90 40 78 	mov.b [w0],w1
 230 0000ce  01 74 A0 	bset.b w1,#7
 231 0000d0  01 48 78 	mov.b w1,[w0]
 232              	.loc 1 190 0
 233 0000d2  00 00 37 	bra .L1
 234              	.L10:
 235              	.loc 1 193 0
 236 0000d4  00 00 20 	mov #_line_coding,w0
 237 0000d6  71 00 20 	mov #7,w1
 238 0000d8  00 00 88 	mov w0,_inPipes
 239 0000da  20 00 20 	mov #_inPipes+2,w0
 240 0000dc  21 00 88 	mov w1,_inPipes+4
 241 0000de  11 CC B3 	mov.b #-63,w1
 242 0000e0  01 48 78 	mov.b w1,[w0]
 243              	.loc 1 197 0
 244 0000e2  00 00 37 	bra .L1
 245              	.L11:
 246              	.loc 1 200 0
 247 0000e4  20 00 20 	mov #_SetupPkt+2,w0
 248 0000e6  31 00 20 	mov #_SetupPkt+3,w1
 249 0000e8  10 80 FB 	ze [w0],w0
 250 0000ea  11 81 FB 	ze [w1],w2
 251              	.loc 1 232 0
 252 0000ec  11 00 80 	mov _inPipes+2,w1
 253              	.loc 1 200 0
 254 0000ee  48 11 DD 	sl w2,#8,w2
 255              	.loc 1 232 0
 256 0000f0  01 70 A0 	bset w1,#7
 257              	.loc 1 200 0
 258 0000f2  02 00 70 	ior w0,w2,w0
 259              	.loc 1 232 0
 260 0000f4  11 00 88 	mov w1,_inPipes+2
 261              	.loc 1 200 0
 262 0000f6  00 40 78 	mov.b w0,w0
 263 0000f8  00 E0 B7 	mov.b WREG,_control_signal_bitmap
 264 0000fa  00 00 37 	bra .L1
 265              	.L13:
 266 0000fc  00 00 37 	bra .L1
 267              	.L14:
 268 0000fe  00 00 37 	bra .L1
 269              	.L15:
 270              	.L1:
 259:lib/lib_pic33e/usb/usb_device_cdc.c ****     }//end switch(SetupPkt.bRequest)
 260:lib/lib_pic33e/usb/usb_device_cdc.c **** 
 261:lib/lib_pic33e/usb/usb_device_cdc.c **** }//end USBCheckCDCRequest
 271              	.loc 1 261 0
 272 000100  00 00 00 	nop 
 273 000102  4F 05 BE 	mov.d [--w15],w10
 274 000104  00 00 00 	nop 
 275 000106  4F 04 BE 	mov.d [--w15],w8
 276 000108  8E 07 78 	mov w14,w15
 277 00010a  4F 07 78 	mov [--w15],w14
 278 00010c  00 40 A9 	bclr CORCON,#2
 279 00010e  00 00 06 	return 
 280              	.set ___PA___,0
 281              	.LFE0:
MPLAB XC16 ASSEMBLY Listing:   			page 11


 282              	.size _USBCheckCDCRequest,.-_USBCheckCDCRequest
 283              	.align 2
 284              	.global _CDCInitEP
 285              	.type _CDCInitEP,@function
 286              	_CDCInitEP:
 287              	.LFB1:
 262:lib/lib_pic33e/usb/usb_device_cdc.c **** 
 263:lib/lib_pic33e/usb/usb_device_cdc.c **** /** U S E R  A P I ***********************************************************/
 264:lib/lib_pic33e/usb/usb_device_cdc.c **** 
 265:lib/lib_pic33e/usb/usb_device_cdc.c **** /**************************************************************************
 266:lib/lib_pic33e/usb/usb_device_cdc.c ****   Function:
 267:lib/lib_pic33e/usb/usb_device_cdc.c ****         void CDCInitEP(void)
 268:lib/lib_pic33e/usb/usb_device_cdc.c ****     
 269:lib/lib_pic33e/usb/usb_device_cdc.c ****   Summary:
 270:lib/lib_pic33e/usb/usb_device_cdc.c ****     This function initializes the CDC function driver. This function should
 271:lib/lib_pic33e/usb/usb_device_cdc.c ****     be called after the SET_CONFIGURATION command (ex: within the context of
 272:lib/lib_pic33e/usb/usb_device_cdc.c ****     the USBCBInitEP() function).
 273:lib/lib_pic33e/usb/usb_device_cdc.c ****   Description:
 274:lib/lib_pic33e/usb/usb_device_cdc.c ****     This function initializes the CDC function driver. This function sets
 275:lib/lib_pic33e/usb/usb_device_cdc.c ****     the default line coding (baud rate, bit parity, number of data bits,
 276:lib/lib_pic33e/usb/usb_device_cdc.c ****     and format). This function also enables the endpoints and prepares for
 277:lib/lib_pic33e/usb/usb_device_cdc.c ****     the first transfer from the host.
 278:lib/lib_pic33e/usb/usb_device_cdc.c ****     
 279:lib/lib_pic33e/usb/usb_device_cdc.c ****     This function should be called after the SET_CONFIGURATION command.
 280:lib/lib_pic33e/usb/usb_device_cdc.c ****     This is most simply done by calling this function from the
 281:lib/lib_pic33e/usb/usb_device_cdc.c ****     USBCBInitEP() function.
 282:lib/lib_pic33e/usb/usb_device_cdc.c ****     
 283:lib/lib_pic33e/usb/usb_device_cdc.c ****     Typical Usage:
 284:lib/lib_pic33e/usb/usb_device_cdc.c ****     <code>
 285:lib/lib_pic33e/usb/usb_device_cdc.c ****         void USBCBInitEP(void)
 286:lib/lib_pic33e/usb/usb_device_cdc.c ****         {
 287:lib/lib_pic33e/usb/usb_device_cdc.c ****             CDCInitEP();
 288:lib/lib_pic33e/usb/usb_device_cdc.c ****         }
 289:lib/lib_pic33e/usb/usb_device_cdc.c ****     </code>
 290:lib/lib_pic33e/usb/usb_device_cdc.c ****   Conditions:
 291:lib/lib_pic33e/usb/usb_device_cdc.c ****     None
 292:lib/lib_pic33e/usb/usb_device_cdc.c ****   Remarks:
 293:lib/lib_pic33e/usb/usb_device_cdc.c ****     None                                                                   
 294:lib/lib_pic33e/usb/usb_device_cdc.c ****   **************************************************************************/
 295:lib/lib_pic33e/usb/usb_device_cdc.c **** void CDCInitEP(void)
 296:lib/lib_pic33e/usb/usb_device_cdc.c **** {
 288              	.loc 1 296 0
 289              	.set ___PA___,1
 290 000110  00 00 FA 	lnk #0
 291              	.LCFI3:
 297:lib/lib_pic33e/usb/usb_device_cdc.c ****     //Abstract line coding information
 298:lib/lib_pic33e/usb/usb_device_cdc.c ****     line_coding.dwDTERate   = 19200;      // baud rate
 299:lib/lib_pic33e/usb/usb_device_cdc.c ****     line_coding.bCharFormat = 0x00;             // 1 stop bit
 300:lib/lib_pic33e/usb/usb_device_cdc.c ****     line_coding.bParityType = 0x00;             // None
 301:lib/lib_pic33e/usb/usb_device_cdc.c ****     line_coding.bDataBits = 0x08;               // 5,6,7,8, or 16
 302:lib/lib_pic33e/usb/usb_device_cdc.c **** 
 303:lib/lib_pic33e/usb/usb_device_cdc.c ****     cdc_rx_len = 0;
 304:lib/lib_pic33e/usb/usb_device_cdc.c ****     
 305:lib/lib_pic33e/usb/usb_device_cdc.c ****     /*
 306:lib/lib_pic33e/usb/usb_device_cdc.c ****      * Do not have to init Cnt of IN pipes here.
 307:lib/lib_pic33e/usb/usb_device_cdc.c ****      * Reason:  Number of BYTEs to send to the host
 308:lib/lib_pic33e/usb/usb_device_cdc.c ****      *          varies from one transaction to
MPLAB XC16 ASSEMBLY Listing:   			page 12


 309:lib/lib_pic33e/usb/usb_device_cdc.c ****      *          another. Cnt should equal the exact
 310:lib/lib_pic33e/usb/usb_device_cdc.c ****      *          number of BYTEs to transmit for
 311:lib/lib_pic33e/usb/usb_device_cdc.c ****      *          a given IN transaction.
 312:lib/lib_pic33e/usb/usb_device_cdc.c ****      *          This number of BYTEs will only
 313:lib/lib_pic33e/usb/usb_device_cdc.c ****      *          be known right before the data is
 314:lib/lib_pic33e/usb/usb_device_cdc.c ****      *          sent.
 315:lib/lib_pic33e/usb/usb_device_cdc.c ****      */
 316:lib/lib_pic33e/usb/usb_device_cdc.c ****     USBEnableEndpoint(CDC_COMM_EP,USB_IN_ENABLED|USB_HANDSHAKE_ENABLED|USB_DISALLOW_SETUP);
 292              	.loc 1 316 0
 293 000112  51 C1 B3 	mov.b #21,w1
 294              	.loc 1 298 0
 295 000114  02 B0 24 	mov #19200,w2
 296 000116  03 00 20 	mov #0,w3
 297              	.loc 1 299 0
 298 000118  40 00 20 	mov #_line_coding+4,w0
 299              	.loc 1 298 0
 300 00011a  02 00 88 	mov w2,_line_coding
 301 00011c  13 00 88 	mov w3,_line_coding+2
 302              	.loc 1 299 0
 303 00011e  80 41 EB 	clr.b w3
 304              	.loc 1 300 0
 305 000120  52 00 20 	mov #_line_coding+5,w2
 306              	.loc 1 299 0
 307 000122  03 48 78 	mov.b w3,[w0]
 308              	.loc 1 300 0
 309 000124  80 41 EB 	clr.b w3
 310              	.loc 1 301 0
 311 000126  60 00 20 	mov #_line_coding+6,w0
 312              	.loc 1 300 0
 313 000128  03 49 78 	mov.b w3,[w2]
 314              	.loc 1 301 0
 315 00012a  82 C0 B3 	mov.b #8,w2
 316              	.loc 1 303 0
 317 00012c  00 60 EF 	clr.b _cdc_rx_len
 318              	.loc 1 301 0
 319 00012e  02 48 78 	mov.b w2,[w0]
 320              	.loc 1 316 0
 321 000130  10 C0 B3 	mov.b #1,w0
 322 000132  00 00 07 	rcall _USBEnableEndpoint
 317:lib/lib_pic33e/usb/usb_device_cdc.c ****     USBEnableEndpoint(CDC_DATA_EP,USB_IN_ENABLED|USB_OUT_ENABLED|USB_HANDSHAKE_ENABLED|USB_DISALLOW
 323              	.loc 1 317 0
 324 000134  D1 C1 B3 	mov.b #29,w1
 325 000136  20 C0 B3 	mov.b #2,w0
 326 000138  00 00 07 	rcall _USBEnableEndpoint
 318:lib/lib_pic33e/usb/usb_device_cdc.c **** 
 319:lib/lib_pic33e/usb/usb_device_cdc.c ****     CDCDataOutHandle = USBRxOnePacket(CDC_DATA_EP,(uint8_t*)&cdc_data_rx,sizeof(cdc_data_rx));
 327              	.loc 1 319 0
 328 00013a  03 C4 B3 	mov.b #64,w3
 329 00013c  02 00 20 	mov #_cdc_data_rx,w2
 330 00013e  80 40 EB 	clr.b w1
 331 000140  20 C0 B3 	mov.b #2,w0
 332 000142  00 00 07 	rcall _USBTransferOnePacket
 320:lib/lib_pic33e/usb/usb_device_cdc.c ****     CDCDataInHandle = NULL;
 333              	.loc 1 320 0
 334 000144  00 20 EF 	clr _CDCDataInHandle
 335              	.loc 1 319 0
 336 000146  00 00 88 	mov w0,_CDCDataOutHandle
MPLAB XC16 ASSEMBLY Listing:   			page 13


 321:lib/lib_pic33e/usb/usb_device_cdc.c **** 
 322:lib/lib_pic33e/usb/usb_device_cdc.c ****     #if defined(USB_CDC_SUPPORT_DSR_REPORTING)
 323:lib/lib_pic33e/usb/usb_device_cdc.c ****       	CDCNotificationInHandle = NULL;
 324:lib/lib_pic33e/usb/usb_device_cdc.c ****         mInitDTSPin();  //Configure DTS as a digital input
 325:lib/lib_pic33e/usb/usb_device_cdc.c ****       	SerialStateBitmap.byte = 0x00;
 326:lib/lib_pic33e/usb/usb_device_cdc.c ****       	OldSerialStateBitmap.byte = !SerialStateBitmap.byte;    //To force firmware to send an initi
 327:lib/lib_pic33e/usb/usb_device_cdc.c ****         //Prepare a SerialState notification element packet (contains info like DSR state)
 328:lib/lib_pic33e/usb/usb_device_cdc.c ****         SerialStatePacket.bmRequestType = 0xA1; //Always 0xA1 for this type of packet.
 329:lib/lib_pic33e/usb/usb_device_cdc.c ****         SerialStatePacket.bNotification = SERIAL_STATE;
 330:lib/lib_pic33e/usb/usb_device_cdc.c ****         SerialStatePacket.wValue = 0x0000;  //Always 0x0000 for this type of packet
 331:lib/lib_pic33e/usb/usb_device_cdc.c ****         SerialStatePacket.wIndex = CDC_COMM_INTF_ID;  //Interface number  
 332:lib/lib_pic33e/usb/usb_device_cdc.c ****         SerialStatePacket.SerialState.byte = 0x00;
 333:lib/lib_pic33e/usb/usb_device_cdc.c ****         SerialStatePacket.Reserved = 0x00;
 334:lib/lib_pic33e/usb/usb_device_cdc.c ****         SerialStatePacket.wLength = 0x02;   //Always 2 bytes for this type of packet    
 335:lib/lib_pic33e/usb/usb_device_cdc.c ****         CDCNotificationHandler();
 336:lib/lib_pic33e/usb/usb_device_cdc.c ****   	#endif
 337:lib/lib_pic33e/usb/usb_device_cdc.c ****   	
 338:lib/lib_pic33e/usb/usb_device_cdc.c ****   	#if defined(USB_CDC_SUPPORT_DTR_SIGNALING)
 339:lib/lib_pic33e/usb/usb_device_cdc.c ****   	    mInitDTRPin();
 340:lib/lib_pic33e/usb/usb_device_cdc.c ****   	#endif
 341:lib/lib_pic33e/usb/usb_device_cdc.c ****   	
 342:lib/lib_pic33e/usb/usb_device_cdc.c ****   	#if defined(USB_CDC_SUPPORT_HARDWARE_FLOW_CONTROL)
 343:lib/lib_pic33e/usb/usb_device_cdc.c ****   	    mInitRTSPin();
 344:lib/lib_pic33e/usb/usb_device_cdc.c ****   	    mInitCTSPin();
 345:lib/lib_pic33e/usb/usb_device_cdc.c ****   	#endif
 346:lib/lib_pic33e/usb/usb_device_cdc.c ****     
 347:lib/lib_pic33e/usb/usb_device_cdc.c ****     cdc_trf_state = CDC_TX_READY;
 337              	.loc 1 347 0
 338 000148  00 60 EF 	clr.b _cdc_trf_state
 348:lib/lib_pic33e/usb/usb_device_cdc.c **** }//end CDCInitEP
 339              	.loc 1 348 0
 340 00014a  8E 07 78 	mov w14,w15
 341 00014c  4F 07 78 	mov [--w15],w14
 342 00014e  00 40 A9 	bclr CORCON,#2
 343 000150  00 00 06 	return 
 344              	.set ___PA___,0
 345              	.LFE1:
 346              	.size _CDCInitEP,.-_CDCInitEP
 347              	.align 2
 348              	.global _USBCDCEventHandler
 349              	.type _USBCDCEventHandler,@function
 350              	_USBCDCEventHandler:
 351              	.LFB2:
 349:lib/lib_pic33e/usb/usb_device_cdc.c **** 
 350:lib/lib_pic33e/usb/usb_device_cdc.c **** 
 351:lib/lib_pic33e/usb/usb_device_cdc.c **** /**************************************************************************
 352:lib/lib_pic33e/usb/usb_device_cdc.c ****   Function: void CDCNotificationHandler(void)
 353:lib/lib_pic33e/usb/usb_device_cdc.c ****   Summary: Checks for changes in DSR status and reports them to the USB host.
 354:lib/lib_pic33e/usb/usb_device_cdc.c ****   Description: Checks for changes in DSR pin state and reports any changes
 355:lib/lib_pic33e/usb/usb_device_cdc.c ****                to the USB host. 
 356:lib/lib_pic33e/usb/usb_device_cdc.c ****   Conditions: CDCInitEP() must have been called previously, prior to calling
 357:lib/lib_pic33e/usb/usb_device_cdc.c ****               CDCNotificationHandler() for the first time.
 358:lib/lib_pic33e/usb/usb_device_cdc.c ****   Remarks:
 359:lib/lib_pic33e/usb/usb_device_cdc.c ****     This function is only implemented and needed when the 
 360:lib/lib_pic33e/usb/usb_device_cdc.c ****     USB_CDC_SUPPORT_DSR_REPORTING option has been enabled.  If the function is
 361:lib/lib_pic33e/usb/usb_device_cdc.c ****     enabled, it should be called periodically to sample the DSR pin and feed
 362:lib/lib_pic33e/usb/usb_device_cdc.c ****     the information to the USB host.  This can be done by calling 
MPLAB XC16 ASSEMBLY Listing:   			page 14


 363:lib/lib_pic33e/usb/usb_device_cdc.c ****     CDCNotificationHandler() by itself, or, by calling CDCTxService() which
 364:lib/lib_pic33e/usb/usb_device_cdc.c ****     also calls CDCNotificationHandler() internally, when appropriate.
 365:lib/lib_pic33e/usb/usb_device_cdc.c ****   **************************************************************************/
 366:lib/lib_pic33e/usb/usb_device_cdc.c **** #if defined(USB_CDC_SUPPORT_DSR_REPORTING)
 367:lib/lib_pic33e/usb/usb_device_cdc.c **** void CDCNotificationHandler(void)
 368:lib/lib_pic33e/usb/usb_device_cdc.c **** {
 369:lib/lib_pic33e/usb/usb_device_cdc.c ****     //Check the DTS I/O pin and if a state change is detected, notify the 
 370:lib/lib_pic33e/usb/usb_device_cdc.c ****     //USB host by sending a serial state notification element packet.
 371:lib/lib_pic33e/usb/usb_device_cdc.c ****     if(UART_DTS == USB_CDC_DSR_ACTIVE_LEVEL) //UART_DTS must be defined to be an I/O pin in the har
 372:lib/lib_pic33e/usb/usb_device_cdc.c ****     {
 373:lib/lib_pic33e/usb/usb_device_cdc.c ****         SerialStateBitmap.bits.DSR = 1;
 374:lib/lib_pic33e/usb/usb_device_cdc.c ****     }  
 375:lib/lib_pic33e/usb/usb_device_cdc.c ****     else
 376:lib/lib_pic33e/usb/usb_device_cdc.c ****     {
 377:lib/lib_pic33e/usb/usb_device_cdc.c ****         SerialStateBitmap.bits.DSR = 0;
 378:lib/lib_pic33e/usb/usb_device_cdc.c ****     }        
 379:lib/lib_pic33e/usb/usb_device_cdc.c ****     
 380:lib/lib_pic33e/usb/usb_device_cdc.c ****     //If the state has changed, and the endpoint is available, send a packet to
 381:lib/lib_pic33e/usb/usb_device_cdc.c ****     //notify the hUSB host of the change.
 382:lib/lib_pic33e/usb/usb_device_cdc.c ****     if((SerialStateBitmap.byte != OldSerialStateBitmap.byte) && (!USBHandleBusy(CDCNotificationInHa
 383:lib/lib_pic33e/usb/usb_device_cdc.c ****     {
 384:lib/lib_pic33e/usb/usb_device_cdc.c ****         //Copy the updated value into the USB packet buffer to send.
 385:lib/lib_pic33e/usb/usb_device_cdc.c ****         SerialStatePacket.SerialState.byte = SerialStateBitmap.byte;
 386:lib/lib_pic33e/usb/usb_device_cdc.c ****         //We don't need to write to the other bytes in the SerialStatePacket USB
 387:lib/lib_pic33e/usb/usb_device_cdc.c ****         //buffer, since they don't change and will always be the same as our
 388:lib/lib_pic33e/usb/usb_device_cdc.c ****         //initialized value.
 389:lib/lib_pic33e/usb/usb_device_cdc.c **** 
 390:lib/lib_pic33e/usb/usb_device_cdc.c ****         //Send the packet over USB to the host.
 391:lib/lib_pic33e/usb/usb_device_cdc.c ****         CDCNotificationInHandle = USBTransferOnePacket(CDC_COMM_EP, IN_TO_HOST, (uint8_t*)&SerialSt
 392:lib/lib_pic33e/usb/usb_device_cdc.c ****         
 393:lib/lib_pic33e/usb/usb_device_cdc.c ****         //Save the old value, so we can detect changes later.
 394:lib/lib_pic33e/usb/usb_device_cdc.c ****         OldSerialStateBitmap.byte = SerialStateBitmap.byte;
 395:lib/lib_pic33e/usb/usb_device_cdc.c ****     }    
 396:lib/lib_pic33e/usb/usb_device_cdc.c **** }//void CDCNotificationHandler(void)    
 397:lib/lib_pic33e/usb/usb_device_cdc.c **** #else
 398:lib/lib_pic33e/usb/usb_device_cdc.c ****     #define CDCNotificationHandler() {}
 399:lib/lib_pic33e/usb/usb_device_cdc.c **** #endif
 400:lib/lib_pic33e/usb/usb_device_cdc.c **** 
 401:lib/lib_pic33e/usb/usb_device_cdc.c **** 
 402:lib/lib_pic33e/usb/usb_device_cdc.c **** /**********************************************************************************
 403:lib/lib_pic33e/usb/usb_device_cdc.c ****   Function:
 404:lib/lib_pic33e/usb/usb_device_cdc.c ****     bool USBCDCEventHandler(USB_EVENT event, void *pdata, uint16_t size)
 405:lib/lib_pic33e/usb/usb_device_cdc.c ****     
 406:lib/lib_pic33e/usb/usb_device_cdc.c ****   Summary:
 407:lib/lib_pic33e/usb/usb_device_cdc.c ****     Handles events from the USB stack, which may have an effect on the CDC 
 408:lib/lib_pic33e/usb/usb_device_cdc.c ****     endpoint(s).
 409:lib/lib_pic33e/usb/usb_device_cdc.c **** 
 410:lib/lib_pic33e/usb/usb_device_cdc.c ****   Description:
 411:lib/lib_pic33e/usb/usb_device_cdc.c ****     Handles events from the USB stack.  This function should be called when 
 412:lib/lib_pic33e/usb/usb_device_cdc.c ****     there is a USB event that needs to be processed by the CDC driver.
 413:lib/lib_pic33e/usb/usb_device_cdc.c ****     
 414:lib/lib_pic33e/usb/usb_device_cdc.c ****   Conditions:
 415:lib/lib_pic33e/usb/usb_device_cdc.c ****     Value of input argument 'len' should be smaller than the maximum
 416:lib/lib_pic33e/usb/usb_device_cdc.c ****     endpoint size responsible for receiving bulk data from USB host for CDC
 417:lib/lib_pic33e/usb/usb_device_cdc.c ****     class. Input argument 'buffer' should point to a buffer area that is
 418:lib/lib_pic33e/usb/usb_device_cdc.c ****     bigger or equal to the size specified by 'len'.
 419:lib/lib_pic33e/usb/usb_device_cdc.c ****   Input:
MPLAB XC16 ASSEMBLY Listing:   			page 15


 420:lib/lib_pic33e/usb/usb_device_cdc.c ****     event - the type of event that occurred
 421:lib/lib_pic33e/usb/usb_device_cdc.c ****     pdata - pointer to the data that caused the event
 422:lib/lib_pic33e/usb/usb_device_cdc.c ****     size - the size of the data that is pointed to by pdata
 423:lib/lib_pic33e/usb/usb_device_cdc.c ****                                                                                    
 424:lib/lib_pic33e/usb/usb_device_cdc.c ****   **********************************************************************************/
 425:lib/lib_pic33e/usb/usb_device_cdc.c **** bool USBCDCEventHandler(USB_EVENT event, void *pdata, uint16_t size)
 426:lib/lib_pic33e/usb/usb_device_cdc.c **** {
 352              	.loc 1 426 0
 353              	.set ___PA___,1
 354 000152  06 00 FA 	lnk #6
 355              	.LCFI4:
 356              	.loc 1 426 0
 357 000154  00 0F 78 	mov w0,[w14]
 358 000156  11 07 98 	mov w1,[w14+2]
 359 000158  22 07 98 	mov w2,[w14+4]
 427:lib/lib_pic33e/usb/usb_device_cdc.c ****     switch( (uint16_t)event )
 360              	.loc 1 427 0
 361 00015a  1E 00 78 	mov [w14],w0
 362 00015c  E5 0F 50 	sub w0,#5,[w15]
 363              	.set ___BP___,0
 364 00015e  00 00 3A 	bra nz,.L23
 365              	.L19:
 428:lib/lib_pic33e/usb/usb_device_cdc.c ****     {  
 429:lib/lib_pic33e/usb/usb_device_cdc.c ****         case EVENT_TRANSFER_TERMINATED:
 430:lib/lib_pic33e/usb/usb_device_cdc.c ****             if(pdata == CDCDataOutHandle)
 366              	.loc 1 430 0
 367 000160  00 00 80 	mov _CDCDataOutHandle,w0
 368 000162  9E 00 90 	mov [w14+2],w1
 369 000164  80 8F 50 	sub w1,w0,[w15]
 370              	.set ___BP___,0
 371 000166  00 00 3A 	bra nz,.L20
 431:lib/lib_pic33e/usb/usb_device_cdc.c ****             {
 432:lib/lib_pic33e/usb/usb_device_cdc.c ****                 CDCDataOutHandle = USBRxOnePacket(CDC_DATA_EP,(uint8_t*)&cdc_data_rx,sizeof(cdc_dat
 372              	.loc 1 432 0
 373 000168  03 C4 B3 	mov.b #64,w3
 374 00016a  02 00 20 	mov #_cdc_data_rx,w2
 375 00016c  80 40 EB 	clr.b w1
 376 00016e  20 C0 B3 	mov.b #2,w0
 377 000170  00 00 07 	rcall _USBTransferOnePacket
 378 000172  00 00 88 	mov w0,_CDCDataOutHandle
 379              	.L20:
 433:lib/lib_pic33e/usb/usb_device_cdc.c ****             }
 434:lib/lib_pic33e/usb/usb_device_cdc.c ****             if(pdata == CDCDataInHandle)
 380              	.loc 1 434 0
 381 000174  00 00 80 	mov _CDCDataInHandle,w0
 382 000176  9E 00 90 	mov [w14+2],w1
 383 000178  80 8F 50 	sub w1,w0,[w15]
 384              	.set ___BP___,0
 385 00017a  00 00 3A 	bra nz,.L21
 435:lib/lib_pic33e/usb/usb_device_cdc.c ****             {
 436:lib/lib_pic33e/usb/usb_device_cdc.c ****                 //flush all of the data in the CDC buffer
 437:lib/lib_pic33e/usb/usb_device_cdc.c ****                 cdc_trf_state = CDC_TX_READY;
 386              	.loc 1 437 0
 387 00017c  00 60 EF 	clr.b _cdc_trf_state
 438:lib/lib_pic33e/usb/usb_device_cdc.c ****                 cdc_tx_len = 0;
 388              	.loc 1 438 0
 389 00017e  00 60 EF 	clr.b _cdc_tx_len
MPLAB XC16 ASSEMBLY Listing:   			page 16


 390              	.L21:
 439:lib/lib_pic33e/usb/usb_device_cdc.c ****             }
 440:lib/lib_pic33e/usb/usb_device_cdc.c ****             break;
 441:lib/lib_pic33e/usb/usb_device_cdc.c ****         default:
 442:lib/lib_pic33e/usb/usb_device_cdc.c ****             return false;
 443:lib/lib_pic33e/usb/usb_device_cdc.c ****     }      
 444:lib/lib_pic33e/usb/usb_device_cdc.c ****     return true;
 391              	.loc 1 444 0
 392 000180  10 C0 B3 	mov.b #1,w0
 393 000182  00 00 37 	bra .L22
 394              	.L23:
 395              	.loc 1 442 0
 396 000184  00 40 EB 	clr.b w0
 397              	.L22:
 445:lib/lib_pic33e/usb/usb_device_cdc.c **** }
 398              	.loc 1 445 0
 399 000186  8E 07 78 	mov w14,w15
 400 000188  4F 07 78 	mov [--w15],w14
 401 00018a  00 40 A9 	bclr CORCON,#2
 402 00018c  00 00 06 	return 
 403              	.set ___PA___,0
 404              	.LFE2:
 405              	.size _USBCDCEventHandler,.-_USBCDCEventHandler
 406              	.align 2
 407              	.global _getsUSBUSART
 408              	.type _getsUSBUSART,@function
 409              	_getsUSBUSART:
 410              	.LFB3:
 446:lib/lib_pic33e/usb/usb_device_cdc.c **** 
 447:lib/lib_pic33e/usb/usb_device_cdc.c **** /**********************************************************************************
 448:lib/lib_pic33e/usb/usb_device_cdc.c ****   Function:
 449:lib/lib_pic33e/usb/usb_device_cdc.c ****         uint8_t getsUSBUSART(char *buffer, uint8_t len)
 450:lib/lib_pic33e/usb/usb_device_cdc.c ****     
 451:lib/lib_pic33e/usb/usb_device_cdc.c ****   Summary:
 452:lib/lib_pic33e/usb/usb_device_cdc.c ****     getsUSBUSART copies a string of BYTEs received through USB CDC Bulk OUT
 453:lib/lib_pic33e/usb/usb_device_cdc.c ****     endpoint to a user's specified location. It is a non-blocking function.
 454:lib/lib_pic33e/usb/usb_device_cdc.c ****     It does not wait for data if there is no data available. Instead it
 455:lib/lib_pic33e/usb/usb_device_cdc.c ****     returns '0' to notify the caller that there is no data available.
 456:lib/lib_pic33e/usb/usb_device_cdc.c **** 
 457:lib/lib_pic33e/usb/usb_device_cdc.c ****   Description:
 458:lib/lib_pic33e/usb/usb_device_cdc.c ****     getsUSBUSART copies a string of BYTEs received through USB CDC Bulk OUT
 459:lib/lib_pic33e/usb/usb_device_cdc.c ****     endpoint to a user's specified location. It is a non-blocking function.
 460:lib/lib_pic33e/usb/usb_device_cdc.c ****     It does not wait for data if there is no data available. Instead it
 461:lib/lib_pic33e/usb/usb_device_cdc.c ****     returns '0' to notify the caller that there is no data available.
 462:lib/lib_pic33e/usb/usb_device_cdc.c ****     
 463:lib/lib_pic33e/usb/usb_device_cdc.c ****     Typical Usage:
 464:lib/lib_pic33e/usb/usb_device_cdc.c ****     <code>
 465:lib/lib_pic33e/usb/usb_device_cdc.c ****         uint8_t numBytes;
 466:lib/lib_pic33e/usb/usb_device_cdc.c ****         uint8_t buffer[64]
 467:lib/lib_pic33e/usb/usb_device_cdc.c ****     
 468:lib/lib_pic33e/usb/usb_device_cdc.c ****         numBytes = getsUSBUSART(buffer,sizeof(buffer)); //until the buffer is free.
 469:lib/lib_pic33e/usb/usb_device_cdc.c ****         if(numBytes \> 0)
 470:lib/lib_pic33e/usb/usb_device_cdc.c ****         {
 471:lib/lib_pic33e/usb/usb_device_cdc.c ****             //we received numBytes bytes of data and they are copied into
 472:lib/lib_pic33e/usb/usb_device_cdc.c ****             //  the "buffer" variable.  We can do something with the data
 473:lib/lib_pic33e/usb/usb_device_cdc.c ****             //  here.
 474:lib/lib_pic33e/usb/usb_device_cdc.c ****         }
MPLAB XC16 ASSEMBLY Listing:   			page 17


 475:lib/lib_pic33e/usb/usb_device_cdc.c ****     </code>
 476:lib/lib_pic33e/usb/usb_device_cdc.c ****   Conditions:
 477:lib/lib_pic33e/usb/usb_device_cdc.c ****     Value of input argument 'len' should be smaller than the maximum
 478:lib/lib_pic33e/usb/usb_device_cdc.c ****     endpoint size responsible for receiving bulk data from USB host for CDC
 479:lib/lib_pic33e/usb/usb_device_cdc.c ****     class. Input argument 'buffer' should point to a buffer area that is
 480:lib/lib_pic33e/usb/usb_device_cdc.c ****     bigger or equal to the size specified by 'len'.
 481:lib/lib_pic33e/usb/usb_device_cdc.c ****   Input:
 482:lib/lib_pic33e/usb/usb_device_cdc.c ****     buffer -  Pointer to where received BYTEs are to be stored
 483:lib/lib_pic33e/usb/usb_device_cdc.c ****     len -     The number of BYTEs expected.
 484:lib/lib_pic33e/usb/usb_device_cdc.c ****                                                                                    
 485:lib/lib_pic33e/usb/usb_device_cdc.c ****   **********************************************************************************/
 486:lib/lib_pic33e/usb/usb_device_cdc.c **** uint8_t getsUSBUSART(uint8_t *buffer, uint8_t len)
 487:lib/lib_pic33e/usb/usb_device_cdc.c **** {
 411              	.loc 1 487 0
 412              	.set ___PA___,1
 413 00018e  04 00 FA 	lnk #4
 414              	.LCFI5:
 415              	.loc 1 487 0
 416 000190  00 0F 78 	mov w0,[w14]
 417 000192  21 47 98 	mov.b w1,[w14+2]
 488:lib/lib_pic33e/usb/usb_device_cdc.c ****     cdc_rx_len = 0;
 418              	.loc 1 488 0
 419 000194  00 60 EF 	clr.b _cdc_rx_len
 489:lib/lib_pic33e/usb/usb_device_cdc.c ****     
 490:lib/lib_pic33e/usb/usb_device_cdc.c ****     if(!USBHandleBusy(CDCDataOutHandle))
 420              	.loc 1 490 0
 421 000196  00 00 80 	mov _CDCDataOutHandle,w0
 422 000198  00 00 E0 	cp0 w0
 423              	.set ___BP___,0
 424 00019a  00 00 32 	bra z,.L25
 425 00019c  00 00 80 	mov _CDCDataOutHandle,w0
 426 00019e  80 00 78 	mov w0,w1
 427 0001a0  00 C8 B3 	mov.b #-128,w0
 428 0001a2  91 40 78 	mov.b [w1],w1
 429 0001a4  00 C0 60 	and.b w1,w0,w0
 430 0001a6  00 04 E0 	cp0.b w0
 431              	.set ___BP___,0
 432 0001a8  00 00 3A 	bra nz,.L26
 433              	.L25:
 491:lib/lib_pic33e/usb/usb_device_cdc.c ****     {
 492:lib/lib_pic33e/usb/usb_device_cdc.c ****         /*
 493:lib/lib_pic33e/usb/usb_device_cdc.c ****          * Adjust the expected number of BYTEs to equal
 494:lib/lib_pic33e/usb/usb_device_cdc.c ****          * the actual number of BYTEs received.
 495:lib/lib_pic33e/usb/usb_device_cdc.c ****          */
 496:lib/lib_pic33e/usb/usb_device_cdc.c ****         if(len > USBHandleGetLength(CDCDataOutHandle))
 434              	.loc 1 496 0
 435 0001aa  00 00 80 	mov _CDCDataOutHandle,w0
 436 0001ac  AE 40 90 	mov.b [w14+2],w1
 437 0001ae  20 41 90 	mov.b [w0+2],w2
 438 0001b0  81 80 FB 	ze w1,w1
 439 0001b2  02 81 FB 	ze w2,w2
 440 0001b4  30 40 90 	mov.b [w0+3],w0
 441 0001b6  00 80 FB 	ze w0,w0
 442 0001b8  63 00 60 	and w0,#3,w0
 443 0001ba  48 00 DD 	sl w0,#8,w0
 444 0001bc  00 00 71 	ior w2,w0,w0
 445 0001be  80 8F 50 	sub w1,w0,[w15]
MPLAB XC16 ASSEMBLY Listing:   			page 18


 446              	.set ___BP___,0
 447 0001c0  00 00 34 	bra le,.L27
 497:lib/lib_pic33e/usb/usb_device_cdc.c ****             len = USBHandleGetLength(CDCDataOutHandle);
 448              	.loc 1 497 0
 449 0001c2  00 00 80 	mov _CDCDataOutHandle,w0
 450 0001c4  20 41 90 	mov.b [w0+2],w2
 451 0001c6  B0 40 90 	mov.b [w0+3],w1
 452 0001c8  02 80 FB 	ze w2,w0
 453 0001ca  81 80 FB 	ze w1,w1
 454 0001cc  E3 80 60 	and w1,#3,w1
 455 0001ce  C8 08 DD 	sl w1,#8,w1
 456 0001d0  01 00 70 	ior w0,w1,w0
 457 0001d2  20 47 98 	mov.b w0,[w14+2]
 458              	.L27:
 498:lib/lib_pic33e/usb/usb_device_cdc.c ****         
 499:lib/lib_pic33e/usb/usb_device_cdc.c ****         /*
 500:lib/lib_pic33e/usb/usb_device_cdc.c ****          * Copy data from dual-ram buffer to user's buffer
 501:lib/lib_pic33e/usb/usb_device_cdc.c ****          */
 502:lib/lib_pic33e/usb/usb_device_cdc.c ****         for(cdc_rx_len = 0; cdc_rx_len < len; cdc_rx_len++)
 459              	.loc 1 502 0
 460 0001d4  00 60 EF 	clr.b _cdc_rx_len
 461 0001d6  00 00 37 	bra .L28
 462              	.L29:
 503:lib/lib_pic33e/usb/usb_device_cdc.c ****             buffer[cdc_rx_len] = cdc_data_rx[cdc_rx_len];
 463              	.loc 1 503 0
 464 0001d8  00 C0 BF 	mov.b _cdc_rx_len,WREG
 465 0001da  01 00 20 	mov #_cdc_data_rx,w1
 466 0001dc  00 80 FB 	ze w0,w0
 467 0001de  80 80 40 	add w1,w0,w1
 468 0001e0  00 C0 BF 	mov.b _cdc_rx_len,WREG
 469 0001e2  91 40 78 	mov.b [w1],w1
 470 0001e4  00 80 FB 	ze w0,w0
 471 0001e6  1E 00 40 	add w0,[w14],w0
 472 0001e8  01 48 78 	mov.b w1,[w0]
 473              	.loc 1 502 0
 474 0001ea  00 C0 BF 	mov.b _cdc_rx_len,WREG
 475 0001ec  00 40 E8 	inc.b w0,w0
 476 0001ee  00 E0 B7 	mov.b WREG,_cdc_rx_len
 477              	.L28:
 478 0001f0  01 00 20 	mov #_cdc_rx_len,w1
 479 0001f2  00 00 00 	nop
 480 0001f4  91 40 78 	mov.b [w1],w1
 481 0001f6  2E 40 90 	mov.b [w14+2],w0
 482 0001f8  80 CF 50 	sub.b w1,w0,[w15]
 483              	.set ___BP___,0
 484 0001fa  00 00 39 	bra ltu,.L29
 504:lib/lib_pic33e/usb/usb_device_cdc.c **** 
 505:lib/lib_pic33e/usb/usb_device_cdc.c ****         /*
 506:lib/lib_pic33e/usb/usb_device_cdc.c ****          * Prepare dual-ram buffer for next OUT transaction
 507:lib/lib_pic33e/usb/usb_device_cdc.c ****          */
 508:lib/lib_pic33e/usb/usb_device_cdc.c **** 
 509:lib/lib_pic33e/usb/usb_device_cdc.c ****         CDCDataOutHandle = USBRxOnePacket(CDC_DATA_EP,(uint8_t*)&cdc_data_rx,sizeof(cdc_data_rx));
 485              	.loc 1 509 0
 486 0001fc  03 C4 B3 	mov.b #64,w3
 487 0001fe  02 00 20 	mov #_cdc_data_rx,w2
 488 000200  80 40 EB 	clr.b w1
 489 000202  20 C0 B3 	mov.b #2,w0
MPLAB XC16 ASSEMBLY Listing:   			page 19


 490 000204  00 00 07 	rcall _USBTransferOnePacket
 491 000206  00 00 88 	mov w0,_CDCDataOutHandle
 492              	.L26:
 510:lib/lib_pic33e/usb/usb_device_cdc.c **** 
 511:lib/lib_pic33e/usb/usb_device_cdc.c ****     }//end if
 512:lib/lib_pic33e/usb/usb_device_cdc.c ****     
 513:lib/lib_pic33e/usb/usb_device_cdc.c ****     return cdc_rx_len;
 493              	.loc 1 513 0
 494 000208  00 C0 BF 	mov.b _cdc_rx_len,WREG
 514:lib/lib_pic33e/usb/usb_device_cdc.c ****     
 515:lib/lib_pic33e/usb/usb_device_cdc.c **** }//end getsUSBUSART
 495              	.loc 1 515 0
 496 00020a  8E 07 78 	mov w14,w15
 497 00020c  4F 07 78 	mov [--w15],w14
 498 00020e  00 40 A9 	bclr CORCON,#2
 499 000210  00 00 06 	return 
 500              	.set ___PA___,0
 501              	.LFE3:
 502              	.size _getsUSBUSART,.-_getsUSBUSART
 503              	.align 2
 504              	.global _putUSBUSART
 505              	.type _putUSBUSART,@function
 506              	_putUSBUSART:
 507              	.LFB4:
 516:lib/lib_pic33e/usb/usb_device_cdc.c **** 
 517:lib/lib_pic33e/usb/usb_device_cdc.c **** /******************************************************************************
 518:lib/lib_pic33e/usb/usb_device_cdc.c ****   Function:
 519:lib/lib_pic33e/usb/usb_device_cdc.c **** 	void putUSBUSART(char *data, uint8_t length)
 520:lib/lib_pic33e/usb/usb_device_cdc.c **** 		
 521:lib/lib_pic33e/usb/usb_device_cdc.c ****   Summary:
 522:lib/lib_pic33e/usb/usb_device_cdc.c ****     putUSBUSART writes an array of data to the USB. Use this version, is
 523:lib/lib_pic33e/usb/usb_device_cdc.c ****     capable of transferring 0x00 (what is typically a NULL character in any of
 524:lib/lib_pic33e/usb/usb_device_cdc.c ****     the string transfer functions).
 525:lib/lib_pic33e/usb/usb_device_cdc.c **** 
 526:lib/lib_pic33e/usb/usb_device_cdc.c ****   Description:
 527:lib/lib_pic33e/usb/usb_device_cdc.c ****     putUSBUSART writes an array of data to the USB. Use this version, is
 528:lib/lib_pic33e/usb/usb_device_cdc.c ****     capable of transferring 0x00 (what is typically a NULL character in any of
 529:lib/lib_pic33e/usb/usb_device_cdc.c ****     the string transfer functions).
 530:lib/lib_pic33e/usb/usb_device_cdc.c ****     
 531:lib/lib_pic33e/usb/usb_device_cdc.c ****     Typical Usage:
 532:lib/lib_pic33e/usb/usb_device_cdc.c ****     <code>
 533:lib/lib_pic33e/usb/usb_device_cdc.c ****         if(USBUSARTIsTxTrfReady())
 534:lib/lib_pic33e/usb/usb_device_cdc.c ****         {
 535:lib/lib_pic33e/usb/usb_device_cdc.c ****             char data[] = {0x00, 0x01, 0x02, 0x03, 0x04};
 536:lib/lib_pic33e/usb/usb_device_cdc.c ****             putUSBUSART(data,5);
 537:lib/lib_pic33e/usb/usb_device_cdc.c ****         }
 538:lib/lib_pic33e/usb/usb_device_cdc.c ****     </code>
 539:lib/lib_pic33e/usb/usb_device_cdc.c ****     
 540:lib/lib_pic33e/usb/usb_device_cdc.c ****     The transfer mechanism for device-to-host(put) is more flexible than
 541:lib/lib_pic33e/usb/usb_device_cdc.c ****     host-to-device(get). It can handle a string of data larger than the
 542:lib/lib_pic33e/usb/usb_device_cdc.c ****     maximum size of bulk IN endpoint. A state machine is used to transfer a
 543:lib/lib_pic33e/usb/usb_device_cdc.c ****     \long string of data over multiple USB transactions. CDCTxService()
 544:lib/lib_pic33e/usb/usb_device_cdc.c ****     must be called periodically to keep sending blocks of data to the host.
 545:lib/lib_pic33e/usb/usb_device_cdc.c **** 
 546:lib/lib_pic33e/usb/usb_device_cdc.c ****   Conditions:
 547:lib/lib_pic33e/usb/usb_device_cdc.c ****     USBUSARTIsTxTrfReady() must return true. This indicates that the last
 548:lib/lib_pic33e/usb/usb_device_cdc.c ****     transfer is complete and is ready to receive a new block of data. The
MPLAB XC16 ASSEMBLY Listing:   			page 20


 549:lib/lib_pic33e/usb/usb_device_cdc.c ****     string of characters pointed to by 'data' must equal to or smaller than
 550:lib/lib_pic33e/usb/usb_device_cdc.c ****     255 BYTEs.
 551:lib/lib_pic33e/usb/usb_device_cdc.c **** 
 552:lib/lib_pic33e/usb/usb_device_cdc.c ****   Input:
 553:lib/lib_pic33e/usb/usb_device_cdc.c ****     char *data - pointer to a RAM array of data to be transfered to the host
 554:lib/lib_pic33e/usb/usb_device_cdc.c ****     uint8_t length - the number of bytes to be transfered (must be less than 255).
 555:lib/lib_pic33e/usb/usb_device_cdc.c **** 		
 556:lib/lib_pic33e/usb/usb_device_cdc.c ****  *****************************************************************************/
 557:lib/lib_pic33e/usb/usb_device_cdc.c **** void putUSBUSART(uint8_t *data, uint8_t  length)
 558:lib/lib_pic33e/usb/usb_device_cdc.c **** {
 508              	.loc 1 558 0
 509              	.set ___PA___,1
 510 000212  04 00 FA 	lnk #4
 511              	.LCFI6:
 512              	.loc 1 558 0
 513 000214  00 0F 78 	mov w0,[w14]
 514 000216  21 47 98 	mov.b w1,[w14+2]
 559:lib/lib_pic33e/usb/usb_device_cdc.c ****     /*
 560:lib/lib_pic33e/usb/usb_device_cdc.c ****      * User should have checked that cdc_trf_state is in CDC_TX_READY state
 561:lib/lib_pic33e/usb/usb_device_cdc.c ****      * before calling this function.
 562:lib/lib_pic33e/usb/usb_device_cdc.c ****      * As a safety precaution, this function checks the state one more time
 563:lib/lib_pic33e/usb/usb_device_cdc.c ****      * to make sure it does not override any pending transactions.
 564:lib/lib_pic33e/usb/usb_device_cdc.c ****      *
 565:lib/lib_pic33e/usb/usb_device_cdc.c ****      * Currently it just quits the routine without reporting any errors back
 566:lib/lib_pic33e/usb/usb_device_cdc.c ****      * to the user.
 567:lib/lib_pic33e/usb/usb_device_cdc.c ****      *
 568:lib/lib_pic33e/usb/usb_device_cdc.c ****      * Bottom line: User MUST make sure that USBUSARTIsTxTrfReady()==1
 569:lib/lib_pic33e/usb/usb_device_cdc.c ****      *             before calling this function!
 570:lib/lib_pic33e/usb/usb_device_cdc.c ****      * Example:
 571:lib/lib_pic33e/usb/usb_device_cdc.c ****      * if(USBUSARTIsTxTrfReady())
 572:lib/lib_pic33e/usb/usb_device_cdc.c ****      *     putUSBUSART(pData, Length);
 573:lib/lib_pic33e/usb/usb_device_cdc.c ****      *
 574:lib/lib_pic33e/usb/usb_device_cdc.c ****      * IMPORTANT: Never use the following blocking while loop to wait:
 575:lib/lib_pic33e/usb/usb_device_cdc.c ****      * while(!USBUSARTIsTxTrfReady())
 576:lib/lib_pic33e/usb/usb_device_cdc.c ****      *     putUSBUSART(pData, Length);
 577:lib/lib_pic33e/usb/usb_device_cdc.c ****      *
 578:lib/lib_pic33e/usb/usb_device_cdc.c ****      * The whole firmware framework is written based on cooperative
 579:lib/lib_pic33e/usb/usb_device_cdc.c ****      * multi-tasking and a blocking code is not acceptable.
 580:lib/lib_pic33e/usb/usb_device_cdc.c ****      * Use a state machine instead.
 581:lib/lib_pic33e/usb/usb_device_cdc.c ****      */
 582:lib/lib_pic33e/usb/usb_device_cdc.c ****     USBMaskInterrupts();
 515              	.loc 1 582 0
 516 000218  00 C0 A9 	bclr.b _IEC5bits,#6
 583:lib/lib_pic33e/usb/usb_device_cdc.c ****     if(cdc_trf_state == CDC_TX_READY)
 517              	.loc 1 583 0
 518 00021a  00 C0 BF 	mov.b _cdc_trf_state,WREG
 519 00021c  00 04 E0 	cp0.b w0
 520              	.set ___BP___,0
 521 00021e  00 00 3A 	bra nz,.L31
 584:lib/lib_pic33e/usb/usb_device_cdc.c ****     {
 585:lib/lib_pic33e/usb/usb_device_cdc.c ****         mUSBUSARTTxRam((uint8_t*)data, length);     // See cdc.h
 522              	.loc 1 585 0
 523 000220  9E 00 78 	mov [w14],w1
 524 000222  01 00 88 	mov w1,_pCDCSrc
 525 000224  AE 41 90 	mov.b [w14+2],w3
 526 000226  02 00 20 	mov #_cdc_tx_len,w2
 527 000228  03 49 78 	mov.b w3,[w2]
MPLAB XC16 ASSEMBLY Listing:   			page 21


 528 00022a  11 C0 B3 	mov.b #1,w1
 529 00022c  10 C0 B3 	mov.b #1,w0
 530 00022e  81 41 78 	mov.b w1,w3
 531 000230  02 00 20 	mov #_cdc_mem_type,w2
 532 000232  03 49 78 	mov.b w3,[w2]
 533 000234  00 E0 B7 	mov.b WREG,_cdc_trf_state
 534              	.L31:
 586:lib/lib_pic33e/usb/usb_device_cdc.c ****     }
 587:lib/lib_pic33e/usb/usb_device_cdc.c ****     USBUnmaskInterrupts();
 535              	.loc 1 587 0
 536 000236  00 C0 A8 	bset.b _IEC5bits,#6
 588:lib/lib_pic33e/usb/usb_device_cdc.c **** }//end putUSBUSART
 537              	.loc 1 588 0
 538 000238  8E 07 78 	mov w14,w15
 539 00023a  4F 07 78 	mov [--w15],w14
 540 00023c  00 40 A9 	bclr CORCON,#2
 541 00023e  00 00 06 	return 
 542              	.set ___PA___,0
 543              	.LFE4:
 544              	.size _putUSBUSART,.-_putUSBUSART
 545              	.align 2
 546              	.global _putsUSBUSART
 547              	.type _putsUSBUSART,@function
 548              	_putsUSBUSART:
 549              	.LFB5:
 589:lib/lib_pic33e/usb/usb_device_cdc.c **** 
 590:lib/lib_pic33e/usb/usb_device_cdc.c **** /******************************************************************************
 591:lib/lib_pic33e/usb/usb_device_cdc.c **** 	Function:
 592:lib/lib_pic33e/usb/usb_device_cdc.c **** 		void putsUSBUSART(char *data)
 593:lib/lib_pic33e/usb/usb_device_cdc.c **** 		
 594:lib/lib_pic33e/usb/usb_device_cdc.c ****   Summary:
 595:lib/lib_pic33e/usb/usb_device_cdc.c ****     putsUSBUSART writes a string of data to the USB including the null
 596:lib/lib_pic33e/usb/usb_device_cdc.c ****     character. Use this version, 'puts', to transfer data from a RAM buffer.
 597:lib/lib_pic33e/usb/usb_device_cdc.c **** 
 598:lib/lib_pic33e/usb/usb_device_cdc.c ****   Description:
 599:lib/lib_pic33e/usb/usb_device_cdc.c ****     putsUSBUSART writes a string of data to the USB including the null
 600:lib/lib_pic33e/usb/usb_device_cdc.c ****     character. Use this version, 'puts', to transfer data from a RAM buffer.
 601:lib/lib_pic33e/usb/usb_device_cdc.c ****     
 602:lib/lib_pic33e/usb/usb_device_cdc.c ****     Typical Usage:
 603:lib/lib_pic33e/usb/usb_device_cdc.c ****     <code>
 604:lib/lib_pic33e/usb/usb_device_cdc.c ****         if(USBUSARTIsTxTrfReady())
 605:lib/lib_pic33e/usb/usb_device_cdc.c ****         {
 606:lib/lib_pic33e/usb/usb_device_cdc.c ****             char data[] = "Hello World";
 607:lib/lib_pic33e/usb/usb_device_cdc.c ****             putsUSBUSART(data);
 608:lib/lib_pic33e/usb/usb_device_cdc.c ****         }
 609:lib/lib_pic33e/usb/usb_device_cdc.c ****     </code>
 610:lib/lib_pic33e/usb/usb_device_cdc.c ****     
 611:lib/lib_pic33e/usb/usb_device_cdc.c ****     The transfer mechanism for device-to-host(put) is more flexible than
 612:lib/lib_pic33e/usb/usb_device_cdc.c ****     host-to-device(get). It can handle a string of data larger than the
 613:lib/lib_pic33e/usb/usb_device_cdc.c ****     maximum size of bulk IN endpoint. A state machine is used to transfer a
 614:lib/lib_pic33e/usb/usb_device_cdc.c ****     \long string of data over multiple USB transactions. CDCTxService()
 615:lib/lib_pic33e/usb/usb_device_cdc.c ****     must be called periodically to keep sending blocks of data to the host.
 616:lib/lib_pic33e/usb/usb_device_cdc.c **** 
 617:lib/lib_pic33e/usb/usb_device_cdc.c ****   Conditions:
 618:lib/lib_pic33e/usb/usb_device_cdc.c ****     USBUSARTIsTxTrfReady() must return true. This indicates that the last
 619:lib/lib_pic33e/usb/usb_device_cdc.c ****     transfer is complete and is ready to receive a new block of data. The
 620:lib/lib_pic33e/usb/usb_device_cdc.c ****     string of characters pointed to by 'data' must equal to or smaller than
MPLAB XC16 ASSEMBLY Listing:   			page 22


 621:lib/lib_pic33e/usb/usb_device_cdc.c ****     255 BYTEs.
 622:lib/lib_pic33e/usb/usb_device_cdc.c **** 
 623:lib/lib_pic33e/usb/usb_device_cdc.c ****   Input:
 624:lib/lib_pic33e/usb/usb_device_cdc.c ****     char *data -  null\-terminated string of constant data. If a
 625:lib/lib_pic33e/usb/usb_device_cdc.c ****                             null character is not found, 255 BYTEs of data
 626:lib/lib_pic33e/usb/usb_device_cdc.c ****                             will be transferred to the host.
 627:lib/lib_pic33e/usb/usb_device_cdc.c **** 		
 628:lib/lib_pic33e/usb/usb_device_cdc.c ****  *****************************************************************************/
 629:lib/lib_pic33e/usb/usb_device_cdc.c ****  
 630:lib/lib_pic33e/usb/usb_device_cdc.c **** void putsUSBUSART(char *data)
 631:lib/lib_pic33e/usb/usb_device_cdc.c **** {
 550              	.loc 1 631 0
 551              	.set ___PA___,1
 552 000240  06 00 FA 	lnk #6
 553              	.LCFI7:
 554              	.loc 1 631 0
 555 000242  20 07 98 	mov w0,[w14+4]
 632:lib/lib_pic33e/usb/usb_device_cdc.c ****     uint8_t len;
 633:lib/lib_pic33e/usb/usb_device_cdc.c ****     char *pData;
 634:lib/lib_pic33e/usb/usb_device_cdc.c **** 
 635:lib/lib_pic33e/usb/usb_device_cdc.c ****     /*
 636:lib/lib_pic33e/usb/usb_device_cdc.c ****      * User should have checked that cdc_trf_state is in CDC_TX_READY state
 637:lib/lib_pic33e/usb/usb_device_cdc.c ****      * before calling this function.
 638:lib/lib_pic33e/usb/usb_device_cdc.c ****      * As a safety precaution, this function checks the state one more time
 639:lib/lib_pic33e/usb/usb_device_cdc.c ****      * to make sure it does not override any pending transactions.
 640:lib/lib_pic33e/usb/usb_device_cdc.c ****      *
 641:lib/lib_pic33e/usb/usb_device_cdc.c ****      * Currently it just quits the routine without reporting any errors back
 642:lib/lib_pic33e/usb/usb_device_cdc.c ****      * to the user.
 643:lib/lib_pic33e/usb/usb_device_cdc.c ****      *
 644:lib/lib_pic33e/usb/usb_device_cdc.c ****      * Bottom line: User MUST make sure that USBUSARTIsTxTrfReady()==1
 645:lib/lib_pic33e/usb/usb_device_cdc.c ****      *             before calling this function!
 646:lib/lib_pic33e/usb/usb_device_cdc.c ****      * Example:
 647:lib/lib_pic33e/usb/usb_device_cdc.c ****      * if(USBUSARTIsTxTrfReady())
 648:lib/lib_pic33e/usb/usb_device_cdc.c ****      *     putsUSBUSART(pData, Length);
 649:lib/lib_pic33e/usb/usb_device_cdc.c ****      *
 650:lib/lib_pic33e/usb/usb_device_cdc.c ****      * IMPORTANT: Never use the following blocking while loop to wait:
 651:lib/lib_pic33e/usb/usb_device_cdc.c ****      * while(!USBUSARTIsTxTrfReady())
 652:lib/lib_pic33e/usb/usb_device_cdc.c ****      *     putsUSBUSART(pData);
 653:lib/lib_pic33e/usb/usb_device_cdc.c ****      *
 654:lib/lib_pic33e/usb/usb_device_cdc.c ****      * The whole firmware framework is written based on cooperative
 655:lib/lib_pic33e/usb/usb_device_cdc.c ****      * multi-tasking and a blocking code is not acceptable.
 656:lib/lib_pic33e/usb/usb_device_cdc.c ****      * Use a state machine instead.
 657:lib/lib_pic33e/usb/usb_device_cdc.c ****      */
 658:lib/lib_pic33e/usb/usb_device_cdc.c ****     USBMaskInterrupts();
 556              	.loc 1 658 0
 557 000244  00 C0 A9 	bclr.b _IEC5bits,#6
 659:lib/lib_pic33e/usb/usb_device_cdc.c ****     if(cdc_trf_state != CDC_TX_READY)
 558              	.loc 1 659 0
 559 000246  00 C0 BF 	mov.b _cdc_trf_state,WREG
 560 000248  00 04 E0 	cp0.b w0
 561              	.set ___BP___,0
 562 00024a  00 00 32 	bra z,.L33
 660:lib/lib_pic33e/usb/usb_device_cdc.c ****     {
 661:lib/lib_pic33e/usb/usb_device_cdc.c ****         USBUnmaskInterrupts();
 563              	.loc 1 661 0
 564 00024c  00 C0 A8 	bset.b _IEC5bits,#6
 662:lib/lib_pic33e/usb/usb_device_cdc.c ****         return;
MPLAB XC16 ASSEMBLY Listing:   			page 23


 565              	.loc 1 662 0
 566 00024e  00 00 37 	bra .L32
 567              	.L33:
 663:lib/lib_pic33e/usb/usb_device_cdc.c ****     }
 664:lib/lib_pic33e/usb/usb_device_cdc.c ****     
 665:lib/lib_pic33e/usb/usb_device_cdc.c ****     /*
 666:lib/lib_pic33e/usb/usb_device_cdc.c ****      * While loop counts the number of BYTEs to send including the
 667:lib/lib_pic33e/usb/usb_device_cdc.c ****      * null character.
 668:lib/lib_pic33e/usb/usb_device_cdc.c ****      */
 669:lib/lib_pic33e/usb/usb_device_cdc.c ****     len = 0;
 670:lib/lib_pic33e/usb/usb_device_cdc.c ****     pData = data;
 568              	.loc 1 670 0
 569 000250  AE 00 90 	mov [w14+4],w1
 570 000252  11 07 98 	mov w1,[w14+2]
 571              	.loc 1 669 0
 572 000254  00 40 EB 	clr.b w0
 573 000256  00 4F 78 	mov.b w0,[w14]
 574              	.L37:
 671:lib/lib_pic33e/usb/usb_device_cdc.c ****     do {
 672:lib/lib_pic33e/usb/usb_device_cdc.c ****         len++;
 575              	.loc 1 672 0
 576 000258  1E 4F E8 	inc.b [w14],[w14]
 673:lib/lib_pic33e/usb/usb_device_cdc.c ****         if(len == 255) break;       // Break loop once max len is reached.
 577              	.loc 1 673 0
 578 00025a  1E 40 78 	mov.b [w14],w0
 579 00025c  E1 4F 40 	add.b w0,#1,[w15]
 580              	.set ___BP___,0
 581 00025e  00 00 32 	bra z,.L38
 582              	.L35:
 674:lib/lib_pic33e/usb/usb_device_cdc.c ****     } while(*pData++);
 583              	.loc 1 674 0
 584 000260  1E 00 90 	mov [w14+2],w0
 585 000262  9E 00 90 	mov [w14+2],w1
 586 000264  10 40 78 	mov.b [w0],w0
 587 000266  81 00 E8 	inc w1,w1
 588 000268  00 80 FB 	ze w0,w0
 589 00026a  11 07 98 	mov w1,[w14+2]
 590 00026c  00 00 EA 	neg w0,w0
 591 00026e  4F 00 DE 	lsr w0,#15,w0
 592 000270  00 40 78 	mov.b w0,w0
 593 000272  00 04 E0 	cp0.b w0
 594              	.set ___BP___,0
 595 000274  00 00 3A 	bra nz,.L37
 596 000276  00 00 37 	bra .L36
 597              	.L38:
 598              	.L36:
 675:lib/lib_pic33e/usb/usb_device_cdc.c ****     
 676:lib/lib_pic33e/usb/usb_device_cdc.c ****     /*
 677:lib/lib_pic33e/usb/usb_device_cdc.c ****      * Second piece of information (length of data to send) is ready.
 678:lib/lib_pic33e/usb/usb_device_cdc.c ****      * Call mUSBUSARTTxRam to setup the transfer.
 679:lib/lib_pic33e/usb/usb_device_cdc.c ****      * The actual transfer process will be handled by CDCTxService(),
 680:lib/lib_pic33e/usb/usb_device_cdc.c ****      * which should be called once per Main Program loop.
 681:lib/lib_pic33e/usb/usb_device_cdc.c ****      */
 682:lib/lib_pic33e/usb/usb_device_cdc.c ****     mUSBUSARTTxRam((uint8_t*)data, len);     // See cdc.h
 599              	.loc 1 682 0
 600 000278  9E 41 78 	mov.b [w14],w3
 601 00027a  02 00 20 	mov #_cdc_tx_len,w2
MPLAB XC16 ASSEMBLY Listing:   			page 24


 602 00027c  03 49 78 	mov.b w3,[w2]
 603 00027e  2E 01 90 	mov [w14+4],w2
 604 000280  11 C0 B3 	mov.b #1,w1
 605 000282  10 C0 B3 	mov.b #1,w0
 606 000284  02 00 88 	mov w2,_pCDCSrc
 607 000286  81 41 78 	mov.b w1,w3
 608 000288  02 00 20 	mov #_cdc_mem_type,w2
 609 00028a  03 49 78 	mov.b w3,[w2]
 610 00028c  00 E0 B7 	mov.b WREG,_cdc_trf_state
 683:lib/lib_pic33e/usb/usb_device_cdc.c ****     USBUnmaskInterrupts();
 611              	.loc 1 683 0
 612 00028e  00 C0 A8 	bset.b _IEC5bits,#6
 613              	.L32:
 684:lib/lib_pic33e/usb/usb_device_cdc.c **** }//end putsUSBUSART
 614              	.loc 1 684 0
 615 000290  8E 07 78 	mov w14,w15
 616 000292  4F 07 78 	mov [--w15],w14
 617 000294  00 40 A9 	bclr CORCON,#2
 618 000296  00 00 06 	return 
 619              	.set ___PA___,0
 620              	.LFE5:
 621              	.size _putsUSBUSART,.-_putsUSBUSART
 622              	.align 2
 623              	.global _CDCTxService
 624              	.type _CDCTxService,@function
 625              	_CDCTxService:
 626              	.LFB6:
 685:lib/lib_pic33e/usb/usb_device_cdc.c **** 
 686:lib/lib_pic33e/usb/usb_device_cdc.c **** 
 687:lib/lib_pic33e/usb/usb_device_cdc.c **** /************************************************************************
 688:lib/lib_pic33e/usb/usb_device_cdc.c ****   Function:
 689:lib/lib_pic33e/usb/usb_device_cdc.c ****         void CDCTxService(void)
 690:lib/lib_pic33e/usb/usb_device_cdc.c ****     
 691:lib/lib_pic33e/usb/usb_device_cdc.c ****   Summary:
 692:lib/lib_pic33e/usb/usb_device_cdc.c ****     CDCTxService handles device-to-host transaction(s). This function
 693:lib/lib_pic33e/usb/usb_device_cdc.c ****     should be called once per Main Program loop after the device reaches
 694:lib/lib_pic33e/usb/usb_device_cdc.c ****     the configured state.
 695:lib/lib_pic33e/usb/usb_device_cdc.c ****   Description:
 696:lib/lib_pic33e/usb/usb_device_cdc.c ****     CDCTxService handles device-to-host transaction(s). This function
 697:lib/lib_pic33e/usb/usb_device_cdc.c ****     should be called once per Main Program loop after the device reaches
 698:lib/lib_pic33e/usb/usb_device_cdc.c ****     the configured state (after the CDCIniEP() function has already executed).
 699:lib/lib_pic33e/usb/usb_device_cdc.c ****     This function is needed, in order to advance the internal software state 
 700:lib/lib_pic33e/usb/usb_device_cdc.c ****     machine that takes care of sending multiple transactions worth of IN USB
 701:lib/lib_pic33e/usb/usb_device_cdc.c ****     data to the host, associated with CDC serial data.  Failure to call 
 702:lib/lib_pic33e/usb/usb_device_cdc.c ****     CDCTxService() periodically will prevent data from being sent to the
 703:lib/lib_pic33e/usb/usb_device_cdc.c ****     USB host, over the CDC serial data interface.
 704:lib/lib_pic33e/usb/usb_device_cdc.c ****     
 705:lib/lib_pic33e/usb/usb_device_cdc.c ****     Typical Usage:
 706:lib/lib_pic33e/usb/usb_device_cdc.c ****     <code>
 707:lib/lib_pic33e/usb/usb_device_cdc.c ****     void main(void)
 708:lib/lib_pic33e/usb/usb_device_cdc.c ****     {
 709:lib/lib_pic33e/usb/usb_device_cdc.c ****         USBDeviceInit();
 710:lib/lib_pic33e/usb/usb_device_cdc.c ****         while(1)
 711:lib/lib_pic33e/usb/usb_device_cdc.c ****         {
 712:lib/lib_pic33e/usb/usb_device_cdc.c ****             USBDeviceTasks();
 713:lib/lib_pic33e/usb/usb_device_cdc.c ****             if((USBGetDeviceState() \< CONFIGURED_STATE) ||
 714:lib/lib_pic33e/usb/usb_device_cdc.c ****                (USBIsDeviceSuspended() == true))
MPLAB XC16 ASSEMBLY Listing:   			page 25


 715:lib/lib_pic33e/usb/usb_device_cdc.c ****             {
 716:lib/lib_pic33e/usb/usb_device_cdc.c ****                 //Either the device is not configured or we are suspended
 717:lib/lib_pic33e/usb/usb_device_cdc.c ****                 //  so we don't want to do execute any application code
 718:lib/lib_pic33e/usb/usb_device_cdc.c ****                 continue;   //go back to the top of the while loop
 719:lib/lib_pic33e/usb/usb_device_cdc.c ****             }
 720:lib/lib_pic33e/usb/usb_device_cdc.c ****             else
 721:lib/lib_pic33e/usb/usb_device_cdc.c ****             {
 722:lib/lib_pic33e/usb/usb_device_cdc.c ****                 //Keep trying to send data to the PC as required
 723:lib/lib_pic33e/usb/usb_device_cdc.c ****                 CDCTxService();
 724:lib/lib_pic33e/usb/usb_device_cdc.c ****     
 725:lib/lib_pic33e/usb/usb_device_cdc.c ****                 //Run application code.
 726:lib/lib_pic33e/usb/usb_device_cdc.c ****                 UserApplication();
 727:lib/lib_pic33e/usb/usb_device_cdc.c ****             }
 728:lib/lib_pic33e/usb/usb_device_cdc.c ****         }
 729:lib/lib_pic33e/usb/usb_device_cdc.c ****     }
 730:lib/lib_pic33e/usb/usb_device_cdc.c ****     </code>
 731:lib/lib_pic33e/usb/usb_device_cdc.c ****   Conditions:
 732:lib/lib_pic33e/usb/usb_device_cdc.c ****     CDCIniEP() function should have already executed/the device should be
 733:lib/lib_pic33e/usb/usb_device_cdc.c ****     in the CONFIGURED_STATE.
 734:lib/lib_pic33e/usb/usb_device_cdc.c ****   Remarks:
 735:lib/lib_pic33e/usb/usb_device_cdc.c ****     None                                                                 
 736:lib/lib_pic33e/usb/usb_device_cdc.c ****   ************************************************************************/
 737:lib/lib_pic33e/usb/usb_device_cdc.c ****  
 738:lib/lib_pic33e/usb/usb_device_cdc.c **** void CDCTxService(void)
 739:lib/lib_pic33e/usb/usb_device_cdc.c **** {
 627              	.loc 1 739 0
 628              	.set ___PA___,1
 629 000298  02 00 FA 	lnk #2
 630              	.LCFI8:
 740:lib/lib_pic33e/usb/usb_device_cdc.c ****     uint8_t byte_to_send;
 741:lib/lib_pic33e/usb/usb_device_cdc.c ****     uint8_t i;
 742:lib/lib_pic33e/usb/usb_device_cdc.c ****     
 743:lib/lib_pic33e/usb/usb_device_cdc.c ****     USBMaskInterrupts();
 631              	.loc 1 743 0
 632 00029a  00 C0 A9 	bclr.b _IEC5bits,#6
 744:lib/lib_pic33e/usb/usb_device_cdc.c ****     
 745:lib/lib_pic33e/usb/usb_device_cdc.c ****     CDCNotificationHandler();
 746:lib/lib_pic33e/usb/usb_device_cdc.c ****     
 747:lib/lib_pic33e/usb/usb_device_cdc.c ****     if(USBHandleBusy(CDCDataInHandle)) 
 633              	.loc 1 747 0
 634 00029c  00 00 80 	mov _CDCDataInHandle,w0
 635 00029e  00 00 E0 	cp0 w0
 636              	.set ___BP___,0
 637 0002a0  00 00 32 	bra z,.L40
 638 0002a2  00 00 80 	mov _CDCDataInHandle,w0
 639 0002a4  80 00 78 	mov w0,w1
 640 0002a6  00 C8 B3 	mov.b #-128,w0
 641 0002a8  91 40 78 	mov.b [w1],w1
 642 0002aa  00 C0 60 	and.b w1,w0,w0
 643 0002ac  00 04 E0 	cp0.b w0
 644              	.set ___BP___,0
 645 0002ae  00 00 32 	bra z,.L40
 748:lib/lib_pic33e/usb/usb_device_cdc.c ****     {
 749:lib/lib_pic33e/usb/usb_device_cdc.c ****         USBUnmaskInterrupts();
 646              	.loc 1 749 0
 647 0002b0  00 C0 A8 	bset.b _IEC5bits,#6
 750:lib/lib_pic33e/usb/usb_device_cdc.c ****         return;
MPLAB XC16 ASSEMBLY Listing:   			page 26


 648              	.loc 1 750 0
 649 0002b2  00 00 37 	bra .L39
 650              	.L40:
 751:lib/lib_pic33e/usb/usb_device_cdc.c ****     }
 752:lib/lib_pic33e/usb/usb_device_cdc.c **** 
 753:lib/lib_pic33e/usb/usb_device_cdc.c ****     /*
 754:lib/lib_pic33e/usb/usb_device_cdc.c ****      * Completing stage is necessary while [ mCDCUSartTxIsBusy()==1 ].
 755:lib/lib_pic33e/usb/usb_device_cdc.c ****      * By having this stage, user can always check cdc_trf_state,
 756:lib/lib_pic33e/usb/usb_device_cdc.c ****      * and not having to call mCDCUsartTxIsBusy() directly.
 757:lib/lib_pic33e/usb/usb_device_cdc.c ****      */
 758:lib/lib_pic33e/usb/usb_device_cdc.c ****     if(cdc_trf_state == CDC_TX_COMPLETING)
 651              	.loc 1 758 0
 652 0002b4  00 C0 BF 	mov.b _cdc_trf_state,WREG
 653 0002b6  E3 4F 50 	sub.b w0,#3,[w15]
 654              	.set ___BP___,0
 655 0002b8  00 00 3A 	bra nz,.L42
 759:lib/lib_pic33e/usb/usb_device_cdc.c ****         cdc_trf_state = CDC_TX_READY;
 656              	.loc 1 759 0
 657 0002ba  00 60 EF 	clr.b _cdc_trf_state
 658              	.L42:
 760:lib/lib_pic33e/usb/usb_device_cdc.c ****     
 761:lib/lib_pic33e/usb/usb_device_cdc.c ****     /*
 762:lib/lib_pic33e/usb/usb_device_cdc.c ****      * If CDC_TX_READY state, nothing to do, just return.
 763:lib/lib_pic33e/usb/usb_device_cdc.c ****      */
 764:lib/lib_pic33e/usb/usb_device_cdc.c ****     if(cdc_trf_state == CDC_TX_READY)
 659              	.loc 1 764 0
 660 0002bc  00 C0 BF 	mov.b _cdc_trf_state,WREG
 661 0002be  00 04 E0 	cp0.b w0
 662              	.set ___BP___,0
 663 0002c0  00 00 3A 	bra nz,.L43
 765:lib/lib_pic33e/usb/usb_device_cdc.c ****     {
 766:lib/lib_pic33e/usb/usb_device_cdc.c ****         USBUnmaskInterrupts();
 664              	.loc 1 766 0
 665 0002c2  00 C0 A8 	bset.b _IEC5bits,#6
 767:lib/lib_pic33e/usb/usb_device_cdc.c ****         return;
 666              	.loc 1 767 0
 667 0002c4  00 00 37 	bra .L39
 668              	.L43:
 768:lib/lib_pic33e/usb/usb_device_cdc.c ****     }
 769:lib/lib_pic33e/usb/usb_device_cdc.c ****     
 770:lib/lib_pic33e/usb/usb_device_cdc.c ****     /*
 771:lib/lib_pic33e/usb/usb_device_cdc.c ****      * If CDC_TX_BUSY_ZLP state, send zero length packet
 772:lib/lib_pic33e/usb/usb_device_cdc.c ****      */
 773:lib/lib_pic33e/usb/usb_device_cdc.c ****     if(cdc_trf_state == CDC_TX_BUSY_ZLP)
 669              	.loc 1 773 0
 670 0002c6  00 C0 BF 	mov.b _cdc_trf_state,WREG
 671 0002c8  E2 4F 50 	sub.b w0,#2,[w15]
 672              	.set ___BP___,0
 673 0002ca  00 00 3A 	bra nz,.L44
 774:lib/lib_pic33e/usb/usb_device_cdc.c ****     {
 775:lib/lib_pic33e/usb/usb_device_cdc.c ****         CDCDataInHandle = USBTxOnePacket(CDC_DATA_EP,NULL,0);
 674              	.loc 1 775 0
 675 0002cc  80 41 EB 	clr.b w3
 676 0002ce  00 01 EB 	clr w2
 677 0002d0  11 C0 B3 	mov.b #1,w1
 678 0002d2  20 C0 B3 	mov.b #2,w0
 679 0002d4  00 00 07 	rcall _USBTransferOnePacket
MPLAB XC16 ASSEMBLY Listing:   			page 27


 776:lib/lib_pic33e/usb/usb_device_cdc.c ****         //CDC_DATA_BD_IN.CNT = 0;
 777:lib/lib_pic33e/usb/usb_device_cdc.c ****         cdc_trf_state = CDC_TX_COMPLETING;
 680              	.loc 1 777 0
 681 0002d6  31 C0 B3 	mov.b #3,w1
 682              	.loc 1 775 0
 683 0002d8  00 00 88 	mov w0,_CDCDataInHandle
 684              	.loc 1 777 0
 685 0002da  81 41 78 	mov.b w1,w3
 686 0002dc  02 00 20 	mov #_cdc_trf_state,w2
 687 0002de  03 49 78 	mov.b w3,[w2]
 688 0002e0  00 00 37 	bra .L45
 689              	.L44:
 778:lib/lib_pic33e/usb/usb_device_cdc.c ****     }
 779:lib/lib_pic33e/usb/usb_device_cdc.c ****     else if(cdc_trf_state == CDC_TX_BUSY)
 690              	.loc 1 779 0
 691 0002e2  00 C0 BF 	mov.b _cdc_trf_state,WREG
 692 0002e4  E1 4F 50 	sub.b w0,#1,[w15]
 693              	.set ___BP___,0
 694 0002e6  00 00 3A 	bra nz,.L45
 780:lib/lib_pic33e/usb/usb_device_cdc.c ****     {
 781:lib/lib_pic33e/usb/usb_device_cdc.c ****         /*
 782:lib/lib_pic33e/usb/usb_device_cdc.c ****          * First, have to figure out how many byte of data to send.
 783:lib/lib_pic33e/usb/usb_device_cdc.c ****          */
 784:lib/lib_pic33e/usb/usb_device_cdc.c ****     	if(cdc_tx_len > sizeof(cdc_data_tx))
 695              	.loc 1 784 0
 696 0002e8  01 00 20 	mov #_cdc_tx_len,w1
 697 0002ea  00 00 00 	nop
 698 0002ec  91 40 78 	mov.b [w1],w1
 699 0002ee  00 C4 B3 	mov.b #64,w0
 700 0002f0  80 CF 50 	sub.b w1,w0,[w15]
 701              	.set ___BP___,0
 702 0002f2  00 00 36 	bra leu,.L46
 785:lib/lib_pic33e/usb/usb_device_cdc.c ****     	    byte_to_send = sizeof(cdc_data_tx);
 703              	.loc 1 785 0
 704 0002f4  00 C4 B3 	mov.b #64,w0
 705 0002f6  00 4F 78 	mov.b w0,[w14]
 706 0002f8  00 00 37 	bra .L47
 707              	.L46:
 786:lib/lib_pic33e/usb/usb_device_cdc.c ****     	else
 787:lib/lib_pic33e/usb/usb_device_cdc.c ****     	    byte_to_send = cdc_tx_len;
 708              	.loc 1 787 0
 709 0002fa  01 00 20 	mov #_cdc_tx_len,w1
 710 0002fc  00 00 00 	nop
 711 0002fe  91 40 78 	mov.b [w1],w1
 712 000300  01 4F 78 	mov.b w1,[w14]
 713              	.L47:
 788:lib/lib_pic33e/usb/usb_device_cdc.c **** 
 789:lib/lib_pic33e/usb/usb_device_cdc.c ****         /*
 790:lib/lib_pic33e/usb/usb_device_cdc.c ****          * Subtract the number of bytes just about to be sent from the total.
 791:lib/lib_pic33e/usb/usb_device_cdc.c ****          */
 792:lib/lib_pic33e/usb/usb_device_cdc.c ****     	cdc_tx_len = cdc_tx_len - byte_to_send;
 714              	.loc 1 792 0
 715 000302  00 C0 BF 	mov.b _cdc_tx_len,WREG
 793:lib/lib_pic33e/usb/usb_device_cdc.c ****     	  
 794:lib/lib_pic33e/usb/usb_device_cdc.c ****         pCDCDst.bRam = (uint8_t*)&cdc_data_tx; // Set destination pointer
 716              	.loc 1 794 0
 717 000304  01 00 20 	mov #_cdc_data_tx,w1
MPLAB XC16 ASSEMBLY Listing:   			page 28


 718              	.loc 1 792 0
 719 000306  00 00 00 	nop 
 720 000308  1E 40 50 	sub.b w0,[w14],w0
 721              	.loc 1 794 0
 722 00030a  01 00 88 	mov w1,_pCDCDst
 723              	.loc 1 792 0
 724 00030c  00 E0 B7 	mov.b WREG,_cdc_tx_len
 795:lib/lib_pic33e/usb/usb_device_cdc.c ****         
 796:lib/lib_pic33e/usb/usb_device_cdc.c ****         i = byte_to_send;
 725              	.loc 1 796 0
 726 00030e  1E 41 78 	mov.b [w14],w2
 727 000310  12 47 98 	mov.b w2,[w14+1]
 797:lib/lib_pic33e/usb/usb_device_cdc.c ****         if(cdc_mem_type == USB_EP0_ROM)            // Determine type of memory source
 728              	.loc 1 797 0
 729 000312  00 C0 BF 	mov.b _cdc_mem_type,WREG
 730 000314  00 04 E0 	cp0.b w0
 731              	.set ___BP___,0
 732 000316  00 00 3A 	bra nz,.L52
 798:lib/lib_pic33e/usb/usb_device_cdc.c ****         {
 799:lib/lib_pic33e/usb/usb_device_cdc.c ****             while(i)
 733              	.loc 1 799 0
 734 000318  00 00 37 	bra .L49
 735              	.L50:
 800:lib/lib_pic33e/usb/usb_device_cdc.c ****             {
 801:lib/lib_pic33e/usb/usb_device_cdc.c ****                 *pCDCDst.bRam = *pCDCSrc.bRom;
 736              	.loc 1 801 0
 737 00031a  01 00 80 	mov _pCDCSrc,w1
 738 00031c  00 00 80 	mov _pCDCDst,w0
 739 00031e  91 40 78 	mov.b [w1],w1
 740 000320  01 48 78 	mov.b w1,[w0]
 802:lib/lib_pic33e/usb/usb_device_cdc.c ****                 pCDCDst.bRam++;
 803:lib/lib_pic33e/usb/usb_device_cdc.c ****                 pCDCSrc.bRom++;
 804:lib/lib_pic33e/usb/usb_device_cdc.c ****                 i--;
 741              	.loc 1 804 0
 742 000322  1E 40 90 	mov.b [w14+1],w0
 743 000324  00 40 E9 	dec.b w0,w0
 744 000326  10 47 98 	mov.b w0,[w14+1]
 745              	.loc 1 802 0
 746 000328  01 00 80 	mov _pCDCDst,w1
 747              	.loc 1 803 0
 748 00032a  00 00 80 	mov _pCDCSrc,w0
 749              	.loc 1 802 0
 750 00032c  81 00 E8 	inc w1,w1
 751              	.loc 1 803 0
 752 00032e  00 00 E8 	inc w0,w0
 753              	.loc 1 802 0
 754 000330  01 00 88 	mov w1,_pCDCDst
 755              	.loc 1 803 0
 756 000332  00 00 88 	mov w0,_pCDCSrc
 757              	.L49:
 758              	.loc 1 799 0
 759 000334  00 00 00 	nop 
 760 000336  1E 40 90 	mov.b [w14+1],w0
 761 000338  00 04 E0 	cp0.b w0
 762              	.set ___BP___,0
 763 00033a  00 00 3A 	bra nz,.L50
 764 00033c  00 00 37 	bra .L51
MPLAB XC16 ASSEMBLY Listing:   			page 29


 765              	.L53:
 805:lib/lib_pic33e/usb/usb_device_cdc.c ****             }//end while(byte_to_send)
 806:lib/lib_pic33e/usb/usb_device_cdc.c ****         }
 807:lib/lib_pic33e/usb/usb_device_cdc.c ****         else
 808:lib/lib_pic33e/usb/usb_device_cdc.c ****         {
 809:lib/lib_pic33e/usb/usb_device_cdc.c ****             while(i)
 810:lib/lib_pic33e/usb/usb_device_cdc.c ****             {
 811:lib/lib_pic33e/usb/usb_device_cdc.c ****                 *pCDCDst.bRam = *pCDCSrc.bRam;
 766              	.loc 1 811 0
 767 00033e  01 00 80 	mov _pCDCSrc,w1
 768 000340  00 00 80 	mov _pCDCDst,w0
 769 000342  91 40 78 	mov.b [w1],w1
 770 000344  01 48 78 	mov.b w1,[w0]
 812:lib/lib_pic33e/usb/usb_device_cdc.c ****                 pCDCDst.bRam++;
 813:lib/lib_pic33e/usb/usb_device_cdc.c ****                 pCDCSrc.bRam++;
 814:lib/lib_pic33e/usb/usb_device_cdc.c ****                 i--;
 771              	.loc 1 814 0
 772 000346  1E 40 90 	mov.b [w14+1],w0
 773 000348  00 40 E9 	dec.b w0,w0
 774 00034a  10 47 98 	mov.b w0,[w14+1]
 775              	.loc 1 812 0
 776 00034c  01 00 80 	mov _pCDCDst,w1
 777              	.loc 1 813 0
 778 00034e  00 00 80 	mov _pCDCSrc,w0
 779              	.loc 1 812 0
 780 000350  81 00 E8 	inc w1,w1
 781              	.loc 1 813 0
 782 000352  00 00 E8 	inc w0,w0
 783              	.loc 1 812 0
 784 000354  01 00 88 	mov w1,_pCDCDst
 785              	.loc 1 813 0
 786 000356  00 00 88 	mov w0,_pCDCSrc
 787              	.L52:
 788              	.loc 1 809 0
 789 000358  00 00 00 	nop 
 790 00035a  1E 40 90 	mov.b [w14+1],w0
 791 00035c  00 04 E0 	cp0.b w0
 792              	.set ___BP___,0
 793 00035e  00 00 3A 	bra nz,.L53
 794              	.L51:
 815:lib/lib_pic33e/usb/usb_device_cdc.c ****             }
 816:lib/lib_pic33e/usb/usb_device_cdc.c ****         }
 817:lib/lib_pic33e/usb/usb_device_cdc.c ****         
 818:lib/lib_pic33e/usb/usb_device_cdc.c ****         /*
 819:lib/lib_pic33e/usb/usb_device_cdc.c ****          * Lastly, determine if a zero length packet state is necessary.
 820:lib/lib_pic33e/usb/usb_device_cdc.c ****          * See explanation in USB Specification 2.0: Section 5.8.3
 821:lib/lib_pic33e/usb/usb_device_cdc.c ****          */
 822:lib/lib_pic33e/usb/usb_device_cdc.c ****         if(cdc_tx_len == 0)
 795              	.loc 1 822 0
 796 000360  00 C0 BF 	mov.b _cdc_tx_len,WREG
 797 000362  00 04 E0 	cp0.b w0
 798              	.set ___BP___,0
 799 000364  00 00 3A 	bra nz,.L54
 823:lib/lib_pic33e/usb/usb_device_cdc.c ****         {
 824:lib/lib_pic33e/usb/usb_device_cdc.c ****             if(byte_to_send == CDC_DATA_IN_EP_SIZE)
 800              	.loc 1 824 0
 801 000366  00 C4 B3 	mov.b #64,w0
MPLAB XC16 ASSEMBLY Listing:   			page 30


 802 000368  9E 40 78 	mov.b [w14],w1
 803 00036a  80 CF 50 	sub.b w1,w0,[w15]
 804              	.set ___BP___,0
 805 00036c  00 00 3A 	bra nz,.L55
 825:lib/lib_pic33e/usb/usb_device_cdc.c ****                 cdc_trf_state = CDC_TX_BUSY_ZLP;
 806              	.loc 1 825 0
 807 00036e  20 C0 B3 	mov.b #2,w0
 808 000370  00 E0 B7 	mov.b WREG,_cdc_trf_state
 809 000372  00 00 37 	bra .L54
 810              	.L55:
 826:lib/lib_pic33e/usb/usb_device_cdc.c ****             else
 827:lib/lib_pic33e/usb/usb_device_cdc.c ****                 cdc_trf_state = CDC_TX_COMPLETING;
 811              	.loc 1 827 0
 812 000374  30 C0 B3 	mov.b #3,w0
 813 000376  00 E0 B7 	mov.b WREG,_cdc_trf_state
 814              	.L54:
 828:lib/lib_pic33e/usb/usb_device_cdc.c ****         }//end if(cdc_tx_len...)
 829:lib/lib_pic33e/usb/usb_device_cdc.c ****         CDCDataInHandle = USBTxOnePacket(CDC_DATA_EP,(uint8_t*)&cdc_data_tx,byte_to_send);
 815              	.loc 1 829 0
 816 000378  00 00 00 	nop 
 817 00037a  9E 41 78 	mov.b [w14],w3
 818 00037c  02 00 20 	mov #_cdc_data_tx,w2
 819 00037e  11 C0 B3 	mov.b #1,w1
 820 000380  20 C0 B3 	mov.b #2,w0
 821 000382  00 00 07 	rcall _USBTransferOnePacket
 822 000384  00 00 88 	mov w0,_CDCDataInHandle
 823              	.L45:
 830:lib/lib_pic33e/usb/usb_device_cdc.c **** 
 831:lib/lib_pic33e/usb/usb_device_cdc.c ****     }//end if(cdc_tx_sate == CDC_TX_BUSY)
 832:lib/lib_pic33e/usb/usb_device_cdc.c ****     
 833:lib/lib_pic33e/usb/usb_device_cdc.c ****     USBUnmaskInterrupts();
 824              	.loc 1 833 0
 825 000386  00 C0 A8 	bset.b _IEC5bits,#6
 826              	.L39:
 834:lib/lib_pic33e/usb/usb_device_cdc.c **** }//end CDCTxService
 827              	.loc 1 834 0
 828 000388  8E 07 78 	mov w14,w15
 829 00038a  4F 07 78 	mov [--w15],w14
 830 00038c  00 40 A9 	bclr CORCON,#2
 831 00038e  00 00 06 	return 
 832              	.set ___PA___,0
 833              	.LFE6:
 834              	.size _CDCTxService,.-_CDCTxService
 835              	.section .debug_frame,info
 836                 	.Lframe0:
 837 0000 10 00 00 00 	.4byte .LECIE0-.LSCIE0
 838                 	.LSCIE0:
 839 0004 FF FF FF FF 	.4byte 0xffffffff
 840 0008 01          	.byte 0x1
 841 0009 00          	.byte 0
 842 000a 01          	.uleb128 0x1
 843 000b 02          	.sleb128 2
 844 000c 25          	.byte 0x25
 845 000d 12          	.byte 0x12
 846 000e 0F          	.uleb128 0xf
 847 000f 7E          	.sleb128 -2
 848 0010 09          	.byte 0x9
MPLAB XC16 ASSEMBLY Listing:   			page 31


 849 0011 25          	.uleb128 0x25
 850 0012 0F          	.uleb128 0xf
 851 0013 00          	.align 4
 852                 	.LECIE0:
 853                 	.LSFDE0:
 854 0014 20 00 00 00 	.4byte .LEFDE0-.LASFDE0
 855                 	.LASFDE0:
 856 0018 00 00 00 00 	.4byte .Lframe0
 857 001c 00 00 00 00 	.4byte .LFB0
 858 0020 10 01 00 00 	.4byte .LFE0-.LFB0
 859 0024 04          	.byte 0x4
 860 0025 02 00 00 00 	.4byte .LCFI0-.LFB0
 861 0029 13          	.byte 0x13
 862 002a 7D          	.sleb128 -3
 863 002b 0D          	.byte 0xd
 864 002c 0E          	.uleb128 0xe
 865 002d 8E          	.byte 0x8e
 866 002e 02          	.uleb128 0x2
 867 002f 04          	.byte 0x4
 868 0030 04 00 00 00 	.4byte .LCFI2-.LCFI0
 869 0034 8A          	.byte 0x8a
 870 0035 05          	.uleb128 0x5
 871 0036 88          	.byte 0x88
 872 0037 03          	.uleb128 0x3
 873                 	.align 4
 874                 	.LEFDE0:
 875                 	.LSFDE2:
 876 0038 18 00 00 00 	.4byte .LEFDE2-.LASFDE2
 877                 	.LASFDE2:
 878 003c 00 00 00 00 	.4byte .Lframe0
 879 0040 00 00 00 00 	.4byte .LFB1
 880 0044 42 00 00 00 	.4byte .LFE1-.LFB1
 881 0048 04          	.byte 0x4
 882 0049 02 00 00 00 	.4byte .LCFI3-.LFB1
 883 004d 13          	.byte 0x13
 884 004e 7D          	.sleb128 -3
 885 004f 0D          	.byte 0xd
 886 0050 0E          	.uleb128 0xe
 887 0051 8E          	.byte 0x8e
 888 0052 02          	.uleb128 0x2
 889 0053 00          	.align 4
 890                 	.LEFDE2:
 891                 	.LSFDE4:
 892 0054 18 00 00 00 	.4byte .LEFDE4-.LASFDE4
 893                 	.LASFDE4:
 894 0058 00 00 00 00 	.4byte .Lframe0
 895 005c 00 00 00 00 	.4byte .LFB2
 896 0060 3C 00 00 00 	.4byte .LFE2-.LFB2
 897 0064 04          	.byte 0x4
 898 0065 02 00 00 00 	.4byte .LCFI4-.LFB2
 899 0069 13          	.byte 0x13
 900 006a 7D          	.sleb128 -3
 901 006b 0D          	.byte 0xd
 902 006c 0E          	.uleb128 0xe
 903 006d 8E          	.byte 0x8e
 904 006e 02          	.uleb128 0x2
 905 006f 00          	.align 4
MPLAB XC16 ASSEMBLY Listing:   			page 32


 906                 	.LEFDE4:
 907                 	.LSFDE6:
 908 0070 18 00 00 00 	.4byte .LEFDE6-.LASFDE6
 909                 	.LASFDE6:
 910 0074 00 00 00 00 	.4byte .Lframe0
 911 0078 00 00 00 00 	.4byte .LFB3
 912 007c 84 00 00 00 	.4byte .LFE3-.LFB3
 913 0080 04          	.byte 0x4
 914 0081 02 00 00 00 	.4byte .LCFI5-.LFB3
 915 0085 13          	.byte 0x13
 916 0086 7D          	.sleb128 -3
 917 0087 0D          	.byte 0xd
 918 0088 0E          	.uleb128 0xe
 919 0089 8E          	.byte 0x8e
 920 008a 02          	.uleb128 0x2
 921 008b 00          	.align 4
 922                 	.LEFDE6:
 923                 	.LSFDE8:
 924 008c 18 00 00 00 	.4byte .LEFDE8-.LASFDE8
 925                 	.LASFDE8:
 926 0090 00 00 00 00 	.4byte .Lframe0
 927 0094 00 00 00 00 	.4byte .LFB4
 928 0098 2E 00 00 00 	.4byte .LFE4-.LFB4
 929 009c 04          	.byte 0x4
 930 009d 02 00 00 00 	.4byte .LCFI6-.LFB4
 931 00a1 13          	.byte 0x13
 932 00a2 7D          	.sleb128 -3
 933 00a3 0D          	.byte 0xd
 934 00a4 0E          	.uleb128 0xe
 935 00a5 8E          	.byte 0x8e
 936 00a6 02          	.uleb128 0x2
 937 00a7 00          	.align 4
 938                 	.LEFDE8:
 939                 	.LSFDE10:
 940 00a8 18 00 00 00 	.4byte .LEFDE10-.LASFDE10
 941                 	.LASFDE10:
 942 00ac 00 00 00 00 	.4byte .Lframe0
 943 00b0 00 00 00 00 	.4byte .LFB5
 944 00b4 58 00 00 00 	.4byte .LFE5-.LFB5
 945 00b8 04          	.byte 0x4
 946 00b9 02 00 00 00 	.4byte .LCFI7-.LFB5
 947 00bd 13          	.byte 0x13
 948 00be 7D          	.sleb128 -3
 949 00bf 0D          	.byte 0xd
 950 00c0 0E          	.uleb128 0xe
 951 00c1 8E          	.byte 0x8e
 952 00c2 02          	.uleb128 0x2
 953 00c3 00          	.align 4
 954                 	.LEFDE10:
 955                 	.LSFDE12:
 956 00c4 18 00 00 00 	.4byte .LEFDE12-.LASFDE12
 957                 	.LASFDE12:
 958 00c8 00 00 00 00 	.4byte .Lframe0
 959 00cc 00 00 00 00 	.4byte .LFB6
 960 00d0 F8 00 00 00 	.4byte .LFE6-.LFB6
 961 00d4 04          	.byte 0x4
 962 00d5 02 00 00 00 	.4byte .LCFI8-.LFB6
MPLAB XC16 ASSEMBLY Listing:   			page 33


 963 00d9 13          	.byte 0x13
 964 00da 7D          	.sleb128 -3
 965 00db 0D          	.byte 0xd
 966 00dc 0E          	.uleb128 0xe
 967 00dd 8E          	.byte 0x8e
 968 00de 02          	.uleb128 0x2
 969 00df 00          	.align 4
 970                 	.LEFDE12:
 971                 	.section .text,code
 972              	.Letext0:
 973              	.file 2 "/opt/microchip/xc16/v1.36/bin/bin/../../include/lega-c/stdint.h"
 974              	.file 3 "/opt/microchip/xc16/v1.36/bin/bin/../../support/dsPIC33E/h/p33EP256MU806.h"
 975              	.file 4 "lib/lib_pic33e/usb/usb_common.h"
 976              	.file 5 "lib/lib_pic33e/usb/usb_ch9.h"
 977              	.file 6 "lib/lib_pic33e/usb/usb_device.h"
 978              	.file 7 "lib/lib_pic33e/usb/usb_hal_dspic33e.h"
 979              	.file 8 "lib/lib_pic33e/usb/usb_device_cdc.h"
 980              	.section .debug_info,info
 981 0000 16 16 00 00 	.4byte 0x1616
 982 0004 02 00       	.2byte 0x2
 983 0006 00 00 00 00 	.4byte .Ldebug_abbrev0
 984 000a 04          	.byte 0x4
 985 000b 01          	.uleb128 0x1
 986 000c 47 4E 55 20 	.asciz "GNU C 4.5.1 (XC16, Microchip v1.36) (A) Build date: Dec  5 2018"
 986      43 20 34 2E 
 986      35 2E 31 20 
 986      28 58 43 31 
 986      36 2C 20 4D 
 986      69 63 72 6F 
 986      63 68 69 70 
 986      20 76 31 2E 
 986      33 36 29 20 
 987 004c 01          	.byte 0x1
 988 004d 6C 69 62 2F 	.asciz "lib/lib_pic33e/usb/usb_device_cdc.c"
 988      6C 69 62 5F 
 988      70 69 63 33 
 988      33 65 2F 75 
 988      73 62 2F 75 
 988      73 62 5F 64 
 988      65 76 69 63 
 988      65 5F 63 64 
 988      63 2E 63 00 
 989 0071 2F 68 6F 6D 	.asciz "/home/user/Documents/FST/Programming/project_sub-zero"
 989      65 2F 75 73 
 989      65 72 2F 44 
 989      6F 63 75 6D 
 989      65 6E 74 73 
 989      2F 46 53 54 
 989      2F 50 72 6F 
 989      67 72 61 6D 
 989      6D 69 6E 67 
 990 00a7 00 00 00 00 	.4byte .Ltext0
 991 00ab 00 00 00 00 	.4byte .Letext0
 992 00af 00 00 00 00 	.4byte .Ldebug_line0
 993 00b3 02          	.uleb128 0x2
 994 00b4 01          	.byte 0x1
 995 00b5 06          	.byte 0x6
MPLAB XC16 ASSEMBLY Listing:   			page 34


 996 00b6 73 69 67 6E 	.asciz "signed char"
 996      65 64 20 63 
 996      68 61 72 00 
 997 00c2 02          	.uleb128 0x2
 998 00c3 02          	.byte 0x2
 999 00c4 05          	.byte 0x5
 1000 00c5 69 6E 74 00 	.asciz "int"
 1001 00c9 02          	.uleb128 0x2
 1002 00ca 04          	.byte 0x4
 1003 00cb 05          	.byte 0x5
 1004 00cc 6C 6F 6E 67 	.asciz "long int"
 1004      20 69 6E 74 
 1004      00 
 1005 00d5 02          	.uleb128 0x2
 1006 00d6 08          	.byte 0x8
 1007 00d7 05          	.byte 0x5
 1008 00d8 6C 6F 6E 67 	.asciz "long long int"
 1008      20 6C 6F 6E 
 1008      67 20 69 6E 
 1008      74 00 
 1009 00e6 03          	.uleb128 0x3
 1010 00e7 75 69 6E 74 	.asciz "uint8_t"
 1010      38 5F 74 00 
 1011 00ef 02          	.byte 0x2
 1012 00f0 2B          	.byte 0x2b
 1013 00f1 F5 00 00 00 	.4byte 0xf5
 1014 00f5 02          	.uleb128 0x2
 1015 00f6 01          	.byte 0x1
 1016 00f7 08          	.byte 0x8
 1017 00f8 75 6E 73 69 	.asciz "unsigned char"
 1017      67 6E 65 64 
 1017      20 63 68 61 
 1017      72 00 
 1018 0106 03          	.uleb128 0x3
 1019 0107 75 69 6E 74 	.asciz "uint16_t"
 1019      31 36 5F 74 
 1019      00 
 1020 0110 02          	.byte 0x2
 1021 0111 31          	.byte 0x31
 1022 0112 16 01 00 00 	.4byte 0x116
 1023 0116 02          	.uleb128 0x2
 1024 0117 02          	.byte 0x2
 1025 0118 07          	.byte 0x7
 1026 0119 75 6E 73 69 	.asciz "unsigned int"
 1026      67 6E 65 64 
 1026      20 69 6E 74 
 1026      00 
 1027 0126 03          	.uleb128 0x3
 1028 0127 75 69 6E 74 	.asciz "uint32_t"
 1028      33 32 5F 74 
 1028      00 
 1029 0130 02          	.byte 0x2
 1030 0131 37          	.byte 0x37
 1031 0132 36 01 00 00 	.4byte 0x136
 1032 0136 02          	.uleb128 0x2
 1033 0137 04          	.byte 0x4
 1034 0138 07          	.byte 0x7
MPLAB XC16 ASSEMBLY Listing:   			page 35


 1035 0139 6C 6F 6E 67 	.asciz "long unsigned int"
 1035      20 75 6E 73 
 1035      69 67 6E 65 
 1035      64 20 69 6E 
 1035      74 00 
 1036 014b 02          	.uleb128 0x2
 1037 014c 08          	.byte 0x8
 1038 014d 07          	.byte 0x7
 1039 014e 6C 6F 6E 67 	.asciz "long long unsigned int"
 1039      20 6C 6F 6E 
 1039      67 20 75 6E 
 1039      73 69 67 6E 
 1039      65 64 20 69 
 1039      6E 74 00 
 1040 0165 04          	.uleb128 0x4
 1041 0166 74 61 67 49 	.asciz "tagIEC5BITS"
 1041      45 43 35 42 
 1041      49 54 53 00 
 1042 0172 02          	.byte 0x2
 1043 0173 03          	.byte 0x3
 1044 0174 1D 26       	.2byte 0x261d
 1045 0176 89 02 00 00 	.4byte 0x289
 1046 017a 05          	.uleb128 0x5
 1047 017b 55 33 45 49 	.asciz "U3EIE"
 1047      45 00 
 1048 0181 03          	.byte 0x3
 1049 0182 1F 26       	.2byte 0x261f
 1050 0184 06 01 00 00 	.4byte 0x106
 1051 0188 02          	.byte 0x2
 1052 0189 01          	.byte 0x1
 1053 018a 0E          	.byte 0xe
 1054 018b 02          	.byte 0x2
 1055 018c 23          	.byte 0x23
 1056 018d 00          	.uleb128 0x0
 1057 018e 05          	.uleb128 0x5
 1058 018f 55 33 52 58 	.asciz "U3RXIE"
 1058      49 45 00 
 1059 0196 03          	.byte 0x3
 1060 0197 20 26       	.2byte 0x2620
 1061 0199 06 01 00 00 	.4byte 0x106
 1062 019d 02          	.byte 0x2
 1063 019e 01          	.byte 0x1
 1064 019f 0D          	.byte 0xd
 1065 01a0 02          	.byte 0x2
 1066 01a1 23          	.byte 0x23
 1067 01a2 00          	.uleb128 0x0
 1068 01a3 05          	.uleb128 0x5
 1069 01a4 55 33 54 58 	.asciz "U3TXIE"
 1069      49 45 00 
 1070 01ab 03          	.byte 0x3
 1071 01ac 21 26       	.2byte 0x2621
 1072 01ae 06 01 00 00 	.4byte 0x106
 1073 01b2 02          	.byte 0x2
 1074 01b3 01          	.byte 0x1
 1075 01b4 0C          	.byte 0xc
 1076 01b5 02          	.byte 0x2
 1077 01b6 23          	.byte 0x23
MPLAB XC16 ASSEMBLY Listing:   			page 36


 1078 01b7 00          	.uleb128 0x0
 1079 01b8 05          	.uleb128 0x5
 1080 01b9 55 53 42 31 	.asciz "USB1IE"
 1080      49 45 00 
 1081 01c0 03          	.byte 0x3
 1082 01c1 23 26       	.2byte 0x2623
 1083 01c3 06 01 00 00 	.4byte 0x106
 1084 01c7 02          	.byte 0x2
 1085 01c8 01          	.byte 0x1
 1086 01c9 09          	.byte 0x9
 1087 01ca 02          	.byte 0x2
 1088 01cb 23          	.byte 0x23
 1089 01cc 00          	.uleb128 0x0
 1090 01cd 05          	.uleb128 0x5
 1091 01ce 55 34 45 49 	.asciz "U4EIE"
 1091      45 00 
 1092 01d4 03          	.byte 0x3
 1093 01d5 24 26       	.2byte 0x2624
 1094 01d7 06 01 00 00 	.4byte 0x106
 1095 01db 02          	.byte 0x2
 1096 01dc 01          	.byte 0x1
 1097 01dd 08          	.byte 0x8
 1098 01de 02          	.byte 0x2
 1099 01df 23          	.byte 0x23
 1100 01e0 00          	.uleb128 0x0
 1101 01e1 05          	.uleb128 0x5
 1102 01e2 55 34 52 58 	.asciz "U4RXIE"
 1102      49 45 00 
 1103 01e9 03          	.byte 0x3
 1104 01ea 25 26       	.2byte 0x2625
 1105 01ec 06 01 00 00 	.4byte 0x106
 1106 01f0 02          	.byte 0x2
 1107 01f1 01          	.byte 0x1
 1108 01f2 07          	.byte 0x7
 1109 01f3 02          	.byte 0x2
 1110 01f4 23          	.byte 0x23
 1111 01f5 00          	.uleb128 0x0
 1112 01f6 05          	.uleb128 0x5
 1113 01f7 55 34 54 58 	.asciz "U4TXIE"
 1113      49 45 00 
 1114 01fe 03          	.byte 0x3
 1115 01ff 26 26       	.2byte 0x2626
 1116 0201 06 01 00 00 	.4byte 0x106
 1117 0205 02          	.byte 0x2
 1118 0206 01          	.byte 0x1
 1119 0207 06          	.byte 0x6
 1120 0208 02          	.byte 0x2
 1121 0209 23          	.byte 0x23
 1122 020a 00          	.uleb128 0x0
 1123 020b 05          	.uleb128 0x5
 1124 020c 53 50 49 33 	.asciz "SPI3EIE"
 1124      45 49 45 00 
 1125 0214 03          	.byte 0x3
 1126 0215 27 26       	.2byte 0x2627
 1127 0217 06 01 00 00 	.4byte 0x106
 1128 021b 02          	.byte 0x2
 1129 021c 01          	.byte 0x1
MPLAB XC16 ASSEMBLY Listing:   			page 37


 1130 021d 05          	.byte 0x5
 1131 021e 02          	.byte 0x2
 1132 021f 23          	.byte 0x23
 1133 0220 00          	.uleb128 0x0
 1134 0221 05          	.uleb128 0x5
 1135 0222 53 50 49 33 	.asciz "SPI3IE"
 1135      49 45 00 
 1136 0229 03          	.byte 0x3
 1137 022a 28 26       	.2byte 0x2628
 1138 022c 06 01 00 00 	.4byte 0x106
 1139 0230 02          	.byte 0x2
 1140 0231 01          	.byte 0x1
 1141 0232 04          	.byte 0x4
 1142 0233 02          	.byte 0x2
 1143 0234 23          	.byte 0x23
 1144 0235 00          	.uleb128 0x0
 1145 0236 05          	.uleb128 0x5
 1146 0237 4F 43 39 49 	.asciz "OC9IE"
 1146      45 00 
 1147 023d 03          	.byte 0x3
 1148 023e 29 26       	.2byte 0x2629
 1149 0240 06 01 00 00 	.4byte 0x106
 1150 0244 02          	.byte 0x2
 1151 0245 01          	.byte 0x1
 1152 0246 03          	.byte 0x3
 1153 0247 02          	.byte 0x2
 1154 0248 23          	.byte 0x23
 1155 0249 00          	.uleb128 0x0
 1156 024a 05          	.uleb128 0x5
 1157 024b 49 43 39 49 	.asciz "IC9IE"
 1157      45 00 
 1158 0251 03          	.byte 0x3
 1159 0252 2A 26       	.2byte 0x262a
 1160 0254 06 01 00 00 	.4byte 0x106
 1161 0258 02          	.byte 0x2
 1162 0259 01          	.byte 0x1
 1163 025a 02          	.byte 0x2
 1164 025b 02          	.byte 0x2
 1165 025c 23          	.byte 0x23
 1166 025d 00          	.uleb128 0x0
 1167 025e 05          	.uleb128 0x5
 1168 025f 50 57 4D 31 	.asciz "PWM1IE"
 1168      49 45 00 
 1169 0266 03          	.byte 0x3
 1170 0267 2B 26       	.2byte 0x262b
 1171 0269 06 01 00 00 	.4byte 0x106
 1172 026d 02          	.byte 0x2
 1173 026e 01          	.byte 0x1
 1174 026f 01          	.byte 0x1
 1175 0270 02          	.byte 0x2
 1176 0271 23          	.byte 0x23
 1177 0272 00          	.uleb128 0x0
 1178 0273 05          	.uleb128 0x5
 1179 0274 50 57 4D 32 	.asciz "PWM2IE"
 1179      49 45 00 
 1180 027b 03          	.byte 0x3
 1181 027c 2C 26       	.2byte 0x262c
MPLAB XC16 ASSEMBLY Listing:   			page 38


 1182 027e 06 01 00 00 	.4byte 0x106
 1183 0282 02          	.byte 0x2
 1184 0283 01          	.byte 0x1
 1185 0284 10          	.byte 0x10
 1186 0285 02          	.byte 0x2
 1187 0286 23          	.byte 0x23
 1188 0287 00          	.uleb128 0x0
 1189 0288 00          	.byte 0x0
 1190 0289 06          	.uleb128 0x6
 1191 028a 49 45 43 35 	.asciz "IEC5BITS"
 1191      42 49 54 53 
 1191      00 
 1192 0293 03          	.byte 0x3
 1193 0294 2D 26       	.2byte 0x262d
 1194 0296 65 01 00 00 	.4byte 0x165
 1195 029a 07          	.uleb128 0x7
 1196 029b 02          	.byte 0x2
 1197 029c 04          	.byte 0x4
 1198 029d CF          	.byte 0xcf
 1199 029e C8 05 00 00 	.4byte 0x5c8
 1200 02a2 08          	.uleb128 0x8
 1201 02a3 45 56 45 4E 	.asciz "EVENT_NONE"
 1201      54 5F 4E 4F 
 1201      4E 45 00 
 1202 02ae 00          	.sleb128 0
 1203 02af 08          	.uleb128 0x8
 1204 02b0 45 56 45 4E 	.asciz "EVENT_DEVICE_STACK_BASE"
 1204      54 5F 44 45 
 1204      56 49 43 45 
 1204      5F 53 54 41 
 1204      43 4B 5F 42 
 1204      41 53 45 00 
 1205 02c8 01          	.sleb128 1
 1206 02c9 08          	.uleb128 0x8
 1207 02ca 45 56 45 4E 	.asciz "EVENT_HOST_STACK_BASE"
 1207      54 5F 48 4F 
 1207      53 54 5F 53 
 1207      54 41 43 4B 
 1207      5F 42 41 53 
 1207      45 00 
 1208 02e0 E4 00       	.sleb128 100
 1209 02e2 08          	.uleb128 0x8
 1210 02e3 45 56 45 4E 	.asciz "EVENT_HUB_ATTACH"
 1210      54 5F 48 55 
 1210      42 5F 41 54 
 1210      54 41 43 48 
 1210      00 
 1211 02f4 E5 00       	.sleb128 101
 1212 02f6 08          	.uleb128 0x8
 1213 02f7 45 56 45 4E 	.asciz "EVENT_STALL"
 1213      54 5F 53 54 
 1213      41 4C 4C 00 
 1214 0303 E6 00       	.sleb128 102
 1215 0305 08          	.uleb128 0x8
 1216 0306 45 56 45 4E 	.asciz "EVENT_VBUS_SES_REQUEST"
 1216      54 5F 56 42 
 1216      55 53 5F 53 
MPLAB XC16 ASSEMBLY Listing:   			page 39


 1216      45 53 5F 52 
 1216      45 51 55 45 
 1216      53 54 00 
 1217 031d E7 00       	.sleb128 103
 1218 031f 08          	.uleb128 0x8
 1219 0320 45 56 45 4E 	.asciz "EVENT_VBUS_OVERCURRENT"
 1219      54 5F 56 42 
 1219      55 53 5F 4F 
 1219      56 45 52 43 
 1219      55 52 52 45 
 1219      4E 54 00 
 1220 0337 E8 00       	.sleb128 104
 1221 0339 08          	.uleb128 0x8
 1222 033a 45 56 45 4E 	.asciz "EVENT_VBUS_REQUEST_POWER"
 1222      54 5F 56 42 
 1222      55 53 5F 52 
 1222      45 51 55 45 
 1222      53 54 5F 50 
 1222      4F 57 45 52 
 1222      00 
 1223 0353 E9 00       	.sleb128 105
 1224 0355 08          	.uleb128 0x8
 1225 0356 45 56 45 4E 	.asciz "EVENT_VBUS_RELEASE_POWER"
 1225      54 5F 56 42 
 1225      55 53 5F 52 
 1225      45 4C 45 41 
 1225      53 45 5F 50 
 1225      4F 57 45 52 
 1225      00 
 1226 036f EA 00       	.sleb128 106
 1227 0371 08          	.uleb128 0x8
 1228 0372 45 56 45 4E 	.asciz "EVENT_VBUS_POWER_AVAILABLE"
 1228      54 5F 56 42 
 1228      55 53 5F 50 
 1228      4F 57 45 52 
 1228      5F 41 56 41 
 1228      49 4C 41 42 
 1228      4C 45 00 
 1229 038d EB 00       	.sleb128 107
 1230 038f 08          	.uleb128 0x8
 1231 0390 45 56 45 4E 	.asciz "EVENT_UNSUPPORTED_DEVICE"
 1231      54 5F 55 4E 
 1231      53 55 50 50 
 1231      4F 52 54 45 
 1231      44 5F 44 45 
 1231      56 49 43 45 
 1231      00 
 1232 03a9 EC 00       	.sleb128 108
 1233 03ab 08          	.uleb128 0x8
 1234 03ac 45 56 45 4E 	.asciz "EVENT_CANNOT_ENUMERATE"
 1234      54 5F 43 41 
 1234      4E 4E 4F 54 
 1234      5F 45 4E 55 
 1234      4D 45 52 41 
 1234      54 45 00 
 1235 03c3 ED 00       	.sleb128 109
 1236 03c5 08          	.uleb128 0x8
MPLAB XC16 ASSEMBLY Listing:   			page 40


 1237 03c6 45 56 45 4E 	.asciz "EVENT_CLIENT_INIT_ERROR"
 1237      54 5F 43 4C 
 1237      49 45 4E 54 
 1237      5F 49 4E 49 
 1237      54 5F 45 52 
 1237      52 4F 52 00 
 1238 03de EE 00       	.sleb128 110
 1239 03e0 08          	.uleb128 0x8
 1240 03e1 45 56 45 4E 	.asciz "EVENT_OUT_OF_MEMORY"
 1240      54 5F 4F 55 
 1240      54 5F 4F 46 
 1240      5F 4D 45 4D 
 1240      4F 52 59 00 
 1241 03f5 EF 00       	.sleb128 111
 1242 03f7 08          	.uleb128 0x8
 1243 03f8 45 56 45 4E 	.asciz "EVENT_UNSPECIFIED_ERROR"
 1243      54 5F 55 4E 
 1243      53 50 45 43 
 1243      49 46 49 45 
 1243      44 5F 45 52 
 1243      52 4F 52 00 
 1244 0410 F0 00       	.sleb128 112
 1245 0412 08          	.uleb128 0x8
 1246 0413 45 56 45 4E 	.asciz "EVENT_DETACH"
 1246      54 5F 44 45 
 1246      54 41 43 48 
 1246      00 
 1247 0420 F1 00       	.sleb128 113
 1248 0422 08          	.uleb128 0x8
 1249 0423 45 56 45 4E 	.asciz "EVENT_TRANSFER"
 1249      54 5F 54 52 
 1249      41 4E 53 46 
 1249      45 52 00 
 1250 0432 F2 00       	.sleb128 114
 1251 0434 08          	.uleb128 0x8
 1252 0435 45 56 45 4E 	.asciz "EVENT_SOF"
 1252      54 5F 53 4F 
 1252      46 00 
 1253 043f F3 00       	.sleb128 115
 1254 0441 08          	.uleb128 0x8
 1255 0442 45 56 45 4E 	.asciz "EVENT_RESUME"
 1255      54 5F 52 45 
 1255      53 55 4D 45 
 1255      00 
 1256 044f F4 00       	.sleb128 116
 1257 0451 08          	.uleb128 0x8
 1258 0452 45 56 45 4E 	.asciz "EVENT_SUSPEND"
 1258      54 5F 53 55 
 1258      53 50 45 4E 
 1258      44 00 
 1259 0460 F5 00       	.sleb128 117
 1260 0462 08          	.uleb128 0x8
 1261 0463 45 56 45 4E 	.asciz "EVENT_RESET"
 1261      54 5F 52 45 
 1261      53 45 54 00 
 1262 046f F6 00       	.sleb128 118
 1263 0471 08          	.uleb128 0x8
MPLAB XC16 ASSEMBLY Listing:   			page 41


 1264 0472 45 56 45 4E 	.asciz "EVENT_DATA_ISOC_READ"
 1264      54 5F 44 41 
 1264      54 41 5F 49 
 1264      53 4F 43 5F 
 1264      52 45 41 44 
 1264      00 
 1265 0487 F7 00       	.sleb128 119
 1266 0489 08          	.uleb128 0x8
 1267 048a 45 56 45 4E 	.asciz "EVENT_DATA_ISOC_WRITE"
 1267      54 5F 44 41 
 1267      54 41 5F 49 
 1267      53 4F 43 5F 
 1267      57 52 49 54 
 1267      45 00 
 1268 04a0 F8 00       	.sleb128 120
 1269 04a2 08          	.uleb128 0x8
 1270 04a3 45 56 45 4E 	.asciz "EVENT_OVERRIDE_CLIENT_DRIVER_SELECTION"
 1270      54 5F 4F 56 
 1270      45 52 52 49 
 1270      44 45 5F 43 
 1270      4C 49 45 4E 
 1270      54 5F 44 52 
 1270      49 56 45 52 
 1270      5F 53 45 4C 
 1270      45 43 54 49 
 1271 04ca F9 00       	.sleb128 121
 1272 04cc 08          	.uleb128 0x8
 1273 04cd 45 56 45 4E 	.asciz "EVENT_1MS"
 1273      54 5F 31 4D 
 1273      53 00 
 1274 04d7 FA 00       	.sleb128 122
 1275 04d9 08          	.uleb128 0x8
 1276 04da 45 56 45 4E 	.asciz "EVENT_ALT_INTERFACE"
 1276      54 5F 41 4C 
 1276      54 5F 49 4E 
 1276      54 45 52 46 
 1276      41 43 45 00 
 1277 04ee FB 00       	.sleb128 123
 1278 04f0 08          	.uleb128 0x8
 1279 04f1 45 56 45 4E 	.asciz "EVENT_HOLD_BEFORE_CONFIGURATION"
 1279      54 5F 48 4F 
 1279      4C 44 5F 42 
 1279      45 46 4F 52 
 1279      45 5F 43 4F 
 1279      4E 46 49 47 
 1279      55 52 41 54 
 1279      49 4F 4E 00 
 1280 0511 FC 00       	.sleb128 124
 1281 0513 08          	.uleb128 0x8
 1282 0514 45 56 45 4E 	.asciz "EVENT_GENERIC_BASE"
 1282      54 5F 47 45 
 1282      4E 45 52 49 
 1282      43 5F 42 41 
 1282      53 45 00 
 1283 0527 90 03       	.sleb128 400
 1284 0529 08          	.uleb128 0x8
 1285 052a 45 56 45 4E 	.asciz "EVENT_MSD_BASE"
MPLAB XC16 ASSEMBLY Listing:   			page 42


 1285      54 5F 4D 53 
 1285      44 5F 42 41 
 1285      53 45 00 
 1286 0539 F4 03       	.sleb128 500
 1287 053b 08          	.uleb128 0x8
 1288 053c 45 56 45 4E 	.asciz "EVENT_HID_BASE"
 1288      54 5F 48 49 
 1288      44 5F 42 41 
 1288      53 45 00 
 1289 054b D8 04       	.sleb128 600
 1290 054d 08          	.uleb128 0x8
 1291 054e 45 56 45 4E 	.asciz "EVENT_PRINTER_BASE"
 1291      54 5F 50 52 
 1291      49 4E 54 45 
 1291      52 5F 42 41 
 1291      53 45 00 
 1292 0561 BC 05       	.sleb128 700
 1293 0563 08          	.uleb128 0x8
 1294 0564 45 56 45 4E 	.asciz "EVENT_CDC_BASE"
 1294      54 5F 43 44 
 1294      43 5F 42 41 
 1294      53 45 00 
 1295 0573 A0 06       	.sleb128 800
 1296 0575 08          	.uleb128 0x8
 1297 0576 45 56 45 4E 	.asciz "EVENT_CHARGER_BASE"
 1297      54 5F 43 48 
 1297      41 52 47 45 
 1297      52 5F 42 41 
 1297      53 45 00 
 1298 0589 84 07       	.sleb128 900
 1299 058b 08          	.uleb128 0x8
 1300 058c 45 56 45 4E 	.asciz "EVENT_AUDIO_BASE"
 1300      54 5F 41 55 
 1300      44 49 4F 5F 
 1300      42 41 53 45 
 1300      00 
 1301 059d E8 07       	.sleb128 1000
 1302 059f 08          	.uleb128 0x8
 1303 05a0 45 56 45 4E 	.asciz "EVENT_USER_BASE"
 1303      54 5F 55 53 
 1303      45 52 5F 42 
 1303      41 53 45 00 
 1304 05b0 90 CE 00    	.sleb128 10000
 1305 05b3 08          	.uleb128 0x8
 1306 05b4 45 56 45 4E 	.asciz "EVENT_BUS_ERROR"
 1306      54 5F 42 55 
 1306      53 5F 45 52 
 1306      52 4F 52 00 
 1307 05c4 FF FF 01    	.sleb128 32767
 1308 05c7 00          	.byte 0x0
 1309 05c8 06          	.uleb128 0x6
 1310 05c9 55 53 42 5F 	.asciz "USB_EVENT"
 1310      45 56 45 4E 
 1310      54 00 
 1311 05d3 04          	.byte 0x4
 1312 05d4 67 01       	.2byte 0x167
 1313 05d6 9A 02 00 00 	.4byte 0x29a
MPLAB XC16 ASSEMBLY Listing:   			page 43


 1314 05da 02          	.uleb128 0x2
 1315 05db 01          	.byte 0x1
 1316 05dc 02          	.byte 0x2
 1317 05dd 5F 42 6F 6F 	.asciz "_Bool"
 1317      6C 00 
 1318 05e3 09          	.uleb128 0x9
 1319 05e4 02          	.byte 0x2
 1320 05e5 0A          	.uleb128 0xa
 1321 05e6 08          	.byte 0x8
 1322 05e7 05          	.byte 0x5
 1323 05e8 0D 01       	.2byte 0x10d
 1324 05ea 49 06 00 00 	.4byte 0x649
 1325 05ee 0B          	.uleb128 0xb
 1326 05ef 00 00 00 00 	.4byte .LASF0
 1327 05f3 05          	.byte 0x5
 1328 05f4 0F 01       	.2byte 0x10f
 1329 05f6 E6 00 00 00 	.4byte 0xe6
 1330 05fa 02          	.byte 0x2
 1331 05fb 23          	.byte 0x23
 1332 05fc 00          	.uleb128 0x0
 1333 05fd 0C          	.uleb128 0xc
 1334 05fe 62 52 65 71 	.asciz "bRequest"
 1334      75 65 73 74 
 1334      00 
 1335 0607 05          	.byte 0x5
 1336 0608 10 01       	.2byte 0x110
 1337 060a E6 00 00 00 	.4byte 0xe6
 1338 060e 02          	.byte 0x2
 1339 060f 23          	.byte 0x23
 1340 0610 01          	.uleb128 0x1
 1341 0611 0C          	.uleb128 0xc
 1342 0612 77 56 61 6C 	.asciz "wValue"
 1342      75 65 00 
 1343 0619 05          	.byte 0x5
 1344 061a 11 01       	.2byte 0x111
 1345 061c 06 01 00 00 	.4byte 0x106
 1346 0620 02          	.byte 0x2
 1347 0621 23          	.byte 0x23
 1348 0622 02          	.uleb128 0x2
 1349 0623 0C          	.uleb128 0xc
 1350 0624 77 49 6E 64 	.asciz "wIndex"
 1350      65 78 00 
 1351 062b 05          	.byte 0x5
 1352 062c 12 01       	.2byte 0x112
 1353 062e 06 01 00 00 	.4byte 0x106
 1354 0632 02          	.byte 0x2
 1355 0633 23          	.byte 0x23
 1356 0634 04          	.uleb128 0x4
 1357 0635 0C          	.uleb128 0xc
 1358 0636 77 4C 65 6E 	.asciz "wLength"
 1358      67 74 68 00 
 1359 063e 05          	.byte 0x5
 1360 063f 13 01       	.2byte 0x113
 1361 0641 06 01 00 00 	.4byte 0x106
 1362 0645 02          	.byte 0x2
 1363 0646 23          	.byte 0x23
 1364 0647 06          	.uleb128 0x6
MPLAB XC16 ASSEMBLY Listing:   			page 44


 1365 0648 00          	.byte 0x0
 1366 0649 0A          	.uleb128 0xa
 1367 064a 02          	.byte 0x2
 1368 064b 05          	.byte 0x5
 1369 064c 1D 01       	.2byte 0x11d
 1370 064e 6F 06 00 00 	.4byte 0x66f
 1371 0652 0C          	.uleb128 0xc
 1372 0653 4C 42 00    	.asciz "LB"
 1373 0656 05          	.byte 0x5
 1374 0657 1F 01       	.2byte 0x11f
 1375 0659 E6 00 00 00 	.4byte 0xe6
 1376 065d 02          	.byte 0x2
 1377 065e 23          	.byte 0x23
 1378 065f 00          	.uleb128 0x0
 1379 0660 0C          	.uleb128 0xc
 1380 0661 48 42 00    	.asciz "HB"
 1381 0664 05          	.byte 0x5
 1382 0665 20 01       	.2byte 0x120
 1383 0667 E6 00 00 00 	.4byte 0xe6
 1384 066b 02          	.byte 0x2
 1385 066c 23          	.byte 0x23
 1386 066d 01          	.uleb128 0x1
 1387 066e 00          	.byte 0x0
 1388 066f 0D          	.uleb128 0xd
 1389 0670 02          	.byte 0x2
 1390 0671 05          	.byte 0x5
 1391 0672 19 01       	.2byte 0x119
 1392 0674 9C 06 00 00 	.4byte 0x69c
 1393 0678 0E          	.uleb128 0xe
 1394 0679 56 61 6C 00 	.asciz "Val"
 1395 067d 05          	.byte 0x5
 1396 067e 1B 01       	.2byte 0x11b
 1397 0680 06 01 00 00 	.4byte 0x106
 1398 0684 0E          	.uleb128 0xe
 1399 0685 76 00       	.asciz "v"
 1400 0687 05          	.byte 0x5
 1401 0688 1C 01       	.2byte 0x11c
 1402 068a 9C 06 00 00 	.4byte 0x69c
 1403 068e 0E          	.uleb128 0xe
 1404 068f 62 79 74 65 	.asciz "byte"
 1404      00 
 1405 0694 05          	.byte 0x5
 1406 0695 21 01       	.2byte 0x121
 1407 0697 49 06 00 00 	.4byte 0x649
 1408 069b 00          	.byte 0x0
 1409 069c 0F          	.uleb128 0xf
 1410 069d E6 00 00 00 	.4byte 0xe6
 1411 06a1 AC 06 00 00 	.4byte 0x6ac
 1412 06a5 10          	.uleb128 0x10
 1413 06a6 16 01 00 00 	.4byte 0x116
 1414 06aa 01          	.byte 0x1
 1415 06ab 00          	.byte 0x0
 1416 06ac 0A          	.uleb128 0xa
 1417 06ad 02          	.byte 0x2
 1418 06ae 05          	.byte 0x5
 1419 06af 28 01       	.2byte 0x128
 1420 06b1 D2 06 00 00 	.4byte 0x6d2
MPLAB XC16 ASSEMBLY Listing:   			page 45


 1421 06b5 0C          	.uleb128 0xc
 1422 06b6 4C 42 00    	.asciz "LB"
 1423 06b9 05          	.byte 0x5
 1424 06ba 2A 01       	.2byte 0x12a
 1425 06bc E6 00 00 00 	.4byte 0xe6
 1426 06c0 02          	.byte 0x2
 1427 06c1 23          	.byte 0x23
 1428 06c2 00          	.uleb128 0x0
 1429 06c3 0C          	.uleb128 0xc
 1430 06c4 48 42 00    	.asciz "HB"
 1431 06c7 05          	.byte 0x5
 1432 06c8 2B 01       	.2byte 0x12b
 1433 06ca E6 00 00 00 	.4byte 0xe6
 1434 06ce 02          	.byte 0x2
 1435 06cf 23          	.byte 0x23
 1436 06d0 01          	.uleb128 0x1
 1437 06d1 00          	.byte 0x0
 1438 06d2 0D          	.uleb128 0xd
 1439 06d3 02          	.byte 0x2
 1440 06d4 05          	.byte 0x5
 1441 06d5 24 01       	.2byte 0x124
 1442 06d7 FF 06 00 00 	.4byte 0x6ff
 1443 06db 0E          	.uleb128 0xe
 1444 06dc 56 61 6C 00 	.asciz "Val"
 1445 06e0 05          	.byte 0x5
 1446 06e1 26 01       	.2byte 0x126
 1447 06e3 06 01 00 00 	.4byte 0x106
 1448 06e7 0E          	.uleb128 0xe
 1449 06e8 76 00       	.asciz "v"
 1450 06ea 05          	.byte 0x5
 1451 06eb 27 01       	.2byte 0x127
 1452 06ed 9C 06 00 00 	.4byte 0x69c
 1453 06f1 0E          	.uleb128 0xe
 1454 06f2 62 79 74 65 	.asciz "byte"
 1454      00 
 1455 06f7 05          	.byte 0x5
 1456 06f8 2C 01       	.2byte 0x12c
 1457 06fa AC 06 00 00 	.4byte 0x6ac
 1458 06fe 00          	.byte 0x0
 1459 06ff 0A          	.uleb128 0xa
 1460 0700 02          	.byte 0x2
 1461 0701 05          	.byte 0x5
 1462 0702 33 01       	.2byte 0x133
 1463 0704 25 07 00 00 	.4byte 0x725
 1464 0708 0C          	.uleb128 0xc
 1465 0709 4C 42 00    	.asciz "LB"
 1466 070c 05          	.byte 0x5
 1467 070d 35 01       	.2byte 0x135
 1468 070f E6 00 00 00 	.4byte 0xe6
 1469 0713 02          	.byte 0x2
 1470 0714 23          	.byte 0x23
 1471 0715 00          	.uleb128 0x0
 1472 0716 0C          	.uleb128 0xc
 1473 0717 48 42 00    	.asciz "HB"
 1474 071a 05          	.byte 0x5
 1475 071b 36 01       	.2byte 0x136
 1476 071d E6 00 00 00 	.4byte 0xe6
MPLAB XC16 ASSEMBLY Listing:   			page 46


 1477 0721 02          	.byte 0x2
 1478 0722 23          	.byte 0x23
 1479 0723 01          	.uleb128 0x1
 1480 0724 00          	.byte 0x0
 1481 0725 0D          	.uleb128 0xd
 1482 0726 02          	.byte 0x2
 1483 0727 05          	.byte 0x5
 1484 0728 2F 01       	.2byte 0x12f
 1485 072a 52 07 00 00 	.4byte 0x752
 1486 072e 0E          	.uleb128 0xe
 1487 072f 56 61 6C 00 	.asciz "Val"
 1488 0733 05          	.byte 0x5
 1489 0734 31 01       	.2byte 0x131
 1490 0736 06 01 00 00 	.4byte 0x106
 1491 073a 0E          	.uleb128 0xe
 1492 073b 76 00       	.asciz "v"
 1493 073d 05          	.byte 0x5
 1494 073e 32 01       	.2byte 0x132
 1495 0740 9C 06 00 00 	.4byte 0x69c
 1496 0744 0E          	.uleb128 0xe
 1497 0745 62 79 74 65 	.asciz "byte"
 1497      00 
 1498 074a 05          	.byte 0x5
 1499 074b 37 01       	.2byte 0x137
 1500 074d FF 06 00 00 	.4byte 0x6ff
 1501 0751 00          	.byte 0x0
 1502 0752 0A          	.uleb128 0xa
 1503 0753 08          	.byte 0x8
 1504 0754 05          	.byte 0x5
 1505 0755 15 01       	.2byte 0x115
 1506 0757 96 07 00 00 	.4byte 0x796
 1507 075b 0C          	.uleb128 0xc
 1508 075c 57 5F 56 61 	.asciz "W_Value"
 1508      6C 75 65 00 
 1509 0764 05          	.byte 0x5
 1510 0765 22 01       	.2byte 0x122
 1511 0767 6F 06 00 00 	.4byte 0x66f
 1512 076b 02          	.byte 0x2
 1513 076c 23          	.byte 0x23
 1514 076d 02          	.uleb128 0x2
 1515 076e 0C          	.uleb128 0xc
 1516 076f 57 5F 49 6E 	.asciz "W_Index"
 1516      64 65 78 00 
 1517 0777 05          	.byte 0x5
 1518 0778 2D 01       	.2byte 0x12d
 1519 077a D2 06 00 00 	.4byte 0x6d2
 1520 077e 02          	.byte 0x2
 1521 077f 23          	.byte 0x23
 1522 0780 04          	.uleb128 0x4
 1523 0781 0C          	.uleb128 0xc
 1524 0782 57 5F 4C 65 	.asciz "W_Length"
 1524      6E 67 74 68 
 1524      00 
 1525 078b 05          	.byte 0x5
 1526 078c 38 01       	.2byte 0x138
 1527 078e 25 07 00 00 	.4byte 0x725
 1528 0792 02          	.byte 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 47


 1529 0793 23          	.byte 0x23
 1530 0794 06          	.uleb128 0x6
 1531 0795 00          	.byte 0x0
 1532 0796 0A          	.uleb128 0xa
 1533 0797 08          	.byte 0x8
 1534 0798 05          	.byte 0x5
 1535 0799 3A 01       	.2byte 0x13a
 1536 079b FC 07 00 00 	.4byte 0x7fc
 1537 079f 05          	.uleb128 0x5
 1538 07a0 52 65 63 69 	.asciz "Recipient"
 1538      70 69 65 6E 
 1538      74 00 
 1539 07aa 05          	.byte 0x5
 1540 07ab 3C 01       	.2byte 0x13c
 1541 07ad 16 01 00 00 	.4byte 0x116
 1542 07b1 02          	.byte 0x2
 1543 07b2 05          	.byte 0x5
 1544 07b3 0B          	.byte 0xb
 1545 07b4 02          	.byte 0x2
 1546 07b5 23          	.byte 0x23
 1547 07b6 00          	.uleb128 0x0
 1548 07b7 05          	.uleb128 0x5
 1549 07b8 52 65 71 75 	.asciz "RequestType"
 1549      65 73 74 54 
 1549      79 70 65 00 
 1550 07c4 05          	.byte 0x5
 1551 07c5 3D 01       	.2byte 0x13d
 1552 07c7 16 01 00 00 	.4byte 0x116
 1553 07cb 02          	.byte 0x2
 1554 07cc 02          	.byte 0x2
 1555 07cd 09          	.byte 0x9
 1556 07ce 02          	.byte 0x2
 1557 07cf 23          	.byte 0x23
 1558 07d0 00          	.uleb128 0x0
 1559 07d1 05          	.uleb128 0x5
 1560 07d2 44 61 74 61 	.asciz "DataDir"
 1560      44 69 72 00 
 1561 07da 05          	.byte 0x5
 1562 07db 3E 01       	.2byte 0x13e
 1563 07dd 16 01 00 00 	.4byte 0x116
 1564 07e1 02          	.byte 0x2
 1565 07e2 01          	.byte 0x1
 1566 07e3 08          	.byte 0x8
 1567 07e4 02          	.byte 0x2
 1568 07e5 23          	.byte 0x23
 1569 07e6 00          	.uleb128 0x0
 1570 07e7 0C          	.uleb128 0xc
 1571 07e8 62 46 65 61 	.asciz "bFeature"
 1571      74 75 72 65 
 1571      00 
 1572 07f1 05          	.byte 0x5
 1573 07f2 40 01       	.2byte 0x140
 1574 07f4 E6 00 00 00 	.4byte 0xe6
 1575 07f8 02          	.byte 0x2
 1576 07f9 23          	.byte 0x23
 1577 07fa 02          	.uleb128 0x2
 1578 07fb 00          	.byte 0x0
MPLAB XC16 ASSEMBLY Listing:   			page 48


 1579 07fc 0A          	.uleb128 0xa
 1580 07fd 01          	.byte 0x1
 1581 07fe 05          	.byte 0x5
 1582 07ff 4C 01       	.2byte 0x14c
 1583 0801 49 08 00 00 	.4byte 0x849
 1584 0805 05          	.uleb128 0x5
 1585 0806 72 65 63 69 	.asciz "recipient"
 1585      70 69 65 6E 
 1585      74 00 
 1586 0810 05          	.byte 0x5
 1587 0811 4E 01       	.2byte 0x14e
 1588 0813 E6 00 00 00 	.4byte 0xe6
 1589 0817 01          	.byte 0x1
 1590 0818 05          	.byte 0x5
 1591 0819 03          	.byte 0x3
 1592 081a 02          	.byte 0x2
 1593 081b 23          	.byte 0x23
 1594 081c 00          	.uleb128 0x0
 1595 081d 05          	.uleb128 0x5
 1596 081e 74 79 70 65 	.asciz "type"
 1596      00 
 1597 0823 05          	.byte 0x5
 1598 0824 4F 01       	.2byte 0x14f
 1599 0826 E6 00 00 00 	.4byte 0xe6
 1600 082a 01          	.byte 0x1
 1601 082b 02          	.byte 0x2
 1602 082c 01          	.byte 0x1
 1603 082d 02          	.byte 0x2
 1604 082e 23          	.byte 0x23
 1605 082f 00          	.uleb128 0x0
 1606 0830 05          	.uleb128 0x5
 1607 0831 64 69 72 65 	.asciz "direction"
 1607      63 74 69 6F 
 1607      6E 00 
 1608 083b 05          	.byte 0x5
 1609 083c 50 01       	.2byte 0x150
 1610 083e E6 00 00 00 	.4byte 0xe6
 1611 0842 01          	.byte 0x1
 1612 0843 01          	.byte 0x1
 1613 0844 08          	.byte 0x8
 1614 0845 02          	.byte 0x2
 1615 0846 23          	.byte 0x23
 1616 0847 00          	.uleb128 0x0
 1617 0848 00          	.byte 0x0
 1618 0849 0D          	.uleb128 0xd
 1619 084a 01          	.byte 0x1
 1620 084b 05          	.byte 0x5
 1621 084c 49 01       	.2byte 0x149
 1622 084e 64 08 00 00 	.4byte 0x864
 1623 0852 11          	.uleb128 0x11
 1624 0853 00 00 00 00 	.4byte .LASF0
 1625 0857 05          	.byte 0x5
 1626 0858 4B 01       	.2byte 0x14b
 1627 085a E6 00 00 00 	.4byte 0xe6
 1628 085e 12          	.uleb128 0x12
 1629 085f FC 07 00 00 	.4byte 0x7fc
 1630 0863 00          	.byte 0x0
MPLAB XC16 ASSEMBLY Listing:   			page 49


 1631 0864 0A          	.uleb128 0xa
 1632 0865 01          	.byte 0x1
 1633 0866 05          	.byte 0x5
 1634 0867 47 01       	.2byte 0x147
 1635 0869 85 08 00 00 	.4byte 0x885
 1636 086d 0C          	.uleb128 0xc
 1637 086e 72 65 71 75 	.asciz "requestInfo"
 1637      65 73 74 49 
 1637      6E 66 6F 00 
 1638 087a 05          	.byte 0x5
 1639 087b 52 01       	.2byte 0x152
 1640 087d 49 08 00 00 	.4byte 0x849
 1641 0881 02          	.byte 0x2
 1642 0882 23          	.byte 0x23
 1643 0883 00          	.uleb128 0x0
 1644 0884 00          	.byte 0x0
 1645 0885 0A          	.uleb128 0xa
 1646 0886 08          	.byte 0x8
 1647 0887 05          	.byte 0x5
 1648 0888 54 01       	.2byte 0x154
 1649 088a D2 08 00 00 	.4byte 0x8d2
 1650 088e 0C          	.uleb128 0xc
 1651 088f 62 44 73 63 	.asciz "bDscIndex"
 1651      49 6E 64 65 
 1651      78 00 
 1652 0899 05          	.byte 0x5
 1653 089a 58 01       	.2byte 0x158
 1654 089c E6 00 00 00 	.4byte 0xe6
 1655 08a0 02          	.byte 0x2
 1656 08a1 23          	.byte 0x23
 1657 08a2 02          	.uleb128 0x2
 1658 08a3 0C          	.uleb128 0xc
 1659 08a4 62 44 65 73 	.asciz "bDescriptorType"
 1659      63 72 69 70 
 1659      74 6F 72 54 
 1659      79 70 65 00 
 1660 08b4 05          	.byte 0x5
 1661 08b5 59 01       	.2byte 0x159
 1662 08b7 E6 00 00 00 	.4byte 0xe6
 1663 08bb 02          	.byte 0x2
 1664 08bc 23          	.byte 0x23
 1665 08bd 03          	.uleb128 0x3
 1666 08be 0C          	.uleb128 0xc
 1667 08bf 77 4C 61 6E 	.asciz "wLangID"
 1667      67 49 44 00 
 1668 08c7 05          	.byte 0x5
 1669 08c8 5A 01       	.2byte 0x15a
 1670 08ca 06 01 00 00 	.4byte 0x106
 1671 08ce 02          	.byte 0x2
 1672 08cf 23          	.byte 0x23
 1673 08d0 04          	.uleb128 0x4
 1674 08d1 00          	.byte 0x0
 1675 08d2 0A          	.uleb128 0xa
 1676 08d3 08          	.byte 0x8
 1677 08d4 05          	.byte 0x5
 1678 08d5 5E 01       	.2byte 0x15e
 1679 08d7 03 09 00 00 	.4byte 0x903
MPLAB XC16 ASSEMBLY Listing:   			page 50


 1680 08db 0C          	.uleb128 0xc
 1681 08dc 62 44 65 76 	.asciz "bDevADR"
 1681      41 44 52 00 
 1682 08e4 05          	.byte 0x5
 1683 08e5 62 01       	.2byte 0x162
 1684 08e7 E6 00 00 00 	.4byte 0xe6
 1685 08eb 02          	.byte 0x2
 1686 08ec 23          	.byte 0x23
 1687 08ed 02          	.uleb128 0x2
 1688 08ee 0C          	.uleb128 0xc
 1689 08ef 62 44 65 76 	.asciz "bDevADRH"
 1689      41 44 52 48 
 1689      00 
 1690 08f8 05          	.byte 0x5
 1691 08f9 63 01       	.2byte 0x163
 1692 08fb E6 00 00 00 	.4byte 0xe6
 1693 08ff 02          	.byte 0x2
 1694 0900 23          	.byte 0x23
 1695 0901 03          	.uleb128 0x3
 1696 0902 00          	.byte 0x0
 1697 0903 0A          	.uleb128 0xa
 1698 0904 08          	.byte 0x8
 1699 0905 05          	.byte 0x5
 1700 0906 69 01       	.2byte 0x169
 1701 0908 3F 09 00 00 	.4byte 0x93f
 1702 090c 0C          	.uleb128 0xc
 1703 090d 62 43 6F 6E 	.asciz "bConfigurationValue"
 1703      66 69 67 75 
 1703      72 61 74 69 
 1703      6F 6E 56 61 
 1703      6C 75 65 00 
 1704 0921 05          	.byte 0x5
 1705 0922 6D 01       	.2byte 0x16d
 1706 0924 E6 00 00 00 	.4byte 0xe6
 1707 0928 02          	.byte 0x2
 1708 0929 23          	.byte 0x23
 1709 092a 02          	.uleb128 0x2
 1710 092b 0C          	.uleb128 0xc
 1711 092c 62 43 66 67 	.asciz "bCfgRSD"
 1711      52 53 44 00 
 1712 0934 05          	.byte 0x5
 1713 0935 6E 01       	.2byte 0x16e
 1714 0937 E6 00 00 00 	.4byte 0xe6
 1715 093b 02          	.byte 0x2
 1716 093c 23          	.byte 0x23
 1717 093d 03          	.uleb128 0x3
 1718 093e 00          	.byte 0x0
 1719 093f 0A          	.uleb128 0xa
 1720 0940 08          	.byte 0x8
 1721 0941 05          	.byte 0x5
 1722 0942 74 01       	.2byte 0x174
 1723 0944 97 09 00 00 	.4byte 0x997
 1724 0948 0C          	.uleb128 0xc
 1725 0949 62 41 6C 74 	.asciz "bAltID"
 1725      49 44 00 
 1726 0950 05          	.byte 0x5
 1727 0951 78 01       	.2byte 0x178
MPLAB XC16 ASSEMBLY Listing:   			page 51


 1728 0953 E6 00 00 00 	.4byte 0xe6
 1729 0957 02          	.byte 0x2
 1730 0958 23          	.byte 0x23
 1731 0959 02          	.uleb128 0x2
 1732 095a 0C          	.uleb128 0xc
 1733 095b 62 41 6C 74 	.asciz "bAltID_H"
 1733      49 44 5F 48 
 1733      00 
 1734 0964 05          	.byte 0x5
 1735 0965 79 01       	.2byte 0x179
 1736 0967 E6 00 00 00 	.4byte 0xe6
 1737 096b 02          	.byte 0x2
 1738 096c 23          	.byte 0x23
 1739 096d 03          	.uleb128 0x3
 1740 096e 0C          	.uleb128 0xc
 1741 096f 62 49 6E 74 	.asciz "bIntfID"
 1741      66 49 44 00 
 1742 0977 05          	.byte 0x5
 1743 0978 7A 01       	.2byte 0x17a
 1744 097a E6 00 00 00 	.4byte 0xe6
 1745 097e 02          	.byte 0x2
 1746 097f 23          	.byte 0x23
 1747 0980 04          	.uleb128 0x4
 1748 0981 0C          	.uleb128 0xc
 1749 0982 62 49 6E 74 	.asciz "bIntfID_H"
 1749      66 49 44 5F 
 1749      48 00 
 1750 098c 05          	.byte 0x5
 1751 098d 7B 01       	.2byte 0x17b
 1752 098f E6 00 00 00 	.4byte 0xe6
 1753 0993 02          	.byte 0x2
 1754 0994 23          	.byte 0x23
 1755 0995 05          	.uleb128 0x5
 1756 0996 00          	.byte 0x0
 1757 0997 0A          	.uleb128 0xa
 1758 0998 08          	.byte 0x8
 1759 0999 05          	.byte 0x5
 1760 099a 7F 01       	.2byte 0x17f
 1761 099c C5 09 00 00 	.4byte 0x9c5
 1762 09a0 0C          	.uleb128 0xc
 1763 09a1 62 45 50 49 	.asciz "bEPID"
 1763      44 00 
 1764 09a7 05          	.byte 0x5
 1765 09a8 85 01       	.2byte 0x185
 1766 09aa E6 00 00 00 	.4byte 0xe6
 1767 09ae 02          	.byte 0x2
 1768 09af 23          	.byte 0x23
 1769 09b0 04          	.uleb128 0x4
 1770 09b1 0C          	.uleb128 0xc
 1771 09b2 62 45 50 49 	.asciz "bEPID_H"
 1771      44 5F 48 00 
 1772 09ba 05          	.byte 0x5
 1773 09bb 86 01       	.2byte 0x186
 1774 09bd E6 00 00 00 	.4byte 0xe6
 1775 09c1 02          	.byte 0x2
 1776 09c2 23          	.byte 0x23
 1777 09c3 05          	.uleb128 0x5
MPLAB XC16 ASSEMBLY Listing:   			page 52


 1778 09c4 00          	.byte 0x0
 1779 09c5 0A          	.uleb128 0xa
 1780 09c6 08          	.byte 0x8
 1781 09c7 05          	.byte 0x5
 1782 09c8 8A 01       	.2byte 0x18a
 1783 09ca F7 09 00 00 	.4byte 0x9f7
 1784 09ce 05          	.uleb128 0x5
 1785 09cf 45 50 4E 75 	.asciz "EPNum"
 1785      6D 00 
 1786 09d5 05          	.byte 0x5
 1787 09d6 90 01       	.2byte 0x190
 1788 09d8 16 01 00 00 	.4byte 0x116
 1789 09dc 02          	.byte 0x2
 1790 09dd 04          	.byte 0x4
 1791 09de 0C          	.byte 0xc
 1792 09df 02          	.byte 0x2
 1793 09e0 23          	.byte 0x23
 1794 09e1 04          	.uleb128 0x4
 1795 09e2 05          	.uleb128 0x5
 1796 09e3 45 50 44 69 	.asciz "EPDir"
 1796      72 00 
 1797 09e9 05          	.byte 0x5
 1798 09ea 92 01       	.2byte 0x192
 1799 09ec 16 01 00 00 	.4byte 0x116
 1800 09f0 02          	.byte 0x2
 1801 09f1 01          	.byte 0x1
 1802 09f2 08          	.byte 0x8
 1803 09f3 02          	.byte 0x2
 1804 09f4 23          	.byte 0x23
 1805 09f5 04          	.uleb128 0x4
 1806 09f6 00          	.byte 0x0
 1807 09f7 0D          	.uleb128 0xd
 1808 09f8 08          	.byte 0x8
 1809 09f9 05          	.byte 0x5
 1810 09fa 0A 01       	.2byte 0x10a
 1811 09fc 33 0A 00 00 	.4byte 0xa33
 1812 0a00 12          	.uleb128 0x12
 1813 0a01 E5 05 00 00 	.4byte 0x5e5
 1814 0a05 12          	.uleb128 0x12
 1815 0a06 52 07 00 00 	.4byte 0x752
 1816 0a0a 12          	.uleb128 0x12
 1817 0a0b 96 07 00 00 	.4byte 0x796
 1818 0a0f 12          	.uleb128 0x12
 1819 0a10 64 08 00 00 	.4byte 0x864
 1820 0a14 12          	.uleb128 0x12
 1821 0a15 85 08 00 00 	.4byte 0x885
 1822 0a19 12          	.uleb128 0x12
 1823 0a1a D2 08 00 00 	.4byte 0x8d2
 1824 0a1e 12          	.uleb128 0x12
 1825 0a1f 03 09 00 00 	.4byte 0x903
 1826 0a23 12          	.uleb128 0x12
 1827 0a24 3F 09 00 00 	.4byte 0x93f
 1828 0a28 12          	.uleb128 0x12
 1829 0a29 97 09 00 00 	.4byte 0x997
 1830 0a2d 12          	.uleb128 0x12
 1831 0a2e C5 09 00 00 	.4byte 0x9c5
 1832 0a32 00          	.byte 0x0
MPLAB XC16 ASSEMBLY Listing:   			page 53


 1833 0a33 06          	.uleb128 0x6
 1834 0a34 43 54 52 4C 	.asciz "CTRL_TRF_SETUP"
 1834      5F 54 52 46 
 1834      5F 53 45 54 
 1834      55 50 00 
 1835 0a43 05          	.byte 0x5
 1836 0a44 9A 01       	.2byte 0x19a
 1837 0a46 F7 09 00 00 	.4byte 0x9f7
 1838 0a4a 07          	.uleb128 0x7
 1839 0a4b 02          	.byte 0x2
 1840 0a4c 06          	.byte 0x6
 1841 0a4d 73          	.byte 0x73
 1842 0a4e BC 0A 00 00 	.4byte 0xabc
 1843 0a52 08          	.uleb128 0x8
 1844 0a53 45 56 45 4E 	.asciz "EVENT_CONFIGURED"
 1844      54 5F 43 4F 
 1844      4E 46 49 47 
 1844      55 52 45 44 
 1844      00 
 1845 0a64 01          	.sleb128 1
 1846 0a65 08          	.uleb128 0x8
 1847 0a66 45 56 45 4E 	.asciz "EVENT_SET_DESCRIPTOR"
 1847      54 5F 53 45 
 1847      54 5F 44 45 
 1847      53 43 52 49 
 1847      50 54 4F 52 
 1847      00 
 1848 0a7b 02          	.sleb128 2
 1849 0a7c 08          	.uleb128 0x8
 1850 0a7d 45 56 45 4E 	.asciz "EVENT_EP0_REQUEST"
 1850      54 5F 45 50 
 1850      30 5F 52 45 
 1850      51 55 45 53 
 1850      54 00 
 1851 0a8f 03          	.sleb128 3
 1852 0a90 08          	.uleb128 0x8
 1853 0a91 45 56 45 4E 	.asciz "EVENT_ATTACH"
 1853      54 5F 41 54 
 1853      54 41 43 48 
 1853      00 
 1854 0a9e 04          	.sleb128 4
 1855 0a9f 08          	.uleb128 0x8
 1856 0aa0 45 56 45 4E 	.asciz "EVENT_TRANSFER_TERMINATED"
 1856      54 5F 54 52 
 1856      41 4E 53 46 
 1856      45 52 5F 54 
 1856      45 52 4D 49 
 1856      4E 41 54 45 
 1856      44 00 
 1857 0aba 05          	.sleb128 5
 1858 0abb 00          	.byte 0x0
 1859 0abc 0A          	.uleb128 0xa
 1860 0abd 02          	.byte 0x2
 1861 0abe 06          	.byte 0x6
 1862 0abf BC 07       	.2byte 0x7bc
 1863 0ac1 E2 0A 00 00 	.4byte 0xae2
 1864 0ac5 0C          	.uleb128 0xc
MPLAB XC16 ASSEMBLY Listing:   			page 54


 1865 0ac6 4C 42 00    	.asciz "LB"
 1866 0ac9 06          	.byte 0x6
 1867 0aca BE 07       	.2byte 0x7be
 1868 0acc E6 00 00 00 	.4byte 0xe6
 1869 0ad0 02          	.byte 0x2
 1870 0ad1 23          	.byte 0x23
 1871 0ad2 00          	.uleb128 0x0
 1872 0ad3 0C          	.uleb128 0xc
 1873 0ad4 48 42 00    	.asciz "HB"
 1874 0ad7 06          	.byte 0x6
 1875 0ad8 BF 07       	.2byte 0x7bf
 1876 0ada E6 00 00 00 	.4byte 0xe6
 1877 0ade 02          	.byte 0x2
 1878 0adf 23          	.byte 0x23
 1879 0ae0 01          	.uleb128 0x1
 1880 0ae1 00          	.byte 0x0
 1881 0ae2 0D          	.uleb128 0xd
 1882 0ae3 02          	.byte 0x2
 1883 0ae4 06          	.byte 0x6
 1884 0ae5 B8 07       	.2byte 0x7b8
 1885 0ae7 0F 0B 00 00 	.4byte 0xb0f
 1886 0aeb 0E          	.uleb128 0xe
 1887 0aec 56 61 6C 00 	.asciz "Val"
 1888 0af0 06          	.byte 0x6
 1889 0af1 BA 07       	.2byte 0x7ba
 1890 0af3 06 01 00 00 	.4byte 0x106
 1891 0af7 0E          	.uleb128 0xe
 1892 0af8 76 00       	.asciz "v"
 1893 0afa 06          	.byte 0x6
 1894 0afb BB 07       	.2byte 0x7bb
 1895 0afd 9C 06 00 00 	.4byte 0x69c
 1896 0b01 0E          	.uleb128 0xe
 1897 0b02 62 79 74 65 	.asciz "byte"
 1897      00 
 1898 0b07 06          	.byte 0x6
 1899 0b08 C0 07       	.2byte 0x7c0
 1900 0b0a BC 0A 00 00 	.4byte 0xabc
 1901 0b0e 00          	.byte 0x0
 1902 0b0f 06          	.uleb128 0x6
 1903 0b10 75 69 6E 74 	.asciz "uint16_t_VAL"
 1903      31 36 5F 74 
 1903      5F 56 41 4C 
 1903      00 
 1904 0b1d 06          	.byte 0x6
 1905 0b1e C1 07       	.2byte 0x7c1
 1906 0b20 E2 0A 00 00 	.4byte 0xae2
 1907 0b24 0D          	.uleb128 0xd
 1908 0b25 02          	.byte 0x2
 1909 0b26 06          	.byte 0x6
 1910 0b27 C8 07       	.2byte 0x7c8
 1911 0b29 62 0B 00 00 	.4byte 0xb62
 1912 0b2d 0E          	.uleb128 0xe
 1913 0b2e 62 52 61 6D 	.asciz "bRam"
 1913      00 
 1914 0b33 06          	.byte 0x6
 1915 0b34 CC 07       	.2byte 0x7cc
 1916 0b36 62 0B 00 00 	.4byte 0xb62
MPLAB XC16 ASSEMBLY Listing:   			page 55


 1917 0b3a 0E          	.uleb128 0xe
 1918 0b3b 62 52 6F 6D 	.asciz "bRom"
 1918      00 
 1919 0b40 06          	.byte 0x6
 1920 0b41 CD 07       	.2byte 0x7cd
 1921 0b43 68 0B 00 00 	.4byte 0xb68
 1922 0b47 0E          	.uleb128 0xe
 1923 0b48 77 52 61 6D 	.asciz "wRam"
 1923      00 
 1924 0b4d 06          	.byte 0x6
 1925 0b4e CE 07       	.2byte 0x7ce
 1926 0b50 73 0B 00 00 	.4byte 0xb73
 1927 0b54 0E          	.uleb128 0xe
 1928 0b55 77 52 6F 6D 	.asciz "wRom"
 1928      00 
 1929 0b5a 06          	.byte 0x6
 1930 0b5b CF 07       	.2byte 0x7cf
 1931 0b5d 79 0B 00 00 	.4byte 0xb79
 1932 0b61 00          	.byte 0x0
 1933 0b62 13          	.uleb128 0x13
 1934 0b63 02          	.byte 0x2
 1935 0b64 E6 00 00 00 	.4byte 0xe6
 1936 0b68 13          	.uleb128 0x13
 1937 0b69 02          	.byte 0x2
 1938 0b6a 6E 0B 00 00 	.4byte 0xb6e
 1939 0b6e 14          	.uleb128 0x14
 1940 0b6f E6 00 00 00 	.4byte 0xe6
 1941 0b73 13          	.uleb128 0x13
 1942 0b74 02          	.byte 0x2
 1943 0b75 06 01 00 00 	.4byte 0x106
 1944 0b79 13          	.uleb128 0x13
 1945 0b7a 02          	.byte 0x2
 1946 0b7b 7F 0B 00 00 	.4byte 0xb7f
 1947 0b7f 14          	.uleb128 0x14
 1948 0b80 06 01 00 00 	.4byte 0x106
 1949 0b84 0A          	.uleb128 0xa
 1950 0b85 01          	.byte 0x1
 1951 0b86 06          	.byte 0x6
 1952 0b87 D3 07       	.2byte 0x7d3
 1953 0b89 E8 0B 00 00 	.4byte 0xbe8
 1954 0b8d 05          	.uleb128 0x5
 1955 0b8e 63 74 72 6C 	.asciz "ctrl_trf_mem"
 1955      5F 74 72 66 
 1955      5F 6D 65 6D 
 1955      00 
 1956 0b9b 06          	.byte 0x6
 1957 0b9c D6 07       	.2byte 0x7d6
 1958 0b9e E6 00 00 00 	.4byte 0xe6
 1959 0ba2 01          	.byte 0x1
 1960 0ba3 01          	.byte 0x1
 1961 0ba4 07          	.byte 0x7
 1962 0ba5 02          	.byte 0x2
 1963 0ba6 23          	.byte 0x23
 1964 0ba7 00          	.uleb128 0x0
 1965 0ba8 15          	.uleb128 0x15
 1966 0ba9 00 00 00 00 	.4byte .LASF1
 1967 0bad 06          	.byte 0x6
MPLAB XC16 ASSEMBLY Listing:   			page 56


 1968 0bae D7 07       	.2byte 0x7d7
 1969 0bb0 E6 00 00 00 	.4byte 0xe6
 1970 0bb4 01          	.byte 0x1
 1971 0bb5 05          	.byte 0x5
 1972 0bb6 02          	.byte 0x2
 1973 0bb7 02          	.byte 0x2
 1974 0bb8 23          	.byte 0x23
 1975 0bb9 00          	.uleb128 0x0
 1976 0bba 05          	.uleb128 0x5
 1977 0bbb 69 6E 63 6C 	.asciz "includeZero"
 1977      75 64 65 5A 
 1977      65 72 6F 00 
 1978 0bc7 06          	.byte 0x6
 1979 0bc8 DA 07       	.2byte 0x7da
 1980 0bca E6 00 00 00 	.4byte 0xe6
 1981 0bce 01          	.byte 0x1
 1982 0bcf 01          	.byte 0x1
 1983 0bd0 01          	.byte 0x1
 1984 0bd1 02          	.byte 0x2
 1985 0bd2 23          	.byte 0x23
 1986 0bd3 00          	.uleb128 0x0
 1987 0bd4 05          	.uleb128 0x5
 1988 0bd5 62 75 73 79 	.asciz "busy"
 1988      00 
 1989 0bda 06          	.byte 0x6
 1990 0bdb DC 07       	.2byte 0x7dc
 1991 0bdd E6 00 00 00 	.4byte 0xe6
 1992 0be1 01          	.byte 0x1
 1993 0be2 01          	.byte 0x1
 1994 0be3 08          	.byte 0x8
 1995 0be4 02          	.byte 0x2
 1996 0be5 23          	.byte 0x23
 1997 0be6 00          	.uleb128 0x0
 1998 0be7 00          	.byte 0x0
 1999 0be8 0D          	.uleb128 0xd
 2000 0be9 01          	.byte 0x1
 2001 0bea 06          	.byte 0x6
 2002 0beb D1 07       	.2byte 0x7d1
 2003 0bed 0B 0C 00 00 	.4byte 0xc0b
 2004 0bf1 0E          	.uleb128 0xe
 2005 0bf2 62 69 74 73 	.asciz "bits"
 2005      00 
 2006 0bf7 06          	.byte 0x6
 2007 0bf8 DD 07       	.2byte 0x7dd
 2008 0bfa 84 0B 00 00 	.4byte 0xb84
 2009 0bfe 0E          	.uleb128 0xe
 2010 0bff 56 61 6C 00 	.asciz "Val"
 2011 0c03 06          	.byte 0x6
 2012 0c04 DE 07       	.2byte 0x7de
 2013 0c06 E6 00 00 00 	.4byte 0xe6
 2014 0c0a 00          	.byte 0x0
 2015 0c0b 0A          	.uleb128 0xa
 2016 0c0c 06          	.byte 0x6
 2017 0c0d 06          	.byte 0x6
 2018 0c0e C6 07       	.2byte 0x7c6
 2019 0c10 47 0C 00 00 	.4byte 0xc47
 2020 0c14 0C          	.uleb128 0xc
MPLAB XC16 ASSEMBLY Listing:   			page 57


 2021 0c15 70 53 72 63 	.asciz "pSrc"
 2021      00 
 2022 0c1a 06          	.byte 0x6
 2023 0c1b D0 07       	.2byte 0x7d0
 2024 0c1d 24 0B 00 00 	.4byte 0xb24
 2025 0c21 02          	.byte 0x2
 2026 0c22 23          	.byte 0x23
 2027 0c23 00          	.uleb128 0x0
 2028 0c24 0C          	.uleb128 0xc
 2029 0c25 69 6E 66 6F 	.asciz "info"
 2029      00 
 2030 0c2a 06          	.byte 0x6
 2031 0c2b DF 07       	.2byte 0x7df
 2032 0c2d E8 0B 00 00 	.4byte 0xbe8
 2033 0c31 02          	.byte 0x2
 2034 0c32 23          	.byte 0x23
 2035 0c33 02          	.uleb128 0x2
 2036 0c34 0C          	.uleb128 0xc
 2037 0c35 77 43 6F 75 	.asciz "wCount"
 2037      6E 74 00 
 2038 0c3c 06          	.byte 0x6
 2039 0c3d E0 07       	.2byte 0x7e0
 2040 0c3f 0F 0B 00 00 	.4byte 0xb0f
 2041 0c43 02          	.byte 0x2
 2042 0c44 23          	.byte 0x23
 2043 0c45 04          	.uleb128 0x4
 2044 0c46 00          	.byte 0x0
 2045 0c47 06          	.uleb128 0x6
 2046 0c48 49 4E 5F 50 	.asciz "IN_PIPE"
 2046      49 50 45 00 
 2047 0c50 06          	.byte 0x6
 2048 0c51 E1 07       	.2byte 0x7e1
 2049 0c53 0B 0C 00 00 	.4byte 0xc0b
 2050 0c57 0D          	.uleb128 0xd
 2051 0c58 02          	.byte 0x2
 2052 0c59 06          	.byte 0x6
 2053 0c5a E7 07       	.2byte 0x7e7
 2054 0c5c 7B 0C 00 00 	.4byte 0xc7b
 2055 0c60 0E          	.uleb128 0xe
 2056 0c61 62 52 61 6D 	.asciz "bRam"
 2056      00 
 2057 0c66 06          	.byte 0x6
 2058 0c67 EB 07       	.2byte 0x7eb
 2059 0c69 62 0B 00 00 	.4byte 0xb62
 2060 0c6d 0E          	.uleb128 0xe
 2061 0c6e 77 52 61 6D 	.asciz "wRam"
 2061      00 
 2062 0c73 06          	.byte 0x6
 2063 0c74 EC 07       	.2byte 0x7ec
 2064 0c76 73 0B 00 00 	.4byte 0xb73
 2065 0c7a 00          	.byte 0x0
 2066 0c7b 0A          	.uleb128 0xa
 2067 0c7c 01          	.byte 0x1
 2068 0c7d 06          	.byte 0x6
 2069 0c7e F0 07       	.2byte 0x7f0
 2070 0c80 AA 0C 00 00 	.4byte 0xcaa
 2071 0c84 15          	.uleb128 0x15
MPLAB XC16 ASSEMBLY Listing:   			page 58


 2072 0c85 00 00 00 00 	.4byte .LASF1
 2073 0c89 06          	.byte 0x6
 2074 0c8a F2 07       	.2byte 0x7f2
 2075 0c8c E6 00 00 00 	.4byte 0xe6
 2076 0c90 01          	.byte 0x1
 2077 0c91 07          	.byte 0x7
 2078 0c92 01          	.byte 0x1
 2079 0c93 02          	.byte 0x2
 2080 0c94 23          	.byte 0x23
 2081 0c95 00          	.uleb128 0x0
 2082 0c96 05          	.uleb128 0x5
 2083 0c97 62 75 73 79 	.asciz "busy"
 2083      00 
 2084 0c9c 06          	.byte 0x6
 2085 0c9d F4 07       	.2byte 0x7f4
 2086 0c9f E6 00 00 00 	.4byte 0xe6
 2087 0ca3 01          	.byte 0x1
 2088 0ca4 01          	.byte 0x1
 2089 0ca5 08          	.byte 0x8
 2090 0ca6 02          	.byte 0x2
 2091 0ca7 23          	.byte 0x23
 2092 0ca8 00          	.uleb128 0x0
 2093 0ca9 00          	.byte 0x0
 2094 0caa 0D          	.uleb128 0xd
 2095 0cab 01          	.byte 0x1
 2096 0cac 06          	.byte 0x6
 2097 0cad EE 07       	.2byte 0x7ee
 2098 0caf CD 0C 00 00 	.4byte 0xccd
 2099 0cb3 0E          	.uleb128 0xe
 2100 0cb4 62 69 74 73 	.asciz "bits"
 2100      00 
 2101 0cb9 06          	.byte 0x6
 2102 0cba F5 07       	.2byte 0x7f5
 2103 0cbc 7B 0C 00 00 	.4byte 0xc7b
 2104 0cc0 0E          	.uleb128 0xe
 2105 0cc1 56 61 6C 00 	.asciz "Val"
 2106 0cc5 06          	.byte 0x6
 2107 0cc6 F6 07       	.2byte 0x7f6
 2108 0cc8 E6 00 00 00 	.4byte 0xe6
 2109 0ccc 00          	.byte 0x0
 2110 0ccd 0A          	.uleb128 0xa
 2111 0cce 07          	.byte 0x7
 2112 0ccf 06          	.byte 0x6
 2113 0cd0 E5 07       	.2byte 0x7e5
 2114 0cd2 1A 0D 00 00 	.4byte 0xd1a
 2115 0cd6 0C          	.uleb128 0xc
 2116 0cd7 70 44 73 74 	.asciz "pDst"
 2116      00 
 2117 0cdc 06          	.byte 0x6
 2118 0cdd ED 07       	.2byte 0x7ed
 2119 0cdf 57 0C 00 00 	.4byte 0xc57
 2120 0ce3 02          	.byte 0x2
 2121 0ce4 23          	.byte 0x23
 2122 0ce5 00          	.uleb128 0x0
 2123 0ce6 0C          	.uleb128 0xc
 2124 0ce7 69 6E 66 6F 	.asciz "info"
 2124      00 
MPLAB XC16 ASSEMBLY Listing:   			page 59


 2125 0cec 06          	.byte 0x6
 2126 0ced F7 07       	.2byte 0x7f7
 2127 0cef AA 0C 00 00 	.4byte 0xcaa
 2128 0cf3 02          	.byte 0x2
 2129 0cf4 23          	.byte 0x23
 2130 0cf5 02          	.uleb128 0x2
 2131 0cf6 0C          	.uleb128 0xc
 2132 0cf7 77 43 6F 75 	.asciz "wCount"
 2132      6E 74 00 
 2133 0cfe 06          	.byte 0x6
 2134 0cff F8 07       	.2byte 0x7f8
 2135 0d01 0F 0B 00 00 	.4byte 0xb0f
 2136 0d05 02          	.byte 0x2
 2137 0d06 23          	.byte 0x23
 2138 0d07 03          	.uleb128 0x3
 2139 0d08 0C          	.uleb128 0xc
 2140 0d09 70 46 75 6E 	.asciz "pFunc"
 2140      63 00 
 2141 0d0f 06          	.byte 0x6
 2142 0d10 F9 07       	.2byte 0x7f9
 2143 0d12 1C 0D 00 00 	.4byte 0xd1c
 2144 0d16 02          	.byte 0x2
 2145 0d17 23          	.byte 0x23
 2146 0d18 05          	.uleb128 0x5
 2147 0d19 00          	.byte 0x0
 2148 0d1a 16          	.uleb128 0x16
 2149 0d1b 01          	.byte 0x1
 2150 0d1c 13          	.uleb128 0x13
 2151 0d1d 02          	.byte 0x2
 2152 0d1e 1A 0D 00 00 	.4byte 0xd1a
 2153 0d22 06          	.uleb128 0x6
 2154 0d23 4F 55 54 5F 	.asciz "OUT_PIPE"
 2154      50 49 50 45 
 2154      00 
 2155 0d2c 06          	.byte 0x6
 2156 0d2d FA 07       	.2byte 0x7fa
 2157 0d2f CD 0C 00 00 	.4byte 0xccd
 2158 0d33 02          	.uleb128 0x2
 2159 0d34 02          	.byte 0x2
 2160 0d35 07          	.byte 0x7
 2161 0d36 73 68 6F 72 	.asciz "short unsigned int"
 2161      74 20 75 6E 
 2161      73 69 67 6E 
 2161      65 64 20 69 
 2161      6E 74 00 
 2162 0d49 17          	.uleb128 0x17
 2163 0d4a 01          	.byte 0x1
 2164 0d4b 07          	.byte 0x7
 2165 0d4c D2          	.byte 0xd2
 2166 0d4d 9C 0D 00 00 	.4byte 0xd9c
 2167 0d51 18          	.uleb128 0x18
 2168 0d52 42 53 54 41 	.asciz "BSTALL"
 2168      4C 4C 00 
 2169 0d59 07          	.byte 0x7
 2170 0d5a D4          	.byte 0xd4
 2171 0d5b 16 01 00 00 	.4byte 0x116
 2172 0d5f 02          	.byte 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 60


 2173 0d60 01          	.byte 0x1
 2174 0d61 0D          	.byte 0xd
 2175 0d62 02          	.byte 0x2
 2176 0d63 23          	.byte 0x23
 2177 0d64 00          	.uleb128 0x0
 2178 0d65 18          	.uleb128 0x18
 2179 0d66 44 54 53 45 	.asciz "DTSEN"
 2179      4E 00 
 2180 0d6c 07          	.byte 0x7
 2181 0d6d D5          	.byte 0xd5
 2182 0d6e 16 01 00 00 	.4byte 0x116
 2183 0d72 02          	.byte 0x2
 2184 0d73 01          	.byte 0x1
 2185 0d74 0C          	.byte 0xc
 2186 0d75 02          	.byte 0x2
 2187 0d76 23          	.byte 0x23
 2188 0d77 00          	.uleb128 0x0
 2189 0d78 18          	.uleb128 0x18
 2190 0d79 44 54 53 00 	.asciz "DTS"
 2191 0d7d 07          	.byte 0x7
 2192 0d7e D7          	.byte 0xd7
 2193 0d7f 16 01 00 00 	.4byte 0x116
 2194 0d83 02          	.byte 0x2
 2195 0d84 01          	.byte 0x1
 2196 0d85 09          	.byte 0x9
 2197 0d86 02          	.byte 0x2
 2198 0d87 23          	.byte 0x23
 2199 0d88 00          	.uleb128 0x0
 2200 0d89 18          	.uleb128 0x18
 2201 0d8a 55 4F 57 4E 	.asciz "UOWN"
 2201      00 
 2202 0d8f 07          	.byte 0x7
 2203 0d90 D8          	.byte 0xd8
 2204 0d91 16 01 00 00 	.4byte 0x116
 2205 0d95 02          	.byte 0x2
 2206 0d96 01          	.byte 0x1
 2207 0d97 08          	.byte 0x8
 2208 0d98 02          	.byte 0x2
 2209 0d99 23          	.byte 0x23
 2210 0d9a 00          	.uleb128 0x0
 2211 0d9b 00          	.byte 0x0
 2212 0d9c 17          	.uleb128 0x17
 2213 0d9d 01          	.byte 0x1
 2214 0d9e 07          	.byte 0x7
 2215 0d9f DA          	.byte 0xda
 2216 0da0 ED 0D 00 00 	.4byte 0xded
 2217 0da4 18          	.uleb128 0x18
 2218 0da5 50 49 44 30 	.asciz "PID0"
 2218      00 
 2219 0daa 07          	.byte 0x7
 2220 0dab DC          	.byte 0xdc
 2221 0dac 16 01 00 00 	.4byte 0x116
 2222 0db0 02          	.byte 0x2
 2223 0db1 01          	.byte 0x1
 2224 0db2 0D          	.byte 0xd
 2225 0db3 02          	.byte 0x2
 2226 0db4 23          	.byte 0x23
MPLAB XC16 ASSEMBLY Listing:   			page 61


 2227 0db5 00          	.uleb128 0x0
 2228 0db6 18          	.uleb128 0x18
 2229 0db7 50 49 44 31 	.asciz "PID1"
 2229      00 
 2230 0dbc 07          	.byte 0x7
 2231 0dbd DD          	.byte 0xdd
 2232 0dbe 16 01 00 00 	.4byte 0x116
 2233 0dc2 02          	.byte 0x2
 2234 0dc3 01          	.byte 0x1
 2235 0dc4 0C          	.byte 0xc
 2236 0dc5 02          	.byte 0x2
 2237 0dc6 23          	.byte 0x23
 2238 0dc7 00          	.uleb128 0x0
 2239 0dc8 18          	.uleb128 0x18
 2240 0dc9 50 49 44 32 	.asciz "PID2"
 2240      00 
 2241 0dce 07          	.byte 0x7
 2242 0dcf DE          	.byte 0xde
 2243 0dd0 16 01 00 00 	.4byte 0x116
 2244 0dd4 02          	.byte 0x2
 2245 0dd5 01          	.byte 0x1
 2246 0dd6 0B          	.byte 0xb
 2247 0dd7 02          	.byte 0x2
 2248 0dd8 23          	.byte 0x23
 2249 0dd9 00          	.uleb128 0x0
 2250 0dda 18          	.uleb128 0x18
 2251 0ddb 50 49 44 33 	.asciz "PID3"
 2251      00 
 2252 0de0 07          	.byte 0x7
 2253 0de1 DF          	.byte 0xdf
 2254 0de2 16 01 00 00 	.4byte 0x116
 2255 0de6 02          	.byte 0x2
 2256 0de7 01          	.byte 0x1
 2257 0de8 0A          	.byte 0xa
 2258 0de9 02          	.byte 0x2
 2259 0dea 23          	.byte 0x23
 2260 0deb 00          	.uleb128 0x0
 2261 0dec 00          	.byte 0x0
 2262 0ded 17          	.uleb128 0x17
 2263 0dee 01          	.byte 0x1
 2264 0def 07          	.byte 0x7
 2265 0df0 E1          	.byte 0xe1
 2266 0df1 07 0E 00 00 	.4byte 0xe07
 2267 0df5 18          	.uleb128 0x18
 2268 0df6 50 49 44 00 	.asciz "PID"
 2269 0dfa 07          	.byte 0x7
 2270 0dfb E3          	.byte 0xe3
 2271 0dfc 16 01 00 00 	.4byte 0x116
 2272 0e00 02          	.byte 0x2
 2273 0e01 04          	.byte 0x4
 2274 0e02 0A          	.byte 0xa
 2275 0e03 02          	.byte 0x2
 2276 0e04 23          	.byte 0x23
 2277 0e05 00          	.uleb128 0x0
 2278 0e06 00          	.byte 0x0
 2279 0e07 19          	.uleb128 0x19
 2280 0e08 5F 42 44 5F 	.asciz "_BD_STAT"
MPLAB XC16 ASSEMBLY Listing:   			page 62


 2280      53 54 41 54 
 2280      00 
 2281 0e11 02          	.byte 0x2
 2282 0e12 07          	.byte 0x7
 2283 0e13 D0          	.byte 0xd0
 2284 0e14 33 0E 00 00 	.4byte 0xe33
 2285 0e18 12          	.uleb128 0x12
 2286 0e19 49 0D 00 00 	.4byte 0xd49
 2287 0e1d 12          	.uleb128 0x12
 2288 0e1e 9C 0D 00 00 	.4byte 0xd9c
 2289 0e22 12          	.uleb128 0x12
 2290 0e23 ED 0D 00 00 	.4byte 0xded
 2291 0e27 1A          	.uleb128 0x1a
 2292 0e28 56 61 6C 00 	.asciz "Val"
 2293 0e2c 07          	.byte 0x7
 2294 0e2d E5          	.byte 0xe5
 2295 0e2e 06 01 00 00 	.4byte 0x106
 2296 0e32 00          	.byte 0x0
 2297 0e33 03          	.uleb128 0x3
 2298 0e34 42 44 5F 53 	.asciz "BD_STAT"
 2298      54 41 54 00 
 2299 0e3c 07          	.byte 0x7
 2300 0e3d E6          	.byte 0xe6
 2301 0e3e 07 0E 00 00 	.4byte 0xe07
 2302 0e42 17          	.uleb128 0x17
 2303 0e43 08          	.byte 0x8
 2304 0e44 07          	.byte 0x7
 2305 0e45 ED          	.byte 0xed
 2306 0e46 88 0E 00 00 	.4byte 0xe88
 2307 0e4a 1B          	.uleb128 0x1b
 2308 0e4b 53 54 41 54 	.asciz "STAT"
 2308      00 
 2309 0e50 07          	.byte 0x7
 2310 0e51 EF          	.byte 0xef
 2311 0e52 33 0E 00 00 	.4byte 0xe33
 2312 0e56 02          	.byte 0x2
 2313 0e57 23          	.byte 0x23
 2314 0e58 00          	.uleb128 0x0
 2315 0e59 18          	.uleb128 0x18
 2316 0e5a 43 4E 54 00 	.asciz "CNT"
 2317 0e5e 07          	.byte 0x7
 2318 0e5f F0          	.byte 0xf0
 2319 0e60 06 01 00 00 	.4byte 0x106
 2320 0e64 02          	.byte 0x2
 2321 0e65 0A          	.byte 0xa
 2322 0e66 06          	.byte 0x6
 2323 0e67 02          	.byte 0x2
 2324 0e68 23          	.byte 0x23
 2325 0e69 02          	.uleb128 0x2
 2326 0e6a 1B          	.uleb128 0x1b
 2327 0e6b 41 44 52 00 	.asciz "ADR"
 2328 0e6f 07          	.byte 0x7
 2329 0e70 F1          	.byte 0xf1
 2330 0e71 06 01 00 00 	.4byte 0x106
 2331 0e75 02          	.byte 0x2
 2332 0e76 23          	.byte 0x23
 2333 0e77 04          	.uleb128 0x4
MPLAB XC16 ASSEMBLY Listing:   			page 63


 2334 0e78 1B          	.uleb128 0x1b
 2335 0e79 41 44 52 48 	.asciz "ADRH"
 2335      00 
 2336 0e7e 07          	.byte 0x7
 2337 0e7f F2          	.byte 0xf2
 2338 0e80 06 01 00 00 	.4byte 0x106
 2339 0e84 02          	.byte 0x2
 2340 0e85 23          	.byte 0x23
 2341 0e86 06          	.uleb128 0x6
 2342 0e87 00          	.byte 0x0
 2343 0e88 17          	.uleb128 0x17
 2344 0e89 04          	.byte 0x4
 2345 0e8a 07          	.byte 0x7
 2346 0e8b F4          	.byte 0xf4
 2347 0e8c B9 0E 00 00 	.4byte 0xeb9
 2348 0e90 18          	.uleb128 0x18
 2349 0e91 72 65 73 00 	.asciz "res"
 2350 0e95 07          	.byte 0x7
 2351 0e96 F6          	.byte 0xf6
 2352 0e97 26 01 00 00 	.4byte 0x126
 2353 0e9b 04          	.byte 0x4
 2354 0e9c 10          	.byte 0x10
 2355 0e9d 10          	.byte 0x10
 2356 0e9e 06          	.byte 0x6
 2357 0e9f 23          	.byte 0x23
 2358 0ea0 FE FF FF FF 	.uleb128 0xfffffffe
 2358      0F 
 2359 0ea5 18          	.uleb128 0x18
 2360 0ea6 63 6F 75 6E 	.asciz "count"
 2360      74 00 
 2361 0eac 07          	.byte 0x7
 2362 0ead F7          	.byte 0xf7
 2363 0eae 26 01 00 00 	.4byte 0x126
 2364 0eb2 04          	.byte 0x4
 2365 0eb3 0A          	.byte 0xa
 2366 0eb4 06          	.byte 0x6
 2367 0eb5 02          	.byte 0x2
 2368 0eb6 23          	.byte 0x23
 2369 0eb7 00          	.uleb128 0x0
 2370 0eb8 00          	.byte 0x0
 2371 0eb9 1C          	.uleb128 0x1c
 2372 0eba 08          	.byte 0x8
 2373 0ebb 07          	.byte 0x7
 2374 0ebc EB          	.byte 0xeb
 2375 0ebd CC 0E 00 00 	.4byte 0xecc
 2376 0ec1 12          	.uleb128 0x12
 2377 0ec2 42 0E 00 00 	.4byte 0xe42
 2378 0ec6 12          	.uleb128 0x12
 2379 0ec7 88 0E 00 00 	.4byte 0xe88
 2380 0ecb 00          	.byte 0x0
 2381 0ecc 19          	.uleb128 0x19
 2382 0ecd 5F 5F 42 44 	.asciz "__BDT"
 2382      54 00 
 2383 0ed3 08          	.byte 0x8
 2384 0ed4 07          	.byte 0x7
 2385 0ed5 E9          	.byte 0xe9
 2386 0ed6 FD 0E 00 00 	.4byte 0xefd
MPLAB XC16 ASSEMBLY Listing:   			page 64


 2387 0eda 12          	.uleb128 0x12
 2388 0edb B9 0E 00 00 	.4byte 0xeb9
 2389 0edf 1A          	.uleb128 0x1a
 2390 0ee0 56 61 6C 00 	.asciz "Val"
 2391 0ee4 07          	.byte 0x7
 2392 0ee5 FA          	.byte 0xfa
 2393 0ee6 26 01 00 00 	.4byte 0x126
 2394 0eea 1A          	.uleb128 0x1a
 2395 0eeb 76 00       	.asciz "v"
 2396 0eed 07          	.byte 0x7
 2397 0eee FB          	.byte 0xfb
 2398 0eef FD 0E 00 00 	.4byte 0xefd
 2399 0ef3 1A          	.uleb128 0x1a
 2400 0ef4 77 00       	.asciz "w"
 2401 0ef6 07          	.byte 0x7
 2402 0ef7 FC          	.byte 0xfc
 2403 0ef8 0D 0F 00 00 	.4byte 0xf0d
 2404 0efc 00          	.byte 0x0
 2405 0efd 0F          	.uleb128 0xf
 2406 0efe 06 01 00 00 	.4byte 0x106
 2407 0f02 0D 0F 00 00 	.4byte 0xf0d
 2408 0f06 10          	.uleb128 0x10
 2409 0f07 16 01 00 00 	.4byte 0x116
 2410 0f0b 03          	.byte 0x3
 2411 0f0c 00          	.byte 0x0
 2412 0f0d 0F          	.uleb128 0xf
 2413 0f0e 26 01 00 00 	.4byte 0x126
 2414 0f12 1D 0F 00 00 	.4byte 0xf1d
 2415 0f16 10          	.uleb128 0x10
 2416 0f17 16 01 00 00 	.4byte 0x116
 2417 0f1b 01          	.byte 0x1
 2418 0f1c 00          	.byte 0x0
 2419 0f1d 03          	.uleb128 0x3
 2420 0f1e 42 44 54 5F 	.asciz "BDT_ENTRY"
 2420      45 4E 54 52 
 2420      59 00 
 2421 0f28 07          	.byte 0x7
 2422 0f29 FD          	.byte 0xfd
 2423 0f2a CC 0E 00 00 	.4byte 0xecc
 2424 0f2e 0A          	.uleb128 0xa
 2425 0f2f 02          	.byte 0x2
 2426 0f30 07          	.byte 0x7
 2427 0f31 18 01       	.2byte 0x118
 2428 0f33 59 0F 00 00 	.4byte 0xf59
 2429 0f37 0C          	.uleb128 0xc
 2430 0f38 62 4C 6F 77 	.asciz "bLow"
 2430      00 
 2431 0f3d 07          	.byte 0x7
 2432 0f3e 1A 01       	.2byte 0x11a
 2433 0f40 E6 00 00 00 	.4byte 0xe6
 2434 0f44 02          	.byte 0x2
 2435 0f45 23          	.byte 0x23
 2436 0f46 00          	.uleb128 0x0
 2437 0f47 0C          	.uleb128 0xc
 2438 0f48 62 48 69 67 	.asciz "bHigh"
 2438      68 00 
 2439 0f4e 07          	.byte 0x7
MPLAB XC16 ASSEMBLY Listing:   			page 65


 2440 0f4f 1B 01       	.2byte 0x11b
 2441 0f51 E6 00 00 00 	.4byte 0xe6
 2442 0f55 02          	.byte 0x2
 2443 0f56 23          	.byte 0x23
 2444 0f57 01          	.uleb128 0x1
 2445 0f58 00          	.byte 0x0
 2446 0f59 1D          	.uleb128 0x1d
 2447 0f5a 5F 50 4F 49 	.asciz "_POINTER"
 2447      4E 54 45 52 
 2447      00 
 2448 0f63 02          	.byte 0x2
 2449 0f64 07          	.byte 0x7
 2450 0f65 16 01       	.2byte 0x116
 2451 0f67 B3 0F 00 00 	.4byte 0xfb3
 2452 0f6b 12          	.uleb128 0x12
 2453 0f6c 2E 0F 00 00 	.4byte 0xf2e
 2454 0f70 0E          	.uleb128 0xe
 2455 0f71 5F 77 6F 72 	.asciz "_word"
 2455      64 00 
 2456 0f77 07          	.byte 0x7
 2457 0f78 1E 01       	.2byte 0x11e
 2458 0f7a 06 01 00 00 	.4byte 0x106
 2459 0f7e 0E          	.uleb128 0xe
 2460 0f7f 62 52 61 6D 	.asciz "bRam"
 2460      00 
 2461 0f84 07          	.byte 0x7
 2462 0f85 22 01       	.2byte 0x122
 2463 0f87 62 0B 00 00 	.4byte 0xb62
 2464 0f8b 0E          	.uleb128 0xe
 2465 0f8c 77 52 61 6D 	.asciz "wRam"
 2465      00 
 2466 0f91 07          	.byte 0x7
 2467 0f92 24 01       	.2byte 0x124
 2468 0f94 73 0B 00 00 	.4byte 0xb73
 2469 0f98 0E          	.uleb128 0xe
 2470 0f99 62 52 6F 6D 	.asciz "bRom"
 2470      00 
 2471 0f9e 07          	.byte 0x7
 2472 0f9f 27 01       	.2byte 0x127
 2473 0fa1 68 0B 00 00 	.4byte 0xb68
 2474 0fa5 0E          	.uleb128 0xe
 2475 0fa6 77 52 6F 6D 	.asciz "wRom"
 2475      00 
 2476 0fab 07          	.byte 0x7
 2477 0fac 28 01       	.2byte 0x128
 2478 0fae 79 0B 00 00 	.4byte 0xb79
 2479 0fb2 00          	.byte 0x0
 2480 0fb3 06          	.uleb128 0x6
 2481 0fb4 50 4F 49 4E 	.asciz "POINTER"
 2481      54 45 52 00 
 2482 0fbc 07          	.byte 0x7
 2483 0fbd 2D 01       	.2byte 0x12d
 2484 0fbf 59 0F 00 00 	.4byte 0xf59
 2485 0fc3 0A          	.uleb128 0xa
 2486 0fc4 07          	.byte 0x7
 2487 0fc5 08          	.byte 0x8
 2488 0fc6 1E 03       	.2byte 0x31e
MPLAB XC16 ASSEMBLY Listing:   			page 66


 2489 0fc8 DE 0F 00 00 	.4byte 0xfde
 2490 0fcc 0C          	.uleb128 0xc
 2491 0fcd 5F 62 79 74 	.asciz "_byte"
 2491      65 00 
 2492 0fd3 08          	.byte 0x8
 2493 0fd4 20 03       	.2byte 0x320
 2494 0fd6 DE 0F 00 00 	.4byte 0xfde
 2495 0fda 02          	.byte 0x2
 2496 0fdb 23          	.byte 0x23
 2497 0fdc 00          	.uleb128 0x0
 2498 0fdd 00          	.byte 0x0
 2499 0fde 0F          	.uleb128 0xf
 2500 0fdf E6 00 00 00 	.4byte 0xe6
 2501 0fe3 EE 0F 00 00 	.4byte 0xfee
 2502 0fe7 10          	.uleb128 0x10
 2503 0fe8 16 01 00 00 	.4byte 0x116
 2504 0fec 06          	.byte 0x6
 2505 0fed 00          	.byte 0x0
 2506 0fee 0A          	.uleb128 0xa
 2507 0fef 08          	.byte 0x8
 2508 0ff0 08          	.byte 0x8
 2509 0ff1 22 03       	.2byte 0x322
 2510 0ff3 50 10 00 00 	.4byte 0x1050
 2511 0ff7 0C          	.uleb128 0xc
 2512 0ff8 64 77 44 54 	.asciz "dwDTERate"
 2512      45 52 61 74 
 2512      65 00 
 2513 1002 08          	.byte 0x8
 2514 1003 24 03       	.2byte 0x324
 2515 1005 26 01 00 00 	.4byte 0x126
 2516 1009 02          	.byte 0x2
 2517 100a 23          	.byte 0x23
 2518 100b 00          	.uleb128 0x0
 2519 100c 0C          	.uleb128 0xc
 2520 100d 62 43 68 61 	.asciz "bCharFormat"
 2520      72 46 6F 72 
 2520      6D 61 74 00 
 2521 1019 08          	.byte 0x8
 2522 101a 25 03       	.2byte 0x325
 2523 101c E6 00 00 00 	.4byte 0xe6
 2524 1020 02          	.byte 0x2
 2525 1021 23          	.byte 0x23
 2526 1022 04          	.uleb128 0x4
 2527 1023 0C          	.uleb128 0xc
 2528 1024 62 50 61 72 	.asciz "bParityType"
 2528      69 74 79 54 
 2528      79 70 65 00 
 2529 1030 08          	.byte 0x8
 2530 1031 26 03       	.2byte 0x326
 2531 1033 E6 00 00 00 	.4byte 0xe6
 2532 1037 02          	.byte 0x2
 2533 1038 23          	.byte 0x23
 2534 1039 05          	.uleb128 0x5
 2535 103a 0C          	.uleb128 0xc
 2536 103b 62 44 61 74 	.asciz "bDataBits"
 2536      61 42 69 74 
 2536      73 00 
MPLAB XC16 ASSEMBLY Listing:   			page 67


 2537 1045 08          	.byte 0x8
 2538 1046 27 03       	.2byte 0x327
 2539 1048 E6 00 00 00 	.4byte 0xe6
 2540 104c 02          	.byte 0x2
 2541 104d 23          	.byte 0x23
 2542 104e 06          	.uleb128 0x6
 2543 104f 00          	.byte 0x0
 2544 1050 1D          	.uleb128 0x1d
 2545 1051 5F 4C 49 4E 	.asciz "_LINE_CODING"
 2545      45 5F 43 4F 
 2545      44 49 4E 47 
 2545      00 
 2546 105e 08          	.byte 0x8
 2547 105f 08          	.byte 0x8
 2548 1060 1C 03       	.2byte 0x31c
 2549 1062 71 10 00 00 	.4byte 0x1071
 2550 1066 12          	.uleb128 0x12
 2551 1067 C3 0F 00 00 	.4byte 0xfc3
 2552 106b 12          	.uleb128 0x12
 2553 106c EE 0F 00 00 	.4byte 0xfee
 2554 1070 00          	.byte 0x0
 2555 1071 06          	.uleb128 0x6
 2556 1072 4C 49 4E 45 	.asciz "LINE_CODING"
 2556      5F 43 4F 44 
 2556      49 4E 47 00 
 2557 107e 08          	.byte 0x8
 2558 107f 29 03       	.2byte 0x329
 2559 1081 50 10 00 00 	.4byte 0x1050
 2560 1085 0A          	.uleb128 0xa
 2561 1086 02          	.byte 0x2
 2562 1087 08          	.byte 0x8
 2563 1088 2E 03       	.2byte 0x32e
 2564 108a C7 10 00 00 	.4byte 0x10c7
 2565 108e 05          	.uleb128 0x5
 2566 108f 44 54 45 5F 	.asciz "DTE_PRESENT"
 2566      50 52 45 53 
 2566      45 4E 54 00 
 2567 109b 08          	.byte 0x8
 2568 109c 30 03       	.2byte 0x330
 2569 109e 16 01 00 00 	.4byte 0x116
 2570 10a2 02          	.byte 0x2
 2571 10a3 01          	.byte 0x1
 2572 10a4 0F          	.byte 0xf
 2573 10a5 02          	.byte 0x2
 2574 10a6 23          	.byte 0x23
 2575 10a7 00          	.uleb128 0x0
 2576 10a8 05          	.uleb128 0x5
 2577 10a9 43 41 52 52 	.asciz "CARRIER_CONTROL"
 2577      49 45 52 5F 
 2577      43 4F 4E 54 
 2577      52 4F 4C 00 
 2578 10b9 08          	.byte 0x8
 2579 10ba 31 03       	.2byte 0x331
 2580 10bc 16 01 00 00 	.4byte 0x116
 2581 10c0 02          	.byte 0x2
 2582 10c1 01          	.byte 0x1
 2583 10c2 0E          	.byte 0xe
MPLAB XC16 ASSEMBLY Listing:   			page 68


 2584 10c3 02          	.byte 0x2
 2585 10c4 23          	.byte 0x23
 2586 10c5 00          	.uleb128 0x0
 2587 10c6 00          	.byte 0x0
 2588 10c7 1D          	.uleb128 0x1d
 2589 10c8 5F 43 4F 4E 	.asciz "_CONTROL_SIGNAL_BITMAP"
 2589      54 52 4F 4C 
 2589      5F 53 49 47 
 2589      4E 41 4C 5F 
 2589      42 49 54 4D 
 2589      41 50 00 
 2590 10df 02          	.byte 0x2
 2591 10e0 08          	.byte 0x8
 2592 10e1 2B 03       	.2byte 0x32b
 2593 10e3 FB 10 00 00 	.4byte 0x10fb
 2594 10e7 0E          	.uleb128 0xe
 2595 10e8 5F 62 79 74 	.asciz "_byte"
 2595      65 00 
 2596 10ee 08          	.byte 0x8
 2597 10ef 2D 03       	.2byte 0x32d
 2598 10f1 E6 00 00 00 	.4byte 0xe6
 2599 10f5 12          	.uleb128 0x12
 2600 10f6 85 10 00 00 	.4byte 0x1085
 2601 10fa 00          	.byte 0x0
 2602 10fb 06          	.uleb128 0x6
 2603 10fc 43 4F 4E 54 	.asciz "CONTROL_SIGNAL_BITMAP"
 2603      52 4F 4C 5F 
 2603      53 49 47 4E 
 2603      41 4C 5F 42 
 2603      49 54 4D 41 
 2603      50 00 
 2604 1112 08          	.byte 0x8
 2605 1113 33 03       	.2byte 0x333
 2606 1115 C7 10 00 00 	.4byte 0x10c7
 2607 1119 1D          	.uleb128 0x1d
 2608 111a 5F 43 44 43 	.asciz "_CDC_NOTICE"
 2608      5F 4E 4F 54 
 2608      49 43 45 00 
 2609 1126 0A          	.byte 0xa
 2610 1127 08          	.byte 0x8
 2611 1128 5E 03       	.2byte 0x35e
 2612 112a 6A 11 00 00 	.4byte 0x116a
 2613 112e 0E          	.uleb128 0xe
 2614 112f 47 65 74 4C 	.asciz "GetLineCoding"
 2614      69 6E 65 43 
 2614      6F 64 69 6E 
 2614      67 00 
 2615 113d 08          	.byte 0x8
 2616 113e 60 03       	.2byte 0x360
 2617 1140 71 10 00 00 	.4byte 0x1071
 2618 1144 0E          	.uleb128 0xe
 2619 1145 53 65 74 4C 	.asciz "SetLineCoding"
 2619      69 6E 65 43 
 2619      6F 64 69 6E 
 2619      67 00 
 2620 1153 08          	.byte 0x8
 2621 1154 61 03       	.2byte 0x361
MPLAB XC16 ASSEMBLY Listing:   			page 69


 2622 1156 71 10 00 00 	.4byte 0x1071
 2623 115a 0E          	.uleb128 0xe
 2624 115b 70 61 63 6B 	.asciz "packet"
 2624      65 74 00 
 2625 1162 08          	.byte 0x8
 2626 1163 62 03       	.2byte 0x362
 2627 1165 6A 11 00 00 	.4byte 0x116a
 2628 1169 00          	.byte 0x0
 2629 116a 0F          	.uleb128 0xf
 2630 116b F5 00 00 00 	.4byte 0xf5
 2631 116f 7A 11 00 00 	.4byte 0x117a
 2632 1173 10          	.uleb128 0x10
 2633 1174 16 01 00 00 	.4byte 0x116
 2634 1178 09          	.byte 0x9
 2635 1179 00          	.byte 0x0
 2636 117a 06          	.uleb128 0x6
 2637 117b 43 44 43 5F 	.asciz "CDC_NOTICE"
 2637      4E 4F 54 49 
 2637      43 45 00 
 2638 1186 08          	.byte 0x8
 2639 1187 63 03       	.2byte 0x363
 2640 1189 19 11 00 00 	.4byte 0x1119
 2641 118d 1E          	.uleb128 0x1e
 2642 118e 01          	.byte 0x1
 2643 118f 55 53 42 43 	.asciz "USBCheckCDCRequest"
 2643      68 65 63 6B 
 2643      43 44 43 52 
 2643      65 71 75 65 
 2643      73 74 00 
 2644 11a2 01          	.byte 0x1
 2645 11a3 94          	.byte 0x94
 2646 11a4 01          	.byte 0x1
 2647 11a5 00 00 00 00 	.4byte .LFB0
 2648 11a9 00 00 00 00 	.4byte .LFE0
 2649 11ad 01          	.byte 0x1
 2650 11ae 5E          	.byte 0x5e
 2651 11af 1F          	.uleb128 0x1f
 2652 11b0 01          	.byte 0x1
 2653 11b1 43 44 43 49 	.asciz "CDCInitEP"
 2653      6E 69 74 45 
 2653      50 00 
 2654 11bb 01          	.byte 0x1
 2655 11bc 27 01       	.2byte 0x127
 2656 11be 01          	.byte 0x1
 2657 11bf 00 00 00 00 	.4byte .LFB1
 2658 11c3 00 00 00 00 	.4byte .LFE1
 2659 11c7 01          	.byte 0x1
 2660 11c8 5E          	.byte 0x5e
 2661 11c9 20          	.uleb128 0x20
 2662 11ca 01          	.byte 0x1
 2663 11cb 55 53 42 43 	.asciz "USBCDCEventHandler"
 2663      44 43 45 76 
 2663      65 6E 74 48 
 2663      61 6E 64 6C 
 2663      65 72 00 
 2664 11de 01          	.byte 0x1
 2665 11df A9 01       	.2byte 0x1a9
MPLAB XC16 ASSEMBLY Listing:   			page 70


 2666 11e1 01          	.byte 0x1
 2667 11e2 DA 05 00 00 	.4byte 0x5da
 2668 11e6 00 00 00 00 	.4byte .LFB2
 2669 11ea 00 00 00 00 	.4byte .LFE2
 2670 11ee 01          	.byte 0x1
 2671 11ef 5E          	.byte 0x5e
 2672 11f0 27 12 00 00 	.4byte 0x1227
 2673 11f4 21          	.uleb128 0x21
 2674 11f5 65 76 65 6E 	.asciz "event"
 2674      74 00 
 2675 11fb 01          	.byte 0x1
 2676 11fc A9 01       	.2byte 0x1a9
 2677 11fe C8 05 00 00 	.4byte 0x5c8
 2678 1202 02          	.byte 0x2
 2679 1203 7E          	.byte 0x7e
 2680 1204 00          	.sleb128 0
 2681 1205 21          	.uleb128 0x21
 2682 1206 70 64 61 74 	.asciz "pdata"
 2682      61 00 
 2683 120c 01          	.byte 0x1
 2684 120d A9 01       	.2byte 0x1a9
 2685 120f E3 05 00 00 	.4byte 0x5e3
 2686 1213 02          	.byte 0x2
 2687 1214 7E          	.byte 0x7e
 2688 1215 02          	.sleb128 2
 2689 1216 21          	.uleb128 0x21
 2690 1217 73 69 7A 65 	.asciz "size"
 2690      00 
 2691 121c 01          	.byte 0x1
 2692 121d A9 01       	.2byte 0x1a9
 2693 121f 06 01 00 00 	.4byte 0x106
 2694 1223 02          	.byte 0x2
 2695 1224 7E          	.byte 0x7e
 2696 1225 04          	.sleb128 4
 2697 1226 00          	.byte 0x0
 2698 1227 20          	.uleb128 0x20
 2699 1228 01          	.byte 0x1
 2700 1229 67 65 74 73 	.asciz "getsUSBUSART"
 2700      55 53 42 55 
 2700      53 41 52 54 
 2700      00 
 2701 1236 01          	.byte 0x1
 2702 1237 E6 01       	.2byte 0x1e6
 2703 1239 01          	.byte 0x1
 2704 123a E6 00 00 00 	.4byte 0xe6
 2705 123e 00 00 00 00 	.4byte .LFB3
 2706 1242 00 00 00 00 	.4byte .LFE3
 2707 1246 01          	.byte 0x1
 2708 1247 5E          	.byte 0x5e
 2709 1248 6E 12 00 00 	.4byte 0x126e
 2710 124c 21          	.uleb128 0x21
 2711 124d 62 75 66 66 	.asciz "buffer"
 2711      65 72 00 
 2712 1254 01          	.byte 0x1
 2713 1255 E6 01       	.2byte 0x1e6
 2714 1257 62 0B 00 00 	.4byte 0xb62
 2715 125b 02          	.byte 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 71


 2716 125c 7E          	.byte 0x7e
 2717 125d 00          	.sleb128 0
 2718 125e 21          	.uleb128 0x21
 2719 125f 6C 65 6E 00 	.asciz "len"
 2720 1263 01          	.byte 0x1
 2721 1264 E6 01       	.2byte 0x1e6
 2722 1266 E6 00 00 00 	.4byte 0xe6
 2723 126a 02          	.byte 0x2
 2724 126b 7E          	.byte 0x7e
 2725 126c 02          	.sleb128 2
 2726 126d 00          	.byte 0x0
 2727 126e 22          	.uleb128 0x22
 2728 126f 01          	.byte 0x1
 2729 1270 70 75 74 55 	.asciz "putUSBUSART"
 2729      53 42 55 53 
 2729      41 52 54 00 
 2730 127c 01          	.byte 0x1
 2731 127d 2D 02       	.2byte 0x22d
 2732 127f 01          	.byte 0x1
 2733 1280 00 00 00 00 	.4byte .LFB4
 2734 1284 00 00 00 00 	.4byte .LFE4
 2735 1288 01          	.byte 0x1
 2736 1289 5E          	.byte 0x5e
 2737 128a B1 12 00 00 	.4byte 0x12b1
 2738 128e 21          	.uleb128 0x21
 2739 128f 64 61 74 61 	.asciz "data"
 2739      00 
 2740 1294 01          	.byte 0x1
 2741 1295 2D 02       	.2byte 0x22d
 2742 1297 62 0B 00 00 	.4byte 0xb62
 2743 129b 02          	.byte 0x2
 2744 129c 7E          	.byte 0x7e
 2745 129d 00          	.sleb128 0
 2746 129e 21          	.uleb128 0x21
 2747 129f 6C 65 6E 67 	.asciz "length"
 2747      74 68 00 
 2748 12a6 01          	.byte 0x1
 2749 12a7 2D 02       	.2byte 0x22d
 2750 12a9 E6 00 00 00 	.4byte 0xe6
 2751 12ad 02          	.byte 0x2
 2752 12ae 7E          	.byte 0x7e
 2753 12af 02          	.sleb128 2
 2754 12b0 00          	.byte 0x0
 2755 12b1 22          	.uleb128 0x22
 2756 12b2 01          	.byte 0x1
 2757 12b3 70 75 74 73 	.asciz "putsUSBUSART"
 2757      55 53 42 55 
 2757      53 41 52 54 
 2757      00 
 2758 12c0 01          	.byte 0x1
 2759 12c1 76 02       	.2byte 0x276
 2760 12c3 01          	.byte 0x1
 2761 12c4 00 00 00 00 	.4byte .LFB5
 2762 12c8 00 00 00 00 	.4byte .LFE5
 2763 12cc 01          	.byte 0x1
 2764 12cd 5E          	.byte 0x5e
 2765 12ce 03 13 00 00 	.4byte 0x1303
MPLAB XC16 ASSEMBLY Listing:   			page 72


 2766 12d2 21          	.uleb128 0x21
 2767 12d3 64 61 74 61 	.asciz "data"
 2767      00 
 2768 12d8 01          	.byte 0x1
 2769 12d9 76 02       	.2byte 0x276
 2770 12db 03 13 00 00 	.4byte 0x1303
 2771 12df 02          	.byte 0x2
 2772 12e0 7E          	.byte 0x7e
 2773 12e1 04          	.sleb128 4
 2774 12e2 23          	.uleb128 0x23
 2775 12e3 6C 65 6E 00 	.asciz "len"
 2776 12e7 01          	.byte 0x1
 2777 12e8 78 02       	.2byte 0x278
 2778 12ea E6 00 00 00 	.4byte 0xe6
 2779 12ee 02          	.byte 0x2
 2780 12ef 7E          	.byte 0x7e
 2781 12f0 00          	.sleb128 0
 2782 12f1 23          	.uleb128 0x23
 2783 12f2 70 44 61 74 	.asciz "pData"
 2783      61 00 
 2784 12f8 01          	.byte 0x1
 2785 12f9 79 02       	.2byte 0x279
 2786 12fb 03 13 00 00 	.4byte 0x1303
 2787 12ff 02          	.byte 0x2
 2788 1300 7E          	.byte 0x7e
 2789 1301 02          	.sleb128 2
 2790 1302 00          	.byte 0x0
 2791 1303 13          	.uleb128 0x13
 2792 1304 02          	.byte 0x2
 2793 1305 09 13 00 00 	.4byte 0x1309
 2794 1309 02          	.uleb128 0x2
 2795 130a 01          	.byte 0x1
 2796 130b 06          	.byte 0x6
 2797 130c 63 68 61 72 	.asciz "char"
 2797      00 
 2798 1311 22          	.uleb128 0x22
 2799 1312 01          	.byte 0x1
 2800 1313 43 44 43 54 	.asciz "CDCTxService"
 2800      78 53 65 72 
 2800      76 69 63 65 
 2800      00 
 2801 1320 01          	.byte 0x1
 2802 1321 E2 02       	.2byte 0x2e2
 2803 1323 01          	.byte 0x1
 2804 1324 00 00 00 00 	.4byte .LFB6
 2805 1328 00 00 00 00 	.4byte .LFE6
 2806 132c 01          	.byte 0x1
 2807 132d 5E          	.byte 0x5e
 2808 132e 58 13 00 00 	.4byte 0x1358
 2809 1332 23          	.uleb128 0x23
 2810 1333 62 79 74 65 	.asciz "byte_to_send"
 2810      5F 74 6F 5F 
 2810      73 65 6E 64 
 2810      00 
 2811 1340 01          	.byte 0x1
 2812 1341 E4 02       	.2byte 0x2e4
 2813 1343 E6 00 00 00 	.4byte 0xe6
MPLAB XC16 ASSEMBLY Listing:   			page 73


 2814 1347 02          	.byte 0x2
 2815 1348 7E          	.byte 0x7e
 2816 1349 00          	.sleb128 0
 2817 134a 23          	.uleb128 0x23
 2818 134b 69 00       	.asciz "i"
 2819 134d 01          	.byte 0x1
 2820 134e E5 02       	.2byte 0x2e5
 2821 1350 E6 00 00 00 	.4byte 0xe6
 2822 1354 02          	.byte 0x2
 2823 1355 7E          	.byte 0x7e
 2824 1356 01          	.sleb128 1
 2825 1357 00          	.byte 0x0
 2826 1358 24          	.uleb128 0x24
 2827 1359 00 00 00 00 	.4byte .LASF2
 2828 135d 03          	.byte 0x3
 2829 135e 2E 26       	.2byte 0x262e
 2830 1360 66 13 00 00 	.4byte 0x1366
 2831 1364 01          	.byte 0x1
 2832 1365 01          	.byte 0x1
 2833 1366 25          	.uleb128 0x25
 2834 1367 89 02 00 00 	.4byte 0x289
 2835 136b 0F          	.uleb128 0xf
 2836 136c 47 0C 00 00 	.4byte 0xc47
 2837 1370 76 13 00 00 	.4byte 0x1376
 2838 1374 26          	.uleb128 0x26
 2839 1375 00          	.byte 0x0
 2840 1376 27          	.uleb128 0x27
 2841 1377 69 6E 50 69 	.asciz "inPipes"
 2841      70 65 73 00 
 2842 137f 06          	.byte 0x6
 2843 1380 E3 07       	.2byte 0x7e3
 2844 1382 88 13 00 00 	.4byte 0x1388
 2845 1386 01          	.byte 0x1
 2846 1387 01          	.byte 0x1
 2847 1388 25          	.uleb128 0x25
 2848 1389 6B 13 00 00 	.4byte 0x136b
 2849 138d 0F          	.uleb128 0xf
 2850 138e 22 0D 00 00 	.4byte 0xd22
 2851 1392 9D 13 00 00 	.4byte 0x139d
 2852 1396 10          	.uleb128 0x10
 2853 1397 16 01 00 00 	.4byte 0x116
 2854 139b 00          	.byte 0x0
 2855 139c 00          	.byte 0x0
 2856 139d 24          	.uleb128 0x24
 2857 139e 00 00 00 00 	.4byte .LASF3
 2858 13a2 07          	.byte 0x7
 2859 13a3 CB 02       	.2byte 0x2cb
 2860 13a5 AB 13 00 00 	.4byte 0x13ab
 2861 13a9 01          	.byte 0x1
 2862 13aa 01          	.byte 0x1
 2863 13ab 25          	.uleb128 0x25
 2864 13ac 8D 13 00 00 	.4byte 0x138d
 2865 13b0 24          	.uleb128 0x24
 2866 13b1 00 00 00 00 	.4byte .LASF4
 2867 13b5 08          	.byte 0x8
 2868 13b6 84 03       	.2byte 0x384
 2869 13b8 E6 00 00 00 	.4byte 0xe6
MPLAB XC16 ASSEMBLY Listing:   			page 74


 2870 13bc 01          	.byte 0x1
 2871 13bd 01          	.byte 0x1
 2872 13be 24          	.uleb128 0x24
 2873 13bf 00 00 00 00 	.4byte .LASF5
 2874 13c3 08          	.byte 0x8
 2875 13c4 87 03       	.2byte 0x387
 2876 13c6 E6 00 00 00 	.4byte 0xe6
 2877 13ca 01          	.byte 0x1
 2878 13cb 01          	.byte 0x1
 2879 13cc 27          	.uleb128 0x27
 2880 13cd 70 43 44 43 	.asciz "pCDCSrc"
 2880      53 72 63 00 
 2881 13d5 08          	.byte 0x8
 2882 13d6 88 03       	.2byte 0x388
 2883 13d8 B3 0F 00 00 	.4byte 0xfb3
 2884 13dc 01          	.byte 0x1
 2885 13dd 01          	.byte 0x1
 2886 13de 24          	.uleb128 0x24
 2887 13df 00 00 00 00 	.4byte .LASF6
 2888 13e3 08          	.byte 0x8
 2889 13e4 89 03       	.2byte 0x389
 2890 13e6 E6 00 00 00 	.4byte 0xe6
 2891 13ea 01          	.byte 0x1
 2892 13eb 01          	.byte 0x1
 2893 13ec 24          	.uleb128 0x24
 2894 13ed 00 00 00 00 	.4byte .LASF7
 2895 13f1 08          	.byte 0x8
 2896 13f2 8A 03       	.2byte 0x38a
 2897 13f4 E6 00 00 00 	.4byte 0xe6
 2898 13f8 01          	.byte 0x1
 2899 13f9 01          	.byte 0x1
 2900 13fa 24          	.uleb128 0x24
 2901 13fb 00 00 00 00 	.4byte .LASF8
 2902 13ff 08          	.byte 0x8
 2903 1400 8D 03       	.2byte 0x38d
 2904 1402 71 10 00 00 	.4byte 0x1071
 2905 1406 01          	.byte 0x1
 2906 1407 01          	.byte 0x1
 2907 1408 24          	.uleb128 0x24
 2908 1409 00 00 00 00 	.4byte .LASF9
 2909 140d 08          	.byte 0x8
 2910 140e 8F 03       	.2byte 0x38f
 2911 1410 16 14 00 00 	.4byte 0x1416
 2912 1414 01          	.byte 0x1
 2913 1415 01          	.byte 0x1
 2914 1416 25          	.uleb128 0x25
 2915 1417 33 0A 00 00 	.4byte 0xa33
 2916 141b 0F          	.uleb128 0xf
 2917 141c F5 00 00 00 	.4byte 0xf5
 2918 1420 2B 14 00 00 	.4byte 0x142b
 2919 1424 10          	.uleb128 0x10
 2920 1425 16 01 00 00 	.4byte 0x116
 2921 1429 3F          	.byte 0x3f
 2922 142a 00          	.byte 0x0
 2923 142b 28          	.uleb128 0x28
 2924 142c 00 00 00 00 	.4byte .LASF10
 2925 1430 01          	.byte 0x1
MPLAB XC16 ASSEMBLY Listing:   			page 75


 2926 1431 42          	.byte 0x42
 2927 1432 38 14 00 00 	.4byte 0x1438
 2928 1436 01          	.byte 0x1
 2929 1437 01          	.byte 0x1
 2930 1438 25          	.uleb128 0x25
 2931 1439 1B 14 00 00 	.4byte 0x141b
 2932 143d 28          	.uleb128 0x28
 2933 143e 00 00 00 00 	.4byte .LASF11
 2934 1442 01          	.byte 0x1
 2935 1443 43          	.byte 0x43
 2936 1444 4A 14 00 00 	.4byte 0x144a
 2937 1448 01          	.byte 0x1
 2938 1449 01          	.byte 0x1
 2939 144a 25          	.uleb128 0x25
 2940 144b 1B 14 00 00 	.4byte 0x141b
 2941 144f 29          	.uleb128 0x29
 2942 1450 70 43 44 43 	.asciz "pCDCDst"
 2942      44 73 74 00 
 2943 1458 01          	.byte 0x1
 2944 1459 57          	.byte 0x57
 2945 145a B3 0F 00 00 	.4byte 0xfb3
 2946 145e 01          	.byte 0x1
 2947 145f 01          	.byte 0x1
 2948 1460 28          	.uleb128 0x28
 2949 1461 00 00 00 00 	.4byte .LASF12
 2950 1465 01          	.byte 0x1
 2951 1466 5B          	.byte 0x5b
 2952 1467 E3 05 00 00 	.4byte 0x5e3
 2953 146b 01          	.byte 0x1
 2954 146c 01          	.byte 0x1
 2955 146d 28          	.uleb128 0x28
 2956 146e 00 00 00 00 	.4byte .LASF13
 2957 1472 01          	.byte 0x1
 2958 1473 5C          	.byte 0x5c
 2959 1474 E3 05 00 00 	.4byte 0x5e3
 2960 1478 01          	.byte 0x1
 2961 1479 01          	.byte 0x1
 2962 147a 28          	.uleb128 0x28
 2963 147b 00 00 00 00 	.4byte .LASF14
 2964 147f 01          	.byte 0x1
 2965 1480 5F          	.byte 0x5f
 2966 1481 FB 10 00 00 	.4byte 0x10fb
 2967 1485 01          	.byte 0x1
 2968 1486 01          	.byte 0x1
 2969 1487 0F          	.uleb128 0xf
 2970 1488 E6 00 00 00 	.4byte 0xe6
 2971 148c 97 14 00 00 	.4byte 0x1497
 2972 1490 10          	.uleb128 0x10
 2973 1491 16 01 00 00 	.4byte 0x116
 2974 1495 07          	.byte 0x7
 2975 1496 00          	.byte 0x0
 2976 1497 28          	.uleb128 0x28
 2977 1498 00 00 00 00 	.4byte .LASF15
 2978 149c 01          	.byte 0x1
 2979 149d 6F          	.byte 0x6f
 2980 149e 87 14 00 00 	.4byte 0x1487
 2981 14a2 01          	.byte 0x1
MPLAB XC16 ASSEMBLY Listing:   			page 76


 2982 14a3 01          	.byte 0x1
 2983 14a4 24          	.uleb128 0x24
 2984 14a5 00 00 00 00 	.4byte .LASF2
 2985 14a9 03          	.byte 0x3
 2986 14aa 2E 26       	.2byte 0x262e
 2987 14ac 66 13 00 00 	.4byte 0x1366
 2988 14b0 01          	.byte 0x1
 2989 14b1 01          	.byte 0x1
 2990 14b2 27          	.uleb128 0x27
 2991 14b3 69 6E 50 69 	.asciz "inPipes"
 2991      70 65 73 00 
 2992 14bb 06          	.byte 0x6
 2993 14bc E3 07       	.2byte 0x7e3
 2994 14be C4 14 00 00 	.4byte 0x14c4
 2995 14c2 01          	.byte 0x1
 2996 14c3 01          	.byte 0x1
 2997 14c4 25          	.uleb128 0x25
 2998 14c5 6B 13 00 00 	.4byte 0x136b
 2999 14c9 24          	.uleb128 0x24
 3000 14ca 00 00 00 00 	.4byte .LASF3
 3001 14ce 07          	.byte 0x7
 3002 14cf CB 02       	.2byte 0x2cb
 3003 14d1 D7 14 00 00 	.4byte 0x14d7
 3004 14d5 01          	.byte 0x1
 3005 14d6 01          	.byte 0x1
 3006 14d7 25          	.uleb128 0x25
 3007 14d8 8D 13 00 00 	.4byte 0x138d
 3008 14dc 2A          	.uleb128 0x2a
 3009 14dd 00 00 00 00 	.4byte .LASF4
 3010 14e1 01          	.byte 0x1
 3011 14e2 54          	.byte 0x54
 3012 14e3 E6 00 00 00 	.4byte 0xe6
 3013 14e7 01          	.byte 0x1
 3014 14e8 05          	.byte 0x5
 3015 14e9 03          	.byte 0x3
 3016 14ea 00 00 00 00 	.4byte _cdc_rx_len
 3017 14ee 2A          	.uleb128 0x2a
 3018 14ef 00 00 00 00 	.4byte .LASF5
 3019 14f3 01          	.byte 0x1
 3020 14f4 55          	.byte 0x55
 3021 14f5 E6 00 00 00 	.4byte 0xe6
 3022 14f9 01          	.byte 0x1
 3023 14fa 05          	.byte 0x5
 3024 14fb 03          	.byte 0x3
 3025 14fc 00 00 00 00 	.4byte _cdc_trf_state
 3026 1500 2B          	.uleb128 0x2b
 3027 1501 70 43 44 43 	.asciz "pCDCSrc"
 3027      53 72 63 00 
 3028 1509 01          	.byte 0x1
 3029 150a 56          	.byte 0x56
 3030 150b B3 0F 00 00 	.4byte 0xfb3
 3031 150f 01          	.byte 0x1
 3032 1510 05          	.byte 0x5
 3033 1511 03          	.byte 0x3
 3034 1512 00 00 00 00 	.4byte _pCDCSrc
 3035 1516 2A          	.uleb128 0x2a
 3036 1517 00 00 00 00 	.4byte .LASF6
MPLAB XC16 ASSEMBLY Listing:   			page 77


 3037 151b 01          	.byte 0x1
 3038 151c 58          	.byte 0x58
 3039 151d E6 00 00 00 	.4byte 0xe6
 3040 1521 01          	.byte 0x1
 3041 1522 05          	.byte 0x5
 3042 1523 03          	.byte 0x3
 3043 1524 00 00 00 00 	.4byte _cdc_tx_len
 3044 1528 2A          	.uleb128 0x2a
 3045 1529 00 00 00 00 	.4byte .LASF7
 3046 152d 01          	.byte 0x1
 3047 152e 59          	.byte 0x59
 3048 152f E6 00 00 00 	.4byte 0xe6
 3049 1533 01          	.byte 0x1
 3050 1534 05          	.byte 0x5
 3051 1535 03          	.byte 0x3
 3052 1536 00 00 00 00 	.4byte _cdc_mem_type
 3053 153a 2B          	.uleb128 0x2b
 3054 153b 63 64 63 5F 	.asciz "cdc_notice"
 3054      6E 6F 74 69 
 3054      63 65 00 
 3055 1546 01          	.byte 0x1
 3056 1547 4E          	.byte 0x4e
 3057 1548 7A 11 00 00 	.4byte 0x117a
 3058 154c 01          	.byte 0x1
 3059 154d 05          	.byte 0x5
 3060 154e 03          	.byte 0x3
 3061 154f 00 00 00 00 	.4byte _cdc_notice
 3062 1553 2A          	.uleb128 0x2a
 3063 1554 00 00 00 00 	.4byte .LASF8
 3064 1558 01          	.byte 0x1
 3065 1559 4D          	.byte 0x4d
 3066 155a 71 10 00 00 	.4byte 0x1071
 3067 155e 01          	.byte 0x1
 3068 155f 05          	.byte 0x5
 3069 1560 03          	.byte 0x3
 3070 1561 00 00 00 00 	.4byte _line_coding
 3071 1565 24          	.uleb128 0x24
 3072 1566 00 00 00 00 	.4byte .LASF9
 3073 156a 08          	.byte 0x8
 3074 156b 8F 03       	.2byte 0x38f
 3075 156d 16 14 00 00 	.4byte 0x1416
 3076 1571 01          	.byte 0x1
 3077 1572 01          	.byte 0x1
 3078 1573 2A          	.uleb128 0x2a
 3079 1574 00 00 00 00 	.4byte .LASF10
 3080 1578 01          	.byte 0x1
 3081 1579 42          	.byte 0x42
 3082 157a 85 15 00 00 	.4byte 0x1585
 3083 157e 01          	.byte 0x1
 3084 157f 05          	.byte 0x5
 3085 1580 03          	.byte 0x3
 3086 1581 00 00 00 00 	.4byte _cdc_data_tx
 3087 1585 25          	.uleb128 0x25
 3088 1586 1B 14 00 00 	.4byte 0x141b
 3089 158a 2A          	.uleb128 0x2a
 3090 158b 00 00 00 00 	.4byte .LASF11
 3091 158f 01          	.byte 0x1
MPLAB XC16 ASSEMBLY Listing:   			page 78


 3092 1590 43          	.byte 0x43
 3093 1591 9C 15 00 00 	.4byte 0x159c
 3094 1595 01          	.byte 0x1
 3095 1596 05          	.byte 0x5
 3096 1597 03          	.byte 0x3
 3097 1598 00 00 00 00 	.4byte _cdc_data_rx
 3098 159c 25          	.uleb128 0x25
 3099 159d 1B 14 00 00 	.4byte 0x141b
 3100 15a1 2B          	.uleb128 0x2b
 3101 15a2 70 43 44 43 	.asciz "pCDCDst"
 3101      44 73 74 00 
 3102 15aa 01          	.byte 0x1
 3103 15ab 57          	.byte 0x57
 3104 15ac B3 0F 00 00 	.4byte 0xfb3
 3105 15b0 01          	.byte 0x1
 3106 15b1 05          	.byte 0x5
 3107 15b2 03          	.byte 0x3
 3108 15b3 00 00 00 00 	.4byte _pCDCDst
 3109 15b7 2A          	.uleb128 0x2a
 3110 15b8 00 00 00 00 	.4byte .LASF12
 3111 15bc 01          	.byte 0x1
 3112 15bd 5B          	.byte 0x5b
 3113 15be E3 05 00 00 	.4byte 0x5e3
 3114 15c2 01          	.byte 0x1
 3115 15c3 05          	.byte 0x5
 3116 15c4 03          	.byte 0x3
 3117 15c5 00 00 00 00 	.4byte _CDCDataOutHandle
 3118 15c9 2A          	.uleb128 0x2a
 3119 15ca 00 00 00 00 	.4byte .LASF13
 3120 15ce 01          	.byte 0x1
 3121 15cf 5C          	.byte 0x5c
 3122 15d0 E3 05 00 00 	.4byte 0x5e3
 3123 15d4 01          	.byte 0x1
 3124 15d5 05          	.byte 0x5
 3125 15d6 03          	.byte 0x3
 3126 15d7 00 00 00 00 	.4byte _CDCDataInHandle
 3127 15db 2A          	.uleb128 0x2a
 3128 15dc 00 00 00 00 	.4byte .LASF14
 3129 15e0 01          	.byte 0x1
 3130 15e1 5F          	.byte 0x5f
 3131 15e2 FB 10 00 00 	.4byte 0x10fb
 3132 15e6 01          	.byte 0x1
 3133 15e7 05          	.byte 0x5
 3134 15e8 03          	.byte 0x3
 3135 15e9 00 00 00 00 	.4byte _control_signal_bitmap
 3136 15ed 2B          	.uleb128 0x2b
 3137 15ee 42 61 75 64 	.asciz "BaudRateGen"
 3137      52 61 74 65 
 3137      47 65 6E 00 
 3138 15fa 01          	.byte 0x1
 3139 15fb 60          	.byte 0x60
 3140 15fc 26 01 00 00 	.4byte 0x126
 3141 1600 01          	.byte 0x1
 3142 1601 05          	.byte 0x5
 3143 1602 03          	.byte 0x3
 3144 1603 00 00 00 00 	.4byte _BaudRateGen
 3145 1607 2A          	.uleb128 0x2a
MPLAB XC16 ASSEMBLY Listing:   			page 79


 3146 1608 00 00 00 00 	.4byte .LASF15
 3147 160c 01          	.byte 0x1
 3148 160d 6F          	.byte 0x6f
 3149 160e 87 14 00 00 	.4byte 0x1487
 3150 1612 01          	.byte 0x1
 3151 1613 05          	.byte 0x5
 3152 1614 03          	.byte 0x3
 3153 1615 00 00 00 00 	.4byte _dummy_encapsulated_cmd_response
 3154 1619 00          	.byte 0x0
 3155                 	.section .debug_abbrev,info
 3156 0000 01          	.uleb128 0x1
 3157 0001 11          	.uleb128 0x11
 3158 0002 01          	.byte 0x1
 3159 0003 25          	.uleb128 0x25
 3160 0004 08          	.uleb128 0x8
 3161 0005 13          	.uleb128 0x13
 3162 0006 0B          	.uleb128 0xb
 3163 0007 03          	.uleb128 0x3
 3164 0008 08          	.uleb128 0x8
 3165 0009 1B          	.uleb128 0x1b
 3166 000a 08          	.uleb128 0x8
 3167 000b 11          	.uleb128 0x11
 3168 000c 01          	.uleb128 0x1
 3169 000d 12          	.uleb128 0x12
 3170 000e 01          	.uleb128 0x1
 3171 000f 10          	.uleb128 0x10
 3172 0010 06          	.uleb128 0x6
 3173 0011 00          	.byte 0x0
 3174 0012 00          	.byte 0x0
 3175 0013 02          	.uleb128 0x2
 3176 0014 24          	.uleb128 0x24
 3177 0015 00          	.byte 0x0
 3178 0016 0B          	.uleb128 0xb
 3179 0017 0B          	.uleb128 0xb
 3180 0018 3E          	.uleb128 0x3e
 3181 0019 0B          	.uleb128 0xb
 3182 001a 03          	.uleb128 0x3
 3183 001b 08          	.uleb128 0x8
 3184 001c 00          	.byte 0x0
 3185 001d 00          	.byte 0x0
 3186 001e 03          	.uleb128 0x3
 3187 001f 16          	.uleb128 0x16
 3188 0020 00          	.byte 0x0
 3189 0021 03          	.uleb128 0x3
 3190 0022 08          	.uleb128 0x8
 3191 0023 3A          	.uleb128 0x3a
 3192 0024 0B          	.uleb128 0xb
 3193 0025 3B          	.uleb128 0x3b
 3194 0026 0B          	.uleb128 0xb
 3195 0027 49          	.uleb128 0x49
 3196 0028 13          	.uleb128 0x13
 3197 0029 00          	.byte 0x0
 3198 002a 00          	.byte 0x0
 3199 002b 04          	.uleb128 0x4
 3200 002c 13          	.uleb128 0x13
 3201 002d 01          	.byte 0x1
 3202 002e 03          	.uleb128 0x3
MPLAB XC16 ASSEMBLY Listing:   			page 80


 3203 002f 08          	.uleb128 0x8
 3204 0030 0B          	.uleb128 0xb
 3205 0031 0B          	.uleb128 0xb
 3206 0032 3A          	.uleb128 0x3a
 3207 0033 0B          	.uleb128 0xb
 3208 0034 3B          	.uleb128 0x3b
 3209 0035 05          	.uleb128 0x5
 3210 0036 01          	.uleb128 0x1
 3211 0037 13          	.uleb128 0x13
 3212 0038 00          	.byte 0x0
 3213 0039 00          	.byte 0x0
 3214 003a 05          	.uleb128 0x5
 3215 003b 0D          	.uleb128 0xd
 3216 003c 00          	.byte 0x0
 3217 003d 03          	.uleb128 0x3
 3218 003e 08          	.uleb128 0x8
 3219 003f 3A          	.uleb128 0x3a
 3220 0040 0B          	.uleb128 0xb
 3221 0041 3B          	.uleb128 0x3b
 3222 0042 05          	.uleb128 0x5
 3223 0043 49          	.uleb128 0x49
 3224 0044 13          	.uleb128 0x13
 3225 0045 0B          	.uleb128 0xb
 3226 0046 0B          	.uleb128 0xb
 3227 0047 0D          	.uleb128 0xd
 3228 0048 0B          	.uleb128 0xb
 3229 0049 0C          	.uleb128 0xc
 3230 004a 0B          	.uleb128 0xb
 3231 004b 38          	.uleb128 0x38
 3232 004c 0A          	.uleb128 0xa
 3233 004d 00          	.byte 0x0
 3234 004e 00          	.byte 0x0
 3235 004f 06          	.uleb128 0x6
 3236 0050 16          	.uleb128 0x16
 3237 0051 00          	.byte 0x0
 3238 0052 03          	.uleb128 0x3
 3239 0053 08          	.uleb128 0x8
 3240 0054 3A          	.uleb128 0x3a
 3241 0055 0B          	.uleb128 0xb
 3242 0056 3B          	.uleb128 0x3b
 3243 0057 05          	.uleb128 0x5
 3244 0058 49          	.uleb128 0x49
 3245 0059 13          	.uleb128 0x13
 3246 005a 00          	.byte 0x0
 3247 005b 00          	.byte 0x0
 3248 005c 07          	.uleb128 0x7
 3249 005d 04          	.uleb128 0x4
 3250 005e 01          	.byte 0x1
 3251 005f 0B          	.uleb128 0xb
 3252 0060 0B          	.uleb128 0xb
 3253 0061 3A          	.uleb128 0x3a
 3254 0062 0B          	.uleb128 0xb
 3255 0063 3B          	.uleb128 0x3b
 3256 0064 0B          	.uleb128 0xb
 3257 0065 01          	.uleb128 0x1
 3258 0066 13          	.uleb128 0x13
 3259 0067 00          	.byte 0x0
MPLAB XC16 ASSEMBLY Listing:   			page 81


 3260 0068 00          	.byte 0x0
 3261 0069 08          	.uleb128 0x8
 3262 006a 28          	.uleb128 0x28
 3263 006b 00          	.byte 0x0
 3264 006c 03          	.uleb128 0x3
 3265 006d 08          	.uleb128 0x8
 3266 006e 1C          	.uleb128 0x1c
 3267 006f 0D          	.uleb128 0xd
 3268 0070 00          	.byte 0x0
 3269 0071 00          	.byte 0x0
 3270 0072 09          	.uleb128 0x9
 3271 0073 0F          	.uleb128 0xf
 3272 0074 00          	.byte 0x0
 3273 0075 0B          	.uleb128 0xb
 3274 0076 0B          	.uleb128 0xb
 3275 0077 00          	.byte 0x0
 3276 0078 00          	.byte 0x0
 3277 0079 0A          	.uleb128 0xa
 3278 007a 13          	.uleb128 0x13
 3279 007b 01          	.byte 0x1
 3280 007c 0B          	.uleb128 0xb
 3281 007d 0B          	.uleb128 0xb
 3282 007e 3A          	.uleb128 0x3a
 3283 007f 0B          	.uleb128 0xb
 3284 0080 3B          	.uleb128 0x3b
 3285 0081 05          	.uleb128 0x5
 3286 0082 01          	.uleb128 0x1
 3287 0083 13          	.uleb128 0x13
 3288 0084 00          	.byte 0x0
 3289 0085 00          	.byte 0x0
 3290 0086 0B          	.uleb128 0xb
 3291 0087 0D          	.uleb128 0xd
 3292 0088 00          	.byte 0x0
 3293 0089 03          	.uleb128 0x3
 3294 008a 0E          	.uleb128 0xe
 3295 008b 3A          	.uleb128 0x3a
 3296 008c 0B          	.uleb128 0xb
 3297 008d 3B          	.uleb128 0x3b
 3298 008e 05          	.uleb128 0x5
 3299 008f 49          	.uleb128 0x49
 3300 0090 13          	.uleb128 0x13
 3301 0091 38          	.uleb128 0x38
 3302 0092 0A          	.uleb128 0xa
 3303 0093 00          	.byte 0x0
 3304 0094 00          	.byte 0x0
 3305 0095 0C          	.uleb128 0xc
 3306 0096 0D          	.uleb128 0xd
 3307 0097 00          	.byte 0x0
 3308 0098 03          	.uleb128 0x3
 3309 0099 08          	.uleb128 0x8
 3310 009a 3A          	.uleb128 0x3a
 3311 009b 0B          	.uleb128 0xb
 3312 009c 3B          	.uleb128 0x3b
 3313 009d 05          	.uleb128 0x5
 3314 009e 49          	.uleb128 0x49
 3315 009f 13          	.uleb128 0x13
 3316 00a0 38          	.uleb128 0x38
MPLAB XC16 ASSEMBLY Listing:   			page 82


 3317 00a1 0A          	.uleb128 0xa
 3318 00a2 00          	.byte 0x0
 3319 00a3 00          	.byte 0x0
 3320 00a4 0D          	.uleb128 0xd
 3321 00a5 17          	.uleb128 0x17
 3322 00a6 01          	.byte 0x1
 3323 00a7 0B          	.uleb128 0xb
 3324 00a8 0B          	.uleb128 0xb
 3325 00a9 3A          	.uleb128 0x3a
 3326 00aa 0B          	.uleb128 0xb
 3327 00ab 3B          	.uleb128 0x3b
 3328 00ac 05          	.uleb128 0x5
 3329 00ad 01          	.uleb128 0x1
 3330 00ae 13          	.uleb128 0x13
 3331 00af 00          	.byte 0x0
 3332 00b0 00          	.byte 0x0
 3333 00b1 0E          	.uleb128 0xe
 3334 00b2 0D          	.uleb128 0xd
 3335 00b3 00          	.byte 0x0
 3336 00b4 03          	.uleb128 0x3
 3337 00b5 08          	.uleb128 0x8
 3338 00b6 3A          	.uleb128 0x3a
 3339 00b7 0B          	.uleb128 0xb
 3340 00b8 3B          	.uleb128 0x3b
 3341 00b9 05          	.uleb128 0x5
 3342 00ba 49          	.uleb128 0x49
 3343 00bb 13          	.uleb128 0x13
 3344 00bc 00          	.byte 0x0
 3345 00bd 00          	.byte 0x0
 3346 00be 0F          	.uleb128 0xf
 3347 00bf 01          	.uleb128 0x1
 3348 00c0 01          	.byte 0x1
 3349 00c1 49          	.uleb128 0x49
 3350 00c2 13          	.uleb128 0x13
 3351 00c3 01          	.uleb128 0x1
 3352 00c4 13          	.uleb128 0x13
 3353 00c5 00          	.byte 0x0
 3354 00c6 00          	.byte 0x0
 3355 00c7 10          	.uleb128 0x10
 3356 00c8 21          	.uleb128 0x21
 3357 00c9 00          	.byte 0x0
 3358 00ca 49          	.uleb128 0x49
 3359 00cb 13          	.uleb128 0x13
 3360 00cc 2F          	.uleb128 0x2f
 3361 00cd 0B          	.uleb128 0xb
 3362 00ce 00          	.byte 0x0
 3363 00cf 00          	.byte 0x0
 3364 00d0 11          	.uleb128 0x11
 3365 00d1 0D          	.uleb128 0xd
 3366 00d2 00          	.byte 0x0
 3367 00d3 03          	.uleb128 0x3
 3368 00d4 0E          	.uleb128 0xe
 3369 00d5 3A          	.uleb128 0x3a
 3370 00d6 0B          	.uleb128 0xb
 3371 00d7 3B          	.uleb128 0x3b
 3372 00d8 05          	.uleb128 0x5
 3373 00d9 49          	.uleb128 0x49
MPLAB XC16 ASSEMBLY Listing:   			page 83


 3374 00da 13          	.uleb128 0x13
 3375 00db 00          	.byte 0x0
 3376 00dc 00          	.byte 0x0
 3377 00dd 12          	.uleb128 0x12
 3378 00de 0D          	.uleb128 0xd
 3379 00df 00          	.byte 0x0
 3380 00e0 49          	.uleb128 0x49
 3381 00e1 13          	.uleb128 0x13
 3382 00e2 00          	.byte 0x0
 3383 00e3 00          	.byte 0x0
 3384 00e4 13          	.uleb128 0x13
 3385 00e5 0F          	.uleb128 0xf
 3386 00e6 00          	.byte 0x0
 3387 00e7 0B          	.uleb128 0xb
 3388 00e8 0B          	.uleb128 0xb
 3389 00e9 49          	.uleb128 0x49
 3390 00ea 13          	.uleb128 0x13
 3391 00eb 00          	.byte 0x0
 3392 00ec 00          	.byte 0x0
 3393 00ed 14          	.uleb128 0x14
 3394 00ee 26          	.uleb128 0x26
 3395 00ef 00          	.byte 0x0
 3396 00f0 49          	.uleb128 0x49
 3397 00f1 13          	.uleb128 0x13
 3398 00f2 00          	.byte 0x0
 3399 00f3 00          	.byte 0x0
 3400 00f4 15          	.uleb128 0x15
 3401 00f5 0D          	.uleb128 0xd
 3402 00f6 00          	.byte 0x0
 3403 00f7 03          	.uleb128 0x3
 3404 00f8 0E          	.uleb128 0xe
 3405 00f9 3A          	.uleb128 0x3a
 3406 00fa 0B          	.uleb128 0xb
 3407 00fb 3B          	.uleb128 0x3b
 3408 00fc 05          	.uleb128 0x5
 3409 00fd 49          	.uleb128 0x49
 3410 00fe 13          	.uleb128 0x13
 3411 00ff 0B          	.uleb128 0xb
 3412 0100 0B          	.uleb128 0xb
 3413 0101 0D          	.uleb128 0xd
 3414 0102 0B          	.uleb128 0xb
 3415 0103 0C          	.uleb128 0xc
 3416 0104 0B          	.uleb128 0xb
 3417 0105 38          	.uleb128 0x38
 3418 0106 0A          	.uleb128 0xa
 3419 0107 00          	.byte 0x0
 3420 0108 00          	.byte 0x0
 3421 0109 16          	.uleb128 0x16
 3422 010a 15          	.uleb128 0x15
 3423 010b 00          	.byte 0x0
 3424 010c 27          	.uleb128 0x27
 3425 010d 0C          	.uleb128 0xc
 3426 010e 00          	.byte 0x0
 3427 010f 00          	.byte 0x0
 3428 0110 17          	.uleb128 0x17
 3429 0111 13          	.uleb128 0x13
 3430 0112 01          	.byte 0x1
MPLAB XC16 ASSEMBLY Listing:   			page 84


 3431 0113 0B          	.uleb128 0xb
 3432 0114 0B          	.uleb128 0xb
 3433 0115 3A          	.uleb128 0x3a
 3434 0116 0B          	.uleb128 0xb
 3435 0117 3B          	.uleb128 0x3b
 3436 0118 0B          	.uleb128 0xb
 3437 0119 01          	.uleb128 0x1
 3438 011a 13          	.uleb128 0x13
 3439 011b 00          	.byte 0x0
 3440 011c 00          	.byte 0x0
 3441 011d 18          	.uleb128 0x18
 3442 011e 0D          	.uleb128 0xd
 3443 011f 00          	.byte 0x0
 3444 0120 03          	.uleb128 0x3
 3445 0121 08          	.uleb128 0x8
 3446 0122 3A          	.uleb128 0x3a
 3447 0123 0B          	.uleb128 0xb
 3448 0124 3B          	.uleb128 0x3b
 3449 0125 0B          	.uleb128 0xb
 3450 0126 49          	.uleb128 0x49
 3451 0127 13          	.uleb128 0x13
 3452 0128 0B          	.uleb128 0xb
 3453 0129 0B          	.uleb128 0xb
 3454 012a 0D          	.uleb128 0xd
 3455 012b 0B          	.uleb128 0xb
 3456 012c 0C          	.uleb128 0xc
 3457 012d 0B          	.uleb128 0xb
 3458 012e 38          	.uleb128 0x38
 3459 012f 0A          	.uleb128 0xa
 3460 0130 00          	.byte 0x0
 3461 0131 00          	.byte 0x0
 3462 0132 19          	.uleb128 0x19
 3463 0133 17          	.uleb128 0x17
 3464 0134 01          	.byte 0x1
 3465 0135 03          	.uleb128 0x3
 3466 0136 08          	.uleb128 0x8
 3467 0137 0B          	.uleb128 0xb
 3468 0138 0B          	.uleb128 0xb
 3469 0139 3A          	.uleb128 0x3a
 3470 013a 0B          	.uleb128 0xb
 3471 013b 3B          	.uleb128 0x3b
 3472 013c 0B          	.uleb128 0xb
 3473 013d 01          	.uleb128 0x1
 3474 013e 13          	.uleb128 0x13
 3475 013f 00          	.byte 0x0
 3476 0140 00          	.byte 0x0
 3477 0141 1A          	.uleb128 0x1a
 3478 0142 0D          	.uleb128 0xd
 3479 0143 00          	.byte 0x0
 3480 0144 03          	.uleb128 0x3
 3481 0145 08          	.uleb128 0x8
 3482 0146 3A          	.uleb128 0x3a
 3483 0147 0B          	.uleb128 0xb
 3484 0148 3B          	.uleb128 0x3b
 3485 0149 0B          	.uleb128 0xb
 3486 014a 49          	.uleb128 0x49
 3487 014b 13          	.uleb128 0x13
MPLAB XC16 ASSEMBLY Listing:   			page 85


 3488 014c 00          	.byte 0x0
 3489 014d 00          	.byte 0x0
 3490 014e 1B          	.uleb128 0x1b
 3491 014f 0D          	.uleb128 0xd
 3492 0150 00          	.byte 0x0
 3493 0151 03          	.uleb128 0x3
 3494 0152 08          	.uleb128 0x8
 3495 0153 3A          	.uleb128 0x3a
 3496 0154 0B          	.uleb128 0xb
 3497 0155 3B          	.uleb128 0x3b
 3498 0156 0B          	.uleb128 0xb
 3499 0157 49          	.uleb128 0x49
 3500 0158 13          	.uleb128 0x13
 3501 0159 38          	.uleb128 0x38
 3502 015a 0A          	.uleb128 0xa
 3503 015b 00          	.byte 0x0
 3504 015c 00          	.byte 0x0
 3505 015d 1C          	.uleb128 0x1c
 3506 015e 17          	.uleb128 0x17
 3507 015f 01          	.byte 0x1
 3508 0160 0B          	.uleb128 0xb
 3509 0161 0B          	.uleb128 0xb
 3510 0162 3A          	.uleb128 0x3a
 3511 0163 0B          	.uleb128 0xb
 3512 0164 3B          	.uleb128 0x3b
 3513 0165 0B          	.uleb128 0xb
 3514 0166 01          	.uleb128 0x1
 3515 0167 13          	.uleb128 0x13
 3516 0168 00          	.byte 0x0
 3517 0169 00          	.byte 0x0
 3518 016a 1D          	.uleb128 0x1d
 3519 016b 17          	.uleb128 0x17
 3520 016c 01          	.byte 0x1
 3521 016d 03          	.uleb128 0x3
 3522 016e 08          	.uleb128 0x8
 3523 016f 0B          	.uleb128 0xb
 3524 0170 0B          	.uleb128 0xb
 3525 0171 3A          	.uleb128 0x3a
 3526 0172 0B          	.uleb128 0xb
 3527 0173 3B          	.uleb128 0x3b
 3528 0174 05          	.uleb128 0x5
 3529 0175 01          	.uleb128 0x1
 3530 0176 13          	.uleb128 0x13
 3531 0177 00          	.byte 0x0
 3532 0178 00          	.byte 0x0
 3533 0179 1E          	.uleb128 0x1e
 3534 017a 2E          	.uleb128 0x2e
 3535 017b 00          	.byte 0x0
 3536 017c 3F          	.uleb128 0x3f
 3537 017d 0C          	.uleb128 0xc
 3538 017e 03          	.uleb128 0x3
 3539 017f 08          	.uleb128 0x8
 3540 0180 3A          	.uleb128 0x3a
 3541 0181 0B          	.uleb128 0xb
 3542 0182 3B          	.uleb128 0x3b
 3543 0183 0B          	.uleb128 0xb
 3544 0184 27          	.uleb128 0x27
MPLAB XC16 ASSEMBLY Listing:   			page 86


 3545 0185 0C          	.uleb128 0xc
 3546 0186 11          	.uleb128 0x11
 3547 0187 01          	.uleb128 0x1
 3548 0188 12          	.uleb128 0x12
 3549 0189 01          	.uleb128 0x1
 3550 018a 40          	.uleb128 0x40
 3551 018b 0A          	.uleb128 0xa
 3552 018c 00          	.byte 0x0
 3553 018d 00          	.byte 0x0
 3554 018e 1F          	.uleb128 0x1f
 3555 018f 2E          	.uleb128 0x2e
 3556 0190 00          	.byte 0x0
 3557 0191 3F          	.uleb128 0x3f
 3558 0192 0C          	.uleb128 0xc
 3559 0193 03          	.uleb128 0x3
 3560 0194 08          	.uleb128 0x8
 3561 0195 3A          	.uleb128 0x3a
 3562 0196 0B          	.uleb128 0xb
 3563 0197 3B          	.uleb128 0x3b
 3564 0198 05          	.uleb128 0x5
 3565 0199 27          	.uleb128 0x27
 3566 019a 0C          	.uleb128 0xc
 3567 019b 11          	.uleb128 0x11
 3568 019c 01          	.uleb128 0x1
 3569 019d 12          	.uleb128 0x12
 3570 019e 01          	.uleb128 0x1
 3571 019f 40          	.uleb128 0x40
 3572 01a0 0A          	.uleb128 0xa
 3573 01a1 00          	.byte 0x0
 3574 01a2 00          	.byte 0x0
 3575 01a3 20          	.uleb128 0x20
 3576 01a4 2E          	.uleb128 0x2e
 3577 01a5 01          	.byte 0x1
 3578 01a6 3F          	.uleb128 0x3f
 3579 01a7 0C          	.uleb128 0xc
 3580 01a8 03          	.uleb128 0x3
 3581 01a9 08          	.uleb128 0x8
 3582 01aa 3A          	.uleb128 0x3a
 3583 01ab 0B          	.uleb128 0xb
 3584 01ac 3B          	.uleb128 0x3b
 3585 01ad 05          	.uleb128 0x5
 3586 01ae 27          	.uleb128 0x27
 3587 01af 0C          	.uleb128 0xc
 3588 01b0 49          	.uleb128 0x49
 3589 01b1 13          	.uleb128 0x13
 3590 01b2 11          	.uleb128 0x11
 3591 01b3 01          	.uleb128 0x1
 3592 01b4 12          	.uleb128 0x12
 3593 01b5 01          	.uleb128 0x1
 3594 01b6 40          	.uleb128 0x40
 3595 01b7 0A          	.uleb128 0xa
 3596 01b8 01          	.uleb128 0x1
 3597 01b9 13          	.uleb128 0x13
 3598 01ba 00          	.byte 0x0
 3599 01bb 00          	.byte 0x0
 3600 01bc 21          	.uleb128 0x21
 3601 01bd 05          	.uleb128 0x5
MPLAB XC16 ASSEMBLY Listing:   			page 87


 3602 01be 00          	.byte 0x0
 3603 01bf 03          	.uleb128 0x3
 3604 01c0 08          	.uleb128 0x8
 3605 01c1 3A          	.uleb128 0x3a
 3606 01c2 0B          	.uleb128 0xb
 3607 01c3 3B          	.uleb128 0x3b
 3608 01c4 05          	.uleb128 0x5
 3609 01c5 49          	.uleb128 0x49
 3610 01c6 13          	.uleb128 0x13
 3611 01c7 02          	.uleb128 0x2
 3612 01c8 0A          	.uleb128 0xa
 3613 01c9 00          	.byte 0x0
 3614 01ca 00          	.byte 0x0
 3615 01cb 22          	.uleb128 0x22
 3616 01cc 2E          	.uleb128 0x2e
 3617 01cd 01          	.byte 0x1
 3618 01ce 3F          	.uleb128 0x3f
 3619 01cf 0C          	.uleb128 0xc
 3620 01d0 03          	.uleb128 0x3
 3621 01d1 08          	.uleb128 0x8
 3622 01d2 3A          	.uleb128 0x3a
 3623 01d3 0B          	.uleb128 0xb
 3624 01d4 3B          	.uleb128 0x3b
 3625 01d5 05          	.uleb128 0x5
 3626 01d6 27          	.uleb128 0x27
 3627 01d7 0C          	.uleb128 0xc
 3628 01d8 11          	.uleb128 0x11
 3629 01d9 01          	.uleb128 0x1
 3630 01da 12          	.uleb128 0x12
 3631 01db 01          	.uleb128 0x1
 3632 01dc 40          	.uleb128 0x40
 3633 01dd 0A          	.uleb128 0xa
 3634 01de 01          	.uleb128 0x1
 3635 01df 13          	.uleb128 0x13
 3636 01e0 00          	.byte 0x0
 3637 01e1 00          	.byte 0x0
 3638 01e2 23          	.uleb128 0x23
 3639 01e3 34          	.uleb128 0x34
 3640 01e4 00          	.byte 0x0
 3641 01e5 03          	.uleb128 0x3
 3642 01e6 08          	.uleb128 0x8
 3643 01e7 3A          	.uleb128 0x3a
 3644 01e8 0B          	.uleb128 0xb
 3645 01e9 3B          	.uleb128 0x3b
 3646 01ea 05          	.uleb128 0x5
 3647 01eb 49          	.uleb128 0x49
 3648 01ec 13          	.uleb128 0x13
 3649 01ed 02          	.uleb128 0x2
 3650 01ee 0A          	.uleb128 0xa
 3651 01ef 00          	.byte 0x0
 3652 01f0 00          	.byte 0x0
 3653 01f1 24          	.uleb128 0x24
 3654 01f2 34          	.uleb128 0x34
 3655 01f3 00          	.byte 0x0
 3656 01f4 03          	.uleb128 0x3
 3657 01f5 0E          	.uleb128 0xe
 3658 01f6 3A          	.uleb128 0x3a
MPLAB XC16 ASSEMBLY Listing:   			page 88


 3659 01f7 0B          	.uleb128 0xb
 3660 01f8 3B          	.uleb128 0x3b
 3661 01f9 05          	.uleb128 0x5
 3662 01fa 49          	.uleb128 0x49
 3663 01fb 13          	.uleb128 0x13
 3664 01fc 3F          	.uleb128 0x3f
 3665 01fd 0C          	.uleb128 0xc
 3666 01fe 3C          	.uleb128 0x3c
 3667 01ff 0C          	.uleb128 0xc
 3668 0200 00          	.byte 0x0
 3669 0201 00          	.byte 0x0
 3670 0202 25          	.uleb128 0x25
 3671 0203 35          	.uleb128 0x35
 3672 0204 00          	.byte 0x0
 3673 0205 49          	.uleb128 0x49
 3674 0206 13          	.uleb128 0x13
 3675 0207 00          	.byte 0x0
 3676 0208 00          	.byte 0x0
 3677 0209 26          	.uleb128 0x26
 3678 020a 21          	.uleb128 0x21
 3679 020b 00          	.byte 0x0
 3680 020c 00          	.byte 0x0
 3681 020d 00          	.byte 0x0
 3682 020e 27          	.uleb128 0x27
 3683 020f 34          	.uleb128 0x34
 3684 0210 00          	.byte 0x0
 3685 0211 03          	.uleb128 0x3
 3686 0212 08          	.uleb128 0x8
 3687 0213 3A          	.uleb128 0x3a
 3688 0214 0B          	.uleb128 0xb
 3689 0215 3B          	.uleb128 0x3b
 3690 0216 05          	.uleb128 0x5
 3691 0217 49          	.uleb128 0x49
 3692 0218 13          	.uleb128 0x13
 3693 0219 3F          	.uleb128 0x3f
 3694 021a 0C          	.uleb128 0xc
 3695 021b 3C          	.uleb128 0x3c
 3696 021c 0C          	.uleb128 0xc
 3697 021d 00          	.byte 0x0
 3698 021e 00          	.byte 0x0
 3699 021f 28          	.uleb128 0x28
 3700 0220 34          	.uleb128 0x34
 3701 0221 00          	.byte 0x0
 3702 0222 03          	.uleb128 0x3
 3703 0223 0E          	.uleb128 0xe
 3704 0224 3A          	.uleb128 0x3a
 3705 0225 0B          	.uleb128 0xb
 3706 0226 3B          	.uleb128 0x3b
 3707 0227 0B          	.uleb128 0xb
 3708 0228 49          	.uleb128 0x49
 3709 0229 13          	.uleb128 0x13
 3710 022a 3F          	.uleb128 0x3f
 3711 022b 0C          	.uleb128 0xc
 3712 022c 3C          	.uleb128 0x3c
 3713 022d 0C          	.uleb128 0xc
 3714 022e 00          	.byte 0x0
 3715 022f 00          	.byte 0x0
MPLAB XC16 ASSEMBLY Listing:   			page 89


 3716 0230 29          	.uleb128 0x29
 3717 0231 34          	.uleb128 0x34
 3718 0232 00          	.byte 0x0
 3719 0233 03          	.uleb128 0x3
 3720 0234 08          	.uleb128 0x8
 3721 0235 3A          	.uleb128 0x3a
 3722 0236 0B          	.uleb128 0xb
 3723 0237 3B          	.uleb128 0x3b
 3724 0238 0B          	.uleb128 0xb
 3725 0239 49          	.uleb128 0x49
 3726 023a 13          	.uleb128 0x13
 3727 023b 3F          	.uleb128 0x3f
 3728 023c 0C          	.uleb128 0xc
 3729 023d 3C          	.uleb128 0x3c
 3730 023e 0C          	.uleb128 0xc
 3731 023f 00          	.byte 0x0
 3732 0240 00          	.byte 0x0
 3733 0241 2A          	.uleb128 0x2a
 3734 0242 34          	.uleb128 0x34
 3735 0243 00          	.byte 0x0
 3736 0244 03          	.uleb128 0x3
 3737 0245 0E          	.uleb128 0xe
 3738 0246 3A          	.uleb128 0x3a
 3739 0247 0B          	.uleb128 0xb
 3740 0248 3B          	.uleb128 0x3b
 3741 0249 0B          	.uleb128 0xb
 3742 024a 49          	.uleb128 0x49
 3743 024b 13          	.uleb128 0x13
 3744 024c 3F          	.uleb128 0x3f
 3745 024d 0C          	.uleb128 0xc
 3746 024e 02          	.uleb128 0x2
 3747 024f 0A          	.uleb128 0xa
 3748 0250 00          	.byte 0x0
 3749 0251 00          	.byte 0x0
 3750 0252 2B          	.uleb128 0x2b
 3751 0253 34          	.uleb128 0x34
 3752 0254 00          	.byte 0x0
 3753 0255 03          	.uleb128 0x3
 3754 0256 08          	.uleb128 0x8
 3755 0257 3A          	.uleb128 0x3a
 3756 0258 0B          	.uleb128 0xb
 3757 0259 3B          	.uleb128 0x3b
 3758 025a 0B          	.uleb128 0xb
 3759 025b 49          	.uleb128 0x49
 3760 025c 13          	.uleb128 0x13
 3761 025d 3F          	.uleb128 0x3f
 3762 025e 0C          	.uleb128 0xc
 3763 025f 02          	.uleb128 0x2
 3764 0260 0A          	.uleb128 0xa
 3765 0261 00          	.byte 0x0
 3766 0262 00          	.byte 0x0
 3767 0263 00          	.byte 0x0
 3768                 	.section .debug_pubnames,info
 3769 0000 9C 01 00 00 	.4byte 0x19c
 3770 0004 02 00       	.2byte 0x2
 3771 0006 00 00 00 00 	.4byte .Ldebug_info0
 3772 000a 1A 16 00 00 	.4byte 0x161a
MPLAB XC16 ASSEMBLY Listing:   			page 90


 3773 000e 8D 11 00 00 	.4byte 0x118d
 3774 0012 55 53 42 43 	.asciz "USBCheckCDCRequest"
 3774      68 65 63 6B 
 3774      43 44 43 52 
 3774      65 71 75 65 
 3774      73 74 00 
 3775 0025 AF 11 00 00 	.4byte 0x11af
 3776 0029 43 44 43 49 	.asciz "CDCInitEP"
 3776      6E 69 74 45 
 3776      50 00 
 3777 0033 C9 11 00 00 	.4byte 0x11c9
 3778 0037 55 53 42 43 	.asciz "USBCDCEventHandler"
 3778      44 43 45 76 
 3778      65 6E 74 48 
 3778      61 6E 64 6C 
 3778      65 72 00 
 3779 004a 27 12 00 00 	.4byte 0x1227
 3780 004e 67 65 74 73 	.asciz "getsUSBUSART"
 3780      55 53 42 55 
 3780      53 41 52 54 
 3780      00 
 3781 005b 6E 12 00 00 	.4byte 0x126e
 3782 005f 70 75 74 55 	.asciz "putUSBUSART"
 3782      53 42 55 53 
 3782      41 52 54 00 
 3783 006b B1 12 00 00 	.4byte 0x12b1
 3784 006f 70 75 74 73 	.asciz "putsUSBUSART"
 3784      55 53 42 55 
 3784      53 41 52 54 
 3784      00 
 3785 007c 11 13 00 00 	.4byte 0x1311
 3786 0080 43 44 43 54 	.asciz "CDCTxService"
 3786      78 53 65 72 
 3786      76 69 63 65 
 3786      00 
 3787 008d DC 14 00 00 	.4byte 0x14dc
 3788 0091 63 64 63 5F 	.asciz "cdc_rx_len"
 3788      72 78 5F 6C 
 3788      65 6E 00 
 3789 009c EE 14 00 00 	.4byte 0x14ee
 3790 00a0 63 64 63 5F 	.asciz "cdc_trf_state"
 3790      74 72 66 5F 
 3790      73 74 61 74 
 3790      65 00 
 3791 00ae 00 15 00 00 	.4byte 0x1500
 3792 00b2 70 43 44 43 	.asciz "pCDCSrc"
 3792      53 72 63 00 
 3793 00ba 16 15 00 00 	.4byte 0x1516
 3794 00be 63 64 63 5F 	.asciz "cdc_tx_len"
 3794      74 78 5F 6C 
 3794      65 6E 00 
 3795 00c9 28 15 00 00 	.4byte 0x1528
 3796 00cd 63 64 63 5F 	.asciz "cdc_mem_type"
 3796      6D 65 6D 5F 
 3796      74 79 70 65 
 3796      00 
 3797 00da 3A 15 00 00 	.4byte 0x153a
MPLAB XC16 ASSEMBLY Listing:   			page 91


 3798 00de 63 64 63 5F 	.asciz "cdc_notice"
 3798      6E 6F 74 69 
 3798      63 65 00 
 3799 00e9 53 15 00 00 	.4byte 0x1553
 3800 00ed 6C 69 6E 65 	.asciz "line_coding"
 3800      5F 63 6F 64 
 3800      69 6E 67 00 
 3801 00f9 73 15 00 00 	.4byte 0x1573
 3802 00fd 63 64 63 5F 	.asciz "cdc_data_tx"
 3802      64 61 74 61 
 3802      5F 74 78 00 
 3803 0109 8A 15 00 00 	.4byte 0x158a
 3804 010d 63 64 63 5F 	.asciz "cdc_data_rx"
 3804      64 61 74 61 
 3804      5F 72 78 00 
 3805 0119 A1 15 00 00 	.4byte 0x15a1
 3806 011d 70 43 44 43 	.asciz "pCDCDst"
 3806      44 73 74 00 
 3807 0125 B7 15 00 00 	.4byte 0x15b7
 3808 0129 43 44 43 44 	.asciz "CDCDataOutHandle"
 3808      61 74 61 4F 
 3808      75 74 48 61 
 3808      6E 64 6C 65 
 3808      00 
 3809 013a C9 15 00 00 	.4byte 0x15c9
 3810 013e 43 44 43 44 	.asciz "CDCDataInHandle"
 3810      61 74 61 49 
 3810      6E 48 61 6E 
 3810      64 6C 65 00 
 3811 014e DB 15 00 00 	.4byte 0x15db
 3812 0152 63 6F 6E 74 	.asciz "control_signal_bitmap"
 3812      72 6F 6C 5F 
 3812      73 69 67 6E 
 3812      61 6C 5F 62 
 3812      69 74 6D 61 
 3812      70 00 
 3813 0168 ED 15 00 00 	.4byte 0x15ed
 3814 016c 42 61 75 64 	.asciz "BaudRateGen"
 3814      52 61 74 65 
 3814      47 65 6E 00 
 3815 0178 07 16 00 00 	.4byte 0x1607
 3816 017c 64 75 6D 6D 	.asciz "dummy_encapsulated_cmd_response"
 3816      79 5F 65 6E 
 3816      63 61 70 73 
 3816      75 6C 61 74 
 3816      65 64 5F 63 
 3816      6D 64 5F 72 
 3816      65 73 70 6F 
 3816      6E 73 65 00 
 3817 019c 00 00 00 00 	.4byte 0x0
 3818                 	.section .debug_pubtypes,info
 3819 0000 5B 01 00 00 	.4byte 0x15b
 3820 0004 02 00       	.2byte 0x2
 3821 0006 00 00 00 00 	.4byte .Ldebug_info0
 3822 000a 1A 16 00 00 	.4byte 0x161a
 3823 000e E6 00 00 00 	.4byte 0xe6
 3824 0012 75 69 6E 74 	.asciz "uint8_t"
MPLAB XC16 ASSEMBLY Listing:   			page 92


 3824      38 5F 74 00 
 3825 001a 06 01 00 00 	.4byte 0x106
 3826 001e 75 69 6E 74 	.asciz "uint16_t"
 3826      31 36 5F 74 
 3826      00 
 3827 0027 26 01 00 00 	.4byte 0x126
 3828 002b 75 69 6E 74 	.asciz "uint32_t"
 3828      33 32 5F 74 
 3828      00 
 3829 0034 65 01 00 00 	.4byte 0x165
 3830 0038 74 61 67 49 	.asciz "tagIEC5BITS"
 3830      45 43 35 42 
 3830      49 54 53 00 
 3831 0044 89 02 00 00 	.4byte 0x289
 3832 0048 49 45 43 35 	.asciz "IEC5BITS"
 3832      42 49 54 53 
 3832      00 
 3833 0051 C8 05 00 00 	.4byte 0x5c8
 3834 0055 55 53 42 5F 	.asciz "USB_EVENT"
 3834      45 56 45 4E 
 3834      54 00 
 3835 005f 33 0A 00 00 	.4byte 0xa33
 3836 0063 43 54 52 4C 	.asciz "CTRL_TRF_SETUP"
 3836      5F 54 52 46 
 3836      5F 53 45 54 
 3836      55 50 00 
 3837 0072 0F 0B 00 00 	.4byte 0xb0f
 3838 0076 75 69 6E 74 	.asciz "uint16_t_VAL"
 3838      31 36 5F 74 
 3838      5F 56 41 4C 
 3838      00 
 3839 0083 47 0C 00 00 	.4byte 0xc47
 3840 0087 49 4E 5F 50 	.asciz "IN_PIPE"
 3840      49 50 45 00 
 3841 008f 22 0D 00 00 	.4byte 0xd22
 3842 0093 4F 55 54 5F 	.asciz "OUT_PIPE"
 3842      50 49 50 45 
 3842      00 
 3843 009c 07 0E 00 00 	.4byte 0xe07
 3844 00a0 5F 42 44 5F 	.asciz "_BD_STAT"
 3844      53 54 41 54 
 3844      00 
 3845 00a9 33 0E 00 00 	.4byte 0xe33
 3846 00ad 42 44 5F 53 	.asciz "BD_STAT"
 3846      54 41 54 00 
 3847 00b5 CC 0E 00 00 	.4byte 0xecc
 3848 00b9 5F 5F 42 44 	.asciz "__BDT"
 3848      54 00 
 3849 00bf 1D 0F 00 00 	.4byte 0xf1d
 3850 00c3 42 44 54 5F 	.asciz "BDT_ENTRY"
 3850      45 4E 54 52 
 3850      59 00 
 3851 00cd 59 0F 00 00 	.4byte 0xf59
 3852 00d1 5F 50 4F 49 	.asciz "_POINTER"
 3852      4E 54 45 52 
 3852      00 
 3853 00da B3 0F 00 00 	.4byte 0xfb3
MPLAB XC16 ASSEMBLY Listing:   			page 93


 3854 00de 50 4F 49 4E 	.asciz "POINTER"
 3854      54 45 52 00 
 3855 00e6 50 10 00 00 	.4byte 0x1050
 3856 00ea 5F 4C 49 4E 	.asciz "_LINE_CODING"
 3856      45 5F 43 4F 
 3856      44 49 4E 47 
 3856      00 
 3857 00f7 71 10 00 00 	.4byte 0x1071
 3858 00fb 4C 49 4E 45 	.asciz "LINE_CODING"
 3858      5F 43 4F 44 
 3858      49 4E 47 00 
 3859 0107 C7 10 00 00 	.4byte 0x10c7
 3860 010b 5F 43 4F 4E 	.asciz "_CONTROL_SIGNAL_BITMAP"
 3860      54 52 4F 4C 
 3860      5F 53 49 47 
 3860      4E 41 4C 5F 
 3860      42 49 54 4D 
 3860      41 50 00 
 3861 0122 FB 10 00 00 	.4byte 0x10fb
 3862 0126 43 4F 4E 54 	.asciz "CONTROL_SIGNAL_BITMAP"
 3862      52 4F 4C 5F 
 3862      53 49 47 4E 
 3862      41 4C 5F 42 
 3862      49 54 4D 41 
 3862      50 00 
 3863 013c 19 11 00 00 	.4byte 0x1119
 3864 0140 5F 43 44 43 	.asciz "_CDC_NOTICE"
 3864      5F 4E 4F 54 
 3864      49 43 45 00 
 3865 014c 7A 11 00 00 	.4byte 0x117a
 3866 0150 43 44 43 5F 	.asciz "CDC_NOTICE"
 3866      4E 4F 54 49 
 3866      43 45 00 
 3867 015b 00 00 00 00 	.4byte 0x0
 3868                 	.section .debug_aranges,info
 3869 0000 14 00 00 00 	.4byte 0x14
 3870 0004 02 00       	.2byte 0x2
 3871 0006 00 00 00 00 	.4byte .Ldebug_info0
 3872 000a 04          	.byte 0x4
 3873 000b 00          	.byte 0x0
 3874 000c 00 00       	.2byte 0x0
 3875 000e 00 00       	.2byte 0x0
 3876 0010 00 00 00 00 	.4byte 0x0
 3877 0014 00 00 00 00 	.4byte 0x0
 3878                 	.section .debug_str,info
 3879                 	.LASF5:
 3880 0000 63 64 63 5F 	.asciz "cdc_trf_state"
 3880      74 72 66 5F 
 3880      73 74 61 74 
 3880      65 00 
 3881                 	.LASF8:
 3882 000e 6C 69 6E 65 	.asciz "line_coding"
 3882      5F 63 6F 64 
 3882      69 6E 67 00 
 3883                 	.LASF4:
 3884 001a 63 64 63 5F 	.asciz "cdc_rx_len"
 3884      72 78 5F 6C 
MPLAB XC16 ASSEMBLY Listing:   			page 94


 3884      65 6E 00 
 3885                 	.LASF2:
 3886 0025 49 45 43 35 	.asciz "IEC5bits"
 3886      62 69 74 73 
 3886      00 
 3887                 	.LASF3:
 3888 002e 6F 75 74 50 	.asciz "outPipes"
 3888      69 70 65 73 
 3888      00 
 3889                 	.LASF11:
 3890 0037 63 64 63 5F 	.asciz "cdc_data_rx"
 3890      64 61 74 61 
 3890      5F 72 78 00 
 3891                 	.LASF13:
 3892 0043 43 44 43 44 	.asciz "CDCDataInHandle"
 3892      61 74 61 49 
 3892      6E 48 61 6E 
 3892      64 6C 65 00 
 3893                 	.LASF1:
 3894 0053 72 65 73 65 	.asciz "reserved"
 3894      72 76 65 64 
 3894      00 
 3895                 	.LASF10:
 3896 005c 63 64 63 5F 	.asciz "cdc_data_tx"
 3896      64 61 74 61 
 3896      5F 74 78 00 
 3897                 	.LASF7:
 3898 0068 63 64 63 5F 	.asciz "cdc_mem_type"
 3898      6D 65 6D 5F 
 3898      74 79 70 65 
 3898      00 
 3899                 	.LASF12:
 3900 0075 43 44 43 44 	.asciz "CDCDataOutHandle"
 3900      61 74 61 4F 
 3900      75 74 48 61 
 3900      6E 64 6C 65 
 3900      00 
 3901                 	.LASF6:
 3902 0086 63 64 63 5F 	.asciz "cdc_tx_len"
 3902      74 78 5F 6C 
 3902      65 6E 00 
 3903                 	.LASF0:
 3904 0091 62 6D 52 65 	.asciz "bmRequestType"
 3904      71 75 65 73 
 3904      74 54 79 70 
 3904      65 00 
 3905                 	.LASF9:
 3906 009f 53 65 74 75 	.asciz "SetupPkt"
 3906      70 50 6B 74 
 3906      00 
 3907                 	.LASF15:
 3908 00a8 64 75 6D 6D 	.asciz "dummy_encapsulated_cmd_response"
 3908      79 5F 65 6E 
 3908      63 61 70 73 
 3908      75 6C 61 74 
 3908      65 64 5F 63 
 3908      6D 64 5F 72 
MPLAB XC16 ASSEMBLY Listing:   			page 95


 3908      65 73 70 6F 
 3908      6E 73 65 00 
 3909                 	.LASF14:
 3910 00c8 63 6F 6E 74 	.asciz "control_signal_bitmap"
 3910      72 6F 6C 5F 
 3910      73 69 67 6E 
 3910      61 6C 5F 62 
 3910      69 74 6D 61 
 3910      70 00 
 3911                 	.section .text,code
 3912              	
 3913              	
 3914              	
 3915              	.section __c30_info,info,bss
 3916                 	__psv_trap_errata:
 3917                 	
 3918                 	.section __c30_signature,info,data
 3919 0000 01 00       	.word 0x0001
 3920 0002 00 00       	.word 0x0000
 3921 0004 00 00       	.word 0x0000
 3922                 	
 3923                 	
 3924                 	
 3925                 	.set ___PA___,0
 3926                 	.end
MPLAB XC16 ASSEMBLY Listing:   			page 96


DEFINED SYMBOLS
                            *ABS*:00000000 /home/user/Documents/FST/Programming/project_sub-zero/lib/lib_pic33e/usb/usb_device_cdc.c
                            *ABS*:00000001 __33EP256MU806
                            *ABS*:00000001 __dsPIC33E
                            *ABS*:00000001 __HAS_DSP
                            *ABS*:00000001 __HAS_DMAV2
                            *ABS*:00000001 __HAS_CODEGUARD
                            *ABS*:00000001 __HAS_PMP_ENHANCED
                            *ABS*:00000001 __HAS_EDS
                            *ABS*:00000001 __HAS_EP
                            *ABS*:00000011 __TARGET_DIVIDE_CYCLES
                            *ABS*:00000001 __C30ELF
    {standard input}:13     .bss:00000000 _cdc_data_tx
    {standard input}:16     .bss:00000040 _cdc_data_rx
    {standard input}:20     .bss:00000080 _line_coding
    {standard input}:23     .bss:00000088 _cdc_notice
    {standard input}:27     .nbss:00000000 _cdc_rx_len
    {standard input}:30     .nbss:00000001 _cdc_trf_state
    {standard input}:34     .nbss:00000002 _pCDCSrc
    {standard input}:38     .nbss:00000004 _pCDCDst
    {standard input}:41     .nbss:00000006 _cdc_tx_len
    {standard input}:44     .nbss:00000007 _cdc_mem_type
    {standard input}:48     .nbss:00000008 _CDCDataOutHandle
    {standard input}:52     .nbss:0000000a _CDCDataInHandle
    {standard input}:56     .nbss:0000000c _control_signal_bitmap
    {standard input}:60     .nbss:0000000e _BaudRateGen
    {standard input}:64     .bss:00000092 _dummy_encapsulated_cmd_response
    {standard input}:69     .text:00000000 _USBCheckCDCRequest
    {standard input}:73     *ABS*:00000000 ___PA___
    {standard input}:85     *ABS*:00000000 ___BP___
    {standard input}:286    .text:00000110 _CDCInitEP
    {standard input}:350    .text:00000152 _USBCDCEventHandler
    {standard input}:409    .text:0000018e _getsUSBUSART
    {standard input}:506    .text:00000212 _putUSBUSART
    {standard input}:548    .text:00000240 _putsUSBUSART
    {standard input}:625    .text:00000298 _CDCTxService
    {standard input}:3916   __c30_info:00000000 __psv_trap_errata
    {standard input}:74     .text:00000000 .L0
                            .text:000000fc .L13
                            .text:000000fe .L14
                            .text:0000002c .L5
                            .text:00000100 .L15
                            .text:00000078 .L9
                            .text:00000048 .L12
                            .text:00000056 .L7
                            .text:0000006c .L8
                            .text:00000100 .L1
                            .text:000000d4 .L10
                            .text:000000e4 .L11
                            .text:00000184 .L23
                            .text:00000174 .L20
                            .text:00000180 .L21
                            .text:00000186 .L22
                            .text:000001aa .L25
                            .text:00000208 .L26
                            .text:000001d4 .L27
                            .text:000001f0 .L28
MPLAB XC16 ASSEMBLY Listing:   			page 97


                            .text:000001d8 .L29
                            .text:00000236 .L31
                            .text:00000250 .L33
                            .text:00000290 .L32
                            .text:00000278 .L38
                            .text:00000258 .L37
                            .text:00000278 .L36
                            .text:000002b4 .L40
                            .text:00000388 .L39
                            .text:000002bc .L42
                            .text:000002c6 .L43
                            .text:000002e2 .L44
                            .text:00000386 .L45
                            .text:000002fa .L46
                            .text:00000302 .L47
                            .text:00000358 .L52
                            .text:00000334 .L49
                            .text:0000031a .L50
                            .text:00000360 .L51
                            .text:0000033e .L53
                            .text:00000378 .L54
                            .text:00000374 .L55
                    .debug_abbrev:00000000 .Ldebug_abbrev0
                            .text:00000000 .Ltext0
                            .text:00000390 .Letext0
                      .debug_line:00000000 .Ldebug_line0
                       .debug_str:00000091 .LASF0
                       .debug_str:00000053 .LASF1
                            .text:00000000 .LFB0
                            .text:00000110 .LFE0
                            .text:00000110 .LFB1
                            .text:00000152 .LFE1
                            .text:00000152 .LFB2
                            .text:0000018e .LFE2
                            .text:0000018e .LFB3
                            .text:00000212 .LFE3
                            .text:00000212 .LFB4
                            .text:00000240 .LFE4
                            .text:00000240 .LFB5
                            .text:00000298 .LFE5
                            .text:00000298 .LFB6
                            .text:00000390 .LFE6
                       .debug_str:00000025 .LASF2
                       .debug_str:0000002e .LASF3
                       .debug_str:0000001a .LASF4
                       .debug_str:00000000 .LASF5
                       .debug_str:00000086 .LASF6
                       .debug_str:00000068 .LASF7
                       .debug_str:0000000e .LASF8
                       .debug_str:0000009f .LASF9
                       .debug_str:0000005c .LASF10
                       .debug_str:00000037 .LASF11
                       .debug_str:00000075 .LASF12
                       .debug_str:00000043 .LASF13
                       .debug_str:000000c8 .LASF14
                       .debug_str:000000a8 .LASF15
                     .debug_frame:00000000 .Lframe0
MPLAB XC16 ASSEMBLY Listing:   			page 98


                      .debug_info:00000000 .Ldebug_info0

UNDEFINED SYMBOLS
_SetupPkt
_inPipes
_outPipes
CORCON
_USBEnableEndpoint
_USBTransferOnePacket
_IEC5bits

EQUATE SYMBOLS
/home/user/Documents/FST/Programming/project_sub-zero/lib/lib_pic33e/usb/usb_device_cdc.c = 0x0
                  __33EP256MU806 = 0x1
                      __dsPIC33E = 0x1
                       __HAS_DSP = 0x1
                     __HAS_DMAV2 = 0x1
                 __HAS_CODEGUARD = 0x1
              __HAS_PMP_ENHANCED = 0x1
                       __HAS_EDS = 0x1
                        __HAS_EP = 0x1
          __TARGET_DIVIDE_CYCLES = 0x11
                        __C30ELF = 0x1
                        ___PA___ = 0x0
                        ___BP___ = 0x0
