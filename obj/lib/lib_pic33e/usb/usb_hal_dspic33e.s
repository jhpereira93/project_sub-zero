MPLAB XC16 ASSEMBLY Listing:   			page 1


   1              	.file "/home/user/Documents/FST/Programming/project_sub-zero/lib/lib_pic33e/usb/usb_hal_dspic33e.c"
   2              	.section .debug_abbrev,info
   3                 	.Ldebug_abbrev0:
   4                 	.section .debug_info,info
   5                 	.Ldebug_info0:
   6                 	.section .debug_line,info
   7                 	.Ldebug_line0:
   8 0000 FC 00 00 00 	.section .text,code
   8      02 00 CA 00 
   8      00 00 01 01 
   8      FB 0E 0A 00 
   8      01 01 01 01 
   8      00 00 00 01 
   8      6C 69 62 2F 
   8      6C 69 62 5F 
   8      70 69 63 33 
   9              	.Ltext0:
  10              	.section .text,code
  11              	.align 2
  12              	.global _USBSleepOnSuspend
  13              	.type _USBSleepOnSuspend,@function
  14              	_USBSleepOnSuspend:
  15              	.LFB0:
  16              	.file 1 "lib/lib_pic33e/usb/usb_hal_dspic33e.c"
   1:lib/lib_pic33e/usb/usb_hal_dspic33e.c **** #ifndef NOUSB
   2:lib/lib_pic33e/usb/usb_hal_dspic33e.c **** // DOM-IGNORE-BEGIN
   3:lib/lib_pic33e/usb/usb_hal_dspic33e.c **** /*******************************************************************************
   4:lib/lib_pic33e/usb/usb_hal_dspic33e.c **** Copyright 2015 Microchip Technology Inc. (www.microchip.com)
   5:lib/lib_pic33e/usb/usb_hal_dspic33e.c **** 
   6:lib/lib_pic33e/usb/usb_hal_dspic33e.c **** Licensed under the Apache License, Version 2.0 (the "License");
   7:lib/lib_pic33e/usb/usb_hal_dspic33e.c **** you may not use this file except in compliance with the License.
   8:lib/lib_pic33e/usb/usb_hal_dspic33e.c **** You may obtain a copy of the License at
   9:lib/lib_pic33e/usb/usb_hal_dspic33e.c **** 
  10:lib/lib_pic33e/usb/usb_hal_dspic33e.c ****     http://www.apache.org/licenses/LICENSE-2.0
  11:lib/lib_pic33e/usb/usb_hal_dspic33e.c **** 
  12:lib/lib_pic33e/usb/usb_hal_dspic33e.c **** Unless required by applicable law or agreed to in writing, software
  13:lib/lib_pic33e/usb/usb_hal_dspic33e.c **** distributed under the License is distributed on an "AS IS" BASIS,
  14:lib/lib_pic33e/usb/usb_hal_dspic33e.c **** WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  15:lib/lib_pic33e/usb/usb_hal_dspic33e.c **** See the License for the specific language governing permissions and
  16:lib/lib_pic33e/usb/usb_hal_dspic33e.c **** limitations under the License.
  17:lib/lib_pic33e/usb/usb_hal_dspic33e.c **** 
  18:lib/lib_pic33e/usb/usb_hal_dspic33e.c **** To request to license the code under the MLA license (www.microchip.com/mla_license), 
  19:lib/lib_pic33e/usb/usb_hal_dspic33e.c **** please contact mla_licensing@microchip.com
  20:lib/lib_pic33e/usb/usb_hal_dspic33e.c **** *******************************************************************************/
  21:lib/lib_pic33e/usb/usb_hal_dspic33e.c **** //DOM-IGNORE-END
  22:lib/lib_pic33e/usb/usb_hal_dspic33e.c **** 
  23:lib/lib_pic33e/usb/usb_hal_dspic33e.c **** 
  24:lib/lib_pic33e/usb/usb_hal_dspic33e.c **** #ifndef USB_HAL_DSPIC33E_C
  25:lib/lib_pic33e/usb/usb_hal_dspic33e.c **** #define USB_HAL_DSPIC33E_C
  26:lib/lib_pic33e/usb/usb_hal_dspic33e.c **** 
  27:lib/lib_pic33e/usb/usb_hal_dspic33e.c **** #include "usb.h"
  28:lib/lib_pic33e/usb/usb_hal_dspic33e.c **** 
  29:lib/lib_pic33e/usb/usb_hal_dspic33e.c **** 
  30:lib/lib_pic33e/usb/usb_hal_dspic33e.c **** /********************************************************************
  31:lib/lib_pic33e/usb/usb_hal_dspic33e.c **** Function:
  32:lib/lib_pic33e/usb/usb_hal_dspic33e.c ****     bool USBSleepOnSuspend(void)
  33:lib/lib_pic33e/usb/usb_hal_dspic33e.c ****     
MPLAB XC16 ASSEMBLY Listing:   			page 2


  34:lib/lib_pic33e/usb/usb_hal_dspic33e.c **** Summary:
  35:lib/lib_pic33e/usb/usb_hal_dspic33e.c ****     Places the core into sleep and sets up the USB module
  36:lib/lib_pic33e/usb/usb_hal_dspic33e.c ****     to wake up the device on USB activity.
  37:lib/lib_pic33e/usb/usb_hal_dspic33e.c ****     
  38:lib/lib_pic33e/usb/usb_hal_dspic33e.c **** PreCondition:
  39:lib/lib_pic33e/usb/usb_hal_dspic33e.c ****     IPL (in the SR register) must be non-zero.
  40:lib/lib_pic33e/usb/usb_hal_dspic33e.c ****     
  41:lib/lib_pic33e/usb/usb_hal_dspic33e.c **** Parameters:
  42:lib/lib_pic33e/usb/usb_hal_dspic33e.c ****     None
  43:lib/lib_pic33e/usb/usb_hal_dspic33e.c ****     
  44:lib/lib_pic33e/usb/usb_hal_dspic33e.c **** Return Values:
  45:lib/lib_pic33e/usb/usb_hal_dspic33e.c ****     true  - if entered sleep successfully
  46:lib/lib_pic33e/usb/usb_hal_dspic33e.c ****     false - if there was an error entering sleep
  47:lib/lib_pic33e/usb/usb_hal_dspic33e.c ****     
  48:lib/lib_pic33e/usb/usb_hal_dspic33e.c **** Remarks:
  49:lib/lib_pic33e/usb/usb_hal_dspic33e.c ****     Please note that before calling this function that it is the
  50:lib/lib_pic33e/usb/usb_hal_dspic33e.c ****     responsibility of the application to place all of the other
  51:lib/lib_pic33e/usb/usb_hal_dspic33e.c ****     peripherals or board features into a lower power state if
  52:lib/lib_pic33e/usb/usb_hal_dspic33e.c ****     required.
  53:lib/lib_pic33e/usb/usb_hal_dspic33e.c **** 
  54:lib/lib_pic33e/usb/usb_hal_dspic33e.c **** *******************************************************************/
  55:lib/lib_pic33e/usb/usb_hal_dspic33e.c **** bool USBSleepOnSuspend(void)
  56:lib/lib_pic33e/usb/usb_hal_dspic33e.c **** {
  17              	.loc 1 56 0
  18              	.set ___PA___,1
  19 000000  08 00 FA 	lnk #8
  20              	.LCFI0:
  57:lib/lib_pic33e/usb/usb_hal_dspic33e.c ****     unsigned int U1EIE_save, U1IE_save, U1OTGIE_save;
  58:lib/lib_pic33e/usb/usb_hal_dspic33e.c ****     unsigned char USB1IE_save;
  59:lib/lib_pic33e/usb/usb_hal_dspic33e.c **** 
  60:lib/lib_pic33e/usb/usb_hal_dspic33e.c ****     #if defined(USB_POLLING)
  61:lib/lib_pic33e/usb/usb_hal_dspic33e.c ****         //If IPL is equal to 0 then there is no way for the USB module to
  62:lib/lib_pic33e/usb/usb_hal_dspic33e.c ****         //  generate an interrupt to wake up the device.  
  63:lib/lib_pic33e/usb/usb_hal_dspic33e.c ****         if(_IPL == 0)
  64:lib/lib_pic33e/usb/usb_hal_dspic33e.c ****         {
  65:lib/lib_pic33e/usb/usb_hal_dspic33e.c ****             return false;
  66:lib/lib_pic33e/usb/usb_hal_dspic33e.c ****         }
  67:lib/lib_pic33e/usb/usb_hal_dspic33e.c **** 
  68:lib/lib_pic33e/usb/usb_hal_dspic33e.c ****         //Set the interrupt priority to a level that will wake up the part (>0)
  69:lib/lib_pic33e/usb/usb_hal_dspic33e.c ****         //  but will not cause a interrupt vector jump (USB1IP<=IPL)
  70:lib/lib_pic33e/usb/usb_hal_dspic33e.c ****         _USB1IP = 1;
  71:lib/lib_pic33e/usb/usb_hal_dspic33e.c ****     #endif 
  72:lib/lib_pic33e/usb/usb_hal_dspic33e.c **** 
  73:lib/lib_pic33e/usb/usb_hal_dspic33e.c ****     //Save the old interrupt and CPU settings
  74:lib/lib_pic33e/usb/usb_hal_dspic33e.c ****     U1EIE_save = U1EIE;
  21              	.loc 1 74 0
  22 000002  01 00 80 	mov _U1EIE,w1
  23 000004  01 0F 78 	mov w1,[w14]
  75:lib/lib_pic33e/usb/usb_hal_dspic33e.c ****     U1IE_save = U1IE;
  76:lib/lib_pic33e/usb/usb_hal_dspic33e.c ****     U1OTGIE_save = U1OTGIE;
  77:lib/lib_pic33e/usb/usb_hal_dspic33e.c ****     USB1IE_save = IEC5bits.USB1IE;
  78:lib/lib_pic33e/usb/usb_hal_dspic33e.c **** 
  79:lib/lib_pic33e/usb/usb_hal_dspic33e.c ****     //Disable all USB interrupts
  80:lib/lib_pic33e/usb/usb_hal_dspic33e.c ****     U1EIE = 0;
  81:lib/lib_pic33e/usb/usb_hal_dspic33e.c ****     U1IE = 0;
  82:lib/lib_pic33e/usb/usb_hal_dspic33e.c ****     U1OTGIE = 0; 
  83:lib/lib_pic33e/usb/usb_hal_dspic33e.c **** 
MPLAB XC16 ASSEMBLY Listing:   			page 3


  84:lib/lib_pic33e/usb/usb_hal_dspic33e.c ****     //Enable the interrupt
  85:lib/lib_pic33e/usb/usb_hal_dspic33e.c ****     IFS5bits.USB1IF = 0;
  86:lib/lib_pic33e/usb/usb_hal_dspic33e.c ****     U1OTGIEbits.ACTVIE = 1;
  87:lib/lib_pic33e/usb/usb_hal_dspic33e.c ****     USBClearInterruptFlag(USBActivityIFReg,USBActivityIFBitNum);
  24              	.loc 1 87 0
  25 000006  00 01 20 	mov #16,w0
  26              	.loc 1 75 0
  27 000008  01 00 80 	mov _U1IE,w1
  28 00000a  11 07 98 	mov w1,[w14+2]
  29              	.loc 1 76 0
  30 00000c  01 00 80 	mov _U1OTGIE,w1
  31 00000e  21 07 98 	mov w1,[w14+4]
  32              	.loc 1 77 0
  33 000010  01 00 80 	mov _IEC5bits,w1
  34              	.loc 1 80 0
  35 000012  00 20 EF 	clr _U1EIE
  36              	.loc 1 77 0
  37 000014  C6 08 DE 	lsr w1,#6,w1
  38              	.loc 1 81 0
  39 000016  00 20 EF 	clr _U1IE
  40              	.loc 1 77 0
  41 000018  E1 C0 60 	and.b w1,#1,w1
  42              	.loc 1 82 0
  43 00001a  00 20 EF 	clr _U1OTGIE
  44              	.loc 1 77 0
  45 00001c  61 47 98 	mov.b w1,[w14+6]
  46              	.loc 1 85 0
  47 00001e  00 C0 A9 	bclr.b _IFS5bits,#6
  48              	.loc 1 86 0
  49 000020  00 80 A8 	bset.b _U1OTGIEbits,#4
  50              	.loc 1 87 0
  51 000022  00 00 88 	mov w0,_U1OTGIR
  88:lib/lib_pic33e/usb/usb_hal_dspic33e.c ****     IEC5bits.USB1IE = 1;
  52              	.loc 1 88 0
  53 000024  00 C0 A8 	bset.b _IEC5bits,#6
  89:lib/lib_pic33e/usb/usb_hal_dspic33e.c **** 
  90:lib/lib_pic33e/usb/usb_hal_dspic33e.c ****     Sleep();
  54              	.loc 1 90 0
  55 000026  00 40 FE 	pwrsav #0
  91:lib/lib_pic33e/usb/usb_hal_dspic33e.c **** 
  92:lib/lib_pic33e/usb/usb_hal_dspic33e.c ****     #if defined(USB_POLLING)
  93:lib/lib_pic33e/usb/usb_hal_dspic33e.c ****         //Disable the interrupt
  94:lib/lib_pic33e/usb/usb_hal_dspic33e.c ****         _USB1IP = 0;
  95:lib/lib_pic33e/usb/usb_hal_dspic33e.c ****     #endif  
  96:lib/lib_pic33e/usb/usb_hal_dspic33e.c **** 
  97:lib/lib_pic33e/usb/usb_hal_dspic33e.c ****     //restore the previous interrupt settings
  98:lib/lib_pic33e/usb/usb_hal_dspic33e.c ****     IEC5bits.USB1IE = USB1IE_save;
  56              	.loc 1 98 0
  57 000028  01 00 80 	mov _IEC5bits,w1
  58 00002a  6E 40 90 	mov.b [w14+6],w0
  59 00002c  01 01 78 	mov w1,w2
  60 00002e  02 60 A1 	bclr w2,#6
  61 000030  E1 40 60 	and.b w0,#1,w1
  99:lib/lib_pic33e/usb/usb_hal_dspic33e.c ****     U1EIE = U1EIE_save;
 100:lib/lib_pic33e/usb/usb_hal_dspic33e.c ****     U1IE = U1IE_save;
 101:lib/lib_pic33e/usb/usb_hal_dspic33e.c ****     U1OTGIE = U1OTGIE_save;
 102:lib/lib_pic33e/usb/usb_hal_dspic33e.c **** 
MPLAB XC16 ASSEMBLY Listing:   			page 4


 103:lib/lib_pic33e/usb/usb_hal_dspic33e.c ****     return true;
  62              	.loc 1 103 0
  63 000032  10 C0 B3 	mov.b #1,w0
  64              	.loc 1 98 0
  65 000034  81 80 FB 	ze w1,w1
  66 000036  E1 80 60 	and w1,#1,w1
  67 000038  C6 08 DD 	sl w1,#6,w1
  68 00003a  82 80 70 	ior w1,w2,w1
  69 00003c  01 00 88 	mov w1,_IEC5bits
  70              	.loc 1 99 0
  71 00003e  9E 00 78 	mov [w14],w1
  72 000040  01 00 88 	mov w1,_U1EIE
  73              	.loc 1 100 0
  74 000042  9E 00 90 	mov [w14+2],w1
  75 000044  01 00 88 	mov w1,_U1IE
  76              	.loc 1 101 0
  77 000046  AE 00 90 	mov [w14+4],w1
  78 000048  01 00 88 	mov w1,_U1OTGIE
 104:lib/lib_pic33e/usb/usb_hal_dspic33e.c **** }
  79              	.loc 1 104 0
  80 00004a  8E 07 78 	mov w14,w15
  81 00004c  4F 07 78 	mov [--w15],w14
  82 00004e  00 40 A9 	bclr CORCON,#2
  83 000050  00 00 06 	return 
  84              	.set ___PA___,0
  85              	.LFE0:
  86              	.size _USBSleepOnSuspend,.-_USBSleepOnSuspend
  87              	.section .debug_frame,info
  88                 	.Lframe0:
  89 0000 10 00 00 00 	.4byte .LECIE0-.LSCIE0
  90                 	.LSCIE0:
  91 0004 FF FF FF FF 	.4byte 0xffffffff
  92 0008 01          	.byte 0x1
  93 0009 00          	.byte 0
  94 000a 01          	.uleb128 0x1
  95 000b 02          	.sleb128 2
  96 000c 25          	.byte 0x25
  97 000d 12          	.byte 0x12
  98 000e 0F          	.uleb128 0xf
  99 000f 7E          	.sleb128 -2
 100 0010 09          	.byte 0x9
 101 0011 25          	.uleb128 0x25
 102 0012 0F          	.uleb128 0xf
 103 0013 00          	.align 4
 104                 	.LECIE0:
 105                 	.LSFDE0:
 106 0014 18 00 00 00 	.4byte .LEFDE0-.LASFDE0
 107                 	.LASFDE0:
 108 0018 00 00 00 00 	.4byte .Lframe0
 109 001c 00 00 00 00 	.4byte .LFB0
 110 0020 52 00 00 00 	.4byte .LFE0-.LFB0
 111 0024 04          	.byte 0x4
 112 0025 02 00 00 00 	.4byte .LCFI0-.LFB0
 113 0029 13          	.byte 0x13
 114 002a 7D          	.sleb128 -3
 115 002b 0D          	.byte 0xd
 116 002c 0E          	.uleb128 0xe
MPLAB XC16 ASSEMBLY Listing:   			page 5


 117 002d 8E          	.byte 0x8e
 118 002e 02          	.uleb128 0x2
 119 002f 00          	.align 4
 120                 	.LEFDE0:
 121                 	.section .text,code
 122              	.Letext0:
 123              	.file 2 "/opt/microchip/xc16/v1.36/bin/bin/../../support/dsPIC33E/h/p33EP256MU806.h"
 124              	.file 3 "/opt/microchip/xc16/v1.36/bin/bin/../../include/lega-c/stdint.h"
 125              	.section .debug_info,info
 126 0000 03 06 00 00 	.4byte 0x603
 127 0004 02 00       	.2byte 0x2
 128 0006 00 00 00 00 	.4byte .Ldebug_abbrev0
 129 000a 04          	.byte 0x4
 130 000b 01          	.uleb128 0x1
 131 000c 47 4E 55 20 	.asciz "GNU C 4.5.1 (XC16, Microchip v1.36) (A) Build date: Dec  5 2018"
 131      43 20 34 2E 
 131      35 2E 31 20 
 131      28 58 43 31 
 131      36 2C 20 4D 
 131      69 63 72 6F 
 131      63 68 69 70 
 131      20 76 31 2E 
 131      33 36 29 20 
 132 004c 01          	.byte 0x1
 133 004d 6C 69 62 2F 	.asciz "lib/lib_pic33e/usb/usb_hal_dspic33e.c"
 133      6C 69 62 5F 
 133      70 69 63 33 
 133      33 65 2F 75 
 133      73 62 2F 75 
 133      73 62 5F 68 
 133      61 6C 5F 64 
 133      73 70 69 63 
 133      33 33 65 2E 
 134 0073 2F 68 6F 6D 	.asciz "/home/user/Documents/FST/Programming/project_sub-zero"
 134      65 2F 75 73 
 134      65 72 2F 44 
 134      6F 63 75 6D 
 134      65 6E 74 73 
 134      2F 46 53 54 
 134      2F 50 72 6F 
 134      67 72 61 6D 
 134      6D 69 6E 67 
 135 00a9 00 00 00 00 	.4byte .Ltext0
 136 00ad 00 00 00 00 	.4byte .Letext0
 137 00b1 00 00 00 00 	.4byte .Ldebug_line0
 138 00b5 02          	.uleb128 0x2
 139 00b6 01          	.byte 0x1
 140 00b7 06          	.byte 0x6
 141 00b8 73 69 67 6E 	.asciz "signed char"
 141      65 64 20 63 
 141      68 61 72 00 
 142 00c4 02          	.uleb128 0x2
 143 00c5 02          	.byte 0x2
 144 00c6 05          	.byte 0x5
 145 00c7 69 6E 74 00 	.asciz "int"
 146 00cb 02          	.uleb128 0x2
 147 00cc 04          	.byte 0x4
MPLAB XC16 ASSEMBLY Listing:   			page 6


 148 00cd 05          	.byte 0x5
 149 00ce 6C 6F 6E 67 	.asciz "long int"
 149      20 69 6E 74 
 149      00 
 150 00d7 02          	.uleb128 0x2
 151 00d8 08          	.byte 0x8
 152 00d9 05          	.byte 0x5
 153 00da 6C 6F 6E 67 	.asciz "long long int"
 153      20 6C 6F 6E 
 153      67 20 69 6E 
 153      74 00 
 154 00e8 02          	.uleb128 0x2
 155 00e9 01          	.byte 0x1
 156 00ea 08          	.byte 0x8
 157 00eb 75 6E 73 69 	.asciz "unsigned char"
 157      67 6E 65 64 
 157      20 63 68 61 
 157      72 00 
 158 00f9 03          	.uleb128 0x3
 159 00fa 75 69 6E 74 	.asciz "uint16_t"
 159      31 36 5F 74 
 159      00 
 160 0103 03          	.byte 0x3
 161 0104 31          	.byte 0x31
 162 0105 09 01 00 00 	.4byte 0x109
 163 0109 02          	.uleb128 0x2
 164 010a 02          	.byte 0x2
 165 010b 07          	.byte 0x7
 166 010c 75 6E 73 69 	.asciz "unsigned int"
 166      67 6E 65 64 
 166      20 69 6E 74 
 166      00 
 167 0119 02          	.uleb128 0x2
 168 011a 04          	.byte 0x4
 169 011b 07          	.byte 0x7
 170 011c 6C 6F 6E 67 	.asciz "long unsigned int"
 170      20 75 6E 73 
 170      69 67 6E 65 
 170      64 20 69 6E 
 170      74 00 
 171 012e 02          	.uleb128 0x2
 172 012f 08          	.byte 0x8
 173 0130 07          	.byte 0x7
 174 0131 6C 6F 6E 67 	.asciz "long long unsigned int"
 174      20 6C 6F 6E 
 174      67 20 75 6E 
 174      73 69 67 6E 
 174      65 64 20 69 
 174      6E 74 00 
 175 0148 02          	.uleb128 0x2
 176 0149 01          	.byte 0x1
 177 014a 02          	.byte 0x2
 178 014b 5F 42 6F 6F 	.asciz "_Bool"
 178      6C 00 
 179 0151 04          	.uleb128 0x4
 180 0152 74 61 67 55 	.asciz "tagU1OTGIEBITS"
 180      31 4F 54 47 
MPLAB XC16 ASSEMBLY Listing:   			page 7


 180      49 45 42 49 
 180      54 53 00 
 181 0161 02          	.byte 0x2
 182 0162 02          	.byte 0x2
 183 0163 61 12       	.2byte 0x1261
 184 0165 04 02 00 00 	.4byte 0x204
 185 0169 05          	.uleb128 0x5
 186 016a 56 42 55 53 	.asciz "VBUSVDIE"
 186      56 44 49 45 
 186      00 
 187 0173 02          	.byte 0x2
 188 0174 62 12       	.2byte 0x1262
 189 0176 F9 00 00 00 	.4byte 0xf9
 190 017a 02          	.byte 0x2
 191 017b 01          	.byte 0x1
 192 017c 0F          	.byte 0xf
 193 017d 02          	.byte 0x2
 194 017e 23          	.byte 0x23
 195 017f 00          	.uleb128 0x0
 196 0180 05          	.uleb128 0x5
 197 0181 53 45 53 45 	.asciz "SESENDIE"
 197      4E 44 49 45 
 197      00 
 198 018a 02          	.byte 0x2
 199 018b 64 12       	.2byte 0x1264
 200 018d F9 00 00 00 	.4byte 0xf9
 201 0191 02          	.byte 0x2
 202 0192 01          	.byte 0x1
 203 0193 0D          	.byte 0xd
 204 0194 02          	.byte 0x2
 205 0195 23          	.byte 0x23
 206 0196 00          	.uleb128 0x0
 207 0197 05          	.uleb128 0x5
 208 0198 53 45 53 56 	.asciz "SESVDIE"
 208      44 49 45 00 
 209 01a0 02          	.byte 0x2
 210 01a1 65 12       	.2byte 0x1265
 211 01a3 F9 00 00 00 	.4byte 0xf9
 212 01a7 02          	.byte 0x2
 213 01a8 01          	.byte 0x1
 214 01a9 0C          	.byte 0xc
 215 01aa 02          	.byte 0x2
 216 01ab 23          	.byte 0x23
 217 01ac 00          	.uleb128 0x0
 218 01ad 05          	.uleb128 0x5
 219 01ae 41 43 54 56 	.asciz "ACTVIE"
 219      49 45 00 
 220 01b5 02          	.byte 0x2
 221 01b6 66 12       	.2byte 0x1266
 222 01b8 F9 00 00 00 	.4byte 0xf9
 223 01bc 02          	.byte 0x2
 224 01bd 01          	.byte 0x1
 225 01be 0B          	.byte 0xb
 226 01bf 02          	.byte 0x2
 227 01c0 23          	.byte 0x23
 228 01c1 00          	.uleb128 0x0
 229 01c2 05          	.uleb128 0x5
MPLAB XC16 ASSEMBLY Listing:   			page 8


 230 01c3 4C 53 54 41 	.asciz "LSTATEIE"
 230      54 45 49 45 
 230      00 
 231 01cc 02          	.byte 0x2
 232 01cd 67 12       	.2byte 0x1267
 233 01cf F9 00 00 00 	.4byte 0xf9
 234 01d3 02          	.byte 0x2
 235 01d4 01          	.byte 0x1
 236 01d5 0A          	.byte 0xa
 237 01d6 02          	.byte 0x2
 238 01d7 23          	.byte 0x23
 239 01d8 00          	.uleb128 0x0
 240 01d9 05          	.uleb128 0x5
 241 01da 54 31 4D 53 	.asciz "T1MSECIE"
 241      45 43 49 45 
 241      00 
 242 01e3 02          	.byte 0x2
 243 01e4 68 12       	.2byte 0x1268
 244 01e6 F9 00 00 00 	.4byte 0xf9
 245 01ea 02          	.byte 0x2
 246 01eb 01          	.byte 0x1
 247 01ec 09          	.byte 0x9
 248 01ed 02          	.byte 0x2
 249 01ee 23          	.byte 0x23
 250 01ef 00          	.uleb128 0x0
 251 01f0 05          	.uleb128 0x5
 252 01f1 49 44 49 45 	.asciz "IDIE"
 252      00 
 253 01f6 02          	.byte 0x2
 254 01f7 69 12       	.2byte 0x1269
 255 01f9 F9 00 00 00 	.4byte 0xf9
 256 01fd 02          	.byte 0x2
 257 01fe 01          	.byte 0x1
 258 01ff 08          	.byte 0x8
 259 0200 02          	.byte 0x2
 260 0201 23          	.byte 0x23
 261 0202 00          	.uleb128 0x0
 262 0203 00          	.byte 0x0
 263 0204 06          	.uleb128 0x6
 264 0205 55 31 4F 54 	.asciz "U1OTGIEBITS"
 264      47 49 45 42 
 264      49 54 53 00 
 265 0211 02          	.byte 0x2
 266 0212 6A 12       	.2byte 0x126a
 267 0214 51 01 00 00 	.4byte 0x151
 268 0218 04          	.uleb128 0x4
 269 0219 74 61 67 49 	.asciz "tagIFS5BITS"
 269      46 53 35 42 
 269      49 54 53 00 
 270 0225 02          	.byte 0x2
 271 0226 02          	.byte 0x2
 272 0227 71 25       	.2byte 0x2571
 273 0229 3C 03 00 00 	.4byte 0x33c
 274 022d 05          	.uleb128 0x5
 275 022e 55 33 45 49 	.asciz "U3EIF"
 275      46 00 
 276 0234 02          	.byte 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 9


 277 0235 73 25       	.2byte 0x2573
 278 0237 F9 00 00 00 	.4byte 0xf9
 279 023b 02          	.byte 0x2
 280 023c 01          	.byte 0x1
 281 023d 0E          	.byte 0xe
 282 023e 02          	.byte 0x2
 283 023f 23          	.byte 0x23
 284 0240 00          	.uleb128 0x0
 285 0241 05          	.uleb128 0x5
 286 0242 55 33 52 58 	.asciz "U3RXIF"
 286      49 46 00 
 287 0249 02          	.byte 0x2
 288 024a 74 25       	.2byte 0x2574
 289 024c F9 00 00 00 	.4byte 0xf9
 290 0250 02          	.byte 0x2
 291 0251 01          	.byte 0x1
 292 0252 0D          	.byte 0xd
 293 0253 02          	.byte 0x2
 294 0254 23          	.byte 0x23
 295 0255 00          	.uleb128 0x0
 296 0256 05          	.uleb128 0x5
 297 0257 55 33 54 58 	.asciz "U3TXIF"
 297      49 46 00 
 298 025e 02          	.byte 0x2
 299 025f 75 25       	.2byte 0x2575
 300 0261 F9 00 00 00 	.4byte 0xf9
 301 0265 02          	.byte 0x2
 302 0266 01          	.byte 0x1
 303 0267 0C          	.byte 0xc
 304 0268 02          	.byte 0x2
 305 0269 23          	.byte 0x23
 306 026a 00          	.uleb128 0x0
 307 026b 05          	.uleb128 0x5
 308 026c 55 53 42 31 	.asciz "USB1IF"
 308      49 46 00 
 309 0273 02          	.byte 0x2
 310 0274 77 25       	.2byte 0x2577
 311 0276 F9 00 00 00 	.4byte 0xf9
 312 027a 02          	.byte 0x2
 313 027b 01          	.byte 0x1
 314 027c 09          	.byte 0x9
 315 027d 02          	.byte 0x2
 316 027e 23          	.byte 0x23
 317 027f 00          	.uleb128 0x0
 318 0280 05          	.uleb128 0x5
 319 0281 55 34 45 49 	.asciz "U4EIF"
 319      46 00 
 320 0287 02          	.byte 0x2
 321 0288 78 25       	.2byte 0x2578
 322 028a F9 00 00 00 	.4byte 0xf9
 323 028e 02          	.byte 0x2
 324 028f 01          	.byte 0x1
 325 0290 08          	.byte 0x8
 326 0291 02          	.byte 0x2
 327 0292 23          	.byte 0x23
 328 0293 00          	.uleb128 0x0
 329 0294 05          	.uleb128 0x5
MPLAB XC16 ASSEMBLY Listing:   			page 10


 330 0295 55 34 52 58 	.asciz "U4RXIF"
 330      49 46 00 
 331 029c 02          	.byte 0x2
 332 029d 79 25       	.2byte 0x2579
 333 029f F9 00 00 00 	.4byte 0xf9
 334 02a3 02          	.byte 0x2
 335 02a4 01          	.byte 0x1
 336 02a5 07          	.byte 0x7
 337 02a6 02          	.byte 0x2
 338 02a7 23          	.byte 0x23
 339 02a8 00          	.uleb128 0x0
 340 02a9 05          	.uleb128 0x5
 341 02aa 55 34 54 58 	.asciz "U4TXIF"
 341      49 46 00 
 342 02b1 02          	.byte 0x2
 343 02b2 7A 25       	.2byte 0x257a
 344 02b4 F9 00 00 00 	.4byte 0xf9
 345 02b8 02          	.byte 0x2
 346 02b9 01          	.byte 0x1
 347 02ba 06          	.byte 0x6
 348 02bb 02          	.byte 0x2
 349 02bc 23          	.byte 0x23
 350 02bd 00          	.uleb128 0x0
 351 02be 05          	.uleb128 0x5
 352 02bf 53 50 49 33 	.asciz "SPI3EIF"
 352      45 49 46 00 
 353 02c7 02          	.byte 0x2
 354 02c8 7B 25       	.2byte 0x257b
 355 02ca F9 00 00 00 	.4byte 0xf9
 356 02ce 02          	.byte 0x2
 357 02cf 01          	.byte 0x1
 358 02d0 05          	.byte 0x5
 359 02d1 02          	.byte 0x2
 360 02d2 23          	.byte 0x23
 361 02d3 00          	.uleb128 0x0
 362 02d4 05          	.uleb128 0x5
 363 02d5 53 50 49 33 	.asciz "SPI3IF"
 363      49 46 00 
 364 02dc 02          	.byte 0x2
 365 02dd 7C 25       	.2byte 0x257c
 366 02df F9 00 00 00 	.4byte 0xf9
 367 02e3 02          	.byte 0x2
 368 02e4 01          	.byte 0x1
 369 02e5 04          	.byte 0x4
 370 02e6 02          	.byte 0x2
 371 02e7 23          	.byte 0x23
 372 02e8 00          	.uleb128 0x0
 373 02e9 05          	.uleb128 0x5
 374 02ea 4F 43 39 49 	.asciz "OC9IF"
 374      46 00 
 375 02f0 02          	.byte 0x2
 376 02f1 7D 25       	.2byte 0x257d
 377 02f3 F9 00 00 00 	.4byte 0xf9
 378 02f7 02          	.byte 0x2
 379 02f8 01          	.byte 0x1
 380 02f9 03          	.byte 0x3
 381 02fa 02          	.byte 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 11


 382 02fb 23          	.byte 0x23
 383 02fc 00          	.uleb128 0x0
 384 02fd 05          	.uleb128 0x5
 385 02fe 49 43 39 49 	.asciz "IC9IF"
 385      46 00 
 386 0304 02          	.byte 0x2
 387 0305 7E 25       	.2byte 0x257e
 388 0307 F9 00 00 00 	.4byte 0xf9
 389 030b 02          	.byte 0x2
 390 030c 01          	.byte 0x1
 391 030d 02          	.byte 0x2
 392 030e 02          	.byte 0x2
 393 030f 23          	.byte 0x23
 394 0310 00          	.uleb128 0x0
 395 0311 05          	.uleb128 0x5
 396 0312 50 57 4D 31 	.asciz "PWM1IF"
 396      49 46 00 
 397 0319 02          	.byte 0x2
 398 031a 7F 25       	.2byte 0x257f
 399 031c F9 00 00 00 	.4byte 0xf9
 400 0320 02          	.byte 0x2
 401 0321 01          	.byte 0x1
 402 0322 01          	.byte 0x1
 403 0323 02          	.byte 0x2
 404 0324 23          	.byte 0x23
 405 0325 00          	.uleb128 0x0
 406 0326 05          	.uleb128 0x5
 407 0327 50 57 4D 32 	.asciz "PWM2IF"
 407      49 46 00 
 408 032e 02          	.byte 0x2
 409 032f 80 25       	.2byte 0x2580
 410 0331 F9 00 00 00 	.4byte 0xf9
 411 0335 02          	.byte 0x2
 412 0336 01          	.byte 0x1
 413 0337 10          	.byte 0x10
 414 0338 02          	.byte 0x2
 415 0339 23          	.byte 0x23
 416 033a 00          	.uleb128 0x0
 417 033b 00          	.byte 0x0
 418 033c 06          	.uleb128 0x6
 419 033d 49 46 53 35 	.asciz "IFS5BITS"
 419      42 49 54 53 
 419      00 
 420 0346 02          	.byte 0x2
 421 0347 81 25       	.2byte 0x2581
 422 0349 18 02 00 00 	.4byte 0x218
 423 034d 04          	.uleb128 0x4
 424 034e 74 61 67 49 	.asciz "tagIEC5BITS"
 424      45 43 35 42 
 424      49 54 53 00 
 425 035a 02          	.byte 0x2
 426 035b 02          	.byte 0x2
 427 035c 1D 26       	.2byte 0x261d
 428 035e 71 04 00 00 	.4byte 0x471
 429 0362 05          	.uleb128 0x5
 430 0363 55 33 45 49 	.asciz "U3EIE"
 430      45 00 
MPLAB XC16 ASSEMBLY Listing:   			page 12


 431 0369 02          	.byte 0x2
 432 036a 1F 26       	.2byte 0x261f
 433 036c F9 00 00 00 	.4byte 0xf9
 434 0370 02          	.byte 0x2
 435 0371 01          	.byte 0x1
 436 0372 0E          	.byte 0xe
 437 0373 02          	.byte 0x2
 438 0374 23          	.byte 0x23
 439 0375 00          	.uleb128 0x0
 440 0376 05          	.uleb128 0x5
 441 0377 55 33 52 58 	.asciz "U3RXIE"
 441      49 45 00 
 442 037e 02          	.byte 0x2
 443 037f 20 26       	.2byte 0x2620
 444 0381 F9 00 00 00 	.4byte 0xf9
 445 0385 02          	.byte 0x2
 446 0386 01          	.byte 0x1
 447 0387 0D          	.byte 0xd
 448 0388 02          	.byte 0x2
 449 0389 23          	.byte 0x23
 450 038a 00          	.uleb128 0x0
 451 038b 05          	.uleb128 0x5
 452 038c 55 33 54 58 	.asciz "U3TXIE"
 452      49 45 00 
 453 0393 02          	.byte 0x2
 454 0394 21 26       	.2byte 0x2621
 455 0396 F9 00 00 00 	.4byte 0xf9
 456 039a 02          	.byte 0x2
 457 039b 01          	.byte 0x1
 458 039c 0C          	.byte 0xc
 459 039d 02          	.byte 0x2
 460 039e 23          	.byte 0x23
 461 039f 00          	.uleb128 0x0
 462 03a0 05          	.uleb128 0x5
 463 03a1 55 53 42 31 	.asciz "USB1IE"
 463      49 45 00 
 464 03a8 02          	.byte 0x2
 465 03a9 23 26       	.2byte 0x2623
 466 03ab F9 00 00 00 	.4byte 0xf9
 467 03af 02          	.byte 0x2
 468 03b0 01          	.byte 0x1
 469 03b1 09          	.byte 0x9
 470 03b2 02          	.byte 0x2
 471 03b3 23          	.byte 0x23
 472 03b4 00          	.uleb128 0x0
 473 03b5 05          	.uleb128 0x5
 474 03b6 55 34 45 49 	.asciz "U4EIE"
 474      45 00 
 475 03bc 02          	.byte 0x2
 476 03bd 24 26       	.2byte 0x2624
 477 03bf F9 00 00 00 	.4byte 0xf9
 478 03c3 02          	.byte 0x2
 479 03c4 01          	.byte 0x1
 480 03c5 08          	.byte 0x8
 481 03c6 02          	.byte 0x2
 482 03c7 23          	.byte 0x23
 483 03c8 00          	.uleb128 0x0
MPLAB XC16 ASSEMBLY Listing:   			page 13


 484 03c9 05          	.uleb128 0x5
 485 03ca 55 34 52 58 	.asciz "U4RXIE"
 485      49 45 00 
 486 03d1 02          	.byte 0x2
 487 03d2 25 26       	.2byte 0x2625
 488 03d4 F9 00 00 00 	.4byte 0xf9
 489 03d8 02          	.byte 0x2
 490 03d9 01          	.byte 0x1
 491 03da 07          	.byte 0x7
 492 03db 02          	.byte 0x2
 493 03dc 23          	.byte 0x23
 494 03dd 00          	.uleb128 0x0
 495 03de 05          	.uleb128 0x5
 496 03df 55 34 54 58 	.asciz "U4TXIE"
 496      49 45 00 
 497 03e6 02          	.byte 0x2
 498 03e7 26 26       	.2byte 0x2626
 499 03e9 F9 00 00 00 	.4byte 0xf9
 500 03ed 02          	.byte 0x2
 501 03ee 01          	.byte 0x1
 502 03ef 06          	.byte 0x6
 503 03f0 02          	.byte 0x2
 504 03f1 23          	.byte 0x23
 505 03f2 00          	.uleb128 0x0
 506 03f3 05          	.uleb128 0x5
 507 03f4 53 50 49 33 	.asciz "SPI3EIE"
 507      45 49 45 00 
 508 03fc 02          	.byte 0x2
 509 03fd 27 26       	.2byte 0x2627
 510 03ff F9 00 00 00 	.4byte 0xf9
 511 0403 02          	.byte 0x2
 512 0404 01          	.byte 0x1
 513 0405 05          	.byte 0x5
 514 0406 02          	.byte 0x2
 515 0407 23          	.byte 0x23
 516 0408 00          	.uleb128 0x0
 517 0409 05          	.uleb128 0x5
 518 040a 53 50 49 33 	.asciz "SPI3IE"
 518      49 45 00 
 519 0411 02          	.byte 0x2
 520 0412 28 26       	.2byte 0x2628
 521 0414 F9 00 00 00 	.4byte 0xf9
 522 0418 02          	.byte 0x2
 523 0419 01          	.byte 0x1
 524 041a 04          	.byte 0x4
 525 041b 02          	.byte 0x2
 526 041c 23          	.byte 0x23
 527 041d 00          	.uleb128 0x0
 528 041e 05          	.uleb128 0x5
 529 041f 4F 43 39 49 	.asciz "OC9IE"
 529      45 00 
 530 0425 02          	.byte 0x2
 531 0426 29 26       	.2byte 0x2629
 532 0428 F9 00 00 00 	.4byte 0xf9
 533 042c 02          	.byte 0x2
 534 042d 01          	.byte 0x1
 535 042e 03          	.byte 0x3
MPLAB XC16 ASSEMBLY Listing:   			page 14


 536 042f 02          	.byte 0x2
 537 0430 23          	.byte 0x23
 538 0431 00          	.uleb128 0x0
 539 0432 05          	.uleb128 0x5
 540 0433 49 43 39 49 	.asciz "IC9IE"
 540      45 00 
 541 0439 02          	.byte 0x2
 542 043a 2A 26       	.2byte 0x262a
 543 043c F9 00 00 00 	.4byte 0xf9
 544 0440 02          	.byte 0x2
 545 0441 01          	.byte 0x1
 546 0442 02          	.byte 0x2
 547 0443 02          	.byte 0x2
 548 0444 23          	.byte 0x23
 549 0445 00          	.uleb128 0x0
 550 0446 05          	.uleb128 0x5
 551 0447 50 57 4D 31 	.asciz "PWM1IE"
 551      49 45 00 
 552 044e 02          	.byte 0x2
 553 044f 2B 26       	.2byte 0x262b
 554 0451 F9 00 00 00 	.4byte 0xf9
 555 0455 02          	.byte 0x2
 556 0456 01          	.byte 0x1
 557 0457 01          	.byte 0x1
 558 0458 02          	.byte 0x2
 559 0459 23          	.byte 0x23
 560 045a 00          	.uleb128 0x0
 561 045b 05          	.uleb128 0x5
 562 045c 50 57 4D 32 	.asciz "PWM2IE"
 562      49 45 00 
 563 0463 02          	.byte 0x2
 564 0464 2C 26       	.2byte 0x262c
 565 0466 F9 00 00 00 	.4byte 0xf9
 566 046a 02          	.byte 0x2
 567 046b 01          	.byte 0x1
 568 046c 10          	.byte 0x10
 569 046d 02          	.byte 0x2
 570 046e 23          	.byte 0x23
 571 046f 00          	.uleb128 0x0
 572 0470 00          	.byte 0x0
 573 0471 06          	.uleb128 0x6
 574 0472 49 45 43 35 	.asciz "IEC5BITS"
 574      42 49 54 53 
 574      00 
 575 047b 02          	.byte 0x2
 576 047c 2D 26       	.2byte 0x262d
 577 047e 4D 03 00 00 	.4byte 0x34d
 578 0482 02          	.uleb128 0x2
 579 0483 02          	.byte 0x2
 580 0484 07          	.byte 0x7
 581 0485 73 68 6F 72 	.asciz "short unsigned int"
 581      74 20 75 6E 
 581      73 69 67 6E 
 581      65 64 20 69 
 581      6E 74 00 
 582 0498 07          	.uleb128 0x7
 583 0499 01          	.byte 0x1
MPLAB XC16 ASSEMBLY Listing:   			page 15


 584 049a 55 53 42 53 	.asciz "USBSleepOnSuspend"
 584      6C 65 65 70 
 584      4F 6E 53 75 
 584      73 70 65 6E 
 584      64 00 
 585 04ac 01          	.byte 0x1
 586 04ad 37          	.byte 0x37
 587 04ae 01          	.byte 0x1
 588 04af 48 01 00 00 	.4byte 0x148
 589 04b3 00 00 00 00 	.4byte .LFB0
 590 04b7 00 00 00 00 	.4byte .LFE0
 591 04bb 01          	.byte 0x1
 592 04bc 5E          	.byte 0x5e
 593 04bd 18 05 00 00 	.4byte 0x518
 594 04c1 08          	.uleb128 0x8
 595 04c2 55 31 45 49 	.asciz "U1EIE_save"
 595      45 5F 73 61 
 595      76 65 00 
 596 04cd 01          	.byte 0x1
 597 04ce 39          	.byte 0x39
 598 04cf 09 01 00 00 	.4byte 0x109
 599 04d3 02          	.byte 0x2
 600 04d4 7E          	.byte 0x7e
 601 04d5 00          	.sleb128 0
 602 04d6 08          	.uleb128 0x8
 603 04d7 55 31 49 45 	.asciz "U1IE_save"
 603      5F 73 61 76 
 603      65 00 
 604 04e1 01          	.byte 0x1
 605 04e2 39          	.byte 0x39
 606 04e3 09 01 00 00 	.4byte 0x109
 607 04e7 02          	.byte 0x2
 608 04e8 7E          	.byte 0x7e
 609 04e9 02          	.sleb128 2
 610 04ea 08          	.uleb128 0x8
 611 04eb 55 31 4F 54 	.asciz "U1OTGIE_save"
 611      47 49 45 5F 
 611      73 61 76 65 
 611      00 
 612 04f8 01          	.byte 0x1
 613 04f9 39          	.byte 0x39
 614 04fa 09 01 00 00 	.4byte 0x109
 615 04fe 02          	.byte 0x2
 616 04ff 7E          	.byte 0x7e
 617 0500 04          	.sleb128 4
 618 0501 08          	.uleb128 0x8
 619 0502 55 53 42 31 	.asciz "USB1IE_save"
 619      49 45 5F 73 
 619      61 76 65 00 
 620 050e 01          	.byte 0x1
 621 050f 3A          	.byte 0x3a
 622 0510 E8 00 00 00 	.4byte 0xe8
 623 0514 02          	.byte 0x2
 624 0515 7E          	.byte 0x7e
 625 0516 06          	.sleb128 6
 626 0517 00          	.byte 0x0
 627 0518 09          	.uleb128 0x9
MPLAB XC16 ASSEMBLY Listing:   			page 16


 628 0519 55 31 4F 54 	.asciz "U1OTGIR"
 628      47 49 52 00 
 629 0521 02          	.byte 0x2
 630 0522 52 12       	.2byte 0x1252
 631 0524 2A 05 00 00 	.4byte 0x52a
 632 0528 01          	.byte 0x1
 633 0529 01          	.byte 0x1
 634 052a 0A          	.uleb128 0xa
 635 052b F9 00 00 00 	.4byte 0xf9
 636 052f 09          	.uleb128 0x9
 637 0530 55 31 4F 54 	.asciz "U1OTGIE"
 637      47 49 45 00 
 638 0538 02          	.byte 0x2
 639 0539 60 12       	.2byte 0x1260
 640 053b 2A 05 00 00 	.4byte 0x52a
 641 053f 01          	.byte 0x1
 642 0540 01          	.byte 0x1
 643 0541 0B          	.uleb128 0xb
 644 0542 00 00 00 00 	.4byte .LASF0
 645 0546 02          	.byte 0x2
 646 0547 6B 12       	.2byte 0x126b
 647 0549 4F 05 00 00 	.4byte 0x54f
 648 054d 01          	.byte 0x1
 649 054e 01          	.byte 0x1
 650 054f 0A          	.uleb128 0xa
 651 0550 04 02 00 00 	.4byte 0x204
 652 0554 09          	.uleb128 0x9
 653 0555 55 31 49 45 	.asciz "U1IE"
 653      00 
 654 055a 02          	.byte 0x2
 655 055b B5 12       	.2byte 0x12b5
 656 055d 2A 05 00 00 	.4byte 0x52a
 657 0561 01          	.byte 0x1
 658 0562 01          	.byte 0x1
 659 0563 09          	.uleb128 0x9
 660 0564 55 31 45 49 	.asciz "U1EIE"
 660      45 00 
 661 056a 02          	.byte 0x2
 662 056b E2 12       	.2byte 0x12e2
 663 056d 2A 05 00 00 	.4byte 0x52a
 664 0571 01          	.byte 0x1
 665 0572 01          	.byte 0x1
 666 0573 0B          	.uleb128 0xb
 667 0574 00 00 00 00 	.4byte .LASF1
 668 0578 02          	.byte 0x2
 669 0579 82 25       	.2byte 0x2582
 670 057b 81 05 00 00 	.4byte 0x581
 671 057f 01          	.byte 0x1
 672 0580 01          	.byte 0x1
 673 0581 0A          	.uleb128 0xa
 674 0582 3C 03 00 00 	.4byte 0x33c
 675 0586 0B          	.uleb128 0xb
 676 0587 00 00 00 00 	.4byte .LASF2
 677 058b 02          	.byte 0x2
 678 058c 2E 26       	.2byte 0x262e
 679 058e 94 05 00 00 	.4byte 0x594
 680 0592 01          	.byte 0x1
MPLAB XC16 ASSEMBLY Listing:   			page 17


 681 0593 01          	.byte 0x1
 682 0594 0A          	.uleb128 0xa
 683 0595 71 04 00 00 	.4byte 0x471
 684 0599 09          	.uleb128 0x9
 685 059a 55 31 4F 54 	.asciz "U1OTGIR"
 685      47 49 52 00 
 686 05a2 02          	.byte 0x2
 687 05a3 52 12       	.2byte 0x1252
 688 05a5 2A 05 00 00 	.4byte 0x52a
 689 05a9 01          	.byte 0x1
 690 05aa 01          	.byte 0x1
 691 05ab 09          	.uleb128 0x9
 692 05ac 55 31 4F 54 	.asciz "U1OTGIE"
 692      47 49 45 00 
 693 05b4 02          	.byte 0x2
 694 05b5 60 12       	.2byte 0x1260
 695 05b7 2A 05 00 00 	.4byte 0x52a
 696 05bb 01          	.byte 0x1
 697 05bc 01          	.byte 0x1
 698 05bd 0B          	.uleb128 0xb
 699 05be 00 00 00 00 	.4byte .LASF0
 700 05c2 02          	.byte 0x2
 701 05c3 6B 12       	.2byte 0x126b
 702 05c5 4F 05 00 00 	.4byte 0x54f
 703 05c9 01          	.byte 0x1
 704 05ca 01          	.byte 0x1
 705 05cb 09          	.uleb128 0x9
 706 05cc 55 31 49 45 	.asciz "U1IE"
 706      00 
 707 05d1 02          	.byte 0x2
 708 05d2 B5 12       	.2byte 0x12b5
 709 05d4 2A 05 00 00 	.4byte 0x52a
 710 05d8 01          	.byte 0x1
 711 05d9 01          	.byte 0x1
 712 05da 09          	.uleb128 0x9
 713 05db 55 31 45 49 	.asciz "U1EIE"
 713      45 00 
 714 05e1 02          	.byte 0x2
 715 05e2 E2 12       	.2byte 0x12e2
 716 05e4 2A 05 00 00 	.4byte 0x52a
 717 05e8 01          	.byte 0x1
 718 05e9 01          	.byte 0x1
 719 05ea 0B          	.uleb128 0xb
 720 05eb 00 00 00 00 	.4byte .LASF1
 721 05ef 02          	.byte 0x2
 722 05f0 82 25       	.2byte 0x2582
 723 05f2 81 05 00 00 	.4byte 0x581
 724 05f6 01          	.byte 0x1
 725 05f7 01          	.byte 0x1
 726 05f8 0B          	.uleb128 0xb
 727 05f9 00 00 00 00 	.4byte .LASF2
 728 05fd 02          	.byte 0x2
 729 05fe 2E 26       	.2byte 0x262e
 730 0600 94 05 00 00 	.4byte 0x594
 731 0604 01          	.byte 0x1
 732 0605 01          	.byte 0x1
 733 0606 00          	.byte 0x0
MPLAB XC16 ASSEMBLY Listing:   			page 18


 734                 	.section .debug_abbrev,info
 735 0000 01          	.uleb128 0x1
 736 0001 11          	.uleb128 0x11
 737 0002 01          	.byte 0x1
 738 0003 25          	.uleb128 0x25
 739 0004 08          	.uleb128 0x8
 740 0005 13          	.uleb128 0x13
 741 0006 0B          	.uleb128 0xb
 742 0007 03          	.uleb128 0x3
 743 0008 08          	.uleb128 0x8
 744 0009 1B          	.uleb128 0x1b
 745 000a 08          	.uleb128 0x8
 746 000b 11          	.uleb128 0x11
 747 000c 01          	.uleb128 0x1
 748 000d 12          	.uleb128 0x12
 749 000e 01          	.uleb128 0x1
 750 000f 10          	.uleb128 0x10
 751 0010 06          	.uleb128 0x6
 752 0011 00          	.byte 0x0
 753 0012 00          	.byte 0x0
 754 0013 02          	.uleb128 0x2
 755 0014 24          	.uleb128 0x24
 756 0015 00          	.byte 0x0
 757 0016 0B          	.uleb128 0xb
 758 0017 0B          	.uleb128 0xb
 759 0018 3E          	.uleb128 0x3e
 760 0019 0B          	.uleb128 0xb
 761 001a 03          	.uleb128 0x3
 762 001b 08          	.uleb128 0x8
 763 001c 00          	.byte 0x0
 764 001d 00          	.byte 0x0
 765 001e 03          	.uleb128 0x3
 766 001f 16          	.uleb128 0x16
 767 0020 00          	.byte 0x0
 768 0021 03          	.uleb128 0x3
 769 0022 08          	.uleb128 0x8
 770 0023 3A          	.uleb128 0x3a
 771 0024 0B          	.uleb128 0xb
 772 0025 3B          	.uleb128 0x3b
 773 0026 0B          	.uleb128 0xb
 774 0027 49          	.uleb128 0x49
 775 0028 13          	.uleb128 0x13
 776 0029 00          	.byte 0x0
 777 002a 00          	.byte 0x0
 778 002b 04          	.uleb128 0x4
 779 002c 13          	.uleb128 0x13
 780 002d 01          	.byte 0x1
 781 002e 03          	.uleb128 0x3
 782 002f 08          	.uleb128 0x8
 783 0030 0B          	.uleb128 0xb
 784 0031 0B          	.uleb128 0xb
 785 0032 3A          	.uleb128 0x3a
 786 0033 0B          	.uleb128 0xb
 787 0034 3B          	.uleb128 0x3b
 788 0035 05          	.uleb128 0x5
 789 0036 01          	.uleb128 0x1
 790 0037 13          	.uleb128 0x13
MPLAB XC16 ASSEMBLY Listing:   			page 19


 791 0038 00          	.byte 0x0
 792 0039 00          	.byte 0x0
 793 003a 05          	.uleb128 0x5
 794 003b 0D          	.uleb128 0xd
 795 003c 00          	.byte 0x0
 796 003d 03          	.uleb128 0x3
 797 003e 08          	.uleb128 0x8
 798 003f 3A          	.uleb128 0x3a
 799 0040 0B          	.uleb128 0xb
 800 0041 3B          	.uleb128 0x3b
 801 0042 05          	.uleb128 0x5
 802 0043 49          	.uleb128 0x49
 803 0044 13          	.uleb128 0x13
 804 0045 0B          	.uleb128 0xb
 805 0046 0B          	.uleb128 0xb
 806 0047 0D          	.uleb128 0xd
 807 0048 0B          	.uleb128 0xb
 808 0049 0C          	.uleb128 0xc
 809 004a 0B          	.uleb128 0xb
 810 004b 38          	.uleb128 0x38
 811 004c 0A          	.uleb128 0xa
 812 004d 00          	.byte 0x0
 813 004e 00          	.byte 0x0
 814 004f 06          	.uleb128 0x6
 815 0050 16          	.uleb128 0x16
 816 0051 00          	.byte 0x0
 817 0052 03          	.uleb128 0x3
 818 0053 08          	.uleb128 0x8
 819 0054 3A          	.uleb128 0x3a
 820 0055 0B          	.uleb128 0xb
 821 0056 3B          	.uleb128 0x3b
 822 0057 05          	.uleb128 0x5
 823 0058 49          	.uleb128 0x49
 824 0059 13          	.uleb128 0x13
 825 005a 00          	.byte 0x0
 826 005b 00          	.byte 0x0
 827 005c 07          	.uleb128 0x7
 828 005d 2E          	.uleb128 0x2e
 829 005e 01          	.byte 0x1
 830 005f 3F          	.uleb128 0x3f
 831 0060 0C          	.uleb128 0xc
 832 0061 03          	.uleb128 0x3
 833 0062 08          	.uleb128 0x8
 834 0063 3A          	.uleb128 0x3a
 835 0064 0B          	.uleb128 0xb
 836 0065 3B          	.uleb128 0x3b
 837 0066 0B          	.uleb128 0xb
 838 0067 27          	.uleb128 0x27
 839 0068 0C          	.uleb128 0xc
 840 0069 49          	.uleb128 0x49
 841 006a 13          	.uleb128 0x13
 842 006b 11          	.uleb128 0x11
 843 006c 01          	.uleb128 0x1
 844 006d 12          	.uleb128 0x12
 845 006e 01          	.uleb128 0x1
 846 006f 40          	.uleb128 0x40
 847 0070 0A          	.uleb128 0xa
MPLAB XC16 ASSEMBLY Listing:   			page 20


 848 0071 01          	.uleb128 0x1
 849 0072 13          	.uleb128 0x13
 850 0073 00          	.byte 0x0
 851 0074 00          	.byte 0x0
 852 0075 08          	.uleb128 0x8
 853 0076 34          	.uleb128 0x34
 854 0077 00          	.byte 0x0
 855 0078 03          	.uleb128 0x3
 856 0079 08          	.uleb128 0x8
 857 007a 3A          	.uleb128 0x3a
 858 007b 0B          	.uleb128 0xb
 859 007c 3B          	.uleb128 0x3b
 860 007d 0B          	.uleb128 0xb
 861 007e 49          	.uleb128 0x49
 862 007f 13          	.uleb128 0x13
 863 0080 02          	.uleb128 0x2
 864 0081 0A          	.uleb128 0xa
 865 0082 00          	.byte 0x0
 866 0083 00          	.byte 0x0
 867 0084 09          	.uleb128 0x9
 868 0085 34          	.uleb128 0x34
 869 0086 00          	.byte 0x0
 870 0087 03          	.uleb128 0x3
 871 0088 08          	.uleb128 0x8
 872 0089 3A          	.uleb128 0x3a
 873 008a 0B          	.uleb128 0xb
 874 008b 3B          	.uleb128 0x3b
 875 008c 05          	.uleb128 0x5
 876 008d 49          	.uleb128 0x49
 877 008e 13          	.uleb128 0x13
 878 008f 3F          	.uleb128 0x3f
 879 0090 0C          	.uleb128 0xc
 880 0091 3C          	.uleb128 0x3c
 881 0092 0C          	.uleb128 0xc
 882 0093 00          	.byte 0x0
 883 0094 00          	.byte 0x0
 884 0095 0A          	.uleb128 0xa
 885 0096 35          	.uleb128 0x35
 886 0097 00          	.byte 0x0
 887 0098 49          	.uleb128 0x49
 888 0099 13          	.uleb128 0x13
 889 009a 00          	.byte 0x0
 890 009b 00          	.byte 0x0
 891 009c 0B          	.uleb128 0xb
 892 009d 34          	.uleb128 0x34
 893 009e 00          	.byte 0x0
 894 009f 03          	.uleb128 0x3
 895 00a0 0E          	.uleb128 0xe
 896 00a1 3A          	.uleb128 0x3a
 897 00a2 0B          	.uleb128 0xb
 898 00a3 3B          	.uleb128 0x3b
 899 00a4 05          	.uleb128 0x5
 900 00a5 49          	.uleb128 0x49
 901 00a6 13          	.uleb128 0x13
 902 00a7 3F          	.uleb128 0x3f
 903 00a8 0C          	.uleb128 0xc
 904 00a9 3C          	.uleb128 0x3c
MPLAB XC16 ASSEMBLY Listing:   			page 21


 905 00aa 0C          	.uleb128 0xc
 906 00ab 00          	.byte 0x0
 907 00ac 00          	.byte 0x0
 908 00ad 00          	.byte 0x0
 909                 	.section .debug_pubnames,info
 910 0000 24 00 00 00 	.4byte 0x24
 911 0004 02 00       	.2byte 0x2
 912 0006 00 00 00 00 	.4byte .Ldebug_info0
 913 000a 07 06 00 00 	.4byte 0x607
 914 000e 98 04 00 00 	.4byte 0x498
 915 0012 55 53 42 53 	.asciz "USBSleepOnSuspend"
 915      6C 65 65 70 
 915      4F 6E 53 75 
 915      73 70 65 6E 
 915      64 00 
 916 0024 00 00 00 00 	.4byte 0x0
 917                 	.section .debug_pubtypes,info
 918 0000 78 00 00 00 	.4byte 0x78
 919 0004 02 00       	.2byte 0x2
 920 0006 00 00 00 00 	.4byte .Ldebug_info0
 921 000a 07 06 00 00 	.4byte 0x607
 922 000e F9 00 00 00 	.4byte 0xf9
 923 0012 75 69 6E 74 	.asciz "uint16_t"
 923      31 36 5F 74 
 923      00 
 924 001b 51 01 00 00 	.4byte 0x151
 925 001f 74 61 67 55 	.asciz "tagU1OTGIEBITS"
 925      31 4F 54 47 
 925      49 45 42 49 
 925      54 53 00 
 926 002e 04 02 00 00 	.4byte 0x204
 927 0032 55 31 4F 54 	.asciz "U1OTGIEBITS"
 927      47 49 45 42 
 927      49 54 53 00 
 928 003e 18 02 00 00 	.4byte 0x218
 929 0042 74 61 67 49 	.asciz "tagIFS5BITS"
 929      46 53 35 42 
 929      49 54 53 00 
 930 004e 3C 03 00 00 	.4byte 0x33c
 931 0052 49 46 53 35 	.asciz "IFS5BITS"
 931      42 49 54 53 
 931      00 
 932 005b 4D 03 00 00 	.4byte 0x34d
 933 005f 74 61 67 49 	.asciz "tagIEC5BITS"
 933      45 43 35 42 
 933      49 54 53 00 
 934 006b 71 04 00 00 	.4byte 0x471
 935 006f 49 45 43 35 	.asciz "IEC5BITS"
 935      42 49 54 53 
 935      00 
 936 0078 00 00 00 00 	.4byte 0x0
 937                 	.section .debug_aranges,info
 938 0000 14 00 00 00 	.4byte 0x14
 939 0004 02 00       	.2byte 0x2
 940 0006 00 00 00 00 	.4byte .Ldebug_info0
 941 000a 04          	.byte 0x4
 942 000b 00          	.byte 0x0
MPLAB XC16 ASSEMBLY Listing:   			page 22


 943 000c 00 00       	.2byte 0x0
 944 000e 00 00       	.2byte 0x0
 945 0010 00 00 00 00 	.4byte 0x0
 946 0014 00 00 00 00 	.4byte 0x0
 947                 	.section .debug_str,info
 948                 	.LASF0:
 949 0000 55 31 4F 54 	.asciz "U1OTGIEbits"
 949      47 49 45 62 
 949      69 74 73 00 
 950                 	.LASF1:
 951 000c 49 46 53 35 	.asciz "IFS5bits"
 951      62 69 74 73 
 951      00 
 952                 	.LASF2:
 953 0015 49 45 43 35 	.asciz "IEC5bits"
 953      62 69 74 73 
 953      00 
 954                 	.section .text,code
 955              	
 956              	
 957              	
 958              	.section __c30_info,info,bss
 959                 	__psv_trap_errata:
 960                 	
 961                 	.section __c30_signature,info,data
 962 0000 01 00       	.word 0x0001
 963 0002 00 00       	.word 0x0000
 964 0004 00 00       	.word 0x0000
 965                 	
 966                 	
 967                 	
 968                 	.set ___PA___,0
 969                 	.end
MPLAB XC16 ASSEMBLY Listing:   			page 23


DEFINED SYMBOLS
                            *ABS*:00000000 /home/user/Documents/FST/Programming/project_sub-zero/lib/lib_pic33e/usb/usb_hal_dspic33e.c
                            *ABS*:00000001 __33EP256MU806
                            *ABS*:00000001 __dsPIC33E
                            *ABS*:00000001 __HAS_DSP
                            *ABS*:00000001 __HAS_DMAV2
                            *ABS*:00000001 __HAS_CODEGUARD
                            *ABS*:00000001 __HAS_PMP_ENHANCED
                            *ABS*:00000001 __HAS_EDS
                            *ABS*:00000001 __HAS_EP
                            *ABS*:00000011 __TARGET_DIVIDE_CYCLES
                            *ABS*:00000001 __C30ELF
    {standard input}:14     .text:00000000 _USBSleepOnSuspend
    {standard input}:18     *ABS*:00000000 ___PA___
    {standard input}:959    __c30_info:00000000 __psv_trap_errata
    {standard input}:19     .text:00000000 .L0
                    .debug_abbrev:00000000 .Ldebug_abbrev0
                            .text:00000000 .Ltext0
                            .text:00000052 .Letext0
                      .debug_line:00000000 .Ldebug_line0
                            .text:00000000 .LFB0
                            .text:00000052 .LFE0
                       .debug_str:00000000 .LASF0
                       .debug_str:0000000c .LASF1
                       .debug_str:00000015 .LASF2
                     .debug_frame:00000000 .Lframe0
                      .debug_info:00000000 .Ldebug_info0

UNDEFINED SYMBOLS
_U1EIE
_U1IE
_U1OTGIE
_IEC5bits
_IFS5bits
_U1OTGIEbits
_U1OTGIR
CORCON

EQUATE SYMBOLS
/home/user/Documents/FST/Programming/project_sub-zero/lib/lib_pic33e/usb/usb_hal_dspic33e.c = 0x0
                  __33EP256MU806 = 0x1
                      __dsPIC33E = 0x1
                       __HAS_DSP = 0x1
                     __HAS_DMAV2 = 0x1
                 __HAS_CODEGUARD = 0x1
              __HAS_PMP_ENHANCED = 0x1
                       __HAS_EDS = 0x1
                        __HAS_EP = 0x1
          __TARGET_DIVIDE_CYCLES = 0x11
                        __C30ELF = 0x1
                        ___PA___ = 0x0
