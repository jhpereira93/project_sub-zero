MPLAB XC16 ASSEMBLY Listing:   			page 1


   1              	.file "/home/user/Documents/FST/Programming/project_sub-zero/lib/lib_pic33e/usb/usb_descriptors.c"
   2              	.section .debug_abbrev,info
   3                 	.Ldebug_abbrev0:
   4                 	.section .debug_info,info
   5                 	.Ldebug_info0:
   6                 	.section .debug_line,info
   7                 	.Ldebug_line0:
   8 0000 B2 00 00 00 	.section .text,code
   8      02 00 AC 00 
   8      00 00 01 01 
   8      FB 0E 0A 00 
   8      01 01 01 01 
   8      00 00 00 01 
   8      2F 6F 70 74 
   8      2F 6D 69 63 
   8      72 6F 63 68 
   9              	.Ltext0:
  10              	.global _device_dsc
  11              	.section .const,psv,page
  12                 	.align 2
  13                 	.type _device_dsc,@object
  14                 	.size _device_dsc,18
  15                 	_device_dsc:
  16 0000 12          	.byte 18
  17 0001 01          	.byte 1
  18 0002 00 02       	.word 512
  19 0004 02          	.byte 2
  20 0005 00          	.byte 0
  21 0006 00          	.byte 0
  22 0007 08          	.byte 8
  23 0008 D8 04       	.word 1240
  24 000a 0A 00       	.word 10
  25 000c 00 01       	.word 256
  26 000e 01          	.byte 1
  27 000f 02          	.byte 2
  28 0010 00          	.byte 0
  29 0011 01          	.byte 1
  30                 	.global _configDescriptor1
  31                 	.type _configDescriptor1,@object
  32                 	.size _configDescriptor1,67
  33                 	_configDescriptor1:
  34 0012 09          	.byte 9
  35 0013 02          	.byte 2
  36 0014 43          	.byte 67
  37 0015 00          	.byte 0
  38 0016 02          	.byte 2
  39 0017 01          	.byte 1
  40 0018 00          	.byte 0
  41 0019 C0          	.byte -64
  42 001a 32          	.byte 50
  43 001b 09          	.byte 9
  44 001c 04          	.byte 4
  45 001d 00          	.byte 0
  46 001e 00          	.byte 0
  47 001f 01          	.byte 1
  48 0020 02          	.byte 2
  49 0021 02          	.byte 2
MPLAB XC16 ASSEMBLY Listing:   			page 2


  50 0022 01          	.byte 1
  51 0023 00          	.byte 0
  52 0024 05          	.byte 5
  53 0025 24          	.byte 36
  54 0026 00          	.byte 0
  55 0027 10          	.byte 16
  56 0028 01          	.byte 1
  57 0029 04          	.byte 4
  58 002a 24          	.byte 36
  59 002b 02          	.byte 2
  60 002c 02          	.byte 2
  61 002d 05          	.byte 5
  62 002e 24          	.byte 36
  63 002f 06          	.byte 6
  64 0030 00          	.byte 0
  65 0031 01          	.byte 1
  66 0032 05          	.byte 5
  67 0033 24          	.byte 36
  68 0034 01          	.byte 1
  69 0035 00          	.byte 0
  70 0036 01          	.byte 1
  71 0037 07          	.byte 7
  72 0038 05          	.byte 5
  73 0039 81          	.byte -127
  74 003a 03          	.byte 3
  75 003b 08          	.byte 8
  76 003c 00          	.byte 0
  77 003d 02          	.byte 2
  78 003e 09          	.byte 9
  79 003f 04          	.byte 4
  80 0040 01          	.byte 1
  81 0041 00          	.byte 0
  82 0042 02          	.byte 2
  83 0043 0A          	.byte 10
  84 0044 00          	.byte 0
  85 0045 00          	.byte 0
  86 0046 00          	.byte 0
  87 0047 07          	.byte 7
  88 0048 05          	.byte 5
  89 0049 02          	.byte 2
  90 004a 02          	.byte 2
  91 004b 40          	.byte 64
  92 004c 00          	.byte 0
  93 004d 00          	.byte 0
  94 004e 07          	.byte 7
  95 004f 05          	.byte 5
  96 0050 82          	.byte -126
  97 0051 02          	.byte 2
  98 0052 40          	.byte 64
  99 0053 00          	.byte 0
 100 0054 00          	.byte 0
 101                 	.global _sd000
 102 0055 00          	.align 2
 103                 	.type _sd000,@object
 104                 	.size _sd000,4
 105                 	_sd000:
 106 0056 04          	.byte 4
MPLAB XC16 ASSEMBLY Listing:   			page 3


 107 0057 03          	.byte 3
 108 0058 09 04       	.word 1033
 109                 	.global _sd001
 110                 	.align 2
 111                 	.type _sd001,@object
 112                 	.size _sd001,52
 113                 	_sd001:
 114 005a 34          	.byte 52
 115 005b 03          	.byte 3
 116 005c 4D 00       	.word 77
 117 005e 69 00       	.word 105
 118 0060 63 00       	.word 99
 119 0062 72 00       	.word 114
 120 0064 6F 00       	.word 111
 121 0066 63 00       	.word 99
 122 0068 68 00       	.word 104
 123 006a 69 00       	.word 105
 124 006c 70 00       	.word 112
 125 006e 20 00       	.word 32
 126 0070 54 00       	.word 84
 127 0072 65 00       	.word 101
 128 0074 63 00       	.word 99
 129 0076 68 00       	.word 104
 130 0078 6E 00       	.word 110
 131 007a 6F 00       	.word 111
 132 007c 6C 00       	.word 108
 133 007e 6F 00       	.word 111
 134 0080 67 00       	.word 103
 135 0082 79 00       	.word 121
 136 0084 20 00       	.word 32
 137 0086 49 00       	.word 73
 138 0088 6E 00       	.word 110
 139 008a 63 00       	.word 99
 140 008c 2E 00       	.word 46
 141                 	.global _sd002
 142                 	.align 2
 143                 	.type _sd002,@object
 144                 	.size _sd002,52
 145                 	_sd002:
 146 008e 34          	.byte 52
 147 008f 03          	.byte 3
 148 0090 43 00       	.word 67
 149 0092 44 00       	.word 68
 150 0094 43 00       	.word 67
 151 0096 20 00       	.word 32
 152 0098 52 00       	.word 82
 153 009a 53 00       	.word 83
 154 009c 2D 00       	.word 45
 155 009e 32 00       	.word 50
 156 00a0 33 00       	.word 51
 157 00a2 32 00       	.word 50
 158 00a4 20 00       	.word 32
 159 00a6 45 00       	.word 69
 160 00a8 6D 00       	.word 109
 161 00aa 75 00       	.word 117
 162 00ac 6C 00       	.word 108
 163 00ae 61 00       	.word 97
MPLAB XC16 ASSEMBLY Listing:   			page 4


 164 00b0 74 00       	.word 116
 165 00b2 69 00       	.word 105
 166 00b4 6F 00       	.word 111
 167 00b6 6E 00       	.word 110
 168 00b8 20 00       	.word 32
 169 00ba 44 00       	.word 68
 170 00bc 65 00       	.word 101
 171 00be 6D 00       	.word 109
 172 00c0 6F 00       	.word 111
 173                 	.global _USB_CD_Ptr
 174                 	.align 2
 175                 	.type _USB_CD_Ptr,@object
 176                 	.size _USB_CD_Ptr,2
 177                 	_USB_CD_Ptr:
 178 00c2 00 00       	.word _configDescriptor1
 179                 	.global _USB_SD_Ptr
 180                 	.align 2
 181                 	.type _USB_SD_Ptr,@object
 182                 	.size _USB_SD_Ptr,6
 183                 	_USB_SD_Ptr:
 184 00c4 00 00       	.word _sd000
 185 00c6 00 00       	.word _sd001
 186 00c8 00 00       	.word _sd002
 187                 	.section .text,code
 188              	.Letext0:
 189              	.file 1 "/opt/microchip/xc16/v1.36/bin/bin/../../include/lega-c/stdint.h"
 190              	.file 2 "lib/lib_pic33e/usb/usb_ch9.h"
 191              	.file 3 "lib/lib_pic33e/usb/usb_descriptors.c"
 192              	.file 4 "lib/lib_pic33e/usb/usb_device_cdc.h"
 193              	.file 5 "lib/lib_pic33e/usb/usb_common.h"
 194              	.section .debug_info,info
 195 0000 1A 08 00 00 	.4byte 0x81a
 196 0004 02 00       	.2byte 0x2
 197 0006 00 00 00 00 	.4byte .Ldebug_abbrev0
 198 000a 04          	.byte 0x4
 199 000b 01          	.uleb128 0x1
 200 000c 47 4E 55 20 	.asciz "GNU C 4.5.1 (XC16, Microchip v1.36) (A) Build date: Dec  5 2018"
 200      43 20 34 2E 
 200      35 2E 31 20 
 200      28 58 43 31 
 200      36 2C 20 4D 
 200      69 63 72 6F 
 200      63 68 69 70 
 200      20 76 31 2E 
 200      33 36 29 20 
 201 004c 01          	.byte 0x1
 202 004d 6C 69 62 2F 	.asciz "lib/lib_pic33e/usb/usb_descriptors.c"
 202      6C 69 62 5F 
 202      70 69 63 33 
 202      33 65 2F 75 
 202      73 62 2F 75 
 202      73 62 5F 64 
 202      65 73 63 72 
 202      69 70 74 6F 
 202      72 73 2E 63 
 203 0072 2F 68 6F 6D 	.asciz "/home/user/Documents/FST/Programming/project_sub-zero"
 203      65 2F 75 73 
MPLAB XC16 ASSEMBLY Listing:   			page 5


 203      65 72 2F 44 
 203      6F 63 75 6D 
 203      65 6E 74 73 
 203      2F 46 53 54 
 203      2F 50 72 6F 
 203      67 72 61 6D 
 203      6D 69 6E 67 
 204 00a8 00 00 00 00 	.4byte .Ltext0
 205 00ac 00 00 00 00 	.4byte .Letext0
 206 00b0 00 00 00 00 	.4byte .Ldebug_line0
 207 00b4 02          	.uleb128 0x2
 208 00b5 01          	.byte 0x1
 209 00b6 06          	.byte 0x6
 210 00b7 73 69 67 6E 	.asciz "signed char"
 210      65 64 20 63 
 210      68 61 72 00 
 211 00c3 02          	.uleb128 0x2
 212 00c4 02          	.byte 0x2
 213 00c5 05          	.byte 0x5
 214 00c6 69 6E 74 00 	.asciz "int"
 215 00ca 02          	.uleb128 0x2
 216 00cb 04          	.byte 0x4
 217 00cc 05          	.byte 0x5
 218 00cd 6C 6F 6E 67 	.asciz "long int"
 218      20 69 6E 74 
 218      00 
 219 00d6 02          	.uleb128 0x2
 220 00d7 08          	.byte 0x8
 221 00d8 05          	.byte 0x5
 222 00d9 6C 6F 6E 67 	.asciz "long long int"
 222      20 6C 6F 6E 
 222      67 20 69 6E 
 222      74 00 
 223 00e7 03          	.uleb128 0x3
 224 00e8 75 69 6E 74 	.asciz "uint8_t"
 224      38 5F 74 00 
 225 00f0 01          	.byte 0x1
 226 00f1 2B          	.byte 0x2b
 227 00f2 F6 00 00 00 	.4byte 0xf6
 228 00f6 02          	.uleb128 0x2
 229 00f7 01          	.byte 0x1
 230 00f8 08          	.byte 0x8
 231 00f9 75 6E 73 69 	.asciz "unsigned char"
 231      67 6E 65 64 
 231      20 63 68 61 
 231      72 00 
 232 0107 03          	.uleb128 0x3
 233 0108 75 69 6E 74 	.asciz "uint16_t"
 233      31 36 5F 74 
 233      00 
 234 0111 01          	.byte 0x1
 235 0112 31          	.byte 0x31
 236 0113 17 01 00 00 	.4byte 0x117
 237 0117 02          	.uleb128 0x2
 238 0118 02          	.byte 0x2
 239 0119 07          	.byte 0x7
 240 011a 75 6E 73 69 	.asciz "unsigned int"
MPLAB XC16 ASSEMBLY Listing:   			page 6


 240      67 6E 65 64 
 240      20 69 6E 74 
 240      00 
 241 0127 02          	.uleb128 0x2
 242 0128 04          	.byte 0x4
 243 0129 07          	.byte 0x7
 244 012a 6C 6F 6E 67 	.asciz "long unsigned int"
 244      20 75 6E 73 
 244      69 67 6E 65 
 244      64 20 69 6E 
 244      74 00 
 245 013c 02          	.uleb128 0x2
 246 013d 08          	.byte 0x8
 247 013e 07          	.byte 0x7
 248 013f 6C 6F 6E 67 	.asciz "long long unsigned int"
 248      20 6C 6F 6E 
 248      67 20 75 6E 
 248      73 69 67 6E 
 248      65 64 20 69 
 248      6E 74 00 
 249 0156 04          	.uleb128 0x4
 250 0157 02          	.byte 0x2
 251 0158 05          	.byte 0x5
 252 0159 CF          	.byte 0xcf
 253 015a 84 04 00 00 	.4byte 0x484
 254 015e 05          	.uleb128 0x5
 255 015f 45 56 45 4E 	.asciz "EVENT_NONE"
 255      54 5F 4E 4F 
 255      4E 45 00 
 256 016a 00          	.sleb128 0
 257 016b 05          	.uleb128 0x5
 258 016c 45 56 45 4E 	.asciz "EVENT_DEVICE_STACK_BASE"
 258      54 5F 44 45 
 258      56 49 43 45 
 258      5F 53 54 41 
 258      43 4B 5F 42 
 258      41 53 45 00 
 259 0184 01          	.sleb128 1
 260 0185 05          	.uleb128 0x5
 261 0186 45 56 45 4E 	.asciz "EVENT_HOST_STACK_BASE"
 261      54 5F 48 4F 
 261      53 54 5F 53 
 261      54 41 43 4B 
 261      5F 42 41 53 
 261      45 00 
 262 019c E4 00       	.sleb128 100
 263 019e 05          	.uleb128 0x5
 264 019f 45 56 45 4E 	.asciz "EVENT_HUB_ATTACH"
 264      54 5F 48 55 
 264      42 5F 41 54 
 264      54 41 43 48 
 264      00 
 265 01b0 E5 00       	.sleb128 101
 266 01b2 05          	.uleb128 0x5
 267 01b3 45 56 45 4E 	.asciz "EVENT_STALL"
 267      54 5F 53 54 
 267      41 4C 4C 00 
MPLAB XC16 ASSEMBLY Listing:   			page 7


 268 01bf E6 00       	.sleb128 102
 269 01c1 05          	.uleb128 0x5
 270 01c2 45 56 45 4E 	.asciz "EVENT_VBUS_SES_REQUEST"
 270      54 5F 56 42 
 270      55 53 5F 53 
 270      45 53 5F 52 
 270      45 51 55 45 
 270      53 54 00 
 271 01d9 E7 00       	.sleb128 103
 272 01db 05          	.uleb128 0x5
 273 01dc 45 56 45 4E 	.asciz "EVENT_VBUS_OVERCURRENT"
 273      54 5F 56 42 
 273      55 53 5F 4F 
 273      56 45 52 43 
 273      55 52 52 45 
 273      4E 54 00 
 274 01f3 E8 00       	.sleb128 104
 275 01f5 05          	.uleb128 0x5
 276 01f6 45 56 45 4E 	.asciz "EVENT_VBUS_REQUEST_POWER"
 276      54 5F 56 42 
 276      55 53 5F 52 
 276      45 51 55 45 
 276      53 54 5F 50 
 276      4F 57 45 52 
 276      00 
 277 020f E9 00       	.sleb128 105
 278 0211 05          	.uleb128 0x5
 279 0212 45 56 45 4E 	.asciz "EVENT_VBUS_RELEASE_POWER"
 279      54 5F 56 42 
 279      55 53 5F 52 
 279      45 4C 45 41 
 279      53 45 5F 50 
 279      4F 57 45 52 
 279      00 
 280 022b EA 00       	.sleb128 106
 281 022d 05          	.uleb128 0x5
 282 022e 45 56 45 4E 	.asciz "EVENT_VBUS_POWER_AVAILABLE"
 282      54 5F 56 42 
 282      55 53 5F 50 
 282      4F 57 45 52 
 282      5F 41 56 41 
 282      49 4C 41 42 
 282      4C 45 00 
 283 0249 EB 00       	.sleb128 107
 284 024b 05          	.uleb128 0x5
 285 024c 45 56 45 4E 	.asciz "EVENT_UNSUPPORTED_DEVICE"
 285      54 5F 55 4E 
 285      53 55 50 50 
 285      4F 52 54 45 
 285      44 5F 44 45 
 285      56 49 43 45 
 285      00 
 286 0265 EC 00       	.sleb128 108
 287 0267 05          	.uleb128 0x5
 288 0268 45 56 45 4E 	.asciz "EVENT_CANNOT_ENUMERATE"
 288      54 5F 43 41 
 288      4E 4E 4F 54 
MPLAB XC16 ASSEMBLY Listing:   			page 8


 288      5F 45 4E 55 
 288      4D 45 52 41 
 288      54 45 00 
 289 027f ED 00       	.sleb128 109
 290 0281 05          	.uleb128 0x5
 291 0282 45 56 45 4E 	.asciz "EVENT_CLIENT_INIT_ERROR"
 291      54 5F 43 4C 
 291      49 45 4E 54 
 291      5F 49 4E 49 
 291      54 5F 45 52 
 291      52 4F 52 00 
 292 029a EE 00       	.sleb128 110
 293 029c 05          	.uleb128 0x5
 294 029d 45 56 45 4E 	.asciz "EVENT_OUT_OF_MEMORY"
 294      54 5F 4F 55 
 294      54 5F 4F 46 
 294      5F 4D 45 4D 
 294      4F 52 59 00 
 295 02b1 EF 00       	.sleb128 111
 296 02b3 05          	.uleb128 0x5
 297 02b4 45 56 45 4E 	.asciz "EVENT_UNSPECIFIED_ERROR"
 297      54 5F 55 4E 
 297      53 50 45 43 
 297      49 46 49 45 
 297      44 5F 45 52 
 297      52 4F 52 00 
 298 02cc F0 00       	.sleb128 112
 299 02ce 05          	.uleb128 0x5
 300 02cf 45 56 45 4E 	.asciz "EVENT_DETACH"
 300      54 5F 44 45 
 300      54 41 43 48 
 300      00 
 301 02dc F1 00       	.sleb128 113
 302 02de 05          	.uleb128 0x5
 303 02df 45 56 45 4E 	.asciz "EVENT_TRANSFER"
 303      54 5F 54 52 
 303      41 4E 53 46 
 303      45 52 00 
 304 02ee F2 00       	.sleb128 114
 305 02f0 05          	.uleb128 0x5
 306 02f1 45 56 45 4E 	.asciz "EVENT_SOF"
 306      54 5F 53 4F 
 306      46 00 
 307 02fb F3 00       	.sleb128 115
 308 02fd 05          	.uleb128 0x5
 309 02fe 45 56 45 4E 	.asciz "EVENT_RESUME"
 309      54 5F 52 45 
 309      53 55 4D 45 
 309      00 
 310 030b F4 00       	.sleb128 116
 311 030d 05          	.uleb128 0x5
 312 030e 45 56 45 4E 	.asciz "EVENT_SUSPEND"
 312      54 5F 53 55 
 312      53 50 45 4E 
 312      44 00 
 313 031c F5 00       	.sleb128 117
 314 031e 05          	.uleb128 0x5
MPLAB XC16 ASSEMBLY Listing:   			page 9


 315 031f 45 56 45 4E 	.asciz "EVENT_RESET"
 315      54 5F 52 45 
 315      53 45 54 00 
 316 032b F6 00       	.sleb128 118
 317 032d 05          	.uleb128 0x5
 318 032e 45 56 45 4E 	.asciz "EVENT_DATA_ISOC_READ"
 318      54 5F 44 41 
 318      54 41 5F 49 
 318      53 4F 43 5F 
 318      52 45 41 44 
 318      00 
 319 0343 F7 00       	.sleb128 119
 320 0345 05          	.uleb128 0x5
 321 0346 45 56 45 4E 	.asciz "EVENT_DATA_ISOC_WRITE"
 321      54 5F 44 41 
 321      54 41 5F 49 
 321      53 4F 43 5F 
 321      57 52 49 54 
 321      45 00 
 322 035c F8 00       	.sleb128 120
 323 035e 05          	.uleb128 0x5
 324 035f 45 56 45 4E 	.asciz "EVENT_OVERRIDE_CLIENT_DRIVER_SELECTION"
 324      54 5F 4F 56 
 324      45 52 52 49 
 324      44 45 5F 43 
 324      4C 49 45 4E 
 324      54 5F 44 52 
 324      49 56 45 52 
 324      5F 53 45 4C 
 324      45 43 54 49 
 325 0386 F9 00       	.sleb128 121
 326 0388 05          	.uleb128 0x5
 327 0389 45 56 45 4E 	.asciz "EVENT_1MS"
 327      54 5F 31 4D 
 327      53 00 
 328 0393 FA 00       	.sleb128 122
 329 0395 05          	.uleb128 0x5
 330 0396 45 56 45 4E 	.asciz "EVENT_ALT_INTERFACE"
 330      54 5F 41 4C 
 330      54 5F 49 4E 
 330      54 45 52 46 
 330      41 43 45 00 
 331 03aa FB 00       	.sleb128 123
 332 03ac 05          	.uleb128 0x5
 333 03ad 45 56 45 4E 	.asciz "EVENT_HOLD_BEFORE_CONFIGURATION"
 333      54 5F 48 4F 
 333      4C 44 5F 42 
 333      45 46 4F 52 
 333      45 5F 43 4F 
 333      4E 46 49 47 
 333      55 52 41 54 
 333      49 4F 4E 00 
 334 03cd FC 00       	.sleb128 124
 335 03cf 05          	.uleb128 0x5
 336 03d0 45 56 45 4E 	.asciz "EVENT_GENERIC_BASE"
 336      54 5F 47 45 
 336      4E 45 52 49 
MPLAB XC16 ASSEMBLY Listing:   			page 10


 336      43 5F 42 41 
 336      53 45 00 
 337 03e3 90 03       	.sleb128 400
 338 03e5 05          	.uleb128 0x5
 339 03e6 45 56 45 4E 	.asciz "EVENT_MSD_BASE"
 339      54 5F 4D 53 
 339      44 5F 42 41 
 339      53 45 00 
 340 03f5 F4 03       	.sleb128 500
 341 03f7 05          	.uleb128 0x5
 342 03f8 45 56 45 4E 	.asciz "EVENT_HID_BASE"
 342      54 5F 48 49 
 342      44 5F 42 41 
 342      53 45 00 
 343 0407 D8 04       	.sleb128 600
 344 0409 05          	.uleb128 0x5
 345 040a 45 56 45 4E 	.asciz "EVENT_PRINTER_BASE"
 345      54 5F 50 52 
 345      49 4E 54 45 
 345      52 5F 42 41 
 345      53 45 00 
 346 041d BC 05       	.sleb128 700
 347 041f 05          	.uleb128 0x5
 348 0420 45 56 45 4E 	.asciz "EVENT_CDC_BASE"
 348      54 5F 43 44 
 348      43 5F 42 41 
 348      53 45 00 
 349 042f A0 06       	.sleb128 800
 350 0431 05          	.uleb128 0x5
 351 0432 45 56 45 4E 	.asciz "EVENT_CHARGER_BASE"
 351      54 5F 43 48 
 351      41 52 47 45 
 351      52 5F 42 41 
 351      53 45 00 
 352 0445 84 07       	.sleb128 900
 353 0447 05          	.uleb128 0x5
 354 0448 45 56 45 4E 	.asciz "EVENT_AUDIO_BASE"
 354      54 5F 41 55 
 354      44 49 4F 5F 
 354      42 41 53 45 
 354      00 
 355 0459 E8 07       	.sleb128 1000
 356 045b 05          	.uleb128 0x5
 357 045c 45 56 45 4E 	.asciz "EVENT_USER_BASE"
 357      54 5F 55 53 
 357      45 52 5F 42 
 357      41 53 45 00 
 358 046c 90 CE 00    	.sleb128 10000
 359 046f 05          	.uleb128 0x5
 360 0470 45 56 45 4E 	.asciz "EVENT_BUS_ERROR"
 360      54 5F 42 55 
 360      53 5F 45 52 
 360      52 4F 52 00 
 361 0480 FF FF 01    	.sleb128 32767
 362 0483 00          	.byte 0x0
 363 0484 02          	.uleb128 0x2
 364 0485 01          	.byte 0x1
MPLAB XC16 ASSEMBLY Listing:   			page 11


 365 0486 02          	.byte 0x2
 366 0487 5F 42 6F 6F 	.asciz "_Bool"
 366      6C 00 
 367 048d 06          	.uleb128 0x6
 368 048e 5F 55 53 42 	.asciz "_USB_DEVICE_DESCRIPTOR"
 368      5F 44 45 56 
 368      49 43 45 5F 
 368      44 45 53 43 
 368      52 49 50 54 
 368      4F 52 00 
 369 04a5 12          	.byte 0x12
 370 04a6 02          	.byte 0x2
 371 04a7 45          	.byte 0x45
 372 04a8 E6 05 00 00 	.4byte 0x5e6
 373 04ac 07          	.uleb128 0x7
 374 04ad 00 00 00 00 	.4byte .LASF0
 375 04b1 02          	.byte 0x2
 376 04b2 47          	.byte 0x47
 377 04b3 E7 00 00 00 	.4byte 0xe7
 378 04b7 02          	.byte 0x2
 379 04b8 23          	.byte 0x23
 380 04b9 00          	.uleb128 0x0
 381 04ba 08          	.uleb128 0x8
 382 04bb 62 44 65 73 	.asciz "bDescriptorType"
 382      63 72 69 70 
 382      74 6F 72 54 
 382      79 70 65 00 
 383 04cb 02          	.byte 0x2
 384 04cc 48          	.byte 0x48
 385 04cd E7 00 00 00 	.4byte 0xe7
 386 04d1 02          	.byte 0x2
 387 04d2 23          	.byte 0x23
 388 04d3 01          	.uleb128 0x1
 389 04d4 08          	.uleb128 0x8
 390 04d5 62 63 64 55 	.asciz "bcdUSB"
 390      53 42 00 
 391 04dc 02          	.byte 0x2
 392 04dd 49          	.byte 0x49
 393 04de 07 01 00 00 	.4byte 0x107
 394 04e2 02          	.byte 0x2
 395 04e3 23          	.byte 0x23
 396 04e4 02          	.uleb128 0x2
 397 04e5 08          	.uleb128 0x8
 398 04e6 62 44 65 76 	.asciz "bDeviceClass"
 398      69 63 65 43 
 398      6C 61 73 73 
 398      00 
 399 04f3 02          	.byte 0x2
 400 04f4 4A          	.byte 0x4a
 401 04f5 E7 00 00 00 	.4byte 0xe7
 402 04f9 02          	.byte 0x2
 403 04fa 23          	.byte 0x23
 404 04fb 04          	.uleb128 0x4
 405 04fc 08          	.uleb128 0x8
 406 04fd 62 44 65 76 	.asciz "bDeviceSubClass"
 406      69 63 65 53 
 406      75 62 43 6C 
MPLAB XC16 ASSEMBLY Listing:   			page 12


 406      61 73 73 00 
 407 050d 02          	.byte 0x2
 408 050e 4B          	.byte 0x4b
 409 050f E7 00 00 00 	.4byte 0xe7
 410 0513 02          	.byte 0x2
 411 0514 23          	.byte 0x23
 412 0515 05          	.uleb128 0x5
 413 0516 08          	.uleb128 0x8
 414 0517 62 44 65 76 	.asciz "bDeviceProtocol"
 414      69 63 65 50 
 414      72 6F 74 6F 
 414      63 6F 6C 00 
 415 0527 02          	.byte 0x2
 416 0528 4C          	.byte 0x4c
 417 0529 E7 00 00 00 	.4byte 0xe7
 418 052d 02          	.byte 0x2
 419 052e 23          	.byte 0x23
 420 052f 06          	.uleb128 0x6
 421 0530 08          	.uleb128 0x8
 422 0531 62 4D 61 78 	.asciz "bMaxPacketSize0"
 422      50 61 63 6B 
 422      65 74 53 69 
 422      7A 65 30 00 
 423 0541 02          	.byte 0x2
 424 0542 4D          	.byte 0x4d
 425 0543 E7 00 00 00 	.4byte 0xe7
 426 0547 02          	.byte 0x2
 427 0548 23          	.byte 0x23
 428 0549 07          	.uleb128 0x7
 429 054a 08          	.uleb128 0x8
 430 054b 69 64 56 65 	.asciz "idVendor"
 430      6E 64 6F 72 
 430      00 
 431 0554 02          	.byte 0x2
 432 0555 4E          	.byte 0x4e
 433 0556 07 01 00 00 	.4byte 0x107
 434 055a 02          	.byte 0x2
 435 055b 23          	.byte 0x23
 436 055c 08          	.uleb128 0x8
 437 055d 08          	.uleb128 0x8
 438 055e 69 64 50 72 	.asciz "idProduct"
 438      6F 64 75 63 
 438      74 00 
 439 0568 02          	.byte 0x2
 440 0569 4F          	.byte 0x4f
 441 056a 07 01 00 00 	.4byte 0x107
 442 056e 02          	.byte 0x2
 443 056f 23          	.byte 0x23
 444 0570 0A          	.uleb128 0xa
 445 0571 08          	.uleb128 0x8
 446 0572 62 63 64 44 	.asciz "bcdDevice"
 446      65 76 69 63 
 446      65 00 
 447 057c 02          	.byte 0x2
 448 057d 50          	.byte 0x50
 449 057e 07 01 00 00 	.4byte 0x107
 450 0582 02          	.byte 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 13


 451 0583 23          	.byte 0x23
 452 0584 0C          	.uleb128 0xc
 453 0585 08          	.uleb128 0x8
 454 0586 69 4D 61 6E 	.asciz "iManufacturer"
 454      75 66 61 63 
 454      74 75 72 65 
 454      72 00 
 455 0594 02          	.byte 0x2
 456 0595 51          	.byte 0x51
 457 0596 E7 00 00 00 	.4byte 0xe7
 458 059a 02          	.byte 0x2
 459 059b 23          	.byte 0x23
 460 059c 0E          	.uleb128 0xe
 461 059d 08          	.uleb128 0x8
 462 059e 69 50 72 6F 	.asciz "iProduct"
 462      64 75 63 74 
 462      00 
 463 05a7 02          	.byte 0x2
 464 05a8 52          	.byte 0x52
 465 05a9 E7 00 00 00 	.4byte 0xe7
 466 05ad 02          	.byte 0x2
 467 05ae 23          	.byte 0x23
 468 05af 0F          	.uleb128 0xf
 469 05b0 08          	.uleb128 0x8
 470 05b1 69 53 65 72 	.asciz "iSerialNumber"
 470      69 61 6C 4E 
 470      75 6D 62 65 
 470      72 00 
 471 05bf 02          	.byte 0x2
 472 05c0 53          	.byte 0x53
 473 05c1 E7 00 00 00 	.4byte 0xe7
 474 05c5 02          	.byte 0x2
 475 05c6 23          	.byte 0x23
 476 05c7 10          	.uleb128 0x10
 477 05c8 08          	.uleb128 0x8
 478 05c9 62 4E 75 6D 	.asciz "bNumConfigurations"
 478      43 6F 6E 66 
 478      69 67 75 72 
 478      61 74 69 6F 
 478      6E 73 00 
 479 05dc 02          	.byte 0x2
 480 05dd 54          	.byte 0x54
 481 05de E7 00 00 00 	.4byte 0xe7
 482 05e2 02          	.byte 0x2
 483 05e3 23          	.byte 0x23
 484 05e4 11          	.uleb128 0x11
 485 05e5 00          	.byte 0x0
 486 05e6 03          	.uleb128 0x3
 487 05e7 55 53 42 5F 	.asciz "USB_DEVICE_DESCRIPTOR"
 487      44 45 56 49 
 487      43 45 5F 44 
 487      45 53 43 52 
 487      49 50 54 4F 
 487      52 00 
 488 05fd 02          	.byte 0x2
 489 05fe 55          	.byte 0x55
 490 05ff 8D 04 00 00 	.4byte 0x48d
MPLAB XC16 ASSEMBLY Listing:   			page 14


 491 0603 09          	.uleb128 0x9
 492 0604 02          	.byte 0x2
 493 0605 09 06 00 00 	.4byte 0x609
 494 0609 0A          	.uleb128 0xa
 495 060a E7 00 00 00 	.4byte 0xe7
 496 060e 02          	.uleb128 0x2
 497 060f 02          	.byte 0x2
 498 0610 07          	.byte 0x7
 499 0611 73 68 6F 72 	.asciz "short unsigned int"
 499      74 20 75 6E 
 499      73 69 67 6E 
 499      65 64 20 69 
 499      6E 74 00 
 500 0624 0B          	.uleb128 0xb
 501 0625 04          	.byte 0x4
 502 0626 03          	.byte 0x3
 503 0627 02 01       	.2byte 0x102
 504 0629 5B 06 00 00 	.4byte 0x65b
 505 062d 0C          	.uleb128 0xc
 506 062e 00 00 00 00 	.4byte .LASF0
 507 0632 03          	.byte 0x3
 508 0633 02 01       	.2byte 0x102
 509 0635 E7 00 00 00 	.4byte 0xe7
 510 0639 02          	.byte 0x2
 511 063a 23          	.byte 0x23
 512 063b 00          	.uleb128 0x0
 513 063c 0C          	.uleb128 0xc
 514 063d 00 00 00 00 	.4byte .LASF1
 515 0641 03          	.byte 0x3
 516 0642 02 01       	.2byte 0x102
 517 0644 E7 00 00 00 	.4byte 0xe7
 518 0648 02          	.byte 0x2
 519 0649 23          	.byte 0x23
 520 064a 01          	.uleb128 0x1
 521 064b 0C          	.uleb128 0xc
 522 064c 00 00 00 00 	.4byte .LASF2
 523 0650 03          	.byte 0x3
 524 0651 02 01       	.2byte 0x102
 525 0653 5B 06 00 00 	.4byte 0x65b
 526 0657 02          	.byte 0x2
 527 0658 23          	.byte 0x23
 528 0659 02          	.uleb128 0x2
 529 065a 00          	.byte 0x0
 530 065b 0D          	.uleb128 0xd
 531 065c 07 01 00 00 	.4byte 0x107
 532 0660 6B 06 00 00 	.4byte 0x66b
 533 0664 0E          	.uleb128 0xe
 534 0665 17 01 00 00 	.4byte 0x117
 535 0669 00          	.byte 0x0
 536 066a 00          	.byte 0x0
 537 066b 0B          	.uleb128 0xb
 538 066c 34          	.byte 0x34
 539 066d 03          	.byte 0x3
 540 066e 06 01       	.2byte 0x106
 541 0670 A2 06 00 00 	.4byte 0x6a2
 542 0674 0C          	.uleb128 0xc
 543 0675 00 00 00 00 	.4byte .LASF0
MPLAB XC16 ASSEMBLY Listing:   			page 15


 544 0679 03          	.byte 0x3
 545 067a 06 01       	.2byte 0x106
 546 067c E7 00 00 00 	.4byte 0xe7
 547 0680 02          	.byte 0x2
 548 0681 23          	.byte 0x23
 549 0682 00          	.uleb128 0x0
 550 0683 0C          	.uleb128 0xc
 551 0684 00 00 00 00 	.4byte .LASF1
 552 0688 03          	.byte 0x3
 553 0689 06 01       	.2byte 0x106
 554 068b E7 00 00 00 	.4byte 0xe7
 555 068f 02          	.byte 0x2
 556 0690 23          	.byte 0x23
 557 0691 01          	.uleb128 0x1
 558 0692 0C          	.uleb128 0xc
 559 0693 00 00 00 00 	.4byte .LASF2
 560 0697 03          	.byte 0x3
 561 0698 06 01       	.2byte 0x106
 562 069a A2 06 00 00 	.4byte 0x6a2
 563 069e 02          	.byte 0x2
 564 069f 23          	.byte 0x23
 565 06a0 02          	.uleb128 0x2
 566 06a1 00          	.byte 0x0
 567 06a2 0D          	.uleb128 0xd
 568 06a3 07 01 00 00 	.4byte 0x107
 569 06a7 B2 06 00 00 	.4byte 0x6b2
 570 06ab 0E          	.uleb128 0xe
 571 06ac 17 01 00 00 	.4byte 0x117
 572 06b0 18          	.byte 0x18
 573 06b1 00          	.byte 0x0
 574 06b2 0B          	.uleb128 0xb
 575 06b3 34          	.byte 0x34
 576 06b4 03          	.byte 0x3
 577 06b5 0D 01       	.2byte 0x10d
 578 06b7 E9 06 00 00 	.4byte 0x6e9
 579 06bb 0C          	.uleb128 0xc
 580 06bc 00 00 00 00 	.4byte .LASF0
 581 06c0 03          	.byte 0x3
 582 06c1 0D 01       	.2byte 0x10d
 583 06c3 E7 00 00 00 	.4byte 0xe7
 584 06c7 02          	.byte 0x2
 585 06c8 23          	.byte 0x23
 586 06c9 00          	.uleb128 0x0
 587 06ca 0C          	.uleb128 0xc
 588 06cb 00 00 00 00 	.4byte .LASF1
 589 06cf 03          	.byte 0x3
 590 06d0 0D 01       	.2byte 0x10d
 591 06d2 E7 00 00 00 	.4byte 0xe7
 592 06d6 02          	.byte 0x2
 593 06d7 23          	.byte 0x23
 594 06d8 01          	.uleb128 0x1
 595 06d9 0C          	.uleb128 0xc
 596 06da 00 00 00 00 	.4byte .LASF2
 597 06de 03          	.byte 0x3
 598 06df 0D 01       	.2byte 0x10d
 599 06e1 A2 06 00 00 	.4byte 0x6a2
 600 06e5 02          	.byte 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 16


 601 06e6 23          	.byte 0x23
 602 06e7 02          	.uleb128 0x2
 603 06e8 00          	.byte 0x0
 604 06e9 0D          	.uleb128 0xd
 605 06ea E7 00 00 00 	.4byte 0xe7
 606 06ee F9 06 00 00 	.4byte 0x6f9
 607 06f2 0E          	.uleb128 0xe
 608 06f3 17 01 00 00 	.4byte 0x117
 609 06f7 42          	.byte 0x42
 610 06f8 00          	.byte 0x0
 611 06f9 0F          	.uleb128 0xf
 612 06fa 00 00 00 00 	.4byte .LASF3
 613 06fe 04          	.byte 0x4
 614 06ff 90 03       	.2byte 0x390
 615 0701 07 07 00 00 	.4byte 0x707
 616 0705 01          	.byte 0x1
 617 0706 01          	.byte 0x1
 618 0707 0A          	.uleb128 0xa
 619 0708 E9 06 00 00 	.4byte 0x6e9
 620 070c 10          	.uleb128 0x10
 621 070d 73 64 30 30 	.asciz "sd000"
 621      30 00 
 622 0713 03          	.byte 0x3
 623 0714 02 01       	.2byte 0x102
 624 0716 1C 07 00 00 	.4byte 0x71c
 625 071a 01          	.byte 0x1
 626 071b 01          	.byte 0x1
 627 071c 0A          	.uleb128 0xa
 628 071d 24 06 00 00 	.4byte 0x624
 629 0721 10          	.uleb128 0x10
 630 0722 73 64 30 30 	.asciz "sd001"
 630      31 00 
 631 0728 03          	.byte 0x3
 632 0729 06 01       	.2byte 0x106
 633 072b 31 07 00 00 	.4byte 0x731
 634 072f 01          	.byte 0x1
 635 0730 01          	.byte 0x1
 636 0731 0A          	.uleb128 0xa
 637 0732 6B 06 00 00 	.4byte 0x66b
 638 0736 10          	.uleb128 0x10
 639 0737 73 64 30 30 	.asciz "sd002"
 639      32 00 
 640 073d 03          	.byte 0x3
 641 073e 0D 01       	.2byte 0x10d
 642 0740 46 07 00 00 	.4byte 0x746
 643 0744 01          	.byte 0x1
 644 0745 01          	.byte 0x1
 645 0746 0A          	.uleb128 0xa
 646 0747 B2 06 00 00 	.4byte 0x6b2
 647 074b 11          	.uleb128 0x11
 648 074c 00 00 00 00 	.4byte .LASF3
 649 0750 03          	.byte 0x3
 650 0751 AC          	.byte 0xac
 651 0752 5D 07 00 00 	.4byte 0x75d
 652 0756 01          	.byte 0x1
 653 0757 05          	.byte 0x5
 654 0758 03          	.byte 0x3
MPLAB XC16 ASSEMBLY Listing:   			page 17


 655 0759 00 00 00 00 	.4byte _configDescriptor1
 656 075d 0A          	.uleb128 0xa
 657 075e E9 06 00 00 	.4byte 0x6e9
 658 0762 12          	.uleb128 0x12
 659 0763 64 65 76 69 	.asciz "device_dsc"
 659      63 65 5F 64 
 659      73 63 00 
 660 076e 03          	.byte 0x3
 661 076f 99          	.byte 0x99
 662 0770 7B 07 00 00 	.4byte 0x77b
 663 0774 01          	.byte 0x1
 664 0775 05          	.byte 0x5
 665 0776 03          	.byte 0x3
 666 0777 00 00 00 00 	.4byte _device_dsc
 667 077b 0A          	.uleb128 0xa
 668 077c E6 05 00 00 	.4byte 0x5e6
 669 0780 13          	.uleb128 0x13
 670 0781 73 64 30 30 	.asciz "sd000"
 670      30 00 
 671 0787 03          	.byte 0x3
 672 0788 02 01       	.2byte 0x102
 673 078a 1C 07 00 00 	.4byte 0x71c
 674 078e 01          	.byte 0x1
 675 078f 05          	.byte 0x5
 676 0790 03          	.byte 0x3
 677 0791 00 00 00 00 	.4byte _sd000
 678 0795 13          	.uleb128 0x13
 679 0796 73 64 30 30 	.asciz "sd001"
 679      31 00 
 680 079c 03          	.byte 0x3
 681 079d 06 01       	.2byte 0x106
 682 079f 31 07 00 00 	.4byte 0x731
 683 07a3 01          	.byte 0x1
 684 07a4 05          	.byte 0x5
 685 07a5 03          	.byte 0x3
 686 07a6 00 00 00 00 	.4byte _sd001
 687 07aa 13          	.uleb128 0x13
 688 07ab 73 64 30 30 	.asciz "sd002"
 688      32 00 
 689 07b1 03          	.byte 0x3
 690 07b2 0D 01       	.2byte 0x10d
 691 07b4 46 07 00 00 	.4byte 0x746
 692 07b8 01          	.byte 0x1
 693 07b9 05          	.byte 0x5
 694 07ba 03          	.byte 0x3
 695 07bb 00 00 00 00 	.4byte _sd002
 696 07bf 0D          	.uleb128 0xd
 697 07c0 03 06 00 00 	.4byte 0x603
 698 07c4 CF 07 00 00 	.4byte 0x7cf
 699 07c8 0E          	.uleb128 0xe
 700 07c9 17 01 00 00 	.4byte 0x117
 701 07cd 00          	.byte 0x0
 702 07ce 00          	.byte 0x0
 703 07cf 13          	.uleb128 0x13
 704 07d0 55 53 42 5F 	.asciz "USB_CD_Ptr"
 704      43 44 5F 50 
 704      74 72 00 
MPLAB XC16 ASSEMBLY Listing:   			page 18


 705 07db 03          	.byte 0x3
 706 07dc 1F 01       	.2byte 0x11f
 707 07de E9 07 00 00 	.4byte 0x7e9
 708 07e2 01          	.byte 0x1
 709 07e3 05          	.byte 0x5
 710 07e4 03          	.byte 0x3
 711 07e5 00 00 00 00 	.4byte _USB_CD_Ptr
 712 07e9 0A          	.uleb128 0xa
 713 07ea BF 07 00 00 	.4byte 0x7bf
 714 07ee 0D          	.uleb128 0xd
 715 07ef 03 06 00 00 	.4byte 0x603
 716 07f3 FE 07 00 00 	.4byte 0x7fe
 717 07f7 0E          	.uleb128 0xe
 718 07f8 17 01 00 00 	.4byte 0x117
 719 07fc 02          	.byte 0x2
 720 07fd 00          	.byte 0x0
 721 07fe 13          	.uleb128 0x13
 722 07ff 55 53 42 5F 	.asciz "USB_SD_Ptr"
 722      53 44 5F 50 
 722      74 72 00 
 723 080a 03          	.byte 0x3
 724 080b 24 01       	.2byte 0x124
 725 080d 18 08 00 00 	.4byte 0x818
 726 0811 01          	.byte 0x1
 727 0812 05          	.byte 0x5
 728 0813 03          	.byte 0x3
 729 0814 00 00 00 00 	.4byte _USB_SD_Ptr
 730 0818 0A          	.uleb128 0xa
 731 0819 EE 07 00 00 	.4byte 0x7ee
 732 081d 00          	.byte 0x0
 733                 	.section .debug_abbrev,info
 734 0000 01          	.uleb128 0x1
 735 0001 11          	.uleb128 0x11
 736 0002 01          	.byte 0x1
 737 0003 25          	.uleb128 0x25
 738 0004 08          	.uleb128 0x8
 739 0005 13          	.uleb128 0x13
 740 0006 0B          	.uleb128 0xb
 741 0007 03          	.uleb128 0x3
 742 0008 08          	.uleb128 0x8
 743 0009 1B          	.uleb128 0x1b
 744 000a 08          	.uleb128 0x8
 745 000b 11          	.uleb128 0x11
 746 000c 01          	.uleb128 0x1
 747 000d 12          	.uleb128 0x12
 748 000e 01          	.uleb128 0x1
 749 000f 10          	.uleb128 0x10
 750 0010 06          	.uleb128 0x6
 751 0011 00          	.byte 0x0
 752 0012 00          	.byte 0x0
 753 0013 02          	.uleb128 0x2
 754 0014 24          	.uleb128 0x24
 755 0015 00          	.byte 0x0
 756 0016 0B          	.uleb128 0xb
 757 0017 0B          	.uleb128 0xb
 758 0018 3E          	.uleb128 0x3e
 759 0019 0B          	.uleb128 0xb
MPLAB XC16 ASSEMBLY Listing:   			page 19


 760 001a 03          	.uleb128 0x3
 761 001b 08          	.uleb128 0x8
 762 001c 00          	.byte 0x0
 763 001d 00          	.byte 0x0
 764 001e 03          	.uleb128 0x3
 765 001f 16          	.uleb128 0x16
 766 0020 00          	.byte 0x0
 767 0021 03          	.uleb128 0x3
 768 0022 08          	.uleb128 0x8
 769 0023 3A          	.uleb128 0x3a
 770 0024 0B          	.uleb128 0xb
 771 0025 3B          	.uleb128 0x3b
 772 0026 0B          	.uleb128 0xb
 773 0027 49          	.uleb128 0x49
 774 0028 13          	.uleb128 0x13
 775 0029 00          	.byte 0x0
 776 002a 00          	.byte 0x0
 777 002b 04          	.uleb128 0x4
 778 002c 04          	.uleb128 0x4
 779 002d 01          	.byte 0x1
 780 002e 0B          	.uleb128 0xb
 781 002f 0B          	.uleb128 0xb
 782 0030 3A          	.uleb128 0x3a
 783 0031 0B          	.uleb128 0xb
 784 0032 3B          	.uleb128 0x3b
 785 0033 0B          	.uleb128 0xb
 786 0034 01          	.uleb128 0x1
 787 0035 13          	.uleb128 0x13
 788 0036 00          	.byte 0x0
 789 0037 00          	.byte 0x0
 790 0038 05          	.uleb128 0x5
 791 0039 28          	.uleb128 0x28
 792 003a 00          	.byte 0x0
 793 003b 03          	.uleb128 0x3
 794 003c 08          	.uleb128 0x8
 795 003d 1C          	.uleb128 0x1c
 796 003e 0D          	.uleb128 0xd
 797 003f 00          	.byte 0x0
 798 0040 00          	.byte 0x0
 799 0041 06          	.uleb128 0x6
 800 0042 13          	.uleb128 0x13
 801 0043 01          	.byte 0x1
 802 0044 03          	.uleb128 0x3
 803 0045 08          	.uleb128 0x8
 804 0046 0B          	.uleb128 0xb
 805 0047 0B          	.uleb128 0xb
 806 0048 3A          	.uleb128 0x3a
 807 0049 0B          	.uleb128 0xb
 808 004a 3B          	.uleb128 0x3b
 809 004b 0B          	.uleb128 0xb
 810 004c 01          	.uleb128 0x1
 811 004d 13          	.uleb128 0x13
 812 004e 00          	.byte 0x0
 813 004f 00          	.byte 0x0
 814 0050 07          	.uleb128 0x7
 815 0051 0D          	.uleb128 0xd
 816 0052 00          	.byte 0x0
MPLAB XC16 ASSEMBLY Listing:   			page 20


 817 0053 03          	.uleb128 0x3
 818 0054 0E          	.uleb128 0xe
 819 0055 3A          	.uleb128 0x3a
 820 0056 0B          	.uleb128 0xb
 821 0057 3B          	.uleb128 0x3b
 822 0058 0B          	.uleb128 0xb
 823 0059 49          	.uleb128 0x49
 824 005a 13          	.uleb128 0x13
 825 005b 38          	.uleb128 0x38
 826 005c 0A          	.uleb128 0xa
 827 005d 00          	.byte 0x0
 828 005e 00          	.byte 0x0
 829 005f 08          	.uleb128 0x8
 830 0060 0D          	.uleb128 0xd
 831 0061 00          	.byte 0x0
 832 0062 03          	.uleb128 0x3
 833 0063 08          	.uleb128 0x8
 834 0064 3A          	.uleb128 0x3a
 835 0065 0B          	.uleb128 0xb
 836 0066 3B          	.uleb128 0x3b
 837 0067 0B          	.uleb128 0xb
 838 0068 49          	.uleb128 0x49
 839 0069 13          	.uleb128 0x13
 840 006a 38          	.uleb128 0x38
 841 006b 0A          	.uleb128 0xa
 842 006c 00          	.byte 0x0
 843 006d 00          	.byte 0x0
 844 006e 09          	.uleb128 0x9
 845 006f 0F          	.uleb128 0xf
 846 0070 00          	.byte 0x0
 847 0071 0B          	.uleb128 0xb
 848 0072 0B          	.uleb128 0xb
 849 0073 49          	.uleb128 0x49
 850 0074 13          	.uleb128 0x13
 851 0075 00          	.byte 0x0
 852 0076 00          	.byte 0x0
 853 0077 0A          	.uleb128 0xa
 854 0078 26          	.uleb128 0x26
 855 0079 00          	.byte 0x0
 856 007a 49          	.uleb128 0x49
 857 007b 13          	.uleb128 0x13
 858 007c 00          	.byte 0x0
 859 007d 00          	.byte 0x0
 860 007e 0B          	.uleb128 0xb
 861 007f 13          	.uleb128 0x13
 862 0080 01          	.byte 0x1
 863 0081 0B          	.uleb128 0xb
 864 0082 0B          	.uleb128 0xb
 865 0083 3A          	.uleb128 0x3a
 866 0084 0B          	.uleb128 0xb
 867 0085 3B          	.uleb128 0x3b
 868 0086 05          	.uleb128 0x5
 869 0087 01          	.uleb128 0x1
 870 0088 13          	.uleb128 0x13
 871 0089 00          	.byte 0x0
 872 008a 00          	.byte 0x0
 873 008b 0C          	.uleb128 0xc
MPLAB XC16 ASSEMBLY Listing:   			page 21


 874 008c 0D          	.uleb128 0xd
 875 008d 00          	.byte 0x0
 876 008e 03          	.uleb128 0x3
 877 008f 0E          	.uleb128 0xe
 878 0090 3A          	.uleb128 0x3a
 879 0091 0B          	.uleb128 0xb
 880 0092 3B          	.uleb128 0x3b
 881 0093 05          	.uleb128 0x5
 882 0094 49          	.uleb128 0x49
 883 0095 13          	.uleb128 0x13
 884 0096 38          	.uleb128 0x38
 885 0097 0A          	.uleb128 0xa
 886 0098 00          	.byte 0x0
 887 0099 00          	.byte 0x0
 888 009a 0D          	.uleb128 0xd
 889 009b 01          	.uleb128 0x1
 890 009c 01          	.byte 0x1
 891 009d 49          	.uleb128 0x49
 892 009e 13          	.uleb128 0x13
 893 009f 01          	.uleb128 0x1
 894 00a0 13          	.uleb128 0x13
 895 00a1 00          	.byte 0x0
 896 00a2 00          	.byte 0x0
 897 00a3 0E          	.uleb128 0xe
 898 00a4 21          	.uleb128 0x21
 899 00a5 00          	.byte 0x0
 900 00a6 49          	.uleb128 0x49
 901 00a7 13          	.uleb128 0x13
 902 00a8 2F          	.uleb128 0x2f
 903 00a9 0B          	.uleb128 0xb
 904 00aa 00          	.byte 0x0
 905 00ab 00          	.byte 0x0
 906 00ac 0F          	.uleb128 0xf
 907 00ad 34          	.uleb128 0x34
 908 00ae 00          	.byte 0x0
 909 00af 03          	.uleb128 0x3
 910 00b0 0E          	.uleb128 0xe
 911 00b1 3A          	.uleb128 0x3a
 912 00b2 0B          	.uleb128 0xb
 913 00b3 3B          	.uleb128 0x3b
 914 00b4 05          	.uleb128 0x5
 915 00b5 49          	.uleb128 0x49
 916 00b6 13          	.uleb128 0x13
 917 00b7 3F          	.uleb128 0x3f
 918 00b8 0C          	.uleb128 0xc
 919 00b9 3C          	.uleb128 0x3c
 920 00ba 0C          	.uleb128 0xc
 921 00bb 00          	.byte 0x0
 922 00bc 00          	.byte 0x0
 923 00bd 10          	.uleb128 0x10
 924 00be 34          	.uleb128 0x34
 925 00bf 00          	.byte 0x0
 926 00c0 03          	.uleb128 0x3
 927 00c1 08          	.uleb128 0x8
 928 00c2 3A          	.uleb128 0x3a
 929 00c3 0B          	.uleb128 0xb
 930 00c4 3B          	.uleb128 0x3b
MPLAB XC16 ASSEMBLY Listing:   			page 22


 931 00c5 05          	.uleb128 0x5
 932 00c6 49          	.uleb128 0x49
 933 00c7 13          	.uleb128 0x13
 934 00c8 3F          	.uleb128 0x3f
 935 00c9 0C          	.uleb128 0xc
 936 00ca 3C          	.uleb128 0x3c
 937 00cb 0C          	.uleb128 0xc
 938 00cc 00          	.byte 0x0
 939 00cd 00          	.byte 0x0
 940 00ce 11          	.uleb128 0x11
 941 00cf 34          	.uleb128 0x34
 942 00d0 00          	.byte 0x0
 943 00d1 03          	.uleb128 0x3
 944 00d2 0E          	.uleb128 0xe
 945 00d3 3A          	.uleb128 0x3a
 946 00d4 0B          	.uleb128 0xb
 947 00d5 3B          	.uleb128 0x3b
 948 00d6 0B          	.uleb128 0xb
 949 00d7 49          	.uleb128 0x49
 950 00d8 13          	.uleb128 0x13
 951 00d9 3F          	.uleb128 0x3f
 952 00da 0C          	.uleb128 0xc
 953 00db 02          	.uleb128 0x2
 954 00dc 0A          	.uleb128 0xa
 955 00dd 00          	.byte 0x0
 956 00de 00          	.byte 0x0
 957 00df 12          	.uleb128 0x12
 958 00e0 34          	.uleb128 0x34
 959 00e1 00          	.byte 0x0
 960 00e2 03          	.uleb128 0x3
 961 00e3 08          	.uleb128 0x8
 962 00e4 3A          	.uleb128 0x3a
 963 00e5 0B          	.uleb128 0xb
 964 00e6 3B          	.uleb128 0x3b
 965 00e7 0B          	.uleb128 0xb
 966 00e8 49          	.uleb128 0x49
 967 00e9 13          	.uleb128 0x13
 968 00ea 3F          	.uleb128 0x3f
 969 00eb 0C          	.uleb128 0xc
 970 00ec 02          	.uleb128 0x2
 971 00ed 0A          	.uleb128 0xa
 972 00ee 00          	.byte 0x0
 973 00ef 00          	.byte 0x0
 974 00f0 13          	.uleb128 0x13
 975 00f1 34          	.uleb128 0x34
 976 00f2 00          	.byte 0x0
 977 00f3 03          	.uleb128 0x3
 978 00f4 08          	.uleb128 0x8
 979 00f5 3A          	.uleb128 0x3a
 980 00f6 0B          	.uleb128 0xb
 981 00f7 3B          	.uleb128 0x3b
 982 00f8 05          	.uleb128 0x5
 983 00f9 49          	.uleb128 0x49
 984 00fa 13          	.uleb128 0x13
 985 00fb 3F          	.uleb128 0x3f
 986 00fc 0C          	.uleb128 0xc
 987 00fd 02          	.uleb128 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 23


 988 00fe 0A          	.uleb128 0xa
 989 00ff 00          	.byte 0x0
 990 0100 00          	.byte 0x0
 991 0101 00          	.byte 0x0
 992                 	.section .debug_pubnames,info
 993 0000 6F 00 00 00 	.4byte 0x6f
 994 0004 02 00       	.2byte 0x2
 995 0006 00 00 00 00 	.4byte .Ldebug_info0
 996 000a 1E 08 00 00 	.4byte 0x81e
 997 000e 4B 07 00 00 	.4byte 0x74b
 998 0012 63 6F 6E 66 	.asciz "configDescriptor1"
 998      69 67 44 65 
 998      73 63 72 69 
 998      70 74 6F 72 
 998      31 00 
 999 0024 62 07 00 00 	.4byte 0x762
 1000 0028 64 65 76 69 	.asciz "device_dsc"
 1000      63 65 5F 64 
 1000      73 63 00 
 1001 0033 80 07 00 00 	.4byte 0x780
 1002 0037 73 64 30 30 	.asciz "sd000"
 1002      30 00 
 1003 003d 95 07 00 00 	.4byte 0x795
 1004 0041 73 64 30 30 	.asciz "sd001"
 1004      31 00 
 1005 0047 AA 07 00 00 	.4byte 0x7aa
 1006 004b 73 64 30 30 	.asciz "sd002"
 1006      32 00 
 1007 0051 CF 07 00 00 	.4byte 0x7cf
 1008 0055 55 53 42 5F 	.asciz "USB_CD_Ptr"
 1008      43 44 5F 50 
 1008      74 72 00 
 1009 0060 FE 07 00 00 	.4byte 0x7fe
 1010 0064 55 53 42 5F 	.asciz "USB_SD_Ptr"
 1010      53 44 5F 50 
 1010      74 72 00 
 1011 006f 00 00 00 00 	.4byte 0x0
 1012                 	.section .debug_pubtypes,info
 1013 0000 5C 00 00 00 	.4byte 0x5c
 1014 0004 02 00       	.2byte 0x2
 1015 0006 00 00 00 00 	.4byte .Ldebug_info0
 1016 000a 1E 08 00 00 	.4byte 0x81e
 1017 000e E7 00 00 00 	.4byte 0xe7
 1018 0012 75 69 6E 74 	.asciz "uint8_t"
 1018      38 5F 74 00 
 1019 001a 07 01 00 00 	.4byte 0x107
 1020 001e 75 69 6E 74 	.asciz "uint16_t"
 1020      31 36 5F 74 
 1020      00 
 1021 0027 8D 04 00 00 	.4byte 0x48d
 1022 002b 5F 55 53 42 	.asciz "_USB_DEVICE_DESCRIPTOR"
 1022      5F 44 45 56 
 1022      49 43 45 5F 
 1022      44 45 53 43 
 1022      52 49 50 54 
 1022      4F 52 00 
 1023 0042 E6 05 00 00 	.4byte 0x5e6
MPLAB XC16 ASSEMBLY Listing:   			page 24


 1024 0046 55 53 42 5F 	.asciz "USB_DEVICE_DESCRIPTOR"
 1024      44 45 56 49 
 1024      43 45 5F 44 
 1024      45 53 43 52 
 1024      49 50 54 4F 
 1024      52 00 
 1025 005c 00 00 00 00 	.4byte 0x0
 1026                 	.section .debug_str,info
 1027                 	.LASF1:
 1028 0000 62 44 73 63 	.asciz "bDscType"
 1028      54 79 70 65 
 1028      00 
 1029                 	.LASF0:
 1030 0009 62 4C 65 6E 	.asciz "bLength"
 1030      67 74 68 00 
 1031                 	.LASF3:
 1032 0011 63 6F 6E 66 	.asciz "configDescriptor1"
 1032      69 67 44 65 
 1032      73 63 72 69 
 1032      70 74 6F 72 
 1032      31 00 
 1033                 	.LASF2:
 1034 0023 73 74 72 69 	.asciz "string"
 1034      6E 67 00 
 1035                 	.section .text,code
 1036              	
 1037              	
 1038              	
 1039              	.section __c30_info,info,bss
 1040                 	__psv_trap_errata:
 1041                 	
 1042                 	.section __c30_signature,info,data
 1043 0000 01 00       	.word 0x0001
 1044 0002 00 00       	.word 0x0000
 1045 0004 00 00       	.word 0x0000
 1046                 	
 1047                 	
 1048                 	
 1049                 	.set ___PA___,0
 1050                 	.end
MPLAB XC16 ASSEMBLY Listing:   			page 25


DEFINED SYMBOLS
                            *ABS*:00000000 /home/user/Documents/FST/Programming/project_sub-zero/lib/lib_pic33e/usb/usb_descriptors.c
                            *ABS*:00000001 __33EP256MU806
                            *ABS*:00000001 __dsPIC33E
                            *ABS*:00000001 __HAS_DSP
                            *ABS*:00000001 __HAS_DMAV2
                            *ABS*:00000001 __HAS_CODEGUARD
                            *ABS*:00000001 __HAS_PMP_ENHANCED
                            *ABS*:00000001 __HAS_EDS
                            *ABS*:00000001 __HAS_EP
                            *ABS*:00000011 __TARGET_DIVIDE_CYCLES
                            *ABS*:00000001 __C30ELF
    {standard input}:15     .const:00000000 _device_dsc
                            *ABS*:00000008 __ext_attr_.const
    {standard input}:33     .const:00000012 _configDescriptor1
    {standard input}:105    .const:00000056 _sd000
    {standard input}:113    .const:0000005a _sd001
    {standard input}:145    .const:0000008e _sd002
    {standard input}:177    .const:000000c2 _USB_CD_Ptr
    {standard input}:183    .const:000000c4 _USB_SD_Ptr
    {standard input}:1040   __c30_info:00000000 __psv_trap_errata
    {standard input}:1049   *ABS*:00000000 ___PA___
                    .debug_abbrev:00000000 .Ldebug_abbrev0
                            .text:00000000 .Ltext0
                            .text:00000000 .Letext0
                      .debug_line:00000000 .Ldebug_line0
                       .debug_str:00000009 .LASF0
                       .debug_str:00000000 .LASF1
                       .debug_str:00000023 .LASF2
                       .debug_str:00000011 .LASF3
                      .debug_info:00000000 .Ldebug_info0

NO UNDEFINED SYMBOLS

EQUATE SYMBOLS
/home/user/Documents/FST/Programming/project_sub-zero/lib/lib_pic33e/usb/usb_descriptors.c = 0x0
                  __33EP256MU806 = 0x1
                      __dsPIC33E = 0x1
                       __HAS_DSP = 0x1
                     __HAS_DMAV2 = 0x1
                 __HAS_CODEGUARD = 0x1
              __HAS_PMP_ENHANCED = 0x1
                       __HAS_EDS = 0x1
                        __HAS_EP = 0x1
          __TARGET_DIVIDE_CYCLES = 0x11
                        __C30ELF = 0x1
               __ext_attr_.const = 0x8
                        ___PA___ = 0x0
