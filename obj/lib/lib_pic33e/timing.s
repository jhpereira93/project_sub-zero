MPLAB XC16 ASSEMBLY Listing:   			page 1


   1              	.file "/home/user/Documents/FST/Programming/project_sub-zero/lib/lib_pic33e/timing.c"
   2              	.section .debug_abbrev,info
   3                 	.Ldebug_abbrev0:
   4                 	.section .debug_info,info
   5                 	.Ldebug_info0:
   6                 	.section .debug_line,info
   7                 	.Ldebug_line0:
   8 0000 02 01 00 00 	.section .text,code
   8      02 00 BC 00 
   8      00 00 01 01 
   8      FB 0E 0A 00 
   8      01 01 01 01 
   8      00 00 00 01 
   8      6C 69 62 2F 
   8      6C 69 62 5F 
   8      70 69 63 33 
   9              	.Ltext0:
  10              	
  11              	.pushsection .user_init,code,keep
  12 000000  00 00 07 	rcall _config_clock
  13              	.popsection
  14              	
  15              	.section .text,code
  16              	.align 2
  17              	.global _config_clock
  18              	.type _config_clock,@function
  19              	_config_clock:
  20              	.LFB0:
  21              	.file 1 "lib/lib_pic33e/timing.c"
   1:lib/lib_pic33e/timing.c **** #include <xc.h>
   2:lib/lib_pic33e/timing.c **** #include "lib_pic33e/timing.h"
   3:lib/lib_pic33e/timing.c **** 
   4:lib/lib_pic33e/timing.c **** // Configure PLL. See dsPIC33 Family Reference section 7. Oscillator,
   5:lib/lib_pic33e/timing.c **** // subsection 7.0 Phase-Locked Loop (PLL).
   6:lib/lib_pic33e/timing.c **** void __attribute__((user_init)) config_clock(void)
   7:lib/lib_pic33e/timing.c **** {
  22              	.loc 1 7 0
  23              	.set ___PA___,1
  24 000000  00 00 FA 	lnk #0
  25              	.LCFI0:
   8:lib/lib_pic33e/timing.c **** 	/*
   9:lib/lib_pic33e/timing.c **** 	 * FIN is external primary oscillator.
  10:lib/lib_pic33e/timing.c **** 	 * FPPLIN = FIN / N1, must be in [0.8, 8] MHz.
  11:lib/lib_pic33e/timing.c **** 	 * FVCO = FPPLIN * M, must be in [120, 340] MHz.
  12:lib/lib_pic33e/timing.c **** 	 * FOSC = FVCO / N2, must be in [15, 140] MHz (max. temperature 85C).
  13:lib/lib_pic33e/timing.c **** 	 * Overall:
  14:lib/lib_pic33e/timing.c **** 	 * FOSC = FIN * M/(N1*N2) =
  15:lib/lib_pic33e/timing.c **** 	 * = FIN * (PLLFDB + 2)/((PPLPRE + 2) * 2(PLLPOST + 1))
  16:lib/lib_pic33e/timing.c **** 	 */
  17:lib/lib_pic33e/timing.c **** 	#if FCY64 // 64 MHz
  18:lib/lib_pic33e/timing.c **** 		CLKDIVbits.PLLPRE = 2; // N1 = 4
  26              	.loc 1 18 0
  27 000002  02 00 80 	mov _CLKDIVbits,w2
  28 000004  00 FE 2F 	mov #-32,w0
  19:lib/lib_pic33e/timing.c **** 		PLLFBD = 62; // M = 64
  29              	.loc 1 19 0
  30 000006  E1 03 20 	mov #62,w1
MPLAB XC16 ASSEMBLY Listing:   			page 2


  31              	.loc 1 18 0
  32 000008  00 00 61 	and w2,w0,w0
  20:lib/lib_pic33e/timing.c **** 		CLKDIVbits.PLLPOST = 0b00; // N2 = 2
  33              	.loc 1 20 0
  34 00000a  F3 F3 2F 	mov #-193,w3
  35              	.loc 1 18 0
  36 00000c  00 10 A0 	bset w0,#1
  21:lib/lib_pic33e/timing.c **** 		// Overall PLL: X8 = 64/(4*2)
  22:lib/lib_pic33e/timing.c **** 	#elif FCY70 // 70 MHz
  23:lib/lib_pic33e/timing.c **** 		CLKDIVbits.PLLPRE = 2; // N1 = 4
  24:lib/lib_pic33e/timing.c **** 		PLLFBD = 68; // M = 70
  25:lib/lib_pic33e/timing.c **** 		CLKDIVbits.PLLPOST = 0b00; // N2 = 2
  26:lib/lib_pic33e/timing.c **** 		// Overall PLL: X8.75 = 70/(4*2)
  27:lib/lib_pic33e/timing.c **** 	#elif FAST // 106 MHz
  28:lib/lib_pic33e/timing.c **** 		// Gotta go fast
  29:lib/lib_pic33e/timing.c **** 		CLKDIVbits.PLLPRE = 2; // N1 = 4
  30:lib/lib_pic33e/timing.c **** 		PLLFBD = 104; // M = 106
  31:lib/lib_pic33e/timing.c **** 		CLKDIVbits.PLLPOST = 0b00; // N2 = 2
  32:lib/lib_pic33e/timing.c **** 		// Overall PLL: X13.25 = 106/(4*2)
  33:lib/lib_pic33e/timing.c **** 	#else // DEFAULT: 16 MHz
  34:lib/lib_pic33e/timing.c **** 		CLKDIVbits.PLLPRE = 2; // N1 = 4
  35:lib/lib_pic33e/timing.c **** 		PLLFBD = 30; // M = 32
  36:lib/lib_pic33e/timing.c **** 		CLKDIVbits.PLLPOST = 0b01; // N2 = 4
  37:lib/lib_pic33e/timing.c **** 		// Overall PLL: X2 = 32/(4*4)
  38:lib/lib_pic33e/timing.c **** 	#endif
  39:lib/lib_pic33e/timing.c **** 
  40:lib/lib_pic33e/timing.c **** 	// Initiate Clock Switch to Primary Oscillator with PLL (NOSC=0b011)
  41:lib/lib_pic33e/timing.c **** 	__builtin_write_OSCCONH(0b011);
  37              	.loc 1 41 0
  38 00000e  32 00 20 	mov #3,w2
  39              	.loc 1 18 0
  40 000010  00 00 88 	mov w0,_CLKDIVbits
  41              	.loc 1 41 0
  42 000012  80 07 20 	mov #120,w0
  43              	.loc 1 19 0
  44 000014  01 00 88 	mov w1,_PLLFBD
  45              	.loc 1 41 0
  46 000016  A1 09 20 	mov #154,w1
  47              	.loc 1 20 0
  48 000018  04 00 80 	mov _CLKDIVbits,w4
  49 00001a  83 01 62 	and w4,w3,w3
  50 00001c  03 00 88 	mov w3,_CLKDIVbits
  51              	.loc 1 41 0
  52 00001e  13 00 20 	mov #_OSCCON+1,w3
  53 000020  80 49 78 	mov.b w0,[w3]
  54 000022  81 49 78 	mov.b w1,[w3]
  55 000024  82 49 78 	mov.b w2,[w3]
  42:lib/lib_pic33e/timing.c **** 	__builtin_write_OSCCONL(OSCCONL | 0b1);
  56              	.loc 1 42 0
  57 000026  01 00 20 	mov #_OSCCONL,w1
  58 000028  00 00 00 	nop
  59 00002a  91 40 78 	mov.b [w1],w1
  60 00002c  60 04 20 	mov #70,w0
  61 00002e  01 41 78 	mov.b w1,w2
  62 000030  02 04 A0 	bset.b w2,#0
  63 000032  71 05 20 	mov #87,w1
  64 000034  02 81 FB 	ze w2,w2
MPLAB XC16 ASSEMBLY Listing:   			page 3


  65 000036  03 00 20 	mov #_OSCCON,w3
  66 000038  80 49 78 	mov.b w0,[w3]
  67 00003a  81 49 78 	mov.b w1,[w3]
  68 00003c  82 49 78 	mov.b w2,[w3]
  69              	.L2:
  43:lib/lib_pic33e/timing.c **** 
  44:lib/lib_pic33e/timing.c **** 	// Wait for Clock switch to occur
  45:lib/lib_pic33e/timing.c **** 	while (OSCCONbits.COSC != 0b011);
  70              	.loc 1 45 0
  71 00003e  02 00 80 	mov _OSCCONbits,w2
  72 000040  01 00 27 	mov #28672,w1
  73 000042  00 00 23 	mov #12288,w0
  74 000044  81 00 61 	and w2,w1,w1
  75 000046  80 8F 50 	sub w1,w0,[w15]
  76              	.set ___BP___,0
  77 000048  00 00 3A 	bra nz,.L2
  78              	.L3:
  46:lib/lib_pic33e/timing.c **** 
  47:lib/lib_pic33e/timing.c **** 	// Wait for PLL to lock
  48:lib/lib_pic33e/timing.c **** 	while (OSCCONbits.LOCK != 1);
  79              	.loc 1 48 0
  80 00004a  01 00 80 	mov _OSCCONbits,w1
  81 00004c  00 02 20 	mov #32,w0
  82 00004e  00 80 60 	and w1,w0,w0
  83 000050  00 00 E0 	cp0 w0
  84              	.set ___BP___,0
  85 000052  00 00 32 	bra z,.L3
  49:lib/lib_pic33e/timing.c **** 
  50:lib/lib_pic33e/timing.c ****     //USB Clock configuration assuming 16MHz crystal
  51:lib/lib_pic33e/timing.c ****     
  52:lib/lib_pic33e/timing.c ****     /*Configure the auxilliary PLL to provide 48MHz needed for USB Operation.*/
  53:lib/lib_pic33e/timing.c ****     
  54:lib/lib_pic33e/timing.c ****     // Configuring the auxiliary PLL, since the primary
  55:lib/lib_pic33e/timing.c ****     // oscillator provides the source clock to the auxiliary
  56:lib/lib_pic33e/timing.c ****     // PLL, the auxiliary oscillator is disabled. Note that
  57:lib/lib_pic33e/timing.c ****     // the AUX PLL is enabled. The input 16MHz clock is divided
  58:lib/lib_pic33e/timing.c ****     // by 2, multiplied by 24 and then divided by 4. Wait till
  59:lib/lib_pic33e/timing.c ****     // the AUX PLL locks.
  60:lib/lib_pic33e/timing.c ****     
  61:lib/lib_pic33e/timing.c ****     ACLKCON3bits.ENAPLL = 0; // APLL is disabled, the USB clock source is the input clock to the AP
  86              	.loc 1 61 0
  87 000054  01 E0 A9 	bclr.b _ACLKCON3bits+1,#7
  62:lib/lib_pic33e/timing.c ****     ACLKCON3bits.SELACLK = 1; // Auxiliary PLL or oscillators provide the source clock for the Auxi
  63:lib/lib_pic33e/timing.c ****     ACLKCON3bits.AOSCMD = 0b00; // (AOSC) Auxiliary Oscillator is disabled (default)
  88              	.loc 1 63 0
  89 000056  F0 7F 2E 	mov #-6145,w0
  90              	.loc 1 62 0
  91 000058  01 A0 A8 	bset.b _ACLKCON3bits+1,#5
  64:lib/lib_pic33e/timing.c ****     ACLKCON3bits.ASRCSEL = 1; // Primary Oscillator is the clock source for APLL
  65:lib/lib_pic33e/timing.c ****     ACLKCON3bits.FRCSEL = 0; // Auxiliary Oscillator or Primary Oscillator is the clock source for 
  66:lib/lib_pic33e/timing.c ****     ACLKCON3bits.APLLPOST = 0b110; // Divided by 2
  92              	.loc 1 66 0
  93 00005a  F2 F1 2F 	mov #-225,w2
  94              	.loc 1 63 0
  95 00005c  03 00 80 	mov _ACLKCON3bits,w3
  96              	.loc 1 66 0
  97 00005e  01 0C 20 	mov #192,w1
MPLAB XC16 ASSEMBLY Listing:   			page 4


  98              	.loc 1 63 0
  99 000060  80 81 61 	and w3,w0,w3
  67:lib/lib_pic33e/timing.c ****     ACLKCON3bits.APLLPRE = 0b011; // Divided by 4
 100              	.loc 1 67 0
 101 000062  80 FF 2F 	mov #-8,w0
 102              	.loc 1 63 0
 103 000064  03 00 88 	mov w3,_ACLKCON3bits
 104              	.loc 1 64 0
 105 000066  01 40 A8 	bset.b _ACLKCON3bits+1,#2
 106              	.loc 1 65 0
 107 000068  01 20 A9 	bclr.b _ACLKCON3bits+1,#1
 108              	.loc 1 66 0
 109 00006a  03 00 80 	mov _ACLKCON3bits,w3
 110 00006c  02 81 61 	and w3,w2,w2
 111 00006e  82 80 70 	ior w1,w2,w1
 112 000070  01 00 88 	mov w1,_ACLKCON3bits
 113              	.loc 1 67 0
 114 000072  01 00 80 	mov _ACLKCON3bits,w1
 115 000074  00 80 60 	and w1,w0,w0
 116 000076  30 00 B3 	ior #3,w0
 117 000078  00 00 88 	mov w0,_ACLKCON3bits
  68:lib/lib_pic33e/timing.c ****     
  69:lib/lib_pic33e/timing.c ****     ACLKDIV3bits.APLLDIV = 0b111;
 118              	.loc 1 69 0
 119 00007a  00 00 80 	mov _ACLKDIV3bits,w0
 120 00007c  70 00 B3 	ior #7,w0
 121 00007e  00 00 88 	mov w0,_ACLKDIV3bits
  70:lib/lib_pic33e/timing.c ****     
  71:lib/lib_pic33e/timing.c ****     ACLKCON3bits.ENAPLL = 1;
 122              	.loc 1 71 0
 123 000080  01 E0 A8 	bset.b _ACLKCON3bits+1,#7
 124              	.L4:
  72:lib/lib_pic33e/timing.c ****     while(ACLKCON3bits.APLLCK != 1);
 125              	.loc 1 72 0
 126 000082  01 00 80 	mov _ACLKCON3bits,w1
 127 000084  00 00 24 	mov #16384,w0
 128 000086  00 80 60 	and w1,w0,w0
 129 000088  00 00 E0 	cp0 w0
 130              	.set ___BP___,0
 131 00008a  00 00 32 	bra z,.L4
  73:lib/lib_pic33e/timing.c **** }
 132              	.loc 1 73 0
 133 00008c  8E 07 78 	mov w14,w15
 134 00008e  4F 07 78 	mov [--w15],w14
 135 000090  00 40 A9 	bclr CORCON,#2
 136 000092  00 00 06 	return 
 137              	.set ___PA___,0
 138              	.LFE0:
 139              	.size _config_clock,.-_config_clock
 140              	.section .debug_frame,info
 141                 	.Lframe0:
 142 0000 10 00 00 00 	.4byte .LECIE0-.LSCIE0
 143                 	.LSCIE0:
 144 0004 FF FF FF FF 	.4byte 0xffffffff
 145 0008 01          	.byte 0x1
 146 0009 00          	.byte 0
 147 000a 01          	.uleb128 0x1
MPLAB XC16 ASSEMBLY Listing:   			page 5


 148 000b 02          	.sleb128 2
 149 000c 25          	.byte 0x25
 150 000d 12          	.byte 0x12
 151 000e 0F          	.uleb128 0xf
 152 000f 7E          	.sleb128 -2
 153 0010 09          	.byte 0x9
 154 0011 25          	.uleb128 0x25
 155 0012 0F          	.uleb128 0xf
 156 0013 00          	.align 4
 157                 	.LECIE0:
 158                 	.LSFDE0:
 159 0014 18 00 00 00 	.4byte .LEFDE0-.LASFDE0
 160                 	.LASFDE0:
 161 0018 00 00 00 00 	.4byte .Lframe0
 162 001c 00 00 00 00 	.4byte .LFB0
 163 0020 94 00 00 00 	.4byte .LFE0-.LFB0
 164 0024 04          	.byte 0x4
 165 0025 02 00 00 00 	.4byte .LCFI0-.LFB0
 166 0029 13          	.byte 0x13
 167 002a 7D          	.sleb128 -3
 168 002b 0D          	.byte 0xd
 169 002c 0E          	.uleb128 0xe
 170 002d 8E          	.byte 0x8e
 171 002e 02          	.uleb128 0x2
 172 002f 00          	.align 4
 173                 	.LEFDE0:
 174                 	.section .text,code
 175              	.Letext0:
 176              	.file 2 "/opt/microchip/xc16/v1.36/bin/bin/../../include/lega-c/stdint.h"
 177              	.file 3 "/opt/microchip/xc16/v1.36/bin/bin/../../support/dsPIC33E/h/p33EP256MU806.h"
 178              	.section .debug_info,info
 179 0000 15 08 00 00 	.4byte 0x815
 180 0004 02 00       	.2byte 0x2
 181 0006 00 00 00 00 	.4byte .Ldebug_abbrev0
 182 000a 04          	.byte 0x4
 183 000b 01          	.uleb128 0x1
 184 000c 47 4E 55 20 	.asciz "GNU C 4.5.1 (XC16, Microchip v1.36) (A) Build date: Dec  5 2018"
 184      43 20 34 2E 
 184      35 2E 31 20 
 184      28 58 43 31 
 184      36 2C 20 4D 
 184      69 63 72 6F 
 184      63 68 69 70 
 184      20 76 31 2E 
 184      33 36 29 20 
 185 004c 01          	.byte 0x1
 186 004d 6C 69 62 2F 	.asciz "lib/lib_pic33e/timing.c"
 186      6C 69 62 5F 
 186      70 69 63 33 
 186      33 65 2F 74 
 186      69 6D 69 6E 
 186      67 2E 63 00 
 187 0065 2F 68 6F 6D 	.asciz "/home/user/Documents/FST/Programming/project_sub-zero"
 187      65 2F 75 73 
 187      65 72 2F 44 
 187      6F 63 75 6D 
 187      65 6E 74 73 
MPLAB XC16 ASSEMBLY Listing:   			page 6


 187      2F 46 53 54 
 187      2F 50 72 6F 
 187      67 72 61 6D 
 187      6D 69 6E 67 
 188 009b 00 00 00 00 	.4byte .Ltext0
 189 009f 00 00 00 00 	.4byte .Letext0
 190 00a3 00 00 00 00 	.4byte .Ldebug_line0
 191 00a7 02          	.uleb128 0x2
 192 00a8 01          	.byte 0x1
 193 00a9 06          	.byte 0x6
 194 00aa 73 69 67 6E 	.asciz "signed char"
 194      65 64 20 63 
 194      68 61 72 00 
 195 00b6 02          	.uleb128 0x2
 196 00b7 02          	.byte 0x2
 197 00b8 05          	.byte 0x5
 198 00b9 69 6E 74 00 	.asciz "int"
 199 00bd 02          	.uleb128 0x2
 200 00be 04          	.byte 0x4
 201 00bf 05          	.byte 0x5
 202 00c0 6C 6F 6E 67 	.asciz "long int"
 202      20 69 6E 74 
 202      00 
 203 00c9 02          	.uleb128 0x2
 204 00ca 08          	.byte 0x8
 205 00cb 05          	.byte 0x5
 206 00cc 6C 6F 6E 67 	.asciz "long long int"
 206      20 6C 6F 6E 
 206      67 20 69 6E 
 206      74 00 
 207 00da 03          	.uleb128 0x3
 208 00db 75 69 6E 74 	.asciz "uint8_t"
 208      38 5F 74 00 
 209 00e3 02          	.byte 0x2
 210 00e4 2B          	.byte 0x2b
 211 00e5 E9 00 00 00 	.4byte 0xe9
 212 00e9 02          	.uleb128 0x2
 213 00ea 01          	.byte 0x1
 214 00eb 08          	.byte 0x8
 215 00ec 75 6E 73 69 	.asciz "unsigned char"
 215      67 6E 65 64 
 215      20 63 68 61 
 215      72 00 
 216 00fa 03          	.uleb128 0x3
 217 00fb 75 69 6E 74 	.asciz "uint16_t"
 217      31 36 5F 74 
 217      00 
 218 0104 02          	.byte 0x2
 219 0105 31          	.byte 0x31
 220 0106 0A 01 00 00 	.4byte 0x10a
 221 010a 02          	.uleb128 0x2
 222 010b 02          	.byte 0x2
 223 010c 07          	.byte 0x7
 224 010d 75 6E 73 69 	.asciz "unsigned int"
 224      67 6E 65 64 
 224      20 69 6E 74 
 224      00 
MPLAB XC16 ASSEMBLY Listing:   			page 7


 225 011a 02          	.uleb128 0x2
 226 011b 04          	.byte 0x4
 227 011c 07          	.byte 0x7
 228 011d 6C 6F 6E 67 	.asciz "long unsigned int"
 228      20 75 6E 73 
 228      69 67 6E 65 
 228      64 20 69 6E 
 228      74 00 
 229 012f 02          	.uleb128 0x2
 230 0130 08          	.byte 0x8
 231 0131 07          	.byte 0x7
 232 0132 6C 6F 6E 67 	.asciz "long long unsigned int"
 232      20 6C 6F 6E 
 232      67 20 75 6E 
 232      73 69 67 6E 
 232      65 64 20 69 
 232      6E 74 00 
 233 0149 04          	.uleb128 0x4
 234 014a 02          	.byte 0x2
 235 014b 03          	.byte 0x3
 236 014c C2 23       	.2byte 0x23c2
 237 014e F2 01 00 00 	.4byte 0x1f2
 238 0152 05          	.uleb128 0x5
 239 0153 4F 53 57 45 	.asciz "OSWEN"
 239      4E 00 
 240 0159 03          	.byte 0x3
 241 015a C3 23       	.2byte 0x23c3
 242 015c FA 00 00 00 	.4byte 0xfa
 243 0160 02          	.byte 0x2
 244 0161 01          	.byte 0x1
 245 0162 0F          	.byte 0xf
 246 0163 02          	.byte 0x2
 247 0164 23          	.byte 0x23
 248 0165 00          	.uleb128 0x0
 249 0166 05          	.uleb128 0x5
 250 0167 4C 50 4F 53 	.asciz "LPOSCEN"
 250      43 45 4E 00 
 251 016f 03          	.byte 0x3
 252 0170 C4 23       	.2byte 0x23c4
 253 0172 FA 00 00 00 	.4byte 0xfa
 254 0176 02          	.byte 0x2
 255 0177 01          	.byte 0x1
 256 0178 0E          	.byte 0xe
 257 0179 02          	.byte 0x2
 258 017a 23          	.byte 0x23
 259 017b 00          	.uleb128 0x0
 260 017c 05          	.uleb128 0x5
 261 017d 43 46 00    	.asciz "CF"
 262 0180 03          	.byte 0x3
 263 0181 C6 23       	.2byte 0x23c6
 264 0183 FA 00 00 00 	.4byte 0xfa
 265 0187 02          	.byte 0x2
 266 0188 01          	.byte 0x1
 267 0189 0C          	.byte 0xc
 268 018a 02          	.byte 0x2
 269 018b 23          	.byte 0x23
 270 018c 00          	.uleb128 0x0
MPLAB XC16 ASSEMBLY Listing:   			page 8


 271 018d 05          	.uleb128 0x5
 272 018e 4C 4F 43 4B 	.asciz "LOCK"
 272      00 
 273 0193 03          	.byte 0x3
 274 0194 C8 23       	.2byte 0x23c8
 275 0196 FA 00 00 00 	.4byte 0xfa
 276 019a 02          	.byte 0x2
 277 019b 01          	.byte 0x1
 278 019c 0A          	.byte 0xa
 279 019d 02          	.byte 0x2
 280 019e 23          	.byte 0x23
 281 019f 00          	.uleb128 0x0
 282 01a0 05          	.uleb128 0x5
 283 01a1 49 4F 4C 4F 	.asciz "IOLOCK"
 283      43 4B 00 
 284 01a8 03          	.byte 0x3
 285 01a9 C9 23       	.2byte 0x23c9
 286 01ab FA 00 00 00 	.4byte 0xfa
 287 01af 02          	.byte 0x2
 288 01b0 01          	.byte 0x1
 289 01b1 09          	.byte 0x9
 290 01b2 02          	.byte 0x2
 291 01b3 23          	.byte 0x23
 292 01b4 00          	.uleb128 0x0
 293 01b5 05          	.uleb128 0x5
 294 01b6 43 4C 4B 4C 	.asciz "CLKLOCK"
 294      4F 43 4B 00 
 295 01be 03          	.byte 0x3
 296 01bf CA 23       	.2byte 0x23ca
 297 01c1 FA 00 00 00 	.4byte 0xfa
 298 01c5 02          	.byte 0x2
 299 01c6 01          	.byte 0x1
 300 01c7 08          	.byte 0x8
 301 01c8 02          	.byte 0x2
 302 01c9 23          	.byte 0x23
 303 01ca 00          	.uleb128 0x0
 304 01cb 05          	.uleb128 0x5
 305 01cc 4E 4F 53 43 	.asciz "NOSC"
 305      00 
 306 01d1 03          	.byte 0x3
 307 01d2 CB 23       	.2byte 0x23cb
 308 01d4 FA 00 00 00 	.4byte 0xfa
 309 01d8 02          	.byte 0x2
 310 01d9 03          	.byte 0x3
 311 01da 05          	.byte 0x5
 312 01db 02          	.byte 0x2
 313 01dc 23          	.byte 0x23
 314 01dd 00          	.uleb128 0x0
 315 01de 05          	.uleb128 0x5
 316 01df 43 4F 53 43 	.asciz "COSC"
 316      00 
 317 01e4 03          	.byte 0x3
 318 01e5 CD 23       	.2byte 0x23cd
 319 01e7 FA 00 00 00 	.4byte 0xfa
 320 01eb 02          	.byte 0x2
 321 01ec 03          	.byte 0x3
 322 01ed 01          	.byte 0x1
MPLAB XC16 ASSEMBLY Listing:   			page 9


 323 01ee 02          	.byte 0x2
 324 01ef 23          	.byte 0x23
 325 01f0 00          	.uleb128 0x0
 326 01f1 00          	.byte 0x0
 327 01f2 04          	.uleb128 0x4
 328 01f3 02          	.byte 0x2
 329 01f4 03          	.byte 0x3
 330 01f5 CF 23       	.2byte 0x23cf
 331 01f7 74 02 00 00 	.4byte 0x274
 332 01fb 05          	.uleb128 0x5
 333 01fc 4E 4F 53 43 	.asciz "NOSC0"
 333      30 00 
 334 0202 03          	.byte 0x3
 335 0203 D1 23       	.2byte 0x23d1
 336 0205 FA 00 00 00 	.4byte 0xfa
 337 0209 02          	.byte 0x2
 338 020a 01          	.byte 0x1
 339 020b 07          	.byte 0x7
 340 020c 02          	.byte 0x2
 341 020d 23          	.byte 0x23
 342 020e 00          	.uleb128 0x0
 343 020f 05          	.uleb128 0x5
 344 0210 4E 4F 53 43 	.asciz "NOSC1"
 344      31 00 
 345 0216 03          	.byte 0x3
 346 0217 D2 23       	.2byte 0x23d2
 347 0219 FA 00 00 00 	.4byte 0xfa
 348 021d 02          	.byte 0x2
 349 021e 01          	.byte 0x1
 350 021f 06          	.byte 0x6
 351 0220 02          	.byte 0x2
 352 0221 23          	.byte 0x23
 353 0222 00          	.uleb128 0x0
 354 0223 05          	.uleb128 0x5
 355 0224 4E 4F 53 43 	.asciz "NOSC2"
 355      32 00 
 356 022a 03          	.byte 0x3
 357 022b D3 23       	.2byte 0x23d3
 358 022d FA 00 00 00 	.4byte 0xfa
 359 0231 02          	.byte 0x2
 360 0232 01          	.byte 0x1
 361 0233 05          	.byte 0x5
 362 0234 02          	.byte 0x2
 363 0235 23          	.byte 0x23
 364 0236 00          	.uleb128 0x0
 365 0237 05          	.uleb128 0x5
 366 0238 43 4F 53 43 	.asciz "COSC0"
 366      30 00 
 367 023e 03          	.byte 0x3
 368 023f D5 23       	.2byte 0x23d5
 369 0241 FA 00 00 00 	.4byte 0xfa
 370 0245 02          	.byte 0x2
 371 0246 01          	.byte 0x1
 372 0247 03          	.byte 0x3
 373 0248 02          	.byte 0x2
 374 0249 23          	.byte 0x23
 375 024a 00          	.uleb128 0x0
MPLAB XC16 ASSEMBLY Listing:   			page 10


 376 024b 05          	.uleb128 0x5
 377 024c 43 4F 53 43 	.asciz "COSC1"
 377      31 00 
 378 0252 03          	.byte 0x3
 379 0253 D6 23       	.2byte 0x23d6
 380 0255 FA 00 00 00 	.4byte 0xfa
 381 0259 02          	.byte 0x2
 382 025a 01          	.byte 0x1
 383 025b 02          	.byte 0x2
 384 025c 02          	.byte 0x2
 385 025d 23          	.byte 0x23
 386 025e 00          	.uleb128 0x0
 387 025f 05          	.uleb128 0x5
 388 0260 43 4F 53 43 	.asciz "COSC2"
 388      32 00 
 389 0266 03          	.byte 0x3
 390 0267 D7 23       	.2byte 0x23d7
 391 0269 FA 00 00 00 	.4byte 0xfa
 392 026d 02          	.byte 0x2
 393 026e 01          	.byte 0x1
 394 026f 01          	.byte 0x1
 395 0270 02          	.byte 0x2
 396 0271 23          	.byte 0x23
 397 0272 00          	.uleb128 0x0
 398 0273 00          	.byte 0x0
 399 0274 06          	.uleb128 0x6
 400 0275 02          	.byte 0x2
 401 0276 03          	.byte 0x3
 402 0277 C1 23       	.2byte 0x23c1
 403 0279 88 02 00 00 	.4byte 0x288
 404 027d 07          	.uleb128 0x7
 405 027e 49 01 00 00 	.4byte 0x149
 406 0282 07          	.uleb128 0x7
 407 0283 F2 01 00 00 	.4byte 0x1f2
 408 0287 00          	.byte 0x0
 409 0288 08          	.uleb128 0x8
 410 0289 74 61 67 4F 	.asciz "tagOSCCONBITS"
 410      53 43 43 4F 
 410      4E 42 49 54 
 410      53 00 
 411 0297 02          	.byte 0x2
 412 0298 03          	.byte 0x3
 413 0299 C0 23       	.2byte 0x23c0
 414 029b A8 02 00 00 	.4byte 0x2a8
 415 029f 09          	.uleb128 0x9
 416 02a0 74 02 00 00 	.4byte 0x274
 417 02a4 02          	.byte 0x2
 418 02a5 23          	.byte 0x23
 419 02a6 00          	.uleb128 0x0
 420 02a7 00          	.byte 0x0
 421 02a8 0A          	.uleb128 0xa
 422 02a9 4F 53 43 43 	.asciz "OSCCONBITS"
 422      4F 4E 42 49 
 422      54 53 00 
 423 02b4 03          	.byte 0x3
 424 02b5 DA 23       	.2byte 0x23da
 425 02b7 88 02 00 00 	.4byte 0x288
MPLAB XC16 ASSEMBLY Listing:   			page 11


 426 02bb 04          	.uleb128 0x4
 427 02bc 02          	.byte 0x2
 428 02bd 03          	.byte 0x3
 429 02be E5 23       	.2byte 0x23e5
 430 02c0 3E 03 00 00 	.4byte 0x33e
 431 02c4 05          	.uleb128 0x5
 432 02c5 50 4C 4C 50 	.asciz "PLLPRE"
 432      52 45 00 
 433 02cc 03          	.byte 0x3
 434 02cd E6 23       	.2byte 0x23e6
 435 02cf FA 00 00 00 	.4byte 0xfa
 436 02d3 02          	.byte 0x2
 437 02d4 05          	.byte 0x5
 438 02d5 0B          	.byte 0xb
 439 02d6 02          	.byte 0x2
 440 02d7 23          	.byte 0x23
 441 02d8 00          	.uleb128 0x0
 442 02d9 05          	.uleb128 0x5
 443 02da 50 4C 4C 50 	.asciz "PLLPOST"
 443      4F 53 54 00 
 444 02e2 03          	.byte 0x3
 445 02e3 E8 23       	.2byte 0x23e8
 446 02e5 FA 00 00 00 	.4byte 0xfa
 447 02e9 02          	.byte 0x2
 448 02ea 02          	.byte 0x2
 449 02eb 08          	.byte 0x8
 450 02ec 02          	.byte 0x2
 451 02ed 23          	.byte 0x23
 452 02ee 00          	.uleb128 0x0
 453 02ef 05          	.uleb128 0x5
 454 02f0 46 52 43 44 	.asciz "FRCDIV"
 454      49 56 00 
 455 02f7 03          	.byte 0x3
 456 02f8 E9 23       	.2byte 0x23e9
 457 02fa FA 00 00 00 	.4byte 0xfa
 458 02fe 02          	.byte 0x2
 459 02ff 03          	.byte 0x3
 460 0300 05          	.byte 0x5
 461 0301 02          	.byte 0x2
 462 0302 23          	.byte 0x23
 463 0303 00          	.uleb128 0x0
 464 0304 05          	.uleb128 0x5
 465 0305 44 4F 5A 45 	.asciz "DOZEN"
 465      4E 00 
 466 030b 03          	.byte 0x3
 467 030c EA 23       	.2byte 0x23ea
 468 030e FA 00 00 00 	.4byte 0xfa
 469 0312 02          	.byte 0x2
 470 0313 01          	.byte 0x1
 471 0314 04          	.byte 0x4
 472 0315 02          	.byte 0x2
 473 0316 23          	.byte 0x23
 474 0317 00          	.uleb128 0x0
 475 0318 05          	.uleb128 0x5
 476 0319 44 4F 5A 45 	.asciz "DOZE"
 476      00 
 477 031e 03          	.byte 0x3
MPLAB XC16 ASSEMBLY Listing:   			page 12


 478 031f EB 23       	.2byte 0x23eb
 479 0321 FA 00 00 00 	.4byte 0xfa
 480 0325 02          	.byte 0x2
 481 0326 03          	.byte 0x3
 482 0327 01          	.byte 0x1
 483 0328 02          	.byte 0x2
 484 0329 23          	.byte 0x23
 485 032a 00          	.uleb128 0x0
 486 032b 05          	.uleb128 0x5
 487 032c 52 4F 49 00 	.asciz "ROI"
 488 0330 03          	.byte 0x3
 489 0331 EC 23       	.2byte 0x23ec
 490 0333 FA 00 00 00 	.4byte 0xfa
 491 0337 02          	.byte 0x2
 492 0338 01          	.byte 0x1
 493 0339 10          	.byte 0x10
 494 033a 02          	.byte 0x2
 495 033b 23          	.byte 0x23
 496 033c 00          	.uleb128 0x0
 497 033d 00          	.byte 0x0
 498 033e 04          	.uleb128 0x4
 499 033f 02          	.byte 0x2
 500 0340 03          	.byte 0x3
 501 0341 EE 23       	.2byte 0x23ee
 502 0343 62 04 00 00 	.4byte 0x462
 503 0347 05          	.uleb128 0x5
 504 0348 50 4C 4C 50 	.asciz "PLLPRE0"
 504      52 45 30 00 
 505 0350 03          	.byte 0x3
 506 0351 EF 23       	.2byte 0x23ef
 507 0353 FA 00 00 00 	.4byte 0xfa
 508 0357 02          	.byte 0x2
 509 0358 01          	.byte 0x1
 510 0359 0F          	.byte 0xf
 511 035a 02          	.byte 0x2
 512 035b 23          	.byte 0x23
 513 035c 00          	.uleb128 0x0
 514 035d 05          	.uleb128 0x5
 515 035e 50 4C 4C 50 	.asciz "PLLPRE1"
 515      52 45 31 00 
 516 0366 03          	.byte 0x3
 517 0367 F0 23       	.2byte 0x23f0
 518 0369 FA 00 00 00 	.4byte 0xfa
 519 036d 02          	.byte 0x2
 520 036e 01          	.byte 0x1
 521 036f 0E          	.byte 0xe
 522 0370 02          	.byte 0x2
 523 0371 23          	.byte 0x23
 524 0372 00          	.uleb128 0x0
 525 0373 05          	.uleb128 0x5
 526 0374 50 4C 4C 50 	.asciz "PLLPRE2"
 526      52 45 32 00 
 527 037c 03          	.byte 0x3
 528 037d F1 23       	.2byte 0x23f1
 529 037f FA 00 00 00 	.4byte 0xfa
 530 0383 02          	.byte 0x2
 531 0384 01          	.byte 0x1
MPLAB XC16 ASSEMBLY Listing:   			page 13


 532 0385 0D          	.byte 0xd
 533 0386 02          	.byte 0x2
 534 0387 23          	.byte 0x23
 535 0388 00          	.uleb128 0x0
 536 0389 05          	.uleb128 0x5
 537 038a 50 4C 4C 50 	.asciz "PLLPRE3"
 537      52 45 33 00 
 538 0392 03          	.byte 0x3
 539 0393 F2 23       	.2byte 0x23f2
 540 0395 FA 00 00 00 	.4byte 0xfa
 541 0399 02          	.byte 0x2
 542 039a 01          	.byte 0x1
 543 039b 0C          	.byte 0xc
 544 039c 02          	.byte 0x2
 545 039d 23          	.byte 0x23
 546 039e 00          	.uleb128 0x0
 547 039f 05          	.uleb128 0x5
 548 03a0 50 4C 4C 50 	.asciz "PLLPRE4"
 548      52 45 34 00 
 549 03a8 03          	.byte 0x3
 550 03a9 F3 23       	.2byte 0x23f3
 551 03ab FA 00 00 00 	.4byte 0xfa
 552 03af 02          	.byte 0x2
 553 03b0 01          	.byte 0x1
 554 03b1 0B          	.byte 0xb
 555 03b2 02          	.byte 0x2
 556 03b3 23          	.byte 0x23
 557 03b4 00          	.uleb128 0x0
 558 03b5 05          	.uleb128 0x5
 559 03b6 50 4C 4C 50 	.asciz "PLLPOST0"
 559      4F 53 54 30 
 559      00 
 560 03bf 03          	.byte 0x3
 561 03c0 F5 23       	.2byte 0x23f5
 562 03c2 FA 00 00 00 	.4byte 0xfa
 563 03c6 02          	.byte 0x2
 564 03c7 01          	.byte 0x1
 565 03c8 09          	.byte 0x9
 566 03c9 02          	.byte 0x2
 567 03ca 23          	.byte 0x23
 568 03cb 00          	.uleb128 0x0
 569 03cc 05          	.uleb128 0x5
 570 03cd 50 4C 4C 50 	.asciz "PLLPOST1"
 570      4F 53 54 31 
 570      00 
 571 03d6 03          	.byte 0x3
 572 03d7 F6 23       	.2byte 0x23f6
 573 03d9 FA 00 00 00 	.4byte 0xfa
 574 03dd 02          	.byte 0x2
 575 03de 01          	.byte 0x1
 576 03df 08          	.byte 0x8
 577 03e0 02          	.byte 0x2
 578 03e1 23          	.byte 0x23
 579 03e2 00          	.uleb128 0x0
 580 03e3 05          	.uleb128 0x5
 581 03e4 46 52 43 44 	.asciz "FRCDIV0"
 581      49 56 30 00 
MPLAB XC16 ASSEMBLY Listing:   			page 14


 582 03ec 03          	.byte 0x3
 583 03ed F7 23       	.2byte 0x23f7
 584 03ef FA 00 00 00 	.4byte 0xfa
 585 03f3 02          	.byte 0x2
 586 03f4 01          	.byte 0x1
 587 03f5 07          	.byte 0x7
 588 03f6 02          	.byte 0x2
 589 03f7 23          	.byte 0x23
 590 03f8 00          	.uleb128 0x0
 591 03f9 05          	.uleb128 0x5
 592 03fa 46 52 43 44 	.asciz "FRCDIV1"
 592      49 56 31 00 
 593 0402 03          	.byte 0x3
 594 0403 F8 23       	.2byte 0x23f8
 595 0405 FA 00 00 00 	.4byte 0xfa
 596 0409 02          	.byte 0x2
 597 040a 01          	.byte 0x1
 598 040b 06          	.byte 0x6
 599 040c 02          	.byte 0x2
 600 040d 23          	.byte 0x23
 601 040e 00          	.uleb128 0x0
 602 040f 05          	.uleb128 0x5
 603 0410 46 52 43 44 	.asciz "FRCDIV2"
 603      49 56 32 00 
 604 0418 03          	.byte 0x3
 605 0419 F9 23       	.2byte 0x23f9
 606 041b FA 00 00 00 	.4byte 0xfa
 607 041f 02          	.byte 0x2
 608 0420 01          	.byte 0x1
 609 0421 05          	.byte 0x5
 610 0422 02          	.byte 0x2
 611 0423 23          	.byte 0x23
 612 0424 00          	.uleb128 0x0
 613 0425 05          	.uleb128 0x5
 614 0426 44 4F 5A 45 	.asciz "DOZE0"
 614      30 00 
 615 042c 03          	.byte 0x3
 616 042d FB 23       	.2byte 0x23fb
 617 042f FA 00 00 00 	.4byte 0xfa
 618 0433 02          	.byte 0x2
 619 0434 01          	.byte 0x1
 620 0435 03          	.byte 0x3
 621 0436 02          	.byte 0x2
 622 0437 23          	.byte 0x23
 623 0438 00          	.uleb128 0x0
 624 0439 05          	.uleb128 0x5
 625 043a 44 4F 5A 45 	.asciz "DOZE1"
 625      31 00 
 626 0440 03          	.byte 0x3
 627 0441 FC 23       	.2byte 0x23fc
 628 0443 FA 00 00 00 	.4byte 0xfa
 629 0447 02          	.byte 0x2
 630 0448 01          	.byte 0x1
 631 0449 02          	.byte 0x2
 632 044a 02          	.byte 0x2
 633 044b 23          	.byte 0x23
 634 044c 00          	.uleb128 0x0
MPLAB XC16 ASSEMBLY Listing:   			page 15


 635 044d 05          	.uleb128 0x5
 636 044e 44 4F 5A 45 	.asciz "DOZE2"
 636      32 00 
 637 0454 03          	.byte 0x3
 638 0455 FD 23       	.2byte 0x23fd
 639 0457 FA 00 00 00 	.4byte 0xfa
 640 045b 02          	.byte 0x2
 641 045c 01          	.byte 0x1
 642 045d 01          	.byte 0x1
 643 045e 02          	.byte 0x2
 644 045f 23          	.byte 0x23
 645 0460 00          	.uleb128 0x0
 646 0461 00          	.byte 0x0
 647 0462 06          	.uleb128 0x6
 648 0463 02          	.byte 0x2
 649 0464 03          	.byte 0x3
 650 0465 E4 23       	.2byte 0x23e4
 651 0467 76 04 00 00 	.4byte 0x476
 652 046b 07          	.uleb128 0x7
 653 046c BB 02 00 00 	.4byte 0x2bb
 654 0470 07          	.uleb128 0x7
 655 0471 3E 03 00 00 	.4byte 0x33e
 656 0475 00          	.byte 0x0
 657 0476 08          	.uleb128 0x8
 658 0477 74 61 67 43 	.asciz "tagCLKDIVBITS"
 658      4C 4B 44 49 
 658      56 42 49 54 
 658      53 00 
 659 0485 02          	.byte 0x2
 660 0486 03          	.byte 0x3
 661 0487 E3 23       	.2byte 0x23e3
 662 0489 96 04 00 00 	.4byte 0x496
 663 048d 09          	.uleb128 0x9
 664 048e 62 04 00 00 	.4byte 0x462
 665 0492 02          	.byte 0x2
 666 0493 23          	.byte 0x23
 667 0494 00          	.uleb128 0x0
 668 0495 00          	.byte 0x0
 669 0496 0A          	.uleb128 0xa
 670 0497 43 4C 4B 44 	.asciz "CLKDIVBITS"
 670      49 56 42 49 
 670      54 53 00 
 671 04a2 03          	.byte 0x3
 672 04a3 00 24       	.2byte 0x2400
 673 04a5 76 04 00 00 	.4byte 0x476
 674 04a9 04          	.uleb128 0x4
 675 04aa 02          	.byte 0x2
 676 04ab 03          	.byte 0x3
 677 04ac 47 24       	.2byte 0x2447
 678 04ae 60 05 00 00 	.4byte 0x560
 679 04b2 05          	.uleb128 0x5
 680 04b3 41 50 4C 4C 	.asciz "APLLPRE"
 680      50 52 45 00 
 681 04bb 03          	.byte 0x3
 682 04bc 48 24       	.2byte 0x2448
 683 04be FA 00 00 00 	.4byte 0xfa
 684 04c2 02          	.byte 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 16


 685 04c3 03          	.byte 0x3
 686 04c4 0D          	.byte 0xd
 687 04c5 02          	.byte 0x2
 688 04c6 23          	.byte 0x23
 689 04c7 00          	.uleb128 0x0
 690 04c8 05          	.uleb128 0x5
 691 04c9 41 50 4C 4C 	.asciz "APLLPOST"
 691      50 4F 53 54 
 691      00 
 692 04d2 03          	.byte 0x3
 693 04d3 4A 24       	.2byte 0x244a
 694 04d5 FA 00 00 00 	.4byte 0xfa
 695 04d9 02          	.byte 0x2
 696 04da 03          	.byte 0x3
 697 04db 08          	.byte 0x8
 698 04dc 02          	.byte 0x2
 699 04dd 23          	.byte 0x23
 700 04de 00          	.uleb128 0x0
 701 04df 05          	.uleb128 0x5
 702 04e0 46 52 43 53 	.asciz "FRCSEL"
 702      45 4C 00 
 703 04e7 03          	.byte 0x3
 704 04e8 4C 24       	.2byte 0x244c
 705 04ea FA 00 00 00 	.4byte 0xfa
 706 04ee 02          	.byte 0x2
 707 04ef 01          	.byte 0x1
 708 04f0 06          	.byte 0x6
 709 04f1 02          	.byte 0x2
 710 04f2 23          	.byte 0x23
 711 04f3 00          	.uleb128 0x0
 712 04f4 05          	.uleb128 0x5
 713 04f5 41 53 52 43 	.asciz "ASRCSEL"
 713      53 45 4C 00 
 714 04fd 03          	.byte 0x3
 715 04fe 4D 24       	.2byte 0x244d
 716 0500 FA 00 00 00 	.4byte 0xfa
 717 0504 02          	.byte 0x2
 718 0505 01          	.byte 0x1
 719 0506 05          	.byte 0x5
 720 0507 02          	.byte 0x2
 721 0508 23          	.byte 0x23
 722 0509 00          	.uleb128 0x0
 723 050a 05          	.uleb128 0x5
 724 050b 41 4F 53 43 	.asciz "AOSCMD"
 724      4D 44 00 
 725 0512 03          	.byte 0x3
 726 0513 4E 24       	.2byte 0x244e
 727 0515 FA 00 00 00 	.4byte 0xfa
 728 0519 02          	.byte 0x2
 729 051a 02          	.byte 0x2
 730 051b 03          	.byte 0x3
 731 051c 02          	.byte 0x2
 732 051d 23          	.byte 0x23
 733 051e 00          	.uleb128 0x0
 734 051f 05          	.uleb128 0x5
 735 0520 53 45 4C 41 	.asciz "SELACLK"
 735      43 4C 4B 00 
MPLAB XC16 ASSEMBLY Listing:   			page 17


 736 0528 03          	.byte 0x3
 737 0529 4F 24       	.2byte 0x244f
 738 052b FA 00 00 00 	.4byte 0xfa
 739 052f 02          	.byte 0x2
 740 0530 01          	.byte 0x1
 741 0531 02          	.byte 0x2
 742 0532 02          	.byte 0x2
 743 0533 23          	.byte 0x23
 744 0534 00          	.uleb128 0x0
 745 0535 05          	.uleb128 0x5
 746 0536 41 50 4C 4C 	.asciz "APLLCK"
 746      43 4B 00 
 747 053d 03          	.byte 0x3
 748 053e 50 24       	.2byte 0x2450
 749 0540 FA 00 00 00 	.4byte 0xfa
 750 0544 02          	.byte 0x2
 751 0545 01          	.byte 0x1
 752 0546 01          	.byte 0x1
 753 0547 02          	.byte 0x2
 754 0548 23          	.byte 0x23
 755 0549 00          	.uleb128 0x0
 756 054a 05          	.uleb128 0x5
 757 054b 45 4E 41 50 	.asciz "ENAPLL"
 757      4C 4C 00 
 758 0552 03          	.byte 0x3
 759 0553 51 24       	.2byte 0x2451
 760 0555 FA 00 00 00 	.4byte 0xfa
 761 0559 02          	.byte 0x2
 762 055a 01          	.byte 0x1
 763 055b 10          	.byte 0x10
 764 055c 02          	.byte 0x2
 765 055d 23          	.byte 0x23
 766 055e 00          	.uleb128 0x0
 767 055f 00          	.byte 0x0
 768 0560 04          	.uleb128 0x4
 769 0561 02          	.byte 0x2
 770 0562 03          	.byte 0x3
 771 0563 53 24       	.2byte 0x2453
 772 0565 23 06 00 00 	.4byte 0x623
 773 0569 05          	.uleb128 0x5
 774 056a 41 50 50 4C 	.asciz "APPLPRE0"
 774      50 52 45 30 
 774      00 
 775 0573 03          	.byte 0x3
 776 0574 54 24       	.2byte 0x2454
 777 0576 FA 00 00 00 	.4byte 0xfa
 778 057a 02          	.byte 0x2
 779 057b 01          	.byte 0x1
 780 057c 0F          	.byte 0xf
 781 057d 02          	.byte 0x2
 782 057e 23          	.byte 0x23
 783 057f 00          	.uleb128 0x0
 784 0580 05          	.uleb128 0x5
 785 0581 41 50 50 4C 	.asciz "APPLPRE1"
 785      50 52 45 31 
 785      00 
 786 058a 03          	.byte 0x3
MPLAB XC16 ASSEMBLY Listing:   			page 18


 787 058b 55 24       	.2byte 0x2455
 788 058d FA 00 00 00 	.4byte 0xfa
 789 0591 02          	.byte 0x2
 790 0592 01          	.byte 0x1
 791 0593 0E          	.byte 0xe
 792 0594 02          	.byte 0x2
 793 0595 23          	.byte 0x23
 794 0596 00          	.uleb128 0x0
 795 0597 05          	.uleb128 0x5
 796 0598 41 50 50 4C 	.asciz "APPLPRE2"
 796      50 52 45 32 
 796      00 
 797 05a1 03          	.byte 0x3
 798 05a2 56 24       	.2byte 0x2456
 799 05a4 FA 00 00 00 	.4byte 0xfa
 800 05a8 02          	.byte 0x2
 801 05a9 01          	.byte 0x1
 802 05aa 0D          	.byte 0xd
 803 05ab 02          	.byte 0x2
 804 05ac 23          	.byte 0x23
 805 05ad 00          	.uleb128 0x0
 806 05ae 05          	.uleb128 0x5
 807 05af 41 50 4C 4C 	.asciz "APLLPOST0"
 807      50 4F 53 54 
 807      30 00 
 808 05b9 03          	.byte 0x3
 809 05ba 58 24       	.2byte 0x2458
 810 05bc FA 00 00 00 	.4byte 0xfa
 811 05c0 02          	.byte 0x2
 812 05c1 01          	.byte 0x1
 813 05c2 0A          	.byte 0xa
 814 05c3 02          	.byte 0x2
 815 05c4 23          	.byte 0x23
 816 05c5 00          	.uleb128 0x0
 817 05c6 05          	.uleb128 0x5
 818 05c7 41 50 4C 4C 	.asciz "APLLPOST1"
 818      50 4F 53 54 
 818      31 00 
 819 05d1 03          	.byte 0x3
 820 05d2 59 24       	.2byte 0x2459
 821 05d4 FA 00 00 00 	.4byte 0xfa
 822 05d8 02          	.byte 0x2
 823 05d9 01          	.byte 0x1
 824 05da 09          	.byte 0x9
 825 05db 02          	.byte 0x2
 826 05dc 23          	.byte 0x23
 827 05dd 00          	.uleb128 0x0
 828 05de 05          	.uleb128 0x5
 829 05df 41 50 4C 4C 	.asciz "APLLPOST2"
 829      50 4F 53 54 
 829      32 00 
 830 05e9 03          	.byte 0x3
 831 05ea 5A 24       	.2byte 0x245a
 832 05ec FA 00 00 00 	.4byte 0xfa
 833 05f0 02          	.byte 0x2
 834 05f1 01          	.byte 0x1
 835 05f2 08          	.byte 0x8
MPLAB XC16 ASSEMBLY Listing:   			page 19


 836 05f3 02          	.byte 0x2
 837 05f4 23          	.byte 0x23
 838 05f5 00          	.uleb128 0x0
 839 05f6 05          	.uleb128 0x5
 840 05f7 41 4F 53 43 	.asciz "AOSCMD0"
 840      4D 44 30 00 
 841 05ff 03          	.byte 0x3
 842 0600 5C 24       	.2byte 0x245c
 843 0602 FA 00 00 00 	.4byte 0xfa
 844 0606 02          	.byte 0x2
 845 0607 01          	.byte 0x1
 846 0608 04          	.byte 0x4
 847 0609 02          	.byte 0x2
 848 060a 23          	.byte 0x23
 849 060b 00          	.uleb128 0x0
 850 060c 05          	.uleb128 0x5
 851 060d 41 4F 53 43 	.asciz "AOSCMD1"
 851      4D 44 31 00 
 852 0615 03          	.byte 0x3
 853 0616 5D 24       	.2byte 0x245d
 854 0618 FA 00 00 00 	.4byte 0xfa
 855 061c 02          	.byte 0x2
 856 061d 01          	.byte 0x1
 857 061e 03          	.byte 0x3
 858 061f 02          	.byte 0x2
 859 0620 23          	.byte 0x23
 860 0621 00          	.uleb128 0x0
 861 0622 00          	.byte 0x0
 862 0623 06          	.uleb128 0x6
 863 0624 02          	.byte 0x2
 864 0625 03          	.byte 0x3
 865 0626 46 24       	.2byte 0x2446
 866 0628 37 06 00 00 	.4byte 0x637
 867 062c 07          	.uleb128 0x7
 868 062d A9 04 00 00 	.4byte 0x4a9
 869 0631 07          	.uleb128 0x7
 870 0632 60 05 00 00 	.4byte 0x560
 871 0636 00          	.byte 0x0
 872 0637 08          	.uleb128 0x8
 873 0638 74 61 67 41 	.asciz "tagACLKCON3BITS"
 873      43 4C 4B 43 
 873      4F 4E 33 42 
 873      49 54 53 00 
 874 0648 02          	.byte 0x2
 875 0649 03          	.byte 0x3
 876 064a 45 24       	.2byte 0x2445
 877 064c 59 06 00 00 	.4byte 0x659
 878 0650 09          	.uleb128 0x9
 879 0651 23 06 00 00 	.4byte 0x623
 880 0655 02          	.byte 0x2
 881 0656 23          	.byte 0x23
 882 0657 00          	.uleb128 0x0
 883 0658 00          	.byte 0x0
 884 0659 0A          	.uleb128 0xa
 885 065a 41 43 4C 4B 	.asciz "ACLKCON3BITS"
 885      43 4F 4E 33 
 885      42 49 54 53 
MPLAB XC16 ASSEMBLY Listing:   			page 20


 885      00 
 886 0667 03          	.byte 0x3
 887 0668 60 24       	.2byte 0x2460
 888 066a 37 06 00 00 	.4byte 0x637
 889 066e 04          	.uleb128 0x4
 890 066f 02          	.byte 0x2
 891 0670 03          	.byte 0x3
 892 0671 67 24       	.2byte 0x2467
 893 0673 8E 06 00 00 	.4byte 0x68e
 894 0677 05          	.uleb128 0x5
 895 0678 41 50 4C 4C 	.asciz "APLLDIV"
 895      44 49 56 00 
 896 0680 03          	.byte 0x3
 897 0681 68 24       	.2byte 0x2468
 898 0683 FA 00 00 00 	.4byte 0xfa
 899 0687 02          	.byte 0x2
 900 0688 03          	.byte 0x3
 901 0689 0D          	.byte 0xd
 902 068a 02          	.byte 0x2
 903 068b 23          	.byte 0x23
 904 068c 00          	.uleb128 0x0
 905 068d 00          	.byte 0x0
 906 068e 04          	.uleb128 0x4
 907 068f 02          	.byte 0x2
 908 0690 03          	.byte 0x3
 909 0691 6A 24       	.2byte 0x246a
 910 0693 DD 06 00 00 	.4byte 0x6dd
 911 0697 05          	.uleb128 0x5
 912 0698 41 50 4C 4C 	.asciz "APLLDIV0"
 912      44 49 56 30 
 912      00 
 913 06a1 03          	.byte 0x3
 914 06a2 6B 24       	.2byte 0x246b
 915 06a4 FA 00 00 00 	.4byte 0xfa
 916 06a8 02          	.byte 0x2
 917 06a9 01          	.byte 0x1
 918 06aa 0F          	.byte 0xf
 919 06ab 02          	.byte 0x2
 920 06ac 23          	.byte 0x23
 921 06ad 00          	.uleb128 0x0
 922 06ae 05          	.uleb128 0x5
 923 06af 41 50 4C 4C 	.asciz "APLLDIV1"
 923      44 49 56 31 
 923      00 
 924 06b8 03          	.byte 0x3
 925 06b9 6C 24       	.2byte 0x246c
 926 06bb FA 00 00 00 	.4byte 0xfa
 927 06bf 02          	.byte 0x2
 928 06c0 01          	.byte 0x1
 929 06c1 0E          	.byte 0xe
 930 06c2 02          	.byte 0x2
 931 06c3 23          	.byte 0x23
 932 06c4 00          	.uleb128 0x0
 933 06c5 05          	.uleb128 0x5
 934 06c6 41 50 4C 4C 	.asciz "APLLDIV2"
 934      44 49 56 32 
 934      00 
MPLAB XC16 ASSEMBLY Listing:   			page 21


 935 06cf 03          	.byte 0x3
 936 06d0 6D 24       	.2byte 0x246d
 937 06d2 FA 00 00 00 	.4byte 0xfa
 938 06d6 02          	.byte 0x2
 939 06d7 01          	.byte 0x1
 940 06d8 0D          	.byte 0xd
 941 06d9 02          	.byte 0x2
 942 06da 23          	.byte 0x23
 943 06db 00          	.uleb128 0x0
 944 06dc 00          	.byte 0x0
 945 06dd 06          	.uleb128 0x6
 946 06de 02          	.byte 0x2
 947 06df 03          	.byte 0x3
 948 06e0 66 24       	.2byte 0x2466
 949 06e2 F1 06 00 00 	.4byte 0x6f1
 950 06e6 07          	.uleb128 0x7
 951 06e7 6E 06 00 00 	.4byte 0x66e
 952 06eb 07          	.uleb128 0x7
 953 06ec 8E 06 00 00 	.4byte 0x68e
 954 06f0 00          	.byte 0x0
 955 06f1 08          	.uleb128 0x8
 956 06f2 74 61 67 41 	.asciz "tagACLKDIV3BITS"
 956      43 4C 4B 44 
 956      49 56 33 42 
 956      49 54 53 00 
 957 0702 02          	.byte 0x2
 958 0703 03          	.byte 0x3
 959 0704 65 24       	.2byte 0x2465
 960 0706 13 07 00 00 	.4byte 0x713
 961 070a 09          	.uleb128 0x9
 962 070b DD 06 00 00 	.4byte 0x6dd
 963 070f 02          	.byte 0x2
 964 0710 23          	.byte 0x23
 965 0711 00          	.uleb128 0x0
 966 0712 00          	.byte 0x0
 967 0713 0A          	.uleb128 0xa
 968 0714 41 43 4C 4B 	.asciz "ACLKDIV3BITS"
 968      44 49 56 33 
 968      42 49 54 53 
 968      00 
 969 0721 03          	.byte 0x3
 970 0722 70 24       	.2byte 0x2470
 971 0724 F1 06 00 00 	.4byte 0x6f1
 972 0728 0B          	.uleb128 0xb
 973 0729 01          	.byte 0x1
 974 072a 63 6F 6E 66 	.asciz "config_clock"
 974      69 67 5F 63 
 974      6C 6F 63 6B 
 974      00 
 975 0737 01          	.byte 0x1
 976 0738 06          	.byte 0x6
 977 0739 01          	.byte 0x1
 978 073a 00 00 00 00 	.4byte .LFB0
 979 073e 00 00 00 00 	.4byte .LFE0
 980 0742 01          	.byte 0x1
 981 0743 5E          	.byte 0x5e
 982 0744 0C          	.uleb128 0xc
MPLAB XC16 ASSEMBLY Listing:   			page 22


 983 0745 00 00 00 00 	.4byte .LASF0
 984 0749 03          	.byte 0x3
 985 074a DB 23       	.2byte 0x23db
 986 074c 52 07 00 00 	.4byte 0x752
 987 0750 01          	.byte 0x1
 988 0751 01          	.byte 0x1
 989 0752 0D          	.uleb128 0xd
 990 0753 A8 02 00 00 	.4byte 0x2a8
 991 0757 0E          	.uleb128 0xe
 992 0758 4F 53 43 43 	.asciz "OSCCONL"
 992      4F 4E 4C 00 
 993 0760 03          	.byte 0x3
 994 0761 DE 23       	.2byte 0x23de
 995 0763 69 07 00 00 	.4byte 0x769
 996 0767 01          	.byte 0x1
 997 0768 01          	.byte 0x1
 998 0769 0D          	.uleb128 0xd
 999 076a DA 00 00 00 	.4byte 0xda
 1000 076e 0C          	.uleb128 0xc
 1001 076f 00 00 00 00 	.4byte .LASF1
 1002 0773 03          	.byte 0x3
 1003 0774 01 24       	.2byte 0x2401
 1004 0776 7C 07 00 00 	.4byte 0x77c
 1005 077a 01          	.byte 0x1
 1006 077b 01          	.byte 0x1
 1007 077c 0D          	.uleb128 0xd
 1008 077d 96 04 00 00 	.4byte 0x496
 1009 0781 0E          	.uleb128 0xe
 1010 0782 50 4C 4C 46 	.asciz "PLLFBD"
 1010      42 44 00 
 1011 0789 03          	.byte 0x3
 1012 078a 04 24       	.2byte 0x2404
 1013 078c 92 07 00 00 	.4byte 0x792
 1014 0790 01          	.byte 0x1
 1015 0791 01          	.byte 0x1
 1016 0792 0D          	.uleb128 0xd
 1017 0793 FA 00 00 00 	.4byte 0xfa
 1018 0797 0C          	.uleb128 0xc
 1019 0798 00 00 00 00 	.4byte .LASF2
 1020 079c 03          	.byte 0x3
 1021 079d 61 24       	.2byte 0x2461
 1022 079f A5 07 00 00 	.4byte 0x7a5
 1023 07a3 01          	.byte 0x1
 1024 07a4 01          	.byte 0x1
 1025 07a5 0D          	.uleb128 0xd
 1026 07a6 59 06 00 00 	.4byte 0x659
 1027 07aa 0C          	.uleb128 0xc
 1028 07ab 00 00 00 00 	.4byte .LASF3
 1029 07af 03          	.byte 0x3
 1030 07b0 71 24       	.2byte 0x2471
 1031 07b2 B8 07 00 00 	.4byte 0x7b8
 1032 07b6 01          	.byte 0x1
 1033 07b7 01          	.byte 0x1
 1034 07b8 0D          	.uleb128 0xd
 1035 07b9 13 07 00 00 	.4byte 0x713
 1036 07bd 0C          	.uleb128 0xc
 1037 07be 00 00 00 00 	.4byte .LASF0
MPLAB XC16 ASSEMBLY Listing:   			page 23


 1038 07c2 03          	.byte 0x3
 1039 07c3 DB 23       	.2byte 0x23db
 1040 07c5 52 07 00 00 	.4byte 0x752
 1041 07c9 01          	.byte 0x1
 1042 07ca 01          	.byte 0x1
 1043 07cb 0E          	.uleb128 0xe
 1044 07cc 4F 53 43 43 	.asciz "OSCCONL"
 1044      4F 4E 4C 00 
 1045 07d4 03          	.byte 0x3
 1046 07d5 DE 23       	.2byte 0x23de
 1047 07d7 69 07 00 00 	.4byte 0x769
 1048 07db 01          	.byte 0x1
 1049 07dc 01          	.byte 0x1
 1050 07dd 0C          	.uleb128 0xc
 1051 07de 00 00 00 00 	.4byte .LASF1
 1052 07e2 03          	.byte 0x3
 1053 07e3 01 24       	.2byte 0x2401
 1054 07e5 7C 07 00 00 	.4byte 0x77c
 1055 07e9 01          	.byte 0x1
 1056 07ea 01          	.byte 0x1
 1057 07eb 0E          	.uleb128 0xe
 1058 07ec 50 4C 4C 46 	.asciz "PLLFBD"
 1058      42 44 00 
 1059 07f3 03          	.byte 0x3
 1060 07f4 04 24       	.2byte 0x2404
 1061 07f6 92 07 00 00 	.4byte 0x792
 1062 07fa 01          	.byte 0x1
 1063 07fb 01          	.byte 0x1
 1064 07fc 0C          	.uleb128 0xc
 1065 07fd 00 00 00 00 	.4byte .LASF2
 1066 0801 03          	.byte 0x3
 1067 0802 61 24       	.2byte 0x2461
 1068 0804 A5 07 00 00 	.4byte 0x7a5
 1069 0808 01          	.byte 0x1
 1070 0809 01          	.byte 0x1
 1071 080a 0C          	.uleb128 0xc
 1072 080b 00 00 00 00 	.4byte .LASF3
 1073 080f 03          	.byte 0x3
 1074 0810 71 24       	.2byte 0x2471
 1075 0812 B8 07 00 00 	.4byte 0x7b8
 1076 0816 01          	.byte 0x1
 1077 0817 01          	.byte 0x1
 1078 0818 00          	.byte 0x0
 1079                 	.section .debug_abbrev,info
 1080 0000 01          	.uleb128 0x1
 1081 0001 11          	.uleb128 0x11
 1082 0002 01          	.byte 0x1
 1083 0003 25          	.uleb128 0x25
 1084 0004 08          	.uleb128 0x8
 1085 0005 13          	.uleb128 0x13
 1086 0006 0B          	.uleb128 0xb
 1087 0007 03          	.uleb128 0x3
 1088 0008 08          	.uleb128 0x8
 1089 0009 1B          	.uleb128 0x1b
 1090 000a 08          	.uleb128 0x8
 1091 000b 11          	.uleb128 0x11
 1092 000c 01          	.uleb128 0x1
MPLAB XC16 ASSEMBLY Listing:   			page 24


 1093 000d 12          	.uleb128 0x12
 1094 000e 01          	.uleb128 0x1
 1095 000f 10          	.uleb128 0x10
 1096 0010 06          	.uleb128 0x6
 1097 0011 00          	.byte 0x0
 1098 0012 00          	.byte 0x0
 1099 0013 02          	.uleb128 0x2
 1100 0014 24          	.uleb128 0x24
 1101 0015 00          	.byte 0x0
 1102 0016 0B          	.uleb128 0xb
 1103 0017 0B          	.uleb128 0xb
 1104 0018 3E          	.uleb128 0x3e
 1105 0019 0B          	.uleb128 0xb
 1106 001a 03          	.uleb128 0x3
 1107 001b 08          	.uleb128 0x8
 1108 001c 00          	.byte 0x0
 1109 001d 00          	.byte 0x0
 1110 001e 03          	.uleb128 0x3
 1111 001f 16          	.uleb128 0x16
 1112 0020 00          	.byte 0x0
 1113 0021 03          	.uleb128 0x3
 1114 0022 08          	.uleb128 0x8
 1115 0023 3A          	.uleb128 0x3a
 1116 0024 0B          	.uleb128 0xb
 1117 0025 3B          	.uleb128 0x3b
 1118 0026 0B          	.uleb128 0xb
 1119 0027 49          	.uleb128 0x49
 1120 0028 13          	.uleb128 0x13
 1121 0029 00          	.byte 0x0
 1122 002a 00          	.byte 0x0
 1123 002b 04          	.uleb128 0x4
 1124 002c 13          	.uleb128 0x13
 1125 002d 01          	.byte 0x1
 1126 002e 0B          	.uleb128 0xb
 1127 002f 0B          	.uleb128 0xb
 1128 0030 3A          	.uleb128 0x3a
 1129 0031 0B          	.uleb128 0xb
 1130 0032 3B          	.uleb128 0x3b
 1131 0033 05          	.uleb128 0x5
 1132 0034 01          	.uleb128 0x1
 1133 0035 13          	.uleb128 0x13
 1134 0036 00          	.byte 0x0
 1135 0037 00          	.byte 0x0
 1136 0038 05          	.uleb128 0x5
 1137 0039 0D          	.uleb128 0xd
 1138 003a 00          	.byte 0x0
 1139 003b 03          	.uleb128 0x3
 1140 003c 08          	.uleb128 0x8
 1141 003d 3A          	.uleb128 0x3a
 1142 003e 0B          	.uleb128 0xb
 1143 003f 3B          	.uleb128 0x3b
 1144 0040 05          	.uleb128 0x5
 1145 0041 49          	.uleb128 0x49
 1146 0042 13          	.uleb128 0x13
 1147 0043 0B          	.uleb128 0xb
 1148 0044 0B          	.uleb128 0xb
 1149 0045 0D          	.uleb128 0xd
MPLAB XC16 ASSEMBLY Listing:   			page 25


 1150 0046 0B          	.uleb128 0xb
 1151 0047 0C          	.uleb128 0xc
 1152 0048 0B          	.uleb128 0xb
 1153 0049 38          	.uleb128 0x38
 1154 004a 0A          	.uleb128 0xa
 1155 004b 00          	.byte 0x0
 1156 004c 00          	.byte 0x0
 1157 004d 06          	.uleb128 0x6
 1158 004e 17          	.uleb128 0x17
 1159 004f 01          	.byte 0x1
 1160 0050 0B          	.uleb128 0xb
 1161 0051 0B          	.uleb128 0xb
 1162 0052 3A          	.uleb128 0x3a
 1163 0053 0B          	.uleb128 0xb
 1164 0054 3B          	.uleb128 0x3b
 1165 0055 05          	.uleb128 0x5
 1166 0056 01          	.uleb128 0x1
 1167 0057 13          	.uleb128 0x13
 1168 0058 00          	.byte 0x0
 1169 0059 00          	.byte 0x0
 1170 005a 07          	.uleb128 0x7
 1171 005b 0D          	.uleb128 0xd
 1172 005c 00          	.byte 0x0
 1173 005d 49          	.uleb128 0x49
 1174 005e 13          	.uleb128 0x13
 1175 005f 00          	.byte 0x0
 1176 0060 00          	.byte 0x0
 1177 0061 08          	.uleb128 0x8
 1178 0062 13          	.uleb128 0x13
 1179 0063 01          	.byte 0x1
 1180 0064 03          	.uleb128 0x3
 1181 0065 08          	.uleb128 0x8
 1182 0066 0B          	.uleb128 0xb
 1183 0067 0B          	.uleb128 0xb
 1184 0068 3A          	.uleb128 0x3a
 1185 0069 0B          	.uleb128 0xb
 1186 006a 3B          	.uleb128 0x3b
 1187 006b 05          	.uleb128 0x5
 1188 006c 01          	.uleb128 0x1
 1189 006d 13          	.uleb128 0x13
 1190 006e 00          	.byte 0x0
 1191 006f 00          	.byte 0x0
 1192 0070 09          	.uleb128 0x9
 1193 0071 0D          	.uleb128 0xd
 1194 0072 00          	.byte 0x0
 1195 0073 49          	.uleb128 0x49
 1196 0074 13          	.uleb128 0x13
 1197 0075 38          	.uleb128 0x38
 1198 0076 0A          	.uleb128 0xa
 1199 0077 00          	.byte 0x0
 1200 0078 00          	.byte 0x0
 1201 0079 0A          	.uleb128 0xa
 1202 007a 16          	.uleb128 0x16
 1203 007b 00          	.byte 0x0
 1204 007c 03          	.uleb128 0x3
 1205 007d 08          	.uleb128 0x8
 1206 007e 3A          	.uleb128 0x3a
MPLAB XC16 ASSEMBLY Listing:   			page 26


 1207 007f 0B          	.uleb128 0xb
 1208 0080 3B          	.uleb128 0x3b
 1209 0081 05          	.uleb128 0x5
 1210 0082 49          	.uleb128 0x49
 1211 0083 13          	.uleb128 0x13
 1212 0084 00          	.byte 0x0
 1213 0085 00          	.byte 0x0
 1214 0086 0B          	.uleb128 0xb
 1215 0087 2E          	.uleb128 0x2e
 1216 0088 00          	.byte 0x0
 1217 0089 3F          	.uleb128 0x3f
 1218 008a 0C          	.uleb128 0xc
 1219 008b 03          	.uleb128 0x3
 1220 008c 08          	.uleb128 0x8
 1221 008d 3A          	.uleb128 0x3a
 1222 008e 0B          	.uleb128 0xb
 1223 008f 3B          	.uleb128 0x3b
 1224 0090 0B          	.uleb128 0xb
 1225 0091 27          	.uleb128 0x27
 1226 0092 0C          	.uleb128 0xc
 1227 0093 11          	.uleb128 0x11
 1228 0094 01          	.uleb128 0x1
 1229 0095 12          	.uleb128 0x12
 1230 0096 01          	.uleb128 0x1
 1231 0097 40          	.uleb128 0x40
 1232 0098 0A          	.uleb128 0xa
 1233 0099 00          	.byte 0x0
 1234 009a 00          	.byte 0x0
 1235 009b 0C          	.uleb128 0xc
 1236 009c 34          	.uleb128 0x34
 1237 009d 00          	.byte 0x0
 1238 009e 03          	.uleb128 0x3
 1239 009f 0E          	.uleb128 0xe
 1240 00a0 3A          	.uleb128 0x3a
 1241 00a1 0B          	.uleb128 0xb
 1242 00a2 3B          	.uleb128 0x3b
 1243 00a3 05          	.uleb128 0x5
 1244 00a4 49          	.uleb128 0x49
 1245 00a5 13          	.uleb128 0x13
 1246 00a6 3F          	.uleb128 0x3f
 1247 00a7 0C          	.uleb128 0xc
 1248 00a8 3C          	.uleb128 0x3c
 1249 00a9 0C          	.uleb128 0xc
 1250 00aa 00          	.byte 0x0
 1251 00ab 00          	.byte 0x0
 1252 00ac 0D          	.uleb128 0xd
 1253 00ad 35          	.uleb128 0x35
 1254 00ae 00          	.byte 0x0
 1255 00af 49          	.uleb128 0x49
 1256 00b0 13          	.uleb128 0x13
 1257 00b1 00          	.byte 0x0
 1258 00b2 00          	.byte 0x0
 1259 00b3 0E          	.uleb128 0xe
 1260 00b4 34          	.uleb128 0x34
 1261 00b5 00          	.byte 0x0
 1262 00b6 03          	.uleb128 0x3
 1263 00b7 08          	.uleb128 0x8
MPLAB XC16 ASSEMBLY Listing:   			page 27


 1264 00b8 3A          	.uleb128 0x3a
 1265 00b9 0B          	.uleb128 0xb
 1266 00ba 3B          	.uleb128 0x3b
 1267 00bb 05          	.uleb128 0x5
 1268 00bc 49          	.uleb128 0x49
 1269 00bd 13          	.uleb128 0x13
 1270 00be 3F          	.uleb128 0x3f
 1271 00bf 0C          	.uleb128 0xc
 1272 00c0 3C          	.uleb128 0x3c
 1273 00c1 0C          	.uleb128 0xc
 1274 00c2 00          	.byte 0x0
 1275 00c3 00          	.byte 0x0
 1276 00c4 00          	.byte 0x0
 1277                 	.section .debug_pubnames,info
 1278 0000 1F 00 00 00 	.4byte 0x1f
 1279 0004 02 00       	.2byte 0x2
 1280 0006 00 00 00 00 	.4byte .Ldebug_info0
 1281 000a 19 08 00 00 	.4byte 0x819
 1282 000e 28 07 00 00 	.4byte 0x728
 1283 0012 63 6F 6E 66 	.asciz "config_clock"
 1283      69 67 5F 63 
 1283      6C 6F 63 6B 
 1283      00 
 1284 001f 00 00 00 00 	.4byte 0x0
 1285                 	.section .debug_pubtypes,info
 1286 0000 B3 00 00 00 	.4byte 0xb3
 1287 0004 02 00       	.2byte 0x2
 1288 0006 00 00 00 00 	.4byte .Ldebug_info0
 1289 000a 19 08 00 00 	.4byte 0x819
 1290 000e DA 00 00 00 	.4byte 0xda
 1291 0012 75 69 6E 74 	.asciz "uint8_t"
 1291      38 5F 74 00 
 1292 001a FA 00 00 00 	.4byte 0xfa
 1293 001e 75 69 6E 74 	.asciz "uint16_t"
 1293      31 36 5F 74 
 1293      00 
 1294 0027 88 02 00 00 	.4byte 0x288
 1295 002b 74 61 67 4F 	.asciz "tagOSCCONBITS"
 1295      53 43 43 4F 
 1295      4E 42 49 54 
 1295      53 00 
 1296 0039 A8 02 00 00 	.4byte 0x2a8
 1297 003d 4F 53 43 43 	.asciz "OSCCONBITS"
 1297      4F 4E 42 49 
 1297      54 53 00 
 1298 0048 76 04 00 00 	.4byte 0x476
 1299 004c 74 61 67 43 	.asciz "tagCLKDIVBITS"
 1299      4C 4B 44 49 
 1299      56 42 49 54 
 1299      53 00 
 1300 005a 96 04 00 00 	.4byte 0x496
 1301 005e 43 4C 4B 44 	.asciz "CLKDIVBITS"
 1301      49 56 42 49 
 1301      54 53 00 
 1302 0069 37 06 00 00 	.4byte 0x637
 1303 006d 74 61 67 41 	.asciz "tagACLKCON3BITS"
 1303      43 4C 4B 43 
MPLAB XC16 ASSEMBLY Listing:   			page 28


 1303      4F 4E 33 42 
 1303      49 54 53 00 
 1304 007d 59 06 00 00 	.4byte 0x659
 1305 0081 41 43 4C 4B 	.asciz "ACLKCON3BITS"
 1305      43 4F 4E 33 
 1305      42 49 54 53 
 1305      00 
 1306 008e F1 06 00 00 	.4byte 0x6f1
 1307 0092 74 61 67 41 	.asciz "tagACLKDIV3BITS"
 1307      43 4C 4B 44 
 1307      49 56 33 42 
 1307      49 54 53 00 
 1308 00a2 13 07 00 00 	.4byte 0x713
 1309 00a6 41 43 4C 4B 	.asciz "ACLKDIV3BITS"
 1309      44 49 56 33 
 1309      42 49 54 53 
 1309      00 
 1310 00b3 00 00 00 00 	.4byte 0x0
 1311                 	.section .debug_aranges,info
 1312 0000 14 00 00 00 	.4byte 0x14
 1313 0004 02 00       	.2byte 0x2
 1314 0006 00 00 00 00 	.4byte .Ldebug_info0
 1315 000a 04          	.byte 0x4
 1316 000b 00          	.byte 0x0
 1317 000c 00 00       	.2byte 0x0
 1318 000e 00 00       	.2byte 0x0
 1319 0010 00 00 00 00 	.4byte 0x0
 1320 0014 00 00 00 00 	.4byte 0x0
 1321                 	.section .debug_str,info
 1322                 	.LASF0:
 1323 0000 4F 53 43 43 	.asciz "OSCCONbits"
 1323      4F 4E 62 69 
 1323      74 73 00 
 1324                 	.LASF2:
 1325 000b 41 43 4C 4B 	.asciz "ACLKCON3bits"
 1325      43 4F 4E 33 
 1325      62 69 74 73 
 1325      00 
 1326                 	.LASF1:
 1327 0018 43 4C 4B 44 	.asciz "CLKDIVbits"
 1327      49 56 62 69 
 1327      74 73 00 
 1328                 	.LASF3:
 1329 0023 41 43 4C 4B 	.asciz "ACLKDIV3bits"
 1329      44 49 56 33 
 1329      62 69 74 73 
 1329      00 
 1330                 	.section .text,code
 1331              	
 1332              	
 1333              	
 1334              	.section __c30_info,info,bss
 1335                 	__psv_trap_errata:
 1336                 	
 1337                 	.section __c30_signature,info,data
 1338 0000 01 00       	.word 0x0001
 1339 0002 00 00       	.word 0x0000
MPLAB XC16 ASSEMBLY Listing:   			page 29


 1340 0004 00 00       	.word 0x0000
 1341                 	
 1342                 	
 1343                 	
 1344                 	.set ___PA___,0
 1345                 	.end
MPLAB XC16 ASSEMBLY Listing:   			page 30


DEFINED SYMBOLS
                            *ABS*:00000000 /home/user/Documents/FST/Programming/project_sub-zero/lib/lib_pic33e/timing.c
                            *ABS*:00000001 __33EP256MU806
                            *ABS*:00000001 __dsPIC33E
                            *ABS*:00000001 __HAS_DSP
                            *ABS*:00000001 __HAS_DMAV2
                            *ABS*:00000001 __HAS_CODEGUARD
                            *ABS*:00000001 __HAS_PMP_ENHANCED
                            *ABS*:00000001 __HAS_EDS
                            *ABS*:00000001 __HAS_EP
                            *ABS*:00000011 __TARGET_DIVIDE_CYCLES
                            *ABS*:00000001 __C30ELF
                            *ABS*:00000040 __ext_attr_.user_init
    {standard input}:19     .text:00000000 _config_clock
    {standard input}:23     *ABS*:00000000 ___PA___
    {standard input}:76     *ABS*:00000000 ___BP___
    {standard input}:1335   __c30_info:00000000 __psv_trap_errata
    {standard input}:24     .text:00000000 .L0
                            .text:0000003e .L2
                            .text:0000004a .L3
                            .text:00000082 .L4
                    .debug_abbrev:00000000 .Ldebug_abbrev0
                            .text:00000000 .Ltext0
                            .text:00000094 .Letext0
                      .debug_line:00000000 .Ldebug_line0
                            .text:00000000 .LFB0
                            .text:00000094 .LFE0
                       .debug_str:00000000 .LASF0
                       .debug_str:00000018 .LASF1
                       .debug_str:0000000b .LASF2
                       .debug_str:00000023 .LASF3
                     .debug_frame:00000000 .Lframe0
                      .debug_info:00000000 .Ldebug_info0

UNDEFINED SYMBOLS
_CLKDIVbits
_PLLFBD
_OSCCON
_OSCCONL
_OSCCONbits
_ACLKCON3bits
_ACLKDIV3bits
CORCON

EQUATE SYMBOLS
/home/user/Documents/FST/Programming/project_sub-zero/lib/lib_pic33e/timing.c = 0x0
                  __33EP256MU806 = 0x1
                      __dsPIC33E = 0x1
                       __HAS_DSP = 0x1
                     __HAS_DMAV2 = 0x1
                 __HAS_CODEGUARD = 0x1
              __HAS_PMP_ENHANCED = 0x1
                       __HAS_EDS = 0x1
                        __HAS_EP = 0x1
          __TARGET_DIVIDE_CYCLES = 0x11
                        __C30ELF = 0x1
           __ext_attr_.user_init = 0x40
                        ___PA___ = 0x0
                        ___BP___ = 0x0
