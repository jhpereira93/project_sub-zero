MPLAB XC16 ASSEMBLY Listing:   			page 1


   1              	.file "/home/user/Documents/FST/Programming/project_sub-zero/lib/lib_pic33e/led.c"
   2              	.section .debug_abbrev,info
   3                 	.Ldebug_abbrev0:
   4                 	.section .debug_info,info
   5                 	.Ldebug_info0:
   6                 	.section .debug_line,info
   7                 	.Ldebug_line0:
   8 0000 FD 00 00 00 	.section .text,code
   8      02 00 B9 00 
   8      00 00 01 01 
   8      FB 0E 0A 00 
   8      01 01 01 01 
   8      00 00 00 01 
   8      6C 69 62 2F 
   8      6C 69 62 5F 
   8      70 69 63 33 
   9              	.Ltext0:
  10              	.section .text,code
  11              	.align 2
  12              	.global _config_rgb
  13              	.type _config_rgb,@function
  14              	_config_rgb:
  15              	.LFB0:
  16              	.file 1 "lib/lib_pic33e/led.c"
   1:lib/lib_pic33e/led.c **** #ifndef NOLED
   2:lib/lib_pic33e/led.c **** 
   3:lib/lib_pic33e/led.c **** #include "led.h"
   4:lib/lib_pic33e/led.c **** 
   5:lib/lib_pic33e/led.c **** #include <xc.h>
   6:lib/lib_pic33e/led.c **** #include <ctype.h>
   7:lib/lib_pic33e/led.c **** 
   8:lib/lib_pic33e/led.c **** void config_rgb (void){
  17              	.loc 1 8 0
  18              	.set ___PA___,1
  19 000000  00 00 FA 	lnk #0
  20              	.LCFI0:
   9:lib/lib_pic33e/led.c ****     TRIS_RGB_R=0;
  21              	.loc 1 9 0
  22 000002  00 A0 A9 	bclr.b _TRISEbits,#5
  10:lib/lib_pic33e/led.c ****     TRIS_RGB_G=0;
  23              	.loc 1 10 0
  24 000004  00 60 A9 	bclr.b _TRISEbits,#3
  11:lib/lib_pic33e/led.c ****     TRIS_RGB_B=0;
  25              	.loc 1 11 0
  26 000006  00 E0 A9 	bclr.b _TRISEbits,#7
  12:lib/lib_pic33e/led.c **** }
  27              	.loc 1 12 0
  28 000008  8E 07 78 	mov w14,w15
  29 00000a  4F 07 78 	mov [--w15],w14
  30 00000c  00 40 A9 	bclr CORCON,#2
  31 00000e  00 00 06 	return 
  32              	.set ___PA___,0
  33              	.LFE0:
  34              	.size _config_rgb,.-_config_rgb
  35              	.align 2
  36              	.global _rgb_clear
  37              	.type _rgb_clear,@function
MPLAB XC16 ASSEMBLY Listing:   			page 2


  38              	_rgb_clear:
  39              	.LFB1:
  13:lib/lib_pic33e/led.c **** 
  14:lib/lib_pic33e/led.c **** void rgb_clear (void){
  40              	.loc 1 14 0
  41              	.set ___PA___,1
  42 000010  00 00 FA 	lnk #0
  43              	.LCFI1:
  15:lib/lib_pic33e/led.c **** 
  16:lib/lib_pic33e/led.c ****     LAT_RGB_G = 0;
  44              	.loc 1 16 0
  45 000012  00 60 A9 	bclr.b _LATEbits,#3
  17:lib/lib_pic33e/led.c ****     LAT_RGB_R = 0;
  46              	.loc 1 17 0
  47 000014  00 A0 A9 	bclr.b _LATEbits,#5
  18:lib/lib_pic33e/led.c ****     LAT_RGB_B = 0;
  48              	.loc 1 18 0
  49 000016  00 E0 A9 	bclr.b _LATEbits,#7
  19:lib/lib_pic33e/led.c **** }
  50              	.loc 1 19 0
  51 000018  8E 07 78 	mov w14,w15
  52 00001a  4F 07 78 	mov [--w15],w14
  53 00001c  00 40 A9 	bclr CORCON,#2
  54 00001e  00 00 06 	return 
  55              	.set ___PA___,0
  56              	.LFE1:
  57              	.size _rgb_clear,.-_rgb_clear
  58              	.align 2
  59              	.global _rgb_on
  60              	.type _rgb_on,@function
  61              	_rgb_on:
  62              	.LFB2:
  20:lib/lib_pic33e/led.c **** 
  21:lib/lib_pic33e/led.c **** 
  22:lib/lib_pic33e/led.c **** /* Turns RGB on with color 'color':
  23:lib/lib_pic33e/led.c ****    'r'- red    , 'g'- green   , 'b'- blue ,
  24:lib/lib_pic33e/led.c ****    'y'- yellow , 'm'- magenta , 'c'- cyan , 
  25:lib/lib_pic33e/led.c ****    'w'- white  , other - off 
  26:lib/lib_pic33e/led.c **** */
  27:lib/lib_pic33e/led.c **** 
  28:lib/lib_pic33e/led.c **** void rgb_on (char color){
  63              	.loc 1 28 0
  64              	.set ___PA___,1
  65 000020  02 00 FA 	lnk #2
  66              	.LCFI2:
  67              	.loc 1 28 0
  68 000022  00 4F 78 	mov.b w0,[w14]
  29:lib/lib_pic33e/led.c ****     
  30:lib/lib_pic33e/led.c ****     color = tolower(color);
  69              	.loc 1 30 0
  70 000024  1E 00 FB 	se [w14],w0
  71 000026  00 00 07 	rcall _tolower
  72 000028  00 4F 78 	mov.b w0,[w14]
  31:lib/lib_pic33e/led.c **** 
  32:lib/lib_pic33e/led.c ****     switch (color) {
  73              	.loc 1 32 0
  74 00002a  D1 06 20 	mov #109,w1
MPLAB XC16 ASSEMBLY Listing:   			page 3


  75 00002c  1E 00 FB 	se [w14],w0
  76 00002e  81 0F 50 	sub w0,w1,[w15]
  77              	.set ___BP___,0
  78 000030  00 00 32 	bra z,.L8
  79 000032  D1 06 20 	mov #109,w1
  80 000034  81 0F 50 	sub w0,w1,[w15]
  81              	.set ___BP___,0
  82 000036  00 00 3C 	bra gt,.L12
  83 000038  31 06 20 	mov #99,w1
  84 00003a  81 0F 50 	sub w0,w1,[w15]
  85              	.set ___BP___,0
  86 00003c  00 00 32 	bra z,.L6
  87 00003e  71 06 20 	mov #103,w1
  88 000040  81 0F 50 	sub w0,w1,[w15]
  89              	.set ___BP___,0
  90 000042  00 00 32 	bra z,.L7
  91 000044  21 06 20 	mov #98,w1
  92 000046  81 0F 50 	sub w0,w1,[w15]
  93              	.set ___BP___,0
  94 000048  00 00 32 	bra z,.L5
  95 00004a  00 00 37 	bra .L4
  96              	.L12:
  97 00004c  71 07 20 	mov #119,w1
  98 00004e  81 0F 50 	sub w0,w1,[w15]
  99              	.set ___BP___,0
 100 000050  00 00 32 	bra z,.L10
 101 000052  91 07 20 	mov #121,w1
 102 000054  81 0F 50 	sub w0,w1,[w15]
 103              	.set ___BP___,0
 104 000056  00 00 32 	bra z,.L11
 105 000058  21 07 20 	mov #114,w1
 106 00005a  81 0F 50 	sub w0,w1,[w15]
 107              	.set ___BP___,0
 108 00005c  00 00 3A 	bra nz,.L4
 109              	.L9:
  33:lib/lib_pic33e/led.c **** 
  34:lib/lib_pic33e/led.c ****         case 'r':
  35:lib/lib_pic33e/led.c ****             LAT_RGB_G = 0;
 110              	.loc 1 35 0
 111 00005e  00 60 A9 	bclr.b _LATEbits,#3
  36:lib/lib_pic33e/led.c ****             LAT_RGB_R = 1;
 112              	.loc 1 36 0
 113 000060  00 A0 A8 	bset.b _LATEbits,#5
  37:lib/lib_pic33e/led.c ****             LAT_RGB_B = 0;
 114              	.loc 1 37 0
 115 000062  00 E0 A9 	bclr.b _LATEbits,#7
  38:lib/lib_pic33e/led.c ****             break;
 116              	.loc 1 38 0
 117 000064  00 00 37 	bra .L3
 118              	.L7:
  39:lib/lib_pic33e/led.c ****     
  40:lib/lib_pic33e/led.c ****         case 'g':
  41:lib/lib_pic33e/led.c ****             LAT_RGB_G = 1;
 119              	.loc 1 41 0
 120 000066  00 60 A8 	bset.b _LATEbits,#3
  42:lib/lib_pic33e/led.c ****             LAT_RGB_R = 0;
 121              	.loc 1 42 0
MPLAB XC16 ASSEMBLY Listing:   			page 4


 122 000068  00 A0 A9 	bclr.b _LATEbits,#5
  43:lib/lib_pic33e/led.c ****             LAT_RGB_B = 0;
 123              	.loc 1 43 0
 124 00006a  00 E0 A9 	bclr.b _LATEbits,#7
  44:lib/lib_pic33e/led.c ****             break;
 125              	.loc 1 44 0
 126 00006c  00 00 37 	bra .L3
 127              	.L5:
  45:lib/lib_pic33e/led.c **** 
  46:lib/lib_pic33e/led.c ****         case 'b':
  47:lib/lib_pic33e/led.c ****             LAT_RGB_G = 0;
 128              	.loc 1 47 0
 129 00006e  00 60 A9 	bclr.b _LATEbits,#3
  48:lib/lib_pic33e/led.c ****             LAT_RGB_R = 0;
 130              	.loc 1 48 0
 131 000070  00 A0 A9 	bclr.b _LATEbits,#5
  49:lib/lib_pic33e/led.c ****             LAT_RGB_B = 1;
 132              	.loc 1 49 0
 133 000072  00 E0 A8 	bset.b _LATEbits,#7
  50:lib/lib_pic33e/led.c ****             break;
 134              	.loc 1 50 0
 135 000074  00 00 37 	bra .L3
 136              	.L11:
  51:lib/lib_pic33e/led.c **** 
  52:lib/lib_pic33e/led.c ****         case 'y':
  53:lib/lib_pic33e/led.c ****             LAT_RGB_G = 1;
 137              	.loc 1 53 0
 138 000076  00 60 A8 	bset.b _LATEbits,#3
  54:lib/lib_pic33e/led.c ****             LAT_RGB_R = 1;
 139              	.loc 1 54 0
 140 000078  00 A0 A8 	bset.b _LATEbits,#5
  55:lib/lib_pic33e/led.c ****             LAT_RGB_B = 0;
 141              	.loc 1 55 0
 142 00007a  00 E0 A9 	bclr.b _LATEbits,#7
  56:lib/lib_pic33e/led.c ****             break;
 143              	.loc 1 56 0
 144 00007c  00 00 37 	bra .L3
 145              	.L8:
  57:lib/lib_pic33e/led.c **** 
  58:lib/lib_pic33e/led.c ****         case 'm':
  59:lib/lib_pic33e/led.c ****             LAT_RGB_G = 0;
 146              	.loc 1 59 0
 147 00007e  00 60 A9 	bclr.b _LATEbits,#3
  60:lib/lib_pic33e/led.c ****             LAT_RGB_R = 1;
 148              	.loc 1 60 0
 149 000080  00 A0 A8 	bset.b _LATEbits,#5
  61:lib/lib_pic33e/led.c ****             LAT_RGB_B = 1;
 150              	.loc 1 61 0
 151 000082  00 E0 A8 	bset.b _LATEbits,#7
  62:lib/lib_pic33e/led.c ****             break;
 152              	.loc 1 62 0
 153 000084  00 00 37 	bra .L3
 154              	.L6:
  63:lib/lib_pic33e/led.c **** 
  64:lib/lib_pic33e/led.c ****         case 'c':
  65:lib/lib_pic33e/led.c ****             LAT_RGB_G = 1;
 155              	.loc 1 65 0
MPLAB XC16 ASSEMBLY Listing:   			page 5


 156 000086  00 60 A8 	bset.b _LATEbits,#3
  66:lib/lib_pic33e/led.c ****             LAT_RGB_R = 0;
 157              	.loc 1 66 0
 158 000088  00 A0 A9 	bclr.b _LATEbits,#5
  67:lib/lib_pic33e/led.c ****             LAT_RGB_B = 1;
 159              	.loc 1 67 0
 160 00008a  00 E0 A8 	bset.b _LATEbits,#7
  68:lib/lib_pic33e/led.c ****             break;
 161              	.loc 1 68 0
 162 00008c  00 00 37 	bra .L3
 163              	.L10:
  69:lib/lib_pic33e/led.c **** 
  70:lib/lib_pic33e/led.c ****         case 'w':
  71:lib/lib_pic33e/led.c ****             LAT_RGB_G = 1;
 164              	.loc 1 71 0
 165 00008e  00 60 A8 	bset.b _LATEbits,#3
  72:lib/lib_pic33e/led.c ****             LAT_RGB_R = 1;
 166              	.loc 1 72 0
 167 000090  00 A0 A8 	bset.b _LATEbits,#5
  73:lib/lib_pic33e/led.c ****             LAT_RGB_B = 1;
 168              	.loc 1 73 0
 169 000092  00 E0 A8 	bset.b _LATEbits,#7
  74:lib/lib_pic33e/led.c ****             break;
 170              	.loc 1 74 0
 171 000094  00 00 37 	bra .L3
 172              	.L4:
  75:lib/lib_pic33e/led.c **** 
  76:lib/lib_pic33e/led.c ****         default:
  77:lib/lib_pic33e/led.c ****             LAT_RGB_G = 0;
 173              	.loc 1 77 0
 174 000096  00 60 A9 	bclr.b _LATEbits,#3
  78:lib/lib_pic33e/led.c ****             LAT_RGB_R = 0;
 175              	.loc 1 78 0
 176 000098  00 A0 A9 	bclr.b _LATEbits,#5
  79:lib/lib_pic33e/led.c ****             LAT_RGB_B = 0;
 177              	.loc 1 79 0
 178 00009a  00 E0 A9 	bclr.b _LATEbits,#7
 179              	.L3:
  80:lib/lib_pic33e/led.c ****             break;
  81:lib/lib_pic33e/led.c ****         }
  82:lib/lib_pic33e/led.c **** }
 180              	.loc 1 82 0
 181 00009c  8E 07 78 	mov w14,w15
 182 00009e  4F 07 78 	mov [--w15],w14
 183 0000a0  00 40 A9 	bclr CORCON,#2
 184 0000a2  00 00 06 	return 
 185              	.set ___PA___,0
 186              	.LFE2:
 187              	.size _rgb_on,.-_rgb_on
 188              	.section .debug_frame,info
 189                 	.Lframe0:
 190 0000 10 00 00 00 	.4byte .LECIE0-.LSCIE0
 191                 	.LSCIE0:
 192 0004 FF FF FF FF 	.4byte 0xffffffff
 193 0008 01          	.byte 0x1
 194 0009 00          	.byte 0
 195 000a 01          	.uleb128 0x1
MPLAB XC16 ASSEMBLY Listing:   			page 6


 196 000b 02          	.sleb128 2
 197 000c 25          	.byte 0x25
 198 000d 12          	.byte 0x12
 199 000e 0F          	.uleb128 0xf
 200 000f 7E          	.sleb128 -2
 201 0010 09          	.byte 0x9
 202 0011 25          	.uleb128 0x25
 203 0012 0F          	.uleb128 0xf
 204 0013 00          	.align 4
 205                 	.LECIE0:
 206                 	.LSFDE0:
 207 0014 18 00 00 00 	.4byte .LEFDE0-.LASFDE0
 208                 	.LASFDE0:
 209 0018 00 00 00 00 	.4byte .Lframe0
 210 001c 00 00 00 00 	.4byte .LFB0
 211 0020 10 00 00 00 	.4byte .LFE0-.LFB0
 212 0024 04          	.byte 0x4
 213 0025 02 00 00 00 	.4byte .LCFI0-.LFB0
 214 0029 13          	.byte 0x13
 215 002a 7D          	.sleb128 -3
 216 002b 0D          	.byte 0xd
 217 002c 0E          	.uleb128 0xe
 218 002d 8E          	.byte 0x8e
 219 002e 02          	.uleb128 0x2
 220 002f 00          	.align 4
 221                 	.LEFDE0:
 222                 	.LSFDE2:
 223 0030 18 00 00 00 	.4byte .LEFDE2-.LASFDE2
 224                 	.LASFDE2:
 225 0034 00 00 00 00 	.4byte .Lframe0
 226 0038 00 00 00 00 	.4byte .LFB1
 227 003c 10 00 00 00 	.4byte .LFE1-.LFB1
 228 0040 04          	.byte 0x4
 229 0041 02 00 00 00 	.4byte .LCFI1-.LFB1
 230 0045 13          	.byte 0x13
 231 0046 7D          	.sleb128 -3
 232 0047 0D          	.byte 0xd
 233 0048 0E          	.uleb128 0xe
 234 0049 8E          	.byte 0x8e
 235 004a 02          	.uleb128 0x2
 236 004b 00          	.align 4
 237                 	.LEFDE2:
 238                 	.LSFDE4:
 239 004c 18 00 00 00 	.4byte .LEFDE4-.LASFDE4
 240                 	.LASFDE4:
 241 0050 00 00 00 00 	.4byte .Lframe0
 242 0054 00 00 00 00 	.4byte .LFB2
 243 0058 84 00 00 00 	.4byte .LFE2-.LFB2
 244 005c 04          	.byte 0x4
 245 005d 02 00 00 00 	.4byte .LCFI2-.LFB2
 246 0061 13          	.byte 0x13
 247 0062 7D          	.sleb128 -3
 248 0063 0D          	.byte 0xd
 249 0064 0E          	.uleb128 0xe
 250 0065 8E          	.byte 0x8e
 251 0066 02          	.uleb128 0x2
 252 0067 00          	.align 4
MPLAB XC16 ASSEMBLY Listing:   			page 7


 253                 	.LEFDE4:
 254                 	.section .text,code
 255              	.Letext0:
 256              	.file 2 "/opt/microchip/xc16/v1.36/bin/bin/../../support/dsPIC33E/h/p33EP256MU806.h"
 257              	.file 3 "/opt/microchip/xc16/v1.36/bin/bin/../../include/lega-c/stdint.h"
 258              	.section .debug_info,info
 259 0000 74 03 00 00 	.4byte 0x374
 260 0004 02 00       	.2byte 0x2
 261 0006 00 00 00 00 	.4byte .Ldebug_abbrev0
 262 000a 04          	.byte 0x4
 263 000b 01          	.uleb128 0x1
 264 000c 47 4E 55 20 	.asciz "GNU C 4.5.1 (XC16, Microchip v1.36) (A) Build date: Dec  5 2018"
 264      43 20 34 2E 
 264      35 2E 31 20 
 264      28 58 43 31 
 264      36 2C 20 4D 
 264      69 63 72 6F 
 264      63 68 69 70 
 264      20 76 31 2E 
 264      33 36 29 20 
 265 004c 01          	.byte 0x1
 266 004d 6C 69 62 2F 	.asciz "lib/lib_pic33e/led.c"
 266      6C 69 62 5F 
 266      70 69 63 33 
 266      33 65 2F 6C 
 266      65 64 2E 63 
 266      00 
 267 0062 2F 68 6F 6D 	.asciz "/home/user/Documents/FST/Programming/project_sub-zero"
 267      65 2F 75 73 
 267      65 72 2F 44 
 267      6F 63 75 6D 
 267      65 6E 74 73 
 267      2F 46 53 54 
 267      2F 50 72 6F 
 267      67 72 61 6D 
 267      6D 69 6E 67 
 268 0098 00 00 00 00 	.4byte .Ltext0
 269 009c 00 00 00 00 	.4byte .Letext0
 270 00a0 00 00 00 00 	.4byte .Ldebug_line0
 271 00a4 02          	.uleb128 0x2
 272 00a5 01          	.byte 0x1
 273 00a6 06          	.byte 0x6
 274 00a7 73 69 67 6E 	.asciz "signed char"
 274      65 64 20 63 
 274      68 61 72 00 
 275 00b3 02          	.uleb128 0x2
 276 00b4 02          	.byte 0x2
 277 00b5 05          	.byte 0x5
 278 00b6 69 6E 74 00 	.asciz "int"
 279 00ba 02          	.uleb128 0x2
 280 00bb 04          	.byte 0x4
 281 00bc 05          	.byte 0x5
 282 00bd 6C 6F 6E 67 	.asciz "long int"
 282      20 69 6E 74 
 282      00 
 283 00c6 02          	.uleb128 0x2
 284 00c7 08          	.byte 0x8
MPLAB XC16 ASSEMBLY Listing:   			page 8


 285 00c8 05          	.byte 0x5
 286 00c9 6C 6F 6E 67 	.asciz "long long int"
 286      20 6C 6F 6E 
 286      67 20 69 6E 
 286      74 00 
 287 00d7 02          	.uleb128 0x2
 288 00d8 01          	.byte 0x1
 289 00d9 08          	.byte 0x8
 290 00da 75 6E 73 69 	.asciz "unsigned char"
 290      67 6E 65 64 
 290      20 63 68 61 
 290      72 00 
 291 00e8 03          	.uleb128 0x3
 292 00e9 75 69 6E 74 	.asciz "uint16_t"
 292      31 36 5F 74 
 292      00 
 293 00f2 03          	.byte 0x3
 294 00f3 31          	.byte 0x31
 295 00f4 F8 00 00 00 	.4byte 0xf8
 296 00f8 02          	.uleb128 0x2
 297 00f9 02          	.byte 0x2
 298 00fa 07          	.byte 0x7
 299 00fb 75 6E 73 69 	.asciz "unsigned int"
 299      67 6E 65 64 
 299      20 69 6E 74 
 299      00 
 300 0108 02          	.uleb128 0x2
 301 0109 04          	.byte 0x4
 302 010a 07          	.byte 0x7
 303 010b 6C 6F 6E 67 	.asciz "long unsigned int"
 303      20 75 6E 73 
 303      69 67 6E 65 
 303      64 20 69 6E 
 303      74 00 
 304 011d 02          	.uleb128 0x2
 305 011e 08          	.byte 0x8
 306 011f 07          	.byte 0x7
 307 0120 6C 6F 6E 67 	.asciz "long long unsigned int"
 307      20 6C 6F 6E 
 307      67 20 75 6E 
 307      73 69 67 6E 
 307      65 64 20 69 
 307      6E 74 00 
 308 0137 04          	.uleb128 0x4
 309 0138 74 61 67 54 	.asciz "tagTRISEBITS"
 309      52 49 53 45 
 309      42 49 54 53 
 309      00 
 310 0145 02          	.byte 0x2
 311 0146 02          	.byte 0x2
 312 0147 A4 3B       	.2byte 0x3ba4
 313 0149 F6 01 00 00 	.4byte 0x1f6
 314 014d 05          	.uleb128 0x5
 315 014e 54 52 49 53 	.asciz "TRISE0"
 315      45 30 00 
 316 0155 02          	.byte 0x2
 317 0156 A5 3B       	.2byte 0x3ba5
MPLAB XC16 ASSEMBLY Listing:   			page 9


 318 0158 E8 00 00 00 	.4byte 0xe8
 319 015c 02          	.byte 0x2
 320 015d 01          	.byte 0x1
 321 015e 0F          	.byte 0xf
 322 015f 02          	.byte 0x2
 323 0160 23          	.byte 0x23
 324 0161 00          	.uleb128 0x0
 325 0162 05          	.uleb128 0x5
 326 0163 54 52 49 53 	.asciz "TRISE1"
 326      45 31 00 
 327 016a 02          	.byte 0x2
 328 016b A6 3B       	.2byte 0x3ba6
 329 016d E8 00 00 00 	.4byte 0xe8
 330 0171 02          	.byte 0x2
 331 0172 01          	.byte 0x1
 332 0173 0E          	.byte 0xe
 333 0174 02          	.byte 0x2
 334 0175 23          	.byte 0x23
 335 0176 00          	.uleb128 0x0
 336 0177 05          	.uleb128 0x5
 337 0178 54 52 49 53 	.asciz "TRISE2"
 337      45 32 00 
 338 017f 02          	.byte 0x2
 339 0180 A7 3B       	.2byte 0x3ba7
 340 0182 E8 00 00 00 	.4byte 0xe8
 341 0186 02          	.byte 0x2
 342 0187 01          	.byte 0x1
 343 0188 0D          	.byte 0xd
 344 0189 02          	.byte 0x2
 345 018a 23          	.byte 0x23
 346 018b 00          	.uleb128 0x0
 347 018c 05          	.uleb128 0x5
 348 018d 54 52 49 53 	.asciz "TRISE3"
 348      45 33 00 
 349 0194 02          	.byte 0x2
 350 0195 A8 3B       	.2byte 0x3ba8
 351 0197 E8 00 00 00 	.4byte 0xe8
 352 019b 02          	.byte 0x2
 353 019c 01          	.byte 0x1
 354 019d 0C          	.byte 0xc
 355 019e 02          	.byte 0x2
 356 019f 23          	.byte 0x23
 357 01a0 00          	.uleb128 0x0
 358 01a1 05          	.uleb128 0x5
 359 01a2 54 52 49 53 	.asciz "TRISE4"
 359      45 34 00 
 360 01a9 02          	.byte 0x2
 361 01aa A9 3B       	.2byte 0x3ba9
 362 01ac E8 00 00 00 	.4byte 0xe8
 363 01b0 02          	.byte 0x2
 364 01b1 01          	.byte 0x1
 365 01b2 0B          	.byte 0xb
 366 01b3 02          	.byte 0x2
 367 01b4 23          	.byte 0x23
 368 01b5 00          	.uleb128 0x0
 369 01b6 05          	.uleb128 0x5
 370 01b7 54 52 49 53 	.asciz "TRISE5"
MPLAB XC16 ASSEMBLY Listing:   			page 10


 370      45 35 00 
 371 01be 02          	.byte 0x2
 372 01bf AA 3B       	.2byte 0x3baa
 373 01c1 E8 00 00 00 	.4byte 0xe8
 374 01c5 02          	.byte 0x2
 375 01c6 01          	.byte 0x1
 376 01c7 0A          	.byte 0xa
 377 01c8 02          	.byte 0x2
 378 01c9 23          	.byte 0x23
 379 01ca 00          	.uleb128 0x0
 380 01cb 05          	.uleb128 0x5
 381 01cc 54 52 49 53 	.asciz "TRISE6"
 381      45 36 00 
 382 01d3 02          	.byte 0x2
 383 01d4 AB 3B       	.2byte 0x3bab
 384 01d6 E8 00 00 00 	.4byte 0xe8
 385 01da 02          	.byte 0x2
 386 01db 01          	.byte 0x1
 387 01dc 09          	.byte 0x9
 388 01dd 02          	.byte 0x2
 389 01de 23          	.byte 0x23
 390 01df 00          	.uleb128 0x0
 391 01e0 05          	.uleb128 0x5
 392 01e1 54 52 49 53 	.asciz "TRISE7"
 392      45 37 00 
 393 01e8 02          	.byte 0x2
 394 01e9 AC 3B       	.2byte 0x3bac
 395 01eb E8 00 00 00 	.4byte 0xe8
 396 01ef 02          	.byte 0x2
 397 01f0 01          	.byte 0x1
 398 01f1 08          	.byte 0x8
 399 01f2 02          	.byte 0x2
 400 01f3 23          	.byte 0x23
 401 01f4 00          	.uleb128 0x0
 402 01f5 00          	.byte 0x0
 403 01f6 06          	.uleb128 0x6
 404 01f7 54 52 49 53 	.asciz "TRISEBITS"
 404      45 42 49 54 
 404      53 00 
 405 0201 02          	.byte 0x2
 406 0202 AD 3B       	.2byte 0x3bad
 407 0204 37 01 00 00 	.4byte 0x137
 408 0208 04          	.uleb128 0x4
 409 0209 74 61 67 4C 	.asciz "tagLATEBITS"
 409      41 54 45 42 
 409      49 54 53 00 
 410 0215 02          	.byte 0x2
 411 0216 02          	.byte 0x2
 412 0217 C0 3B       	.2byte 0x3bc0
 413 0219 BE 02 00 00 	.4byte 0x2be
 414 021d 05          	.uleb128 0x5
 415 021e 4C 41 54 45 	.asciz "LATE0"
 415      30 00 
 416 0224 02          	.byte 0x2
 417 0225 C1 3B       	.2byte 0x3bc1
 418 0227 E8 00 00 00 	.4byte 0xe8
 419 022b 02          	.byte 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 11


 420 022c 01          	.byte 0x1
 421 022d 0F          	.byte 0xf
 422 022e 02          	.byte 0x2
 423 022f 23          	.byte 0x23
 424 0230 00          	.uleb128 0x0
 425 0231 05          	.uleb128 0x5
 426 0232 4C 41 54 45 	.asciz "LATE1"
 426      31 00 
 427 0238 02          	.byte 0x2
 428 0239 C2 3B       	.2byte 0x3bc2
 429 023b E8 00 00 00 	.4byte 0xe8
 430 023f 02          	.byte 0x2
 431 0240 01          	.byte 0x1
 432 0241 0E          	.byte 0xe
 433 0242 02          	.byte 0x2
 434 0243 23          	.byte 0x23
 435 0244 00          	.uleb128 0x0
 436 0245 05          	.uleb128 0x5
 437 0246 4C 41 54 45 	.asciz "LATE2"
 437      32 00 
 438 024c 02          	.byte 0x2
 439 024d C3 3B       	.2byte 0x3bc3
 440 024f E8 00 00 00 	.4byte 0xe8
 441 0253 02          	.byte 0x2
 442 0254 01          	.byte 0x1
 443 0255 0D          	.byte 0xd
 444 0256 02          	.byte 0x2
 445 0257 23          	.byte 0x23
 446 0258 00          	.uleb128 0x0
 447 0259 05          	.uleb128 0x5
 448 025a 4C 41 54 45 	.asciz "LATE3"
 448      33 00 
 449 0260 02          	.byte 0x2
 450 0261 C4 3B       	.2byte 0x3bc4
 451 0263 E8 00 00 00 	.4byte 0xe8
 452 0267 02          	.byte 0x2
 453 0268 01          	.byte 0x1
 454 0269 0C          	.byte 0xc
 455 026a 02          	.byte 0x2
 456 026b 23          	.byte 0x23
 457 026c 00          	.uleb128 0x0
 458 026d 05          	.uleb128 0x5
 459 026e 4C 41 54 45 	.asciz "LATE4"
 459      34 00 
 460 0274 02          	.byte 0x2
 461 0275 C5 3B       	.2byte 0x3bc5
 462 0277 E8 00 00 00 	.4byte 0xe8
 463 027b 02          	.byte 0x2
 464 027c 01          	.byte 0x1
 465 027d 0B          	.byte 0xb
 466 027e 02          	.byte 0x2
 467 027f 23          	.byte 0x23
 468 0280 00          	.uleb128 0x0
 469 0281 05          	.uleb128 0x5
 470 0282 4C 41 54 45 	.asciz "LATE5"
 470      35 00 
 471 0288 02          	.byte 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 12


 472 0289 C6 3B       	.2byte 0x3bc6
 473 028b E8 00 00 00 	.4byte 0xe8
 474 028f 02          	.byte 0x2
 475 0290 01          	.byte 0x1
 476 0291 0A          	.byte 0xa
 477 0292 02          	.byte 0x2
 478 0293 23          	.byte 0x23
 479 0294 00          	.uleb128 0x0
 480 0295 05          	.uleb128 0x5
 481 0296 4C 41 54 45 	.asciz "LATE6"
 481      36 00 
 482 029c 02          	.byte 0x2
 483 029d C7 3B       	.2byte 0x3bc7
 484 029f E8 00 00 00 	.4byte 0xe8
 485 02a3 02          	.byte 0x2
 486 02a4 01          	.byte 0x1
 487 02a5 09          	.byte 0x9
 488 02a6 02          	.byte 0x2
 489 02a7 23          	.byte 0x23
 490 02a8 00          	.uleb128 0x0
 491 02a9 05          	.uleb128 0x5
 492 02aa 4C 41 54 45 	.asciz "LATE7"
 492      37 00 
 493 02b0 02          	.byte 0x2
 494 02b1 C8 3B       	.2byte 0x3bc8
 495 02b3 E8 00 00 00 	.4byte 0xe8
 496 02b7 02          	.byte 0x2
 497 02b8 01          	.byte 0x1
 498 02b9 08          	.byte 0x8
 499 02ba 02          	.byte 0x2
 500 02bb 23          	.byte 0x23
 501 02bc 00          	.uleb128 0x0
 502 02bd 00          	.byte 0x0
 503 02be 06          	.uleb128 0x6
 504 02bf 4C 41 54 45 	.asciz "LATEBITS"
 504      42 49 54 53 
 504      00 
 505 02c8 02          	.byte 0x2
 506 02c9 C9 3B       	.2byte 0x3bc9
 507 02cb 08 02 00 00 	.4byte 0x208
 508 02cf 07          	.uleb128 0x7
 509 02d0 01          	.byte 0x1
 510 02d1 63 6F 6E 66 	.asciz "config_rgb"
 510      69 67 5F 72 
 510      67 62 00 
 511 02dc 01          	.byte 0x1
 512 02dd 08          	.byte 0x8
 513 02de 01          	.byte 0x1
 514 02df 00 00 00 00 	.4byte .LFB0
 515 02e3 00 00 00 00 	.4byte .LFE0
 516 02e7 01          	.byte 0x1
 517 02e8 5E          	.byte 0x5e
 518 02e9 07          	.uleb128 0x7
 519 02ea 01          	.byte 0x1
 520 02eb 72 67 62 5F 	.asciz "rgb_clear"
 520      63 6C 65 61 
 520      72 00 
MPLAB XC16 ASSEMBLY Listing:   			page 13


 521 02f5 01          	.byte 0x1
 522 02f6 0E          	.byte 0xe
 523 02f7 01          	.byte 0x1
 524 02f8 00 00 00 00 	.4byte .LFB1
 525 02fc 00 00 00 00 	.4byte .LFE1
 526 0300 01          	.byte 0x1
 527 0301 5E          	.byte 0x5e
 528 0302 08          	.uleb128 0x8
 529 0303 01          	.byte 0x1
 530 0304 72 67 62 5F 	.asciz "rgb_on"
 530      6F 6E 00 
 531 030b 01          	.byte 0x1
 532 030c 1C          	.byte 0x1c
 533 030d 01          	.byte 0x1
 534 030e 00 00 00 00 	.4byte .LFB2
 535 0312 00 00 00 00 	.4byte .LFE2
 536 0316 01          	.byte 0x1
 537 0317 5E          	.byte 0x5e
 538 0318 2D 03 00 00 	.4byte 0x32d
 539 031c 09          	.uleb128 0x9
 540 031d 63 6F 6C 6F 	.asciz "color"
 540      72 00 
 541 0323 01          	.byte 0x1
 542 0324 1C          	.byte 0x1c
 543 0325 2D 03 00 00 	.4byte 0x32d
 544 0329 02          	.byte 0x2
 545 032a 7E          	.byte 0x7e
 546 032b 00          	.sleb128 0
 547 032c 00          	.byte 0x0
 548 032d 02          	.uleb128 0x2
 549 032e 01          	.byte 0x1
 550 032f 06          	.byte 0x6
 551 0330 63 68 61 72 	.asciz "char"
 551      00 
 552 0335 0A          	.uleb128 0xa
 553 0336 00 00 00 00 	.4byte .LASF0
 554 033a 02          	.byte 0x2
 555 033b AE 3B       	.2byte 0x3bae
 556 033d 43 03 00 00 	.4byte 0x343
 557 0341 01          	.byte 0x1
 558 0342 01          	.byte 0x1
 559 0343 0B          	.uleb128 0xb
 560 0344 F6 01 00 00 	.4byte 0x1f6
 561 0348 0A          	.uleb128 0xa
 562 0349 00 00 00 00 	.4byte .LASF1
 563 034d 02          	.byte 0x2
 564 034e CA 3B       	.2byte 0x3bca
 565 0350 56 03 00 00 	.4byte 0x356
 566 0354 01          	.byte 0x1
 567 0355 01          	.byte 0x1
 568 0356 0B          	.uleb128 0xb
 569 0357 BE 02 00 00 	.4byte 0x2be
 570 035b 0A          	.uleb128 0xa
 571 035c 00 00 00 00 	.4byte .LASF0
 572 0360 02          	.byte 0x2
 573 0361 AE 3B       	.2byte 0x3bae
 574 0363 43 03 00 00 	.4byte 0x343
MPLAB XC16 ASSEMBLY Listing:   			page 14


 575 0367 01          	.byte 0x1
 576 0368 01          	.byte 0x1
 577 0369 0A          	.uleb128 0xa
 578 036a 00 00 00 00 	.4byte .LASF1
 579 036e 02          	.byte 0x2
 580 036f CA 3B       	.2byte 0x3bca
 581 0371 56 03 00 00 	.4byte 0x356
 582 0375 01          	.byte 0x1
 583 0376 01          	.byte 0x1
 584 0377 00          	.byte 0x0
 585                 	.section .debug_abbrev,info
 586 0000 01          	.uleb128 0x1
 587 0001 11          	.uleb128 0x11
 588 0002 01          	.byte 0x1
 589 0003 25          	.uleb128 0x25
 590 0004 08          	.uleb128 0x8
 591 0005 13          	.uleb128 0x13
 592 0006 0B          	.uleb128 0xb
 593 0007 03          	.uleb128 0x3
 594 0008 08          	.uleb128 0x8
 595 0009 1B          	.uleb128 0x1b
 596 000a 08          	.uleb128 0x8
 597 000b 11          	.uleb128 0x11
 598 000c 01          	.uleb128 0x1
 599 000d 12          	.uleb128 0x12
 600 000e 01          	.uleb128 0x1
 601 000f 10          	.uleb128 0x10
 602 0010 06          	.uleb128 0x6
 603 0011 00          	.byte 0x0
 604 0012 00          	.byte 0x0
 605 0013 02          	.uleb128 0x2
 606 0014 24          	.uleb128 0x24
 607 0015 00          	.byte 0x0
 608 0016 0B          	.uleb128 0xb
 609 0017 0B          	.uleb128 0xb
 610 0018 3E          	.uleb128 0x3e
 611 0019 0B          	.uleb128 0xb
 612 001a 03          	.uleb128 0x3
 613 001b 08          	.uleb128 0x8
 614 001c 00          	.byte 0x0
 615 001d 00          	.byte 0x0
 616 001e 03          	.uleb128 0x3
 617 001f 16          	.uleb128 0x16
 618 0020 00          	.byte 0x0
 619 0021 03          	.uleb128 0x3
 620 0022 08          	.uleb128 0x8
 621 0023 3A          	.uleb128 0x3a
 622 0024 0B          	.uleb128 0xb
 623 0025 3B          	.uleb128 0x3b
 624 0026 0B          	.uleb128 0xb
 625 0027 49          	.uleb128 0x49
 626 0028 13          	.uleb128 0x13
 627 0029 00          	.byte 0x0
 628 002a 00          	.byte 0x0
 629 002b 04          	.uleb128 0x4
 630 002c 13          	.uleb128 0x13
 631 002d 01          	.byte 0x1
MPLAB XC16 ASSEMBLY Listing:   			page 15


 632 002e 03          	.uleb128 0x3
 633 002f 08          	.uleb128 0x8
 634 0030 0B          	.uleb128 0xb
 635 0031 0B          	.uleb128 0xb
 636 0032 3A          	.uleb128 0x3a
 637 0033 0B          	.uleb128 0xb
 638 0034 3B          	.uleb128 0x3b
 639 0035 05          	.uleb128 0x5
 640 0036 01          	.uleb128 0x1
 641 0037 13          	.uleb128 0x13
 642 0038 00          	.byte 0x0
 643 0039 00          	.byte 0x0
 644 003a 05          	.uleb128 0x5
 645 003b 0D          	.uleb128 0xd
 646 003c 00          	.byte 0x0
 647 003d 03          	.uleb128 0x3
 648 003e 08          	.uleb128 0x8
 649 003f 3A          	.uleb128 0x3a
 650 0040 0B          	.uleb128 0xb
 651 0041 3B          	.uleb128 0x3b
 652 0042 05          	.uleb128 0x5
 653 0043 49          	.uleb128 0x49
 654 0044 13          	.uleb128 0x13
 655 0045 0B          	.uleb128 0xb
 656 0046 0B          	.uleb128 0xb
 657 0047 0D          	.uleb128 0xd
 658 0048 0B          	.uleb128 0xb
 659 0049 0C          	.uleb128 0xc
 660 004a 0B          	.uleb128 0xb
 661 004b 38          	.uleb128 0x38
 662 004c 0A          	.uleb128 0xa
 663 004d 00          	.byte 0x0
 664 004e 00          	.byte 0x0
 665 004f 06          	.uleb128 0x6
 666 0050 16          	.uleb128 0x16
 667 0051 00          	.byte 0x0
 668 0052 03          	.uleb128 0x3
 669 0053 08          	.uleb128 0x8
 670 0054 3A          	.uleb128 0x3a
 671 0055 0B          	.uleb128 0xb
 672 0056 3B          	.uleb128 0x3b
 673 0057 05          	.uleb128 0x5
 674 0058 49          	.uleb128 0x49
 675 0059 13          	.uleb128 0x13
 676 005a 00          	.byte 0x0
 677 005b 00          	.byte 0x0
 678 005c 07          	.uleb128 0x7
 679 005d 2E          	.uleb128 0x2e
 680 005e 00          	.byte 0x0
 681 005f 3F          	.uleb128 0x3f
 682 0060 0C          	.uleb128 0xc
 683 0061 03          	.uleb128 0x3
 684 0062 08          	.uleb128 0x8
 685 0063 3A          	.uleb128 0x3a
 686 0064 0B          	.uleb128 0xb
 687 0065 3B          	.uleb128 0x3b
 688 0066 0B          	.uleb128 0xb
MPLAB XC16 ASSEMBLY Listing:   			page 16


 689 0067 27          	.uleb128 0x27
 690 0068 0C          	.uleb128 0xc
 691 0069 11          	.uleb128 0x11
 692 006a 01          	.uleb128 0x1
 693 006b 12          	.uleb128 0x12
 694 006c 01          	.uleb128 0x1
 695 006d 40          	.uleb128 0x40
 696 006e 0A          	.uleb128 0xa
 697 006f 00          	.byte 0x0
 698 0070 00          	.byte 0x0
 699 0071 08          	.uleb128 0x8
 700 0072 2E          	.uleb128 0x2e
 701 0073 01          	.byte 0x1
 702 0074 3F          	.uleb128 0x3f
 703 0075 0C          	.uleb128 0xc
 704 0076 03          	.uleb128 0x3
 705 0077 08          	.uleb128 0x8
 706 0078 3A          	.uleb128 0x3a
 707 0079 0B          	.uleb128 0xb
 708 007a 3B          	.uleb128 0x3b
 709 007b 0B          	.uleb128 0xb
 710 007c 27          	.uleb128 0x27
 711 007d 0C          	.uleb128 0xc
 712 007e 11          	.uleb128 0x11
 713 007f 01          	.uleb128 0x1
 714 0080 12          	.uleb128 0x12
 715 0081 01          	.uleb128 0x1
 716 0082 40          	.uleb128 0x40
 717 0083 0A          	.uleb128 0xa
 718 0084 01          	.uleb128 0x1
 719 0085 13          	.uleb128 0x13
 720 0086 00          	.byte 0x0
 721 0087 00          	.byte 0x0
 722 0088 09          	.uleb128 0x9
 723 0089 05          	.uleb128 0x5
 724 008a 00          	.byte 0x0
 725 008b 03          	.uleb128 0x3
 726 008c 08          	.uleb128 0x8
 727 008d 3A          	.uleb128 0x3a
 728 008e 0B          	.uleb128 0xb
 729 008f 3B          	.uleb128 0x3b
 730 0090 0B          	.uleb128 0xb
 731 0091 49          	.uleb128 0x49
 732 0092 13          	.uleb128 0x13
 733 0093 02          	.uleb128 0x2
 734 0094 0A          	.uleb128 0xa
 735 0095 00          	.byte 0x0
 736 0096 00          	.byte 0x0
 737 0097 0A          	.uleb128 0xa
 738 0098 34          	.uleb128 0x34
 739 0099 00          	.byte 0x0
 740 009a 03          	.uleb128 0x3
 741 009b 0E          	.uleb128 0xe
 742 009c 3A          	.uleb128 0x3a
 743 009d 0B          	.uleb128 0xb
 744 009e 3B          	.uleb128 0x3b
 745 009f 05          	.uleb128 0x5
MPLAB XC16 ASSEMBLY Listing:   			page 17


 746 00a0 49          	.uleb128 0x49
 747 00a1 13          	.uleb128 0x13
 748 00a2 3F          	.uleb128 0x3f
 749 00a3 0C          	.uleb128 0xc
 750 00a4 3C          	.uleb128 0x3c
 751 00a5 0C          	.uleb128 0xc
 752 00a6 00          	.byte 0x0
 753 00a7 00          	.byte 0x0
 754 00a8 0B          	.uleb128 0xb
 755 00a9 35          	.uleb128 0x35
 756 00aa 00          	.byte 0x0
 757 00ab 49          	.uleb128 0x49
 758 00ac 13          	.uleb128 0x13
 759 00ad 00          	.byte 0x0
 760 00ae 00          	.byte 0x0
 761 00af 00          	.byte 0x0
 762                 	.section .debug_pubnames,info
 763 0000 36 00 00 00 	.4byte 0x36
 764 0004 02 00       	.2byte 0x2
 765 0006 00 00 00 00 	.4byte .Ldebug_info0
 766 000a 78 03 00 00 	.4byte 0x378
 767 000e CF 02 00 00 	.4byte 0x2cf
 768 0012 63 6F 6E 66 	.asciz "config_rgb"
 768      69 67 5F 72 
 768      67 62 00 
 769 001d E9 02 00 00 	.4byte 0x2e9
 770 0021 72 67 62 5F 	.asciz "rgb_clear"
 770      63 6C 65 61 
 770      72 00 
 771 002b 02 03 00 00 	.4byte 0x302
 772 002f 72 67 62 5F 	.asciz "rgb_on"
 772      6F 6E 00 
 773 0036 00 00 00 00 	.4byte 0x0
 774                 	.section .debug_pubtypes,info
 775 0000 57 00 00 00 	.4byte 0x57
 776 0004 02 00       	.2byte 0x2
 777 0006 00 00 00 00 	.4byte .Ldebug_info0
 778 000a 78 03 00 00 	.4byte 0x378
 779 000e E8 00 00 00 	.4byte 0xe8
 780 0012 75 69 6E 74 	.asciz "uint16_t"
 780      31 36 5F 74 
 780      00 
 781 001b 37 01 00 00 	.4byte 0x137
 782 001f 74 61 67 54 	.asciz "tagTRISEBITS"
 782      52 49 53 45 
 782      42 49 54 53 
 782      00 
 783 002c F6 01 00 00 	.4byte 0x1f6
 784 0030 54 52 49 53 	.asciz "TRISEBITS"
 784      45 42 49 54 
 784      53 00 
 785 003a 08 02 00 00 	.4byte 0x208
 786 003e 74 61 67 4C 	.asciz "tagLATEBITS"
 786      41 54 45 42 
 786      49 54 53 00 
 787 004a BE 02 00 00 	.4byte 0x2be
 788 004e 4C 41 54 45 	.asciz "LATEBITS"
MPLAB XC16 ASSEMBLY Listing:   			page 18


 788      42 49 54 53 
 788      00 
 789 0057 00 00 00 00 	.4byte 0x0
 790                 	.section .debug_aranges,info
 791 0000 14 00 00 00 	.4byte 0x14
 792 0004 02 00       	.2byte 0x2
 793 0006 00 00 00 00 	.4byte .Ldebug_info0
 794 000a 04          	.byte 0x4
 795 000b 00          	.byte 0x0
 796 000c 00 00       	.2byte 0x0
 797 000e 00 00       	.2byte 0x0
 798 0010 00 00 00 00 	.4byte 0x0
 799 0014 00 00 00 00 	.4byte 0x0
 800                 	.section .debug_str,info
 801                 	.LASF1:
 802 0000 4C 41 54 45 	.asciz "LATEbits"
 802      62 69 74 73 
 802      00 
 803                 	.LASF0:
 804 0009 54 52 49 53 	.asciz "TRISEbits"
 804      45 62 69 74 
 804      73 00 
 805                 	.section .text,code
 806              	
 807              	
 808              	
 809              	.section __c30_info,info,bss
 810                 	__psv_trap_errata:
 811                 	
 812                 	.section __c30_signature,info,data
 813 0000 01 00       	.word 0x0001
 814 0002 00 00       	.word 0x0000
 815 0004 00 00       	.word 0x0000
 816                 	
 817                 	
 818                 	
 819                 	.set ___PA___,0
 820                 	.end
MPLAB XC16 ASSEMBLY Listing:   			page 19


DEFINED SYMBOLS
                            *ABS*:00000000 /home/user/Documents/FST/Programming/project_sub-zero/lib/lib_pic33e/led.c
                            *ABS*:00000001 __33EP256MU806
                            *ABS*:00000001 __dsPIC33E
                            *ABS*:00000001 __HAS_DSP
                            *ABS*:00000001 __HAS_DMAV2
                            *ABS*:00000001 __HAS_CODEGUARD
                            *ABS*:00000001 __HAS_PMP_ENHANCED
                            *ABS*:00000001 __HAS_EDS
                            *ABS*:00000001 __HAS_EP
                            *ABS*:00000011 __TARGET_DIVIDE_CYCLES
                            *ABS*:00000001 __C30ELF
    {standard input}:14     .text:00000000 _config_rgb
    {standard input}:18     *ABS*:00000000 ___PA___
    {standard input}:38     .text:00000010 _rgb_clear
    {standard input}:61     .text:00000020 _rgb_on
    {standard input}:77     *ABS*:00000000 ___BP___
    {standard input}:810    __c30_info:00000000 __psv_trap_errata
    {standard input}:19     .text:00000000 .L0
                            .text:0000007e .L8
                            .text:0000004c .L12
                            .text:00000086 .L6
                            .text:00000066 .L7
                            .text:0000006e .L5
                            .text:00000096 .L4
                            .text:0000008e .L10
                            .text:00000076 .L11
                            .text:0000009c .L3
                    .debug_abbrev:00000000 .Ldebug_abbrev0
                            .text:00000000 .Ltext0
                            .text:000000a4 .Letext0
                      .debug_line:00000000 .Ldebug_line0
                            .text:00000000 .LFB0
                            .text:00000010 .LFE0
                            .text:00000010 .LFB1
                            .text:00000020 .LFE1
                            .text:00000020 .LFB2
                            .text:000000a4 .LFE2
                       .debug_str:00000009 .LASF0
                       .debug_str:00000000 .LASF1
                     .debug_frame:00000000 .Lframe0
                      .debug_info:00000000 .Ldebug_info0

UNDEFINED SYMBOLS
_TRISEbits
CORCON
_LATEbits
_tolower

EQUATE SYMBOLS
/home/user/Documents/FST/Programming/project_sub-zero/lib/lib_pic33e/led.c = 0x0
                  __33EP256MU806 = 0x1
                      __dsPIC33E = 0x1
                       __HAS_DSP = 0x1
                     __HAS_DMAV2 = 0x1
                 __HAS_CODEGUARD = 0x1
              __HAS_PMP_ENHANCED = 0x1
                       __HAS_EDS = 0x1
                        __HAS_EP = 0x1
          __TARGET_DIVIDE_CYCLES = 0x11
                        __C30ELF = 0x1
                        ___PA___ = 0x0
                        ___BP___ = 0x0
