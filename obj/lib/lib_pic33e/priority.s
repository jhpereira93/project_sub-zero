MPLAB XC16 ASSEMBLY Listing:   			page 1


   1              	.file "/home/user/Documents/FST/Programming/project_sub-zero/lib/lib_pic33e/priority.c"
   2              	.section .debug_abbrev,info
   3                 	.Ldebug_abbrev0:
   4                 	.section .debug_info,info
   5                 	.Ldebug_info0:
   6                 	.section .debug_line,info
   7                 	.Ldebug_line0:
   8 0000 DC 00 00 00 	.section .text,code
   8      02 00 BE 00 
   8      00 00 01 01 
   8      FB 0E 0A 00 
   8      01 01 01 01 
   8      00 00 00 01 
   8      6C 69 62 2F 
   8      6C 69 62 5F 
   8      70 69 63 33 
   9              	.Ltext0:
  10              	.section .text,code
  11              	.align 2
  12              	.global _set_CPU_priority
  13              	.type _set_CPU_priority,@function
  14              	_set_CPU_priority:
  15              	.LFB0:
  16              	.file 1 "lib/lib_pic33e/priority.c"
   1:lib/lib_pic33e/priority.c **** #ifndef PRIORITY
   2:lib/lib_pic33e/priority.c **** 
   3:lib/lib_pic33e/priority.c **** #include <xc.h>
   4:lib/lib_pic33e/priority.c **** 
   5:lib/lib_pic33e/priority.c **** /**********************************************************************
   6:lib/lib_pic33e/priority.c ****  * Name:	set_CPU_priority
   7:lib/lib_pic33e/priority.c ****  * Args:    unsigned int n
   8:lib/lib_pic33e/priority.c ****  * Return:	previous priority OR error code
   9:lib/lib_pic33e/priority.c ****  * Desc:	Sets CPU priority to n, provided it is between 0 and 7.
  10:lib/lib_pic33e/priority.c ****  *          Provides a way to disable interruptions in critical parts
  11:lib/lib_pic33e/priority.c ****  *          of the code.
  12:lib/lib_pic33e/priority.c ****  *          Needs to be set to 6 or lower to allow interruptions,
  13:lib/lib_pic33e/priority.c ****  *          depending on interruptions' priorities.
  14:lib/lib_pic33e/priority.c ****  *          Previous priority returned for restoring purposes.
  15:lib/lib_pic33e/priority.c ****  **********************************************************************/
  16:lib/lib_pic33e/priority.c **** int set_CPU_priority(unsigned int n){
  17              	.loc 1 16 0
  18              	.set ___PA___,1
  19 000000  04 00 FA 	lnk #4
  20              	.LCFI0:
  21              	.loc 1 16 0
  22 000002  10 07 98 	mov w0,[w14+2]
  17:lib/lib_pic33e/priority.c **** 
  18:lib/lib_pic33e/priority.c **** 	unsigned int prevIPL;
  19:lib/lib_pic33e/priority.c **** 
  20:lib/lib_pic33e/priority.c **** 	if(n<=7){
  23              	.loc 1 20 0
  24 000004  1E 00 90 	mov [w14+2],w0
  25 000006  E7 0F 50 	sub w0,#7,[w15]
  26              	.set ___BP___,0
  27 000008  00 00 3E 	bra gtu,.L2
  21:lib/lib_pic33e/priority.c **** 		prevIPL = SRbits.IPL;
  28              	.loc 1 21 0
MPLAB XC16 ASSEMBLY Listing:   			page 2


  29 00000a  00 00 80 	mov _SRbits,w0
  30 00000c  45 00 DE 	lsr w0,#5,w0
  31 00000e  67 40 60 	and.b w0,#7,w0
  32 000010  00 80 FB 	ze w0,w0
  33 000012  00 0F 78 	mov w0,[w14]
  22:lib/lib_pic33e/priority.c **** 		SRbits.IPL = n; 			/* Status Register . CPU priority 0-7 */
  34              	.loc 1 22 0
  35 000014  02 00 80 	mov _SRbits,w2
  36 000016  1E 00 90 	mov [w14+2],w0
  37 000018  F1 F1 2F 	mov #-225,w1
  38 00001a  00 40 78 	mov.b w0,w0
  39 00001c  01 01 61 	and w2,w1,w2
  40 00001e  E7 40 60 	and.b w0,#7,w1
  23:lib/lib_pic33e/priority.c **** 		return prevIPL;
  41              	.loc 1 23 0
  42 000020  1E 00 78 	mov [w14],w0
  43              	.loc 1 22 0
  44 000022  81 80 FB 	ze w1,w1
  45 000024  E7 80 60 	and w1,#7,w1
  46 000026  C5 08 DD 	sl w1,#5,w1
  47 000028  82 80 70 	ior w1,w2,w1
  48 00002a  01 00 88 	mov w1,_SRbits
  49              	.loc 1 23 0
  50 00002c  00 00 37 	bra .L3
  51              	.L2:
  24:lib/lib_pic33e/priority.c **** 	}
  25:lib/lib_pic33e/priority.c **** 	return -1;
  52              	.loc 1 25 0
  53 00002e  00 80 EB 	setm w0
  54              	.L3:
  26:lib/lib_pic33e/priority.c **** }
  55              	.loc 1 26 0
  56 000030  8E 07 78 	mov w14,w15
  57 000032  4F 07 78 	mov [--w15],w14
  58 000034  00 40 A9 	bclr CORCON,#2
  59 000036  00 00 06 	return 
  60              	.set ___PA___,0
  61              	.LFE0:
  62              	.size _set_CPU_priority,.-_set_CPU_priority
  63              	.section .debug_frame,info
  64                 	.Lframe0:
  65 0000 10 00 00 00 	.4byte .LECIE0-.LSCIE0
  66                 	.LSCIE0:
  67 0004 FF FF FF FF 	.4byte 0xffffffff
  68 0008 01          	.byte 0x1
  69 0009 00          	.byte 0
  70 000a 01          	.uleb128 0x1
  71 000b 02          	.sleb128 2
  72 000c 25          	.byte 0x25
  73 000d 12          	.byte 0x12
  74 000e 0F          	.uleb128 0xf
  75 000f 7E          	.sleb128 -2
  76 0010 09          	.byte 0x9
  77 0011 25          	.uleb128 0x25
  78 0012 0F          	.uleb128 0xf
  79 0013 00          	.align 4
  80                 	.LECIE0:
MPLAB XC16 ASSEMBLY Listing:   			page 3


  81                 	.LSFDE0:
  82 0014 18 00 00 00 	.4byte .LEFDE0-.LASFDE0
  83                 	.LASFDE0:
  84 0018 00 00 00 00 	.4byte .Lframe0
  85 001c 00 00 00 00 	.4byte .LFB0
  86 0020 38 00 00 00 	.4byte .LFE0-.LFB0
  87 0024 04          	.byte 0x4
  88 0025 02 00 00 00 	.4byte .LCFI0-.LFB0
  89 0029 13          	.byte 0x13
  90 002a 7D          	.sleb128 -3
  91 002b 0D          	.byte 0xd
  92 002c 0E          	.uleb128 0xe
  93 002d 8E          	.byte 0x8e
  94 002e 02          	.uleb128 0x2
  95 002f 00          	.align 4
  96                 	.LEFDE0:
  97                 	.section .text,code
  98              	.Letext0:
  99              	.file 2 "/opt/microchip/xc16/v1.36/bin/bin/../../support/dsPIC33E/h/p33EP256MU806.h"
 100              	.file 3 "/opt/microchip/xc16/v1.36/bin/bin/../../include/lega-c/stdint.h"
 101              	.section .debug_info,info
 102 0000 09 03 00 00 	.4byte 0x309
 103 0004 02 00       	.2byte 0x2
 104 0006 00 00 00 00 	.4byte .Ldebug_abbrev0
 105 000a 04          	.byte 0x4
 106 000b 01          	.uleb128 0x1
 107 000c 47 4E 55 20 	.asciz "GNU C 4.5.1 (XC16, Microchip v1.36) (A) Build date: Dec  5 2018"
 107      43 20 34 2E 
 107      35 2E 31 20 
 107      28 58 43 31 
 107      36 2C 20 4D 
 107      69 63 72 6F 
 107      63 68 69 70 
 107      20 76 31 2E 
 107      33 36 29 20 
 108 004c 01          	.byte 0x1
 109 004d 6C 69 62 2F 	.asciz "lib/lib_pic33e/priority.c"
 109      6C 69 62 5F 
 109      70 69 63 33 
 109      33 65 2F 70 
 109      72 69 6F 72 
 109      69 74 79 2E 
 109      63 00 
 110 0067 2F 68 6F 6D 	.asciz "/home/user/Documents/FST/Programming/project_sub-zero"
 110      65 2F 75 73 
 110      65 72 2F 44 
 110      6F 63 75 6D 
 110      65 6E 74 73 
 110      2F 46 53 54 
 110      2F 50 72 6F 
 110      67 72 61 6D 
 110      6D 69 6E 67 
 111 009d 00 00 00 00 	.4byte .Ltext0
 112 00a1 00 00 00 00 	.4byte .Letext0
 113 00a5 00 00 00 00 	.4byte .Ldebug_line0
 114 00a9 02          	.uleb128 0x2
 115 00aa 01          	.byte 0x1
MPLAB XC16 ASSEMBLY Listing:   			page 4


 116 00ab 06          	.byte 0x6
 117 00ac 73 69 67 6E 	.asciz "signed char"
 117      65 64 20 63 
 117      68 61 72 00 
 118 00b8 02          	.uleb128 0x2
 119 00b9 02          	.byte 0x2
 120 00ba 05          	.byte 0x5
 121 00bb 69 6E 74 00 	.asciz "int"
 122 00bf 02          	.uleb128 0x2
 123 00c0 04          	.byte 0x4
 124 00c1 05          	.byte 0x5
 125 00c2 6C 6F 6E 67 	.asciz "long int"
 125      20 69 6E 74 
 125      00 
 126 00cb 02          	.uleb128 0x2
 127 00cc 08          	.byte 0x8
 128 00cd 05          	.byte 0x5
 129 00ce 6C 6F 6E 67 	.asciz "long long int"
 129      20 6C 6F 6E 
 129      67 20 69 6E 
 129      74 00 
 130 00dc 02          	.uleb128 0x2
 131 00dd 01          	.byte 0x1
 132 00de 08          	.byte 0x8
 133 00df 75 6E 73 69 	.asciz "unsigned char"
 133      67 6E 65 64 
 133      20 63 68 61 
 133      72 00 
 134 00ed 03          	.uleb128 0x3
 135 00ee 75 69 6E 74 	.asciz "uint16_t"
 135      31 36 5F 74 
 135      00 
 136 00f7 03          	.byte 0x3
 137 00f8 31          	.byte 0x31
 138 00f9 FD 00 00 00 	.4byte 0xfd
 139 00fd 02          	.uleb128 0x2
 140 00fe 02          	.byte 0x2
 141 00ff 07          	.byte 0x7
 142 0100 75 6E 73 69 	.asciz "unsigned int"
 142      67 6E 65 64 
 142      20 69 6E 74 
 142      00 
 143 010d 02          	.uleb128 0x2
 144 010e 04          	.byte 0x4
 145 010f 07          	.byte 0x7
 146 0110 6C 6F 6E 67 	.asciz "long unsigned int"
 146      20 75 6E 73 
 146      69 67 6E 65 
 146      64 20 69 6E 
 146      74 00 
 147 0122 02          	.uleb128 0x2
 148 0123 08          	.byte 0x8
 149 0124 07          	.byte 0x7
 150 0125 6C 6F 6E 67 	.asciz "long long unsigned int"
 150      20 6C 6F 6E 
 150      67 20 75 6E 
 150      73 69 67 6E 
MPLAB XC16 ASSEMBLY Listing:   			page 5


 150      65 64 20 69 
 150      6E 74 00 
 151 013c 04          	.uleb128 0x4
 152 013d 02          	.byte 0x2
 153 013e 02          	.byte 0x2
 154 013f 87          	.byte 0x87
 155 0140 25 02 00 00 	.4byte 0x225
 156 0144 05          	.uleb128 0x5
 157 0145 43 00       	.asciz "C"
 158 0147 02          	.byte 0x2
 159 0148 88          	.byte 0x88
 160 0149 ED 00 00 00 	.4byte 0xed
 161 014d 02          	.byte 0x2
 162 014e 01          	.byte 0x1
 163 014f 0F          	.byte 0xf
 164 0150 02          	.byte 0x2
 165 0151 23          	.byte 0x23
 166 0152 00          	.uleb128 0x0
 167 0153 05          	.uleb128 0x5
 168 0154 5A 00       	.asciz "Z"
 169 0156 02          	.byte 0x2
 170 0157 89          	.byte 0x89
 171 0158 ED 00 00 00 	.4byte 0xed
 172 015c 02          	.byte 0x2
 173 015d 01          	.byte 0x1
 174 015e 0E          	.byte 0xe
 175 015f 02          	.byte 0x2
 176 0160 23          	.byte 0x23
 177 0161 00          	.uleb128 0x0
 178 0162 05          	.uleb128 0x5
 179 0163 4F 56 00    	.asciz "OV"
 180 0166 02          	.byte 0x2
 181 0167 8A          	.byte 0x8a
 182 0168 ED 00 00 00 	.4byte 0xed
 183 016c 02          	.byte 0x2
 184 016d 01          	.byte 0x1
 185 016e 0D          	.byte 0xd
 186 016f 02          	.byte 0x2
 187 0170 23          	.byte 0x23
 188 0171 00          	.uleb128 0x0
 189 0172 05          	.uleb128 0x5
 190 0173 4E 00       	.asciz "N"
 191 0175 02          	.byte 0x2
 192 0176 8B          	.byte 0x8b
 193 0177 ED 00 00 00 	.4byte 0xed
 194 017b 02          	.byte 0x2
 195 017c 01          	.byte 0x1
 196 017d 0C          	.byte 0xc
 197 017e 02          	.byte 0x2
 198 017f 23          	.byte 0x23
 199 0180 00          	.uleb128 0x0
 200 0181 05          	.uleb128 0x5
 201 0182 52 41 00    	.asciz "RA"
 202 0185 02          	.byte 0x2
 203 0186 8C          	.byte 0x8c
 204 0187 ED 00 00 00 	.4byte 0xed
 205 018b 02          	.byte 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 6


 206 018c 01          	.byte 0x1
 207 018d 0B          	.byte 0xb
 208 018e 02          	.byte 0x2
 209 018f 23          	.byte 0x23
 210 0190 00          	.uleb128 0x0
 211 0191 05          	.uleb128 0x5
 212 0192 49 50 4C 00 	.asciz "IPL"
 213 0196 02          	.byte 0x2
 214 0197 8D          	.byte 0x8d
 215 0198 ED 00 00 00 	.4byte 0xed
 216 019c 02          	.byte 0x2
 217 019d 03          	.byte 0x3
 218 019e 08          	.byte 0x8
 219 019f 02          	.byte 0x2
 220 01a0 23          	.byte 0x23
 221 01a1 00          	.uleb128 0x0
 222 01a2 05          	.uleb128 0x5
 223 01a3 44 43 00    	.asciz "DC"
 224 01a6 02          	.byte 0x2
 225 01a7 8E          	.byte 0x8e
 226 01a8 ED 00 00 00 	.4byte 0xed
 227 01ac 02          	.byte 0x2
 228 01ad 01          	.byte 0x1
 229 01ae 07          	.byte 0x7
 230 01af 02          	.byte 0x2
 231 01b0 23          	.byte 0x23
 232 01b1 00          	.uleb128 0x0
 233 01b2 05          	.uleb128 0x5
 234 01b3 44 41 00    	.asciz "DA"
 235 01b6 02          	.byte 0x2
 236 01b7 8F          	.byte 0x8f
 237 01b8 ED 00 00 00 	.4byte 0xed
 238 01bc 02          	.byte 0x2
 239 01bd 01          	.byte 0x1
 240 01be 06          	.byte 0x6
 241 01bf 02          	.byte 0x2
 242 01c0 23          	.byte 0x23
 243 01c1 00          	.uleb128 0x0
 244 01c2 05          	.uleb128 0x5
 245 01c3 53 41 42 00 	.asciz "SAB"
 246 01c7 02          	.byte 0x2
 247 01c8 90          	.byte 0x90
 248 01c9 ED 00 00 00 	.4byte 0xed
 249 01cd 02          	.byte 0x2
 250 01ce 01          	.byte 0x1
 251 01cf 05          	.byte 0x5
 252 01d0 02          	.byte 0x2
 253 01d1 23          	.byte 0x23
 254 01d2 00          	.uleb128 0x0
 255 01d3 05          	.uleb128 0x5
 256 01d4 4F 41 42 00 	.asciz "OAB"
 257 01d8 02          	.byte 0x2
 258 01d9 91          	.byte 0x91
 259 01da ED 00 00 00 	.4byte 0xed
 260 01de 02          	.byte 0x2
 261 01df 01          	.byte 0x1
 262 01e0 04          	.byte 0x4
MPLAB XC16 ASSEMBLY Listing:   			page 7


 263 01e1 02          	.byte 0x2
 264 01e2 23          	.byte 0x23
 265 01e3 00          	.uleb128 0x0
 266 01e4 05          	.uleb128 0x5
 267 01e5 53 42 00    	.asciz "SB"
 268 01e8 02          	.byte 0x2
 269 01e9 92          	.byte 0x92
 270 01ea ED 00 00 00 	.4byte 0xed
 271 01ee 02          	.byte 0x2
 272 01ef 01          	.byte 0x1
 273 01f0 03          	.byte 0x3
 274 01f1 02          	.byte 0x2
 275 01f2 23          	.byte 0x23
 276 01f3 00          	.uleb128 0x0
 277 01f4 05          	.uleb128 0x5
 278 01f5 53 41 00    	.asciz "SA"
 279 01f8 02          	.byte 0x2
 280 01f9 93          	.byte 0x93
 281 01fa ED 00 00 00 	.4byte 0xed
 282 01fe 02          	.byte 0x2
 283 01ff 01          	.byte 0x1
 284 0200 02          	.byte 0x2
 285 0201 02          	.byte 0x2
 286 0202 23          	.byte 0x23
 287 0203 00          	.uleb128 0x0
 288 0204 05          	.uleb128 0x5
 289 0205 4F 42 00    	.asciz "OB"
 290 0208 02          	.byte 0x2
 291 0209 94          	.byte 0x94
 292 020a ED 00 00 00 	.4byte 0xed
 293 020e 02          	.byte 0x2
 294 020f 01          	.byte 0x1
 295 0210 01          	.byte 0x1
 296 0211 02          	.byte 0x2
 297 0212 23          	.byte 0x23
 298 0213 00          	.uleb128 0x0
 299 0214 05          	.uleb128 0x5
 300 0215 4F 41 00    	.asciz "OA"
 301 0218 02          	.byte 0x2
 302 0219 95          	.byte 0x95
 303 021a ED 00 00 00 	.4byte 0xed
 304 021e 02          	.byte 0x2
 305 021f 01          	.byte 0x1
 306 0220 10          	.byte 0x10
 307 0221 02          	.byte 0x2
 308 0222 23          	.byte 0x23
 309 0223 00          	.uleb128 0x0
 310 0224 00          	.byte 0x0
 311 0225 04          	.uleb128 0x4
 312 0226 02          	.byte 0x2
 313 0227 02          	.byte 0x2
 314 0228 97          	.byte 0x97
 315 0229 64 02 00 00 	.4byte 0x264
 316 022d 05          	.uleb128 0x5
 317 022e 49 50 4C 30 	.asciz "IPL0"
 317      00 
 318 0233 02          	.byte 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 8


 319 0234 99          	.byte 0x99
 320 0235 ED 00 00 00 	.4byte 0xed
 321 0239 02          	.byte 0x2
 322 023a 01          	.byte 0x1
 323 023b 0A          	.byte 0xa
 324 023c 02          	.byte 0x2
 325 023d 23          	.byte 0x23
 326 023e 00          	.uleb128 0x0
 327 023f 05          	.uleb128 0x5
 328 0240 49 50 4C 31 	.asciz "IPL1"
 328      00 
 329 0245 02          	.byte 0x2
 330 0246 9A          	.byte 0x9a
 331 0247 ED 00 00 00 	.4byte 0xed
 332 024b 02          	.byte 0x2
 333 024c 01          	.byte 0x1
 334 024d 09          	.byte 0x9
 335 024e 02          	.byte 0x2
 336 024f 23          	.byte 0x23
 337 0250 00          	.uleb128 0x0
 338 0251 05          	.uleb128 0x5
 339 0252 49 50 4C 32 	.asciz "IPL2"
 339      00 
 340 0257 02          	.byte 0x2
 341 0258 9B          	.byte 0x9b
 342 0259 ED 00 00 00 	.4byte 0xed
 343 025d 02          	.byte 0x2
 344 025e 01          	.byte 0x1
 345 025f 08          	.byte 0x8
 346 0260 02          	.byte 0x2
 347 0261 23          	.byte 0x23
 348 0262 00          	.uleb128 0x0
 349 0263 00          	.byte 0x0
 350 0264 06          	.uleb128 0x6
 351 0265 02          	.byte 0x2
 352 0266 02          	.byte 0x2
 353 0267 86          	.byte 0x86
 354 0268 77 02 00 00 	.4byte 0x277
 355 026c 07          	.uleb128 0x7
 356 026d 3C 01 00 00 	.4byte 0x13c
 357 0271 07          	.uleb128 0x7
 358 0272 25 02 00 00 	.4byte 0x225
 359 0276 00          	.byte 0x0
 360 0277 08          	.uleb128 0x8
 361 0278 74 61 67 53 	.asciz "tagSRBITS"
 361      52 42 49 54 
 361      53 00 
 362 0282 02          	.byte 0x2
 363 0283 02          	.byte 0x2
 364 0284 85          	.byte 0x85
 365 0285 92 02 00 00 	.4byte 0x292
 366 0289 09          	.uleb128 0x9
 367 028a 64 02 00 00 	.4byte 0x264
 368 028e 02          	.byte 0x2
 369 028f 23          	.byte 0x23
 370 0290 00          	.uleb128 0x0
 371 0291 00          	.byte 0x0
MPLAB XC16 ASSEMBLY Listing:   			page 9


 372 0292 03          	.uleb128 0x3
 373 0293 53 52 42 49 	.asciz "SRBITS"
 373      54 53 00 
 374 029a 02          	.byte 0x2
 375 029b 9E          	.byte 0x9e
 376 029c 77 02 00 00 	.4byte 0x277
 377 02a0 0A          	.uleb128 0xa
 378 02a1 01          	.byte 0x1
 379 02a2 73 65 74 5F 	.asciz "set_CPU_priority"
 379      43 50 55 5F 
 379      70 72 69 6F 
 379      72 69 74 79 
 379      00 
 380 02b3 01          	.byte 0x1
 381 02b4 10          	.byte 0x10
 382 02b5 01          	.byte 0x1
 383 02b6 B8 00 00 00 	.4byte 0xb8
 384 02ba 00 00 00 00 	.4byte .LFB0
 385 02be 00 00 00 00 	.4byte .LFE0
 386 02c2 01          	.byte 0x1
 387 02c3 5E          	.byte 0x5e
 388 02c4 E7 02 00 00 	.4byte 0x2e7
 389 02c8 0B          	.uleb128 0xb
 390 02c9 6E 00       	.asciz "n"
 391 02cb 01          	.byte 0x1
 392 02cc 10          	.byte 0x10
 393 02cd FD 00 00 00 	.4byte 0xfd
 394 02d1 02          	.byte 0x2
 395 02d2 7E          	.byte 0x7e
 396 02d3 02          	.sleb128 2
 397 02d4 0C          	.uleb128 0xc
 398 02d5 70 72 65 76 	.asciz "prevIPL"
 398      49 50 4C 00 
 399 02dd 01          	.byte 0x1
 400 02de 12          	.byte 0x12
 401 02df FD 00 00 00 	.4byte 0xfd
 402 02e3 02          	.byte 0x2
 403 02e4 7E          	.byte 0x7e
 404 02e5 00          	.sleb128 0
 405 02e6 00          	.byte 0x0
 406 02e7 0D          	.uleb128 0xd
 407 02e8 53 52 62 69 	.asciz "SRbits"
 407      74 73 00 
 408 02ef 02          	.byte 0x2
 409 02f0 9F          	.byte 0x9f
 410 02f1 F7 02 00 00 	.4byte 0x2f7
 411 02f5 01          	.byte 0x1
 412 02f6 01          	.byte 0x1
 413 02f7 0E          	.uleb128 0xe
 414 02f8 92 02 00 00 	.4byte 0x292
 415 02fc 0D          	.uleb128 0xd
 416 02fd 53 52 62 69 	.asciz "SRbits"
 416      74 73 00 
 417 0304 02          	.byte 0x2
 418 0305 9F          	.byte 0x9f
 419 0306 F7 02 00 00 	.4byte 0x2f7
 420 030a 01          	.byte 0x1
MPLAB XC16 ASSEMBLY Listing:   			page 10


 421 030b 01          	.byte 0x1
 422 030c 00          	.byte 0x0
 423                 	.section .debug_abbrev,info
 424 0000 01          	.uleb128 0x1
 425 0001 11          	.uleb128 0x11
 426 0002 01          	.byte 0x1
 427 0003 25          	.uleb128 0x25
 428 0004 08          	.uleb128 0x8
 429 0005 13          	.uleb128 0x13
 430 0006 0B          	.uleb128 0xb
 431 0007 03          	.uleb128 0x3
 432 0008 08          	.uleb128 0x8
 433 0009 1B          	.uleb128 0x1b
 434 000a 08          	.uleb128 0x8
 435 000b 11          	.uleb128 0x11
 436 000c 01          	.uleb128 0x1
 437 000d 12          	.uleb128 0x12
 438 000e 01          	.uleb128 0x1
 439 000f 10          	.uleb128 0x10
 440 0010 06          	.uleb128 0x6
 441 0011 00          	.byte 0x0
 442 0012 00          	.byte 0x0
 443 0013 02          	.uleb128 0x2
 444 0014 24          	.uleb128 0x24
 445 0015 00          	.byte 0x0
 446 0016 0B          	.uleb128 0xb
 447 0017 0B          	.uleb128 0xb
 448 0018 3E          	.uleb128 0x3e
 449 0019 0B          	.uleb128 0xb
 450 001a 03          	.uleb128 0x3
 451 001b 08          	.uleb128 0x8
 452 001c 00          	.byte 0x0
 453 001d 00          	.byte 0x0
 454 001e 03          	.uleb128 0x3
 455 001f 16          	.uleb128 0x16
 456 0020 00          	.byte 0x0
 457 0021 03          	.uleb128 0x3
 458 0022 08          	.uleb128 0x8
 459 0023 3A          	.uleb128 0x3a
 460 0024 0B          	.uleb128 0xb
 461 0025 3B          	.uleb128 0x3b
 462 0026 0B          	.uleb128 0xb
 463 0027 49          	.uleb128 0x49
 464 0028 13          	.uleb128 0x13
 465 0029 00          	.byte 0x0
 466 002a 00          	.byte 0x0
 467 002b 04          	.uleb128 0x4
 468 002c 13          	.uleb128 0x13
 469 002d 01          	.byte 0x1
 470 002e 0B          	.uleb128 0xb
 471 002f 0B          	.uleb128 0xb
 472 0030 3A          	.uleb128 0x3a
 473 0031 0B          	.uleb128 0xb
 474 0032 3B          	.uleb128 0x3b
 475 0033 0B          	.uleb128 0xb
 476 0034 01          	.uleb128 0x1
 477 0035 13          	.uleb128 0x13
MPLAB XC16 ASSEMBLY Listing:   			page 11


 478 0036 00          	.byte 0x0
 479 0037 00          	.byte 0x0
 480 0038 05          	.uleb128 0x5
 481 0039 0D          	.uleb128 0xd
 482 003a 00          	.byte 0x0
 483 003b 03          	.uleb128 0x3
 484 003c 08          	.uleb128 0x8
 485 003d 3A          	.uleb128 0x3a
 486 003e 0B          	.uleb128 0xb
 487 003f 3B          	.uleb128 0x3b
 488 0040 0B          	.uleb128 0xb
 489 0041 49          	.uleb128 0x49
 490 0042 13          	.uleb128 0x13
 491 0043 0B          	.uleb128 0xb
 492 0044 0B          	.uleb128 0xb
 493 0045 0D          	.uleb128 0xd
 494 0046 0B          	.uleb128 0xb
 495 0047 0C          	.uleb128 0xc
 496 0048 0B          	.uleb128 0xb
 497 0049 38          	.uleb128 0x38
 498 004a 0A          	.uleb128 0xa
 499 004b 00          	.byte 0x0
 500 004c 00          	.byte 0x0
 501 004d 06          	.uleb128 0x6
 502 004e 17          	.uleb128 0x17
 503 004f 01          	.byte 0x1
 504 0050 0B          	.uleb128 0xb
 505 0051 0B          	.uleb128 0xb
 506 0052 3A          	.uleb128 0x3a
 507 0053 0B          	.uleb128 0xb
 508 0054 3B          	.uleb128 0x3b
 509 0055 0B          	.uleb128 0xb
 510 0056 01          	.uleb128 0x1
 511 0057 13          	.uleb128 0x13
 512 0058 00          	.byte 0x0
 513 0059 00          	.byte 0x0
 514 005a 07          	.uleb128 0x7
 515 005b 0D          	.uleb128 0xd
 516 005c 00          	.byte 0x0
 517 005d 49          	.uleb128 0x49
 518 005e 13          	.uleb128 0x13
 519 005f 00          	.byte 0x0
 520 0060 00          	.byte 0x0
 521 0061 08          	.uleb128 0x8
 522 0062 13          	.uleb128 0x13
 523 0063 01          	.byte 0x1
 524 0064 03          	.uleb128 0x3
 525 0065 08          	.uleb128 0x8
 526 0066 0B          	.uleb128 0xb
 527 0067 0B          	.uleb128 0xb
 528 0068 3A          	.uleb128 0x3a
 529 0069 0B          	.uleb128 0xb
 530 006a 3B          	.uleb128 0x3b
 531 006b 0B          	.uleb128 0xb
 532 006c 01          	.uleb128 0x1
 533 006d 13          	.uleb128 0x13
 534 006e 00          	.byte 0x0
MPLAB XC16 ASSEMBLY Listing:   			page 12


 535 006f 00          	.byte 0x0
 536 0070 09          	.uleb128 0x9
 537 0071 0D          	.uleb128 0xd
 538 0072 00          	.byte 0x0
 539 0073 49          	.uleb128 0x49
 540 0074 13          	.uleb128 0x13
 541 0075 38          	.uleb128 0x38
 542 0076 0A          	.uleb128 0xa
 543 0077 00          	.byte 0x0
 544 0078 00          	.byte 0x0
 545 0079 0A          	.uleb128 0xa
 546 007a 2E          	.uleb128 0x2e
 547 007b 01          	.byte 0x1
 548 007c 3F          	.uleb128 0x3f
 549 007d 0C          	.uleb128 0xc
 550 007e 03          	.uleb128 0x3
 551 007f 08          	.uleb128 0x8
 552 0080 3A          	.uleb128 0x3a
 553 0081 0B          	.uleb128 0xb
 554 0082 3B          	.uleb128 0x3b
 555 0083 0B          	.uleb128 0xb
 556 0084 27          	.uleb128 0x27
 557 0085 0C          	.uleb128 0xc
 558 0086 49          	.uleb128 0x49
 559 0087 13          	.uleb128 0x13
 560 0088 11          	.uleb128 0x11
 561 0089 01          	.uleb128 0x1
 562 008a 12          	.uleb128 0x12
 563 008b 01          	.uleb128 0x1
 564 008c 40          	.uleb128 0x40
 565 008d 0A          	.uleb128 0xa
 566 008e 01          	.uleb128 0x1
 567 008f 13          	.uleb128 0x13
 568 0090 00          	.byte 0x0
 569 0091 00          	.byte 0x0
 570 0092 0B          	.uleb128 0xb
 571 0093 05          	.uleb128 0x5
 572 0094 00          	.byte 0x0
 573 0095 03          	.uleb128 0x3
 574 0096 08          	.uleb128 0x8
 575 0097 3A          	.uleb128 0x3a
 576 0098 0B          	.uleb128 0xb
 577 0099 3B          	.uleb128 0x3b
 578 009a 0B          	.uleb128 0xb
 579 009b 49          	.uleb128 0x49
 580 009c 13          	.uleb128 0x13
 581 009d 02          	.uleb128 0x2
 582 009e 0A          	.uleb128 0xa
 583 009f 00          	.byte 0x0
 584 00a0 00          	.byte 0x0
 585 00a1 0C          	.uleb128 0xc
 586 00a2 34          	.uleb128 0x34
 587 00a3 00          	.byte 0x0
 588 00a4 03          	.uleb128 0x3
 589 00a5 08          	.uleb128 0x8
 590 00a6 3A          	.uleb128 0x3a
 591 00a7 0B          	.uleb128 0xb
MPLAB XC16 ASSEMBLY Listing:   			page 13


 592 00a8 3B          	.uleb128 0x3b
 593 00a9 0B          	.uleb128 0xb
 594 00aa 49          	.uleb128 0x49
 595 00ab 13          	.uleb128 0x13
 596 00ac 02          	.uleb128 0x2
 597 00ad 0A          	.uleb128 0xa
 598 00ae 00          	.byte 0x0
 599 00af 00          	.byte 0x0
 600 00b0 0D          	.uleb128 0xd
 601 00b1 34          	.uleb128 0x34
 602 00b2 00          	.byte 0x0
 603 00b3 03          	.uleb128 0x3
 604 00b4 08          	.uleb128 0x8
 605 00b5 3A          	.uleb128 0x3a
 606 00b6 0B          	.uleb128 0xb
 607 00b7 3B          	.uleb128 0x3b
 608 00b8 0B          	.uleb128 0xb
 609 00b9 49          	.uleb128 0x49
 610 00ba 13          	.uleb128 0x13
 611 00bb 3F          	.uleb128 0x3f
 612 00bc 0C          	.uleb128 0xc
 613 00bd 3C          	.uleb128 0x3c
 614 00be 0C          	.uleb128 0xc
 615 00bf 00          	.byte 0x0
 616 00c0 00          	.byte 0x0
 617 00c1 0E          	.uleb128 0xe
 618 00c2 35          	.uleb128 0x35
 619 00c3 00          	.byte 0x0
 620 00c4 49          	.uleb128 0x49
 621 00c5 13          	.uleb128 0x13
 622 00c6 00          	.byte 0x0
 623 00c7 00          	.byte 0x0
 624 00c8 00          	.byte 0x0
 625                 	.section .debug_pubnames,info
 626 0000 23 00 00 00 	.4byte 0x23
 627 0004 02 00       	.2byte 0x2
 628 0006 00 00 00 00 	.4byte .Ldebug_info0
 629 000a 0D 03 00 00 	.4byte 0x30d
 630 000e A0 02 00 00 	.4byte 0x2a0
 631 0012 73 65 74 5F 	.asciz "set_CPU_priority"
 631      43 50 55 5F 
 631      70 72 69 6F 
 631      72 69 74 79 
 631      00 
 632 0023 00 00 00 00 	.4byte 0x0
 633                 	.section .debug_pubtypes,info
 634 0000 34 00 00 00 	.4byte 0x34
 635 0004 02 00       	.2byte 0x2
 636 0006 00 00 00 00 	.4byte .Ldebug_info0
 637 000a 0D 03 00 00 	.4byte 0x30d
 638 000e ED 00 00 00 	.4byte 0xed
 639 0012 75 69 6E 74 	.asciz "uint16_t"
 639      31 36 5F 74 
 639      00 
 640 001b 77 02 00 00 	.4byte 0x277
 641 001f 74 61 67 53 	.asciz "tagSRBITS"
 641      52 42 49 54 
MPLAB XC16 ASSEMBLY Listing:   			page 14


 641      53 00 
 642 0029 92 02 00 00 	.4byte 0x292
 643 002d 53 52 42 49 	.asciz "SRBITS"
 643      54 53 00 
 644 0034 00 00 00 00 	.4byte 0x0
 645                 	.section .debug_aranges,info
 646 0000 14 00 00 00 	.4byte 0x14
 647 0004 02 00       	.2byte 0x2
 648 0006 00 00 00 00 	.4byte .Ldebug_info0
 649 000a 04          	.byte 0x4
 650 000b 00          	.byte 0x0
 651 000c 00 00       	.2byte 0x0
 652 000e 00 00       	.2byte 0x0
 653 0010 00 00 00 00 	.4byte 0x0
 654 0014 00 00 00 00 	.4byte 0x0
 655                 	.section .debug_str,info
 656                 	.section .text,code
 657              	
 658              	
 659              	
 660              	.section __c30_info,info,bss
 661                 	__psv_trap_errata:
 662                 	
 663                 	.section __c30_signature,info,data
 664 0000 01 00       	.word 0x0001
 665 0002 00 00       	.word 0x0000
 666 0004 00 00       	.word 0x0000
 667                 	
 668                 	
 669                 	
 670                 	.set ___PA___,0
 671                 	.end
MPLAB XC16 ASSEMBLY Listing:   			page 15


DEFINED SYMBOLS
                            *ABS*:00000000 /home/user/Documents/FST/Programming/project_sub-zero/lib/lib_pic33e/priority.c
                            *ABS*:00000001 __33EP256MU806
                            *ABS*:00000001 __dsPIC33E
                            *ABS*:00000001 __HAS_DSP
                            *ABS*:00000001 __HAS_DMAV2
                            *ABS*:00000001 __HAS_CODEGUARD
                            *ABS*:00000001 __HAS_PMP_ENHANCED
                            *ABS*:00000001 __HAS_EDS
                            *ABS*:00000001 __HAS_EP
                            *ABS*:00000011 __TARGET_DIVIDE_CYCLES
                            *ABS*:00000001 __C30ELF
    {standard input}:14     .text:00000000 _set_CPU_priority
    {standard input}:18     *ABS*:00000000 ___PA___
    {standard input}:26     *ABS*:00000000 ___BP___
    {standard input}:661    __c30_info:00000000 __psv_trap_errata
    {standard input}:19     .text:00000000 .L0
                            .text:0000002e .L2
                            .text:00000030 .L3
                    .debug_abbrev:00000000 .Ldebug_abbrev0
                            .text:00000000 .Ltext0
                            .text:00000038 .Letext0
                      .debug_line:00000000 .Ldebug_line0
                            .text:00000000 .LFB0
                            .text:00000038 .LFE0
                     .debug_frame:00000000 .Lframe0
                      .debug_info:00000000 .Ldebug_info0

UNDEFINED SYMBOLS
_SRbits
CORCON

EQUATE SYMBOLS
/home/user/Documents/FST/Programming/project_sub-zero/lib/lib_pic33e/priority.c = 0x0
                  __33EP256MU806 = 0x1
                      __dsPIC33E = 0x1
                       __HAS_DSP = 0x1
                     __HAS_DMAV2 = 0x1
                 __HAS_CODEGUARD = 0x1
              __HAS_PMP_ENHANCED = 0x1
                       __HAS_EDS = 0x1
                        __HAS_EP = 0x1
          __TARGET_DIVIDE_CYCLES = 0x11
                        __C30ELF = 0x1
                        ___PA___ = 0x0
                        ___BP___ = 0x0
