MPLAB XC16 ASSEMBLY Listing:   			page 1


   1              	.file "/home/user/Documents/FST/Programming/project_sub-zero/lib/lib_pic33e/external.c"
   2              	.section .debug_abbrev,info
   3                 	.Ldebug_abbrev0:
   4                 	.section .debug_info,info
   5                 	.Ldebug_info0:
   6                 	.section .debug_line,info
   7                 	.Ldebug_line0:
   8 0000 58 01 00 00 	.section .text,code
   8      02 00 BE 00 
   8      00 00 01 01 
   8      FB 0E 0A 00 
   8      01 01 01 01 
   8      00 00 00 01 
   8      6C 69 62 2F 
   8      6C 69 62 5F 
   8      70 69 63 33 
   9              	.Ltext0:
  10              	.section .text,code
  11              	.align 2
  12              	.global _config_external0
  13              	.type _config_external0,@function
  14              	_config_external0:
  15              	.LFB0:
  16              	.file 1 "lib/lib_pic33e/external.c"
   1:lib/lib_pic33e/external.c **** #ifndef NOEXTERNAL
   2:lib/lib_pic33e/external.c **** 
   3:lib/lib_pic33e/external.c **** #include "external.h"
   4:lib/lib_pic33e/external.c **** #include <xc.h>
   5:lib/lib_pic33e/external.c **** 
   6:lib/lib_pic33e/external.c **** /**********************************************************************
   7:lib/lib_pic33e/external.c ****  * Name:    config_external0
   8:lib/lib_pic33e/external.c ****  * Args:    polarity (0/1), priority (0-7)
   9:lib/lib_pic33e/external.c ****  * Return:  -
  10:lib/lib_pic33e/external.c ****  * Desc:    Configures and enables INT0 module.
  11:lib/lib_pic33e/external.c ****  **********************************************************************/
  12:lib/lib_pic33e/external.c **** 
  13:lib/lib_pic33e/external.c **** void config_external0(bool polarity, unsigned int priority)
  14:lib/lib_pic33e/external.c **** {
  17              	.loc 1 14 0
  18              	.set ___PA___,1
  19 000000  04 00 FA 	lnk #4
  20              	.LCFI0:
  21              	.loc 1 14 0
  22 000002  00 4F 78 	mov.b w0,[w14]
  23 000004  11 07 98 	mov w1,[w14+2]
  15:lib/lib_pic33e/external.c ****     INTCON2bits.INT0EP = polarity; /*interrupt on 1=negative or 0=positive edge */
  24              	.loc 1 15 0
  25 000006  01 00 80 	mov _INTCON2bits,w1
  26 000008  1E 80 FB 	ze [w14],w0
  27 00000a  01 01 78 	mov w1,w2
  28 00000c  02 00 A1 	bclr w2,#0
  29 00000e  E1 00 60 	and w0,#1,w1
  16:lib/lib_pic33e/external.c ****     IFS0bits.INT0IF = 0;           /* clear interrupt flag                      */
  17:lib/lib_pic33e/external.c ****     IPC0bits.INT0IP = priority;    /* External 0 Interrupt Priority 0-7         */
  30              	.loc 1 17 0
  31 000010  1E 00 90 	mov [w14+2],w0
  32              	.loc 1 15 0
MPLAB XC16 ASSEMBLY Listing:   			page 2


  33 000012  82 80 70 	ior w1,w2,w1
  34              	.loc 1 17 0
  35 000014  00 40 78 	mov.b w0,w0
  36              	.loc 1 15 0
  37 000016  01 00 88 	mov w1,_INTCON2bits
  38              	.loc 1 17 0
  39 000018  67 40 60 	and.b w0,#7,w0
  40              	.loc 1 16 0
  41 00001a  00 00 A9 	bclr.b _IFS0bits,#0
  42              	.loc 1 17 0
  43 00001c  00 80 FB 	ze w0,w0
  44 00001e  02 00 80 	mov _IPC0bits,w2
  45 000020  67 00 60 	and w0,#7,w0
  46 000022  81 FF 2F 	mov #-8,w1
  47 000024  81 00 61 	and w2,w1,w1
  48 000026  01 00 70 	ior w0,w1,w0
  49 000028  00 00 88 	mov w0,_IPC0bits
  18:lib/lib_pic33e/external.c ****     IEC0bits.INT0IE = 1;           /* External 0 Interrupt Enable               */
  50              	.loc 1 18 0
  51 00002a  00 00 A8 	bset.b _IEC0bits,#0
  19:lib/lib_pic33e/external.c ****     return;
  20:lib/lib_pic33e/external.c **** 
  21:lib/lib_pic33e/external.c **** }
  52              	.loc 1 21 0
  53 00002c  8E 07 78 	mov w14,w15
  54 00002e  4F 07 78 	mov [--w15],w14
  55 000030  00 40 A9 	bclr CORCON,#2
  56 000032  00 00 06 	return 
  57              	.set ___PA___,0
  58              	.LFE0:
  59              	.size _config_external0,.-_config_external0
  60              	.align 2
  61              	.global _config_external1
  62              	.type _config_external1,@function
  63              	_config_external1:
  64              	.LFB1:
  22:lib/lib_pic33e/external.c **** 
  23:lib/lib_pic33e/external.c **** /**********************************************************************
  24:lib/lib_pic33e/external.c ****  * Name:    config_external1
  25:lib/lib_pic33e/external.c ****  * Args:    polarity (0/1), priority (0-7)
  26:lib/lib_pic33e/external.c ****  * Return:  -
  27:lib/lib_pic33e/external.c ****  * Desc:    Configures and enables INT1 module.
  28:lib/lib_pic33e/external.c ****  **********************************************************************/
  29:lib/lib_pic33e/external.c **** 
  30:lib/lib_pic33e/external.c **** void config_external1(bool polarity, unsigned int priority)
  31:lib/lib_pic33e/external.c **** {
  65              	.loc 1 31 0
  66              	.set ___PA___,1
  67 000034  04 00 FA 	lnk #4
  68              	.LCFI1:
  69              	.loc 1 31 0
  70 000036  00 4F 78 	mov.b w0,[w14]
  71 000038  11 07 98 	mov w1,[w14+2]
  32:lib/lib_pic33e/external.c ****     INTCON2bits.INT1EP = polarity; /*interrupt on 1=negative or 0=positive edge */
  72              	.loc 1 32 0
  73 00003a  01 00 80 	mov _INTCON2bits,w1
  74 00003c  1E 80 FB 	ze [w14],w0
MPLAB XC16 ASSEMBLY Listing:   			page 3


  75 00003e  01 01 78 	mov w1,w2
  76 000040  02 10 A1 	bclr w2,#1
  77 000042  61 00 60 	and w0,#1,w0
  33:lib/lib_pic33e/external.c ****     IFS1bits.INT1IF = 0;           /* clear interrupt flag                      */
  34:lib/lib_pic33e/external.c ****     IEC1bits.INT1IE = 1;           /* External 1 Interrupt Enable               */
  35:lib/lib_pic33e/external.c ****     IPC5bits.INT1IP = priority;    /* External 1 Interrupt Priority 0-7         */
  78              	.loc 1 35 0
  79 000044  9E 01 90 	mov [w14+2],w3
  80              	.loc 1 32 0
  81 000046  80 00 40 	add w0,w0,w1
  82              	.loc 1 35 0
  83 000048  03 40 78 	mov.b w3,w0
  84              	.loc 1 32 0
  85 00004a  82 80 70 	ior w1,w2,w1
  86              	.loc 1 35 0
  87 00004c  67 40 60 	and.b w0,#7,w0
  88              	.loc 1 32 0
  89 00004e  01 00 88 	mov w1,_INTCON2bits
  90              	.loc 1 35 0
  91 000050  00 80 FB 	ze w0,w0
  92              	.loc 1 33 0
  93 000052  00 80 A9 	bclr.b _IFS1bits,#4
  94              	.loc 1 35 0
  95 000054  67 00 60 	and w0,#7,w0
  96              	.loc 1 34 0
  97 000056  00 80 A8 	bset.b _IEC1bits,#4
  98              	.loc 1 35 0
  99 000058  81 FF 2F 	mov #-8,w1
 100 00005a  02 00 80 	mov _IPC5bits,w2
 101 00005c  81 00 61 	and w2,w1,w1
 102 00005e  01 00 70 	ior w0,w1,w0
 103 000060  00 00 88 	mov w0,_IPC5bits
  36:lib/lib_pic33e/external.c ****     return;
  37:lib/lib_pic33e/external.c **** 
  38:lib/lib_pic33e/external.c **** }
 104              	.loc 1 38 0
 105 000062  8E 07 78 	mov w14,w15
 106 000064  4F 07 78 	mov [--w15],w14
 107 000066  00 40 A9 	bclr CORCON,#2
 108 000068  00 00 06 	return 
 109              	.set ___PA___,0
 110              	.LFE1:
 111              	.size _config_external1,.-_config_external1
 112              	.align 2
 113              	.global _config_external2
 114              	.type _config_external2,@function
 115              	_config_external2:
 116              	.LFB2:
  39:lib/lib_pic33e/external.c **** 
  40:lib/lib_pic33e/external.c **** /**********************************************************************
  41:lib/lib_pic33e/external.c ****  * Name:    config_external2
  42:lib/lib_pic33e/external.c ****  * Args:    polarity (0/1), priority (0-7)
  43:lib/lib_pic33e/external.c ****  * Return:  -
  44:lib/lib_pic33e/external.c ****  * Desc:    Configures and enables INT2 module.
  45:lib/lib_pic33e/external.c ****  **********************************************************************/
  46:lib/lib_pic33e/external.c **** 
  47:lib/lib_pic33e/external.c **** void config_external2(bool polarity, unsigned int priority)
MPLAB XC16 ASSEMBLY Listing:   			page 4


  48:lib/lib_pic33e/external.c **** {
 117              	.loc 1 48 0
 118              	.set ___PA___,1
 119 00006a  04 00 FA 	lnk #4
 120              	.LCFI2:
 121              	.loc 1 48 0
 122 00006c  00 4F 78 	mov.b w0,[w14]
 123 00006e  11 07 98 	mov w1,[w14+2]
  49:lib/lib_pic33e/external.c ****     INTCON2bits.INT2EP = polarity; /*interrupt on 1=negative or 0=positive edge */
 124              	.loc 1 49 0
 125 000070  01 00 80 	mov _INTCON2bits,w1
 126 000072  1E 80 FB 	ze [w14],w0
 127 000074  01 01 78 	mov w1,w2
 128 000076  02 20 A1 	bclr w2,#2
 129 000078  E1 00 60 	and w0,#1,w1
  50:lib/lib_pic33e/external.c ****     IFS1bits.INT2IF = 0;           /* clear interrupt flag                      */
  51:lib/lib_pic33e/external.c ****     IEC1bits.INT2IE = 1;           /* External 2 Interrupt Enable               */
  52:lib/lib_pic33e/external.c ****     IPC7bits.INT2IP = priority;    /* External 2 Interrupt Priority 0-7         */
 130              	.loc 1 52 0
 131 00007a  1E 00 90 	mov [w14+2],w0
 132              	.loc 1 49 0
 133 00007c  C2 08 DD 	sl w1,#2,w1
 134              	.loc 1 52 0
 135 00007e  00 40 78 	mov.b w0,w0
 136              	.loc 1 49 0
 137 000080  82 80 70 	ior w1,w2,w1
 138              	.loc 1 52 0
 139 000082  67 40 60 	and.b w0,#7,w0
 140              	.loc 1 49 0
 141 000084  01 00 88 	mov w1,_INTCON2bits
 142              	.loc 1 52 0
 143 000086  00 80 FB 	ze w0,w0
 144              	.loc 1 50 0
 145 000088  01 A0 A9 	bclr.b _IFS1bits+1,#5
 146              	.loc 1 52 0
 147 00008a  67 00 60 	and w0,#7,w0
 148              	.loc 1 51 0
 149 00008c  01 A0 A8 	bset.b _IEC1bits+1,#5
 150              	.loc 1 52 0
 151 00008e  44 00 DD 	sl w0,#4,w0
 152 000090  02 00 80 	mov _IPC7bits,w2
 153 000092  F1 F8 2F 	mov #-113,w1
 154 000094  81 00 61 	and w2,w1,w1
 155 000096  01 00 70 	ior w0,w1,w0
 156 000098  00 00 88 	mov w0,_IPC7bits
  53:lib/lib_pic33e/external.c ****     return;
  54:lib/lib_pic33e/external.c **** }
 157              	.loc 1 54 0
 158 00009a  8E 07 78 	mov w14,w15
 159 00009c  4F 07 78 	mov [--w15],w14
 160 00009e  00 40 A9 	bclr CORCON,#2
 161 0000a0  00 00 06 	return 
 162              	.set ___PA___,0
 163              	.LFE2:
 164              	.size _config_external2,.-_config_external2
 165              	.align 2
 166              	.global _config_external3
MPLAB XC16 ASSEMBLY Listing:   			page 5


 167              	.type _config_external3,@function
 168              	_config_external3:
 169              	.LFB3:
  55:lib/lib_pic33e/external.c **** 
  56:lib/lib_pic33e/external.c **** /**********************************************************************
  57:lib/lib_pic33e/external.c ****  * Name:    config_external3
  58:lib/lib_pic33e/external.c ****  * Args:    polarity (0/1), priority (0-7)
  59:lib/lib_pic33e/external.c ****  * Return:  -
  60:lib/lib_pic33e/external.c ****  * Desc:    Configures and enables INT3 module.
  61:lib/lib_pic33e/external.c ****  **********************************************************************/
  62:lib/lib_pic33e/external.c **** 
  63:lib/lib_pic33e/external.c **** void config_external3(bool polarity, unsigned int priority)
  64:lib/lib_pic33e/external.c **** {
 170              	.loc 1 64 0
 171              	.set ___PA___,1
 172 0000a2  04 00 FA 	lnk #4
 173              	.LCFI3:
 174              	.loc 1 64 0
 175 0000a4  00 4F 78 	mov.b w0,[w14]
 176 0000a6  11 07 98 	mov w1,[w14+2]
  65:lib/lib_pic33e/external.c ****     INTCON2bits.INT3EP = polarity; /*interrupt on 1=negative or 0=positive edge */
 177              	.loc 1 65 0
 178 0000a8  01 00 80 	mov _INTCON2bits,w1
 179 0000aa  1E 80 FB 	ze [w14],w0
 180 0000ac  01 01 78 	mov w1,w2
 181 0000ae  02 30 A1 	bclr w2,#3
 182 0000b0  E1 00 60 	and w0,#1,w1
  66:lib/lib_pic33e/external.c ****     IFS3bits.INT3IF = 0;           /* clear interrupt flag                      */
  67:lib/lib_pic33e/external.c ****     IEC3bits.INT3IE = 1;           /* External 3 Interrupt Enable               */
  68:lib/lib_pic33e/external.c ****     IPC13bits.INT3IP = priority;    /* External 3 Interrupt Priority 0-7         */
 183              	.loc 1 68 0
 184 0000b2  1E 00 90 	mov [w14+2],w0
 185              	.loc 1 65 0
 186 0000b4  C3 08 DD 	sl w1,#3,w1
 187              	.loc 1 68 0
 188 0000b6  00 40 78 	mov.b w0,w0
 189              	.loc 1 65 0
 190 0000b8  82 80 70 	ior w1,w2,w1
 191              	.loc 1 68 0
 192 0000ba  67 40 60 	and.b w0,#7,w0
 193              	.loc 1 65 0
 194 0000bc  01 00 88 	mov w1,_INTCON2bits
 195              	.loc 1 68 0
 196 0000be  00 80 FB 	ze w0,w0
 197              	.loc 1 66 0
 198 0000c0  00 A0 A9 	bclr.b _IFS3bits,#5
 199              	.loc 1 68 0
 200 0000c2  67 00 60 	and w0,#7,w0
 201              	.loc 1 67 0
 202 0000c4  00 A0 A8 	bset.b _IEC3bits,#5
 203              	.loc 1 68 0
 204 0000c6  44 00 DD 	sl w0,#4,w0
 205 0000c8  02 00 80 	mov _IPC13bits,w2
 206 0000ca  F1 F8 2F 	mov #-113,w1
 207 0000cc  81 00 61 	and w2,w1,w1
 208 0000ce  01 00 70 	ior w0,w1,w0
 209 0000d0  00 00 88 	mov w0,_IPC13bits
MPLAB XC16 ASSEMBLY Listing:   			page 6


  69:lib/lib_pic33e/external.c ****     return;
  70:lib/lib_pic33e/external.c **** 
  71:lib/lib_pic33e/external.c **** }
 210              	.loc 1 71 0
 211 0000d2  8E 07 78 	mov w14,w15
 212 0000d4  4F 07 78 	mov [--w15],w14
 213 0000d6  00 40 A9 	bclr CORCON,#2
 214 0000d8  00 00 06 	return 
 215              	.set ___PA___,0
 216              	.LFE3:
 217              	.size _config_external3,.-_config_external3
 218              	.align 2
 219              	.global _config_external4
 220              	.type _config_external4,@function
 221              	_config_external4:
 222              	.LFB4:
  72:lib/lib_pic33e/external.c **** 
  73:lib/lib_pic33e/external.c **** /**********************************************************************
  74:lib/lib_pic33e/external.c ****  * Name:    config_external4
  75:lib/lib_pic33e/external.c ****  * Args:    polarity (0/1), priority (0-7)
  76:lib/lib_pic33e/external.c ****  * Return:  -
  77:lib/lib_pic33e/external.c ****  * Desc:    Configures and enables INT4 module.
  78:lib/lib_pic33e/external.c ****  **********************************************************************/
  79:lib/lib_pic33e/external.c **** 
  80:lib/lib_pic33e/external.c **** void config_external4(bool polarity, unsigned int priority)
  81:lib/lib_pic33e/external.c **** {
 223              	.loc 1 81 0
 224              	.set ___PA___,1
 225 0000da  04 00 FA 	lnk #4
 226              	.LCFI4:
 227              	.loc 1 81 0
 228 0000dc  00 4F 78 	mov.b w0,[w14]
 229 0000de  11 07 98 	mov w1,[w14+2]
  82:lib/lib_pic33e/external.c ****     INTCON2bits.INT4EP = polarity; /*interrupt on 1=negative or 0=positive edge */
 230              	.loc 1 82 0
 231 0000e0  01 00 80 	mov _INTCON2bits,w1
 232 0000e2  1E 80 FB 	ze [w14],w0
 233 0000e4  01 01 78 	mov w1,w2
 234 0000e6  02 40 A1 	bclr w2,#4
 235 0000e8  E1 00 60 	and w0,#1,w1
  83:lib/lib_pic33e/external.c ****     IFS3bits.INT4IF = 0;           /* clear interrupt flag                      */
  84:lib/lib_pic33e/external.c ****     IEC3bits.INT4IE = 1;           /* External 0 Interrupt Enable               */
  85:lib/lib_pic33e/external.c ****     IPC13bits.INT4IP = priority;    /* External 0 Interrupt Priority 0-7         */
 236              	.loc 1 85 0
 237 0000ea  1E 00 90 	mov [w14+2],w0
 238              	.loc 1 82 0
 239 0000ec  C4 08 DD 	sl w1,#4,w1
 240              	.loc 1 85 0
 241 0000ee  00 40 78 	mov.b w0,w0
 242              	.loc 1 82 0
 243 0000f0  82 80 70 	ior w1,w2,w1
 244              	.loc 1 85 0
 245 0000f2  67 40 60 	and.b w0,#7,w0
 246              	.loc 1 82 0
 247 0000f4  01 00 88 	mov w1,_INTCON2bits
 248              	.loc 1 85 0
 249 0000f6  00 80 FB 	ze w0,w0
MPLAB XC16 ASSEMBLY Listing:   			page 7


 250              	.loc 1 83 0
 251 0000f8  00 C0 A9 	bclr.b _IFS3bits,#6
 252              	.loc 1 85 0
 253 0000fa  67 00 60 	and w0,#7,w0
 254              	.loc 1 84 0
 255 0000fc  00 C0 A8 	bset.b _IEC3bits,#6
 256              	.loc 1 85 0
 257 0000fe  48 00 DD 	sl w0,#8,w0
 258 000100  02 00 80 	mov _IPC13bits,w2
 259 000102  F1 8F 2F 	mov #-1793,w1
 260 000104  81 00 61 	and w2,w1,w1
 261 000106  01 00 70 	ior w0,w1,w0
 262 000108  00 00 88 	mov w0,_IPC13bits
  86:lib/lib_pic33e/external.c ****     return;
  87:lib/lib_pic33e/external.c **** 
  88:lib/lib_pic33e/external.c **** }
 263              	.loc 1 88 0
 264 00010a  8E 07 78 	mov w14,w15
 265 00010c  4F 07 78 	mov [--w15],w14
 266 00010e  00 40 A9 	bclr CORCON,#2
 267 000110  00 00 06 	return 
 268              	.set ___PA___,0
 269              	.LFE4:
 270              	.size _config_external4,.-_config_external4
 271              	.align 2
 272              	.weak _external0_callback
 273              	.type _external0_callback,@function
 274              	_external0_callback:
 275              	.LFB5:
  89:lib/lib_pic33e/external.c **** 
  90:lib/lib_pic33e/external.c **** void __attribute__((weak)) external0_callback(void) {
 276              	.loc 1 90 0
 277              	.set ___PA___,1
 278 000112  00 00 FA 	lnk #0
 279              	.LCFI5:
  91:lib/lib_pic33e/external.c **** 	return;
  92:lib/lib_pic33e/external.c **** }
 280              	.loc 1 92 0
 281 000114  8E 07 78 	mov w14,w15
 282 000116  4F 07 78 	mov [--w15],w14
 283 000118  00 40 A9 	bclr CORCON,#2
 284 00011a  00 00 06 	return 
 285              	.set ___PA___,0
 286              	.LFE5:
 287              	.size _external0_callback,.-_external0_callback
 288              	.align 2
 289              	.weak _external1_callback
 290              	.type _external1_callback,@function
 291              	_external1_callback:
 292              	.LFB6:
  93:lib/lib_pic33e/external.c **** 
  94:lib/lib_pic33e/external.c **** void __attribute__((weak)) external1_callback(void) {
 293              	.loc 1 94 0
 294              	.set ___PA___,1
 295 00011c  00 00 FA 	lnk #0
 296              	.LCFI6:
  95:lib/lib_pic33e/external.c **** 	return;
MPLAB XC16 ASSEMBLY Listing:   			page 8


  96:lib/lib_pic33e/external.c **** }
 297              	.loc 1 96 0
 298 00011e  8E 07 78 	mov w14,w15
 299 000120  4F 07 78 	mov [--w15],w14
 300 000122  00 40 A9 	bclr CORCON,#2
 301 000124  00 00 06 	return 
 302              	.set ___PA___,0
 303              	.LFE6:
 304              	.size _external1_callback,.-_external1_callback
 305              	.align 2
 306              	.weak _external2_callback
 307              	.type _external2_callback,@function
 308              	_external2_callback:
 309              	.LFB7:
  97:lib/lib_pic33e/external.c **** 
  98:lib/lib_pic33e/external.c **** void __attribute__((weak)) external2_callback(void) {
 310              	.loc 1 98 0
 311              	.set ___PA___,1
 312 000126  00 00 FA 	lnk #0
 313              	.LCFI7:
  99:lib/lib_pic33e/external.c **** 	return;
 100:lib/lib_pic33e/external.c **** }
 314              	.loc 1 100 0
 315 000128  8E 07 78 	mov w14,w15
 316 00012a  4F 07 78 	mov [--w15],w14
 317 00012c  00 40 A9 	bclr CORCON,#2
 318 00012e  00 00 06 	return 
 319              	.set ___PA___,0
 320              	.LFE7:
 321              	.size _external2_callback,.-_external2_callback
 322              	.align 2
 323              	.weak _external3_callback
 324              	.type _external3_callback,@function
 325              	_external3_callback:
 326              	.LFB8:
 101:lib/lib_pic33e/external.c **** 
 102:lib/lib_pic33e/external.c **** void __attribute__((weak)) external3_callback(void) {
 327              	.loc 1 102 0
 328              	.set ___PA___,1
 329 000130  00 00 FA 	lnk #0
 330              	.LCFI8:
 103:lib/lib_pic33e/external.c **** 	return;
 104:lib/lib_pic33e/external.c **** }
 331              	.loc 1 104 0
 332 000132  8E 07 78 	mov w14,w15
 333 000134  4F 07 78 	mov [--w15],w14
 334 000136  00 40 A9 	bclr CORCON,#2
 335 000138  00 00 06 	return 
 336              	.set ___PA___,0
 337              	.LFE8:
 338              	.size _external3_callback,.-_external3_callback
 339              	.align 2
 340              	.weak _external4_callback
 341              	.type _external4_callback,@function
 342              	_external4_callback:
 343              	.LFB9:
 105:lib/lib_pic33e/external.c **** 
MPLAB XC16 ASSEMBLY Listing:   			page 9


 106:lib/lib_pic33e/external.c **** void __attribute__((weak)) external4_callback(void) {
 344              	.loc 1 106 0
 345              	.set ___PA___,1
 346 00013a  00 00 FA 	lnk #0
 347              	.LCFI9:
 107:lib/lib_pic33e/external.c **** 	return;
 108:lib/lib_pic33e/external.c **** }
 348              	.loc 1 108 0
 349 00013c  8E 07 78 	mov w14,w15
 350 00013e  4F 07 78 	mov [--w15],w14
 351 000140  00 40 A9 	bclr CORCON,#2
 352 000142  00 00 06 	return 
 353              	.set ___PA___,0
 354              	.LFE9:
 355              	.size _external4_callback,.-_external4_callback
 356              	.section .isr.text,code,keep
 357              	.align 2
 358              	.global __INT0Interrupt
 359              	.type __INT0Interrupt,@function
 360              	__INT0Interrupt:
 361              	.section .isr.text,code,keep
 362              	.LFB10:
 363              	.section .isr.text,code,keep
 109:lib/lib_pic33e/external.c **** 
 110:lib/lib_pic33e/external.c **** /**********************************************************************
 111:lib/lib_pic33e/external.c ****  * Assign INT0 interruption
 112:lib/lib_pic33e/external.c ****  **********************************************************************/
 113:lib/lib_pic33e/external.c **** void __attribute__((interrupt, auto_psv, shadow)) _INT0Interrupt (void){
 364              	.loc 1 113 0
 365              	.set ___PA___,1
 366 000000  00 A0 FE 	push.s 
 367 000002  00 00 F8 	push _RCOUNT
 368              	.LCFI10:
 369 000004  84 9F BE 	mov.d w4,[w15++]
 370              	.LCFI11:
 371 000006  86 9F BE 	mov.d w6,[w15++]
 372              	.LCFI12:
 373 000008  00 00 F8 	push _DSRPAG
 374              	.LCFI13:
 375 00000a  00 00 F8 	push _DSWPAG
 376              	.LCFI14:
 377 00000c  14 00 20 	mov #1,w4
 378 00000e  04 00 88 	mov w4,_DSWPAG
 379 000010  04 00 20 	mov #__const_psvpage,w4
 380 000012  04 00 88 	mov w4,_DSRPAG
 381 000014  00 00 00 	nop 
 382 000016  00 00 FA 	lnk #0
 383              	.LCFI15:
 384              	.section .isr.text,code,keep
 114:lib/lib_pic33e/external.c ****     external0_callback();
 385              	.loc 1 114 0
 386 000018  00 00 07 	rcall _external0_callback
 387              	.section .isr.text,code,keep
 115:lib/lib_pic33e/external.c ****     IFS0bits.INT0IF = 0;           /* clear interrupt flag                      */
 388              	.loc 1 115 0
 389 00001a  00 00 A9 	bclr.b _IFS0bits,#0
 390              	.section .isr.text,code,keep
MPLAB XC16 ASSEMBLY Listing:   			page 10


 116:lib/lib_pic33e/external.c **** 
 117:lib/lib_pic33e/external.c **** }
 391              	.loc 1 117 0
 392 00001c  8E 07 78 	mov w14,w15
 393 00001e  4F 07 78 	mov [--w15],w14
 394 000020  00 40 A9 	bclr CORCON,#2
 395 000022  00 00 F9 	pop _DSWPAG
 396 000024  00 00 F9 	pop _DSRPAG
 397 000026  80 1F 78 	mov w0,[w15++]
 398 000028  00 0E 20 	mov #224,w0
 399 00002a  00 20 B7 	ior _SR
 400 00002c  4F 00 78 	mov [--w15],w0
 401 00002e  4F 03 BE 	mov.d [--w15],w6
 402 000030  4F 02 BE 	mov.d [--w15],w4
 403 000032  00 00 F9 	pop _RCOUNT
 404 000034  00 80 FE 	pop.s 
 405 000036  00 40 06 	retfie 
 406              	.set ___PA___,0
 407              	.LFE10:
 408              	.size __INT0Interrupt,.-__INT0Interrupt
 409              	.section .isr.text,code,keep
 410              	.align 2
 411              	.global __INT1Interrupt
 412              	.type __INT1Interrupt,@function
 413              	__INT1Interrupt:
 414              	.section .isr.text,code,keep
 415              	.LFB11:
 416              	.section .isr.text,code,keep
 118:lib/lib_pic33e/external.c **** 
 119:lib/lib_pic33e/external.c **** /**********************************************************************
 120:lib/lib_pic33e/external.c ****  * Assign INT1 interruption
 121:lib/lib_pic33e/external.c ****  **********************************************************************/
 122:lib/lib_pic33e/external.c **** 
 123:lib/lib_pic33e/external.c **** void __attribute__((interrupt, auto_psv, shadow)) _INT1Interrupt (void){
 417              	.loc 1 123 0
 418              	.set ___PA___,1
 419 000038  00 A0 FE 	push.s 
 420 00003a  00 00 F8 	push _RCOUNT
 421              	.LCFI16:
 422 00003c  84 9F BE 	mov.d w4,[w15++]
 423              	.LCFI17:
 424 00003e  86 9F BE 	mov.d w6,[w15++]
 425              	.LCFI18:
 426 000040  00 00 F8 	push _DSRPAG
 427              	.LCFI19:
 428 000042  00 00 F8 	push _DSWPAG
 429              	.LCFI20:
 430 000044  14 00 20 	mov #1,w4
 431 000046  04 00 88 	mov w4,_DSWPAG
 432 000048  04 00 20 	mov #__const_psvpage,w4
 433 00004a  04 00 88 	mov w4,_DSRPAG
 434 00004c  00 00 00 	nop 
 435 00004e  00 00 FA 	lnk #0
 436              	.LCFI21:
 437              	.section .isr.text,code,keep
 124:lib/lib_pic33e/external.c ****     external1_callback();
 438              	.loc 1 124 0
MPLAB XC16 ASSEMBLY Listing:   			page 11


 439 000050  00 00 07 	rcall _external1_callback
 440              	.section .isr.text,code,keep
 125:lib/lib_pic33e/external.c ****     IFS1bits.INT1IF = 0;           /* clear interrupt flag                      */
 441              	.loc 1 125 0
 442 000052  00 80 A9 	bclr.b _IFS1bits,#4
 443              	.section .isr.text,code,keep
 126:lib/lib_pic33e/external.c **** 
 127:lib/lib_pic33e/external.c ****     return;
 128:lib/lib_pic33e/external.c **** 
 129:lib/lib_pic33e/external.c **** }
 444              	.loc 1 129 0
 445 000054  8E 07 78 	mov w14,w15
 446 000056  4F 07 78 	mov [--w15],w14
 447 000058  00 40 A9 	bclr CORCON,#2
 448 00005a  00 00 F9 	pop _DSWPAG
 449 00005c  00 00 F9 	pop _DSRPAG
 450 00005e  80 1F 78 	mov w0,[w15++]
 451 000060  00 0E 20 	mov #224,w0
 452 000062  00 20 B7 	ior _SR
 453 000064  4F 00 78 	mov [--w15],w0
 454 000066  4F 03 BE 	mov.d [--w15],w6
 455 000068  4F 02 BE 	mov.d [--w15],w4
 456 00006a  00 00 F9 	pop _RCOUNT
 457 00006c  00 80 FE 	pop.s 
 458 00006e  00 40 06 	retfie 
 459              	.set ___PA___,0
 460              	.LFE11:
 461              	.size __INT1Interrupt,.-__INT1Interrupt
 462              	.section .isr.text,code,keep
 463              	.align 2
 464              	.global __INT2Interrupt
 465              	.type __INT2Interrupt,@function
 466              	__INT2Interrupt:
 467              	.section .isr.text,code,keep
 468              	.LFB12:
 469              	.section .isr.text,code,keep
 130:lib/lib_pic33e/external.c **** 
 131:lib/lib_pic33e/external.c **** /**********************************************************************
 132:lib/lib_pic33e/external.c ****  * Assign INT2 interruption
 133:lib/lib_pic33e/external.c ****  **********************************************************************/
 134:lib/lib_pic33e/external.c **** 
 135:lib/lib_pic33e/external.c **** void __attribute__((interrupt, auto_psv, shadow)) _INT2Interrupt (void){
 470              	.loc 1 135 0
 471              	.set ___PA___,1
 472 000070  00 A0 FE 	push.s 
 473 000072  00 00 F8 	push _RCOUNT
 474              	.LCFI22:
 475 000074  84 9F BE 	mov.d w4,[w15++]
 476              	.LCFI23:
 477 000076  86 9F BE 	mov.d w6,[w15++]
 478              	.LCFI24:
 479 000078  00 00 F8 	push _DSRPAG
 480              	.LCFI25:
 481 00007a  00 00 F8 	push _DSWPAG
 482              	.LCFI26:
 483 00007c  14 00 20 	mov #1,w4
 484 00007e  04 00 88 	mov w4,_DSWPAG
MPLAB XC16 ASSEMBLY Listing:   			page 12


 485 000080  04 00 20 	mov #__const_psvpage,w4
 486 000082  04 00 88 	mov w4,_DSRPAG
 487 000084  00 00 00 	nop 
 488 000086  00 00 FA 	lnk #0
 489              	.LCFI27:
 490              	.section .isr.text,code,keep
 136:lib/lib_pic33e/external.c ****     external2_callback();
 491              	.loc 1 136 0
 492 000088  00 00 07 	rcall _external2_callback
 493              	.section .isr.text,code,keep
 137:lib/lib_pic33e/external.c ****     IFS1bits.INT2IF = 0;           /* clear interrupt flag                      */
 494              	.loc 1 137 0
 495 00008a  01 A0 A9 	bclr.b _IFS1bits+1,#5
 496              	.section .isr.text,code,keep
 138:lib/lib_pic33e/external.c **** 
 139:lib/lib_pic33e/external.c ****     return;
 140:lib/lib_pic33e/external.c **** }
 497              	.loc 1 140 0
 498 00008c  8E 07 78 	mov w14,w15
 499 00008e  4F 07 78 	mov [--w15],w14
 500 000090  00 40 A9 	bclr CORCON,#2
 501 000092  00 00 F9 	pop _DSWPAG
 502 000094  00 00 F9 	pop _DSRPAG
 503 000096  80 1F 78 	mov w0,[w15++]
 504 000098  00 0E 20 	mov #224,w0
 505 00009a  00 20 B7 	ior _SR
 506 00009c  4F 00 78 	mov [--w15],w0
 507 00009e  4F 03 BE 	mov.d [--w15],w6
 508 0000a0  4F 02 BE 	mov.d [--w15],w4
 509 0000a2  00 00 F9 	pop _RCOUNT
 510 0000a4  00 80 FE 	pop.s 
 511 0000a6  00 40 06 	retfie 
 512              	.set ___PA___,0
 513              	.LFE12:
 514              	.size __INT2Interrupt,.-__INT2Interrupt
 515              	.section .isr.text,code,keep
 516              	.align 2
 517              	.global __INT3Interrupt
 518              	.type __INT3Interrupt,@function
 519              	__INT3Interrupt:
 520              	.section .isr.text,code,keep
 521              	.LFB13:
 522              	.section .isr.text,code,keep
 141:lib/lib_pic33e/external.c **** 
 142:lib/lib_pic33e/external.c **** /**********************************************************************
 143:lib/lib_pic33e/external.c ****  * Assign INT3 interruption
 144:lib/lib_pic33e/external.c ****  **********************************************************************/
 145:lib/lib_pic33e/external.c **** 
 146:lib/lib_pic33e/external.c **** void __attribute__((interrupt, auto_psv, shadow)) _INT3Interrupt (void){
 523              	.loc 1 146 0
 524              	.set ___PA___,1
 525 0000a8  00 A0 FE 	push.s 
 526 0000aa  00 00 F8 	push _RCOUNT
 527              	.LCFI28:
 528 0000ac  84 9F BE 	mov.d w4,[w15++]
 529              	.LCFI29:
 530 0000ae  86 9F BE 	mov.d w6,[w15++]
MPLAB XC16 ASSEMBLY Listing:   			page 13


 531              	.LCFI30:
 532 0000b0  00 00 F8 	push _DSRPAG
 533              	.LCFI31:
 534 0000b2  00 00 F8 	push _DSWPAG
 535              	.LCFI32:
 536 0000b4  14 00 20 	mov #1,w4
 537 0000b6  04 00 88 	mov w4,_DSWPAG
 538 0000b8  04 00 20 	mov #__const_psvpage,w4
 539 0000ba  04 00 88 	mov w4,_DSRPAG
 540 0000bc  00 00 00 	nop 
 541 0000be  00 00 FA 	lnk #0
 542              	.LCFI33:
 543              	.section .isr.text,code,keep
 147:lib/lib_pic33e/external.c ****     external3_callback();
 544              	.loc 1 147 0
 545 0000c0  00 00 07 	rcall _external3_callback
 546              	.section .isr.text,code,keep
 148:lib/lib_pic33e/external.c ****     IFS3bits.INT3IF = 0;           /* clear interrupt flag                      */
 547              	.loc 1 148 0
 548 0000c2  00 A0 A9 	bclr.b _IFS3bits,#5
 549              	.section .isr.text,code,keep
 149:lib/lib_pic33e/external.c **** 
 150:lib/lib_pic33e/external.c ****     return;
 151:lib/lib_pic33e/external.c **** }
 550              	.loc 1 151 0
 551 0000c4  8E 07 78 	mov w14,w15
 552 0000c6  4F 07 78 	mov [--w15],w14
 553 0000c8  00 40 A9 	bclr CORCON,#2
 554 0000ca  00 00 F9 	pop _DSWPAG
 555 0000cc  00 00 F9 	pop _DSRPAG
 556 0000ce  80 1F 78 	mov w0,[w15++]
 557 0000d0  00 0E 20 	mov #224,w0
 558 0000d2  00 20 B7 	ior _SR
 559 0000d4  4F 00 78 	mov [--w15],w0
 560 0000d6  4F 03 BE 	mov.d [--w15],w6
 561 0000d8  4F 02 BE 	mov.d [--w15],w4
 562 0000da  00 00 F9 	pop _RCOUNT
 563 0000dc  00 80 FE 	pop.s 
 564 0000de  00 40 06 	retfie 
 565              	.set ___PA___,0
 566              	.LFE13:
 567              	.size __INT3Interrupt,.-__INT3Interrupt
 568              	.section .isr.text,code,keep
 569              	.align 2
 570              	.global __INT4Interrupt
 571              	.type __INT4Interrupt,@function
 572              	__INT4Interrupt:
 573              	.section .isr.text,code,keep
 574              	.LFB14:
 575              	.section .isr.text,code,keep
 152:lib/lib_pic33e/external.c **** 
 153:lib/lib_pic33e/external.c **** /**********************************************************************
 154:lib/lib_pic33e/external.c ****  * Assign INT4 interruption
 155:lib/lib_pic33e/external.c ****  **********************************************************************/
 156:lib/lib_pic33e/external.c **** 
 157:lib/lib_pic33e/external.c **** void __attribute__((interrupt, auto_psv, shadow)) _INT4Interrupt (void){
 576              	.loc 1 157 0
MPLAB XC16 ASSEMBLY Listing:   			page 14


 577              	.set ___PA___,1
 578 0000e0  00 A0 FE 	push.s 
 579 0000e2  00 00 F8 	push _RCOUNT
 580              	.LCFI34:
 581 0000e4  84 9F BE 	mov.d w4,[w15++]
 582              	.LCFI35:
 583 0000e6  86 9F BE 	mov.d w6,[w15++]
 584              	.LCFI36:
 585 0000e8  00 00 F8 	push _DSRPAG
 586              	.LCFI37:
 587 0000ea  00 00 F8 	push _DSWPAG
 588              	.LCFI38:
 589 0000ec  14 00 20 	mov #1,w4
 590 0000ee  04 00 88 	mov w4,_DSWPAG
 591 0000f0  04 00 20 	mov #__const_psvpage,w4
 592 0000f2  04 00 88 	mov w4,_DSRPAG
 593 0000f4  00 00 00 	nop 
 594 0000f6  00 00 FA 	lnk #0
 595              	.LCFI39:
 596              	.section .isr.text,code,keep
 158:lib/lib_pic33e/external.c ****     external4_callback();
 597              	.loc 1 158 0
 598 0000f8  00 00 07 	rcall _external4_callback
 599              	.section .isr.text,code,keep
 159:lib/lib_pic33e/external.c ****     IFS3bits.INT4IF = 0;           /* clear interrupt flag                      */
 600              	.loc 1 159 0
 601 0000fa  00 C0 A9 	bclr.b _IFS3bits,#6
 602              	.section .isr.text,code,keep
 160:lib/lib_pic33e/external.c **** 
 161:lib/lib_pic33e/external.c ****     return;
 162:lib/lib_pic33e/external.c **** }
 603              	.loc 1 162 0
 604 0000fc  8E 07 78 	mov w14,w15
 605 0000fe  4F 07 78 	mov [--w15],w14
 606 000100  00 40 A9 	bclr CORCON,#2
 607 000102  00 00 F9 	pop _DSWPAG
 608 000104  00 00 F9 	pop _DSRPAG
 609 000106  80 1F 78 	mov w0,[w15++]
 610 000108  00 0E 20 	mov #224,w0
 611 00010a  00 20 B7 	ior _SR
 612 00010c  4F 00 78 	mov [--w15],w0
 613 00010e  4F 03 BE 	mov.d [--w15],w6
 614 000110  4F 02 BE 	mov.d [--w15],w4
 615 000112  00 00 F9 	pop _RCOUNT
 616 000114  00 80 FE 	pop.s 
 617 000116  00 40 06 	retfie 
 618              	.set ___PA___,0
 619              	.LFE14:
 620              	.size __INT4Interrupt,.-__INT4Interrupt
 621              	.section .debug_frame,info
 622                 	.Lframe0:
 623 0000 10 00 00 00 	.4byte .LECIE0-.LSCIE0
 624                 	.LSCIE0:
 625 0004 FF FF FF FF 	.4byte 0xffffffff
 626 0008 01          	.byte 0x1
 627 0009 00          	.byte 0
 628 000a 01          	.uleb128 0x1
MPLAB XC16 ASSEMBLY Listing:   			page 15


 629 000b 02          	.sleb128 2
 630 000c 25          	.byte 0x25
 631 000d 12          	.byte 0x12
 632 000e 0F          	.uleb128 0xf
 633 000f 7E          	.sleb128 -2
 634 0010 09          	.byte 0x9
 635 0011 25          	.uleb128 0x25
 636 0012 0F          	.uleb128 0xf
 637 0013 00          	.align 4
 638                 	.LECIE0:
 639                 	.LSFDE0:
 640 0014 18 00 00 00 	.4byte .LEFDE0-.LASFDE0
 641                 	.LASFDE0:
 642 0018 00 00 00 00 	.4byte .Lframe0
 643 001c 00 00 00 00 	.4byte .LFB0
 644 0020 34 00 00 00 	.4byte .LFE0-.LFB0
 645 0024 04          	.byte 0x4
 646 0025 02 00 00 00 	.4byte .LCFI0-.LFB0
 647 0029 13          	.byte 0x13
 648 002a 7D          	.sleb128 -3
 649 002b 0D          	.byte 0xd
 650 002c 0E          	.uleb128 0xe
 651 002d 8E          	.byte 0x8e
 652 002e 02          	.uleb128 0x2
 653 002f 00          	.align 4
 654                 	.LEFDE0:
 655                 	.LSFDE2:
 656 0030 18 00 00 00 	.4byte .LEFDE2-.LASFDE2
 657                 	.LASFDE2:
 658 0034 00 00 00 00 	.4byte .Lframe0
 659 0038 00 00 00 00 	.4byte .LFB1
 660 003c 36 00 00 00 	.4byte .LFE1-.LFB1
 661 0040 04          	.byte 0x4
 662 0041 02 00 00 00 	.4byte .LCFI1-.LFB1
 663 0045 13          	.byte 0x13
 664 0046 7D          	.sleb128 -3
 665 0047 0D          	.byte 0xd
 666 0048 0E          	.uleb128 0xe
 667 0049 8E          	.byte 0x8e
 668 004a 02          	.uleb128 0x2
 669 004b 00          	.align 4
 670                 	.LEFDE2:
 671                 	.LSFDE4:
 672 004c 18 00 00 00 	.4byte .LEFDE4-.LASFDE4
 673                 	.LASFDE4:
 674 0050 00 00 00 00 	.4byte .Lframe0
 675 0054 00 00 00 00 	.4byte .LFB2
 676 0058 38 00 00 00 	.4byte .LFE2-.LFB2
 677 005c 04          	.byte 0x4
 678 005d 02 00 00 00 	.4byte .LCFI2-.LFB2
 679 0061 13          	.byte 0x13
 680 0062 7D          	.sleb128 -3
 681 0063 0D          	.byte 0xd
 682 0064 0E          	.uleb128 0xe
 683 0065 8E          	.byte 0x8e
 684 0066 02          	.uleb128 0x2
 685 0067 00          	.align 4
MPLAB XC16 ASSEMBLY Listing:   			page 16


 686                 	.LEFDE4:
 687                 	.LSFDE6:
 688 0068 18 00 00 00 	.4byte .LEFDE6-.LASFDE6
 689                 	.LASFDE6:
 690 006c 00 00 00 00 	.4byte .Lframe0
 691 0070 00 00 00 00 	.4byte .LFB3
 692 0074 38 00 00 00 	.4byte .LFE3-.LFB3
 693 0078 04          	.byte 0x4
 694 0079 02 00 00 00 	.4byte .LCFI3-.LFB3
 695 007d 13          	.byte 0x13
 696 007e 7D          	.sleb128 -3
 697 007f 0D          	.byte 0xd
 698 0080 0E          	.uleb128 0xe
 699 0081 8E          	.byte 0x8e
 700 0082 02          	.uleb128 0x2
 701 0083 00          	.align 4
 702                 	.LEFDE6:
 703                 	.LSFDE8:
 704 0084 18 00 00 00 	.4byte .LEFDE8-.LASFDE8
 705                 	.LASFDE8:
 706 0088 00 00 00 00 	.4byte .Lframe0
 707 008c 00 00 00 00 	.4byte .LFB4
 708 0090 38 00 00 00 	.4byte .LFE4-.LFB4
 709 0094 04          	.byte 0x4
 710 0095 02 00 00 00 	.4byte .LCFI4-.LFB4
 711 0099 13          	.byte 0x13
 712 009a 7D          	.sleb128 -3
 713 009b 0D          	.byte 0xd
 714 009c 0E          	.uleb128 0xe
 715 009d 8E          	.byte 0x8e
 716 009e 02          	.uleb128 0x2
 717 009f 00          	.align 4
 718                 	.LEFDE8:
 719                 	.LSFDE10:
 720 00a0 18 00 00 00 	.4byte .LEFDE10-.LASFDE10
 721                 	.LASFDE10:
 722 00a4 00 00 00 00 	.4byte .Lframe0
 723 00a8 00 00 00 00 	.4byte .LFB5
 724 00ac 0A 00 00 00 	.4byte .LFE5-.LFB5
 725 00b0 04          	.byte 0x4
 726 00b1 02 00 00 00 	.4byte .LCFI5-.LFB5
 727 00b5 13          	.byte 0x13
 728 00b6 7D          	.sleb128 -3
 729 00b7 0D          	.byte 0xd
 730 00b8 0E          	.uleb128 0xe
 731 00b9 8E          	.byte 0x8e
 732 00ba 02          	.uleb128 0x2
 733 00bb 00          	.align 4
 734                 	.LEFDE10:
 735                 	.LSFDE12:
 736 00bc 18 00 00 00 	.4byte .LEFDE12-.LASFDE12
 737                 	.LASFDE12:
 738 00c0 00 00 00 00 	.4byte .Lframe0
 739 00c4 00 00 00 00 	.4byte .LFB6
 740 00c8 0A 00 00 00 	.4byte .LFE6-.LFB6
 741 00cc 04          	.byte 0x4
 742 00cd 02 00 00 00 	.4byte .LCFI6-.LFB6
MPLAB XC16 ASSEMBLY Listing:   			page 17


 743 00d1 13          	.byte 0x13
 744 00d2 7D          	.sleb128 -3
 745 00d3 0D          	.byte 0xd
 746 00d4 0E          	.uleb128 0xe
 747 00d5 8E          	.byte 0x8e
 748 00d6 02          	.uleb128 0x2
 749 00d7 00          	.align 4
 750                 	.LEFDE12:
 751                 	.LSFDE14:
 752 00d8 18 00 00 00 	.4byte .LEFDE14-.LASFDE14
 753                 	.LASFDE14:
 754 00dc 00 00 00 00 	.4byte .Lframe0
 755 00e0 00 00 00 00 	.4byte .LFB7
 756 00e4 0A 00 00 00 	.4byte .LFE7-.LFB7
 757 00e8 04          	.byte 0x4
 758 00e9 02 00 00 00 	.4byte .LCFI7-.LFB7
 759 00ed 13          	.byte 0x13
 760 00ee 7D          	.sleb128 -3
 761 00ef 0D          	.byte 0xd
 762 00f0 0E          	.uleb128 0xe
 763 00f1 8E          	.byte 0x8e
 764 00f2 02          	.uleb128 0x2
 765 00f3 00          	.align 4
 766                 	.LEFDE14:
 767                 	.LSFDE16:
 768 00f4 18 00 00 00 	.4byte .LEFDE16-.LASFDE16
 769                 	.LASFDE16:
 770 00f8 00 00 00 00 	.4byte .Lframe0
 771 00fc 00 00 00 00 	.4byte .LFB8
 772 0100 0A 00 00 00 	.4byte .LFE8-.LFB8
 773 0104 04          	.byte 0x4
 774 0105 02 00 00 00 	.4byte .LCFI8-.LFB8
 775 0109 13          	.byte 0x13
 776 010a 7D          	.sleb128 -3
 777 010b 0D          	.byte 0xd
 778 010c 0E          	.uleb128 0xe
 779 010d 8E          	.byte 0x8e
 780 010e 02          	.uleb128 0x2
 781 010f 00          	.align 4
 782                 	.LEFDE16:
 783                 	.LSFDE18:
 784 0110 18 00 00 00 	.4byte .LEFDE18-.LASFDE18
 785                 	.LASFDE18:
 786 0114 00 00 00 00 	.4byte .Lframe0
 787 0118 00 00 00 00 	.4byte .LFB9
 788 011c 0A 00 00 00 	.4byte .LFE9-.LFB9
 789 0120 04          	.byte 0x4
 790 0121 02 00 00 00 	.4byte .LCFI9-.LFB9
 791 0125 13          	.byte 0x13
 792 0126 7D          	.sleb128 -3
 793 0127 0D          	.byte 0xd
 794 0128 0E          	.uleb128 0xe
 795 0129 8E          	.byte 0x8e
 796 012a 02          	.uleb128 0x2
 797 012b 00          	.align 4
 798                 	.LEFDE18:
 799                 	.LSFDE20:
MPLAB XC16 ASSEMBLY Listing:   			page 18


 800 012c 2E 00 00 00 	.4byte .LEFDE20-.LASFDE20
 801                 	.LASFDE20:
 802 0130 00 00 00 00 	.4byte .Lframe0
 803 0134 00 00 00 00 	.4byte .LFB10
 804 0138 38 00 00 00 	.4byte .LFE10-.LFB10
 805 013c 04          	.byte 0x4
 806 013d 06 00 00 00 	.4byte .LCFI11-.LFB10
 807 0141 13          	.byte 0x13
 808 0142 7B          	.sleb128 -5
 809 0143 04          	.byte 0x4
 810 0144 02 00 00 00 	.4byte .LCFI12-.LCFI11
 811 0148 13          	.byte 0x13
 812 0149 79          	.sleb128 -7
 813 014a 04          	.byte 0x4
 814 014b 04 00 00 00 	.4byte .LCFI14-.LCFI12
 815 014f 86          	.byte 0x86
 816 0150 05          	.uleb128 0x5
 817 0151 84          	.byte 0x84
 818 0152 03          	.uleb128 0x3
 819 0153 04          	.byte 0x4
 820 0154 0C 00 00 00 	.4byte .LCFI15-.LCFI14
 821 0158 13          	.byte 0x13
 822 0159 76          	.sleb128 -10
 823 015a 0D          	.byte 0xd
 824 015b 0E          	.uleb128 0xe
 825 015c 8E          	.byte 0x8e
 826 015d 09          	.uleb128 0x9
 827                 	.align 4
 828                 	.LEFDE20:
 829                 	.LSFDE22:
 830 015e 2E 00 00 00 	.4byte .LEFDE22-.LASFDE22
 831                 	.LASFDE22:
 832 0162 00 00 00 00 	.4byte .Lframe0
 833 0166 00 00 00 00 	.4byte .LFB11
 834 016a 38 00 00 00 	.4byte .LFE11-.LFB11
 835 016e 04          	.byte 0x4
 836 016f 06 00 00 00 	.4byte .LCFI17-.LFB11
 837 0173 13          	.byte 0x13
 838 0174 7B          	.sleb128 -5
 839 0175 04          	.byte 0x4
 840 0176 02 00 00 00 	.4byte .LCFI18-.LCFI17
 841 017a 13          	.byte 0x13
 842 017b 79          	.sleb128 -7
 843 017c 04          	.byte 0x4
 844 017d 04 00 00 00 	.4byte .LCFI20-.LCFI18
 845 0181 86          	.byte 0x86
 846 0182 05          	.uleb128 0x5
 847 0183 84          	.byte 0x84
 848 0184 03          	.uleb128 0x3
 849 0185 04          	.byte 0x4
 850 0186 0C 00 00 00 	.4byte .LCFI21-.LCFI20
 851 018a 13          	.byte 0x13
 852 018b 76          	.sleb128 -10
 853 018c 0D          	.byte 0xd
 854 018d 0E          	.uleb128 0xe
 855 018e 8E          	.byte 0x8e
 856 018f 09          	.uleb128 0x9
MPLAB XC16 ASSEMBLY Listing:   			page 19


 857                 	.align 4
 858                 	.LEFDE22:
 859                 	.LSFDE24:
 860 0190 2E 00 00 00 	.4byte .LEFDE24-.LASFDE24
 861                 	.LASFDE24:
 862 0194 00 00 00 00 	.4byte .Lframe0
 863 0198 00 00 00 00 	.4byte .LFB12
 864 019c 38 00 00 00 	.4byte .LFE12-.LFB12
 865 01a0 04          	.byte 0x4
 866 01a1 06 00 00 00 	.4byte .LCFI23-.LFB12
 867 01a5 13          	.byte 0x13
 868 01a6 7B          	.sleb128 -5
 869 01a7 04          	.byte 0x4
 870 01a8 02 00 00 00 	.4byte .LCFI24-.LCFI23
 871 01ac 13          	.byte 0x13
 872 01ad 79          	.sleb128 -7
 873 01ae 04          	.byte 0x4
 874 01af 04 00 00 00 	.4byte .LCFI26-.LCFI24
 875 01b3 86          	.byte 0x86
 876 01b4 05          	.uleb128 0x5
 877 01b5 84          	.byte 0x84
 878 01b6 03          	.uleb128 0x3
 879 01b7 04          	.byte 0x4
 880 01b8 0C 00 00 00 	.4byte .LCFI27-.LCFI26
 881 01bc 13          	.byte 0x13
 882 01bd 76          	.sleb128 -10
 883 01be 0D          	.byte 0xd
 884 01bf 0E          	.uleb128 0xe
 885 01c0 8E          	.byte 0x8e
 886 01c1 09          	.uleb128 0x9
 887                 	.align 4
 888                 	.LEFDE24:
 889                 	.LSFDE26:
 890 01c2 2E 00 00 00 	.4byte .LEFDE26-.LASFDE26
 891                 	.LASFDE26:
 892 01c6 00 00 00 00 	.4byte .Lframe0
 893 01ca 00 00 00 00 	.4byte .LFB13
 894 01ce 38 00 00 00 	.4byte .LFE13-.LFB13
 895 01d2 04          	.byte 0x4
 896 01d3 06 00 00 00 	.4byte .LCFI29-.LFB13
 897 01d7 13          	.byte 0x13
 898 01d8 7B          	.sleb128 -5
 899 01d9 04          	.byte 0x4
 900 01da 02 00 00 00 	.4byte .LCFI30-.LCFI29
 901 01de 13          	.byte 0x13
 902 01df 79          	.sleb128 -7
 903 01e0 04          	.byte 0x4
 904 01e1 04 00 00 00 	.4byte .LCFI32-.LCFI30
 905 01e5 86          	.byte 0x86
 906 01e6 05          	.uleb128 0x5
 907 01e7 84          	.byte 0x84
 908 01e8 03          	.uleb128 0x3
 909 01e9 04          	.byte 0x4
 910 01ea 0C 00 00 00 	.4byte .LCFI33-.LCFI32
 911 01ee 13          	.byte 0x13
 912 01ef 76          	.sleb128 -10
 913 01f0 0D          	.byte 0xd
MPLAB XC16 ASSEMBLY Listing:   			page 20


 914 01f1 0E          	.uleb128 0xe
 915 01f2 8E          	.byte 0x8e
 916 01f3 09          	.uleb128 0x9
 917                 	.align 4
 918                 	.LEFDE26:
 919                 	.LSFDE28:
 920 01f4 2E 00 00 00 	.4byte .LEFDE28-.LASFDE28
 921                 	.LASFDE28:
 922 01f8 00 00 00 00 	.4byte .Lframe0
 923 01fc 00 00 00 00 	.4byte .LFB14
 924 0200 38 00 00 00 	.4byte .LFE14-.LFB14
 925 0204 04          	.byte 0x4
 926 0205 06 00 00 00 	.4byte .LCFI35-.LFB14
 927 0209 13          	.byte 0x13
 928 020a 7B          	.sleb128 -5
 929 020b 04          	.byte 0x4
 930 020c 02 00 00 00 	.4byte .LCFI36-.LCFI35
 931 0210 13          	.byte 0x13
 932 0211 79          	.sleb128 -7
 933 0212 04          	.byte 0x4
 934 0213 04 00 00 00 	.4byte .LCFI38-.LCFI36
 935 0217 86          	.byte 0x86
 936 0218 05          	.uleb128 0x5
 937 0219 84          	.byte 0x84
 938 021a 03          	.uleb128 0x3
 939 021b 04          	.byte 0x4
 940 021c 0C 00 00 00 	.4byte .LCFI39-.LCFI38
 941 0220 13          	.byte 0x13
 942 0221 76          	.sleb128 -10
 943 0222 0D          	.byte 0xd
 944 0223 0E          	.uleb128 0xe
 945 0224 8E          	.byte 0x8e
 946 0225 09          	.uleb128 0x9
 947                 	.align 4
 948                 	.LEFDE28:
 949                 	.section .text,code
 950              	.Letext0:
 951              	.file 2 "/opt/microchip/xc16/v1.36/bin/bin/../../support/dsPIC33E/h/p33EP256MU806.h"
 952              	.file 3 "/opt/microchip/xc16/v1.36/bin/bin/../../include/lega-c/stdint.h"
 953              	.section .debug_info,info
 954 0000 08 15 00 00 	.4byte 0x1508
 955 0004 02 00       	.2byte 0x2
 956 0006 00 00 00 00 	.4byte .Ldebug_abbrev0
 957 000a 04          	.byte 0x4
 958 000b 01          	.uleb128 0x1
 959 000c 47 4E 55 20 	.asciz "GNU C 4.5.1 (XC16, Microchip v1.36) (A) Build date: Dec  5 2018"
 959      43 20 34 2E 
 959      35 2E 31 20 
 959      28 58 43 31 
 959      36 2C 20 4D 
 959      69 63 72 6F 
 959      63 68 69 70 
 959      20 76 31 2E 
 959      33 36 29 20 
 960 004c 01          	.byte 0x1
 961 004d 6C 69 62 2F 	.asciz "lib/lib_pic33e/external.c"
 961      6C 69 62 5F 
MPLAB XC16 ASSEMBLY Listing:   			page 21


 961      70 69 63 33 
 961      33 65 2F 65 
 961      78 74 65 72 
 961      6E 61 6C 2E 
 961      63 00 
 962 0067 2F 68 6F 6D 	.asciz "/home/user/Documents/FST/Programming/project_sub-zero"
 962      65 2F 75 73 
 962      65 72 2F 44 
 962      6F 63 75 6D 
 962      65 6E 74 73 
 962      2F 46 53 54 
 962      2F 50 72 6F 
 962      67 72 61 6D 
 962      6D 69 6E 67 
 963 009d 00 00 00 00 	.4byte .Ltext0
 964 00a1 00 00 00 00 	.4byte .Letext0
 965 00a5 00 00 00 00 	.4byte .Ldebug_line0
 966 00a9 02          	.uleb128 0x2
 967 00aa 01          	.byte 0x1
 968 00ab 06          	.byte 0x6
 969 00ac 73 69 67 6E 	.asciz "signed char"
 969      65 64 20 63 
 969      68 61 72 00 
 970 00b8 02          	.uleb128 0x2
 971 00b9 02          	.byte 0x2
 972 00ba 05          	.byte 0x5
 973 00bb 69 6E 74 00 	.asciz "int"
 974 00bf 02          	.uleb128 0x2
 975 00c0 04          	.byte 0x4
 976 00c1 05          	.byte 0x5
 977 00c2 6C 6F 6E 67 	.asciz "long int"
 977      20 69 6E 74 
 977      00 
 978 00cb 02          	.uleb128 0x2
 979 00cc 08          	.byte 0x8
 980 00cd 05          	.byte 0x5
 981 00ce 6C 6F 6E 67 	.asciz "long long int"
 981      20 6C 6F 6E 
 981      67 20 69 6E 
 981      74 00 
 982 00dc 02          	.uleb128 0x2
 983 00dd 01          	.byte 0x1
 984 00de 08          	.byte 0x8
 985 00df 75 6E 73 69 	.asciz "unsigned char"
 985      67 6E 65 64 
 985      20 63 68 61 
 985      72 00 
 986 00ed 03          	.uleb128 0x3
 987 00ee 75 69 6E 74 	.asciz "uint16_t"
 987      31 36 5F 74 
 987      00 
 988 00f7 03          	.byte 0x3
 989 00f8 31          	.byte 0x31
 990 00f9 FD 00 00 00 	.4byte 0xfd
 991 00fd 02          	.uleb128 0x2
 992 00fe 02          	.byte 0x2
 993 00ff 07          	.byte 0x7
MPLAB XC16 ASSEMBLY Listing:   			page 22


 994 0100 75 6E 73 69 	.asciz "unsigned int"
 994      67 6E 65 64 
 994      20 69 6E 74 
 994      00 
 995 010d 02          	.uleb128 0x2
 996 010e 04          	.byte 0x4
 997 010f 07          	.byte 0x7
 998 0110 6C 6F 6E 67 	.asciz "long unsigned int"
 998      20 75 6E 73 
 998      69 67 6E 65 
 998      64 20 69 6E 
 998      74 00 
 999 0122 02          	.uleb128 0x2
 1000 0123 08          	.byte 0x8
 1001 0124 07          	.byte 0x7
 1002 0125 6C 6F 6E 67 	.asciz "long long unsigned int"
 1002      20 6C 6F 6E 
 1002      67 20 75 6E 
 1002      73 69 67 6E 
 1002      65 64 20 69 
 1002      6E 74 00 
 1003 013c 04          	.uleb128 0x4
 1004 013d 74 61 67 49 	.asciz "tagIFS0BITS"
 1004      46 53 30 42 
 1004      49 54 53 00 
 1005 0149 02          	.byte 0x2
 1006 014a 02          	.byte 0x2
 1007 014b 08 25       	.2byte 0x2508
 1008 014d 97 02 00 00 	.4byte 0x297
 1009 0151 05          	.uleb128 0x5
 1010 0152 49 4E 54 30 	.asciz "INT0IF"
 1010      49 46 00 
 1011 0159 02          	.byte 0x2
 1012 015a 09 25       	.2byte 0x2509
 1013 015c ED 00 00 00 	.4byte 0xed
 1014 0160 02          	.byte 0x2
 1015 0161 01          	.byte 0x1
 1016 0162 0F          	.byte 0xf
 1017 0163 02          	.byte 0x2
 1018 0164 23          	.byte 0x23
 1019 0165 00          	.uleb128 0x0
 1020 0166 05          	.uleb128 0x5
 1021 0167 49 43 31 49 	.asciz "IC1IF"
 1021      46 00 
 1022 016d 02          	.byte 0x2
 1023 016e 0A 25       	.2byte 0x250a
 1024 0170 ED 00 00 00 	.4byte 0xed
 1025 0174 02          	.byte 0x2
 1026 0175 01          	.byte 0x1
 1027 0176 0E          	.byte 0xe
 1028 0177 02          	.byte 0x2
 1029 0178 23          	.byte 0x23
 1030 0179 00          	.uleb128 0x0
 1031 017a 05          	.uleb128 0x5
 1032 017b 4F 43 31 49 	.asciz "OC1IF"
 1032      46 00 
 1033 0181 02          	.byte 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 23


 1034 0182 0B 25       	.2byte 0x250b
 1035 0184 ED 00 00 00 	.4byte 0xed
 1036 0188 02          	.byte 0x2
 1037 0189 01          	.byte 0x1
 1038 018a 0D          	.byte 0xd
 1039 018b 02          	.byte 0x2
 1040 018c 23          	.byte 0x23
 1041 018d 00          	.uleb128 0x0
 1042 018e 05          	.uleb128 0x5
 1043 018f 54 31 49 46 	.asciz "T1IF"
 1043      00 
 1044 0194 02          	.byte 0x2
 1045 0195 0C 25       	.2byte 0x250c
 1046 0197 ED 00 00 00 	.4byte 0xed
 1047 019b 02          	.byte 0x2
 1048 019c 01          	.byte 0x1
 1049 019d 0C          	.byte 0xc
 1050 019e 02          	.byte 0x2
 1051 019f 23          	.byte 0x23
 1052 01a0 00          	.uleb128 0x0
 1053 01a1 05          	.uleb128 0x5
 1054 01a2 44 4D 41 30 	.asciz "DMA0IF"
 1054      49 46 00 
 1055 01a9 02          	.byte 0x2
 1056 01aa 0D 25       	.2byte 0x250d
 1057 01ac ED 00 00 00 	.4byte 0xed
 1058 01b0 02          	.byte 0x2
 1059 01b1 01          	.byte 0x1
 1060 01b2 0B          	.byte 0xb
 1061 01b3 02          	.byte 0x2
 1062 01b4 23          	.byte 0x23
 1063 01b5 00          	.uleb128 0x0
 1064 01b6 05          	.uleb128 0x5
 1065 01b7 49 43 32 49 	.asciz "IC2IF"
 1065      46 00 
 1066 01bd 02          	.byte 0x2
 1067 01be 0E 25       	.2byte 0x250e
 1068 01c0 ED 00 00 00 	.4byte 0xed
 1069 01c4 02          	.byte 0x2
 1070 01c5 01          	.byte 0x1
 1071 01c6 0A          	.byte 0xa
 1072 01c7 02          	.byte 0x2
 1073 01c8 23          	.byte 0x23
 1074 01c9 00          	.uleb128 0x0
 1075 01ca 05          	.uleb128 0x5
 1076 01cb 4F 43 32 49 	.asciz "OC2IF"
 1076      46 00 
 1077 01d1 02          	.byte 0x2
 1078 01d2 0F 25       	.2byte 0x250f
 1079 01d4 ED 00 00 00 	.4byte 0xed
 1080 01d8 02          	.byte 0x2
 1081 01d9 01          	.byte 0x1
 1082 01da 09          	.byte 0x9
 1083 01db 02          	.byte 0x2
 1084 01dc 23          	.byte 0x23
 1085 01dd 00          	.uleb128 0x0
 1086 01de 05          	.uleb128 0x5
MPLAB XC16 ASSEMBLY Listing:   			page 24


 1087 01df 54 32 49 46 	.asciz "T2IF"
 1087      00 
 1088 01e4 02          	.byte 0x2
 1089 01e5 10 25       	.2byte 0x2510
 1090 01e7 ED 00 00 00 	.4byte 0xed
 1091 01eb 02          	.byte 0x2
 1092 01ec 01          	.byte 0x1
 1093 01ed 08          	.byte 0x8
 1094 01ee 02          	.byte 0x2
 1095 01ef 23          	.byte 0x23
 1096 01f0 00          	.uleb128 0x0
 1097 01f1 05          	.uleb128 0x5
 1098 01f2 54 33 49 46 	.asciz "T3IF"
 1098      00 
 1099 01f7 02          	.byte 0x2
 1100 01f8 11 25       	.2byte 0x2511
 1101 01fa ED 00 00 00 	.4byte 0xed
 1102 01fe 02          	.byte 0x2
 1103 01ff 01          	.byte 0x1
 1104 0200 07          	.byte 0x7
 1105 0201 02          	.byte 0x2
 1106 0202 23          	.byte 0x23
 1107 0203 00          	.uleb128 0x0
 1108 0204 05          	.uleb128 0x5
 1109 0205 53 50 49 31 	.asciz "SPI1EIF"
 1109      45 49 46 00 
 1110 020d 02          	.byte 0x2
 1111 020e 12 25       	.2byte 0x2512
 1112 0210 ED 00 00 00 	.4byte 0xed
 1113 0214 02          	.byte 0x2
 1114 0215 01          	.byte 0x1
 1115 0216 06          	.byte 0x6
 1116 0217 02          	.byte 0x2
 1117 0218 23          	.byte 0x23
 1118 0219 00          	.uleb128 0x0
 1119 021a 05          	.uleb128 0x5
 1120 021b 53 50 49 31 	.asciz "SPI1IF"
 1120      49 46 00 
 1121 0222 02          	.byte 0x2
 1122 0223 13 25       	.2byte 0x2513
 1123 0225 ED 00 00 00 	.4byte 0xed
 1124 0229 02          	.byte 0x2
 1125 022a 01          	.byte 0x1
 1126 022b 05          	.byte 0x5
 1127 022c 02          	.byte 0x2
 1128 022d 23          	.byte 0x23
 1129 022e 00          	.uleb128 0x0
 1130 022f 05          	.uleb128 0x5
 1131 0230 55 31 52 58 	.asciz "U1RXIF"
 1131      49 46 00 
 1132 0237 02          	.byte 0x2
 1133 0238 14 25       	.2byte 0x2514
 1134 023a ED 00 00 00 	.4byte 0xed
 1135 023e 02          	.byte 0x2
 1136 023f 01          	.byte 0x1
 1137 0240 04          	.byte 0x4
 1138 0241 02          	.byte 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 25


 1139 0242 23          	.byte 0x23
 1140 0243 00          	.uleb128 0x0
 1141 0244 05          	.uleb128 0x5
 1142 0245 55 31 54 58 	.asciz "U1TXIF"
 1142      49 46 00 
 1143 024c 02          	.byte 0x2
 1144 024d 15 25       	.2byte 0x2515
 1145 024f ED 00 00 00 	.4byte 0xed
 1146 0253 02          	.byte 0x2
 1147 0254 01          	.byte 0x1
 1148 0255 03          	.byte 0x3
 1149 0256 02          	.byte 0x2
 1150 0257 23          	.byte 0x23
 1151 0258 00          	.uleb128 0x0
 1152 0259 05          	.uleb128 0x5
 1153 025a 41 44 31 49 	.asciz "AD1IF"
 1153      46 00 
 1154 0260 02          	.byte 0x2
 1155 0261 16 25       	.2byte 0x2516
 1156 0263 ED 00 00 00 	.4byte 0xed
 1157 0267 02          	.byte 0x2
 1158 0268 01          	.byte 0x1
 1159 0269 02          	.byte 0x2
 1160 026a 02          	.byte 0x2
 1161 026b 23          	.byte 0x23
 1162 026c 00          	.uleb128 0x0
 1163 026d 05          	.uleb128 0x5
 1164 026e 44 4D 41 31 	.asciz "DMA1IF"
 1164      49 46 00 
 1165 0275 02          	.byte 0x2
 1166 0276 17 25       	.2byte 0x2517
 1167 0278 ED 00 00 00 	.4byte 0xed
 1168 027c 02          	.byte 0x2
 1169 027d 01          	.byte 0x1
 1170 027e 01          	.byte 0x1
 1171 027f 02          	.byte 0x2
 1172 0280 23          	.byte 0x23
 1173 0281 00          	.uleb128 0x0
 1174 0282 05          	.uleb128 0x5
 1175 0283 4E 56 4D 49 	.asciz "NVMIF"
 1175      46 00 
 1176 0289 02          	.byte 0x2
 1177 028a 18 25       	.2byte 0x2518
 1178 028c ED 00 00 00 	.4byte 0xed
 1179 0290 02          	.byte 0x2
 1180 0291 01          	.byte 0x1
 1181 0292 10          	.byte 0x10
 1182 0293 02          	.byte 0x2
 1183 0294 23          	.byte 0x23
 1184 0295 00          	.uleb128 0x0
 1185 0296 00          	.byte 0x0
 1186 0297 06          	.uleb128 0x6
 1187 0298 49 46 53 30 	.asciz "IFS0BITS"
 1187      42 49 54 53 
 1187      00 
 1188 02a1 02          	.byte 0x2
 1189 02a2 19 25       	.2byte 0x2519
MPLAB XC16 ASSEMBLY Listing:   			page 26


 1190 02a4 3C 01 00 00 	.4byte 0x13c
 1191 02a8 04          	.uleb128 0x4
 1192 02a9 74 61 67 49 	.asciz "tagIFS1BITS"
 1192      46 53 31 42 
 1192      49 54 53 00 
 1193 02b5 02          	.byte 0x2
 1194 02b6 02          	.byte 0x2
 1195 02b7 1E 25       	.2byte 0x251e
 1196 02b9 03 04 00 00 	.4byte 0x403
 1197 02bd 05          	.uleb128 0x5
 1198 02be 53 49 32 43 	.asciz "SI2C1IF"
 1198      31 49 46 00 
 1199 02c6 02          	.byte 0x2
 1200 02c7 1F 25       	.2byte 0x251f
 1201 02c9 ED 00 00 00 	.4byte 0xed
 1202 02cd 02          	.byte 0x2
 1203 02ce 01          	.byte 0x1
 1204 02cf 0F          	.byte 0xf
 1205 02d0 02          	.byte 0x2
 1206 02d1 23          	.byte 0x23
 1207 02d2 00          	.uleb128 0x0
 1208 02d3 05          	.uleb128 0x5
 1209 02d4 4D 49 32 43 	.asciz "MI2C1IF"
 1209      31 49 46 00 
 1210 02dc 02          	.byte 0x2
 1211 02dd 20 25       	.2byte 0x2520
 1212 02df ED 00 00 00 	.4byte 0xed
 1213 02e3 02          	.byte 0x2
 1214 02e4 01          	.byte 0x1
 1215 02e5 0E          	.byte 0xe
 1216 02e6 02          	.byte 0x2
 1217 02e7 23          	.byte 0x23
 1218 02e8 00          	.uleb128 0x0
 1219 02e9 05          	.uleb128 0x5
 1220 02ea 43 4D 49 46 	.asciz "CMIF"
 1220      00 
 1221 02ef 02          	.byte 0x2
 1222 02f0 21 25       	.2byte 0x2521
 1223 02f2 ED 00 00 00 	.4byte 0xed
 1224 02f6 02          	.byte 0x2
 1225 02f7 01          	.byte 0x1
 1226 02f8 0D          	.byte 0xd
 1227 02f9 02          	.byte 0x2
 1228 02fa 23          	.byte 0x23
 1229 02fb 00          	.uleb128 0x0
 1230 02fc 05          	.uleb128 0x5
 1231 02fd 43 4E 49 46 	.asciz "CNIF"
 1231      00 
 1232 0302 02          	.byte 0x2
 1233 0303 22 25       	.2byte 0x2522
 1234 0305 ED 00 00 00 	.4byte 0xed
 1235 0309 02          	.byte 0x2
 1236 030a 01          	.byte 0x1
 1237 030b 0C          	.byte 0xc
 1238 030c 02          	.byte 0x2
 1239 030d 23          	.byte 0x23
 1240 030e 00          	.uleb128 0x0
MPLAB XC16 ASSEMBLY Listing:   			page 27


 1241 030f 05          	.uleb128 0x5
 1242 0310 49 4E 54 31 	.asciz "INT1IF"
 1242      49 46 00 
 1243 0317 02          	.byte 0x2
 1244 0318 23 25       	.2byte 0x2523
 1245 031a ED 00 00 00 	.4byte 0xed
 1246 031e 02          	.byte 0x2
 1247 031f 01          	.byte 0x1
 1248 0320 0B          	.byte 0xb
 1249 0321 02          	.byte 0x2
 1250 0322 23          	.byte 0x23
 1251 0323 00          	.uleb128 0x0
 1252 0324 05          	.uleb128 0x5
 1253 0325 41 44 32 49 	.asciz "AD2IF"
 1253      46 00 
 1254 032b 02          	.byte 0x2
 1255 032c 24 25       	.2byte 0x2524
 1256 032e ED 00 00 00 	.4byte 0xed
 1257 0332 02          	.byte 0x2
 1258 0333 01          	.byte 0x1
 1259 0334 0A          	.byte 0xa
 1260 0335 02          	.byte 0x2
 1261 0336 23          	.byte 0x23
 1262 0337 00          	.uleb128 0x0
 1263 0338 05          	.uleb128 0x5
 1264 0339 49 43 37 49 	.asciz "IC7IF"
 1264      46 00 
 1265 033f 02          	.byte 0x2
 1266 0340 25 25       	.2byte 0x2525
 1267 0342 ED 00 00 00 	.4byte 0xed
 1268 0346 02          	.byte 0x2
 1269 0347 01          	.byte 0x1
 1270 0348 09          	.byte 0x9
 1271 0349 02          	.byte 0x2
 1272 034a 23          	.byte 0x23
 1273 034b 00          	.uleb128 0x0
 1274 034c 05          	.uleb128 0x5
 1275 034d 49 43 38 49 	.asciz "IC8IF"
 1275      46 00 
 1276 0353 02          	.byte 0x2
 1277 0354 26 25       	.2byte 0x2526
 1278 0356 ED 00 00 00 	.4byte 0xed
 1279 035a 02          	.byte 0x2
 1280 035b 01          	.byte 0x1
 1281 035c 08          	.byte 0x8
 1282 035d 02          	.byte 0x2
 1283 035e 23          	.byte 0x23
 1284 035f 00          	.uleb128 0x0
 1285 0360 05          	.uleb128 0x5
 1286 0361 44 4D 41 32 	.asciz "DMA2IF"
 1286      49 46 00 
 1287 0368 02          	.byte 0x2
 1288 0369 27 25       	.2byte 0x2527
 1289 036b ED 00 00 00 	.4byte 0xed
 1290 036f 02          	.byte 0x2
 1291 0370 01          	.byte 0x1
 1292 0371 07          	.byte 0x7
MPLAB XC16 ASSEMBLY Listing:   			page 28


 1293 0372 02          	.byte 0x2
 1294 0373 23          	.byte 0x23
 1295 0374 00          	.uleb128 0x0
 1296 0375 05          	.uleb128 0x5
 1297 0376 4F 43 33 49 	.asciz "OC3IF"
 1297      46 00 
 1298 037c 02          	.byte 0x2
 1299 037d 28 25       	.2byte 0x2528
 1300 037f ED 00 00 00 	.4byte 0xed
 1301 0383 02          	.byte 0x2
 1302 0384 01          	.byte 0x1
 1303 0385 06          	.byte 0x6
 1304 0386 02          	.byte 0x2
 1305 0387 23          	.byte 0x23
 1306 0388 00          	.uleb128 0x0
 1307 0389 05          	.uleb128 0x5
 1308 038a 4F 43 34 49 	.asciz "OC4IF"
 1308      46 00 
 1309 0390 02          	.byte 0x2
 1310 0391 29 25       	.2byte 0x2529
 1311 0393 ED 00 00 00 	.4byte 0xed
 1312 0397 02          	.byte 0x2
 1313 0398 01          	.byte 0x1
 1314 0399 05          	.byte 0x5
 1315 039a 02          	.byte 0x2
 1316 039b 23          	.byte 0x23
 1317 039c 00          	.uleb128 0x0
 1318 039d 05          	.uleb128 0x5
 1319 039e 54 34 49 46 	.asciz "T4IF"
 1319      00 
 1320 03a3 02          	.byte 0x2
 1321 03a4 2A 25       	.2byte 0x252a
 1322 03a6 ED 00 00 00 	.4byte 0xed
 1323 03aa 02          	.byte 0x2
 1324 03ab 01          	.byte 0x1
 1325 03ac 04          	.byte 0x4
 1326 03ad 02          	.byte 0x2
 1327 03ae 23          	.byte 0x23
 1328 03af 00          	.uleb128 0x0
 1329 03b0 05          	.uleb128 0x5
 1330 03b1 54 35 49 46 	.asciz "T5IF"
 1330      00 
 1331 03b6 02          	.byte 0x2
 1332 03b7 2B 25       	.2byte 0x252b
 1333 03b9 ED 00 00 00 	.4byte 0xed
 1334 03bd 02          	.byte 0x2
 1335 03be 01          	.byte 0x1
 1336 03bf 03          	.byte 0x3
 1337 03c0 02          	.byte 0x2
 1338 03c1 23          	.byte 0x23
 1339 03c2 00          	.uleb128 0x0
 1340 03c3 05          	.uleb128 0x5
 1341 03c4 49 4E 54 32 	.asciz "INT2IF"
 1341      49 46 00 
 1342 03cb 02          	.byte 0x2
 1343 03cc 2C 25       	.2byte 0x252c
 1344 03ce ED 00 00 00 	.4byte 0xed
MPLAB XC16 ASSEMBLY Listing:   			page 29


 1345 03d2 02          	.byte 0x2
 1346 03d3 01          	.byte 0x1
 1347 03d4 02          	.byte 0x2
 1348 03d5 02          	.byte 0x2
 1349 03d6 23          	.byte 0x23
 1350 03d7 00          	.uleb128 0x0
 1351 03d8 05          	.uleb128 0x5
 1352 03d9 55 32 52 58 	.asciz "U2RXIF"
 1352      49 46 00 
 1353 03e0 02          	.byte 0x2
 1354 03e1 2D 25       	.2byte 0x252d
 1355 03e3 ED 00 00 00 	.4byte 0xed
 1356 03e7 02          	.byte 0x2
 1357 03e8 01          	.byte 0x1
 1358 03e9 01          	.byte 0x1
 1359 03ea 02          	.byte 0x2
 1360 03eb 23          	.byte 0x23
 1361 03ec 00          	.uleb128 0x0
 1362 03ed 05          	.uleb128 0x5
 1363 03ee 55 32 54 58 	.asciz "U2TXIF"
 1363      49 46 00 
 1364 03f5 02          	.byte 0x2
 1365 03f6 2E 25       	.2byte 0x252e
 1366 03f8 ED 00 00 00 	.4byte 0xed
 1367 03fc 02          	.byte 0x2
 1368 03fd 01          	.byte 0x1
 1369 03fe 10          	.byte 0x10
 1370 03ff 02          	.byte 0x2
 1371 0400 23          	.byte 0x23
 1372 0401 00          	.uleb128 0x0
 1373 0402 00          	.byte 0x0
 1374 0403 06          	.uleb128 0x6
 1375 0404 49 46 53 31 	.asciz "IFS1BITS"
 1375      42 49 54 53 
 1375      00 
 1376 040d 02          	.byte 0x2
 1377 040e 2F 25       	.2byte 0x252f
 1378 0410 A8 02 00 00 	.4byte 0x2a8
 1379 0414 04          	.uleb128 0x4
 1380 0415 74 61 67 49 	.asciz "tagIFS3BITS"
 1380      46 53 33 42 
 1380      49 54 53 00 
 1381 0421 02          	.byte 0x2
 1382 0422 02          	.byte 0x2
 1383 0423 4A 25       	.2byte 0x254a
 1384 0425 5D 05 00 00 	.4byte 0x55d
 1385 0429 05          	.uleb128 0x5
 1386 042a 54 37 49 46 	.asciz "T7IF"
 1386      00 
 1387 042f 02          	.byte 0x2
 1388 0430 4B 25       	.2byte 0x254b
 1389 0432 ED 00 00 00 	.4byte 0xed
 1390 0436 02          	.byte 0x2
 1391 0437 01          	.byte 0x1
 1392 0438 0F          	.byte 0xf
 1393 0439 02          	.byte 0x2
 1394 043a 23          	.byte 0x23
MPLAB XC16 ASSEMBLY Listing:   			page 30


 1395 043b 00          	.uleb128 0x0
 1396 043c 05          	.uleb128 0x5
 1397 043d 53 49 32 43 	.asciz "SI2C2IF"
 1397      32 49 46 00 
 1398 0445 02          	.byte 0x2
 1399 0446 4C 25       	.2byte 0x254c
 1400 0448 ED 00 00 00 	.4byte 0xed
 1401 044c 02          	.byte 0x2
 1402 044d 01          	.byte 0x1
 1403 044e 0E          	.byte 0xe
 1404 044f 02          	.byte 0x2
 1405 0450 23          	.byte 0x23
 1406 0451 00          	.uleb128 0x0
 1407 0452 05          	.uleb128 0x5
 1408 0453 4D 49 32 43 	.asciz "MI2C2IF"
 1408      32 49 46 00 
 1409 045b 02          	.byte 0x2
 1410 045c 4D 25       	.2byte 0x254d
 1411 045e ED 00 00 00 	.4byte 0xed
 1412 0462 02          	.byte 0x2
 1413 0463 01          	.byte 0x1
 1414 0464 0D          	.byte 0xd
 1415 0465 02          	.byte 0x2
 1416 0466 23          	.byte 0x23
 1417 0467 00          	.uleb128 0x0
 1418 0468 05          	.uleb128 0x5
 1419 0469 54 38 49 46 	.asciz "T8IF"
 1419      00 
 1420 046e 02          	.byte 0x2
 1421 046f 4E 25       	.2byte 0x254e
 1422 0471 ED 00 00 00 	.4byte 0xed
 1423 0475 02          	.byte 0x2
 1424 0476 01          	.byte 0x1
 1425 0477 0C          	.byte 0xc
 1426 0478 02          	.byte 0x2
 1427 0479 23          	.byte 0x23
 1428 047a 00          	.uleb128 0x0
 1429 047b 05          	.uleb128 0x5
 1430 047c 54 39 49 46 	.asciz "T9IF"
 1430      00 
 1431 0481 02          	.byte 0x2
 1432 0482 4F 25       	.2byte 0x254f
 1433 0484 ED 00 00 00 	.4byte 0xed
 1434 0488 02          	.byte 0x2
 1435 0489 01          	.byte 0x1
 1436 048a 0B          	.byte 0xb
 1437 048b 02          	.byte 0x2
 1438 048c 23          	.byte 0x23
 1439 048d 00          	.uleb128 0x0
 1440 048e 05          	.uleb128 0x5
 1441 048f 49 4E 54 33 	.asciz "INT3IF"
 1441      49 46 00 
 1442 0496 02          	.byte 0x2
 1443 0497 50 25       	.2byte 0x2550
 1444 0499 ED 00 00 00 	.4byte 0xed
 1445 049d 02          	.byte 0x2
 1446 049e 01          	.byte 0x1
MPLAB XC16 ASSEMBLY Listing:   			page 31


 1447 049f 0A          	.byte 0xa
 1448 04a0 02          	.byte 0x2
 1449 04a1 23          	.byte 0x23
 1450 04a2 00          	.uleb128 0x0
 1451 04a3 05          	.uleb128 0x5
 1452 04a4 49 4E 54 34 	.asciz "INT4IF"
 1452      49 46 00 
 1453 04ab 02          	.byte 0x2
 1454 04ac 51 25       	.2byte 0x2551
 1455 04ae ED 00 00 00 	.4byte 0xed
 1456 04b2 02          	.byte 0x2
 1457 04b3 01          	.byte 0x1
 1458 04b4 09          	.byte 0x9
 1459 04b5 02          	.byte 0x2
 1460 04b6 23          	.byte 0x23
 1461 04b7 00          	.uleb128 0x0
 1462 04b8 05          	.uleb128 0x5
 1463 04b9 43 32 52 58 	.asciz "C2RXIF"
 1463      49 46 00 
 1464 04c0 02          	.byte 0x2
 1465 04c1 52 25       	.2byte 0x2552
 1466 04c3 ED 00 00 00 	.4byte 0xed
 1467 04c7 02          	.byte 0x2
 1468 04c8 01          	.byte 0x1
 1469 04c9 08          	.byte 0x8
 1470 04ca 02          	.byte 0x2
 1471 04cb 23          	.byte 0x23
 1472 04cc 00          	.uleb128 0x0
 1473 04cd 05          	.uleb128 0x5
 1474 04ce 43 32 49 46 	.asciz "C2IF"
 1474      00 
 1475 04d3 02          	.byte 0x2
 1476 04d4 53 25       	.2byte 0x2553
 1477 04d6 ED 00 00 00 	.4byte 0xed
 1478 04da 02          	.byte 0x2
 1479 04db 01          	.byte 0x1
 1480 04dc 07          	.byte 0x7
 1481 04dd 02          	.byte 0x2
 1482 04de 23          	.byte 0x23
 1483 04df 00          	.uleb128 0x0
 1484 04e0 05          	.uleb128 0x5
 1485 04e1 50 53 45 4D 	.asciz "PSEMIF"
 1485      49 46 00 
 1486 04e8 02          	.byte 0x2
 1487 04e9 54 25       	.2byte 0x2554
 1488 04eb ED 00 00 00 	.4byte 0xed
 1489 04ef 02          	.byte 0x2
 1490 04f0 01          	.byte 0x1
 1491 04f1 06          	.byte 0x6
 1492 04f2 02          	.byte 0x2
 1493 04f3 23          	.byte 0x23
 1494 04f4 00          	.uleb128 0x0
 1495 04f5 05          	.uleb128 0x5
 1496 04f6 51 45 49 31 	.asciz "QEI1IF"
 1496      49 46 00 
 1497 04fd 02          	.byte 0x2
 1498 04fe 55 25       	.2byte 0x2555
MPLAB XC16 ASSEMBLY Listing:   			page 32


 1499 0500 ED 00 00 00 	.4byte 0xed
 1500 0504 02          	.byte 0x2
 1501 0505 01          	.byte 0x1
 1502 0506 05          	.byte 0x5
 1503 0507 02          	.byte 0x2
 1504 0508 23          	.byte 0x23
 1505 0509 00          	.uleb128 0x0
 1506 050a 05          	.uleb128 0x5
 1507 050b 44 43 49 45 	.asciz "DCIEIF"
 1507      49 46 00 
 1508 0512 02          	.byte 0x2
 1509 0513 56 25       	.2byte 0x2556
 1510 0515 ED 00 00 00 	.4byte 0xed
 1511 0519 02          	.byte 0x2
 1512 051a 01          	.byte 0x1
 1513 051b 04          	.byte 0x4
 1514 051c 02          	.byte 0x2
 1515 051d 23          	.byte 0x23
 1516 051e 00          	.uleb128 0x0
 1517 051f 05          	.uleb128 0x5
 1518 0520 44 43 49 49 	.asciz "DCIIF"
 1518      46 00 
 1519 0526 02          	.byte 0x2
 1520 0527 57 25       	.2byte 0x2557
 1521 0529 ED 00 00 00 	.4byte 0xed
 1522 052d 02          	.byte 0x2
 1523 052e 01          	.byte 0x1
 1524 052f 03          	.byte 0x3
 1525 0530 02          	.byte 0x2
 1526 0531 23          	.byte 0x23
 1527 0532 00          	.uleb128 0x0
 1528 0533 05          	.uleb128 0x5
 1529 0534 44 4D 41 35 	.asciz "DMA5IF"
 1529      49 46 00 
 1530 053b 02          	.byte 0x2
 1531 053c 58 25       	.2byte 0x2558
 1532 053e ED 00 00 00 	.4byte 0xed
 1533 0542 02          	.byte 0x2
 1534 0543 01          	.byte 0x1
 1535 0544 02          	.byte 0x2
 1536 0545 02          	.byte 0x2
 1537 0546 23          	.byte 0x23
 1538 0547 00          	.uleb128 0x0
 1539 0548 05          	.uleb128 0x5
 1540 0549 52 54 43 49 	.asciz "RTCIF"
 1540      46 00 
 1541 054f 02          	.byte 0x2
 1542 0550 59 25       	.2byte 0x2559
 1543 0552 ED 00 00 00 	.4byte 0xed
 1544 0556 02          	.byte 0x2
 1545 0557 01          	.byte 0x1
 1546 0558 01          	.byte 0x1
 1547 0559 02          	.byte 0x2
 1548 055a 23          	.byte 0x23
 1549 055b 00          	.uleb128 0x0
 1550 055c 00          	.byte 0x0
 1551 055d 06          	.uleb128 0x6
MPLAB XC16 ASSEMBLY Listing:   			page 33


 1552 055e 49 46 53 33 	.asciz "IFS3BITS"
 1552      42 49 54 53 
 1552      00 
 1553 0567 02          	.byte 0x2
 1554 0568 5A 25       	.2byte 0x255a
 1555 056a 14 04 00 00 	.4byte 0x414
 1556 056e 04          	.uleb128 0x4
 1557 056f 74 61 67 49 	.asciz "tagIEC0BITS"
 1557      45 43 30 42 
 1557      49 54 53 00 
 1558 057b 02          	.byte 0x2
 1559 057c 02          	.byte 0x2
 1560 057d B4 25       	.2byte 0x25b4
 1561 057f C9 06 00 00 	.4byte 0x6c9
 1562 0583 05          	.uleb128 0x5
 1563 0584 49 4E 54 30 	.asciz "INT0IE"
 1563      49 45 00 
 1564 058b 02          	.byte 0x2
 1565 058c B5 25       	.2byte 0x25b5
 1566 058e ED 00 00 00 	.4byte 0xed
 1567 0592 02          	.byte 0x2
 1568 0593 01          	.byte 0x1
 1569 0594 0F          	.byte 0xf
 1570 0595 02          	.byte 0x2
 1571 0596 23          	.byte 0x23
 1572 0597 00          	.uleb128 0x0
 1573 0598 05          	.uleb128 0x5
 1574 0599 49 43 31 49 	.asciz "IC1IE"
 1574      45 00 
 1575 059f 02          	.byte 0x2
 1576 05a0 B6 25       	.2byte 0x25b6
 1577 05a2 ED 00 00 00 	.4byte 0xed
 1578 05a6 02          	.byte 0x2
 1579 05a7 01          	.byte 0x1
 1580 05a8 0E          	.byte 0xe
 1581 05a9 02          	.byte 0x2
 1582 05aa 23          	.byte 0x23
 1583 05ab 00          	.uleb128 0x0
 1584 05ac 05          	.uleb128 0x5
 1585 05ad 4F 43 31 49 	.asciz "OC1IE"
 1585      45 00 
 1586 05b3 02          	.byte 0x2
 1587 05b4 B7 25       	.2byte 0x25b7
 1588 05b6 ED 00 00 00 	.4byte 0xed
 1589 05ba 02          	.byte 0x2
 1590 05bb 01          	.byte 0x1
 1591 05bc 0D          	.byte 0xd
 1592 05bd 02          	.byte 0x2
 1593 05be 23          	.byte 0x23
 1594 05bf 00          	.uleb128 0x0
 1595 05c0 05          	.uleb128 0x5
 1596 05c1 54 31 49 45 	.asciz "T1IE"
 1596      00 
 1597 05c6 02          	.byte 0x2
 1598 05c7 B8 25       	.2byte 0x25b8
 1599 05c9 ED 00 00 00 	.4byte 0xed
 1600 05cd 02          	.byte 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 34


 1601 05ce 01          	.byte 0x1
 1602 05cf 0C          	.byte 0xc
 1603 05d0 02          	.byte 0x2
 1604 05d1 23          	.byte 0x23
 1605 05d2 00          	.uleb128 0x0
 1606 05d3 05          	.uleb128 0x5
 1607 05d4 44 4D 41 30 	.asciz "DMA0IE"
 1607      49 45 00 
 1608 05db 02          	.byte 0x2
 1609 05dc B9 25       	.2byte 0x25b9
 1610 05de ED 00 00 00 	.4byte 0xed
 1611 05e2 02          	.byte 0x2
 1612 05e3 01          	.byte 0x1
 1613 05e4 0B          	.byte 0xb
 1614 05e5 02          	.byte 0x2
 1615 05e6 23          	.byte 0x23
 1616 05e7 00          	.uleb128 0x0
 1617 05e8 05          	.uleb128 0x5
 1618 05e9 49 43 32 49 	.asciz "IC2IE"
 1618      45 00 
 1619 05ef 02          	.byte 0x2
 1620 05f0 BA 25       	.2byte 0x25ba
 1621 05f2 ED 00 00 00 	.4byte 0xed
 1622 05f6 02          	.byte 0x2
 1623 05f7 01          	.byte 0x1
 1624 05f8 0A          	.byte 0xa
 1625 05f9 02          	.byte 0x2
 1626 05fa 23          	.byte 0x23
 1627 05fb 00          	.uleb128 0x0
 1628 05fc 05          	.uleb128 0x5
 1629 05fd 4F 43 32 49 	.asciz "OC2IE"
 1629      45 00 
 1630 0603 02          	.byte 0x2
 1631 0604 BB 25       	.2byte 0x25bb
 1632 0606 ED 00 00 00 	.4byte 0xed
 1633 060a 02          	.byte 0x2
 1634 060b 01          	.byte 0x1
 1635 060c 09          	.byte 0x9
 1636 060d 02          	.byte 0x2
 1637 060e 23          	.byte 0x23
 1638 060f 00          	.uleb128 0x0
 1639 0610 05          	.uleb128 0x5
 1640 0611 54 32 49 45 	.asciz "T2IE"
 1640      00 
 1641 0616 02          	.byte 0x2
 1642 0617 BC 25       	.2byte 0x25bc
 1643 0619 ED 00 00 00 	.4byte 0xed
 1644 061d 02          	.byte 0x2
 1645 061e 01          	.byte 0x1
 1646 061f 08          	.byte 0x8
 1647 0620 02          	.byte 0x2
 1648 0621 23          	.byte 0x23
 1649 0622 00          	.uleb128 0x0
 1650 0623 05          	.uleb128 0x5
 1651 0624 54 33 49 45 	.asciz "T3IE"
 1651      00 
 1652 0629 02          	.byte 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 35


 1653 062a BD 25       	.2byte 0x25bd
 1654 062c ED 00 00 00 	.4byte 0xed
 1655 0630 02          	.byte 0x2
 1656 0631 01          	.byte 0x1
 1657 0632 07          	.byte 0x7
 1658 0633 02          	.byte 0x2
 1659 0634 23          	.byte 0x23
 1660 0635 00          	.uleb128 0x0
 1661 0636 05          	.uleb128 0x5
 1662 0637 53 50 49 31 	.asciz "SPI1EIE"
 1662      45 49 45 00 
 1663 063f 02          	.byte 0x2
 1664 0640 BE 25       	.2byte 0x25be
 1665 0642 ED 00 00 00 	.4byte 0xed
 1666 0646 02          	.byte 0x2
 1667 0647 01          	.byte 0x1
 1668 0648 06          	.byte 0x6
 1669 0649 02          	.byte 0x2
 1670 064a 23          	.byte 0x23
 1671 064b 00          	.uleb128 0x0
 1672 064c 05          	.uleb128 0x5
 1673 064d 53 50 49 31 	.asciz "SPI1IE"
 1673      49 45 00 
 1674 0654 02          	.byte 0x2
 1675 0655 BF 25       	.2byte 0x25bf
 1676 0657 ED 00 00 00 	.4byte 0xed
 1677 065b 02          	.byte 0x2
 1678 065c 01          	.byte 0x1
 1679 065d 05          	.byte 0x5
 1680 065e 02          	.byte 0x2
 1681 065f 23          	.byte 0x23
 1682 0660 00          	.uleb128 0x0
 1683 0661 05          	.uleb128 0x5
 1684 0662 55 31 52 58 	.asciz "U1RXIE"
 1684      49 45 00 
 1685 0669 02          	.byte 0x2
 1686 066a C0 25       	.2byte 0x25c0
 1687 066c ED 00 00 00 	.4byte 0xed
 1688 0670 02          	.byte 0x2
 1689 0671 01          	.byte 0x1
 1690 0672 04          	.byte 0x4
 1691 0673 02          	.byte 0x2
 1692 0674 23          	.byte 0x23
 1693 0675 00          	.uleb128 0x0
 1694 0676 05          	.uleb128 0x5
 1695 0677 55 31 54 58 	.asciz "U1TXIE"
 1695      49 45 00 
 1696 067e 02          	.byte 0x2
 1697 067f C1 25       	.2byte 0x25c1
 1698 0681 ED 00 00 00 	.4byte 0xed
 1699 0685 02          	.byte 0x2
 1700 0686 01          	.byte 0x1
 1701 0687 03          	.byte 0x3
 1702 0688 02          	.byte 0x2
 1703 0689 23          	.byte 0x23
 1704 068a 00          	.uleb128 0x0
 1705 068b 05          	.uleb128 0x5
MPLAB XC16 ASSEMBLY Listing:   			page 36


 1706 068c 41 44 31 49 	.asciz "AD1IE"
 1706      45 00 
 1707 0692 02          	.byte 0x2
 1708 0693 C2 25       	.2byte 0x25c2
 1709 0695 ED 00 00 00 	.4byte 0xed
 1710 0699 02          	.byte 0x2
 1711 069a 01          	.byte 0x1
 1712 069b 02          	.byte 0x2
 1713 069c 02          	.byte 0x2
 1714 069d 23          	.byte 0x23
 1715 069e 00          	.uleb128 0x0
 1716 069f 05          	.uleb128 0x5
 1717 06a0 44 4D 41 31 	.asciz "DMA1IE"
 1717      49 45 00 
 1718 06a7 02          	.byte 0x2
 1719 06a8 C3 25       	.2byte 0x25c3
 1720 06aa ED 00 00 00 	.4byte 0xed
 1721 06ae 02          	.byte 0x2
 1722 06af 01          	.byte 0x1
 1723 06b0 01          	.byte 0x1
 1724 06b1 02          	.byte 0x2
 1725 06b2 23          	.byte 0x23
 1726 06b3 00          	.uleb128 0x0
 1727 06b4 05          	.uleb128 0x5
 1728 06b5 4E 56 4D 49 	.asciz "NVMIE"
 1728      45 00 
 1729 06bb 02          	.byte 0x2
 1730 06bc C4 25       	.2byte 0x25c4
 1731 06be ED 00 00 00 	.4byte 0xed
 1732 06c2 02          	.byte 0x2
 1733 06c3 01          	.byte 0x1
 1734 06c4 10          	.byte 0x10
 1735 06c5 02          	.byte 0x2
 1736 06c6 23          	.byte 0x23
 1737 06c7 00          	.uleb128 0x0
 1738 06c8 00          	.byte 0x0
 1739 06c9 06          	.uleb128 0x6
 1740 06ca 49 45 43 30 	.asciz "IEC0BITS"
 1740      42 49 54 53 
 1740      00 
 1741 06d3 02          	.byte 0x2
 1742 06d4 C5 25       	.2byte 0x25c5
 1743 06d6 6E 05 00 00 	.4byte 0x56e
 1744 06da 04          	.uleb128 0x4
 1745 06db 74 61 67 49 	.asciz "tagIEC1BITS"
 1745      45 43 31 42 
 1745      49 54 53 00 
 1746 06e7 02          	.byte 0x2
 1747 06e8 02          	.byte 0x2
 1748 06e9 CA 25       	.2byte 0x25ca
 1749 06eb 35 08 00 00 	.4byte 0x835
 1750 06ef 05          	.uleb128 0x5
 1751 06f0 53 49 32 43 	.asciz "SI2C1IE"
 1751      31 49 45 00 
 1752 06f8 02          	.byte 0x2
 1753 06f9 CB 25       	.2byte 0x25cb
 1754 06fb ED 00 00 00 	.4byte 0xed
MPLAB XC16 ASSEMBLY Listing:   			page 37


 1755 06ff 02          	.byte 0x2
 1756 0700 01          	.byte 0x1
 1757 0701 0F          	.byte 0xf
 1758 0702 02          	.byte 0x2
 1759 0703 23          	.byte 0x23
 1760 0704 00          	.uleb128 0x0
 1761 0705 05          	.uleb128 0x5
 1762 0706 4D 49 32 43 	.asciz "MI2C1IE"
 1762      31 49 45 00 
 1763 070e 02          	.byte 0x2
 1764 070f CC 25       	.2byte 0x25cc
 1765 0711 ED 00 00 00 	.4byte 0xed
 1766 0715 02          	.byte 0x2
 1767 0716 01          	.byte 0x1
 1768 0717 0E          	.byte 0xe
 1769 0718 02          	.byte 0x2
 1770 0719 23          	.byte 0x23
 1771 071a 00          	.uleb128 0x0
 1772 071b 05          	.uleb128 0x5
 1773 071c 43 4D 49 45 	.asciz "CMIE"
 1773      00 
 1774 0721 02          	.byte 0x2
 1775 0722 CD 25       	.2byte 0x25cd
 1776 0724 ED 00 00 00 	.4byte 0xed
 1777 0728 02          	.byte 0x2
 1778 0729 01          	.byte 0x1
 1779 072a 0D          	.byte 0xd
 1780 072b 02          	.byte 0x2
 1781 072c 23          	.byte 0x23
 1782 072d 00          	.uleb128 0x0
 1783 072e 05          	.uleb128 0x5
 1784 072f 43 4E 49 45 	.asciz "CNIE"
 1784      00 
 1785 0734 02          	.byte 0x2
 1786 0735 CE 25       	.2byte 0x25ce
 1787 0737 ED 00 00 00 	.4byte 0xed
 1788 073b 02          	.byte 0x2
 1789 073c 01          	.byte 0x1
 1790 073d 0C          	.byte 0xc
 1791 073e 02          	.byte 0x2
 1792 073f 23          	.byte 0x23
 1793 0740 00          	.uleb128 0x0
 1794 0741 05          	.uleb128 0x5
 1795 0742 49 4E 54 31 	.asciz "INT1IE"
 1795      49 45 00 
 1796 0749 02          	.byte 0x2
 1797 074a CF 25       	.2byte 0x25cf
 1798 074c ED 00 00 00 	.4byte 0xed
 1799 0750 02          	.byte 0x2
 1800 0751 01          	.byte 0x1
 1801 0752 0B          	.byte 0xb
 1802 0753 02          	.byte 0x2
 1803 0754 23          	.byte 0x23
 1804 0755 00          	.uleb128 0x0
 1805 0756 05          	.uleb128 0x5
 1806 0757 41 44 32 49 	.asciz "AD2IE"
 1806      45 00 
MPLAB XC16 ASSEMBLY Listing:   			page 38


 1807 075d 02          	.byte 0x2
 1808 075e D0 25       	.2byte 0x25d0
 1809 0760 ED 00 00 00 	.4byte 0xed
 1810 0764 02          	.byte 0x2
 1811 0765 01          	.byte 0x1
 1812 0766 0A          	.byte 0xa
 1813 0767 02          	.byte 0x2
 1814 0768 23          	.byte 0x23
 1815 0769 00          	.uleb128 0x0
 1816 076a 05          	.uleb128 0x5
 1817 076b 49 43 37 49 	.asciz "IC7IE"
 1817      45 00 
 1818 0771 02          	.byte 0x2
 1819 0772 D1 25       	.2byte 0x25d1
 1820 0774 ED 00 00 00 	.4byte 0xed
 1821 0778 02          	.byte 0x2
 1822 0779 01          	.byte 0x1
 1823 077a 09          	.byte 0x9
 1824 077b 02          	.byte 0x2
 1825 077c 23          	.byte 0x23
 1826 077d 00          	.uleb128 0x0
 1827 077e 05          	.uleb128 0x5
 1828 077f 49 43 38 49 	.asciz "IC8IE"
 1828      45 00 
 1829 0785 02          	.byte 0x2
 1830 0786 D2 25       	.2byte 0x25d2
 1831 0788 ED 00 00 00 	.4byte 0xed
 1832 078c 02          	.byte 0x2
 1833 078d 01          	.byte 0x1
 1834 078e 08          	.byte 0x8
 1835 078f 02          	.byte 0x2
 1836 0790 23          	.byte 0x23
 1837 0791 00          	.uleb128 0x0
 1838 0792 05          	.uleb128 0x5
 1839 0793 44 4D 41 32 	.asciz "DMA2IE"
 1839      49 45 00 
 1840 079a 02          	.byte 0x2
 1841 079b D3 25       	.2byte 0x25d3
 1842 079d ED 00 00 00 	.4byte 0xed
 1843 07a1 02          	.byte 0x2
 1844 07a2 01          	.byte 0x1
 1845 07a3 07          	.byte 0x7
 1846 07a4 02          	.byte 0x2
 1847 07a5 23          	.byte 0x23
 1848 07a6 00          	.uleb128 0x0
 1849 07a7 05          	.uleb128 0x5
 1850 07a8 4F 43 33 49 	.asciz "OC3IE"
 1850      45 00 
 1851 07ae 02          	.byte 0x2
 1852 07af D4 25       	.2byte 0x25d4
 1853 07b1 ED 00 00 00 	.4byte 0xed
 1854 07b5 02          	.byte 0x2
 1855 07b6 01          	.byte 0x1
 1856 07b7 06          	.byte 0x6
 1857 07b8 02          	.byte 0x2
 1858 07b9 23          	.byte 0x23
 1859 07ba 00          	.uleb128 0x0
MPLAB XC16 ASSEMBLY Listing:   			page 39


 1860 07bb 05          	.uleb128 0x5
 1861 07bc 4F 43 34 49 	.asciz "OC4IE"
 1861      45 00 
 1862 07c2 02          	.byte 0x2
 1863 07c3 D5 25       	.2byte 0x25d5
 1864 07c5 ED 00 00 00 	.4byte 0xed
 1865 07c9 02          	.byte 0x2
 1866 07ca 01          	.byte 0x1
 1867 07cb 05          	.byte 0x5
 1868 07cc 02          	.byte 0x2
 1869 07cd 23          	.byte 0x23
 1870 07ce 00          	.uleb128 0x0
 1871 07cf 05          	.uleb128 0x5
 1872 07d0 54 34 49 45 	.asciz "T4IE"
 1872      00 
 1873 07d5 02          	.byte 0x2
 1874 07d6 D6 25       	.2byte 0x25d6
 1875 07d8 ED 00 00 00 	.4byte 0xed
 1876 07dc 02          	.byte 0x2
 1877 07dd 01          	.byte 0x1
 1878 07de 04          	.byte 0x4
 1879 07df 02          	.byte 0x2
 1880 07e0 23          	.byte 0x23
 1881 07e1 00          	.uleb128 0x0
 1882 07e2 05          	.uleb128 0x5
 1883 07e3 54 35 49 45 	.asciz "T5IE"
 1883      00 
 1884 07e8 02          	.byte 0x2
 1885 07e9 D7 25       	.2byte 0x25d7
 1886 07eb ED 00 00 00 	.4byte 0xed
 1887 07ef 02          	.byte 0x2
 1888 07f0 01          	.byte 0x1
 1889 07f1 03          	.byte 0x3
 1890 07f2 02          	.byte 0x2
 1891 07f3 23          	.byte 0x23
 1892 07f4 00          	.uleb128 0x0
 1893 07f5 05          	.uleb128 0x5
 1894 07f6 49 4E 54 32 	.asciz "INT2IE"
 1894      49 45 00 
 1895 07fd 02          	.byte 0x2
 1896 07fe D8 25       	.2byte 0x25d8
 1897 0800 ED 00 00 00 	.4byte 0xed
 1898 0804 02          	.byte 0x2
 1899 0805 01          	.byte 0x1
 1900 0806 02          	.byte 0x2
 1901 0807 02          	.byte 0x2
 1902 0808 23          	.byte 0x23
 1903 0809 00          	.uleb128 0x0
 1904 080a 05          	.uleb128 0x5
 1905 080b 55 32 52 58 	.asciz "U2RXIE"
 1905      49 45 00 
 1906 0812 02          	.byte 0x2
 1907 0813 D9 25       	.2byte 0x25d9
 1908 0815 ED 00 00 00 	.4byte 0xed
 1909 0819 02          	.byte 0x2
 1910 081a 01          	.byte 0x1
 1911 081b 01          	.byte 0x1
MPLAB XC16 ASSEMBLY Listing:   			page 40


 1912 081c 02          	.byte 0x2
 1913 081d 23          	.byte 0x23
 1914 081e 00          	.uleb128 0x0
 1915 081f 05          	.uleb128 0x5
 1916 0820 55 32 54 58 	.asciz "U2TXIE"
 1916      49 45 00 
 1917 0827 02          	.byte 0x2
 1918 0828 DA 25       	.2byte 0x25da
 1919 082a ED 00 00 00 	.4byte 0xed
 1920 082e 02          	.byte 0x2
 1921 082f 01          	.byte 0x1
 1922 0830 10          	.byte 0x10
 1923 0831 02          	.byte 0x2
 1924 0832 23          	.byte 0x23
 1925 0833 00          	.uleb128 0x0
 1926 0834 00          	.byte 0x0
 1927 0835 06          	.uleb128 0x6
 1928 0836 49 45 43 31 	.asciz "IEC1BITS"
 1928      42 49 54 53 
 1928      00 
 1929 083f 02          	.byte 0x2
 1930 0840 DB 25       	.2byte 0x25db
 1931 0842 DA 06 00 00 	.4byte 0x6da
 1932 0846 04          	.uleb128 0x4
 1933 0847 74 61 67 49 	.asciz "tagIEC3BITS"
 1933      45 43 33 42 
 1933      49 54 53 00 
 1934 0853 02          	.byte 0x2
 1935 0854 02          	.byte 0x2
 1936 0855 F6 25       	.2byte 0x25f6
 1937 0857 8F 09 00 00 	.4byte 0x98f
 1938 085b 05          	.uleb128 0x5
 1939 085c 54 37 49 45 	.asciz "T7IE"
 1939      00 
 1940 0861 02          	.byte 0x2
 1941 0862 F7 25       	.2byte 0x25f7
 1942 0864 ED 00 00 00 	.4byte 0xed
 1943 0868 02          	.byte 0x2
 1944 0869 01          	.byte 0x1
 1945 086a 0F          	.byte 0xf
 1946 086b 02          	.byte 0x2
 1947 086c 23          	.byte 0x23
 1948 086d 00          	.uleb128 0x0
 1949 086e 05          	.uleb128 0x5
 1950 086f 53 49 32 43 	.asciz "SI2C2IE"
 1950      32 49 45 00 
 1951 0877 02          	.byte 0x2
 1952 0878 F8 25       	.2byte 0x25f8
 1953 087a ED 00 00 00 	.4byte 0xed
 1954 087e 02          	.byte 0x2
 1955 087f 01          	.byte 0x1
 1956 0880 0E          	.byte 0xe
 1957 0881 02          	.byte 0x2
 1958 0882 23          	.byte 0x23
 1959 0883 00          	.uleb128 0x0
 1960 0884 05          	.uleb128 0x5
 1961 0885 4D 49 32 43 	.asciz "MI2C2IE"
MPLAB XC16 ASSEMBLY Listing:   			page 41


 1961      32 49 45 00 
 1962 088d 02          	.byte 0x2
 1963 088e F9 25       	.2byte 0x25f9
 1964 0890 ED 00 00 00 	.4byte 0xed
 1965 0894 02          	.byte 0x2
 1966 0895 01          	.byte 0x1
 1967 0896 0D          	.byte 0xd
 1968 0897 02          	.byte 0x2
 1969 0898 23          	.byte 0x23
 1970 0899 00          	.uleb128 0x0
 1971 089a 05          	.uleb128 0x5
 1972 089b 54 38 49 45 	.asciz "T8IE"
 1972      00 
 1973 08a0 02          	.byte 0x2
 1974 08a1 FA 25       	.2byte 0x25fa
 1975 08a3 ED 00 00 00 	.4byte 0xed
 1976 08a7 02          	.byte 0x2
 1977 08a8 01          	.byte 0x1
 1978 08a9 0C          	.byte 0xc
 1979 08aa 02          	.byte 0x2
 1980 08ab 23          	.byte 0x23
 1981 08ac 00          	.uleb128 0x0
 1982 08ad 05          	.uleb128 0x5
 1983 08ae 54 39 49 45 	.asciz "T9IE"
 1983      00 
 1984 08b3 02          	.byte 0x2
 1985 08b4 FB 25       	.2byte 0x25fb
 1986 08b6 ED 00 00 00 	.4byte 0xed
 1987 08ba 02          	.byte 0x2
 1988 08bb 01          	.byte 0x1
 1989 08bc 0B          	.byte 0xb
 1990 08bd 02          	.byte 0x2
 1991 08be 23          	.byte 0x23
 1992 08bf 00          	.uleb128 0x0
 1993 08c0 05          	.uleb128 0x5
 1994 08c1 49 4E 54 33 	.asciz "INT3IE"
 1994      49 45 00 
 1995 08c8 02          	.byte 0x2
 1996 08c9 FC 25       	.2byte 0x25fc
 1997 08cb ED 00 00 00 	.4byte 0xed
 1998 08cf 02          	.byte 0x2
 1999 08d0 01          	.byte 0x1
 2000 08d1 0A          	.byte 0xa
 2001 08d2 02          	.byte 0x2
 2002 08d3 23          	.byte 0x23
 2003 08d4 00          	.uleb128 0x0
 2004 08d5 05          	.uleb128 0x5
 2005 08d6 49 4E 54 34 	.asciz "INT4IE"
 2005      49 45 00 
 2006 08dd 02          	.byte 0x2
 2007 08de FD 25       	.2byte 0x25fd
 2008 08e0 ED 00 00 00 	.4byte 0xed
 2009 08e4 02          	.byte 0x2
 2010 08e5 01          	.byte 0x1
 2011 08e6 09          	.byte 0x9
 2012 08e7 02          	.byte 0x2
 2013 08e8 23          	.byte 0x23
MPLAB XC16 ASSEMBLY Listing:   			page 42


 2014 08e9 00          	.uleb128 0x0
 2015 08ea 05          	.uleb128 0x5
 2016 08eb 43 32 52 58 	.asciz "C2RXIE"
 2016      49 45 00 
 2017 08f2 02          	.byte 0x2
 2018 08f3 FE 25       	.2byte 0x25fe
 2019 08f5 ED 00 00 00 	.4byte 0xed
 2020 08f9 02          	.byte 0x2
 2021 08fa 01          	.byte 0x1
 2022 08fb 08          	.byte 0x8
 2023 08fc 02          	.byte 0x2
 2024 08fd 23          	.byte 0x23
 2025 08fe 00          	.uleb128 0x0
 2026 08ff 05          	.uleb128 0x5
 2027 0900 43 32 49 45 	.asciz "C2IE"
 2027      00 
 2028 0905 02          	.byte 0x2
 2029 0906 FF 25       	.2byte 0x25ff
 2030 0908 ED 00 00 00 	.4byte 0xed
 2031 090c 02          	.byte 0x2
 2032 090d 01          	.byte 0x1
 2033 090e 07          	.byte 0x7
 2034 090f 02          	.byte 0x2
 2035 0910 23          	.byte 0x23
 2036 0911 00          	.uleb128 0x0
 2037 0912 05          	.uleb128 0x5
 2038 0913 50 53 45 4D 	.asciz "PSEMIE"
 2038      49 45 00 
 2039 091a 02          	.byte 0x2
 2040 091b 00 26       	.2byte 0x2600
 2041 091d ED 00 00 00 	.4byte 0xed
 2042 0921 02          	.byte 0x2
 2043 0922 01          	.byte 0x1
 2044 0923 06          	.byte 0x6
 2045 0924 02          	.byte 0x2
 2046 0925 23          	.byte 0x23
 2047 0926 00          	.uleb128 0x0
 2048 0927 05          	.uleb128 0x5
 2049 0928 51 45 49 31 	.asciz "QEI1IE"
 2049      49 45 00 
 2050 092f 02          	.byte 0x2
 2051 0930 01 26       	.2byte 0x2601
 2052 0932 ED 00 00 00 	.4byte 0xed
 2053 0936 02          	.byte 0x2
 2054 0937 01          	.byte 0x1
 2055 0938 05          	.byte 0x5
 2056 0939 02          	.byte 0x2
 2057 093a 23          	.byte 0x23
 2058 093b 00          	.uleb128 0x0
 2059 093c 05          	.uleb128 0x5
 2060 093d 44 43 49 45 	.asciz "DCIEIE"
 2060      49 45 00 
 2061 0944 02          	.byte 0x2
 2062 0945 02 26       	.2byte 0x2602
 2063 0947 ED 00 00 00 	.4byte 0xed
 2064 094b 02          	.byte 0x2
 2065 094c 01          	.byte 0x1
MPLAB XC16 ASSEMBLY Listing:   			page 43


 2066 094d 04          	.byte 0x4
 2067 094e 02          	.byte 0x2
 2068 094f 23          	.byte 0x23
 2069 0950 00          	.uleb128 0x0
 2070 0951 05          	.uleb128 0x5
 2071 0952 44 43 49 49 	.asciz "DCIIE"
 2071      45 00 
 2072 0958 02          	.byte 0x2
 2073 0959 03 26       	.2byte 0x2603
 2074 095b ED 00 00 00 	.4byte 0xed
 2075 095f 02          	.byte 0x2
 2076 0960 01          	.byte 0x1
 2077 0961 03          	.byte 0x3
 2078 0962 02          	.byte 0x2
 2079 0963 23          	.byte 0x23
 2080 0964 00          	.uleb128 0x0
 2081 0965 05          	.uleb128 0x5
 2082 0966 44 4D 41 35 	.asciz "DMA5IE"
 2082      49 45 00 
 2083 096d 02          	.byte 0x2
 2084 096e 04 26       	.2byte 0x2604
 2085 0970 ED 00 00 00 	.4byte 0xed
 2086 0974 02          	.byte 0x2
 2087 0975 01          	.byte 0x1
 2088 0976 02          	.byte 0x2
 2089 0977 02          	.byte 0x2
 2090 0978 23          	.byte 0x23
 2091 0979 00          	.uleb128 0x0
 2092 097a 05          	.uleb128 0x5
 2093 097b 52 54 43 49 	.asciz "RTCIE"
 2093      45 00 
 2094 0981 02          	.byte 0x2
 2095 0982 05 26       	.2byte 0x2605
 2096 0984 ED 00 00 00 	.4byte 0xed
 2097 0988 02          	.byte 0x2
 2098 0989 01          	.byte 0x1
 2099 098a 01          	.byte 0x1
 2100 098b 02          	.byte 0x2
 2101 098c 23          	.byte 0x23
 2102 098d 00          	.uleb128 0x0
 2103 098e 00          	.byte 0x0
 2104 098f 06          	.uleb128 0x6
 2105 0990 49 45 43 33 	.asciz "IEC3BITS"
 2105      42 49 54 53 
 2105      00 
 2106 0999 02          	.byte 0x2
 2107 099a 06 26       	.2byte 0x2606
 2108 099c 46 08 00 00 	.4byte 0x846
 2109 09a0 07          	.uleb128 0x7
 2110 09a1 02          	.byte 0x2
 2111 09a2 02          	.byte 0x2
 2112 09a3 62 26       	.2byte 0x2662
 2113 09a5 FA 09 00 00 	.4byte 0x9fa
 2114 09a9 05          	.uleb128 0x5
 2115 09aa 49 4E 54 30 	.asciz "INT0IP"
 2115      49 50 00 
 2116 09b1 02          	.byte 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 44


 2117 09b2 63 26       	.2byte 0x2663
 2118 09b4 ED 00 00 00 	.4byte 0xed
 2119 09b8 02          	.byte 0x2
 2120 09b9 03          	.byte 0x3
 2121 09ba 0D          	.byte 0xd
 2122 09bb 02          	.byte 0x2
 2123 09bc 23          	.byte 0x23
 2124 09bd 00          	.uleb128 0x0
 2125 09be 05          	.uleb128 0x5
 2126 09bf 49 43 31 49 	.asciz "IC1IP"
 2126      50 00 
 2127 09c5 02          	.byte 0x2
 2128 09c6 65 26       	.2byte 0x2665
 2129 09c8 ED 00 00 00 	.4byte 0xed
 2130 09cc 02          	.byte 0x2
 2131 09cd 03          	.byte 0x3
 2132 09ce 09          	.byte 0x9
 2133 09cf 02          	.byte 0x2
 2134 09d0 23          	.byte 0x23
 2135 09d1 00          	.uleb128 0x0
 2136 09d2 05          	.uleb128 0x5
 2137 09d3 4F 43 31 49 	.asciz "OC1IP"
 2137      50 00 
 2138 09d9 02          	.byte 0x2
 2139 09da 67 26       	.2byte 0x2667
 2140 09dc ED 00 00 00 	.4byte 0xed
 2141 09e0 02          	.byte 0x2
 2142 09e1 03          	.byte 0x3
 2143 09e2 05          	.byte 0x5
 2144 09e3 02          	.byte 0x2
 2145 09e4 23          	.byte 0x23
 2146 09e5 00          	.uleb128 0x0
 2147 09e6 05          	.uleb128 0x5
 2148 09e7 54 31 49 50 	.asciz "T1IP"
 2148      00 
 2149 09ec 02          	.byte 0x2
 2150 09ed 69 26       	.2byte 0x2669
 2151 09ef ED 00 00 00 	.4byte 0xed
 2152 09f3 02          	.byte 0x2
 2153 09f4 03          	.byte 0x3
 2154 09f5 01          	.byte 0x1
 2155 09f6 02          	.byte 0x2
 2156 09f7 23          	.byte 0x23
 2157 09f8 00          	.uleb128 0x0
 2158 09f9 00          	.byte 0x0
 2159 09fa 07          	.uleb128 0x7
 2160 09fb 02          	.byte 0x2
 2161 09fc 02          	.byte 0x2
 2162 09fd 6B 26       	.2byte 0x266b
 2163 09ff 00 0B 00 00 	.4byte 0xb00
 2164 0a03 05          	.uleb128 0x5
 2165 0a04 49 4E 54 30 	.asciz "INT0IP0"
 2165      49 50 30 00 
 2166 0a0c 02          	.byte 0x2
 2167 0a0d 6C 26       	.2byte 0x266c
 2168 0a0f ED 00 00 00 	.4byte 0xed
 2169 0a13 02          	.byte 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 45


 2170 0a14 01          	.byte 0x1
 2171 0a15 0F          	.byte 0xf
 2172 0a16 02          	.byte 0x2
 2173 0a17 23          	.byte 0x23
 2174 0a18 00          	.uleb128 0x0
 2175 0a19 05          	.uleb128 0x5
 2176 0a1a 49 4E 54 30 	.asciz "INT0IP1"
 2176      49 50 31 00 
 2177 0a22 02          	.byte 0x2
 2178 0a23 6D 26       	.2byte 0x266d
 2179 0a25 ED 00 00 00 	.4byte 0xed
 2180 0a29 02          	.byte 0x2
 2181 0a2a 01          	.byte 0x1
 2182 0a2b 0E          	.byte 0xe
 2183 0a2c 02          	.byte 0x2
 2184 0a2d 23          	.byte 0x23
 2185 0a2e 00          	.uleb128 0x0
 2186 0a2f 05          	.uleb128 0x5
 2187 0a30 49 4E 54 30 	.asciz "INT0IP2"
 2187      49 50 32 00 
 2188 0a38 02          	.byte 0x2
 2189 0a39 6E 26       	.2byte 0x266e
 2190 0a3b ED 00 00 00 	.4byte 0xed
 2191 0a3f 02          	.byte 0x2
 2192 0a40 01          	.byte 0x1
 2193 0a41 0D          	.byte 0xd
 2194 0a42 02          	.byte 0x2
 2195 0a43 23          	.byte 0x23
 2196 0a44 00          	.uleb128 0x0
 2197 0a45 05          	.uleb128 0x5
 2198 0a46 49 43 31 49 	.asciz "IC1IP0"
 2198      50 30 00 
 2199 0a4d 02          	.byte 0x2
 2200 0a4e 70 26       	.2byte 0x2670
 2201 0a50 ED 00 00 00 	.4byte 0xed
 2202 0a54 02          	.byte 0x2
 2203 0a55 01          	.byte 0x1
 2204 0a56 0B          	.byte 0xb
 2205 0a57 02          	.byte 0x2
 2206 0a58 23          	.byte 0x23
 2207 0a59 00          	.uleb128 0x0
 2208 0a5a 05          	.uleb128 0x5
 2209 0a5b 49 43 31 49 	.asciz "IC1IP1"
 2209      50 31 00 
 2210 0a62 02          	.byte 0x2
 2211 0a63 71 26       	.2byte 0x2671
 2212 0a65 ED 00 00 00 	.4byte 0xed
 2213 0a69 02          	.byte 0x2
 2214 0a6a 01          	.byte 0x1
 2215 0a6b 0A          	.byte 0xa
 2216 0a6c 02          	.byte 0x2
 2217 0a6d 23          	.byte 0x23
 2218 0a6e 00          	.uleb128 0x0
 2219 0a6f 05          	.uleb128 0x5
 2220 0a70 49 43 31 49 	.asciz "IC1IP2"
 2220      50 32 00 
 2221 0a77 02          	.byte 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 46


 2222 0a78 72 26       	.2byte 0x2672
 2223 0a7a ED 00 00 00 	.4byte 0xed
 2224 0a7e 02          	.byte 0x2
 2225 0a7f 01          	.byte 0x1
 2226 0a80 09          	.byte 0x9
 2227 0a81 02          	.byte 0x2
 2228 0a82 23          	.byte 0x23
 2229 0a83 00          	.uleb128 0x0
 2230 0a84 05          	.uleb128 0x5
 2231 0a85 4F 43 31 49 	.asciz "OC1IP0"
 2231      50 30 00 
 2232 0a8c 02          	.byte 0x2
 2233 0a8d 74 26       	.2byte 0x2674
 2234 0a8f ED 00 00 00 	.4byte 0xed
 2235 0a93 02          	.byte 0x2
 2236 0a94 01          	.byte 0x1
 2237 0a95 07          	.byte 0x7
 2238 0a96 02          	.byte 0x2
 2239 0a97 23          	.byte 0x23
 2240 0a98 00          	.uleb128 0x0
 2241 0a99 05          	.uleb128 0x5
 2242 0a9a 4F 43 31 49 	.asciz "OC1IP1"
 2242      50 31 00 
 2243 0aa1 02          	.byte 0x2
 2244 0aa2 75 26       	.2byte 0x2675
 2245 0aa4 ED 00 00 00 	.4byte 0xed
 2246 0aa8 02          	.byte 0x2
 2247 0aa9 01          	.byte 0x1
 2248 0aaa 06          	.byte 0x6
 2249 0aab 02          	.byte 0x2
 2250 0aac 23          	.byte 0x23
 2251 0aad 00          	.uleb128 0x0
 2252 0aae 05          	.uleb128 0x5
 2253 0aaf 4F 43 31 49 	.asciz "OC1IP2"
 2253      50 32 00 
 2254 0ab6 02          	.byte 0x2
 2255 0ab7 76 26       	.2byte 0x2676
 2256 0ab9 ED 00 00 00 	.4byte 0xed
 2257 0abd 02          	.byte 0x2
 2258 0abe 01          	.byte 0x1
 2259 0abf 05          	.byte 0x5
 2260 0ac0 02          	.byte 0x2
 2261 0ac1 23          	.byte 0x23
 2262 0ac2 00          	.uleb128 0x0
 2263 0ac3 05          	.uleb128 0x5
 2264 0ac4 54 31 49 50 	.asciz "T1IP0"
 2264      30 00 
 2265 0aca 02          	.byte 0x2
 2266 0acb 78 26       	.2byte 0x2678
 2267 0acd ED 00 00 00 	.4byte 0xed
 2268 0ad1 02          	.byte 0x2
 2269 0ad2 01          	.byte 0x1
 2270 0ad3 03          	.byte 0x3
 2271 0ad4 02          	.byte 0x2
 2272 0ad5 23          	.byte 0x23
 2273 0ad6 00          	.uleb128 0x0
 2274 0ad7 05          	.uleb128 0x5
MPLAB XC16 ASSEMBLY Listing:   			page 47


 2275 0ad8 54 31 49 50 	.asciz "T1IP1"
 2275      31 00 
 2276 0ade 02          	.byte 0x2
 2277 0adf 79 26       	.2byte 0x2679
 2278 0ae1 ED 00 00 00 	.4byte 0xed
 2279 0ae5 02          	.byte 0x2
 2280 0ae6 01          	.byte 0x1
 2281 0ae7 02          	.byte 0x2
 2282 0ae8 02          	.byte 0x2
 2283 0ae9 23          	.byte 0x23
 2284 0aea 00          	.uleb128 0x0
 2285 0aeb 05          	.uleb128 0x5
 2286 0aec 54 31 49 50 	.asciz "T1IP2"
 2286      32 00 
 2287 0af2 02          	.byte 0x2
 2288 0af3 7A 26       	.2byte 0x267a
 2289 0af5 ED 00 00 00 	.4byte 0xed
 2290 0af9 02          	.byte 0x2
 2291 0afa 01          	.byte 0x1
 2292 0afb 01          	.byte 0x1
 2293 0afc 02          	.byte 0x2
 2294 0afd 23          	.byte 0x23
 2295 0afe 00          	.uleb128 0x0
 2296 0aff 00          	.byte 0x0
 2297 0b00 08          	.uleb128 0x8
 2298 0b01 02          	.byte 0x2
 2299 0b02 02          	.byte 0x2
 2300 0b03 61 26       	.2byte 0x2661
 2301 0b05 14 0B 00 00 	.4byte 0xb14
 2302 0b09 09          	.uleb128 0x9
 2303 0b0a A0 09 00 00 	.4byte 0x9a0
 2304 0b0e 09          	.uleb128 0x9
 2305 0b0f FA 09 00 00 	.4byte 0x9fa
 2306 0b13 00          	.byte 0x0
 2307 0b14 04          	.uleb128 0x4
 2308 0b15 74 61 67 49 	.asciz "tagIPC0BITS"
 2308      50 43 30 42 
 2308      49 54 53 00 
 2309 0b21 02          	.byte 0x2
 2310 0b22 02          	.byte 0x2
 2311 0b23 60 26       	.2byte 0x2660
 2312 0b25 32 0B 00 00 	.4byte 0xb32
 2313 0b29 0A          	.uleb128 0xa
 2314 0b2a 00 0B 00 00 	.4byte 0xb00
 2315 0b2e 02          	.byte 0x2
 2316 0b2f 23          	.byte 0x23
 2317 0b30 00          	.uleb128 0x0
 2318 0b31 00          	.byte 0x0
 2319 0b32 06          	.uleb128 0x6
 2320 0b33 49 50 43 30 	.asciz "IPC0BITS"
 2320      42 49 54 53 
 2320      00 
 2321 0b3c 02          	.byte 0x2
 2322 0b3d 7D 26       	.2byte 0x267d
 2323 0b3f 14 0B 00 00 	.4byte 0xb14
 2324 0b43 07          	.uleb128 0x7
 2325 0b44 02          	.byte 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 48


 2326 0b45 02          	.byte 0x2
 2327 0b46 0C 27       	.2byte 0x270c
 2328 0b48 9E 0B 00 00 	.4byte 0xb9e
 2329 0b4c 05          	.uleb128 0x5
 2330 0b4d 49 4E 54 31 	.asciz "INT1IP"
 2330      49 50 00 
 2331 0b54 02          	.byte 0x2
 2332 0b55 0D 27       	.2byte 0x270d
 2333 0b57 ED 00 00 00 	.4byte 0xed
 2334 0b5b 02          	.byte 0x2
 2335 0b5c 03          	.byte 0x3
 2336 0b5d 0D          	.byte 0xd
 2337 0b5e 02          	.byte 0x2
 2338 0b5f 23          	.byte 0x23
 2339 0b60 00          	.uleb128 0x0
 2340 0b61 05          	.uleb128 0x5
 2341 0b62 41 44 32 49 	.asciz "AD2IP"
 2341      50 00 
 2342 0b68 02          	.byte 0x2
 2343 0b69 0F 27       	.2byte 0x270f
 2344 0b6b ED 00 00 00 	.4byte 0xed
 2345 0b6f 02          	.byte 0x2
 2346 0b70 03          	.byte 0x3
 2347 0b71 09          	.byte 0x9
 2348 0b72 02          	.byte 0x2
 2349 0b73 23          	.byte 0x23
 2350 0b74 00          	.uleb128 0x0
 2351 0b75 05          	.uleb128 0x5
 2352 0b76 49 43 37 49 	.asciz "IC7IP"
 2352      50 00 
 2353 0b7c 02          	.byte 0x2
 2354 0b7d 11 27       	.2byte 0x2711
 2355 0b7f ED 00 00 00 	.4byte 0xed
 2356 0b83 02          	.byte 0x2
 2357 0b84 03          	.byte 0x3
 2358 0b85 05          	.byte 0x5
 2359 0b86 02          	.byte 0x2
 2360 0b87 23          	.byte 0x23
 2361 0b88 00          	.uleb128 0x0
 2362 0b89 05          	.uleb128 0x5
 2363 0b8a 49 43 38 49 	.asciz "IC8IP"
 2363      50 00 
 2364 0b90 02          	.byte 0x2
 2365 0b91 13 27       	.2byte 0x2713
 2366 0b93 ED 00 00 00 	.4byte 0xed
 2367 0b97 02          	.byte 0x2
 2368 0b98 03          	.byte 0x3
 2369 0b99 01          	.byte 0x1
 2370 0b9a 02          	.byte 0x2
 2371 0b9b 23          	.byte 0x23
 2372 0b9c 00          	.uleb128 0x0
 2373 0b9d 00          	.byte 0x0
 2374 0b9e 07          	.uleb128 0x7
 2375 0b9f 02          	.byte 0x2
 2376 0ba0 02          	.byte 0x2
 2377 0ba1 15 27       	.2byte 0x2715
 2378 0ba3 A7 0C 00 00 	.4byte 0xca7
MPLAB XC16 ASSEMBLY Listing:   			page 49


 2379 0ba7 05          	.uleb128 0x5
 2380 0ba8 49 4E 54 31 	.asciz "INT1IP0"
 2380      49 50 30 00 
 2381 0bb0 02          	.byte 0x2
 2382 0bb1 16 27       	.2byte 0x2716
 2383 0bb3 ED 00 00 00 	.4byte 0xed
 2384 0bb7 02          	.byte 0x2
 2385 0bb8 01          	.byte 0x1
 2386 0bb9 0F          	.byte 0xf
 2387 0bba 02          	.byte 0x2
 2388 0bbb 23          	.byte 0x23
 2389 0bbc 00          	.uleb128 0x0
 2390 0bbd 05          	.uleb128 0x5
 2391 0bbe 49 4E 54 31 	.asciz "INT1IP1"
 2391      49 50 31 00 
 2392 0bc6 02          	.byte 0x2
 2393 0bc7 17 27       	.2byte 0x2717
 2394 0bc9 ED 00 00 00 	.4byte 0xed
 2395 0bcd 02          	.byte 0x2
 2396 0bce 01          	.byte 0x1
 2397 0bcf 0E          	.byte 0xe
 2398 0bd0 02          	.byte 0x2
 2399 0bd1 23          	.byte 0x23
 2400 0bd2 00          	.uleb128 0x0
 2401 0bd3 05          	.uleb128 0x5
 2402 0bd4 49 4E 54 31 	.asciz "INT1IP2"
 2402      49 50 32 00 
 2403 0bdc 02          	.byte 0x2
 2404 0bdd 18 27       	.2byte 0x2718
 2405 0bdf ED 00 00 00 	.4byte 0xed
 2406 0be3 02          	.byte 0x2
 2407 0be4 01          	.byte 0x1
 2408 0be5 0D          	.byte 0xd
 2409 0be6 02          	.byte 0x2
 2410 0be7 23          	.byte 0x23
 2411 0be8 00          	.uleb128 0x0
 2412 0be9 05          	.uleb128 0x5
 2413 0bea 41 44 32 49 	.asciz "AD2IP0"
 2413      50 30 00 
 2414 0bf1 02          	.byte 0x2
 2415 0bf2 1A 27       	.2byte 0x271a
 2416 0bf4 ED 00 00 00 	.4byte 0xed
 2417 0bf8 02          	.byte 0x2
 2418 0bf9 01          	.byte 0x1
 2419 0bfa 0B          	.byte 0xb
 2420 0bfb 02          	.byte 0x2
 2421 0bfc 23          	.byte 0x23
 2422 0bfd 00          	.uleb128 0x0
 2423 0bfe 05          	.uleb128 0x5
 2424 0bff 41 44 32 49 	.asciz "AD2IP1"
 2424      50 31 00 
 2425 0c06 02          	.byte 0x2
 2426 0c07 1B 27       	.2byte 0x271b
 2427 0c09 ED 00 00 00 	.4byte 0xed
 2428 0c0d 02          	.byte 0x2
 2429 0c0e 01          	.byte 0x1
 2430 0c0f 0A          	.byte 0xa
MPLAB XC16 ASSEMBLY Listing:   			page 50


 2431 0c10 02          	.byte 0x2
 2432 0c11 23          	.byte 0x23
 2433 0c12 00          	.uleb128 0x0
 2434 0c13 05          	.uleb128 0x5
 2435 0c14 41 44 32 49 	.asciz "AD2IP2"
 2435      50 32 00 
 2436 0c1b 02          	.byte 0x2
 2437 0c1c 1C 27       	.2byte 0x271c
 2438 0c1e ED 00 00 00 	.4byte 0xed
 2439 0c22 02          	.byte 0x2
 2440 0c23 01          	.byte 0x1
 2441 0c24 09          	.byte 0x9
 2442 0c25 02          	.byte 0x2
 2443 0c26 23          	.byte 0x23
 2444 0c27 00          	.uleb128 0x0
 2445 0c28 05          	.uleb128 0x5
 2446 0c29 49 43 37 49 	.asciz "IC7IP0"
 2446      50 30 00 
 2447 0c30 02          	.byte 0x2
 2448 0c31 1E 27       	.2byte 0x271e
 2449 0c33 ED 00 00 00 	.4byte 0xed
 2450 0c37 02          	.byte 0x2
 2451 0c38 01          	.byte 0x1
 2452 0c39 07          	.byte 0x7
 2453 0c3a 02          	.byte 0x2
 2454 0c3b 23          	.byte 0x23
 2455 0c3c 00          	.uleb128 0x0
 2456 0c3d 05          	.uleb128 0x5
 2457 0c3e 49 43 37 49 	.asciz "IC7IP1"
 2457      50 31 00 
 2458 0c45 02          	.byte 0x2
 2459 0c46 1F 27       	.2byte 0x271f
 2460 0c48 ED 00 00 00 	.4byte 0xed
 2461 0c4c 02          	.byte 0x2
 2462 0c4d 01          	.byte 0x1
 2463 0c4e 06          	.byte 0x6
 2464 0c4f 02          	.byte 0x2
 2465 0c50 23          	.byte 0x23
 2466 0c51 00          	.uleb128 0x0
 2467 0c52 05          	.uleb128 0x5
 2468 0c53 49 43 37 49 	.asciz "IC7IP2"
 2468      50 32 00 
 2469 0c5a 02          	.byte 0x2
 2470 0c5b 20 27       	.2byte 0x2720
 2471 0c5d ED 00 00 00 	.4byte 0xed
 2472 0c61 02          	.byte 0x2
 2473 0c62 01          	.byte 0x1
 2474 0c63 05          	.byte 0x5
 2475 0c64 02          	.byte 0x2
 2476 0c65 23          	.byte 0x23
 2477 0c66 00          	.uleb128 0x0
 2478 0c67 05          	.uleb128 0x5
 2479 0c68 49 43 38 49 	.asciz "IC8IP0"
 2479      50 30 00 
 2480 0c6f 02          	.byte 0x2
 2481 0c70 22 27       	.2byte 0x2722
 2482 0c72 ED 00 00 00 	.4byte 0xed
MPLAB XC16 ASSEMBLY Listing:   			page 51


 2483 0c76 02          	.byte 0x2
 2484 0c77 01          	.byte 0x1
 2485 0c78 03          	.byte 0x3
 2486 0c79 02          	.byte 0x2
 2487 0c7a 23          	.byte 0x23
 2488 0c7b 00          	.uleb128 0x0
 2489 0c7c 05          	.uleb128 0x5
 2490 0c7d 49 43 38 49 	.asciz "IC8IP1"
 2490      50 31 00 
 2491 0c84 02          	.byte 0x2
 2492 0c85 23 27       	.2byte 0x2723
 2493 0c87 ED 00 00 00 	.4byte 0xed
 2494 0c8b 02          	.byte 0x2
 2495 0c8c 01          	.byte 0x1
 2496 0c8d 02          	.byte 0x2
 2497 0c8e 02          	.byte 0x2
 2498 0c8f 23          	.byte 0x23
 2499 0c90 00          	.uleb128 0x0
 2500 0c91 05          	.uleb128 0x5
 2501 0c92 49 43 38 49 	.asciz "IC8IP2"
 2501      50 32 00 
 2502 0c99 02          	.byte 0x2
 2503 0c9a 24 27       	.2byte 0x2724
 2504 0c9c ED 00 00 00 	.4byte 0xed
 2505 0ca0 02          	.byte 0x2
 2506 0ca1 01          	.byte 0x1
 2507 0ca2 01          	.byte 0x1
 2508 0ca3 02          	.byte 0x2
 2509 0ca4 23          	.byte 0x23
 2510 0ca5 00          	.uleb128 0x0
 2511 0ca6 00          	.byte 0x0
 2512 0ca7 08          	.uleb128 0x8
 2513 0ca8 02          	.byte 0x2
 2514 0ca9 02          	.byte 0x2
 2515 0caa 0B 27       	.2byte 0x270b
 2516 0cac BB 0C 00 00 	.4byte 0xcbb
 2517 0cb0 09          	.uleb128 0x9
 2518 0cb1 43 0B 00 00 	.4byte 0xb43
 2519 0cb5 09          	.uleb128 0x9
 2520 0cb6 9E 0B 00 00 	.4byte 0xb9e
 2521 0cba 00          	.byte 0x0
 2522 0cbb 04          	.uleb128 0x4
 2523 0cbc 74 61 67 49 	.asciz "tagIPC5BITS"
 2523      50 43 35 42 
 2523      49 54 53 00 
 2524 0cc8 02          	.byte 0x2
 2525 0cc9 02          	.byte 0x2
 2526 0cca 0A 27       	.2byte 0x270a
 2527 0ccc D9 0C 00 00 	.4byte 0xcd9
 2528 0cd0 0A          	.uleb128 0xa
 2529 0cd1 A7 0C 00 00 	.4byte 0xca7
 2530 0cd5 02          	.byte 0x2
 2531 0cd6 23          	.byte 0x23
 2532 0cd7 00          	.uleb128 0x0
 2533 0cd8 00          	.byte 0x0
 2534 0cd9 06          	.uleb128 0x6
 2535 0cda 49 50 43 35 	.asciz "IPC5BITS"
MPLAB XC16 ASSEMBLY Listing:   			page 52


 2535      42 49 54 53 
 2535      00 
 2536 0ce3 02          	.byte 0x2
 2537 0ce4 27 27       	.2byte 0x2727
 2538 0ce6 BB 0C 00 00 	.4byte 0xcbb
 2539 0cea 07          	.uleb128 0x7
 2540 0ceb 02          	.byte 0x2
 2541 0cec 02          	.byte 0x2
 2542 0ced 50 27       	.2byte 0x2750
 2543 0cef 46 0D 00 00 	.4byte 0xd46
 2544 0cf3 05          	.uleb128 0x5
 2545 0cf4 54 35 49 50 	.asciz "T5IP"
 2545      00 
 2546 0cf9 02          	.byte 0x2
 2547 0cfa 51 27       	.2byte 0x2751
 2548 0cfc ED 00 00 00 	.4byte 0xed
 2549 0d00 02          	.byte 0x2
 2550 0d01 03          	.byte 0x3
 2551 0d02 0D          	.byte 0xd
 2552 0d03 02          	.byte 0x2
 2553 0d04 23          	.byte 0x23
 2554 0d05 00          	.uleb128 0x0
 2555 0d06 05          	.uleb128 0x5
 2556 0d07 49 4E 54 32 	.asciz "INT2IP"
 2556      49 50 00 
 2557 0d0e 02          	.byte 0x2
 2558 0d0f 53 27       	.2byte 0x2753
 2559 0d11 ED 00 00 00 	.4byte 0xed
 2560 0d15 02          	.byte 0x2
 2561 0d16 03          	.byte 0x3
 2562 0d17 09          	.byte 0x9
 2563 0d18 02          	.byte 0x2
 2564 0d19 23          	.byte 0x23
 2565 0d1a 00          	.uleb128 0x0
 2566 0d1b 05          	.uleb128 0x5
 2567 0d1c 55 32 52 58 	.asciz "U2RXIP"
 2567      49 50 00 
 2568 0d23 02          	.byte 0x2
 2569 0d24 55 27       	.2byte 0x2755
 2570 0d26 ED 00 00 00 	.4byte 0xed
 2571 0d2a 02          	.byte 0x2
 2572 0d2b 03          	.byte 0x3
 2573 0d2c 05          	.byte 0x5
 2574 0d2d 02          	.byte 0x2
 2575 0d2e 23          	.byte 0x23
 2576 0d2f 00          	.uleb128 0x0
 2577 0d30 05          	.uleb128 0x5
 2578 0d31 55 32 54 58 	.asciz "U2TXIP"
 2578      49 50 00 
 2579 0d38 02          	.byte 0x2
 2580 0d39 57 27       	.2byte 0x2757
 2581 0d3b ED 00 00 00 	.4byte 0xed
 2582 0d3f 02          	.byte 0x2
 2583 0d40 03          	.byte 0x3
 2584 0d41 01          	.byte 0x1
 2585 0d42 02          	.byte 0x2
 2586 0d43 23          	.byte 0x23
MPLAB XC16 ASSEMBLY Listing:   			page 53


 2587 0d44 00          	.uleb128 0x0
 2588 0d45 00          	.byte 0x0
 2589 0d46 07          	.uleb128 0x7
 2590 0d47 02          	.byte 0x2
 2591 0d48 02          	.byte 0x2
 2592 0d49 59 27       	.2byte 0x2759
 2593 0d4b 52 0E 00 00 	.4byte 0xe52
 2594 0d4f 05          	.uleb128 0x5
 2595 0d50 54 35 49 50 	.asciz "T5IP0"
 2595      30 00 
 2596 0d56 02          	.byte 0x2
 2597 0d57 5A 27       	.2byte 0x275a
 2598 0d59 ED 00 00 00 	.4byte 0xed
 2599 0d5d 02          	.byte 0x2
 2600 0d5e 01          	.byte 0x1
 2601 0d5f 0F          	.byte 0xf
 2602 0d60 02          	.byte 0x2
 2603 0d61 23          	.byte 0x23
 2604 0d62 00          	.uleb128 0x0
 2605 0d63 05          	.uleb128 0x5
 2606 0d64 54 35 49 50 	.asciz "T5IP1"
 2606      31 00 
 2607 0d6a 02          	.byte 0x2
 2608 0d6b 5B 27       	.2byte 0x275b
 2609 0d6d ED 00 00 00 	.4byte 0xed
 2610 0d71 02          	.byte 0x2
 2611 0d72 01          	.byte 0x1
 2612 0d73 0E          	.byte 0xe
 2613 0d74 02          	.byte 0x2
 2614 0d75 23          	.byte 0x23
 2615 0d76 00          	.uleb128 0x0
 2616 0d77 05          	.uleb128 0x5
 2617 0d78 54 35 49 50 	.asciz "T5IP2"
 2617      32 00 
 2618 0d7e 02          	.byte 0x2
 2619 0d7f 5C 27       	.2byte 0x275c
 2620 0d81 ED 00 00 00 	.4byte 0xed
 2621 0d85 02          	.byte 0x2
 2622 0d86 01          	.byte 0x1
 2623 0d87 0D          	.byte 0xd
 2624 0d88 02          	.byte 0x2
 2625 0d89 23          	.byte 0x23
 2626 0d8a 00          	.uleb128 0x0
 2627 0d8b 05          	.uleb128 0x5
 2628 0d8c 49 4E 54 32 	.asciz "INT2IP0"
 2628      49 50 30 00 
 2629 0d94 02          	.byte 0x2
 2630 0d95 5E 27       	.2byte 0x275e
 2631 0d97 ED 00 00 00 	.4byte 0xed
 2632 0d9b 02          	.byte 0x2
 2633 0d9c 01          	.byte 0x1
 2634 0d9d 0B          	.byte 0xb
 2635 0d9e 02          	.byte 0x2
 2636 0d9f 23          	.byte 0x23
 2637 0da0 00          	.uleb128 0x0
 2638 0da1 05          	.uleb128 0x5
 2639 0da2 49 4E 54 32 	.asciz "INT2IP1"
MPLAB XC16 ASSEMBLY Listing:   			page 54


 2639      49 50 31 00 
 2640 0daa 02          	.byte 0x2
 2641 0dab 5F 27       	.2byte 0x275f
 2642 0dad ED 00 00 00 	.4byte 0xed
 2643 0db1 02          	.byte 0x2
 2644 0db2 01          	.byte 0x1
 2645 0db3 0A          	.byte 0xa
 2646 0db4 02          	.byte 0x2
 2647 0db5 23          	.byte 0x23
 2648 0db6 00          	.uleb128 0x0
 2649 0db7 05          	.uleb128 0x5
 2650 0db8 49 4E 54 32 	.asciz "INT2IP2"
 2650      49 50 32 00 
 2651 0dc0 02          	.byte 0x2
 2652 0dc1 60 27       	.2byte 0x2760
 2653 0dc3 ED 00 00 00 	.4byte 0xed
 2654 0dc7 02          	.byte 0x2
 2655 0dc8 01          	.byte 0x1
 2656 0dc9 09          	.byte 0x9
 2657 0dca 02          	.byte 0x2
 2658 0dcb 23          	.byte 0x23
 2659 0dcc 00          	.uleb128 0x0
 2660 0dcd 05          	.uleb128 0x5
 2661 0dce 55 32 52 58 	.asciz "U2RXIP0"
 2661      49 50 30 00 
 2662 0dd6 02          	.byte 0x2
 2663 0dd7 62 27       	.2byte 0x2762
 2664 0dd9 ED 00 00 00 	.4byte 0xed
 2665 0ddd 02          	.byte 0x2
 2666 0dde 01          	.byte 0x1
 2667 0ddf 07          	.byte 0x7
 2668 0de0 02          	.byte 0x2
 2669 0de1 23          	.byte 0x23
 2670 0de2 00          	.uleb128 0x0
 2671 0de3 05          	.uleb128 0x5
 2672 0de4 55 32 52 58 	.asciz "U2RXIP1"
 2672      49 50 31 00 
 2673 0dec 02          	.byte 0x2
 2674 0ded 63 27       	.2byte 0x2763
 2675 0def ED 00 00 00 	.4byte 0xed
 2676 0df3 02          	.byte 0x2
 2677 0df4 01          	.byte 0x1
 2678 0df5 06          	.byte 0x6
 2679 0df6 02          	.byte 0x2
 2680 0df7 23          	.byte 0x23
 2681 0df8 00          	.uleb128 0x0
 2682 0df9 05          	.uleb128 0x5
 2683 0dfa 55 32 52 58 	.asciz "U2RXIP2"
 2683      49 50 32 00 
 2684 0e02 02          	.byte 0x2
 2685 0e03 64 27       	.2byte 0x2764
 2686 0e05 ED 00 00 00 	.4byte 0xed
 2687 0e09 02          	.byte 0x2
 2688 0e0a 01          	.byte 0x1
 2689 0e0b 05          	.byte 0x5
 2690 0e0c 02          	.byte 0x2
 2691 0e0d 23          	.byte 0x23
MPLAB XC16 ASSEMBLY Listing:   			page 55


 2692 0e0e 00          	.uleb128 0x0
 2693 0e0f 05          	.uleb128 0x5
 2694 0e10 55 32 54 58 	.asciz "U2TXIP0"
 2694      49 50 30 00 
 2695 0e18 02          	.byte 0x2
 2696 0e19 66 27       	.2byte 0x2766
 2697 0e1b ED 00 00 00 	.4byte 0xed
 2698 0e1f 02          	.byte 0x2
 2699 0e20 01          	.byte 0x1
 2700 0e21 03          	.byte 0x3
 2701 0e22 02          	.byte 0x2
 2702 0e23 23          	.byte 0x23
 2703 0e24 00          	.uleb128 0x0
 2704 0e25 05          	.uleb128 0x5
 2705 0e26 55 32 54 58 	.asciz "U2TXIP1"
 2705      49 50 31 00 
 2706 0e2e 02          	.byte 0x2
 2707 0e2f 67 27       	.2byte 0x2767
 2708 0e31 ED 00 00 00 	.4byte 0xed
 2709 0e35 02          	.byte 0x2
 2710 0e36 01          	.byte 0x1
 2711 0e37 02          	.byte 0x2
 2712 0e38 02          	.byte 0x2
 2713 0e39 23          	.byte 0x23
 2714 0e3a 00          	.uleb128 0x0
 2715 0e3b 05          	.uleb128 0x5
 2716 0e3c 55 32 54 58 	.asciz "U2TXIP2"
 2716      49 50 32 00 
 2717 0e44 02          	.byte 0x2
 2718 0e45 68 27       	.2byte 0x2768
 2719 0e47 ED 00 00 00 	.4byte 0xed
 2720 0e4b 02          	.byte 0x2
 2721 0e4c 01          	.byte 0x1
 2722 0e4d 01          	.byte 0x1
 2723 0e4e 02          	.byte 0x2
 2724 0e4f 23          	.byte 0x23
 2725 0e50 00          	.uleb128 0x0
 2726 0e51 00          	.byte 0x0
 2727 0e52 08          	.uleb128 0x8
 2728 0e53 02          	.byte 0x2
 2729 0e54 02          	.byte 0x2
 2730 0e55 4F 27       	.2byte 0x274f
 2731 0e57 66 0E 00 00 	.4byte 0xe66
 2732 0e5b 09          	.uleb128 0x9
 2733 0e5c EA 0C 00 00 	.4byte 0xcea
 2734 0e60 09          	.uleb128 0x9
 2735 0e61 46 0D 00 00 	.4byte 0xd46
 2736 0e65 00          	.byte 0x0
 2737 0e66 04          	.uleb128 0x4
 2738 0e67 74 61 67 49 	.asciz "tagIPC7BITS"
 2738      50 43 37 42 
 2738      49 54 53 00 
 2739 0e73 02          	.byte 0x2
 2740 0e74 02          	.byte 0x2
 2741 0e75 4E 27       	.2byte 0x274e
 2742 0e77 84 0E 00 00 	.4byte 0xe84
 2743 0e7b 0A          	.uleb128 0xa
MPLAB XC16 ASSEMBLY Listing:   			page 56


 2744 0e7c 52 0E 00 00 	.4byte 0xe52
 2745 0e80 02          	.byte 0x2
 2746 0e81 23          	.byte 0x23
 2747 0e82 00          	.uleb128 0x0
 2748 0e83 00          	.byte 0x0
 2749 0e84 06          	.uleb128 0x6
 2750 0e85 49 50 43 37 	.asciz "IPC7BITS"
 2750      42 49 54 53 
 2750      00 
 2751 0e8e 02          	.byte 0x2
 2752 0e8f 6B 27       	.2byte 0x276b
 2753 0e91 66 0E 00 00 	.4byte 0xe66
 2754 0e95 07          	.uleb128 0x7
 2755 0e96 02          	.byte 0x2
 2756 0e97 02          	.byte 0x2
 2757 0e98 1C 28       	.2byte 0x281c
 2758 0e9a F1 0E 00 00 	.4byte 0xef1
 2759 0e9e 05          	.uleb128 0x5
 2760 0e9f 54 39 49 50 	.asciz "T9IP"
 2760      00 
 2761 0ea4 02          	.byte 0x2
 2762 0ea5 1D 28       	.2byte 0x281d
 2763 0ea7 ED 00 00 00 	.4byte 0xed
 2764 0eab 02          	.byte 0x2
 2765 0eac 03          	.byte 0x3
 2766 0ead 0D          	.byte 0xd
 2767 0eae 02          	.byte 0x2
 2768 0eaf 23          	.byte 0x23
 2769 0eb0 00          	.uleb128 0x0
 2770 0eb1 05          	.uleb128 0x5
 2771 0eb2 49 4E 54 33 	.asciz "INT3IP"
 2771      49 50 00 
 2772 0eb9 02          	.byte 0x2
 2773 0eba 1F 28       	.2byte 0x281f
 2774 0ebc ED 00 00 00 	.4byte 0xed
 2775 0ec0 02          	.byte 0x2
 2776 0ec1 03          	.byte 0x3
 2777 0ec2 09          	.byte 0x9
 2778 0ec3 02          	.byte 0x2
 2779 0ec4 23          	.byte 0x23
 2780 0ec5 00          	.uleb128 0x0
 2781 0ec6 05          	.uleb128 0x5
 2782 0ec7 49 4E 54 34 	.asciz "INT4IP"
 2782      49 50 00 
 2783 0ece 02          	.byte 0x2
 2784 0ecf 21 28       	.2byte 0x2821
 2785 0ed1 ED 00 00 00 	.4byte 0xed
 2786 0ed5 02          	.byte 0x2
 2787 0ed6 03          	.byte 0x3
 2788 0ed7 05          	.byte 0x5
 2789 0ed8 02          	.byte 0x2
 2790 0ed9 23          	.byte 0x23
 2791 0eda 00          	.uleb128 0x0
 2792 0edb 05          	.uleb128 0x5
 2793 0edc 43 32 52 58 	.asciz "C2RXIP"
 2793      49 50 00 
 2794 0ee3 02          	.byte 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 57


 2795 0ee4 23 28       	.2byte 0x2823
 2796 0ee6 ED 00 00 00 	.4byte 0xed
 2797 0eea 02          	.byte 0x2
 2798 0eeb 03          	.byte 0x3
 2799 0eec 01          	.byte 0x1
 2800 0eed 02          	.byte 0x2
 2801 0eee 23          	.byte 0x23
 2802 0eef 00          	.uleb128 0x0
 2803 0ef0 00          	.byte 0x0
 2804 0ef1 07          	.uleb128 0x7
 2805 0ef2 02          	.byte 0x2
 2806 0ef3 02          	.byte 0x2
 2807 0ef4 25 28       	.2byte 0x2825
 2808 0ef6 FD 0F 00 00 	.4byte 0xffd
 2809 0efa 05          	.uleb128 0x5
 2810 0efb 54 39 49 50 	.asciz "T9IP0"
 2810      30 00 
 2811 0f01 02          	.byte 0x2
 2812 0f02 26 28       	.2byte 0x2826
 2813 0f04 ED 00 00 00 	.4byte 0xed
 2814 0f08 02          	.byte 0x2
 2815 0f09 01          	.byte 0x1
 2816 0f0a 0F          	.byte 0xf
 2817 0f0b 02          	.byte 0x2
 2818 0f0c 23          	.byte 0x23
 2819 0f0d 00          	.uleb128 0x0
 2820 0f0e 05          	.uleb128 0x5
 2821 0f0f 54 39 49 50 	.asciz "T9IP1"
 2821      31 00 
 2822 0f15 02          	.byte 0x2
 2823 0f16 27 28       	.2byte 0x2827
 2824 0f18 ED 00 00 00 	.4byte 0xed
 2825 0f1c 02          	.byte 0x2
 2826 0f1d 01          	.byte 0x1
 2827 0f1e 0E          	.byte 0xe
 2828 0f1f 02          	.byte 0x2
 2829 0f20 23          	.byte 0x23
 2830 0f21 00          	.uleb128 0x0
 2831 0f22 05          	.uleb128 0x5
 2832 0f23 54 39 49 50 	.asciz "T9IP2"
 2832      32 00 
 2833 0f29 02          	.byte 0x2
 2834 0f2a 28 28       	.2byte 0x2828
 2835 0f2c ED 00 00 00 	.4byte 0xed
 2836 0f30 02          	.byte 0x2
 2837 0f31 01          	.byte 0x1
 2838 0f32 0D          	.byte 0xd
 2839 0f33 02          	.byte 0x2
 2840 0f34 23          	.byte 0x23
 2841 0f35 00          	.uleb128 0x0
 2842 0f36 05          	.uleb128 0x5
 2843 0f37 49 4E 54 33 	.asciz "INT3IP0"
 2843      49 50 30 00 
 2844 0f3f 02          	.byte 0x2
 2845 0f40 2A 28       	.2byte 0x282a
 2846 0f42 ED 00 00 00 	.4byte 0xed
 2847 0f46 02          	.byte 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 58


 2848 0f47 01          	.byte 0x1
 2849 0f48 0B          	.byte 0xb
 2850 0f49 02          	.byte 0x2
 2851 0f4a 23          	.byte 0x23
 2852 0f4b 00          	.uleb128 0x0
 2853 0f4c 05          	.uleb128 0x5
 2854 0f4d 49 4E 54 33 	.asciz "INT3IP1"
 2854      49 50 31 00 
 2855 0f55 02          	.byte 0x2
 2856 0f56 2B 28       	.2byte 0x282b
 2857 0f58 ED 00 00 00 	.4byte 0xed
 2858 0f5c 02          	.byte 0x2
 2859 0f5d 01          	.byte 0x1
 2860 0f5e 0A          	.byte 0xa
 2861 0f5f 02          	.byte 0x2
 2862 0f60 23          	.byte 0x23
 2863 0f61 00          	.uleb128 0x0
 2864 0f62 05          	.uleb128 0x5
 2865 0f63 49 4E 54 33 	.asciz "INT3IP2"
 2865      49 50 32 00 
 2866 0f6b 02          	.byte 0x2
 2867 0f6c 2C 28       	.2byte 0x282c
 2868 0f6e ED 00 00 00 	.4byte 0xed
 2869 0f72 02          	.byte 0x2
 2870 0f73 01          	.byte 0x1
 2871 0f74 09          	.byte 0x9
 2872 0f75 02          	.byte 0x2
 2873 0f76 23          	.byte 0x23
 2874 0f77 00          	.uleb128 0x0
 2875 0f78 05          	.uleb128 0x5
 2876 0f79 49 4E 54 34 	.asciz "INT4IP0"
 2876      49 50 30 00 
 2877 0f81 02          	.byte 0x2
 2878 0f82 2E 28       	.2byte 0x282e
 2879 0f84 ED 00 00 00 	.4byte 0xed
 2880 0f88 02          	.byte 0x2
 2881 0f89 01          	.byte 0x1
 2882 0f8a 07          	.byte 0x7
 2883 0f8b 02          	.byte 0x2
 2884 0f8c 23          	.byte 0x23
 2885 0f8d 00          	.uleb128 0x0
 2886 0f8e 05          	.uleb128 0x5
 2887 0f8f 49 4E 54 34 	.asciz "INT4IP1"
 2887      49 50 31 00 
 2888 0f97 02          	.byte 0x2
 2889 0f98 2F 28       	.2byte 0x282f
 2890 0f9a ED 00 00 00 	.4byte 0xed
 2891 0f9e 02          	.byte 0x2
 2892 0f9f 01          	.byte 0x1
 2893 0fa0 06          	.byte 0x6
 2894 0fa1 02          	.byte 0x2
 2895 0fa2 23          	.byte 0x23
 2896 0fa3 00          	.uleb128 0x0
 2897 0fa4 05          	.uleb128 0x5
 2898 0fa5 49 4E 54 34 	.asciz "INT4IP2"
 2898      49 50 32 00 
 2899 0fad 02          	.byte 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 59


 2900 0fae 30 28       	.2byte 0x2830
 2901 0fb0 ED 00 00 00 	.4byte 0xed
 2902 0fb4 02          	.byte 0x2
 2903 0fb5 01          	.byte 0x1
 2904 0fb6 05          	.byte 0x5
 2905 0fb7 02          	.byte 0x2
 2906 0fb8 23          	.byte 0x23
 2907 0fb9 00          	.uleb128 0x0
 2908 0fba 05          	.uleb128 0x5
 2909 0fbb 43 32 52 58 	.asciz "C2RXIP0"
 2909      49 50 30 00 
 2910 0fc3 02          	.byte 0x2
 2911 0fc4 32 28       	.2byte 0x2832
 2912 0fc6 ED 00 00 00 	.4byte 0xed
 2913 0fca 02          	.byte 0x2
 2914 0fcb 01          	.byte 0x1
 2915 0fcc 03          	.byte 0x3
 2916 0fcd 02          	.byte 0x2
 2917 0fce 23          	.byte 0x23
 2918 0fcf 00          	.uleb128 0x0
 2919 0fd0 05          	.uleb128 0x5
 2920 0fd1 43 32 52 58 	.asciz "C2RXIP1"
 2920      49 50 31 00 
 2921 0fd9 02          	.byte 0x2
 2922 0fda 33 28       	.2byte 0x2833
 2923 0fdc ED 00 00 00 	.4byte 0xed
 2924 0fe0 02          	.byte 0x2
 2925 0fe1 01          	.byte 0x1
 2926 0fe2 02          	.byte 0x2
 2927 0fe3 02          	.byte 0x2
 2928 0fe4 23          	.byte 0x23
 2929 0fe5 00          	.uleb128 0x0
 2930 0fe6 05          	.uleb128 0x5
 2931 0fe7 43 32 52 58 	.asciz "C2RXIP2"
 2931      49 50 32 00 
 2932 0fef 02          	.byte 0x2
 2933 0ff0 34 28       	.2byte 0x2834
 2934 0ff2 ED 00 00 00 	.4byte 0xed
 2935 0ff6 02          	.byte 0x2
 2936 0ff7 01          	.byte 0x1
 2937 0ff8 01          	.byte 0x1
 2938 0ff9 02          	.byte 0x2
 2939 0ffa 23          	.byte 0x23
 2940 0ffb 00          	.uleb128 0x0
 2941 0ffc 00          	.byte 0x0
 2942 0ffd 08          	.uleb128 0x8
 2943 0ffe 02          	.byte 0x2
 2944 0fff 02          	.byte 0x2
 2945 1000 1B 28       	.2byte 0x281b
 2946 1002 11 10 00 00 	.4byte 0x1011
 2947 1006 09          	.uleb128 0x9
 2948 1007 95 0E 00 00 	.4byte 0xe95
 2949 100b 09          	.uleb128 0x9
 2950 100c F1 0E 00 00 	.4byte 0xef1
 2951 1010 00          	.byte 0x0
 2952 1011 04          	.uleb128 0x4
 2953 1012 74 61 67 49 	.asciz "tagIPC13BITS"
MPLAB XC16 ASSEMBLY Listing:   			page 60


 2953      50 43 31 33 
 2953      42 49 54 53 
 2953      00 
 2954 101f 02          	.byte 0x2
 2955 1020 02          	.byte 0x2
 2956 1021 1A 28       	.2byte 0x281a
 2957 1023 30 10 00 00 	.4byte 0x1030
 2958 1027 0A          	.uleb128 0xa
 2959 1028 FD 0F 00 00 	.4byte 0xffd
 2960 102c 02          	.byte 0x2
 2961 102d 23          	.byte 0x23
 2962 102e 00          	.uleb128 0x0
 2963 102f 00          	.byte 0x0
 2964 1030 06          	.uleb128 0x6
 2965 1031 49 50 43 31 	.asciz "IPC13BITS"
 2965      33 42 49 54 
 2965      53 00 
 2966 103b 02          	.byte 0x2
 2967 103c 37 28       	.2byte 0x2837
 2968 103e 11 10 00 00 	.4byte 0x1011
 2969 1042 04          	.uleb128 0x4
 2970 1043 74 61 67 49 	.asciz "tagINTCON2BITS"
 2970      4E 54 43 4F 
 2970      4E 32 42 49 
 2970      54 53 00 
 2971 1052 02          	.byte 0x2
 2972 1053 02          	.byte 0x2
 2973 1054 50 2A       	.2byte 0x2a50
 2974 1056 FE 10 00 00 	.4byte 0x10fe
 2975 105a 05          	.uleb128 0x5
 2976 105b 49 4E 54 30 	.asciz "INT0EP"
 2976      45 50 00 
 2977 1062 02          	.byte 0x2
 2978 1063 51 2A       	.2byte 0x2a51
 2979 1065 ED 00 00 00 	.4byte 0xed
 2980 1069 02          	.byte 0x2
 2981 106a 01          	.byte 0x1
 2982 106b 0F          	.byte 0xf
 2983 106c 02          	.byte 0x2
 2984 106d 23          	.byte 0x23
 2985 106e 00          	.uleb128 0x0
 2986 106f 05          	.uleb128 0x5
 2987 1070 49 4E 54 31 	.asciz "INT1EP"
 2987      45 50 00 
 2988 1077 02          	.byte 0x2
 2989 1078 52 2A       	.2byte 0x2a52
 2990 107a ED 00 00 00 	.4byte 0xed
 2991 107e 02          	.byte 0x2
 2992 107f 01          	.byte 0x1
 2993 1080 0E          	.byte 0xe
 2994 1081 02          	.byte 0x2
 2995 1082 23          	.byte 0x23
 2996 1083 00          	.uleb128 0x0
 2997 1084 05          	.uleb128 0x5
 2998 1085 49 4E 54 32 	.asciz "INT2EP"
 2998      45 50 00 
 2999 108c 02          	.byte 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 61


 3000 108d 53 2A       	.2byte 0x2a53
 3001 108f ED 00 00 00 	.4byte 0xed
 3002 1093 02          	.byte 0x2
 3003 1094 01          	.byte 0x1
 3004 1095 0D          	.byte 0xd
 3005 1096 02          	.byte 0x2
 3006 1097 23          	.byte 0x23
 3007 1098 00          	.uleb128 0x0
 3008 1099 05          	.uleb128 0x5
 3009 109a 49 4E 54 33 	.asciz "INT3EP"
 3009      45 50 00 
 3010 10a1 02          	.byte 0x2
 3011 10a2 54 2A       	.2byte 0x2a54
 3012 10a4 ED 00 00 00 	.4byte 0xed
 3013 10a8 02          	.byte 0x2
 3014 10a9 01          	.byte 0x1
 3015 10aa 0C          	.byte 0xc
 3016 10ab 02          	.byte 0x2
 3017 10ac 23          	.byte 0x23
 3018 10ad 00          	.uleb128 0x0
 3019 10ae 05          	.uleb128 0x5
 3020 10af 49 4E 54 34 	.asciz "INT4EP"
 3020      45 50 00 
 3021 10b6 02          	.byte 0x2
 3022 10b7 55 2A       	.2byte 0x2a55
 3023 10b9 ED 00 00 00 	.4byte 0xed
 3024 10bd 02          	.byte 0x2
 3025 10be 01          	.byte 0x1
 3026 10bf 0B          	.byte 0xb
 3027 10c0 02          	.byte 0x2
 3028 10c1 23          	.byte 0x23
 3029 10c2 00          	.uleb128 0x0
 3030 10c3 05          	.uleb128 0x5
 3031 10c4 53 57 54 52 	.asciz "SWTRAP"
 3031      41 50 00 
 3032 10cb 02          	.byte 0x2
 3033 10cc 57 2A       	.2byte 0x2a57
 3034 10ce ED 00 00 00 	.4byte 0xed
 3035 10d2 02          	.byte 0x2
 3036 10d3 01          	.byte 0x1
 3037 10d4 02          	.byte 0x2
 3038 10d5 02          	.byte 0x2
 3039 10d6 23          	.byte 0x23
 3040 10d7 00          	.uleb128 0x0
 3041 10d8 05          	.uleb128 0x5
 3042 10d9 44 49 53 49 	.asciz "DISI"
 3042      00 
 3043 10de 02          	.byte 0x2
 3044 10df 58 2A       	.2byte 0x2a58
 3045 10e1 ED 00 00 00 	.4byte 0xed
 3046 10e5 02          	.byte 0x2
 3047 10e6 01          	.byte 0x1
 3048 10e7 01          	.byte 0x1
 3049 10e8 02          	.byte 0x2
 3050 10e9 23          	.byte 0x23
 3051 10ea 00          	.uleb128 0x0
 3052 10eb 05          	.uleb128 0x5
MPLAB XC16 ASSEMBLY Listing:   			page 62


 3053 10ec 47 49 45 00 	.asciz "GIE"
 3054 10f0 02          	.byte 0x2
 3055 10f1 59 2A       	.2byte 0x2a59
 3056 10f3 ED 00 00 00 	.4byte 0xed
 3057 10f7 02          	.byte 0x2
 3058 10f8 01          	.byte 0x1
 3059 10f9 10          	.byte 0x10
 3060 10fa 02          	.byte 0x2
 3061 10fb 23          	.byte 0x23
 3062 10fc 00          	.uleb128 0x0
 3063 10fd 00          	.byte 0x0
 3064 10fe 06          	.uleb128 0x6
 3065 10ff 49 4E 54 43 	.asciz "INTCON2BITS"
 3065      4F 4E 32 42 
 3065      49 54 53 00 
 3066 110b 02          	.byte 0x2
 3067 110c 5A 2A       	.2byte 0x2a5a
 3068 110e 42 10 00 00 	.4byte 0x1042
 3069 1112 0B          	.uleb128 0xb
 3070 1113 01          	.byte 0x1
 3071 1114 63 6F 6E 66 	.asciz "config_external0"
 3071      69 67 5F 65 
 3071      78 74 65 72 
 3071      6E 61 6C 30 
 3071      00 
 3072 1125 01          	.byte 0x1
 3073 1126 0D          	.byte 0xd
 3074 1127 01          	.byte 0x1
 3075 1128 00 00 00 00 	.4byte .LFB0
 3076 112c 00 00 00 00 	.4byte .LFE0
 3077 1130 01          	.byte 0x1
 3078 1131 5E          	.byte 0x5e
 3079 1132 53 11 00 00 	.4byte 0x1153
 3080 1136 0C          	.uleb128 0xc
 3081 1137 00 00 00 00 	.4byte .LASF0
 3082 113b 01          	.byte 0x1
 3083 113c 0D          	.byte 0xd
 3084 113d 53 11 00 00 	.4byte 0x1153
 3085 1141 02          	.byte 0x2
 3086 1142 7E          	.byte 0x7e
 3087 1143 00          	.sleb128 0
 3088 1144 0C          	.uleb128 0xc
 3089 1145 00 00 00 00 	.4byte .LASF1
 3090 1149 01          	.byte 0x1
 3091 114a 0D          	.byte 0xd
 3092 114b FD 00 00 00 	.4byte 0xfd
 3093 114f 02          	.byte 0x2
 3094 1150 7E          	.byte 0x7e
 3095 1151 02          	.sleb128 2
 3096 1152 00          	.byte 0x0
 3097 1153 02          	.uleb128 0x2
 3098 1154 01          	.byte 0x1
 3099 1155 02          	.byte 0x2
 3100 1156 5F 42 6F 6F 	.asciz "_Bool"
 3100      6C 00 
 3101 115c 0B          	.uleb128 0xb
 3102 115d 01          	.byte 0x1
MPLAB XC16 ASSEMBLY Listing:   			page 63


 3103 115e 63 6F 6E 66 	.asciz "config_external1"
 3103      69 67 5F 65 
 3103      78 74 65 72 
 3103      6E 61 6C 31 
 3103      00 
 3104 116f 01          	.byte 0x1
 3105 1170 1E          	.byte 0x1e
 3106 1171 01          	.byte 0x1
 3107 1172 00 00 00 00 	.4byte .LFB1
 3108 1176 00 00 00 00 	.4byte .LFE1
 3109 117a 01          	.byte 0x1
 3110 117b 5E          	.byte 0x5e
 3111 117c 9D 11 00 00 	.4byte 0x119d
 3112 1180 0C          	.uleb128 0xc
 3113 1181 00 00 00 00 	.4byte .LASF0
 3114 1185 01          	.byte 0x1
 3115 1186 1E          	.byte 0x1e
 3116 1187 53 11 00 00 	.4byte 0x1153
 3117 118b 02          	.byte 0x2
 3118 118c 7E          	.byte 0x7e
 3119 118d 00          	.sleb128 0
 3120 118e 0C          	.uleb128 0xc
 3121 118f 00 00 00 00 	.4byte .LASF1
 3122 1193 01          	.byte 0x1
 3123 1194 1E          	.byte 0x1e
 3124 1195 FD 00 00 00 	.4byte 0xfd
 3125 1199 02          	.byte 0x2
 3126 119a 7E          	.byte 0x7e
 3127 119b 02          	.sleb128 2
 3128 119c 00          	.byte 0x0
 3129 119d 0B          	.uleb128 0xb
 3130 119e 01          	.byte 0x1
 3131 119f 63 6F 6E 66 	.asciz "config_external2"
 3131      69 67 5F 65 
 3131      78 74 65 72 
 3131      6E 61 6C 32 
 3131      00 
 3132 11b0 01          	.byte 0x1
 3133 11b1 2F          	.byte 0x2f
 3134 11b2 01          	.byte 0x1
 3135 11b3 00 00 00 00 	.4byte .LFB2
 3136 11b7 00 00 00 00 	.4byte .LFE2
 3137 11bb 01          	.byte 0x1
 3138 11bc 5E          	.byte 0x5e
 3139 11bd DE 11 00 00 	.4byte 0x11de
 3140 11c1 0C          	.uleb128 0xc
 3141 11c2 00 00 00 00 	.4byte .LASF0
 3142 11c6 01          	.byte 0x1
 3143 11c7 2F          	.byte 0x2f
 3144 11c8 53 11 00 00 	.4byte 0x1153
 3145 11cc 02          	.byte 0x2
 3146 11cd 7E          	.byte 0x7e
 3147 11ce 00          	.sleb128 0
 3148 11cf 0C          	.uleb128 0xc
 3149 11d0 00 00 00 00 	.4byte .LASF1
 3150 11d4 01          	.byte 0x1
 3151 11d5 2F          	.byte 0x2f
MPLAB XC16 ASSEMBLY Listing:   			page 64


 3152 11d6 FD 00 00 00 	.4byte 0xfd
 3153 11da 02          	.byte 0x2
 3154 11db 7E          	.byte 0x7e
 3155 11dc 02          	.sleb128 2
 3156 11dd 00          	.byte 0x0
 3157 11de 0B          	.uleb128 0xb
 3158 11df 01          	.byte 0x1
 3159 11e0 63 6F 6E 66 	.asciz "config_external3"
 3159      69 67 5F 65 
 3159      78 74 65 72 
 3159      6E 61 6C 33 
 3159      00 
 3160 11f1 01          	.byte 0x1
 3161 11f2 3F          	.byte 0x3f
 3162 11f3 01          	.byte 0x1
 3163 11f4 00 00 00 00 	.4byte .LFB3
 3164 11f8 00 00 00 00 	.4byte .LFE3
 3165 11fc 01          	.byte 0x1
 3166 11fd 5E          	.byte 0x5e
 3167 11fe 1F 12 00 00 	.4byte 0x121f
 3168 1202 0C          	.uleb128 0xc
 3169 1203 00 00 00 00 	.4byte .LASF0
 3170 1207 01          	.byte 0x1
 3171 1208 3F          	.byte 0x3f
 3172 1209 53 11 00 00 	.4byte 0x1153
 3173 120d 02          	.byte 0x2
 3174 120e 7E          	.byte 0x7e
 3175 120f 00          	.sleb128 0
 3176 1210 0C          	.uleb128 0xc
 3177 1211 00 00 00 00 	.4byte .LASF1
 3178 1215 01          	.byte 0x1
 3179 1216 3F          	.byte 0x3f
 3180 1217 FD 00 00 00 	.4byte 0xfd
 3181 121b 02          	.byte 0x2
 3182 121c 7E          	.byte 0x7e
 3183 121d 02          	.sleb128 2
 3184 121e 00          	.byte 0x0
 3185 121f 0B          	.uleb128 0xb
 3186 1220 01          	.byte 0x1
 3187 1221 63 6F 6E 66 	.asciz "config_external4"
 3187      69 67 5F 65 
 3187      78 74 65 72 
 3187      6E 61 6C 34 
 3187      00 
 3188 1232 01          	.byte 0x1
 3189 1233 50          	.byte 0x50
 3190 1234 01          	.byte 0x1
 3191 1235 00 00 00 00 	.4byte .LFB4
 3192 1239 00 00 00 00 	.4byte .LFE4
 3193 123d 01          	.byte 0x1
 3194 123e 5E          	.byte 0x5e
 3195 123f 60 12 00 00 	.4byte 0x1260
 3196 1243 0C          	.uleb128 0xc
 3197 1244 00 00 00 00 	.4byte .LASF0
 3198 1248 01          	.byte 0x1
 3199 1249 50          	.byte 0x50
 3200 124a 53 11 00 00 	.4byte 0x1153
MPLAB XC16 ASSEMBLY Listing:   			page 65


 3201 124e 02          	.byte 0x2
 3202 124f 7E          	.byte 0x7e
 3203 1250 00          	.sleb128 0
 3204 1251 0C          	.uleb128 0xc
 3205 1252 00 00 00 00 	.4byte .LASF1
 3206 1256 01          	.byte 0x1
 3207 1257 50          	.byte 0x50
 3208 1258 FD 00 00 00 	.4byte 0xfd
 3209 125c 02          	.byte 0x2
 3210 125d 7E          	.byte 0x7e
 3211 125e 02          	.sleb128 2
 3212 125f 00          	.byte 0x0
 3213 1260 0D          	.uleb128 0xd
 3214 1261 01          	.byte 0x1
 3215 1262 65 78 74 65 	.asciz "external0_callback"
 3215      72 6E 61 6C 
 3215      30 5F 63 61 
 3215      6C 6C 62 61 
 3215      63 6B 00 
 3216 1275 01          	.byte 0x1
 3217 1276 5A          	.byte 0x5a
 3218 1277 01          	.byte 0x1
 3219 1278 00 00 00 00 	.4byte .LFB5
 3220 127c 00 00 00 00 	.4byte .LFE5
 3221 1280 01          	.byte 0x1
 3222 1281 5E          	.byte 0x5e
 3223 1282 0D          	.uleb128 0xd
 3224 1283 01          	.byte 0x1
 3225 1284 65 78 74 65 	.asciz "external1_callback"
 3225      72 6E 61 6C 
 3225      31 5F 63 61 
 3225      6C 6C 62 61 
 3225      63 6B 00 
 3226 1297 01          	.byte 0x1
 3227 1298 5E          	.byte 0x5e
 3228 1299 01          	.byte 0x1
 3229 129a 00 00 00 00 	.4byte .LFB6
 3230 129e 00 00 00 00 	.4byte .LFE6
 3231 12a2 01          	.byte 0x1
 3232 12a3 5E          	.byte 0x5e
 3233 12a4 0D          	.uleb128 0xd
 3234 12a5 01          	.byte 0x1
 3235 12a6 65 78 74 65 	.asciz "external2_callback"
 3235      72 6E 61 6C 
 3235      32 5F 63 61 
 3235      6C 6C 62 61 
 3235      63 6B 00 
 3236 12b9 01          	.byte 0x1
 3237 12ba 62          	.byte 0x62
 3238 12bb 01          	.byte 0x1
 3239 12bc 00 00 00 00 	.4byte .LFB7
 3240 12c0 00 00 00 00 	.4byte .LFE7
 3241 12c4 01          	.byte 0x1
 3242 12c5 5E          	.byte 0x5e
 3243 12c6 0D          	.uleb128 0xd
 3244 12c7 01          	.byte 0x1
 3245 12c8 65 78 74 65 	.asciz "external3_callback"
MPLAB XC16 ASSEMBLY Listing:   			page 66


 3245      72 6E 61 6C 
 3245      33 5F 63 61 
 3245      6C 6C 62 61 
 3245      63 6B 00 
 3246 12db 01          	.byte 0x1
 3247 12dc 66          	.byte 0x66
 3248 12dd 01          	.byte 0x1
 3249 12de 00 00 00 00 	.4byte .LFB8
 3250 12e2 00 00 00 00 	.4byte .LFE8
 3251 12e6 01          	.byte 0x1
 3252 12e7 5E          	.byte 0x5e
 3253 12e8 0D          	.uleb128 0xd
 3254 12e9 01          	.byte 0x1
 3255 12ea 65 78 74 65 	.asciz "external4_callback"
 3255      72 6E 61 6C 
 3255      34 5F 63 61 
 3255      6C 6C 62 61 
 3255      63 6B 00 
 3256 12fd 01          	.byte 0x1
 3257 12fe 6A          	.byte 0x6a
 3258 12ff 01          	.byte 0x1
 3259 1300 00 00 00 00 	.4byte .LFB9
 3260 1304 00 00 00 00 	.4byte .LFE9
 3261 1308 01          	.byte 0x1
 3262 1309 5E          	.byte 0x5e
 3263 130a 0D          	.uleb128 0xd
 3264 130b 01          	.byte 0x1
 3265 130c 5F 49 4E 54 	.asciz "_INT0Interrupt"
 3265      30 49 6E 74 
 3265      65 72 72 75 
 3265      70 74 00 
 3266 131b 01          	.byte 0x1
 3267 131c 71          	.byte 0x71
 3268 131d 01          	.byte 0x1
 3269 131e 00 00 00 00 	.4byte .LFB10
 3270 1322 00 00 00 00 	.4byte .LFE10
 3271 1326 01          	.byte 0x1
 3272 1327 5E          	.byte 0x5e
 3273 1328 0D          	.uleb128 0xd
 3274 1329 01          	.byte 0x1
 3275 132a 5F 49 4E 54 	.asciz "_INT1Interrupt"
 3275      31 49 6E 74 
 3275      65 72 72 75 
 3275      70 74 00 
 3276 1339 01          	.byte 0x1
 3277 133a 7B          	.byte 0x7b
 3278 133b 01          	.byte 0x1
 3279 133c 00 00 00 00 	.4byte .LFB11
 3280 1340 00 00 00 00 	.4byte .LFE11
 3281 1344 01          	.byte 0x1
 3282 1345 5E          	.byte 0x5e
 3283 1346 0D          	.uleb128 0xd
 3284 1347 01          	.byte 0x1
 3285 1348 5F 49 4E 54 	.asciz "_INT2Interrupt"
 3285      32 49 6E 74 
 3285      65 72 72 75 
 3285      70 74 00 
MPLAB XC16 ASSEMBLY Listing:   			page 67


 3286 1357 01          	.byte 0x1
 3287 1358 87          	.byte 0x87
 3288 1359 01          	.byte 0x1
 3289 135a 00 00 00 00 	.4byte .LFB12
 3290 135e 00 00 00 00 	.4byte .LFE12
 3291 1362 01          	.byte 0x1
 3292 1363 5E          	.byte 0x5e
 3293 1364 0D          	.uleb128 0xd
 3294 1365 01          	.byte 0x1
 3295 1366 5F 49 4E 54 	.asciz "_INT3Interrupt"
 3295      33 49 6E 74 
 3295      65 72 72 75 
 3295      70 74 00 
 3296 1375 01          	.byte 0x1
 3297 1376 92          	.byte 0x92
 3298 1377 01          	.byte 0x1
 3299 1378 00 00 00 00 	.4byte .LFB13
 3300 137c 00 00 00 00 	.4byte .LFE13
 3301 1380 01          	.byte 0x1
 3302 1381 5E          	.byte 0x5e
 3303 1382 0D          	.uleb128 0xd
 3304 1383 01          	.byte 0x1
 3305 1384 5F 49 4E 54 	.asciz "_INT4Interrupt"
 3305      34 49 6E 74 
 3305      65 72 72 75 
 3305      70 74 00 
 3306 1393 01          	.byte 0x1
 3307 1394 9D          	.byte 0x9d
 3308 1395 01          	.byte 0x1
 3309 1396 00 00 00 00 	.4byte .LFB14
 3310 139a 00 00 00 00 	.4byte .LFE14
 3311 139e 01          	.byte 0x1
 3312 139f 5E          	.byte 0x5e
 3313 13a0 0E          	.uleb128 0xe
 3314 13a1 00 00 00 00 	.4byte .LASF2
 3315 13a5 02          	.byte 0x2
 3316 13a6 1A 25       	.2byte 0x251a
 3317 13a8 AE 13 00 00 	.4byte 0x13ae
 3318 13ac 01          	.byte 0x1
 3319 13ad 01          	.byte 0x1
 3320 13ae 0F          	.uleb128 0xf
 3321 13af 97 02 00 00 	.4byte 0x297
 3322 13b3 0E          	.uleb128 0xe
 3323 13b4 00 00 00 00 	.4byte .LASF3
 3324 13b8 02          	.byte 0x2
 3325 13b9 30 25       	.2byte 0x2530
 3326 13bb C1 13 00 00 	.4byte 0x13c1
 3327 13bf 01          	.byte 0x1
 3328 13c0 01          	.byte 0x1
 3329 13c1 0F          	.uleb128 0xf
 3330 13c2 03 04 00 00 	.4byte 0x403
 3331 13c6 0E          	.uleb128 0xe
 3332 13c7 00 00 00 00 	.4byte .LASF4
 3333 13cb 02          	.byte 0x2
 3334 13cc 5B 25       	.2byte 0x255b
 3335 13ce D4 13 00 00 	.4byte 0x13d4
 3336 13d2 01          	.byte 0x1
MPLAB XC16 ASSEMBLY Listing:   			page 68


 3337 13d3 01          	.byte 0x1
 3338 13d4 0F          	.uleb128 0xf
 3339 13d5 5D 05 00 00 	.4byte 0x55d
 3340 13d9 0E          	.uleb128 0xe
 3341 13da 00 00 00 00 	.4byte .LASF5
 3342 13de 02          	.byte 0x2
 3343 13df C6 25       	.2byte 0x25c6
 3344 13e1 E7 13 00 00 	.4byte 0x13e7
 3345 13e5 01          	.byte 0x1
 3346 13e6 01          	.byte 0x1
 3347 13e7 0F          	.uleb128 0xf
 3348 13e8 C9 06 00 00 	.4byte 0x6c9
 3349 13ec 0E          	.uleb128 0xe
 3350 13ed 00 00 00 00 	.4byte .LASF6
 3351 13f1 02          	.byte 0x2
 3352 13f2 DC 25       	.2byte 0x25dc
 3353 13f4 FA 13 00 00 	.4byte 0x13fa
 3354 13f8 01          	.byte 0x1
 3355 13f9 01          	.byte 0x1
 3356 13fa 0F          	.uleb128 0xf
 3357 13fb 35 08 00 00 	.4byte 0x835
 3358 13ff 0E          	.uleb128 0xe
 3359 1400 00 00 00 00 	.4byte .LASF7
 3360 1404 02          	.byte 0x2
 3361 1405 07 26       	.2byte 0x2607
 3362 1407 0D 14 00 00 	.4byte 0x140d
 3363 140b 01          	.byte 0x1
 3364 140c 01          	.byte 0x1
 3365 140d 0F          	.uleb128 0xf
 3366 140e 8F 09 00 00 	.4byte 0x98f
 3367 1412 0E          	.uleb128 0xe
 3368 1413 00 00 00 00 	.4byte .LASF8
 3369 1417 02          	.byte 0x2
 3370 1418 7E 26       	.2byte 0x267e
 3371 141a 20 14 00 00 	.4byte 0x1420
 3372 141e 01          	.byte 0x1
 3373 141f 01          	.byte 0x1
 3374 1420 0F          	.uleb128 0xf
 3375 1421 32 0B 00 00 	.4byte 0xb32
 3376 1425 0E          	.uleb128 0xe
 3377 1426 00 00 00 00 	.4byte .LASF9
 3378 142a 02          	.byte 0x2
 3379 142b 28 27       	.2byte 0x2728
 3380 142d 33 14 00 00 	.4byte 0x1433
 3381 1431 01          	.byte 0x1
 3382 1432 01          	.byte 0x1
 3383 1433 0F          	.uleb128 0xf
 3384 1434 D9 0C 00 00 	.4byte 0xcd9
 3385 1438 0E          	.uleb128 0xe
 3386 1439 00 00 00 00 	.4byte .LASF10
 3387 143d 02          	.byte 0x2
 3388 143e 6C 27       	.2byte 0x276c
 3389 1440 46 14 00 00 	.4byte 0x1446
 3390 1444 01          	.byte 0x1
 3391 1445 01          	.byte 0x1
 3392 1446 0F          	.uleb128 0xf
 3393 1447 84 0E 00 00 	.4byte 0xe84
MPLAB XC16 ASSEMBLY Listing:   			page 69


 3394 144b 0E          	.uleb128 0xe
 3395 144c 00 00 00 00 	.4byte .LASF11
 3396 1450 02          	.byte 0x2
 3397 1451 38 28       	.2byte 0x2838
 3398 1453 59 14 00 00 	.4byte 0x1459
 3399 1457 01          	.byte 0x1
 3400 1458 01          	.byte 0x1
 3401 1459 0F          	.uleb128 0xf
 3402 145a 30 10 00 00 	.4byte 0x1030
 3403 145e 0E          	.uleb128 0xe
 3404 145f 00 00 00 00 	.4byte .LASF12
 3405 1463 02          	.byte 0x2
 3406 1464 5B 2A       	.2byte 0x2a5b
 3407 1466 6C 14 00 00 	.4byte 0x146c
 3408 146a 01          	.byte 0x1
 3409 146b 01          	.byte 0x1
 3410 146c 0F          	.uleb128 0xf
 3411 146d FE 10 00 00 	.4byte 0x10fe
 3412 1471 0E          	.uleb128 0xe
 3413 1472 00 00 00 00 	.4byte .LASF2
 3414 1476 02          	.byte 0x2
 3415 1477 1A 25       	.2byte 0x251a
 3416 1479 AE 13 00 00 	.4byte 0x13ae
 3417 147d 01          	.byte 0x1
 3418 147e 01          	.byte 0x1
 3419 147f 0E          	.uleb128 0xe
 3420 1480 00 00 00 00 	.4byte .LASF3
 3421 1484 02          	.byte 0x2
 3422 1485 30 25       	.2byte 0x2530
 3423 1487 C1 13 00 00 	.4byte 0x13c1
 3424 148b 01          	.byte 0x1
 3425 148c 01          	.byte 0x1
 3426 148d 0E          	.uleb128 0xe
 3427 148e 00 00 00 00 	.4byte .LASF4
 3428 1492 02          	.byte 0x2
 3429 1493 5B 25       	.2byte 0x255b
 3430 1495 D4 13 00 00 	.4byte 0x13d4
 3431 1499 01          	.byte 0x1
 3432 149a 01          	.byte 0x1
 3433 149b 0E          	.uleb128 0xe
 3434 149c 00 00 00 00 	.4byte .LASF5
 3435 14a0 02          	.byte 0x2
 3436 14a1 C6 25       	.2byte 0x25c6
 3437 14a3 E7 13 00 00 	.4byte 0x13e7
 3438 14a7 01          	.byte 0x1
 3439 14a8 01          	.byte 0x1
 3440 14a9 0E          	.uleb128 0xe
 3441 14aa 00 00 00 00 	.4byte .LASF6
 3442 14ae 02          	.byte 0x2
 3443 14af DC 25       	.2byte 0x25dc
 3444 14b1 FA 13 00 00 	.4byte 0x13fa
 3445 14b5 01          	.byte 0x1
 3446 14b6 01          	.byte 0x1
 3447 14b7 0E          	.uleb128 0xe
 3448 14b8 00 00 00 00 	.4byte .LASF7
 3449 14bc 02          	.byte 0x2
 3450 14bd 07 26       	.2byte 0x2607
MPLAB XC16 ASSEMBLY Listing:   			page 70


 3451 14bf 0D 14 00 00 	.4byte 0x140d
 3452 14c3 01          	.byte 0x1
 3453 14c4 01          	.byte 0x1
 3454 14c5 0E          	.uleb128 0xe
 3455 14c6 00 00 00 00 	.4byte .LASF8
 3456 14ca 02          	.byte 0x2
 3457 14cb 7E 26       	.2byte 0x267e
 3458 14cd 20 14 00 00 	.4byte 0x1420
 3459 14d1 01          	.byte 0x1
 3460 14d2 01          	.byte 0x1
 3461 14d3 0E          	.uleb128 0xe
 3462 14d4 00 00 00 00 	.4byte .LASF9
 3463 14d8 02          	.byte 0x2
 3464 14d9 28 27       	.2byte 0x2728
 3465 14db 33 14 00 00 	.4byte 0x1433
 3466 14df 01          	.byte 0x1
 3467 14e0 01          	.byte 0x1
 3468 14e1 0E          	.uleb128 0xe
 3469 14e2 00 00 00 00 	.4byte .LASF10
 3470 14e6 02          	.byte 0x2
 3471 14e7 6C 27       	.2byte 0x276c
 3472 14e9 46 14 00 00 	.4byte 0x1446
 3473 14ed 01          	.byte 0x1
 3474 14ee 01          	.byte 0x1
 3475 14ef 0E          	.uleb128 0xe
 3476 14f0 00 00 00 00 	.4byte .LASF11
 3477 14f4 02          	.byte 0x2
 3478 14f5 38 28       	.2byte 0x2838
 3479 14f7 59 14 00 00 	.4byte 0x1459
 3480 14fb 01          	.byte 0x1
 3481 14fc 01          	.byte 0x1
 3482 14fd 0E          	.uleb128 0xe
 3483 14fe 00 00 00 00 	.4byte .LASF12
 3484 1502 02          	.byte 0x2
 3485 1503 5B 2A       	.2byte 0x2a5b
 3486 1505 6C 14 00 00 	.4byte 0x146c
 3487 1509 01          	.byte 0x1
 3488 150a 01          	.byte 0x1
 3489 150b 00          	.byte 0x0
 3490                 	.section .debug_abbrev,info
 3491 0000 01          	.uleb128 0x1
 3492 0001 11          	.uleb128 0x11
 3493 0002 01          	.byte 0x1
 3494 0003 25          	.uleb128 0x25
 3495 0004 08          	.uleb128 0x8
 3496 0005 13          	.uleb128 0x13
 3497 0006 0B          	.uleb128 0xb
 3498 0007 03          	.uleb128 0x3
 3499 0008 08          	.uleb128 0x8
 3500 0009 1B          	.uleb128 0x1b
 3501 000a 08          	.uleb128 0x8
 3502 000b 11          	.uleb128 0x11
 3503 000c 01          	.uleb128 0x1
 3504 000d 12          	.uleb128 0x12
 3505 000e 01          	.uleb128 0x1
 3506 000f 10          	.uleb128 0x10
 3507 0010 06          	.uleb128 0x6
MPLAB XC16 ASSEMBLY Listing:   			page 71


 3508 0011 00          	.byte 0x0
 3509 0012 00          	.byte 0x0
 3510 0013 02          	.uleb128 0x2
 3511 0014 24          	.uleb128 0x24
 3512 0015 00          	.byte 0x0
 3513 0016 0B          	.uleb128 0xb
 3514 0017 0B          	.uleb128 0xb
 3515 0018 3E          	.uleb128 0x3e
 3516 0019 0B          	.uleb128 0xb
 3517 001a 03          	.uleb128 0x3
 3518 001b 08          	.uleb128 0x8
 3519 001c 00          	.byte 0x0
 3520 001d 00          	.byte 0x0
 3521 001e 03          	.uleb128 0x3
 3522 001f 16          	.uleb128 0x16
 3523 0020 00          	.byte 0x0
 3524 0021 03          	.uleb128 0x3
 3525 0022 08          	.uleb128 0x8
 3526 0023 3A          	.uleb128 0x3a
 3527 0024 0B          	.uleb128 0xb
 3528 0025 3B          	.uleb128 0x3b
 3529 0026 0B          	.uleb128 0xb
 3530 0027 49          	.uleb128 0x49
 3531 0028 13          	.uleb128 0x13
 3532 0029 00          	.byte 0x0
 3533 002a 00          	.byte 0x0
 3534 002b 04          	.uleb128 0x4
 3535 002c 13          	.uleb128 0x13
 3536 002d 01          	.byte 0x1
 3537 002e 03          	.uleb128 0x3
 3538 002f 08          	.uleb128 0x8
 3539 0030 0B          	.uleb128 0xb
 3540 0031 0B          	.uleb128 0xb
 3541 0032 3A          	.uleb128 0x3a
 3542 0033 0B          	.uleb128 0xb
 3543 0034 3B          	.uleb128 0x3b
 3544 0035 05          	.uleb128 0x5
 3545 0036 01          	.uleb128 0x1
 3546 0037 13          	.uleb128 0x13
 3547 0038 00          	.byte 0x0
 3548 0039 00          	.byte 0x0
 3549 003a 05          	.uleb128 0x5
 3550 003b 0D          	.uleb128 0xd
 3551 003c 00          	.byte 0x0
 3552 003d 03          	.uleb128 0x3
 3553 003e 08          	.uleb128 0x8
 3554 003f 3A          	.uleb128 0x3a
 3555 0040 0B          	.uleb128 0xb
 3556 0041 3B          	.uleb128 0x3b
 3557 0042 05          	.uleb128 0x5
 3558 0043 49          	.uleb128 0x49
 3559 0044 13          	.uleb128 0x13
 3560 0045 0B          	.uleb128 0xb
 3561 0046 0B          	.uleb128 0xb
 3562 0047 0D          	.uleb128 0xd
 3563 0048 0B          	.uleb128 0xb
 3564 0049 0C          	.uleb128 0xc
MPLAB XC16 ASSEMBLY Listing:   			page 72


 3565 004a 0B          	.uleb128 0xb
 3566 004b 38          	.uleb128 0x38
 3567 004c 0A          	.uleb128 0xa
 3568 004d 00          	.byte 0x0
 3569 004e 00          	.byte 0x0
 3570 004f 06          	.uleb128 0x6
 3571 0050 16          	.uleb128 0x16
 3572 0051 00          	.byte 0x0
 3573 0052 03          	.uleb128 0x3
 3574 0053 08          	.uleb128 0x8
 3575 0054 3A          	.uleb128 0x3a
 3576 0055 0B          	.uleb128 0xb
 3577 0056 3B          	.uleb128 0x3b
 3578 0057 05          	.uleb128 0x5
 3579 0058 49          	.uleb128 0x49
 3580 0059 13          	.uleb128 0x13
 3581 005a 00          	.byte 0x0
 3582 005b 00          	.byte 0x0
 3583 005c 07          	.uleb128 0x7
 3584 005d 13          	.uleb128 0x13
 3585 005e 01          	.byte 0x1
 3586 005f 0B          	.uleb128 0xb
 3587 0060 0B          	.uleb128 0xb
 3588 0061 3A          	.uleb128 0x3a
 3589 0062 0B          	.uleb128 0xb
 3590 0063 3B          	.uleb128 0x3b
 3591 0064 05          	.uleb128 0x5
 3592 0065 01          	.uleb128 0x1
 3593 0066 13          	.uleb128 0x13
 3594 0067 00          	.byte 0x0
 3595 0068 00          	.byte 0x0
 3596 0069 08          	.uleb128 0x8
 3597 006a 17          	.uleb128 0x17
 3598 006b 01          	.byte 0x1
 3599 006c 0B          	.uleb128 0xb
 3600 006d 0B          	.uleb128 0xb
 3601 006e 3A          	.uleb128 0x3a
 3602 006f 0B          	.uleb128 0xb
 3603 0070 3B          	.uleb128 0x3b
 3604 0071 05          	.uleb128 0x5
 3605 0072 01          	.uleb128 0x1
 3606 0073 13          	.uleb128 0x13
 3607 0074 00          	.byte 0x0
 3608 0075 00          	.byte 0x0
 3609 0076 09          	.uleb128 0x9
 3610 0077 0D          	.uleb128 0xd
 3611 0078 00          	.byte 0x0
 3612 0079 49          	.uleb128 0x49
 3613 007a 13          	.uleb128 0x13
 3614 007b 00          	.byte 0x0
 3615 007c 00          	.byte 0x0
 3616 007d 0A          	.uleb128 0xa
 3617 007e 0D          	.uleb128 0xd
 3618 007f 00          	.byte 0x0
 3619 0080 49          	.uleb128 0x49
 3620 0081 13          	.uleb128 0x13
 3621 0082 38          	.uleb128 0x38
MPLAB XC16 ASSEMBLY Listing:   			page 73


 3622 0083 0A          	.uleb128 0xa
 3623 0084 00          	.byte 0x0
 3624 0085 00          	.byte 0x0
 3625 0086 0B          	.uleb128 0xb
 3626 0087 2E          	.uleb128 0x2e
 3627 0088 01          	.byte 0x1
 3628 0089 3F          	.uleb128 0x3f
 3629 008a 0C          	.uleb128 0xc
 3630 008b 03          	.uleb128 0x3
 3631 008c 08          	.uleb128 0x8
 3632 008d 3A          	.uleb128 0x3a
 3633 008e 0B          	.uleb128 0xb
 3634 008f 3B          	.uleb128 0x3b
 3635 0090 0B          	.uleb128 0xb
 3636 0091 27          	.uleb128 0x27
 3637 0092 0C          	.uleb128 0xc
 3638 0093 11          	.uleb128 0x11
 3639 0094 01          	.uleb128 0x1
 3640 0095 12          	.uleb128 0x12
 3641 0096 01          	.uleb128 0x1
 3642 0097 40          	.uleb128 0x40
 3643 0098 0A          	.uleb128 0xa
 3644 0099 01          	.uleb128 0x1
 3645 009a 13          	.uleb128 0x13
 3646 009b 00          	.byte 0x0
 3647 009c 00          	.byte 0x0
 3648 009d 0C          	.uleb128 0xc
 3649 009e 05          	.uleb128 0x5
 3650 009f 00          	.byte 0x0
 3651 00a0 03          	.uleb128 0x3
 3652 00a1 0E          	.uleb128 0xe
 3653 00a2 3A          	.uleb128 0x3a
 3654 00a3 0B          	.uleb128 0xb
 3655 00a4 3B          	.uleb128 0x3b
 3656 00a5 0B          	.uleb128 0xb
 3657 00a6 49          	.uleb128 0x49
 3658 00a7 13          	.uleb128 0x13
 3659 00a8 02          	.uleb128 0x2
 3660 00a9 0A          	.uleb128 0xa
 3661 00aa 00          	.byte 0x0
 3662 00ab 00          	.byte 0x0
 3663 00ac 0D          	.uleb128 0xd
 3664 00ad 2E          	.uleb128 0x2e
 3665 00ae 00          	.byte 0x0
 3666 00af 3F          	.uleb128 0x3f
 3667 00b0 0C          	.uleb128 0xc
 3668 00b1 03          	.uleb128 0x3
 3669 00b2 08          	.uleb128 0x8
 3670 00b3 3A          	.uleb128 0x3a
 3671 00b4 0B          	.uleb128 0xb
 3672 00b5 3B          	.uleb128 0x3b
 3673 00b6 0B          	.uleb128 0xb
 3674 00b7 27          	.uleb128 0x27
 3675 00b8 0C          	.uleb128 0xc
 3676 00b9 11          	.uleb128 0x11
 3677 00ba 01          	.uleb128 0x1
 3678 00bb 12          	.uleb128 0x12
MPLAB XC16 ASSEMBLY Listing:   			page 74


 3679 00bc 01          	.uleb128 0x1
 3680 00bd 40          	.uleb128 0x40
 3681 00be 0A          	.uleb128 0xa
 3682 00bf 00          	.byte 0x0
 3683 00c0 00          	.byte 0x0
 3684 00c1 0E          	.uleb128 0xe
 3685 00c2 34          	.uleb128 0x34
 3686 00c3 00          	.byte 0x0
 3687 00c4 03          	.uleb128 0x3
 3688 00c5 0E          	.uleb128 0xe
 3689 00c6 3A          	.uleb128 0x3a
 3690 00c7 0B          	.uleb128 0xb
 3691 00c8 3B          	.uleb128 0x3b
 3692 00c9 05          	.uleb128 0x5
 3693 00ca 49          	.uleb128 0x49
 3694 00cb 13          	.uleb128 0x13
 3695 00cc 3F          	.uleb128 0x3f
 3696 00cd 0C          	.uleb128 0xc
 3697 00ce 3C          	.uleb128 0x3c
 3698 00cf 0C          	.uleb128 0xc
 3699 00d0 00          	.byte 0x0
 3700 00d1 00          	.byte 0x0
 3701 00d2 0F          	.uleb128 0xf
 3702 00d3 35          	.uleb128 0x35
 3703 00d4 00          	.byte 0x0
 3704 00d5 49          	.uleb128 0x49
 3705 00d6 13          	.uleb128 0x13
 3706 00d7 00          	.byte 0x0
 3707 00d8 00          	.byte 0x0
 3708 00d9 00          	.byte 0x0
 3709                 	.section .debug_pubnames,info
 3710 0000 49 01 00 00 	.4byte 0x149
 3711 0004 02 00       	.2byte 0x2
 3712 0006 00 00 00 00 	.4byte .Ldebug_info0
 3713 000a 0C 15 00 00 	.4byte 0x150c
 3714 000e 12 11 00 00 	.4byte 0x1112
 3715 0012 63 6F 6E 66 	.asciz "config_external0"
 3715      69 67 5F 65 
 3715      78 74 65 72 
 3715      6E 61 6C 30 
 3715      00 
 3716 0023 5C 11 00 00 	.4byte 0x115c
 3717 0027 63 6F 6E 66 	.asciz "config_external1"
 3717      69 67 5F 65 
 3717      78 74 65 72 
 3717      6E 61 6C 31 
 3717      00 
 3718 0038 9D 11 00 00 	.4byte 0x119d
 3719 003c 63 6F 6E 66 	.asciz "config_external2"
 3719      69 67 5F 65 
 3719      78 74 65 72 
 3719      6E 61 6C 32 
 3719      00 
 3720 004d DE 11 00 00 	.4byte 0x11de
 3721 0051 63 6F 6E 66 	.asciz "config_external3"
 3721      69 67 5F 65 
 3721      78 74 65 72 
MPLAB XC16 ASSEMBLY Listing:   			page 75


 3721      6E 61 6C 33 
 3721      00 
 3722 0062 1F 12 00 00 	.4byte 0x121f
 3723 0066 63 6F 6E 66 	.asciz "config_external4"
 3723      69 67 5F 65 
 3723      78 74 65 72 
 3723      6E 61 6C 34 
 3723      00 
 3724 0077 60 12 00 00 	.4byte 0x1260
 3725 007b 65 78 74 65 	.asciz "external0_callback"
 3725      72 6E 61 6C 
 3725      30 5F 63 61 
 3725      6C 6C 62 61 
 3725      63 6B 00 
 3726 008e 82 12 00 00 	.4byte 0x1282
 3727 0092 65 78 74 65 	.asciz "external1_callback"
 3727      72 6E 61 6C 
 3727      31 5F 63 61 
 3727      6C 6C 62 61 
 3727      63 6B 00 
 3728 00a5 A4 12 00 00 	.4byte 0x12a4
 3729 00a9 65 78 74 65 	.asciz "external2_callback"
 3729      72 6E 61 6C 
 3729      32 5F 63 61 
 3729      6C 6C 62 61 
 3729      63 6B 00 
 3730 00bc C6 12 00 00 	.4byte 0x12c6
 3731 00c0 65 78 74 65 	.asciz "external3_callback"
 3731      72 6E 61 6C 
 3731      33 5F 63 61 
 3731      6C 6C 62 61 
 3731      63 6B 00 
 3732 00d3 E8 12 00 00 	.4byte 0x12e8
 3733 00d7 65 78 74 65 	.asciz "external4_callback"
 3733      72 6E 61 6C 
 3733      34 5F 63 61 
 3733      6C 6C 62 61 
 3733      63 6B 00 
 3734 00ea 0A 13 00 00 	.4byte 0x130a
 3735 00ee 5F 49 4E 54 	.asciz "_INT0Interrupt"
 3735      30 49 6E 74 
 3735      65 72 72 75 
 3735      70 74 00 
 3736 00fd 28 13 00 00 	.4byte 0x1328
 3737 0101 5F 49 4E 54 	.asciz "_INT1Interrupt"
 3737      31 49 6E 74 
 3737      65 72 72 75 
 3737      70 74 00 
 3738 0110 46 13 00 00 	.4byte 0x1346
 3739 0114 5F 49 4E 54 	.asciz "_INT2Interrupt"
 3739      32 49 6E 74 
 3739      65 72 72 75 
 3739      70 74 00 
 3740 0123 64 13 00 00 	.4byte 0x1364
 3741 0127 5F 49 4E 54 	.asciz "_INT3Interrupt"
 3741      33 49 6E 74 
 3741      65 72 72 75 
MPLAB XC16 ASSEMBLY Listing:   			page 76


 3741      70 74 00 
 3742 0136 82 13 00 00 	.4byte 0x1382
 3743 013a 5F 49 4E 54 	.asciz "_INT4Interrupt"
 3743      34 49 6E 74 
 3743      65 72 72 75 
 3743      70 74 00 
 3744 0149 00 00 00 00 	.4byte 0x0
 3745                 	.section .debug_pubtypes,info
 3746 0000 62 01 00 00 	.4byte 0x162
 3747 0004 02 00       	.2byte 0x2
 3748 0006 00 00 00 00 	.4byte .Ldebug_info0
 3749 000a 0C 15 00 00 	.4byte 0x150c
 3750 000e ED 00 00 00 	.4byte 0xed
 3751 0012 75 69 6E 74 	.asciz "uint16_t"
 3751      31 36 5F 74 
 3751      00 
 3752 001b 3C 01 00 00 	.4byte 0x13c
 3753 001f 74 61 67 49 	.asciz "tagIFS0BITS"
 3753      46 53 30 42 
 3753      49 54 53 00 
 3754 002b 97 02 00 00 	.4byte 0x297
 3755 002f 49 46 53 30 	.asciz "IFS0BITS"
 3755      42 49 54 53 
 3755      00 
 3756 0038 A8 02 00 00 	.4byte 0x2a8
 3757 003c 74 61 67 49 	.asciz "tagIFS1BITS"
 3757      46 53 31 42 
 3757      49 54 53 00 
 3758 0048 03 04 00 00 	.4byte 0x403
 3759 004c 49 46 53 31 	.asciz "IFS1BITS"
 3759      42 49 54 53 
 3759      00 
 3760 0055 14 04 00 00 	.4byte 0x414
 3761 0059 74 61 67 49 	.asciz "tagIFS3BITS"
 3761      46 53 33 42 
 3761      49 54 53 00 
 3762 0065 5D 05 00 00 	.4byte 0x55d
 3763 0069 49 46 53 33 	.asciz "IFS3BITS"
 3763      42 49 54 53 
 3763      00 
 3764 0072 6E 05 00 00 	.4byte 0x56e
 3765 0076 74 61 67 49 	.asciz "tagIEC0BITS"
 3765      45 43 30 42 
 3765      49 54 53 00 
 3766 0082 C9 06 00 00 	.4byte 0x6c9
 3767 0086 49 45 43 30 	.asciz "IEC0BITS"
 3767      42 49 54 53 
 3767      00 
 3768 008f DA 06 00 00 	.4byte 0x6da
 3769 0093 74 61 67 49 	.asciz "tagIEC1BITS"
 3769      45 43 31 42 
 3769      49 54 53 00 
 3770 009f 35 08 00 00 	.4byte 0x835
 3771 00a3 49 45 43 31 	.asciz "IEC1BITS"
 3771      42 49 54 53 
 3771      00 
 3772 00ac 46 08 00 00 	.4byte 0x846
MPLAB XC16 ASSEMBLY Listing:   			page 77


 3773 00b0 74 61 67 49 	.asciz "tagIEC3BITS"
 3773      45 43 33 42 
 3773      49 54 53 00 
 3774 00bc 8F 09 00 00 	.4byte 0x98f
 3775 00c0 49 45 43 33 	.asciz "IEC3BITS"
 3775      42 49 54 53 
 3775      00 
 3776 00c9 14 0B 00 00 	.4byte 0xb14
 3777 00cd 74 61 67 49 	.asciz "tagIPC0BITS"
 3777      50 43 30 42 
 3777      49 54 53 00 
 3778 00d9 32 0B 00 00 	.4byte 0xb32
 3779 00dd 49 50 43 30 	.asciz "IPC0BITS"
 3779      42 49 54 53 
 3779      00 
 3780 00e6 BB 0C 00 00 	.4byte 0xcbb
 3781 00ea 74 61 67 49 	.asciz "tagIPC5BITS"
 3781      50 43 35 42 
 3781      49 54 53 00 
 3782 00f6 D9 0C 00 00 	.4byte 0xcd9
 3783 00fa 49 50 43 35 	.asciz "IPC5BITS"
 3783      42 49 54 53 
 3783      00 
 3784 0103 66 0E 00 00 	.4byte 0xe66
 3785 0107 74 61 67 49 	.asciz "tagIPC7BITS"
 3785      50 43 37 42 
 3785      49 54 53 00 
 3786 0113 84 0E 00 00 	.4byte 0xe84
 3787 0117 49 50 43 37 	.asciz "IPC7BITS"
 3787      42 49 54 53 
 3787      00 
 3788 0120 11 10 00 00 	.4byte 0x1011
 3789 0124 74 61 67 49 	.asciz "tagIPC13BITS"
 3789      50 43 31 33 
 3789      42 49 54 53 
 3789      00 
 3790 0131 30 10 00 00 	.4byte 0x1030
 3791 0135 49 50 43 31 	.asciz "IPC13BITS"
 3791      33 42 49 54 
 3791      53 00 
 3792 013f 42 10 00 00 	.4byte 0x1042
 3793 0143 74 61 67 49 	.asciz "tagINTCON2BITS"
 3793      4E 54 43 4F 
 3793      4E 32 42 49 
 3793      54 53 00 
 3794 0152 FE 10 00 00 	.4byte 0x10fe
 3795 0156 49 4E 54 43 	.asciz "INTCON2BITS"
 3795      4F 4E 32 42 
 3795      49 54 53 00 
 3796 0162 00 00 00 00 	.4byte 0x0
 3797                 	.section .debug_aranges,info
 3798 0000 14 00 00 00 	.4byte 0x14
 3799 0004 02 00       	.2byte 0x2
 3800 0006 00 00 00 00 	.4byte .Ldebug_info0
 3801 000a 04          	.byte 0x4
 3802 000b 00          	.byte 0x0
 3803 000c 00 00       	.2byte 0x0
MPLAB XC16 ASSEMBLY Listing:   			page 78


 3804 000e 00 00       	.2byte 0x0
 3805 0010 00 00 00 00 	.4byte 0x0
 3806 0014 00 00 00 00 	.4byte 0x0
 3807                 	.section .debug_str,info
 3808                 	.LASF4:
 3809 0000 49 46 53 33 	.asciz "IFS3bits"
 3809      62 69 74 73 
 3809      00 
 3810                 	.LASF6:
 3811 0009 49 45 43 31 	.asciz "IEC1bits"
 3811      62 69 74 73 
 3811      00 
 3812                 	.LASF9:
 3813 0012 49 50 43 35 	.asciz "IPC5bits"
 3813      62 69 74 73 
 3813      00 
 3814                 	.LASF0:
 3815 001b 70 6F 6C 61 	.asciz "polarity"
 3815      72 69 74 79 
 3815      00 
 3816                 	.LASF8:
 3817 0024 49 50 43 30 	.asciz "IPC0bits"
 3817      62 69 74 73 
 3817      00 
 3818                 	.LASF7:
 3819 002d 49 45 43 33 	.asciz "IEC3bits"
 3819      62 69 74 73 
 3819      00 
 3820                 	.LASF1:
 3821 0036 70 72 69 6F 	.asciz "priority"
 3821      72 69 74 79 
 3821      00 
 3822                 	.LASF10:
 3823 003f 49 50 43 37 	.asciz "IPC7bits"
 3823      62 69 74 73 
 3823      00 
 3824                 	.LASF2:
 3825 0048 49 46 53 30 	.asciz "IFS0bits"
 3825      62 69 74 73 
 3825      00 
 3826                 	.LASF3:
 3827 0051 49 46 53 31 	.asciz "IFS1bits"
 3827      62 69 74 73 
 3827      00 
 3828                 	.LASF12:
 3829 005a 49 4E 54 43 	.asciz "INTCON2bits"
 3829      4F 4E 32 62 
 3829      69 74 73 00 
 3830                 	.LASF5:
 3831 0066 49 45 43 30 	.asciz "IEC0bits"
 3831      62 69 74 73 
 3831      00 
 3832                 	.LASF11:
 3833 006f 49 50 43 31 	.asciz "IPC13bits"
 3833      33 62 69 74 
 3833      73 00 
 3834                 	.section .text,code
MPLAB XC16 ASSEMBLY Listing:   			page 79


 3835              	
 3836              	
 3837              	
 3838              	.section __c30_info,info,bss
 3839                 	__psv_trap_errata:
 3840                 	
 3841                 	.section __c30_signature,info,data
 3842 0000 01 00       	.word 0x0001
 3843 0002 00 00       	.word 0x0000
 3844 0004 00 00       	.word 0x0000
 3845                 	
 3846                 	
 3847                 	
 3848                 	.set ___PA___,0
 3849                 	.end
MPLAB XC16 ASSEMBLY Listing:   			page 80


DEFINED SYMBOLS
                            *ABS*:00000000 /home/user/Documents/FST/Programming/project_sub-zero/lib/lib_pic33e/external.c
                            *ABS*:00000001 __33EP256MU806
                            *ABS*:00000001 __dsPIC33E
                            *ABS*:00000001 __HAS_DSP
                            *ABS*:00000001 __HAS_DMAV2
                            *ABS*:00000001 __HAS_CODEGUARD
                            *ABS*:00000001 __HAS_PMP_ENHANCED
                            *ABS*:00000001 __HAS_EDS
                            *ABS*:00000001 __HAS_EP
                            *ABS*:00000011 __TARGET_DIVIDE_CYCLES
                            *ABS*:00000001 __C30ELF
    {standard input}:14     .text:00000000 _config_external0
    {standard input}:18     *ABS*:00000000 ___PA___
    {standard input}:63     .text:00000034 _config_external1
    {standard input}:115    .text:0000006a _config_external2
    {standard input}:168    .text:000000a2 _config_external3
    {standard input}:221    .text:000000da _config_external4
    {standard input}:274    .text:00000112 _external0_callback
    {standard input}:291    .text:0000011c _external1_callback
    {standard input}:308    .text:00000126 _external2_callback
    {standard input}:325    .text:00000130 _external3_callback
    {standard input}:342    .text:0000013a _external4_callback
                            *ABS*:00000040 __ext_attr_.isr.text
    {standard input}:360    .isr.text:00000000 __INT0Interrupt
    {standard input}:413    .isr.text:00000038 __INT1Interrupt
    {standard input}:466    .isr.text:00000070 __INT2Interrupt
    {standard input}:519    .isr.text:000000a8 __INT3Interrupt
    {standard input}:572    .isr.text:000000e0 __INT4Interrupt
    {standard input}:3839   __c30_info:00000000 __psv_trap_errata
    {standard input}:366    .isr.text:00000000 .L0
    {standard input}:19     .text:00000000 .L0
                    .debug_abbrev:00000000 .Ldebug_abbrev0
                            .text:00000000 .Ltext0
                            .text:00000144 .Letext0
                      .debug_line:00000000 .Ldebug_line0
                            .text:00000000 .LFB0
                            .text:00000034 .LFE0
                       .debug_str:0000001b .LASF0
                       .debug_str:00000036 .LASF1
                            .text:00000034 .LFB1
                            .text:0000006a .LFE1
                            .text:0000006a .LFB2
                            .text:000000a2 .LFE2
                            .text:000000a2 .LFB3
                            .text:000000da .LFE3
                            .text:000000da .LFB4
                            .text:00000112 .LFE4
                            .text:00000112 .LFB5
                            .text:0000011c .LFE5
                            .text:0000011c .LFB6
                            .text:00000126 .LFE6
                            .text:00000126 .LFB7
                            .text:00000130 .LFE7
                            .text:00000130 .LFB8
                            .text:0000013a .LFE8
                            .text:0000013a .LFB9
MPLAB XC16 ASSEMBLY Listing:   			page 81


                            .text:00000144 .LFE9
                        .isr.text:00000000 .LFB10
                        .isr.text:00000038 .LFE10
                        .isr.text:00000038 .LFB11
                        .isr.text:00000070 .LFE11
                        .isr.text:00000070 .LFB12
                        .isr.text:000000a8 .LFE12
                        .isr.text:000000a8 .LFB13
                        .isr.text:000000e0 .LFE13
                        .isr.text:000000e0 .LFB14
                        .isr.text:00000118 .LFE14
                       .debug_str:00000048 .LASF2
                       .debug_str:00000051 .LASF3
                       .debug_str:00000000 .LASF4
                       .debug_str:00000066 .LASF5
                       .debug_str:00000009 .LASF6
                       .debug_str:0000002d .LASF7
                       .debug_str:00000024 .LASF8
                       .debug_str:00000012 .LASF9
                       .debug_str:0000003f .LASF10
                       .debug_str:0000006f .LASF11
                       .debug_str:0000005a .LASF12
                     .debug_frame:00000000 .Lframe0
                      .debug_info:00000000 .Ldebug_info0

UNDEFINED SYMBOLS
_INTCON2bits
_IFS0bits
_IPC0bits
_IEC0bits
CORCON
_IFS1bits
_IEC1bits
_IPC5bits
_IPC7bits
_IFS3bits
_IEC3bits
_IPC13bits
_RCOUNT
_DSRPAG
_DSWPAG
__const_psvpage
_SR

EQUATE SYMBOLS
/home/user/Documents/FST/Programming/project_sub-zero/lib/lib_pic33e/external.c = 0x0
                  __33EP256MU806 = 0x1
                      __dsPIC33E = 0x1
                       __HAS_DSP = 0x1
                     __HAS_DMAV2 = 0x1
                 __HAS_CODEGUARD = 0x1
              __HAS_PMP_ENHANCED = 0x1
                       __HAS_EDS = 0x1
                        __HAS_EP = 0x1
          __TARGET_DIVIDE_CYCLES = 0x11
                        __C30ELF = 0x1
                        ___PA___ = 0x0
            __ext_attr_.isr.text = 0x40
