MPLAB XC16 ASSEMBLY Listing:   			page 1


   1              	.file "/home/user/Documents/FST/Programming/project_sub-zero/lib/lib_pic33e/version.c"
   2              	.section .debug_abbrev,info
   3                 	.Ldebug_abbrev0:
   4                 	.section .debug_info,info
   5                 	.Ldebug_info0:
   6                 	.section .debug_line,info
   7                 	.Ldebug_line0:
   8 0000 A4 00 00 00 	.section .text,code
   8      02 00 7C 00 
   8      00 00 01 01 
   8      FB 0E 0A 00 
   8      01 01 01 01 
   8      00 00 00 01 
   8      6C 69 62 2F 
   8      6C 69 62 5F 
   8      70 69 63 33 
   9              	.Ltext0:
  10              	.section .text,code
  11              	.align 2
  12              	.global _get_version
  13              	.type _get_version,@function
  14              	_get_version:
  15              	.LFB0:
  16              	.file 1 "lib/lib_pic33e/version.c"
   1:lib/lib_pic33e/version.c **** #include <xc.h>
   2:lib/lib_pic33e/version.c **** #include <stdint.h>
   3:lib/lib_pic33e/version.c **** #include <stdlib.h>
   4:lib/lib_pic33e/version.c **** #include "version.h"
   5:lib/lib_pic33e/version.c **** 
   6:lib/lib_pic33e/version.c **** /**********************************************************************
   7:lib/lib_pic33e/version.c ****  * Name:    get_version
   8:lib/lib_pic33e/version.c ****  * Args:    -
   9:lib/lib_pic33e/version.c ****  * Return:  -
  10:lib/lib_pic33e/version.c ****  * Desc:    Obtains your current code version from your commit log.
  11:lib/lib_pic33e/version.c ****  **********************************************************************/
  12:lib/lib_pic33e/version.c **** 
  13:lib/lib_pic33e/version.c **** VERSIONerror get_version(uint64_t version, uint16_t *data){
  17              	.loc 1 13 0
  18              	.set ___PA___,1
  19 000000  0A 00 FA 	lnk #10
  20              	.LCFI0:
  21 000002  88 1F 78 	mov w8,[w15++]
  22              	.LCFI1:
  23              	.loc 1 13 0
  24 000004  00 9F BE 	mov.d w0,[w14++]
  25 000006  02 97 BE 	mov.d w2,[w14--]
  26 000008  44 07 98 	mov w4,[w14+8]
  14:lib/lib_pic33e/version.c **** 	// version is defined in the Makefile as VERSION.
  15:lib/lib_pic33e/version.c ****     
  16:lib/lib_pic33e/version.c **** 	if (data == NULL)
  27              	.loc 1 16 0
  28 00000a  4E 00 90 	mov [w14+8],w0
  29 00000c  00 00 E0 	cp0 w0
  30              	.set ___BP___,0
  31 00000e  00 00 3A 	bra nz,.L2
  17:lib/lib_pic33e/version.c **** 		return VEMPTY;
  32              	.loc 1 17 0
MPLAB XC16 ASSEMBLY Listing:   			page 2


  33 000010  00 02 EB 	clr w4
  34 000012  00 00 37 	bra .L3
  35              	.L2:
  18:lib/lib_pic33e/version.c **** 
  19:lib/lib_pic33e/version.c ****     data[3] = (version & 0xF) + ((version) & 0xF0);
  36              	.loc 1 19 0
  37 000014  4E 00 90 	mov [w14+8],w0
  38 000016  1E 01 78 	mov [w14],w2
  39 000018  66 00 40 	add w0,#6,w0
  40 00001a  F1 0F 20 	mov #255,w1
  20:lib/lib_pic33e/version.c **** 	data[2] = ((version >> 8) & 0xF) + ((version >> 8 & 0xF0));
  41              	.loc 1 20 0
  42 00001c  F8 0F 20 	mov #255,w8
  43              	.loc 1 19 0
  44 00001e  81 00 61 	and w2,w1,w1
  21:lib/lib_pic33e/version.c **** 	data[1] = ((version >> 16) & 0xF) + ((version >> 16) & 0xF0);
  45              	.loc 1 21 0
  46 000020  F7 0F 20 	mov #255,w7
  47              	.loc 1 19 0
  48 000022  01 08 78 	mov w1,[w0]
  22:lib/lib_pic33e/version.c **** 	data[0] = ((version >> 24) & 0xF) + ((version >> 24) & 0xF0);
  49              	.loc 1 22 0
  50 000024  F6 0F 20 	mov #255,w6
  51              	.loc 1 20 0
  52 000026  4E 02 90 	mov [w14+8],w4
  53 000028  1E 00 78 	mov [w14],w0
  54 00002a  9E 00 90 	mov [w14+2],w1
  55 00002c  2E 01 90 	mov [w14+4],w2
  56 00002e  BE 01 90 	mov [w14+6],w3
  57 000030  E4 02 42 	add w4,#4,w5
  58              	.set ___BP___,0
  59 000032  48 00 DE 	lsr w0,#8,w0
  60 000034  48 0A DD 	sl w1,#8,w4
  61 000036  00 00 72 	ior w4,w0,w0
  23:lib/lib_pic33e/version.c **** 
  24:lib/lib_pic33e/version.c **** 	return VSUCCESS;
  62              	.loc 1 24 0
  63 000038  14 00 20 	mov #1,w4
  64              	.loc 1 20 0
  65 00003a  00 00 78 	mov w0,w0
  66 00003c  08 00 60 	and w0,w8,w0
  67 00003e  80 0A 78 	mov w0,[w5]
  68              	.loc 1 21 0
  69 000040  CE 02 90 	mov [w14+8],w5
  70 000042  1E 00 78 	mov [w14],w0
  71 000044  9E 00 90 	mov [w14+2],w1
  72 000046  2E 01 90 	mov [w14+4],w2
  73 000048  BE 01 90 	mov [w14+6],w3
  74 00004a  85 82 E8 	inc2 w5,w5
  75              	.set ___BP___,0
  76 00004c  01 00 78 	mov w1,w0
  77 00004e  00 00 78 	mov w0,w0
  78 000050  07 00 60 	and w0,w7,w0
  79 000052  80 0A 78 	mov w0,[w5]
  80              	.loc 1 22 0
  81 000054  1E 00 78 	mov [w14],w0
  82 000056  9E 00 90 	mov [w14+2],w1
MPLAB XC16 ASSEMBLY Listing:   			page 3


  83 000058  2E 01 90 	mov [w14+4],w2
  84 00005a  BE 01 90 	mov [w14+6],w3
  85 00005c  CE 02 90 	mov [w14+8],w5
  86              	.set ___BP___,0
  87 00005e  48 08 DE 	lsr w1,#8,w0
  88 000060  C8 13 DD 	sl w2,#8,w7
  89 000062  00 80 73 	ior w7,w0,w0
  90 000064  00 00 78 	mov w0,w0
  91 000066  06 00 60 	and w0,w6,w0
  92 000068  80 0A 78 	mov w0,[w5]
  93              	.L3:
  94              	.loc 1 24 0
  95 00006a  04 00 78 	mov w4,w0
  25:lib/lib_pic33e/version.c **** }
  96              	.loc 1 25 0
  97 00006c  4F 04 78 	mov [--w15],w8
  98 00006e  8E 07 78 	mov w14,w15
  99 000070  4F 07 78 	mov [--w15],w14
 100 000072  00 40 A9 	bclr CORCON,#2
 101 000074  00 00 06 	return 
 102              	.set ___PA___,0
 103              	.LFE0:
 104              	.size _get_version,.-_get_version
 105              	.section .debug_frame,info
 106                 	.Lframe0:
 107 0000 10 00 00 00 	.4byte .LECIE0-.LSCIE0
 108                 	.LSCIE0:
 109 0004 FF FF FF FF 	.4byte 0xffffffff
 110 0008 01          	.byte 0x1
 111 0009 00          	.byte 0
 112 000a 01          	.uleb128 0x1
 113 000b 02          	.sleb128 2
 114 000c 25          	.byte 0x25
 115 000d 12          	.byte 0x12
 116 000e 0F          	.uleb128 0xf
 117 000f 7E          	.sleb128 -2
 118 0010 09          	.byte 0x9
 119 0011 25          	.uleb128 0x25
 120 0012 0F          	.uleb128 0xf
 121 0013 00          	.align 4
 122                 	.LECIE0:
 123                 	.LSFDE0:
 124 0014 1E 00 00 00 	.4byte .LEFDE0-.LASFDE0
 125                 	.LASFDE0:
 126 0018 00 00 00 00 	.4byte .Lframe0
 127 001c 00 00 00 00 	.4byte .LFB0
 128 0020 76 00 00 00 	.4byte .LFE0-.LFB0
 129 0024 04          	.byte 0x4
 130 0025 02 00 00 00 	.4byte .LCFI0-.LFB0
 131 0029 13          	.byte 0x13
 132 002a 7D          	.sleb128 -3
 133 002b 0D          	.byte 0xd
 134 002c 0E          	.uleb128 0xe
 135 002d 8E          	.byte 0x8e
 136 002e 02          	.uleb128 0x2
 137 002f 04          	.byte 0x4
 138 0030 02 00 00 00 	.4byte .LCFI1-.LCFI0
MPLAB XC16 ASSEMBLY Listing:   			page 4


 139 0034 88          	.byte 0x88
 140 0035 08          	.uleb128 0x8
 141                 	.align 4
 142                 	.LEFDE0:
 143                 	.section .text,code
 144              	.Letext0:
 145              	.file 2 "/opt/microchip/xc16/v1.36/bin/bin/../../include/lega-c/stdint.h"
 146              	.file 3 "lib/lib_pic33e/version.h"
 147              	.section .debug_info,info
 148 0000 DA 01 00 00 	.4byte 0x1da
 149 0004 02 00       	.2byte 0x2
 150 0006 00 00 00 00 	.4byte .Ldebug_abbrev0
 151 000a 04          	.byte 0x4
 152 000b 01          	.uleb128 0x1
 153 000c 47 4E 55 20 	.asciz "GNU C 4.5.1 (XC16, Microchip v1.36) (A) Build date: Dec  5 2018"
 153      43 20 34 2E 
 153      35 2E 31 20 
 153      28 58 43 31 
 153      36 2C 20 4D 
 153      69 63 72 6F 
 153      63 68 69 70 
 153      20 76 31 2E 
 153      33 36 29 20 
 154 004c 01          	.byte 0x1
 155 004d 6C 69 62 2F 	.asciz "lib/lib_pic33e/version.c"
 155      6C 69 62 5F 
 155      70 69 63 33 
 155      33 65 2F 76 
 155      65 72 73 69 
 155      6F 6E 2E 63 
 155      00 
 156 0066 2F 68 6F 6D 	.asciz "/home/user/Documents/FST/Programming/project_sub-zero"
 156      65 2F 75 73 
 156      65 72 2F 44 
 156      6F 63 75 6D 
 156      65 6E 74 73 
 156      2F 46 53 54 
 156      2F 50 72 6F 
 156      67 72 61 6D 
 156      6D 69 6E 67 
 157 009c 00 00 00 00 	.4byte .Ltext0
 158 00a0 00 00 00 00 	.4byte .Letext0
 159 00a4 00 00 00 00 	.4byte .Ldebug_line0
 160 00a8 02          	.uleb128 0x2
 161 00a9 01          	.byte 0x1
 162 00aa 06          	.byte 0x6
 163 00ab 73 69 67 6E 	.asciz "signed char"
 163      65 64 20 63 
 163      68 61 72 00 
 164 00b7 02          	.uleb128 0x2
 165 00b8 02          	.byte 0x2
 166 00b9 05          	.byte 0x5
 167 00ba 69 6E 74 00 	.asciz "int"
 168 00be 02          	.uleb128 0x2
 169 00bf 04          	.byte 0x4
 170 00c0 05          	.byte 0x5
 171 00c1 6C 6F 6E 67 	.asciz "long int"
MPLAB XC16 ASSEMBLY Listing:   			page 5


 171      20 69 6E 74 
 171      00 
 172 00ca 02          	.uleb128 0x2
 173 00cb 08          	.byte 0x8
 174 00cc 05          	.byte 0x5
 175 00cd 6C 6F 6E 67 	.asciz "long long int"
 175      20 6C 6F 6E 
 175      67 20 69 6E 
 175      74 00 
 176 00db 02          	.uleb128 0x2
 177 00dc 01          	.byte 0x1
 178 00dd 08          	.byte 0x8
 179 00de 75 6E 73 69 	.asciz "unsigned char"
 179      67 6E 65 64 
 179      20 63 68 61 
 179      72 00 
 180 00ec 03          	.uleb128 0x3
 181 00ed 75 69 6E 74 	.asciz "uint16_t"
 181      31 36 5F 74 
 181      00 
 182 00f6 02          	.byte 0x2
 183 00f7 31          	.byte 0x31
 184 00f8 FC 00 00 00 	.4byte 0xfc
 185 00fc 02          	.uleb128 0x2
 186 00fd 02          	.byte 0x2
 187 00fe 07          	.byte 0x7
 188 00ff 75 6E 73 69 	.asciz "unsigned int"
 188      67 6E 65 64 
 188      20 69 6E 74 
 188      00 
 189 010c 02          	.uleb128 0x2
 190 010d 04          	.byte 0x4
 191 010e 07          	.byte 0x7
 192 010f 6C 6F 6E 67 	.asciz "long unsigned int"
 192      20 75 6E 73 
 192      69 67 6E 65 
 192      64 20 69 6E 
 192      74 00 
 193 0121 03          	.uleb128 0x3
 194 0122 75 69 6E 74 	.asciz "uint64_t"
 194      36 34 5F 74 
 194      00 
 195 012b 02          	.byte 0x2
 196 012c 3D          	.byte 0x3d
 197 012d 31 01 00 00 	.4byte 0x131
 198 0131 02          	.uleb128 0x2
 199 0132 08          	.byte 0x8
 200 0133 07          	.byte 0x7
 201 0134 6C 6F 6E 67 	.asciz "long long unsigned int"
 201      20 6C 6F 6E 
 201      67 20 75 6E 
 201      73 69 67 6E 
 201      65 64 20 69 
 201      6E 74 00 
 202 014b 02          	.uleb128 0x2
 203 014c 02          	.byte 0x2
 204 014d 07          	.byte 0x7
MPLAB XC16 ASSEMBLY Listing:   			page 6


 205 014e 73 68 6F 72 	.asciz "short unsigned int"
 205      74 20 75 6E 
 205      73 69 67 6E 
 205      65 64 20 69 
 205      6E 74 00 
 206 0161 04          	.uleb128 0x4
 207 0162 02          	.byte 0x2
 208 0163 03          	.byte 0x3
 209 0164 1B          	.byte 0x1b
 210 0165 7E 01 00 00 	.4byte 0x17e
 211 0169 05          	.uleb128 0x5
 212 016a 56 45 4D 50 	.asciz "VEMPTY"
 212      54 59 00 
 213 0171 00          	.sleb128 0
 214 0172 05          	.uleb128 0x5
 215 0173 56 53 55 43 	.asciz "VSUCCESS"
 215      43 45 53 53 
 215      00 
 216 017c 01          	.sleb128 1
 217 017d 00          	.byte 0x0
 218 017e 03          	.uleb128 0x3
 219 017f 56 45 52 53 	.asciz "VERSIONerror"
 219      49 4F 4E 65 
 219      72 72 6F 72 
 219      00 
 220 018c 03          	.byte 0x3
 221 018d 1E          	.byte 0x1e
 222 018e 61 01 00 00 	.4byte 0x161
 223 0192 06          	.uleb128 0x6
 224 0193 01          	.byte 0x1
 225 0194 67 65 74 5F 	.asciz "get_version"
 225      76 65 72 73 
 225      69 6F 6E 00 
 226 01a0 01          	.byte 0x1
 227 01a1 0D          	.byte 0xd
 228 01a2 01          	.byte 0x1
 229 01a3 7E 01 00 00 	.4byte 0x17e
 230 01a7 00 00 00 00 	.4byte .LFB0
 231 01ab 00 00 00 00 	.4byte .LFE0
 232 01af 01          	.byte 0x1
 233 01b0 5E          	.byte 0x5e
 234 01b1 D7 01 00 00 	.4byte 0x1d7
 235 01b5 07          	.uleb128 0x7
 236 01b6 76 65 72 73 	.asciz "version"
 236      69 6F 6E 00 
 237 01be 01          	.byte 0x1
 238 01bf 0D          	.byte 0xd
 239 01c0 21 01 00 00 	.4byte 0x121
 240 01c4 02          	.byte 0x2
 241 01c5 7E          	.byte 0x7e
 242 01c6 00          	.sleb128 0
 243 01c7 07          	.uleb128 0x7
 244 01c8 64 61 74 61 	.asciz "data"
 244      00 
 245 01cd 01          	.byte 0x1
 246 01ce 0D          	.byte 0xd
 247 01cf D7 01 00 00 	.4byte 0x1d7
MPLAB XC16 ASSEMBLY Listing:   			page 7


 248 01d3 02          	.byte 0x2
 249 01d4 7E          	.byte 0x7e
 250 01d5 08          	.sleb128 8
 251 01d6 00          	.byte 0x0
 252 01d7 08          	.uleb128 0x8
 253 01d8 02          	.byte 0x2
 254 01d9 EC 00 00 00 	.4byte 0xec
 255 01dd 00          	.byte 0x0
 256                 	.section .debug_abbrev,info
 257 0000 01          	.uleb128 0x1
 258 0001 11          	.uleb128 0x11
 259 0002 01          	.byte 0x1
 260 0003 25          	.uleb128 0x25
 261 0004 08          	.uleb128 0x8
 262 0005 13          	.uleb128 0x13
 263 0006 0B          	.uleb128 0xb
 264 0007 03          	.uleb128 0x3
 265 0008 08          	.uleb128 0x8
 266 0009 1B          	.uleb128 0x1b
 267 000a 08          	.uleb128 0x8
 268 000b 11          	.uleb128 0x11
 269 000c 01          	.uleb128 0x1
 270 000d 12          	.uleb128 0x12
 271 000e 01          	.uleb128 0x1
 272 000f 10          	.uleb128 0x10
 273 0010 06          	.uleb128 0x6
 274 0011 00          	.byte 0x0
 275 0012 00          	.byte 0x0
 276 0013 02          	.uleb128 0x2
 277 0014 24          	.uleb128 0x24
 278 0015 00          	.byte 0x0
 279 0016 0B          	.uleb128 0xb
 280 0017 0B          	.uleb128 0xb
 281 0018 3E          	.uleb128 0x3e
 282 0019 0B          	.uleb128 0xb
 283 001a 03          	.uleb128 0x3
 284 001b 08          	.uleb128 0x8
 285 001c 00          	.byte 0x0
 286 001d 00          	.byte 0x0
 287 001e 03          	.uleb128 0x3
 288 001f 16          	.uleb128 0x16
 289 0020 00          	.byte 0x0
 290 0021 03          	.uleb128 0x3
 291 0022 08          	.uleb128 0x8
 292 0023 3A          	.uleb128 0x3a
 293 0024 0B          	.uleb128 0xb
 294 0025 3B          	.uleb128 0x3b
 295 0026 0B          	.uleb128 0xb
 296 0027 49          	.uleb128 0x49
 297 0028 13          	.uleb128 0x13
 298 0029 00          	.byte 0x0
 299 002a 00          	.byte 0x0
 300 002b 04          	.uleb128 0x4
 301 002c 04          	.uleb128 0x4
 302 002d 01          	.byte 0x1
 303 002e 0B          	.uleb128 0xb
 304 002f 0B          	.uleb128 0xb
MPLAB XC16 ASSEMBLY Listing:   			page 8


 305 0030 3A          	.uleb128 0x3a
 306 0031 0B          	.uleb128 0xb
 307 0032 3B          	.uleb128 0x3b
 308 0033 0B          	.uleb128 0xb
 309 0034 01          	.uleb128 0x1
 310 0035 13          	.uleb128 0x13
 311 0036 00          	.byte 0x0
 312 0037 00          	.byte 0x0
 313 0038 05          	.uleb128 0x5
 314 0039 28          	.uleb128 0x28
 315 003a 00          	.byte 0x0
 316 003b 03          	.uleb128 0x3
 317 003c 08          	.uleb128 0x8
 318 003d 1C          	.uleb128 0x1c
 319 003e 0D          	.uleb128 0xd
 320 003f 00          	.byte 0x0
 321 0040 00          	.byte 0x0
 322 0041 06          	.uleb128 0x6
 323 0042 2E          	.uleb128 0x2e
 324 0043 01          	.byte 0x1
 325 0044 3F          	.uleb128 0x3f
 326 0045 0C          	.uleb128 0xc
 327 0046 03          	.uleb128 0x3
 328 0047 08          	.uleb128 0x8
 329 0048 3A          	.uleb128 0x3a
 330 0049 0B          	.uleb128 0xb
 331 004a 3B          	.uleb128 0x3b
 332 004b 0B          	.uleb128 0xb
 333 004c 27          	.uleb128 0x27
 334 004d 0C          	.uleb128 0xc
 335 004e 49          	.uleb128 0x49
 336 004f 13          	.uleb128 0x13
 337 0050 11          	.uleb128 0x11
 338 0051 01          	.uleb128 0x1
 339 0052 12          	.uleb128 0x12
 340 0053 01          	.uleb128 0x1
 341 0054 40          	.uleb128 0x40
 342 0055 0A          	.uleb128 0xa
 343 0056 01          	.uleb128 0x1
 344 0057 13          	.uleb128 0x13
 345 0058 00          	.byte 0x0
 346 0059 00          	.byte 0x0
 347 005a 07          	.uleb128 0x7
 348 005b 05          	.uleb128 0x5
 349 005c 00          	.byte 0x0
 350 005d 03          	.uleb128 0x3
 351 005e 08          	.uleb128 0x8
 352 005f 3A          	.uleb128 0x3a
 353 0060 0B          	.uleb128 0xb
 354 0061 3B          	.uleb128 0x3b
 355 0062 0B          	.uleb128 0xb
 356 0063 49          	.uleb128 0x49
 357 0064 13          	.uleb128 0x13
 358 0065 02          	.uleb128 0x2
 359 0066 0A          	.uleb128 0xa
 360 0067 00          	.byte 0x0
 361 0068 00          	.byte 0x0
MPLAB XC16 ASSEMBLY Listing:   			page 9


 362 0069 08          	.uleb128 0x8
 363 006a 0F          	.uleb128 0xf
 364 006b 00          	.byte 0x0
 365 006c 0B          	.uleb128 0xb
 366 006d 0B          	.uleb128 0xb
 367 006e 49          	.uleb128 0x49
 368 006f 13          	.uleb128 0x13
 369 0070 00          	.byte 0x0
 370 0071 00          	.byte 0x0
 371 0072 00          	.byte 0x0
 372                 	.section .debug_pubnames,info
 373 0000 1E 00 00 00 	.4byte 0x1e
 374 0004 02 00       	.2byte 0x2
 375 0006 00 00 00 00 	.4byte .Ldebug_info0
 376 000a DE 01 00 00 	.4byte 0x1de
 377 000e 92 01 00 00 	.4byte 0x192
 378 0012 67 65 74 5F 	.asciz "get_version"
 378      76 65 72 73 
 378      69 6F 6E 00 
 379 001e 00 00 00 00 	.4byte 0x0
 380                 	.section .debug_pubtypes,info
 381 0000 39 00 00 00 	.4byte 0x39
 382 0004 02 00       	.2byte 0x2
 383 0006 00 00 00 00 	.4byte .Ldebug_info0
 384 000a DE 01 00 00 	.4byte 0x1de
 385 000e EC 00 00 00 	.4byte 0xec
 386 0012 75 69 6E 74 	.asciz "uint16_t"
 386      31 36 5F 74 
 386      00 
 387 001b 21 01 00 00 	.4byte 0x121
 388 001f 75 69 6E 74 	.asciz "uint64_t"
 388      36 34 5F 74 
 388      00 
 389 0028 7E 01 00 00 	.4byte 0x17e
 390 002c 56 45 52 53 	.asciz "VERSIONerror"
 390      49 4F 4E 65 
 390      72 72 6F 72 
 390      00 
 391 0039 00 00 00 00 	.4byte 0x0
 392                 	.section .debug_aranges,info
 393 0000 14 00 00 00 	.4byte 0x14
 394 0004 02 00       	.2byte 0x2
 395 0006 00 00 00 00 	.4byte .Ldebug_info0
 396 000a 04          	.byte 0x4
 397 000b 00          	.byte 0x0
 398 000c 00 00       	.2byte 0x0
 399 000e 00 00       	.2byte 0x0
 400 0010 00 00 00 00 	.4byte 0x0
 401 0014 00 00 00 00 	.4byte 0x0
 402                 	.section .debug_str,info
 403                 	.section .text,code
 404              	
 405              	
 406              	
 407              	.section __c30_info,info,bss
 408                 	__psv_trap_errata:
 409                 	
MPLAB XC16 ASSEMBLY Listing:   			page 10


 410                 	.section __c30_signature,info,data
 411 0000 01 00       	.word 0x0001
 412 0002 00 00       	.word 0x0000
 413 0004 00 00       	.word 0x0000
 414                 	
 415                 	
 416                 	
 417                 	.set ___PA___,0
 418                 	.end
MPLAB XC16 ASSEMBLY Listing:   			page 11


DEFINED SYMBOLS
                            *ABS*:00000000 /home/user/Documents/FST/Programming/project_sub-zero/lib/lib_pic33e/version.c
                            *ABS*:00000001 __33EP256MU806
                            *ABS*:00000001 __dsPIC33E
                            *ABS*:00000001 __HAS_DSP
                            *ABS*:00000001 __HAS_DMAV2
                            *ABS*:00000001 __HAS_CODEGUARD
                            *ABS*:00000001 __HAS_PMP_ENHANCED
                            *ABS*:00000001 __HAS_EDS
                            *ABS*:00000001 __HAS_EP
                            *ABS*:00000011 __TARGET_DIVIDE_CYCLES
                            *ABS*:00000001 __C30ELF
    {standard input}:14     .text:00000000 _get_version
    {standard input}:18     *ABS*:00000000 ___PA___
    {standard input}:30     *ABS*:00000000 ___BP___
    {standard input}:408    __c30_info:00000000 __psv_trap_errata
    {standard input}:19     .text:00000000 .L0
                            .text:00000014 .L2
                            .text:0000006a .L3
                    .debug_abbrev:00000000 .Ldebug_abbrev0
                            .text:00000000 .Ltext0
                            .text:00000076 .Letext0
                      .debug_line:00000000 .Ldebug_line0
                            .text:00000000 .LFB0
                            .text:00000076 .LFE0
                     .debug_frame:00000000 .Lframe0
                      .debug_info:00000000 .Ldebug_info0

UNDEFINED SYMBOLS
CORCON

EQUATE SYMBOLS
/home/user/Documents/FST/Programming/project_sub-zero/lib/lib_pic33e/version.c = 0x0
                  __33EP256MU806 = 0x1
                      __dsPIC33E = 0x1
                       __HAS_DSP = 0x1
                     __HAS_DMAV2 = 0x1
                 __HAS_CODEGUARD = 0x1
              __HAS_PMP_ENHANCED = 0x1
                       __HAS_EDS = 0x1
                        __HAS_EP = 0x1
          __TARGET_DIVIDE_CYCLES = 0x11
                        __C30ELF = 0x1
                        ___PA___ = 0x0
                        ___BP___ = 0x0
