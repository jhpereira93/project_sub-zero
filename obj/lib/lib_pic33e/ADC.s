MPLAB XC16 ASSEMBLY Listing:   			page 1


   1              	.file "/home/user/Documents/FST/Programming/project_sub-zero/lib/lib_pic33e/ADC.c"
   2              	.section .debug_abbrev,info
   3                 	.Ldebug_abbrev0:
   4                 	.section .debug_info,info
   5                 	.Ldebug_info0:
   6                 	.section .debug_line,info
   7                 	.Ldebug_line0:
   8 0000 31 02 00 00 	.section .text,code
   8      02 00 C2 00 
   8      00 00 01 01 
   8      FB 0E 0A 00 
   8      01 01 01 01 
   8      00 00 00 01 
   8      6C 69 62 2F 
   8      6C 69 62 5F 
   8      70 69 63 33 
   9              	.Ltext0:
  10              	.section .nbss,bss,near
  11                 	.align 2
  12                 	.type _size_pin_cycling_vector,@object
  13                 	.size _size_pin_cycling_vector,2
  14                 	_size_pin_cycling_vector:
  15 0000 00 00       	.skip 2
  16                 	.section .text,code
  17              	.align 2
  18              	.global _config_adc1
  19              	.type _config_adc1,@function
  20              	_config_adc1:
  21              	.LFB0:
  22              	.file 1 "lib/lib_pic33e/ADC.c"
   1:lib/lib_pic33e/ADC.c **** #ifndef NOADC
   2:lib/lib_pic33e/ADC.c **** 
   3:lib/lib_pic33e/ADC.c **** #include <xc.h>
   4:lib/lib_pic33e/ADC.c **** #include <stdlib.h>
   5:lib/lib_pic33e/ADC.c **** #include "ADC.h"
   6:lib/lib_pic33e/ADC.c **** #include "utils.h"
   7:lib/lib_pic33e/ADC.c **** 
   8:lib/lib_pic33e/ADC.c **** static uint16_t size_pin_cycling_vector=0;
   9:lib/lib_pic33e/ADC.c **** 
  10:lib/lib_pic33e/ADC.c **** void config_adc1 (ADC_parameters parameter){
  23              	.loc 1 10 0
  24              	.set ___PA___,1
  25 000000  0A 00 FA 	lnk #10
  26              	.LCFI0:
  27 000002  88 9F BE 	mov.d w8,[w15++]
  28              	.LCFI1:
  29 000004  8A 9F BE 	mov.d w10,[w15++]
  30              	.LCFI2:
  31              	.loc 1 10 0
  32 000006  00 0F 78 	mov w0,[w14]
  33 000008  22 07 98 	mov w2,[w14+4]
  11:lib/lib_pic33e/ADC.c **** 
  12:lib/lib_pic33e/ADC.c ****     AD1CON1bits.ADSIDL  = ~parameter.idle;
  34              	.loc 1 12 0
  35 00000a  09 00 80 	mov _AD1CON1bits,w9
  36 00000c  9E 05 78 	mov [w14],w11
  13:lib/lib_pic33e/ADC.c ****     AD1CON1bits.AD12B   = 1;
MPLAB XC16 ASSEMBLY Listing:   			page 2


  14:lib/lib_pic33e/ADC.c ****     AD1CON1bits.FORM    = parameter.form;
  37              	.loc 1 14 0
  38 00000e  9E 03 78 	mov [w14],w7
  15:lib/lib_pic33e/ADC.c ****     AD1CON1bits.SSRCG   = 0;
  16:lib/lib_pic33e/ADC.c ****     AD1CON1bits.SSRC    = parameter.conversion_trigger;
  39              	.loc 1 16 0
  40 000010  1E 05 78 	mov [w14],w10
  17:lib/lib_pic33e/ADC.c ****     AD1CON1bits.ASAM    = parameter.sampling_type;
  41              	.loc 1 17 0
  42 000012  1E 04 78 	mov [w14],w8
  18:lib/lib_pic33e/ADC.c **** 
  19:lib/lib_pic33e/ADC.c ****     AD1CON2bits.VCFG    = parameter.voltage_ref;
  43              	.loc 1 19 0
  44 000014  1E 01 78 	mov [w14],w2
  20:lib/lib_pic33e/ADC.c ****     AD1CON2bits.CSCNA   = parameter.sample_pin_select_type;
  45              	.loc 1 20 0
  46 000016  1E 03 78 	mov [w14],w6
  21:lib/lib_pic33e/ADC.c ****     AD1CON2bits.SMPI    = parameter.smpi;
  47              	.loc 1 21 0
  48 000018  9E 02 78 	mov [w14],w5
  22:lib/lib_pic33e/ADC.c ****     AD1CON2bits.BUFM    = 0;                                    //Always starts filling the buffer 
  23:lib/lib_pic33e/ADC.c ****     AD1CON2bits.ALTS    = 0;                                    /* always use MUX A                
  24:lib/lib_pic33e/ADC.c **** 
  25:lib/lib_pic33e/ADC.c ****     AD1CON3bits.SAMC    = parameter.auto_sample_time;           //Auto-Sample Time (31 is MAX)
  49              	.loc 1 25 0
  50 00001a  2E 00 90 	mov [w14+4],w0
  51              	.loc 1 10 0
  52 00001c  11 07 98 	mov w1,[w14+2]
  53 00001e  33 07 98 	mov w3,[w14+6]
  54 000020  44 07 98 	mov w4,[w14+8]
  55              	.loc 1 12 0
  56 000022  E1 C0 65 	and.b w11,#1,w1
  57 000024  09 D0 A1 	bclr w9,#13
  58 000026  81 41 78 	mov.b w1,w3
  59              	.loc 1 14 0
  60 000028  87 00 D1 	lsr w7,w1
  61              	.loc 1 12 0
  62 00002a  83 C1 EA 	com.b w3,w3
  63              	.loc 1 14 0
  64 00002c  E3 C0 60 	and.b w1,#3,w1
  65 00002e  81 80 FB 	ze w1,w1
  66              	.loc 1 12 0
  67 000030  E1 C1 61 	and.b w3,#1,w3
  68              	.loc 1 14 0
  69 000032  E3 80 60 	and w1,#3,w1
  70              	.loc 1 12 0
  71 000034  83 81 FB 	ze w3,w3
  72              	.loc 1 14 0
  73 000036  C8 08 DD 	sl w1,#8,w1
  74              	.loc 1 12 0
  75 000038  E1 81 61 	and w3,#1,w3
  76              	.loc 1 14 0
  77 00003a  F4 CF 2F 	mov #-769,w4
  78              	.loc 1 12 0
  79 00003c  CD 1B DD 	sl w3,#13,w7
  80              	.loc 1 16 0
  81 00003e  C3 51 DE 	lsr w10,#3,w3
MPLAB XC16 ASSEMBLY Listing:   			page 3


  82              	.loc 1 12 0
  83 000040  89 83 73 	ior w7,w9,w7
  84              	.loc 1 16 0
  85 000042  E7 C1 61 	and.b w3,#7,w3
  86              	.loc 1 12 0
  87 000044  07 00 88 	mov w7,_AD1CON1bits
  88              	.loc 1 16 0
  89 000046  83 81 FB 	ze w3,w3
  90              	.loc 1 13 0
  91 000048  01 40 A8 	bset.b _AD1CON1bits+1,#2
  92              	.loc 1 16 0
  93 00004a  E7 81 61 	and w3,#7,w3
  94              	.loc 1 14 0
  95 00004c  07 00 80 	mov _AD1CON1bits,w7
  96              	.loc 1 16 0
  97 00004e  C5 19 DD 	sl w3,#5,w3
  98              	.loc 1 14 0
  99 000050  84 83 63 	and w7,w4,w7
 100              	.loc 1 16 0
 101 000052  F4 F1 2F 	mov #-225,w4
 102              	.loc 1 14 0
 103 000054  87 83 70 	ior w1,w7,w7
 104              	.loc 1 17 0
 105 000056  C6 40 DE 	lsr w8,#6,w1
 106              	.loc 1 14 0
 107 000058  07 00 88 	mov w7,_AD1CON1bits
 108              	.loc 1 17 0
 109 00005a  E1 C0 60 	and.b w1,#1,w1
 110              	.loc 1 15 0
 111 00005c  00 80 A9 	bclr.b _AD1CON1bits,#4
 112              	.loc 1 17 0
 113 00005e  81 80 FB 	ze w1,w1
 114              	.loc 1 16 0
 115 000060  07 00 80 	mov _AD1CON1bits,w7
 116              	.loc 1 17 0
 117 000062  E1 80 60 	and w1,#1,w1
 118              	.loc 1 16 0
 119 000064  04 82 63 	and w7,w4,w4
 120              	.loc 1 17 0
 121 000066  C2 08 DD 	sl w1,#2,w1
 122              	.loc 1 16 0
 123 000068  84 81 71 	ior w3,w4,w3
 124              	.loc 1 19 0
 125 00006a  47 11 DE 	lsr w2,#7,w2
 126              	.loc 1 16 0
 127 00006c  03 00 88 	mov w3,_AD1CON1bits
 128              	.loc 1 19 0
 129 00006e  63 41 61 	and.b w2,#3,w2
 130              	.loc 1 17 0
 131 000070  03 00 80 	mov _AD1CON1bits,w3
 132              	.loc 1 19 0
 133 000072  67 41 61 	and.b w2,#7,w2
 134              	.loc 1 17 0
 135 000074  03 20 A1 	bclr w3,#2
 136              	.loc 1 19 0
 137 000076  02 81 FB 	ze w2,w2
 138              	.loc 1 17 0
MPLAB XC16 ASSEMBLY Listing:   			page 4


 139 000078  83 80 70 	ior w1,w3,w1
 140              	.loc 1 19 0
 141 00007a  4D 11 DD 	sl w2,#13,w2
 142              	.loc 1 17 0
 143 00007c  01 00 88 	mov w1,_AD1CON1bits
 144              	.loc 1 19 0
 145 00007e  F3 FF 21 	mov #8191,w3
 146 000080  04 00 80 	mov _AD1CON2bits,w4
 147              	.loc 1 20 0
 148 000082  C9 30 DE 	lsr w6,#9,w1
 149              	.loc 1 19 0
 150 000084  83 01 62 	and w4,w3,w3
 151              	.loc 1 20 0
 152 000086  E1 C0 60 	and.b w1,#1,w1
 153              	.loc 1 19 0
 154 000088  03 01 71 	ior w2,w3,w2
 155              	.loc 1 20 0
 156 00008a  81 80 FB 	ze w1,w1
 157              	.loc 1 19 0
 158 00008c  02 00 88 	mov w2,_AD1CON2bits
 159              	.loc 1 20 0
 160 00008e  61 81 60 	and w1,#1,w2
 161 000090  01 00 80 	mov _AD1CON2bits,w1
 162 000092  4A 11 DD 	sl w2,#10,w2
 163 000094  81 01 78 	mov w1,w3
 164 000096  03 A0 A1 	bclr w3,#10
 165              	.loc 1 21 0
 166 000098  CA 28 DE 	lsr w5,#10,w1
 167              	.loc 1 20 0
 168 00009a  03 01 71 	ior w2,w3,w2
 169              	.loc 1 21 0
 170 00009c  EF C0 60 	and.b w1,#15,w1
 171              	.loc 1 20 0
 172 00009e  02 00 88 	mov w2,_AD1CON2bits
 173              	.loc 1 21 0
 174 0000a0  FF C0 60 	and.b w1,#31,w1
 175 0000a2  03 00 80 	mov _AD1CON2bits,w3
 176 0000a4  81 80 FB 	ze w1,w1
 177 0000a6  32 F8 2F 	mov #-125,w2
 178 0000a8  FF 80 60 	and w1,#31,w1
 179 0000aa  02 81 61 	and w3,w2,w2
 180 0000ac  C2 08 DD 	sl w1,#2,w1
 181              	.loc 1 25 0
 182 0000ae  00 40 78 	mov.b w0,w0
 183              	.loc 1 21 0
 184 0000b0  82 80 70 	ior w1,w2,w1
 185              	.loc 1 25 0
 186 0000b2  7F 40 60 	and.b w0,#31,w0
 187              	.loc 1 21 0
 188 0000b4  01 00 88 	mov w1,_AD1CON2bits
 189              	.loc 1 25 0
 190 0000b6  00 80 FB 	ze w0,w0
 191              	.loc 1 22 0
 192 0000b8  00 20 A9 	bclr.b _AD1CON2bits,#1
 193              	.loc 1 25 0
 194 0000ba  7F 00 60 	and w0,#31,w0
 195              	.loc 1 23 0
MPLAB XC16 ASSEMBLY Listing:   			page 5


 196 0000bc  00 00 A9 	bclr.b _AD1CON2bits,#0
 197              	.loc 1 25 0
 198 0000be  C8 00 DD 	sl w0,#8,w1
 199 0000c0  03 00 80 	mov _AD1CON3bits,w3
 200 0000c2  F2 0F 2E 	mov #-7937,w2
  26:lib/lib_pic33e/ADC.c ****     AD1CON3bits.ADRC    = 0;                                    //Clock derived from system clock  
  27:lib/lib_pic33e/ADC.c ****     AD1CON3bits.ADCS    = parameter.conversion_time;            //ADC Conversion Clock 256.TAD (255
  28:lib/lib_pic33e/ADC.c ****     
  29:lib/lib_pic33e/ADC.c ****     AD1CON4bits.ADDMAEN = 0;
  30:lib/lib_pic33e/ADC.c **** 
  31:lib/lib_pic33e/ADC.c ****     //ANSEL##parameter.port## = parameter.pin_select;           /*set pins as analog inputs        
  32:lib/lib_pic33e/ADC.c ****     ANSELB  = parameter.pin_select;                             /*set pins as analog inputs        
  33:lib/lib_pic33e/ADC.c ****     AD1CSSL = parameter.pin_select;                             /*select which pins to scan        
  34:lib/lib_pic33e/ADC.c **** 
  35:lib/lib_pic33e/ADC.c ****     AD1CON1bits.ADON = parameter.turn_on;
  36:lib/lib_pic33e/ADC.c **** 
  37:lib/lib_pic33e/ADC.c ****     /* interrupts */
  38:lib/lib_pic33e/ADC.c ****     IFS0bits.AD1IF = 0;                                       /*clear interrupt flag               
  39:lib/lib_pic33e/ADC.c ****     IEC0bits.AD1IE = (parameter.interrupt_priority > 0);      /*enable interrupt if the priority is
 201              	.loc 1 39 0
 202 0000c4  10 C0 B3 	mov.b #1,w0
 203              	.loc 1 25 0
 204 0000c6  02 81 61 	and w3,w2,w2
 205 0000c8  82 80 70 	ior w1,w2,w1
 206 0000ca  01 00 88 	mov w1,_AD1CON3bits
 207              	.loc 1 26 0
 208 0000cc  01 E0 A9 	bclr.b _AD1CON3bits+1,#7
 209              	.loc 1 27 0
 210 0000ce  BE 00 90 	mov [w14+6],w1
 211              	.loc 1 32 0
 212 0000d0  9E 01 90 	mov [w14+2],w3
 213              	.loc 1 27 0
 214 0000d2  81 40 78 	mov.b w1,w1
 215              	.loc 1 33 0
 216 0000d4  1E 01 90 	mov [w14+2],w2
 217              	.loc 1 27 0
 218 0000d6  81 42 78 	mov.b w1,w5
 219 0000d8  04 00 20 	mov #_AD1CON3bits,w4
 220 0000da  05 4A 78 	mov.b w5,[w4]
 221              	.loc 1 35 0
 222 0000dc  CE 00 90 	mov [w14+8],w1
 223              	.loc 1 29 0
 224 0000de  01 00 A9 	bclr.b _AD1CON4bits+1,#0
 225              	.loc 1 35 0
 226 0000e0  C3 08 DE 	lsr w1,#3,w1
 227              	.loc 1 32 0
 228 0000e2  03 00 88 	mov w3,_ANSELB
 229              	.loc 1 35 0
 230 0000e4  E1 C0 60 	and.b w1,#1,w1
 231              	.loc 1 33 0
 232 0000e6  02 00 88 	mov w2,_AD1CSSL
 233              	.loc 1 35 0
 234 0000e8  01 81 FB 	ze w1,w2
 235 0000ea  01 00 80 	mov _AD1CON1bits,w1
 236 0000ec  4F 11 DD 	sl w2,#15,w2
 237 0000ee  81 01 78 	mov w1,w3
 238 0000f0  03 F0 A1 	bclr w3,#15
MPLAB XC16 ASSEMBLY Listing:   			page 6


 239              	.loc 1 39 0
 240 0000f2  CE 00 90 	mov [w14+8],w1
 241              	.loc 1 35 0
 242 0000f4  03 01 71 	ior w2,w3,w2
 243              	.loc 1 39 0
 244 0000f6  E7 C0 60 	and.b w1,#7,w1
 245              	.loc 1 35 0
 246 0000f8  02 00 88 	mov w2,_AD1CON1bits
 247              	.loc 1 39 0
 248 0000fa  81 80 FB 	ze w1,w1
 249              	.loc 1 38 0
 250 0000fc  01 A0 A9 	bclr.b _IFS0bits+1,#5
 251              	.loc 1 39 0
 252 0000fe  01 00 E0 	cp0 w1
 253              	.set ___BP___,0
 254 000100  00 00 3C 	bra gt,.L2
 255 000102  00 40 EB 	clr.b w0
 256              	.L2:
 257 000104  01 00 80 	mov _IEC0bits,w1
 258 000106  00 80 FB 	ze w0,w0
 259 000108  01 01 78 	mov w1,w2
 260 00010a  02 D0 A1 	bclr w2,#13
 261 00010c  E1 00 60 	and w0,#1,w1
  40:lib/lib_pic33e/ADC.c ****     IPC3bits.AD1IP = parameter.interrupt_priority;
 262              	.loc 1 40 0
 263 00010e  4E 00 90 	mov [w14+8],w0
 264              	.loc 1 39 0
 265 000110  CD 08 DD 	sl w1,#13,w1
 266              	.loc 1 40 0
 267 000112  67 40 60 	and.b w0,#7,w0
 268              	.loc 1 39 0
 269 000114  82 80 70 	ior w1,w2,w1
 270              	.loc 1 40 0
 271 000116  00 80 FB 	ze w0,w0
 272              	.loc 1 39 0
 273 000118  01 00 88 	mov w1,_IEC0bits
 274              	.loc 1 40 0
 275 00011a  67 00 60 	and w0,#7,w0
 276 00011c  02 00 80 	mov _IPC3bits,w2
 277 00011e  44 00 DD 	sl w0,#4,w0
 278 000120  F1 F8 2F 	mov #-113,w1
 279 000122  81 00 61 	and w2,w1,w1
 280 000124  01 00 70 	ior w0,w1,w0
 281 000126  00 00 88 	mov w0,_IPC3bits
  41:lib/lib_pic33e/ADC.c **** 
  42:lib/lib_pic33e/ADC.c **** }
 282              	.loc 1 42 0
 283 000128  4F 05 BE 	mov.d [--w15],w10
 284 00012a  4F 04 BE 	mov.d [--w15],w8
 285 00012c  8E 07 78 	mov w14,w15
 286 00012e  4F 07 78 	mov [--w15],w14
 287 000130  00 40 A9 	bclr CORCON,#2
 288 000132  00 00 06 	return 
 289              	.set ___PA___,0
 290              	.LFE0:
 291              	.size _config_adc1,.-_config_adc1
 292              	.align 2
MPLAB XC16 ASSEMBLY Listing:   			page 7


 293              	.weak _adc_callback
 294              	.type _adc_callback,@function
 295              	_adc_callback:
 296              	.LFB1:
  43:lib/lib_pic33e/ADC.c **** 
  44:lib/lib_pic33e/ADC.c **** void __attribute__((weak)) adc_callback(void) {
 297              	.loc 1 44 0
 298              	.set ___PA___,1
 299 000134  00 00 FA 	lnk #0
 300              	.LCFI3:
  45:lib/lib_pic33e/ADC.c **** 	return;
  46:lib/lib_pic33e/ADC.c **** }
 301              	.loc 1 46 0
 302 000136  8E 07 78 	mov w14,w15
 303 000138  4F 07 78 	mov [--w15],w14
 304 00013a  00 40 A9 	bclr CORCON,#2
 305 00013c  00 00 06 	return 
 306              	.set ___PA___,0
 307              	.LFE1:
 308              	.size _adc_callback,.-_adc_callback
 309              	.section .isr.text,code,keep
 310              	.align 2
 311              	.global __AD1Interrupt
 312              	.type __AD1Interrupt,@function
 313              	__AD1Interrupt:
 314              	.section .isr.text,code,keep
 315              	.LFB2:
 316              	.section .isr.text,code,keep
  47:lib/lib_pic33e/ADC.c **** 
  48:lib/lib_pic33e/ADC.c **** /**********************************************************************
  49:lib/lib_pic33e/ADC.c ****  * Assign End of ADC conversion interruption
  50:lib/lib_pic33e/ADC.c ****  **********************************************************************/
  51:lib/lib_pic33e/ADC.c **** void __attribute__((interrupt, auto_psv, shadow)) _AD1Interrupt (void){
 317              	.loc 1 51 0
 318              	.set ___PA___,1
 319 000000  00 A0 FE 	push.s 
 320 000002  00 00 F8 	push _RCOUNT
 321              	.LCFI4:
 322 000004  84 9F BE 	mov.d w4,[w15++]
 323              	.LCFI5:
 324 000006  86 9F BE 	mov.d w6,[w15++]
 325              	.LCFI6:
 326 000008  00 00 F8 	push _DSRPAG
 327              	.LCFI7:
 328 00000a  00 00 F8 	push _DSWPAG
 329              	.LCFI8:
 330 00000c  14 00 20 	mov #1,w4
 331 00000e  04 00 88 	mov w4,_DSWPAG
 332 000010  04 00 20 	mov #__const_psvpage,w4
 333 000012  04 00 88 	mov w4,_DSRPAG
 334 000014  00 00 00 	nop 
 335 000016  00 00 FA 	lnk #0
 336              	.LCFI9:
 337              	.section .isr.text,code,keep
  52:lib/lib_pic33e/ADC.c ****     
  53:lib/lib_pic33e/ADC.c ****     adc_callback();
 338              	.loc 1 53 0
MPLAB XC16 ASSEMBLY Listing:   			page 8


 339 000018  00 00 07 	rcall _adc_callback
 340              	.section .isr.text,code,keep
  54:lib/lib_pic33e/ADC.c **** 
  55:lib/lib_pic33e/ADC.c ****     IFS0bits.AD1IF = 0;        /*clear interrupt flag */
 341              	.loc 1 55 0
 342 00001a  01 A0 A9 	bclr.b _IFS0bits+1,#5
 343              	.section .isr.text,code,keep
  56:lib/lib_pic33e/ADC.c ****     return;
  57:lib/lib_pic33e/ADC.c **** }
 344              	.loc 1 57 0
 345 00001c  8E 07 78 	mov w14,w15
 346 00001e  4F 07 78 	mov [--w15],w14
 347 000020  00 40 A9 	bclr CORCON,#2
 348 000022  00 00 F9 	pop _DSWPAG
 349 000024  00 00 F9 	pop _DSRPAG
 350 000026  80 1F 78 	mov w0,[w15++]
 351 000028  00 0E 20 	mov #224,w0
 352 00002a  00 20 B7 	ior _SR
 353 00002c  4F 00 78 	mov [--w15],w0
 354 00002e  4F 03 BE 	mov.d [--w15],w6
 355 000030  4F 02 BE 	mov.d [--w15],w4
 356 000032  00 00 F9 	pop _RCOUNT
 357 000034  00 80 FE 	pop.s 
 358 000036  00 40 06 	retfie 
 359              	.set ___PA___,0
 360              	.LFE2:
 361              	.size __AD1Interrupt,.-__AD1Interrupt
 362              	.section .text,code
 363              	.align 2
 364              	.global _get_adc_value
 365              	.type _get_adc_value,@function
 366              	_get_adc_value:
 367              	.LFB3:
  58:lib/lib_pic33e/ADC.c **** 
  59:lib/lib_pic33e/ADC.c **** /**********************************************************************
  60:lib/lib_pic33e/ADC.c ****  * Name:    get_adc_value
  61:lib/lib_pic33e/ADC.c ****  * Args:    the sample you want to retrive (the firs one is sample_number 0,
  62:lib/lib_pic33e/ADC.c ****  *          the second one is sample_number one...)
  63:lib/lib_pic33e/ADC.c ****  * Return:  the value read by the adc
  64:lib/lib_pic33e/ADC.c ****  * Desc:    gets the adc value from the buffer and returns it
  65:lib/lib_pic33e/ADC.c ****  **********************************************************************/
  66:lib/lib_pic33e/ADC.c **** 
  67:lib/lib_pic33e/ADC.c **** unsigned int get_adc_value(unsigned int sample_number){
 368              	.loc 1 67 0
 369              	.set ___PA___,1
 370 00013e  02 00 FA 	lnk #2
 371              	.LCFI10:
 372              	.loc 1 67 0
 373 000140  00 0F 78 	mov w0,[w14]
  68:lib/lib_pic33e/ADC.c ****     if(sample_number>15){
 374              	.loc 1 68 0
 375 000142  1E 00 78 	mov [w14],w0
 376 000144  EF 0F 50 	sub w0,#15,[w15]
 377              	.set ___BP___,0
 378 000146  00 00 36 	bra leu,.L6
  69:lib/lib_pic33e/ADC.c ****         return -1;
 379              	.loc 1 69 0
MPLAB XC16 ASSEMBLY Listing:   			page 9


 380 000148  00 80 EB 	setm w0
 381 00014a  00 00 37 	bra .L7
 382              	.L6:
  70:lib/lib_pic33e/ADC.c ****     }else{
  71:lib/lib_pic33e/ADC.c ****         return *(&ADC1BUF0 + sample_number);
 383              	.loc 1 71 0
 384 00014c  1E 00 78 	mov [w14],w0
 385 00014e  01 00 20 	mov #_ADC1BUF0,w1
 386 000150  00 00 40 	add w0,w0,w0
 387 000152  01 00 40 	add w0,w1,w0
 388 000154  10 00 78 	mov [w0],w0
 389              	.L7:
  72:lib/lib_pic33e/ADC.c ****     }
  73:lib/lib_pic33e/ADC.c **** }
 390              	.loc 1 73 0
 391 000156  8E 07 78 	mov w14,w15
 392 000158  4F 07 78 	mov [--w15],w14
 393 00015a  00 40 A9 	bclr CORCON,#2
 394 00015c  00 00 06 	return 
 395              	.set ___PA___,0
 396              	.LFE3:
 397              	.size _get_adc_value,.-_get_adc_value
 398              	.align 2
 399              	.global _turn_on_adc
 400              	.type _turn_on_adc,@function
 401              	_turn_on_adc:
 402              	.LFB4:
  74:lib/lib_pic33e/ADC.c **** 
  75:lib/lib_pic33e/ADC.c **** /**********************************************************************
  76:lib/lib_pic33e/ADC.c ****  * Name:    turn_on_adc
  77:lib/lib_pic33e/ADC.c ****  * Args:    -
  78:lib/lib_pic33e/ADC.c ****  * Return:  -
  79:lib/lib_pic33e/ADC.c ****  * Desc:    turns on the adc module
  80:lib/lib_pic33e/ADC.c ****  **********************************************************************/
  81:lib/lib_pic33e/ADC.c **** void turn_on_adc(){
 403              	.loc 1 81 0
 404              	.set ___PA___,1
 405 00015e  00 00 FA 	lnk #0
 406              	.LCFI11:
  82:lib/lib_pic33e/ADC.c ****     AD1CON1bits.ADON = 1;
 407              	.loc 1 82 0
 408 000160  01 E0 A8 	bset.b _AD1CON1bits+1,#7
  83:lib/lib_pic33e/ADC.c **** }
 409              	.loc 1 83 0
 410 000162  8E 07 78 	mov w14,w15
 411 000164  4F 07 78 	mov [--w15],w14
 412 000166  00 40 A9 	bclr CORCON,#2
 413 000168  00 00 06 	return 
 414              	.set ___PA___,0
 415              	.LFE4:
 416              	.size _turn_on_adc,.-_turn_on_adc
 417              	.align 2
 418              	.global _turn_off_adc
 419              	.type _turn_off_adc,@function
 420              	_turn_off_adc:
 421              	.LFB5:
  84:lib/lib_pic33e/ADC.c **** 
MPLAB XC16 ASSEMBLY Listing:   			page 10


  85:lib/lib_pic33e/ADC.c **** /**********************************************************************
  86:lib/lib_pic33e/ADC.c ****  * Name:    turn_off_adc
  87:lib/lib_pic33e/ADC.c ****  * Args:    -
  88:lib/lib_pic33e/ADC.c ****  * Return:  -
  89:lib/lib_pic33e/ADC.c ****  * Desc:    turns off the adc module
  90:lib/lib_pic33e/ADC.c ****  **********************************************************************/
  91:lib/lib_pic33e/ADC.c **** void turn_off_adc(){
 422              	.loc 1 91 0
 423              	.set ___PA___,1
 424 00016a  00 00 FA 	lnk #0
 425              	.LCFI12:
  92:lib/lib_pic33e/ADC.c ****     AD1CON1bits.ADON = 0;
 426              	.loc 1 92 0
 427 00016c  01 E0 A9 	bclr.b _AD1CON1bits+1,#7
  93:lib/lib_pic33e/ADC.c **** }
 428              	.loc 1 93 0
 429 00016e  8E 07 78 	mov w14,w15
 430 000170  4F 07 78 	mov [--w15],w14
 431 000172  00 40 A9 	bclr CORCON,#2
 432 000174  00 00 06 	return 
 433              	.set ___PA___,0
 434              	.LFE5:
 435              	.size _turn_off_adc,.-_turn_off_adc
 436              	.align 2
 437              	.global _start_samp
 438              	.type _start_samp,@function
 439              	_start_samp:
 440              	.LFB6:
  94:lib/lib_pic33e/ADC.c **** 
  95:lib/lib_pic33e/ADC.c **** /**********************************************************************
  96:lib/lib_pic33e/ADC.c ****  * Name:    start_samp
  97:lib/lib_pic33e/ADC.c ****  * Args:    -
  98:lib/lib_pic33e/ADC.c ****  * Return:  -
  99:lib/lib_pic33e/ADC.c ****  * Desc:    starts a new sample 
 100:lib/lib_pic33e/ADC.c ****  * IMPORTANTE: only use this fuction if your sampling_type is set to MANUAL
 101:lib/lib_pic33e/ADC.c ****  **********************************************************************/
 102:lib/lib_pic33e/ADC.c **** void start_samp(){
 441              	.loc 1 102 0
 442              	.set ___PA___,1
 443 000176  00 00 FA 	lnk #0
 444              	.LCFI13:
 103:lib/lib_pic33e/ADC.c ****     AD1CON1bits.SAMP = 1;
 445              	.loc 1 103 0
 446 000178  00 20 A8 	bset.b _AD1CON1bits,#1
 104:lib/lib_pic33e/ADC.c **** }
 447              	.loc 1 104 0
 448 00017a  8E 07 78 	mov w14,w15
 449 00017c  4F 07 78 	mov [--w15],w14
 450 00017e  00 40 A9 	bclr CORCON,#2
 451 000180  00 00 06 	return 
 452              	.set ___PA___,0
 453              	.LFE6:
 454              	.size _start_samp,.-_start_samp
 455              	.align 2
 456              	.global _start_conversion
 457              	.type _start_conversion,@function
 458              	_start_conversion:
MPLAB XC16 ASSEMBLY Listing:   			page 11


 459              	.LFB7:
 105:lib/lib_pic33e/ADC.c **** 
 106:lib/lib_pic33e/ADC.c **** /**********************************************************************
 107:lib/lib_pic33e/ADC.c ****  * Name:    start_conversion
 108:lib/lib_pic33e/ADC.c ****  * Args:    -
 109:lib/lib_pic33e/ADC.c ****  * Return:  -
 110:lib/lib_pic33e/ADC.c ****  * Desc:    ends sampling and starts conversion
 111:lib/lib_pic33e/ADC.c ****  * IMPORTANTE: only use this fuction if your conversion_trigger is set to SAMP
 112:lib/lib_pic33e/ADC.c ****  **********************************************************************/
 113:lib/lib_pic33e/ADC.c **** void start_conversion(){
 460              	.loc 1 113 0
 461              	.set ___PA___,1
 462 000182  00 00 FA 	lnk #0
 463              	.LCFI14:
 114:lib/lib_pic33e/ADC.c ****     AD1CON1bits.SAMP = 0;
 464              	.loc 1 114 0
 465 000184  00 20 A9 	bclr.b _AD1CON1bits,#1
 115:lib/lib_pic33e/ADC.c **** }
 466              	.loc 1 115 0
 467 000186  8E 07 78 	mov w14,w15
 468 000188  4F 07 78 	mov [--w15],w14
 469 00018a  00 40 A9 	bclr CORCON,#2
 470 00018c  00 00 06 	return 
 471              	.set ___PA___,0
 472              	.LFE7:
 473              	.size _start_conversion,.-_start_conversion
 474              	.align 2
 475              	.global _wait_for_done
 476              	.type _wait_for_done,@function
 477              	_wait_for_done:
 478              	.LFB8:
 116:lib/lib_pic33e/ADC.c **** 
 117:lib/lib_pic33e/ADC.c **** /**********************************************************************
 118:lib/lib_pic33e/ADC.c ****  * Name:    wait_for_done
 119:lib/lib_pic33e/ADC.c ****  * Args:    -
 120:lib/lib_pic33e/ADC.c ****  * Return:  -
 121:lib/lib_pic33e/ADC.c ****  * Desc:    waits until conversion is done
 122:lib/lib_pic33e/ADC.c ****  **********************************************************************/
 123:lib/lib_pic33e/ADC.c **** void wait_for_done(){
 479              	.loc 1 123 0
 480              	.set ___PA___,1
 481 00018e  00 00 FA 	lnk #0
 482              	.LCFI15:
 483              	.L13:
 124:lib/lib_pic33e/ADC.c ****     while(!AD1CON1bits.DONE);
 484              	.loc 1 124 0
 485 000190  00 00 80 	mov _AD1CON1bits,w0
 486 000192  61 00 60 	and w0,#1,w0
 487 000194  00 00 E0 	cp0 w0
 488              	.set ___BP___,0
 489 000196  00 00 32 	bra z,.L13
 125:lib/lib_pic33e/ADC.c ****     return;
 126:lib/lib_pic33e/ADC.c **** }
 490              	.loc 1 126 0
 491 000198  8E 07 78 	mov w14,w15
 492 00019a  4F 07 78 	mov [--w15],w14
 493 00019c  00 40 A9 	bclr CORCON,#2
MPLAB XC16 ASSEMBLY Listing:   			page 12


 494 00019e  00 00 06 	return 
 495              	.set ___PA___,0
 496              	.LFE8:
 497              	.size _wait_for_done,.-_wait_for_done
 498              	.align 2
 499              	.global _set_default_parameters
 500              	.type _set_default_parameters,@function
 501              	_set_default_parameters:
 502              	.LFB9:
 127:lib/lib_pic33e/ADC.c **** 
 128:lib/lib_pic33e/ADC.c **** /**********************************************************************
 129:lib/lib_pic33e/ADC.c ****  * Name:    set_default_parameters
 130:lib/lib_pic33e/ADC.c ****  * Args:    ADC_parameters * (empty struct to be filled)
 131:lib/lib_pic33e/ADC.c ****  * Return:  -
 132:lib/lib_pic33e/ADC.c ****  * Desc:    fills a ADC_parameters struct with default configuration
 133:lib/lib_pic33e/ADC.c ****  **********************************************************************/
 134:lib/lib_pic33e/ADC.c **** void set_default_parameters(ADC_parameters *parameters){
 503              	.loc 1 134 0
 504              	.set ___PA___,1
 505 0001a0  02 00 FA 	lnk #2
 506              	.LCFI16:
 507              	.loc 1 134 0
 508 0001a2  00 0F 78 	mov w0,[w14]
 135:lib/lib_pic33e/ADC.c **** 
 136:lib/lib_pic33e/ADC.c ****     parameters->idle = 1;                        /*Works on idle*/
 137:lib/lib_pic33e/ADC.c ****     parameters->form = 0;                        /*form set to unsigned integer             */
 509              	.loc 1 137 0
 510 0001a4  92 FF 2F 	mov #-7,w2
 511              	.loc 1 136 0
 512 0001a6  1E 00 78 	mov [w14],w0
 138:lib/lib_pic33e/ADC.c ****     parameters->conversion_trigger = AUTO_CONV;  /*auto conversion                             */
 513              	.loc 1 138 0
 514 0001a8  81 03 20 	mov #56,w1
 515              	.loc 1 136 0
 516 0001aa  90 01 78 	mov [w0],w3
 517 0001ac  03 00 A0 	bset w3,#0
 518 0001ae  03 08 78 	mov w3,[w0]
 519              	.loc 1 137 0
 520 0001b0  1E 00 78 	mov [w14],w0
 139:lib/lib_pic33e/ADC.c ****     parameters->sampling_type = AUTO;            /*auto sampling                               */
 140:lib/lib_pic33e/ADC.c ****     parameters->voltage_ref = INTERNAL;          /*use internal voltage reference                  
 521              	.loc 1 140 0
 522 0001b2  F4 E7 2F 	mov #-385,w4
 523              	.loc 1 137 0
 524 0001b4  90 01 78 	mov [w0],w3
 525 0001b6  02 81 61 	and w3,w2,w2
 526 0001b8  02 08 78 	mov w2,[w0]
 527              	.loc 1 138 0
 528 0001ba  1E 00 78 	mov [w14],w0
 141:lib/lib_pic33e/ADC.c ****     parameters->sample_pin_select_type = AUTO;    /*auto change between differrent inputs          
 142:lib/lib_pic33e/ADC.c ****     parameters->smpi = 0;                        /*1 sample per interrupt                          
 529              	.loc 1 142 0
 530 0001bc  F2 3F 2C 	mov #-15361,w2
 531              	.loc 1 138 0
 532 0001be  90 01 78 	mov [w0],w3
 533 0001c0  83 80 70 	ior w1,w3,w1
 534 0001c2  01 08 78 	mov w1,[w0]
MPLAB XC16 ASSEMBLY Listing:   			page 13


 535              	.loc 1 139 0
 536 0001c4  1E 00 78 	mov [w14],w0
 143:lib/lib_pic33e/ADC.c ****     parameters->pin_select = 1;                  /*scan pin AN0                                    
 537              	.loc 1 143 0
 538 0001c6  13 00 20 	mov #1,w3
 539              	.loc 1 139 0
 540 0001c8  90 00 78 	mov [w0],w1
 541 0001ca  01 60 A0 	bset w1,#6
 542 0001cc  01 08 78 	mov w1,[w0]
 543              	.loc 1 140 0
 544 0001ce  1E 00 78 	mov [w14],w0
 144:lib/lib_pic33e/ADC.c ****     parameters->interrupt_priority = 5;          /*interrupt priority set to 5                     
 545              	.loc 1 144 0
 546 0001d0  81 FF 2F 	mov #-8,w1
 547              	.loc 1 140 0
 548 0001d2  90 02 78 	mov [w0],w5
 549 0001d4  04 82 62 	and w5,w4,w4
 550 0001d6  04 08 78 	mov w4,[w0]
 551              	.loc 1 141 0
 552 0001d8  1E 00 78 	mov [w14],w0
 553 0001da  10 02 78 	mov [w0],w4
 554 0001dc  04 90 A0 	bset w4,#9
 555 0001de  04 08 78 	mov w4,[w0]
 556              	.loc 1 142 0
 557 0001e0  1E 00 78 	mov [w14],w0
 558 0001e2  10 02 78 	mov [w0],w4
 559 0001e4  02 01 62 	and w4,w2,w2
 560 0001e6  02 08 78 	mov w2,[w0]
 561              	.loc 1 144 0
 562 0001e8  1E 00 78 	mov [w14],w0
 563              	.loc 1 143 0
 564 0001ea  1E 01 78 	mov [w14],w2
 565 0001ec  13 01 98 	mov w3,[w2+2]
 566              	.loc 1 144 0
 567 0001ee  40 01 90 	mov [w0+8],w2
 568 0001f0  81 00 61 	and w2,w1,w1
 569 0001f2  51 00 B3 	ior #5,w1
 570 0001f4  41 00 98 	mov w1,[w0+8]
 145:lib/lib_pic33e/ADC.c ****     parameters->turn_on = 1;                     /*turn the module on                              
 571              	.loc 1 145 0
 572 0001f6  1E 00 78 	mov [w14],w0
 573 0001f8  C0 00 90 	mov [w0+8],w1
 574 0001fa  01 30 A0 	bset w1,#3
 575 0001fc  41 00 98 	mov w1,[w0+8]
 146:lib/lib_pic33e/ADC.c ****     return;
 147:lib/lib_pic33e/ADC.c **** }
 576              	.loc 1 147 0
 577 0001fe  8E 07 78 	mov w14,w15
 578 000200  4F 07 78 	mov [--w15],w14
 579 000202  00 40 A9 	bclr CORCON,#2
 580 000204  00 00 06 	return 
 581              	.set ___PA___,0
 582              	.LFE9:
 583              	.size _set_default_parameters,.-_set_default_parameters
 584              	.align 2
 585              	.global _select_input_pin
 586              	.type _select_input_pin,@function
MPLAB XC16 ASSEMBLY Listing:   			page 14


 587              	_select_input_pin:
 588              	.LFB10:
 148:lib/lib_pic33e/ADC.c **** 
 149:lib/lib_pic33e/ADC.c **** /**********************************************************************
 150:lib/lib_pic33e/ADC.c ****  * Name:    select_input_pin
 151:lib/lib_pic33e/ADC.c ****  * Args:    number of the pin you want to scan
 152:lib/lib_pic33e/ADC.c ****  * Return:  -
 153:lib/lib_pic33e/ADC.c ****  * Desc:    defines what pin to scan next
 154:lib/lib_pic33e/ADC.c ****  **********************************************************************/
 155:lib/lib_pic33e/ADC.c **** void select_input_pin(unsigned int pin){
 589              	.loc 1 155 0
 590              	.set ___PA___,1
 591 000206  02 00 FA 	lnk #2
 592              	.LCFI17:
 593              	.loc 1 155 0
 594 000208  00 0F 78 	mov w0,[w14]
 156:lib/lib_pic33e/ADC.c ****     AD1CHS0bits.CH0SA = pin;
 595              	.loc 1 156 0
 596 00020a  01 FE 2F 	mov #-32,w1
 597 00020c  02 00 80 	mov _AD1CHS0bits,w2
 598 00020e  1E 00 78 	mov [w14],w0
 599 000210  81 00 61 	and w2,w1,w1
 600 000212  00 40 78 	mov.b w0,w0
 601 000214  7F 40 60 	and.b w0,#31,w0
 602 000216  00 80 FB 	ze w0,w0
 603 000218  7F 00 60 	and w0,#31,w0
 604 00021a  01 00 70 	ior w0,w1,w0
 605 00021c  00 00 88 	mov w0,_AD1CHS0bits
 157:lib/lib_pic33e/ADC.c ****     return;
 158:lib/lib_pic33e/ADC.c **** }
 606              	.loc 1 158 0
 607 00021e  8E 07 78 	mov w14,w15
 608 000220  4F 07 78 	mov [--w15],w14
 609 000222  00 40 A9 	bclr CORCON,#2
 610 000224  00 00 06 	return 
 611              	.set ___PA___,0
 612              	.LFE10:
 613              	.size _select_input_pin,.-_select_input_pin
 614              	.align 2
 615              	.global _config_pin_cycling
 616              	.type _config_pin_cycling,@function
 617              	_config_pin_cycling:
 618              	.LFB11:
 159:lib/lib_pic33e/ADC.c **** 
 160:lib/lib_pic33e/ADC.c **** /**********************************************************************
 161:lib/lib_pic33e/ADC.c ****  * Name:    config_pin_cycling
 162:lib/lib_pic33e/ADC.c ****  * Args:    -16bit number where bit x is high if the corresponding ANx pin is 
 163:lib/lib_pic33e/ADC.c ****             being used. Example: you want to read AN0, AN2 and AN15.
 164:lib/lib_pic33e/ADC.c ****             pin_vector = AN0|AN2|AN15 where AN0=2⁰, AN2=2² and AN15=2¹⁵;
 165:lib/lib_pic33e/ADC.c ****             -The sampling frequency. 250kHz is the maximum value. The minimum 
 166:lib/lib_pic33e/ADC.c ****             frequency depends on the systems instruction frequency, for 16MHz 
 167:lib/lib_pic33e/ADC.c ****             you can do 1Hz. The frequency at which you refresh the ADC buffer 
 168:lib/lib_pic33e/ADC.c ****             is equal to the sampling frequency (the value you input) divided by 
 169:lib/lib_pic33e/ADC.c ****             the numbers of channels sampling.
 170:lib/lib_pic33e/ADC.c ****             Example: 125000UL, Hz units and with unsigned long type;
 171:lib/lib_pic33e/ADC.c ****             -The system instruction frequency;
 172:lib/lib_pic33e/ADC.c ****             -The priority you desire for ADC interrupts;
MPLAB XC16 ASSEMBLY Listing:   			page 15


 173:lib/lib_pic33e/ADC.c ****             -ADC_parameters struct to be filled.
 174:lib/lib_pic33e/ADC.c ****  * Return:  Error value.
 175:lib/lib_pic33e/ADC.c ****  * Desc:    Fuction used to easily configure several pins for ADC reading:
 176:lib/lib_pic33e/ADC.c ****             -Configures pins for sequential reading using only channel0.
 177:lib/lib_pic33e/ADC.c ****             -Uses channel scanning to automatically go from one pin to the next 
 178:lib/lib_pic33e/ADC.c ****             one.
 179:lib/lib_pic33e/ADC.c ****             -Reading is done by numerical order AN0>...>AN15;
 180:lib/lib_pic33e/ADC.c ****             -The ADC buffer (filled) size = number os channels being read;
 181:lib/lib_pic33e/ADC.c ****             -Receives a large range of frequency values [fmin,250kHz];
 182:lib/lib_pic33e/ADC.c ****             -fmin  = floor(sys_clock[Hz]/(256*2¹⁶)) + 1;
 183:lib/lib_pic33e/ADC.c ****             -Since the PRx buffer can only take whole numbers, if 
 184:lib/lib_pic33e/ADC.c ****             sys_clock/(frequency*prescaler) is not a whole number the real 
 185:lib/lib_pic33e/ADC.c ****             frequency is not precisely what you insert;
 186:lib/lib_pic33e/ADC.c ****             -freal = sys_clocK/[floor(sys_clock/(frequency*ps))*ps] where 
 187:lib/lib_pic33e/ADC.c ****             ps=prescaler={1,8,64,256}.
 188:lib/lib_pic33e/ADC.c ****  **********************************************************************/
 189:lib/lib_pic33e/ADC.c **** Pin_Cycling_Error config_pin_cycling(uint16_t pin_vector, uint32_t frequency, 
 190:lib/lib_pic33e/ADC.c ****     uint32_t sys_clock, unsigned int priority, ADC_parameters *parameters)
 191:lib/lib_pic33e/ADC.c **** {
 619              	.loc 1 191 0
 620              	.set ___PA___,1
 621 000226  1C 00 FA 	lnk #28
 622              	.LCFI18:
 623              	.loc 1 191 0
 624 000228  70 07 98 	mov w0,[w14+14]
 625 00022a  24 0F 98 	mov w4,[w14+20]
 626 00022c  35 0F 98 	mov w5,[w14+22]
 627 00022e  02 0F 98 	mov w2,[w14+16]
 628 000230  13 0F 98 	mov w3,[w14+18]
 629 000232  41 0F 98 	mov w1,[w14+24]
 192:lib/lib_pic33e/ADC.c ****     //VERIFY INPUTS////////////////////////////////////////////////////////////
 193:lib/lib_pic33e/ADC.c ****     //Minimum acquisition frequency for a given FCY. FCY is defined in the 
 194:lib/lib_pic33e/ADC.c ****     //Makefile. For more information on FCY check timing library!
 195:lib/lib_pic33e/ADC.c ****     uint32_t fmin = ((sys_clock*596U)/(10000U))+1;
 630              	.loc 1 195 0
 631 000234  3E 0A 90 	mov [w14+22],w4
 632 000236  2E 08 90 	mov [w14+20],w0
 633 000238  AE 08 90 	mov [w14+20],w1
 634 00023a  E0 01 B9 	mulw.su w0,#0,w2
 635              	.loc 1 191 0
 636 00023c  56 0F 98 	mov w6,[w14+26]
 637              	.loc 1 195 0
 638 00023e  43 25 20 	mov #596,w3
 639 000240  40 25 20 	mov #596,w0
 640 000242  83 A2 B9 	mulw.ss w4,w3,w4
 641 000244  00 08 B8 	mul.uu w1,w0,w0
 642 000246  02 02 42 	add w4,w2,w4
 643 000248  02 71 22 	mov #10000,w2
 644 00024a  03 00 20 	mov #0,w3
 645 00024c  01 02 42 	add w4,w1,w4
 646 00024e  84 00 78 	mov w4,w1
 647 000250  00 00 07 	rcall ___udivsi3
 196:lib/lib_pic33e/ADC.c **** 
 197:lib/lib_pic33e/ADC.c ****     if (parameters == NULL)
 648              	.loc 1 197 0
 649 000252  5E 09 90 	mov [w14+26],w2
 650              	.loc 1 195 0
MPLAB XC16 ASSEMBLY Listing:   			page 16


 651 000254  61 00 40 	add w0,#1,w0
 652 000256  E0 80 48 	addc w1,#0,w1
 653 000258  10 07 98 	mov w0,[w14+2]
 654 00025a  21 07 98 	mov w1,[w14+4]
 655              	.loc 1 197 0
 656 00025c  02 00 E0 	cp0 w2
 657              	.set ___BP___,0
 658 00025e  00 00 3A 	bra nz,.L17
 198:lib/lib_pic33e/ADC.c ****     {
 199:lib/lib_pic33e/ADC.c ****         return POINTER_NULL_ADC;
 659              	.loc 1 199 0
 660 000260  20 00 20 	mov #2,w0
 661 000262  00 00 37 	bra .L18
 662              	.L17:
 200:lib/lib_pic33e/ADC.c ****     }
 201:lib/lib_pic33e/ADC.c ****     if (sys_clock>106 || 16>sys_clock)
 663              	.loc 1 201 0
 664 000264  2E 09 90 	mov [w14+20],w2
 665 000266  BE 09 90 	mov [w14+22],w3
 666 000268  A0 06 20 	mov #106,w0
 667 00026a  01 00 20 	mov #0,w1
 668 00026c  80 0F 51 	sub w2,w0,[w15]
 669 00026e  81 8F 59 	subb w3,w1,[w15]
 670              	.set ___BP___,0
 671 000270  00 00 3E 	bra gtu,.L19
 672 000272  2E 09 90 	mov [w14+20],w2
 673 000274  BE 09 90 	mov [w14+22],w3
 674 000276  F0 00 20 	mov #15,w0
 675 000278  01 00 20 	mov #0,w1
 676 00027a  80 0F 51 	sub w2,w0,[w15]
 677 00027c  81 8F 59 	subb w3,w1,[w15]
 678              	.set ___BP___,0
 679 00027e  00 00 3E 	bra gtu,.L20
 680              	.L19:
 202:lib/lib_pic33e/ADC.c ****     {
 203:lib/lib_pic33e/ADC.c ****         return WRONG_CLOCK_ADC;
 681              	.loc 1 203 0
 682 000280  30 00 20 	mov #3,w0
 683 000282  00 00 37 	bra .L18
 684              	.L20:
 204:lib/lib_pic33e/ADC.c ****     }
 205:lib/lib_pic33e/ADC.c **** 
 206:lib/lib_pic33e/ADC.c ****     if (frequency<fmin)
 685              	.loc 1 206 0
 686 000284  0E 09 90 	mov [w14+16],w2
 687 000286  9E 09 90 	mov [w14+18],w3
 688 000288  1E 00 90 	mov [w14+2],w0
 689 00028a  AE 00 90 	mov [w14+4],w1
 690 00028c  80 0F 51 	sub w2,w0,[w15]
 691 00028e  81 8F 59 	subb w3,w1,[w15]
 692              	.set ___BP___,0
 693 000290  00 00 31 	bra geu,.L21
 207:lib/lib_pic33e/ADC.c ****     {
 208:lib/lib_pic33e/ADC.c ****         return TOO_SLOW_ADC;
 694              	.loc 1 208 0
 695 000292  10 00 20 	mov #1,w0
 696 000294  00 00 37 	bra .L18
MPLAB XC16 ASSEMBLY Listing:   			page 17


 697              	.L21:
 209:lib/lib_pic33e/ADC.c ****     }
 210:lib/lib_pic33e/ADC.c ****     if (frequency>250000UL)
 698              	.loc 1 210 0
 699 000296  0E 09 90 	mov [w14+16],w2
 700 000298  9E 09 90 	mov [w14+18],w3
 701 00029a  00 09 2D 	mov #53392,w0
 702 00029c  31 00 20 	mov #3,w1
 703 00029e  80 0F 51 	sub w2,w0,[w15]
 704 0002a0  81 8F 59 	subb w3,w1,[w15]
 705              	.set ___BP___,0
 706 0002a2  00 00 36 	bra leu,.L22
 211:lib/lib_pic33e/ADC.c ****     {
 212:lib/lib_pic33e/ADC.c ****         return TOO_FAST_ADC;
 707              	.loc 1 212 0
 708 0002a4  00 00 EB 	clr w0
 709 0002a6  00 00 37 	bra .L18
 710              	.L22:
 213:lib/lib_pic33e/ADC.c ****     }
 214:lib/lib_pic33e/ADC.c ****     ///////////////////////////////////////////////////////////////////////////
 215:lib/lib_pic33e/ADC.c **** 
 216:lib/lib_pic33e/ADC.c ****     
 217:lib/lib_pic33e/ADC.c ****     //Timer5 CONFIGURATION FOR ADC CLOCK SOURCE////////////////////////////////
 218:lib/lib_pic33e/ADC.c ****     //minimum prescaler for a given FCY and frequency. (multiplied by 1000)
 219:lib/lib_pic33e/ADC.c ****     uint32_t Prescaler = ((sys_clock*15259)/(frequency)); 
 711              	.loc 1 219 0
 712 0002a8  3E 0A 90 	mov [w14+22],w4
 713 0002aa  2E 08 90 	mov [w14+20],w0
 714 0002ac  AE 08 90 	mov [w14+20],w1
 715 0002ae  E0 01 B9 	mulw.su w0,#0,w2
 716 0002b0  B3 B9 23 	mov #15259,w3
 717 0002b2  B0 B9 23 	mov #15259,w0
 718 0002b4  83 A2 B9 	mulw.ss w4,w3,w4
 719 0002b6  00 08 B8 	mul.uu w1,w0,w0
 720 0002b8  02 02 42 	add w4,w2,w4
 721 0002ba  0E 09 90 	mov [w14+16],w2
 722 0002bc  9E 09 90 	mov [w14+18],w3
 723 0002be  01 02 42 	add w4,w1,w4
 724 0002c0  84 00 78 	mov w4,w1
 725 0002c2  00 00 07 	rcall ___udivsi3
 220:lib/lib_pic33e/ADC.c ****     uint16_t period;
 221:lib/lib_pic33e/ADC.c **** 
 222:lib/lib_pic33e/ADC.c ****     TMR5 = 0x0000;
 726              	.loc 1 222 0
 727 0002c4  00 20 EF 	clr _TMR5
 728              	.loc 1 219 0
 729 0002c6  30 07 98 	mov w0,[w14+6]
 730 0002c8  41 07 98 	mov w1,[w14+8]
 223:lib/lib_pic33e/ADC.c ****     if (Prescaler>0U && Prescaler<1000U)
 731              	.loc 1 223 0
 732 0002ca  3E 00 90 	mov [w14+6],w0
 733 0002cc  CE 00 90 	mov [w14+8],w1
 734 0002ce  E0 0F 50 	sub w0,#0,[w15]
 735 0002d0  E0 8F 58 	subb w1,#0,[w15]
 736              	.set ___BP___,0
 737 0002d2  00 00 32 	bra z,.L23
 738 0002d4  3E 01 90 	mov [w14+6],w2
MPLAB XC16 ASSEMBLY Listing:   			page 18


 739 0002d6  CE 01 90 	mov [w14+8],w3
 740 0002d8  70 3E 20 	mov #999,w0
 741 0002da  01 00 20 	mov #0,w1
 742 0002dc  80 0F 51 	sub w2,w0,[w15]
 743 0002de  81 8F 59 	subb w3,w1,[w15]
 744              	.set ___BP___,0
 745 0002e0  00 00 3E 	bra gtu,.L23
 224:lib/lib_pic33e/ADC.c ****     {
 225:lib/lib_pic33e/ADC.c ****         T5CONbits.TCKPS = 0b00; //Prescaler=1
 746              	.loc 1 225 0
 747 0002e2  05 00 80 	mov _T5CONbits,w5
 226:lib/lib_pic33e/ADC.c ****         period = ((sys_clock*1000000UL)/(frequency))-1;
 748              	.loc 1 226 0
 749 0002e4  3E 0A 90 	mov [w14+22],w4
 750 0002e6  2E 08 90 	mov [w14+20],w0
 751 0002e8  AE 08 90 	mov [w14+20],w1
 752 0002ea  EF 00 B9 	mulw.su w0,#15,w0
 753              	.loc 1 225 0
 754 0002ec  F2 FC 2F 	mov #-49,w2
 755              	.loc 1 226 0
 756 0002ee  03 24 24 	mov #16960,w3
 757              	.loc 1 225 0
 758 0002f0  02 81 62 	and w5,w2,w2
 759              	.loc 1 226 0
 760 0002f2  83 A2 B9 	mulw.ss w4,w3,w4
 761              	.loc 1 225 0
 762 0002f4  02 00 88 	mov w2,_T5CONbits
 763              	.loc 1 226 0
 764 0002f6  00 02 42 	add w4,w0,w4
 765 0002f8  00 24 24 	mov #16960,w0
 766 0002fa  0E 09 90 	mov [w14+16],w2
 767 0002fc  9E 09 90 	mov [w14+18],w3
 768 0002fe  00 08 B8 	mul.uu w1,w0,w0
 769 000300  01 02 42 	add w4,w1,w4
 770 000302  84 00 78 	mov w4,w1
 771 000304  00 00 07 	rcall ___udivsi3
 772 000306  00 00 78 	mov w0,w0
 773 000308  00 00 E9 	dec w0,w0
 774 00030a  50 07 98 	mov w0,[w14+10]
 227:lib/lib_pic33e/ADC.c ****         PR5 = period;
 775              	.loc 1 227 0
 776 00030c  DE 00 90 	mov [w14+10],w1
 777 00030e  01 00 88 	mov w1,_PR5
 778 000310  00 00 37 	bra .L24
 779              	.L23:
 228:lib/lib_pic33e/ADC.c ****     }
 229:lib/lib_pic33e/ADC.c ****     else if (Prescaler>=1000U && Prescaler<8000U)
 780              	.loc 1 229 0
 781 000312  00 00 00 	nop 
 782 000314  3E 01 90 	mov [w14+6],w2
 783 000316  CE 01 90 	mov [w14+8],w3
 784 000318  70 3E 20 	mov #999,w0
 785 00031a  01 00 20 	mov #0,w1
 786 00031c  80 0F 51 	sub w2,w0,[w15]
 787 00031e  81 8F 59 	subb w3,w1,[w15]
 788              	.set ___BP___,0
 789 000320  00 00 36 	bra leu,.L25
MPLAB XC16 ASSEMBLY Listing:   			page 19


 790 000322  3E 01 90 	mov [w14+6],w2
 791 000324  CE 01 90 	mov [w14+8],w3
 792 000326  F0 F3 21 	mov #7999,w0
 793 000328  01 00 20 	mov #0,w1
 794 00032a  80 0F 51 	sub w2,w0,[w15]
 795 00032c  81 8F 59 	subb w3,w1,[w15]
 796              	.set ___BP___,0
 797 00032e  00 00 3E 	bra gtu,.L25
 230:lib/lib_pic33e/ADC.c ****     {
 231:lib/lib_pic33e/ADC.c ****         T5CONbits.TCKPS = 0b01; //Prescaler=8
 798              	.loc 1 231 0
 799 000330  05 00 80 	mov _T5CONbits,w5
 232:lib/lib_pic33e/ADC.c ****         period = ((sys_clock*125000UL)/(frequency))-1;
 800              	.loc 1 232 0
 801 000332  3E 0A 90 	mov [w14+22],w4
 802 000334  2E 08 90 	mov [w14+20],w0
 803 000336  AE 08 90 	mov [w14+20],w1
 804 000338  E1 01 B9 	mulw.su w0,#1,w2
 805              	.loc 1 231 0
 806 00033a  F0 FC 2F 	mov #-49,w0
 807              	.loc 1 232 0
 808 00033c  83 84 2E 	mov #-6072,w3
 809              	.loc 1 231 0
 810 00033e  00 80 62 	and w5,w0,w0
 811              	.loc 1 232 0
 812 000340  83 A2 B9 	mulw.ss w4,w3,w4
 813              	.loc 1 231 0
 814 000342  00 40 A0 	bset w0,#4
 815              	.loc 1 232 0
 816 000344  02 02 42 	add w4,w2,w4
 817              	.loc 1 231 0
 818 000346  00 00 88 	mov w0,_T5CONbits
 819              	.loc 1 232 0
 820 000348  80 84 2E 	mov #-6072,w0
 821 00034a  0E 09 90 	mov [w14+16],w2
 822 00034c  9E 09 90 	mov [w14+18],w3
 823 00034e  00 08 B8 	mul.uu w1,w0,w0
 824 000350  01 02 42 	add w4,w1,w4
 825 000352  84 00 78 	mov w4,w1
 826 000354  00 00 07 	rcall ___udivsi3
 827 000356  00 00 78 	mov w0,w0
 828 000358  00 00 E9 	dec w0,w0
 829 00035a  50 07 98 	mov w0,[w14+10]
 233:lib/lib_pic33e/ADC.c ****         PR5 = period;
 830              	.loc 1 233 0
 831 00035c  5E 01 90 	mov [w14+10],w2
 832 00035e  02 00 88 	mov w2,_PR5
 833 000360  00 00 37 	bra .L24
 834              	.L25:
 234:lib/lib_pic33e/ADC.c ****     }
 235:lib/lib_pic33e/ADC.c ****     else if (Prescaler>=8000U && Prescaler<64000U)
 835              	.loc 1 235 0
 836 000362  00 00 00 	nop 
 837 000364  3E 01 90 	mov [w14+6],w2
 838 000366  CE 01 90 	mov [w14+8],w3
 839 000368  F0 F3 21 	mov #7999,w0
 840 00036a  01 00 20 	mov #0,w1
MPLAB XC16 ASSEMBLY Listing:   			page 20


 841 00036c  80 0F 51 	sub w2,w0,[w15]
 842 00036e  81 8F 59 	subb w3,w1,[w15]
 843              	.set ___BP___,0
 844 000370  00 00 36 	bra leu,.L26
 845 000372  3E 01 90 	mov [w14+6],w2
 846 000374  CE 01 90 	mov [w14+8],w3
 847 000376  F0 9F 2F 	mov #63999,w0
 848 000378  01 00 20 	mov #0,w1
 849 00037a  80 0F 51 	sub w2,w0,[w15]
 850 00037c  81 8F 59 	subb w3,w1,[w15]
 851              	.set ___BP___,0
 852 00037e  00 00 3E 	bra gtu,.L26
 236:lib/lib_pic33e/ADC.c ****     {
 237:lib/lib_pic33e/ADC.c ****         T5CONbits.TCKPS = 0b10; //Prescaler=64
 853              	.loc 1 237 0
 854 000380  05 00 80 	mov _T5CONbits,w5
 238:lib/lib_pic33e/ADC.c ****         period = ((sys_clock*15625UL)/(frequency))-1;
 855              	.loc 1 238 0
 856 000382  3E 0A 90 	mov [w14+22],w4
 857 000384  2E 08 90 	mov [w14+20],w0
 858 000386  AE 08 90 	mov [w14+20],w1
 859 000388  E0 01 B9 	mulw.su w0,#0,w2
 860              	.loc 1 237 0
 861 00038a  F0 FC 2F 	mov #-49,w0
 862              	.loc 1 238 0
 863 00038c  93 D0 23 	mov #15625,w3
 864              	.loc 1 237 0
 865 00038e  00 80 62 	and w5,w0,w0
 866              	.loc 1 238 0
 867 000390  83 A2 B9 	mulw.ss w4,w3,w4
 868              	.loc 1 237 0
 869 000392  00 50 A0 	bset w0,#5
 870              	.loc 1 238 0
 871 000394  02 02 42 	add w4,w2,w4
 872              	.loc 1 237 0
 873 000396  00 00 88 	mov w0,_T5CONbits
 874              	.loc 1 238 0
 875 000398  90 D0 23 	mov #15625,w0
 876 00039a  0E 09 90 	mov [w14+16],w2
 877 00039c  9E 09 90 	mov [w14+18],w3
 878 00039e  00 08 B8 	mul.uu w1,w0,w0
 879 0003a0  01 02 42 	add w4,w1,w4
 880 0003a2  84 00 78 	mov w4,w1
 881 0003a4  00 00 07 	rcall ___udivsi3
 882 0003a6  00 00 78 	mov w0,w0
 883 0003a8  00 00 E9 	dec w0,w0
 884 0003aa  50 07 98 	mov w0,[w14+10]
 239:lib/lib_pic33e/ADC.c ****         PR5 = period;
 885              	.loc 1 239 0
 886 0003ac  DE 01 90 	mov [w14+10],w3
 887 0003ae  03 00 88 	mov w3,_PR5
 888 0003b0  00 00 37 	bra .L24
 889              	.L26:
 240:lib/lib_pic33e/ADC.c ****     }
 241:lib/lib_pic33e/ADC.c ****     else if (Prescaler>=64000U && Prescaler<256000UL)
 890              	.loc 1 241 0
 891 0003b2  00 00 00 	nop 
MPLAB XC16 ASSEMBLY Listing:   			page 21


 892 0003b4  3E 01 90 	mov [w14+6],w2
 893 0003b6  CE 01 90 	mov [w14+8],w3
 894 0003b8  F0 9F 2F 	mov #63999,w0
 895 0003ba  01 00 20 	mov #0,w1
 896 0003bc  80 0F 51 	sub w2,w0,[w15]
 897 0003be  81 8F 59 	subb w3,w1,[w15]
 898              	.set ___BP___,0
 899 0003c0  00 00 36 	bra leu,.L24
 900 0003c2  3E 01 90 	mov [w14+6],w2
 901 0003c4  CE 01 90 	mov [w14+8],w3
 902 0003c6  F0 7F 2E 	mov #59391,w0
 903 0003c8  31 00 20 	mov #3,w1
 904 0003ca  80 0F 51 	sub w2,w0,[w15]
 905 0003cc  81 8F 59 	subb w3,w1,[w15]
 906              	.set ___BP___,0
 907 0003ce  00 00 3E 	bra gtu,.L24
 242:lib/lib_pic33e/ADC.c ****     {
 243:lib/lib_pic33e/ADC.c ****         T5CONbits.TCKPS = 0b11; //Prescaler=256
 908              	.loc 1 243 0
 909 0003d0  05 00 80 	mov _T5CONbits,w5
 244:lib/lib_pic33e/ADC.c ****         period = ((sys_clock*1000000UL)/(256*frequency))-1;
 910              	.loc 1 244 0
 911 0003d2  3E 0A 90 	mov [w14+22],w4
 912 0003d4  2E 08 90 	mov [w14+20],w0
 913 0003d6  AE 08 90 	mov [w14+20],w1
 914 0003d8  EF 00 B9 	mulw.su w0,#15,w0
 915              	.loc 1 243 0
 916 0003da  02 03 20 	mov #48,w2
 917              	.loc 1 244 0
 918 0003dc  03 24 24 	mov #16960,w3
 919              	.loc 1 243 0
 920 0003de  05 01 71 	ior w2,w5,w2
 921              	.loc 1 244 0
 922 0003e0  83 A2 B9 	mulw.ss w4,w3,w4
 923              	.loc 1 243 0
 924 0003e2  02 00 88 	mov w2,_T5CONbits
 925              	.loc 1 244 0
 926 0003e4  00 02 42 	add w4,w0,w4
 927 0003e6  00 24 24 	mov #16960,w0
 928 0003e8  0E 0B 90 	mov [w14+16],w6
 929 0003ea  9E 0B 90 	mov [w14+18],w7
 930 0003ec  00 08 B8 	mul.uu w1,w0,w0
 931 0003ee  48 39 DD 	sl w7,#8,w2
 932 0003f0  C8 31 DE 	lsr w6,#8,w3
 933 0003f2  83 01 71 	ior w2,w3,w3
 934 0003f4  48 31 DD 	sl w6,#8,w2
 935 0003f6  01 02 42 	add w4,w1,w4
 936 0003f8  84 00 78 	mov w4,w1
 937 0003fa  00 00 07 	rcall ___udivsi3
 938 0003fc  00 00 78 	mov w0,w0
 939 0003fe  00 00 E9 	dec w0,w0
 940 000400  50 07 98 	mov w0,[w14+10]
 245:lib/lib_pic33e/ADC.c ****         PR5 = period;
 941              	.loc 1 245 0
 942 000402  DE 00 90 	mov [w14+10],w1
 943 000404  01 00 88 	mov w1,_PR5
 944              	.L24:
MPLAB XC16 ASSEMBLY Listing:   			page 22


 246:lib/lib_pic33e/ADC.c ****     }
 247:lib/lib_pic33e/ADC.c ****     IFS1bits.T5IF = 0;      // Clear Timer5 interrupt flag;
 945              	.loc 1 247 0
 946 000406  00 00 00 	nop 
 947 000408  01 80 A9 	bclr.b _IFS1bits+1,#4
 248:lib/lib_pic33e/ADC.c ****     IEC1bits.T5IE = 0;      // Disable Timer5 interrupt;
 249:lib/lib_pic33e/ADC.c ****     ///////////////////////////////////////////////////////////////////////////
 250:lib/lib_pic33e/ADC.c **** 
 251:lib/lib_pic33e/ADC.c ****     
 252:lib/lib_pic33e/ADC.c ****     //ADC CONFIGURATION FOR 12 bit MODE USING CHANNEL SCANNING/////////////////
 253:lib/lib_pic33e/ADC.c ****     uint16_t i=0;
 254:lib/lib_pic33e/ADC.c ****     uint16_t clock_factor = 140*sys_clock/1000;
 948              	.loc 1 254 0
 949 00040a  00 00 00 	nop 
 950 00040c  BE 09 90 	mov [w14+22],w3
 951              	.loc 1 248 0
 952 00040e  00 00 00 	nop 
 953 000410  01 80 A9 	bclr.b _IEC1bits+1,#4
 954              	.loc 1 254 0
 955 000412  00 00 00 	nop 
 956 000414  2E 08 90 	mov [w14+20],w0
 957 000416  00 00 00 	nop 
 958 000418  AE 08 90 	mov [w14+20],w1
 959 00041a  E0 01 B9 	mulw.su w0,#0,w2
 960              	.loc 1 253 0
 961 00041c  00 02 EB 	clr w4
 962              	.loc 1 254 0
 963 00041e  C0 08 20 	mov #140,w0
 964              	.loc 1 253 0
 965 000420  04 0F 78 	mov w4,[w14]
 966              	.loc 1 254 0
 967 000422  80 9A B9 	mulw.ss w3,w0,w4
 968 000424  C0 08 20 	mov #140,w0
 969 000426  02 02 42 	add w4,w2,w4
 970 000428  00 08 B8 	mul.uu w1,w0,w0
 971 00042a  82 3E 20 	mov #1000,w2
 972 00042c  03 00 20 	mov #0,w3
 973 00042e  01 02 42 	add w4,w1,w4
 974 000430  84 00 78 	mov w4,w1
 975 000432  00 00 07 	rcall ___udivsi3
 255:lib/lib_pic33e/ADC.c **** 
 256:lib/lib_pic33e/ADC.c ****     AD1CON1 = 0;
 257:lib/lib_pic33e/ADC.c ****     AD1CON2 = 0;
 258:lib/lib_pic33e/ADC.c ****     AD1CON3 = 0;
 259:lib/lib_pic33e/ADC.c ****     AD1CON4 = 0;
 260:lib/lib_pic33e/ADC.c ****     AD1CHS0 = 0;
 261:lib/lib_pic33e/ADC.c ****     AD1CSSH = 0;
 262:lib/lib_pic33e/ADC.c ****     AD1CSSL = 0;
 263:lib/lib_pic33e/ADC.c ****     parameters->idle = 1;
 976              	.loc 1 263 0
 977 000434  5E 09 90 	mov [w14+26],w2
 978              	.loc 1 256 0
 979 000436  00 20 EF 	clr _AD1CON1
 980              	.loc 1 257 0
 981 000438  00 20 EF 	clr _AD1CON2
 982              	.loc 1 254 0
 983 00043a  60 07 98 	mov w0,[w14+12]
MPLAB XC16 ASSEMBLY Listing:   			page 23


 984              	.loc 1 258 0
 985 00043c  00 20 EF 	clr _AD1CON3
 986              	.loc 1 259 0
 987 00043e  00 20 EF 	clr _AD1CON4
 988              	.loc 1 260 0
 989 000440  00 20 EF 	clr _AD1CHS0
 990              	.loc 1 261 0
 991 000442  00 20 EF 	clr _AD1CSSH
 992              	.loc 1 262 0
 993 000444  00 20 EF 	clr _AD1CSSL
 994              	.loc 1 263 0
 995 000446  12 00 78 	mov [w2],w0
 996 000448  00 00 A0 	bset w0,#0
 997 00044a  00 09 78 	mov w0,[w2]
 264:lib/lib_pic33e/ADC.c ****     parameters->form = 0;
 998              	.loc 1 264 0
 999 00044c  5E 08 90 	mov [w14+26],w0
 1000 00044e  91 FF 2F 	mov #-7,w1
 1001 000450  10 01 78 	mov [w0],w2
 1002 000452  81 00 61 	and w2,w1,w1
 1003 000454  01 08 78 	mov w1,[w0]
 265:lib/lib_pic33e/ADC.c ****     parameters->conversion_trigger = TIMER5;
 1004              	.loc 1 265 0
 1005 000456  5E 08 90 	mov [w14+26],w0
 1006 000458  71 FC 2F 	mov #-57,w1
 1007 00045a  10 01 78 	mov [w0],w2
 1008 00045c  81 00 61 	and w2,w1,w1
 1009 00045e  01 50 A0 	bset w1,#5
 1010 000460  01 08 78 	mov w1,[w0]
 266:lib/lib_pic33e/ADC.c ****     parameters->sampling_type = AUTO;           
 1011              	.loc 1 266 0
 1012 000462  5E 08 90 	mov [w14+26],w0
 267:lib/lib_pic33e/ADC.c ****     parameters->voltage_ref = INTERNAL;          
 1013              	.loc 1 267 0
 1014 000464  F2 E7 2F 	mov #-385,w2
 1015              	.loc 1 266 0
 1016 000466  90 00 78 	mov [w0],w1
 1017 000468  01 60 A0 	bset w1,#6
 1018 00046a  01 08 78 	mov w1,[w0]
 1019              	.loc 1 267 0
 1020 00046c  5E 08 90 	mov [w14+26],w0
 268:lib/lib_pic33e/ADC.c ****     parameters->sample_pin_select_type = AUTO;
 269:lib/lib_pic33e/ADC.c ****     //Get number of pins in pin_vector. Size is global and is used to set the 
 270:lib/lib_pic33e/ADC.c ****     //number of conversions per interrupt (SMPI).
 271:lib/lib_pic33e/ADC.c ****     for(i=0;i<16;i++)
 1021              	.loc 1 271 0
 1022 00046e  80 00 EB 	clr w1
 1023              	.loc 1 267 0
 1024 000470  90 01 78 	mov [w0],w3
 1025 000472  02 81 61 	and w3,w2,w2
 1026 000474  02 08 78 	mov w2,[w0]
 1027              	.loc 1 268 0
 1028 000476  5E 08 90 	mov [w14+26],w0
 1029 000478  10 01 78 	mov [w0],w2
 1030 00047a  02 90 A0 	bset w2,#9
 1031 00047c  02 08 78 	mov w2,[w0]
 1032              	.loc 1 271 0
MPLAB XC16 ASSEMBLY Listing:   			page 24


 1033 00047e  01 0F 78 	mov w1,[w14]
 1034 000480  00 00 37 	bra .L27
 1035              	.L29:
 272:lib/lib_pic33e/ADC.c ****     {
 273:lib/lib_pic33e/ADC.c ****         if (RD_BITFLD(pin_vector,i)==1)
 1036              	.loc 1 273 0
 1037 000482  1E 00 78 	mov [w14],w0
 1038 000484  FE 00 90 	mov [w14+14],w1
 1039 000486  00 08 DE 	lsr w1,w0,w0
 1040 000488  61 00 60 	and w0,#1,w0
 1041 00048a  00 40 78 	mov.b w0,w0
 1042 00048c  00 04 E0 	cp0.b w0
 1043              	.set ___BP___,0
 1044 00048e  00 00 32 	bra z,.L28
 274:lib/lib_pic33e/ADC.c ****         {
 275:lib/lib_pic33e/ADC.c ****             size_pin_cycling_vector++;
 1045              	.loc 1 275 0
 1046 000490  00 00 80 	mov _size_pin_cycling_vector,w0
 1047 000492  00 00 E8 	inc w0,w0
 1048 000494  00 00 88 	mov w0,_size_pin_cycling_vector
 1049              	.L28:
 1050              	.loc 1 271 0
 1051 000496  1E 0F E8 	inc [w14],[w14]
 1052              	.L27:
 1053 000498  1E 00 78 	mov [w14],w0
 1054 00049a  EF 0F 50 	sub w0,#15,[w15]
 1055              	.set ___BP___,0
 1056 00049c  00 00 36 	bra leu,.L29
 276:lib/lib_pic33e/ADC.c ****         }
 277:lib/lib_pic33e/ADC.c ****     }
 278:lib/lib_pic33e/ADC.c ****     parameters->smpi = size_pin_cycling_vector-1;
 1057              	.loc 1 278 0
 1058 00049e  5E 08 90 	mov [w14+26],w0
 1059 0004a0  01 00 80 	mov _size_pin_cycling_vector,w1
 1060 0004a2  90 01 78 	mov [w0],w3
 1061 0004a4  81 40 78 	mov.b w1,w1
 1062 0004a6  F2 3F 2C 	mov #-15361,w2
 1063 0004a8  81 40 E9 	dec.b w1,w1
 1064 0004aa  02 81 61 	and w3,w2,w2
 1065 0004ac  EF C0 60 	and.b w1,#15,w1
 1066 0004ae  81 80 FB 	ze w1,w1
 1067 0004b0  EF 80 60 	and w1,#15,w1
 1068 0004b2  CA 08 DD 	sl w1,#10,w1
 1069 0004b4  82 80 70 	ior w1,w2,w1
 1070 0004b6  01 08 78 	mov w1,[w0]
 279:lib/lib_pic33e/ADC.c ****     // clock_factor multiples by the system clock period. Calculated for 140ns 
 280:lib/lib_pic33e/ADC.c ****     //ADC clock (Tad) but can be higher due to rounding down:
 281:lib/lib_pic33e/ADC.c ****     parameters->conversion_time = clock_factor;
 282:lib/lib_pic33e/ADC.c ****     parameters->pin_select = pin_vector;                  
 283:lib/lib_pic33e/ADC.c ****     parameters->interrupt_priority = priority;
 1071              	.loc 1 283 0
 1072 0004b8  5E 08 90 	mov [w14+26],w0
 1073              	.loc 1 281 0
 1074 0004ba  5E 09 90 	mov [w14+26],w2
 1075              	.loc 1 282 0
 1076 0004bc  DE 08 90 	mov [w14+26],w1
 1077              	.loc 1 281 0
MPLAB XC16 ASSEMBLY Listing:   			page 25


 1078 0004be  EE 01 90 	mov [w14+12],w3
 1079 0004c0  33 01 98 	mov w3,[w2+6]
 1080              	.loc 1 282 0
 1081 0004c2  7E 01 90 	mov [w14+14],w2
 1082 0004c4  92 00 98 	mov w2,[w1+2]
 1083              	.loc 1 283 0
 1084 0004c6  CE 08 90 	mov [w14+24],w1
 1085 0004c8  C0 01 90 	mov [w0+8],w3
 1086 0004ca  81 40 78 	mov.b w1,w1
 1087 0004cc  82 FF 2F 	mov #-8,w2
 1088 0004ce  E7 C0 60 	and.b w1,#7,w1
 1089 0004d0  02 81 61 	and w3,w2,w2
 1090 0004d2  81 80 FB 	ze w1,w1
 1091 0004d4  E7 80 60 	and w1,#7,w1
 1092 0004d6  82 80 70 	ior w1,w2,w1
 1093 0004d8  41 00 98 	mov w1,[w0+8]
 284:lib/lib_pic33e/ADC.c ****     parameters->turn_on = 1;
 1094              	.loc 1 284 0
 1095 0004da  DE 08 90 	mov [w14+26],w1
 285:lib/lib_pic33e/ADC.c ****     T5CONbits.TON = 1;//Turn on Timer 5
 286:lib/lib_pic33e/ADC.c ****     ///////////////////////////////////////////////////////////////////////////
 287:lib/lib_pic33e/ADC.c **** 
 288:lib/lib_pic33e/ADC.c ****     return SUCESS_ADC;
 1096              	.loc 1 288 0
 1097 0004dc  40 00 20 	mov #4,w0
 1098              	.loc 1 284 0
 1099 0004de  41 01 90 	mov [w1+8],w2
 1100 0004e0  02 30 A0 	bset w2,#3
 1101 0004e2  C2 00 98 	mov w2,[w1+8]
 1102              	.loc 1 285 0
 1103 0004e4  01 E0 A8 	bset.b _T5CONbits+1,#7
 1104              	.L18:
 289:lib/lib_pic33e/ADC.c **** }
 1105              	.loc 1 289 0
 1106 0004e6  8E 07 78 	mov w14,w15
 1107 0004e8  4F 07 78 	mov [--w15],w14
 1108 0004ea  00 40 A9 	bclr CORCON,#2
 1109 0004ec  00 00 06 	return 
 1110              	.set ___PA___,0
 1111              	.LFE11:
 1112              	.size _config_pin_cycling,.-_config_pin_cycling
 1113              	.align 2
 1114              	.global _get_pin_cycling_values
 1115              	.type _get_pin_cycling_values,@function
 1116              	_get_pin_cycling_values:
 1117              	.LFB12:
 290:lib/lib_pic33e/ADC.c **** 
 291:lib/lib_pic33e/ADC.c **** /**********************************************************************
 292:lib/lib_pic33e/ADC.c ****  * Name:    get_pin_cycling_values
 293:lib/lib_pic33e/ADC.c ****  * Args:    -The array adress to where you want to save the values;
 294:lib/lib_pic33e/ADC.c ****             -The position of the ADC buffer where you want to start 
 295:lib/lib_pic33e/ADC.c ****             reading. Example: you sample 8 different channels and want to read 
 296:lib/lib_pic33e/ADC.c ****             only from the 6th to the 8th, your starting position is 6-1 (since 
 297:lib/lib_pic33e/ADC.c ****             the buffer starts in position 0);
 298:lib/lib_pic33e/ADC.c ****             -How many values, startin from "position" do you want to read.
 299:lib/lib_pic33e/ADC.c ****  * Return:  Error value.
 300:lib/lib_pic33e/ADC.c ****  * Desc:    Gets the ADC values from the buffer and returns them. Verifies if 
MPLAB XC16 ASSEMBLY Listing:   			page 26


 301:lib/lib_pic33e/ADC.c ****             your inputs are within the buffer's size.
 302:lib/lib_pic33e/ADC.c ****  **********************************************************************/
 303:lib/lib_pic33e/ADC.c **** uint16_t get_pin_cycling_values(uint16_t *value, uint16_t position, 
 304:lib/lib_pic33e/ADC.c ****     uint16_t length){
 1118              	.loc 1 304 0
 1119              	.set ___PA___,1
 1120 0004ee  2C 00 FA 	lnk #44
 1121              	.LCFI19:
 305:lib/lib_pic33e/ADC.c **** 
 306:lib/lib_pic33e/ADC.c ****     uint16_t adc_aquisitions[16];
 307:lib/lib_pic33e/ADC.c ****     int i;
 308:lib/lib_pic33e/ADC.c ****     int j;
 309:lib/lib_pic33e/ADC.c ****     uint16_t *ptr = &ADC1BUF0;
 1122              	.loc 1 309 0
 1123 0004f0  04 00 20 	mov #_ADC1BUF0,w4
 310:lib/lib_pic33e/ADC.c **** 
 311:lib/lib_pic33e/ADC.c ****     for (i=0; i<size_pin_cycling_vector; i++) {
 1124              	.loc 1 311 0
 1125 0004f2  80 01 EB 	clr w3
 1126              	.loc 1 304 0
 1127 0004f4  30 17 98 	mov w0,[w14+38]
 1128 0004f6  41 17 98 	mov w1,[w14+40]
 1129 0004f8  52 17 98 	mov w2,[w14+42]
 1130              	.loc 1 309 0
 1131 0004fa  24 07 98 	mov w4,[w14+4]
 1132              	.loc 1 311 0
 1133 0004fc  03 0F 78 	mov w3,[w14]
 1134 0004fe  00 00 37 	bra .L31
 1135              	.L32:
 312:lib/lib_pic33e/ADC.c ****         adc_aquisitions[i] = *(ptr++); 
 1136              	.loc 1 312 0
 1137 000500  AE 00 90 	mov [w14+4],w1
 1138 000502  1E 00 78 	mov [w14],w0
 1139 000504  91 00 78 	mov [w1],w1
 1140              	.loc 1 311 0
 1141 000506  1E 0F E8 	inc [w14],[w14]
 1142              	.loc 1 312 0
 1143 000508  2E 01 90 	mov [w14+4],w2
 1144 00050a  00 00 40 	add w0,w0,w0
 1145 00050c  02 81 E8 	inc2 w2,w2
 1146 00050e  66 00 40 	add w0,#6,w0
 1147 000510  22 07 98 	mov w2,[w14+4]
 1148 000512  01 37 78 	mov w1,[w14+w0]
 1149              	.L31:
 1150              	.loc 1 311 0
 1151 000514  9E 00 78 	mov [w14],w1
 1152 000516  00 00 80 	mov _size_pin_cycling_vector,w0
 1153 000518  80 8F 50 	sub w1,w0,[w15]
 1154              	.set ___BP___,0
 1155 00051a  00 00 39 	bra ltu,.L32
 313:lib/lib_pic33e/ADC.c ****     }
 314:lib/lib_pic33e/ADC.c **** 
 315:lib/lib_pic33e/ADC.c ****     if (position > size_pin_cycling_vector || 
 1156              	.loc 1 315 0
 1157 00051c  00 00 80 	mov _size_pin_cycling_vector,w0
 1158 00051e  CE 10 90 	mov [w14+40],w1
 1159 000520  80 8F 50 	sub w1,w0,[w15]
MPLAB XC16 ASSEMBLY Listing:   			page 27


 1160              	.set ___BP___,0
 1161 000522  00 00 3E 	bra gtu,.L33
 316:lib/lib_pic33e/ADC.c ****         position + length > (size_pin_cycling_vector +1)|| value == NULL)
 1162              	.loc 1 316 0
 1163 000524  4E 11 90 	mov [w14+40],w2
 1164 000526  DE 10 90 	mov [w14+42],w1
 1165 000528  00 00 80 	mov _size_pin_cycling_vector,w0
 1166 00052a  81 00 41 	add w2,w1,w1
 1167 00052c  00 00 E8 	inc w0,w0
 1168              	.loc 1 315 0
 1169 00052e  80 8F 50 	sub w1,w0,[w15]
 1170              	.set ___BP___,0
 1171 000530  00 00 3E 	bra gtu,.L33
 1172              	.loc 1 316 0
 1173 000532  3E 10 90 	mov [w14+38],w0
 1174 000534  00 00 E0 	cp0 w0
 1175              	.set ___BP___,0
 1176 000536  00 00 3A 	bra nz,.L34
 1177              	.L33:
 317:lib/lib_pic33e/ADC.c ****     {
 318:lib/lib_pic33e/ADC.c ****         return 1;
 1178              	.loc 1 318 0
 1179 000538  10 00 20 	mov #1,w0
 1180 00053a  00 00 37 	bra .L35
 1181              	.L34:
 319:lib/lib_pic33e/ADC.c ****     }
 320:lib/lib_pic33e/ADC.c **** 
 321:lib/lib_pic33e/ADC.c ****     for (i=position,j=0; i<position+length; i++, j++) {
 1182              	.loc 1 321 0
 1183 00053c  00 00 EB 	clr w0
 1184 00053e  CE 10 90 	mov [w14+40],w1
 1185 000540  01 0F 78 	mov w1,[w14]
 1186 000542  10 07 98 	mov w0,[w14+2]
 1187 000544  00 00 37 	bra .L36
 1188              	.L37:
 322:lib/lib_pic33e/ADC.c ****         *(value+j) = adc_aquisitions[i];
 1189              	.loc 1 322 0
 1190 000546  00 00 00 	nop 
 1191 000548  1E 00 78 	mov [w14],w0
 1192 00054a  00 00 40 	add w0,w0,w0
 1193 00054c  E6 00 40 	add w0,#6,w1
 1194 00054e  1E 00 90 	mov [w14+2],w0
 1195 000550  3E 11 90 	mov [w14+38],w2
 1196 000552  00 00 40 	add w0,w0,w0
 1197 000554  EE 80 78 	mov [w14+w1],w1
 1198 000556  00 00 41 	add w2,w0,w0
 1199 000558  01 08 78 	mov w1,[w0]
 1200              	.loc 1 321 0
 1201 00055a  1E 0F E8 	inc [w14],[w14]
 1202 00055c  1E 00 90 	mov [w14+2],w0
 1203 00055e  00 00 E8 	inc w0,w0
 1204 000560  10 07 98 	mov w0,[w14+2]
 1205              	.L36:
 1206 000562  00 00 00 	nop 
 1207 000564  9E 00 78 	mov [w14],w1
 1208 000566  00 00 00 	nop 
 1209 000568  4E 11 90 	mov [w14+40],w2
MPLAB XC16 ASSEMBLY Listing:   			page 28


 1210 00056a  00 00 00 	nop 
 1211 00056c  5E 10 90 	mov [w14+42],w0
 1212 00056e  00 00 41 	add w2,w0,w0
 1213 000570  80 8F 50 	sub w1,w0,[w15]
 1214              	.set ___BP___,0
 1215 000572  00 00 39 	bra ltu,.L37
 323:lib/lib_pic33e/ADC.c ****     }
 324:lib/lib_pic33e/ADC.c **** 
 325:lib/lib_pic33e/ADC.c ****     return 0;
 1216              	.loc 1 325 0
 1217 000574  00 00 EB 	clr w0
 1218              	.L35:
 326:lib/lib_pic33e/ADC.c **** }
 1219              	.loc 1 326 0
 1220 000576  8E 07 78 	mov w14,w15
 1221 000578  4F 07 78 	mov [--w15],w14
 1222 00057a  00 40 A9 	bclr CORCON,#2
 1223 00057c  00 00 06 	return 
 1224              	.set ___PA___,0
 1225              	.LFE12:
 1226              	.size _get_pin_cycling_values,.-_get_pin_cycling_values
 1227              	.section .debug_frame,info
 1228                 	.Lframe0:
 1229 0000 10 00 00 00 	.4byte .LECIE0-.LSCIE0
 1230                 	.LSCIE0:
 1231 0004 FF FF FF FF 	.4byte 0xffffffff
 1232 0008 01          	.byte 0x1
 1233 0009 00          	.byte 0
 1234 000a 01          	.uleb128 0x1
 1235 000b 02          	.sleb128 2
 1236 000c 25          	.byte 0x25
 1237 000d 12          	.byte 0x12
 1238 000e 0F          	.uleb128 0xf
 1239 000f 7E          	.sleb128 -2
 1240 0010 09          	.byte 0x9
 1241 0011 25          	.uleb128 0x25
 1242 0012 0F          	.uleb128 0xf
 1243 0013 00          	.align 4
 1244                 	.LECIE0:
 1245                 	.LSFDE0:
 1246 0014 20 00 00 00 	.4byte .LEFDE0-.LASFDE0
 1247                 	.LASFDE0:
 1248 0018 00 00 00 00 	.4byte .Lframe0
 1249 001c 00 00 00 00 	.4byte .LFB0
 1250 0020 34 01 00 00 	.4byte .LFE0-.LFB0
 1251 0024 04          	.byte 0x4
 1252 0025 02 00 00 00 	.4byte .LCFI0-.LFB0
 1253 0029 13          	.byte 0x13
 1254 002a 7D          	.sleb128 -3
 1255 002b 0D          	.byte 0xd
 1256 002c 0E          	.uleb128 0xe
 1257 002d 8E          	.byte 0x8e
 1258 002e 02          	.uleb128 0x2
 1259 002f 04          	.byte 0x4
 1260 0030 04 00 00 00 	.4byte .LCFI2-.LCFI0
 1261 0034 8A          	.byte 0x8a
 1262 0035 0A          	.uleb128 0xa
MPLAB XC16 ASSEMBLY Listing:   			page 29


 1263 0036 88          	.byte 0x88
 1264 0037 08          	.uleb128 0x8
 1265                 	.align 4
 1266                 	.LEFDE0:
 1267                 	.LSFDE2:
 1268 0038 18 00 00 00 	.4byte .LEFDE2-.LASFDE2
 1269                 	.LASFDE2:
 1270 003c 00 00 00 00 	.4byte .Lframe0
 1271 0040 00 00 00 00 	.4byte .LFB1
 1272 0044 0A 00 00 00 	.4byte .LFE1-.LFB1
 1273 0048 04          	.byte 0x4
 1274 0049 02 00 00 00 	.4byte .LCFI3-.LFB1
 1275 004d 13          	.byte 0x13
 1276 004e 7D          	.sleb128 -3
 1277 004f 0D          	.byte 0xd
 1278 0050 0E          	.uleb128 0xe
 1279 0051 8E          	.byte 0x8e
 1280 0052 02          	.uleb128 0x2
 1281 0053 00          	.align 4
 1282                 	.LEFDE2:
 1283                 	.LSFDE4:
 1284 0054 2E 00 00 00 	.4byte .LEFDE4-.LASFDE4
 1285                 	.LASFDE4:
 1286 0058 00 00 00 00 	.4byte .Lframe0
 1287 005c 00 00 00 00 	.4byte .LFB2
 1288 0060 38 00 00 00 	.4byte .LFE2-.LFB2
 1289 0064 04          	.byte 0x4
 1290 0065 06 00 00 00 	.4byte .LCFI5-.LFB2
 1291 0069 13          	.byte 0x13
 1292 006a 7B          	.sleb128 -5
 1293 006b 04          	.byte 0x4
 1294 006c 02 00 00 00 	.4byte .LCFI6-.LCFI5
 1295 0070 13          	.byte 0x13
 1296 0071 79          	.sleb128 -7
 1297 0072 04          	.byte 0x4
 1298 0073 04 00 00 00 	.4byte .LCFI8-.LCFI6
 1299 0077 86          	.byte 0x86
 1300 0078 05          	.uleb128 0x5
 1301 0079 84          	.byte 0x84
 1302 007a 03          	.uleb128 0x3
 1303 007b 04          	.byte 0x4
 1304 007c 0C 00 00 00 	.4byte .LCFI9-.LCFI8
 1305 0080 13          	.byte 0x13
 1306 0081 76          	.sleb128 -10
 1307 0082 0D          	.byte 0xd
 1308 0083 0E          	.uleb128 0xe
 1309 0084 8E          	.byte 0x8e
 1310 0085 09          	.uleb128 0x9
 1311                 	.align 4
 1312                 	.LEFDE4:
 1313                 	.LSFDE6:
 1314 0086 18 00 00 00 	.4byte .LEFDE6-.LASFDE6
 1315                 	.LASFDE6:
 1316 008a 00 00 00 00 	.4byte .Lframe0
 1317 008e 00 00 00 00 	.4byte .LFB3
 1318 0092 20 00 00 00 	.4byte .LFE3-.LFB3
 1319 0096 04          	.byte 0x4
MPLAB XC16 ASSEMBLY Listing:   			page 30


 1320 0097 02 00 00 00 	.4byte .LCFI10-.LFB3
 1321 009b 13          	.byte 0x13
 1322 009c 7D          	.sleb128 -3
 1323 009d 0D          	.byte 0xd
 1324 009e 0E          	.uleb128 0xe
 1325 009f 8E          	.byte 0x8e
 1326 00a0 02          	.uleb128 0x2
 1327 00a1 00          	.align 4
 1328                 	.LEFDE6:
 1329                 	.LSFDE8:
 1330 00a2 18 00 00 00 	.4byte .LEFDE8-.LASFDE8
 1331                 	.LASFDE8:
 1332 00a6 00 00 00 00 	.4byte .Lframe0
 1333 00aa 00 00 00 00 	.4byte .LFB4
 1334 00ae 0C 00 00 00 	.4byte .LFE4-.LFB4
 1335 00b2 04          	.byte 0x4
 1336 00b3 02 00 00 00 	.4byte .LCFI11-.LFB4
 1337 00b7 13          	.byte 0x13
 1338 00b8 7D          	.sleb128 -3
 1339 00b9 0D          	.byte 0xd
 1340 00ba 0E          	.uleb128 0xe
 1341 00bb 8E          	.byte 0x8e
 1342 00bc 02          	.uleb128 0x2
 1343 00bd 00          	.align 4
 1344                 	.LEFDE8:
 1345                 	.LSFDE10:
 1346 00be 18 00 00 00 	.4byte .LEFDE10-.LASFDE10
 1347                 	.LASFDE10:
 1348 00c2 00 00 00 00 	.4byte .Lframe0
 1349 00c6 00 00 00 00 	.4byte .LFB5
 1350 00ca 0C 00 00 00 	.4byte .LFE5-.LFB5
 1351 00ce 04          	.byte 0x4
 1352 00cf 02 00 00 00 	.4byte .LCFI12-.LFB5
 1353 00d3 13          	.byte 0x13
 1354 00d4 7D          	.sleb128 -3
 1355 00d5 0D          	.byte 0xd
 1356 00d6 0E          	.uleb128 0xe
 1357 00d7 8E          	.byte 0x8e
 1358 00d8 02          	.uleb128 0x2
 1359 00d9 00          	.align 4
 1360                 	.LEFDE10:
 1361                 	.LSFDE12:
 1362 00da 18 00 00 00 	.4byte .LEFDE12-.LASFDE12
 1363                 	.LASFDE12:
 1364 00de 00 00 00 00 	.4byte .Lframe0
 1365 00e2 00 00 00 00 	.4byte .LFB6
 1366 00e6 0C 00 00 00 	.4byte .LFE6-.LFB6
 1367 00ea 04          	.byte 0x4
 1368 00eb 02 00 00 00 	.4byte .LCFI13-.LFB6
 1369 00ef 13          	.byte 0x13
 1370 00f0 7D          	.sleb128 -3
 1371 00f1 0D          	.byte 0xd
 1372 00f2 0E          	.uleb128 0xe
 1373 00f3 8E          	.byte 0x8e
 1374 00f4 02          	.uleb128 0x2
 1375 00f5 00          	.align 4
 1376                 	.LEFDE12:
MPLAB XC16 ASSEMBLY Listing:   			page 31


 1377                 	.LSFDE14:
 1378 00f6 18 00 00 00 	.4byte .LEFDE14-.LASFDE14
 1379                 	.LASFDE14:
 1380 00fa 00 00 00 00 	.4byte .Lframe0
 1381 00fe 00 00 00 00 	.4byte .LFB7
 1382 0102 0C 00 00 00 	.4byte .LFE7-.LFB7
 1383 0106 04          	.byte 0x4
 1384 0107 02 00 00 00 	.4byte .LCFI14-.LFB7
 1385 010b 13          	.byte 0x13
 1386 010c 7D          	.sleb128 -3
 1387 010d 0D          	.byte 0xd
 1388 010e 0E          	.uleb128 0xe
 1389 010f 8E          	.byte 0x8e
 1390 0110 02          	.uleb128 0x2
 1391 0111 00          	.align 4
 1392                 	.LEFDE14:
 1393                 	.LSFDE16:
 1394 0112 18 00 00 00 	.4byte .LEFDE16-.LASFDE16
 1395                 	.LASFDE16:
 1396 0116 00 00 00 00 	.4byte .Lframe0
 1397 011a 00 00 00 00 	.4byte .LFB8
 1398 011e 12 00 00 00 	.4byte .LFE8-.LFB8
 1399 0122 04          	.byte 0x4
 1400 0123 02 00 00 00 	.4byte .LCFI15-.LFB8
 1401 0127 13          	.byte 0x13
 1402 0128 7D          	.sleb128 -3
 1403 0129 0D          	.byte 0xd
 1404 012a 0E          	.uleb128 0xe
 1405 012b 8E          	.byte 0x8e
 1406 012c 02          	.uleb128 0x2
 1407 012d 00          	.align 4
 1408                 	.LEFDE16:
 1409                 	.LSFDE18:
 1410 012e 18 00 00 00 	.4byte .LEFDE18-.LASFDE18
 1411                 	.LASFDE18:
 1412 0132 00 00 00 00 	.4byte .Lframe0
 1413 0136 00 00 00 00 	.4byte .LFB9
 1414 013a 66 00 00 00 	.4byte .LFE9-.LFB9
 1415 013e 04          	.byte 0x4
 1416 013f 02 00 00 00 	.4byte .LCFI16-.LFB9
 1417 0143 13          	.byte 0x13
 1418 0144 7D          	.sleb128 -3
 1419 0145 0D          	.byte 0xd
 1420 0146 0E          	.uleb128 0xe
 1421 0147 8E          	.byte 0x8e
 1422 0148 02          	.uleb128 0x2
 1423 0149 00          	.align 4
 1424                 	.LEFDE18:
 1425                 	.LSFDE20:
 1426 014a 18 00 00 00 	.4byte .LEFDE20-.LASFDE20
 1427                 	.LASFDE20:
 1428 014e 00 00 00 00 	.4byte .Lframe0
 1429 0152 00 00 00 00 	.4byte .LFB10
 1430 0156 20 00 00 00 	.4byte .LFE10-.LFB10
 1431 015a 04          	.byte 0x4
 1432 015b 02 00 00 00 	.4byte .LCFI17-.LFB10
 1433 015f 13          	.byte 0x13
MPLAB XC16 ASSEMBLY Listing:   			page 32


 1434 0160 7D          	.sleb128 -3
 1435 0161 0D          	.byte 0xd
 1436 0162 0E          	.uleb128 0xe
 1437 0163 8E          	.byte 0x8e
 1438 0164 02          	.uleb128 0x2
 1439 0165 00          	.align 4
 1440                 	.LEFDE20:
 1441                 	.LSFDE22:
 1442 0166 18 00 00 00 	.4byte .LEFDE22-.LASFDE22
 1443                 	.LASFDE22:
 1444 016a 00 00 00 00 	.4byte .Lframe0
 1445 016e 00 00 00 00 	.4byte .LFB11
 1446 0172 C8 02 00 00 	.4byte .LFE11-.LFB11
 1447 0176 04          	.byte 0x4
 1448 0177 02 00 00 00 	.4byte .LCFI18-.LFB11
 1449 017b 13          	.byte 0x13
 1450 017c 7D          	.sleb128 -3
 1451 017d 0D          	.byte 0xd
 1452 017e 0E          	.uleb128 0xe
 1453 017f 8E          	.byte 0x8e
 1454 0180 02          	.uleb128 0x2
 1455 0181 00          	.align 4
 1456                 	.LEFDE22:
 1457                 	.LSFDE24:
 1458 0182 18 00 00 00 	.4byte .LEFDE24-.LASFDE24
 1459                 	.LASFDE24:
 1460 0186 00 00 00 00 	.4byte .Lframe0
 1461 018a 00 00 00 00 	.4byte .LFB12
 1462 018e 90 00 00 00 	.4byte .LFE12-.LFB12
 1463 0192 04          	.byte 0x4
 1464 0193 02 00 00 00 	.4byte .LCFI19-.LFB12
 1465 0197 13          	.byte 0x13
 1466 0198 7D          	.sleb128 -3
 1467 0199 0D          	.byte 0xd
 1468 019a 0E          	.uleb128 0xe
 1469 019b 8E          	.byte 0x8e
 1470 019c 02          	.uleb128 0x2
 1471 019d 00          	.align 4
 1472                 	.LEFDE24:
 1473                 	.section .text,code
 1474              	.Letext0:
 1475              	.file 2 "/opt/microchip/xc16/v1.36/bin/bin/../../include/lega-c/stdint.h"
 1476              	.file 3 "/opt/microchip/xc16/v1.36/bin/bin/../../support/dsPIC33E/h/p33EP256MU806.h"
 1477              	.file 4 "lib/lib_pic33e/ADC.h"
 1478              	.section .debug_info,info
 1479 0000 9E 19 00 00 	.4byte 0x199e
 1480 0004 02 00       	.2byte 0x2
 1481 0006 00 00 00 00 	.4byte .Ldebug_abbrev0
 1482 000a 04          	.byte 0x4
 1483 000b 01          	.uleb128 0x1
 1484 000c 47 4E 55 20 	.asciz "GNU C 4.5.1 (XC16, Microchip v1.36) (A) Build date: Dec  5 2018"
 1484      43 20 34 2E 
 1484      35 2E 31 20 
 1484      28 58 43 31 
 1484      36 2C 20 4D 
 1484      69 63 72 6F 
 1484      63 68 69 70 
MPLAB XC16 ASSEMBLY Listing:   			page 33


 1484      20 76 31 2E 
 1484      33 36 29 20 
 1485 004c 01          	.byte 0x1
 1486 004d 6C 69 62 2F 	.asciz "lib/lib_pic33e/ADC.c"
 1486      6C 69 62 5F 
 1486      70 69 63 33 
 1486      33 65 2F 41 
 1486      44 43 2E 63 
 1486      00 
 1487 0062 2F 68 6F 6D 	.asciz "/home/user/Documents/FST/Programming/project_sub-zero"
 1487      65 2F 75 73 
 1487      65 72 2F 44 
 1487      6F 63 75 6D 
 1487      65 6E 74 73 
 1487      2F 46 53 54 
 1487      2F 50 72 6F 
 1487      67 72 61 6D 
 1487      6D 69 6E 67 
 1488 0098 00 00 00 00 	.4byte .Ltext0
 1489 009c 00 00 00 00 	.4byte .Letext0
 1490 00a0 00 00 00 00 	.4byte .Ldebug_line0
 1491 00a4 02          	.uleb128 0x2
 1492 00a5 01          	.byte 0x1
 1493 00a6 06          	.byte 0x6
 1494 00a7 73 69 67 6E 	.asciz "signed char"
 1494      65 64 20 63 
 1494      68 61 72 00 
 1495 00b3 02          	.uleb128 0x2
 1496 00b4 02          	.byte 0x2
 1497 00b5 05          	.byte 0x5
 1498 00b6 69 6E 74 00 	.asciz "int"
 1499 00ba 02          	.uleb128 0x2
 1500 00bb 04          	.byte 0x4
 1501 00bc 05          	.byte 0x5
 1502 00bd 6C 6F 6E 67 	.asciz "long int"
 1502      20 69 6E 74 
 1502      00 
 1503 00c6 02          	.uleb128 0x2
 1504 00c7 08          	.byte 0x8
 1505 00c8 05          	.byte 0x5
 1506 00c9 6C 6F 6E 67 	.asciz "long long int"
 1506      20 6C 6F 6E 
 1506      67 20 69 6E 
 1506      74 00 
 1507 00d7 02          	.uleb128 0x2
 1508 00d8 01          	.byte 0x1
 1509 00d9 08          	.byte 0x8
 1510 00da 75 6E 73 69 	.asciz "unsigned char"
 1510      67 6E 65 64 
 1510      20 63 68 61 
 1510      72 00 
 1511 00e8 03          	.uleb128 0x3
 1512 00e9 75 69 6E 74 	.asciz "uint16_t"
 1512      31 36 5F 74 
 1512      00 
 1513 00f2 02          	.byte 0x2
 1514 00f3 31          	.byte 0x31
MPLAB XC16 ASSEMBLY Listing:   			page 34


 1515 00f4 F8 00 00 00 	.4byte 0xf8
 1516 00f8 02          	.uleb128 0x2
 1517 00f9 02          	.byte 0x2
 1518 00fa 07          	.byte 0x7
 1519 00fb 75 6E 73 69 	.asciz "unsigned int"
 1519      67 6E 65 64 
 1519      20 69 6E 74 
 1519      00 
 1520 0108 03          	.uleb128 0x3
 1521 0109 75 69 6E 74 	.asciz "uint32_t"
 1521      33 32 5F 74 
 1521      00 
 1522 0112 02          	.byte 0x2
 1523 0113 37          	.byte 0x37
 1524 0114 18 01 00 00 	.4byte 0x118
 1525 0118 02          	.uleb128 0x2
 1526 0119 04          	.byte 0x4
 1527 011a 07          	.byte 0x7
 1528 011b 6C 6F 6E 67 	.asciz "long unsigned int"
 1528      20 75 6E 73 
 1528      69 67 6E 65 
 1528      64 20 69 6E 
 1528      74 00 
 1529 012d 02          	.uleb128 0x2
 1530 012e 08          	.byte 0x8
 1531 012f 07          	.byte 0x7
 1532 0130 6C 6F 6E 67 	.asciz "long long unsigned int"
 1532      20 6C 6F 6E 
 1532      67 20 75 6E 
 1532      73 69 67 6E 
 1532      65 64 20 69 
 1532      6E 74 00 
 1533 0147 04          	.uleb128 0x4
 1534 0148 02          	.byte 0x2
 1535 0149 03          	.byte 0x3
 1536 014a 8E 01       	.2byte 0x18e
 1537 014c B1 01 00 00 	.4byte 0x1b1
 1538 0150 05          	.uleb128 0x5
 1539 0151 54 43 53 00 	.asciz "TCS"
 1540 0155 03          	.byte 0x3
 1541 0156 90 01       	.2byte 0x190
 1542 0158 E8 00 00 00 	.4byte 0xe8
 1543 015c 02          	.byte 0x2
 1544 015d 01          	.byte 0x1
 1545 015e 0E          	.byte 0xe
 1546 015f 02          	.byte 0x2
 1547 0160 23          	.byte 0x23
 1548 0161 00          	.uleb128 0x0
 1549 0162 05          	.uleb128 0x5
 1550 0163 54 43 4B 50 	.asciz "TCKPS"
 1550      53 00 
 1551 0169 03          	.byte 0x3
 1552 016a 92 01       	.2byte 0x192
 1553 016c E8 00 00 00 	.4byte 0xe8
 1554 0170 02          	.byte 0x2
 1555 0171 02          	.byte 0x2
 1556 0172 0A          	.byte 0xa
MPLAB XC16 ASSEMBLY Listing:   			page 35


 1557 0173 02          	.byte 0x2
 1558 0174 23          	.byte 0x23
 1559 0175 00          	.uleb128 0x0
 1560 0176 05          	.uleb128 0x5
 1561 0177 54 47 41 54 	.asciz "TGATE"
 1561      45 00 
 1562 017d 03          	.byte 0x3
 1563 017e 93 01       	.2byte 0x193
 1564 0180 E8 00 00 00 	.4byte 0xe8
 1565 0184 02          	.byte 0x2
 1566 0185 01          	.byte 0x1
 1567 0186 09          	.byte 0x9
 1568 0187 02          	.byte 0x2
 1569 0188 23          	.byte 0x23
 1570 0189 00          	.uleb128 0x0
 1571 018a 05          	.uleb128 0x5
 1572 018b 54 53 49 44 	.asciz "TSIDL"
 1572      4C 00 
 1573 0191 03          	.byte 0x3
 1574 0192 95 01       	.2byte 0x195
 1575 0194 E8 00 00 00 	.4byte 0xe8
 1576 0198 02          	.byte 0x2
 1577 0199 01          	.byte 0x1
 1578 019a 02          	.byte 0x2
 1579 019b 02          	.byte 0x2
 1580 019c 23          	.byte 0x23
 1581 019d 00          	.uleb128 0x0
 1582 019e 05          	.uleb128 0x5
 1583 019f 54 4F 4E 00 	.asciz "TON"
 1584 01a3 03          	.byte 0x3
 1585 01a4 97 01       	.2byte 0x197
 1586 01a6 E8 00 00 00 	.4byte 0xe8
 1587 01aa 02          	.byte 0x2
 1588 01ab 01          	.byte 0x1
 1589 01ac 10          	.byte 0x10
 1590 01ad 02          	.byte 0x2
 1591 01ae 23          	.byte 0x23
 1592 01af 00          	.uleb128 0x0
 1593 01b0 00          	.byte 0x0
 1594 01b1 04          	.uleb128 0x4
 1595 01b2 02          	.byte 0x2
 1596 01b3 03          	.byte 0x3
 1597 01b4 99 01       	.2byte 0x199
 1598 01b6 E5 01 00 00 	.4byte 0x1e5
 1599 01ba 05          	.uleb128 0x5
 1600 01bb 54 43 4B 50 	.asciz "TCKPS0"
 1600      53 30 00 
 1601 01c2 03          	.byte 0x3
 1602 01c3 9B 01       	.2byte 0x19b
 1603 01c5 E8 00 00 00 	.4byte 0xe8
 1604 01c9 02          	.byte 0x2
 1605 01ca 01          	.byte 0x1
 1606 01cb 0B          	.byte 0xb
 1607 01cc 02          	.byte 0x2
 1608 01cd 23          	.byte 0x23
 1609 01ce 00          	.uleb128 0x0
 1610 01cf 05          	.uleb128 0x5
MPLAB XC16 ASSEMBLY Listing:   			page 36


 1611 01d0 54 43 4B 50 	.asciz "TCKPS1"
 1611      53 31 00 
 1612 01d7 03          	.byte 0x3
 1613 01d8 9C 01       	.2byte 0x19c
 1614 01da E8 00 00 00 	.4byte 0xe8
 1615 01de 02          	.byte 0x2
 1616 01df 01          	.byte 0x1
 1617 01e0 0A          	.byte 0xa
 1618 01e1 02          	.byte 0x2
 1619 01e2 23          	.byte 0x23
 1620 01e3 00          	.uleb128 0x0
 1621 01e4 00          	.byte 0x0
 1622 01e5 06          	.uleb128 0x6
 1623 01e6 02          	.byte 0x2
 1624 01e7 03          	.byte 0x3
 1625 01e8 8D 01       	.2byte 0x18d
 1626 01ea F9 01 00 00 	.4byte 0x1f9
 1627 01ee 07          	.uleb128 0x7
 1628 01ef 47 01 00 00 	.4byte 0x147
 1629 01f3 07          	.uleb128 0x7
 1630 01f4 B1 01 00 00 	.4byte 0x1b1
 1631 01f8 00          	.byte 0x0
 1632 01f9 08          	.uleb128 0x8
 1633 01fa 74 61 67 54 	.asciz "tagT5CONBITS"
 1633      35 43 4F 4E 
 1633      42 49 54 53 
 1633      00 
 1634 0207 02          	.byte 0x2
 1635 0208 03          	.byte 0x3
 1636 0209 8C 01       	.2byte 0x18c
 1637 020b 18 02 00 00 	.4byte 0x218
 1638 020f 09          	.uleb128 0x9
 1639 0210 E5 01 00 00 	.4byte 0x1e5
 1640 0214 02          	.byte 0x2
 1641 0215 23          	.byte 0x23
 1642 0216 00          	.uleb128 0x0
 1643 0217 00          	.byte 0x0
 1644 0218 0A          	.uleb128 0xa
 1645 0219 54 35 43 4F 	.asciz "T5CONBITS"
 1645      4E 42 49 54 
 1645      53 00 
 1646 0223 03          	.byte 0x3
 1647 0224 9F 01       	.2byte 0x19f
 1648 0226 F9 01 00 00 	.4byte 0x1f9
 1649 022a 04          	.uleb128 0x4
 1650 022b 02          	.byte 0x2
 1651 022c 03          	.byte 0x3
 1652 022d A3 09       	.2byte 0x9a3
 1653 022f 0E 03 00 00 	.4byte 0x30e
 1654 0233 05          	.uleb128 0x5
 1655 0234 44 4F 4E 45 	.asciz "DONE"
 1655      00 
 1656 0239 03          	.byte 0x3
 1657 023a A4 09       	.2byte 0x9a4
 1658 023c E8 00 00 00 	.4byte 0xe8
 1659 0240 02          	.byte 0x2
 1660 0241 01          	.byte 0x1
MPLAB XC16 ASSEMBLY Listing:   			page 37


 1661 0242 0F          	.byte 0xf
 1662 0243 02          	.byte 0x2
 1663 0244 23          	.byte 0x23
 1664 0245 00          	.uleb128 0x0
 1665 0246 05          	.uleb128 0x5
 1666 0247 53 41 4D 50 	.asciz "SAMP"
 1666      00 
 1667 024c 03          	.byte 0x3
 1668 024d A5 09       	.2byte 0x9a5
 1669 024f E8 00 00 00 	.4byte 0xe8
 1670 0253 02          	.byte 0x2
 1671 0254 01          	.byte 0x1
 1672 0255 0E          	.byte 0xe
 1673 0256 02          	.byte 0x2
 1674 0257 23          	.byte 0x23
 1675 0258 00          	.uleb128 0x0
 1676 0259 05          	.uleb128 0x5
 1677 025a 41 53 41 4D 	.asciz "ASAM"
 1677      00 
 1678 025f 03          	.byte 0x3
 1679 0260 A6 09       	.2byte 0x9a6
 1680 0262 E8 00 00 00 	.4byte 0xe8
 1681 0266 02          	.byte 0x2
 1682 0267 01          	.byte 0x1
 1683 0268 0D          	.byte 0xd
 1684 0269 02          	.byte 0x2
 1685 026a 23          	.byte 0x23
 1686 026b 00          	.uleb128 0x0
 1687 026c 05          	.uleb128 0x5
 1688 026d 53 49 4D 53 	.asciz "SIMSAM"
 1688      41 4D 00 
 1689 0274 03          	.byte 0x3
 1690 0275 A7 09       	.2byte 0x9a7
 1691 0277 E8 00 00 00 	.4byte 0xe8
 1692 027b 02          	.byte 0x2
 1693 027c 01          	.byte 0x1
 1694 027d 0C          	.byte 0xc
 1695 027e 02          	.byte 0x2
 1696 027f 23          	.byte 0x23
 1697 0280 00          	.uleb128 0x0
 1698 0281 05          	.uleb128 0x5
 1699 0282 53 53 52 43 	.asciz "SSRCG"
 1699      47 00 
 1700 0288 03          	.byte 0x3
 1701 0289 A8 09       	.2byte 0x9a8
 1702 028b E8 00 00 00 	.4byte 0xe8
 1703 028f 02          	.byte 0x2
 1704 0290 01          	.byte 0x1
 1705 0291 0B          	.byte 0xb
 1706 0292 02          	.byte 0x2
 1707 0293 23          	.byte 0x23
 1708 0294 00          	.uleb128 0x0
 1709 0295 05          	.uleb128 0x5
 1710 0296 53 53 52 43 	.asciz "SSRC"
 1710      00 
 1711 029b 03          	.byte 0x3
 1712 029c A9 09       	.2byte 0x9a9
MPLAB XC16 ASSEMBLY Listing:   			page 38


 1713 029e E8 00 00 00 	.4byte 0xe8
 1714 02a2 02          	.byte 0x2
 1715 02a3 03          	.byte 0x3
 1716 02a4 08          	.byte 0x8
 1717 02a5 02          	.byte 0x2
 1718 02a6 23          	.byte 0x23
 1719 02a7 00          	.uleb128 0x0
 1720 02a8 05          	.uleb128 0x5
 1721 02a9 46 4F 52 4D 	.asciz "FORM"
 1721      00 
 1722 02ae 03          	.byte 0x3
 1723 02af AA 09       	.2byte 0x9aa
 1724 02b1 E8 00 00 00 	.4byte 0xe8
 1725 02b5 02          	.byte 0x2
 1726 02b6 02          	.byte 0x2
 1727 02b7 06          	.byte 0x6
 1728 02b8 02          	.byte 0x2
 1729 02b9 23          	.byte 0x23
 1730 02ba 00          	.uleb128 0x0
 1731 02bb 05          	.uleb128 0x5
 1732 02bc 41 44 31 32 	.asciz "AD12B"
 1732      42 00 
 1733 02c2 03          	.byte 0x3
 1734 02c3 AB 09       	.2byte 0x9ab
 1735 02c5 E8 00 00 00 	.4byte 0xe8
 1736 02c9 02          	.byte 0x2
 1737 02ca 01          	.byte 0x1
 1738 02cb 05          	.byte 0x5
 1739 02cc 02          	.byte 0x2
 1740 02cd 23          	.byte 0x23
 1741 02ce 00          	.uleb128 0x0
 1742 02cf 05          	.uleb128 0x5
 1743 02d0 41 44 44 4D 	.asciz "ADDMABM"
 1743      41 42 4D 00 
 1744 02d8 03          	.byte 0x3
 1745 02d9 AD 09       	.2byte 0x9ad
 1746 02db E8 00 00 00 	.4byte 0xe8
 1747 02df 02          	.byte 0x2
 1748 02e0 01          	.byte 0x1
 1749 02e1 03          	.byte 0x3
 1750 02e2 02          	.byte 0x2
 1751 02e3 23          	.byte 0x23
 1752 02e4 00          	.uleb128 0x0
 1753 02e5 05          	.uleb128 0x5
 1754 02e6 41 44 53 49 	.asciz "ADSIDL"
 1754      44 4C 00 
 1755 02ed 03          	.byte 0x3
 1756 02ee AE 09       	.2byte 0x9ae
 1757 02f0 E8 00 00 00 	.4byte 0xe8
 1758 02f4 02          	.byte 0x2
 1759 02f5 01          	.byte 0x1
 1760 02f6 02          	.byte 0x2
 1761 02f7 02          	.byte 0x2
 1762 02f8 23          	.byte 0x23
 1763 02f9 00          	.uleb128 0x0
 1764 02fa 05          	.uleb128 0x5
 1765 02fb 41 44 4F 4E 	.asciz "ADON"
MPLAB XC16 ASSEMBLY Listing:   			page 39


 1765      00 
 1766 0300 03          	.byte 0x3
 1767 0301 B0 09       	.2byte 0x9b0
 1768 0303 E8 00 00 00 	.4byte 0xe8
 1769 0307 02          	.byte 0x2
 1770 0308 01          	.byte 0x1
 1771 0309 10          	.byte 0x10
 1772 030a 02          	.byte 0x2
 1773 030b 23          	.byte 0x23
 1774 030c 00          	.uleb128 0x0
 1775 030d 00          	.byte 0x0
 1776 030e 04          	.uleb128 0x4
 1777 030f 02          	.byte 0x2
 1778 0310 03          	.byte 0x3
 1779 0311 B2 09       	.2byte 0x9b2
 1780 0313 7C 03 00 00 	.4byte 0x37c
 1781 0317 05          	.uleb128 0x5
 1782 0318 53 53 52 43 	.asciz "SSRC0"
 1782      30 00 
 1783 031e 03          	.byte 0x3
 1784 031f B4 09       	.2byte 0x9b4
 1785 0321 E8 00 00 00 	.4byte 0xe8
 1786 0325 02          	.byte 0x2
 1787 0326 01          	.byte 0x1
 1788 0327 0A          	.byte 0xa
 1789 0328 02          	.byte 0x2
 1790 0329 23          	.byte 0x23
 1791 032a 00          	.uleb128 0x0
 1792 032b 05          	.uleb128 0x5
 1793 032c 53 53 52 43 	.asciz "SSRC1"
 1793      31 00 
 1794 0332 03          	.byte 0x3
 1795 0333 B5 09       	.2byte 0x9b5
 1796 0335 E8 00 00 00 	.4byte 0xe8
 1797 0339 02          	.byte 0x2
 1798 033a 01          	.byte 0x1
 1799 033b 09          	.byte 0x9
 1800 033c 02          	.byte 0x2
 1801 033d 23          	.byte 0x23
 1802 033e 00          	.uleb128 0x0
 1803 033f 05          	.uleb128 0x5
 1804 0340 53 53 52 43 	.asciz "SSRC2"
 1804      32 00 
 1805 0346 03          	.byte 0x3
 1806 0347 B6 09       	.2byte 0x9b6
 1807 0349 E8 00 00 00 	.4byte 0xe8
 1808 034d 02          	.byte 0x2
 1809 034e 01          	.byte 0x1
 1810 034f 08          	.byte 0x8
 1811 0350 02          	.byte 0x2
 1812 0351 23          	.byte 0x23
 1813 0352 00          	.uleb128 0x0
 1814 0353 05          	.uleb128 0x5
 1815 0354 46 4F 52 4D 	.asciz "FORM0"
 1815      30 00 
 1816 035a 03          	.byte 0x3
 1817 035b B7 09       	.2byte 0x9b7
MPLAB XC16 ASSEMBLY Listing:   			page 40


 1818 035d E8 00 00 00 	.4byte 0xe8
 1819 0361 02          	.byte 0x2
 1820 0362 01          	.byte 0x1
 1821 0363 07          	.byte 0x7
 1822 0364 02          	.byte 0x2
 1823 0365 23          	.byte 0x23
 1824 0366 00          	.uleb128 0x0
 1825 0367 05          	.uleb128 0x5
 1826 0368 46 4F 52 4D 	.asciz "FORM1"
 1826      31 00 
 1827 036e 03          	.byte 0x3
 1828 036f B8 09       	.2byte 0x9b8
 1829 0371 E8 00 00 00 	.4byte 0xe8
 1830 0375 02          	.byte 0x2
 1831 0376 01          	.byte 0x1
 1832 0377 06          	.byte 0x6
 1833 0378 02          	.byte 0x2
 1834 0379 23          	.byte 0x23
 1835 037a 00          	.uleb128 0x0
 1836 037b 00          	.byte 0x0
 1837 037c 06          	.uleb128 0x6
 1838 037d 02          	.byte 0x2
 1839 037e 03          	.byte 0x3
 1840 037f A2 09       	.2byte 0x9a2
 1841 0381 90 03 00 00 	.4byte 0x390
 1842 0385 07          	.uleb128 0x7
 1843 0386 2A 02 00 00 	.4byte 0x22a
 1844 038a 07          	.uleb128 0x7
 1845 038b 0E 03 00 00 	.4byte 0x30e
 1846 038f 00          	.byte 0x0
 1847 0390 08          	.uleb128 0x8
 1848 0391 74 61 67 41 	.asciz "tagAD1CON1BITS"
 1848      44 31 43 4F 
 1848      4E 31 42 49 
 1848      54 53 00 
 1849 03a0 02          	.byte 0x2
 1850 03a1 03          	.byte 0x3
 1851 03a2 A1 09       	.2byte 0x9a1
 1852 03a4 B1 03 00 00 	.4byte 0x3b1
 1853 03a8 09          	.uleb128 0x9
 1854 03a9 7C 03 00 00 	.4byte 0x37c
 1855 03ad 02          	.byte 0x2
 1856 03ae 23          	.byte 0x23
 1857 03af 00          	.uleb128 0x0
 1858 03b0 00          	.byte 0x0
 1859 03b1 0A          	.uleb128 0xa
 1860 03b2 41 44 31 43 	.asciz "AD1CON1BITS"
 1860      4F 4E 31 42 
 1860      49 54 53 00 
 1861 03be 03          	.byte 0x3
 1862 03bf BB 09       	.2byte 0x9bb
 1863 03c1 90 03 00 00 	.4byte 0x390
 1864 03c5 04          	.uleb128 0x4
 1865 03c6 02          	.byte 0x2
 1866 03c7 03          	.byte 0x3
 1867 03c8 C2 09       	.2byte 0x9c2
 1868 03ca 55 04 00 00 	.4byte 0x455
MPLAB XC16 ASSEMBLY Listing:   			page 41


 1869 03ce 05          	.uleb128 0x5
 1870 03cf 41 4C 54 53 	.asciz "ALTS"
 1870      00 
 1871 03d4 03          	.byte 0x3
 1872 03d5 C3 09       	.2byte 0x9c3
 1873 03d7 E8 00 00 00 	.4byte 0xe8
 1874 03db 02          	.byte 0x2
 1875 03dc 01          	.byte 0x1
 1876 03dd 0F          	.byte 0xf
 1877 03de 02          	.byte 0x2
 1878 03df 23          	.byte 0x23
 1879 03e0 00          	.uleb128 0x0
 1880 03e1 05          	.uleb128 0x5
 1881 03e2 42 55 46 4D 	.asciz "BUFM"
 1881      00 
 1882 03e7 03          	.byte 0x3
 1883 03e8 C4 09       	.2byte 0x9c4
 1884 03ea E8 00 00 00 	.4byte 0xe8
 1885 03ee 02          	.byte 0x2
 1886 03ef 01          	.byte 0x1
 1887 03f0 0E          	.byte 0xe
 1888 03f1 02          	.byte 0x2
 1889 03f2 23          	.byte 0x23
 1890 03f3 00          	.uleb128 0x0
 1891 03f4 05          	.uleb128 0x5
 1892 03f5 53 4D 50 49 	.asciz "SMPI"
 1892      00 
 1893 03fa 03          	.byte 0x3
 1894 03fb C5 09       	.2byte 0x9c5
 1895 03fd E8 00 00 00 	.4byte 0xe8
 1896 0401 02          	.byte 0x2
 1897 0402 05          	.byte 0x5
 1898 0403 09          	.byte 0x9
 1899 0404 02          	.byte 0x2
 1900 0405 23          	.byte 0x23
 1901 0406 00          	.uleb128 0x0
 1902 0407 05          	.uleb128 0x5
 1903 0408 42 55 46 53 	.asciz "BUFS"
 1903      00 
 1904 040d 03          	.byte 0x3
 1905 040e C6 09       	.2byte 0x9c6
 1906 0410 E8 00 00 00 	.4byte 0xe8
 1907 0414 02          	.byte 0x2
 1908 0415 01          	.byte 0x1
 1909 0416 08          	.byte 0x8
 1910 0417 02          	.byte 0x2
 1911 0418 23          	.byte 0x23
 1912 0419 00          	.uleb128 0x0
 1913 041a 05          	.uleb128 0x5
 1914 041b 43 48 50 53 	.asciz "CHPS"
 1914      00 
 1915 0420 03          	.byte 0x3
 1916 0421 C7 09       	.2byte 0x9c7
 1917 0423 E8 00 00 00 	.4byte 0xe8
 1918 0427 02          	.byte 0x2
 1919 0428 02          	.byte 0x2
 1920 0429 06          	.byte 0x6
MPLAB XC16 ASSEMBLY Listing:   			page 42


 1921 042a 02          	.byte 0x2
 1922 042b 23          	.byte 0x23
 1923 042c 00          	.uleb128 0x0
 1924 042d 05          	.uleb128 0x5
 1925 042e 43 53 43 4E 	.asciz "CSCNA"
 1925      41 00 
 1926 0434 03          	.byte 0x3
 1927 0435 C8 09       	.2byte 0x9c8
 1928 0437 E8 00 00 00 	.4byte 0xe8
 1929 043b 02          	.byte 0x2
 1930 043c 01          	.byte 0x1
 1931 043d 05          	.byte 0x5
 1932 043e 02          	.byte 0x2
 1933 043f 23          	.byte 0x23
 1934 0440 00          	.uleb128 0x0
 1935 0441 05          	.uleb128 0x5
 1936 0442 56 43 46 47 	.asciz "VCFG"
 1936      00 
 1937 0447 03          	.byte 0x3
 1938 0448 CA 09       	.2byte 0x9ca
 1939 044a E8 00 00 00 	.4byte 0xe8
 1940 044e 02          	.byte 0x2
 1941 044f 03          	.byte 0x3
 1942 0450 10          	.byte 0x10
 1943 0451 02          	.byte 0x2
 1944 0452 23          	.byte 0x23
 1945 0453 00          	.uleb128 0x0
 1946 0454 00          	.byte 0x0
 1947 0455 04          	.uleb128 0x4
 1948 0456 02          	.byte 0x2
 1949 0457 03          	.byte 0x3
 1950 0458 CC 09       	.2byte 0x9cc
 1951 045a 27 05 00 00 	.4byte 0x527
 1952 045e 05          	.uleb128 0x5
 1953 045f 53 4D 50 49 	.asciz "SMPI0"
 1953      30 00 
 1954 0465 03          	.byte 0x3
 1955 0466 CE 09       	.2byte 0x9ce
 1956 0468 E8 00 00 00 	.4byte 0xe8
 1957 046c 02          	.byte 0x2
 1958 046d 01          	.byte 0x1
 1959 046e 0D          	.byte 0xd
 1960 046f 02          	.byte 0x2
 1961 0470 23          	.byte 0x23
 1962 0471 00          	.uleb128 0x0
 1963 0472 05          	.uleb128 0x5
 1964 0473 53 4D 50 49 	.asciz "SMPI1"
 1964      31 00 
 1965 0479 03          	.byte 0x3
 1966 047a CF 09       	.2byte 0x9cf
 1967 047c E8 00 00 00 	.4byte 0xe8
 1968 0480 02          	.byte 0x2
 1969 0481 01          	.byte 0x1
 1970 0482 0C          	.byte 0xc
 1971 0483 02          	.byte 0x2
 1972 0484 23          	.byte 0x23
 1973 0485 00          	.uleb128 0x0
MPLAB XC16 ASSEMBLY Listing:   			page 43


 1974 0486 05          	.uleb128 0x5
 1975 0487 53 4D 50 49 	.asciz "SMPI2"
 1975      32 00 
 1976 048d 03          	.byte 0x3
 1977 048e D0 09       	.2byte 0x9d0
 1978 0490 E8 00 00 00 	.4byte 0xe8
 1979 0494 02          	.byte 0x2
 1980 0495 01          	.byte 0x1
 1981 0496 0B          	.byte 0xb
 1982 0497 02          	.byte 0x2
 1983 0498 23          	.byte 0x23
 1984 0499 00          	.uleb128 0x0
 1985 049a 05          	.uleb128 0x5
 1986 049b 53 4D 50 49 	.asciz "SMPI3"
 1986      33 00 
 1987 04a1 03          	.byte 0x3
 1988 04a2 D1 09       	.2byte 0x9d1
 1989 04a4 E8 00 00 00 	.4byte 0xe8
 1990 04a8 02          	.byte 0x2
 1991 04a9 01          	.byte 0x1
 1992 04aa 0A          	.byte 0xa
 1993 04ab 02          	.byte 0x2
 1994 04ac 23          	.byte 0x23
 1995 04ad 00          	.uleb128 0x0
 1996 04ae 05          	.uleb128 0x5
 1997 04af 53 4D 50 49 	.asciz "SMPI4"
 1997      34 00 
 1998 04b5 03          	.byte 0x3
 1999 04b6 D2 09       	.2byte 0x9d2
 2000 04b8 E8 00 00 00 	.4byte 0xe8
 2001 04bc 02          	.byte 0x2
 2002 04bd 01          	.byte 0x1
 2003 04be 09          	.byte 0x9
 2004 04bf 02          	.byte 0x2
 2005 04c0 23          	.byte 0x23
 2006 04c1 00          	.uleb128 0x0
 2007 04c2 05          	.uleb128 0x5
 2008 04c3 43 48 50 53 	.asciz "CHPS0"
 2008      30 00 
 2009 04c9 03          	.byte 0x3
 2010 04ca D4 09       	.2byte 0x9d4
 2011 04cc E8 00 00 00 	.4byte 0xe8
 2012 04d0 02          	.byte 0x2
 2013 04d1 01          	.byte 0x1
 2014 04d2 07          	.byte 0x7
 2015 04d3 02          	.byte 0x2
 2016 04d4 23          	.byte 0x23
 2017 04d5 00          	.uleb128 0x0
 2018 04d6 05          	.uleb128 0x5
 2019 04d7 43 48 50 53 	.asciz "CHPS1"
 2019      31 00 
 2020 04dd 03          	.byte 0x3
 2021 04de D5 09       	.2byte 0x9d5
 2022 04e0 E8 00 00 00 	.4byte 0xe8
 2023 04e4 02          	.byte 0x2
 2024 04e5 01          	.byte 0x1
 2025 04e6 06          	.byte 0x6
MPLAB XC16 ASSEMBLY Listing:   			page 44


 2026 04e7 02          	.byte 0x2
 2027 04e8 23          	.byte 0x23
 2028 04e9 00          	.uleb128 0x0
 2029 04ea 05          	.uleb128 0x5
 2030 04eb 56 43 46 47 	.asciz "VCFG0"
 2030      30 00 
 2031 04f1 03          	.byte 0x3
 2032 04f2 D7 09       	.2byte 0x9d7
 2033 04f4 E8 00 00 00 	.4byte 0xe8
 2034 04f8 02          	.byte 0x2
 2035 04f9 01          	.byte 0x1
 2036 04fa 02          	.byte 0x2
 2037 04fb 02          	.byte 0x2
 2038 04fc 23          	.byte 0x23
 2039 04fd 00          	.uleb128 0x0
 2040 04fe 05          	.uleb128 0x5
 2041 04ff 56 43 46 47 	.asciz "VCFG1"
 2041      31 00 
 2042 0505 03          	.byte 0x3
 2043 0506 D8 09       	.2byte 0x9d8
 2044 0508 E8 00 00 00 	.4byte 0xe8
 2045 050c 02          	.byte 0x2
 2046 050d 01          	.byte 0x1
 2047 050e 01          	.byte 0x1
 2048 050f 02          	.byte 0x2
 2049 0510 23          	.byte 0x23
 2050 0511 00          	.uleb128 0x0
 2051 0512 05          	.uleb128 0x5
 2052 0513 56 43 46 47 	.asciz "VCFG2"
 2052      32 00 
 2053 0519 03          	.byte 0x3
 2054 051a D9 09       	.2byte 0x9d9
 2055 051c E8 00 00 00 	.4byte 0xe8
 2056 0520 02          	.byte 0x2
 2057 0521 01          	.byte 0x1
 2058 0522 10          	.byte 0x10
 2059 0523 02          	.byte 0x2
 2060 0524 23          	.byte 0x23
 2061 0525 00          	.uleb128 0x0
 2062 0526 00          	.byte 0x0
 2063 0527 06          	.uleb128 0x6
 2064 0528 02          	.byte 0x2
 2065 0529 03          	.byte 0x3
 2066 052a C1 09       	.2byte 0x9c1
 2067 052c 3B 05 00 00 	.4byte 0x53b
 2068 0530 07          	.uleb128 0x7
 2069 0531 C5 03 00 00 	.4byte 0x3c5
 2070 0535 07          	.uleb128 0x7
 2071 0536 55 04 00 00 	.4byte 0x455
 2072 053a 00          	.byte 0x0
 2073 053b 08          	.uleb128 0x8
 2074 053c 74 61 67 41 	.asciz "tagAD1CON2BITS"
 2074      44 31 43 4F 
 2074      4E 32 42 49 
 2074      54 53 00 
 2075 054b 02          	.byte 0x2
 2076 054c 03          	.byte 0x3
MPLAB XC16 ASSEMBLY Listing:   			page 45


 2077 054d C0 09       	.2byte 0x9c0
 2078 054f 5C 05 00 00 	.4byte 0x55c
 2079 0553 09          	.uleb128 0x9
 2080 0554 27 05 00 00 	.4byte 0x527
 2081 0558 02          	.byte 0x2
 2082 0559 23          	.byte 0x23
 2083 055a 00          	.uleb128 0x0
 2084 055b 00          	.byte 0x0
 2085 055c 0A          	.uleb128 0xa
 2086 055d 41 44 31 43 	.asciz "AD1CON2BITS"
 2086      4F 4E 32 42 
 2086      49 54 53 00 
 2087 0569 03          	.byte 0x3
 2088 056a DC 09       	.2byte 0x9dc
 2089 056c 3B 05 00 00 	.4byte 0x53b
 2090 0570 04          	.uleb128 0x4
 2091 0571 02          	.byte 0x2
 2092 0572 03          	.byte 0x3
 2093 0573 E3 09       	.2byte 0x9e3
 2094 0575 B3 05 00 00 	.4byte 0x5b3
 2095 0579 05          	.uleb128 0x5
 2096 057a 41 44 43 53 	.asciz "ADCS"
 2096      00 
 2097 057f 03          	.byte 0x3
 2098 0580 E4 09       	.2byte 0x9e4
 2099 0582 E8 00 00 00 	.4byte 0xe8
 2100 0586 02          	.byte 0x2
 2101 0587 08          	.byte 0x8
 2102 0588 08          	.byte 0x8
 2103 0589 02          	.byte 0x2
 2104 058a 23          	.byte 0x23
 2105 058b 00          	.uleb128 0x0
 2106 058c 05          	.uleb128 0x5
 2107 058d 53 41 4D 43 	.asciz "SAMC"
 2107      00 
 2108 0592 03          	.byte 0x3
 2109 0593 E5 09       	.2byte 0x9e5
 2110 0595 E8 00 00 00 	.4byte 0xe8
 2111 0599 02          	.byte 0x2
 2112 059a 05          	.byte 0x5
 2113 059b 03          	.byte 0x3
 2114 059c 02          	.byte 0x2
 2115 059d 23          	.byte 0x23
 2116 059e 00          	.uleb128 0x0
 2117 059f 05          	.uleb128 0x5
 2118 05a0 41 44 52 43 	.asciz "ADRC"
 2118      00 
 2119 05a5 03          	.byte 0x3
 2120 05a6 E7 09       	.2byte 0x9e7
 2121 05a8 E8 00 00 00 	.4byte 0xe8
 2122 05ac 02          	.byte 0x2
 2123 05ad 01          	.byte 0x1
 2124 05ae 10          	.byte 0x10
 2125 05af 02          	.byte 0x2
 2126 05b0 23          	.byte 0x23
 2127 05b1 00          	.uleb128 0x0
 2128 05b2 00          	.byte 0x0
MPLAB XC16 ASSEMBLY Listing:   			page 46


 2129 05b3 04          	.uleb128 0x4
 2130 05b4 02          	.byte 0x2
 2131 05b5 03          	.byte 0x3
 2132 05b6 E9 09       	.2byte 0x9e9
 2133 05b8 C1 06 00 00 	.4byte 0x6c1
 2134 05bc 05          	.uleb128 0x5
 2135 05bd 41 44 43 53 	.asciz "ADCS0"
 2135      30 00 
 2136 05c3 03          	.byte 0x3
 2137 05c4 EA 09       	.2byte 0x9ea
 2138 05c6 E8 00 00 00 	.4byte 0xe8
 2139 05ca 02          	.byte 0x2
 2140 05cb 01          	.byte 0x1
 2141 05cc 0F          	.byte 0xf
 2142 05cd 02          	.byte 0x2
 2143 05ce 23          	.byte 0x23
 2144 05cf 00          	.uleb128 0x0
 2145 05d0 05          	.uleb128 0x5
 2146 05d1 41 44 43 53 	.asciz "ADCS1"
 2146      31 00 
 2147 05d7 03          	.byte 0x3
 2148 05d8 EB 09       	.2byte 0x9eb
 2149 05da E8 00 00 00 	.4byte 0xe8
 2150 05de 02          	.byte 0x2
 2151 05df 01          	.byte 0x1
 2152 05e0 0E          	.byte 0xe
 2153 05e1 02          	.byte 0x2
 2154 05e2 23          	.byte 0x23
 2155 05e3 00          	.uleb128 0x0
 2156 05e4 05          	.uleb128 0x5
 2157 05e5 41 44 43 53 	.asciz "ADCS2"
 2157      32 00 
 2158 05eb 03          	.byte 0x3
 2159 05ec EC 09       	.2byte 0x9ec
 2160 05ee E8 00 00 00 	.4byte 0xe8
 2161 05f2 02          	.byte 0x2
 2162 05f3 01          	.byte 0x1
 2163 05f4 0D          	.byte 0xd
 2164 05f5 02          	.byte 0x2
 2165 05f6 23          	.byte 0x23
 2166 05f7 00          	.uleb128 0x0
 2167 05f8 05          	.uleb128 0x5
 2168 05f9 41 44 43 53 	.asciz "ADCS3"
 2168      33 00 
 2169 05ff 03          	.byte 0x3
 2170 0600 ED 09       	.2byte 0x9ed
 2171 0602 E8 00 00 00 	.4byte 0xe8
 2172 0606 02          	.byte 0x2
 2173 0607 01          	.byte 0x1
 2174 0608 0C          	.byte 0xc
 2175 0609 02          	.byte 0x2
 2176 060a 23          	.byte 0x23
 2177 060b 00          	.uleb128 0x0
 2178 060c 05          	.uleb128 0x5
 2179 060d 41 44 43 53 	.asciz "ADCS4"
 2179      34 00 
 2180 0613 03          	.byte 0x3
MPLAB XC16 ASSEMBLY Listing:   			page 47


 2181 0614 EE 09       	.2byte 0x9ee
 2182 0616 E8 00 00 00 	.4byte 0xe8
 2183 061a 02          	.byte 0x2
 2184 061b 01          	.byte 0x1
 2185 061c 0B          	.byte 0xb
 2186 061d 02          	.byte 0x2
 2187 061e 23          	.byte 0x23
 2188 061f 00          	.uleb128 0x0
 2189 0620 05          	.uleb128 0x5
 2190 0621 41 44 43 53 	.asciz "ADCS5"
 2190      35 00 
 2191 0627 03          	.byte 0x3
 2192 0628 EF 09       	.2byte 0x9ef
 2193 062a E8 00 00 00 	.4byte 0xe8
 2194 062e 02          	.byte 0x2
 2195 062f 01          	.byte 0x1
 2196 0630 0A          	.byte 0xa
 2197 0631 02          	.byte 0x2
 2198 0632 23          	.byte 0x23
 2199 0633 00          	.uleb128 0x0
 2200 0634 05          	.uleb128 0x5
 2201 0635 41 44 43 53 	.asciz "ADCS6"
 2201      36 00 
 2202 063b 03          	.byte 0x3
 2203 063c F0 09       	.2byte 0x9f0
 2204 063e E8 00 00 00 	.4byte 0xe8
 2205 0642 02          	.byte 0x2
 2206 0643 01          	.byte 0x1
 2207 0644 09          	.byte 0x9
 2208 0645 02          	.byte 0x2
 2209 0646 23          	.byte 0x23
 2210 0647 00          	.uleb128 0x0
 2211 0648 05          	.uleb128 0x5
 2212 0649 41 44 43 53 	.asciz "ADCS7"
 2212      37 00 
 2213 064f 03          	.byte 0x3
 2214 0650 F1 09       	.2byte 0x9f1
 2215 0652 E8 00 00 00 	.4byte 0xe8
 2216 0656 02          	.byte 0x2
 2217 0657 01          	.byte 0x1
 2218 0658 08          	.byte 0x8
 2219 0659 02          	.byte 0x2
 2220 065a 23          	.byte 0x23
 2221 065b 00          	.uleb128 0x0
 2222 065c 05          	.uleb128 0x5
 2223 065d 53 41 4D 43 	.asciz "SAMC0"
 2223      30 00 
 2224 0663 03          	.byte 0x3
 2225 0664 F2 09       	.2byte 0x9f2
 2226 0666 E8 00 00 00 	.4byte 0xe8
 2227 066a 02          	.byte 0x2
 2228 066b 01          	.byte 0x1
 2229 066c 07          	.byte 0x7
 2230 066d 02          	.byte 0x2
 2231 066e 23          	.byte 0x23
 2232 066f 00          	.uleb128 0x0
 2233 0670 05          	.uleb128 0x5
MPLAB XC16 ASSEMBLY Listing:   			page 48


 2234 0671 53 41 4D 43 	.asciz "SAMC1"
 2234      31 00 
 2235 0677 03          	.byte 0x3
 2236 0678 F3 09       	.2byte 0x9f3
 2237 067a E8 00 00 00 	.4byte 0xe8
 2238 067e 02          	.byte 0x2
 2239 067f 01          	.byte 0x1
 2240 0680 06          	.byte 0x6
 2241 0681 02          	.byte 0x2
 2242 0682 23          	.byte 0x23
 2243 0683 00          	.uleb128 0x0
 2244 0684 05          	.uleb128 0x5
 2245 0685 53 41 4D 43 	.asciz "SAMC2"
 2245      32 00 
 2246 068b 03          	.byte 0x3
 2247 068c F4 09       	.2byte 0x9f4
 2248 068e E8 00 00 00 	.4byte 0xe8
 2249 0692 02          	.byte 0x2
 2250 0693 01          	.byte 0x1
 2251 0694 05          	.byte 0x5
 2252 0695 02          	.byte 0x2
 2253 0696 23          	.byte 0x23
 2254 0697 00          	.uleb128 0x0
 2255 0698 05          	.uleb128 0x5
 2256 0699 53 41 4D 43 	.asciz "SAMC3"
 2256      33 00 
 2257 069f 03          	.byte 0x3
 2258 06a0 F5 09       	.2byte 0x9f5
 2259 06a2 E8 00 00 00 	.4byte 0xe8
 2260 06a6 02          	.byte 0x2
 2261 06a7 01          	.byte 0x1
 2262 06a8 04          	.byte 0x4
 2263 06a9 02          	.byte 0x2
 2264 06aa 23          	.byte 0x23
 2265 06ab 00          	.uleb128 0x0
 2266 06ac 05          	.uleb128 0x5
 2267 06ad 53 41 4D 43 	.asciz "SAMC4"
 2267      34 00 
 2268 06b3 03          	.byte 0x3
 2269 06b4 F6 09       	.2byte 0x9f6
 2270 06b6 E8 00 00 00 	.4byte 0xe8
 2271 06ba 02          	.byte 0x2
 2272 06bb 01          	.byte 0x1
 2273 06bc 03          	.byte 0x3
 2274 06bd 02          	.byte 0x2
 2275 06be 23          	.byte 0x23
 2276 06bf 00          	.uleb128 0x0
 2277 06c0 00          	.byte 0x0
 2278 06c1 06          	.uleb128 0x6
 2279 06c2 02          	.byte 0x2
 2280 06c3 03          	.byte 0x3
 2281 06c4 E2 09       	.2byte 0x9e2
 2282 06c6 D5 06 00 00 	.4byte 0x6d5
 2283 06ca 07          	.uleb128 0x7
 2284 06cb 70 05 00 00 	.4byte 0x570
 2285 06cf 07          	.uleb128 0x7
 2286 06d0 B3 05 00 00 	.4byte 0x5b3
MPLAB XC16 ASSEMBLY Listing:   			page 49


 2287 06d4 00          	.byte 0x0
 2288 06d5 08          	.uleb128 0x8
 2289 06d6 74 61 67 41 	.asciz "tagAD1CON3BITS"
 2289      44 31 43 4F 
 2289      4E 33 42 49 
 2289      54 53 00 
 2290 06e5 02          	.byte 0x2
 2291 06e6 03          	.byte 0x3
 2292 06e7 E1 09       	.2byte 0x9e1
 2293 06e9 F6 06 00 00 	.4byte 0x6f6
 2294 06ed 09          	.uleb128 0x9
 2295 06ee C1 06 00 00 	.4byte 0x6c1
 2296 06f2 02          	.byte 0x2
 2297 06f3 23          	.byte 0x23
 2298 06f4 00          	.uleb128 0x0
 2299 06f5 00          	.byte 0x0
 2300 06f6 0A          	.uleb128 0xa
 2301 06f7 41 44 31 43 	.asciz "AD1CON3BITS"
 2301      4F 4E 33 42 
 2301      49 54 53 00 
 2302 0703 03          	.byte 0x3
 2303 0704 F9 09       	.2byte 0x9f9
 2304 0706 D5 06 00 00 	.4byte 0x6d5
 2305 070a 04          	.uleb128 0x4
 2306 070b 02          	.byte 0x2
 2307 070c 03          	.byte 0x3
 2308 070d 17 0A       	.2byte 0xa17
 2309 070f 64 07 00 00 	.4byte 0x764
 2310 0713 05          	.uleb128 0x5
 2311 0714 43 48 30 53 	.asciz "CH0SA"
 2311      41 00 
 2312 071a 03          	.byte 0x3
 2313 071b 18 0A       	.2byte 0xa18
 2314 071d E8 00 00 00 	.4byte 0xe8
 2315 0721 02          	.byte 0x2
 2316 0722 05          	.byte 0x5
 2317 0723 0B          	.byte 0xb
 2318 0724 02          	.byte 0x2
 2319 0725 23          	.byte 0x23
 2320 0726 00          	.uleb128 0x0
 2321 0727 05          	.uleb128 0x5
 2322 0728 43 48 30 4E 	.asciz "CH0NA"
 2322      41 00 
 2323 072e 03          	.byte 0x3
 2324 072f 1A 0A       	.2byte 0xa1a
 2325 0731 E8 00 00 00 	.4byte 0xe8
 2326 0735 02          	.byte 0x2
 2327 0736 01          	.byte 0x1
 2328 0737 08          	.byte 0x8
 2329 0738 02          	.byte 0x2
 2330 0739 23          	.byte 0x23
 2331 073a 00          	.uleb128 0x0
 2332 073b 05          	.uleb128 0x5
 2333 073c 43 48 30 53 	.asciz "CH0SB"
 2333      42 00 
 2334 0742 03          	.byte 0x3
 2335 0743 1B 0A       	.2byte 0xa1b
MPLAB XC16 ASSEMBLY Listing:   			page 50


 2336 0745 E8 00 00 00 	.4byte 0xe8
 2337 0749 02          	.byte 0x2
 2338 074a 05          	.byte 0x5
 2339 074b 03          	.byte 0x3
 2340 074c 02          	.byte 0x2
 2341 074d 23          	.byte 0x23
 2342 074e 00          	.uleb128 0x0
 2343 074f 05          	.uleb128 0x5
 2344 0750 43 48 30 4E 	.asciz "CH0NB"
 2344      42 00 
 2345 0756 03          	.byte 0x3
 2346 0757 1D 0A       	.2byte 0xa1d
 2347 0759 E8 00 00 00 	.4byte 0xe8
 2348 075d 02          	.byte 0x2
 2349 075e 01          	.byte 0x1
 2350 075f 10          	.byte 0x10
 2351 0760 02          	.byte 0x2
 2352 0761 23          	.byte 0x23
 2353 0762 00          	.uleb128 0x0
 2354 0763 00          	.byte 0x0
 2355 0764 04          	.uleb128 0x4
 2356 0765 02          	.byte 0x2
 2357 0766 03          	.byte 0x3
 2358 0767 1F 0A       	.2byte 0xa1f
 2359 0769 40 08 00 00 	.4byte 0x840
 2360 076d 05          	.uleb128 0x5
 2361 076e 43 48 30 53 	.asciz "CH0SA0"
 2361      41 30 00 
 2362 0775 03          	.byte 0x3
 2363 0776 20 0A       	.2byte 0xa20
 2364 0778 E8 00 00 00 	.4byte 0xe8
 2365 077c 02          	.byte 0x2
 2366 077d 01          	.byte 0x1
 2367 077e 0F          	.byte 0xf
 2368 077f 02          	.byte 0x2
 2369 0780 23          	.byte 0x23
 2370 0781 00          	.uleb128 0x0
 2371 0782 05          	.uleb128 0x5
 2372 0783 43 48 30 53 	.asciz "CH0SA1"
 2372      41 31 00 
 2373 078a 03          	.byte 0x3
 2374 078b 21 0A       	.2byte 0xa21
 2375 078d E8 00 00 00 	.4byte 0xe8
 2376 0791 02          	.byte 0x2
 2377 0792 01          	.byte 0x1
 2378 0793 0E          	.byte 0xe
 2379 0794 02          	.byte 0x2
 2380 0795 23          	.byte 0x23
 2381 0796 00          	.uleb128 0x0
 2382 0797 05          	.uleb128 0x5
 2383 0798 43 48 30 53 	.asciz "CH0SA2"
 2383      41 32 00 
 2384 079f 03          	.byte 0x3
 2385 07a0 22 0A       	.2byte 0xa22
 2386 07a2 E8 00 00 00 	.4byte 0xe8
 2387 07a6 02          	.byte 0x2
 2388 07a7 01          	.byte 0x1
MPLAB XC16 ASSEMBLY Listing:   			page 51


 2389 07a8 0D          	.byte 0xd
 2390 07a9 02          	.byte 0x2
 2391 07aa 23          	.byte 0x23
 2392 07ab 00          	.uleb128 0x0
 2393 07ac 05          	.uleb128 0x5
 2394 07ad 43 48 30 53 	.asciz "CH0SA3"
 2394      41 33 00 
 2395 07b4 03          	.byte 0x3
 2396 07b5 23 0A       	.2byte 0xa23
 2397 07b7 E8 00 00 00 	.4byte 0xe8
 2398 07bb 02          	.byte 0x2
 2399 07bc 01          	.byte 0x1
 2400 07bd 0C          	.byte 0xc
 2401 07be 02          	.byte 0x2
 2402 07bf 23          	.byte 0x23
 2403 07c0 00          	.uleb128 0x0
 2404 07c1 05          	.uleb128 0x5
 2405 07c2 43 48 30 53 	.asciz "CH0SA4"
 2405      41 34 00 
 2406 07c9 03          	.byte 0x3
 2407 07ca 24 0A       	.2byte 0xa24
 2408 07cc E8 00 00 00 	.4byte 0xe8
 2409 07d0 02          	.byte 0x2
 2410 07d1 01          	.byte 0x1
 2411 07d2 0B          	.byte 0xb
 2412 07d3 02          	.byte 0x2
 2413 07d4 23          	.byte 0x23
 2414 07d5 00          	.uleb128 0x0
 2415 07d6 05          	.uleb128 0x5
 2416 07d7 43 48 30 53 	.asciz "CH0SB0"
 2416      42 30 00 
 2417 07de 03          	.byte 0x3
 2418 07df 26 0A       	.2byte 0xa26
 2419 07e1 E8 00 00 00 	.4byte 0xe8
 2420 07e5 02          	.byte 0x2
 2421 07e6 01          	.byte 0x1
 2422 07e7 07          	.byte 0x7
 2423 07e8 02          	.byte 0x2
 2424 07e9 23          	.byte 0x23
 2425 07ea 00          	.uleb128 0x0
 2426 07eb 05          	.uleb128 0x5
 2427 07ec 43 48 30 53 	.asciz "CH0SB1"
 2427      42 31 00 
 2428 07f3 03          	.byte 0x3
 2429 07f4 27 0A       	.2byte 0xa27
 2430 07f6 E8 00 00 00 	.4byte 0xe8
 2431 07fa 02          	.byte 0x2
 2432 07fb 01          	.byte 0x1
 2433 07fc 06          	.byte 0x6
 2434 07fd 02          	.byte 0x2
 2435 07fe 23          	.byte 0x23
 2436 07ff 00          	.uleb128 0x0
 2437 0800 05          	.uleb128 0x5
 2438 0801 43 48 30 53 	.asciz "CH0SB2"
 2438      42 32 00 
 2439 0808 03          	.byte 0x3
 2440 0809 28 0A       	.2byte 0xa28
MPLAB XC16 ASSEMBLY Listing:   			page 52


 2441 080b E8 00 00 00 	.4byte 0xe8
 2442 080f 02          	.byte 0x2
 2443 0810 01          	.byte 0x1
 2444 0811 05          	.byte 0x5
 2445 0812 02          	.byte 0x2
 2446 0813 23          	.byte 0x23
 2447 0814 00          	.uleb128 0x0
 2448 0815 05          	.uleb128 0x5
 2449 0816 43 48 30 53 	.asciz "CH0SB3"
 2449      42 33 00 
 2450 081d 03          	.byte 0x3
 2451 081e 29 0A       	.2byte 0xa29
 2452 0820 E8 00 00 00 	.4byte 0xe8
 2453 0824 02          	.byte 0x2
 2454 0825 01          	.byte 0x1
 2455 0826 04          	.byte 0x4
 2456 0827 02          	.byte 0x2
 2457 0828 23          	.byte 0x23
 2458 0829 00          	.uleb128 0x0
 2459 082a 05          	.uleb128 0x5
 2460 082b 43 48 30 53 	.asciz "CH0SB4"
 2460      42 34 00 
 2461 0832 03          	.byte 0x3
 2462 0833 2A 0A       	.2byte 0xa2a
 2463 0835 E8 00 00 00 	.4byte 0xe8
 2464 0839 02          	.byte 0x2
 2465 083a 01          	.byte 0x1
 2466 083b 03          	.byte 0x3
 2467 083c 02          	.byte 0x2
 2468 083d 23          	.byte 0x23
 2469 083e 00          	.uleb128 0x0
 2470 083f 00          	.byte 0x0
 2471 0840 06          	.uleb128 0x6
 2472 0841 02          	.byte 0x2
 2473 0842 03          	.byte 0x3
 2474 0843 16 0A       	.2byte 0xa16
 2475 0845 54 08 00 00 	.4byte 0x854
 2476 0849 07          	.uleb128 0x7
 2477 084a 0A 07 00 00 	.4byte 0x70a
 2478 084e 07          	.uleb128 0x7
 2479 084f 64 07 00 00 	.4byte 0x764
 2480 0853 00          	.byte 0x0
 2481 0854 08          	.uleb128 0x8
 2482 0855 74 61 67 41 	.asciz "tagAD1CHS0BITS"
 2482      44 31 43 48 
 2482      53 30 42 49 
 2482      54 53 00 
 2483 0864 02          	.byte 0x2
 2484 0865 03          	.byte 0x3
 2485 0866 15 0A       	.2byte 0xa15
 2486 0868 75 08 00 00 	.4byte 0x875
 2487 086c 09          	.uleb128 0x9
 2488 086d 40 08 00 00 	.4byte 0x840
 2489 0871 02          	.byte 0x2
 2490 0872 23          	.byte 0x23
 2491 0873 00          	.uleb128 0x0
 2492 0874 00          	.byte 0x0
MPLAB XC16 ASSEMBLY Listing:   			page 53


 2493 0875 0A          	.uleb128 0xa
 2494 0876 41 44 31 43 	.asciz "AD1CHS0BITS"
 2494      48 53 30 42 
 2494      49 54 53 00 
 2495 0882 03          	.byte 0x3
 2496 0883 2D 0A       	.2byte 0xa2d
 2497 0885 54 08 00 00 	.4byte 0x854
 2498 0889 04          	.uleb128 0x4
 2499 088a 02          	.byte 0x2
 2500 088b 03          	.byte 0x3
 2501 088c 60 0A       	.2byte 0xa60
 2502 088e BD 08 00 00 	.4byte 0x8bd
 2503 0892 05          	.uleb128 0x5
 2504 0893 44 4D 41 42 	.asciz "DMABL"
 2504      4C 00 
 2505 0899 03          	.byte 0x3
 2506 089a 61 0A       	.2byte 0xa61
 2507 089c E8 00 00 00 	.4byte 0xe8
 2508 08a0 02          	.byte 0x2
 2509 08a1 03          	.byte 0x3
 2510 08a2 0D          	.byte 0xd
 2511 08a3 02          	.byte 0x2
 2512 08a4 23          	.byte 0x23
 2513 08a5 00          	.uleb128 0x0
 2514 08a6 05          	.uleb128 0x5
 2515 08a7 41 44 44 4D 	.asciz "ADDMAEN"
 2515      41 45 4E 00 
 2516 08af 03          	.byte 0x3
 2517 08b0 63 0A       	.2byte 0xa63
 2518 08b2 E8 00 00 00 	.4byte 0xe8
 2519 08b6 02          	.byte 0x2
 2520 08b7 01          	.byte 0x1
 2521 08b8 07          	.byte 0x7
 2522 08b9 02          	.byte 0x2
 2523 08ba 23          	.byte 0x23
 2524 08bb 00          	.uleb128 0x0
 2525 08bc 00          	.byte 0x0
 2526 08bd 04          	.uleb128 0x4
 2527 08be 02          	.byte 0x2
 2528 08bf 03          	.byte 0x3
 2529 08c0 65 0A       	.2byte 0xa65
 2530 08c2 06 09 00 00 	.4byte 0x906
 2531 08c6 05          	.uleb128 0x5
 2532 08c7 44 4D 41 42 	.asciz "DMABL0"
 2532      4C 30 00 
 2533 08ce 03          	.byte 0x3
 2534 08cf 66 0A       	.2byte 0xa66
 2535 08d1 E8 00 00 00 	.4byte 0xe8
 2536 08d5 02          	.byte 0x2
 2537 08d6 01          	.byte 0x1
 2538 08d7 0F          	.byte 0xf
 2539 08d8 02          	.byte 0x2
 2540 08d9 23          	.byte 0x23
 2541 08da 00          	.uleb128 0x0
 2542 08db 05          	.uleb128 0x5
 2543 08dc 44 4D 41 42 	.asciz "DMABL1"
 2543      4C 31 00 
MPLAB XC16 ASSEMBLY Listing:   			page 54


 2544 08e3 03          	.byte 0x3
 2545 08e4 67 0A       	.2byte 0xa67
 2546 08e6 E8 00 00 00 	.4byte 0xe8
 2547 08ea 02          	.byte 0x2
 2548 08eb 01          	.byte 0x1
 2549 08ec 0E          	.byte 0xe
 2550 08ed 02          	.byte 0x2
 2551 08ee 23          	.byte 0x23
 2552 08ef 00          	.uleb128 0x0
 2553 08f0 05          	.uleb128 0x5
 2554 08f1 44 4D 41 42 	.asciz "DMABL2"
 2554      4C 32 00 
 2555 08f8 03          	.byte 0x3
 2556 08f9 68 0A       	.2byte 0xa68
 2557 08fb E8 00 00 00 	.4byte 0xe8
 2558 08ff 02          	.byte 0x2
 2559 0900 01          	.byte 0x1
 2560 0901 0D          	.byte 0xd
 2561 0902 02          	.byte 0x2
 2562 0903 23          	.byte 0x23
 2563 0904 00          	.uleb128 0x0
 2564 0905 00          	.byte 0x0
 2565 0906 06          	.uleb128 0x6
 2566 0907 02          	.byte 0x2
 2567 0908 03          	.byte 0x3
 2568 0909 5F 0A       	.2byte 0xa5f
 2569 090b 1A 09 00 00 	.4byte 0x91a
 2570 090f 07          	.uleb128 0x7
 2571 0910 89 08 00 00 	.4byte 0x889
 2572 0914 07          	.uleb128 0x7
 2573 0915 BD 08 00 00 	.4byte 0x8bd
 2574 0919 00          	.byte 0x0
 2575 091a 08          	.uleb128 0x8
 2576 091b 74 61 67 41 	.asciz "tagAD1CON4BITS"
 2576      44 31 43 4F 
 2576      4E 34 42 49 
 2576      54 53 00 
 2577 092a 02          	.byte 0x2
 2578 092b 03          	.byte 0x3
 2579 092c 5E 0A       	.2byte 0xa5e
 2580 092e 3B 09 00 00 	.4byte 0x93b
 2581 0932 09          	.uleb128 0x9
 2582 0933 06 09 00 00 	.4byte 0x906
 2583 0937 02          	.byte 0x2
 2584 0938 23          	.byte 0x23
 2585 0939 00          	.uleb128 0x0
 2586 093a 00          	.byte 0x0
 2587 093b 0A          	.uleb128 0xa
 2588 093c 41 44 31 43 	.asciz "AD1CON4BITS"
 2588      4F 4E 34 42 
 2588      49 54 53 00 
 2589 0948 03          	.byte 0x3
 2590 0949 6B 0A       	.2byte 0xa6b
 2591 094b 1A 09 00 00 	.4byte 0x91a
 2592 094f 08          	.uleb128 0x8
 2593 0950 74 61 67 49 	.asciz "tagIFS0BITS"
 2593      46 53 30 42 
MPLAB XC16 ASSEMBLY Listing:   			page 55


 2593      49 54 53 00 
 2594 095c 02          	.byte 0x2
 2595 095d 03          	.byte 0x3
 2596 095e 08 25       	.2byte 0x2508
 2597 0960 AA 0A 00 00 	.4byte 0xaaa
 2598 0964 05          	.uleb128 0x5
 2599 0965 49 4E 54 30 	.asciz "INT0IF"
 2599      49 46 00 
 2600 096c 03          	.byte 0x3
 2601 096d 09 25       	.2byte 0x2509
 2602 096f E8 00 00 00 	.4byte 0xe8
 2603 0973 02          	.byte 0x2
 2604 0974 01          	.byte 0x1
 2605 0975 0F          	.byte 0xf
 2606 0976 02          	.byte 0x2
 2607 0977 23          	.byte 0x23
 2608 0978 00          	.uleb128 0x0
 2609 0979 05          	.uleb128 0x5
 2610 097a 49 43 31 49 	.asciz "IC1IF"
 2610      46 00 
 2611 0980 03          	.byte 0x3
 2612 0981 0A 25       	.2byte 0x250a
 2613 0983 E8 00 00 00 	.4byte 0xe8
 2614 0987 02          	.byte 0x2
 2615 0988 01          	.byte 0x1
 2616 0989 0E          	.byte 0xe
 2617 098a 02          	.byte 0x2
 2618 098b 23          	.byte 0x23
 2619 098c 00          	.uleb128 0x0
 2620 098d 05          	.uleb128 0x5
 2621 098e 4F 43 31 49 	.asciz "OC1IF"
 2621      46 00 
 2622 0994 03          	.byte 0x3
 2623 0995 0B 25       	.2byte 0x250b
 2624 0997 E8 00 00 00 	.4byte 0xe8
 2625 099b 02          	.byte 0x2
 2626 099c 01          	.byte 0x1
 2627 099d 0D          	.byte 0xd
 2628 099e 02          	.byte 0x2
 2629 099f 23          	.byte 0x23
 2630 09a0 00          	.uleb128 0x0
 2631 09a1 05          	.uleb128 0x5
 2632 09a2 54 31 49 46 	.asciz "T1IF"
 2632      00 
 2633 09a7 03          	.byte 0x3
 2634 09a8 0C 25       	.2byte 0x250c
 2635 09aa E8 00 00 00 	.4byte 0xe8
 2636 09ae 02          	.byte 0x2
 2637 09af 01          	.byte 0x1
 2638 09b0 0C          	.byte 0xc
 2639 09b1 02          	.byte 0x2
 2640 09b2 23          	.byte 0x23
 2641 09b3 00          	.uleb128 0x0
 2642 09b4 05          	.uleb128 0x5
 2643 09b5 44 4D 41 30 	.asciz "DMA0IF"
 2643      49 46 00 
 2644 09bc 03          	.byte 0x3
MPLAB XC16 ASSEMBLY Listing:   			page 56


 2645 09bd 0D 25       	.2byte 0x250d
 2646 09bf E8 00 00 00 	.4byte 0xe8
 2647 09c3 02          	.byte 0x2
 2648 09c4 01          	.byte 0x1
 2649 09c5 0B          	.byte 0xb
 2650 09c6 02          	.byte 0x2
 2651 09c7 23          	.byte 0x23
 2652 09c8 00          	.uleb128 0x0
 2653 09c9 05          	.uleb128 0x5
 2654 09ca 49 43 32 49 	.asciz "IC2IF"
 2654      46 00 
 2655 09d0 03          	.byte 0x3
 2656 09d1 0E 25       	.2byte 0x250e
 2657 09d3 E8 00 00 00 	.4byte 0xe8
 2658 09d7 02          	.byte 0x2
 2659 09d8 01          	.byte 0x1
 2660 09d9 0A          	.byte 0xa
 2661 09da 02          	.byte 0x2
 2662 09db 23          	.byte 0x23
 2663 09dc 00          	.uleb128 0x0
 2664 09dd 05          	.uleb128 0x5
 2665 09de 4F 43 32 49 	.asciz "OC2IF"
 2665      46 00 
 2666 09e4 03          	.byte 0x3
 2667 09e5 0F 25       	.2byte 0x250f
 2668 09e7 E8 00 00 00 	.4byte 0xe8
 2669 09eb 02          	.byte 0x2
 2670 09ec 01          	.byte 0x1
 2671 09ed 09          	.byte 0x9
 2672 09ee 02          	.byte 0x2
 2673 09ef 23          	.byte 0x23
 2674 09f0 00          	.uleb128 0x0
 2675 09f1 05          	.uleb128 0x5
 2676 09f2 54 32 49 46 	.asciz "T2IF"
 2676      00 
 2677 09f7 03          	.byte 0x3
 2678 09f8 10 25       	.2byte 0x2510
 2679 09fa E8 00 00 00 	.4byte 0xe8
 2680 09fe 02          	.byte 0x2
 2681 09ff 01          	.byte 0x1
 2682 0a00 08          	.byte 0x8
 2683 0a01 02          	.byte 0x2
 2684 0a02 23          	.byte 0x23
 2685 0a03 00          	.uleb128 0x0
 2686 0a04 05          	.uleb128 0x5
 2687 0a05 54 33 49 46 	.asciz "T3IF"
 2687      00 
 2688 0a0a 03          	.byte 0x3
 2689 0a0b 11 25       	.2byte 0x2511
 2690 0a0d E8 00 00 00 	.4byte 0xe8
 2691 0a11 02          	.byte 0x2
 2692 0a12 01          	.byte 0x1
 2693 0a13 07          	.byte 0x7
 2694 0a14 02          	.byte 0x2
 2695 0a15 23          	.byte 0x23
 2696 0a16 00          	.uleb128 0x0
 2697 0a17 05          	.uleb128 0x5
MPLAB XC16 ASSEMBLY Listing:   			page 57


 2698 0a18 53 50 49 31 	.asciz "SPI1EIF"
 2698      45 49 46 00 
 2699 0a20 03          	.byte 0x3
 2700 0a21 12 25       	.2byte 0x2512
 2701 0a23 E8 00 00 00 	.4byte 0xe8
 2702 0a27 02          	.byte 0x2
 2703 0a28 01          	.byte 0x1
 2704 0a29 06          	.byte 0x6
 2705 0a2a 02          	.byte 0x2
 2706 0a2b 23          	.byte 0x23
 2707 0a2c 00          	.uleb128 0x0
 2708 0a2d 05          	.uleb128 0x5
 2709 0a2e 53 50 49 31 	.asciz "SPI1IF"
 2709      49 46 00 
 2710 0a35 03          	.byte 0x3
 2711 0a36 13 25       	.2byte 0x2513
 2712 0a38 E8 00 00 00 	.4byte 0xe8
 2713 0a3c 02          	.byte 0x2
 2714 0a3d 01          	.byte 0x1
 2715 0a3e 05          	.byte 0x5
 2716 0a3f 02          	.byte 0x2
 2717 0a40 23          	.byte 0x23
 2718 0a41 00          	.uleb128 0x0
 2719 0a42 05          	.uleb128 0x5
 2720 0a43 55 31 52 58 	.asciz "U1RXIF"
 2720      49 46 00 
 2721 0a4a 03          	.byte 0x3
 2722 0a4b 14 25       	.2byte 0x2514
 2723 0a4d E8 00 00 00 	.4byte 0xe8
 2724 0a51 02          	.byte 0x2
 2725 0a52 01          	.byte 0x1
 2726 0a53 04          	.byte 0x4
 2727 0a54 02          	.byte 0x2
 2728 0a55 23          	.byte 0x23
 2729 0a56 00          	.uleb128 0x0
 2730 0a57 05          	.uleb128 0x5
 2731 0a58 55 31 54 58 	.asciz "U1TXIF"
 2731      49 46 00 
 2732 0a5f 03          	.byte 0x3
 2733 0a60 15 25       	.2byte 0x2515
 2734 0a62 E8 00 00 00 	.4byte 0xe8
 2735 0a66 02          	.byte 0x2
 2736 0a67 01          	.byte 0x1
 2737 0a68 03          	.byte 0x3
 2738 0a69 02          	.byte 0x2
 2739 0a6a 23          	.byte 0x23
 2740 0a6b 00          	.uleb128 0x0
 2741 0a6c 05          	.uleb128 0x5
 2742 0a6d 41 44 31 49 	.asciz "AD1IF"
 2742      46 00 
 2743 0a73 03          	.byte 0x3
 2744 0a74 16 25       	.2byte 0x2516
 2745 0a76 E8 00 00 00 	.4byte 0xe8
 2746 0a7a 02          	.byte 0x2
 2747 0a7b 01          	.byte 0x1
 2748 0a7c 02          	.byte 0x2
 2749 0a7d 02          	.byte 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 58


 2750 0a7e 23          	.byte 0x23
 2751 0a7f 00          	.uleb128 0x0
 2752 0a80 05          	.uleb128 0x5
 2753 0a81 44 4D 41 31 	.asciz "DMA1IF"
 2753      49 46 00 
 2754 0a88 03          	.byte 0x3
 2755 0a89 17 25       	.2byte 0x2517
 2756 0a8b E8 00 00 00 	.4byte 0xe8
 2757 0a8f 02          	.byte 0x2
 2758 0a90 01          	.byte 0x1
 2759 0a91 01          	.byte 0x1
 2760 0a92 02          	.byte 0x2
 2761 0a93 23          	.byte 0x23
 2762 0a94 00          	.uleb128 0x0
 2763 0a95 05          	.uleb128 0x5
 2764 0a96 4E 56 4D 49 	.asciz "NVMIF"
 2764      46 00 
 2765 0a9c 03          	.byte 0x3
 2766 0a9d 18 25       	.2byte 0x2518
 2767 0a9f E8 00 00 00 	.4byte 0xe8
 2768 0aa3 02          	.byte 0x2
 2769 0aa4 01          	.byte 0x1
 2770 0aa5 10          	.byte 0x10
 2771 0aa6 02          	.byte 0x2
 2772 0aa7 23          	.byte 0x23
 2773 0aa8 00          	.uleb128 0x0
 2774 0aa9 00          	.byte 0x0
 2775 0aaa 0A          	.uleb128 0xa
 2776 0aab 49 46 53 30 	.asciz "IFS0BITS"
 2776      42 49 54 53 
 2776      00 
 2777 0ab4 03          	.byte 0x3
 2778 0ab5 19 25       	.2byte 0x2519
 2779 0ab7 4F 09 00 00 	.4byte 0x94f
 2780 0abb 08          	.uleb128 0x8
 2781 0abc 74 61 67 49 	.asciz "tagIFS1BITS"
 2781      46 53 31 42 
 2781      49 54 53 00 
 2782 0ac8 02          	.byte 0x2
 2783 0ac9 03          	.byte 0x3
 2784 0aca 1E 25       	.2byte 0x251e
 2785 0acc 16 0C 00 00 	.4byte 0xc16
 2786 0ad0 05          	.uleb128 0x5
 2787 0ad1 53 49 32 43 	.asciz "SI2C1IF"
 2787      31 49 46 00 
 2788 0ad9 03          	.byte 0x3
 2789 0ada 1F 25       	.2byte 0x251f
 2790 0adc E8 00 00 00 	.4byte 0xe8
 2791 0ae0 02          	.byte 0x2
 2792 0ae1 01          	.byte 0x1
 2793 0ae2 0F          	.byte 0xf
 2794 0ae3 02          	.byte 0x2
 2795 0ae4 23          	.byte 0x23
 2796 0ae5 00          	.uleb128 0x0
 2797 0ae6 05          	.uleb128 0x5
 2798 0ae7 4D 49 32 43 	.asciz "MI2C1IF"
 2798      31 49 46 00 
MPLAB XC16 ASSEMBLY Listing:   			page 59


 2799 0aef 03          	.byte 0x3
 2800 0af0 20 25       	.2byte 0x2520
 2801 0af2 E8 00 00 00 	.4byte 0xe8
 2802 0af6 02          	.byte 0x2
 2803 0af7 01          	.byte 0x1
 2804 0af8 0E          	.byte 0xe
 2805 0af9 02          	.byte 0x2
 2806 0afa 23          	.byte 0x23
 2807 0afb 00          	.uleb128 0x0
 2808 0afc 05          	.uleb128 0x5
 2809 0afd 43 4D 49 46 	.asciz "CMIF"
 2809      00 
 2810 0b02 03          	.byte 0x3
 2811 0b03 21 25       	.2byte 0x2521
 2812 0b05 E8 00 00 00 	.4byte 0xe8
 2813 0b09 02          	.byte 0x2
 2814 0b0a 01          	.byte 0x1
 2815 0b0b 0D          	.byte 0xd
 2816 0b0c 02          	.byte 0x2
 2817 0b0d 23          	.byte 0x23
 2818 0b0e 00          	.uleb128 0x0
 2819 0b0f 05          	.uleb128 0x5
 2820 0b10 43 4E 49 46 	.asciz "CNIF"
 2820      00 
 2821 0b15 03          	.byte 0x3
 2822 0b16 22 25       	.2byte 0x2522
 2823 0b18 E8 00 00 00 	.4byte 0xe8
 2824 0b1c 02          	.byte 0x2
 2825 0b1d 01          	.byte 0x1
 2826 0b1e 0C          	.byte 0xc
 2827 0b1f 02          	.byte 0x2
 2828 0b20 23          	.byte 0x23
 2829 0b21 00          	.uleb128 0x0
 2830 0b22 05          	.uleb128 0x5
 2831 0b23 49 4E 54 31 	.asciz "INT1IF"
 2831      49 46 00 
 2832 0b2a 03          	.byte 0x3
 2833 0b2b 23 25       	.2byte 0x2523
 2834 0b2d E8 00 00 00 	.4byte 0xe8
 2835 0b31 02          	.byte 0x2
 2836 0b32 01          	.byte 0x1
 2837 0b33 0B          	.byte 0xb
 2838 0b34 02          	.byte 0x2
 2839 0b35 23          	.byte 0x23
 2840 0b36 00          	.uleb128 0x0
 2841 0b37 05          	.uleb128 0x5
 2842 0b38 41 44 32 49 	.asciz "AD2IF"
 2842      46 00 
 2843 0b3e 03          	.byte 0x3
 2844 0b3f 24 25       	.2byte 0x2524
 2845 0b41 E8 00 00 00 	.4byte 0xe8
 2846 0b45 02          	.byte 0x2
 2847 0b46 01          	.byte 0x1
 2848 0b47 0A          	.byte 0xa
 2849 0b48 02          	.byte 0x2
 2850 0b49 23          	.byte 0x23
 2851 0b4a 00          	.uleb128 0x0
MPLAB XC16 ASSEMBLY Listing:   			page 60


 2852 0b4b 05          	.uleb128 0x5
 2853 0b4c 49 43 37 49 	.asciz "IC7IF"
 2853      46 00 
 2854 0b52 03          	.byte 0x3
 2855 0b53 25 25       	.2byte 0x2525
 2856 0b55 E8 00 00 00 	.4byte 0xe8
 2857 0b59 02          	.byte 0x2
 2858 0b5a 01          	.byte 0x1
 2859 0b5b 09          	.byte 0x9
 2860 0b5c 02          	.byte 0x2
 2861 0b5d 23          	.byte 0x23
 2862 0b5e 00          	.uleb128 0x0
 2863 0b5f 05          	.uleb128 0x5
 2864 0b60 49 43 38 49 	.asciz "IC8IF"
 2864      46 00 
 2865 0b66 03          	.byte 0x3
 2866 0b67 26 25       	.2byte 0x2526
 2867 0b69 E8 00 00 00 	.4byte 0xe8
 2868 0b6d 02          	.byte 0x2
 2869 0b6e 01          	.byte 0x1
 2870 0b6f 08          	.byte 0x8
 2871 0b70 02          	.byte 0x2
 2872 0b71 23          	.byte 0x23
 2873 0b72 00          	.uleb128 0x0
 2874 0b73 05          	.uleb128 0x5
 2875 0b74 44 4D 41 32 	.asciz "DMA2IF"
 2875      49 46 00 
 2876 0b7b 03          	.byte 0x3
 2877 0b7c 27 25       	.2byte 0x2527
 2878 0b7e E8 00 00 00 	.4byte 0xe8
 2879 0b82 02          	.byte 0x2
 2880 0b83 01          	.byte 0x1
 2881 0b84 07          	.byte 0x7
 2882 0b85 02          	.byte 0x2
 2883 0b86 23          	.byte 0x23
 2884 0b87 00          	.uleb128 0x0
 2885 0b88 05          	.uleb128 0x5
 2886 0b89 4F 43 33 49 	.asciz "OC3IF"
 2886      46 00 
 2887 0b8f 03          	.byte 0x3
 2888 0b90 28 25       	.2byte 0x2528
 2889 0b92 E8 00 00 00 	.4byte 0xe8
 2890 0b96 02          	.byte 0x2
 2891 0b97 01          	.byte 0x1
 2892 0b98 06          	.byte 0x6
 2893 0b99 02          	.byte 0x2
 2894 0b9a 23          	.byte 0x23
 2895 0b9b 00          	.uleb128 0x0
 2896 0b9c 05          	.uleb128 0x5
 2897 0b9d 4F 43 34 49 	.asciz "OC4IF"
 2897      46 00 
 2898 0ba3 03          	.byte 0x3
 2899 0ba4 29 25       	.2byte 0x2529
 2900 0ba6 E8 00 00 00 	.4byte 0xe8
 2901 0baa 02          	.byte 0x2
 2902 0bab 01          	.byte 0x1
 2903 0bac 05          	.byte 0x5
MPLAB XC16 ASSEMBLY Listing:   			page 61


 2904 0bad 02          	.byte 0x2
 2905 0bae 23          	.byte 0x23
 2906 0baf 00          	.uleb128 0x0
 2907 0bb0 05          	.uleb128 0x5
 2908 0bb1 54 34 49 46 	.asciz "T4IF"
 2908      00 
 2909 0bb6 03          	.byte 0x3
 2910 0bb7 2A 25       	.2byte 0x252a
 2911 0bb9 E8 00 00 00 	.4byte 0xe8
 2912 0bbd 02          	.byte 0x2
 2913 0bbe 01          	.byte 0x1
 2914 0bbf 04          	.byte 0x4
 2915 0bc0 02          	.byte 0x2
 2916 0bc1 23          	.byte 0x23
 2917 0bc2 00          	.uleb128 0x0
 2918 0bc3 05          	.uleb128 0x5
 2919 0bc4 54 35 49 46 	.asciz "T5IF"
 2919      00 
 2920 0bc9 03          	.byte 0x3
 2921 0bca 2B 25       	.2byte 0x252b
 2922 0bcc E8 00 00 00 	.4byte 0xe8
 2923 0bd0 02          	.byte 0x2
 2924 0bd1 01          	.byte 0x1
 2925 0bd2 03          	.byte 0x3
 2926 0bd3 02          	.byte 0x2
 2927 0bd4 23          	.byte 0x23
 2928 0bd5 00          	.uleb128 0x0
 2929 0bd6 05          	.uleb128 0x5
 2930 0bd7 49 4E 54 32 	.asciz "INT2IF"
 2930      49 46 00 
 2931 0bde 03          	.byte 0x3
 2932 0bdf 2C 25       	.2byte 0x252c
 2933 0be1 E8 00 00 00 	.4byte 0xe8
 2934 0be5 02          	.byte 0x2
 2935 0be6 01          	.byte 0x1
 2936 0be7 02          	.byte 0x2
 2937 0be8 02          	.byte 0x2
 2938 0be9 23          	.byte 0x23
 2939 0bea 00          	.uleb128 0x0
 2940 0beb 05          	.uleb128 0x5
 2941 0bec 55 32 52 58 	.asciz "U2RXIF"
 2941      49 46 00 
 2942 0bf3 03          	.byte 0x3
 2943 0bf4 2D 25       	.2byte 0x252d
 2944 0bf6 E8 00 00 00 	.4byte 0xe8
 2945 0bfa 02          	.byte 0x2
 2946 0bfb 01          	.byte 0x1
 2947 0bfc 01          	.byte 0x1
 2948 0bfd 02          	.byte 0x2
 2949 0bfe 23          	.byte 0x23
 2950 0bff 00          	.uleb128 0x0
 2951 0c00 05          	.uleb128 0x5
 2952 0c01 55 32 54 58 	.asciz "U2TXIF"
 2952      49 46 00 
 2953 0c08 03          	.byte 0x3
 2954 0c09 2E 25       	.2byte 0x252e
 2955 0c0b E8 00 00 00 	.4byte 0xe8
MPLAB XC16 ASSEMBLY Listing:   			page 62


 2956 0c0f 02          	.byte 0x2
 2957 0c10 01          	.byte 0x1
 2958 0c11 10          	.byte 0x10
 2959 0c12 02          	.byte 0x2
 2960 0c13 23          	.byte 0x23
 2961 0c14 00          	.uleb128 0x0
 2962 0c15 00          	.byte 0x0
 2963 0c16 0A          	.uleb128 0xa
 2964 0c17 49 46 53 31 	.asciz "IFS1BITS"
 2964      42 49 54 53 
 2964      00 
 2965 0c20 03          	.byte 0x3
 2966 0c21 2F 25       	.2byte 0x252f
 2967 0c23 BB 0A 00 00 	.4byte 0xabb
 2968 0c27 08          	.uleb128 0x8
 2969 0c28 74 61 67 49 	.asciz "tagIEC0BITS"
 2969      45 43 30 42 
 2969      49 54 53 00 
 2970 0c34 02          	.byte 0x2
 2971 0c35 03          	.byte 0x3
 2972 0c36 B4 25       	.2byte 0x25b4
 2973 0c38 82 0D 00 00 	.4byte 0xd82
 2974 0c3c 05          	.uleb128 0x5
 2975 0c3d 49 4E 54 30 	.asciz "INT0IE"
 2975      49 45 00 
 2976 0c44 03          	.byte 0x3
 2977 0c45 B5 25       	.2byte 0x25b5
 2978 0c47 E8 00 00 00 	.4byte 0xe8
 2979 0c4b 02          	.byte 0x2
 2980 0c4c 01          	.byte 0x1
 2981 0c4d 0F          	.byte 0xf
 2982 0c4e 02          	.byte 0x2
 2983 0c4f 23          	.byte 0x23
 2984 0c50 00          	.uleb128 0x0
 2985 0c51 05          	.uleb128 0x5
 2986 0c52 49 43 31 49 	.asciz "IC1IE"
 2986      45 00 
 2987 0c58 03          	.byte 0x3
 2988 0c59 B6 25       	.2byte 0x25b6
 2989 0c5b E8 00 00 00 	.4byte 0xe8
 2990 0c5f 02          	.byte 0x2
 2991 0c60 01          	.byte 0x1
 2992 0c61 0E          	.byte 0xe
 2993 0c62 02          	.byte 0x2
 2994 0c63 23          	.byte 0x23
 2995 0c64 00          	.uleb128 0x0
 2996 0c65 05          	.uleb128 0x5
 2997 0c66 4F 43 31 49 	.asciz "OC1IE"
 2997      45 00 
 2998 0c6c 03          	.byte 0x3
 2999 0c6d B7 25       	.2byte 0x25b7
 3000 0c6f E8 00 00 00 	.4byte 0xe8
 3001 0c73 02          	.byte 0x2
 3002 0c74 01          	.byte 0x1
 3003 0c75 0D          	.byte 0xd
 3004 0c76 02          	.byte 0x2
 3005 0c77 23          	.byte 0x23
MPLAB XC16 ASSEMBLY Listing:   			page 63


 3006 0c78 00          	.uleb128 0x0
 3007 0c79 05          	.uleb128 0x5
 3008 0c7a 54 31 49 45 	.asciz "T1IE"
 3008      00 
 3009 0c7f 03          	.byte 0x3
 3010 0c80 B8 25       	.2byte 0x25b8
 3011 0c82 E8 00 00 00 	.4byte 0xe8
 3012 0c86 02          	.byte 0x2
 3013 0c87 01          	.byte 0x1
 3014 0c88 0C          	.byte 0xc
 3015 0c89 02          	.byte 0x2
 3016 0c8a 23          	.byte 0x23
 3017 0c8b 00          	.uleb128 0x0
 3018 0c8c 05          	.uleb128 0x5
 3019 0c8d 44 4D 41 30 	.asciz "DMA0IE"
 3019      49 45 00 
 3020 0c94 03          	.byte 0x3
 3021 0c95 B9 25       	.2byte 0x25b9
 3022 0c97 E8 00 00 00 	.4byte 0xe8
 3023 0c9b 02          	.byte 0x2
 3024 0c9c 01          	.byte 0x1
 3025 0c9d 0B          	.byte 0xb
 3026 0c9e 02          	.byte 0x2
 3027 0c9f 23          	.byte 0x23
 3028 0ca0 00          	.uleb128 0x0
 3029 0ca1 05          	.uleb128 0x5
 3030 0ca2 49 43 32 49 	.asciz "IC2IE"
 3030      45 00 
 3031 0ca8 03          	.byte 0x3
 3032 0ca9 BA 25       	.2byte 0x25ba
 3033 0cab E8 00 00 00 	.4byte 0xe8
 3034 0caf 02          	.byte 0x2
 3035 0cb0 01          	.byte 0x1
 3036 0cb1 0A          	.byte 0xa
 3037 0cb2 02          	.byte 0x2
 3038 0cb3 23          	.byte 0x23
 3039 0cb4 00          	.uleb128 0x0
 3040 0cb5 05          	.uleb128 0x5
 3041 0cb6 4F 43 32 49 	.asciz "OC2IE"
 3041      45 00 
 3042 0cbc 03          	.byte 0x3
 3043 0cbd BB 25       	.2byte 0x25bb
 3044 0cbf E8 00 00 00 	.4byte 0xe8
 3045 0cc3 02          	.byte 0x2
 3046 0cc4 01          	.byte 0x1
 3047 0cc5 09          	.byte 0x9
 3048 0cc6 02          	.byte 0x2
 3049 0cc7 23          	.byte 0x23
 3050 0cc8 00          	.uleb128 0x0
 3051 0cc9 05          	.uleb128 0x5
 3052 0cca 54 32 49 45 	.asciz "T2IE"
 3052      00 
 3053 0ccf 03          	.byte 0x3
 3054 0cd0 BC 25       	.2byte 0x25bc
 3055 0cd2 E8 00 00 00 	.4byte 0xe8
 3056 0cd6 02          	.byte 0x2
 3057 0cd7 01          	.byte 0x1
MPLAB XC16 ASSEMBLY Listing:   			page 64


 3058 0cd8 08          	.byte 0x8
 3059 0cd9 02          	.byte 0x2
 3060 0cda 23          	.byte 0x23
 3061 0cdb 00          	.uleb128 0x0
 3062 0cdc 05          	.uleb128 0x5
 3063 0cdd 54 33 49 45 	.asciz "T3IE"
 3063      00 
 3064 0ce2 03          	.byte 0x3
 3065 0ce3 BD 25       	.2byte 0x25bd
 3066 0ce5 E8 00 00 00 	.4byte 0xe8
 3067 0ce9 02          	.byte 0x2
 3068 0cea 01          	.byte 0x1
 3069 0ceb 07          	.byte 0x7
 3070 0cec 02          	.byte 0x2
 3071 0ced 23          	.byte 0x23
 3072 0cee 00          	.uleb128 0x0
 3073 0cef 05          	.uleb128 0x5
 3074 0cf0 53 50 49 31 	.asciz "SPI1EIE"
 3074      45 49 45 00 
 3075 0cf8 03          	.byte 0x3
 3076 0cf9 BE 25       	.2byte 0x25be
 3077 0cfb E8 00 00 00 	.4byte 0xe8
 3078 0cff 02          	.byte 0x2
 3079 0d00 01          	.byte 0x1
 3080 0d01 06          	.byte 0x6
 3081 0d02 02          	.byte 0x2
 3082 0d03 23          	.byte 0x23
 3083 0d04 00          	.uleb128 0x0
 3084 0d05 05          	.uleb128 0x5
 3085 0d06 53 50 49 31 	.asciz "SPI1IE"
 3085      49 45 00 
 3086 0d0d 03          	.byte 0x3
 3087 0d0e BF 25       	.2byte 0x25bf
 3088 0d10 E8 00 00 00 	.4byte 0xe8
 3089 0d14 02          	.byte 0x2
 3090 0d15 01          	.byte 0x1
 3091 0d16 05          	.byte 0x5
 3092 0d17 02          	.byte 0x2
 3093 0d18 23          	.byte 0x23
 3094 0d19 00          	.uleb128 0x0
 3095 0d1a 05          	.uleb128 0x5
 3096 0d1b 55 31 52 58 	.asciz "U1RXIE"
 3096      49 45 00 
 3097 0d22 03          	.byte 0x3
 3098 0d23 C0 25       	.2byte 0x25c0
 3099 0d25 E8 00 00 00 	.4byte 0xe8
 3100 0d29 02          	.byte 0x2
 3101 0d2a 01          	.byte 0x1
 3102 0d2b 04          	.byte 0x4
 3103 0d2c 02          	.byte 0x2
 3104 0d2d 23          	.byte 0x23
 3105 0d2e 00          	.uleb128 0x0
 3106 0d2f 05          	.uleb128 0x5
 3107 0d30 55 31 54 58 	.asciz "U1TXIE"
 3107      49 45 00 
 3108 0d37 03          	.byte 0x3
 3109 0d38 C1 25       	.2byte 0x25c1
MPLAB XC16 ASSEMBLY Listing:   			page 65


 3110 0d3a E8 00 00 00 	.4byte 0xe8
 3111 0d3e 02          	.byte 0x2
 3112 0d3f 01          	.byte 0x1
 3113 0d40 03          	.byte 0x3
 3114 0d41 02          	.byte 0x2
 3115 0d42 23          	.byte 0x23
 3116 0d43 00          	.uleb128 0x0
 3117 0d44 05          	.uleb128 0x5
 3118 0d45 41 44 31 49 	.asciz "AD1IE"
 3118      45 00 
 3119 0d4b 03          	.byte 0x3
 3120 0d4c C2 25       	.2byte 0x25c2
 3121 0d4e E8 00 00 00 	.4byte 0xe8
 3122 0d52 02          	.byte 0x2
 3123 0d53 01          	.byte 0x1
 3124 0d54 02          	.byte 0x2
 3125 0d55 02          	.byte 0x2
 3126 0d56 23          	.byte 0x23
 3127 0d57 00          	.uleb128 0x0
 3128 0d58 05          	.uleb128 0x5
 3129 0d59 44 4D 41 31 	.asciz "DMA1IE"
 3129      49 45 00 
 3130 0d60 03          	.byte 0x3
 3131 0d61 C3 25       	.2byte 0x25c3
 3132 0d63 E8 00 00 00 	.4byte 0xe8
 3133 0d67 02          	.byte 0x2
 3134 0d68 01          	.byte 0x1
 3135 0d69 01          	.byte 0x1
 3136 0d6a 02          	.byte 0x2
 3137 0d6b 23          	.byte 0x23
 3138 0d6c 00          	.uleb128 0x0
 3139 0d6d 05          	.uleb128 0x5
 3140 0d6e 4E 56 4D 49 	.asciz "NVMIE"
 3140      45 00 
 3141 0d74 03          	.byte 0x3
 3142 0d75 C4 25       	.2byte 0x25c4
 3143 0d77 E8 00 00 00 	.4byte 0xe8
 3144 0d7b 02          	.byte 0x2
 3145 0d7c 01          	.byte 0x1
 3146 0d7d 10          	.byte 0x10
 3147 0d7e 02          	.byte 0x2
 3148 0d7f 23          	.byte 0x23
 3149 0d80 00          	.uleb128 0x0
 3150 0d81 00          	.byte 0x0
 3151 0d82 0A          	.uleb128 0xa
 3152 0d83 49 45 43 30 	.asciz "IEC0BITS"
 3152      42 49 54 53 
 3152      00 
 3153 0d8c 03          	.byte 0x3
 3154 0d8d C5 25       	.2byte 0x25c5
 3155 0d8f 27 0C 00 00 	.4byte 0xc27
 3156 0d93 08          	.uleb128 0x8
 3157 0d94 74 61 67 49 	.asciz "tagIEC1BITS"
 3157      45 43 31 42 
 3157      49 54 53 00 
 3158 0da0 02          	.byte 0x2
 3159 0da1 03          	.byte 0x3
MPLAB XC16 ASSEMBLY Listing:   			page 66


 3160 0da2 CA 25       	.2byte 0x25ca
 3161 0da4 EE 0E 00 00 	.4byte 0xeee
 3162 0da8 05          	.uleb128 0x5
 3163 0da9 53 49 32 43 	.asciz "SI2C1IE"
 3163      31 49 45 00 
 3164 0db1 03          	.byte 0x3
 3165 0db2 CB 25       	.2byte 0x25cb
 3166 0db4 E8 00 00 00 	.4byte 0xe8
 3167 0db8 02          	.byte 0x2
 3168 0db9 01          	.byte 0x1
 3169 0dba 0F          	.byte 0xf
 3170 0dbb 02          	.byte 0x2
 3171 0dbc 23          	.byte 0x23
 3172 0dbd 00          	.uleb128 0x0
 3173 0dbe 05          	.uleb128 0x5
 3174 0dbf 4D 49 32 43 	.asciz "MI2C1IE"
 3174      31 49 45 00 
 3175 0dc7 03          	.byte 0x3
 3176 0dc8 CC 25       	.2byte 0x25cc
 3177 0dca E8 00 00 00 	.4byte 0xe8
 3178 0dce 02          	.byte 0x2
 3179 0dcf 01          	.byte 0x1
 3180 0dd0 0E          	.byte 0xe
 3181 0dd1 02          	.byte 0x2
 3182 0dd2 23          	.byte 0x23
 3183 0dd3 00          	.uleb128 0x0
 3184 0dd4 05          	.uleb128 0x5
 3185 0dd5 43 4D 49 45 	.asciz "CMIE"
 3185      00 
 3186 0dda 03          	.byte 0x3
 3187 0ddb CD 25       	.2byte 0x25cd
 3188 0ddd E8 00 00 00 	.4byte 0xe8
 3189 0de1 02          	.byte 0x2
 3190 0de2 01          	.byte 0x1
 3191 0de3 0D          	.byte 0xd
 3192 0de4 02          	.byte 0x2
 3193 0de5 23          	.byte 0x23
 3194 0de6 00          	.uleb128 0x0
 3195 0de7 05          	.uleb128 0x5
 3196 0de8 43 4E 49 45 	.asciz "CNIE"
 3196      00 
 3197 0ded 03          	.byte 0x3
 3198 0dee CE 25       	.2byte 0x25ce
 3199 0df0 E8 00 00 00 	.4byte 0xe8
 3200 0df4 02          	.byte 0x2
 3201 0df5 01          	.byte 0x1
 3202 0df6 0C          	.byte 0xc
 3203 0df7 02          	.byte 0x2
 3204 0df8 23          	.byte 0x23
 3205 0df9 00          	.uleb128 0x0
 3206 0dfa 05          	.uleb128 0x5
 3207 0dfb 49 4E 54 31 	.asciz "INT1IE"
 3207      49 45 00 
 3208 0e02 03          	.byte 0x3
 3209 0e03 CF 25       	.2byte 0x25cf
 3210 0e05 E8 00 00 00 	.4byte 0xe8
 3211 0e09 02          	.byte 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 67


 3212 0e0a 01          	.byte 0x1
 3213 0e0b 0B          	.byte 0xb
 3214 0e0c 02          	.byte 0x2
 3215 0e0d 23          	.byte 0x23
 3216 0e0e 00          	.uleb128 0x0
 3217 0e0f 05          	.uleb128 0x5
 3218 0e10 41 44 32 49 	.asciz "AD2IE"
 3218      45 00 
 3219 0e16 03          	.byte 0x3
 3220 0e17 D0 25       	.2byte 0x25d0
 3221 0e19 E8 00 00 00 	.4byte 0xe8
 3222 0e1d 02          	.byte 0x2
 3223 0e1e 01          	.byte 0x1
 3224 0e1f 0A          	.byte 0xa
 3225 0e20 02          	.byte 0x2
 3226 0e21 23          	.byte 0x23
 3227 0e22 00          	.uleb128 0x0
 3228 0e23 05          	.uleb128 0x5
 3229 0e24 49 43 37 49 	.asciz "IC7IE"
 3229      45 00 
 3230 0e2a 03          	.byte 0x3
 3231 0e2b D1 25       	.2byte 0x25d1
 3232 0e2d E8 00 00 00 	.4byte 0xe8
 3233 0e31 02          	.byte 0x2
 3234 0e32 01          	.byte 0x1
 3235 0e33 09          	.byte 0x9
 3236 0e34 02          	.byte 0x2
 3237 0e35 23          	.byte 0x23
 3238 0e36 00          	.uleb128 0x0
 3239 0e37 05          	.uleb128 0x5
 3240 0e38 49 43 38 49 	.asciz "IC8IE"
 3240      45 00 
 3241 0e3e 03          	.byte 0x3
 3242 0e3f D2 25       	.2byte 0x25d2
 3243 0e41 E8 00 00 00 	.4byte 0xe8
 3244 0e45 02          	.byte 0x2
 3245 0e46 01          	.byte 0x1
 3246 0e47 08          	.byte 0x8
 3247 0e48 02          	.byte 0x2
 3248 0e49 23          	.byte 0x23
 3249 0e4a 00          	.uleb128 0x0
 3250 0e4b 05          	.uleb128 0x5
 3251 0e4c 44 4D 41 32 	.asciz "DMA2IE"
 3251      49 45 00 
 3252 0e53 03          	.byte 0x3
 3253 0e54 D3 25       	.2byte 0x25d3
 3254 0e56 E8 00 00 00 	.4byte 0xe8
 3255 0e5a 02          	.byte 0x2
 3256 0e5b 01          	.byte 0x1
 3257 0e5c 07          	.byte 0x7
 3258 0e5d 02          	.byte 0x2
 3259 0e5e 23          	.byte 0x23
 3260 0e5f 00          	.uleb128 0x0
 3261 0e60 05          	.uleb128 0x5
 3262 0e61 4F 43 33 49 	.asciz "OC3IE"
 3262      45 00 
 3263 0e67 03          	.byte 0x3
MPLAB XC16 ASSEMBLY Listing:   			page 68


 3264 0e68 D4 25       	.2byte 0x25d4
 3265 0e6a E8 00 00 00 	.4byte 0xe8
 3266 0e6e 02          	.byte 0x2
 3267 0e6f 01          	.byte 0x1
 3268 0e70 06          	.byte 0x6
 3269 0e71 02          	.byte 0x2
 3270 0e72 23          	.byte 0x23
 3271 0e73 00          	.uleb128 0x0
 3272 0e74 05          	.uleb128 0x5
 3273 0e75 4F 43 34 49 	.asciz "OC4IE"
 3273      45 00 
 3274 0e7b 03          	.byte 0x3
 3275 0e7c D5 25       	.2byte 0x25d5
 3276 0e7e E8 00 00 00 	.4byte 0xe8
 3277 0e82 02          	.byte 0x2
 3278 0e83 01          	.byte 0x1
 3279 0e84 05          	.byte 0x5
 3280 0e85 02          	.byte 0x2
 3281 0e86 23          	.byte 0x23
 3282 0e87 00          	.uleb128 0x0
 3283 0e88 05          	.uleb128 0x5
 3284 0e89 54 34 49 45 	.asciz "T4IE"
 3284      00 
 3285 0e8e 03          	.byte 0x3
 3286 0e8f D6 25       	.2byte 0x25d6
 3287 0e91 E8 00 00 00 	.4byte 0xe8
 3288 0e95 02          	.byte 0x2
 3289 0e96 01          	.byte 0x1
 3290 0e97 04          	.byte 0x4
 3291 0e98 02          	.byte 0x2
 3292 0e99 23          	.byte 0x23
 3293 0e9a 00          	.uleb128 0x0
 3294 0e9b 05          	.uleb128 0x5
 3295 0e9c 54 35 49 45 	.asciz "T5IE"
 3295      00 
 3296 0ea1 03          	.byte 0x3
 3297 0ea2 D7 25       	.2byte 0x25d7
 3298 0ea4 E8 00 00 00 	.4byte 0xe8
 3299 0ea8 02          	.byte 0x2
 3300 0ea9 01          	.byte 0x1
 3301 0eaa 03          	.byte 0x3
 3302 0eab 02          	.byte 0x2
 3303 0eac 23          	.byte 0x23
 3304 0ead 00          	.uleb128 0x0
 3305 0eae 05          	.uleb128 0x5
 3306 0eaf 49 4E 54 32 	.asciz "INT2IE"
 3306      49 45 00 
 3307 0eb6 03          	.byte 0x3
 3308 0eb7 D8 25       	.2byte 0x25d8
 3309 0eb9 E8 00 00 00 	.4byte 0xe8
 3310 0ebd 02          	.byte 0x2
 3311 0ebe 01          	.byte 0x1
 3312 0ebf 02          	.byte 0x2
 3313 0ec0 02          	.byte 0x2
 3314 0ec1 23          	.byte 0x23
 3315 0ec2 00          	.uleb128 0x0
 3316 0ec3 05          	.uleb128 0x5
MPLAB XC16 ASSEMBLY Listing:   			page 69


 3317 0ec4 55 32 52 58 	.asciz "U2RXIE"
 3317      49 45 00 
 3318 0ecb 03          	.byte 0x3
 3319 0ecc D9 25       	.2byte 0x25d9
 3320 0ece E8 00 00 00 	.4byte 0xe8
 3321 0ed2 02          	.byte 0x2
 3322 0ed3 01          	.byte 0x1
 3323 0ed4 01          	.byte 0x1
 3324 0ed5 02          	.byte 0x2
 3325 0ed6 23          	.byte 0x23
 3326 0ed7 00          	.uleb128 0x0
 3327 0ed8 05          	.uleb128 0x5
 3328 0ed9 55 32 54 58 	.asciz "U2TXIE"
 3328      49 45 00 
 3329 0ee0 03          	.byte 0x3
 3330 0ee1 DA 25       	.2byte 0x25da
 3331 0ee3 E8 00 00 00 	.4byte 0xe8
 3332 0ee7 02          	.byte 0x2
 3333 0ee8 01          	.byte 0x1
 3334 0ee9 10          	.byte 0x10
 3335 0eea 02          	.byte 0x2
 3336 0eeb 23          	.byte 0x23
 3337 0eec 00          	.uleb128 0x0
 3338 0eed 00          	.byte 0x0
 3339 0eee 0A          	.uleb128 0xa
 3340 0eef 49 45 43 31 	.asciz "IEC1BITS"
 3340      42 49 54 53 
 3340      00 
 3341 0ef8 03          	.byte 0x3
 3342 0ef9 DB 25       	.2byte 0x25db
 3343 0efb 93 0D 00 00 	.4byte 0xd93
 3344 0eff 04          	.uleb128 0x4
 3345 0f00 02          	.byte 0x2
 3346 0f01 03          	.byte 0x3
 3347 0f02 C8 26       	.2byte 0x26c8
 3348 0f04 5B 0F 00 00 	.4byte 0xf5b
 3349 0f08 05          	.uleb128 0x5
 3350 0f09 55 31 54 58 	.asciz "U1TXIP"
 3350      49 50 00 
 3351 0f10 03          	.byte 0x3
 3352 0f11 C9 26       	.2byte 0x26c9
 3353 0f13 E8 00 00 00 	.4byte 0xe8
 3354 0f17 02          	.byte 0x2
 3355 0f18 03          	.byte 0x3
 3356 0f19 0D          	.byte 0xd
 3357 0f1a 02          	.byte 0x2
 3358 0f1b 23          	.byte 0x23
 3359 0f1c 00          	.uleb128 0x0
 3360 0f1d 05          	.uleb128 0x5
 3361 0f1e 41 44 31 49 	.asciz "AD1IP"
 3361      50 00 
 3362 0f24 03          	.byte 0x3
 3363 0f25 CB 26       	.2byte 0x26cb
 3364 0f27 E8 00 00 00 	.4byte 0xe8
 3365 0f2b 02          	.byte 0x2
 3366 0f2c 03          	.byte 0x3
 3367 0f2d 09          	.byte 0x9
MPLAB XC16 ASSEMBLY Listing:   			page 70


 3368 0f2e 02          	.byte 0x2
 3369 0f2f 23          	.byte 0x23
 3370 0f30 00          	.uleb128 0x0
 3371 0f31 05          	.uleb128 0x5
 3372 0f32 44 4D 41 31 	.asciz "DMA1IP"
 3372      49 50 00 
 3373 0f39 03          	.byte 0x3
 3374 0f3a CD 26       	.2byte 0x26cd
 3375 0f3c E8 00 00 00 	.4byte 0xe8
 3376 0f40 02          	.byte 0x2
 3377 0f41 03          	.byte 0x3
 3378 0f42 05          	.byte 0x5
 3379 0f43 02          	.byte 0x2
 3380 0f44 23          	.byte 0x23
 3381 0f45 00          	.uleb128 0x0
 3382 0f46 05          	.uleb128 0x5
 3383 0f47 4E 56 4D 49 	.asciz "NVMIP"
 3383      50 00 
 3384 0f4d 03          	.byte 0x3
 3385 0f4e CF 26       	.2byte 0x26cf
 3386 0f50 E8 00 00 00 	.4byte 0xe8
 3387 0f54 02          	.byte 0x2
 3388 0f55 03          	.byte 0x3
 3389 0f56 01          	.byte 0x1
 3390 0f57 02          	.byte 0x2
 3391 0f58 23          	.byte 0x23
 3392 0f59 00          	.uleb128 0x0
 3393 0f5a 00          	.byte 0x0
 3394 0f5b 04          	.uleb128 0x4
 3395 0f5c 02          	.byte 0x2
 3396 0f5d 03          	.byte 0x3
 3397 0f5e D1 26       	.2byte 0x26d1
 3398 0f60 67 10 00 00 	.4byte 0x1067
 3399 0f64 05          	.uleb128 0x5
 3400 0f65 55 31 54 58 	.asciz "U1TXIP0"
 3400      49 50 30 00 
 3401 0f6d 03          	.byte 0x3
 3402 0f6e D2 26       	.2byte 0x26d2
 3403 0f70 E8 00 00 00 	.4byte 0xe8
 3404 0f74 02          	.byte 0x2
 3405 0f75 01          	.byte 0x1
 3406 0f76 0F          	.byte 0xf
 3407 0f77 02          	.byte 0x2
 3408 0f78 23          	.byte 0x23
 3409 0f79 00          	.uleb128 0x0
 3410 0f7a 05          	.uleb128 0x5
 3411 0f7b 55 31 54 58 	.asciz "U1TXIP1"
 3411      49 50 31 00 
 3412 0f83 03          	.byte 0x3
 3413 0f84 D3 26       	.2byte 0x26d3
 3414 0f86 E8 00 00 00 	.4byte 0xe8
 3415 0f8a 02          	.byte 0x2
 3416 0f8b 01          	.byte 0x1
 3417 0f8c 0E          	.byte 0xe
 3418 0f8d 02          	.byte 0x2
 3419 0f8e 23          	.byte 0x23
 3420 0f8f 00          	.uleb128 0x0
MPLAB XC16 ASSEMBLY Listing:   			page 71


 3421 0f90 05          	.uleb128 0x5
 3422 0f91 55 31 54 58 	.asciz "U1TXIP2"
 3422      49 50 32 00 
 3423 0f99 03          	.byte 0x3
 3424 0f9a D4 26       	.2byte 0x26d4
 3425 0f9c E8 00 00 00 	.4byte 0xe8
 3426 0fa0 02          	.byte 0x2
 3427 0fa1 01          	.byte 0x1
 3428 0fa2 0D          	.byte 0xd
 3429 0fa3 02          	.byte 0x2
 3430 0fa4 23          	.byte 0x23
 3431 0fa5 00          	.uleb128 0x0
 3432 0fa6 05          	.uleb128 0x5
 3433 0fa7 41 44 31 49 	.asciz "AD1IP0"
 3433      50 30 00 
 3434 0fae 03          	.byte 0x3
 3435 0faf D6 26       	.2byte 0x26d6
 3436 0fb1 E8 00 00 00 	.4byte 0xe8
 3437 0fb5 02          	.byte 0x2
 3438 0fb6 01          	.byte 0x1
 3439 0fb7 0B          	.byte 0xb
 3440 0fb8 02          	.byte 0x2
 3441 0fb9 23          	.byte 0x23
 3442 0fba 00          	.uleb128 0x0
 3443 0fbb 05          	.uleb128 0x5
 3444 0fbc 41 44 31 49 	.asciz "AD1IP1"
 3444      50 31 00 
 3445 0fc3 03          	.byte 0x3
 3446 0fc4 D7 26       	.2byte 0x26d7
 3447 0fc6 E8 00 00 00 	.4byte 0xe8
 3448 0fca 02          	.byte 0x2
 3449 0fcb 01          	.byte 0x1
 3450 0fcc 0A          	.byte 0xa
 3451 0fcd 02          	.byte 0x2
 3452 0fce 23          	.byte 0x23
 3453 0fcf 00          	.uleb128 0x0
 3454 0fd0 05          	.uleb128 0x5
 3455 0fd1 41 44 31 49 	.asciz "AD1IP2"
 3455      50 32 00 
 3456 0fd8 03          	.byte 0x3
 3457 0fd9 D8 26       	.2byte 0x26d8
 3458 0fdb E8 00 00 00 	.4byte 0xe8
 3459 0fdf 02          	.byte 0x2
 3460 0fe0 01          	.byte 0x1
 3461 0fe1 09          	.byte 0x9
 3462 0fe2 02          	.byte 0x2
 3463 0fe3 23          	.byte 0x23
 3464 0fe4 00          	.uleb128 0x0
 3465 0fe5 05          	.uleb128 0x5
 3466 0fe6 44 4D 41 31 	.asciz "DMA1IP0"
 3466      49 50 30 00 
 3467 0fee 03          	.byte 0x3
 3468 0fef DA 26       	.2byte 0x26da
 3469 0ff1 E8 00 00 00 	.4byte 0xe8
 3470 0ff5 02          	.byte 0x2
 3471 0ff6 01          	.byte 0x1
 3472 0ff7 07          	.byte 0x7
MPLAB XC16 ASSEMBLY Listing:   			page 72


 3473 0ff8 02          	.byte 0x2
 3474 0ff9 23          	.byte 0x23
 3475 0ffa 00          	.uleb128 0x0
 3476 0ffb 05          	.uleb128 0x5
 3477 0ffc 44 4D 41 31 	.asciz "DMA1IP1"
 3477      49 50 31 00 
 3478 1004 03          	.byte 0x3
 3479 1005 DB 26       	.2byte 0x26db
 3480 1007 E8 00 00 00 	.4byte 0xe8
 3481 100b 02          	.byte 0x2
 3482 100c 01          	.byte 0x1
 3483 100d 06          	.byte 0x6
 3484 100e 02          	.byte 0x2
 3485 100f 23          	.byte 0x23
 3486 1010 00          	.uleb128 0x0
 3487 1011 05          	.uleb128 0x5
 3488 1012 44 4D 41 31 	.asciz "DMA1IP2"
 3488      49 50 32 00 
 3489 101a 03          	.byte 0x3
 3490 101b DC 26       	.2byte 0x26dc
 3491 101d E8 00 00 00 	.4byte 0xe8
 3492 1021 02          	.byte 0x2
 3493 1022 01          	.byte 0x1
 3494 1023 05          	.byte 0x5
 3495 1024 02          	.byte 0x2
 3496 1025 23          	.byte 0x23
 3497 1026 00          	.uleb128 0x0
 3498 1027 05          	.uleb128 0x5
 3499 1028 4E 56 4D 49 	.asciz "NVMIP0"
 3499      50 30 00 
 3500 102f 03          	.byte 0x3
 3501 1030 DE 26       	.2byte 0x26de
 3502 1032 E8 00 00 00 	.4byte 0xe8
 3503 1036 02          	.byte 0x2
 3504 1037 01          	.byte 0x1
 3505 1038 03          	.byte 0x3
 3506 1039 02          	.byte 0x2
 3507 103a 23          	.byte 0x23
 3508 103b 00          	.uleb128 0x0
 3509 103c 05          	.uleb128 0x5
 3510 103d 4E 56 4D 49 	.asciz "NVMIP1"
 3510      50 31 00 
 3511 1044 03          	.byte 0x3
 3512 1045 DF 26       	.2byte 0x26df
 3513 1047 E8 00 00 00 	.4byte 0xe8
 3514 104b 02          	.byte 0x2
 3515 104c 01          	.byte 0x1
 3516 104d 02          	.byte 0x2
 3517 104e 02          	.byte 0x2
 3518 104f 23          	.byte 0x23
 3519 1050 00          	.uleb128 0x0
 3520 1051 05          	.uleb128 0x5
 3521 1052 4E 56 4D 49 	.asciz "NVMIP2"
 3521      50 32 00 
 3522 1059 03          	.byte 0x3
 3523 105a E0 26       	.2byte 0x26e0
 3524 105c E8 00 00 00 	.4byte 0xe8
MPLAB XC16 ASSEMBLY Listing:   			page 73


 3525 1060 02          	.byte 0x2
 3526 1061 01          	.byte 0x1
 3527 1062 01          	.byte 0x1
 3528 1063 02          	.byte 0x2
 3529 1064 23          	.byte 0x23
 3530 1065 00          	.uleb128 0x0
 3531 1066 00          	.byte 0x0
 3532 1067 06          	.uleb128 0x6
 3533 1068 02          	.byte 0x2
 3534 1069 03          	.byte 0x3
 3535 106a C7 26       	.2byte 0x26c7
 3536 106c 7B 10 00 00 	.4byte 0x107b
 3537 1070 07          	.uleb128 0x7
 3538 1071 FF 0E 00 00 	.4byte 0xeff
 3539 1075 07          	.uleb128 0x7
 3540 1076 5B 0F 00 00 	.4byte 0xf5b
 3541 107a 00          	.byte 0x0
 3542 107b 08          	.uleb128 0x8
 3543 107c 74 61 67 49 	.asciz "tagIPC3BITS"
 3543      50 43 33 42 
 3543      49 54 53 00 
 3544 1088 02          	.byte 0x2
 3545 1089 03          	.byte 0x3
 3546 108a C6 26       	.2byte 0x26c6
 3547 108c 99 10 00 00 	.4byte 0x1099
 3548 1090 09          	.uleb128 0x9
 3549 1091 67 10 00 00 	.4byte 0x1067
 3550 1095 02          	.byte 0x2
 3551 1096 23          	.byte 0x23
 3552 1097 00          	.uleb128 0x0
 3553 1098 00          	.byte 0x0
 3554 1099 0A          	.uleb128 0xa
 3555 109a 49 50 43 33 	.asciz "IPC3BITS"
 3555      42 49 54 53 
 3555      00 
 3556 10a3 03          	.byte 0x3
 3557 10a4 E3 26       	.2byte 0x26e3
 3558 10a6 7B 10 00 00 	.4byte 0x107b
 3559 10aa 02          	.uleb128 0x2
 3560 10ab 02          	.byte 0x2
 3561 10ac 07          	.byte 0x7
 3562 10ad 73 68 6F 72 	.asciz "short unsigned int"
 3562      74 20 75 6E 
 3562      73 69 67 6E 
 3562      65 64 20 69 
 3562      6E 74 00 
 3563 10c0 0B          	.uleb128 0xb
 3564 10c1 02          	.byte 0x2
 3565 10c2 04          	.byte 0x4
 3566 10c3 1F          	.byte 0x1f
 3567 10c4 D9 10 00 00 	.4byte 0x10d9
 3568 10c8 0C          	.uleb128 0xc
 3569 10c9 4D 41 4E 55 	.asciz "MANUAL"
 3569      41 4C 00 
 3570 10d0 00          	.sleb128 0
 3571 10d1 0C          	.uleb128 0xc
 3572 10d2 41 55 54 4F 	.asciz "AUTO"
MPLAB XC16 ASSEMBLY Listing:   			page 74


 3572      00 
 3573 10d7 01          	.sleb128 1
 3574 10d8 00          	.byte 0x0
 3575 10d9 03          	.uleb128 0x3
 3576 10da 53 61 6D 70 	.asciz "Sampling"
 3576      6C 69 6E 67 
 3576      00 
 3577 10e3 04          	.byte 0x4
 3578 10e4 1F          	.byte 0x1f
 3579 10e5 C0 10 00 00 	.4byte 0x10c0
 3580 10e9 0B          	.uleb128 0xb
 3581 10ea 02          	.byte 0x2
 3582 10eb 04          	.byte 0x4
 3583 10ec 24          	.byte 0x24
 3584 10ed 2E 11 00 00 	.4byte 0x112e
 3585 10f1 0C          	.uleb128 0xc
 3586 10f2 53 41 4D 50 	.asciz "SAMP"
 3586      00 
 3587 10f7 00          	.sleb128 0
 3588 10f8 0C          	.uleb128 0xc
 3589 10f9 49 4E 54 30 	.asciz "INT0"
 3589      00 
 3590 10fe 01          	.sleb128 1
 3591 10ff 0C          	.uleb128 0xc
 3592 1100 54 49 4D 45 	.asciz "TIMER3"
 3592      52 33 00 
 3593 1107 02          	.sleb128 2
 3594 1108 0C          	.uleb128 0xc
 3595 1109 50 57 4D 5F 	.asciz "PWM_P"
 3595      50 00 
 3596 110f 03          	.sleb128 3
 3597 1110 0C          	.uleb128 0xc
 3598 1111 54 49 4D 45 	.asciz "TIMER5"
 3598      52 35 00 
 3599 1118 04          	.sleb128 4
 3600 1119 0C          	.uleb128 0xc
 3601 111a 50 57 4D 5F 	.asciz "PWM_S"
 3601      53 00 
 3602 1120 05          	.sleb128 5
 3603 1121 0C          	.uleb128 0xc
 3604 1122 41 55 54 4F 	.asciz "AUTO_CONV"
 3604      5F 43 4F 4E 
 3604      56 00 
 3605 112c 07          	.sleb128 7
 3606 112d 00          	.byte 0x0
 3607 112e 03          	.uleb128 0x3
 3608 112f 53 53 52 43 	.asciz "SSRC"
 3608      00 
 3609 1134 04          	.byte 0x4
 3610 1135 24          	.byte 0x24
 3611 1136 E9 10 00 00 	.4byte 0x10e9
 3612 113a 0B          	.uleb128 0xb
 3613 113b 02          	.byte 0x2
 3614 113c 04          	.byte 0x4
 3615 113d 2A          	.byte 0x2a
 3616 113e 81 11 00 00 	.4byte 0x1181
 3617 1142 0C          	.uleb128 0xc
MPLAB XC16 ASSEMBLY Listing:   			page 75


 3618 1143 49 4E 54 45 	.asciz "INTERNAL"
 3618      52 4E 41 4C 
 3618      00 
 3619 114c 00          	.sleb128 0
 3620 114d 0C          	.uleb128 0xc
 3621 114e 50 4F 53 49 	.asciz "POSITIVE_EXTERNAL"
 3621      54 49 56 45 
 3621      5F 45 58 54 
 3621      45 52 4E 41 
 3621      4C 00 
 3622 1160 01          	.sleb128 1
 3623 1161 0C          	.uleb128 0xc
 3624 1162 4E 45 47 41 	.asciz "NEGATIVE_EXTERNAL"
 3624      54 49 56 45 
 3624      5F 45 58 54 
 3624      45 52 4E 41 
 3624      4C 00 
 3625 1174 02          	.sleb128 2
 3626 1175 0C          	.uleb128 0xc
 3627 1176 45 58 54 45 	.asciz "EXTERNAL"
 3627      52 4E 41 4C 
 3627      00 
 3628 117f 03          	.sleb128 3
 3629 1180 00          	.byte 0x0
 3630 1181 03          	.uleb128 0x3
 3631 1182 56 6F 6C 74 	.asciz "Voltage"
 3631      61 67 65 00 
 3632 118a 04          	.byte 0x4
 3633 118b 2F          	.byte 0x2f
 3634 118c 3A 11 00 00 	.4byte 0x113a
 3635 1190 0B          	.uleb128 0xb
 3636 1191 02          	.byte 0x2
 3637 1192 04          	.byte 0x4
 3638 1193 4D          	.byte 0x4d
 3639 1194 E9 11 00 00 	.4byte 0x11e9
 3640 1198 0C          	.uleb128 0xc
 3641 1199 54 4F 4F 5F 	.asciz "TOO_FAST_ADC"
 3641      46 41 53 54 
 3641      5F 41 44 43 
 3641      00 
 3642 11a6 00          	.sleb128 0
 3643 11a7 0C          	.uleb128 0xc
 3644 11a8 54 4F 4F 5F 	.asciz "TOO_SLOW_ADC"
 3644      53 4C 4F 57 
 3644      5F 41 44 43 
 3644      00 
 3645 11b5 01          	.sleb128 1
 3646 11b6 0C          	.uleb128 0xc
 3647 11b7 50 4F 49 4E 	.asciz "POINTER_NULL_ADC"
 3647      54 45 52 5F 
 3647      4E 55 4C 4C 
 3647      5F 41 44 43 
 3647      00 
 3648 11c8 02          	.sleb128 2
 3649 11c9 0C          	.uleb128 0xc
 3650 11ca 57 52 4F 4E 	.asciz "WRONG_CLOCK_ADC"
 3650      47 5F 43 4C 
MPLAB XC16 ASSEMBLY Listing:   			page 76


 3650      4F 43 4B 5F 
 3650      41 44 43 00 
 3651 11da 03          	.sleb128 3
 3652 11db 0C          	.uleb128 0xc
 3653 11dc 53 55 43 45 	.asciz "SUCESS_ADC"
 3653      53 53 5F 41 
 3653      44 43 00 
 3654 11e7 04          	.sleb128 4
 3655 11e8 00          	.byte 0x0
 3656 11e9 03          	.uleb128 0x3
 3657 11ea 50 69 6E 5F 	.asciz "Pin_Cycling_Error"
 3657      43 79 63 6C 
 3657      69 6E 67 5F 
 3657      45 72 72 6F 
 3657      72 00 
 3658 11fc 04          	.byte 0x4
 3659 11fd 53          	.byte 0x53
 3660 11fe 90 11 00 00 	.4byte 0x1190
 3661 1202 0D          	.uleb128 0xd
 3662 1203 0A          	.byte 0xa
 3663 1204 04          	.byte 0x4
 3664 1205 58          	.byte 0x58
 3665 1206 3B 13 00 00 	.4byte 0x133b
 3666 120a 0E          	.uleb128 0xe
 3667 120b 69 64 6C 65 	.asciz "idle"
 3667      00 
 3668 1210 04          	.byte 0x4
 3669 1211 5A          	.byte 0x5a
 3670 1212 F8 00 00 00 	.4byte 0xf8
 3671 1216 02          	.byte 0x2
 3672 1217 01          	.byte 0x1
 3673 1218 0F          	.byte 0xf
 3674 1219 02          	.byte 0x2
 3675 121a 23          	.byte 0x23
 3676 121b 00          	.uleb128 0x0
 3677 121c 0E          	.uleb128 0xe
 3678 121d 66 6F 72 6D 	.asciz "form"
 3678      00 
 3679 1222 04          	.byte 0x4
 3680 1223 5B          	.byte 0x5b
 3681 1224 F8 00 00 00 	.4byte 0xf8
 3682 1228 02          	.byte 0x2
 3683 1229 02          	.byte 0x2
 3684 122a 0D          	.byte 0xd
 3685 122b 02          	.byte 0x2
 3686 122c 23          	.byte 0x23
 3687 122d 00          	.uleb128 0x0
 3688 122e 0E          	.uleb128 0xe
 3689 122f 63 6F 6E 76 	.asciz "conversion_trigger"
 3689      65 72 73 69 
 3689      6F 6E 5F 74 
 3689      72 69 67 67 
 3689      65 72 00 
 3690 1242 04          	.byte 0x4
 3691 1243 5E          	.byte 0x5e
 3692 1244 2E 11 00 00 	.4byte 0x112e
 3693 1248 02          	.byte 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 77


 3694 1249 03          	.byte 0x3
 3695 124a 0A          	.byte 0xa
 3696 124b 02          	.byte 0x2
 3697 124c 23          	.byte 0x23
 3698 124d 00          	.uleb128 0x0
 3699 124e 0E          	.uleb128 0xe
 3700 124f 73 61 6D 70 	.asciz "sampling_type"
 3700      6C 69 6E 67 
 3700      5F 74 79 70 
 3700      65 00 
 3701 125d 04          	.byte 0x4
 3702 125e 62          	.byte 0x62
 3703 125f D9 10 00 00 	.4byte 0x10d9
 3704 1263 02          	.byte 0x2
 3705 1264 01          	.byte 0x1
 3706 1265 09          	.byte 0x9
 3707 1266 02          	.byte 0x2
 3708 1267 23          	.byte 0x23
 3709 1268 00          	.uleb128 0x0
 3710 1269 0E          	.uleb128 0xe
 3711 126a 76 6F 6C 74 	.asciz "voltage_ref"
 3711      61 67 65 5F 
 3711      72 65 66 00 
 3712 1276 04          	.byte 0x4
 3713 1277 63          	.byte 0x63
 3714 1278 81 11 00 00 	.4byte 0x1181
 3715 127c 02          	.byte 0x2
 3716 127d 02          	.byte 0x2
 3717 127e 07          	.byte 0x7
 3718 127f 02          	.byte 0x2
 3719 1280 23          	.byte 0x23
 3720 1281 00          	.uleb128 0x0
 3721 1282 0E          	.uleb128 0xe
 3722 1283 73 61 6D 70 	.asciz "sample_pin_select_type"
 3722      6C 65 5F 70 
 3722      69 6E 5F 73 
 3722      65 6C 65 63 
 3722      74 5F 74 79 
 3722      70 65 00 
 3723 129a 04          	.byte 0x4
 3724 129b 65          	.byte 0x65
 3725 129c D9 10 00 00 	.4byte 0x10d9
 3726 12a0 02          	.byte 0x2
 3727 12a1 01          	.byte 0x1
 3728 12a2 06          	.byte 0x6
 3729 12a3 02          	.byte 0x2
 3730 12a4 23          	.byte 0x23
 3731 12a5 00          	.uleb128 0x0
 3732 12a6 0E          	.uleb128 0xe
 3733 12a7 73 6D 70 69 	.asciz "smpi"
 3733      00 
 3734 12ac 04          	.byte 0x4
 3735 12ad 67          	.byte 0x67
 3736 12ae F8 00 00 00 	.4byte 0xf8
 3737 12b2 02          	.byte 0x2
 3738 12b3 04          	.byte 0x4
 3739 12b4 02          	.byte 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 78


 3740 12b5 02          	.byte 0x2
 3741 12b6 23          	.byte 0x23
 3742 12b7 00          	.uleb128 0x0
 3743 12b8 0E          	.uleb128 0xe
 3744 12b9 70 69 6E 5F 	.asciz "pin_select"
 3744      73 65 6C 65 
 3744      63 74 00 
 3745 12c4 04          	.byte 0x4
 3746 12c5 68          	.byte 0x68
 3747 12c6 F8 00 00 00 	.4byte 0xf8
 3748 12ca 02          	.byte 0x2
 3749 12cb 10          	.byte 0x10
 3750 12cc 10          	.byte 0x10
 3751 12cd 02          	.byte 0x2
 3752 12ce 23          	.byte 0x23
 3753 12cf 02          	.uleb128 0x2
 3754 12d0 0F          	.uleb128 0xf
 3755 12d1 61 75 74 6F 	.asciz "auto_sample_time"
 3755      5F 73 61 6D 
 3755      70 6C 65 5F 
 3755      74 69 6D 65 
 3755      00 
 3756 12e2 04          	.byte 0x4
 3757 12e3 6A          	.byte 0x6a
 3758 12e4 F8 00 00 00 	.4byte 0xf8
 3759 12e8 02          	.byte 0x2
 3760 12e9 23          	.byte 0x23
 3761 12ea 04          	.uleb128 0x4
 3762 12eb 0F          	.uleb128 0xf
 3763 12ec 63 6F 6E 76 	.asciz "conversion_time"
 3763      65 72 73 69 
 3763      6F 6E 5F 74 
 3763      69 6D 65 00 
 3764 12fc 04          	.byte 0x4
 3765 12fd 6B          	.byte 0x6b
 3766 12fe F8 00 00 00 	.4byte 0xf8
 3767 1302 02          	.byte 0x2
 3768 1303 23          	.byte 0x23
 3769 1304 06          	.uleb128 0x6
 3770 1305 0E          	.uleb128 0xe
 3771 1306 69 6E 74 65 	.asciz "interrupt_priority"
 3771      72 72 75 70 
 3771      74 5F 70 72 
 3771      69 6F 72 69 
 3771      74 79 00 
 3772 1319 04          	.byte 0x4
 3773 131a 6C          	.byte 0x6c
 3774 131b F8 00 00 00 	.4byte 0xf8
 3775 131f 02          	.byte 0x2
 3776 1320 03          	.byte 0x3
 3777 1321 0D          	.byte 0xd
 3778 1322 02          	.byte 0x2
 3779 1323 23          	.byte 0x23
 3780 1324 08          	.uleb128 0x8
 3781 1325 0E          	.uleb128 0xe
 3782 1326 74 75 72 6E 	.asciz "turn_on"
 3782      5F 6F 6E 00 
MPLAB XC16 ASSEMBLY Listing:   			page 79


 3783 132e 04          	.byte 0x4
 3784 132f 6D          	.byte 0x6d
 3785 1330 F8 00 00 00 	.4byte 0xf8
 3786 1334 02          	.byte 0x2
 3787 1335 01          	.byte 0x1
 3788 1336 0C          	.byte 0xc
 3789 1337 02          	.byte 0x2
 3790 1338 23          	.byte 0x23
 3791 1339 08          	.uleb128 0x8
 3792 133a 00          	.byte 0x0
 3793 133b 03          	.uleb128 0x3
 3794 133c 41 44 43 5F 	.asciz "ADC_parameters"
 3794      70 61 72 61 
 3794      6D 65 74 65 
 3794      72 73 00 
 3795 134b 04          	.byte 0x4
 3796 134c 70          	.byte 0x70
 3797 134d 02 12 00 00 	.4byte 0x1202
 3798 1351 10          	.uleb128 0x10
 3799 1352 01          	.byte 0x1
 3800 1353 63 6F 6E 66 	.asciz "config_adc1"
 3800      69 67 5F 61 
 3800      64 63 31 00 
 3801 135f 01          	.byte 0x1
 3802 1360 0A          	.byte 0xa
 3803 1361 01          	.byte 0x1
 3804 1362 00 00 00 00 	.4byte .LFB0
 3805 1366 00 00 00 00 	.4byte .LFE0
 3806 136a 01          	.byte 0x1
 3807 136b 5E          	.byte 0x5e
 3808 136c 85 13 00 00 	.4byte 0x1385
 3809 1370 11          	.uleb128 0x11
 3810 1371 70 61 72 61 	.asciz "parameter"
 3810      6D 65 74 65 
 3810      72 00 
 3811 137b 01          	.byte 0x1
 3812 137c 0A          	.byte 0xa
 3813 137d 3B 13 00 00 	.4byte 0x133b
 3814 1381 02          	.byte 0x2
 3815 1382 7E          	.byte 0x7e
 3816 1383 00          	.sleb128 0
 3817 1384 00          	.byte 0x0
 3818 1385 12          	.uleb128 0x12
 3819 1386 01          	.byte 0x1
 3820 1387 61 64 63 5F 	.asciz "adc_callback"
 3820      63 61 6C 6C 
 3820      62 61 63 6B 
 3820      00 
 3821 1394 01          	.byte 0x1
 3822 1395 2C          	.byte 0x2c
 3823 1396 01          	.byte 0x1
 3824 1397 00 00 00 00 	.4byte .LFB1
 3825 139b 00 00 00 00 	.4byte .LFE1
 3826 139f 01          	.byte 0x1
 3827 13a0 5E          	.byte 0x5e
 3828 13a1 12          	.uleb128 0x12
 3829 13a2 01          	.byte 0x1
MPLAB XC16 ASSEMBLY Listing:   			page 80


 3830 13a3 5F 41 44 31 	.asciz "_AD1Interrupt"
 3830      49 6E 74 65 
 3830      72 72 75 70 
 3830      74 00 
 3831 13b1 01          	.byte 0x1
 3832 13b2 33          	.byte 0x33
 3833 13b3 01          	.byte 0x1
 3834 13b4 00 00 00 00 	.4byte .LFB2
 3835 13b8 00 00 00 00 	.4byte .LFE2
 3836 13bc 01          	.byte 0x1
 3837 13bd 5E          	.byte 0x5e
 3838 13be 13          	.uleb128 0x13
 3839 13bf 01          	.byte 0x1
 3840 13c0 67 65 74 5F 	.asciz "get_adc_value"
 3840      61 64 63 5F 
 3840      76 61 6C 75 
 3840      65 00 
 3841 13ce 01          	.byte 0x1
 3842 13cf 43          	.byte 0x43
 3843 13d0 01          	.byte 0x1
 3844 13d1 F8 00 00 00 	.4byte 0xf8
 3845 13d5 00 00 00 00 	.4byte .LFB3
 3846 13d9 00 00 00 00 	.4byte .LFE3
 3847 13dd 01          	.byte 0x1
 3848 13de 5E          	.byte 0x5e
 3849 13df FC 13 00 00 	.4byte 0x13fc
 3850 13e3 11          	.uleb128 0x11
 3851 13e4 73 61 6D 70 	.asciz "sample_number"
 3851      6C 65 5F 6E 
 3851      75 6D 62 65 
 3851      72 00 
 3852 13f2 01          	.byte 0x1
 3853 13f3 43          	.byte 0x43
 3854 13f4 F8 00 00 00 	.4byte 0xf8
 3855 13f8 02          	.byte 0x2
 3856 13f9 7E          	.byte 0x7e
 3857 13fa 00          	.sleb128 0
 3858 13fb 00          	.byte 0x0
 3859 13fc 14          	.uleb128 0x14
 3860 13fd 01          	.byte 0x1
 3861 13fe 74 75 72 6E 	.asciz "turn_on_adc"
 3861      5F 6F 6E 5F 
 3861      61 64 63 00 
 3862 140a 01          	.byte 0x1
 3863 140b 51          	.byte 0x51
 3864 140c 00 00 00 00 	.4byte .LFB4
 3865 1410 00 00 00 00 	.4byte .LFE4
 3866 1414 01          	.byte 0x1
 3867 1415 5E          	.byte 0x5e
 3868 1416 14          	.uleb128 0x14
 3869 1417 01          	.byte 0x1
 3870 1418 74 75 72 6E 	.asciz "turn_off_adc"
 3870      5F 6F 66 66 
 3870      5F 61 64 63 
 3870      00 
 3871 1425 01          	.byte 0x1
 3872 1426 5B          	.byte 0x5b
MPLAB XC16 ASSEMBLY Listing:   			page 81


 3873 1427 00 00 00 00 	.4byte .LFB5
 3874 142b 00 00 00 00 	.4byte .LFE5
 3875 142f 01          	.byte 0x1
 3876 1430 5E          	.byte 0x5e
 3877 1431 14          	.uleb128 0x14
 3878 1432 01          	.byte 0x1
 3879 1433 73 74 61 72 	.asciz "start_samp"
 3879      74 5F 73 61 
 3879      6D 70 00 
 3880 143e 01          	.byte 0x1
 3881 143f 66          	.byte 0x66
 3882 1440 00 00 00 00 	.4byte .LFB6
 3883 1444 00 00 00 00 	.4byte .LFE6
 3884 1448 01          	.byte 0x1
 3885 1449 5E          	.byte 0x5e
 3886 144a 14          	.uleb128 0x14
 3887 144b 01          	.byte 0x1
 3888 144c 73 74 61 72 	.asciz "start_conversion"
 3888      74 5F 63 6F 
 3888      6E 76 65 72 
 3888      73 69 6F 6E 
 3888      00 
 3889 145d 01          	.byte 0x1
 3890 145e 71          	.byte 0x71
 3891 145f 00 00 00 00 	.4byte .LFB7
 3892 1463 00 00 00 00 	.4byte .LFE7
 3893 1467 01          	.byte 0x1
 3894 1468 5E          	.byte 0x5e
 3895 1469 14          	.uleb128 0x14
 3896 146a 01          	.byte 0x1
 3897 146b 77 61 69 74 	.asciz "wait_for_done"
 3897      5F 66 6F 72 
 3897      5F 64 6F 6E 
 3897      65 00 
 3898 1479 01          	.byte 0x1
 3899 147a 7B          	.byte 0x7b
 3900 147b 00 00 00 00 	.4byte .LFB8
 3901 147f 00 00 00 00 	.4byte .LFE8
 3902 1483 01          	.byte 0x1
 3903 1484 5E          	.byte 0x5e
 3904 1485 10          	.uleb128 0x10
 3905 1486 01          	.byte 0x1
 3906 1487 73 65 74 5F 	.asciz "set_default_parameters"
 3906      64 65 66 61 
 3906      75 6C 74 5F 
 3906      70 61 72 61 
 3906      6D 65 74 65 
 3906      72 73 00 
 3907 149e 01          	.byte 0x1
 3908 149f 86          	.byte 0x86
 3909 14a0 01          	.byte 0x1
 3910 14a1 00 00 00 00 	.4byte .LFB9
 3911 14a5 00 00 00 00 	.4byte .LFE9
 3912 14a9 01          	.byte 0x1
 3913 14aa 5E          	.byte 0x5e
 3914 14ab BE 14 00 00 	.4byte 0x14be
 3915 14af 15          	.uleb128 0x15
MPLAB XC16 ASSEMBLY Listing:   			page 82


 3916 14b0 00 00 00 00 	.4byte .LASF0
 3917 14b4 01          	.byte 0x1
 3918 14b5 86          	.byte 0x86
 3919 14b6 BE 14 00 00 	.4byte 0x14be
 3920 14ba 02          	.byte 0x2
 3921 14bb 7E          	.byte 0x7e
 3922 14bc 00          	.sleb128 0
 3923 14bd 00          	.byte 0x0
 3924 14be 16          	.uleb128 0x16
 3925 14bf 02          	.byte 0x2
 3926 14c0 3B 13 00 00 	.4byte 0x133b
 3927 14c4 10          	.uleb128 0x10
 3928 14c5 01          	.byte 0x1
 3929 14c6 73 65 6C 65 	.asciz "select_input_pin"
 3929      63 74 5F 69 
 3929      6E 70 75 74 
 3929      5F 70 69 6E 
 3929      00 
 3930 14d7 01          	.byte 0x1
 3931 14d8 9B          	.byte 0x9b
 3932 14d9 01          	.byte 0x1
 3933 14da 00 00 00 00 	.4byte .LFB10
 3934 14de 00 00 00 00 	.4byte .LFE10
 3935 14e2 01          	.byte 0x1
 3936 14e3 5E          	.byte 0x5e
 3937 14e4 F7 14 00 00 	.4byte 0x14f7
 3938 14e8 11          	.uleb128 0x11
 3939 14e9 70 69 6E 00 	.asciz "pin"
 3940 14ed 01          	.byte 0x1
 3941 14ee 9B          	.byte 0x9b
 3942 14ef F8 00 00 00 	.4byte 0xf8
 3943 14f3 02          	.byte 0x2
 3944 14f4 7E          	.byte 0x7e
 3945 14f5 00          	.sleb128 0
 3946 14f6 00          	.byte 0x0
 3947 14f7 13          	.uleb128 0x13
 3948 14f8 01          	.byte 0x1
 3949 14f9 63 6F 6E 66 	.asciz "config_pin_cycling"
 3949      69 67 5F 70 
 3949      69 6E 5F 63 
 3949      79 63 6C 69 
 3949      6E 67 00 
 3950 150c 01          	.byte 0x1
 3951 150d BD          	.byte 0xbd
 3952 150e 01          	.byte 0x1
 3953 150f E9 11 00 00 	.4byte 0x11e9
 3954 1513 00 00 00 00 	.4byte .LFB11
 3955 1517 00 00 00 00 	.4byte .LFE11
 3956 151b 01          	.byte 0x1
 3957 151c 5E          	.byte 0x5e
 3958 151d D7 15 00 00 	.4byte 0x15d7
 3959 1521 11          	.uleb128 0x11
 3960 1522 70 69 6E 5F 	.asciz "pin_vector"
 3960      76 65 63 74 
 3960      6F 72 00 
 3961 152d 01          	.byte 0x1
 3962 152e BD          	.byte 0xbd
MPLAB XC16 ASSEMBLY Listing:   			page 83


 3963 152f E8 00 00 00 	.4byte 0xe8
 3964 1533 02          	.byte 0x2
 3965 1534 7E          	.byte 0x7e
 3966 1535 0E          	.sleb128 14
 3967 1536 11          	.uleb128 0x11
 3968 1537 66 72 65 71 	.asciz "frequency"
 3968      75 65 6E 63 
 3968      79 00 
 3969 1541 01          	.byte 0x1
 3970 1542 BD          	.byte 0xbd
 3971 1543 08 01 00 00 	.4byte 0x108
 3972 1547 02          	.byte 0x2
 3973 1548 7E          	.byte 0x7e
 3974 1549 10          	.sleb128 16
 3975 154a 11          	.uleb128 0x11
 3976 154b 73 79 73 5F 	.asciz "sys_clock"
 3976      63 6C 6F 63 
 3976      6B 00 
 3977 1555 01          	.byte 0x1
 3978 1556 BE          	.byte 0xbe
 3979 1557 08 01 00 00 	.4byte 0x108
 3980 155b 02          	.byte 0x2
 3981 155c 7E          	.byte 0x7e
 3982 155d 14          	.sleb128 20
 3983 155e 11          	.uleb128 0x11
 3984 155f 70 72 69 6F 	.asciz "priority"
 3984      72 69 74 79 
 3984      00 
 3985 1568 01          	.byte 0x1
 3986 1569 BE          	.byte 0xbe
 3987 156a F8 00 00 00 	.4byte 0xf8
 3988 156e 02          	.byte 0x2
 3989 156f 7E          	.byte 0x7e
 3990 1570 18          	.sleb128 24
 3991 1571 15          	.uleb128 0x15
 3992 1572 00 00 00 00 	.4byte .LASF0
 3993 1576 01          	.byte 0x1
 3994 1577 BE          	.byte 0xbe
 3995 1578 BE 14 00 00 	.4byte 0x14be
 3996 157c 02          	.byte 0x2
 3997 157d 7E          	.byte 0x7e
 3998 157e 1A          	.sleb128 26
 3999 157f 17          	.uleb128 0x17
 4000 1580 66 6D 69 6E 	.asciz "fmin"
 4000      00 
 4001 1585 01          	.byte 0x1
 4002 1586 C3          	.byte 0xc3
 4003 1587 08 01 00 00 	.4byte 0x108
 4004 158b 02          	.byte 0x2
 4005 158c 7E          	.byte 0x7e
 4006 158d 02          	.sleb128 2
 4007 158e 17          	.uleb128 0x17
 4008 158f 50 72 65 73 	.asciz "Prescaler"
 4008      63 61 6C 65 
 4008      72 00 
 4009 1599 01          	.byte 0x1
 4010 159a DB          	.byte 0xdb
MPLAB XC16 ASSEMBLY Listing:   			page 84


 4011 159b 08 01 00 00 	.4byte 0x108
 4012 159f 02          	.byte 0x2
 4013 15a0 7E          	.byte 0x7e
 4014 15a1 06          	.sleb128 6
 4015 15a2 17          	.uleb128 0x17
 4016 15a3 70 65 72 69 	.asciz "period"
 4016      6F 64 00 
 4017 15aa 01          	.byte 0x1
 4018 15ab DC          	.byte 0xdc
 4019 15ac E8 00 00 00 	.4byte 0xe8
 4020 15b0 02          	.byte 0x2
 4021 15b1 7E          	.byte 0x7e
 4022 15b2 0A          	.sleb128 10
 4023 15b3 17          	.uleb128 0x17
 4024 15b4 69 00       	.asciz "i"
 4025 15b6 01          	.byte 0x1
 4026 15b7 FD          	.byte 0xfd
 4027 15b8 E8 00 00 00 	.4byte 0xe8
 4028 15bc 02          	.byte 0x2
 4029 15bd 7E          	.byte 0x7e
 4030 15be 00          	.sleb128 0
 4031 15bf 17          	.uleb128 0x17
 4032 15c0 63 6C 6F 63 	.asciz "clock_factor"
 4032      6B 5F 66 61 
 4032      63 74 6F 72 
 4032      00 
 4033 15cd 01          	.byte 0x1
 4034 15ce FE          	.byte 0xfe
 4035 15cf E8 00 00 00 	.4byte 0xe8
 4036 15d3 02          	.byte 0x2
 4037 15d4 7E          	.byte 0x7e
 4038 15d5 0C          	.sleb128 12
 4039 15d6 00          	.byte 0x0
 4040 15d7 18          	.uleb128 0x18
 4041 15d8 01          	.byte 0x1
 4042 15d9 67 65 74 5F 	.asciz "get_pin_cycling_values"
 4042      70 69 6E 5F 
 4042      63 79 63 6C 
 4042      69 6E 67 5F 
 4042      76 61 6C 75 
 4042      65 73 00 
 4043 15f0 01          	.byte 0x1
 4044 15f1 2F 01       	.2byte 0x12f
 4045 15f3 01          	.byte 0x1
 4046 15f4 E8 00 00 00 	.4byte 0xe8
 4047 15f8 00 00 00 00 	.4byte .LFB12
 4048 15fc 00 00 00 00 	.4byte .LFE12
 4049 1600 01          	.byte 0x1
 4050 1601 5E          	.byte 0x5e
 4051 1602 82 16 00 00 	.4byte 0x1682
 4052 1606 19          	.uleb128 0x19
 4053 1607 76 61 6C 75 	.asciz "value"
 4053      65 00 
 4054 160d 01          	.byte 0x1
 4055 160e 2F 01       	.2byte 0x12f
 4056 1610 82 16 00 00 	.4byte 0x1682
 4057 1614 02          	.byte 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 85


 4058 1615 7E          	.byte 0x7e
 4059 1616 26          	.sleb128 38
 4060 1617 19          	.uleb128 0x19
 4061 1618 70 6F 73 69 	.asciz "position"
 4061      74 69 6F 6E 
 4061      00 
 4062 1621 01          	.byte 0x1
 4063 1622 2F 01       	.2byte 0x12f
 4064 1624 E8 00 00 00 	.4byte 0xe8
 4065 1628 02          	.byte 0x2
 4066 1629 7E          	.byte 0x7e
 4067 162a 28          	.sleb128 40
 4068 162b 19          	.uleb128 0x19
 4069 162c 6C 65 6E 67 	.asciz "length"
 4069      74 68 00 
 4070 1633 01          	.byte 0x1
 4071 1634 30 01       	.2byte 0x130
 4072 1636 E8 00 00 00 	.4byte 0xe8
 4073 163a 02          	.byte 0x2
 4074 163b 7E          	.byte 0x7e
 4075 163c 2A          	.sleb128 42
 4076 163d 1A          	.uleb128 0x1a
 4077 163e 61 64 63 5F 	.asciz "adc_aquisitions"
 4077      61 71 75 69 
 4077      73 69 74 69 
 4077      6F 6E 73 00 
 4078 164e 01          	.byte 0x1
 4079 164f 32 01       	.2byte 0x132
 4080 1651 88 16 00 00 	.4byte 0x1688
 4081 1655 02          	.byte 0x2
 4082 1656 7E          	.byte 0x7e
 4083 1657 06          	.sleb128 6
 4084 1658 1A          	.uleb128 0x1a
 4085 1659 69 00       	.asciz "i"
 4086 165b 01          	.byte 0x1
 4087 165c 33 01       	.2byte 0x133
 4088 165e B3 00 00 00 	.4byte 0xb3
 4089 1662 02          	.byte 0x2
 4090 1663 7E          	.byte 0x7e
 4091 1664 00          	.sleb128 0
 4092 1665 1A          	.uleb128 0x1a
 4093 1666 6A 00       	.asciz "j"
 4094 1668 01          	.byte 0x1
 4095 1669 34 01       	.2byte 0x134
 4096 166b B3 00 00 00 	.4byte 0xb3
 4097 166f 02          	.byte 0x2
 4098 1670 7E          	.byte 0x7e
 4099 1671 02          	.sleb128 2
 4100 1672 1A          	.uleb128 0x1a
 4101 1673 70 74 72 00 	.asciz "ptr"
 4102 1677 01          	.byte 0x1
 4103 1678 35 01       	.2byte 0x135
 4104 167a 82 16 00 00 	.4byte 0x1682
 4105 167e 02          	.byte 0x2
 4106 167f 7E          	.byte 0x7e
 4107 1680 04          	.sleb128 4
 4108 1681 00          	.byte 0x0
MPLAB XC16 ASSEMBLY Listing:   			page 86


 4109 1682 16          	.uleb128 0x16
 4110 1683 02          	.byte 0x2
 4111 1684 E8 00 00 00 	.4byte 0xe8
 4112 1688 1B          	.uleb128 0x1b
 4113 1689 E8 00 00 00 	.4byte 0xe8
 4114 168d 98 16 00 00 	.4byte 0x1698
 4115 1691 1C          	.uleb128 0x1c
 4116 1692 F8 00 00 00 	.4byte 0xf8
 4117 1696 0F          	.byte 0xf
 4118 1697 00          	.byte 0x0
 4119 1698 1D          	.uleb128 0x1d
 4120 1699 54 4D 52 35 	.asciz "TMR5"
 4120      00 
 4121 169e 03          	.byte 0x3
 4122 169f 6C 01       	.2byte 0x16c
 4123 16a1 A7 16 00 00 	.4byte 0x16a7
 4124 16a5 01          	.byte 0x1
 4125 16a6 01          	.byte 0x1
 4126 16a7 1E          	.uleb128 0x1e
 4127 16a8 E8 00 00 00 	.4byte 0xe8
 4128 16ac 1D          	.uleb128 0x1d
 4129 16ad 50 52 35 00 	.asciz "PR5"
 4130 16b1 03          	.byte 0x3
 4131 16b2 70 01       	.2byte 0x170
 4132 16b4 A7 16 00 00 	.4byte 0x16a7
 4133 16b8 01          	.byte 0x1
 4134 16b9 01          	.byte 0x1
 4135 16ba 1F          	.uleb128 0x1f
 4136 16bb 00 00 00 00 	.4byte .LASF1
 4137 16bf 03          	.byte 0x3
 4138 16c0 A0 01       	.2byte 0x1a0
 4139 16c2 C8 16 00 00 	.4byte 0x16c8
 4140 16c6 01          	.byte 0x1
 4141 16c7 01          	.byte 0x1
 4142 16c8 1E          	.uleb128 0x1e
 4143 16c9 18 02 00 00 	.4byte 0x218
 4144 16cd 1F          	.uleb128 0x1f
 4145 16ce 00 00 00 00 	.4byte .LASF2
 4146 16d2 03          	.byte 0x3
 4147 16d3 80 09       	.2byte 0x980
 4148 16d5 A7 16 00 00 	.4byte 0x16a7
 4149 16d9 01          	.byte 0x1
 4150 16da 01          	.byte 0x1
 4151 16db 1D          	.uleb128 0x1d
 4152 16dc 41 44 31 43 	.asciz "AD1CON1"
 4152      4F 4E 31 00 
 4153 16e4 03          	.byte 0x3
 4154 16e5 A0 09       	.2byte 0x9a0
 4155 16e7 A7 16 00 00 	.4byte 0x16a7
 4156 16eb 01          	.byte 0x1
 4157 16ec 01          	.byte 0x1
 4158 16ed 1F          	.uleb128 0x1f
 4159 16ee 00 00 00 00 	.4byte .LASF3
 4160 16f2 03          	.byte 0x3
 4161 16f3 BC 09       	.2byte 0x9bc
 4162 16f5 FB 16 00 00 	.4byte 0x16fb
 4163 16f9 01          	.byte 0x1
MPLAB XC16 ASSEMBLY Listing:   			page 87


 4164 16fa 01          	.byte 0x1
 4165 16fb 1E          	.uleb128 0x1e
 4166 16fc B1 03 00 00 	.4byte 0x3b1
 4167 1700 1D          	.uleb128 0x1d
 4168 1701 41 44 31 43 	.asciz "AD1CON2"
 4168      4F 4E 32 00 
 4169 1709 03          	.byte 0x3
 4170 170a BF 09       	.2byte 0x9bf
 4171 170c A7 16 00 00 	.4byte 0x16a7
 4172 1710 01          	.byte 0x1
 4173 1711 01          	.byte 0x1
 4174 1712 1F          	.uleb128 0x1f
 4175 1713 00 00 00 00 	.4byte .LASF4
 4176 1717 03          	.byte 0x3
 4177 1718 DD 09       	.2byte 0x9dd
 4178 171a 20 17 00 00 	.4byte 0x1720
 4179 171e 01          	.byte 0x1
 4180 171f 01          	.byte 0x1
 4181 1720 1E          	.uleb128 0x1e
 4182 1721 5C 05 00 00 	.4byte 0x55c
 4183 1725 1D          	.uleb128 0x1d
 4184 1726 41 44 31 43 	.asciz "AD1CON3"
 4184      4F 4E 33 00 
 4185 172e 03          	.byte 0x3
 4186 172f E0 09       	.2byte 0x9e0
 4187 1731 A7 16 00 00 	.4byte 0x16a7
 4188 1735 01          	.byte 0x1
 4189 1736 01          	.byte 0x1
 4190 1737 1F          	.uleb128 0x1f
 4191 1738 00 00 00 00 	.4byte .LASF5
 4192 173c 03          	.byte 0x3
 4193 173d FA 09       	.2byte 0x9fa
 4194 173f 45 17 00 00 	.4byte 0x1745
 4195 1743 01          	.byte 0x1
 4196 1744 01          	.byte 0x1
 4197 1745 1E          	.uleb128 0x1e
 4198 1746 F6 06 00 00 	.4byte 0x6f6
 4199 174a 1D          	.uleb128 0x1d
 4200 174b 41 44 31 43 	.asciz "AD1CHS0"
 4200      48 53 30 00 
 4201 1753 03          	.byte 0x3
 4202 1754 14 0A       	.2byte 0xa14
 4203 1756 A7 16 00 00 	.4byte 0x16a7
 4204 175a 01          	.byte 0x1
 4205 175b 01          	.byte 0x1
 4206 175c 1F          	.uleb128 0x1f
 4207 175d 00 00 00 00 	.4byte .LASF6
 4208 1761 03          	.byte 0x3
 4209 1762 2E 0A       	.2byte 0xa2e
 4210 1764 6A 17 00 00 	.4byte 0x176a
 4211 1768 01          	.byte 0x1
 4212 1769 01          	.byte 0x1
 4213 176a 1E          	.uleb128 0x1e
 4214 176b 75 08 00 00 	.4byte 0x875
 4215 176f 1D          	.uleb128 0x1d
 4216 1770 41 44 31 43 	.asciz "AD1CSSH"
 4216      53 53 48 00 
MPLAB XC16 ASSEMBLY Listing:   			page 88


 4217 1778 03          	.byte 0x3
 4218 1779 31 0A       	.2byte 0xa31
 4219 177b A7 16 00 00 	.4byte 0x16a7
 4220 177f 01          	.byte 0x1
 4221 1780 01          	.byte 0x1
 4222 1781 1D          	.uleb128 0x1d
 4223 1782 41 44 31 43 	.asciz "AD1CSSL"
 4223      53 53 4C 00 
 4224 178a 03          	.byte 0x3
 4225 178b 47 0A       	.2byte 0xa47
 4226 178d A7 16 00 00 	.4byte 0x16a7
 4227 1791 01          	.byte 0x1
 4228 1792 01          	.byte 0x1
 4229 1793 1D          	.uleb128 0x1d
 4230 1794 41 44 31 43 	.asciz "AD1CON4"
 4230      4F 4E 34 00 
 4231 179c 03          	.byte 0x3
 4232 179d 5D 0A       	.2byte 0xa5d
 4233 179f A7 16 00 00 	.4byte 0x16a7
 4234 17a3 01          	.byte 0x1
 4235 17a4 01          	.byte 0x1
 4236 17a5 1F          	.uleb128 0x1f
 4237 17a6 00 00 00 00 	.4byte .LASF7
 4238 17aa 03          	.byte 0x3
 4239 17ab 6C 0A       	.2byte 0xa6c
 4240 17ad B3 17 00 00 	.4byte 0x17b3
 4241 17b1 01          	.byte 0x1
 4242 17b2 01          	.byte 0x1
 4243 17b3 1E          	.uleb128 0x1e
 4244 17b4 3B 09 00 00 	.4byte 0x93b
 4245 17b8 1F          	.uleb128 0x1f
 4246 17b9 00 00 00 00 	.4byte .LASF8
 4247 17bd 03          	.byte 0x3
 4248 17be 1A 25       	.2byte 0x251a
 4249 17c0 C6 17 00 00 	.4byte 0x17c6
 4250 17c4 01          	.byte 0x1
 4251 17c5 01          	.byte 0x1
 4252 17c6 1E          	.uleb128 0x1e
 4253 17c7 AA 0A 00 00 	.4byte 0xaaa
 4254 17cb 1F          	.uleb128 0x1f
 4255 17cc 00 00 00 00 	.4byte .LASF9
 4256 17d0 03          	.byte 0x3
 4257 17d1 30 25       	.2byte 0x2530
 4258 17d3 D9 17 00 00 	.4byte 0x17d9
 4259 17d7 01          	.byte 0x1
 4260 17d8 01          	.byte 0x1
 4261 17d9 1E          	.uleb128 0x1e
 4262 17da 16 0C 00 00 	.4byte 0xc16
 4263 17de 1F          	.uleb128 0x1f
 4264 17df 00 00 00 00 	.4byte .LASF10
 4265 17e3 03          	.byte 0x3
 4266 17e4 C6 25       	.2byte 0x25c6
 4267 17e6 EC 17 00 00 	.4byte 0x17ec
 4268 17ea 01          	.byte 0x1
 4269 17eb 01          	.byte 0x1
 4270 17ec 1E          	.uleb128 0x1e
 4271 17ed 82 0D 00 00 	.4byte 0xd82
MPLAB XC16 ASSEMBLY Listing:   			page 89


 4272 17f1 1F          	.uleb128 0x1f
 4273 17f2 00 00 00 00 	.4byte .LASF11
 4274 17f6 03          	.byte 0x3
 4275 17f7 DC 25       	.2byte 0x25dc
 4276 17f9 FF 17 00 00 	.4byte 0x17ff
 4277 17fd 01          	.byte 0x1
 4278 17fe 01          	.byte 0x1
 4279 17ff 1E          	.uleb128 0x1e
 4280 1800 EE 0E 00 00 	.4byte 0xeee
 4281 1804 1F          	.uleb128 0x1f
 4282 1805 00 00 00 00 	.4byte .LASF12
 4283 1809 03          	.byte 0x3
 4284 180a E4 26       	.2byte 0x26e4
 4285 180c 12 18 00 00 	.4byte 0x1812
 4286 1810 01          	.byte 0x1
 4287 1811 01          	.byte 0x1
 4288 1812 1E          	.uleb128 0x1e
 4289 1813 99 10 00 00 	.4byte 0x1099
 4290 1817 1D          	.uleb128 0x1d
 4291 1818 41 4E 53 45 	.asciz "ANSELB"
 4291      4C 42 00 
 4292 181f 03          	.byte 0x3
 4293 1820 BC 3A       	.2byte 0x3abc
 4294 1822 A7 16 00 00 	.4byte 0x16a7
 4295 1826 01          	.byte 0x1
 4296 1827 01          	.byte 0x1
 4297 1828 17          	.uleb128 0x17
 4298 1829 73 69 7A 65 	.asciz "size_pin_cycling_vector"
 4298      5F 70 69 6E 
 4298      5F 63 79 63 
 4298      6C 69 6E 67 
 4298      5F 76 65 63 
 4298      74 6F 72 00 
 4299 1841 01          	.byte 0x1
 4300 1842 08          	.byte 0x8
 4301 1843 E8 00 00 00 	.4byte 0xe8
 4302 1847 05          	.byte 0x5
 4303 1848 03          	.byte 0x3
 4304 1849 00 00 00 00 	.4byte _size_pin_cycling_vector
 4305 184d 1D          	.uleb128 0x1d
 4306 184e 54 4D 52 35 	.asciz "TMR5"
 4306      00 
 4307 1853 03          	.byte 0x3
 4308 1854 6C 01       	.2byte 0x16c
 4309 1856 A7 16 00 00 	.4byte 0x16a7
 4310 185a 01          	.byte 0x1
 4311 185b 01          	.byte 0x1
 4312 185c 1D          	.uleb128 0x1d
 4313 185d 50 52 35 00 	.asciz "PR5"
 4314 1861 03          	.byte 0x3
 4315 1862 70 01       	.2byte 0x170
 4316 1864 A7 16 00 00 	.4byte 0x16a7
 4317 1868 01          	.byte 0x1
 4318 1869 01          	.byte 0x1
 4319 186a 1F          	.uleb128 0x1f
 4320 186b 00 00 00 00 	.4byte .LASF1
 4321 186f 03          	.byte 0x3
MPLAB XC16 ASSEMBLY Listing:   			page 90


 4322 1870 A0 01       	.2byte 0x1a0
 4323 1872 C8 16 00 00 	.4byte 0x16c8
 4324 1876 01          	.byte 0x1
 4325 1877 01          	.byte 0x1
 4326 1878 1F          	.uleb128 0x1f
 4327 1879 00 00 00 00 	.4byte .LASF2
 4328 187d 03          	.byte 0x3
 4329 187e 80 09       	.2byte 0x980
 4330 1880 A7 16 00 00 	.4byte 0x16a7
 4331 1884 01          	.byte 0x1
 4332 1885 01          	.byte 0x1
 4333 1886 1D          	.uleb128 0x1d
 4334 1887 41 44 31 43 	.asciz "AD1CON1"
 4334      4F 4E 31 00 
 4335 188f 03          	.byte 0x3
 4336 1890 A0 09       	.2byte 0x9a0
 4337 1892 A7 16 00 00 	.4byte 0x16a7
 4338 1896 01          	.byte 0x1
 4339 1897 01          	.byte 0x1
 4340 1898 1F          	.uleb128 0x1f
 4341 1899 00 00 00 00 	.4byte .LASF3
 4342 189d 03          	.byte 0x3
 4343 189e BC 09       	.2byte 0x9bc
 4344 18a0 FB 16 00 00 	.4byte 0x16fb
 4345 18a4 01          	.byte 0x1
 4346 18a5 01          	.byte 0x1
 4347 18a6 1D          	.uleb128 0x1d
 4348 18a7 41 44 31 43 	.asciz "AD1CON2"
 4348      4F 4E 32 00 
 4349 18af 03          	.byte 0x3
 4350 18b0 BF 09       	.2byte 0x9bf
 4351 18b2 A7 16 00 00 	.4byte 0x16a7
 4352 18b6 01          	.byte 0x1
 4353 18b7 01          	.byte 0x1
 4354 18b8 1F          	.uleb128 0x1f
 4355 18b9 00 00 00 00 	.4byte .LASF4
 4356 18bd 03          	.byte 0x3
 4357 18be DD 09       	.2byte 0x9dd
 4358 18c0 20 17 00 00 	.4byte 0x1720
 4359 18c4 01          	.byte 0x1
 4360 18c5 01          	.byte 0x1
 4361 18c6 1D          	.uleb128 0x1d
 4362 18c7 41 44 31 43 	.asciz "AD1CON3"
 4362      4F 4E 33 00 
 4363 18cf 03          	.byte 0x3
 4364 18d0 E0 09       	.2byte 0x9e0
 4365 18d2 A7 16 00 00 	.4byte 0x16a7
 4366 18d6 01          	.byte 0x1
 4367 18d7 01          	.byte 0x1
 4368 18d8 1F          	.uleb128 0x1f
 4369 18d9 00 00 00 00 	.4byte .LASF5
 4370 18dd 03          	.byte 0x3
 4371 18de FA 09       	.2byte 0x9fa
 4372 18e0 45 17 00 00 	.4byte 0x1745
 4373 18e4 01          	.byte 0x1
 4374 18e5 01          	.byte 0x1
 4375 18e6 1D          	.uleb128 0x1d
MPLAB XC16 ASSEMBLY Listing:   			page 91


 4376 18e7 41 44 31 43 	.asciz "AD1CHS0"
 4376      48 53 30 00 
 4377 18ef 03          	.byte 0x3
 4378 18f0 14 0A       	.2byte 0xa14
 4379 18f2 A7 16 00 00 	.4byte 0x16a7
 4380 18f6 01          	.byte 0x1
 4381 18f7 01          	.byte 0x1
 4382 18f8 1F          	.uleb128 0x1f
 4383 18f9 00 00 00 00 	.4byte .LASF6
 4384 18fd 03          	.byte 0x3
 4385 18fe 2E 0A       	.2byte 0xa2e
 4386 1900 6A 17 00 00 	.4byte 0x176a
 4387 1904 01          	.byte 0x1
 4388 1905 01          	.byte 0x1
 4389 1906 1D          	.uleb128 0x1d
 4390 1907 41 44 31 43 	.asciz "AD1CSSH"
 4390      53 53 48 00 
 4391 190f 03          	.byte 0x3
 4392 1910 31 0A       	.2byte 0xa31
 4393 1912 A7 16 00 00 	.4byte 0x16a7
 4394 1916 01          	.byte 0x1
 4395 1917 01          	.byte 0x1
 4396 1918 1D          	.uleb128 0x1d
 4397 1919 41 44 31 43 	.asciz "AD1CSSL"
 4397      53 53 4C 00 
 4398 1921 03          	.byte 0x3
 4399 1922 47 0A       	.2byte 0xa47
 4400 1924 A7 16 00 00 	.4byte 0x16a7
 4401 1928 01          	.byte 0x1
 4402 1929 01          	.byte 0x1
 4403 192a 1D          	.uleb128 0x1d
 4404 192b 41 44 31 43 	.asciz "AD1CON4"
 4404      4F 4E 34 00 
 4405 1933 03          	.byte 0x3
 4406 1934 5D 0A       	.2byte 0xa5d
 4407 1936 A7 16 00 00 	.4byte 0x16a7
 4408 193a 01          	.byte 0x1
 4409 193b 01          	.byte 0x1
 4410 193c 1F          	.uleb128 0x1f
 4411 193d 00 00 00 00 	.4byte .LASF7
 4412 1941 03          	.byte 0x3
 4413 1942 6C 0A       	.2byte 0xa6c
 4414 1944 B3 17 00 00 	.4byte 0x17b3
 4415 1948 01          	.byte 0x1
 4416 1949 01          	.byte 0x1
 4417 194a 1F          	.uleb128 0x1f
 4418 194b 00 00 00 00 	.4byte .LASF8
 4419 194f 03          	.byte 0x3
 4420 1950 1A 25       	.2byte 0x251a
 4421 1952 C6 17 00 00 	.4byte 0x17c6
 4422 1956 01          	.byte 0x1
 4423 1957 01          	.byte 0x1
 4424 1958 1F          	.uleb128 0x1f
 4425 1959 00 00 00 00 	.4byte .LASF9
 4426 195d 03          	.byte 0x3
 4427 195e 30 25       	.2byte 0x2530
 4428 1960 D9 17 00 00 	.4byte 0x17d9
MPLAB XC16 ASSEMBLY Listing:   			page 92


 4429 1964 01          	.byte 0x1
 4430 1965 01          	.byte 0x1
 4431 1966 1F          	.uleb128 0x1f
 4432 1967 00 00 00 00 	.4byte .LASF10
 4433 196b 03          	.byte 0x3
 4434 196c C6 25       	.2byte 0x25c6
 4435 196e EC 17 00 00 	.4byte 0x17ec
 4436 1972 01          	.byte 0x1
 4437 1973 01          	.byte 0x1
 4438 1974 1F          	.uleb128 0x1f
 4439 1975 00 00 00 00 	.4byte .LASF11
 4440 1979 03          	.byte 0x3
 4441 197a DC 25       	.2byte 0x25dc
 4442 197c FF 17 00 00 	.4byte 0x17ff
 4443 1980 01          	.byte 0x1
 4444 1981 01          	.byte 0x1
 4445 1982 1F          	.uleb128 0x1f
 4446 1983 00 00 00 00 	.4byte .LASF12
 4447 1987 03          	.byte 0x3
 4448 1988 E4 26       	.2byte 0x26e4
 4449 198a 12 18 00 00 	.4byte 0x1812
 4450 198e 01          	.byte 0x1
 4451 198f 01          	.byte 0x1
 4452 1990 1D          	.uleb128 0x1d
 4453 1991 41 4E 53 45 	.asciz "ANSELB"
 4453      4C 42 00 
 4454 1998 03          	.byte 0x3
 4455 1999 BC 3A       	.2byte 0x3abc
 4456 199b A7 16 00 00 	.4byte 0x16a7
 4457 199f 01          	.byte 0x1
 4458 19a0 01          	.byte 0x1
 4459 19a1 00          	.byte 0x0
 4460                 	.section .debug_abbrev,info
 4461 0000 01          	.uleb128 0x1
 4462 0001 11          	.uleb128 0x11
 4463 0002 01          	.byte 0x1
 4464 0003 25          	.uleb128 0x25
 4465 0004 08          	.uleb128 0x8
 4466 0005 13          	.uleb128 0x13
 4467 0006 0B          	.uleb128 0xb
 4468 0007 03          	.uleb128 0x3
 4469 0008 08          	.uleb128 0x8
 4470 0009 1B          	.uleb128 0x1b
 4471 000a 08          	.uleb128 0x8
 4472 000b 11          	.uleb128 0x11
 4473 000c 01          	.uleb128 0x1
 4474 000d 12          	.uleb128 0x12
 4475 000e 01          	.uleb128 0x1
 4476 000f 10          	.uleb128 0x10
 4477 0010 06          	.uleb128 0x6
 4478 0011 00          	.byte 0x0
 4479 0012 00          	.byte 0x0
 4480 0013 02          	.uleb128 0x2
 4481 0014 24          	.uleb128 0x24
 4482 0015 00          	.byte 0x0
 4483 0016 0B          	.uleb128 0xb
 4484 0017 0B          	.uleb128 0xb
MPLAB XC16 ASSEMBLY Listing:   			page 93


 4485 0018 3E          	.uleb128 0x3e
 4486 0019 0B          	.uleb128 0xb
 4487 001a 03          	.uleb128 0x3
 4488 001b 08          	.uleb128 0x8
 4489 001c 00          	.byte 0x0
 4490 001d 00          	.byte 0x0
 4491 001e 03          	.uleb128 0x3
 4492 001f 16          	.uleb128 0x16
 4493 0020 00          	.byte 0x0
 4494 0021 03          	.uleb128 0x3
 4495 0022 08          	.uleb128 0x8
 4496 0023 3A          	.uleb128 0x3a
 4497 0024 0B          	.uleb128 0xb
 4498 0025 3B          	.uleb128 0x3b
 4499 0026 0B          	.uleb128 0xb
 4500 0027 49          	.uleb128 0x49
 4501 0028 13          	.uleb128 0x13
 4502 0029 00          	.byte 0x0
 4503 002a 00          	.byte 0x0
 4504 002b 04          	.uleb128 0x4
 4505 002c 13          	.uleb128 0x13
 4506 002d 01          	.byte 0x1
 4507 002e 0B          	.uleb128 0xb
 4508 002f 0B          	.uleb128 0xb
 4509 0030 3A          	.uleb128 0x3a
 4510 0031 0B          	.uleb128 0xb
 4511 0032 3B          	.uleb128 0x3b
 4512 0033 05          	.uleb128 0x5
 4513 0034 01          	.uleb128 0x1
 4514 0035 13          	.uleb128 0x13
 4515 0036 00          	.byte 0x0
 4516 0037 00          	.byte 0x0
 4517 0038 05          	.uleb128 0x5
 4518 0039 0D          	.uleb128 0xd
 4519 003a 00          	.byte 0x0
 4520 003b 03          	.uleb128 0x3
 4521 003c 08          	.uleb128 0x8
 4522 003d 3A          	.uleb128 0x3a
 4523 003e 0B          	.uleb128 0xb
 4524 003f 3B          	.uleb128 0x3b
 4525 0040 05          	.uleb128 0x5
 4526 0041 49          	.uleb128 0x49
 4527 0042 13          	.uleb128 0x13
 4528 0043 0B          	.uleb128 0xb
 4529 0044 0B          	.uleb128 0xb
 4530 0045 0D          	.uleb128 0xd
 4531 0046 0B          	.uleb128 0xb
 4532 0047 0C          	.uleb128 0xc
 4533 0048 0B          	.uleb128 0xb
 4534 0049 38          	.uleb128 0x38
 4535 004a 0A          	.uleb128 0xa
 4536 004b 00          	.byte 0x0
 4537 004c 00          	.byte 0x0
 4538 004d 06          	.uleb128 0x6
 4539 004e 17          	.uleb128 0x17
 4540 004f 01          	.byte 0x1
 4541 0050 0B          	.uleb128 0xb
MPLAB XC16 ASSEMBLY Listing:   			page 94


 4542 0051 0B          	.uleb128 0xb
 4543 0052 3A          	.uleb128 0x3a
 4544 0053 0B          	.uleb128 0xb
 4545 0054 3B          	.uleb128 0x3b
 4546 0055 05          	.uleb128 0x5
 4547 0056 01          	.uleb128 0x1
 4548 0057 13          	.uleb128 0x13
 4549 0058 00          	.byte 0x0
 4550 0059 00          	.byte 0x0
 4551 005a 07          	.uleb128 0x7
 4552 005b 0D          	.uleb128 0xd
 4553 005c 00          	.byte 0x0
 4554 005d 49          	.uleb128 0x49
 4555 005e 13          	.uleb128 0x13
 4556 005f 00          	.byte 0x0
 4557 0060 00          	.byte 0x0
 4558 0061 08          	.uleb128 0x8
 4559 0062 13          	.uleb128 0x13
 4560 0063 01          	.byte 0x1
 4561 0064 03          	.uleb128 0x3
 4562 0065 08          	.uleb128 0x8
 4563 0066 0B          	.uleb128 0xb
 4564 0067 0B          	.uleb128 0xb
 4565 0068 3A          	.uleb128 0x3a
 4566 0069 0B          	.uleb128 0xb
 4567 006a 3B          	.uleb128 0x3b
 4568 006b 05          	.uleb128 0x5
 4569 006c 01          	.uleb128 0x1
 4570 006d 13          	.uleb128 0x13
 4571 006e 00          	.byte 0x0
 4572 006f 00          	.byte 0x0
 4573 0070 09          	.uleb128 0x9
 4574 0071 0D          	.uleb128 0xd
 4575 0072 00          	.byte 0x0
 4576 0073 49          	.uleb128 0x49
 4577 0074 13          	.uleb128 0x13
 4578 0075 38          	.uleb128 0x38
 4579 0076 0A          	.uleb128 0xa
 4580 0077 00          	.byte 0x0
 4581 0078 00          	.byte 0x0
 4582 0079 0A          	.uleb128 0xa
 4583 007a 16          	.uleb128 0x16
 4584 007b 00          	.byte 0x0
 4585 007c 03          	.uleb128 0x3
 4586 007d 08          	.uleb128 0x8
 4587 007e 3A          	.uleb128 0x3a
 4588 007f 0B          	.uleb128 0xb
 4589 0080 3B          	.uleb128 0x3b
 4590 0081 05          	.uleb128 0x5
 4591 0082 49          	.uleb128 0x49
 4592 0083 13          	.uleb128 0x13
 4593 0084 00          	.byte 0x0
 4594 0085 00          	.byte 0x0
 4595 0086 0B          	.uleb128 0xb
 4596 0087 04          	.uleb128 0x4
 4597 0088 01          	.byte 0x1
 4598 0089 0B          	.uleb128 0xb
MPLAB XC16 ASSEMBLY Listing:   			page 95


 4599 008a 0B          	.uleb128 0xb
 4600 008b 3A          	.uleb128 0x3a
 4601 008c 0B          	.uleb128 0xb
 4602 008d 3B          	.uleb128 0x3b
 4603 008e 0B          	.uleb128 0xb
 4604 008f 01          	.uleb128 0x1
 4605 0090 13          	.uleb128 0x13
 4606 0091 00          	.byte 0x0
 4607 0092 00          	.byte 0x0
 4608 0093 0C          	.uleb128 0xc
 4609 0094 28          	.uleb128 0x28
 4610 0095 00          	.byte 0x0
 4611 0096 03          	.uleb128 0x3
 4612 0097 08          	.uleb128 0x8
 4613 0098 1C          	.uleb128 0x1c
 4614 0099 0D          	.uleb128 0xd
 4615 009a 00          	.byte 0x0
 4616 009b 00          	.byte 0x0
 4617 009c 0D          	.uleb128 0xd
 4618 009d 13          	.uleb128 0x13
 4619 009e 01          	.byte 0x1
 4620 009f 0B          	.uleb128 0xb
 4621 00a0 0B          	.uleb128 0xb
 4622 00a1 3A          	.uleb128 0x3a
 4623 00a2 0B          	.uleb128 0xb
 4624 00a3 3B          	.uleb128 0x3b
 4625 00a4 0B          	.uleb128 0xb
 4626 00a5 01          	.uleb128 0x1
 4627 00a6 13          	.uleb128 0x13
 4628 00a7 00          	.byte 0x0
 4629 00a8 00          	.byte 0x0
 4630 00a9 0E          	.uleb128 0xe
 4631 00aa 0D          	.uleb128 0xd
 4632 00ab 00          	.byte 0x0
 4633 00ac 03          	.uleb128 0x3
 4634 00ad 08          	.uleb128 0x8
 4635 00ae 3A          	.uleb128 0x3a
 4636 00af 0B          	.uleb128 0xb
 4637 00b0 3B          	.uleb128 0x3b
 4638 00b1 0B          	.uleb128 0xb
 4639 00b2 49          	.uleb128 0x49
 4640 00b3 13          	.uleb128 0x13
 4641 00b4 0B          	.uleb128 0xb
 4642 00b5 0B          	.uleb128 0xb
 4643 00b6 0D          	.uleb128 0xd
 4644 00b7 0B          	.uleb128 0xb
 4645 00b8 0C          	.uleb128 0xc
 4646 00b9 0B          	.uleb128 0xb
 4647 00ba 38          	.uleb128 0x38
 4648 00bb 0A          	.uleb128 0xa
 4649 00bc 00          	.byte 0x0
 4650 00bd 00          	.byte 0x0
 4651 00be 0F          	.uleb128 0xf
 4652 00bf 0D          	.uleb128 0xd
 4653 00c0 00          	.byte 0x0
 4654 00c1 03          	.uleb128 0x3
 4655 00c2 08          	.uleb128 0x8
MPLAB XC16 ASSEMBLY Listing:   			page 96


 4656 00c3 3A          	.uleb128 0x3a
 4657 00c4 0B          	.uleb128 0xb
 4658 00c5 3B          	.uleb128 0x3b
 4659 00c6 0B          	.uleb128 0xb
 4660 00c7 49          	.uleb128 0x49
 4661 00c8 13          	.uleb128 0x13
 4662 00c9 38          	.uleb128 0x38
 4663 00ca 0A          	.uleb128 0xa
 4664 00cb 00          	.byte 0x0
 4665 00cc 00          	.byte 0x0
 4666 00cd 10          	.uleb128 0x10
 4667 00ce 2E          	.uleb128 0x2e
 4668 00cf 01          	.byte 0x1
 4669 00d0 3F          	.uleb128 0x3f
 4670 00d1 0C          	.uleb128 0xc
 4671 00d2 03          	.uleb128 0x3
 4672 00d3 08          	.uleb128 0x8
 4673 00d4 3A          	.uleb128 0x3a
 4674 00d5 0B          	.uleb128 0xb
 4675 00d6 3B          	.uleb128 0x3b
 4676 00d7 0B          	.uleb128 0xb
 4677 00d8 27          	.uleb128 0x27
 4678 00d9 0C          	.uleb128 0xc
 4679 00da 11          	.uleb128 0x11
 4680 00db 01          	.uleb128 0x1
 4681 00dc 12          	.uleb128 0x12
 4682 00dd 01          	.uleb128 0x1
 4683 00de 40          	.uleb128 0x40
 4684 00df 0A          	.uleb128 0xa
 4685 00e0 01          	.uleb128 0x1
 4686 00e1 13          	.uleb128 0x13
 4687 00e2 00          	.byte 0x0
 4688 00e3 00          	.byte 0x0
 4689 00e4 11          	.uleb128 0x11
 4690 00e5 05          	.uleb128 0x5
 4691 00e6 00          	.byte 0x0
 4692 00e7 03          	.uleb128 0x3
 4693 00e8 08          	.uleb128 0x8
 4694 00e9 3A          	.uleb128 0x3a
 4695 00ea 0B          	.uleb128 0xb
 4696 00eb 3B          	.uleb128 0x3b
 4697 00ec 0B          	.uleb128 0xb
 4698 00ed 49          	.uleb128 0x49
 4699 00ee 13          	.uleb128 0x13
 4700 00ef 02          	.uleb128 0x2
 4701 00f0 0A          	.uleb128 0xa
 4702 00f1 00          	.byte 0x0
 4703 00f2 00          	.byte 0x0
 4704 00f3 12          	.uleb128 0x12
 4705 00f4 2E          	.uleb128 0x2e
 4706 00f5 00          	.byte 0x0
 4707 00f6 3F          	.uleb128 0x3f
 4708 00f7 0C          	.uleb128 0xc
 4709 00f8 03          	.uleb128 0x3
 4710 00f9 08          	.uleb128 0x8
 4711 00fa 3A          	.uleb128 0x3a
 4712 00fb 0B          	.uleb128 0xb
MPLAB XC16 ASSEMBLY Listing:   			page 97


 4713 00fc 3B          	.uleb128 0x3b
 4714 00fd 0B          	.uleb128 0xb
 4715 00fe 27          	.uleb128 0x27
 4716 00ff 0C          	.uleb128 0xc
 4717 0100 11          	.uleb128 0x11
 4718 0101 01          	.uleb128 0x1
 4719 0102 12          	.uleb128 0x12
 4720 0103 01          	.uleb128 0x1
 4721 0104 40          	.uleb128 0x40
 4722 0105 0A          	.uleb128 0xa
 4723 0106 00          	.byte 0x0
 4724 0107 00          	.byte 0x0
 4725 0108 13          	.uleb128 0x13
 4726 0109 2E          	.uleb128 0x2e
 4727 010a 01          	.byte 0x1
 4728 010b 3F          	.uleb128 0x3f
 4729 010c 0C          	.uleb128 0xc
 4730 010d 03          	.uleb128 0x3
 4731 010e 08          	.uleb128 0x8
 4732 010f 3A          	.uleb128 0x3a
 4733 0110 0B          	.uleb128 0xb
 4734 0111 3B          	.uleb128 0x3b
 4735 0112 0B          	.uleb128 0xb
 4736 0113 27          	.uleb128 0x27
 4737 0114 0C          	.uleb128 0xc
 4738 0115 49          	.uleb128 0x49
 4739 0116 13          	.uleb128 0x13
 4740 0117 11          	.uleb128 0x11
 4741 0118 01          	.uleb128 0x1
 4742 0119 12          	.uleb128 0x12
 4743 011a 01          	.uleb128 0x1
 4744 011b 40          	.uleb128 0x40
 4745 011c 0A          	.uleb128 0xa
 4746 011d 01          	.uleb128 0x1
 4747 011e 13          	.uleb128 0x13
 4748 011f 00          	.byte 0x0
 4749 0120 00          	.byte 0x0
 4750 0121 14          	.uleb128 0x14
 4751 0122 2E          	.uleb128 0x2e
 4752 0123 00          	.byte 0x0
 4753 0124 3F          	.uleb128 0x3f
 4754 0125 0C          	.uleb128 0xc
 4755 0126 03          	.uleb128 0x3
 4756 0127 08          	.uleb128 0x8
 4757 0128 3A          	.uleb128 0x3a
 4758 0129 0B          	.uleb128 0xb
 4759 012a 3B          	.uleb128 0x3b
 4760 012b 0B          	.uleb128 0xb
 4761 012c 11          	.uleb128 0x11
 4762 012d 01          	.uleb128 0x1
 4763 012e 12          	.uleb128 0x12
 4764 012f 01          	.uleb128 0x1
 4765 0130 40          	.uleb128 0x40
 4766 0131 0A          	.uleb128 0xa
 4767 0132 00          	.byte 0x0
 4768 0133 00          	.byte 0x0
 4769 0134 15          	.uleb128 0x15
MPLAB XC16 ASSEMBLY Listing:   			page 98


 4770 0135 05          	.uleb128 0x5
 4771 0136 00          	.byte 0x0
 4772 0137 03          	.uleb128 0x3
 4773 0138 0E          	.uleb128 0xe
 4774 0139 3A          	.uleb128 0x3a
 4775 013a 0B          	.uleb128 0xb
 4776 013b 3B          	.uleb128 0x3b
 4777 013c 0B          	.uleb128 0xb
 4778 013d 49          	.uleb128 0x49
 4779 013e 13          	.uleb128 0x13
 4780 013f 02          	.uleb128 0x2
 4781 0140 0A          	.uleb128 0xa
 4782 0141 00          	.byte 0x0
 4783 0142 00          	.byte 0x0
 4784 0143 16          	.uleb128 0x16
 4785 0144 0F          	.uleb128 0xf
 4786 0145 00          	.byte 0x0
 4787 0146 0B          	.uleb128 0xb
 4788 0147 0B          	.uleb128 0xb
 4789 0148 49          	.uleb128 0x49
 4790 0149 13          	.uleb128 0x13
 4791 014a 00          	.byte 0x0
 4792 014b 00          	.byte 0x0
 4793 014c 17          	.uleb128 0x17
 4794 014d 34          	.uleb128 0x34
 4795 014e 00          	.byte 0x0
 4796 014f 03          	.uleb128 0x3
 4797 0150 08          	.uleb128 0x8
 4798 0151 3A          	.uleb128 0x3a
 4799 0152 0B          	.uleb128 0xb
 4800 0153 3B          	.uleb128 0x3b
 4801 0154 0B          	.uleb128 0xb
 4802 0155 49          	.uleb128 0x49
 4803 0156 13          	.uleb128 0x13
 4804 0157 02          	.uleb128 0x2
 4805 0158 0A          	.uleb128 0xa
 4806 0159 00          	.byte 0x0
 4807 015a 00          	.byte 0x0
 4808 015b 18          	.uleb128 0x18
 4809 015c 2E          	.uleb128 0x2e
 4810 015d 01          	.byte 0x1
 4811 015e 3F          	.uleb128 0x3f
 4812 015f 0C          	.uleb128 0xc
 4813 0160 03          	.uleb128 0x3
 4814 0161 08          	.uleb128 0x8
 4815 0162 3A          	.uleb128 0x3a
 4816 0163 0B          	.uleb128 0xb
 4817 0164 3B          	.uleb128 0x3b
 4818 0165 05          	.uleb128 0x5
 4819 0166 27          	.uleb128 0x27
 4820 0167 0C          	.uleb128 0xc
 4821 0168 49          	.uleb128 0x49
 4822 0169 13          	.uleb128 0x13
 4823 016a 11          	.uleb128 0x11
 4824 016b 01          	.uleb128 0x1
 4825 016c 12          	.uleb128 0x12
 4826 016d 01          	.uleb128 0x1
MPLAB XC16 ASSEMBLY Listing:   			page 99


 4827 016e 40          	.uleb128 0x40
 4828 016f 0A          	.uleb128 0xa
 4829 0170 01          	.uleb128 0x1
 4830 0171 13          	.uleb128 0x13
 4831 0172 00          	.byte 0x0
 4832 0173 00          	.byte 0x0
 4833 0174 19          	.uleb128 0x19
 4834 0175 05          	.uleb128 0x5
 4835 0176 00          	.byte 0x0
 4836 0177 03          	.uleb128 0x3
 4837 0178 08          	.uleb128 0x8
 4838 0179 3A          	.uleb128 0x3a
 4839 017a 0B          	.uleb128 0xb
 4840 017b 3B          	.uleb128 0x3b
 4841 017c 05          	.uleb128 0x5
 4842 017d 49          	.uleb128 0x49
 4843 017e 13          	.uleb128 0x13
 4844 017f 02          	.uleb128 0x2
 4845 0180 0A          	.uleb128 0xa
 4846 0181 00          	.byte 0x0
 4847 0182 00          	.byte 0x0
 4848 0183 1A          	.uleb128 0x1a
 4849 0184 34          	.uleb128 0x34
 4850 0185 00          	.byte 0x0
 4851 0186 03          	.uleb128 0x3
 4852 0187 08          	.uleb128 0x8
 4853 0188 3A          	.uleb128 0x3a
 4854 0189 0B          	.uleb128 0xb
 4855 018a 3B          	.uleb128 0x3b
 4856 018b 05          	.uleb128 0x5
 4857 018c 49          	.uleb128 0x49
 4858 018d 13          	.uleb128 0x13
 4859 018e 02          	.uleb128 0x2
 4860 018f 0A          	.uleb128 0xa
 4861 0190 00          	.byte 0x0
 4862 0191 00          	.byte 0x0
 4863 0192 1B          	.uleb128 0x1b
 4864 0193 01          	.uleb128 0x1
 4865 0194 01          	.byte 0x1
 4866 0195 49          	.uleb128 0x49
 4867 0196 13          	.uleb128 0x13
 4868 0197 01          	.uleb128 0x1
 4869 0198 13          	.uleb128 0x13
 4870 0199 00          	.byte 0x0
 4871 019a 00          	.byte 0x0
 4872 019b 1C          	.uleb128 0x1c
 4873 019c 21          	.uleb128 0x21
 4874 019d 00          	.byte 0x0
 4875 019e 49          	.uleb128 0x49
 4876 019f 13          	.uleb128 0x13
 4877 01a0 2F          	.uleb128 0x2f
 4878 01a1 0B          	.uleb128 0xb
 4879 01a2 00          	.byte 0x0
 4880 01a3 00          	.byte 0x0
 4881 01a4 1D          	.uleb128 0x1d
 4882 01a5 34          	.uleb128 0x34
 4883 01a6 00          	.byte 0x0
MPLAB XC16 ASSEMBLY Listing:   			page 100


 4884 01a7 03          	.uleb128 0x3
 4885 01a8 08          	.uleb128 0x8
 4886 01a9 3A          	.uleb128 0x3a
 4887 01aa 0B          	.uleb128 0xb
 4888 01ab 3B          	.uleb128 0x3b
 4889 01ac 05          	.uleb128 0x5
 4890 01ad 49          	.uleb128 0x49
 4891 01ae 13          	.uleb128 0x13
 4892 01af 3F          	.uleb128 0x3f
 4893 01b0 0C          	.uleb128 0xc
 4894 01b1 3C          	.uleb128 0x3c
 4895 01b2 0C          	.uleb128 0xc
 4896 01b3 00          	.byte 0x0
 4897 01b4 00          	.byte 0x0
 4898 01b5 1E          	.uleb128 0x1e
 4899 01b6 35          	.uleb128 0x35
 4900 01b7 00          	.byte 0x0
 4901 01b8 49          	.uleb128 0x49
 4902 01b9 13          	.uleb128 0x13
 4903 01ba 00          	.byte 0x0
 4904 01bb 00          	.byte 0x0
 4905 01bc 1F          	.uleb128 0x1f
 4906 01bd 34          	.uleb128 0x34
 4907 01be 00          	.byte 0x0
 4908 01bf 03          	.uleb128 0x3
 4909 01c0 0E          	.uleb128 0xe
 4910 01c1 3A          	.uleb128 0x3a
 4911 01c2 0B          	.uleb128 0xb
 4912 01c3 3B          	.uleb128 0x3b
 4913 01c4 05          	.uleb128 0x5
 4914 01c5 49          	.uleb128 0x49
 4915 01c6 13          	.uleb128 0x13
 4916 01c7 3F          	.uleb128 0x3f
 4917 01c8 0C          	.uleb128 0xc
 4918 01c9 3C          	.uleb128 0x3c
 4919 01ca 0C          	.uleb128 0xc
 4920 01cb 00          	.byte 0x0
 4921 01cc 00          	.byte 0x0
 4922 01cd 00          	.byte 0x0
 4923                 	.section .debug_pubnames,info
 4924 0000 0C 01 00 00 	.4byte 0x10c
 4925 0004 02 00       	.2byte 0x2
 4926 0006 00 00 00 00 	.4byte .Ldebug_info0
 4927 000a A2 19 00 00 	.4byte 0x19a2
 4928 000e 51 13 00 00 	.4byte 0x1351
 4929 0012 63 6F 6E 66 	.asciz "config_adc1"
 4929      69 67 5F 61 
 4929      64 63 31 00 
 4930 001e 85 13 00 00 	.4byte 0x1385
 4931 0022 61 64 63 5F 	.asciz "adc_callback"
 4931      63 61 6C 6C 
 4931      62 61 63 6B 
 4931      00 
 4932 002f A1 13 00 00 	.4byte 0x13a1
 4933 0033 5F 41 44 31 	.asciz "_AD1Interrupt"
 4933      49 6E 74 65 
 4933      72 72 75 70 
MPLAB XC16 ASSEMBLY Listing:   			page 101


 4933      74 00 
 4934 0041 BE 13 00 00 	.4byte 0x13be
 4935 0045 67 65 74 5F 	.asciz "get_adc_value"
 4935      61 64 63 5F 
 4935      76 61 6C 75 
 4935      65 00 
 4936 0053 FC 13 00 00 	.4byte 0x13fc
 4937 0057 74 75 72 6E 	.asciz "turn_on_adc"
 4937      5F 6F 6E 5F 
 4937      61 64 63 00 
 4938 0063 16 14 00 00 	.4byte 0x1416
 4939 0067 74 75 72 6E 	.asciz "turn_off_adc"
 4939      5F 6F 66 66 
 4939      5F 61 64 63 
 4939      00 
 4940 0074 31 14 00 00 	.4byte 0x1431
 4941 0078 73 74 61 72 	.asciz "start_samp"
 4941      74 5F 73 61 
 4941      6D 70 00 
 4942 0083 4A 14 00 00 	.4byte 0x144a
 4943 0087 73 74 61 72 	.asciz "start_conversion"
 4943      74 5F 63 6F 
 4943      6E 76 65 72 
 4943      73 69 6F 6E 
 4943      00 
 4944 0098 69 14 00 00 	.4byte 0x1469
 4945 009c 77 61 69 74 	.asciz "wait_for_done"
 4945      5F 66 6F 72 
 4945      5F 64 6F 6E 
 4945      65 00 
 4946 00aa 85 14 00 00 	.4byte 0x1485
 4947 00ae 73 65 74 5F 	.asciz "set_default_parameters"
 4947      64 65 66 61 
 4947      75 6C 74 5F 
 4947      70 61 72 61 
 4947      6D 65 74 65 
 4947      72 73 00 
 4948 00c5 C4 14 00 00 	.4byte 0x14c4
 4949 00c9 73 65 6C 65 	.asciz "select_input_pin"
 4949      63 74 5F 69 
 4949      6E 70 75 74 
 4949      5F 70 69 6E 
 4949      00 
 4950 00da F7 14 00 00 	.4byte 0x14f7
 4951 00de 63 6F 6E 66 	.asciz "config_pin_cycling"
 4951      69 67 5F 70 
 4951      69 6E 5F 63 
 4951      79 63 6C 69 
 4951      6E 67 00 
 4952 00f1 D7 15 00 00 	.4byte 0x15d7
 4953 00f5 67 65 74 5F 	.asciz "get_pin_cycling_values"
 4953      70 69 6E 5F 
 4953      63 79 63 6C 
 4953      69 6E 67 5F 
 4953      76 61 6C 75 
 4953      65 73 00 
 4954 010c 00 00 00 00 	.4byte 0x0
MPLAB XC16 ASSEMBLY Listing:   			page 102


 4955                 	.section .debug_pubtypes,info
 4956 0000 D2 01 00 00 	.4byte 0x1d2
 4957 0004 02 00       	.2byte 0x2
 4958 0006 00 00 00 00 	.4byte .Ldebug_info0
 4959 000a A2 19 00 00 	.4byte 0x19a2
 4960 000e E8 00 00 00 	.4byte 0xe8
 4961 0012 75 69 6E 74 	.asciz "uint16_t"
 4961      31 36 5F 74 
 4961      00 
 4962 001b 08 01 00 00 	.4byte 0x108
 4963 001f 75 69 6E 74 	.asciz "uint32_t"
 4963      33 32 5F 74 
 4963      00 
 4964 0028 F9 01 00 00 	.4byte 0x1f9
 4965 002c 74 61 67 54 	.asciz "tagT5CONBITS"
 4965      35 43 4F 4E 
 4965      42 49 54 53 
 4965      00 
 4966 0039 18 02 00 00 	.4byte 0x218
 4967 003d 54 35 43 4F 	.asciz "T5CONBITS"
 4967      4E 42 49 54 
 4967      53 00 
 4968 0047 90 03 00 00 	.4byte 0x390
 4969 004b 74 61 67 41 	.asciz "tagAD1CON1BITS"
 4969      44 31 43 4F 
 4969      4E 31 42 49 
 4969      54 53 00 
 4970 005a B1 03 00 00 	.4byte 0x3b1
 4971 005e 41 44 31 43 	.asciz "AD1CON1BITS"
 4971      4F 4E 31 42 
 4971      49 54 53 00 
 4972 006a 3B 05 00 00 	.4byte 0x53b
 4973 006e 74 61 67 41 	.asciz "tagAD1CON2BITS"
 4973      44 31 43 4F 
 4973      4E 32 42 49 
 4973      54 53 00 
 4974 007d 5C 05 00 00 	.4byte 0x55c
 4975 0081 41 44 31 43 	.asciz "AD1CON2BITS"
 4975      4F 4E 32 42 
 4975      49 54 53 00 
 4976 008d D5 06 00 00 	.4byte 0x6d5
 4977 0091 74 61 67 41 	.asciz "tagAD1CON3BITS"
 4977      44 31 43 4F 
 4977      4E 33 42 49 
 4977      54 53 00 
 4978 00a0 F6 06 00 00 	.4byte 0x6f6
 4979 00a4 41 44 31 43 	.asciz "AD1CON3BITS"
 4979      4F 4E 33 42 
 4979      49 54 53 00 
 4980 00b0 54 08 00 00 	.4byte 0x854
 4981 00b4 74 61 67 41 	.asciz "tagAD1CHS0BITS"
 4981      44 31 43 48 
 4981      53 30 42 49 
 4981      54 53 00 
 4982 00c3 75 08 00 00 	.4byte 0x875
 4983 00c7 41 44 31 43 	.asciz "AD1CHS0BITS"
 4983      48 53 30 42 
MPLAB XC16 ASSEMBLY Listing:   			page 103


 4983      49 54 53 00 
 4984 00d3 1A 09 00 00 	.4byte 0x91a
 4985 00d7 74 61 67 41 	.asciz "tagAD1CON4BITS"
 4985      44 31 43 4F 
 4985      4E 34 42 49 
 4985      54 53 00 
 4986 00e6 3B 09 00 00 	.4byte 0x93b
 4987 00ea 41 44 31 43 	.asciz "AD1CON4BITS"
 4987      4F 4E 34 42 
 4987      49 54 53 00 
 4988 00f6 4F 09 00 00 	.4byte 0x94f
 4989 00fa 74 61 67 49 	.asciz "tagIFS0BITS"
 4989      46 53 30 42 
 4989      49 54 53 00 
 4990 0106 AA 0A 00 00 	.4byte 0xaaa
 4991 010a 49 46 53 30 	.asciz "IFS0BITS"
 4991      42 49 54 53 
 4991      00 
 4992 0113 BB 0A 00 00 	.4byte 0xabb
 4993 0117 74 61 67 49 	.asciz "tagIFS1BITS"
 4993      46 53 31 42 
 4993      49 54 53 00 
 4994 0123 16 0C 00 00 	.4byte 0xc16
 4995 0127 49 46 53 31 	.asciz "IFS1BITS"
 4995      42 49 54 53 
 4995      00 
 4996 0130 27 0C 00 00 	.4byte 0xc27
 4997 0134 74 61 67 49 	.asciz "tagIEC0BITS"
 4997      45 43 30 42 
 4997      49 54 53 00 
 4998 0140 82 0D 00 00 	.4byte 0xd82
 4999 0144 49 45 43 30 	.asciz "IEC0BITS"
 4999      42 49 54 53 
 4999      00 
 5000 014d 93 0D 00 00 	.4byte 0xd93
 5001 0151 74 61 67 49 	.asciz "tagIEC1BITS"
 5001      45 43 31 42 
 5001      49 54 53 00 
 5002 015d EE 0E 00 00 	.4byte 0xeee
 5003 0161 49 45 43 31 	.asciz "IEC1BITS"
 5003      42 49 54 53 
 5003      00 
 5004 016a 7B 10 00 00 	.4byte 0x107b
 5005 016e 74 61 67 49 	.asciz "tagIPC3BITS"
 5005      50 43 33 42 
 5005      49 54 53 00 
 5006 017a 99 10 00 00 	.4byte 0x1099
 5007 017e 49 50 43 33 	.asciz "IPC3BITS"
 5007      42 49 54 53 
 5007      00 
 5008 0187 D9 10 00 00 	.4byte 0x10d9
 5009 018b 53 61 6D 70 	.asciz "Sampling"
 5009      6C 69 6E 67 
 5009      00 
 5010 0194 2E 11 00 00 	.4byte 0x112e
 5011 0198 53 53 52 43 	.asciz "SSRC"
 5011      00 
MPLAB XC16 ASSEMBLY Listing:   			page 104


 5012 019d 81 11 00 00 	.4byte 0x1181
 5013 01a1 56 6F 6C 74 	.asciz "Voltage"
 5013      61 67 65 00 
 5014 01a9 E9 11 00 00 	.4byte 0x11e9
 5015 01ad 50 69 6E 5F 	.asciz "Pin_Cycling_Error"
 5015      43 79 63 6C 
 5015      69 6E 67 5F 
 5015      45 72 72 6F 
 5015      72 00 
 5016 01bf 3B 13 00 00 	.4byte 0x133b
 5017 01c3 41 44 43 5F 	.asciz "ADC_parameters"
 5017      70 61 72 61 
 5017      6D 65 74 65 
 5017      72 73 00 
 5018 01d2 00 00 00 00 	.4byte 0x0
 5019                 	.section .debug_aranges,info
 5020 0000 14 00 00 00 	.4byte 0x14
 5021 0004 02 00       	.2byte 0x2
 5022 0006 00 00 00 00 	.4byte .Ldebug_info0
 5023 000a 04          	.byte 0x4
 5024 000b 00          	.byte 0x0
 5025 000c 00 00       	.2byte 0x0
 5026 000e 00 00       	.2byte 0x0
 5027 0010 00 00 00 00 	.4byte 0x0
 5028 0014 00 00 00 00 	.4byte 0x0
 5029                 	.section .debug_str,info
 5030                 	.LASF3:
 5031 0000 41 44 31 43 	.asciz "AD1CON1bits"
 5031      4F 4E 31 62 
 5031      69 74 73 00 
 5032                 	.LASF4:
 5033 000c 41 44 31 43 	.asciz "AD1CON2bits"
 5033      4F 4E 32 62 
 5033      69 74 73 00 
 5034                 	.LASF10:
 5035 0018 49 45 43 30 	.asciz "IEC0bits"
 5035      62 69 74 73 
 5035      00 
 5036                 	.LASF1:
 5037 0021 54 35 43 4F 	.asciz "T5CONbits"
 5037      4E 62 69 74 
 5037      73 00 
 5038                 	.LASF2:
 5039 002b 41 44 43 31 	.asciz "ADC1BUF0"
 5039      42 55 46 30 
 5039      00 
 5040                 	.LASF8:
 5041 0034 49 46 53 30 	.asciz "IFS0bits"
 5041      62 69 74 73 
 5041      00 
 5042                 	.LASF5:
 5043 003d 41 44 31 43 	.asciz "AD1CON3bits"
 5043      4F 4E 33 62 
 5043      69 74 73 00 
 5044                 	.LASF0:
 5045 0049 70 61 72 61 	.asciz "parameters"
 5045      6D 65 74 65 
MPLAB XC16 ASSEMBLY Listing:   			page 105


 5045      72 73 00 
 5046                 	.LASF11:
 5047 0054 49 45 43 31 	.asciz "IEC1bits"
 5047      62 69 74 73 
 5047      00 
 5048                 	.LASF6:
 5049 005d 41 44 31 43 	.asciz "AD1CHS0bits"
 5049      48 53 30 62 
 5049      69 74 73 00 
 5050                 	.LASF12:
 5051 0069 49 50 43 33 	.asciz "IPC3bits"
 5051      62 69 74 73 
 5051      00 
 5052                 	.LASF9:
 5053 0072 49 46 53 31 	.asciz "IFS1bits"
 5053      62 69 74 73 
 5053      00 
 5054                 	.LASF7:
 5055 007b 41 44 31 43 	.asciz "AD1CON4bits"
 5055      4F 4E 34 62 
 5055      69 74 73 00 
 5056                 	.section .text,code
 5057              	
 5058              	
 5059              	
 5060              	.section __c30_info,info,bss
 5061                 	__psv_trap_errata:
 5062                 	
 5063                 	.section __c30_signature,info,data
 5064 0000 01 00       	.word 0x0001
 5065 0002 00 00       	.word 0x0000
 5066 0004 00 00       	.word 0x0000
 5067                 	
 5068                 	
 5069                 	
 5070                 	.set ___PA___,0
 5071                 	.end
MPLAB XC16 ASSEMBLY Listing:   			page 106


DEFINED SYMBOLS
                            *ABS*:00000000 /home/user/Documents/FST/Programming/project_sub-zero/lib/lib_pic33e/ADC.c
                            *ABS*:00000001 __33EP256MU806
                            *ABS*:00000001 __dsPIC33E
                            *ABS*:00000001 __HAS_DSP
                            *ABS*:00000001 __HAS_DMAV2
                            *ABS*:00000001 __HAS_CODEGUARD
                            *ABS*:00000001 __HAS_PMP_ENHANCED
                            *ABS*:00000001 __HAS_EDS
                            *ABS*:00000001 __HAS_EP
                            *ABS*:00000011 __TARGET_DIVIDE_CYCLES
                            *ABS*:00000001 __C30ELF
    {standard input}:14     .nbss:00000000 _size_pin_cycling_vector
    {standard input}:20     .text:00000000 _config_adc1
    {standard input}:24     *ABS*:00000000 ___PA___
    {standard input}:253    *ABS*:00000000 ___BP___
    {standard input}:295    .text:00000134 _adc_callback
                            *ABS*:00000040 __ext_attr_.isr.text
    {standard input}:313    .isr.text:00000000 __AD1Interrupt
    {standard input}:366    .text:0000013e _get_adc_value
    {standard input}:401    .text:0000015e _turn_on_adc
    {standard input}:420    .text:0000016a _turn_off_adc
    {standard input}:439    .text:00000176 _start_samp
    {standard input}:458    .text:00000182 _start_conversion
    {standard input}:477    .text:0000018e _wait_for_done
    {standard input}:501    .text:000001a0 _set_default_parameters
    {standard input}:587    .text:00000206 _select_input_pin
    {standard input}:617    .text:00000226 _config_pin_cycling
    {standard input}:1116   .text:000004ee _get_pin_cycling_values
    {standard input}:5061   __c30_info:00000000 __psv_trap_errata
    {standard input}:319    .isr.text:00000000 .L0
    {standard input}:25     .text:00000000 .L0
                            .text:00000104 .L2
                            .text:0000014c .L6
                            .text:00000156 .L7
                            .text:00000190 .L13
                            .text:00000264 .L17
                            .text:000004e6 .L18
                            .text:00000280 .L19
                            .text:00000284 .L20
                            .text:00000296 .L21
                            .text:000002a8 .L22
                            .text:00000312 .L23
                            .text:00000406 .L24
                            .text:00000362 .L25
                            .text:000003b2 .L26
                            .text:00000498 .L27
                            .text:00000496 .L28
                            .text:00000482 .L29
                            .text:00000514 .L31
                            .text:00000500 .L32
                            .text:00000538 .L33
                            .text:0000053c .L34
                            .text:00000576 .L35
                            .text:00000562 .L36
                            .text:00000546 .L37
                    .debug_abbrev:00000000 .Ldebug_abbrev0
MPLAB XC16 ASSEMBLY Listing:   			page 107


                            .text:00000000 .Ltext0
                            .text:0000057e .Letext0
                      .debug_line:00000000 .Ldebug_line0
                            .text:00000000 .LFB0
                            .text:00000134 .LFE0
                            .text:00000134 .LFB1
                            .text:0000013e .LFE1
                        .isr.text:00000000 .LFB2
                        .isr.text:00000038 .LFE2
                            .text:0000013e .LFB3
                            .text:0000015e .LFE3
                            .text:0000015e .LFB4
                            .text:0000016a .LFE4
                            .text:0000016a .LFB5
                            .text:00000176 .LFE5
                            .text:00000176 .LFB6
                            .text:00000182 .LFE6
                            .text:00000182 .LFB7
                            .text:0000018e .LFE7
                            .text:0000018e .LFB8
                            .text:000001a0 .LFE8
                            .text:000001a0 .LFB9
                            .text:00000206 .LFE9
                       .debug_str:00000049 .LASF0
                            .text:00000206 .LFB10
                            .text:00000226 .LFE10
                            .text:00000226 .LFB11
                            .text:000004ee .LFE11
                            .text:000004ee .LFB12
                            .text:0000057e .LFE12
                       .debug_str:00000021 .LASF1
                       .debug_str:0000002b .LASF2
                       .debug_str:00000000 .LASF3
                       .debug_str:0000000c .LASF4
                       .debug_str:0000003d .LASF5
                       .debug_str:0000005d .LASF6
                       .debug_str:0000007b .LASF7
                       .debug_str:00000034 .LASF8
                       .debug_str:00000072 .LASF9
                       .debug_str:00000018 .LASF10
                       .debug_str:00000054 .LASF11
                       .debug_str:00000069 .LASF12
                     .debug_frame:00000000 .Lframe0
                      .debug_info:00000000 .Ldebug_info0

UNDEFINED SYMBOLS
_AD1CON1bits
_AD1CON2bits
_AD1CON3bits
_AD1CON4bits
_ANSELB
_AD1CSSL
_IFS0bits
_IEC0bits
_IPC3bits
CORCON
_RCOUNT
MPLAB XC16 ASSEMBLY Listing:   			page 108


_DSRPAG
_DSWPAG
__const_psvpage
_SR
_ADC1BUF0
_AD1CHS0bits
___udivsi3
_TMR5
_T5CONbits
_PR5
_IFS1bits
_IEC1bits
_AD1CON1
_AD1CON2
_AD1CON3
_AD1CON4
_AD1CHS0
_AD1CSSH

EQUATE SYMBOLS
/home/user/Documents/FST/Programming/project_sub-zero/lib/lib_pic33e/ADC.c = 0x0
                  __33EP256MU806 = 0x1
                      __dsPIC33E = 0x1
                       __HAS_DSP = 0x1
                     __HAS_DMAV2 = 0x1
                 __HAS_CODEGUARD = 0x1
              __HAS_PMP_ENHANCED = 0x1
                       __HAS_EDS = 0x1
                        __HAS_EP = 0x1
          __TARGET_DIVIDE_CYCLES = 0x11
                        __C30ELF = 0x1
                        ___PA___ = 0x0
                        ___BP___ = 0x0
            __ext_attr_.isr.text = 0x40
