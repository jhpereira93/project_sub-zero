MPLAB XC16 ASSEMBLY Listing:   			page 1


   1              	.file "/home/user/Documents/FST/Programming/project_sub-zero/lib/lib_pic33e/SPI.c"
   2              	.section .debug_abbrev,info
   3                 	.Ldebug_abbrev0:
   4                 	.section .debug_info,info
   5                 	.Ldebug_info0:
   6                 	.section .debug_line,info
   7                 	.Ldebug_line0:
   8 0000 45 05 00 00 	.section .text,code
   8      02 00 C2 00 
   8      00 00 01 01 
   8      FB 0E 0A 00 
   8      01 01 01 01 
   8      00 00 00 01 
   8      6C 69 62 2F 
   8      6C 69 62 5F 
   8      70 69 63 33 
   9              	.Ltext0:
  10              	.section .text,code
  11              	.align 2
  12              	.global _get_prescales
  13              	.type _get_prescales,@function
  14              	_get_prescales:
  15              	.LFB0:
  16              	.file 1 "lib/lib_pic33e/SPI.c"
   1:lib/lib_pic33e/SPI.c **** #ifndef NOSPI
   2:lib/lib_pic33e/SPI.c **** 
   3:lib/lib_pic33e/SPI.c **** #include <xc.h>
   4:lib/lib_pic33e/SPI.c **** #include <stdlib.h>
   5:lib/lib_pic33e/SPI.c **** 
   6:lib/lib_pic33e/SPI.c **** #include "lib_pic33e/timing.h"
   7:lib/lib_pic33e/SPI.c **** #include "SPI.h"
   8:lib/lib_pic33e/SPI.c **** 
   9:lib/lib_pic33e/SPI.c **** inline __attribute__((__always_inline__)) SPI_TRANSFER_MODE SPI1_TransferModeGet(void);
  10:lib/lib_pic33e/SPI.c **** inline __attribute__((__always_inline__)) SPI_TRANSFER_MODE SPI2_TransferModeGet(void);
  11:lib/lib_pic33e/SPI.c **** inline __attribute__((__always_inline__)) SPI_TRANSFER_MODE SPI3_TransferModeGet(void);
  12:lib/lib_pic33e/SPI.c **** inline __attribute__((__always_inline__)) SPI_TRANSFER_MODE SPI4_TransferModeGet(void);
  13:lib/lib_pic33e/SPI.c **** 
  14:lib/lib_pic33e/SPI.c **** /**********************************************************************
  15:lib/lib_pic33e/SPI.c ****  * Name:    get_prescales
  16:lib/lib_pic33e/SPI.c ****  * Args:    desired baudrate, *primary prescale, *secondary prescale
  17:lib/lib_pic33e/SPI.c ****  * Return:  best baudrate you can get for the defined FCY
  18:lib/lib_pic33e/SPI.c ****  * Desc:    calculates the prescales that will get you the closest baudrate
  19:lib/lib_pic33e/SPI.c ****  **********************************************************************/
  20:lib/lib_pic33e/SPI.c **** uint32_t get_prescales(uint32_t baudrate, uint8_t *p_presc, uint8_t *s_presc){
  17              	.loc 1 20 0
  18              	.set ___PA___,1
  19 000000  26 00 FA 	lnk #38
  20              	.LCFI0:
  21 000002  88 9F BE 	mov.d w8,[w15++]
  22              	.LCFI1:
  23 000004  8A 1F 78 	mov w10,[w15++]
  24              	.LCFI2:
  25              	.loc 1 20 0
  26 000006  70 0F 98 	mov w0,[w14+30]
  27 000008  01 17 98 	mov w1,[w14+32]
  28 00000a  12 17 98 	mov w2,[w14+34]
  29 00000c  23 17 98 	mov w3,[w14+36]
MPLAB XC16 ASSEMBLY Listing:   			page 2


  21:lib/lib_pic33e/SPI.c **** 
  22:lib/lib_pic33e/SPI.c ****     int i;
  23:lib/lib_pic33e/SPI.c ****     int j;
  24:lib/lib_pic33e/SPI.c ****     float error = 100;
  30              	.loc 1 24 0
  31 00000e  02 00 20 	mov #0,w2
  32 000010  83 2C 24 	mov #17096,w3
  25:lib/lib_pic33e/SPI.c ****     float best_prescaler_error = 100;
  33              	.loc 1 25 0
  34 000012  00 00 20 	mov #0,w0
  35 000014  81 2C 24 	mov #17096,w1
  36              	.loc 1 24 0
  37 000016  42 07 98 	mov w2,[w14+8]
  38 000018  53 07 98 	mov w3,[w14+10]
  39              	.loc 1 25 0
  40 00001a  20 07 98 	mov w0,[w14+4]
  41 00001c  31 07 98 	mov w1,[w14+6]
  26:lib/lib_pic33e/SPI.c ****     const uint8_t prim[4] = {64,16,4,1};
  42              	.loc 1 26 0
  43 00001e  70 00 47 	add w14,#16,w0
  44 000020  01 00 20 	mov #_C.68.18415,w1
  45 000022  42 00 20 	mov #4,w2
  46 000024  00 00 07 	rcall _memcpy
  27:lib/lib_pic33e/SPI.c ****     const uint8_t sec[8] = {8,7,6,5,4,3,2,1};
  47              	.loc 1 27 0
  48 000026  74 00 47 	add w14,#20,w0
  49 000028  01 00 20 	mov #_C.69.18416,w1
  50 00002a  82 00 20 	mov #8,w2
  51 00002c  00 00 07 	rcall _memcpy
  28:lib/lib_pic33e/SPI.c ****     uint32_t fsck;
  29:lib/lib_pic33e/SPI.c ****     uint8_t best_prescaler[2] = {0,0};
  52              	.loc 1 29 0
  53 00002e  00 41 EB 	clr.b w2
  54 000030  80 40 EB 	clr.b w1
  30:lib/lib_pic33e/SPI.c **** 
  31:lib/lib_pic33e/SPI.c ****     for(i=0; i<4; i++){
  55              	.loc 1 31 0
  56 000032  00 00 EB 	clr w0
  57              	.loc 1 29 0
  58 000034  42 5F 98 	mov.b w2,[w14+28]
  59 000036  51 5F 98 	mov.b w1,[w14+29]
  60              	.loc 1 31 0
  61 000038  00 0F 78 	mov w0,[w14]
  62 00003a  00 00 37 	bra .L2
  63              	.L7:
  32:lib/lib_pic33e/SPI.c ****         for(j=0; j<8; j++){
  64              	.loc 1 32 0
  65 00003c  00 00 EB 	clr w0
  66 00003e  10 07 98 	mov w0,[w14+2]
  67 000040  00 00 37 	bra .L3
  68              	.L6:
  33:lib/lib_pic33e/SPI.c ****             fsck = FCY/(prim[i]*sec[j]);
  69              	.loc 1 33 0
  70 000042  1E 00 90 	mov [w14+2],w0
  71 000044  1E 01 78 	mov [w14],w2
  72 000046  F0 00 41 	add w2,#16,w1
  73 000048  74 00 40 	add w0,#20,w0
MPLAB XC16 ASSEMBLY Listing:   			page 3


  74 00004a  EE C0 78 	mov.b [w14+w1],w1
  75 00004c  6E 40 78 	mov.b [w14+w0],w0
  76 00004e  81 80 FB 	ze w1,w1
  77 000050  00 80 FB 	ze w0,w0
  34:lib/lib_pic33e/SPI.c ****             error = (abs((int)fsck-(int)baudrate)*100.0/baudrate);
  35:lib/lib_pic33e/SPI.c ****             if(error < best_prescaler_error){
  78              	.loc 1 35 0
  79 000052  1A C0 B3 	mov.b #1,w10
  80              	.loc 1 33 0
  81 000054  80 89 B9 	mulw.ss w1,w0,w2
  82 000056  00 00 29 	mov #36864,w0
  83 000058  01 3D 20 	mov #976,w1
  84 00005a  CF 91 DE 	asr w2,#15,w3
  85 00005c  00 00 07 	rcall ___udivsi3
  86              	.loc 1 34 0
  87 00005e  7E 09 90 	mov [w14+30],w2
  88              	.loc 1 33 0
  89 000060  60 07 98 	mov w0,[w14+12]
  90 000062  71 07 98 	mov w1,[w14+14]
  91              	.loc 1 34 0
  92 000064  6E 00 90 	mov [w14+12],w0
  93 000066  02 00 50 	sub w0,w2,w0
  94 000068  00 F0 A7 	btsc w0,#15
  95 00006a  00 00 EA 	neg w0,w0
  96 00006c  CF 80 DE 	asr w0,#15,w1
  97 00006e  00 00 07 	rcall ___floatsisf
  98 000070  02 00 20 	mov #0,w2
  99 000072  83 2C 24 	mov #17096,w3
 100 000074  00 00 07 	rcall ___mulsf3
 101 000076  00 01 BE 	mov.d w0,w2
 102 000078  7E 08 90 	mov [w14+30],w0
 103 00007a  8E 10 90 	mov [w14+32],w1
 104 00007c  02 04 BE 	mov.d w2,w8
 105 00007e  00 00 07 	rcall ___floatunsisf
 106 000080  00 01 BE 	mov.d w0,w2
 107 000082  08 00 BE 	mov.d w8,w0
 108 000084  00 00 07 	rcall ___divsf3
 109              	.loc 1 35 0
 110 000086  2E 01 90 	mov [w14+4],w2
 111 000088  BE 01 90 	mov [w14+6],w3
 112              	.loc 1 34 0
 113 00008a  40 07 98 	mov w0,[w14+8]
 114 00008c  51 07 98 	mov w1,[w14+10]
 115              	.loc 1 35 0
 116 00008e  4E 00 90 	mov [w14+8],w0
 117 000090  DE 00 90 	mov [w14+10],w1
 118 000092  00 00 07 	rcall ___ltsf2
 119 000094  00 00 E0 	cp0 w0
 120              	.set ___BP___,0
 121 000096  00 00 35 	bra lt,.L4
 122 000098  00 45 EB 	clr.b w10
 123              	.L4:
 124 00009a  0A 04 E0 	cp0.b w10
 125              	.set ___BP___,0
 126 00009c  00 00 32 	bra z,.L5
  36:lib/lib_pic33e/SPI.c ****                 best_prescaler[0] = i;
 127              	.loc 1 36 0
MPLAB XC16 ASSEMBLY Listing:   			page 4


 128 00009e  1E 00 78 	mov [w14],w0
 129 0000a0  00 40 78 	mov.b w0,w0
 130 0000a2  40 5F 98 	mov.b w0,[w14+28]
  37:lib/lib_pic33e/SPI.c ****                 best_prescaler[1] = j;
 131              	.loc 1 37 0
 132 0000a4  1E 00 90 	mov [w14+2],w0
 133 0000a6  00 40 78 	mov.b w0,w0
 134 0000a8  50 5F 98 	mov.b w0,[w14+29]
  38:lib/lib_pic33e/SPI.c ****                 best_prescaler_error = error;
 135              	.loc 1 38 0
 136 0000aa  4E 01 90 	mov [w14+8],w2
 137 0000ac  DE 01 90 	mov [w14+10],w3
 138 0000ae  22 07 98 	mov w2,[w14+4]
 139 0000b0  33 07 98 	mov w3,[w14+6]
 140              	.L5:
 141              	.loc 1 32 0
 142 0000b2  00 00 00 	nop 
 143 0000b4  1E 00 90 	mov [w14+2],w0
 144 0000b6  00 00 E8 	inc w0,w0
 145 0000b8  10 07 98 	mov w0,[w14+2]
 146              	.L3:
 147 0000ba  00 00 00 	nop 
 148 0000bc  1E 00 90 	mov [w14+2],w0
 149 0000be  E7 0F 50 	sub w0,#7,[w15]
 150              	.set ___BP___,0
 151 0000c0  00 00 34 	bra le,.L6
 152              	.loc 1 31 0
 153 0000c2  1E 0F E8 	inc [w14],[w14]
 154              	.L2:
 155 0000c4  00 00 00 	nop 
 156 0000c6  1E 00 78 	mov [w14],w0
 157 0000c8  E3 0F 50 	sub w0,#3,[w15]
 158              	.set ___BP___,0
 159 0000ca  00 00 34 	bra le,.L7
  39:lib/lib_pic33e/SPI.c ****             }
  40:lib/lib_pic33e/SPI.c ****             
  41:lib/lib_pic33e/SPI.c ****         }
  42:lib/lib_pic33e/SPI.c ****     }
  43:lib/lib_pic33e/SPI.c ****     *p_presc = best_prescaler[0];
 160              	.loc 1 43 0
 161 0000cc  CE 58 90 	mov.b [w14+28],w1
 162 0000ce  1E 10 90 	mov [w14+34],w0
 163 0000d0  01 48 78 	mov.b w1,[w0]
  44:lib/lib_pic33e/SPI.c ****     *s_presc = best_prescaler[1];
 164              	.loc 1 44 0
 165 0000d2  DE 58 90 	mov.b [w14+29],w1
 166 0000d4  2E 10 90 	mov [w14+36],w0
 167 0000d6  01 48 78 	mov.b w1,[w0]
  45:lib/lib_pic33e/SPI.c **** 	fsck = FCY/(prim[best_prescaler[0]]*sec[best_prescaler[1]]);
 168              	.loc 1 45 0
 169 0000d8  CE 58 90 	mov.b [w14+28],w1
 170 0000da  5E 58 90 	mov.b [w14+29],w0
 171 0000dc  81 80 FB 	ze w1,w1
 172 0000de  00 80 FB 	ze w0,w0
 173 0000e0  F0 80 40 	add w1,#16,w1
 174 0000e2  74 00 40 	add w0,#20,w0
 175 0000e4  EE C0 78 	mov.b [w14+w1],w1
MPLAB XC16 ASSEMBLY Listing:   			page 5


 176 0000e6  6E 40 78 	mov.b [w14+w0],w0
 177 0000e8  81 81 FB 	ze w1,w3
 178 0000ea  00 81 FB 	ze w0,w2
 179 0000ec  00 00 29 	mov #36864,w0
 180 0000ee  01 3D 20 	mov #976,w1
 181 0000f0  82 99 B9 	mulw.ss w3,w2,w2
 182 0000f2  CF 91 DE 	asr w2,#15,w3
 183 0000f4  00 00 07 	rcall ___udivsi3
 184 0000f6  60 07 98 	mov w0,[w14+12]
 185 0000f8  71 07 98 	mov w1,[w14+14]
  46:lib/lib_pic33e/SPI.c **** 
  47:lib/lib_pic33e/SPI.c **** 
  48:lib/lib_pic33e/SPI.c ****     return fsck;
 186              	.loc 1 48 0
 187 0000fa  6E 00 90 	mov [w14+12],w0
 188 0000fc  FE 00 90 	mov [w14+14],w1
  49:lib/lib_pic33e/SPI.c **** 
  50:lib/lib_pic33e/SPI.c **** }
 189              	.loc 1 50 0
 190 0000fe  4F 05 78 	mov [--w15],w10
 191 000100  4F 04 BE 	mov.d [--w15],w8
 192 000102  8E 07 78 	mov w14,w15
 193 000104  4F 07 78 	mov [--w15],w14
 194 000106  00 40 A9 	bclr CORCON,#2
 195 000108  00 00 06 	return 
 196              	.set ___PA___,0
 197              	.LFE0:
 198              	.size _get_prescales,.-_get_prescales
 199              	.align 2
 200              	.global _config_SPI1
 201              	.type _config_SPI1,@function
 202              	_config_SPI1:
 203              	.LFB1:
  51:lib/lib_pic33e/SPI.c **** 
  52:lib/lib_pic33e/SPI.c **** /**********************************************************************
  53:lib/lib_pic33e/SPI.c ****  * Name:    config_SPI1
  54:lib/lib_pic33e/SPI.c ****  * Args:    SPI_Peripheral_Configuration config - configuration struct
  55:lib/lib_pic33e/SPI.c ****  * Return:  best baudrate you can get for the defined FCY
  56:lib/lib_pic33e/SPI.c ****  * Desc:    config SPI module
  57:lib/lib_pic33e/SPI.c ****  **********************************************************************/
  58:lib/lib_pic33e/SPI.c **** uint32_t config_SPI1(SPI_Peripheral_Configuration config){
 204              	.loc 1 58 0
 205              	.set ___PA___,1
 206 00010a  0C 00 FA 	lnk #12
 207              	.LCFI3:
 208 00010c  88 1F 78 	mov w8,[w15++]
 209              	.LCFI4:
 210              	.loc 1 58 0
 211 00010e  30 07 98 	mov w0,[w14+6]
  59:lib/lib_pic33e/SPI.c **** 
  60:lib/lib_pic33e/SPI.c ****     uint32_t baudrate;
  61:lib/lib_pic33e/SPI.c ****     uint8_t p_presc;
  62:lib/lib_pic33e/SPI.c ****     uint8_t s_presc;
  63:lib/lib_pic33e/SPI.c **** 
  64:lib/lib_pic33e/SPI.c ****     SPI1STATbits.SPIEN = 0; //Assure module is disabled
 212              	.loc 1 64 0
 213 000110  01 E0 A9 	bclr.b _SPI1STATbits+1,#7
MPLAB XC16 ASSEMBLY Listing:   			page 6


 214              	.loc 1 58 0
 215 000112  41 07 98 	mov w1,[w14+8]
  65:lib/lib_pic33e/SPI.c **** 
  66:lib/lib_pic33e/SPI.c ****     /* SPI1CON1 */
  67:lib/lib_pic33e/SPI.c ****     SPI1CON1bits.DISSCK = config.disable_SCK_pin;
 216              	.loc 1 67 0
 217 000114  08 00 80 	mov _SPI1CON1bits,w8
 218              	.loc 1 58 0
 219 000116  52 07 98 	mov w2,[w14+10]
 220              	.loc 1 67 0
 221 000118  BE 03 90 	mov [w14+6],w7
  68:lib/lib_pic33e/SPI.c ****     SPI1CON1bits.DISSDO = config.disable_SDO_pin;
 222              	.loc 1 68 0
 223 00011a  3E 03 90 	mov [w14+6],w6
  69:lib/lib_pic33e/SPI.c ****     SPI1CON1bits.MODE16 = config.transfer_mode;
 224              	.loc 1 69 0
 225 00011c  BE 02 90 	mov [w14+6],w5
  70:lib/lib_pic33e/SPI.c ****     SPI1CON1bits.SSEN = config.enable_SS_pin;
 226              	.loc 1 70 0
 227 00011e  3E 02 90 	mov [w14+6],w4
  71:lib/lib_pic33e/SPI.c ****     SPI1CON1bits.MSTEN = config.enable_master_mode;
 228              	.loc 1 71 0
 229 000120  3E 01 90 	mov [w14+6],w2
  72:lib/lib_pic33e/SPI.c ****     /* Timing configuration */
  73:lib/lib_pic33e/SPI.c ****     SPI1CON1bits.CKP = ((config.SPI_mode >> 1) & 0b1);
 230              	.loc 1 73 0
 231 000122  3E 00 90 	mov [w14+6],w0
  74:lib/lib_pic33e/SPI.c ****     SPI1CON1bits.CKE = (~(config.SPI_mode & 0b1));
 232              	.loc 1 74 0
 233 000124  BE 00 90 	mov [w14+6],w1
  75:lib/lib_pic33e/SPI.c ****     SPI1CON1bits.SMP = config.data_sample_phase;
 234              	.loc 1 75 0
 235 000126  BE 01 90 	mov [w14+6],w3
 236              	.loc 1 67 0
 237 000128  C2 3B DE 	lsr w7,#2,w7
 238 00012a  08 C0 A1 	bclr w8,#12
 239 00012c  E1 C3 63 	and.b w7,#1,w7
 240              	.loc 1 68 0
 241 00012e  43 33 DE 	lsr w6,#3,w6
 242              	.loc 1 67 0
 243 000130  87 83 FB 	ze w7,w7
 244              	.loc 1 68 0
 245 000132  61 43 63 	and.b w6,#1,w6
 246              	.loc 1 67 0
 247 000134  E1 83 63 	and w7,#1,w7
 248              	.loc 1 68 0
 249 000136  06 83 FB 	ze w6,w6
 250              	.loc 1 67 0
 251 000138  CC 3B DD 	sl w7,#12,w7
 252              	.loc 1 68 0
 253 00013a  61 03 63 	and w6,#1,w6
 254              	.loc 1 67 0
 255 00013c  88 83 73 	ior w7,w8,w7
 256              	.loc 1 68 0
 257 00013e  4B 33 DD 	sl w6,#11,w6
 258              	.loc 1 67 0
 259 000140  07 00 88 	mov w7,_SPI1CON1bits
MPLAB XC16 ASSEMBLY Listing:   			page 7


 260              	.loc 1 69 0
 261 000142  C4 2A DE 	lsr w5,#4,w5
 262              	.loc 1 68 0
 263 000144  07 00 80 	mov _SPI1CON1bits,w7
 264              	.loc 1 69 0
 265 000146  E1 C2 62 	and.b w5,#1,w5
 266              	.loc 1 68 0
 267 000148  07 B0 A1 	bclr w7,#11
 268              	.loc 1 69 0
 269 00014a  85 82 FB 	ze w5,w5
 270              	.loc 1 68 0
 271 00014c  07 03 73 	ior w6,w7,w6
 272              	.loc 1 69 0
 273 00014e  E1 82 62 	and w5,#1,w5
 274              	.loc 1 68 0
 275 000150  06 00 88 	mov w6,_SPI1CON1bits
 276              	.loc 1 69 0
 277 000152  CA 2A DD 	sl w5,#10,w5
 278 000154  06 00 80 	mov _SPI1CON1bits,w6
 279              	.loc 1 70 0
 280 000156  48 22 DE 	lsr w4,#8,w4
 281              	.loc 1 69 0
 282 000158  06 A0 A1 	bclr w6,#10
 283              	.loc 1 70 0
 284 00015a  61 42 62 	and.b w4,#1,w4
 285              	.loc 1 69 0
 286 00015c  86 82 72 	ior w5,w6,w5
 287              	.loc 1 70 0
 288 00015e  04 82 FB 	ze w4,w4
 289              	.loc 1 69 0
 290 000160  05 00 88 	mov w5,_SPI1CON1bits
 291              	.loc 1 70 0
 292 000162  61 02 62 	and w4,#1,w4
 293 000164  05 00 80 	mov _SPI1CON1bits,w5
 294 000166  47 22 DD 	sl w4,#7,w4
 295 000168  05 70 A1 	bclr w5,#7
 296              	.loc 1 71 0
 297 00016a  49 11 DE 	lsr w2,#9,w2
 298              	.loc 1 70 0
 299 00016c  05 02 72 	ior w4,w5,w4
 300              	.loc 1 71 0
 301 00016e  61 41 61 	and.b w2,#1,w2
 302              	.loc 1 70 0
 303 000170  04 00 88 	mov w4,_SPI1CON1bits
 304              	.loc 1 71 0
 305 000172  02 81 FB 	ze w2,w2
 306 000174  04 00 80 	mov _SPI1CON1bits,w4
 307 000176  61 01 61 	and w2,#1,w2
 308 000178  04 50 A1 	bclr w4,#5
 309 00017a  45 11 DD 	sl w2,#5,w2
 310              	.loc 1 73 0
 311 00017c  46 00 DE 	lsr w0,#6,w0
 312              	.loc 1 71 0
 313 00017e  04 01 71 	ior w2,w4,w2
 314              	.loc 1 73 0
 315 000180  63 40 60 	and.b w0,#3,w0
 316              	.loc 1 71 0
MPLAB XC16 ASSEMBLY Listing:   			page 8


 317 000182  02 00 88 	mov w2,_SPI1CON1bits
 318              	.loc 1 73 0
 319 000184  00 80 FB 	ze w0,w0
 320 000186  02 00 80 	mov _SPI1CON1bits,w2
 321 000188  00 80 D1 	asr w0,w0
 322 00018a  02 60 A1 	bclr w2,#6
 323 00018c  00 40 78 	mov.b w0,w0
 324              	.loc 1 74 0
 325 00018e  C6 08 DE 	lsr w1,#6,w1
 326              	.loc 1 73 0
 327 000190  61 40 60 	and.b w0,#1,w0
 328              	.loc 1 74 0
 329 000192  E3 C0 60 	and.b w1,#3,w1
 330              	.loc 1 73 0
 331 000194  61 40 60 	and.b w0,#1,w0
 332 000196  61 40 60 	and.b w0,#1,w0
 333              	.loc 1 74 0
 334 000198  61 C2 60 	and.b w1,#1,w4
 335              	.loc 1 73 0
 336 00019a  80 80 FB 	ze w0,w1
 337              	.loc 1 74 0
 338 00019c  04 C0 EA 	com.b w4,w0
 339              	.loc 1 73 0
 340 00019e  E1 80 60 	and w1,#1,w1
 341 0001a0  C6 08 DD 	sl w1,#6,w1
 342              	.loc 1 74 0
 343 0001a2  61 40 60 	and.b w0,#1,w0
 344              	.loc 1 73 0
 345 0001a4  82 80 70 	ior w1,w2,w1
 346              	.loc 1 74 0
 347 0001a6  00 80 FB 	ze w0,w0
 348              	.loc 1 73 0
 349 0001a8  01 00 88 	mov w1,_SPI1CON1bits
 350              	.loc 1 74 0
 351 0001aa  E1 00 60 	and w0,#1,w1
 352 0001ac  00 00 80 	mov _SPI1CON1bits,w0
 353 0001ae  C8 08 DD 	sl w1,#8,w1
 354 0001b0  00 01 78 	mov w0,w2
 355 0001b2  02 80 A1 	bclr w2,#8
 356              	.loc 1 75 0
 357 0001b4  45 18 DE 	lsr w3,#5,w0
 358              	.loc 1 74 0
 359 0001b6  82 80 70 	ior w1,w2,w1
 360              	.loc 1 75 0
 361 0001b8  61 40 60 	and.b w0,#1,w0
 362              	.loc 1 74 0
 363 0001ba  01 00 88 	mov w1,_SPI1CON1bits
 364              	.loc 1 75 0
 365 0001bc  00 80 FB 	ze w0,w0
 366 0001be  01 00 80 	mov _SPI1CON1bits,w1
 367 0001c0  61 00 60 	and w0,#1,w0
 368 0001c2  81 01 78 	mov w1,w3
 369 0001c4  03 90 A1 	bclr w3,#9
 370 0001c6  49 01 DD 	sl w0,#9,w2
  76:lib/lib_pic33e/SPI.c **** 
  77:lib/lib_pic33e/SPI.c ****     /* Chose baudrate prescalers */
  78:lib/lib_pic33e/SPI.c ****     baudrate = get_prescales(config.baudrate, &p_presc, &s_presc);
MPLAB XC16 ASSEMBLY Listing:   			page 9


 371              	.loc 1 78 0
 372 0001c8  4E 00 90 	mov [w14+8],w0
 373 0001ca  DE 00 90 	mov [w14+10],w1
 374              	.loc 1 75 0
 375 0001cc  03 01 71 	ior w2,w3,w2
 376              	.loc 1 78 0
 377 0001ce  E5 01 47 	add w14,#5,w3
 378              	.loc 1 75 0
 379 0001d0  02 00 88 	mov w2,_SPI1CON1bits
 380              	.loc 1 78 0
 381 0001d2  64 01 47 	add w14,#4,w2
 382 0001d4  00 00 07 	rcall _get_prescales
 383 0001d6  00 8F BE 	mov.d w0,[w14]
  79:lib/lib_pic33e/SPI.c ****     SPI1CON1bits.PPRE = p_presc;
 384              	.loc 1 79 0
 385 0001d8  02 00 80 	mov _SPI1CON1bits,w2
 386 0001da  4E 40 90 	mov.b [w14+4],w0
 387 0001dc  C1 FF 2F 	mov #-4,w1
 388 0001de  63 40 60 	and.b w0,#3,w0
 389 0001e0  01 01 61 	and w2,w1,w2
 390 0001e2  80 80 FB 	ze w0,w1
  80:lib/lib_pic33e/SPI.c ****     SPI1CON1bits.SPRE = s_presc;
 391              	.loc 1 80 0
 392 0001e4  5E 40 90 	mov.b [w14+5],w0
 393              	.loc 1 79 0
 394 0001e6  E3 80 60 	and w1,#3,w1
 395              	.loc 1 80 0
 396 0001e8  67 40 60 	and.b w0,#7,w0
 397              	.loc 1 79 0
 398 0001ea  82 80 70 	ior w1,w2,w1
 399              	.loc 1 80 0
 400 0001ec  00 80 FB 	ze w0,w0
 401              	.loc 1 79 0
 402 0001ee  01 00 88 	mov w1,_SPI1CON1bits
 403              	.loc 1 80 0
 404 0001f0  67 00 60 	and w0,#7,w0
 405 0001f2  02 00 80 	mov _SPI1CON1bits,w2
 406 0001f4  C2 00 DD 	sl w0,#2,w1
 407 0001f6  30 FE 2F 	mov #-29,w0
  81:lib/lib_pic33e/SPI.c **** 
  82:lib/lib_pic33e/SPI.c ****     /* SPI1CON2 */
  83:lib/lib_pic33e/SPI.c ****     SPI1CON2bits.FRMEN = 0;  // Disable all framed SPI support
  84:lib/lib_pic33e/SPI.c ****     SPI1CON2bits.SPIBEN = 1; // Enhanced mode is enabled
  85:lib/lib_pic33e/SPI.c **** 
  86:lib/lib_pic33e/SPI.c ****     /* SPI1STAT */
  87:lib/lib_pic33e/SPI.c ****     SPI1STATbits.SPISIDL = 0; // continue operation in Idle
  88:lib/lib_pic33e/SPI.c ****     SPI1STATbits.SPIROV = 0; // Clear overflow
  89:lib/lib_pic33e/SPI.c ****     SPI1STATbits.SISEL = 0b011; // Set interrupt to SPI receive buffer full is set (Not used)
 408              	.loc 1 89 0
 409 0001f8  33 FE 2F 	mov #-29,w3
 410              	.loc 1 80 0
 411 0001fa  00 01 61 	and w2,w0,w2
  90:lib/lib_pic33e/SPI.c **** 
  91:lib/lib_pic33e/SPI.c ****     /* Configuration is finished. Enable the module */
  92:lib/lib_pic33e/SPI.c ****     SPI1STATbits.SPIEN = config.enabled;
 412              	.loc 1 92 0
 413 0001fc  3E 00 90 	mov [w14+6],w0
MPLAB XC16 ASSEMBLY Listing:   			page 10


 414              	.loc 1 80 0
 415 0001fe  82 80 70 	ior w1,w2,w1
 416              	.loc 1 92 0
 417 000200  61 40 60 	and.b w0,#1,w0
 418              	.loc 1 80 0
 419 000202  01 00 88 	mov w1,_SPI1CON1bits
 420              	.loc 1 92 0
 421 000204  00 80 FB 	ze w0,w0
 422              	.loc 1 83 0
 423 000206  01 E0 A9 	bclr.b _SPI1CON2bits+1,#7
 424              	.loc 1 92 0
 425 000208  4F 01 DD 	sl w0,#15,w2
 426              	.loc 1 84 0
 427 00020a  00 00 A8 	bset.b _SPI1CON2bits,#0
  93:lib/lib_pic33e/SPI.c ****     return baudrate;
 428              	.loc 1 93 0
 429 00020c  1E 00 78 	mov [w14],w0
 430 00020e  9E 00 90 	mov [w14+2],w1
 431              	.loc 1 87 0
 432 000210  01 A0 A9 	bclr.b _SPI1STATbits+1,#5
 433              	.loc 1 88 0
 434 000212  00 C0 A9 	bclr.b _SPI1STATbits,#6
 435              	.loc 1 89 0
 436 000214  04 00 80 	mov _SPI1STATbits,w4
 437 000216  83 01 62 	and w4,w3,w3
 438 000218  C3 00 B3 	ior #12,w3
 439 00021a  03 00 88 	mov w3,_SPI1STATbits
 440              	.loc 1 92 0
 441 00021c  03 00 80 	mov _SPI1STATbits,w3
 442 00021e  03 F0 A1 	bclr w3,#15
 443 000220  03 01 71 	ior w2,w3,w2
 444 000222  02 00 88 	mov w2,_SPI1STATbits
  94:lib/lib_pic33e/SPI.c **** }
 445              	.loc 1 94 0
 446 000224  4F 04 78 	mov [--w15],w8
 447 000226  8E 07 78 	mov w14,w15
 448 000228  4F 07 78 	mov [--w15],w14
 449 00022a  00 40 A9 	bclr CORCON,#2
 450 00022c  00 00 06 	return 
 451              	.set ___PA___,0
 452              	.LFE1:
 453              	.size _config_SPI1,.-_config_SPI1
 454              	.align 2
 455              	.global _config_SPI2
 456              	.type _config_SPI2,@function
 457              	_config_SPI2:
 458              	.LFB2:
  95:lib/lib_pic33e/SPI.c **** 
  96:lib/lib_pic33e/SPI.c **** /**********************************************************************
  97:lib/lib_pic33e/SPI.c ****  * Name:    config_SPI2
  98:lib/lib_pic33e/SPI.c ****  * Args:    SPI_Peripheral_Configuration config - configuration struct
  99:lib/lib_pic33e/SPI.c ****  * Return:  best baudrate you can get for the defined FCY
 100:lib/lib_pic33e/SPI.c ****  * Desc:    config SPI module
 101:lib/lib_pic33e/SPI.c ****  **********************************************************************/
 102:lib/lib_pic33e/SPI.c **** uint32_t config_SPI2(SPI_Peripheral_Configuration config){
 459              	.loc 1 102 0
 460              	.set ___PA___,1
MPLAB XC16 ASSEMBLY Listing:   			page 11


 461 00022e  0C 00 FA 	lnk #12
 462              	.LCFI5:
 463 000230  88 1F 78 	mov w8,[w15++]
 464              	.LCFI6:
 465              	.loc 1 102 0
 466 000232  30 07 98 	mov w0,[w14+6]
 103:lib/lib_pic33e/SPI.c ****     
 104:lib/lib_pic33e/SPI.c ****     uint32_t baudrate;
 105:lib/lib_pic33e/SPI.c ****     uint8_t p_presc;
 106:lib/lib_pic33e/SPI.c ****     uint8_t s_presc;
 107:lib/lib_pic33e/SPI.c **** 
 108:lib/lib_pic33e/SPI.c ****     SPI2STATbits.SPIEN = 0; //Assure module is disabled
 467              	.loc 1 108 0
 468 000234  01 E0 A9 	bclr.b _SPI2STATbits+1,#7
 469              	.loc 1 102 0
 470 000236  41 07 98 	mov w1,[w14+8]
 109:lib/lib_pic33e/SPI.c **** 
 110:lib/lib_pic33e/SPI.c ****     /* SPI2CON1 */
 111:lib/lib_pic33e/SPI.c ****     SPI2CON1bits.DISSCK = config.disable_SCK_pin;
 471              	.loc 1 111 0
 472 000238  08 00 80 	mov _SPI2CON1bits,w8
 473              	.loc 1 102 0
 474 00023a  52 07 98 	mov w2,[w14+10]
 475              	.loc 1 111 0
 476 00023c  BE 03 90 	mov [w14+6],w7
 112:lib/lib_pic33e/SPI.c ****     SPI2CON1bits.DISSDO = config.disable_SDO_pin;
 477              	.loc 1 112 0
 478 00023e  3E 03 90 	mov [w14+6],w6
 113:lib/lib_pic33e/SPI.c ****     SPI2CON1bits.MODE16 = config.transfer_mode;
 479              	.loc 1 113 0
 480 000240  BE 02 90 	mov [w14+6],w5
 114:lib/lib_pic33e/SPI.c ****     SPI2CON1bits.SSEN = config.enable_SS_pin;
 481              	.loc 1 114 0
 482 000242  3E 02 90 	mov [w14+6],w4
 115:lib/lib_pic33e/SPI.c ****     SPI2CON1bits.MSTEN = config.enable_master_mode;
 483              	.loc 1 115 0
 484 000244  3E 01 90 	mov [w14+6],w2
 116:lib/lib_pic33e/SPI.c ****     /* Timing configuration */
 117:lib/lib_pic33e/SPI.c ****     SPI2CON1bits.CKP = ((config.SPI_mode >> 1) & 0b1);
 485              	.loc 1 117 0
 486 000246  3E 00 90 	mov [w14+6],w0
 118:lib/lib_pic33e/SPI.c ****     SPI2CON1bits.CKE = (~(config.SPI_mode & 0b1));
 487              	.loc 1 118 0
 488 000248  BE 00 90 	mov [w14+6],w1
 119:lib/lib_pic33e/SPI.c ****     SPI2CON1bits.SMP = config.data_sample_phase;
 489              	.loc 1 119 0
 490 00024a  BE 01 90 	mov [w14+6],w3
 491              	.loc 1 111 0
 492 00024c  C2 3B DE 	lsr w7,#2,w7
 493 00024e  08 C0 A1 	bclr w8,#12
 494 000250  E1 C3 63 	and.b w7,#1,w7
 495              	.loc 1 112 0
 496 000252  43 33 DE 	lsr w6,#3,w6
 497              	.loc 1 111 0
 498 000254  87 83 FB 	ze w7,w7
 499              	.loc 1 112 0
 500 000256  61 43 63 	and.b w6,#1,w6
MPLAB XC16 ASSEMBLY Listing:   			page 12


 501              	.loc 1 111 0
 502 000258  E1 83 63 	and w7,#1,w7
 503              	.loc 1 112 0
 504 00025a  06 83 FB 	ze w6,w6
 505              	.loc 1 111 0
 506 00025c  CC 3B DD 	sl w7,#12,w7
 507              	.loc 1 112 0
 508 00025e  61 03 63 	and w6,#1,w6
 509              	.loc 1 111 0
 510 000260  88 83 73 	ior w7,w8,w7
 511              	.loc 1 112 0
 512 000262  4B 33 DD 	sl w6,#11,w6
 513              	.loc 1 111 0
 514 000264  07 00 88 	mov w7,_SPI2CON1bits
 515              	.loc 1 113 0
 516 000266  C4 2A DE 	lsr w5,#4,w5
 517              	.loc 1 112 0
 518 000268  07 00 80 	mov _SPI2CON1bits,w7
 519              	.loc 1 113 0
 520 00026a  E1 C2 62 	and.b w5,#1,w5
 521              	.loc 1 112 0
 522 00026c  07 B0 A1 	bclr w7,#11
 523              	.loc 1 113 0
 524 00026e  85 82 FB 	ze w5,w5
 525              	.loc 1 112 0
 526 000270  07 03 73 	ior w6,w7,w6
 527              	.loc 1 113 0
 528 000272  E1 82 62 	and w5,#1,w5
 529              	.loc 1 112 0
 530 000274  06 00 88 	mov w6,_SPI2CON1bits
 531              	.loc 1 113 0
 532 000276  CA 2A DD 	sl w5,#10,w5
 533 000278  06 00 80 	mov _SPI2CON1bits,w6
 534              	.loc 1 114 0
 535 00027a  48 22 DE 	lsr w4,#8,w4
 536              	.loc 1 113 0
 537 00027c  06 A0 A1 	bclr w6,#10
 538              	.loc 1 114 0
 539 00027e  61 42 62 	and.b w4,#1,w4
 540              	.loc 1 113 0
 541 000280  86 82 72 	ior w5,w6,w5
 542              	.loc 1 114 0
 543 000282  04 82 FB 	ze w4,w4
 544              	.loc 1 113 0
 545 000284  05 00 88 	mov w5,_SPI2CON1bits
 546              	.loc 1 114 0
 547 000286  61 02 62 	and w4,#1,w4
 548 000288  05 00 80 	mov _SPI2CON1bits,w5
 549 00028a  47 22 DD 	sl w4,#7,w4
 550 00028c  05 70 A1 	bclr w5,#7
 551              	.loc 1 115 0
 552 00028e  49 11 DE 	lsr w2,#9,w2
 553              	.loc 1 114 0
 554 000290  05 02 72 	ior w4,w5,w4
 555              	.loc 1 115 0
 556 000292  61 41 61 	and.b w2,#1,w2
 557              	.loc 1 114 0
MPLAB XC16 ASSEMBLY Listing:   			page 13


 558 000294  04 00 88 	mov w4,_SPI2CON1bits
 559              	.loc 1 115 0
 560 000296  02 81 FB 	ze w2,w2
 561 000298  04 00 80 	mov _SPI2CON1bits,w4
 562 00029a  61 01 61 	and w2,#1,w2
 563 00029c  04 50 A1 	bclr w4,#5
 564 00029e  45 11 DD 	sl w2,#5,w2
 565              	.loc 1 117 0
 566 0002a0  46 00 DE 	lsr w0,#6,w0
 567              	.loc 1 115 0
 568 0002a2  04 01 71 	ior w2,w4,w2
 569              	.loc 1 117 0
 570 0002a4  63 40 60 	and.b w0,#3,w0
 571              	.loc 1 115 0
 572 0002a6  02 00 88 	mov w2,_SPI2CON1bits
 573              	.loc 1 117 0
 574 0002a8  00 80 FB 	ze w0,w0
 575 0002aa  02 00 80 	mov _SPI2CON1bits,w2
 576 0002ac  00 80 D1 	asr w0,w0
 577 0002ae  02 60 A1 	bclr w2,#6
 578 0002b0  00 40 78 	mov.b w0,w0
 579              	.loc 1 118 0
 580 0002b2  C6 08 DE 	lsr w1,#6,w1
 581              	.loc 1 117 0
 582 0002b4  61 40 60 	and.b w0,#1,w0
 583              	.loc 1 118 0
 584 0002b6  E3 C0 60 	and.b w1,#3,w1
 585              	.loc 1 117 0
 586 0002b8  61 40 60 	and.b w0,#1,w0
 587 0002ba  61 40 60 	and.b w0,#1,w0
 588              	.loc 1 118 0
 589 0002bc  61 C2 60 	and.b w1,#1,w4
 590              	.loc 1 117 0
 591 0002be  80 80 FB 	ze w0,w1
 592              	.loc 1 118 0
 593 0002c0  04 C0 EA 	com.b w4,w0
 594              	.loc 1 117 0
 595 0002c2  E1 80 60 	and w1,#1,w1
 596 0002c4  C6 08 DD 	sl w1,#6,w1
 597              	.loc 1 118 0
 598 0002c6  61 40 60 	and.b w0,#1,w0
 599              	.loc 1 117 0
 600 0002c8  82 80 70 	ior w1,w2,w1
 601              	.loc 1 118 0
 602 0002ca  00 80 FB 	ze w0,w0
 603              	.loc 1 117 0
 604 0002cc  01 00 88 	mov w1,_SPI2CON1bits
 605              	.loc 1 118 0
 606 0002ce  E1 00 60 	and w0,#1,w1
 607 0002d0  00 00 80 	mov _SPI2CON1bits,w0
 608 0002d2  C8 08 DD 	sl w1,#8,w1
 609 0002d4  00 01 78 	mov w0,w2
 610 0002d6  02 80 A1 	bclr w2,#8
 611              	.loc 1 119 0
 612 0002d8  45 18 DE 	lsr w3,#5,w0
 613              	.loc 1 118 0
 614 0002da  82 80 70 	ior w1,w2,w1
MPLAB XC16 ASSEMBLY Listing:   			page 14


 615              	.loc 1 119 0
 616 0002dc  61 40 60 	and.b w0,#1,w0
 617              	.loc 1 118 0
 618 0002de  01 00 88 	mov w1,_SPI2CON1bits
 619              	.loc 1 119 0
 620 0002e0  00 80 FB 	ze w0,w0
 621 0002e2  01 00 80 	mov _SPI2CON1bits,w1
 622 0002e4  61 00 60 	and w0,#1,w0
 623 0002e6  81 01 78 	mov w1,w3
 624 0002e8  03 90 A1 	bclr w3,#9
 625 0002ea  49 01 DD 	sl w0,#9,w2
 120:lib/lib_pic33e/SPI.c **** 
 121:lib/lib_pic33e/SPI.c ****     /* Chose baudrate prescalers */
 122:lib/lib_pic33e/SPI.c ****     baudrate = get_prescales(config.baudrate, &p_presc, &s_presc);
 626              	.loc 1 122 0
 627 0002ec  4E 00 90 	mov [w14+8],w0
 628 0002ee  DE 00 90 	mov [w14+10],w1
 629              	.loc 1 119 0
 630 0002f0  03 01 71 	ior w2,w3,w2
 631              	.loc 1 122 0
 632 0002f2  E5 01 47 	add w14,#5,w3
 633              	.loc 1 119 0
 634 0002f4  02 00 88 	mov w2,_SPI2CON1bits
 635              	.loc 1 122 0
 636 0002f6  64 01 47 	add w14,#4,w2
 637 0002f8  00 00 07 	rcall _get_prescales
 638 0002fa  00 8F BE 	mov.d w0,[w14]
 123:lib/lib_pic33e/SPI.c ****     SPI2CON1bits.PPRE = p_presc;
 639              	.loc 1 123 0
 640 0002fc  02 00 80 	mov _SPI2CON1bits,w2
 641 0002fe  4E 40 90 	mov.b [w14+4],w0
 642 000300  C1 FF 2F 	mov #-4,w1
 643 000302  63 40 60 	and.b w0,#3,w0
 644 000304  01 01 61 	and w2,w1,w2
 645 000306  80 80 FB 	ze w0,w1
 124:lib/lib_pic33e/SPI.c ****     SPI2CON1bits.SPRE = s_presc;
 646              	.loc 1 124 0
 647 000308  5E 40 90 	mov.b [w14+5],w0
 648              	.loc 1 123 0
 649 00030a  E3 80 60 	and w1,#3,w1
 650              	.loc 1 124 0
 651 00030c  67 40 60 	and.b w0,#7,w0
 652              	.loc 1 123 0
 653 00030e  82 80 70 	ior w1,w2,w1
 654              	.loc 1 124 0
 655 000310  00 80 FB 	ze w0,w0
 656              	.loc 1 123 0
 657 000312  01 00 88 	mov w1,_SPI2CON1bits
 658              	.loc 1 124 0
 659 000314  67 00 60 	and w0,#7,w0
 660 000316  02 00 80 	mov _SPI2CON1bits,w2
 661 000318  C2 00 DD 	sl w0,#2,w1
 662 00031a  30 FE 2F 	mov #-29,w0
 125:lib/lib_pic33e/SPI.c **** 
 126:lib/lib_pic33e/SPI.c ****     /* SPI2CON2 */
 127:lib/lib_pic33e/SPI.c ****     SPI2CON2bits.FRMEN = 0;  // Disable all framed SPI support
 128:lib/lib_pic33e/SPI.c ****     SPI2CON2bits.SPIBEN = 1; // Enhanced mode is enabled
MPLAB XC16 ASSEMBLY Listing:   			page 15


 129:lib/lib_pic33e/SPI.c **** 
 130:lib/lib_pic33e/SPI.c ****     /* SPI2STAT */
 131:lib/lib_pic33e/SPI.c ****     SPI2STATbits.SPISIDL = 0; // continue operation in Idle
 132:lib/lib_pic33e/SPI.c ****     SPI2STATbits.SPIROV = 0; // Clear overflow
 133:lib/lib_pic33e/SPI.c ****     SPI2STATbits.SISEL = 0b011; // Set interrupt to SPI receive buffer full is set (Not used)
 663              	.loc 1 133 0
 664 00031c  33 FE 2F 	mov #-29,w3
 665              	.loc 1 124 0
 666 00031e  00 01 61 	and w2,w0,w2
 134:lib/lib_pic33e/SPI.c **** 
 135:lib/lib_pic33e/SPI.c ****     /* Configuration is finished. Enable the module */
 136:lib/lib_pic33e/SPI.c ****     SPI2STATbits.SPIEN = config.enabled;
 667              	.loc 1 136 0
 668 000320  3E 00 90 	mov [w14+6],w0
 669              	.loc 1 124 0
 670 000322  82 80 70 	ior w1,w2,w1
 671              	.loc 1 136 0
 672 000324  61 40 60 	and.b w0,#1,w0
 673              	.loc 1 124 0
 674 000326  01 00 88 	mov w1,_SPI2CON1bits
 675              	.loc 1 136 0
 676 000328  00 80 FB 	ze w0,w0
 677              	.loc 1 127 0
 678 00032a  01 E0 A9 	bclr.b _SPI2CON2bits+1,#7
 679              	.loc 1 136 0
 680 00032c  4F 01 DD 	sl w0,#15,w2
 681              	.loc 1 128 0
 682 00032e  00 00 A8 	bset.b _SPI2CON2bits,#0
 137:lib/lib_pic33e/SPI.c ****     return baudrate;
 683              	.loc 1 137 0
 684 000330  1E 00 78 	mov [w14],w0
 685 000332  9E 00 90 	mov [w14+2],w1
 686              	.loc 1 131 0
 687 000334  01 A0 A9 	bclr.b _SPI2STATbits+1,#5
 688              	.loc 1 132 0
 689 000336  00 C0 A9 	bclr.b _SPI2STATbits,#6
 690              	.loc 1 133 0
 691 000338  04 00 80 	mov _SPI2STATbits,w4
 692 00033a  83 01 62 	and w4,w3,w3
 693 00033c  C3 00 B3 	ior #12,w3
 694 00033e  03 00 88 	mov w3,_SPI2STATbits
 695              	.loc 1 136 0
 696 000340  03 00 80 	mov _SPI2STATbits,w3
 697 000342  03 F0 A1 	bclr w3,#15
 698 000344  03 01 71 	ior w2,w3,w2
 699 000346  02 00 88 	mov w2,_SPI2STATbits
 138:lib/lib_pic33e/SPI.c **** }
 700              	.loc 1 138 0
 701 000348  4F 04 78 	mov [--w15],w8
 702 00034a  8E 07 78 	mov w14,w15
 703 00034c  4F 07 78 	mov [--w15],w14
 704 00034e  00 40 A9 	bclr CORCON,#2
 705 000350  00 00 06 	return 
 706              	.set ___PA___,0
 707              	.LFE2:
 708              	.size _config_SPI2,.-_config_SPI2
 709              	.align 2
MPLAB XC16 ASSEMBLY Listing:   			page 16


 710              	.global _config_SPI3
 711              	.type _config_SPI3,@function
 712              	_config_SPI3:
 713              	.LFB3:
 139:lib/lib_pic33e/SPI.c **** 
 140:lib/lib_pic33e/SPI.c **** /**********************************************************************
 141:lib/lib_pic33e/SPI.c ****  * Name:    config_SPI3
 142:lib/lib_pic33e/SPI.c ****  * Args:    SPI_Peripheral_Configuration config - configuration struct
 143:lib/lib_pic33e/SPI.c ****  * Return:  best baudrate you can get for the defined FCY
 144:lib/lib_pic33e/SPI.c ****  * Desc:    config SPI module
 145:lib/lib_pic33e/SPI.c ****  **********************************************************************/
 146:lib/lib_pic33e/SPI.c **** uint32_t config_SPI3(SPI_Peripheral_Configuration config){
 714              	.loc 1 146 0
 715              	.set ___PA___,1
 716 000352  0C 00 FA 	lnk #12
 717              	.LCFI7:
 718 000354  88 1F 78 	mov w8,[w15++]
 719              	.LCFI8:
 720              	.loc 1 146 0
 721 000356  30 07 98 	mov w0,[w14+6]
 147:lib/lib_pic33e/SPI.c ****     
 148:lib/lib_pic33e/SPI.c ****     uint32_t baudrate;
 149:lib/lib_pic33e/SPI.c ****     uint8_t p_presc;
 150:lib/lib_pic33e/SPI.c ****     uint8_t s_presc;
 151:lib/lib_pic33e/SPI.c **** 
 152:lib/lib_pic33e/SPI.c ****     SPI1STATbits.SPIEN = 0; //Assure module is disabled
 722              	.loc 1 152 0
 723 000358  01 E0 A9 	bclr.b _SPI1STATbits+1,#7
 724              	.loc 1 146 0
 725 00035a  41 07 98 	mov w1,[w14+8]
 153:lib/lib_pic33e/SPI.c **** 
 154:lib/lib_pic33e/SPI.c ****     /* SPI3CON1 */
 155:lib/lib_pic33e/SPI.c ****     SPI3CON1bits.DISSCK = config.disable_SCK_pin;
 726              	.loc 1 155 0
 727 00035c  08 00 80 	mov _SPI3CON1bits,w8
 728              	.loc 1 146 0
 729 00035e  52 07 98 	mov w2,[w14+10]
 730              	.loc 1 155 0
 731 000360  BE 03 90 	mov [w14+6],w7
 156:lib/lib_pic33e/SPI.c ****     SPI3CON1bits.DISSDO = config.disable_SDO_pin;
 732              	.loc 1 156 0
 733 000362  3E 03 90 	mov [w14+6],w6
 157:lib/lib_pic33e/SPI.c ****     SPI3CON1bits.MODE16 = config.transfer_mode;
 734              	.loc 1 157 0
 735 000364  BE 02 90 	mov [w14+6],w5
 158:lib/lib_pic33e/SPI.c ****     SPI3CON1bits.SSEN = config.enable_SS_pin;
 736              	.loc 1 158 0
 737 000366  3E 02 90 	mov [w14+6],w4
 159:lib/lib_pic33e/SPI.c ****     SPI3CON1bits.MSTEN = config.enable_master_mode;
 738              	.loc 1 159 0
 739 000368  3E 01 90 	mov [w14+6],w2
 160:lib/lib_pic33e/SPI.c ****     /* Timing configuration */
 161:lib/lib_pic33e/SPI.c ****     SPI3CON1bits.CKP = ((config.SPI_mode >> 1) & 0b1);
 740              	.loc 1 161 0
 741 00036a  3E 00 90 	mov [w14+6],w0
 162:lib/lib_pic33e/SPI.c ****     SPI3CON1bits.CKE = (~(config.SPI_mode & 0b1));
 742              	.loc 1 162 0
MPLAB XC16 ASSEMBLY Listing:   			page 17


 743 00036c  BE 00 90 	mov [w14+6],w1
 163:lib/lib_pic33e/SPI.c ****     SPI3CON1bits.SMP = config.data_sample_phase;
 744              	.loc 1 163 0
 745 00036e  BE 01 90 	mov [w14+6],w3
 746              	.loc 1 155 0
 747 000370  C2 3B DE 	lsr w7,#2,w7
 748 000372  08 C0 A1 	bclr w8,#12
 749 000374  E1 C3 63 	and.b w7,#1,w7
 750              	.loc 1 156 0
 751 000376  43 33 DE 	lsr w6,#3,w6
 752              	.loc 1 155 0
 753 000378  87 83 FB 	ze w7,w7
 754              	.loc 1 156 0
 755 00037a  61 43 63 	and.b w6,#1,w6
 756              	.loc 1 155 0
 757 00037c  E1 83 63 	and w7,#1,w7
 758              	.loc 1 156 0
 759 00037e  06 83 FB 	ze w6,w6
 760              	.loc 1 155 0
 761 000380  CC 3B DD 	sl w7,#12,w7
 762              	.loc 1 156 0
 763 000382  61 03 63 	and w6,#1,w6
 764              	.loc 1 155 0
 765 000384  88 83 73 	ior w7,w8,w7
 766              	.loc 1 156 0
 767 000386  4B 33 DD 	sl w6,#11,w6
 768              	.loc 1 155 0
 769 000388  07 00 88 	mov w7,_SPI3CON1bits
 770              	.loc 1 157 0
 771 00038a  C4 2A DE 	lsr w5,#4,w5
 772              	.loc 1 156 0
 773 00038c  07 00 80 	mov _SPI3CON1bits,w7
 774              	.loc 1 157 0
 775 00038e  E1 C2 62 	and.b w5,#1,w5
 776              	.loc 1 156 0
 777 000390  07 B0 A1 	bclr w7,#11
 778              	.loc 1 157 0
 779 000392  85 82 FB 	ze w5,w5
 780              	.loc 1 156 0
 781 000394  07 03 73 	ior w6,w7,w6
 782              	.loc 1 157 0
 783 000396  E1 82 62 	and w5,#1,w5
 784              	.loc 1 156 0
 785 000398  06 00 88 	mov w6,_SPI3CON1bits
 786              	.loc 1 157 0
 787 00039a  CA 2A DD 	sl w5,#10,w5
 788 00039c  06 00 80 	mov _SPI3CON1bits,w6
 789              	.loc 1 158 0
 790 00039e  48 22 DE 	lsr w4,#8,w4
 791              	.loc 1 157 0
 792 0003a0  06 A0 A1 	bclr w6,#10
 793              	.loc 1 158 0
 794 0003a2  61 42 62 	and.b w4,#1,w4
 795              	.loc 1 157 0
 796 0003a4  86 82 72 	ior w5,w6,w5
 797              	.loc 1 158 0
 798 0003a6  04 82 FB 	ze w4,w4
MPLAB XC16 ASSEMBLY Listing:   			page 18


 799              	.loc 1 157 0
 800 0003a8  05 00 88 	mov w5,_SPI3CON1bits
 801              	.loc 1 158 0
 802 0003aa  61 02 62 	and w4,#1,w4
 803 0003ac  05 00 80 	mov _SPI3CON1bits,w5
 804 0003ae  47 22 DD 	sl w4,#7,w4
 805 0003b0  05 70 A1 	bclr w5,#7
 806              	.loc 1 159 0
 807 0003b2  49 11 DE 	lsr w2,#9,w2
 808              	.loc 1 158 0
 809 0003b4  05 02 72 	ior w4,w5,w4
 810              	.loc 1 159 0
 811 0003b6  61 41 61 	and.b w2,#1,w2
 812              	.loc 1 158 0
 813 0003b8  04 00 88 	mov w4,_SPI3CON1bits
 814              	.loc 1 159 0
 815 0003ba  02 81 FB 	ze w2,w2
 816 0003bc  04 00 80 	mov _SPI3CON1bits,w4
 817 0003be  61 01 61 	and w2,#1,w2
 818 0003c0  04 50 A1 	bclr w4,#5
 819 0003c2  45 11 DD 	sl w2,#5,w2
 820              	.loc 1 161 0
 821 0003c4  46 00 DE 	lsr w0,#6,w0
 822              	.loc 1 159 0
 823 0003c6  04 01 71 	ior w2,w4,w2
 824              	.loc 1 161 0
 825 0003c8  63 40 60 	and.b w0,#3,w0
 826              	.loc 1 159 0
 827 0003ca  02 00 88 	mov w2,_SPI3CON1bits
 828              	.loc 1 161 0
 829 0003cc  00 80 FB 	ze w0,w0
 830 0003ce  02 00 80 	mov _SPI3CON1bits,w2
 831 0003d0  00 80 D1 	asr w0,w0
 832 0003d2  02 60 A1 	bclr w2,#6
 833 0003d4  00 40 78 	mov.b w0,w0
 834              	.loc 1 162 0
 835 0003d6  C6 08 DE 	lsr w1,#6,w1
 836              	.loc 1 161 0
 837 0003d8  61 40 60 	and.b w0,#1,w0
 838              	.loc 1 162 0
 839 0003da  E3 C0 60 	and.b w1,#3,w1
 840              	.loc 1 161 0
 841 0003dc  61 40 60 	and.b w0,#1,w0
 842 0003de  61 40 60 	and.b w0,#1,w0
 843              	.loc 1 162 0
 844 0003e0  61 C2 60 	and.b w1,#1,w4
 845              	.loc 1 161 0
 846 0003e2  80 80 FB 	ze w0,w1
 847              	.loc 1 162 0
 848 0003e4  04 C0 EA 	com.b w4,w0
 849              	.loc 1 161 0
 850 0003e6  E1 80 60 	and w1,#1,w1
 851 0003e8  C6 08 DD 	sl w1,#6,w1
 852              	.loc 1 162 0
 853 0003ea  61 40 60 	and.b w0,#1,w0
 854              	.loc 1 161 0
 855 0003ec  82 80 70 	ior w1,w2,w1
MPLAB XC16 ASSEMBLY Listing:   			page 19


 856              	.loc 1 162 0
 857 0003ee  00 80 FB 	ze w0,w0
 858              	.loc 1 161 0
 859 0003f0  01 00 88 	mov w1,_SPI3CON1bits
 860              	.loc 1 162 0
 861 0003f2  E1 00 60 	and w0,#1,w1
 862 0003f4  00 00 80 	mov _SPI3CON1bits,w0
 863 0003f6  C8 08 DD 	sl w1,#8,w1
 864 0003f8  00 01 78 	mov w0,w2
 865 0003fa  02 80 A1 	bclr w2,#8
 866              	.loc 1 163 0
 867 0003fc  45 18 DE 	lsr w3,#5,w0
 868              	.loc 1 162 0
 869 0003fe  82 80 70 	ior w1,w2,w1
 870              	.loc 1 163 0
 871 000400  61 40 60 	and.b w0,#1,w0
 872              	.loc 1 162 0
 873 000402  01 00 88 	mov w1,_SPI3CON1bits
 874              	.loc 1 163 0
 875 000404  00 80 FB 	ze w0,w0
 876 000406  01 00 80 	mov _SPI3CON1bits,w1
 877 000408  61 00 60 	and w0,#1,w0
 878 00040a  81 01 78 	mov w1,w3
 879 00040c  03 90 A1 	bclr w3,#9
 880 00040e  49 01 DD 	sl w0,#9,w2
 164:lib/lib_pic33e/SPI.c **** 
 165:lib/lib_pic33e/SPI.c ****     /* Chose baudrate prescalers */
 166:lib/lib_pic33e/SPI.c ****     baudrate = get_prescales(config.baudrate, &p_presc, &s_presc);
 881              	.loc 1 166 0
 882 000410  4E 00 90 	mov [w14+8],w0
 883 000412  DE 00 90 	mov [w14+10],w1
 884              	.loc 1 163 0
 885 000414  03 01 71 	ior w2,w3,w2
 886              	.loc 1 166 0
 887 000416  E5 01 47 	add w14,#5,w3
 888              	.loc 1 163 0
 889 000418  02 00 88 	mov w2,_SPI3CON1bits
 890              	.loc 1 166 0
 891 00041a  64 01 47 	add w14,#4,w2
 892 00041c  00 00 07 	rcall _get_prescales
 893 00041e  00 8F BE 	mov.d w0,[w14]
 167:lib/lib_pic33e/SPI.c ****     SPI3CON1bits.PPRE = p_presc;
 894              	.loc 1 167 0
 895 000420  02 00 80 	mov _SPI3CON1bits,w2
 896 000422  4E 40 90 	mov.b [w14+4],w0
 897 000424  C1 FF 2F 	mov #-4,w1
 898 000426  63 40 60 	and.b w0,#3,w0
 899 000428  01 01 61 	and w2,w1,w2
 900 00042a  80 80 FB 	ze w0,w1
 168:lib/lib_pic33e/SPI.c ****     SPI3CON1bits.SPRE = s_presc;
 901              	.loc 1 168 0
 902 00042c  5E 40 90 	mov.b [w14+5],w0
 903              	.loc 1 167 0
 904 00042e  E3 80 60 	and w1,#3,w1
 905              	.loc 1 168 0
 906 000430  67 40 60 	and.b w0,#7,w0
 907              	.loc 1 167 0
MPLAB XC16 ASSEMBLY Listing:   			page 20


 908 000432  82 80 70 	ior w1,w2,w1
 909              	.loc 1 168 0
 910 000434  00 80 FB 	ze w0,w0
 911              	.loc 1 167 0
 912 000436  01 00 88 	mov w1,_SPI3CON1bits
 913              	.loc 1 168 0
 914 000438  67 00 60 	and w0,#7,w0
 915 00043a  02 00 80 	mov _SPI3CON1bits,w2
 916 00043c  C2 00 DD 	sl w0,#2,w1
 917 00043e  30 FE 2F 	mov #-29,w0
 169:lib/lib_pic33e/SPI.c **** 
 170:lib/lib_pic33e/SPI.c ****     /* SPI3CON2 */
 171:lib/lib_pic33e/SPI.c ****     SPI3CON2bits.FRMEN = 0;  // Disable all framed SPI support
 172:lib/lib_pic33e/SPI.c ****     SPI3CON2bits.SPIBEN = 1; // Enhanced mode is enabled
 173:lib/lib_pic33e/SPI.c **** 
 174:lib/lib_pic33e/SPI.c ****     /* SPI3STAT */
 175:lib/lib_pic33e/SPI.c ****     SPI3STATbits.SPISIDL = 0; // continue operation in Idle
 176:lib/lib_pic33e/SPI.c ****     SPI3STATbits.SPIROV = 0; // Clear overflow
 177:lib/lib_pic33e/SPI.c ****     SPI3STATbits.SISEL = 0b011; // Set interrupt to SPI receive buffer full is set (Not used)
 918              	.loc 1 177 0
 919 000440  33 FE 2F 	mov #-29,w3
 920              	.loc 1 168 0
 921 000442  00 01 61 	and w2,w0,w2
 178:lib/lib_pic33e/SPI.c **** 
 179:lib/lib_pic33e/SPI.c ****     /* Configuration is finished. Enable the module */
 180:lib/lib_pic33e/SPI.c ****     SPI3STATbits.SPIEN = config.enabled;
 922              	.loc 1 180 0
 923 000444  3E 00 90 	mov [w14+6],w0
 924              	.loc 1 168 0
 925 000446  82 80 70 	ior w1,w2,w1
 926              	.loc 1 180 0
 927 000448  61 40 60 	and.b w0,#1,w0
 928              	.loc 1 168 0
 929 00044a  01 00 88 	mov w1,_SPI3CON1bits
 930              	.loc 1 180 0
 931 00044c  00 80 FB 	ze w0,w0
 932              	.loc 1 171 0
 933 00044e  01 E0 A9 	bclr.b _SPI3CON2bits+1,#7
 934              	.loc 1 180 0
 935 000450  4F 01 DD 	sl w0,#15,w2
 936              	.loc 1 172 0
 937 000452  00 00 A8 	bset.b _SPI3CON2bits,#0
 181:lib/lib_pic33e/SPI.c ****     return baudrate;
 938              	.loc 1 181 0
 939 000454  1E 00 78 	mov [w14],w0
 940 000456  9E 00 90 	mov [w14+2],w1
 941              	.loc 1 175 0
 942 000458  01 A0 A9 	bclr.b _SPI3STATbits+1,#5
 943              	.loc 1 176 0
 944 00045a  00 C0 A9 	bclr.b _SPI3STATbits,#6
 945              	.loc 1 177 0
 946 00045c  04 00 80 	mov _SPI3STATbits,w4
 947 00045e  83 01 62 	and w4,w3,w3
 948 000460  C3 00 B3 	ior #12,w3
 949 000462  03 00 88 	mov w3,_SPI3STATbits
 950              	.loc 1 180 0
 951 000464  03 00 80 	mov _SPI3STATbits,w3
MPLAB XC16 ASSEMBLY Listing:   			page 21


 952 000466  03 F0 A1 	bclr w3,#15
 953 000468  03 01 71 	ior w2,w3,w2
 954 00046a  02 00 88 	mov w2,_SPI3STATbits
 182:lib/lib_pic33e/SPI.c **** }
 955              	.loc 1 182 0
 956 00046c  4F 04 78 	mov [--w15],w8
 957 00046e  8E 07 78 	mov w14,w15
 958 000470  4F 07 78 	mov [--w15],w14
 959 000472  00 40 A9 	bclr CORCON,#2
 960 000474  00 00 06 	return 
 961              	.set ___PA___,0
 962              	.LFE3:
 963              	.size _config_SPI3,.-_config_SPI3
 964              	.align 2
 965              	.global _config_SPI4
 966              	.type _config_SPI4,@function
 967              	_config_SPI4:
 968              	.LFB4:
 183:lib/lib_pic33e/SPI.c **** 
 184:lib/lib_pic33e/SPI.c **** /**********************************************************************
 185:lib/lib_pic33e/SPI.c ****  * Name:    config_SPI4
 186:lib/lib_pic33e/SPI.c ****  * Args:    SPI_Peripheral_Configuration config - configuration struct
 187:lib/lib_pic33e/SPI.c ****  * Return:  best baudrate you can get for the defined FCY
 188:lib/lib_pic33e/SPI.c ****  * Desc:    config SPI module
 189:lib/lib_pic33e/SPI.c ****  **********************************************************************/
 190:lib/lib_pic33e/SPI.c **** uint32_t config_SPI4(SPI_Peripheral_Configuration config){
 969              	.loc 1 190 0
 970              	.set ___PA___,1
 971 000476  0C 00 FA 	lnk #12
 972              	.LCFI9:
 973 000478  88 1F 78 	mov w8,[w15++]
 974              	.LCFI10:
 975              	.loc 1 190 0
 976 00047a  30 07 98 	mov w0,[w14+6]
 191:lib/lib_pic33e/SPI.c ****     
 192:lib/lib_pic33e/SPI.c ****     uint32_t baudrate;
 193:lib/lib_pic33e/SPI.c ****     uint8_t p_presc;
 194:lib/lib_pic33e/SPI.c ****     uint8_t s_presc;
 195:lib/lib_pic33e/SPI.c **** 
 196:lib/lib_pic33e/SPI.c ****     SPI4STATbits.SPIEN = 0; //Assure module is disabled
 977              	.loc 1 196 0
 978 00047c  01 E0 A9 	bclr.b _SPI4STATbits+1,#7
 979              	.loc 1 190 0
 980 00047e  41 07 98 	mov w1,[w14+8]
 197:lib/lib_pic33e/SPI.c **** 
 198:lib/lib_pic33e/SPI.c ****     /* SPI3CON1 */
 199:lib/lib_pic33e/SPI.c ****     SPI4CON1bits.DISSCK = config.disable_SCK_pin;
 981              	.loc 1 199 0
 982 000480  08 00 80 	mov _SPI4CON1bits,w8
 983              	.loc 1 190 0
 984 000482  52 07 98 	mov w2,[w14+10]
 985              	.loc 1 199 0
 986 000484  BE 03 90 	mov [w14+6],w7
 200:lib/lib_pic33e/SPI.c ****     SPI4CON1bits.DISSDO = config.disable_SDO_pin;
 987              	.loc 1 200 0
 988 000486  3E 03 90 	mov [w14+6],w6
 201:lib/lib_pic33e/SPI.c ****     SPI4CON1bits.MODE16 = config.transfer_mode;
MPLAB XC16 ASSEMBLY Listing:   			page 22


 989              	.loc 1 201 0
 990 000488  BE 02 90 	mov [w14+6],w5
 202:lib/lib_pic33e/SPI.c ****     SPI4CON1bits.SSEN = config.enable_SS_pin;
 991              	.loc 1 202 0
 992 00048a  3E 02 90 	mov [w14+6],w4
 203:lib/lib_pic33e/SPI.c ****     SPI4CON1bits.MSTEN = config.enable_master_mode;
 993              	.loc 1 203 0
 994 00048c  3E 01 90 	mov [w14+6],w2
 204:lib/lib_pic33e/SPI.c ****     /* Timing configuration */
 205:lib/lib_pic33e/SPI.c ****     SPI4CON1bits.CKP = ((config.SPI_mode >> 1) & 0b1);
 995              	.loc 1 205 0
 996 00048e  3E 00 90 	mov [w14+6],w0
 206:lib/lib_pic33e/SPI.c ****     SPI4CON1bits.CKE = (~(config.SPI_mode & 0b1));
 997              	.loc 1 206 0
 998 000490  BE 00 90 	mov [w14+6],w1
 207:lib/lib_pic33e/SPI.c ****     SPI4CON1bits.SMP = config.data_sample_phase;
 999              	.loc 1 207 0
 1000 000492  BE 01 90 	mov [w14+6],w3
 1001              	.loc 1 199 0
 1002 000494  C2 3B DE 	lsr w7,#2,w7
 1003 000496  08 C0 A1 	bclr w8,#12
 1004 000498  E1 C3 63 	and.b w7,#1,w7
 1005              	.loc 1 200 0
 1006 00049a  43 33 DE 	lsr w6,#3,w6
 1007              	.loc 1 199 0
 1008 00049c  87 83 FB 	ze w7,w7
 1009              	.loc 1 200 0
 1010 00049e  61 43 63 	and.b w6,#1,w6
 1011              	.loc 1 199 0
 1012 0004a0  E1 83 63 	and w7,#1,w7
 1013              	.loc 1 200 0
 1014 0004a2  06 83 FB 	ze w6,w6
 1015              	.loc 1 199 0
 1016 0004a4  CC 3B DD 	sl w7,#12,w7
 1017              	.loc 1 200 0
 1018 0004a6  61 03 63 	and w6,#1,w6
 1019              	.loc 1 199 0
 1020 0004a8  88 83 73 	ior w7,w8,w7
 1021              	.loc 1 200 0
 1022 0004aa  4B 33 DD 	sl w6,#11,w6
 1023              	.loc 1 199 0
 1024 0004ac  07 00 88 	mov w7,_SPI4CON1bits
 1025              	.loc 1 201 0
 1026 0004ae  C4 2A DE 	lsr w5,#4,w5
 1027              	.loc 1 200 0
 1028 0004b0  07 00 80 	mov _SPI4CON1bits,w7
 1029              	.loc 1 201 0
 1030 0004b2  E1 C2 62 	and.b w5,#1,w5
 1031              	.loc 1 200 0
 1032 0004b4  07 B0 A1 	bclr w7,#11
 1033              	.loc 1 201 0
 1034 0004b6  85 82 FB 	ze w5,w5
 1035              	.loc 1 200 0
 1036 0004b8  07 03 73 	ior w6,w7,w6
 1037              	.loc 1 201 0
 1038 0004ba  E1 82 62 	and w5,#1,w5
 1039              	.loc 1 200 0
MPLAB XC16 ASSEMBLY Listing:   			page 23


 1040 0004bc  06 00 88 	mov w6,_SPI4CON1bits
 1041              	.loc 1 201 0
 1042 0004be  CA 2A DD 	sl w5,#10,w5
 1043 0004c0  06 00 80 	mov _SPI4CON1bits,w6
 1044              	.loc 1 202 0
 1045 0004c2  48 22 DE 	lsr w4,#8,w4
 1046              	.loc 1 201 0
 1047 0004c4  06 A0 A1 	bclr w6,#10
 1048              	.loc 1 202 0
 1049 0004c6  61 42 62 	and.b w4,#1,w4
 1050              	.loc 1 201 0
 1051 0004c8  86 82 72 	ior w5,w6,w5
 1052              	.loc 1 202 0
 1053 0004ca  04 82 FB 	ze w4,w4
 1054              	.loc 1 201 0
 1055 0004cc  05 00 88 	mov w5,_SPI4CON1bits
 1056              	.loc 1 202 0
 1057 0004ce  61 02 62 	and w4,#1,w4
 1058 0004d0  05 00 80 	mov _SPI4CON1bits,w5
 1059 0004d2  47 22 DD 	sl w4,#7,w4
 1060 0004d4  05 70 A1 	bclr w5,#7
 1061              	.loc 1 203 0
 1062 0004d6  49 11 DE 	lsr w2,#9,w2
 1063              	.loc 1 202 0
 1064 0004d8  05 02 72 	ior w4,w5,w4
 1065              	.loc 1 203 0
 1066 0004da  61 41 61 	and.b w2,#1,w2
 1067              	.loc 1 202 0
 1068 0004dc  04 00 88 	mov w4,_SPI4CON1bits
 1069              	.loc 1 203 0
 1070 0004de  02 81 FB 	ze w2,w2
 1071 0004e0  04 00 80 	mov _SPI4CON1bits,w4
 1072 0004e2  61 01 61 	and w2,#1,w2
 1073 0004e4  04 50 A1 	bclr w4,#5
 1074 0004e6  45 11 DD 	sl w2,#5,w2
 1075              	.loc 1 205 0
 1076 0004e8  46 00 DE 	lsr w0,#6,w0
 1077              	.loc 1 203 0
 1078 0004ea  04 01 71 	ior w2,w4,w2
 1079              	.loc 1 205 0
 1080 0004ec  63 40 60 	and.b w0,#3,w0
 1081              	.loc 1 203 0
 1082 0004ee  02 00 88 	mov w2,_SPI4CON1bits
 1083              	.loc 1 205 0
 1084 0004f0  00 80 FB 	ze w0,w0
 1085 0004f2  02 00 80 	mov _SPI4CON1bits,w2
 1086 0004f4  00 80 D1 	asr w0,w0
 1087 0004f6  02 60 A1 	bclr w2,#6
 1088 0004f8  00 40 78 	mov.b w0,w0
 1089              	.loc 1 206 0
 1090 0004fa  C6 08 DE 	lsr w1,#6,w1
 1091              	.loc 1 205 0
 1092 0004fc  61 40 60 	and.b w0,#1,w0
 1093              	.loc 1 206 0
 1094 0004fe  E3 C0 60 	and.b w1,#3,w1
 1095              	.loc 1 205 0
 1096 000500  61 40 60 	and.b w0,#1,w0
MPLAB XC16 ASSEMBLY Listing:   			page 24


 1097 000502  61 40 60 	and.b w0,#1,w0
 1098              	.loc 1 206 0
 1099 000504  61 C2 60 	and.b w1,#1,w4
 1100              	.loc 1 205 0
 1101 000506  80 80 FB 	ze w0,w1
 1102              	.loc 1 206 0
 1103 000508  04 C0 EA 	com.b w4,w0
 1104              	.loc 1 205 0
 1105 00050a  E1 80 60 	and w1,#1,w1
 1106 00050c  C6 08 DD 	sl w1,#6,w1
 1107              	.loc 1 206 0
 1108 00050e  61 40 60 	and.b w0,#1,w0
 1109              	.loc 1 205 0
 1110 000510  82 80 70 	ior w1,w2,w1
 1111              	.loc 1 206 0
 1112 000512  00 80 FB 	ze w0,w0
 1113              	.loc 1 205 0
 1114 000514  01 00 88 	mov w1,_SPI4CON1bits
 1115              	.loc 1 206 0
 1116 000516  E1 00 60 	and w0,#1,w1
 1117 000518  00 00 80 	mov _SPI4CON1bits,w0
 1118 00051a  C8 08 DD 	sl w1,#8,w1
 1119 00051c  00 01 78 	mov w0,w2
 1120 00051e  02 80 A1 	bclr w2,#8
 1121              	.loc 1 207 0
 1122 000520  45 18 DE 	lsr w3,#5,w0
 1123              	.loc 1 206 0
 1124 000522  82 80 70 	ior w1,w2,w1
 1125              	.loc 1 207 0
 1126 000524  61 40 60 	and.b w0,#1,w0
 1127              	.loc 1 206 0
 1128 000526  01 00 88 	mov w1,_SPI4CON1bits
 1129              	.loc 1 207 0
 1130 000528  00 80 FB 	ze w0,w0
 1131 00052a  01 00 80 	mov _SPI4CON1bits,w1
 1132 00052c  61 00 60 	and w0,#1,w0
 1133 00052e  81 01 78 	mov w1,w3
 1134 000530  03 90 A1 	bclr w3,#9
 1135 000532  49 01 DD 	sl w0,#9,w2
 208:lib/lib_pic33e/SPI.c **** 
 209:lib/lib_pic33e/SPI.c ****     /* Chose baudrate prescalers */
 210:lib/lib_pic33e/SPI.c ****     baudrate = get_prescales(config.baudrate, &p_presc, &s_presc);
 1136              	.loc 1 210 0
 1137 000534  4E 00 90 	mov [w14+8],w0
 1138 000536  DE 00 90 	mov [w14+10],w1
 1139              	.loc 1 207 0
 1140 000538  03 01 71 	ior w2,w3,w2
 1141              	.loc 1 210 0
 1142 00053a  E5 01 47 	add w14,#5,w3
 1143              	.loc 1 207 0
 1144 00053c  02 00 88 	mov w2,_SPI4CON1bits
 1145              	.loc 1 210 0
 1146 00053e  64 01 47 	add w14,#4,w2
 1147 000540  00 00 07 	rcall _get_prescales
 1148 000542  00 8F BE 	mov.d w0,[w14]
 211:lib/lib_pic33e/SPI.c ****     SPI4CON1bits.PPRE = p_presc;
 1149              	.loc 1 211 0
MPLAB XC16 ASSEMBLY Listing:   			page 25


 1150 000544  02 00 80 	mov _SPI4CON1bits,w2
 1151 000546  4E 40 90 	mov.b [w14+4],w0
 1152 000548  C1 FF 2F 	mov #-4,w1
 1153 00054a  63 40 60 	and.b w0,#3,w0
 1154 00054c  01 01 61 	and w2,w1,w2
 1155 00054e  80 80 FB 	ze w0,w1
 212:lib/lib_pic33e/SPI.c ****     SPI4CON1bits.SPRE = s_presc;
 1156              	.loc 1 212 0
 1157 000550  5E 40 90 	mov.b [w14+5],w0
 1158              	.loc 1 211 0
 1159 000552  E3 80 60 	and w1,#3,w1
 1160              	.loc 1 212 0
 1161 000554  67 40 60 	and.b w0,#7,w0
 1162              	.loc 1 211 0
 1163 000556  82 80 70 	ior w1,w2,w1
 1164              	.loc 1 212 0
 1165 000558  00 80 FB 	ze w0,w0
 1166              	.loc 1 211 0
 1167 00055a  01 00 88 	mov w1,_SPI4CON1bits
 1168              	.loc 1 212 0
 1169 00055c  67 00 60 	and w0,#7,w0
 1170 00055e  02 00 80 	mov _SPI4CON1bits,w2
 1171 000560  C2 00 DD 	sl w0,#2,w1
 1172 000562  30 FE 2F 	mov #-29,w0
 213:lib/lib_pic33e/SPI.c **** 
 214:lib/lib_pic33e/SPI.c ****     /* SPI3CON2 */
 215:lib/lib_pic33e/SPI.c ****     SPI4CON2bits.FRMEN = 0;  // Disable all framed SPI support
 216:lib/lib_pic33e/SPI.c ****     SPI4CON2bits.SPIBEN = 1; // Enhanced mode is enabled
 217:lib/lib_pic33e/SPI.c **** 
 218:lib/lib_pic33e/SPI.c ****     /* SPI3STAT */
 219:lib/lib_pic33e/SPI.c ****     SPI4STATbits.SPISIDL = 0; // continue operation in Idle
 220:lib/lib_pic33e/SPI.c ****     SPI4STATbits.SPIROV = 0; // Clear overflow
 221:lib/lib_pic33e/SPI.c ****     SPI4STATbits.SISEL = 0b011; // Set interrupt to SPI receive buffer full is set (Not used)
 1173              	.loc 1 221 0
 1174 000564  33 FE 2F 	mov #-29,w3
 1175              	.loc 1 212 0
 1176 000566  00 01 61 	and w2,w0,w2
 222:lib/lib_pic33e/SPI.c **** 
 223:lib/lib_pic33e/SPI.c ****     /* Configuration is finished. Enable the module */
 224:lib/lib_pic33e/SPI.c ****     SPI4STATbits.SPIEN = config.enabled;
 1177              	.loc 1 224 0
 1178 000568  3E 00 90 	mov [w14+6],w0
 1179              	.loc 1 212 0
 1180 00056a  82 80 70 	ior w1,w2,w1
 1181              	.loc 1 224 0
 1182 00056c  61 40 60 	and.b w0,#1,w0
 1183              	.loc 1 212 0
 1184 00056e  01 00 88 	mov w1,_SPI4CON1bits
 1185              	.loc 1 224 0
 1186 000570  00 80 FB 	ze w0,w0
 1187              	.loc 1 215 0
 1188 000572  01 E0 A9 	bclr.b _SPI4CON2bits+1,#7
 1189              	.loc 1 224 0
 1190 000574  4F 01 DD 	sl w0,#15,w2
 1191              	.loc 1 216 0
 1192 000576  00 00 A8 	bset.b _SPI4CON2bits,#0
 225:lib/lib_pic33e/SPI.c ****     return baudrate;
MPLAB XC16 ASSEMBLY Listing:   			page 26


 1193              	.loc 1 225 0
 1194 000578  1E 00 78 	mov [w14],w0
 1195 00057a  9E 00 90 	mov [w14+2],w1
 1196              	.loc 1 219 0
 1197 00057c  01 A0 A9 	bclr.b _SPI4STATbits+1,#5
 1198              	.loc 1 220 0
 1199 00057e  00 C0 A9 	bclr.b _SPI4STATbits,#6
 1200              	.loc 1 221 0
 1201 000580  04 00 80 	mov _SPI4STATbits,w4
 1202 000582  83 01 62 	and w4,w3,w3
 1203 000584  C3 00 B3 	ior #12,w3
 1204 000586  03 00 88 	mov w3,_SPI4STATbits
 1205              	.loc 1 224 0
 1206 000588  03 00 80 	mov _SPI4STATbits,w3
 1207 00058a  03 F0 A1 	bclr w3,#15
 1208 00058c  03 01 71 	ior w2,w3,w2
 1209 00058e  02 00 88 	mov w2,_SPI4STATbits
 226:lib/lib_pic33e/SPI.c **** }
 1210              	.loc 1 226 0
 1211 000590  4F 04 78 	mov [--w15],w8
 1212 000592  8E 07 78 	mov w14,w15
 1213 000594  4F 07 78 	mov [--w15],w14
 1214 000596  00 40 A9 	bclr CORCON,#2
 1215 000598  00 00 06 	return 
 1216              	.set ___PA___,0
 1217              	.LFE4:
 1218              	.size _config_SPI4,.-_config_SPI4
 1219              	.align 2
 1220              	.global _SPI1_enable_module
 1221              	.type _SPI1_enable_module,@function
 1222              	_SPI1_enable_module:
 1223              	.LFB5:
 227:lib/lib_pic33e/SPI.c **** 
 228:lib/lib_pic33e/SPI.c **** /**********************************************************************
 229:lib/lib_pic33e/SPI.c ****  * Name:    SPI1_enable_module
 230:lib/lib_pic33e/SPI.c ****  * Args:    -
 231:lib/lib_pic33e/SPI.c ****  * Return:  -
 232:lib/lib_pic33e/SPI.c ****  * Desc:    enables SPI1 module
 233:lib/lib_pic33e/SPI.c ****  **********************************************************************/
 234:lib/lib_pic33e/SPI.c **** void SPI1_enable_module(){
 1224              	.loc 1 234 0
 1225              	.set ___PA___,1
 1226 00059a  00 00 FA 	lnk #0
 1227              	.LCFI11:
 235:lib/lib_pic33e/SPI.c ****     SPI1STATbits.SPIEN = 1;
 1228              	.loc 1 235 0
 1229 00059c  01 E0 A8 	bset.b _SPI1STATbits+1,#7
 236:lib/lib_pic33e/SPI.c ****     return;
 237:lib/lib_pic33e/SPI.c **** }
 1230              	.loc 1 237 0
 1231 00059e  8E 07 78 	mov w14,w15
 1232 0005a0  4F 07 78 	mov [--w15],w14
 1233 0005a2  00 40 A9 	bclr CORCON,#2
 1234 0005a4  00 00 06 	return 
 1235              	.set ___PA___,0
 1236              	.LFE5:
 1237              	.size _SPI1_enable_module,.-_SPI1_enable_module
MPLAB XC16 ASSEMBLY Listing:   			page 27


 1238              	.align 2
 1239              	.global _SPI2_enable_module
 1240              	.type _SPI2_enable_module,@function
 1241              	_SPI2_enable_module:
 1242              	.LFB6:
 238:lib/lib_pic33e/SPI.c **** 
 239:lib/lib_pic33e/SPI.c **** /**********************************************************************
 240:lib/lib_pic33e/SPI.c ****  * Name:    SPI2_enable_module
 241:lib/lib_pic33e/SPI.c ****  * Args:    -
 242:lib/lib_pic33e/SPI.c ****  * Return:  -
 243:lib/lib_pic33e/SPI.c ****  * Desc:    enables SPI2 module
 244:lib/lib_pic33e/SPI.c ****  **********************************************************************/
 245:lib/lib_pic33e/SPI.c **** void SPI2_enable_module(){
 1243              	.loc 1 245 0
 1244              	.set ___PA___,1
 1245 0005a6  00 00 FA 	lnk #0
 1246              	.LCFI12:
 246:lib/lib_pic33e/SPI.c ****     SPI2STATbits.SPIEN = 1;
 1247              	.loc 1 246 0
 1248 0005a8  01 E0 A8 	bset.b _SPI2STATbits+1,#7
 247:lib/lib_pic33e/SPI.c ****     return;
 248:lib/lib_pic33e/SPI.c **** }
 1249              	.loc 1 248 0
 1250 0005aa  8E 07 78 	mov w14,w15
 1251 0005ac  4F 07 78 	mov [--w15],w14
 1252 0005ae  00 40 A9 	bclr CORCON,#2
 1253 0005b0  00 00 06 	return 
 1254              	.set ___PA___,0
 1255              	.LFE6:
 1256              	.size _SPI2_enable_module,.-_SPI2_enable_module
 1257              	.align 2
 1258              	.global _SPI3_enable_module
 1259              	.type _SPI3_enable_module,@function
 1260              	_SPI3_enable_module:
 1261              	.LFB7:
 249:lib/lib_pic33e/SPI.c **** 
 250:lib/lib_pic33e/SPI.c **** /**********************************************************************
 251:lib/lib_pic33e/SPI.c ****  * Name:    SPI3_enable_module
 252:lib/lib_pic33e/SPI.c ****  * Args:    -
 253:lib/lib_pic33e/SPI.c ****  * Return:  -
 254:lib/lib_pic33e/SPI.c ****  * Desc:    enables SPI3 module
 255:lib/lib_pic33e/SPI.c ****  **********************************************************************/
 256:lib/lib_pic33e/SPI.c **** void SPI3_enable_module(){
 1262              	.loc 1 256 0
 1263              	.set ___PA___,1
 1264 0005b2  00 00 FA 	lnk #0
 1265              	.LCFI13:
 257:lib/lib_pic33e/SPI.c ****     SPI3STATbits.SPIEN = 1;
 1266              	.loc 1 257 0
 1267 0005b4  01 E0 A8 	bset.b _SPI3STATbits+1,#7
 258:lib/lib_pic33e/SPI.c ****     return;
 259:lib/lib_pic33e/SPI.c **** }
 1268              	.loc 1 259 0
 1269 0005b6  8E 07 78 	mov w14,w15
 1270 0005b8  4F 07 78 	mov [--w15],w14
 1271 0005ba  00 40 A9 	bclr CORCON,#2
 1272 0005bc  00 00 06 	return 
MPLAB XC16 ASSEMBLY Listing:   			page 28


 1273              	.set ___PA___,0
 1274              	.LFE7:
 1275              	.size _SPI3_enable_module,.-_SPI3_enable_module
 1276              	.align 2
 1277              	.global _SPI4_enable_module
 1278              	.type _SPI4_enable_module,@function
 1279              	_SPI4_enable_module:
 1280              	.LFB8:
 260:lib/lib_pic33e/SPI.c **** 
 261:lib/lib_pic33e/SPI.c **** /**********************************************************************
 262:lib/lib_pic33e/SPI.c ****  * Name:    SPI4_enable_module
 263:lib/lib_pic33e/SPI.c ****  * Args:    -
 264:lib/lib_pic33e/SPI.c ****  * Return:  -
 265:lib/lib_pic33e/SPI.c ****  * Desc:    enables SPI4 module
 266:lib/lib_pic33e/SPI.c ****  **********************************************************************/
 267:lib/lib_pic33e/SPI.c **** void SPI4_enable_module(){
 1281              	.loc 1 267 0
 1282              	.set ___PA___,1
 1283 0005be  00 00 FA 	lnk #0
 1284              	.LCFI14:
 268:lib/lib_pic33e/SPI.c ****     SPI4STATbits.SPIEN = 1;
 1285              	.loc 1 268 0
 1286 0005c0  01 E0 A8 	bset.b _SPI4STATbits+1,#7
 269:lib/lib_pic33e/SPI.c ****     return;
 270:lib/lib_pic33e/SPI.c **** }
 1287              	.loc 1 270 0
 1288 0005c2  8E 07 78 	mov w14,w15
 1289 0005c4  4F 07 78 	mov [--w15],w14
 1290 0005c6  00 40 A9 	bclr CORCON,#2
 1291 0005c8  00 00 06 	return 
 1292              	.set ___PA___,0
 1293              	.LFE8:
 1294              	.size _SPI4_enable_module,.-_SPI4_enable_module
 1295              	.align 2
 1296              	.global _SPI1_Exchange
 1297              	.type _SPI1_Exchange,@function
 1298              	_SPI1_Exchange:
 1299              	.LFB9:
 271:lib/lib_pic33e/SPI.c **** 
 272:lib/lib_pic33e/SPI.c **** /**
 273:lib/lib_pic33e/SPI.c ****  * @brief Exchange one element of data. Transmit or receive
 274:lib/lib_pic33e/SPI.c ****  *
 275:lib/lib_pic33e/SPI.c ****  * @param pTransmitData Data element to send. NULL when receiving
 276:lib/lib_pic33e/SPI.c ****  * only.
 277:lib/lib_pic33e/SPI.c ****  * @param pReceiveData  Data element to receive. NULL when
 278:lib/lib_pic33e/SPI.c ****  * transmitting only.
 279:lib/lib_pic33e/SPI.c ****  */
 280:lib/lib_pic33e/SPI.c **** void SPI1_Exchange( uint8_t *pTransmitData, uint8_t *pReceiveData )
 281:lib/lib_pic33e/SPI.c **** {
 1300              	.loc 1 281 0
 1301              	.set ___PA___,1
 1302 0005ca  04 00 FA 	lnk #4
 1303              	.LCFI15:
 1304 0005cc  00 0F 78 	mov w0,[w14]
 1305 0005ce  11 07 98 	mov w1,[w14+2]
 1306              	.L17:
 282:lib/lib_pic33e/SPI.c **** 
MPLAB XC16 ASSEMBLY Listing:   			page 29


 283:lib/lib_pic33e/SPI.c ****     while( SPI1STATbits.SPITBF == true )
 1307              	.loc 1 283 0
 1308 0005d0  00 00 80 	mov _SPI1STATbits,w0
 1309 0005d2  62 00 60 	and w0,#2,w0
 1310 0005d4  00 00 E0 	cp0 w0
 1311              	.set ___BP___,0
 1312 0005d6  00 00 3A 	bra nz,.L17
 1313              	.LBB26:
 1314              	.LBB27:
 284:lib/lib_pic33e/SPI.c ****     {
 285:lib/lib_pic33e/SPI.c **** 
 286:lib/lib_pic33e/SPI.c ****     }
 287:lib/lib_pic33e/SPI.c **** 
 288:lib/lib_pic33e/SPI.c ****     if (SPI1_TransferModeGet() == SPI_TRANSFER_MODE_16BIT)
 289:lib/lib_pic33e/SPI.c ****         SPI1BUF = *((uint16_t*)pTransmitData);
 290:lib/lib_pic33e/SPI.c ****     else
 291:lib/lib_pic33e/SPI.c ****         SPI1BUF = *((uint8_t*)pTransmitData);
 292:lib/lib_pic33e/SPI.c **** 
 293:lib/lib_pic33e/SPI.c ****     while ( SPI1STATbits.SRXMPT == true);
 294:lib/lib_pic33e/SPI.c **** 
 295:lib/lib_pic33e/SPI.c ****     if (SPI1_TransferModeGet() == SPI_TRANSFER_MODE_16BIT)
 296:lib/lib_pic33e/SPI.c ****         *((uint16_t*)pReceiveData) = SPI1BUF;
 297:lib/lib_pic33e/SPI.c ****     else
 298:lib/lib_pic33e/SPI.c ****         *((uint8_t*)pReceiveData) = SPI1BUF;
 299:lib/lib_pic33e/SPI.c **** 
 300:lib/lib_pic33e/SPI.c ****     return;
 301:lib/lib_pic33e/SPI.c **** }
 302:lib/lib_pic33e/SPI.c **** 
 303:lib/lib_pic33e/SPI.c **** /**
 304:lib/lib_pic33e/SPI.c ****  * @brief Exchange one element of data. Transmit or receive
 305:lib/lib_pic33e/SPI.c ****  *
 306:lib/lib_pic33e/SPI.c ****  * @param pTransmitData Data element to send. NULL when receiving
 307:lib/lib_pic33e/SPI.c ****  * only.
 308:lib/lib_pic33e/SPI.c ****  * @param pReceiveData  Data element to receive. NULL when
 309:lib/lib_pic33e/SPI.c ****  * transmitting only.
 310:lib/lib_pic33e/SPI.c ****  */
 311:lib/lib_pic33e/SPI.c **** void SPI2_Exchange( uint8_t *pTransmitData, uint8_t *pReceiveData )
 312:lib/lib_pic33e/SPI.c **** {
 313:lib/lib_pic33e/SPI.c **** 
 314:lib/lib_pic33e/SPI.c ****     while( SPI2STATbits.SPITBF == true )
 315:lib/lib_pic33e/SPI.c ****     {
 316:lib/lib_pic33e/SPI.c **** 
 317:lib/lib_pic33e/SPI.c ****     }
 318:lib/lib_pic33e/SPI.c **** 
 319:lib/lib_pic33e/SPI.c ****     if (SPI2_TransferModeGet() == SPI_TRANSFER_MODE_16BIT)
 320:lib/lib_pic33e/SPI.c ****         SPI2BUF = *((uint16_t*)pTransmitData);
 321:lib/lib_pic33e/SPI.c ****     else
 322:lib/lib_pic33e/SPI.c ****         SPI2BUF = *((uint8_t*)pTransmitData);
 323:lib/lib_pic33e/SPI.c **** 
 324:lib/lib_pic33e/SPI.c ****     while ( SPI2STATbits.SRXMPT == true);
 325:lib/lib_pic33e/SPI.c **** 
 326:lib/lib_pic33e/SPI.c ****     if (SPI2_TransferModeGet() == SPI_TRANSFER_MODE_16BIT)
 327:lib/lib_pic33e/SPI.c ****         *((uint16_t*)pReceiveData) = SPI2BUF;
 328:lib/lib_pic33e/SPI.c ****     else
 329:lib/lib_pic33e/SPI.c ****         *((uint8_t*)pReceiveData) = SPI2BUF;
 330:lib/lib_pic33e/SPI.c **** 
 331:lib/lib_pic33e/SPI.c ****     return;
MPLAB XC16 ASSEMBLY Listing:   			page 30


 332:lib/lib_pic33e/SPI.c **** }
 333:lib/lib_pic33e/SPI.c **** 
 334:lib/lib_pic33e/SPI.c **** 
 335:lib/lib_pic33e/SPI.c **** /**
 336:lib/lib_pic33e/SPI.c ****  * @brief Exchange one element of data. Transmit or receive
 337:lib/lib_pic33e/SPI.c ****  *
 338:lib/lib_pic33e/SPI.c ****  * @param pTransmitData Data element to send. NULL when receiving
 339:lib/lib_pic33e/SPI.c ****  * only.
 340:lib/lib_pic33e/SPI.c ****  * @param pReceiveData  Data element to receive. NULL when
 341:lib/lib_pic33e/SPI.c ****  * transmitting only.
 342:lib/lib_pic33e/SPI.c ****  */
 343:lib/lib_pic33e/SPI.c **** void SPI3_Exchange( uint8_t *pTransmitData, uint8_t *pReceiveData )
 344:lib/lib_pic33e/SPI.c **** {
 345:lib/lib_pic33e/SPI.c **** 
 346:lib/lib_pic33e/SPI.c ****     while( SPI3STATbits.SPITBF == true )
 347:lib/lib_pic33e/SPI.c ****     {
 348:lib/lib_pic33e/SPI.c **** 
 349:lib/lib_pic33e/SPI.c ****     }
 350:lib/lib_pic33e/SPI.c **** 
 351:lib/lib_pic33e/SPI.c ****     if (SPI3_TransferModeGet() == SPI_TRANSFER_MODE_16BIT)
 352:lib/lib_pic33e/SPI.c ****         SPI3BUF = *((uint16_t*)pTransmitData);
 353:lib/lib_pic33e/SPI.c ****     else
 354:lib/lib_pic33e/SPI.c ****         SPI3BUF = *((uint8_t*)pTransmitData);
 355:lib/lib_pic33e/SPI.c **** 
 356:lib/lib_pic33e/SPI.c ****     while ( SPI3STATbits.SRXMPT == true);
 357:lib/lib_pic33e/SPI.c **** 
 358:lib/lib_pic33e/SPI.c ****     if (SPI3_TransferModeGet() == SPI_TRANSFER_MODE_16BIT)
 359:lib/lib_pic33e/SPI.c ****         *((uint16_t*)pReceiveData) = SPI3BUF;
 360:lib/lib_pic33e/SPI.c ****     else
 361:lib/lib_pic33e/SPI.c ****         *((uint8_t*)pReceiveData) = SPI3BUF;
 362:lib/lib_pic33e/SPI.c **** 
 363:lib/lib_pic33e/SPI.c ****     return;
 364:lib/lib_pic33e/SPI.c **** }
 365:lib/lib_pic33e/SPI.c **** 
 366:lib/lib_pic33e/SPI.c **** 
 367:lib/lib_pic33e/SPI.c **** /**
 368:lib/lib_pic33e/SPI.c ****  * @brief Exchange one element of data. Transmit or receive
 369:lib/lib_pic33e/SPI.c ****  *
 370:lib/lib_pic33e/SPI.c ****  * @param pTransmitData Data element to send. NULL when receiving
 371:lib/lib_pic33e/SPI.c ****  * only.
 372:lib/lib_pic33e/SPI.c ****  * @param pReceiveData  Data element to receive. NULL when
 373:lib/lib_pic33e/SPI.c ****  * transmitting only.
 374:lib/lib_pic33e/SPI.c ****  */
 375:lib/lib_pic33e/SPI.c **** void SPI4_Exchange( uint8_t *pTransmitData, uint8_t *pReceiveData )
 376:lib/lib_pic33e/SPI.c **** {
 377:lib/lib_pic33e/SPI.c **** 
 378:lib/lib_pic33e/SPI.c ****     while( SPI4STATbits.SPITBF == true )
 379:lib/lib_pic33e/SPI.c ****     {
 380:lib/lib_pic33e/SPI.c **** 
 381:lib/lib_pic33e/SPI.c ****     }
 382:lib/lib_pic33e/SPI.c **** 
 383:lib/lib_pic33e/SPI.c ****     if (SPI4_TransferModeGet() == SPI_TRANSFER_MODE_16BIT)
 384:lib/lib_pic33e/SPI.c ****         SPI4BUF = *((uint16_t*)pTransmitData);
 385:lib/lib_pic33e/SPI.c ****     else
 386:lib/lib_pic33e/SPI.c ****         SPI4BUF = *((uint8_t*)pTransmitData);
 387:lib/lib_pic33e/SPI.c **** 
 388:lib/lib_pic33e/SPI.c ****     while ( SPI4STATbits.SRXMPT == true);
MPLAB XC16 ASSEMBLY Listing:   			page 31


 389:lib/lib_pic33e/SPI.c **** 
 390:lib/lib_pic33e/SPI.c ****     if (SPI4_TransferModeGet() == SPI_TRANSFER_MODE_16BIT)
 391:lib/lib_pic33e/SPI.c ****         *((uint16_t*)pReceiveData) = SPI4BUF;
 392:lib/lib_pic33e/SPI.c ****     else
 393:lib/lib_pic33e/SPI.c ****         *((uint8_t*)pReceiveData) = SPI4BUF;
 394:lib/lib_pic33e/SPI.c **** 
 395:lib/lib_pic33e/SPI.c ****     return;
 396:lib/lib_pic33e/SPI.c **** }
 397:lib/lib_pic33e/SPI.c **** 
 398:lib/lib_pic33e/SPI.c **** 
 399:lib/lib_pic33e/SPI.c **** /**
 400:lib/lib_pic33e/SPI.c ****  * @brief Exchange an entire buffer of data. Either transmit or
 401:lib/lib_pic33e/SPI.c ****  * receive.
 402:lib/lib_pic33e/SPI.c ****  *
 403:lib/lib_pic33e/SPI.c ****  * @param pTransmitData Data array to send. NULL when receiving data
 404:lib/lib_pic33e/SPI.c ****  * only.
 405:lib/lib_pic33e/SPI.c ****  * @param byteCount     Number of bytes in the array.
 406:lib/lib_pic33e/SPI.c ****  * @param pReceiveData  Array to receive the data. NULL when
 407:lib/lib_pic33e/SPI.c ****  * transmitting data only.
 408:lib/lib_pic33e/SPI.c ****  *
 409:lib/lib_pic33e/SPI.c ****  * @return Number of data elements transmitted
 410:lib/lib_pic33e/SPI.c ****  */
 411:lib/lib_pic33e/SPI.c **** uint16_t SPI1_ExchangeBuffer(uint8_t *pTransmitData, uint16_t byteCount, uint8_t *pReceiveData)
 412:lib/lib_pic33e/SPI.c **** {
 413:lib/lib_pic33e/SPI.c **** 
 414:lib/lib_pic33e/SPI.c ****     uint16_t dataSentCount = 0;
 415:lib/lib_pic33e/SPI.c ****     uint16_t count = 0;
 416:lib/lib_pic33e/SPI.c ****     uint16_t dummyDataReceived = 0;
 417:lib/lib_pic33e/SPI.c ****     uint16_t dummyDataTransmit = SPI_DUMMY_DATA;
 418:lib/lib_pic33e/SPI.c **** 
 419:lib/lib_pic33e/SPI.c ****     uint8_t  *pSend, *pReceived;
 420:lib/lib_pic33e/SPI.c ****     uint16_t addressIncrement;
 421:lib/lib_pic33e/SPI.c ****     uint16_t receiveAddressIncrement, sendAddressIncrement;
 422:lib/lib_pic33e/SPI.c **** 
 423:lib/lib_pic33e/SPI.c ****     SPI_TRANSFER_MODE spiModeStatus;
 424:lib/lib_pic33e/SPI.c **** 
 425:lib/lib_pic33e/SPI.c ****     spiModeStatus = SPI1_TransferModeGet();
 426:lib/lib_pic33e/SPI.c ****     // set up the address increment variable
 427:lib/lib_pic33e/SPI.c ****     if (spiModeStatus == SPI_TRANSFER_MODE_16BIT)
 428:lib/lib_pic33e/SPI.c ****     {
 429:lib/lib_pic33e/SPI.c ****         addressIncrement = 2;
 430:lib/lib_pic33e/SPI.c ****         byteCount >>= 1;
 431:lib/lib_pic33e/SPI.c ****     }
 432:lib/lib_pic33e/SPI.c ****     else
 433:lib/lib_pic33e/SPI.c ****     {
 434:lib/lib_pic33e/SPI.c ****         addressIncrement = 1;
 435:lib/lib_pic33e/SPI.c ****     }
 436:lib/lib_pic33e/SPI.c **** 
 437:lib/lib_pic33e/SPI.c ****     // set the pointers and increment delta
 438:lib/lib_pic33e/SPI.c ****     // for transmit and receive operations
 439:lib/lib_pic33e/SPI.c ****     if (pTransmitData == NULL)
 440:lib/lib_pic33e/SPI.c ****     {
 441:lib/lib_pic33e/SPI.c ****         sendAddressIncrement = 0;
 442:lib/lib_pic33e/SPI.c ****         pSend = (uint8_t*)&dummyDataTransmit;
 443:lib/lib_pic33e/SPI.c ****     }
 444:lib/lib_pic33e/SPI.c ****     else
 445:lib/lib_pic33e/SPI.c ****     {
MPLAB XC16 ASSEMBLY Listing:   			page 32


 446:lib/lib_pic33e/SPI.c ****         sendAddressIncrement = addressIncrement;
 447:lib/lib_pic33e/SPI.c ****         pSend = (uint8_t*)pTransmitData;
 448:lib/lib_pic33e/SPI.c ****     }
 449:lib/lib_pic33e/SPI.c **** 
 450:lib/lib_pic33e/SPI.c ****     if (pReceiveData == NULL)
 451:lib/lib_pic33e/SPI.c ****     {
 452:lib/lib_pic33e/SPI.c ****        receiveAddressIncrement = 0;
 453:lib/lib_pic33e/SPI.c ****        pReceived = (uint8_t*)&dummyDataReceived;
 454:lib/lib_pic33e/SPI.c ****     }
 455:lib/lib_pic33e/SPI.c ****     else
 456:lib/lib_pic33e/SPI.c ****     {
 457:lib/lib_pic33e/SPI.c ****        receiveAddressIncrement = addressIncrement;
 458:lib/lib_pic33e/SPI.c ****        pReceived = (uint8_t*)pReceiveData;
 459:lib/lib_pic33e/SPI.c ****     }
 460:lib/lib_pic33e/SPI.c **** 
 461:lib/lib_pic33e/SPI.c **** 
 462:lib/lib_pic33e/SPI.c ****     while( SPI1STATbits.SPITBF == true )
 463:lib/lib_pic33e/SPI.c ****     {
 464:lib/lib_pic33e/SPI.c **** 
 465:lib/lib_pic33e/SPI.c ****     }
 466:lib/lib_pic33e/SPI.c **** 
 467:lib/lib_pic33e/SPI.c ****     while (dataSentCount < byteCount)
 468:lib/lib_pic33e/SPI.c ****     {
 469:lib/lib_pic33e/SPI.c ****         if ((count < SPI_FIFO_FILL_LIMIT))
 470:lib/lib_pic33e/SPI.c ****         {
 471:lib/lib_pic33e/SPI.c ****             if (spiModeStatus == SPI_TRANSFER_MODE_16BIT)
 472:lib/lib_pic33e/SPI.c ****                 SPI1BUF = *((uint16_t*)pSend);
 473:lib/lib_pic33e/SPI.c ****             else
 474:lib/lib_pic33e/SPI.c ****                 SPI1BUF = *pSend;
 475:lib/lib_pic33e/SPI.c ****             pSend += sendAddressIncrement;
 476:lib/lib_pic33e/SPI.c ****             dataSentCount++;
 477:lib/lib_pic33e/SPI.c ****             count++;
 478:lib/lib_pic33e/SPI.c ****         }
 479:lib/lib_pic33e/SPI.c **** 
 480:lib/lib_pic33e/SPI.c ****         if (SPI1STATbits.SRXMPT == false)
 481:lib/lib_pic33e/SPI.c ****         {
 482:lib/lib_pic33e/SPI.c ****             if (spiModeStatus == SPI_TRANSFER_MODE_16BIT)
 483:lib/lib_pic33e/SPI.c ****                 *((uint16_t*)pReceived) = SPI1BUF;
 484:lib/lib_pic33e/SPI.c ****             else
 485:lib/lib_pic33e/SPI.c ****                 *pReceived = SPI1BUF;
 486:lib/lib_pic33e/SPI.c ****             pReceived += receiveAddressIncrement;
 487:lib/lib_pic33e/SPI.c ****             count--;
 488:lib/lib_pic33e/SPI.c ****         }
 489:lib/lib_pic33e/SPI.c **** 
 490:lib/lib_pic33e/SPI.c ****     }
 491:lib/lib_pic33e/SPI.c ****     while (count)
 492:lib/lib_pic33e/SPI.c ****     {
 493:lib/lib_pic33e/SPI.c ****         if (SPI1STATbits.SRXMPT == false)
 494:lib/lib_pic33e/SPI.c ****         {
 495:lib/lib_pic33e/SPI.c ****             if (spiModeStatus == SPI_TRANSFER_MODE_16BIT)
 496:lib/lib_pic33e/SPI.c ****                 *((uint16_t*)pReceived) = SPI1BUF;
 497:lib/lib_pic33e/SPI.c ****             else
 498:lib/lib_pic33e/SPI.c ****                 *pReceived = SPI1BUF;
 499:lib/lib_pic33e/SPI.c ****             pReceived += receiveAddressIncrement;
 500:lib/lib_pic33e/SPI.c ****             count--;
 501:lib/lib_pic33e/SPI.c ****         }
 502:lib/lib_pic33e/SPI.c ****     }
MPLAB XC16 ASSEMBLY Listing:   			page 33


 503:lib/lib_pic33e/SPI.c **** 
 504:lib/lib_pic33e/SPI.c ****     return dataSentCount;
 505:lib/lib_pic33e/SPI.c **** }
 506:lib/lib_pic33e/SPI.c **** 
 507:lib/lib_pic33e/SPI.c **** /**
 508:lib/lib_pic33e/SPI.c ****  * @brief Exchange an entire buffer of data. Either transmit or
 509:lib/lib_pic33e/SPI.c ****  * receive.
 510:lib/lib_pic33e/SPI.c ****  *
 511:lib/lib_pic33e/SPI.c ****  * @param pTransmitData Data array to send. NULL when receiving data
 512:lib/lib_pic33e/SPI.c ****  * only.
 513:lib/lib_pic33e/SPI.c ****  * @param byteCount     Number of bytes in the array.
 514:lib/lib_pic33e/SPI.c ****  * @param pReceiveData  Array to receive the data. NULL when
 515:lib/lib_pic33e/SPI.c ****  * transmitting data only.
 516:lib/lib_pic33e/SPI.c ****  *
 517:lib/lib_pic33e/SPI.c ****  * @return Number of data elements transmitted
 518:lib/lib_pic33e/SPI.c ****  */
 519:lib/lib_pic33e/SPI.c **** uint16_t SPI2_ExchangeBuffer(uint8_t *pTransmitData, uint16_t byteCount, uint8_t *pReceiveData)
 520:lib/lib_pic33e/SPI.c **** {
 521:lib/lib_pic33e/SPI.c **** 
 522:lib/lib_pic33e/SPI.c ****     uint16_t dataSentCount = 0;
 523:lib/lib_pic33e/SPI.c ****     uint16_t count = 0;
 524:lib/lib_pic33e/SPI.c ****     uint16_t dummyDataReceived = 0;
 525:lib/lib_pic33e/SPI.c ****     uint16_t dummyDataTransmit = SPI_DUMMY_DATA;
 526:lib/lib_pic33e/SPI.c **** 
 527:lib/lib_pic33e/SPI.c ****     uint8_t  *pSend, *pReceived;
 528:lib/lib_pic33e/SPI.c ****     uint16_t addressIncrement;
 529:lib/lib_pic33e/SPI.c ****     uint16_t receiveAddressIncrement, sendAddressIncrement;
 530:lib/lib_pic33e/SPI.c **** 
 531:lib/lib_pic33e/SPI.c ****     SPI_TRANSFER_MODE spiModeStatus;
 532:lib/lib_pic33e/SPI.c **** 
 533:lib/lib_pic33e/SPI.c ****     spiModeStatus = SPI2_TransferModeGet();
 534:lib/lib_pic33e/SPI.c ****     // set up the address increment variable
 535:lib/lib_pic33e/SPI.c ****     if (spiModeStatus == SPI_TRANSFER_MODE_16BIT)
 536:lib/lib_pic33e/SPI.c ****     {
 537:lib/lib_pic33e/SPI.c ****         addressIncrement = 2;
 538:lib/lib_pic33e/SPI.c ****         byteCount >>= 1;
 539:lib/lib_pic33e/SPI.c ****     }
 540:lib/lib_pic33e/SPI.c ****     else
 541:lib/lib_pic33e/SPI.c ****     {
 542:lib/lib_pic33e/SPI.c ****         addressIncrement = 1;
 543:lib/lib_pic33e/SPI.c ****     }
 544:lib/lib_pic33e/SPI.c **** 
 545:lib/lib_pic33e/SPI.c ****     // set the pointers and increment delta
 546:lib/lib_pic33e/SPI.c ****     // for transmit and receive operations
 547:lib/lib_pic33e/SPI.c ****     if (pTransmitData == NULL)
 548:lib/lib_pic33e/SPI.c ****     {
 549:lib/lib_pic33e/SPI.c ****         sendAddressIncrement = 0;
 550:lib/lib_pic33e/SPI.c ****         pSend = (uint8_t*)&dummyDataTransmit;
 551:lib/lib_pic33e/SPI.c ****     }
 552:lib/lib_pic33e/SPI.c ****     else
 553:lib/lib_pic33e/SPI.c ****     {
 554:lib/lib_pic33e/SPI.c ****         sendAddressIncrement = addressIncrement;
 555:lib/lib_pic33e/SPI.c ****         pSend = (uint8_t*)pTransmitData;
 556:lib/lib_pic33e/SPI.c ****     }
 557:lib/lib_pic33e/SPI.c **** 
 558:lib/lib_pic33e/SPI.c ****     if (pReceiveData == NULL)
 559:lib/lib_pic33e/SPI.c ****     {
MPLAB XC16 ASSEMBLY Listing:   			page 34


 560:lib/lib_pic33e/SPI.c ****        receiveAddressIncrement = 0;
 561:lib/lib_pic33e/SPI.c ****        pReceived = (uint8_t*)&dummyDataReceived;
 562:lib/lib_pic33e/SPI.c ****     }
 563:lib/lib_pic33e/SPI.c ****     else
 564:lib/lib_pic33e/SPI.c ****     {
 565:lib/lib_pic33e/SPI.c ****        receiveAddressIncrement = addressIncrement;
 566:lib/lib_pic33e/SPI.c ****        pReceived = (uint8_t*)pReceiveData;
 567:lib/lib_pic33e/SPI.c ****     }
 568:lib/lib_pic33e/SPI.c **** 
 569:lib/lib_pic33e/SPI.c **** 
 570:lib/lib_pic33e/SPI.c ****     while( SPI2STATbits.SPITBF == true )
 571:lib/lib_pic33e/SPI.c ****     {
 572:lib/lib_pic33e/SPI.c **** 
 573:lib/lib_pic33e/SPI.c ****     }
 574:lib/lib_pic33e/SPI.c **** 
 575:lib/lib_pic33e/SPI.c ****     while (dataSentCount < byteCount)
 576:lib/lib_pic33e/SPI.c ****     {
 577:lib/lib_pic33e/SPI.c ****         if ((count < SPI_FIFO_FILL_LIMIT))
 578:lib/lib_pic33e/SPI.c ****         {
 579:lib/lib_pic33e/SPI.c ****             if (spiModeStatus == SPI_TRANSFER_MODE_16BIT)
 580:lib/lib_pic33e/SPI.c ****                 SPI2BUF = *((uint16_t*)pSend);
 581:lib/lib_pic33e/SPI.c ****             else
 582:lib/lib_pic33e/SPI.c ****                 SPI2BUF = *pSend;
 583:lib/lib_pic33e/SPI.c ****             pSend += sendAddressIncrement;
 584:lib/lib_pic33e/SPI.c ****             dataSentCount++;
 585:lib/lib_pic33e/SPI.c ****             count++;
 586:lib/lib_pic33e/SPI.c ****         }
 587:lib/lib_pic33e/SPI.c **** 
 588:lib/lib_pic33e/SPI.c ****         if (SPI2STATbits.SRXMPT == false)
 589:lib/lib_pic33e/SPI.c ****         {
 590:lib/lib_pic33e/SPI.c ****             if (spiModeStatus == SPI_TRANSFER_MODE_16BIT)
 591:lib/lib_pic33e/SPI.c ****                 *((uint16_t*)pReceived) = SPI2BUF;
 592:lib/lib_pic33e/SPI.c ****             else
 593:lib/lib_pic33e/SPI.c ****                 *pReceived = SPI2BUF;
 594:lib/lib_pic33e/SPI.c ****             pReceived += receiveAddressIncrement;
 595:lib/lib_pic33e/SPI.c ****             count--;
 596:lib/lib_pic33e/SPI.c ****         }
 597:lib/lib_pic33e/SPI.c **** 
 598:lib/lib_pic33e/SPI.c ****     }
 599:lib/lib_pic33e/SPI.c ****     while (count)
 600:lib/lib_pic33e/SPI.c ****     {
 601:lib/lib_pic33e/SPI.c ****         if (SPI2STATbits.SRXMPT == false)
 602:lib/lib_pic33e/SPI.c ****         {
 603:lib/lib_pic33e/SPI.c ****             if (spiModeStatus == SPI_TRANSFER_MODE_16BIT)
 604:lib/lib_pic33e/SPI.c ****                 *((uint16_t*)pReceived) = SPI2BUF;
 605:lib/lib_pic33e/SPI.c ****             else
 606:lib/lib_pic33e/SPI.c ****                 *pReceived = SPI2BUF;
 607:lib/lib_pic33e/SPI.c ****             pReceived += receiveAddressIncrement;
 608:lib/lib_pic33e/SPI.c ****             count--;
 609:lib/lib_pic33e/SPI.c ****         }
 610:lib/lib_pic33e/SPI.c ****     }
 611:lib/lib_pic33e/SPI.c **** 
 612:lib/lib_pic33e/SPI.c ****     return dataSentCount;
 613:lib/lib_pic33e/SPI.c **** }
 614:lib/lib_pic33e/SPI.c **** /**
 615:lib/lib_pic33e/SPI.c ****  * @brief Exchange an entire buffer of data. Either transmit or
 616:lib/lib_pic33e/SPI.c ****  * receive.
MPLAB XC16 ASSEMBLY Listing:   			page 35


 617:lib/lib_pic33e/SPI.c ****  *
 618:lib/lib_pic33e/SPI.c ****  * @param pTransmitData Data array to send. NULL when receiving data
 619:lib/lib_pic33e/SPI.c ****  * only.
 620:lib/lib_pic33e/SPI.c ****  * @param byteCount     Number of bytes in the array.
 621:lib/lib_pic33e/SPI.c ****  * @param pReceiveData  Array to receive the data. NULL when
 622:lib/lib_pic33e/SPI.c ****  * transmitting data only.
 623:lib/lib_pic33e/SPI.c ****  *
 624:lib/lib_pic33e/SPI.c ****  * @return Number of data elements transmitted
 625:lib/lib_pic33e/SPI.c ****  */
 626:lib/lib_pic33e/SPI.c **** uint16_t SPI3_ExchangeBuffer(uint8_t *pTransmitData, uint16_t byteCount, uint8_t *pReceiveData)
 627:lib/lib_pic33e/SPI.c **** {
 628:lib/lib_pic33e/SPI.c **** 
 629:lib/lib_pic33e/SPI.c ****     uint16_t dataSentCount = 0;
 630:lib/lib_pic33e/SPI.c ****     uint16_t count = 0;
 631:lib/lib_pic33e/SPI.c ****     uint16_t dummyDataReceived = 0;
 632:lib/lib_pic33e/SPI.c ****     uint16_t dummyDataTransmit = SPI_DUMMY_DATA;
 633:lib/lib_pic33e/SPI.c **** 
 634:lib/lib_pic33e/SPI.c ****     uint8_t  *pSend, *pReceived;
 635:lib/lib_pic33e/SPI.c ****     uint16_t addressIncrement;
 636:lib/lib_pic33e/SPI.c ****     uint16_t receiveAddressIncrement, sendAddressIncrement;
 637:lib/lib_pic33e/SPI.c **** 
 638:lib/lib_pic33e/SPI.c ****     SPI_TRANSFER_MODE spiModeStatus;
 639:lib/lib_pic33e/SPI.c **** 
 640:lib/lib_pic33e/SPI.c ****     spiModeStatus = SPI3_TransferModeGet();
 641:lib/lib_pic33e/SPI.c ****     // set up the address increment variable
 642:lib/lib_pic33e/SPI.c ****     if (spiModeStatus == SPI_TRANSFER_MODE_16BIT)
 643:lib/lib_pic33e/SPI.c ****     {
 644:lib/lib_pic33e/SPI.c ****         addressIncrement = 2;
 645:lib/lib_pic33e/SPI.c ****         byteCount >>= 1;
 646:lib/lib_pic33e/SPI.c ****     }
 647:lib/lib_pic33e/SPI.c ****     else
 648:lib/lib_pic33e/SPI.c ****     {
 649:lib/lib_pic33e/SPI.c ****         addressIncrement = 1;
 650:lib/lib_pic33e/SPI.c ****     }
 651:lib/lib_pic33e/SPI.c **** 
 652:lib/lib_pic33e/SPI.c ****     // set the pointers and increment delta
 653:lib/lib_pic33e/SPI.c ****     // for transmit and receive operations
 654:lib/lib_pic33e/SPI.c ****     if (pTransmitData == NULL)
 655:lib/lib_pic33e/SPI.c ****     {
 656:lib/lib_pic33e/SPI.c ****         sendAddressIncrement = 0;
 657:lib/lib_pic33e/SPI.c ****         pSend = (uint8_t*)&dummyDataTransmit;
 658:lib/lib_pic33e/SPI.c ****     }
 659:lib/lib_pic33e/SPI.c ****     else
 660:lib/lib_pic33e/SPI.c ****     {
 661:lib/lib_pic33e/SPI.c ****         sendAddressIncrement = addressIncrement;
 662:lib/lib_pic33e/SPI.c ****         pSend = (uint8_t*)pTransmitData;
 663:lib/lib_pic33e/SPI.c ****     }
 664:lib/lib_pic33e/SPI.c **** 
 665:lib/lib_pic33e/SPI.c ****     if (pReceiveData == NULL)
 666:lib/lib_pic33e/SPI.c ****     {
 667:lib/lib_pic33e/SPI.c ****        receiveAddressIncrement = 0;
 668:lib/lib_pic33e/SPI.c ****        pReceived = (uint8_t*)&dummyDataReceived;
 669:lib/lib_pic33e/SPI.c ****     }
 670:lib/lib_pic33e/SPI.c ****     else
 671:lib/lib_pic33e/SPI.c ****     {
 672:lib/lib_pic33e/SPI.c ****        receiveAddressIncrement = addressIncrement;
 673:lib/lib_pic33e/SPI.c ****        pReceived = (uint8_t*)pReceiveData;
MPLAB XC16 ASSEMBLY Listing:   			page 36


 674:lib/lib_pic33e/SPI.c ****     }
 675:lib/lib_pic33e/SPI.c **** 
 676:lib/lib_pic33e/SPI.c **** 
 677:lib/lib_pic33e/SPI.c ****     while( SPI3STATbits.SPITBF == true )
 678:lib/lib_pic33e/SPI.c ****     {
 679:lib/lib_pic33e/SPI.c **** 
 680:lib/lib_pic33e/SPI.c ****     }
 681:lib/lib_pic33e/SPI.c **** 
 682:lib/lib_pic33e/SPI.c ****     while (dataSentCount < byteCount)
 683:lib/lib_pic33e/SPI.c ****     {
 684:lib/lib_pic33e/SPI.c ****         if ((count < SPI_FIFO_FILL_LIMIT))
 685:lib/lib_pic33e/SPI.c ****         {
 686:lib/lib_pic33e/SPI.c ****             if (spiModeStatus == SPI_TRANSFER_MODE_16BIT)
 687:lib/lib_pic33e/SPI.c ****                 SPI3BUF = *((uint16_t*)pSend);
 688:lib/lib_pic33e/SPI.c ****             else
 689:lib/lib_pic33e/SPI.c ****                 SPI3BUF = *pSend;
 690:lib/lib_pic33e/SPI.c ****             pSend += sendAddressIncrement;
 691:lib/lib_pic33e/SPI.c ****             dataSentCount++;
 692:lib/lib_pic33e/SPI.c ****             count++;
 693:lib/lib_pic33e/SPI.c ****         }
 694:lib/lib_pic33e/SPI.c **** 
 695:lib/lib_pic33e/SPI.c ****         if (SPI3STATbits.SRXMPT == false)
 696:lib/lib_pic33e/SPI.c ****         {
 697:lib/lib_pic33e/SPI.c ****             if (spiModeStatus == SPI_TRANSFER_MODE_16BIT)
 698:lib/lib_pic33e/SPI.c ****                 *((uint16_t*)pReceived) = SPI3BUF;
 699:lib/lib_pic33e/SPI.c ****             else
 700:lib/lib_pic33e/SPI.c ****                 *pReceived = SPI3BUF;
 701:lib/lib_pic33e/SPI.c ****             pReceived += receiveAddressIncrement;
 702:lib/lib_pic33e/SPI.c ****             count--;
 703:lib/lib_pic33e/SPI.c ****         }
 704:lib/lib_pic33e/SPI.c **** 
 705:lib/lib_pic33e/SPI.c ****     }
 706:lib/lib_pic33e/SPI.c ****     while (count)
 707:lib/lib_pic33e/SPI.c ****     {
 708:lib/lib_pic33e/SPI.c ****         if (SPI3STATbits.SRXMPT == false)
 709:lib/lib_pic33e/SPI.c ****         {
 710:lib/lib_pic33e/SPI.c ****             if (spiModeStatus == SPI_TRANSFER_MODE_16BIT)
 711:lib/lib_pic33e/SPI.c ****                 *((uint16_t*)pReceived) = SPI3BUF;
 712:lib/lib_pic33e/SPI.c ****             else
 713:lib/lib_pic33e/SPI.c ****                 *pReceived = SPI3BUF;
 714:lib/lib_pic33e/SPI.c ****             pReceived += receiveAddressIncrement;
 715:lib/lib_pic33e/SPI.c ****             count--;
 716:lib/lib_pic33e/SPI.c ****         }
 717:lib/lib_pic33e/SPI.c ****     }
 718:lib/lib_pic33e/SPI.c **** 
 719:lib/lib_pic33e/SPI.c ****     return dataSentCount;
 720:lib/lib_pic33e/SPI.c **** }
 721:lib/lib_pic33e/SPI.c **** /**
 722:lib/lib_pic33e/SPI.c ****  * @brief Exchange an entire buffer of data. Either transmit or
 723:lib/lib_pic33e/SPI.c ****  * receive.
 724:lib/lib_pic33e/SPI.c ****  *
 725:lib/lib_pic33e/SPI.c ****  * @param pTransmitData Data array to send. NULL when receiving data
 726:lib/lib_pic33e/SPI.c ****  * only.
 727:lib/lib_pic33e/SPI.c ****  * @param byteCount     Number of bytes in the array.
 728:lib/lib_pic33e/SPI.c ****  * @param pReceiveData  Array to receive the data. NULL when
 729:lib/lib_pic33e/SPI.c ****  * transmitting data only.
 730:lib/lib_pic33e/SPI.c ****  *
MPLAB XC16 ASSEMBLY Listing:   			page 37


 731:lib/lib_pic33e/SPI.c ****  * @return Number of data elements transmitted
 732:lib/lib_pic33e/SPI.c ****  */
 733:lib/lib_pic33e/SPI.c **** uint16_t SPI4_ExchangeBuffer(uint8_t *pTransmitData, uint16_t byteCount, uint8_t *pReceiveData)
 734:lib/lib_pic33e/SPI.c **** {
 735:lib/lib_pic33e/SPI.c **** 
 736:lib/lib_pic33e/SPI.c ****     uint16_t dataSentCount = 0;
 737:lib/lib_pic33e/SPI.c ****     uint16_t count = 0;
 738:lib/lib_pic33e/SPI.c ****     uint16_t dummyDataReceived = 0;
 739:lib/lib_pic33e/SPI.c ****     uint16_t dummyDataTransmit = SPI_DUMMY_DATA;
 740:lib/lib_pic33e/SPI.c **** 
 741:lib/lib_pic33e/SPI.c ****     uint8_t  *pSend, *pReceived;
 742:lib/lib_pic33e/SPI.c ****     uint16_t addressIncrement;
 743:lib/lib_pic33e/SPI.c ****     uint16_t receiveAddressIncrement, sendAddressIncrement;
 744:lib/lib_pic33e/SPI.c **** 
 745:lib/lib_pic33e/SPI.c ****     SPI_TRANSFER_MODE spiModeStatus;
 746:lib/lib_pic33e/SPI.c **** 
 747:lib/lib_pic33e/SPI.c ****     spiModeStatus = SPI4_TransferModeGet();
 748:lib/lib_pic33e/SPI.c ****     // set up the address increment variable
 749:lib/lib_pic33e/SPI.c ****     if (spiModeStatus == SPI_TRANSFER_MODE_16BIT)
 750:lib/lib_pic33e/SPI.c ****     {
 751:lib/lib_pic33e/SPI.c ****         addressIncrement = 2;
 752:lib/lib_pic33e/SPI.c ****         byteCount >>= 1;
 753:lib/lib_pic33e/SPI.c ****     }
 754:lib/lib_pic33e/SPI.c ****     else
 755:lib/lib_pic33e/SPI.c ****     {
 756:lib/lib_pic33e/SPI.c ****         addressIncrement = 1;
 757:lib/lib_pic33e/SPI.c ****     }
 758:lib/lib_pic33e/SPI.c **** 
 759:lib/lib_pic33e/SPI.c ****     // set the pointers and increment delta
 760:lib/lib_pic33e/SPI.c ****     // for transmit and receive operations
 761:lib/lib_pic33e/SPI.c ****     if (pTransmitData == NULL)
 762:lib/lib_pic33e/SPI.c ****     {
 763:lib/lib_pic33e/SPI.c ****         sendAddressIncrement = 0;
 764:lib/lib_pic33e/SPI.c ****         pSend = (uint8_t*)&dummyDataTransmit;
 765:lib/lib_pic33e/SPI.c ****     }
 766:lib/lib_pic33e/SPI.c ****     else
 767:lib/lib_pic33e/SPI.c ****     {
 768:lib/lib_pic33e/SPI.c ****         sendAddressIncrement = addressIncrement;
 769:lib/lib_pic33e/SPI.c ****         pSend = (uint8_t*)pTransmitData;
 770:lib/lib_pic33e/SPI.c ****     }
 771:lib/lib_pic33e/SPI.c **** 
 772:lib/lib_pic33e/SPI.c ****     if (pReceiveData == NULL)
 773:lib/lib_pic33e/SPI.c ****     {
 774:lib/lib_pic33e/SPI.c ****        receiveAddressIncrement = 0;
 775:lib/lib_pic33e/SPI.c ****        pReceived = (uint8_t*)&dummyDataReceived;
 776:lib/lib_pic33e/SPI.c ****     }
 777:lib/lib_pic33e/SPI.c ****     else
 778:lib/lib_pic33e/SPI.c ****     {
 779:lib/lib_pic33e/SPI.c ****        receiveAddressIncrement = addressIncrement;
 780:lib/lib_pic33e/SPI.c ****        pReceived = (uint8_t*)pReceiveData;
 781:lib/lib_pic33e/SPI.c ****     }
 782:lib/lib_pic33e/SPI.c **** 
 783:lib/lib_pic33e/SPI.c **** 
 784:lib/lib_pic33e/SPI.c ****     while( SPI4STATbits.SPITBF == true )
 785:lib/lib_pic33e/SPI.c ****     {
 786:lib/lib_pic33e/SPI.c **** 
 787:lib/lib_pic33e/SPI.c ****     }
MPLAB XC16 ASSEMBLY Listing:   			page 38


 788:lib/lib_pic33e/SPI.c **** 
 789:lib/lib_pic33e/SPI.c ****     while (dataSentCount < byteCount)
 790:lib/lib_pic33e/SPI.c ****     {
 791:lib/lib_pic33e/SPI.c ****         if ((count < SPI_FIFO_FILL_LIMIT))
 792:lib/lib_pic33e/SPI.c ****         {
 793:lib/lib_pic33e/SPI.c ****             if (spiModeStatus == SPI_TRANSFER_MODE_16BIT)
 794:lib/lib_pic33e/SPI.c ****                 SPI4BUF = *((uint16_t*)pSend);
 795:lib/lib_pic33e/SPI.c ****             else
 796:lib/lib_pic33e/SPI.c ****                 SPI4BUF = *pSend;
 797:lib/lib_pic33e/SPI.c ****             pSend += sendAddressIncrement;
 798:lib/lib_pic33e/SPI.c ****             dataSentCount++;
 799:lib/lib_pic33e/SPI.c ****             count++;
 800:lib/lib_pic33e/SPI.c ****         }
 801:lib/lib_pic33e/SPI.c **** 
 802:lib/lib_pic33e/SPI.c ****         if (SPI4STATbits.SRXMPT == false)
 803:lib/lib_pic33e/SPI.c ****         {
 804:lib/lib_pic33e/SPI.c ****             if (spiModeStatus == SPI_TRANSFER_MODE_16BIT)
 805:lib/lib_pic33e/SPI.c ****                 *((uint16_t*)pReceived) = SPI4BUF;
 806:lib/lib_pic33e/SPI.c ****             else
 807:lib/lib_pic33e/SPI.c ****                 *pReceived = SPI4BUF;
 808:lib/lib_pic33e/SPI.c ****             pReceived += receiveAddressIncrement;
 809:lib/lib_pic33e/SPI.c ****             count--;
 810:lib/lib_pic33e/SPI.c ****         }
 811:lib/lib_pic33e/SPI.c **** 
 812:lib/lib_pic33e/SPI.c ****     }
 813:lib/lib_pic33e/SPI.c ****     while (count)
 814:lib/lib_pic33e/SPI.c ****     {
 815:lib/lib_pic33e/SPI.c ****         if (SPI4STATbits.SRXMPT == false)
 816:lib/lib_pic33e/SPI.c ****         {
 817:lib/lib_pic33e/SPI.c ****             if (spiModeStatus == SPI_TRANSFER_MODE_16BIT)
 818:lib/lib_pic33e/SPI.c ****                 *((uint16_t*)pReceived) = SPI4BUF;
 819:lib/lib_pic33e/SPI.c ****             else
 820:lib/lib_pic33e/SPI.c ****                 *pReceived = SPI4BUF;
 821:lib/lib_pic33e/SPI.c ****             pReceived += receiveAddressIncrement;
 822:lib/lib_pic33e/SPI.c ****             count--;
 823:lib/lib_pic33e/SPI.c ****         }
 824:lib/lib_pic33e/SPI.c ****     }
 825:lib/lib_pic33e/SPI.c **** 
 826:lib/lib_pic33e/SPI.c ****     return dataSentCount;
 827:lib/lib_pic33e/SPI.c **** }
 828:lib/lib_pic33e/SPI.c **** /**
 829:lib/lib_pic33e/SPI.c ****  * @brief Get SPI1 transfer mode
 830:lib/lib_pic33e/SPI.c ****  *
 831:lib/lib_pic33e/SPI.c ****  */
 832:lib/lib_pic33e/SPI.c **** inline __attribute__((__always_inline__)) SPI_TRANSFER_MODE SPI1_TransferModeGet(void)
 833:lib/lib_pic33e/SPI.c **** {
 834:lib/lib_pic33e/SPI.c **** 	if (SPI1CON1bits.MODE16 == 0)
 1315              	.loc 1 834 0
 1316 0005d8  01 00 80 	mov _SPI1CON1bits,w1
 1317 0005da  00 40 20 	mov #1024,w0
 1318 0005dc  00 80 60 	and w1,w0,w0
 1319 0005de  00 00 E0 	cp0 w0
 1320              	.set ___BP___,0
 1321 0005e0  00 00 3A 	bra nz,.L18
 835:lib/lib_pic33e/SPI.c ****         return SPI_TRANSFER_MODE_8BIT;
 1322              	.loc 1 835 0
 1323 0005e2  00 00 EB 	clr w0
MPLAB XC16 ASSEMBLY Listing:   			page 39


 1324 0005e4  00 00 37 	bra .L19
 1325              	.L18:
 836:lib/lib_pic33e/SPI.c ****     else
 837:lib/lib_pic33e/SPI.c ****         return SPI_TRANSFER_MODE_16BIT;
 1326              	.loc 1 837 0
 1327 0005e6  10 00 20 	mov #1,w0
 1328              	.L19:
 1329              	.LBE27:
 1330              	.LBE26:
 1331              	.loc 1 288 0
 1332 0005e8  E1 0F 50 	sub w0,#1,[w15]
 1333              	.set ___BP___,0
 1334 0005ea  00 00 3A 	bra nz,.L20
 1335              	.loc 1 289 0
 1336 0005ec  1E 00 78 	mov [w14],w0
 1337 0005ee  10 00 78 	mov [w0],w0
 1338 0005f0  00 00 88 	mov w0,_SPI1BUF
 1339              	.loc 1 293 0
 1340 0005f2  00 00 37 	bra .L22
 1341              	.L20:
 1342              	.loc 1 291 0
 1343 0005f4  1E 00 78 	mov [w14],w0
 1344 0005f6  10 40 78 	mov.b [w0],w0
 1345 0005f8  00 80 FB 	ze w0,w0
 1346 0005fa  00 00 88 	mov w0,_SPI1BUF
 1347              	.L22:
 1348              	.loc 1 293 0
 1349 0005fc  01 00 80 	mov _SPI1STATbits,w1
 1350 0005fe  00 02 20 	mov #32,w0
 1351 000600  00 80 60 	and w1,w0,w0
 1352 000602  00 00 E0 	cp0 w0
 1353              	.set ___BP___,0
 1354 000604  00 00 3A 	bra nz,.L22
 1355              	.LBB28:
 1356              	.LBB29:
 1357              	.loc 1 834 0
 1358 000606  01 00 80 	mov _SPI1CON1bits,w1
 1359 000608  00 40 20 	mov #1024,w0
 1360 00060a  00 80 60 	and w1,w0,w0
 1361 00060c  00 00 E0 	cp0 w0
 1362              	.set ___BP___,0
 1363 00060e  00 00 3A 	bra nz,.L23
 1364              	.loc 1 835 0
 1365 000610  00 00 EB 	clr w0
 1366 000612  00 00 37 	bra .L24
 1367              	.L23:
 1368              	.loc 1 837 0
 1369 000614  10 00 20 	mov #1,w0
 1370              	.L24:
 1371              	.LBE29:
 1372              	.LBE28:
 1373              	.loc 1 295 0
 1374 000616  E1 0F 50 	sub w0,#1,[w15]
 1375              	.set ___BP___,0
 1376 000618  00 00 3A 	bra nz,.L25
 1377              	.loc 1 296 0
 1378 00061a  1E 00 90 	mov [w14+2],w0
MPLAB XC16 ASSEMBLY Listing:   			page 40


 1379 00061c  01 00 80 	mov _SPI1BUF,w1
 1380 00061e  01 08 78 	mov w1,[w0]
 1381 000620  00 00 37 	bra .L16
 1382              	.L25:
 1383              	.loc 1 298 0
 1384 000622  01 00 80 	mov _SPI1BUF,w1
 1385 000624  1E 00 90 	mov [w14+2],w0
 1386 000626  81 40 78 	mov.b w1,w1
 1387 000628  01 48 78 	mov.b w1,[w0]
 1388              	.L16:
 1389              	.loc 1 301 0
 1390 00062a  8E 07 78 	mov w14,w15
 1391 00062c  4F 07 78 	mov [--w15],w14
 1392 00062e  00 40 A9 	bclr CORCON,#2
 1393 000630  00 00 06 	return 
 1394              	.set ___PA___,0
 1395              	.LFE9:
 1396              	.size _SPI1_Exchange,.-_SPI1_Exchange
 1397              	.align 2
 1398              	.global _SPI2_Exchange
 1399              	.type _SPI2_Exchange,@function
 1400              	_SPI2_Exchange:
 1401              	.LFB10:
 1402              	.loc 1 312 0
 1403              	.set ___PA___,1
 1404 000632  04 00 FA 	lnk #4
 1405              	.LCFI16:
 1406 000634  00 0F 78 	mov w0,[w14]
 1407 000636  11 07 98 	mov w1,[w14+2]
 1408              	.L28:
 1409              	.loc 1 314 0
 1410 000638  00 00 80 	mov _SPI2STATbits,w0
 1411 00063a  62 00 60 	and w0,#2,w0
 1412 00063c  00 00 E0 	cp0 w0
 1413              	.set ___BP___,0
 1414 00063e  00 00 3A 	bra nz,.L28
 1415              	.LBB30:
 1416              	.LBB31:
 838:lib/lib_pic33e/SPI.c **** }
 839:lib/lib_pic33e/SPI.c **** 
 840:lib/lib_pic33e/SPI.c **** /**
 841:lib/lib_pic33e/SPI.c ****  * @brief Get SPI1 transfer mode
 842:lib/lib_pic33e/SPI.c ****  *
 843:lib/lib_pic33e/SPI.c ****  */
 844:lib/lib_pic33e/SPI.c **** inline __attribute__((__always_inline__)) SPI_TRANSFER_MODE SPI2_TransferModeGet(void)
 845:lib/lib_pic33e/SPI.c **** {
 846:lib/lib_pic33e/SPI.c **** 	if (SPI2CON1bits.MODE16 == 0)
 1417              	.loc 1 846 0
 1418 000640  01 00 80 	mov _SPI2CON1bits,w1
 1419 000642  00 40 20 	mov #1024,w0
 1420 000644  00 80 60 	and w1,w0,w0
 1421 000646  00 00 E0 	cp0 w0
 1422              	.set ___BP___,0
 1423 000648  00 00 3A 	bra nz,.L29
 847:lib/lib_pic33e/SPI.c ****         return SPI_TRANSFER_MODE_8BIT;
 1424              	.loc 1 847 0
 1425 00064a  00 00 EB 	clr w0
MPLAB XC16 ASSEMBLY Listing:   			page 41


 1426 00064c  00 00 37 	bra .L30
 1427              	.L29:
 848:lib/lib_pic33e/SPI.c ****     else
 849:lib/lib_pic33e/SPI.c ****         return SPI_TRANSFER_MODE_16BIT;
 1428              	.loc 1 849 0
 1429 00064e  10 00 20 	mov #1,w0
 1430              	.L30:
 1431              	.LBE31:
 1432              	.LBE30:
 1433              	.loc 1 319 0
 1434 000650  E1 0F 50 	sub w0,#1,[w15]
 1435              	.set ___BP___,0
 1436 000652  00 00 3A 	bra nz,.L31
 1437              	.loc 1 320 0
 1438 000654  1E 00 78 	mov [w14],w0
 1439 000656  10 00 78 	mov [w0],w0
 1440 000658  00 00 88 	mov w0,_SPI2BUF
 1441              	.loc 1 324 0
 1442 00065a  00 00 37 	bra .L33
 1443              	.L31:
 1444              	.loc 1 322 0
 1445 00065c  1E 00 78 	mov [w14],w0
 1446 00065e  10 40 78 	mov.b [w0],w0
 1447 000660  00 80 FB 	ze w0,w0
 1448 000662  00 00 88 	mov w0,_SPI2BUF
 1449              	.L33:
 1450              	.loc 1 324 0
 1451 000664  01 00 80 	mov _SPI2STATbits,w1
 1452 000666  00 02 20 	mov #32,w0
 1453 000668  00 80 60 	and w1,w0,w0
 1454 00066a  00 00 E0 	cp0 w0
 1455              	.set ___BP___,0
 1456 00066c  00 00 3A 	bra nz,.L33
 1457              	.LBB32:
 1458              	.LBB33:
 1459              	.loc 1 846 0
 1460 00066e  01 00 80 	mov _SPI2CON1bits,w1
 1461 000670  00 40 20 	mov #1024,w0
 1462 000672  00 80 60 	and w1,w0,w0
 1463 000674  00 00 E0 	cp0 w0
 1464              	.set ___BP___,0
 1465 000676  00 00 3A 	bra nz,.L34
 1466              	.loc 1 847 0
 1467 000678  00 00 EB 	clr w0
 1468 00067a  00 00 37 	bra .L35
 1469              	.L34:
 1470              	.loc 1 849 0
 1471 00067c  10 00 20 	mov #1,w0
 1472              	.L35:
 1473              	.LBE33:
 1474              	.LBE32:
 1475              	.loc 1 326 0
 1476 00067e  E1 0F 50 	sub w0,#1,[w15]
 1477              	.set ___BP___,0
 1478 000680  00 00 3A 	bra nz,.L36
 1479              	.loc 1 327 0
 1480 000682  1E 00 90 	mov [w14+2],w0
MPLAB XC16 ASSEMBLY Listing:   			page 42


 1481 000684  01 00 80 	mov _SPI2BUF,w1
 1482 000686  01 08 78 	mov w1,[w0]
 1483 000688  00 00 37 	bra .L27
 1484              	.L36:
 1485              	.loc 1 329 0
 1486 00068a  01 00 80 	mov _SPI2BUF,w1
 1487 00068c  1E 00 90 	mov [w14+2],w0
 1488 00068e  81 40 78 	mov.b w1,w1
 1489 000690  01 48 78 	mov.b w1,[w0]
 1490              	.L27:
 1491              	.loc 1 332 0
 1492 000692  8E 07 78 	mov w14,w15
 1493 000694  4F 07 78 	mov [--w15],w14
 1494 000696  00 40 A9 	bclr CORCON,#2
 1495 000698  00 00 06 	return 
 1496              	.set ___PA___,0
 1497              	.LFE10:
 1498              	.size _SPI2_Exchange,.-_SPI2_Exchange
 1499              	.align 2
 1500              	.global _SPI3_Exchange
 1501              	.type _SPI3_Exchange,@function
 1502              	_SPI3_Exchange:
 1503              	.LFB11:
 1504              	.loc 1 344 0
 1505              	.set ___PA___,1
 1506 00069a  04 00 FA 	lnk #4
 1507              	.LCFI17:
 1508 00069c  00 0F 78 	mov w0,[w14]
 1509 00069e  11 07 98 	mov w1,[w14+2]
 1510              	.L39:
 1511              	.loc 1 346 0
 1512 0006a0  00 00 80 	mov _SPI3STATbits,w0
 1513 0006a2  62 00 60 	and w0,#2,w0
 1514 0006a4  00 00 E0 	cp0 w0
 1515              	.set ___BP___,0
 1516 0006a6  00 00 3A 	bra nz,.L39
 1517              	.LBB34:
 1518              	.LBB35:
 850:lib/lib_pic33e/SPI.c **** }
 851:lib/lib_pic33e/SPI.c **** 
 852:lib/lib_pic33e/SPI.c **** /**
 853:lib/lib_pic33e/SPI.c ****  * @brief Get SPI1 transfer mode
 854:lib/lib_pic33e/SPI.c ****  *
 855:lib/lib_pic33e/SPI.c ****  */
 856:lib/lib_pic33e/SPI.c **** inline __attribute__((__always_inline__)) SPI_TRANSFER_MODE SPI3_TransferModeGet(void)
 857:lib/lib_pic33e/SPI.c **** {
 858:lib/lib_pic33e/SPI.c **** 	if (SPI3CON1bits.MODE16 == 0)
 1519              	.loc 1 858 0
 1520 0006a8  01 00 80 	mov _SPI3CON1bits,w1
 1521 0006aa  00 40 20 	mov #1024,w0
 1522 0006ac  00 80 60 	and w1,w0,w0
 1523 0006ae  00 00 E0 	cp0 w0
 1524              	.set ___BP___,0
 1525 0006b0  00 00 3A 	bra nz,.L40
 859:lib/lib_pic33e/SPI.c ****         return SPI_TRANSFER_MODE_8BIT;
 1526              	.loc 1 859 0
 1527 0006b2  00 00 EB 	clr w0
MPLAB XC16 ASSEMBLY Listing:   			page 43


 1528 0006b4  00 00 37 	bra .L41
 1529              	.L40:
 860:lib/lib_pic33e/SPI.c ****     else
 861:lib/lib_pic33e/SPI.c ****         return SPI_TRANSFER_MODE_16BIT;
 1530              	.loc 1 861 0
 1531 0006b6  10 00 20 	mov #1,w0
 1532              	.L41:
 1533              	.LBE35:
 1534              	.LBE34:
 1535              	.loc 1 351 0
 1536 0006b8  E1 0F 50 	sub w0,#1,[w15]
 1537              	.set ___BP___,0
 1538 0006ba  00 00 3A 	bra nz,.L42
 1539              	.loc 1 352 0
 1540 0006bc  1E 00 78 	mov [w14],w0
 1541 0006be  10 00 78 	mov [w0],w0
 1542 0006c0  00 00 88 	mov w0,_SPI3BUF
 1543              	.loc 1 356 0
 1544 0006c2  00 00 37 	bra .L44
 1545              	.L42:
 1546              	.loc 1 354 0
 1547 0006c4  1E 00 78 	mov [w14],w0
 1548 0006c6  10 40 78 	mov.b [w0],w0
 1549 0006c8  00 80 FB 	ze w0,w0
 1550 0006ca  00 00 88 	mov w0,_SPI3BUF
 1551              	.L44:
 1552              	.loc 1 356 0
 1553 0006cc  01 00 80 	mov _SPI3STATbits,w1
 1554 0006ce  00 02 20 	mov #32,w0
 1555 0006d0  00 80 60 	and w1,w0,w0
 1556 0006d2  00 00 E0 	cp0 w0
 1557              	.set ___BP___,0
 1558 0006d4  00 00 3A 	bra nz,.L44
 1559              	.LBB36:
 1560              	.LBB37:
 1561              	.loc 1 858 0
 1562 0006d6  01 00 80 	mov _SPI3CON1bits,w1
 1563 0006d8  00 40 20 	mov #1024,w0
 1564 0006da  00 80 60 	and w1,w0,w0
 1565 0006dc  00 00 E0 	cp0 w0
 1566              	.set ___BP___,0
 1567 0006de  00 00 3A 	bra nz,.L45
 1568              	.loc 1 859 0
 1569 0006e0  00 00 EB 	clr w0
 1570 0006e2  00 00 37 	bra .L46
 1571              	.L45:
 1572              	.loc 1 861 0
 1573 0006e4  10 00 20 	mov #1,w0
 1574              	.L46:
 1575              	.LBE37:
 1576              	.LBE36:
 1577              	.loc 1 358 0
 1578 0006e6  E1 0F 50 	sub w0,#1,[w15]
 1579              	.set ___BP___,0
 1580 0006e8  00 00 3A 	bra nz,.L47
 1581              	.loc 1 359 0
 1582 0006ea  1E 00 90 	mov [w14+2],w0
MPLAB XC16 ASSEMBLY Listing:   			page 44


 1583 0006ec  01 00 80 	mov _SPI3BUF,w1
 1584 0006ee  01 08 78 	mov w1,[w0]
 1585 0006f0  00 00 37 	bra .L38
 1586              	.L47:
 1587              	.loc 1 361 0
 1588 0006f2  01 00 80 	mov _SPI3BUF,w1
 1589 0006f4  1E 00 90 	mov [w14+2],w0
 1590 0006f6  81 40 78 	mov.b w1,w1
 1591 0006f8  01 48 78 	mov.b w1,[w0]
 1592              	.L38:
 1593              	.loc 1 364 0
 1594 0006fa  8E 07 78 	mov w14,w15
 1595 0006fc  4F 07 78 	mov [--w15],w14
 1596 0006fe  00 40 A9 	bclr CORCON,#2
 1597 000700  00 00 06 	return 
 1598              	.set ___PA___,0
 1599              	.LFE11:
 1600              	.size _SPI3_Exchange,.-_SPI3_Exchange
 1601              	.align 2
 1602              	.global _SPI4_Exchange
 1603              	.type _SPI4_Exchange,@function
 1604              	_SPI4_Exchange:
 1605              	.LFB12:
 1606              	.loc 1 376 0
 1607              	.set ___PA___,1
 1608 000702  04 00 FA 	lnk #4
 1609              	.LCFI18:
 1610 000704  00 0F 78 	mov w0,[w14]
 1611 000706  11 07 98 	mov w1,[w14+2]
 1612              	.L50:
 1613              	.loc 1 378 0
 1614 000708  00 00 80 	mov _SPI4STATbits,w0
 1615 00070a  62 00 60 	and w0,#2,w0
 1616 00070c  00 00 E0 	cp0 w0
 1617              	.set ___BP___,0
 1618 00070e  00 00 3A 	bra nz,.L50
 1619              	.LBB38:
 1620              	.LBB39:
 862:lib/lib_pic33e/SPI.c **** }
 863:lib/lib_pic33e/SPI.c **** 
 864:lib/lib_pic33e/SPI.c **** /**
 865:lib/lib_pic33e/SPI.c ****  * @brief Get SPI1 transfer mode
 866:lib/lib_pic33e/SPI.c ****  *
 867:lib/lib_pic33e/SPI.c ****  */
 868:lib/lib_pic33e/SPI.c **** inline __attribute__((__always_inline__)) SPI_TRANSFER_MODE SPI4_TransferModeGet(void)
 869:lib/lib_pic33e/SPI.c **** {
 870:lib/lib_pic33e/SPI.c **** 	if (SPI4CON1bits.MODE16 == 0)
 1621              	.loc 1 870 0
 1622 000710  01 00 80 	mov _SPI4CON1bits,w1
 1623 000712  00 40 20 	mov #1024,w0
 1624 000714  00 80 60 	and w1,w0,w0
 1625 000716  00 00 E0 	cp0 w0
 1626              	.set ___BP___,0
 1627 000718  00 00 3A 	bra nz,.L51
 871:lib/lib_pic33e/SPI.c ****         return SPI_TRANSFER_MODE_8BIT;
 1628              	.loc 1 871 0
 1629 00071a  00 00 EB 	clr w0
MPLAB XC16 ASSEMBLY Listing:   			page 45


 1630 00071c  00 00 37 	bra .L52
 1631              	.L51:
 872:lib/lib_pic33e/SPI.c ****     else
 873:lib/lib_pic33e/SPI.c ****         return SPI_TRANSFER_MODE_16BIT;
 1632              	.loc 1 873 0
 1633 00071e  10 00 20 	mov #1,w0
 1634              	.L52:
 1635              	.LBE39:
 1636              	.LBE38:
 1637              	.loc 1 383 0
 1638 000720  E1 0F 50 	sub w0,#1,[w15]
 1639              	.set ___BP___,0
 1640 000722  00 00 3A 	bra nz,.L53
 1641              	.loc 1 384 0
 1642 000724  1E 00 78 	mov [w14],w0
 1643 000726  10 00 78 	mov [w0],w0
 1644 000728  00 00 88 	mov w0,_SPI4BUF
 1645              	.loc 1 388 0
 1646 00072a  00 00 37 	bra .L55
 1647              	.L53:
 1648              	.loc 1 386 0
 1649 00072c  1E 00 78 	mov [w14],w0
 1650 00072e  10 40 78 	mov.b [w0],w0
 1651 000730  00 80 FB 	ze w0,w0
 1652 000732  00 00 88 	mov w0,_SPI4BUF
 1653              	.L55:
 1654              	.loc 1 388 0
 1655 000734  01 00 80 	mov _SPI4STATbits,w1
 1656 000736  00 02 20 	mov #32,w0
 1657 000738  00 80 60 	and w1,w0,w0
 1658 00073a  00 00 E0 	cp0 w0
 1659              	.set ___BP___,0
 1660 00073c  00 00 3A 	bra nz,.L55
 1661              	.LBB40:
 1662              	.LBB41:
 1663              	.loc 1 870 0
 1664 00073e  01 00 80 	mov _SPI4CON1bits,w1
 1665 000740  00 40 20 	mov #1024,w0
 1666 000742  00 80 60 	and w1,w0,w0
 1667 000744  00 00 E0 	cp0 w0
 1668              	.set ___BP___,0
 1669 000746  00 00 3A 	bra nz,.L56
 1670              	.loc 1 871 0
 1671 000748  00 00 EB 	clr w0
 1672 00074a  00 00 37 	bra .L57
 1673              	.L56:
 1674              	.loc 1 873 0
 1675 00074c  10 00 20 	mov #1,w0
 1676              	.L57:
 1677              	.LBE41:
 1678              	.LBE40:
 1679              	.loc 1 390 0
 1680 00074e  E1 0F 50 	sub w0,#1,[w15]
 1681              	.set ___BP___,0
 1682 000750  00 00 3A 	bra nz,.L58
 1683              	.loc 1 391 0
 1684 000752  1E 00 90 	mov [w14+2],w0
MPLAB XC16 ASSEMBLY Listing:   			page 46


 1685 000754  01 00 80 	mov _SPI4BUF,w1
 1686 000756  01 08 78 	mov w1,[w0]
 1687 000758  00 00 37 	bra .L49
 1688              	.L58:
 1689              	.loc 1 393 0
 1690 00075a  01 00 80 	mov _SPI4BUF,w1
 1691 00075c  1E 00 90 	mov [w14+2],w0
 1692 00075e  81 40 78 	mov.b w1,w1
 1693 000760  01 48 78 	mov.b w1,[w0]
 1694              	.L49:
 1695              	.loc 1 396 0
 1696 000762  8E 07 78 	mov w14,w15
 1697 000764  4F 07 78 	mov [--w15],w14
 1698 000766  00 40 A9 	bclr CORCON,#2
 1699 000768  00 00 06 	return 
 1700              	.set ___PA___,0
 1701              	.LFE12:
 1702              	.size _SPI4_Exchange,.-_SPI4_Exchange
 1703              	.align 2
 1704              	.global _SPI1_ExchangeBuffer
 1705              	.type _SPI1_ExchangeBuffer,@function
 1706              	_SPI1_ExchangeBuffer:
 1707              	.LFB13:
 1708              	.loc 1 412 0
 1709              	.set ___PA___,1
 1710 00076a  1A 00 FA 	lnk #26
 1711              	.LCFI19:
 1712              	.loc 1 412 0
 1713 00076c  20 0F 98 	mov w0,[w14+20]
 1714              	.loc 1 414 0
 1715 00076e  80 03 EB 	clr w7
 1716              	.loc 1 415 0
 1717 000770  00 03 EB 	clr w6
 1718              	.loc 1 416 0
 1719 000772  80 02 EB 	clr w5
 1720              	.loc 1 417 0
 1721 000774  F4 0F 20 	mov #255,w4
 1722              	.LBB42:
 1723              	.LBB43:
 1724              	.loc 1 834 0
 1725 000776  03 00 80 	mov _SPI1CON1bits,w3
 1726 000778  00 40 20 	mov #1024,w0
 1727              	.LBE43:
 1728              	.LBE42:
 1729              	.loc 1 412 0
 1730 00077a  31 0F 98 	mov w1,[w14+22]
 1731 00077c  42 0F 98 	mov w2,[w14+24]
 1732              	.loc 1 414 0
 1733 00077e  07 0F 78 	mov w7,[w14]
 1734              	.loc 1 415 0
 1735 000780  16 07 98 	mov w6,[w14+2]
 1736              	.loc 1 416 0
 1737 000782  05 0F 98 	mov w5,[w14+16]
 1738              	.loc 1 417 0
 1739 000784  14 0F 98 	mov w4,[w14+18]
 1740              	.LBB45:
 1741              	.LBB44:
MPLAB XC16 ASSEMBLY Listing:   			page 47


 1742              	.loc 1 834 0
 1743 000786  00 80 61 	and w3,w0,w0
 1744 000788  00 00 E0 	cp0 w0
 1745              	.set ___BP___,0
 1746 00078a  00 00 3A 	bra nz,.L61
 1747              	.loc 1 835 0
 1748 00078c  00 00 EB 	clr w0
 1749 00078e  00 00 37 	bra .L62
 1750              	.L61:
 1751              	.loc 1 837 0
 1752 000790  10 00 20 	mov #1,w0
 1753              	.L62:
 1754              	.loc 1 425 0
 1755 000792  70 07 98 	mov w0,[w14+14]
 1756              	.LBE44:
 1757              	.LBE45:
 1758              	.loc 1 427 0
 1759 000794  7E 00 90 	mov [w14+14],w0
 1760 000796  E1 0F 50 	sub w0,#1,[w15]
 1761              	.set ___BP___,0
 1762 000798  00 00 3A 	bra nz,.L63
 1763              	.loc 1 430 0
 1764 00079a  3E 08 90 	mov [w14+22],w0
 1765              	.loc 1 429 0
 1766 00079c  21 00 20 	mov #2,w1
 1767              	.loc 1 430 0
 1768 00079e  00 00 D1 	lsr w0,w0
 1769              	.loc 1 429 0
 1770 0007a0  41 07 98 	mov w1,[w14+8]
 1771              	.loc 1 430 0
 1772 0007a2  30 0F 98 	mov w0,[w14+22]
 1773 0007a4  00 00 37 	bra .L64
 1774              	.L63:
 1775              	.loc 1 434 0
 1776 0007a6  10 00 20 	mov #1,w0
 1777 0007a8  40 07 98 	mov w0,[w14+8]
 1778              	.L64:
 1779              	.loc 1 439 0
 1780 0007aa  2E 08 90 	mov [w14+20],w0
 1781 0007ac  00 00 E0 	cp0 w0
 1782              	.set ___BP___,0
 1783 0007ae  00 00 3A 	bra nz,.L65
 1784              	.loc 1 441 0
 1785 0007b0  80 00 EB 	clr w1
 1786              	.loc 1 442 0
 1787 0007b2  72 00 47 	add w14,#18,w0
 1788              	.loc 1 441 0
 1789 0007b4  61 07 98 	mov w1,[w14+12]
 1790              	.loc 1 442 0
 1791 0007b6  20 07 98 	mov w0,[w14+4]
 1792 0007b8  00 00 37 	bra .L66
 1793              	.L65:
 1794              	.loc 1 446 0
 1795 0007ba  CE 00 90 	mov [w14+8],w1
 1796 0007bc  61 07 98 	mov w1,[w14+12]
 1797              	.loc 1 447 0
 1798 0007be  AE 08 90 	mov [w14+20],w1
MPLAB XC16 ASSEMBLY Listing:   			page 48


 1799 0007c0  21 07 98 	mov w1,[w14+4]
 1800              	.L66:
 1801              	.loc 1 450 0
 1802 0007c2  4E 08 90 	mov [w14+24],w0
 1803 0007c4  00 00 E0 	cp0 w0
 1804              	.set ___BP___,0
 1805 0007c6  00 00 3A 	bra nz,.L67
 1806              	.loc 1 452 0
 1807 0007c8  80 00 EB 	clr w1
 1808              	.loc 1 453 0
 1809 0007ca  70 00 47 	add w14,#16,w0
 1810              	.loc 1 452 0
 1811 0007cc  51 07 98 	mov w1,[w14+10]
 1812              	.loc 1 453 0
 1813 0007ce  30 07 98 	mov w0,[w14+6]
 1814              	.loc 1 462 0
 1815 0007d0  00 00 37 	bra .L69
 1816              	.L67:
 1817              	.loc 1 457 0
 1818 0007d2  CE 00 90 	mov [w14+8],w1
 1819 0007d4  51 07 98 	mov w1,[w14+10]
 1820              	.loc 1 458 0
 1821 0007d6  CE 08 90 	mov [w14+24],w1
 1822 0007d8  31 07 98 	mov w1,[w14+6]
 1823              	.L69:
 1824              	.loc 1 462 0
 1825 0007da  00 00 80 	mov _SPI1STATbits,w0
 1826 0007dc  62 00 60 	and w0,#2,w0
 1827 0007de  00 00 E0 	cp0 w0
 1828              	.set ___BP___,0
 1829 0007e0  00 00 3A 	bra nz,.L69
 1830              	.loc 1 467 0
 1831 0007e2  00 00 37 	bra .L70
 1832              	.L76:
 1833              	.loc 1 469 0
 1834 0007e4  1E 00 90 	mov [w14+2],w0
 1835 0007e6  E7 0F 50 	sub w0,#7,[w15]
 1836              	.set ___BP___,0
 1837 0007e8  00 00 3E 	bra gtu,.L71
 1838              	.loc 1 471 0
 1839 0007ea  7E 00 90 	mov [w14+14],w0
 1840 0007ec  E1 0F 50 	sub w0,#1,[w15]
 1841              	.set ___BP___,0
 1842 0007ee  00 00 3A 	bra nz,.L72
 1843              	.loc 1 472 0
 1844 0007f0  2E 00 90 	mov [w14+4],w0
 1845 0007f2  10 00 78 	mov [w0],w0
 1846 0007f4  00 00 88 	mov w0,_SPI1BUF
 1847 0007f6  00 00 37 	bra .L73
 1848              	.L72:
 1849              	.loc 1 474 0
 1850 0007f8  2E 00 90 	mov [w14+4],w0
 1851 0007fa  10 40 78 	mov.b [w0],w0
 1852 0007fc  00 80 FB 	ze w0,w0
 1853 0007fe  00 00 88 	mov w0,_SPI1BUF
 1854              	.L73:
 1855              	.loc 1 476 0
MPLAB XC16 ASSEMBLY Listing:   			page 49


 1856 000800  1E 0F E8 	inc [w14],[w14]
 1857              	.loc 1 475 0
 1858 000802  2E 01 90 	mov [w14+4],w2
 1859              	.loc 1 477 0
 1860 000804  9E 00 90 	mov [w14+2],w1
 1861              	.loc 1 475 0
 1862 000806  6E 00 90 	mov [w14+12],w0
 1863              	.loc 1 477 0
 1864 000808  81 00 E8 	inc w1,w1
 1865              	.loc 1 475 0
 1866 00080a  00 00 41 	add w2,w0,w0
 1867              	.loc 1 477 0
 1868 00080c  11 07 98 	mov w1,[w14+2]
 1869              	.loc 1 475 0
 1870 00080e  20 07 98 	mov w0,[w14+4]
 1871              	.L71:
 1872              	.loc 1 480 0
 1873 000810  01 00 80 	mov _SPI1STATbits,w1
 1874 000812  00 02 20 	mov #32,w0
 1875 000814  00 80 60 	and w1,w0,w0
 1876 000816  00 00 E0 	cp0 w0
 1877              	.set ___BP___,0
 1878 000818  00 00 3A 	bra nz,.L70
 1879              	.loc 1 482 0
 1880 00081a  7E 00 90 	mov [w14+14],w0
 1881 00081c  E1 0F 50 	sub w0,#1,[w15]
 1882              	.set ___BP___,0
 1883 00081e  00 00 3A 	bra nz,.L74
 1884              	.loc 1 483 0
 1885 000820  3E 00 90 	mov [w14+6],w0
 1886 000822  01 00 80 	mov _SPI1BUF,w1
 1887 000824  01 08 78 	mov w1,[w0]
 1888 000826  00 00 37 	bra .L75
 1889              	.L74:
 1890              	.loc 1 485 0
 1891 000828  01 00 80 	mov _SPI1BUF,w1
 1892 00082a  3E 00 90 	mov [w14+6],w0
 1893 00082c  81 40 78 	mov.b w1,w1
 1894 00082e  01 48 78 	mov.b w1,[w0]
 1895              	.L75:
 1896              	.loc 1 486 0
 1897 000830  00 00 00 	nop 
 1898 000832  3E 01 90 	mov [w14+6],w2
 1899              	.loc 1 487 0
 1900 000834  00 00 00 	nop 
 1901 000836  9E 00 90 	mov [w14+2],w1
 1902              	.loc 1 486 0
 1903 000838  00 00 00 	nop 
 1904 00083a  5E 00 90 	mov [w14+10],w0
 1905              	.loc 1 487 0
 1906 00083c  81 00 E9 	dec w1,w1
 1907              	.loc 1 486 0
 1908 00083e  00 00 41 	add w2,w0,w0
 1909              	.loc 1 487 0
 1910 000840  11 07 98 	mov w1,[w14+2]
 1911              	.loc 1 486 0
 1912 000842  30 07 98 	mov w0,[w14+6]
MPLAB XC16 ASSEMBLY Listing:   			page 50


 1913              	.L70:
 1914              	.loc 1 467 0
 1915 000844  00 00 00 	nop 
 1916 000846  3E 08 90 	mov [w14+22],w0
 1917 000848  00 00 00 	nop 
 1918 00084a  9E 00 78 	mov [w14],w1
 1919 00084c  80 8F 50 	sub w1,w0,[w15]
 1920              	.set ___BP___,0
 1921 00084e  00 00 39 	bra ltu,.L76
 1922              	.loc 1 491 0
 1923 000850  00 00 37 	bra .L77
 1924              	.L80:
 1925              	.loc 1 493 0
 1926 000852  01 00 80 	mov _SPI1STATbits,w1
 1927 000854  00 02 20 	mov #32,w0
 1928 000856  00 80 60 	and w1,w0,w0
 1929 000858  00 00 E0 	cp0 w0
 1930              	.set ___BP___,0
 1931 00085a  00 00 3A 	bra nz,.L77
 1932              	.loc 1 495 0
 1933 00085c  7E 00 90 	mov [w14+14],w0
 1934 00085e  E1 0F 50 	sub w0,#1,[w15]
 1935              	.set ___BP___,0
 1936 000860  00 00 3A 	bra nz,.L78
 1937              	.loc 1 496 0
 1938 000862  3E 00 90 	mov [w14+6],w0
 1939 000864  01 00 80 	mov _SPI1BUF,w1
 1940 000866  01 08 78 	mov w1,[w0]
 1941 000868  00 00 37 	bra .L79
 1942              	.L78:
 1943              	.loc 1 498 0
 1944 00086a  01 00 80 	mov _SPI1BUF,w1
 1945 00086c  3E 00 90 	mov [w14+6],w0
 1946 00086e  81 40 78 	mov.b w1,w1
 1947 000870  01 48 78 	mov.b w1,[w0]
 1948              	.L79:
 1949              	.loc 1 499 0
 1950 000872  00 00 00 	nop 
 1951 000874  3E 01 90 	mov [w14+6],w2
 1952              	.loc 1 500 0
 1953 000876  00 00 00 	nop 
 1954 000878  9E 00 90 	mov [w14+2],w1
 1955              	.loc 1 499 0
 1956 00087a  00 00 00 	nop 
 1957 00087c  5E 00 90 	mov [w14+10],w0
 1958              	.loc 1 500 0
 1959 00087e  81 00 E9 	dec w1,w1
 1960              	.loc 1 499 0
 1961 000880  00 00 41 	add w2,w0,w0
 1962              	.loc 1 500 0
 1963 000882  11 07 98 	mov w1,[w14+2]
 1964              	.loc 1 499 0
 1965 000884  30 07 98 	mov w0,[w14+6]
 1966              	.L77:
 1967              	.loc 1 491 0
 1968 000886  00 00 00 	nop 
 1969 000888  1E 00 90 	mov [w14+2],w0
MPLAB XC16 ASSEMBLY Listing:   			page 51


 1970 00088a  00 00 E0 	cp0 w0
 1971              	.set ___BP___,0
 1972 00088c  00 00 3A 	bra nz,.L80
 1973              	.loc 1 504 0
 1974 00088e  1E 00 78 	mov [w14],w0
 1975              	.loc 1 505 0
 1976 000890  8E 07 78 	mov w14,w15
 1977 000892  4F 07 78 	mov [--w15],w14
 1978 000894  00 40 A9 	bclr CORCON,#2
 1979 000896  00 00 06 	return 
 1980              	.set ___PA___,0
 1981              	.LFE13:
 1982              	.size _SPI1_ExchangeBuffer,.-_SPI1_ExchangeBuffer
 1983              	.align 2
 1984              	.global _SPI2_ExchangeBuffer
 1985              	.type _SPI2_ExchangeBuffer,@function
 1986              	_SPI2_ExchangeBuffer:
 1987              	.LFB14:
 1988              	.loc 1 520 0
 1989              	.set ___PA___,1
 1990 000898  1A 00 FA 	lnk #26
 1991              	.LCFI20:
 1992              	.loc 1 520 0
 1993 00089a  20 0F 98 	mov w0,[w14+20]
 1994              	.loc 1 522 0
 1995 00089c  80 03 EB 	clr w7
 1996              	.loc 1 523 0
 1997 00089e  00 03 EB 	clr w6
 1998              	.loc 1 524 0
 1999 0008a0  80 02 EB 	clr w5
 2000              	.loc 1 525 0
 2001 0008a2  F4 0F 20 	mov #255,w4
 2002              	.LBB46:
 2003              	.LBB47:
 2004              	.loc 1 846 0
 2005 0008a4  03 00 80 	mov _SPI2CON1bits,w3
 2006 0008a6  00 40 20 	mov #1024,w0
 2007              	.LBE47:
 2008              	.LBE46:
 2009              	.loc 1 520 0
 2010 0008a8  31 0F 98 	mov w1,[w14+22]
 2011 0008aa  42 0F 98 	mov w2,[w14+24]
 2012              	.loc 1 522 0
 2013 0008ac  07 0F 78 	mov w7,[w14]
 2014              	.loc 1 523 0
 2015 0008ae  16 07 98 	mov w6,[w14+2]
 2016              	.loc 1 524 0
 2017 0008b0  05 0F 98 	mov w5,[w14+16]
 2018              	.loc 1 525 0
 2019 0008b2  14 0F 98 	mov w4,[w14+18]
 2020              	.LBB49:
 2021              	.LBB48:
 2022              	.loc 1 846 0
 2023 0008b4  00 80 61 	and w3,w0,w0
 2024 0008b6  00 00 E0 	cp0 w0
 2025              	.set ___BP___,0
 2026 0008b8  00 00 3A 	bra nz,.L82
MPLAB XC16 ASSEMBLY Listing:   			page 52


 2027              	.loc 1 847 0
 2028 0008ba  00 00 EB 	clr w0
 2029 0008bc  00 00 37 	bra .L83
 2030              	.L82:
 2031              	.loc 1 849 0
 2032 0008be  10 00 20 	mov #1,w0
 2033              	.L83:
 2034              	.loc 1 533 0
 2035 0008c0  70 07 98 	mov w0,[w14+14]
 2036              	.LBE48:
 2037              	.LBE49:
 2038              	.loc 1 535 0
 2039 0008c2  7E 00 90 	mov [w14+14],w0
 2040 0008c4  E1 0F 50 	sub w0,#1,[w15]
 2041              	.set ___BP___,0
 2042 0008c6  00 00 3A 	bra nz,.L84
 2043              	.loc 1 538 0
 2044 0008c8  3E 08 90 	mov [w14+22],w0
 2045              	.loc 1 537 0
 2046 0008ca  21 00 20 	mov #2,w1
 2047              	.loc 1 538 0
 2048 0008cc  00 00 D1 	lsr w0,w0
 2049              	.loc 1 537 0
 2050 0008ce  41 07 98 	mov w1,[w14+8]
 2051              	.loc 1 538 0
 2052 0008d0  30 0F 98 	mov w0,[w14+22]
 2053 0008d2  00 00 37 	bra .L85
 2054              	.L84:
 2055              	.loc 1 542 0
 2056 0008d4  10 00 20 	mov #1,w0
 2057 0008d6  40 07 98 	mov w0,[w14+8]
 2058              	.L85:
 2059              	.loc 1 547 0
 2060 0008d8  2E 08 90 	mov [w14+20],w0
 2061 0008da  00 00 E0 	cp0 w0
 2062              	.set ___BP___,0
 2063 0008dc  00 00 3A 	bra nz,.L86
 2064              	.loc 1 549 0
 2065 0008de  80 00 EB 	clr w1
 2066              	.loc 1 550 0
 2067 0008e0  72 00 47 	add w14,#18,w0
 2068              	.loc 1 549 0
 2069 0008e2  61 07 98 	mov w1,[w14+12]
 2070              	.loc 1 550 0
 2071 0008e4  20 07 98 	mov w0,[w14+4]
 2072 0008e6  00 00 37 	bra .L87
 2073              	.L86:
 2074              	.loc 1 554 0
 2075 0008e8  CE 00 90 	mov [w14+8],w1
 2076 0008ea  61 07 98 	mov w1,[w14+12]
 2077              	.loc 1 555 0
 2078 0008ec  AE 08 90 	mov [w14+20],w1
 2079 0008ee  21 07 98 	mov w1,[w14+4]
 2080              	.L87:
 2081              	.loc 1 558 0
 2082 0008f0  4E 08 90 	mov [w14+24],w0
 2083 0008f2  00 00 E0 	cp0 w0
MPLAB XC16 ASSEMBLY Listing:   			page 53


 2084              	.set ___BP___,0
 2085 0008f4  00 00 3A 	bra nz,.L88
 2086              	.loc 1 560 0
 2087 0008f6  80 00 EB 	clr w1
 2088              	.loc 1 561 0
 2089 0008f8  70 00 47 	add w14,#16,w0
 2090              	.loc 1 560 0
 2091 0008fa  51 07 98 	mov w1,[w14+10]
 2092              	.loc 1 561 0
 2093 0008fc  30 07 98 	mov w0,[w14+6]
 2094              	.loc 1 570 0
 2095 0008fe  00 00 37 	bra .L90
 2096              	.L88:
 2097              	.loc 1 565 0
 2098 000900  CE 00 90 	mov [w14+8],w1
 2099 000902  51 07 98 	mov w1,[w14+10]
 2100              	.loc 1 566 0
 2101 000904  CE 08 90 	mov [w14+24],w1
 2102 000906  31 07 98 	mov w1,[w14+6]
 2103              	.L90:
 2104              	.loc 1 570 0
 2105 000908  00 00 80 	mov _SPI2STATbits,w0
 2106 00090a  62 00 60 	and w0,#2,w0
 2107 00090c  00 00 E0 	cp0 w0
 2108              	.set ___BP___,0
 2109 00090e  00 00 3A 	bra nz,.L90
 2110              	.loc 1 575 0
 2111 000910  00 00 37 	bra .L91
 2112              	.L97:
 2113              	.loc 1 577 0
 2114 000912  1E 00 90 	mov [w14+2],w0
 2115 000914  E7 0F 50 	sub w0,#7,[w15]
 2116              	.set ___BP___,0
 2117 000916  00 00 3E 	bra gtu,.L92
 2118              	.loc 1 579 0
 2119 000918  7E 00 90 	mov [w14+14],w0
 2120 00091a  E1 0F 50 	sub w0,#1,[w15]
 2121              	.set ___BP___,0
 2122 00091c  00 00 3A 	bra nz,.L93
 2123              	.loc 1 580 0
 2124 00091e  2E 00 90 	mov [w14+4],w0
 2125 000920  10 00 78 	mov [w0],w0
 2126 000922  00 00 88 	mov w0,_SPI2BUF
 2127 000924  00 00 37 	bra .L94
 2128              	.L93:
 2129              	.loc 1 582 0
 2130 000926  2E 00 90 	mov [w14+4],w0
 2131 000928  10 40 78 	mov.b [w0],w0
 2132 00092a  00 80 FB 	ze w0,w0
 2133 00092c  00 00 88 	mov w0,_SPI2BUF
 2134              	.L94:
 2135              	.loc 1 584 0
 2136 00092e  1E 0F E8 	inc [w14],[w14]
 2137              	.loc 1 583 0
 2138 000930  2E 01 90 	mov [w14+4],w2
 2139              	.loc 1 585 0
 2140 000932  9E 00 90 	mov [w14+2],w1
MPLAB XC16 ASSEMBLY Listing:   			page 54


 2141              	.loc 1 583 0
 2142 000934  6E 00 90 	mov [w14+12],w0
 2143              	.loc 1 585 0
 2144 000936  81 00 E8 	inc w1,w1
 2145              	.loc 1 583 0
 2146 000938  00 00 41 	add w2,w0,w0
 2147              	.loc 1 585 0
 2148 00093a  11 07 98 	mov w1,[w14+2]
 2149              	.loc 1 583 0
 2150 00093c  20 07 98 	mov w0,[w14+4]
 2151              	.L92:
 2152              	.loc 1 588 0
 2153 00093e  01 00 80 	mov _SPI2STATbits,w1
 2154 000940  00 02 20 	mov #32,w0
 2155 000942  00 80 60 	and w1,w0,w0
 2156 000944  00 00 E0 	cp0 w0
 2157              	.set ___BP___,0
 2158 000946  00 00 3A 	bra nz,.L91
 2159              	.loc 1 590 0
 2160 000948  7E 00 90 	mov [w14+14],w0
 2161 00094a  E1 0F 50 	sub w0,#1,[w15]
 2162              	.set ___BP___,0
 2163 00094c  00 00 3A 	bra nz,.L95
 2164              	.loc 1 591 0
 2165 00094e  3E 00 90 	mov [w14+6],w0
 2166 000950  01 00 80 	mov _SPI2BUF,w1
 2167 000952  01 08 78 	mov w1,[w0]
 2168 000954  00 00 37 	bra .L96
 2169              	.L95:
 2170              	.loc 1 593 0
 2171 000956  01 00 80 	mov _SPI2BUF,w1
 2172 000958  3E 00 90 	mov [w14+6],w0
 2173 00095a  81 40 78 	mov.b w1,w1
 2174 00095c  01 48 78 	mov.b w1,[w0]
 2175              	.L96:
 2176              	.loc 1 594 0
 2177 00095e  00 00 00 	nop 
 2178 000960  3E 01 90 	mov [w14+6],w2
 2179              	.loc 1 595 0
 2180 000962  00 00 00 	nop 
 2181 000964  9E 00 90 	mov [w14+2],w1
 2182              	.loc 1 594 0
 2183 000966  00 00 00 	nop 
 2184 000968  5E 00 90 	mov [w14+10],w0
 2185              	.loc 1 595 0
 2186 00096a  81 00 E9 	dec w1,w1
 2187              	.loc 1 594 0
 2188 00096c  00 00 41 	add w2,w0,w0
 2189              	.loc 1 595 0
 2190 00096e  11 07 98 	mov w1,[w14+2]
 2191              	.loc 1 594 0
 2192 000970  30 07 98 	mov w0,[w14+6]
 2193              	.L91:
 2194              	.loc 1 575 0
 2195 000972  00 00 00 	nop 
 2196 000974  3E 08 90 	mov [w14+22],w0
 2197 000976  00 00 00 	nop 
MPLAB XC16 ASSEMBLY Listing:   			page 55


 2198 000978  9E 00 78 	mov [w14],w1
 2199 00097a  80 8F 50 	sub w1,w0,[w15]
 2200              	.set ___BP___,0
 2201 00097c  00 00 39 	bra ltu,.L97
 2202              	.loc 1 599 0
 2203 00097e  00 00 37 	bra .L98
 2204              	.L101:
 2205              	.loc 1 601 0
 2206 000980  01 00 80 	mov _SPI2STATbits,w1
 2207 000982  00 02 20 	mov #32,w0
 2208 000984  00 80 60 	and w1,w0,w0
 2209 000986  00 00 E0 	cp0 w0
 2210              	.set ___BP___,0
 2211 000988  00 00 3A 	bra nz,.L98
 2212              	.loc 1 603 0
 2213 00098a  7E 00 90 	mov [w14+14],w0
 2214 00098c  E1 0F 50 	sub w0,#1,[w15]
 2215              	.set ___BP___,0
 2216 00098e  00 00 3A 	bra nz,.L99
 2217              	.loc 1 604 0
 2218 000990  3E 00 90 	mov [w14+6],w0
 2219 000992  01 00 80 	mov _SPI2BUF,w1
 2220 000994  01 08 78 	mov w1,[w0]
 2221 000996  00 00 37 	bra .L100
 2222              	.L99:
 2223              	.loc 1 606 0
 2224 000998  01 00 80 	mov _SPI2BUF,w1
 2225 00099a  3E 00 90 	mov [w14+6],w0
 2226 00099c  81 40 78 	mov.b w1,w1
 2227 00099e  01 48 78 	mov.b w1,[w0]
 2228              	.L100:
 2229              	.loc 1 607 0
 2230 0009a0  00 00 00 	nop 
 2231 0009a2  3E 01 90 	mov [w14+6],w2
 2232              	.loc 1 608 0
 2233 0009a4  00 00 00 	nop 
 2234 0009a6  9E 00 90 	mov [w14+2],w1
 2235              	.loc 1 607 0
 2236 0009a8  00 00 00 	nop 
 2237 0009aa  5E 00 90 	mov [w14+10],w0
 2238              	.loc 1 608 0
 2239 0009ac  81 00 E9 	dec w1,w1
 2240              	.loc 1 607 0
 2241 0009ae  00 00 41 	add w2,w0,w0
 2242              	.loc 1 608 0
 2243 0009b0  11 07 98 	mov w1,[w14+2]
 2244              	.loc 1 607 0
 2245 0009b2  30 07 98 	mov w0,[w14+6]
 2246              	.L98:
 2247              	.loc 1 599 0
 2248 0009b4  00 00 00 	nop 
 2249 0009b6  1E 00 90 	mov [w14+2],w0
 2250 0009b8  00 00 E0 	cp0 w0
 2251              	.set ___BP___,0
 2252 0009ba  00 00 3A 	bra nz,.L101
 2253              	.loc 1 612 0
 2254 0009bc  1E 00 78 	mov [w14],w0
MPLAB XC16 ASSEMBLY Listing:   			page 56


 2255              	.loc 1 613 0
 2256 0009be  8E 07 78 	mov w14,w15
 2257 0009c0  4F 07 78 	mov [--w15],w14
 2258 0009c2  00 40 A9 	bclr CORCON,#2
 2259 0009c4  00 00 06 	return 
 2260              	.set ___PA___,0
 2261              	.LFE14:
 2262              	.size _SPI2_ExchangeBuffer,.-_SPI2_ExchangeBuffer
 2263              	.align 2
 2264              	.global _SPI3_ExchangeBuffer
 2265              	.type _SPI3_ExchangeBuffer,@function
 2266              	_SPI3_ExchangeBuffer:
 2267              	.LFB15:
 2268              	.loc 1 627 0
 2269              	.set ___PA___,1
 2270 0009c6  1A 00 FA 	lnk #26
 2271              	.LCFI21:
 2272              	.loc 1 627 0
 2273 0009c8  20 0F 98 	mov w0,[w14+20]
 2274              	.loc 1 629 0
 2275 0009ca  80 03 EB 	clr w7
 2276              	.loc 1 630 0
 2277 0009cc  00 03 EB 	clr w6
 2278              	.loc 1 631 0
 2279 0009ce  80 02 EB 	clr w5
 2280              	.loc 1 632 0
 2281 0009d0  F4 0F 20 	mov #255,w4
 2282              	.LBB50:
 2283              	.LBB51:
 2284              	.loc 1 858 0
 2285 0009d2  03 00 80 	mov _SPI3CON1bits,w3
 2286 0009d4  00 40 20 	mov #1024,w0
 2287              	.LBE51:
 2288              	.LBE50:
 2289              	.loc 1 627 0
 2290 0009d6  31 0F 98 	mov w1,[w14+22]
 2291 0009d8  42 0F 98 	mov w2,[w14+24]
 2292              	.loc 1 629 0
 2293 0009da  07 0F 78 	mov w7,[w14]
 2294              	.loc 1 630 0
 2295 0009dc  16 07 98 	mov w6,[w14+2]
 2296              	.loc 1 631 0
 2297 0009de  05 0F 98 	mov w5,[w14+16]
 2298              	.loc 1 632 0
 2299 0009e0  14 0F 98 	mov w4,[w14+18]
 2300              	.LBB53:
 2301              	.LBB52:
 2302              	.loc 1 858 0
 2303 0009e2  00 80 61 	and w3,w0,w0
 2304 0009e4  00 00 E0 	cp0 w0
 2305              	.set ___BP___,0
 2306 0009e6  00 00 3A 	bra nz,.L103
 2307              	.loc 1 859 0
 2308 0009e8  00 00 EB 	clr w0
 2309 0009ea  00 00 37 	bra .L104
 2310              	.L103:
 2311              	.loc 1 861 0
MPLAB XC16 ASSEMBLY Listing:   			page 57


 2312 0009ec  10 00 20 	mov #1,w0
 2313              	.L104:
 2314              	.loc 1 640 0
 2315 0009ee  70 07 98 	mov w0,[w14+14]
 2316              	.LBE52:
 2317              	.LBE53:
 2318              	.loc 1 642 0
 2319 0009f0  7E 00 90 	mov [w14+14],w0
 2320 0009f2  E1 0F 50 	sub w0,#1,[w15]
 2321              	.set ___BP___,0
 2322 0009f4  00 00 3A 	bra nz,.L105
 2323              	.loc 1 645 0
 2324 0009f6  3E 08 90 	mov [w14+22],w0
 2325              	.loc 1 644 0
 2326 0009f8  21 00 20 	mov #2,w1
 2327              	.loc 1 645 0
 2328 0009fa  00 00 D1 	lsr w0,w0
 2329              	.loc 1 644 0
 2330 0009fc  41 07 98 	mov w1,[w14+8]
 2331              	.loc 1 645 0
 2332 0009fe  30 0F 98 	mov w0,[w14+22]
 2333 000a00  00 00 37 	bra .L106
 2334              	.L105:
 2335              	.loc 1 649 0
 2336 000a02  10 00 20 	mov #1,w0
 2337 000a04  40 07 98 	mov w0,[w14+8]
 2338              	.L106:
 2339              	.loc 1 654 0
 2340 000a06  2E 08 90 	mov [w14+20],w0
 2341 000a08  00 00 E0 	cp0 w0
 2342              	.set ___BP___,0
 2343 000a0a  00 00 3A 	bra nz,.L107
 2344              	.loc 1 656 0
 2345 000a0c  80 00 EB 	clr w1
 2346              	.loc 1 657 0
 2347 000a0e  72 00 47 	add w14,#18,w0
 2348              	.loc 1 656 0
 2349 000a10  61 07 98 	mov w1,[w14+12]
 2350              	.loc 1 657 0
 2351 000a12  20 07 98 	mov w0,[w14+4]
 2352 000a14  00 00 37 	bra .L108
 2353              	.L107:
 2354              	.loc 1 661 0
 2355 000a16  CE 00 90 	mov [w14+8],w1
 2356 000a18  61 07 98 	mov w1,[w14+12]
 2357              	.loc 1 662 0
 2358 000a1a  AE 08 90 	mov [w14+20],w1
 2359 000a1c  21 07 98 	mov w1,[w14+4]
 2360              	.L108:
 2361              	.loc 1 665 0
 2362 000a1e  4E 08 90 	mov [w14+24],w0
 2363 000a20  00 00 E0 	cp0 w0
 2364              	.set ___BP___,0
 2365 000a22  00 00 3A 	bra nz,.L109
 2366              	.loc 1 667 0
 2367 000a24  80 00 EB 	clr w1
 2368              	.loc 1 668 0
MPLAB XC16 ASSEMBLY Listing:   			page 58


 2369 000a26  70 00 47 	add w14,#16,w0
 2370              	.loc 1 667 0
 2371 000a28  51 07 98 	mov w1,[w14+10]
 2372              	.loc 1 668 0
 2373 000a2a  30 07 98 	mov w0,[w14+6]
 2374              	.loc 1 677 0
 2375 000a2c  00 00 37 	bra .L111
 2376              	.L109:
 2377              	.loc 1 672 0
 2378 000a2e  CE 00 90 	mov [w14+8],w1
 2379 000a30  51 07 98 	mov w1,[w14+10]
 2380              	.loc 1 673 0
 2381 000a32  CE 08 90 	mov [w14+24],w1
 2382 000a34  31 07 98 	mov w1,[w14+6]
 2383              	.L111:
 2384              	.loc 1 677 0
 2385 000a36  00 00 80 	mov _SPI3STATbits,w0
 2386 000a38  62 00 60 	and w0,#2,w0
 2387 000a3a  00 00 E0 	cp0 w0
 2388              	.set ___BP___,0
 2389 000a3c  00 00 3A 	bra nz,.L111
 2390              	.loc 1 682 0
 2391 000a3e  00 00 37 	bra .L112
 2392              	.L118:
 2393              	.loc 1 684 0
 2394 000a40  1E 00 90 	mov [w14+2],w0
 2395 000a42  E7 0F 50 	sub w0,#7,[w15]
 2396              	.set ___BP___,0
 2397 000a44  00 00 3E 	bra gtu,.L113
 2398              	.loc 1 686 0
 2399 000a46  7E 00 90 	mov [w14+14],w0
 2400 000a48  E1 0F 50 	sub w0,#1,[w15]
 2401              	.set ___BP___,0
 2402 000a4a  00 00 3A 	bra nz,.L114
 2403              	.loc 1 687 0
 2404 000a4c  2E 00 90 	mov [w14+4],w0
 2405 000a4e  10 00 78 	mov [w0],w0
 2406 000a50  00 00 88 	mov w0,_SPI3BUF
 2407 000a52  00 00 37 	bra .L115
 2408              	.L114:
 2409              	.loc 1 689 0
 2410 000a54  2E 00 90 	mov [w14+4],w0
 2411 000a56  10 40 78 	mov.b [w0],w0
 2412 000a58  00 80 FB 	ze w0,w0
 2413 000a5a  00 00 88 	mov w0,_SPI3BUF
 2414              	.L115:
 2415              	.loc 1 691 0
 2416 000a5c  1E 0F E8 	inc [w14],[w14]
 2417              	.loc 1 690 0
 2418 000a5e  2E 01 90 	mov [w14+4],w2
 2419              	.loc 1 692 0
 2420 000a60  9E 00 90 	mov [w14+2],w1
 2421              	.loc 1 690 0
 2422 000a62  6E 00 90 	mov [w14+12],w0
 2423              	.loc 1 692 0
 2424 000a64  81 00 E8 	inc w1,w1
 2425              	.loc 1 690 0
MPLAB XC16 ASSEMBLY Listing:   			page 59


 2426 000a66  00 00 41 	add w2,w0,w0
 2427              	.loc 1 692 0
 2428 000a68  11 07 98 	mov w1,[w14+2]
 2429              	.loc 1 690 0
 2430 000a6a  20 07 98 	mov w0,[w14+4]
 2431              	.L113:
 2432              	.loc 1 695 0
 2433 000a6c  01 00 80 	mov _SPI3STATbits,w1
 2434 000a6e  00 02 20 	mov #32,w0
 2435 000a70  00 80 60 	and w1,w0,w0
 2436 000a72  00 00 E0 	cp0 w0
 2437              	.set ___BP___,0
 2438 000a74  00 00 3A 	bra nz,.L112
 2439              	.loc 1 697 0
 2440 000a76  7E 00 90 	mov [w14+14],w0
 2441 000a78  E1 0F 50 	sub w0,#1,[w15]
 2442              	.set ___BP___,0
 2443 000a7a  00 00 3A 	bra nz,.L116
 2444              	.loc 1 698 0
 2445 000a7c  3E 00 90 	mov [w14+6],w0
 2446 000a7e  01 00 80 	mov _SPI3BUF,w1
 2447 000a80  01 08 78 	mov w1,[w0]
 2448 000a82  00 00 37 	bra .L117
 2449              	.L116:
 2450              	.loc 1 700 0
 2451 000a84  01 00 80 	mov _SPI3BUF,w1
 2452 000a86  3E 00 90 	mov [w14+6],w0
 2453 000a88  81 40 78 	mov.b w1,w1
 2454 000a8a  01 48 78 	mov.b w1,[w0]
 2455              	.L117:
 2456              	.loc 1 701 0
 2457 000a8c  00 00 00 	nop 
 2458 000a8e  3E 01 90 	mov [w14+6],w2
 2459              	.loc 1 702 0
 2460 000a90  00 00 00 	nop 
 2461 000a92  9E 00 90 	mov [w14+2],w1
 2462              	.loc 1 701 0
 2463 000a94  00 00 00 	nop 
 2464 000a96  5E 00 90 	mov [w14+10],w0
 2465              	.loc 1 702 0
 2466 000a98  81 00 E9 	dec w1,w1
 2467              	.loc 1 701 0
 2468 000a9a  00 00 41 	add w2,w0,w0
 2469              	.loc 1 702 0
 2470 000a9c  11 07 98 	mov w1,[w14+2]
 2471              	.loc 1 701 0
 2472 000a9e  30 07 98 	mov w0,[w14+6]
 2473              	.L112:
 2474              	.loc 1 682 0
 2475 000aa0  00 00 00 	nop 
 2476 000aa2  3E 08 90 	mov [w14+22],w0
 2477 000aa4  00 00 00 	nop 
 2478 000aa6  9E 00 78 	mov [w14],w1
 2479 000aa8  80 8F 50 	sub w1,w0,[w15]
 2480              	.set ___BP___,0
 2481 000aaa  00 00 39 	bra ltu,.L118
 2482              	.loc 1 706 0
MPLAB XC16 ASSEMBLY Listing:   			page 60


 2483 000aac  00 00 37 	bra .L119
 2484              	.L122:
 2485              	.loc 1 708 0
 2486 000aae  01 00 80 	mov _SPI3STATbits,w1
 2487 000ab0  00 02 20 	mov #32,w0
 2488 000ab2  00 80 60 	and w1,w0,w0
 2489 000ab4  00 00 E0 	cp0 w0
 2490              	.set ___BP___,0
 2491 000ab6  00 00 3A 	bra nz,.L119
 2492              	.loc 1 710 0
 2493 000ab8  7E 00 90 	mov [w14+14],w0
 2494 000aba  E1 0F 50 	sub w0,#1,[w15]
 2495              	.set ___BP___,0
 2496 000abc  00 00 3A 	bra nz,.L120
 2497              	.loc 1 711 0
 2498 000abe  3E 00 90 	mov [w14+6],w0
 2499 000ac0  01 00 80 	mov _SPI3BUF,w1
 2500 000ac2  01 08 78 	mov w1,[w0]
 2501 000ac4  00 00 37 	bra .L121
 2502              	.L120:
 2503              	.loc 1 713 0
 2504 000ac6  01 00 80 	mov _SPI3BUF,w1
 2505 000ac8  3E 00 90 	mov [w14+6],w0
 2506 000aca  81 40 78 	mov.b w1,w1
 2507 000acc  01 48 78 	mov.b w1,[w0]
 2508              	.L121:
 2509              	.loc 1 714 0
 2510 000ace  00 00 00 	nop 
 2511 000ad0  3E 01 90 	mov [w14+6],w2
 2512              	.loc 1 715 0
 2513 000ad2  00 00 00 	nop 
 2514 000ad4  9E 00 90 	mov [w14+2],w1
 2515              	.loc 1 714 0
 2516 000ad6  00 00 00 	nop 
 2517 000ad8  5E 00 90 	mov [w14+10],w0
 2518              	.loc 1 715 0
 2519 000ada  81 00 E9 	dec w1,w1
 2520              	.loc 1 714 0
 2521 000adc  00 00 41 	add w2,w0,w0
 2522              	.loc 1 715 0
 2523 000ade  11 07 98 	mov w1,[w14+2]
 2524              	.loc 1 714 0
 2525 000ae0  30 07 98 	mov w0,[w14+6]
 2526              	.L119:
 2527              	.loc 1 706 0
 2528 000ae2  00 00 00 	nop 
 2529 000ae4  1E 00 90 	mov [w14+2],w0
 2530 000ae6  00 00 E0 	cp0 w0
 2531              	.set ___BP___,0
 2532 000ae8  00 00 3A 	bra nz,.L122
 2533              	.loc 1 719 0
 2534 000aea  1E 00 78 	mov [w14],w0
 2535              	.loc 1 720 0
 2536 000aec  8E 07 78 	mov w14,w15
 2537 000aee  4F 07 78 	mov [--w15],w14
 2538 000af0  00 40 A9 	bclr CORCON,#2
 2539 000af2  00 00 06 	return 
MPLAB XC16 ASSEMBLY Listing:   			page 61


 2540              	.set ___PA___,0
 2541              	.LFE15:
 2542              	.size _SPI3_ExchangeBuffer,.-_SPI3_ExchangeBuffer
 2543              	.align 2
 2544              	.global _SPI4_ExchangeBuffer
 2545              	.type _SPI4_ExchangeBuffer,@function
 2546              	_SPI4_ExchangeBuffer:
 2547              	.LFB16:
 2548              	.loc 1 734 0
 2549              	.set ___PA___,1
 2550 000af4  1A 00 FA 	lnk #26
 2551              	.LCFI22:
 2552              	.loc 1 734 0
 2553 000af6  20 0F 98 	mov w0,[w14+20]
 2554              	.loc 1 736 0
 2555 000af8  80 03 EB 	clr w7
 2556              	.loc 1 737 0
 2557 000afa  00 03 EB 	clr w6
 2558              	.loc 1 738 0
 2559 000afc  80 02 EB 	clr w5
 2560              	.loc 1 739 0
 2561 000afe  F4 0F 20 	mov #255,w4
 2562              	.LBB54:
 2563              	.LBB55:
 2564              	.loc 1 870 0
 2565 000b00  03 00 80 	mov _SPI4CON1bits,w3
 2566 000b02  00 40 20 	mov #1024,w0
 2567              	.LBE55:
 2568              	.LBE54:
 2569              	.loc 1 734 0
 2570 000b04  31 0F 98 	mov w1,[w14+22]
 2571 000b06  42 0F 98 	mov w2,[w14+24]
 2572              	.loc 1 736 0
 2573 000b08  07 0F 78 	mov w7,[w14]
 2574              	.loc 1 737 0
 2575 000b0a  16 07 98 	mov w6,[w14+2]
 2576              	.loc 1 738 0
 2577 000b0c  05 0F 98 	mov w5,[w14+16]
 2578              	.loc 1 739 0
 2579 000b0e  14 0F 98 	mov w4,[w14+18]
 2580              	.LBB57:
 2581              	.LBB56:
 2582              	.loc 1 870 0
 2583 000b10  00 80 61 	and w3,w0,w0
 2584 000b12  00 00 E0 	cp0 w0
 2585              	.set ___BP___,0
 2586 000b14  00 00 3A 	bra nz,.L124
 2587              	.loc 1 871 0
 2588 000b16  00 00 EB 	clr w0
 2589 000b18  00 00 37 	bra .L125
 2590              	.L124:
 2591              	.loc 1 873 0
 2592 000b1a  10 00 20 	mov #1,w0
 2593              	.L125:
 2594              	.loc 1 747 0
 2595 000b1c  70 07 98 	mov w0,[w14+14]
 2596              	.LBE56:
MPLAB XC16 ASSEMBLY Listing:   			page 62


 2597              	.LBE57:
 2598              	.loc 1 749 0
 2599 000b1e  7E 00 90 	mov [w14+14],w0
 2600 000b20  E1 0F 50 	sub w0,#1,[w15]
 2601              	.set ___BP___,0
 2602 000b22  00 00 3A 	bra nz,.L126
 2603              	.loc 1 752 0
 2604 000b24  3E 08 90 	mov [w14+22],w0
 2605              	.loc 1 751 0
 2606 000b26  21 00 20 	mov #2,w1
 2607              	.loc 1 752 0
 2608 000b28  00 00 D1 	lsr w0,w0
 2609              	.loc 1 751 0
 2610 000b2a  41 07 98 	mov w1,[w14+8]
 2611              	.loc 1 752 0
 2612 000b2c  30 0F 98 	mov w0,[w14+22]
 2613 000b2e  00 00 37 	bra .L127
 2614              	.L126:
 2615              	.loc 1 756 0
 2616 000b30  10 00 20 	mov #1,w0
 2617 000b32  40 07 98 	mov w0,[w14+8]
 2618              	.L127:
 2619              	.loc 1 761 0
 2620 000b34  2E 08 90 	mov [w14+20],w0
 2621 000b36  00 00 E0 	cp0 w0
 2622              	.set ___BP___,0
 2623 000b38  00 00 3A 	bra nz,.L128
 2624              	.loc 1 763 0
 2625 000b3a  80 00 EB 	clr w1
 2626              	.loc 1 764 0
 2627 000b3c  72 00 47 	add w14,#18,w0
 2628              	.loc 1 763 0
 2629 000b3e  61 07 98 	mov w1,[w14+12]
 2630              	.loc 1 764 0
 2631 000b40  20 07 98 	mov w0,[w14+4]
 2632 000b42  00 00 37 	bra .L129
 2633              	.L128:
 2634              	.loc 1 768 0
 2635 000b44  CE 00 90 	mov [w14+8],w1
 2636 000b46  61 07 98 	mov w1,[w14+12]
 2637              	.loc 1 769 0
 2638 000b48  AE 08 90 	mov [w14+20],w1
 2639 000b4a  21 07 98 	mov w1,[w14+4]
 2640              	.L129:
 2641              	.loc 1 772 0
 2642 000b4c  4E 08 90 	mov [w14+24],w0
 2643 000b4e  00 00 E0 	cp0 w0
 2644              	.set ___BP___,0
 2645 000b50  00 00 3A 	bra nz,.L130
 2646              	.loc 1 774 0
 2647 000b52  80 00 EB 	clr w1
 2648              	.loc 1 775 0
 2649 000b54  70 00 47 	add w14,#16,w0
 2650              	.loc 1 774 0
 2651 000b56  51 07 98 	mov w1,[w14+10]
 2652              	.loc 1 775 0
 2653 000b58  30 07 98 	mov w0,[w14+6]
MPLAB XC16 ASSEMBLY Listing:   			page 63


 2654              	.loc 1 784 0
 2655 000b5a  00 00 37 	bra .L132
 2656              	.L130:
 2657              	.loc 1 779 0
 2658 000b5c  CE 00 90 	mov [w14+8],w1
 2659 000b5e  51 07 98 	mov w1,[w14+10]
 2660              	.loc 1 780 0
 2661 000b60  CE 08 90 	mov [w14+24],w1
 2662 000b62  31 07 98 	mov w1,[w14+6]
 2663              	.L132:
 2664              	.loc 1 784 0
 2665 000b64  00 00 80 	mov _SPI4STATbits,w0
 2666 000b66  62 00 60 	and w0,#2,w0
 2667 000b68  00 00 E0 	cp0 w0
 2668              	.set ___BP___,0
 2669 000b6a  00 00 3A 	bra nz,.L132
 2670              	.loc 1 789 0
 2671 000b6c  00 00 37 	bra .L133
 2672              	.L139:
 2673              	.loc 1 791 0
 2674 000b6e  1E 00 90 	mov [w14+2],w0
 2675 000b70  E7 0F 50 	sub w0,#7,[w15]
 2676              	.set ___BP___,0
 2677 000b72  00 00 3E 	bra gtu,.L134
 2678              	.loc 1 793 0
 2679 000b74  7E 00 90 	mov [w14+14],w0
 2680 000b76  E1 0F 50 	sub w0,#1,[w15]
 2681              	.set ___BP___,0
 2682 000b78  00 00 3A 	bra nz,.L135
 2683              	.loc 1 794 0
 2684 000b7a  2E 00 90 	mov [w14+4],w0
 2685 000b7c  10 00 78 	mov [w0],w0
 2686 000b7e  00 00 88 	mov w0,_SPI4BUF
 2687 000b80  00 00 37 	bra .L136
 2688              	.L135:
 2689              	.loc 1 796 0
 2690 000b82  2E 00 90 	mov [w14+4],w0
 2691 000b84  10 40 78 	mov.b [w0],w0
 2692 000b86  00 80 FB 	ze w0,w0
 2693 000b88  00 00 88 	mov w0,_SPI4BUF
 2694              	.L136:
 2695              	.loc 1 798 0
 2696 000b8a  1E 0F E8 	inc [w14],[w14]
 2697              	.loc 1 797 0
 2698 000b8c  2E 01 90 	mov [w14+4],w2
 2699              	.loc 1 799 0
 2700 000b8e  9E 00 90 	mov [w14+2],w1
 2701              	.loc 1 797 0
 2702 000b90  6E 00 90 	mov [w14+12],w0
 2703              	.loc 1 799 0
 2704 000b92  81 00 E8 	inc w1,w1
 2705              	.loc 1 797 0
 2706 000b94  00 00 41 	add w2,w0,w0
 2707              	.loc 1 799 0
 2708 000b96  11 07 98 	mov w1,[w14+2]
 2709              	.loc 1 797 0
 2710 000b98  20 07 98 	mov w0,[w14+4]
MPLAB XC16 ASSEMBLY Listing:   			page 64


 2711              	.L134:
 2712              	.loc 1 802 0
 2713 000b9a  01 00 80 	mov _SPI4STATbits,w1
 2714 000b9c  00 02 20 	mov #32,w0
 2715 000b9e  00 80 60 	and w1,w0,w0
 2716 000ba0  00 00 E0 	cp0 w0
 2717              	.set ___BP___,0
 2718 000ba2  00 00 3A 	bra nz,.L133
 2719              	.loc 1 804 0
 2720 000ba4  7E 00 90 	mov [w14+14],w0
 2721 000ba6  E1 0F 50 	sub w0,#1,[w15]
 2722              	.set ___BP___,0
 2723 000ba8  00 00 3A 	bra nz,.L137
 2724              	.loc 1 805 0
 2725 000baa  3E 00 90 	mov [w14+6],w0
 2726 000bac  01 00 80 	mov _SPI4BUF,w1
 2727 000bae  01 08 78 	mov w1,[w0]
 2728 000bb0  00 00 37 	bra .L138
 2729              	.L137:
 2730              	.loc 1 807 0
 2731 000bb2  01 00 80 	mov _SPI4BUF,w1
 2732 000bb4  3E 00 90 	mov [w14+6],w0
 2733 000bb6  81 40 78 	mov.b w1,w1
 2734 000bb8  01 48 78 	mov.b w1,[w0]
 2735              	.L138:
 2736              	.loc 1 808 0
 2737 000bba  00 00 00 	nop 
 2738 000bbc  3E 01 90 	mov [w14+6],w2
 2739              	.loc 1 809 0
 2740 000bbe  00 00 00 	nop 
 2741 000bc0  9E 00 90 	mov [w14+2],w1
 2742              	.loc 1 808 0
 2743 000bc2  00 00 00 	nop 
 2744 000bc4  5E 00 90 	mov [w14+10],w0
 2745              	.loc 1 809 0
 2746 000bc6  81 00 E9 	dec w1,w1
 2747              	.loc 1 808 0
 2748 000bc8  00 00 41 	add w2,w0,w0
 2749              	.loc 1 809 0
 2750 000bca  11 07 98 	mov w1,[w14+2]
 2751              	.loc 1 808 0
 2752 000bcc  30 07 98 	mov w0,[w14+6]
 2753              	.L133:
 2754              	.loc 1 789 0
 2755 000bce  00 00 00 	nop 
 2756 000bd0  3E 08 90 	mov [w14+22],w0
 2757 000bd2  00 00 00 	nop 
 2758 000bd4  9E 00 78 	mov [w14],w1
 2759 000bd6  80 8F 50 	sub w1,w0,[w15]
 2760              	.set ___BP___,0
 2761 000bd8  00 00 39 	bra ltu,.L139
 2762              	.loc 1 813 0
 2763 000bda  00 00 37 	bra .L140
 2764              	.L143:
 2765              	.loc 1 815 0
 2766 000bdc  01 00 80 	mov _SPI4STATbits,w1
 2767 000bde  00 02 20 	mov #32,w0
MPLAB XC16 ASSEMBLY Listing:   			page 65


 2768 000be0  00 80 60 	and w1,w0,w0
 2769 000be2  00 00 E0 	cp0 w0
 2770              	.set ___BP___,0
 2771 000be4  00 00 3A 	bra nz,.L140
 2772              	.loc 1 817 0
 2773 000be6  7E 00 90 	mov [w14+14],w0
 2774 000be8  E1 0F 50 	sub w0,#1,[w15]
 2775              	.set ___BP___,0
 2776 000bea  00 00 3A 	bra nz,.L141
 2777              	.loc 1 818 0
 2778 000bec  3E 00 90 	mov [w14+6],w0
 2779 000bee  01 00 80 	mov _SPI4BUF,w1
 2780 000bf0  01 08 78 	mov w1,[w0]
 2781 000bf2  00 00 37 	bra .L142
 2782              	.L141:
 2783              	.loc 1 820 0
 2784 000bf4  01 00 80 	mov _SPI4BUF,w1
 2785 000bf6  3E 00 90 	mov [w14+6],w0
 2786 000bf8  81 40 78 	mov.b w1,w1
 2787 000bfa  01 48 78 	mov.b w1,[w0]
 2788              	.L142:
 2789              	.loc 1 821 0
 2790 000bfc  00 00 00 	nop 
 2791 000bfe  3E 01 90 	mov [w14+6],w2
 2792              	.loc 1 822 0
 2793 000c00  00 00 00 	nop 
 2794 000c02  9E 00 90 	mov [w14+2],w1
 2795              	.loc 1 821 0
 2796 000c04  00 00 00 	nop 
 2797 000c06  5E 00 90 	mov [w14+10],w0
 2798              	.loc 1 822 0
 2799 000c08  81 00 E9 	dec w1,w1
 2800              	.loc 1 821 0
 2801 000c0a  00 00 41 	add w2,w0,w0
 2802              	.loc 1 822 0
 2803 000c0c  11 07 98 	mov w1,[w14+2]
 2804              	.loc 1 821 0
 2805 000c0e  30 07 98 	mov w0,[w14+6]
 2806              	.L140:
 2807              	.loc 1 813 0
 2808 000c10  00 00 00 	nop 
 2809 000c12  1E 00 90 	mov [w14+2],w0
 2810 000c14  00 00 E0 	cp0 w0
 2811              	.set ___BP___,0
 2812 000c16  00 00 3A 	bra nz,.L143
 2813              	.loc 1 826 0
 2814 000c18  1E 00 78 	mov [w14],w0
 2815              	.loc 1 827 0
 2816 000c1a  8E 07 78 	mov w14,w15
 2817 000c1c  4F 07 78 	mov [--w15],w14
 2818 000c1e  00 40 A9 	bclr CORCON,#2
 2819 000c20  00 00 06 	return 
 2820              	.set ___PA___,0
 2821              	.LFE16:
 2822              	.size _SPI4_ExchangeBuffer,.-_SPI4_ExchangeBuffer
 2823              	.align 2
 2824              	.global _SPI1_TransferModeGet
MPLAB XC16 ASSEMBLY Listing:   			page 66


 2825              	.type _SPI1_TransferModeGet,@function
 2826              	_SPI1_TransferModeGet:
 2827              	.LFB17:
 2828              	.loc 1 833 0
 2829              	.set ___PA___,1
 2830 000c22  00 00 FA 	lnk #0
 2831              	.LCFI23:
 2832              	.loc 1 834 0
 2833 000c24  01 00 80 	mov _SPI1CON1bits,w1
 2834 000c26  00 40 20 	mov #1024,w0
 2835 000c28  00 80 60 	and w1,w0,w0
 2836 000c2a  00 00 E0 	cp0 w0
 2837              	.set ___BP___,0
 2838 000c2c  00 00 3A 	bra nz,.L145
 2839              	.loc 1 835 0
 2840 000c2e  00 00 EB 	clr w0
 2841 000c30  00 00 37 	bra .L146
 2842              	.L145:
 2843              	.loc 1 837 0
 2844 000c32  10 00 20 	mov #1,w0
 2845              	.L146:
 2846              	.loc 1 838 0
 2847 000c34  8E 07 78 	mov w14,w15
 2848 000c36  4F 07 78 	mov [--w15],w14
 2849 000c38  00 40 A9 	bclr CORCON,#2
 2850 000c3a  00 00 06 	return 
 2851              	.set ___PA___,0
 2852              	.LFE17:
 2853              	.size _SPI1_TransferModeGet,.-_SPI1_TransferModeGet
 2854              	.align 2
 2855              	.global _SPI2_TransferModeGet
 2856              	.type _SPI2_TransferModeGet,@function
 2857              	_SPI2_TransferModeGet:
 2858              	.LFB18:
 2859              	.loc 1 845 0
 2860              	.set ___PA___,1
 2861 000c3c  00 00 FA 	lnk #0
 2862              	.LCFI24:
 2863              	.loc 1 846 0
 2864 000c3e  01 00 80 	mov _SPI2CON1bits,w1
 2865 000c40  00 40 20 	mov #1024,w0
 2866 000c42  00 80 60 	and w1,w0,w0
 2867 000c44  00 00 E0 	cp0 w0
 2868              	.set ___BP___,0
 2869 000c46  00 00 3A 	bra nz,.L148
 2870              	.loc 1 847 0
 2871 000c48  00 00 EB 	clr w0
 2872 000c4a  00 00 37 	bra .L149
 2873              	.L148:
 2874              	.loc 1 849 0
 2875 000c4c  10 00 20 	mov #1,w0
 2876              	.L149:
 2877              	.loc 1 850 0
 2878 000c4e  8E 07 78 	mov w14,w15
 2879 000c50  4F 07 78 	mov [--w15],w14
 2880 000c52  00 40 A9 	bclr CORCON,#2
 2881 000c54  00 00 06 	return 
MPLAB XC16 ASSEMBLY Listing:   			page 67


 2882              	.set ___PA___,0
 2883              	.LFE18:
 2884              	.size _SPI2_TransferModeGet,.-_SPI2_TransferModeGet
 2885              	.align 2
 2886              	.global _SPI3_TransferModeGet
 2887              	.type _SPI3_TransferModeGet,@function
 2888              	_SPI3_TransferModeGet:
 2889              	.LFB19:
 2890              	.loc 1 857 0
 2891              	.set ___PA___,1
 2892 000c56  00 00 FA 	lnk #0
 2893              	.LCFI25:
 2894              	.loc 1 858 0
 2895 000c58  01 00 80 	mov _SPI3CON1bits,w1
 2896 000c5a  00 40 20 	mov #1024,w0
 2897 000c5c  00 80 60 	and w1,w0,w0
 2898 000c5e  00 00 E0 	cp0 w0
 2899              	.set ___BP___,0
 2900 000c60  00 00 3A 	bra nz,.L151
 2901              	.loc 1 859 0
 2902 000c62  00 00 EB 	clr w0
 2903 000c64  00 00 37 	bra .L152
 2904              	.L151:
 2905              	.loc 1 861 0
 2906 000c66  10 00 20 	mov #1,w0
 2907              	.L152:
 2908              	.loc 1 862 0
 2909 000c68  8E 07 78 	mov w14,w15
 2910 000c6a  4F 07 78 	mov [--w15],w14
 2911 000c6c  00 40 A9 	bclr CORCON,#2
 2912 000c6e  00 00 06 	return 
 2913              	.set ___PA___,0
 2914              	.LFE19:
 2915              	.size _SPI3_TransferModeGet,.-_SPI3_TransferModeGet
 2916              	.align 2
 2917              	.global _SPI4_TransferModeGet
 2918              	.type _SPI4_TransferModeGet,@function
 2919              	_SPI4_TransferModeGet:
 2920              	.LFB20:
 2921              	.loc 1 869 0
 2922              	.set ___PA___,1
 2923 000c70  00 00 FA 	lnk #0
 2924              	.LCFI26:
 2925              	.loc 1 870 0
 2926 000c72  01 00 80 	mov _SPI4CON1bits,w1
 2927 000c74  00 40 20 	mov #1024,w0
 2928 000c76  00 80 60 	and w1,w0,w0
 2929 000c78  00 00 E0 	cp0 w0
 2930              	.set ___BP___,0
 2931 000c7a  00 00 3A 	bra nz,.L154
 2932              	.loc 1 871 0
 2933 000c7c  00 00 EB 	clr w0
 2934 000c7e  00 00 37 	bra .L155
 2935              	.L154:
 2936              	.loc 1 873 0
 2937 000c80  10 00 20 	mov #1,w0
 2938              	.L155:
MPLAB XC16 ASSEMBLY Listing:   			page 68


 874:lib/lib_pic33e/SPI.c **** }
 2939              	.loc 1 874 0
 2940 000c82  8E 07 78 	mov w14,w15
 2941 000c84  4F 07 78 	mov [--w15],w14
 2942 000c86  00 40 A9 	bclr CORCON,#2
 2943 000c88  00 00 06 	return 
 2944              	.set ___PA___,0
 2945              	.LFE20:
 2946              	.size _SPI4_TransferModeGet,.-_SPI4_TransferModeGet
 2947              	.section .const,psv,page
 2948                 	.type _C.68.18415,@object
 2949                 	.size _C.68.18415,4
 2950                 	_C.68.18415:
 2951 0000 40          	.byte 64
 2952 0001 10          	.byte 16
 2953 0002 04          	.byte 4
 2954 0003 01          	.byte 1
 2955                 	.type _C.69.18416,@object
 2956                 	.size _C.69.18416,8
 2957                 	_C.69.18416:
 2958 0004 08          	.byte 8
 2959 0005 07          	.byte 7
 2960 0006 06          	.byte 6
 2961 0007 05          	.byte 5
 2962 0008 04          	.byte 4
 2963 0009 03          	.byte 3
 2964 000a 02          	.byte 2
 2965 000b 01          	.byte 1
 2966                 	.section .debug_frame,info
 2967                 	.Lframe0:
 2968 0000 10 00 00 00 	.4byte .LECIE0-.LSCIE0
 2969                 	.LSCIE0:
 2970 0004 FF FF FF FF 	.4byte 0xffffffff
 2971 0008 01          	.byte 0x1
 2972 0009 00          	.byte 0
 2973 000a 01          	.uleb128 0x1
 2974 000b 02          	.sleb128 2
 2975 000c 25          	.byte 0x25
 2976 000d 12          	.byte 0x12
 2977 000e 0F          	.uleb128 0xf
 2978 000f 7E          	.sleb128 -2
 2979 0010 09          	.byte 0x9
 2980 0011 25          	.uleb128 0x25
 2981 0012 0F          	.uleb128 0xf
 2982 0013 00          	.align 4
 2983                 	.LECIE0:
 2984                 	.LSFDE0:
 2985 0014 20 00 00 00 	.4byte .LEFDE0-.LASFDE0
 2986                 	.LASFDE0:
 2987 0018 00 00 00 00 	.4byte .Lframe0
 2988 001c 00 00 00 00 	.4byte .LFB0
 2989 0020 0A 01 00 00 	.4byte .LFE0-.LFB0
 2990 0024 04          	.byte 0x4
 2991 0025 02 00 00 00 	.4byte .LCFI0-.LFB0
 2992 0029 13          	.byte 0x13
 2993 002a 7D          	.sleb128 -3
 2994 002b 0D          	.byte 0xd
MPLAB XC16 ASSEMBLY Listing:   			page 69


 2995 002c 0E          	.uleb128 0xe
 2996 002d 8E          	.byte 0x8e
 2997 002e 02          	.uleb128 0x2
 2998 002f 04          	.byte 0x4
 2999 0030 04 00 00 00 	.4byte .LCFI2-.LCFI0
 3000 0034 8A          	.byte 0x8a
 3001 0035 18          	.uleb128 0x18
 3002 0036 88          	.byte 0x88
 3003 0037 16          	.uleb128 0x16
 3004                 	.align 4
 3005                 	.LEFDE0:
 3006                 	.LSFDE2:
 3007 0038 1E 00 00 00 	.4byte .LEFDE2-.LASFDE2
 3008                 	.LASFDE2:
 3009 003c 00 00 00 00 	.4byte .Lframe0
 3010 0040 00 00 00 00 	.4byte .LFB1
 3011 0044 24 01 00 00 	.4byte .LFE1-.LFB1
 3012 0048 04          	.byte 0x4
 3013 0049 02 00 00 00 	.4byte .LCFI3-.LFB1
 3014 004d 13          	.byte 0x13
 3015 004e 7D          	.sleb128 -3
 3016 004f 0D          	.byte 0xd
 3017 0050 0E          	.uleb128 0xe
 3018 0051 8E          	.byte 0x8e
 3019 0052 02          	.uleb128 0x2
 3020 0053 04          	.byte 0x4
 3021 0054 02 00 00 00 	.4byte .LCFI4-.LCFI3
 3022 0058 88          	.byte 0x88
 3023 0059 09          	.uleb128 0x9
 3024                 	.align 4
 3025                 	.LEFDE2:
 3026                 	.LSFDE4:
 3027 005a 1E 00 00 00 	.4byte .LEFDE4-.LASFDE4
 3028                 	.LASFDE4:
 3029 005e 00 00 00 00 	.4byte .Lframe0
 3030 0062 00 00 00 00 	.4byte .LFB2
 3031 0066 24 01 00 00 	.4byte .LFE2-.LFB2
 3032 006a 04          	.byte 0x4
 3033 006b 02 00 00 00 	.4byte .LCFI5-.LFB2
 3034 006f 13          	.byte 0x13
 3035 0070 7D          	.sleb128 -3
 3036 0071 0D          	.byte 0xd
 3037 0072 0E          	.uleb128 0xe
 3038 0073 8E          	.byte 0x8e
 3039 0074 02          	.uleb128 0x2
 3040 0075 04          	.byte 0x4
 3041 0076 02 00 00 00 	.4byte .LCFI6-.LCFI5
 3042 007a 88          	.byte 0x88
 3043 007b 09          	.uleb128 0x9
 3044                 	.align 4
 3045                 	.LEFDE4:
 3046                 	.LSFDE6:
 3047 007c 1E 00 00 00 	.4byte .LEFDE6-.LASFDE6
 3048                 	.LASFDE6:
 3049 0080 00 00 00 00 	.4byte .Lframe0
 3050 0084 00 00 00 00 	.4byte .LFB3
 3051 0088 24 01 00 00 	.4byte .LFE3-.LFB3
MPLAB XC16 ASSEMBLY Listing:   			page 70


 3052 008c 04          	.byte 0x4
 3053 008d 02 00 00 00 	.4byte .LCFI7-.LFB3
 3054 0091 13          	.byte 0x13
 3055 0092 7D          	.sleb128 -3
 3056 0093 0D          	.byte 0xd
 3057 0094 0E          	.uleb128 0xe
 3058 0095 8E          	.byte 0x8e
 3059 0096 02          	.uleb128 0x2
 3060 0097 04          	.byte 0x4
 3061 0098 02 00 00 00 	.4byte .LCFI8-.LCFI7
 3062 009c 88          	.byte 0x88
 3063 009d 09          	.uleb128 0x9
 3064                 	.align 4
 3065                 	.LEFDE6:
 3066                 	.LSFDE8:
 3067 009e 1E 00 00 00 	.4byte .LEFDE8-.LASFDE8
 3068                 	.LASFDE8:
 3069 00a2 00 00 00 00 	.4byte .Lframe0
 3070 00a6 00 00 00 00 	.4byte .LFB4
 3071 00aa 24 01 00 00 	.4byte .LFE4-.LFB4
 3072 00ae 04          	.byte 0x4
 3073 00af 02 00 00 00 	.4byte .LCFI9-.LFB4
 3074 00b3 13          	.byte 0x13
 3075 00b4 7D          	.sleb128 -3
 3076 00b5 0D          	.byte 0xd
 3077 00b6 0E          	.uleb128 0xe
 3078 00b7 8E          	.byte 0x8e
 3079 00b8 02          	.uleb128 0x2
 3080 00b9 04          	.byte 0x4
 3081 00ba 02 00 00 00 	.4byte .LCFI10-.LCFI9
 3082 00be 88          	.byte 0x88
 3083 00bf 09          	.uleb128 0x9
 3084                 	.align 4
 3085                 	.LEFDE8:
 3086                 	.LSFDE10:
 3087 00c0 18 00 00 00 	.4byte .LEFDE10-.LASFDE10
 3088                 	.LASFDE10:
 3089 00c4 00 00 00 00 	.4byte .Lframe0
 3090 00c8 00 00 00 00 	.4byte .LFB5
 3091 00cc 0C 00 00 00 	.4byte .LFE5-.LFB5
 3092 00d0 04          	.byte 0x4
 3093 00d1 02 00 00 00 	.4byte .LCFI11-.LFB5
 3094 00d5 13          	.byte 0x13
 3095 00d6 7D          	.sleb128 -3
 3096 00d7 0D          	.byte 0xd
 3097 00d8 0E          	.uleb128 0xe
 3098 00d9 8E          	.byte 0x8e
 3099 00da 02          	.uleb128 0x2
 3100 00db 00          	.align 4
 3101                 	.LEFDE10:
 3102                 	.LSFDE12:
 3103 00dc 18 00 00 00 	.4byte .LEFDE12-.LASFDE12
 3104                 	.LASFDE12:
 3105 00e0 00 00 00 00 	.4byte .Lframe0
 3106 00e4 00 00 00 00 	.4byte .LFB6
 3107 00e8 0C 00 00 00 	.4byte .LFE6-.LFB6
 3108 00ec 04          	.byte 0x4
MPLAB XC16 ASSEMBLY Listing:   			page 71


 3109 00ed 02 00 00 00 	.4byte .LCFI12-.LFB6
 3110 00f1 13          	.byte 0x13
 3111 00f2 7D          	.sleb128 -3
 3112 00f3 0D          	.byte 0xd
 3113 00f4 0E          	.uleb128 0xe
 3114 00f5 8E          	.byte 0x8e
 3115 00f6 02          	.uleb128 0x2
 3116 00f7 00          	.align 4
 3117                 	.LEFDE12:
 3118                 	.LSFDE14:
 3119 00f8 18 00 00 00 	.4byte .LEFDE14-.LASFDE14
 3120                 	.LASFDE14:
 3121 00fc 00 00 00 00 	.4byte .Lframe0
 3122 0100 00 00 00 00 	.4byte .LFB7
 3123 0104 0C 00 00 00 	.4byte .LFE7-.LFB7
 3124 0108 04          	.byte 0x4
 3125 0109 02 00 00 00 	.4byte .LCFI13-.LFB7
 3126 010d 13          	.byte 0x13
 3127 010e 7D          	.sleb128 -3
 3128 010f 0D          	.byte 0xd
 3129 0110 0E          	.uleb128 0xe
 3130 0111 8E          	.byte 0x8e
 3131 0112 02          	.uleb128 0x2
 3132 0113 00          	.align 4
 3133                 	.LEFDE14:
 3134                 	.LSFDE16:
 3135 0114 18 00 00 00 	.4byte .LEFDE16-.LASFDE16
 3136                 	.LASFDE16:
 3137 0118 00 00 00 00 	.4byte .Lframe0
 3138 011c 00 00 00 00 	.4byte .LFB8
 3139 0120 0C 00 00 00 	.4byte .LFE8-.LFB8
 3140 0124 04          	.byte 0x4
 3141 0125 02 00 00 00 	.4byte .LCFI14-.LFB8
 3142 0129 13          	.byte 0x13
 3143 012a 7D          	.sleb128 -3
 3144 012b 0D          	.byte 0xd
 3145 012c 0E          	.uleb128 0xe
 3146 012d 8E          	.byte 0x8e
 3147 012e 02          	.uleb128 0x2
 3148 012f 00          	.align 4
 3149                 	.LEFDE16:
 3150                 	.LSFDE18:
 3151 0130 18 00 00 00 	.4byte .LEFDE18-.LASFDE18
 3152                 	.LASFDE18:
 3153 0134 00 00 00 00 	.4byte .Lframe0
 3154 0138 00 00 00 00 	.4byte .LFB9
 3155 013c 68 00 00 00 	.4byte .LFE9-.LFB9
 3156 0140 04          	.byte 0x4
 3157 0141 02 00 00 00 	.4byte .LCFI15-.LFB9
 3158 0145 13          	.byte 0x13
 3159 0146 7D          	.sleb128 -3
 3160 0147 0D          	.byte 0xd
 3161 0148 0E          	.uleb128 0xe
 3162 0149 8E          	.byte 0x8e
 3163 014a 02          	.uleb128 0x2
 3164 014b 00          	.align 4
 3165                 	.LEFDE18:
MPLAB XC16 ASSEMBLY Listing:   			page 72


 3166                 	.LSFDE20:
 3167 014c 18 00 00 00 	.4byte .LEFDE20-.LASFDE20
 3168                 	.LASFDE20:
 3169 0150 00 00 00 00 	.4byte .Lframe0
 3170 0154 00 00 00 00 	.4byte .LFB10
 3171 0158 68 00 00 00 	.4byte .LFE10-.LFB10
 3172 015c 04          	.byte 0x4
 3173 015d 02 00 00 00 	.4byte .LCFI16-.LFB10
 3174 0161 13          	.byte 0x13
 3175 0162 7D          	.sleb128 -3
 3176 0163 0D          	.byte 0xd
 3177 0164 0E          	.uleb128 0xe
 3178 0165 8E          	.byte 0x8e
 3179 0166 02          	.uleb128 0x2
 3180 0167 00          	.align 4
 3181                 	.LEFDE20:
 3182                 	.LSFDE22:
 3183 0168 18 00 00 00 	.4byte .LEFDE22-.LASFDE22
 3184                 	.LASFDE22:
 3185 016c 00 00 00 00 	.4byte .Lframe0
 3186 0170 00 00 00 00 	.4byte .LFB11
 3187 0174 68 00 00 00 	.4byte .LFE11-.LFB11
 3188 0178 04          	.byte 0x4
 3189 0179 02 00 00 00 	.4byte .LCFI17-.LFB11
 3190 017d 13          	.byte 0x13
 3191 017e 7D          	.sleb128 -3
 3192 017f 0D          	.byte 0xd
 3193 0180 0E          	.uleb128 0xe
 3194 0181 8E          	.byte 0x8e
 3195 0182 02          	.uleb128 0x2
 3196 0183 00          	.align 4
 3197                 	.LEFDE22:
 3198                 	.LSFDE24:
 3199 0184 18 00 00 00 	.4byte .LEFDE24-.LASFDE24
 3200                 	.LASFDE24:
 3201 0188 00 00 00 00 	.4byte .Lframe0
 3202 018c 00 00 00 00 	.4byte .LFB12
 3203 0190 68 00 00 00 	.4byte .LFE12-.LFB12
 3204 0194 04          	.byte 0x4
 3205 0195 02 00 00 00 	.4byte .LCFI18-.LFB12
 3206 0199 13          	.byte 0x13
 3207 019a 7D          	.sleb128 -3
 3208 019b 0D          	.byte 0xd
 3209 019c 0E          	.uleb128 0xe
 3210 019d 8E          	.byte 0x8e
 3211 019e 02          	.uleb128 0x2
 3212 019f 00          	.align 4
 3213                 	.LEFDE24:
 3214                 	.LSFDE26:
 3215 01a0 18 00 00 00 	.4byte .LEFDE26-.LASFDE26
 3216                 	.LASFDE26:
 3217 01a4 00 00 00 00 	.4byte .Lframe0
 3218 01a8 00 00 00 00 	.4byte .LFB13
 3219 01ac 2E 01 00 00 	.4byte .LFE13-.LFB13
 3220 01b0 04          	.byte 0x4
 3221 01b1 02 00 00 00 	.4byte .LCFI19-.LFB13
 3222 01b5 13          	.byte 0x13
MPLAB XC16 ASSEMBLY Listing:   			page 73


 3223 01b6 7D          	.sleb128 -3
 3224 01b7 0D          	.byte 0xd
 3225 01b8 0E          	.uleb128 0xe
 3226 01b9 8E          	.byte 0x8e
 3227 01ba 02          	.uleb128 0x2
 3228 01bb 00          	.align 4
 3229                 	.LEFDE26:
 3230                 	.LSFDE28:
 3231 01bc 18 00 00 00 	.4byte .LEFDE28-.LASFDE28
 3232                 	.LASFDE28:
 3233 01c0 00 00 00 00 	.4byte .Lframe0
 3234 01c4 00 00 00 00 	.4byte .LFB14
 3235 01c8 2E 01 00 00 	.4byte .LFE14-.LFB14
 3236 01cc 04          	.byte 0x4
 3237 01cd 02 00 00 00 	.4byte .LCFI20-.LFB14
 3238 01d1 13          	.byte 0x13
 3239 01d2 7D          	.sleb128 -3
 3240 01d3 0D          	.byte 0xd
 3241 01d4 0E          	.uleb128 0xe
 3242 01d5 8E          	.byte 0x8e
 3243 01d6 02          	.uleb128 0x2
 3244 01d7 00          	.align 4
 3245                 	.LEFDE28:
 3246                 	.LSFDE30:
 3247 01d8 18 00 00 00 	.4byte .LEFDE30-.LASFDE30
 3248                 	.LASFDE30:
 3249 01dc 00 00 00 00 	.4byte .Lframe0
 3250 01e0 00 00 00 00 	.4byte .LFB15
 3251 01e4 2E 01 00 00 	.4byte .LFE15-.LFB15
 3252 01e8 04          	.byte 0x4
 3253 01e9 02 00 00 00 	.4byte .LCFI21-.LFB15
 3254 01ed 13          	.byte 0x13
 3255 01ee 7D          	.sleb128 -3
 3256 01ef 0D          	.byte 0xd
 3257 01f0 0E          	.uleb128 0xe
 3258 01f1 8E          	.byte 0x8e
 3259 01f2 02          	.uleb128 0x2
 3260 01f3 00          	.align 4
 3261                 	.LEFDE30:
 3262                 	.LSFDE32:
 3263 01f4 18 00 00 00 	.4byte .LEFDE32-.LASFDE32
 3264                 	.LASFDE32:
 3265 01f8 00 00 00 00 	.4byte .Lframe0
 3266 01fc 00 00 00 00 	.4byte .LFB16
 3267 0200 2E 01 00 00 	.4byte .LFE16-.LFB16
 3268 0204 04          	.byte 0x4
 3269 0205 02 00 00 00 	.4byte .LCFI22-.LFB16
 3270 0209 13          	.byte 0x13
 3271 020a 7D          	.sleb128 -3
 3272 020b 0D          	.byte 0xd
 3273 020c 0E          	.uleb128 0xe
 3274 020d 8E          	.byte 0x8e
 3275 020e 02          	.uleb128 0x2
 3276 020f 00          	.align 4
 3277                 	.LEFDE32:
 3278                 	.LSFDE34:
 3279 0210 18 00 00 00 	.4byte .LEFDE34-.LASFDE34
MPLAB XC16 ASSEMBLY Listing:   			page 74


 3280                 	.LASFDE34:
 3281 0214 00 00 00 00 	.4byte .Lframe0
 3282 0218 00 00 00 00 	.4byte .LFB17
 3283 021c 1A 00 00 00 	.4byte .LFE17-.LFB17
 3284 0220 04          	.byte 0x4
 3285 0221 02 00 00 00 	.4byte .LCFI23-.LFB17
 3286 0225 13          	.byte 0x13
 3287 0226 7D          	.sleb128 -3
 3288 0227 0D          	.byte 0xd
 3289 0228 0E          	.uleb128 0xe
 3290 0229 8E          	.byte 0x8e
 3291 022a 02          	.uleb128 0x2
 3292 022b 00          	.align 4
 3293                 	.LEFDE34:
 3294                 	.LSFDE36:
 3295 022c 18 00 00 00 	.4byte .LEFDE36-.LASFDE36
 3296                 	.LASFDE36:
 3297 0230 00 00 00 00 	.4byte .Lframe0
 3298 0234 00 00 00 00 	.4byte .LFB18
 3299 0238 1A 00 00 00 	.4byte .LFE18-.LFB18
 3300 023c 04          	.byte 0x4
 3301 023d 02 00 00 00 	.4byte .LCFI24-.LFB18
 3302 0241 13          	.byte 0x13
 3303 0242 7D          	.sleb128 -3
 3304 0243 0D          	.byte 0xd
 3305 0244 0E          	.uleb128 0xe
 3306 0245 8E          	.byte 0x8e
 3307 0246 02          	.uleb128 0x2
 3308 0247 00          	.align 4
 3309                 	.LEFDE36:
 3310                 	.LSFDE38:
 3311 0248 18 00 00 00 	.4byte .LEFDE38-.LASFDE38
 3312                 	.LASFDE38:
 3313 024c 00 00 00 00 	.4byte .Lframe0
 3314 0250 00 00 00 00 	.4byte .LFB19
 3315 0254 1A 00 00 00 	.4byte .LFE19-.LFB19
 3316 0258 04          	.byte 0x4
 3317 0259 02 00 00 00 	.4byte .LCFI25-.LFB19
 3318 025d 13          	.byte 0x13
 3319 025e 7D          	.sleb128 -3
 3320 025f 0D          	.byte 0xd
 3321 0260 0E          	.uleb128 0xe
 3322 0261 8E          	.byte 0x8e
 3323 0262 02          	.uleb128 0x2
 3324 0263 00          	.align 4
 3325                 	.LEFDE38:
 3326                 	.LSFDE40:
 3327 0264 18 00 00 00 	.4byte .LEFDE40-.LASFDE40
 3328                 	.LASFDE40:
 3329 0268 00 00 00 00 	.4byte .Lframe0
 3330 026c 00 00 00 00 	.4byte .LFB20
 3331 0270 1A 00 00 00 	.4byte .LFE20-.LFB20
 3332 0274 04          	.byte 0x4
 3333 0275 02 00 00 00 	.4byte .LCFI26-.LFB20
 3334 0279 13          	.byte 0x13
 3335 027a 7D          	.sleb128 -3
 3336 027b 0D          	.byte 0xd
MPLAB XC16 ASSEMBLY Listing:   			page 75


 3337 027c 0E          	.uleb128 0xe
 3338 027d 8E          	.byte 0x8e
 3339 027e 02          	.uleb128 0x2
 3340 027f 00          	.align 4
 3341                 	.LEFDE40:
 3342                 	.section .text,code
 3343              	.Letext0:
 3344              	.file 2 "/opt/microchip/xc16/v1.36/bin/bin/../../include/lega-c/stdint.h"
 3345              	.file 3 "/opt/microchip/xc16/v1.36/bin/bin/../../support/dsPIC33E/h/p33EP256MU806.h"
 3346              	.file 4 "lib/lib_pic33e/SPI.h"
 3347              	.section .debug_info,info
 3348 0000 B1 1C 00 00 	.4byte 0x1cb1
 3349 0004 02 00       	.2byte 0x2
 3350 0006 00 00 00 00 	.4byte .Ldebug_abbrev0
 3351 000a 04          	.byte 0x4
 3352 000b 01          	.uleb128 0x1
 3353 000c 47 4E 55 20 	.asciz "GNU C 4.5.1 (XC16, Microchip v1.36) (A) Build date: Dec  5 2018"
 3353      43 20 34 2E 
 3353      35 2E 31 20 
 3353      28 58 43 31 
 3353      36 2C 20 4D 
 3353      69 63 72 6F 
 3353      63 68 69 70 
 3353      20 76 31 2E 
 3353      33 36 29 20 
 3354 004c 01          	.byte 0x1
 3355 004d 6C 69 62 2F 	.asciz "lib/lib_pic33e/SPI.c"
 3355      6C 69 62 5F 
 3355      70 69 63 33 
 3355      33 65 2F 53 
 3355      50 49 2E 63 
 3355      00 
 3356 0062 2F 68 6F 6D 	.asciz "/home/user/Documents/FST/Programming/project_sub-zero"
 3356      65 2F 75 73 
 3356      65 72 2F 44 
 3356      6F 63 75 6D 
 3356      65 6E 74 73 
 3356      2F 46 53 54 
 3356      2F 50 72 6F 
 3356      67 72 61 6D 
 3356      6D 69 6E 67 
 3357 0098 00 00 00 00 	.4byte .Ltext0
 3358 009c 00 00 00 00 	.4byte .Letext0
 3359 00a0 00 00 00 00 	.4byte .Ldebug_line0
 3360 00a4 02          	.uleb128 0x2
 3361 00a5 01          	.byte 0x1
 3362 00a6 06          	.byte 0x6
 3363 00a7 73 69 67 6E 	.asciz "signed char"
 3363      65 64 20 63 
 3363      68 61 72 00 
 3364 00b3 02          	.uleb128 0x2
 3365 00b4 02          	.byte 0x2
 3366 00b5 05          	.byte 0x5
 3367 00b6 69 6E 74 00 	.asciz "int"
 3368 00ba 02          	.uleb128 0x2
 3369 00bb 04          	.byte 0x4
 3370 00bc 05          	.byte 0x5
MPLAB XC16 ASSEMBLY Listing:   			page 76


 3371 00bd 6C 6F 6E 67 	.asciz "long int"
 3371      20 69 6E 74 
 3371      00 
 3372 00c6 02          	.uleb128 0x2
 3373 00c7 08          	.byte 0x8
 3374 00c8 05          	.byte 0x5
 3375 00c9 6C 6F 6E 67 	.asciz "long long int"
 3375      20 6C 6F 6E 
 3375      67 20 69 6E 
 3375      74 00 
 3376 00d7 03          	.uleb128 0x3
 3377 00d8 75 69 6E 74 	.asciz "uint8_t"
 3377      38 5F 74 00 
 3378 00e0 02          	.byte 0x2
 3379 00e1 2B          	.byte 0x2b
 3380 00e2 E6 00 00 00 	.4byte 0xe6
 3381 00e6 02          	.uleb128 0x2
 3382 00e7 01          	.byte 0x1
 3383 00e8 08          	.byte 0x8
 3384 00e9 75 6E 73 69 	.asciz "unsigned char"
 3384      67 6E 65 64 
 3384      20 63 68 61 
 3384      72 00 
 3385 00f7 03          	.uleb128 0x3
 3386 00f8 75 69 6E 74 	.asciz "uint16_t"
 3386      31 36 5F 74 
 3386      00 
 3387 0101 02          	.byte 0x2
 3388 0102 31          	.byte 0x31
 3389 0103 07 01 00 00 	.4byte 0x107
 3390 0107 02          	.uleb128 0x2
 3391 0108 02          	.byte 0x2
 3392 0109 07          	.byte 0x7
 3393 010a 75 6E 73 69 	.asciz "unsigned int"
 3393      67 6E 65 64 
 3393      20 69 6E 74 
 3393      00 
 3394 0117 03          	.uleb128 0x3
 3395 0118 75 69 6E 74 	.asciz "uint32_t"
 3395      33 32 5F 74 
 3395      00 
 3396 0121 02          	.byte 0x2
 3397 0122 37          	.byte 0x37
 3398 0123 27 01 00 00 	.4byte 0x127
 3399 0127 02          	.uleb128 0x2
 3400 0128 04          	.byte 0x4
 3401 0129 07          	.byte 0x7
 3402 012a 6C 6F 6E 67 	.asciz "long unsigned int"
 3402      20 75 6E 73 
 3402      69 67 6E 65 
 3402      64 20 69 6E 
 3402      74 00 
 3403 013c 02          	.uleb128 0x2
 3404 013d 08          	.byte 0x8
 3405 013e 07          	.byte 0x7
 3406 013f 6C 6F 6E 67 	.asciz "long long unsigned int"
 3406      20 6C 6F 6E 
MPLAB XC16 ASSEMBLY Listing:   			page 77


 3406      67 20 75 6E 
 3406      73 69 67 6E 
 3406      65 64 20 69 
 3406      6E 74 00 
 3407 0156 04          	.uleb128 0x4
 3408 0157 02          	.byte 0x2
 3409 0158 03          	.byte 0x3
 3410 0159 2F 07       	.2byte 0x72f
 3411 015b 02 02 00 00 	.4byte 0x202
 3412 015f 05          	.uleb128 0x5
 3413 0160 00 00 00 00 	.4byte .LASF0
 3414 0164 03          	.byte 0x3
 3415 0165 30 07       	.2byte 0x730
 3416 0167 F7 00 00 00 	.4byte 0xf7
 3417 016b 02          	.byte 0x2
 3418 016c 01          	.byte 0x1
 3419 016d 0F          	.byte 0xf
 3420 016e 02          	.byte 0x2
 3421 016f 23          	.byte 0x23
 3422 0170 00          	.uleb128 0x0
 3423 0171 05          	.uleb128 0x5
 3424 0172 00 00 00 00 	.4byte .LASF1
 3425 0176 03          	.byte 0x3
 3426 0177 31 07       	.2byte 0x731
 3427 0179 F7 00 00 00 	.4byte 0xf7
 3428 017d 02          	.byte 0x2
 3429 017e 01          	.byte 0x1
 3430 017f 0E          	.byte 0xe
 3431 0180 02          	.byte 0x2
 3432 0181 23          	.byte 0x23
 3433 0182 00          	.uleb128 0x0
 3434 0183 05          	.uleb128 0x5
 3435 0184 00 00 00 00 	.4byte .LASF2
 3436 0188 03          	.byte 0x3
 3437 0189 32 07       	.2byte 0x732
 3438 018b F7 00 00 00 	.4byte 0xf7
 3439 018f 02          	.byte 0x2
 3440 0190 03          	.byte 0x3
 3441 0191 0B          	.byte 0xb
 3442 0192 02          	.byte 0x2
 3443 0193 23          	.byte 0x23
 3444 0194 00          	.uleb128 0x0
 3445 0195 05          	.uleb128 0x5
 3446 0196 00 00 00 00 	.4byte .LASF3
 3447 019a 03          	.byte 0x3
 3448 019b 33 07       	.2byte 0x733
 3449 019d F7 00 00 00 	.4byte 0xf7
 3450 01a1 02          	.byte 0x2
 3451 01a2 01          	.byte 0x1
 3452 01a3 0A          	.byte 0xa
 3453 01a4 02          	.byte 0x2
 3454 01a5 23          	.byte 0x23
 3455 01a6 00          	.uleb128 0x0
 3456 01a7 05          	.uleb128 0x5
 3457 01a8 00 00 00 00 	.4byte .LASF4
 3458 01ac 03          	.byte 0x3
 3459 01ad 34 07       	.2byte 0x734
MPLAB XC16 ASSEMBLY Listing:   			page 78


 3460 01af F7 00 00 00 	.4byte 0xf7
 3461 01b3 02          	.byte 0x2
 3462 01b4 01          	.byte 0x1
 3463 01b5 09          	.byte 0x9
 3464 01b6 02          	.byte 0x2
 3465 01b7 23          	.byte 0x23
 3466 01b8 00          	.uleb128 0x0
 3467 01b9 05          	.uleb128 0x5
 3468 01ba 00 00 00 00 	.4byte .LASF5
 3469 01be 03          	.byte 0x3
 3470 01bf 35 07       	.2byte 0x735
 3471 01c1 F7 00 00 00 	.4byte 0xf7
 3472 01c5 02          	.byte 0x2
 3473 01c6 01          	.byte 0x1
 3474 01c7 08          	.byte 0x8
 3475 01c8 02          	.byte 0x2
 3476 01c9 23          	.byte 0x23
 3477 01ca 00          	.uleb128 0x0
 3478 01cb 05          	.uleb128 0x5
 3479 01cc 00 00 00 00 	.4byte .LASF6
 3480 01d0 03          	.byte 0x3
 3481 01d1 36 07       	.2byte 0x736
 3482 01d3 F7 00 00 00 	.4byte 0xf7
 3483 01d7 02          	.byte 0x2
 3484 01d8 03          	.byte 0x3
 3485 01d9 05          	.byte 0x5
 3486 01da 02          	.byte 0x2
 3487 01db 23          	.byte 0x23
 3488 01dc 00          	.uleb128 0x0
 3489 01dd 05          	.uleb128 0x5
 3490 01de 00 00 00 00 	.4byte .LASF7
 3491 01e2 03          	.byte 0x3
 3492 01e3 38 07       	.2byte 0x738
 3493 01e5 F7 00 00 00 	.4byte 0xf7
 3494 01e9 02          	.byte 0x2
 3495 01ea 01          	.byte 0x1
 3496 01eb 02          	.byte 0x2
 3497 01ec 02          	.byte 0x2
 3498 01ed 23          	.byte 0x23
 3499 01ee 00          	.uleb128 0x0
 3500 01ef 05          	.uleb128 0x5
 3501 01f0 00 00 00 00 	.4byte .LASF8
 3502 01f4 03          	.byte 0x3
 3503 01f5 3A 07       	.2byte 0x73a
 3504 01f7 F7 00 00 00 	.4byte 0xf7
 3505 01fb 02          	.byte 0x2
 3506 01fc 01          	.byte 0x1
 3507 01fd 10          	.byte 0x10
 3508 01fe 02          	.byte 0x2
 3509 01ff 23          	.byte 0x23
 3510 0200 00          	.uleb128 0x0
 3511 0201 00          	.byte 0x0
 3512 0202 04          	.uleb128 0x4
 3513 0203 02          	.byte 0x2
 3514 0204 03          	.byte 0x3
 3515 0205 3C 07       	.2byte 0x73c
 3516 0207 78 02 00 00 	.4byte 0x278
MPLAB XC16 ASSEMBLY Listing:   			page 79


 3517 020b 05          	.uleb128 0x5
 3518 020c 00 00 00 00 	.4byte .LASF9
 3519 0210 03          	.byte 0x3
 3520 0211 3E 07       	.2byte 0x73e
 3521 0213 F7 00 00 00 	.4byte 0xf7
 3522 0217 02          	.byte 0x2
 3523 0218 01          	.byte 0x1
 3524 0219 0D          	.byte 0xd
 3525 021a 02          	.byte 0x2
 3526 021b 23          	.byte 0x23
 3527 021c 00          	.uleb128 0x0
 3528 021d 05          	.uleb128 0x5
 3529 021e 00 00 00 00 	.4byte .LASF10
 3530 0222 03          	.byte 0x3
 3531 0223 3F 07       	.2byte 0x73f
 3532 0225 F7 00 00 00 	.4byte 0xf7
 3533 0229 02          	.byte 0x2
 3534 022a 01          	.byte 0x1
 3535 022b 0C          	.byte 0xc
 3536 022c 02          	.byte 0x2
 3537 022d 23          	.byte 0x23
 3538 022e 00          	.uleb128 0x0
 3539 022f 05          	.uleb128 0x5
 3540 0230 00 00 00 00 	.4byte .LASF11
 3541 0234 03          	.byte 0x3
 3542 0235 40 07       	.2byte 0x740
 3543 0237 F7 00 00 00 	.4byte 0xf7
 3544 023b 02          	.byte 0x2
 3545 023c 01          	.byte 0x1
 3546 023d 0B          	.byte 0xb
 3547 023e 02          	.byte 0x2
 3548 023f 23          	.byte 0x23
 3549 0240 00          	.uleb128 0x0
 3550 0241 05          	.uleb128 0x5
 3551 0242 00 00 00 00 	.4byte .LASF12
 3552 0246 03          	.byte 0x3
 3553 0247 42 07       	.2byte 0x742
 3554 0249 F7 00 00 00 	.4byte 0xf7
 3555 024d 02          	.byte 0x2
 3556 024e 01          	.byte 0x1
 3557 024f 07          	.byte 0x7
 3558 0250 02          	.byte 0x2
 3559 0251 23          	.byte 0x23
 3560 0252 00          	.uleb128 0x0
 3561 0253 05          	.uleb128 0x5
 3562 0254 00 00 00 00 	.4byte .LASF13
 3563 0258 03          	.byte 0x3
 3564 0259 43 07       	.2byte 0x743
 3565 025b F7 00 00 00 	.4byte 0xf7
 3566 025f 02          	.byte 0x2
 3567 0260 01          	.byte 0x1
 3568 0261 06          	.byte 0x6
 3569 0262 02          	.byte 0x2
 3570 0263 23          	.byte 0x23
 3571 0264 00          	.uleb128 0x0
 3572 0265 05          	.uleb128 0x5
 3573 0266 00 00 00 00 	.4byte .LASF14
MPLAB XC16 ASSEMBLY Listing:   			page 80


 3574 026a 03          	.byte 0x3
 3575 026b 44 07       	.2byte 0x744
 3576 026d F7 00 00 00 	.4byte 0xf7
 3577 0271 02          	.byte 0x2
 3578 0272 01          	.byte 0x1
 3579 0273 05          	.byte 0x5
 3580 0274 02          	.byte 0x2
 3581 0275 23          	.byte 0x23
 3582 0276 00          	.uleb128 0x0
 3583 0277 00          	.byte 0x0
 3584 0278 06          	.uleb128 0x6
 3585 0279 02          	.byte 0x2
 3586 027a 03          	.byte 0x3
 3587 027b 2E 07       	.2byte 0x72e
 3588 027d 8C 02 00 00 	.4byte 0x28c
 3589 0281 07          	.uleb128 0x7
 3590 0282 56 01 00 00 	.4byte 0x156
 3591 0286 07          	.uleb128 0x7
 3592 0287 02 02 00 00 	.4byte 0x202
 3593 028b 00          	.byte 0x0
 3594 028c 08          	.uleb128 0x8
 3595 028d 74 61 67 53 	.asciz "tagSPI1STATBITS"
 3595      50 49 31 53 
 3595      54 41 54 42 
 3595      49 54 53 00 
 3596 029d 02          	.byte 0x2
 3597 029e 03          	.byte 0x3
 3598 029f 2D 07       	.2byte 0x72d
 3599 02a1 AE 02 00 00 	.4byte 0x2ae
 3600 02a5 09          	.uleb128 0x9
 3601 02a6 78 02 00 00 	.4byte 0x278
 3602 02aa 02          	.byte 0x2
 3603 02ab 23          	.byte 0x23
 3604 02ac 00          	.uleb128 0x0
 3605 02ad 00          	.byte 0x0
 3606 02ae 0A          	.uleb128 0xa
 3607 02af 53 50 49 31 	.asciz "SPI1STATBITS"
 3607      53 54 41 54 
 3607      42 49 54 53 
 3607      00 
 3608 02bc 03          	.byte 0x3
 3609 02bd 47 07       	.2byte 0x747
 3610 02bf 8C 02 00 00 	.4byte 0x28c
 3611 02c3 04          	.uleb128 0x4
 3612 02c4 02          	.byte 0x2
 3613 02c5 03          	.byte 0x3
 3614 02c6 4E 07       	.2byte 0x74e
 3615 02c8 84 03 00 00 	.4byte 0x384
 3616 02cc 0B          	.uleb128 0xb
 3617 02cd 50 50 52 45 	.asciz "PPRE"
 3617      00 
 3618 02d2 03          	.byte 0x3
 3619 02d3 4F 07       	.2byte 0x74f
 3620 02d5 F7 00 00 00 	.4byte 0xf7
 3621 02d9 02          	.byte 0x2
 3622 02da 02          	.byte 0x2
 3623 02db 0E          	.byte 0xe
MPLAB XC16 ASSEMBLY Listing:   			page 81


 3624 02dc 02          	.byte 0x2
 3625 02dd 23          	.byte 0x23
 3626 02de 00          	.uleb128 0x0
 3627 02df 0B          	.uleb128 0xb
 3628 02e0 53 50 52 45 	.asciz "SPRE"
 3628      00 
 3629 02e5 03          	.byte 0x3
 3630 02e6 50 07       	.2byte 0x750
 3631 02e8 F7 00 00 00 	.4byte 0xf7
 3632 02ec 02          	.byte 0x2
 3633 02ed 03          	.byte 0x3
 3634 02ee 0B          	.byte 0xb
 3635 02ef 02          	.byte 0x2
 3636 02f0 23          	.byte 0x23
 3637 02f1 00          	.uleb128 0x0
 3638 02f2 05          	.uleb128 0x5
 3639 02f3 00 00 00 00 	.4byte .LASF15
 3640 02f7 03          	.byte 0x3
 3641 02f8 51 07       	.2byte 0x751
 3642 02fa F7 00 00 00 	.4byte 0xf7
 3643 02fe 02          	.byte 0x2
 3644 02ff 01          	.byte 0x1
 3645 0300 0A          	.byte 0xa
 3646 0301 02          	.byte 0x2
 3647 0302 23          	.byte 0x23
 3648 0303 00          	.uleb128 0x0
 3649 0304 0B          	.uleb128 0xb
 3650 0305 43 4B 50 00 	.asciz "CKP"
 3651 0309 03          	.byte 0x3
 3652 030a 52 07       	.2byte 0x752
 3653 030c F7 00 00 00 	.4byte 0xf7
 3654 0310 02          	.byte 0x2
 3655 0311 01          	.byte 0x1
 3656 0312 09          	.byte 0x9
 3657 0313 02          	.byte 0x2
 3658 0314 23          	.byte 0x23
 3659 0315 00          	.uleb128 0x0
 3660 0316 0B          	.uleb128 0xb
 3661 0317 53 53 45 4E 	.asciz "SSEN"
 3661      00 
 3662 031c 03          	.byte 0x3
 3663 031d 53 07       	.2byte 0x753
 3664 031f F7 00 00 00 	.4byte 0xf7
 3665 0323 02          	.byte 0x2
 3666 0324 01          	.byte 0x1
 3667 0325 08          	.byte 0x8
 3668 0326 02          	.byte 0x2
 3669 0327 23          	.byte 0x23
 3670 0328 00          	.uleb128 0x0
 3671 0329 0B          	.uleb128 0xb
 3672 032a 43 4B 45 00 	.asciz "CKE"
 3673 032e 03          	.byte 0x3
 3674 032f 54 07       	.2byte 0x754
 3675 0331 F7 00 00 00 	.4byte 0xf7
 3676 0335 02          	.byte 0x2
 3677 0336 01          	.byte 0x1
 3678 0337 07          	.byte 0x7
MPLAB XC16 ASSEMBLY Listing:   			page 82


 3679 0338 02          	.byte 0x2
 3680 0339 23          	.byte 0x23
 3681 033a 00          	.uleb128 0x0
 3682 033b 0B          	.uleb128 0xb
 3683 033c 53 4D 50 00 	.asciz "SMP"
 3684 0340 03          	.byte 0x3
 3685 0341 55 07       	.2byte 0x755
 3686 0343 F7 00 00 00 	.4byte 0xf7
 3687 0347 02          	.byte 0x2
 3688 0348 01          	.byte 0x1
 3689 0349 06          	.byte 0x6
 3690 034a 02          	.byte 0x2
 3691 034b 23          	.byte 0x23
 3692 034c 00          	.uleb128 0x0
 3693 034d 05          	.uleb128 0x5
 3694 034e 00 00 00 00 	.4byte .LASF16
 3695 0352 03          	.byte 0x3
 3696 0353 56 07       	.2byte 0x756
 3697 0355 F7 00 00 00 	.4byte 0xf7
 3698 0359 02          	.byte 0x2
 3699 035a 01          	.byte 0x1
 3700 035b 05          	.byte 0x5
 3701 035c 02          	.byte 0x2
 3702 035d 23          	.byte 0x23
 3703 035e 00          	.uleb128 0x0
 3704 035f 05          	.uleb128 0x5
 3705 0360 00 00 00 00 	.4byte .LASF17
 3706 0364 03          	.byte 0x3
 3707 0365 57 07       	.2byte 0x757
 3708 0367 F7 00 00 00 	.4byte 0xf7
 3709 036b 02          	.byte 0x2
 3710 036c 01          	.byte 0x1
 3711 036d 04          	.byte 0x4
 3712 036e 02          	.byte 0x2
 3713 036f 23          	.byte 0x23
 3714 0370 00          	.uleb128 0x0
 3715 0371 05          	.uleb128 0x5
 3716 0372 00 00 00 00 	.4byte .LASF18
 3717 0376 03          	.byte 0x3
 3718 0377 58 07       	.2byte 0x758
 3719 0379 F7 00 00 00 	.4byte 0xf7
 3720 037d 02          	.byte 0x2
 3721 037e 01          	.byte 0x1
 3722 037f 03          	.byte 0x3
 3723 0380 02          	.byte 0x2
 3724 0381 23          	.byte 0x23
 3725 0382 00          	.uleb128 0x0
 3726 0383 00          	.byte 0x0
 3727 0384 04          	.uleb128 0x4
 3728 0385 02          	.byte 0x2
 3729 0386 03          	.byte 0x3
 3730 0387 5A 07       	.2byte 0x75a
 3731 0389 E8 03 00 00 	.4byte 0x3e8
 3732 038d 05          	.uleb128 0x5
 3733 038e 00 00 00 00 	.4byte .LASF19
 3734 0392 03          	.byte 0x3
 3735 0393 5B 07       	.2byte 0x75b
MPLAB XC16 ASSEMBLY Listing:   			page 83


 3736 0395 F7 00 00 00 	.4byte 0xf7
 3737 0399 02          	.byte 0x2
 3738 039a 01          	.byte 0x1
 3739 039b 0F          	.byte 0xf
 3740 039c 02          	.byte 0x2
 3741 039d 23          	.byte 0x23
 3742 039e 00          	.uleb128 0x0
 3743 039f 05          	.uleb128 0x5
 3744 03a0 00 00 00 00 	.4byte .LASF20
 3745 03a4 03          	.byte 0x3
 3746 03a5 5C 07       	.2byte 0x75c
 3747 03a7 F7 00 00 00 	.4byte 0xf7
 3748 03ab 02          	.byte 0x2
 3749 03ac 01          	.byte 0x1
 3750 03ad 0E          	.byte 0xe
 3751 03ae 02          	.byte 0x2
 3752 03af 23          	.byte 0x23
 3753 03b0 00          	.uleb128 0x0
 3754 03b1 05          	.uleb128 0x5
 3755 03b2 00 00 00 00 	.4byte .LASF21
 3756 03b6 03          	.byte 0x3
 3757 03b7 5D 07       	.2byte 0x75d
 3758 03b9 F7 00 00 00 	.4byte 0xf7
 3759 03bd 02          	.byte 0x2
 3760 03be 01          	.byte 0x1
 3761 03bf 0D          	.byte 0xd
 3762 03c0 02          	.byte 0x2
 3763 03c1 23          	.byte 0x23
 3764 03c2 00          	.uleb128 0x0
 3765 03c3 05          	.uleb128 0x5
 3766 03c4 00 00 00 00 	.4byte .LASF22
 3767 03c8 03          	.byte 0x3
 3768 03c9 5E 07       	.2byte 0x75e
 3769 03cb F7 00 00 00 	.4byte 0xf7
 3770 03cf 02          	.byte 0x2
 3771 03d0 01          	.byte 0x1
 3772 03d1 0C          	.byte 0xc
 3773 03d2 02          	.byte 0x2
 3774 03d3 23          	.byte 0x23
 3775 03d4 00          	.uleb128 0x0
 3776 03d5 05          	.uleb128 0x5
 3777 03d6 00 00 00 00 	.4byte .LASF23
 3778 03da 03          	.byte 0x3
 3779 03db 5F 07       	.2byte 0x75f
 3780 03dd F7 00 00 00 	.4byte 0xf7
 3781 03e1 02          	.byte 0x2
 3782 03e2 01          	.byte 0x1
 3783 03e3 0B          	.byte 0xb
 3784 03e4 02          	.byte 0x2
 3785 03e5 23          	.byte 0x23
 3786 03e6 00          	.uleb128 0x0
 3787 03e7 00          	.byte 0x0
 3788 03e8 06          	.uleb128 0x6
 3789 03e9 02          	.byte 0x2
 3790 03ea 03          	.byte 0x3
 3791 03eb 4D 07       	.2byte 0x74d
 3792 03ed FC 03 00 00 	.4byte 0x3fc
MPLAB XC16 ASSEMBLY Listing:   			page 84


 3793 03f1 07          	.uleb128 0x7
 3794 03f2 C3 02 00 00 	.4byte 0x2c3
 3795 03f6 07          	.uleb128 0x7
 3796 03f7 84 03 00 00 	.4byte 0x384
 3797 03fb 00          	.byte 0x0
 3798 03fc 08          	.uleb128 0x8
 3799 03fd 74 61 67 53 	.asciz "tagSPI1CON1BITS"
 3799      50 49 31 43 
 3799      4F 4E 31 42 
 3799      49 54 53 00 
 3800 040d 02          	.byte 0x2
 3801 040e 03          	.byte 0x3
 3802 040f 4C 07       	.2byte 0x74c
 3803 0411 1E 04 00 00 	.4byte 0x41e
 3804 0415 09          	.uleb128 0x9
 3805 0416 E8 03 00 00 	.4byte 0x3e8
 3806 041a 02          	.byte 0x2
 3807 041b 23          	.byte 0x23
 3808 041c 00          	.uleb128 0x0
 3809 041d 00          	.byte 0x0
 3810 041e 0A          	.uleb128 0xa
 3811 041f 53 50 49 31 	.asciz "SPI1CON1BITS"
 3811      43 4F 4E 31 
 3811      42 49 54 53 
 3811      00 
 3812 042c 03          	.byte 0x3
 3813 042d 62 07       	.2byte 0x762
 3814 042f FC 03 00 00 	.4byte 0x3fc
 3815 0433 08          	.uleb128 0x8
 3816 0434 74 61 67 53 	.asciz "tagSPI1CON2BITS"
 3816      50 49 31 43 
 3816      4F 4E 32 42 
 3816      49 54 53 00 
 3817 0444 02          	.byte 0x2
 3818 0445 03          	.byte 0x3
 3819 0446 67 07       	.2byte 0x767
 3820 0448 A7 04 00 00 	.4byte 0x4a7
 3821 044c 05          	.uleb128 0x5
 3822 044d 00 00 00 00 	.4byte .LASF24
 3823 0451 03          	.byte 0x3
 3824 0452 68 07       	.2byte 0x768
 3825 0454 F7 00 00 00 	.4byte 0xf7
 3826 0458 02          	.byte 0x2
 3827 0459 01          	.byte 0x1
 3828 045a 0F          	.byte 0xf
 3829 045b 02          	.byte 0x2
 3830 045c 23          	.byte 0x23
 3831 045d 00          	.uleb128 0x0
 3832 045e 05          	.uleb128 0x5
 3833 045f 00 00 00 00 	.4byte .LASF25
 3834 0463 03          	.byte 0x3
 3835 0464 69 07       	.2byte 0x769
 3836 0466 F7 00 00 00 	.4byte 0xf7
 3837 046a 02          	.byte 0x2
 3838 046b 01          	.byte 0x1
 3839 046c 0E          	.byte 0xe
 3840 046d 02          	.byte 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 85


 3841 046e 23          	.byte 0x23
 3842 046f 00          	.uleb128 0x0
 3843 0470 05          	.uleb128 0x5
 3844 0471 00 00 00 00 	.4byte .LASF26
 3845 0475 03          	.byte 0x3
 3846 0476 6B 07       	.2byte 0x76b
 3847 0478 F7 00 00 00 	.4byte 0xf7
 3848 047c 02          	.byte 0x2
 3849 047d 01          	.byte 0x1
 3850 047e 02          	.byte 0x2
 3851 047f 02          	.byte 0x2
 3852 0480 23          	.byte 0x23
 3853 0481 00          	.uleb128 0x0
 3854 0482 05          	.uleb128 0x5
 3855 0483 00 00 00 00 	.4byte .LASF27
 3856 0487 03          	.byte 0x3
 3857 0488 6C 07       	.2byte 0x76c
 3858 048a F7 00 00 00 	.4byte 0xf7
 3859 048e 02          	.byte 0x2
 3860 048f 01          	.byte 0x1
 3861 0490 01          	.byte 0x1
 3862 0491 02          	.byte 0x2
 3863 0492 23          	.byte 0x23
 3864 0493 00          	.uleb128 0x0
 3865 0494 05          	.uleb128 0x5
 3866 0495 00 00 00 00 	.4byte .LASF28
 3867 0499 03          	.byte 0x3
 3868 049a 6D 07       	.2byte 0x76d
 3869 049c F7 00 00 00 	.4byte 0xf7
 3870 04a0 02          	.byte 0x2
 3871 04a1 01          	.byte 0x1
 3872 04a2 10          	.byte 0x10
 3873 04a3 02          	.byte 0x2
 3874 04a4 23          	.byte 0x23
 3875 04a5 00          	.uleb128 0x0
 3876 04a6 00          	.byte 0x0
 3877 04a7 0A          	.uleb128 0xa
 3878 04a8 53 50 49 31 	.asciz "SPI1CON2BITS"
 3878      43 4F 4E 32 
 3878      42 49 54 53 
 3878      00 
 3879 04b5 03          	.byte 0x3
 3880 04b6 6E 07       	.2byte 0x76e
 3881 04b8 33 04 00 00 	.4byte 0x433
 3882 04bc 04          	.uleb128 0x4
 3883 04bd 02          	.byte 0x2
 3884 04be 03          	.byte 0x3
 3885 04bf BF 07       	.2byte 0x7bf
 3886 04c1 68 05 00 00 	.4byte 0x568
 3887 04c5 05          	.uleb128 0x5
 3888 04c6 00 00 00 00 	.4byte .LASF0
 3889 04ca 03          	.byte 0x3
 3890 04cb C0 07       	.2byte 0x7c0
 3891 04cd F7 00 00 00 	.4byte 0xf7
 3892 04d1 02          	.byte 0x2
 3893 04d2 01          	.byte 0x1
 3894 04d3 0F          	.byte 0xf
MPLAB XC16 ASSEMBLY Listing:   			page 86


 3895 04d4 02          	.byte 0x2
 3896 04d5 23          	.byte 0x23
 3897 04d6 00          	.uleb128 0x0
 3898 04d7 05          	.uleb128 0x5
 3899 04d8 00 00 00 00 	.4byte .LASF1
 3900 04dc 03          	.byte 0x3
 3901 04dd C1 07       	.2byte 0x7c1
 3902 04df F7 00 00 00 	.4byte 0xf7
 3903 04e3 02          	.byte 0x2
 3904 04e4 01          	.byte 0x1
 3905 04e5 0E          	.byte 0xe
 3906 04e6 02          	.byte 0x2
 3907 04e7 23          	.byte 0x23
 3908 04e8 00          	.uleb128 0x0
 3909 04e9 05          	.uleb128 0x5
 3910 04ea 00 00 00 00 	.4byte .LASF2
 3911 04ee 03          	.byte 0x3
 3912 04ef C2 07       	.2byte 0x7c2
 3913 04f1 F7 00 00 00 	.4byte 0xf7
 3914 04f5 02          	.byte 0x2
 3915 04f6 03          	.byte 0x3
 3916 04f7 0B          	.byte 0xb
 3917 04f8 02          	.byte 0x2
 3918 04f9 23          	.byte 0x23
 3919 04fa 00          	.uleb128 0x0
 3920 04fb 05          	.uleb128 0x5
 3921 04fc 00 00 00 00 	.4byte .LASF3
 3922 0500 03          	.byte 0x3
 3923 0501 C3 07       	.2byte 0x7c3
 3924 0503 F7 00 00 00 	.4byte 0xf7
 3925 0507 02          	.byte 0x2
 3926 0508 01          	.byte 0x1
 3927 0509 0A          	.byte 0xa
 3928 050a 02          	.byte 0x2
 3929 050b 23          	.byte 0x23
 3930 050c 00          	.uleb128 0x0
 3931 050d 05          	.uleb128 0x5
 3932 050e 00 00 00 00 	.4byte .LASF4
 3933 0512 03          	.byte 0x3
 3934 0513 C4 07       	.2byte 0x7c4
 3935 0515 F7 00 00 00 	.4byte 0xf7
 3936 0519 02          	.byte 0x2
 3937 051a 01          	.byte 0x1
 3938 051b 09          	.byte 0x9
 3939 051c 02          	.byte 0x2
 3940 051d 23          	.byte 0x23
 3941 051e 00          	.uleb128 0x0
 3942 051f 05          	.uleb128 0x5
 3943 0520 00 00 00 00 	.4byte .LASF5
 3944 0524 03          	.byte 0x3
 3945 0525 C5 07       	.2byte 0x7c5
 3946 0527 F7 00 00 00 	.4byte 0xf7
 3947 052b 02          	.byte 0x2
 3948 052c 01          	.byte 0x1
 3949 052d 08          	.byte 0x8
 3950 052e 02          	.byte 0x2
 3951 052f 23          	.byte 0x23
MPLAB XC16 ASSEMBLY Listing:   			page 87


 3952 0530 00          	.uleb128 0x0
 3953 0531 05          	.uleb128 0x5
 3954 0532 00 00 00 00 	.4byte .LASF6
 3955 0536 03          	.byte 0x3
 3956 0537 C6 07       	.2byte 0x7c6
 3957 0539 F7 00 00 00 	.4byte 0xf7
 3958 053d 02          	.byte 0x2
 3959 053e 03          	.byte 0x3
 3960 053f 05          	.byte 0x5
 3961 0540 02          	.byte 0x2
 3962 0541 23          	.byte 0x23
 3963 0542 00          	.uleb128 0x0
 3964 0543 05          	.uleb128 0x5
 3965 0544 00 00 00 00 	.4byte .LASF7
 3966 0548 03          	.byte 0x3
 3967 0549 C8 07       	.2byte 0x7c8
 3968 054b F7 00 00 00 	.4byte 0xf7
 3969 054f 02          	.byte 0x2
 3970 0550 01          	.byte 0x1
 3971 0551 02          	.byte 0x2
 3972 0552 02          	.byte 0x2
 3973 0553 23          	.byte 0x23
 3974 0554 00          	.uleb128 0x0
 3975 0555 05          	.uleb128 0x5
 3976 0556 00 00 00 00 	.4byte .LASF8
 3977 055a 03          	.byte 0x3
 3978 055b CA 07       	.2byte 0x7ca
 3979 055d F7 00 00 00 	.4byte 0xf7
 3980 0561 02          	.byte 0x2
 3981 0562 01          	.byte 0x1
 3982 0563 10          	.byte 0x10
 3983 0564 02          	.byte 0x2
 3984 0565 23          	.byte 0x23
 3985 0566 00          	.uleb128 0x0
 3986 0567 00          	.byte 0x0
 3987 0568 04          	.uleb128 0x4
 3988 0569 02          	.byte 0x2
 3989 056a 03          	.byte 0x3
 3990 056b CC 07       	.2byte 0x7cc
 3991 056d DE 05 00 00 	.4byte 0x5de
 3992 0571 05          	.uleb128 0x5
 3993 0572 00 00 00 00 	.4byte .LASF9
 3994 0576 03          	.byte 0x3
 3995 0577 CE 07       	.2byte 0x7ce
 3996 0579 F7 00 00 00 	.4byte 0xf7
 3997 057d 02          	.byte 0x2
 3998 057e 01          	.byte 0x1
 3999 057f 0D          	.byte 0xd
 4000 0580 02          	.byte 0x2
 4001 0581 23          	.byte 0x23
 4002 0582 00          	.uleb128 0x0
 4003 0583 05          	.uleb128 0x5
 4004 0584 00 00 00 00 	.4byte .LASF10
 4005 0588 03          	.byte 0x3
 4006 0589 CF 07       	.2byte 0x7cf
 4007 058b F7 00 00 00 	.4byte 0xf7
 4008 058f 02          	.byte 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 88


 4009 0590 01          	.byte 0x1
 4010 0591 0C          	.byte 0xc
 4011 0592 02          	.byte 0x2
 4012 0593 23          	.byte 0x23
 4013 0594 00          	.uleb128 0x0
 4014 0595 05          	.uleb128 0x5
 4015 0596 00 00 00 00 	.4byte .LASF11
 4016 059a 03          	.byte 0x3
 4017 059b D0 07       	.2byte 0x7d0
 4018 059d F7 00 00 00 	.4byte 0xf7
 4019 05a1 02          	.byte 0x2
 4020 05a2 01          	.byte 0x1
 4021 05a3 0B          	.byte 0xb
 4022 05a4 02          	.byte 0x2
 4023 05a5 23          	.byte 0x23
 4024 05a6 00          	.uleb128 0x0
 4025 05a7 05          	.uleb128 0x5
 4026 05a8 00 00 00 00 	.4byte .LASF12
 4027 05ac 03          	.byte 0x3
 4028 05ad D2 07       	.2byte 0x7d2
 4029 05af F7 00 00 00 	.4byte 0xf7
 4030 05b3 02          	.byte 0x2
 4031 05b4 01          	.byte 0x1
 4032 05b5 07          	.byte 0x7
 4033 05b6 02          	.byte 0x2
 4034 05b7 23          	.byte 0x23
 4035 05b8 00          	.uleb128 0x0
 4036 05b9 05          	.uleb128 0x5
 4037 05ba 00 00 00 00 	.4byte .LASF13
 4038 05be 03          	.byte 0x3
 4039 05bf D3 07       	.2byte 0x7d3
 4040 05c1 F7 00 00 00 	.4byte 0xf7
 4041 05c5 02          	.byte 0x2
 4042 05c6 01          	.byte 0x1
 4043 05c7 06          	.byte 0x6
 4044 05c8 02          	.byte 0x2
 4045 05c9 23          	.byte 0x23
 4046 05ca 00          	.uleb128 0x0
 4047 05cb 05          	.uleb128 0x5
 4048 05cc 00 00 00 00 	.4byte .LASF14
 4049 05d0 03          	.byte 0x3
 4050 05d1 D4 07       	.2byte 0x7d4
 4051 05d3 F7 00 00 00 	.4byte 0xf7
 4052 05d7 02          	.byte 0x2
 4053 05d8 01          	.byte 0x1
 4054 05d9 05          	.byte 0x5
 4055 05da 02          	.byte 0x2
 4056 05db 23          	.byte 0x23
 4057 05dc 00          	.uleb128 0x0
 4058 05dd 00          	.byte 0x0
 4059 05de 06          	.uleb128 0x6
 4060 05df 02          	.byte 0x2
 4061 05e0 03          	.byte 0x3
 4062 05e1 BE 07       	.2byte 0x7be
 4063 05e3 F2 05 00 00 	.4byte 0x5f2
 4064 05e7 07          	.uleb128 0x7
 4065 05e8 BC 04 00 00 	.4byte 0x4bc
MPLAB XC16 ASSEMBLY Listing:   			page 89


 4066 05ec 07          	.uleb128 0x7
 4067 05ed 68 05 00 00 	.4byte 0x568
 4068 05f1 00          	.byte 0x0
 4069 05f2 08          	.uleb128 0x8
 4070 05f3 74 61 67 53 	.asciz "tagSPI2STATBITS"
 4070      50 49 32 53 
 4070      54 41 54 42 
 4070      49 54 53 00 
 4071 0603 02          	.byte 0x2
 4072 0604 03          	.byte 0x3
 4073 0605 BD 07       	.2byte 0x7bd
 4074 0607 14 06 00 00 	.4byte 0x614
 4075 060b 09          	.uleb128 0x9
 4076 060c DE 05 00 00 	.4byte 0x5de
 4077 0610 02          	.byte 0x2
 4078 0611 23          	.byte 0x23
 4079 0612 00          	.uleb128 0x0
 4080 0613 00          	.byte 0x0
 4081 0614 0A          	.uleb128 0xa
 4082 0615 53 50 49 32 	.asciz "SPI2STATBITS"
 4082      53 54 41 54 
 4082      42 49 54 53 
 4082      00 
 4083 0622 03          	.byte 0x3
 4084 0623 D7 07       	.2byte 0x7d7
 4085 0625 F2 05 00 00 	.4byte 0x5f2
 4086 0629 04          	.uleb128 0x4
 4087 062a 02          	.byte 0x2
 4088 062b 03          	.byte 0x3
 4089 062c DE 07       	.2byte 0x7de
 4090 062e EA 06 00 00 	.4byte 0x6ea
 4091 0632 0B          	.uleb128 0xb
 4092 0633 50 50 52 45 	.asciz "PPRE"
 4092      00 
 4093 0638 03          	.byte 0x3
 4094 0639 DF 07       	.2byte 0x7df
 4095 063b F7 00 00 00 	.4byte 0xf7
 4096 063f 02          	.byte 0x2
 4097 0640 02          	.byte 0x2
 4098 0641 0E          	.byte 0xe
 4099 0642 02          	.byte 0x2
 4100 0643 23          	.byte 0x23
 4101 0644 00          	.uleb128 0x0
 4102 0645 0B          	.uleb128 0xb
 4103 0646 53 50 52 45 	.asciz "SPRE"
 4103      00 
 4104 064b 03          	.byte 0x3
 4105 064c E0 07       	.2byte 0x7e0
 4106 064e F7 00 00 00 	.4byte 0xf7
 4107 0652 02          	.byte 0x2
 4108 0653 03          	.byte 0x3
 4109 0654 0B          	.byte 0xb
 4110 0655 02          	.byte 0x2
 4111 0656 23          	.byte 0x23
 4112 0657 00          	.uleb128 0x0
 4113 0658 05          	.uleb128 0x5
 4114 0659 00 00 00 00 	.4byte .LASF15
MPLAB XC16 ASSEMBLY Listing:   			page 90


 4115 065d 03          	.byte 0x3
 4116 065e E1 07       	.2byte 0x7e1
 4117 0660 F7 00 00 00 	.4byte 0xf7
 4118 0664 02          	.byte 0x2
 4119 0665 01          	.byte 0x1
 4120 0666 0A          	.byte 0xa
 4121 0667 02          	.byte 0x2
 4122 0668 23          	.byte 0x23
 4123 0669 00          	.uleb128 0x0
 4124 066a 0B          	.uleb128 0xb
 4125 066b 43 4B 50 00 	.asciz "CKP"
 4126 066f 03          	.byte 0x3
 4127 0670 E2 07       	.2byte 0x7e2
 4128 0672 F7 00 00 00 	.4byte 0xf7
 4129 0676 02          	.byte 0x2
 4130 0677 01          	.byte 0x1
 4131 0678 09          	.byte 0x9
 4132 0679 02          	.byte 0x2
 4133 067a 23          	.byte 0x23
 4134 067b 00          	.uleb128 0x0
 4135 067c 0B          	.uleb128 0xb
 4136 067d 53 53 45 4E 	.asciz "SSEN"
 4136      00 
 4137 0682 03          	.byte 0x3
 4138 0683 E3 07       	.2byte 0x7e3
 4139 0685 F7 00 00 00 	.4byte 0xf7
 4140 0689 02          	.byte 0x2
 4141 068a 01          	.byte 0x1
 4142 068b 08          	.byte 0x8
 4143 068c 02          	.byte 0x2
 4144 068d 23          	.byte 0x23
 4145 068e 00          	.uleb128 0x0
 4146 068f 0B          	.uleb128 0xb
 4147 0690 43 4B 45 00 	.asciz "CKE"
 4148 0694 03          	.byte 0x3
 4149 0695 E4 07       	.2byte 0x7e4
 4150 0697 F7 00 00 00 	.4byte 0xf7
 4151 069b 02          	.byte 0x2
 4152 069c 01          	.byte 0x1
 4153 069d 07          	.byte 0x7
 4154 069e 02          	.byte 0x2
 4155 069f 23          	.byte 0x23
 4156 06a0 00          	.uleb128 0x0
 4157 06a1 0B          	.uleb128 0xb
 4158 06a2 53 4D 50 00 	.asciz "SMP"
 4159 06a6 03          	.byte 0x3
 4160 06a7 E5 07       	.2byte 0x7e5
 4161 06a9 F7 00 00 00 	.4byte 0xf7
 4162 06ad 02          	.byte 0x2
 4163 06ae 01          	.byte 0x1
 4164 06af 06          	.byte 0x6
 4165 06b0 02          	.byte 0x2
 4166 06b1 23          	.byte 0x23
 4167 06b2 00          	.uleb128 0x0
 4168 06b3 05          	.uleb128 0x5
 4169 06b4 00 00 00 00 	.4byte .LASF16
 4170 06b8 03          	.byte 0x3
MPLAB XC16 ASSEMBLY Listing:   			page 91


 4171 06b9 E6 07       	.2byte 0x7e6
 4172 06bb F7 00 00 00 	.4byte 0xf7
 4173 06bf 02          	.byte 0x2
 4174 06c0 01          	.byte 0x1
 4175 06c1 05          	.byte 0x5
 4176 06c2 02          	.byte 0x2
 4177 06c3 23          	.byte 0x23
 4178 06c4 00          	.uleb128 0x0
 4179 06c5 05          	.uleb128 0x5
 4180 06c6 00 00 00 00 	.4byte .LASF17
 4181 06ca 03          	.byte 0x3
 4182 06cb E7 07       	.2byte 0x7e7
 4183 06cd F7 00 00 00 	.4byte 0xf7
 4184 06d1 02          	.byte 0x2
 4185 06d2 01          	.byte 0x1
 4186 06d3 04          	.byte 0x4
 4187 06d4 02          	.byte 0x2
 4188 06d5 23          	.byte 0x23
 4189 06d6 00          	.uleb128 0x0
 4190 06d7 05          	.uleb128 0x5
 4191 06d8 00 00 00 00 	.4byte .LASF18
 4192 06dc 03          	.byte 0x3
 4193 06dd E8 07       	.2byte 0x7e8
 4194 06df F7 00 00 00 	.4byte 0xf7
 4195 06e3 02          	.byte 0x2
 4196 06e4 01          	.byte 0x1
 4197 06e5 03          	.byte 0x3
 4198 06e6 02          	.byte 0x2
 4199 06e7 23          	.byte 0x23
 4200 06e8 00          	.uleb128 0x0
 4201 06e9 00          	.byte 0x0
 4202 06ea 04          	.uleb128 0x4
 4203 06eb 02          	.byte 0x2
 4204 06ec 03          	.byte 0x3
 4205 06ed EA 07       	.2byte 0x7ea
 4206 06ef 4E 07 00 00 	.4byte 0x74e
 4207 06f3 05          	.uleb128 0x5
 4208 06f4 00 00 00 00 	.4byte .LASF19
 4209 06f8 03          	.byte 0x3
 4210 06f9 EB 07       	.2byte 0x7eb
 4211 06fb F7 00 00 00 	.4byte 0xf7
 4212 06ff 02          	.byte 0x2
 4213 0700 01          	.byte 0x1
 4214 0701 0F          	.byte 0xf
 4215 0702 02          	.byte 0x2
 4216 0703 23          	.byte 0x23
 4217 0704 00          	.uleb128 0x0
 4218 0705 05          	.uleb128 0x5
 4219 0706 00 00 00 00 	.4byte .LASF20
 4220 070a 03          	.byte 0x3
 4221 070b EC 07       	.2byte 0x7ec
 4222 070d F7 00 00 00 	.4byte 0xf7
 4223 0711 02          	.byte 0x2
 4224 0712 01          	.byte 0x1
 4225 0713 0E          	.byte 0xe
 4226 0714 02          	.byte 0x2
 4227 0715 23          	.byte 0x23
MPLAB XC16 ASSEMBLY Listing:   			page 92


 4228 0716 00          	.uleb128 0x0
 4229 0717 05          	.uleb128 0x5
 4230 0718 00 00 00 00 	.4byte .LASF21
 4231 071c 03          	.byte 0x3
 4232 071d ED 07       	.2byte 0x7ed
 4233 071f F7 00 00 00 	.4byte 0xf7
 4234 0723 02          	.byte 0x2
 4235 0724 01          	.byte 0x1
 4236 0725 0D          	.byte 0xd
 4237 0726 02          	.byte 0x2
 4238 0727 23          	.byte 0x23
 4239 0728 00          	.uleb128 0x0
 4240 0729 05          	.uleb128 0x5
 4241 072a 00 00 00 00 	.4byte .LASF22
 4242 072e 03          	.byte 0x3
 4243 072f EE 07       	.2byte 0x7ee
 4244 0731 F7 00 00 00 	.4byte 0xf7
 4245 0735 02          	.byte 0x2
 4246 0736 01          	.byte 0x1
 4247 0737 0C          	.byte 0xc
 4248 0738 02          	.byte 0x2
 4249 0739 23          	.byte 0x23
 4250 073a 00          	.uleb128 0x0
 4251 073b 05          	.uleb128 0x5
 4252 073c 00 00 00 00 	.4byte .LASF23
 4253 0740 03          	.byte 0x3
 4254 0741 EF 07       	.2byte 0x7ef
 4255 0743 F7 00 00 00 	.4byte 0xf7
 4256 0747 02          	.byte 0x2
 4257 0748 01          	.byte 0x1
 4258 0749 0B          	.byte 0xb
 4259 074a 02          	.byte 0x2
 4260 074b 23          	.byte 0x23
 4261 074c 00          	.uleb128 0x0
 4262 074d 00          	.byte 0x0
 4263 074e 06          	.uleb128 0x6
 4264 074f 02          	.byte 0x2
 4265 0750 03          	.byte 0x3
 4266 0751 DD 07       	.2byte 0x7dd
 4267 0753 62 07 00 00 	.4byte 0x762
 4268 0757 07          	.uleb128 0x7
 4269 0758 29 06 00 00 	.4byte 0x629
 4270 075c 07          	.uleb128 0x7
 4271 075d EA 06 00 00 	.4byte 0x6ea
 4272 0761 00          	.byte 0x0
 4273 0762 08          	.uleb128 0x8
 4274 0763 74 61 67 53 	.asciz "tagSPI2CON1BITS"
 4274      50 49 32 43 
 4274      4F 4E 31 42 
 4274      49 54 53 00 
 4275 0773 02          	.byte 0x2
 4276 0774 03          	.byte 0x3
 4277 0775 DC 07       	.2byte 0x7dc
 4278 0777 84 07 00 00 	.4byte 0x784
 4279 077b 09          	.uleb128 0x9
 4280 077c 4E 07 00 00 	.4byte 0x74e
 4281 0780 02          	.byte 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 93


 4282 0781 23          	.byte 0x23
 4283 0782 00          	.uleb128 0x0
 4284 0783 00          	.byte 0x0
 4285 0784 0A          	.uleb128 0xa
 4286 0785 53 50 49 32 	.asciz "SPI2CON1BITS"
 4286      43 4F 4E 31 
 4286      42 49 54 53 
 4286      00 
 4287 0792 03          	.byte 0x3
 4288 0793 F2 07       	.2byte 0x7f2
 4289 0795 62 07 00 00 	.4byte 0x762
 4290 0799 08          	.uleb128 0x8
 4291 079a 74 61 67 53 	.asciz "tagSPI2CON2BITS"
 4291      50 49 32 43 
 4291      4F 4E 32 42 
 4291      49 54 53 00 
 4292 07aa 02          	.byte 0x2
 4293 07ab 03          	.byte 0x3
 4294 07ac F7 07       	.2byte 0x7f7
 4295 07ae 0D 08 00 00 	.4byte 0x80d
 4296 07b2 05          	.uleb128 0x5
 4297 07b3 00 00 00 00 	.4byte .LASF24
 4298 07b7 03          	.byte 0x3
 4299 07b8 F8 07       	.2byte 0x7f8
 4300 07ba F7 00 00 00 	.4byte 0xf7
 4301 07be 02          	.byte 0x2
 4302 07bf 01          	.byte 0x1
 4303 07c0 0F          	.byte 0xf
 4304 07c1 02          	.byte 0x2
 4305 07c2 23          	.byte 0x23
 4306 07c3 00          	.uleb128 0x0
 4307 07c4 05          	.uleb128 0x5
 4308 07c5 00 00 00 00 	.4byte .LASF25
 4309 07c9 03          	.byte 0x3
 4310 07ca F9 07       	.2byte 0x7f9
 4311 07cc F7 00 00 00 	.4byte 0xf7
 4312 07d0 02          	.byte 0x2
 4313 07d1 01          	.byte 0x1
 4314 07d2 0E          	.byte 0xe
 4315 07d3 02          	.byte 0x2
 4316 07d4 23          	.byte 0x23
 4317 07d5 00          	.uleb128 0x0
 4318 07d6 05          	.uleb128 0x5
 4319 07d7 00 00 00 00 	.4byte .LASF26
 4320 07db 03          	.byte 0x3
 4321 07dc FB 07       	.2byte 0x7fb
 4322 07de F7 00 00 00 	.4byte 0xf7
 4323 07e2 02          	.byte 0x2
 4324 07e3 01          	.byte 0x1
 4325 07e4 02          	.byte 0x2
 4326 07e5 02          	.byte 0x2
 4327 07e6 23          	.byte 0x23
 4328 07e7 00          	.uleb128 0x0
 4329 07e8 05          	.uleb128 0x5
 4330 07e9 00 00 00 00 	.4byte .LASF27
 4331 07ed 03          	.byte 0x3
 4332 07ee FC 07       	.2byte 0x7fc
MPLAB XC16 ASSEMBLY Listing:   			page 94


 4333 07f0 F7 00 00 00 	.4byte 0xf7
 4334 07f4 02          	.byte 0x2
 4335 07f5 01          	.byte 0x1
 4336 07f6 01          	.byte 0x1
 4337 07f7 02          	.byte 0x2
 4338 07f8 23          	.byte 0x23
 4339 07f9 00          	.uleb128 0x0
 4340 07fa 05          	.uleb128 0x5
 4341 07fb 00 00 00 00 	.4byte .LASF28
 4342 07ff 03          	.byte 0x3
 4343 0800 FD 07       	.2byte 0x7fd
 4344 0802 F7 00 00 00 	.4byte 0xf7
 4345 0806 02          	.byte 0x2
 4346 0807 01          	.byte 0x1
 4347 0808 10          	.byte 0x10
 4348 0809 02          	.byte 0x2
 4349 080a 23          	.byte 0x23
 4350 080b 00          	.uleb128 0x0
 4351 080c 00          	.byte 0x0
 4352 080d 0A          	.uleb128 0xa
 4353 080e 53 50 49 32 	.asciz "SPI2CON2BITS"
 4353      43 4F 4E 32 
 4353      42 49 54 53 
 4353      00 
 4354 081b 03          	.byte 0x3
 4355 081c FE 07       	.2byte 0x7fe
 4356 081e 99 07 00 00 	.4byte 0x799
 4357 0822 04          	.uleb128 0x4
 4358 0823 02          	.byte 0x2
 4359 0824 03          	.byte 0x3
 4360 0825 AB 08       	.2byte 0x8ab
 4361 0827 CE 08 00 00 	.4byte 0x8ce
 4362 082b 05          	.uleb128 0x5
 4363 082c 00 00 00 00 	.4byte .LASF0
 4364 0830 03          	.byte 0x3
 4365 0831 AC 08       	.2byte 0x8ac
 4366 0833 F7 00 00 00 	.4byte 0xf7
 4367 0837 02          	.byte 0x2
 4368 0838 01          	.byte 0x1
 4369 0839 0F          	.byte 0xf
 4370 083a 02          	.byte 0x2
 4371 083b 23          	.byte 0x23
 4372 083c 00          	.uleb128 0x0
 4373 083d 05          	.uleb128 0x5
 4374 083e 00 00 00 00 	.4byte .LASF1
 4375 0842 03          	.byte 0x3
 4376 0843 AD 08       	.2byte 0x8ad
 4377 0845 F7 00 00 00 	.4byte 0xf7
 4378 0849 02          	.byte 0x2
 4379 084a 01          	.byte 0x1
 4380 084b 0E          	.byte 0xe
 4381 084c 02          	.byte 0x2
 4382 084d 23          	.byte 0x23
 4383 084e 00          	.uleb128 0x0
 4384 084f 05          	.uleb128 0x5
 4385 0850 00 00 00 00 	.4byte .LASF2
 4386 0854 03          	.byte 0x3
MPLAB XC16 ASSEMBLY Listing:   			page 95


 4387 0855 AE 08       	.2byte 0x8ae
 4388 0857 F7 00 00 00 	.4byte 0xf7
 4389 085b 02          	.byte 0x2
 4390 085c 03          	.byte 0x3
 4391 085d 0B          	.byte 0xb
 4392 085e 02          	.byte 0x2
 4393 085f 23          	.byte 0x23
 4394 0860 00          	.uleb128 0x0
 4395 0861 05          	.uleb128 0x5
 4396 0862 00 00 00 00 	.4byte .LASF3
 4397 0866 03          	.byte 0x3
 4398 0867 AF 08       	.2byte 0x8af
 4399 0869 F7 00 00 00 	.4byte 0xf7
 4400 086d 02          	.byte 0x2
 4401 086e 01          	.byte 0x1
 4402 086f 0A          	.byte 0xa
 4403 0870 02          	.byte 0x2
 4404 0871 23          	.byte 0x23
 4405 0872 00          	.uleb128 0x0
 4406 0873 05          	.uleb128 0x5
 4407 0874 00 00 00 00 	.4byte .LASF4
 4408 0878 03          	.byte 0x3
 4409 0879 B0 08       	.2byte 0x8b0
 4410 087b F7 00 00 00 	.4byte 0xf7
 4411 087f 02          	.byte 0x2
 4412 0880 01          	.byte 0x1
 4413 0881 09          	.byte 0x9
 4414 0882 02          	.byte 0x2
 4415 0883 23          	.byte 0x23
 4416 0884 00          	.uleb128 0x0
 4417 0885 05          	.uleb128 0x5
 4418 0886 00 00 00 00 	.4byte .LASF5
 4419 088a 03          	.byte 0x3
 4420 088b B1 08       	.2byte 0x8b1
 4421 088d F7 00 00 00 	.4byte 0xf7
 4422 0891 02          	.byte 0x2
 4423 0892 01          	.byte 0x1
 4424 0893 08          	.byte 0x8
 4425 0894 02          	.byte 0x2
 4426 0895 23          	.byte 0x23
 4427 0896 00          	.uleb128 0x0
 4428 0897 05          	.uleb128 0x5
 4429 0898 00 00 00 00 	.4byte .LASF6
 4430 089c 03          	.byte 0x3
 4431 089d B2 08       	.2byte 0x8b2
 4432 089f F7 00 00 00 	.4byte 0xf7
 4433 08a3 02          	.byte 0x2
 4434 08a4 03          	.byte 0x3
 4435 08a5 05          	.byte 0x5
 4436 08a6 02          	.byte 0x2
 4437 08a7 23          	.byte 0x23
 4438 08a8 00          	.uleb128 0x0
 4439 08a9 05          	.uleb128 0x5
 4440 08aa 00 00 00 00 	.4byte .LASF7
 4441 08ae 03          	.byte 0x3
 4442 08af B4 08       	.2byte 0x8b4
 4443 08b1 F7 00 00 00 	.4byte 0xf7
MPLAB XC16 ASSEMBLY Listing:   			page 96


 4444 08b5 02          	.byte 0x2
 4445 08b6 01          	.byte 0x1
 4446 08b7 02          	.byte 0x2
 4447 08b8 02          	.byte 0x2
 4448 08b9 23          	.byte 0x23
 4449 08ba 00          	.uleb128 0x0
 4450 08bb 05          	.uleb128 0x5
 4451 08bc 00 00 00 00 	.4byte .LASF8
 4452 08c0 03          	.byte 0x3
 4453 08c1 B6 08       	.2byte 0x8b6
 4454 08c3 F7 00 00 00 	.4byte 0xf7
 4455 08c7 02          	.byte 0x2
 4456 08c8 01          	.byte 0x1
 4457 08c9 10          	.byte 0x10
 4458 08ca 02          	.byte 0x2
 4459 08cb 23          	.byte 0x23
 4460 08cc 00          	.uleb128 0x0
 4461 08cd 00          	.byte 0x0
 4462 08ce 04          	.uleb128 0x4
 4463 08cf 02          	.byte 0x2
 4464 08d0 03          	.byte 0x3
 4465 08d1 B8 08       	.2byte 0x8b8
 4466 08d3 44 09 00 00 	.4byte 0x944
 4467 08d7 05          	.uleb128 0x5
 4468 08d8 00 00 00 00 	.4byte .LASF9
 4469 08dc 03          	.byte 0x3
 4470 08dd BA 08       	.2byte 0x8ba
 4471 08df F7 00 00 00 	.4byte 0xf7
 4472 08e3 02          	.byte 0x2
 4473 08e4 01          	.byte 0x1
 4474 08e5 0D          	.byte 0xd
 4475 08e6 02          	.byte 0x2
 4476 08e7 23          	.byte 0x23
 4477 08e8 00          	.uleb128 0x0
 4478 08e9 05          	.uleb128 0x5
 4479 08ea 00 00 00 00 	.4byte .LASF10
 4480 08ee 03          	.byte 0x3
 4481 08ef BB 08       	.2byte 0x8bb
 4482 08f1 F7 00 00 00 	.4byte 0xf7
 4483 08f5 02          	.byte 0x2
 4484 08f6 01          	.byte 0x1
 4485 08f7 0C          	.byte 0xc
 4486 08f8 02          	.byte 0x2
 4487 08f9 23          	.byte 0x23
 4488 08fa 00          	.uleb128 0x0
 4489 08fb 05          	.uleb128 0x5
 4490 08fc 00 00 00 00 	.4byte .LASF11
 4491 0900 03          	.byte 0x3
 4492 0901 BC 08       	.2byte 0x8bc
 4493 0903 F7 00 00 00 	.4byte 0xf7
 4494 0907 02          	.byte 0x2
 4495 0908 01          	.byte 0x1
 4496 0909 0B          	.byte 0xb
 4497 090a 02          	.byte 0x2
 4498 090b 23          	.byte 0x23
 4499 090c 00          	.uleb128 0x0
 4500 090d 05          	.uleb128 0x5
MPLAB XC16 ASSEMBLY Listing:   			page 97


 4501 090e 00 00 00 00 	.4byte .LASF12
 4502 0912 03          	.byte 0x3
 4503 0913 BE 08       	.2byte 0x8be
 4504 0915 F7 00 00 00 	.4byte 0xf7
 4505 0919 02          	.byte 0x2
 4506 091a 01          	.byte 0x1
 4507 091b 07          	.byte 0x7
 4508 091c 02          	.byte 0x2
 4509 091d 23          	.byte 0x23
 4510 091e 00          	.uleb128 0x0
 4511 091f 05          	.uleb128 0x5
 4512 0920 00 00 00 00 	.4byte .LASF13
 4513 0924 03          	.byte 0x3
 4514 0925 BF 08       	.2byte 0x8bf
 4515 0927 F7 00 00 00 	.4byte 0xf7
 4516 092b 02          	.byte 0x2
 4517 092c 01          	.byte 0x1
 4518 092d 06          	.byte 0x6
 4519 092e 02          	.byte 0x2
 4520 092f 23          	.byte 0x23
 4521 0930 00          	.uleb128 0x0
 4522 0931 05          	.uleb128 0x5
 4523 0932 00 00 00 00 	.4byte .LASF14
 4524 0936 03          	.byte 0x3
 4525 0937 C0 08       	.2byte 0x8c0
 4526 0939 F7 00 00 00 	.4byte 0xf7
 4527 093d 02          	.byte 0x2
 4528 093e 01          	.byte 0x1
 4529 093f 05          	.byte 0x5
 4530 0940 02          	.byte 0x2
 4531 0941 23          	.byte 0x23
 4532 0942 00          	.uleb128 0x0
 4533 0943 00          	.byte 0x0
 4534 0944 06          	.uleb128 0x6
 4535 0945 02          	.byte 0x2
 4536 0946 03          	.byte 0x3
 4537 0947 AA 08       	.2byte 0x8aa
 4538 0949 58 09 00 00 	.4byte 0x958
 4539 094d 07          	.uleb128 0x7
 4540 094e 22 08 00 00 	.4byte 0x822
 4541 0952 07          	.uleb128 0x7
 4542 0953 CE 08 00 00 	.4byte 0x8ce
 4543 0957 00          	.byte 0x0
 4544 0958 08          	.uleb128 0x8
 4545 0959 74 61 67 53 	.asciz "tagSPI3STATBITS"
 4545      50 49 33 53 
 4545      54 41 54 42 
 4545      49 54 53 00 
 4546 0969 02          	.byte 0x2
 4547 096a 03          	.byte 0x3
 4548 096b A9 08       	.2byte 0x8a9
 4549 096d 7A 09 00 00 	.4byte 0x97a
 4550 0971 09          	.uleb128 0x9
 4551 0972 44 09 00 00 	.4byte 0x944
 4552 0976 02          	.byte 0x2
 4553 0977 23          	.byte 0x23
 4554 0978 00          	.uleb128 0x0
MPLAB XC16 ASSEMBLY Listing:   			page 98


 4555 0979 00          	.byte 0x0
 4556 097a 0A          	.uleb128 0xa
 4557 097b 53 50 49 33 	.asciz "SPI3STATBITS"
 4557      53 54 41 54 
 4557      42 49 54 53 
 4557      00 
 4558 0988 03          	.byte 0x3
 4559 0989 C3 08       	.2byte 0x8c3
 4560 098b 58 09 00 00 	.4byte 0x958
 4561 098f 04          	.uleb128 0x4
 4562 0990 02          	.byte 0x2
 4563 0991 03          	.byte 0x3
 4564 0992 CA 08       	.2byte 0x8ca
 4565 0994 50 0A 00 00 	.4byte 0xa50
 4566 0998 0B          	.uleb128 0xb
 4567 0999 50 50 52 45 	.asciz "PPRE"
 4567      00 
 4568 099e 03          	.byte 0x3
 4569 099f CB 08       	.2byte 0x8cb
 4570 09a1 F7 00 00 00 	.4byte 0xf7
 4571 09a5 02          	.byte 0x2
 4572 09a6 02          	.byte 0x2
 4573 09a7 0E          	.byte 0xe
 4574 09a8 02          	.byte 0x2
 4575 09a9 23          	.byte 0x23
 4576 09aa 00          	.uleb128 0x0
 4577 09ab 0B          	.uleb128 0xb
 4578 09ac 53 50 52 45 	.asciz "SPRE"
 4578      00 
 4579 09b1 03          	.byte 0x3
 4580 09b2 CC 08       	.2byte 0x8cc
 4581 09b4 F7 00 00 00 	.4byte 0xf7
 4582 09b8 02          	.byte 0x2
 4583 09b9 03          	.byte 0x3
 4584 09ba 0B          	.byte 0xb
 4585 09bb 02          	.byte 0x2
 4586 09bc 23          	.byte 0x23
 4587 09bd 00          	.uleb128 0x0
 4588 09be 05          	.uleb128 0x5
 4589 09bf 00 00 00 00 	.4byte .LASF15
 4590 09c3 03          	.byte 0x3
 4591 09c4 CD 08       	.2byte 0x8cd
 4592 09c6 F7 00 00 00 	.4byte 0xf7
 4593 09ca 02          	.byte 0x2
 4594 09cb 01          	.byte 0x1
 4595 09cc 0A          	.byte 0xa
 4596 09cd 02          	.byte 0x2
 4597 09ce 23          	.byte 0x23
 4598 09cf 00          	.uleb128 0x0
 4599 09d0 0B          	.uleb128 0xb
 4600 09d1 43 4B 50 00 	.asciz "CKP"
 4601 09d5 03          	.byte 0x3
 4602 09d6 CE 08       	.2byte 0x8ce
 4603 09d8 F7 00 00 00 	.4byte 0xf7
 4604 09dc 02          	.byte 0x2
 4605 09dd 01          	.byte 0x1
 4606 09de 09          	.byte 0x9
MPLAB XC16 ASSEMBLY Listing:   			page 99


 4607 09df 02          	.byte 0x2
 4608 09e0 23          	.byte 0x23
 4609 09e1 00          	.uleb128 0x0
 4610 09e2 0B          	.uleb128 0xb
 4611 09e3 53 53 45 4E 	.asciz "SSEN"
 4611      00 
 4612 09e8 03          	.byte 0x3
 4613 09e9 CF 08       	.2byte 0x8cf
 4614 09eb F7 00 00 00 	.4byte 0xf7
 4615 09ef 02          	.byte 0x2
 4616 09f0 01          	.byte 0x1
 4617 09f1 08          	.byte 0x8
 4618 09f2 02          	.byte 0x2
 4619 09f3 23          	.byte 0x23
 4620 09f4 00          	.uleb128 0x0
 4621 09f5 0B          	.uleb128 0xb
 4622 09f6 43 4B 45 00 	.asciz "CKE"
 4623 09fa 03          	.byte 0x3
 4624 09fb D0 08       	.2byte 0x8d0
 4625 09fd F7 00 00 00 	.4byte 0xf7
 4626 0a01 02          	.byte 0x2
 4627 0a02 01          	.byte 0x1
 4628 0a03 07          	.byte 0x7
 4629 0a04 02          	.byte 0x2
 4630 0a05 23          	.byte 0x23
 4631 0a06 00          	.uleb128 0x0
 4632 0a07 0B          	.uleb128 0xb
 4633 0a08 53 4D 50 00 	.asciz "SMP"
 4634 0a0c 03          	.byte 0x3
 4635 0a0d D1 08       	.2byte 0x8d1
 4636 0a0f F7 00 00 00 	.4byte 0xf7
 4637 0a13 02          	.byte 0x2
 4638 0a14 01          	.byte 0x1
 4639 0a15 06          	.byte 0x6
 4640 0a16 02          	.byte 0x2
 4641 0a17 23          	.byte 0x23
 4642 0a18 00          	.uleb128 0x0
 4643 0a19 05          	.uleb128 0x5
 4644 0a1a 00 00 00 00 	.4byte .LASF16
 4645 0a1e 03          	.byte 0x3
 4646 0a1f D2 08       	.2byte 0x8d2
 4647 0a21 F7 00 00 00 	.4byte 0xf7
 4648 0a25 02          	.byte 0x2
 4649 0a26 01          	.byte 0x1
 4650 0a27 05          	.byte 0x5
 4651 0a28 02          	.byte 0x2
 4652 0a29 23          	.byte 0x23
 4653 0a2a 00          	.uleb128 0x0
 4654 0a2b 05          	.uleb128 0x5
 4655 0a2c 00 00 00 00 	.4byte .LASF17
 4656 0a30 03          	.byte 0x3
 4657 0a31 D3 08       	.2byte 0x8d3
 4658 0a33 F7 00 00 00 	.4byte 0xf7
 4659 0a37 02          	.byte 0x2
 4660 0a38 01          	.byte 0x1
 4661 0a39 04          	.byte 0x4
 4662 0a3a 02          	.byte 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 100


 4663 0a3b 23          	.byte 0x23
 4664 0a3c 00          	.uleb128 0x0
 4665 0a3d 05          	.uleb128 0x5
 4666 0a3e 00 00 00 00 	.4byte .LASF18
 4667 0a42 03          	.byte 0x3
 4668 0a43 D4 08       	.2byte 0x8d4
 4669 0a45 F7 00 00 00 	.4byte 0xf7
 4670 0a49 02          	.byte 0x2
 4671 0a4a 01          	.byte 0x1
 4672 0a4b 03          	.byte 0x3
 4673 0a4c 02          	.byte 0x2
 4674 0a4d 23          	.byte 0x23
 4675 0a4e 00          	.uleb128 0x0
 4676 0a4f 00          	.byte 0x0
 4677 0a50 04          	.uleb128 0x4
 4678 0a51 02          	.byte 0x2
 4679 0a52 03          	.byte 0x3
 4680 0a53 D6 08       	.2byte 0x8d6
 4681 0a55 B4 0A 00 00 	.4byte 0xab4
 4682 0a59 05          	.uleb128 0x5
 4683 0a5a 00 00 00 00 	.4byte .LASF19
 4684 0a5e 03          	.byte 0x3
 4685 0a5f D7 08       	.2byte 0x8d7
 4686 0a61 F7 00 00 00 	.4byte 0xf7
 4687 0a65 02          	.byte 0x2
 4688 0a66 01          	.byte 0x1
 4689 0a67 0F          	.byte 0xf
 4690 0a68 02          	.byte 0x2
 4691 0a69 23          	.byte 0x23
 4692 0a6a 00          	.uleb128 0x0
 4693 0a6b 05          	.uleb128 0x5
 4694 0a6c 00 00 00 00 	.4byte .LASF20
 4695 0a70 03          	.byte 0x3
 4696 0a71 D8 08       	.2byte 0x8d8
 4697 0a73 F7 00 00 00 	.4byte 0xf7
 4698 0a77 02          	.byte 0x2
 4699 0a78 01          	.byte 0x1
 4700 0a79 0E          	.byte 0xe
 4701 0a7a 02          	.byte 0x2
 4702 0a7b 23          	.byte 0x23
 4703 0a7c 00          	.uleb128 0x0
 4704 0a7d 05          	.uleb128 0x5
 4705 0a7e 00 00 00 00 	.4byte .LASF21
 4706 0a82 03          	.byte 0x3
 4707 0a83 D9 08       	.2byte 0x8d9
 4708 0a85 F7 00 00 00 	.4byte 0xf7
 4709 0a89 02          	.byte 0x2
 4710 0a8a 01          	.byte 0x1
 4711 0a8b 0D          	.byte 0xd
 4712 0a8c 02          	.byte 0x2
 4713 0a8d 23          	.byte 0x23
 4714 0a8e 00          	.uleb128 0x0
 4715 0a8f 05          	.uleb128 0x5
 4716 0a90 00 00 00 00 	.4byte .LASF22
 4717 0a94 03          	.byte 0x3
 4718 0a95 DA 08       	.2byte 0x8da
 4719 0a97 F7 00 00 00 	.4byte 0xf7
MPLAB XC16 ASSEMBLY Listing:   			page 101


 4720 0a9b 02          	.byte 0x2
 4721 0a9c 01          	.byte 0x1
 4722 0a9d 0C          	.byte 0xc
 4723 0a9e 02          	.byte 0x2
 4724 0a9f 23          	.byte 0x23
 4725 0aa0 00          	.uleb128 0x0
 4726 0aa1 05          	.uleb128 0x5
 4727 0aa2 00 00 00 00 	.4byte .LASF23
 4728 0aa6 03          	.byte 0x3
 4729 0aa7 DB 08       	.2byte 0x8db
 4730 0aa9 F7 00 00 00 	.4byte 0xf7
 4731 0aad 02          	.byte 0x2
 4732 0aae 01          	.byte 0x1
 4733 0aaf 0B          	.byte 0xb
 4734 0ab0 02          	.byte 0x2
 4735 0ab1 23          	.byte 0x23
 4736 0ab2 00          	.uleb128 0x0
 4737 0ab3 00          	.byte 0x0
 4738 0ab4 06          	.uleb128 0x6
 4739 0ab5 02          	.byte 0x2
 4740 0ab6 03          	.byte 0x3
 4741 0ab7 C9 08       	.2byte 0x8c9
 4742 0ab9 C8 0A 00 00 	.4byte 0xac8
 4743 0abd 07          	.uleb128 0x7
 4744 0abe 8F 09 00 00 	.4byte 0x98f
 4745 0ac2 07          	.uleb128 0x7
 4746 0ac3 50 0A 00 00 	.4byte 0xa50
 4747 0ac7 00          	.byte 0x0
 4748 0ac8 08          	.uleb128 0x8
 4749 0ac9 74 61 67 53 	.asciz "tagSPI3CON1BITS"
 4749      50 49 33 43 
 4749      4F 4E 31 42 
 4749      49 54 53 00 
 4750 0ad9 02          	.byte 0x2
 4751 0ada 03          	.byte 0x3
 4752 0adb C8 08       	.2byte 0x8c8
 4753 0add EA 0A 00 00 	.4byte 0xaea
 4754 0ae1 09          	.uleb128 0x9
 4755 0ae2 B4 0A 00 00 	.4byte 0xab4
 4756 0ae6 02          	.byte 0x2
 4757 0ae7 23          	.byte 0x23
 4758 0ae8 00          	.uleb128 0x0
 4759 0ae9 00          	.byte 0x0
 4760 0aea 0A          	.uleb128 0xa
 4761 0aeb 53 50 49 33 	.asciz "SPI3CON1BITS"
 4761      43 4F 4E 31 
 4761      42 49 54 53 
 4761      00 
 4762 0af8 03          	.byte 0x3
 4763 0af9 DE 08       	.2byte 0x8de
 4764 0afb C8 0A 00 00 	.4byte 0xac8
 4765 0aff 08          	.uleb128 0x8
 4766 0b00 74 61 67 53 	.asciz "tagSPI3CON2BITS"
 4766      50 49 33 43 
 4766      4F 4E 32 42 
 4766      49 54 53 00 
 4767 0b10 02          	.byte 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 102


 4768 0b11 03          	.byte 0x3
 4769 0b12 E3 08       	.2byte 0x8e3
 4770 0b14 73 0B 00 00 	.4byte 0xb73
 4771 0b18 05          	.uleb128 0x5
 4772 0b19 00 00 00 00 	.4byte .LASF24
 4773 0b1d 03          	.byte 0x3
 4774 0b1e E4 08       	.2byte 0x8e4
 4775 0b20 F7 00 00 00 	.4byte 0xf7
 4776 0b24 02          	.byte 0x2
 4777 0b25 01          	.byte 0x1
 4778 0b26 0F          	.byte 0xf
 4779 0b27 02          	.byte 0x2
 4780 0b28 23          	.byte 0x23
 4781 0b29 00          	.uleb128 0x0
 4782 0b2a 05          	.uleb128 0x5
 4783 0b2b 00 00 00 00 	.4byte .LASF25
 4784 0b2f 03          	.byte 0x3
 4785 0b30 E5 08       	.2byte 0x8e5
 4786 0b32 F7 00 00 00 	.4byte 0xf7
 4787 0b36 02          	.byte 0x2
 4788 0b37 01          	.byte 0x1
 4789 0b38 0E          	.byte 0xe
 4790 0b39 02          	.byte 0x2
 4791 0b3a 23          	.byte 0x23
 4792 0b3b 00          	.uleb128 0x0
 4793 0b3c 05          	.uleb128 0x5
 4794 0b3d 00 00 00 00 	.4byte .LASF26
 4795 0b41 03          	.byte 0x3
 4796 0b42 E7 08       	.2byte 0x8e7
 4797 0b44 F7 00 00 00 	.4byte 0xf7
 4798 0b48 02          	.byte 0x2
 4799 0b49 01          	.byte 0x1
 4800 0b4a 02          	.byte 0x2
 4801 0b4b 02          	.byte 0x2
 4802 0b4c 23          	.byte 0x23
 4803 0b4d 00          	.uleb128 0x0
 4804 0b4e 05          	.uleb128 0x5
 4805 0b4f 00 00 00 00 	.4byte .LASF27
 4806 0b53 03          	.byte 0x3
 4807 0b54 E8 08       	.2byte 0x8e8
 4808 0b56 F7 00 00 00 	.4byte 0xf7
 4809 0b5a 02          	.byte 0x2
 4810 0b5b 01          	.byte 0x1
 4811 0b5c 01          	.byte 0x1
 4812 0b5d 02          	.byte 0x2
 4813 0b5e 23          	.byte 0x23
 4814 0b5f 00          	.uleb128 0x0
 4815 0b60 05          	.uleb128 0x5
 4816 0b61 00 00 00 00 	.4byte .LASF28
 4817 0b65 03          	.byte 0x3
 4818 0b66 E9 08       	.2byte 0x8e9
 4819 0b68 F7 00 00 00 	.4byte 0xf7
 4820 0b6c 02          	.byte 0x2
 4821 0b6d 01          	.byte 0x1
 4822 0b6e 10          	.byte 0x10
 4823 0b6f 02          	.byte 0x2
 4824 0b70 23          	.byte 0x23
MPLAB XC16 ASSEMBLY Listing:   			page 103


 4825 0b71 00          	.uleb128 0x0
 4826 0b72 00          	.byte 0x0
 4827 0b73 0A          	.uleb128 0xa
 4828 0b74 53 50 49 33 	.asciz "SPI3CON2BITS"
 4828      43 4F 4E 32 
 4828      42 49 54 53 
 4828      00 
 4829 0b81 03          	.byte 0x3
 4830 0b82 EA 08       	.2byte 0x8ea
 4831 0b84 FF 0A 00 00 	.4byte 0xaff
 4832 0b88 04          	.uleb128 0x4
 4833 0b89 02          	.byte 0x2
 4834 0b8a 03          	.byte 0x3
 4835 0b8b 3B 09       	.2byte 0x93b
 4836 0b8d 34 0C 00 00 	.4byte 0xc34
 4837 0b91 05          	.uleb128 0x5
 4838 0b92 00 00 00 00 	.4byte .LASF0
 4839 0b96 03          	.byte 0x3
 4840 0b97 3C 09       	.2byte 0x93c
 4841 0b99 F7 00 00 00 	.4byte 0xf7
 4842 0b9d 02          	.byte 0x2
 4843 0b9e 01          	.byte 0x1
 4844 0b9f 0F          	.byte 0xf
 4845 0ba0 02          	.byte 0x2
 4846 0ba1 23          	.byte 0x23
 4847 0ba2 00          	.uleb128 0x0
 4848 0ba3 05          	.uleb128 0x5
 4849 0ba4 00 00 00 00 	.4byte .LASF1
 4850 0ba8 03          	.byte 0x3
 4851 0ba9 3D 09       	.2byte 0x93d
 4852 0bab F7 00 00 00 	.4byte 0xf7
 4853 0baf 02          	.byte 0x2
 4854 0bb0 01          	.byte 0x1
 4855 0bb1 0E          	.byte 0xe
 4856 0bb2 02          	.byte 0x2
 4857 0bb3 23          	.byte 0x23
 4858 0bb4 00          	.uleb128 0x0
 4859 0bb5 05          	.uleb128 0x5
 4860 0bb6 00 00 00 00 	.4byte .LASF2
 4861 0bba 03          	.byte 0x3
 4862 0bbb 3E 09       	.2byte 0x93e
 4863 0bbd F7 00 00 00 	.4byte 0xf7
 4864 0bc1 02          	.byte 0x2
 4865 0bc2 03          	.byte 0x3
 4866 0bc3 0B          	.byte 0xb
 4867 0bc4 02          	.byte 0x2
 4868 0bc5 23          	.byte 0x23
 4869 0bc6 00          	.uleb128 0x0
 4870 0bc7 05          	.uleb128 0x5
 4871 0bc8 00 00 00 00 	.4byte .LASF3
 4872 0bcc 03          	.byte 0x3
 4873 0bcd 3F 09       	.2byte 0x93f
 4874 0bcf F7 00 00 00 	.4byte 0xf7
 4875 0bd3 02          	.byte 0x2
 4876 0bd4 01          	.byte 0x1
 4877 0bd5 0A          	.byte 0xa
 4878 0bd6 02          	.byte 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 104


 4879 0bd7 23          	.byte 0x23
 4880 0bd8 00          	.uleb128 0x0
 4881 0bd9 05          	.uleb128 0x5
 4882 0bda 00 00 00 00 	.4byte .LASF4
 4883 0bde 03          	.byte 0x3
 4884 0bdf 40 09       	.2byte 0x940
 4885 0be1 F7 00 00 00 	.4byte 0xf7
 4886 0be5 02          	.byte 0x2
 4887 0be6 01          	.byte 0x1
 4888 0be7 09          	.byte 0x9
 4889 0be8 02          	.byte 0x2
 4890 0be9 23          	.byte 0x23
 4891 0bea 00          	.uleb128 0x0
 4892 0beb 05          	.uleb128 0x5
 4893 0bec 00 00 00 00 	.4byte .LASF5
 4894 0bf0 03          	.byte 0x3
 4895 0bf1 41 09       	.2byte 0x941
 4896 0bf3 F7 00 00 00 	.4byte 0xf7
 4897 0bf7 02          	.byte 0x2
 4898 0bf8 01          	.byte 0x1
 4899 0bf9 08          	.byte 0x8
 4900 0bfa 02          	.byte 0x2
 4901 0bfb 23          	.byte 0x23
 4902 0bfc 00          	.uleb128 0x0
 4903 0bfd 05          	.uleb128 0x5
 4904 0bfe 00 00 00 00 	.4byte .LASF6
 4905 0c02 03          	.byte 0x3
 4906 0c03 42 09       	.2byte 0x942
 4907 0c05 F7 00 00 00 	.4byte 0xf7
 4908 0c09 02          	.byte 0x2
 4909 0c0a 03          	.byte 0x3
 4910 0c0b 05          	.byte 0x5
 4911 0c0c 02          	.byte 0x2
 4912 0c0d 23          	.byte 0x23
 4913 0c0e 00          	.uleb128 0x0
 4914 0c0f 05          	.uleb128 0x5
 4915 0c10 00 00 00 00 	.4byte .LASF7
 4916 0c14 03          	.byte 0x3
 4917 0c15 44 09       	.2byte 0x944
 4918 0c17 F7 00 00 00 	.4byte 0xf7
 4919 0c1b 02          	.byte 0x2
 4920 0c1c 01          	.byte 0x1
 4921 0c1d 02          	.byte 0x2
 4922 0c1e 02          	.byte 0x2
 4923 0c1f 23          	.byte 0x23
 4924 0c20 00          	.uleb128 0x0
 4925 0c21 05          	.uleb128 0x5
 4926 0c22 00 00 00 00 	.4byte .LASF8
 4927 0c26 03          	.byte 0x3
 4928 0c27 46 09       	.2byte 0x946
 4929 0c29 F7 00 00 00 	.4byte 0xf7
 4930 0c2d 02          	.byte 0x2
 4931 0c2e 01          	.byte 0x1
 4932 0c2f 10          	.byte 0x10
 4933 0c30 02          	.byte 0x2
 4934 0c31 23          	.byte 0x23
 4935 0c32 00          	.uleb128 0x0
MPLAB XC16 ASSEMBLY Listing:   			page 105


 4936 0c33 00          	.byte 0x0
 4937 0c34 04          	.uleb128 0x4
 4938 0c35 02          	.byte 0x2
 4939 0c36 03          	.byte 0x3
 4940 0c37 48 09       	.2byte 0x948
 4941 0c39 AA 0C 00 00 	.4byte 0xcaa
 4942 0c3d 05          	.uleb128 0x5
 4943 0c3e 00 00 00 00 	.4byte .LASF9
 4944 0c42 03          	.byte 0x3
 4945 0c43 4A 09       	.2byte 0x94a
 4946 0c45 F7 00 00 00 	.4byte 0xf7
 4947 0c49 02          	.byte 0x2
 4948 0c4a 01          	.byte 0x1
 4949 0c4b 0D          	.byte 0xd
 4950 0c4c 02          	.byte 0x2
 4951 0c4d 23          	.byte 0x23
 4952 0c4e 00          	.uleb128 0x0
 4953 0c4f 05          	.uleb128 0x5
 4954 0c50 00 00 00 00 	.4byte .LASF10
 4955 0c54 03          	.byte 0x3
 4956 0c55 4B 09       	.2byte 0x94b
 4957 0c57 F7 00 00 00 	.4byte 0xf7
 4958 0c5b 02          	.byte 0x2
 4959 0c5c 01          	.byte 0x1
 4960 0c5d 0C          	.byte 0xc
 4961 0c5e 02          	.byte 0x2
 4962 0c5f 23          	.byte 0x23
 4963 0c60 00          	.uleb128 0x0
 4964 0c61 05          	.uleb128 0x5
 4965 0c62 00 00 00 00 	.4byte .LASF11
 4966 0c66 03          	.byte 0x3
 4967 0c67 4C 09       	.2byte 0x94c
 4968 0c69 F7 00 00 00 	.4byte 0xf7
 4969 0c6d 02          	.byte 0x2
 4970 0c6e 01          	.byte 0x1
 4971 0c6f 0B          	.byte 0xb
 4972 0c70 02          	.byte 0x2
 4973 0c71 23          	.byte 0x23
 4974 0c72 00          	.uleb128 0x0
 4975 0c73 05          	.uleb128 0x5
 4976 0c74 00 00 00 00 	.4byte .LASF12
 4977 0c78 03          	.byte 0x3
 4978 0c79 4E 09       	.2byte 0x94e
 4979 0c7b F7 00 00 00 	.4byte 0xf7
 4980 0c7f 02          	.byte 0x2
 4981 0c80 01          	.byte 0x1
 4982 0c81 07          	.byte 0x7
 4983 0c82 02          	.byte 0x2
 4984 0c83 23          	.byte 0x23
 4985 0c84 00          	.uleb128 0x0
 4986 0c85 05          	.uleb128 0x5
 4987 0c86 00 00 00 00 	.4byte .LASF13
 4988 0c8a 03          	.byte 0x3
 4989 0c8b 4F 09       	.2byte 0x94f
 4990 0c8d F7 00 00 00 	.4byte 0xf7
 4991 0c91 02          	.byte 0x2
 4992 0c92 01          	.byte 0x1
MPLAB XC16 ASSEMBLY Listing:   			page 106


 4993 0c93 06          	.byte 0x6
 4994 0c94 02          	.byte 0x2
 4995 0c95 23          	.byte 0x23
 4996 0c96 00          	.uleb128 0x0
 4997 0c97 05          	.uleb128 0x5
 4998 0c98 00 00 00 00 	.4byte .LASF14
 4999 0c9c 03          	.byte 0x3
 5000 0c9d 50 09       	.2byte 0x950
 5001 0c9f F7 00 00 00 	.4byte 0xf7
 5002 0ca3 02          	.byte 0x2
 5003 0ca4 01          	.byte 0x1
 5004 0ca5 05          	.byte 0x5
 5005 0ca6 02          	.byte 0x2
 5006 0ca7 23          	.byte 0x23
 5007 0ca8 00          	.uleb128 0x0
 5008 0ca9 00          	.byte 0x0
 5009 0caa 06          	.uleb128 0x6
 5010 0cab 02          	.byte 0x2
 5011 0cac 03          	.byte 0x3
 5012 0cad 3A 09       	.2byte 0x93a
 5013 0caf BE 0C 00 00 	.4byte 0xcbe
 5014 0cb3 07          	.uleb128 0x7
 5015 0cb4 88 0B 00 00 	.4byte 0xb88
 5016 0cb8 07          	.uleb128 0x7
 5017 0cb9 34 0C 00 00 	.4byte 0xc34
 5018 0cbd 00          	.byte 0x0
 5019 0cbe 08          	.uleb128 0x8
 5020 0cbf 74 61 67 53 	.asciz "tagSPI4STATBITS"
 5020      50 49 34 53 
 5020      54 41 54 42 
 5020      49 54 53 00 
 5021 0ccf 02          	.byte 0x2
 5022 0cd0 03          	.byte 0x3
 5023 0cd1 39 09       	.2byte 0x939
 5024 0cd3 E0 0C 00 00 	.4byte 0xce0
 5025 0cd7 09          	.uleb128 0x9
 5026 0cd8 AA 0C 00 00 	.4byte 0xcaa
 5027 0cdc 02          	.byte 0x2
 5028 0cdd 23          	.byte 0x23
 5029 0cde 00          	.uleb128 0x0
 5030 0cdf 00          	.byte 0x0
 5031 0ce0 0A          	.uleb128 0xa
 5032 0ce1 53 50 49 34 	.asciz "SPI4STATBITS"
 5032      53 54 41 54 
 5032      42 49 54 53 
 5032      00 
 5033 0cee 03          	.byte 0x3
 5034 0cef 53 09       	.2byte 0x953
 5035 0cf1 BE 0C 00 00 	.4byte 0xcbe
 5036 0cf5 04          	.uleb128 0x4
 5037 0cf6 02          	.byte 0x2
 5038 0cf7 03          	.byte 0x3
 5039 0cf8 5A 09       	.2byte 0x95a
 5040 0cfa B6 0D 00 00 	.4byte 0xdb6
 5041 0cfe 0B          	.uleb128 0xb
 5042 0cff 50 50 52 45 	.asciz "PPRE"
 5042      00 
MPLAB XC16 ASSEMBLY Listing:   			page 107


 5043 0d04 03          	.byte 0x3
 5044 0d05 5B 09       	.2byte 0x95b
 5045 0d07 F7 00 00 00 	.4byte 0xf7
 5046 0d0b 02          	.byte 0x2
 5047 0d0c 02          	.byte 0x2
 5048 0d0d 0E          	.byte 0xe
 5049 0d0e 02          	.byte 0x2
 5050 0d0f 23          	.byte 0x23
 5051 0d10 00          	.uleb128 0x0
 5052 0d11 0B          	.uleb128 0xb
 5053 0d12 53 50 52 45 	.asciz "SPRE"
 5053      00 
 5054 0d17 03          	.byte 0x3
 5055 0d18 5C 09       	.2byte 0x95c
 5056 0d1a F7 00 00 00 	.4byte 0xf7
 5057 0d1e 02          	.byte 0x2
 5058 0d1f 03          	.byte 0x3
 5059 0d20 0B          	.byte 0xb
 5060 0d21 02          	.byte 0x2
 5061 0d22 23          	.byte 0x23
 5062 0d23 00          	.uleb128 0x0
 5063 0d24 05          	.uleb128 0x5
 5064 0d25 00 00 00 00 	.4byte .LASF15
 5065 0d29 03          	.byte 0x3
 5066 0d2a 5D 09       	.2byte 0x95d
 5067 0d2c F7 00 00 00 	.4byte 0xf7
 5068 0d30 02          	.byte 0x2
 5069 0d31 01          	.byte 0x1
 5070 0d32 0A          	.byte 0xa
 5071 0d33 02          	.byte 0x2
 5072 0d34 23          	.byte 0x23
 5073 0d35 00          	.uleb128 0x0
 5074 0d36 0B          	.uleb128 0xb
 5075 0d37 43 4B 50 00 	.asciz "CKP"
 5076 0d3b 03          	.byte 0x3
 5077 0d3c 5E 09       	.2byte 0x95e
 5078 0d3e F7 00 00 00 	.4byte 0xf7
 5079 0d42 02          	.byte 0x2
 5080 0d43 01          	.byte 0x1
 5081 0d44 09          	.byte 0x9
 5082 0d45 02          	.byte 0x2
 5083 0d46 23          	.byte 0x23
 5084 0d47 00          	.uleb128 0x0
 5085 0d48 0B          	.uleb128 0xb
 5086 0d49 53 53 45 4E 	.asciz "SSEN"
 5086      00 
 5087 0d4e 03          	.byte 0x3
 5088 0d4f 5F 09       	.2byte 0x95f
 5089 0d51 F7 00 00 00 	.4byte 0xf7
 5090 0d55 02          	.byte 0x2
 5091 0d56 01          	.byte 0x1
 5092 0d57 08          	.byte 0x8
 5093 0d58 02          	.byte 0x2
 5094 0d59 23          	.byte 0x23
 5095 0d5a 00          	.uleb128 0x0
 5096 0d5b 0B          	.uleb128 0xb
 5097 0d5c 43 4B 45 00 	.asciz "CKE"
MPLAB XC16 ASSEMBLY Listing:   			page 108


 5098 0d60 03          	.byte 0x3
 5099 0d61 60 09       	.2byte 0x960
 5100 0d63 F7 00 00 00 	.4byte 0xf7
 5101 0d67 02          	.byte 0x2
 5102 0d68 01          	.byte 0x1
 5103 0d69 07          	.byte 0x7
 5104 0d6a 02          	.byte 0x2
 5105 0d6b 23          	.byte 0x23
 5106 0d6c 00          	.uleb128 0x0
 5107 0d6d 0B          	.uleb128 0xb
 5108 0d6e 53 4D 50 00 	.asciz "SMP"
 5109 0d72 03          	.byte 0x3
 5110 0d73 61 09       	.2byte 0x961
 5111 0d75 F7 00 00 00 	.4byte 0xf7
 5112 0d79 02          	.byte 0x2
 5113 0d7a 01          	.byte 0x1
 5114 0d7b 06          	.byte 0x6
 5115 0d7c 02          	.byte 0x2
 5116 0d7d 23          	.byte 0x23
 5117 0d7e 00          	.uleb128 0x0
 5118 0d7f 05          	.uleb128 0x5
 5119 0d80 00 00 00 00 	.4byte .LASF16
 5120 0d84 03          	.byte 0x3
 5121 0d85 62 09       	.2byte 0x962
 5122 0d87 F7 00 00 00 	.4byte 0xf7
 5123 0d8b 02          	.byte 0x2
 5124 0d8c 01          	.byte 0x1
 5125 0d8d 05          	.byte 0x5
 5126 0d8e 02          	.byte 0x2
 5127 0d8f 23          	.byte 0x23
 5128 0d90 00          	.uleb128 0x0
 5129 0d91 05          	.uleb128 0x5
 5130 0d92 00 00 00 00 	.4byte .LASF17
 5131 0d96 03          	.byte 0x3
 5132 0d97 63 09       	.2byte 0x963
 5133 0d99 F7 00 00 00 	.4byte 0xf7
 5134 0d9d 02          	.byte 0x2
 5135 0d9e 01          	.byte 0x1
 5136 0d9f 04          	.byte 0x4
 5137 0da0 02          	.byte 0x2
 5138 0da1 23          	.byte 0x23
 5139 0da2 00          	.uleb128 0x0
 5140 0da3 05          	.uleb128 0x5
 5141 0da4 00 00 00 00 	.4byte .LASF18
 5142 0da8 03          	.byte 0x3
 5143 0da9 64 09       	.2byte 0x964
 5144 0dab F7 00 00 00 	.4byte 0xf7
 5145 0daf 02          	.byte 0x2
 5146 0db0 01          	.byte 0x1
 5147 0db1 03          	.byte 0x3
 5148 0db2 02          	.byte 0x2
 5149 0db3 23          	.byte 0x23
 5150 0db4 00          	.uleb128 0x0
 5151 0db5 00          	.byte 0x0
 5152 0db6 04          	.uleb128 0x4
 5153 0db7 02          	.byte 0x2
 5154 0db8 03          	.byte 0x3
MPLAB XC16 ASSEMBLY Listing:   			page 109


 5155 0db9 66 09       	.2byte 0x966
 5156 0dbb 1A 0E 00 00 	.4byte 0xe1a
 5157 0dbf 05          	.uleb128 0x5
 5158 0dc0 00 00 00 00 	.4byte .LASF19
 5159 0dc4 03          	.byte 0x3
 5160 0dc5 67 09       	.2byte 0x967
 5161 0dc7 F7 00 00 00 	.4byte 0xf7
 5162 0dcb 02          	.byte 0x2
 5163 0dcc 01          	.byte 0x1
 5164 0dcd 0F          	.byte 0xf
 5165 0dce 02          	.byte 0x2
 5166 0dcf 23          	.byte 0x23
 5167 0dd0 00          	.uleb128 0x0
 5168 0dd1 05          	.uleb128 0x5
 5169 0dd2 00 00 00 00 	.4byte .LASF20
 5170 0dd6 03          	.byte 0x3
 5171 0dd7 68 09       	.2byte 0x968
 5172 0dd9 F7 00 00 00 	.4byte 0xf7
 5173 0ddd 02          	.byte 0x2
 5174 0dde 01          	.byte 0x1
 5175 0ddf 0E          	.byte 0xe
 5176 0de0 02          	.byte 0x2
 5177 0de1 23          	.byte 0x23
 5178 0de2 00          	.uleb128 0x0
 5179 0de3 05          	.uleb128 0x5
 5180 0de4 00 00 00 00 	.4byte .LASF21
 5181 0de8 03          	.byte 0x3
 5182 0de9 69 09       	.2byte 0x969
 5183 0deb F7 00 00 00 	.4byte 0xf7
 5184 0def 02          	.byte 0x2
 5185 0df0 01          	.byte 0x1
 5186 0df1 0D          	.byte 0xd
 5187 0df2 02          	.byte 0x2
 5188 0df3 23          	.byte 0x23
 5189 0df4 00          	.uleb128 0x0
 5190 0df5 05          	.uleb128 0x5
 5191 0df6 00 00 00 00 	.4byte .LASF22
 5192 0dfa 03          	.byte 0x3
 5193 0dfb 6A 09       	.2byte 0x96a
 5194 0dfd F7 00 00 00 	.4byte 0xf7
 5195 0e01 02          	.byte 0x2
 5196 0e02 01          	.byte 0x1
 5197 0e03 0C          	.byte 0xc
 5198 0e04 02          	.byte 0x2
 5199 0e05 23          	.byte 0x23
 5200 0e06 00          	.uleb128 0x0
 5201 0e07 05          	.uleb128 0x5
 5202 0e08 00 00 00 00 	.4byte .LASF23
 5203 0e0c 03          	.byte 0x3
 5204 0e0d 6B 09       	.2byte 0x96b
 5205 0e0f F7 00 00 00 	.4byte 0xf7
 5206 0e13 02          	.byte 0x2
 5207 0e14 01          	.byte 0x1
 5208 0e15 0B          	.byte 0xb
 5209 0e16 02          	.byte 0x2
 5210 0e17 23          	.byte 0x23
 5211 0e18 00          	.uleb128 0x0
MPLAB XC16 ASSEMBLY Listing:   			page 110


 5212 0e19 00          	.byte 0x0
 5213 0e1a 06          	.uleb128 0x6
 5214 0e1b 02          	.byte 0x2
 5215 0e1c 03          	.byte 0x3
 5216 0e1d 59 09       	.2byte 0x959
 5217 0e1f 2E 0E 00 00 	.4byte 0xe2e
 5218 0e23 07          	.uleb128 0x7
 5219 0e24 F5 0C 00 00 	.4byte 0xcf5
 5220 0e28 07          	.uleb128 0x7
 5221 0e29 B6 0D 00 00 	.4byte 0xdb6
 5222 0e2d 00          	.byte 0x0
 5223 0e2e 08          	.uleb128 0x8
 5224 0e2f 74 61 67 53 	.asciz "tagSPI4CON1BITS"
 5224      50 49 34 43 
 5224      4F 4E 31 42 
 5224      49 54 53 00 
 5225 0e3f 02          	.byte 0x2
 5226 0e40 03          	.byte 0x3
 5227 0e41 58 09       	.2byte 0x958
 5228 0e43 50 0E 00 00 	.4byte 0xe50
 5229 0e47 09          	.uleb128 0x9
 5230 0e48 1A 0E 00 00 	.4byte 0xe1a
 5231 0e4c 02          	.byte 0x2
 5232 0e4d 23          	.byte 0x23
 5233 0e4e 00          	.uleb128 0x0
 5234 0e4f 00          	.byte 0x0
 5235 0e50 0A          	.uleb128 0xa
 5236 0e51 53 50 49 34 	.asciz "SPI4CON1BITS"
 5236      43 4F 4E 31 
 5236      42 49 54 53 
 5236      00 
 5237 0e5e 03          	.byte 0x3
 5238 0e5f 6E 09       	.2byte 0x96e
 5239 0e61 2E 0E 00 00 	.4byte 0xe2e
 5240 0e65 08          	.uleb128 0x8
 5241 0e66 74 61 67 53 	.asciz "tagSPI4CON2BITS"
 5241      50 49 34 43 
 5241      4F 4E 32 42 
 5241      49 54 53 00 
 5242 0e76 02          	.byte 0x2
 5243 0e77 03          	.byte 0x3
 5244 0e78 73 09       	.2byte 0x973
 5245 0e7a D9 0E 00 00 	.4byte 0xed9
 5246 0e7e 05          	.uleb128 0x5
 5247 0e7f 00 00 00 00 	.4byte .LASF24
 5248 0e83 03          	.byte 0x3
 5249 0e84 74 09       	.2byte 0x974
 5250 0e86 F7 00 00 00 	.4byte 0xf7
 5251 0e8a 02          	.byte 0x2
 5252 0e8b 01          	.byte 0x1
 5253 0e8c 0F          	.byte 0xf
 5254 0e8d 02          	.byte 0x2
 5255 0e8e 23          	.byte 0x23
 5256 0e8f 00          	.uleb128 0x0
 5257 0e90 05          	.uleb128 0x5
 5258 0e91 00 00 00 00 	.4byte .LASF25
 5259 0e95 03          	.byte 0x3
MPLAB XC16 ASSEMBLY Listing:   			page 111


 5260 0e96 75 09       	.2byte 0x975
 5261 0e98 F7 00 00 00 	.4byte 0xf7
 5262 0e9c 02          	.byte 0x2
 5263 0e9d 01          	.byte 0x1
 5264 0e9e 0E          	.byte 0xe
 5265 0e9f 02          	.byte 0x2
 5266 0ea0 23          	.byte 0x23
 5267 0ea1 00          	.uleb128 0x0
 5268 0ea2 05          	.uleb128 0x5
 5269 0ea3 00 00 00 00 	.4byte .LASF26
 5270 0ea7 03          	.byte 0x3
 5271 0ea8 77 09       	.2byte 0x977
 5272 0eaa F7 00 00 00 	.4byte 0xf7
 5273 0eae 02          	.byte 0x2
 5274 0eaf 01          	.byte 0x1
 5275 0eb0 02          	.byte 0x2
 5276 0eb1 02          	.byte 0x2
 5277 0eb2 23          	.byte 0x23
 5278 0eb3 00          	.uleb128 0x0
 5279 0eb4 05          	.uleb128 0x5
 5280 0eb5 00 00 00 00 	.4byte .LASF27
 5281 0eb9 03          	.byte 0x3
 5282 0eba 78 09       	.2byte 0x978
 5283 0ebc F7 00 00 00 	.4byte 0xf7
 5284 0ec0 02          	.byte 0x2
 5285 0ec1 01          	.byte 0x1
 5286 0ec2 01          	.byte 0x1
 5287 0ec3 02          	.byte 0x2
 5288 0ec4 23          	.byte 0x23
 5289 0ec5 00          	.uleb128 0x0
 5290 0ec6 05          	.uleb128 0x5
 5291 0ec7 00 00 00 00 	.4byte .LASF28
 5292 0ecb 03          	.byte 0x3
 5293 0ecc 79 09       	.2byte 0x979
 5294 0ece F7 00 00 00 	.4byte 0xf7
 5295 0ed2 02          	.byte 0x2
 5296 0ed3 01          	.byte 0x1
 5297 0ed4 10          	.byte 0x10
 5298 0ed5 02          	.byte 0x2
 5299 0ed6 23          	.byte 0x23
 5300 0ed7 00          	.uleb128 0x0
 5301 0ed8 00          	.byte 0x0
 5302 0ed9 0A          	.uleb128 0xa
 5303 0eda 53 50 49 34 	.asciz "SPI4CON2BITS"
 5303      43 4F 4E 32 
 5303      42 49 54 53 
 5303      00 
 5304 0ee7 03          	.byte 0x3
 5305 0ee8 7A 09       	.2byte 0x97a
 5306 0eea 65 0E 00 00 	.4byte 0xe65
 5307 0eee 02          	.uleb128 0x2
 5308 0eef 02          	.byte 0x2
 5309 0ef0 07          	.byte 0x7
 5310 0ef1 73 68 6F 72 	.asciz "short unsigned int"
 5310      74 20 75 6E 
 5310      73 69 67 6E 
 5310      65 64 20 69 
MPLAB XC16 ASSEMBLY Listing:   			page 112


 5310      6E 74 00 
 5311 0f04 0C          	.uleb128 0xc
 5312 0f05 02          	.byte 0x2
 5313 0f06 04          	.byte 0x4
 5314 0f07 1A          	.byte 0x1a
 5315 0f08 40 0F 00 00 	.4byte 0xf40
 5316 0f0c 0D          	.uleb128 0xd
 5317 0f0d 53 50 49 5F 	.asciz "SPI_TRANSFER_MODE_8BIT"
 5317      54 52 41 4E 
 5317      53 46 45 52 
 5317      5F 4D 4F 44 
 5317      45 5F 38 42 
 5317      49 54 00 
 5318 0f24 00          	.sleb128 0
 5319 0f25 0D          	.uleb128 0xd
 5320 0f26 53 50 49 5F 	.asciz "SPI_TRANSFER_MODE_16BIT"
 5320      54 52 41 4E 
 5320      53 46 45 52 
 5320      5F 4D 4F 44 
 5320      45 5F 31 36 
 5320      42 49 54 00 
 5321 0f3e 01          	.sleb128 1
 5322 0f3f 00          	.byte 0x0
 5323 0f40 03          	.uleb128 0x3
 5324 0f41 53 50 49 5F 	.asciz "SPI_TRANSFER_MODE"
 5324      54 52 41 4E 
 5324      53 46 45 52 
 5324      5F 4D 4F 44 
 5324      45 00 
 5325 0f53 04          	.byte 0x4
 5326 0f54 1D          	.byte 0x1d
 5327 0f55 04 0F 00 00 	.4byte 0xf04
 5328 0f59 0C          	.uleb128 0xc
 5329 0f5a 02          	.byte 0x2
 5330 0f5b 04          	.byte 0x4
 5331 0f5c 1F          	.byte 0x1f
 5332 0f5d 9D 0F 00 00 	.4byte 0xf9d
 5333 0f61 0D          	.uleb128 0xd
 5334 0f62 53 50 49 5F 	.asciz "SPI_DATA_SAMPLE_PHASE_MIDDLE"
 5334      44 41 54 41 
 5334      5F 53 41 4D 
 5334      50 4C 45 5F 
 5334      50 48 41 53 
 5334      45 5F 4D 49 
 5334      44 44 4C 45 
 5334      00 
 5335 0f7f 00          	.sleb128 0
 5336 0f80 0D          	.uleb128 0xd
 5337 0f81 53 50 49 5F 	.asciz "SPI_DATA_SAMPLE_PHASE_END"
 5337      44 41 54 41 
 5337      5F 53 41 4D 
 5337      50 4C 45 5F 
 5337      50 48 41 53 
 5337      45 5F 45 4E 
 5337      44 00 
 5338 0f9b 01          	.sleb128 1
 5339 0f9c 00          	.byte 0x0
MPLAB XC16 ASSEMBLY Listing:   			page 113


 5340 0f9d 03          	.uleb128 0x3
 5341 0f9e 53 50 49 5F 	.asciz "SPI_DATA_SAMPLE_PHASE"
 5341      44 41 54 41 
 5341      5F 53 41 4D 
 5341      50 4C 45 5F 
 5341      50 48 41 53 
 5341      45 00 
 5342 0fb4 04          	.byte 0x4
 5343 0fb5 22          	.byte 0x22
 5344 0fb6 59 0F 00 00 	.4byte 0xf59
 5345 0fba 0C          	.uleb128 0xc
 5346 0fbb 02          	.byte 0x2
 5347 0fbc 04          	.byte 0x4
 5348 0fbd 24          	.byte 0x24
 5349 0fbe F7 0F 00 00 	.4byte 0xff7
 5350 0fc2 0D          	.uleb128 0xd
 5351 0fc3 53 50 49 5F 	.asciz "SPI_MODE_0"
 5351      4D 4F 44 45 
 5351      5F 30 00 
 5352 0fce 00          	.sleb128 0
 5353 0fcf 0D          	.uleb128 0xd
 5354 0fd0 53 50 49 5F 	.asciz "SPI_MODE_1"
 5354      4D 4F 44 45 
 5354      5F 31 00 
 5355 0fdb 01          	.sleb128 1
 5356 0fdc 0D          	.uleb128 0xd
 5357 0fdd 53 50 49 5F 	.asciz "SPI_MODE_2"
 5357      4D 4F 44 45 
 5357      5F 32 00 
 5358 0fe8 02          	.sleb128 2
 5359 0fe9 0D          	.uleb128 0xd
 5360 0fea 53 50 49 5F 	.asciz "SPI_MODE_3"
 5360      4D 4F 44 45 
 5360      5F 33 00 
 5361 0ff5 03          	.sleb128 3
 5362 0ff6 00          	.byte 0x0
 5363 0ff7 03          	.uleb128 0x3
 5364 0ff8 53 50 49 5F 	.asciz "SPI_MODE"
 5364      4D 4F 44 45 
 5364      00 
 5365 1001 04          	.byte 0x4
 5366 1002 29          	.byte 0x29
 5367 1003 BA 0F 00 00 	.4byte 0xfba
 5368 1007 0E          	.uleb128 0xe
 5369 1008 53 50 49 5F 	.asciz "SPI_PERIPHERAL_CONFIGURATION"
 5369      50 45 52 49 
 5369      50 48 45 52 
 5369      41 4C 5F 43 
 5369      4F 4E 46 49 
 5369      47 55 52 41 
 5369      54 49 4F 4E 
 5369      00 
 5370 1025 06          	.byte 0x6
 5371 1026 04          	.byte 0x4
 5372 1027 2E          	.byte 0x2e
 5373 1028 2F 11 00 00 	.4byte 0x112f
 5374 102c 0F          	.uleb128 0xf
MPLAB XC16 ASSEMBLY Listing:   			page 114


 5375 102d 65 6E 61 62 	.asciz "enabled"
 5375      6C 65 64 00 
 5376 1035 04          	.byte 0x4
 5377 1036 2F          	.byte 0x2f
 5378 1037 2F 11 00 00 	.4byte 0x112f
 5379 103b 01          	.byte 0x1
 5380 103c 01          	.byte 0x1
 5381 103d 07          	.byte 0x7
 5382 103e 02          	.byte 0x2
 5383 103f 23          	.byte 0x23
 5384 1040 00          	.uleb128 0x0
 5385 1041 0F          	.uleb128 0xf
 5386 1042 73 74 6F 70 	.asciz "stop_in_idle"
 5386      5F 69 6E 5F 
 5386      69 64 6C 65 
 5386      00 
 5387 104f 04          	.byte 0x4
 5388 1050 30          	.byte 0x30
 5389 1051 2F 11 00 00 	.4byte 0x112f
 5390 1055 01          	.byte 0x1
 5391 1056 01          	.byte 0x1
 5392 1057 06          	.byte 0x6
 5393 1058 02          	.byte 0x2
 5394 1059 23          	.byte 0x23
 5395 105a 00          	.uleb128 0x0
 5396 105b 0F          	.uleb128 0xf
 5397 105c 64 69 73 61 	.asciz "disable_SCK_pin"
 5397      62 6C 65 5F 
 5397      53 43 4B 5F 
 5397      70 69 6E 00 
 5398 106c 04          	.byte 0x4
 5399 106d 31          	.byte 0x31
 5400 106e 2F 11 00 00 	.4byte 0x112f
 5401 1072 01          	.byte 0x1
 5402 1073 01          	.byte 0x1
 5403 1074 05          	.byte 0x5
 5404 1075 02          	.byte 0x2
 5405 1076 23          	.byte 0x23
 5406 1077 00          	.uleb128 0x0
 5407 1078 0F          	.uleb128 0xf
 5408 1079 64 69 73 61 	.asciz "disable_SDO_pin"
 5408      62 6C 65 5F 
 5408      53 44 4F 5F 
 5408      70 69 6E 00 
 5409 1089 04          	.byte 0x4
 5410 108a 32          	.byte 0x32
 5411 108b 2F 11 00 00 	.4byte 0x112f
 5412 108f 01          	.byte 0x1
 5413 1090 01          	.byte 0x1
 5414 1091 04          	.byte 0x4
 5415 1092 02          	.byte 0x2
 5416 1093 23          	.byte 0x23
 5417 1094 00          	.uleb128 0x0
 5418 1095 0F          	.uleb128 0xf
 5419 1096 74 72 61 6E 	.asciz "transfer_mode"
 5419      73 66 65 72 
 5419      5F 6D 6F 64 
MPLAB XC16 ASSEMBLY Listing:   			page 115


 5419      65 00 
 5420 10a4 04          	.byte 0x4
 5421 10a5 33          	.byte 0x33
 5422 10a6 40 0F 00 00 	.4byte 0xf40
 5423 10aa 02          	.byte 0x2
 5424 10ab 01          	.byte 0x1
 5425 10ac 0B          	.byte 0xb
 5426 10ad 02          	.byte 0x2
 5427 10ae 23          	.byte 0x23
 5428 10af 00          	.uleb128 0x0
 5429 10b0 0F          	.uleb128 0xf
 5430 10b1 64 61 74 61 	.asciz "data_sample_phase"
 5430      5F 73 61 6D 
 5430      70 6C 65 5F 
 5430      70 68 61 73 
 5430      65 00 
 5431 10c3 04          	.byte 0x4
 5432 10c4 34          	.byte 0x34
 5433 10c5 9D 0F 00 00 	.4byte 0xf9d
 5434 10c9 02          	.byte 0x2
 5435 10ca 01          	.byte 0x1
 5436 10cb 0A          	.byte 0xa
 5437 10cc 02          	.byte 0x2
 5438 10cd 23          	.byte 0x23
 5439 10ce 00          	.uleb128 0x0
 5440 10cf 0F          	.uleb128 0xf
 5441 10d0 53 50 49 5F 	.asciz "SPI_mode"
 5441      6D 6F 64 65 
 5441      00 
 5442 10d9 04          	.byte 0x4
 5443 10da 35          	.byte 0x35
 5444 10db F7 0F 00 00 	.4byte 0xff7
 5445 10df 02          	.byte 0x2
 5446 10e0 02          	.byte 0x2
 5447 10e1 08          	.byte 0x8
 5448 10e2 02          	.byte 0x2
 5449 10e3 23          	.byte 0x23
 5450 10e4 00          	.uleb128 0x0
 5451 10e5 0F          	.uleb128 0xf
 5452 10e6 65 6E 61 62 	.asciz "enable_SS_pin"
 5452      6C 65 5F 53 
 5452      53 5F 70 69 
 5452      6E 00 
 5453 10f4 04          	.byte 0x4
 5454 10f5 36          	.byte 0x36
 5455 10f6 D7 00 00 00 	.4byte 0xd7
 5456 10fa 01          	.byte 0x1
 5457 10fb 01          	.byte 0x1
 5458 10fc 07          	.byte 0x7
 5459 10fd 02          	.byte 0x2
 5460 10fe 23          	.byte 0x23
 5461 10ff 01          	.uleb128 0x1
 5462 1100 0F          	.uleb128 0xf
 5463 1101 65 6E 61 62 	.asciz "enable_master_mode"
 5463      6C 65 5F 6D 
 5463      61 73 74 65 
 5463      72 5F 6D 6F 
MPLAB XC16 ASSEMBLY Listing:   			page 116


 5463      64 65 00 
 5464 1114 04          	.byte 0x4
 5465 1115 37          	.byte 0x37
 5466 1116 2F 11 00 00 	.4byte 0x112f
 5467 111a 01          	.byte 0x1
 5468 111b 01          	.byte 0x1
 5469 111c 06          	.byte 0x6
 5470 111d 02          	.byte 0x2
 5471 111e 23          	.byte 0x23
 5472 111f 01          	.uleb128 0x1
 5473 1120 10          	.uleb128 0x10
 5474 1121 00 00 00 00 	.4byte .LASF29
 5475 1125 04          	.byte 0x4
 5476 1126 38          	.byte 0x38
 5477 1127 17 01 00 00 	.4byte 0x117
 5478 112b 02          	.byte 0x2
 5479 112c 23          	.byte 0x23
 5480 112d 02          	.uleb128 0x2
 5481 112e 00          	.byte 0x0
 5482 112f 02          	.uleb128 0x2
 5483 1130 01          	.byte 0x1
 5484 1131 02          	.byte 0x2
 5485 1132 5F 42 6F 6F 	.asciz "_Bool"
 5485      6C 00 
 5486 1138 03          	.uleb128 0x3
 5487 1139 53 50 49 5F 	.asciz "SPI_Peripheral_Configuration"
 5487      50 65 72 69 
 5487      70 68 65 72 
 5487      61 6C 5F 43 
 5487      6F 6E 66 69 
 5487      67 75 72 61 
 5487      74 69 6F 6E 
 5487      00 
 5488 1156 04          	.byte 0x4
 5489 1157 39          	.byte 0x39
 5490 1158 07 10 00 00 	.4byte 0x1007
 5491 115c 11          	.uleb128 0x11
 5492 115d 01          	.byte 0x1
 5493 115e 53 50 49 31 	.asciz "SPI1_TransferModeGet"
 5493      5F 54 72 61 
 5493      6E 73 66 65 
 5493      72 4D 6F 64 
 5493      65 47 65 74 
 5493      00 
 5494 1173 01          	.byte 0x1
 5495 1174 40 03       	.2byte 0x340
 5496 1176 01          	.byte 0x1
 5497 1177 40 0F 00 00 	.4byte 0xf40
 5498 117b 03          	.byte 0x3
 5499 117c 11          	.uleb128 0x11
 5500 117d 01          	.byte 0x1
 5501 117e 53 50 49 32 	.asciz "SPI2_TransferModeGet"
 5501      5F 54 72 61 
 5501      6E 73 66 65 
 5501      72 4D 6F 64 
 5501      65 47 65 74 
 5501      00 
MPLAB XC16 ASSEMBLY Listing:   			page 117


 5502 1193 01          	.byte 0x1
 5503 1194 4C 03       	.2byte 0x34c
 5504 1196 01          	.byte 0x1
 5505 1197 40 0F 00 00 	.4byte 0xf40
 5506 119b 03          	.byte 0x3
 5507 119c 11          	.uleb128 0x11
 5508 119d 01          	.byte 0x1
 5509 119e 53 50 49 33 	.asciz "SPI3_TransferModeGet"
 5509      5F 54 72 61 
 5509      6E 73 66 65 
 5509      72 4D 6F 64 
 5509      65 47 65 74 
 5509      00 
 5510 11b3 01          	.byte 0x1
 5511 11b4 58 03       	.2byte 0x358
 5512 11b6 01          	.byte 0x1
 5513 11b7 40 0F 00 00 	.4byte 0xf40
 5514 11bb 03          	.byte 0x3
 5515 11bc 11          	.uleb128 0x11
 5516 11bd 01          	.byte 0x1
 5517 11be 53 50 49 34 	.asciz "SPI4_TransferModeGet"
 5517      5F 54 72 61 
 5517      6E 73 66 65 
 5517      72 4D 6F 64 
 5517      65 47 65 74 
 5517      00 
 5518 11d3 01          	.byte 0x1
 5519 11d4 64 03       	.2byte 0x364
 5520 11d6 01          	.byte 0x1
 5521 11d7 40 0F 00 00 	.4byte 0xf40
 5522 11db 03          	.byte 0x3
 5523 11dc 12          	.uleb128 0x12
 5524 11dd 01          	.byte 0x1
 5525 11de 67 65 74 5F 	.asciz "get_prescales"
 5525      70 72 65 73 
 5525      63 61 6C 65 
 5525      73 00 
 5526 11ec 01          	.byte 0x1
 5527 11ed 14          	.byte 0x14
 5528 11ee 01          	.byte 0x1
 5529 11ef 17 01 00 00 	.4byte 0x117
 5530 11f3 00 00 00 00 	.4byte .LFB0
 5531 11f7 00 00 00 00 	.4byte .LFE0
 5532 11fb 01          	.byte 0x1
 5533 11fc 5E          	.byte 0x5e
 5534 11fd B8 12 00 00 	.4byte 0x12b8
 5535 1201 13          	.uleb128 0x13
 5536 1202 00 00 00 00 	.4byte .LASF29
 5537 1206 01          	.byte 0x1
 5538 1207 14          	.byte 0x14
 5539 1208 17 01 00 00 	.4byte 0x117
 5540 120c 02          	.byte 0x2
 5541 120d 7E          	.byte 0x7e
 5542 120e 1E          	.sleb128 30
 5543 120f 13          	.uleb128 0x13
 5544 1210 00 00 00 00 	.4byte .LASF30
 5545 1214 01          	.byte 0x1
MPLAB XC16 ASSEMBLY Listing:   			page 118


 5546 1215 14          	.byte 0x14
 5547 1216 B8 12 00 00 	.4byte 0x12b8
 5548 121a 02          	.byte 0x2
 5549 121b 7E          	.byte 0x7e
 5550 121c 22          	.sleb128 34
 5551 121d 13          	.uleb128 0x13
 5552 121e 00 00 00 00 	.4byte .LASF31
 5553 1222 01          	.byte 0x1
 5554 1223 14          	.byte 0x14
 5555 1224 B8 12 00 00 	.4byte 0x12b8
 5556 1228 02          	.byte 0x2
 5557 1229 7E          	.byte 0x7e
 5558 122a 24          	.sleb128 36
 5559 122b 14          	.uleb128 0x14
 5560 122c 69 00       	.asciz "i"
 5561 122e 01          	.byte 0x1
 5562 122f 16          	.byte 0x16
 5563 1230 B3 00 00 00 	.4byte 0xb3
 5564 1234 02          	.byte 0x2
 5565 1235 7E          	.byte 0x7e
 5566 1236 00          	.sleb128 0
 5567 1237 14          	.uleb128 0x14
 5568 1238 6A 00       	.asciz "j"
 5569 123a 01          	.byte 0x1
 5570 123b 17          	.byte 0x17
 5571 123c B3 00 00 00 	.4byte 0xb3
 5572 1240 02          	.byte 0x2
 5573 1241 7E          	.byte 0x7e
 5574 1242 02          	.sleb128 2
 5575 1243 14          	.uleb128 0x14
 5576 1244 65 72 72 6F 	.asciz "error"
 5576      72 00 
 5577 124a 01          	.byte 0x1
 5578 124b 18          	.byte 0x18
 5579 124c BE 12 00 00 	.4byte 0x12be
 5580 1250 02          	.byte 0x2
 5581 1251 7E          	.byte 0x7e
 5582 1252 08          	.sleb128 8
 5583 1253 14          	.uleb128 0x14
 5584 1254 62 65 73 74 	.asciz "best_prescaler_error"
 5584      5F 70 72 65 
 5584      73 63 61 6C 
 5584      65 72 5F 65 
 5584      72 72 6F 72 
 5584      00 
 5585 1269 01          	.byte 0x1
 5586 126a 19          	.byte 0x19
 5587 126b BE 12 00 00 	.4byte 0x12be
 5588 126f 02          	.byte 0x2
 5589 1270 7E          	.byte 0x7e
 5590 1271 04          	.sleb128 4
 5591 1272 14          	.uleb128 0x14
 5592 1273 70 72 69 6D 	.asciz "prim"
 5592      00 
 5593 1278 01          	.byte 0x1
 5594 1279 1A          	.byte 0x1a
 5595 127a D7 12 00 00 	.4byte 0x12d7
MPLAB XC16 ASSEMBLY Listing:   			page 119


 5596 127e 02          	.byte 0x2
 5597 127f 7E          	.byte 0x7e
 5598 1280 10          	.sleb128 16
 5599 1281 14          	.uleb128 0x14
 5600 1282 73 65 63 00 	.asciz "sec"
 5601 1286 01          	.byte 0x1
 5602 1287 1B          	.byte 0x1b
 5603 1288 EC 12 00 00 	.4byte 0x12ec
 5604 128c 02          	.byte 0x2
 5605 128d 7E          	.byte 0x7e
 5606 128e 14          	.sleb128 20
 5607 128f 14          	.uleb128 0x14
 5608 1290 66 73 63 6B 	.asciz "fsck"
 5608      00 
 5609 1295 01          	.byte 0x1
 5610 1296 1C          	.byte 0x1c
 5611 1297 17 01 00 00 	.4byte 0x117
 5612 129b 02          	.byte 0x2
 5613 129c 7E          	.byte 0x7e
 5614 129d 0C          	.sleb128 12
 5615 129e 14          	.uleb128 0x14
 5616 129f 62 65 73 74 	.asciz "best_prescaler"
 5616      5F 70 72 65 
 5616      73 63 61 6C 
 5616      65 72 00 
 5617 12ae 01          	.byte 0x1
 5618 12af 1D          	.byte 0x1d
 5619 12b0 F1 12 00 00 	.4byte 0x12f1
 5620 12b4 02          	.byte 0x2
 5621 12b5 7E          	.byte 0x7e
 5622 12b6 1C          	.sleb128 28
 5623 12b7 00          	.byte 0x0
 5624 12b8 15          	.uleb128 0x15
 5625 12b9 02          	.byte 0x2
 5626 12ba D7 00 00 00 	.4byte 0xd7
 5627 12be 02          	.uleb128 0x2
 5628 12bf 04          	.byte 0x4
 5629 12c0 04          	.byte 0x4
 5630 12c1 66 6C 6F 61 	.asciz "float"
 5630      74 00 
 5631 12c7 16          	.uleb128 0x16
 5632 12c8 D7 00 00 00 	.4byte 0xd7
 5633 12cc D7 12 00 00 	.4byte 0x12d7
 5634 12d0 17          	.uleb128 0x17
 5635 12d1 07 01 00 00 	.4byte 0x107
 5636 12d5 03          	.byte 0x3
 5637 12d6 00          	.byte 0x0
 5638 12d7 18          	.uleb128 0x18
 5639 12d8 C7 12 00 00 	.4byte 0x12c7
 5640 12dc 16          	.uleb128 0x16
 5641 12dd D7 00 00 00 	.4byte 0xd7
 5642 12e1 EC 12 00 00 	.4byte 0x12ec
 5643 12e5 17          	.uleb128 0x17
 5644 12e6 07 01 00 00 	.4byte 0x107
 5645 12ea 07          	.byte 0x7
 5646 12eb 00          	.byte 0x0
 5647 12ec 18          	.uleb128 0x18
MPLAB XC16 ASSEMBLY Listing:   			page 120


 5648 12ed DC 12 00 00 	.4byte 0x12dc
 5649 12f1 16          	.uleb128 0x16
 5650 12f2 D7 00 00 00 	.4byte 0xd7
 5651 12f6 01 13 00 00 	.4byte 0x1301
 5652 12fa 17          	.uleb128 0x17
 5653 12fb 07 01 00 00 	.4byte 0x107
 5654 12ff 01          	.byte 0x1
 5655 1300 00          	.byte 0x0
 5656 1301 12          	.uleb128 0x12
 5657 1302 01          	.byte 0x1
 5658 1303 63 6F 6E 66 	.asciz "config_SPI1"
 5658      69 67 5F 53 
 5658      50 49 31 00 
 5659 130f 01          	.byte 0x1
 5660 1310 3A          	.byte 0x3a
 5661 1311 01          	.byte 0x1
 5662 1312 17 01 00 00 	.4byte 0x117
 5663 1316 00 00 00 00 	.4byte .LFB1
 5664 131a 00 00 00 00 	.4byte .LFE1
 5665 131e 01          	.byte 0x1
 5666 131f 5E          	.byte 0x5e
 5667 1320 5D 13 00 00 	.4byte 0x135d
 5668 1324 13          	.uleb128 0x13
 5669 1325 00 00 00 00 	.4byte .LASF32
 5670 1329 01          	.byte 0x1
 5671 132a 3A          	.byte 0x3a
 5672 132b 38 11 00 00 	.4byte 0x1138
 5673 132f 02          	.byte 0x2
 5674 1330 7E          	.byte 0x7e
 5675 1331 06          	.sleb128 6
 5676 1332 19          	.uleb128 0x19
 5677 1333 00 00 00 00 	.4byte .LASF29
 5678 1337 01          	.byte 0x1
 5679 1338 3C          	.byte 0x3c
 5680 1339 17 01 00 00 	.4byte 0x117
 5681 133d 02          	.byte 0x2
 5682 133e 7E          	.byte 0x7e
 5683 133f 00          	.sleb128 0
 5684 1340 19          	.uleb128 0x19
 5685 1341 00 00 00 00 	.4byte .LASF30
 5686 1345 01          	.byte 0x1
 5687 1346 3D          	.byte 0x3d
 5688 1347 D7 00 00 00 	.4byte 0xd7
 5689 134b 02          	.byte 0x2
 5690 134c 7E          	.byte 0x7e
 5691 134d 04          	.sleb128 4
 5692 134e 19          	.uleb128 0x19
 5693 134f 00 00 00 00 	.4byte .LASF31
 5694 1353 01          	.byte 0x1
 5695 1354 3E          	.byte 0x3e
 5696 1355 D7 00 00 00 	.4byte 0xd7
 5697 1359 02          	.byte 0x2
 5698 135a 7E          	.byte 0x7e
 5699 135b 05          	.sleb128 5
 5700 135c 00          	.byte 0x0
 5701 135d 12          	.uleb128 0x12
 5702 135e 01          	.byte 0x1
MPLAB XC16 ASSEMBLY Listing:   			page 121


 5703 135f 63 6F 6E 66 	.asciz "config_SPI2"
 5703      69 67 5F 53 
 5703      50 49 32 00 
 5704 136b 01          	.byte 0x1
 5705 136c 66          	.byte 0x66
 5706 136d 01          	.byte 0x1
 5707 136e 17 01 00 00 	.4byte 0x117
 5708 1372 00 00 00 00 	.4byte .LFB2
 5709 1376 00 00 00 00 	.4byte .LFE2
 5710 137a 01          	.byte 0x1
 5711 137b 5E          	.byte 0x5e
 5712 137c B9 13 00 00 	.4byte 0x13b9
 5713 1380 13          	.uleb128 0x13
 5714 1381 00 00 00 00 	.4byte .LASF32
 5715 1385 01          	.byte 0x1
 5716 1386 66          	.byte 0x66
 5717 1387 38 11 00 00 	.4byte 0x1138
 5718 138b 02          	.byte 0x2
 5719 138c 7E          	.byte 0x7e
 5720 138d 06          	.sleb128 6
 5721 138e 19          	.uleb128 0x19
 5722 138f 00 00 00 00 	.4byte .LASF29
 5723 1393 01          	.byte 0x1
 5724 1394 68          	.byte 0x68
 5725 1395 17 01 00 00 	.4byte 0x117
 5726 1399 02          	.byte 0x2
 5727 139a 7E          	.byte 0x7e
 5728 139b 00          	.sleb128 0
 5729 139c 19          	.uleb128 0x19
 5730 139d 00 00 00 00 	.4byte .LASF30
 5731 13a1 01          	.byte 0x1
 5732 13a2 69          	.byte 0x69
 5733 13a3 D7 00 00 00 	.4byte 0xd7
 5734 13a7 02          	.byte 0x2
 5735 13a8 7E          	.byte 0x7e
 5736 13a9 04          	.sleb128 4
 5737 13aa 19          	.uleb128 0x19
 5738 13ab 00 00 00 00 	.4byte .LASF31
 5739 13af 01          	.byte 0x1
 5740 13b0 6A          	.byte 0x6a
 5741 13b1 D7 00 00 00 	.4byte 0xd7
 5742 13b5 02          	.byte 0x2
 5743 13b6 7E          	.byte 0x7e
 5744 13b7 05          	.sleb128 5
 5745 13b8 00          	.byte 0x0
 5746 13b9 12          	.uleb128 0x12
 5747 13ba 01          	.byte 0x1
 5748 13bb 63 6F 6E 66 	.asciz "config_SPI3"
 5748      69 67 5F 53 
 5748      50 49 33 00 
 5749 13c7 01          	.byte 0x1
 5750 13c8 92          	.byte 0x92
 5751 13c9 01          	.byte 0x1
 5752 13ca 17 01 00 00 	.4byte 0x117
 5753 13ce 00 00 00 00 	.4byte .LFB3
 5754 13d2 00 00 00 00 	.4byte .LFE3
 5755 13d6 01          	.byte 0x1
MPLAB XC16 ASSEMBLY Listing:   			page 122


 5756 13d7 5E          	.byte 0x5e
 5757 13d8 15 14 00 00 	.4byte 0x1415
 5758 13dc 13          	.uleb128 0x13
 5759 13dd 00 00 00 00 	.4byte .LASF32
 5760 13e1 01          	.byte 0x1
 5761 13e2 92          	.byte 0x92
 5762 13e3 38 11 00 00 	.4byte 0x1138
 5763 13e7 02          	.byte 0x2
 5764 13e8 7E          	.byte 0x7e
 5765 13e9 06          	.sleb128 6
 5766 13ea 19          	.uleb128 0x19
 5767 13eb 00 00 00 00 	.4byte .LASF29
 5768 13ef 01          	.byte 0x1
 5769 13f0 94          	.byte 0x94
 5770 13f1 17 01 00 00 	.4byte 0x117
 5771 13f5 02          	.byte 0x2
 5772 13f6 7E          	.byte 0x7e
 5773 13f7 00          	.sleb128 0
 5774 13f8 19          	.uleb128 0x19
 5775 13f9 00 00 00 00 	.4byte .LASF30
 5776 13fd 01          	.byte 0x1
 5777 13fe 95          	.byte 0x95
 5778 13ff D7 00 00 00 	.4byte 0xd7
 5779 1403 02          	.byte 0x2
 5780 1404 7E          	.byte 0x7e
 5781 1405 04          	.sleb128 4
 5782 1406 19          	.uleb128 0x19
 5783 1407 00 00 00 00 	.4byte .LASF31
 5784 140b 01          	.byte 0x1
 5785 140c 96          	.byte 0x96
 5786 140d D7 00 00 00 	.4byte 0xd7
 5787 1411 02          	.byte 0x2
 5788 1412 7E          	.byte 0x7e
 5789 1413 05          	.sleb128 5
 5790 1414 00          	.byte 0x0
 5791 1415 12          	.uleb128 0x12
 5792 1416 01          	.byte 0x1
 5793 1417 63 6F 6E 66 	.asciz "config_SPI4"
 5793      69 67 5F 53 
 5793      50 49 34 00 
 5794 1423 01          	.byte 0x1
 5795 1424 BE          	.byte 0xbe
 5796 1425 01          	.byte 0x1
 5797 1426 17 01 00 00 	.4byte 0x117
 5798 142a 00 00 00 00 	.4byte .LFB4
 5799 142e 00 00 00 00 	.4byte .LFE4
 5800 1432 01          	.byte 0x1
 5801 1433 5E          	.byte 0x5e
 5802 1434 71 14 00 00 	.4byte 0x1471
 5803 1438 13          	.uleb128 0x13
 5804 1439 00 00 00 00 	.4byte .LASF32
 5805 143d 01          	.byte 0x1
 5806 143e BE          	.byte 0xbe
 5807 143f 38 11 00 00 	.4byte 0x1138
 5808 1443 02          	.byte 0x2
 5809 1444 7E          	.byte 0x7e
 5810 1445 06          	.sleb128 6
MPLAB XC16 ASSEMBLY Listing:   			page 123


 5811 1446 19          	.uleb128 0x19
 5812 1447 00 00 00 00 	.4byte .LASF29
 5813 144b 01          	.byte 0x1
 5814 144c C0          	.byte 0xc0
 5815 144d 17 01 00 00 	.4byte 0x117
 5816 1451 02          	.byte 0x2
 5817 1452 7E          	.byte 0x7e
 5818 1453 00          	.sleb128 0
 5819 1454 19          	.uleb128 0x19
 5820 1455 00 00 00 00 	.4byte .LASF30
 5821 1459 01          	.byte 0x1
 5822 145a C1          	.byte 0xc1
 5823 145b D7 00 00 00 	.4byte 0xd7
 5824 145f 02          	.byte 0x2
 5825 1460 7E          	.byte 0x7e
 5826 1461 04          	.sleb128 4
 5827 1462 19          	.uleb128 0x19
 5828 1463 00 00 00 00 	.4byte .LASF31
 5829 1467 01          	.byte 0x1
 5830 1468 C2          	.byte 0xc2
 5831 1469 D7 00 00 00 	.4byte 0xd7
 5832 146d 02          	.byte 0x2
 5833 146e 7E          	.byte 0x7e
 5834 146f 05          	.sleb128 5
 5835 1470 00          	.byte 0x0
 5836 1471 1A          	.uleb128 0x1a
 5837 1472 01          	.byte 0x1
 5838 1473 53 50 49 31 	.asciz "SPI1_enable_module"
 5838      5F 65 6E 61 
 5838      62 6C 65 5F 
 5838      6D 6F 64 75 
 5838      6C 65 00 
 5839 1486 01          	.byte 0x1
 5840 1487 EA          	.byte 0xea
 5841 1488 00 00 00 00 	.4byte .LFB5
 5842 148c 00 00 00 00 	.4byte .LFE5
 5843 1490 01          	.byte 0x1
 5844 1491 5E          	.byte 0x5e
 5845 1492 1A          	.uleb128 0x1a
 5846 1493 01          	.byte 0x1
 5847 1494 53 50 49 32 	.asciz "SPI2_enable_module"
 5847      5F 65 6E 61 
 5847      62 6C 65 5F 
 5847      6D 6F 64 75 
 5847      6C 65 00 
 5848 14a7 01          	.byte 0x1
 5849 14a8 F5          	.byte 0xf5
 5850 14a9 00 00 00 00 	.4byte .LFB6
 5851 14ad 00 00 00 00 	.4byte .LFE6
 5852 14b1 01          	.byte 0x1
 5853 14b2 5E          	.byte 0x5e
 5854 14b3 1B          	.uleb128 0x1b
 5855 14b4 01          	.byte 0x1
 5856 14b5 53 50 49 33 	.asciz "SPI3_enable_module"
 5856      5F 65 6E 61 
 5856      62 6C 65 5F 
 5856      6D 6F 64 75 
MPLAB XC16 ASSEMBLY Listing:   			page 124


 5856      6C 65 00 
 5857 14c8 01          	.byte 0x1
 5858 14c9 00 01       	.2byte 0x100
 5859 14cb 00 00 00 00 	.4byte .LFB7
 5860 14cf 00 00 00 00 	.4byte .LFE7
 5861 14d3 01          	.byte 0x1
 5862 14d4 5E          	.byte 0x5e
 5863 14d5 1B          	.uleb128 0x1b
 5864 14d6 01          	.byte 0x1
 5865 14d7 53 50 49 34 	.asciz "SPI4_enable_module"
 5865      5F 65 6E 61 
 5865      62 6C 65 5F 
 5865      6D 6F 64 75 
 5865      6C 65 00 
 5866 14ea 01          	.byte 0x1
 5867 14eb 0B 01       	.2byte 0x10b
 5868 14ed 00 00 00 00 	.4byte .LFB8
 5869 14f1 00 00 00 00 	.4byte .LFE8
 5870 14f5 01          	.byte 0x1
 5871 14f6 5E          	.byte 0x5e
 5872 14f7 1C          	.uleb128 0x1c
 5873 14f8 01          	.byte 0x1
 5874 14f9 53 50 49 31 	.asciz "SPI1_Exchange"
 5874      5F 45 78 63 
 5874      68 61 6E 67 
 5874      65 00 
 5875 1507 01          	.byte 0x1
 5876 1508 18 01       	.2byte 0x118
 5877 150a 01          	.byte 0x1
 5878 150b 00 00 00 00 	.4byte .LFB9
 5879 150f 00 00 00 00 	.4byte .LFE9
 5880 1513 01          	.byte 0x1
 5881 1514 5E          	.byte 0x5e
 5882 1515 52 15 00 00 	.4byte 0x1552
 5883 1519 1D          	.uleb128 0x1d
 5884 151a 00 00 00 00 	.4byte .LASF33
 5885 151e 01          	.byte 0x1
 5886 151f 18 01       	.2byte 0x118
 5887 1521 B8 12 00 00 	.4byte 0x12b8
 5888 1525 02          	.byte 0x2
 5889 1526 7E          	.byte 0x7e
 5890 1527 00          	.sleb128 0
 5891 1528 1D          	.uleb128 0x1d
 5892 1529 00 00 00 00 	.4byte .LASF34
 5893 152d 01          	.byte 0x1
 5894 152e 18 01       	.2byte 0x118
 5895 1530 B8 12 00 00 	.4byte 0x12b8
 5896 1534 02          	.byte 0x2
 5897 1535 7E          	.byte 0x7e
 5898 1536 02          	.sleb128 2
 5899 1537 1E          	.uleb128 0x1e
 5900 1538 5C 11 00 00 	.4byte 0x115c
 5901 153c 00 00 00 00 	.4byte .LBB26
 5902 1540 00 00 00 00 	.4byte .LBE26
 5903 1544 1E          	.uleb128 0x1e
 5904 1545 5C 11 00 00 	.4byte 0x115c
 5905 1549 00 00 00 00 	.4byte .LBB28
MPLAB XC16 ASSEMBLY Listing:   			page 125


 5906 154d 00 00 00 00 	.4byte .LBE28
 5907 1551 00          	.byte 0x0
 5908 1552 1C          	.uleb128 0x1c
 5909 1553 01          	.byte 0x1
 5910 1554 53 50 49 32 	.asciz "SPI2_Exchange"
 5910      5F 45 78 63 
 5910      68 61 6E 67 
 5910      65 00 
 5911 1562 01          	.byte 0x1
 5912 1563 37 01       	.2byte 0x137
 5913 1565 01          	.byte 0x1
 5914 1566 00 00 00 00 	.4byte .LFB10
 5915 156a 00 00 00 00 	.4byte .LFE10
 5916 156e 01          	.byte 0x1
 5917 156f 5E          	.byte 0x5e
 5918 1570 AD 15 00 00 	.4byte 0x15ad
 5919 1574 1D          	.uleb128 0x1d
 5920 1575 00 00 00 00 	.4byte .LASF33
 5921 1579 01          	.byte 0x1
 5922 157a 37 01       	.2byte 0x137
 5923 157c B8 12 00 00 	.4byte 0x12b8
 5924 1580 02          	.byte 0x2
 5925 1581 7E          	.byte 0x7e
 5926 1582 00          	.sleb128 0
 5927 1583 1D          	.uleb128 0x1d
 5928 1584 00 00 00 00 	.4byte .LASF34
 5929 1588 01          	.byte 0x1
 5930 1589 37 01       	.2byte 0x137
 5931 158b B8 12 00 00 	.4byte 0x12b8
 5932 158f 02          	.byte 0x2
 5933 1590 7E          	.byte 0x7e
 5934 1591 02          	.sleb128 2
 5935 1592 1E          	.uleb128 0x1e
 5936 1593 7C 11 00 00 	.4byte 0x117c
 5937 1597 00 00 00 00 	.4byte .LBB30
 5938 159b 00 00 00 00 	.4byte .LBE30
 5939 159f 1E          	.uleb128 0x1e
 5940 15a0 7C 11 00 00 	.4byte 0x117c
 5941 15a4 00 00 00 00 	.4byte .LBB32
 5942 15a8 00 00 00 00 	.4byte .LBE32
 5943 15ac 00          	.byte 0x0
 5944 15ad 1C          	.uleb128 0x1c
 5945 15ae 01          	.byte 0x1
 5946 15af 53 50 49 33 	.asciz "SPI3_Exchange"
 5946      5F 45 78 63 
 5946      68 61 6E 67 
 5946      65 00 
 5947 15bd 01          	.byte 0x1
 5948 15be 57 01       	.2byte 0x157
 5949 15c0 01          	.byte 0x1
 5950 15c1 00 00 00 00 	.4byte .LFB11
 5951 15c5 00 00 00 00 	.4byte .LFE11
 5952 15c9 01          	.byte 0x1
 5953 15ca 5E          	.byte 0x5e
 5954 15cb 08 16 00 00 	.4byte 0x1608
 5955 15cf 1D          	.uleb128 0x1d
 5956 15d0 00 00 00 00 	.4byte .LASF33
MPLAB XC16 ASSEMBLY Listing:   			page 126


 5957 15d4 01          	.byte 0x1
 5958 15d5 57 01       	.2byte 0x157
 5959 15d7 B8 12 00 00 	.4byte 0x12b8
 5960 15db 02          	.byte 0x2
 5961 15dc 7E          	.byte 0x7e
 5962 15dd 00          	.sleb128 0
 5963 15de 1D          	.uleb128 0x1d
 5964 15df 00 00 00 00 	.4byte .LASF34
 5965 15e3 01          	.byte 0x1
 5966 15e4 57 01       	.2byte 0x157
 5967 15e6 B8 12 00 00 	.4byte 0x12b8
 5968 15ea 02          	.byte 0x2
 5969 15eb 7E          	.byte 0x7e
 5970 15ec 02          	.sleb128 2
 5971 15ed 1E          	.uleb128 0x1e
 5972 15ee 9C 11 00 00 	.4byte 0x119c
 5973 15f2 00 00 00 00 	.4byte .LBB34
 5974 15f6 00 00 00 00 	.4byte .LBE34
 5975 15fa 1E          	.uleb128 0x1e
 5976 15fb 9C 11 00 00 	.4byte 0x119c
 5977 15ff 00 00 00 00 	.4byte .LBB36
 5978 1603 00 00 00 00 	.4byte .LBE36
 5979 1607 00          	.byte 0x0
 5980 1608 1C          	.uleb128 0x1c
 5981 1609 01          	.byte 0x1
 5982 160a 53 50 49 34 	.asciz "SPI4_Exchange"
 5982      5F 45 78 63 
 5982      68 61 6E 67 
 5982      65 00 
 5983 1618 01          	.byte 0x1
 5984 1619 77 01       	.2byte 0x177
 5985 161b 01          	.byte 0x1
 5986 161c 00 00 00 00 	.4byte .LFB12
 5987 1620 00 00 00 00 	.4byte .LFE12
 5988 1624 01          	.byte 0x1
 5989 1625 5E          	.byte 0x5e
 5990 1626 63 16 00 00 	.4byte 0x1663
 5991 162a 1D          	.uleb128 0x1d
 5992 162b 00 00 00 00 	.4byte .LASF33
 5993 162f 01          	.byte 0x1
 5994 1630 77 01       	.2byte 0x177
 5995 1632 B8 12 00 00 	.4byte 0x12b8
 5996 1636 02          	.byte 0x2
 5997 1637 7E          	.byte 0x7e
 5998 1638 00          	.sleb128 0
 5999 1639 1D          	.uleb128 0x1d
 6000 163a 00 00 00 00 	.4byte .LASF34
 6001 163e 01          	.byte 0x1
 6002 163f 77 01       	.2byte 0x177
 6003 1641 B8 12 00 00 	.4byte 0x12b8
 6004 1645 02          	.byte 0x2
 6005 1646 7E          	.byte 0x7e
 6006 1647 02          	.sleb128 2
 6007 1648 1E          	.uleb128 0x1e
 6008 1649 BC 11 00 00 	.4byte 0x11bc
 6009 164d 00 00 00 00 	.4byte .LBB38
 6010 1651 00 00 00 00 	.4byte .LBE38
MPLAB XC16 ASSEMBLY Listing:   			page 127


 6011 1655 1E          	.uleb128 0x1e
 6012 1656 BC 11 00 00 	.4byte 0x11bc
 6013 165a 00 00 00 00 	.4byte .LBB40
 6014 165e 00 00 00 00 	.4byte .LBE40
 6015 1662 00          	.byte 0x0
 6016 1663 1F          	.uleb128 0x1f
 6017 1664 01          	.byte 0x1
 6018 1665 53 50 49 31 	.asciz "SPI1_ExchangeBuffer"
 6018      5F 45 78 63 
 6018      68 61 6E 67 
 6018      65 42 75 66 
 6018      66 65 72 00 
 6019 1679 01          	.byte 0x1
 6020 167a 9B 01       	.2byte 0x19b
 6021 167c 01          	.byte 0x1
 6022 167d F7 00 00 00 	.4byte 0xf7
 6023 1681 00 00 00 00 	.4byte .LFB13
 6024 1685 00 00 00 00 	.4byte .LFE13
 6025 1689 01          	.byte 0x1
 6026 168a 5E          	.byte 0x5e
 6027 168b 60 17 00 00 	.4byte 0x1760
 6028 168f 1D          	.uleb128 0x1d
 6029 1690 00 00 00 00 	.4byte .LASF33
 6030 1694 01          	.byte 0x1
 6031 1695 9B 01       	.2byte 0x19b
 6032 1697 B8 12 00 00 	.4byte 0x12b8
 6033 169b 02          	.byte 0x2
 6034 169c 7E          	.byte 0x7e
 6035 169d 14          	.sleb128 20
 6036 169e 1D          	.uleb128 0x1d
 6037 169f 00 00 00 00 	.4byte .LASF35
 6038 16a3 01          	.byte 0x1
 6039 16a4 9B 01       	.2byte 0x19b
 6040 16a6 F7 00 00 00 	.4byte 0xf7
 6041 16aa 02          	.byte 0x2
 6042 16ab 7E          	.byte 0x7e
 6043 16ac 16          	.sleb128 22
 6044 16ad 1D          	.uleb128 0x1d
 6045 16ae 00 00 00 00 	.4byte .LASF34
 6046 16b2 01          	.byte 0x1
 6047 16b3 9B 01       	.2byte 0x19b
 6048 16b5 B8 12 00 00 	.4byte 0x12b8
 6049 16b9 02          	.byte 0x2
 6050 16ba 7E          	.byte 0x7e
 6051 16bb 18          	.sleb128 24
 6052 16bc 20          	.uleb128 0x20
 6053 16bd 00 00 00 00 	.4byte .LASF36
 6054 16c1 01          	.byte 0x1
 6055 16c2 9E 01       	.2byte 0x19e
 6056 16c4 F7 00 00 00 	.4byte 0xf7
 6057 16c8 02          	.byte 0x2
 6058 16c9 7E          	.byte 0x7e
 6059 16ca 00          	.sleb128 0
 6060 16cb 20          	.uleb128 0x20
 6061 16cc 00 00 00 00 	.4byte .LASF37
 6062 16d0 01          	.byte 0x1
 6063 16d1 9F 01       	.2byte 0x19f
MPLAB XC16 ASSEMBLY Listing:   			page 128


 6064 16d3 F7 00 00 00 	.4byte 0xf7
 6065 16d7 02          	.byte 0x2
 6066 16d8 7E          	.byte 0x7e
 6067 16d9 02          	.sleb128 2
 6068 16da 20          	.uleb128 0x20
 6069 16db 00 00 00 00 	.4byte .LASF38
 6070 16df 01          	.byte 0x1
 6071 16e0 A0 01       	.2byte 0x1a0
 6072 16e2 F7 00 00 00 	.4byte 0xf7
 6073 16e6 02          	.byte 0x2
 6074 16e7 7E          	.byte 0x7e
 6075 16e8 10          	.sleb128 16
 6076 16e9 20          	.uleb128 0x20
 6077 16ea 00 00 00 00 	.4byte .LASF39
 6078 16ee 01          	.byte 0x1
 6079 16ef A1 01       	.2byte 0x1a1
 6080 16f1 F7 00 00 00 	.4byte 0xf7
 6081 16f5 02          	.byte 0x2
 6082 16f6 7E          	.byte 0x7e
 6083 16f7 12          	.sleb128 18
 6084 16f8 20          	.uleb128 0x20
 6085 16f9 00 00 00 00 	.4byte .LASF40
 6086 16fd 01          	.byte 0x1
 6087 16fe A3 01       	.2byte 0x1a3
 6088 1700 B8 12 00 00 	.4byte 0x12b8
 6089 1704 02          	.byte 0x2
 6090 1705 7E          	.byte 0x7e
 6091 1706 04          	.sleb128 4
 6092 1707 20          	.uleb128 0x20
 6093 1708 00 00 00 00 	.4byte .LASF41
 6094 170c 01          	.byte 0x1
 6095 170d A3 01       	.2byte 0x1a3
 6096 170f B8 12 00 00 	.4byte 0x12b8
 6097 1713 02          	.byte 0x2
 6098 1714 7E          	.byte 0x7e
 6099 1715 06          	.sleb128 6
 6100 1716 20          	.uleb128 0x20
 6101 1717 00 00 00 00 	.4byte .LASF42
 6102 171b 01          	.byte 0x1
 6103 171c A4 01       	.2byte 0x1a4
 6104 171e F7 00 00 00 	.4byte 0xf7
 6105 1722 02          	.byte 0x2
 6106 1723 7E          	.byte 0x7e
 6107 1724 08          	.sleb128 8
 6108 1725 20          	.uleb128 0x20
 6109 1726 00 00 00 00 	.4byte .LASF43
 6110 172a 01          	.byte 0x1
 6111 172b A5 01       	.2byte 0x1a5
 6112 172d F7 00 00 00 	.4byte 0xf7
 6113 1731 02          	.byte 0x2
 6114 1732 7E          	.byte 0x7e
 6115 1733 0A          	.sleb128 10
 6116 1734 20          	.uleb128 0x20
 6117 1735 00 00 00 00 	.4byte .LASF44
 6118 1739 01          	.byte 0x1
 6119 173a A5 01       	.2byte 0x1a5
 6120 173c F7 00 00 00 	.4byte 0xf7
MPLAB XC16 ASSEMBLY Listing:   			page 129


 6121 1740 02          	.byte 0x2
 6122 1741 7E          	.byte 0x7e
 6123 1742 0C          	.sleb128 12
 6124 1743 20          	.uleb128 0x20
 6125 1744 00 00 00 00 	.4byte .LASF45
 6126 1748 01          	.byte 0x1
 6127 1749 A7 01       	.2byte 0x1a7
 6128 174b 40 0F 00 00 	.4byte 0xf40
 6129 174f 02          	.byte 0x2
 6130 1750 7E          	.byte 0x7e
 6131 1751 0E          	.sleb128 14
 6132 1752 1E          	.uleb128 0x1e
 6133 1753 5C 11 00 00 	.4byte 0x115c
 6134 1757 00 00 00 00 	.4byte .LBB42
 6135 175b 00 00 00 00 	.4byte .LBE42
 6136 175f 00          	.byte 0x0
 6137 1760 1F          	.uleb128 0x1f
 6138 1761 01          	.byte 0x1
 6139 1762 53 50 49 32 	.asciz "SPI2_ExchangeBuffer"
 6139      5F 45 78 63 
 6139      68 61 6E 67 
 6139      65 42 75 66 
 6139      66 65 72 00 
 6140 1776 01          	.byte 0x1
 6141 1777 07 02       	.2byte 0x207
 6142 1779 01          	.byte 0x1
 6143 177a F7 00 00 00 	.4byte 0xf7
 6144 177e 00 00 00 00 	.4byte .LFB14
 6145 1782 00 00 00 00 	.4byte .LFE14
 6146 1786 01          	.byte 0x1
 6147 1787 5E          	.byte 0x5e
 6148 1788 5D 18 00 00 	.4byte 0x185d
 6149 178c 1D          	.uleb128 0x1d
 6150 178d 00 00 00 00 	.4byte .LASF33
 6151 1791 01          	.byte 0x1
 6152 1792 07 02       	.2byte 0x207
 6153 1794 B8 12 00 00 	.4byte 0x12b8
 6154 1798 02          	.byte 0x2
 6155 1799 7E          	.byte 0x7e
 6156 179a 14          	.sleb128 20
 6157 179b 1D          	.uleb128 0x1d
 6158 179c 00 00 00 00 	.4byte .LASF35
 6159 17a0 01          	.byte 0x1
 6160 17a1 07 02       	.2byte 0x207
 6161 17a3 F7 00 00 00 	.4byte 0xf7
 6162 17a7 02          	.byte 0x2
 6163 17a8 7E          	.byte 0x7e
 6164 17a9 16          	.sleb128 22
 6165 17aa 1D          	.uleb128 0x1d
 6166 17ab 00 00 00 00 	.4byte .LASF34
 6167 17af 01          	.byte 0x1
 6168 17b0 07 02       	.2byte 0x207
 6169 17b2 B8 12 00 00 	.4byte 0x12b8
 6170 17b6 02          	.byte 0x2
 6171 17b7 7E          	.byte 0x7e
 6172 17b8 18          	.sleb128 24
 6173 17b9 20          	.uleb128 0x20
MPLAB XC16 ASSEMBLY Listing:   			page 130


 6174 17ba 00 00 00 00 	.4byte .LASF36
 6175 17be 01          	.byte 0x1
 6176 17bf 0A 02       	.2byte 0x20a
 6177 17c1 F7 00 00 00 	.4byte 0xf7
 6178 17c5 02          	.byte 0x2
 6179 17c6 7E          	.byte 0x7e
 6180 17c7 00          	.sleb128 0
 6181 17c8 20          	.uleb128 0x20
 6182 17c9 00 00 00 00 	.4byte .LASF37
 6183 17cd 01          	.byte 0x1
 6184 17ce 0B 02       	.2byte 0x20b
 6185 17d0 F7 00 00 00 	.4byte 0xf7
 6186 17d4 02          	.byte 0x2
 6187 17d5 7E          	.byte 0x7e
 6188 17d6 02          	.sleb128 2
 6189 17d7 20          	.uleb128 0x20
 6190 17d8 00 00 00 00 	.4byte .LASF38
 6191 17dc 01          	.byte 0x1
 6192 17dd 0C 02       	.2byte 0x20c
 6193 17df F7 00 00 00 	.4byte 0xf7
 6194 17e3 02          	.byte 0x2
 6195 17e4 7E          	.byte 0x7e
 6196 17e5 10          	.sleb128 16
 6197 17e6 20          	.uleb128 0x20
 6198 17e7 00 00 00 00 	.4byte .LASF39
 6199 17eb 01          	.byte 0x1
 6200 17ec 0D 02       	.2byte 0x20d
 6201 17ee F7 00 00 00 	.4byte 0xf7
 6202 17f2 02          	.byte 0x2
 6203 17f3 7E          	.byte 0x7e
 6204 17f4 12          	.sleb128 18
 6205 17f5 20          	.uleb128 0x20
 6206 17f6 00 00 00 00 	.4byte .LASF40
 6207 17fa 01          	.byte 0x1
 6208 17fb 0F 02       	.2byte 0x20f
 6209 17fd B8 12 00 00 	.4byte 0x12b8
 6210 1801 02          	.byte 0x2
 6211 1802 7E          	.byte 0x7e
 6212 1803 04          	.sleb128 4
 6213 1804 20          	.uleb128 0x20
 6214 1805 00 00 00 00 	.4byte .LASF41
 6215 1809 01          	.byte 0x1
 6216 180a 0F 02       	.2byte 0x20f
 6217 180c B8 12 00 00 	.4byte 0x12b8
 6218 1810 02          	.byte 0x2
 6219 1811 7E          	.byte 0x7e
 6220 1812 06          	.sleb128 6
 6221 1813 20          	.uleb128 0x20
 6222 1814 00 00 00 00 	.4byte .LASF42
 6223 1818 01          	.byte 0x1
 6224 1819 10 02       	.2byte 0x210
 6225 181b F7 00 00 00 	.4byte 0xf7
 6226 181f 02          	.byte 0x2
 6227 1820 7E          	.byte 0x7e
 6228 1821 08          	.sleb128 8
 6229 1822 20          	.uleb128 0x20
 6230 1823 00 00 00 00 	.4byte .LASF43
MPLAB XC16 ASSEMBLY Listing:   			page 131


 6231 1827 01          	.byte 0x1
 6232 1828 11 02       	.2byte 0x211
 6233 182a F7 00 00 00 	.4byte 0xf7
 6234 182e 02          	.byte 0x2
 6235 182f 7E          	.byte 0x7e
 6236 1830 0A          	.sleb128 10
 6237 1831 20          	.uleb128 0x20
 6238 1832 00 00 00 00 	.4byte .LASF44
 6239 1836 01          	.byte 0x1
 6240 1837 11 02       	.2byte 0x211
 6241 1839 F7 00 00 00 	.4byte 0xf7
 6242 183d 02          	.byte 0x2
 6243 183e 7E          	.byte 0x7e
 6244 183f 0C          	.sleb128 12
 6245 1840 20          	.uleb128 0x20
 6246 1841 00 00 00 00 	.4byte .LASF45
 6247 1845 01          	.byte 0x1
 6248 1846 13 02       	.2byte 0x213
 6249 1848 40 0F 00 00 	.4byte 0xf40
 6250 184c 02          	.byte 0x2
 6251 184d 7E          	.byte 0x7e
 6252 184e 0E          	.sleb128 14
 6253 184f 1E          	.uleb128 0x1e
 6254 1850 7C 11 00 00 	.4byte 0x117c
 6255 1854 00 00 00 00 	.4byte .LBB46
 6256 1858 00 00 00 00 	.4byte .LBE46
 6257 185c 00          	.byte 0x0
 6258 185d 1F          	.uleb128 0x1f
 6259 185e 01          	.byte 0x1
 6260 185f 53 50 49 33 	.asciz "SPI3_ExchangeBuffer"
 6260      5F 45 78 63 
 6260      68 61 6E 67 
 6260      65 42 75 66 
 6260      66 65 72 00 
 6261 1873 01          	.byte 0x1
 6262 1874 72 02       	.2byte 0x272
 6263 1876 01          	.byte 0x1
 6264 1877 F7 00 00 00 	.4byte 0xf7
 6265 187b 00 00 00 00 	.4byte .LFB15
 6266 187f 00 00 00 00 	.4byte .LFE15
 6267 1883 01          	.byte 0x1
 6268 1884 5E          	.byte 0x5e
 6269 1885 5A 19 00 00 	.4byte 0x195a
 6270 1889 1D          	.uleb128 0x1d
 6271 188a 00 00 00 00 	.4byte .LASF33
 6272 188e 01          	.byte 0x1
 6273 188f 72 02       	.2byte 0x272
 6274 1891 B8 12 00 00 	.4byte 0x12b8
 6275 1895 02          	.byte 0x2
 6276 1896 7E          	.byte 0x7e
 6277 1897 14          	.sleb128 20
 6278 1898 1D          	.uleb128 0x1d
 6279 1899 00 00 00 00 	.4byte .LASF35
 6280 189d 01          	.byte 0x1
 6281 189e 72 02       	.2byte 0x272
 6282 18a0 F7 00 00 00 	.4byte 0xf7
 6283 18a4 02          	.byte 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 132


 6284 18a5 7E          	.byte 0x7e
 6285 18a6 16          	.sleb128 22
 6286 18a7 1D          	.uleb128 0x1d
 6287 18a8 00 00 00 00 	.4byte .LASF34
 6288 18ac 01          	.byte 0x1
 6289 18ad 72 02       	.2byte 0x272
 6290 18af B8 12 00 00 	.4byte 0x12b8
 6291 18b3 02          	.byte 0x2
 6292 18b4 7E          	.byte 0x7e
 6293 18b5 18          	.sleb128 24
 6294 18b6 20          	.uleb128 0x20
 6295 18b7 00 00 00 00 	.4byte .LASF36
 6296 18bb 01          	.byte 0x1
 6297 18bc 75 02       	.2byte 0x275
 6298 18be F7 00 00 00 	.4byte 0xf7
 6299 18c2 02          	.byte 0x2
 6300 18c3 7E          	.byte 0x7e
 6301 18c4 00          	.sleb128 0
 6302 18c5 20          	.uleb128 0x20
 6303 18c6 00 00 00 00 	.4byte .LASF37
 6304 18ca 01          	.byte 0x1
 6305 18cb 76 02       	.2byte 0x276
 6306 18cd F7 00 00 00 	.4byte 0xf7
 6307 18d1 02          	.byte 0x2
 6308 18d2 7E          	.byte 0x7e
 6309 18d3 02          	.sleb128 2
 6310 18d4 20          	.uleb128 0x20
 6311 18d5 00 00 00 00 	.4byte .LASF38
 6312 18d9 01          	.byte 0x1
 6313 18da 77 02       	.2byte 0x277
 6314 18dc F7 00 00 00 	.4byte 0xf7
 6315 18e0 02          	.byte 0x2
 6316 18e1 7E          	.byte 0x7e
 6317 18e2 10          	.sleb128 16
 6318 18e3 20          	.uleb128 0x20
 6319 18e4 00 00 00 00 	.4byte .LASF39
 6320 18e8 01          	.byte 0x1
 6321 18e9 78 02       	.2byte 0x278
 6322 18eb F7 00 00 00 	.4byte 0xf7
 6323 18ef 02          	.byte 0x2
 6324 18f0 7E          	.byte 0x7e
 6325 18f1 12          	.sleb128 18
 6326 18f2 20          	.uleb128 0x20
 6327 18f3 00 00 00 00 	.4byte .LASF40
 6328 18f7 01          	.byte 0x1
 6329 18f8 7A 02       	.2byte 0x27a
 6330 18fa B8 12 00 00 	.4byte 0x12b8
 6331 18fe 02          	.byte 0x2
 6332 18ff 7E          	.byte 0x7e
 6333 1900 04          	.sleb128 4
 6334 1901 20          	.uleb128 0x20
 6335 1902 00 00 00 00 	.4byte .LASF41
 6336 1906 01          	.byte 0x1
 6337 1907 7A 02       	.2byte 0x27a
 6338 1909 B8 12 00 00 	.4byte 0x12b8
 6339 190d 02          	.byte 0x2
 6340 190e 7E          	.byte 0x7e
MPLAB XC16 ASSEMBLY Listing:   			page 133


 6341 190f 06          	.sleb128 6
 6342 1910 20          	.uleb128 0x20
 6343 1911 00 00 00 00 	.4byte .LASF42
 6344 1915 01          	.byte 0x1
 6345 1916 7B 02       	.2byte 0x27b
 6346 1918 F7 00 00 00 	.4byte 0xf7
 6347 191c 02          	.byte 0x2
 6348 191d 7E          	.byte 0x7e
 6349 191e 08          	.sleb128 8
 6350 191f 20          	.uleb128 0x20
 6351 1920 00 00 00 00 	.4byte .LASF43
 6352 1924 01          	.byte 0x1
 6353 1925 7C 02       	.2byte 0x27c
 6354 1927 F7 00 00 00 	.4byte 0xf7
 6355 192b 02          	.byte 0x2
 6356 192c 7E          	.byte 0x7e
 6357 192d 0A          	.sleb128 10
 6358 192e 20          	.uleb128 0x20
 6359 192f 00 00 00 00 	.4byte .LASF44
 6360 1933 01          	.byte 0x1
 6361 1934 7C 02       	.2byte 0x27c
 6362 1936 F7 00 00 00 	.4byte 0xf7
 6363 193a 02          	.byte 0x2
 6364 193b 7E          	.byte 0x7e
 6365 193c 0C          	.sleb128 12
 6366 193d 20          	.uleb128 0x20
 6367 193e 00 00 00 00 	.4byte .LASF45
 6368 1942 01          	.byte 0x1
 6369 1943 7E 02       	.2byte 0x27e
 6370 1945 40 0F 00 00 	.4byte 0xf40
 6371 1949 02          	.byte 0x2
 6372 194a 7E          	.byte 0x7e
 6373 194b 0E          	.sleb128 14
 6374 194c 1E          	.uleb128 0x1e
 6375 194d 9C 11 00 00 	.4byte 0x119c
 6376 1951 00 00 00 00 	.4byte .LBB50
 6377 1955 00 00 00 00 	.4byte .LBE50
 6378 1959 00          	.byte 0x0
 6379 195a 1F          	.uleb128 0x1f
 6380 195b 01          	.byte 0x1
 6381 195c 53 50 49 34 	.asciz "SPI4_ExchangeBuffer"
 6381      5F 45 78 63 
 6381      68 61 6E 67 
 6381      65 42 75 66 
 6381      66 65 72 00 
 6382 1970 01          	.byte 0x1
 6383 1971 DD 02       	.2byte 0x2dd
 6384 1973 01          	.byte 0x1
 6385 1974 F7 00 00 00 	.4byte 0xf7
 6386 1978 00 00 00 00 	.4byte .LFB16
 6387 197c 00 00 00 00 	.4byte .LFE16
 6388 1980 01          	.byte 0x1
 6389 1981 5E          	.byte 0x5e
 6390 1982 57 1A 00 00 	.4byte 0x1a57
 6391 1986 1D          	.uleb128 0x1d
 6392 1987 00 00 00 00 	.4byte .LASF33
 6393 198b 01          	.byte 0x1
MPLAB XC16 ASSEMBLY Listing:   			page 134


 6394 198c DD 02       	.2byte 0x2dd
 6395 198e B8 12 00 00 	.4byte 0x12b8
 6396 1992 02          	.byte 0x2
 6397 1993 7E          	.byte 0x7e
 6398 1994 14          	.sleb128 20
 6399 1995 1D          	.uleb128 0x1d
 6400 1996 00 00 00 00 	.4byte .LASF35
 6401 199a 01          	.byte 0x1
 6402 199b DD 02       	.2byte 0x2dd
 6403 199d F7 00 00 00 	.4byte 0xf7
 6404 19a1 02          	.byte 0x2
 6405 19a2 7E          	.byte 0x7e
 6406 19a3 16          	.sleb128 22
 6407 19a4 1D          	.uleb128 0x1d
 6408 19a5 00 00 00 00 	.4byte .LASF34
 6409 19a9 01          	.byte 0x1
 6410 19aa DD 02       	.2byte 0x2dd
 6411 19ac B8 12 00 00 	.4byte 0x12b8
 6412 19b0 02          	.byte 0x2
 6413 19b1 7E          	.byte 0x7e
 6414 19b2 18          	.sleb128 24
 6415 19b3 20          	.uleb128 0x20
 6416 19b4 00 00 00 00 	.4byte .LASF36
 6417 19b8 01          	.byte 0x1
 6418 19b9 E0 02       	.2byte 0x2e0
 6419 19bb F7 00 00 00 	.4byte 0xf7
 6420 19bf 02          	.byte 0x2
 6421 19c0 7E          	.byte 0x7e
 6422 19c1 00          	.sleb128 0
 6423 19c2 20          	.uleb128 0x20
 6424 19c3 00 00 00 00 	.4byte .LASF37
 6425 19c7 01          	.byte 0x1
 6426 19c8 E1 02       	.2byte 0x2e1
 6427 19ca F7 00 00 00 	.4byte 0xf7
 6428 19ce 02          	.byte 0x2
 6429 19cf 7E          	.byte 0x7e
 6430 19d0 02          	.sleb128 2
 6431 19d1 20          	.uleb128 0x20
 6432 19d2 00 00 00 00 	.4byte .LASF38
 6433 19d6 01          	.byte 0x1
 6434 19d7 E2 02       	.2byte 0x2e2
 6435 19d9 F7 00 00 00 	.4byte 0xf7
 6436 19dd 02          	.byte 0x2
 6437 19de 7E          	.byte 0x7e
 6438 19df 10          	.sleb128 16
 6439 19e0 20          	.uleb128 0x20
 6440 19e1 00 00 00 00 	.4byte .LASF39
 6441 19e5 01          	.byte 0x1
 6442 19e6 E3 02       	.2byte 0x2e3
 6443 19e8 F7 00 00 00 	.4byte 0xf7
 6444 19ec 02          	.byte 0x2
 6445 19ed 7E          	.byte 0x7e
 6446 19ee 12          	.sleb128 18
 6447 19ef 20          	.uleb128 0x20
 6448 19f0 00 00 00 00 	.4byte .LASF40
 6449 19f4 01          	.byte 0x1
 6450 19f5 E5 02       	.2byte 0x2e5
MPLAB XC16 ASSEMBLY Listing:   			page 135


 6451 19f7 B8 12 00 00 	.4byte 0x12b8
 6452 19fb 02          	.byte 0x2
 6453 19fc 7E          	.byte 0x7e
 6454 19fd 04          	.sleb128 4
 6455 19fe 20          	.uleb128 0x20
 6456 19ff 00 00 00 00 	.4byte .LASF41
 6457 1a03 01          	.byte 0x1
 6458 1a04 E5 02       	.2byte 0x2e5
 6459 1a06 B8 12 00 00 	.4byte 0x12b8
 6460 1a0a 02          	.byte 0x2
 6461 1a0b 7E          	.byte 0x7e
 6462 1a0c 06          	.sleb128 6
 6463 1a0d 20          	.uleb128 0x20
 6464 1a0e 00 00 00 00 	.4byte .LASF42
 6465 1a12 01          	.byte 0x1
 6466 1a13 E6 02       	.2byte 0x2e6
 6467 1a15 F7 00 00 00 	.4byte 0xf7
 6468 1a19 02          	.byte 0x2
 6469 1a1a 7E          	.byte 0x7e
 6470 1a1b 08          	.sleb128 8
 6471 1a1c 20          	.uleb128 0x20
 6472 1a1d 00 00 00 00 	.4byte .LASF43
 6473 1a21 01          	.byte 0x1
 6474 1a22 E7 02       	.2byte 0x2e7
 6475 1a24 F7 00 00 00 	.4byte 0xf7
 6476 1a28 02          	.byte 0x2
 6477 1a29 7E          	.byte 0x7e
 6478 1a2a 0A          	.sleb128 10
 6479 1a2b 20          	.uleb128 0x20
 6480 1a2c 00 00 00 00 	.4byte .LASF44
 6481 1a30 01          	.byte 0x1
 6482 1a31 E7 02       	.2byte 0x2e7
 6483 1a33 F7 00 00 00 	.4byte 0xf7
 6484 1a37 02          	.byte 0x2
 6485 1a38 7E          	.byte 0x7e
 6486 1a39 0C          	.sleb128 12
 6487 1a3a 20          	.uleb128 0x20
 6488 1a3b 00 00 00 00 	.4byte .LASF45
 6489 1a3f 01          	.byte 0x1
 6490 1a40 E9 02       	.2byte 0x2e9
 6491 1a42 40 0F 00 00 	.4byte 0xf40
 6492 1a46 02          	.byte 0x2
 6493 1a47 7E          	.byte 0x7e
 6494 1a48 0E          	.sleb128 14
 6495 1a49 1E          	.uleb128 0x1e
 6496 1a4a BC 11 00 00 	.4byte 0x11bc
 6497 1a4e 00 00 00 00 	.4byte .LBB54
 6498 1a52 00 00 00 00 	.4byte .LBE54
 6499 1a56 00          	.byte 0x0
 6500 1a57 21          	.uleb128 0x21
 6501 1a58 5C 11 00 00 	.4byte 0x115c
 6502 1a5c 00 00 00 00 	.4byte .LFB17
 6503 1a60 00 00 00 00 	.4byte .LFE17
 6504 1a64 01          	.byte 0x1
 6505 1a65 5E          	.byte 0x5e
 6506 1a66 21          	.uleb128 0x21
 6507 1a67 7C 11 00 00 	.4byte 0x117c
MPLAB XC16 ASSEMBLY Listing:   			page 136


 6508 1a6b 00 00 00 00 	.4byte .LFB18
 6509 1a6f 00 00 00 00 	.4byte .LFE18
 6510 1a73 01          	.byte 0x1
 6511 1a74 5E          	.byte 0x5e
 6512 1a75 21          	.uleb128 0x21
 6513 1a76 9C 11 00 00 	.4byte 0x119c
 6514 1a7a 00 00 00 00 	.4byte .LFB19
 6515 1a7e 00 00 00 00 	.4byte .LFE19
 6516 1a82 01          	.byte 0x1
 6517 1a83 5E          	.byte 0x5e
 6518 1a84 21          	.uleb128 0x21
 6519 1a85 BC 11 00 00 	.4byte 0x11bc
 6520 1a89 00 00 00 00 	.4byte .LFB20
 6521 1a8d 00 00 00 00 	.4byte .LFE20
 6522 1a91 01          	.byte 0x1
 6523 1a92 5E          	.byte 0x5e
 6524 1a93 22          	.uleb128 0x22
 6525 1a94 00 00 00 00 	.4byte .LASF46
 6526 1a98 03          	.byte 0x3
 6527 1a99 48 07       	.2byte 0x748
 6528 1a9b A1 1A 00 00 	.4byte 0x1aa1
 6529 1a9f 01          	.byte 0x1
 6530 1aa0 01          	.byte 0x1
 6531 1aa1 23          	.uleb128 0x23
 6532 1aa2 AE 02 00 00 	.4byte 0x2ae
 6533 1aa6 22          	.uleb128 0x22
 6534 1aa7 00 00 00 00 	.4byte .LASF47
 6535 1aab 03          	.byte 0x3
 6536 1aac 63 07       	.2byte 0x763
 6537 1aae B4 1A 00 00 	.4byte 0x1ab4
 6538 1ab2 01          	.byte 0x1
 6539 1ab3 01          	.byte 0x1
 6540 1ab4 23          	.uleb128 0x23
 6541 1ab5 1E 04 00 00 	.4byte 0x41e
 6542 1ab9 22          	.uleb128 0x22
 6543 1aba 00 00 00 00 	.4byte .LASF48
 6544 1abe 03          	.byte 0x3
 6545 1abf 6F 07       	.2byte 0x76f
 6546 1ac1 C7 1A 00 00 	.4byte 0x1ac7
 6547 1ac5 01          	.byte 0x1
 6548 1ac6 01          	.byte 0x1
 6549 1ac7 23          	.uleb128 0x23
 6550 1ac8 A7 04 00 00 	.4byte 0x4a7
 6551 1acc 24          	.uleb128 0x24
 6552 1acd 53 50 49 31 	.asciz "SPI1BUF"
 6552      42 55 46 00 
 6553 1ad5 03          	.byte 0x3
 6554 1ad6 72 07       	.2byte 0x772
 6555 1ad8 DE 1A 00 00 	.4byte 0x1ade
 6556 1adc 01          	.byte 0x1
 6557 1add 01          	.byte 0x1
 6558 1ade 23          	.uleb128 0x23
 6559 1adf F7 00 00 00 	.4byte 0xf7
 6560 1ae3 22          	.uleb128 0x22
 6561 1ae4 00 00 00 00 	.4byte .LASF49
 6562 1ae8 03          	.byte 0x3
 6563 1ae9 D8 07       	.2byte 0x7d8
MPLAB XC16 ASSEMBLY Listing:   			page 137


 6564 1aeb F1 1A 00 00 	.4byte 0x1af1
 6565 1aef 01          	.byte 0x1
 6566 1af0 01          	.byte 0x1
 6567 1af1 23          	.uleb128 0x23
 6568 1af2 14 06 00 00 	.4byte 0x614
 6569 1af6 22          	.uleb128 0x22
 6570 1af7 00 00 00 00 	.4byte .LASF50
 6571 1afb 03          	.byte 0x3
 6572 1afc F3 07       	.2byte 0x7f3
 6573 1afe 04 1B 00 00 	.4byte 0x1b04
 6574 1b02 01          	.byte 0x1
 6575 1b03 01          	.byte 0x1
 6576 1b04 23          	.uleb128 0x23
 6577 1b05 84 07 00 00 	.4byte 0x784
 6578 1b09 22          	.uleb128 0x22
 6579 1b0a 00 00 00 00 	.4byte .LASF51
 6580 1b0e 03          	.byte 0x3
 6581 1b0f FF 07       	.2byte 0x7ff
 6582 1b11 17 1B 00 00 	.4byte 0x1b17
 6583 1b15 01          	.byte 0x1
 6584 1b16 01          	.byte 0x1
 6585 1b17 23          	.uleb128 0x23
 6586 1b18 0D 08 00 00 	.4byte 0x80d
 6587 1b1c 24          	.uleb128 0x24
 6588 1b1d 53 50 49 32 	.asciz "SPI2BUF"
 6588      42 55 46 00 
 6589 1b25 03          	.byte 0x3
 6590 1b26 02 08       	.2byte 0x802
 6591 1b28 DE 1A 00 00 	.4byte 0x1ade
 6592 1b2c 01          	.byte 0x1
 6593 1b2d 01          	.byte 0x1
 6594 1b2e 22          	.uleb128 0x22
 6595 1b2f 00 00 00 00 	.4byte .LASF52
 6596 1b33 03          	.byte 0x3
 6597 1b34 C4 08       	.2byte 0x8c4
 6598 1b36 3C 1B 00 00 	.4byte 0x1b3c
 6599 1b3a 01          	.byte 0x1
 6600 1b3b 01          	.byte 0x1
 6601 1b3c 23          	.uleb128 0x23
 6602 1b3d 7A 09 00 00 	.4byte 0x97a
 6603 1b41 22          	.uleb128 0x22
 6604 1b42 00 00 00 00 	.4byte .LASF53
 6605 1b46 03          	.byte 0x3
 6606 1b47 DF 08       	.2byte 0x8df
 6607 1b49 4F 1B 00 00 	.4byte 0x1b4f
 6608 1b4d 01          	.byte 0x1
 6609 1b4e 01          	.byte 0x1
 6610 1b4f 23          	.uleb128 0x23
 6611 1b50 EA 0A 00 00 	.4byte 0xaea
 6612 1b54 22          	.uleb128 0x22
 6613 1b55 00 00 00 00 	.4byte .LASF54
 6614 1b59 03          	.byte 0x3
 6615 1b5a EB 08       	.2byte 0x8eb
 6616 1b5c 62 1B 00 00 	.4byte 0x1b62
 6617 1b60 01          	.byte 0x1
 6618 1b61 01          	.byte 0x1
 6619 1b62 23          	.uleb128 0x23
MPLAB XC16 ASSEMBLY Listing:   			page 138


 6620 1b63 73 0B 00 00 	.4byte 0xb73
 6621 1b67 24          	.uleb128 0x24
 6622 1b68 53 50 49 33 	.asciz "SPI3BUF"
 6622      42 55 46 00 
 6623 1b70 03          	.byte 0x3
 6624 1b71 EE 08       	.2byte 0x8ee
 6625 1b73 DE 1A 00 00 	.4byte 0x1ade
 6626 1b77 01          	.byte 0x1
 6627 1b78 01          	.byte 0x1
 6628 1b79 22          	.uleb128 0x22
 6629 1b7a 00 00 00 00 	.4byte .LASF55
 6630 1b7e 03          	.byte 0x3
 6631 1b7f 54 09       	.2byte 0x954
 6632 1b81 87 1B 00 00 	.4byte 0x1b87
 6633 1b85 01          	.byte 0x1
 6634 1b86 01          	.byte 0x1
 6635 1b87 23          	.uleb128 0x23
 6636 1b88 E0 0C 00 00 	.4byte 0xce0
 6637 1b8c 22          	.uleb128 0x22
 6638 1b8d 00 00 00 00 	.4byte .LASF56
 6639 1b91 03          	.byte 0x3
 6640 1b92 6F 09       	.2byte 0x96f
 6641 1b94 9A 1B 00 00 	.4byte 0x1b9a
 6642 1b98 01          	.byte 0x1
 6643 1b99 01          	.byte 0x1
 6644 1b9a 23          	.uleb128 0x23
 6645 1b9b 50 0E 00 00 	.4byte 0xe50
 6646 1b9f 22          	.uleb128 0x22
 6647 1ba0 00 00 00 00 	.4byte .LASF57
 6648 1ba4 03          	.byte 0x3
 6649 1ba5 7B 09       	.2byte 0x97b
 6650 1ba7 AD 1B 00 00 	.4byte 0x1bad
 6651 1bab 01          	.byte 0x1
 6652 1bac 01          	.byte 0x1
 6653 1bad 23          	.uleb128 0x23
 6654 1bae D9 0E 00 00 	.4byte 0xed9
 6655 1bb2 24          	.uleb128 0x24
 6656 1bb3 53 50 49 34 	.asciz "SPI4BUF"
 6656      42 55 46 00 
 6657 1bbb 03          	.byte 0x3
 6658 1bbc 7E 09       	.2byte 0x97e
 6659 1bbe DE 1A 00 00 	.4byte 0x1ade
 6660 1bc2 01          	.byte 0x1
 6661 1bc3 01          	.byte 0x1
 6662 1bc4 22          	.uleb128 0x22
 6663 1bc5 00 00 00 00 	.4byte .LASF46
 6664 1bc9 03          	.byte 0x3
 6665 1bca 48 07       	.2byte 0x748
 6666 1bcc A1 1A 00 00 	.4byte 0x1aa1
 6667 1bd0 01          	.byte 0x1
 6668 1bd1 01          	.byte 0x1
 6669 1bd2 22          	.uleb128 0x22
 6670 1bd3 00 00 00 00 	.4byte .LASF47
 6671 1bd7 03          	.byte 0x3
 6672 1bd8 63 07       	.2byte 0x763
 6673 1bda B4 1A 00 00 	.4byte 0x1ab4
 6674 1bde 01          	.byte 0x1
MPLAB XC16 ASSEMBLY Listing:   			page 139


 6675 1bdf 01          	.byte 0x1
 6676 1be0 22          	.uleb128 0x22
 6677 1be1 00 00 00 00 	.4byte .LASF48
 6678 1be5 03          	.byte 0x3
 6679 1be6 6F 07       	.2byte 0x76f
 6680 1be8 C7 1A 00 00 	.4byte 0x1ac7
 6681 1bec 01          	.byte 0x1
 6682 1bed 01          	.byte 0x1
 6683 1bee 24          	.uleb128 0x24
 6684 1bef 53 50 49 31 	.asciz "SPI1BUF"
 6684      42 55 46 00 
 6685 1bf7 03          	.byte 0x3
 6686 1bf8 72 07       	.2byte 0x772
 6687 1bfa DE 1A 00 00 	.4byte 0x1ade
 6688 1bfe 01          	.byte 0x1
 6689 1bff 01          	.byte 0x1
 6690 1c00 22          	.uleb128 0x22
 6691 1c01 00 00 00 00 	.4byte .LASF49
 6692 1c05 03          	.byte 0x3
 6693 1c06 D8 07       	.2byte 0x7d8
 6694 1c08 F1 1A 00 00 	.4byte 0x1af1
 6695 1c0c 01          	.byte 0x1
 6696 1c0d 01          	.byte 0x1
 6697 1c0e 22          	.uleb128 0x22
 6698 1c0f 00 00 00 00 	.4byte .LASF50
 6699 1c13 03          	.byte 0x3
 6700 1c14 F3 07       	.2byte 0x7f3
 6701 1c16 04 1B 00 00 	.4byte 0x1b04
 6702 1c1a 01          	.byte 0x1
 6703 1c1b 01          	.byte 0x1
 6704 1c1c 22          	.uleb128 0x22
 6705 1c1d 00 00 00 00 	.4byte .LASF51
 6706 1c21 03          	.byte 0x3
 6707 1c22 FF 07       	.2byte 0x7ff
 6708 1c24 17 1B 00 00 	.4byte 0x1b17
 6709 1c28 01          	.byte 0x1
 6710 1c29 01          	.byte 0x1
 6711 1c2a 24          	.uleb128 0x24
 6712 1c2b 53 50 49 32 	.asciz "SPI2BUF"
 6712      42 55 46 00 
 6713 1c33 03          	.byte 0x3
 6714 1c34 02 08       	.2byte 0x802
 6715 1c36 DE 1A 00 00 	.4byte 0x1ade
 6716 1c3a 01          	.byte 0x1
 6717 1c3b 01          	.byte 0x1
 6718 1c3c 22          	.uleb128 0x22
 6719 1c3d 00 00 00 00 	.4byte .LASF52
 6720 1c41 03          	.byte 0x3
 6721 1c42 C4 08       	.2byte 0x8c4
 6722 1c44 3C 1B 00 00 	.4byte 0x1b3c
 6723 1c48 01          	.byte 0x1
 6724 1c49 01          	.byte 0x1
 6725 1c4a 22          	.uleb128 0x22
 6726 1c4b 00 00 00 00 	.4byte .LASF53
 6727 1c4f 03          	.byte 0x3
 6728 1c50 DF 08       	.2byte 0x8df
 6729 1c52 4F 1B 00 00 	.4byte 0x1b4f
MPLAB XC16 ASSEMBLY Listing:   			page 140


 6730 1c56 01          	.byte 0x1
 6731 1c57 01          	.byte 0x1
 6732 1c58 22          	.uleb128 0x22
 6733 1c59 00 00 00 00 	.4byte .LASF54
 6734 1c5d 03          	.byte 0x3
 6735 1c5e EB 08       	.2byte 0x8eb
 6736 1c60 62 1B 00 00 	.4byte 0x1b62
 6737 1c64 01          	.byte 0x1
 6738 1c65 01          	.byte 0x1
 6739 1c66 24          	.uleb128 0x24
 6740 1c67 53 50 49 33 	.asciz "SPI3BUF"
 6740      42 55 46 00 
 6741 1c6f 03          	.byte 0x3
 6742 1c70 EE 08       	.2byte 0x8ee
 6743 1c72 DE 1A 00 00 	.4byte 0x1ade
 6744 1c76 01          	.byte 0x1
 6745 1c77 01          	.byte 0x1
 6746 1c78 22          	.uleb128 0x22
 6747 1c79 00 00 00 00 	.4byte .LASF55
 6748 1c7d 03          	.byte 0x3
 6749 1c7e 54 09       	.2byte 0x954
 6750 1c80 87 1B 00 00 	.4byte 0x1b87
 6751 1c84 01          	.byte 0x1
 6752 1c85 01          	.byte 0x1
 6753 1c86 22          	.uleb128 0x22
 6754 1c87 00 00 00 00 	.4byte .LASF56
 6755 1c8b 03          	.byte 0x3
 6756 1c8c 6F 09       	.2byte 0x96f
 6757 1c8e 9A 1B 00 00 	.4byte 0x1b9a
 6758 1c92 01          	.byte 0x1
 6759 1c93 01          	.byte 0x1
 6760 1c94 22          	.uleb128 0x22
 6761 1c95 00 00 00 00 	.4byte .LASF57
 6762 1c99 03          	.byte 0x3
 6763 1c9a 7B 09       	.2byte 0x97b
 6764 1c9c AD 1B 00 00 	.4byte 0x1bad
 6765 1ca0 01          	.byte 0x1
 6766 1ca1 01          	.byte 0x1
 6767 1ca2 24          	.uleb128 0x24
 6768 1ca3 53 50 49 34 	.asciz "SPI4BUF"
 6768      42 55 46 00 
 6769 1cab 03          	.byte 0x3
 6770 1cac 7E 09       	.2byte 0x97e
 6771 1cae DE 1A 00 00 	.4byte 0x1ade
 6772 1cb2 01          	.byte 0x1
 6773 1cb3 01          	.byte 0x1
 6774 1cb4 00          	.byte 0x0
 6775                 	.section .debug_abbrev,info
 6776 0000 01          	.uleb128 0x1
 6777 0001 11          	.uleb128 0x11
 6778 0002 01          	.byte 0x1
 6779 0003 25          	.uleb128 0x25
 6780 0004 08          	.uleb128 0x8
 6781 0005 13          	.uleb128 0x13
 6782 0006 0B          	.uleb128 0xb
 6783 0007 03          	.uleb128 0x3
 6784 0008 08          	.uleb128 0x8
MPLAB XC16 ASSEMBLY Listing:   			page 141


 6785 0009 1B          	.uleb128 0x1b
 6786 000a 08          	.uleb128 0x8
 6787 000b 11          	.uleb128 0x11
 6788 000c 01          	.uleb128 0x1
 6789 000d 12          	.uleb128 0x12
 6790 000e 01          	.uleb128 0x1
 6791 000f 10          	.uleb128 0x10
 6792 0010 06          	.uleb128 0x6
 6793 0011 00          	.byte 0x0
 6794 0012 00          	.byte 0x0
 6795 0013 02          	.uleb128 0x2
 6796 0014 24          	.uleb128 0x24
 6797 0015 00          	.byte 0x0
 6798 0016 0B          	.uleb128 0xb
 6799 0017 0B          	.uleb128 0xb
 6800 0018 3E          	.uleb128 0x3e
 6801 0019 0B          	.uleb128 0xb
 6802 001a 03          	.uleb128 0x3
 6803 001b 08          	.uleb128 0x8
 6804 001c 00          	.byte 0x0
 6805 001d 00          	.byte 0x0
 6806 001e 03          	.uleb128 0x3
 6807 001f 16          	.uleb128 0x16
 6808 0020 00          	.byte 0x0
 6809 0021 03          	.uleb128 0x3
 6810 0022 08          	.uleb128 0x8
 6811 0023 3A          	.uleb128 0x3a
 6812 0024 0B          	.uleb128 0xb
 6813 0025 3B          	.uleb128 0x3b
 6814 0026 0B          	.uleb128 0xb
 6815 0027 49          	.uleb128 0x49
 6816 0028 13          	.uleb128 0x13
 6817 0029 00          	.byte 0x0
 6818 002a 00          	.byte 0x0
 6819 002b 04          	.uleb128 0x4
 6820 002c 13          	.uleb128 0x13
 6821 002d 01          	.byte 0x1
 6822 002e 0B          	.uleb128 0xb
 6823 002f 0B          	.uleb128 0xb
 6824 0030 3A          	.uleb128 0x3a
 6825 0031 0B          	.uleb128 0xb
 6826 0032 3B          	.uleb128 0x3b
 6827 0033 05          	.uleb128 0x5
 6828 0034 01          	.uleb128 0x1
 6829 0035 13          	.uleb128 0x13
 6830 0036 00          	.byte 0x0
 6831 0037 00          	.byte 0x0
 6832 0038 05          	.uleb128 0x5
 6833 0039 0D          	.uleb128 0xd
 6834 003a 00          	.byte 0x0
 6835 003b 03          	.uleb128 0x3
 6836 003c 0E          	.uleb128 0xe
 6837 003d 3A          	.uleb128 0x3a
 6838 003e 0B          	.uleb128 0xb
 6839 003f 3B          	.uleb128 0x3b
 6840 0040 05          	.uleb128 0x5
 6841 0041 49          	.uleb128 0x49
MPLAB XC16 ASSEMBLY Listing:   			page 142


 6842 0042 13          	.uleb128 0x13
 6843 0043 0B          	.uleb128 0xb
 6844 0044 0B          	.uleb128 0xb
 6845 0045 0D          	.uleb128 0xd
 6846 0046 0B          	.uleb128 0xb
 6847 0047 0C          	.uleb128 0xc
 6848 0048 0B          	.uleb128 0xb
 6849 0049 38          	.uleb128 0x38
 6850 004a 0A          	.uleb128 0xa
 6851 004b 00          	.byte 0x0
 6852 004c 00          	.byte 0x0
 6853 004d 06          	.uleb128 0x6
 6854 004e 17          	.uleb128 0x17
 6855 004f 01          	.byte 0x1
 6856 0050 0B          	.uleb128 0xb
 6857 0051 0B          	.uleb128 0xb
 6858 0052 3A          	.uleb128 0x3a
 6859 0053 0B          	.uleb128 0xb
 6860 0054 3B          	.uleb128 0x3b
 6861 0055 05          	.uleb128 0x5
 6862 0056 01          	.uleb128 0x1
 6863 0057 13          	.uleb128 0x13
 6864 0058 00          	.byte 0x0
 6865 0059 00          	.byte 0x0
 6866 005a 07          	.uleb128 0x7
 6867 005b 0D          	.uleb128 0xd
 6868 005c 00          	.byte 0x0
 6869 005d 49          	.uleb128 0x49
 6870 005e 13          	.uleb128 0x13
 6871 005f 00          	.byte 0x0
 6872 0060 00          	.byte 0x0
 6873 0061 08          	.uleb128 0x8
 6874 0062 13          	.uleb128 0x13
 6875 0063 01          	.byte 0x1
 6876 0064 03          	.uleb128 0x3
 6877 0065 08          	.uleb128 0x8
 6878 0066 0B          	.uleb128 0xb
 6879 0067 0B          	.uleb128 0xb
 6880 0068 3A          	.uleb128 0x3a
 6881 0069 0B          	.uleb128 0xb
 6882 006a 3B          	.uleb128 0x3b
 6883 006b 05          	.uleb128 0x5
 6884 006c 01          	.uleb128 0x1
 6885 006d 13          	.uleb128 0x13
 6886 006e 00          	.byte 0x0
 6887 006f 00          	.byte 0x0
 6888 0070 09          	.uleb128 0x9
 6889 0071 0D          	.uleb128 0xd
 6890 0072 00          	.byte 0x0
 6891 0073 49          	.uleb128 0x49
 6892 0074 13          	.uleb128 0x13
 6893 0075 38          	.uleb128 0x38
 6894 0076 0A          	.uleb128 0xa
 6895 0077 00          	.byte 0x0
 6896 0078 00          	.byte 0x0
 6897 0079 0A          	.uleb128 0xa
 6898 007a 16          	.uleb128 0x16
MPLAB XC16 ASSEMBLY Listing:   			page 143


 6899 007b 00          	.byte 0x0
 6900 007c 03          	.uleb128 0x3
 6901 007d 08          	.uleb128 0x8
 6902 007e 3A          	.uleb128 0x3a
 6903 007f 0B          	.uleb128 0xb
 6904 0080 3B          	.uleb128 0x3b
 6905 0081 05          	.uleb128 0x5
 6906 0082 49          	.uleb128 0x49
 6907 0083 13          	.uleb128 0x13
 6908 0084 00          	.byte 0x0
 6909 0085 00          	.byte 0x0
 6910 0086 0B          	.uleb128 0xb
 6911 0087 0D          	.uleb128 0xd
 6912 0088 00          	.byte 0x0
 6913 0089 03          	.uleb128 0x3
 6914 008a 08          	.uleb128 0x8
 6915 008b 3A          	.uleb128 0x3a
 6916 008c 0B          	.uleb128 0xb
 6917 008d 3B          	.uleb128 0x3b
 6918 008e 05          	.uleb128 0x5
 6919 008f 49          	.uleb128 0x49
 6920 0090 13          	.uleb128 0x13
 6921 0091 0B          	.uleb128 0xb
 6922 0092 0B          	.uleb128 0xb
 6923 0093 0D          	.uleb128 0xd
 6924 0094 0B          	.uleb128 0xb
 6925 0095 0C          	.uleb128 0xc
 6926 0096 0B          	.uleb128 0xb
 6927 0097 38          	.uleb128 0x38
 6928 0098 0A          	.uleb128 0xa
 6929 0099 00          	.byte 0x0
 6930 009a 00          	.byte 0x0
 6931 009b 0C          	.uleb128 0xc
 6932 009c 04          	.uleb128 0x4
 6933 009d 01          	.byte 0x1
 6934 009e 0B          	.uleb128 0xb
 6935 009f 0B          	.uleb128 0xb
 6936 00a0 3A          	.uleb128 0x3a
 6937 00a1 0B          	.uleb128 0xb
 6938 00a2 3B          	.uleb128 0x3b
 6939 00a3 0B          	.uleb128 0xb
 6940 00a4 01          	.uleb128 0x1
 6941 00a5 13          	.uleb128 0x13
 6942 00a6 00          	.byte 0x0
 6943 00a7 00          	.byte 0x0
 6944 00a8 0D          	.uleb128 0xd
 6945 00a9 28          	.uleb128 0x28
 6946 00aa 00          	.byte 0x0
 6947 00ab 03          	.uleb128 0x3
 6948 00ac 08          	.uleb128 0x8
 6949 00ad 1C          	.uleb128 0x1c
 6950 00ae 0D          	.uleb128 0xd
 6951 00af 00          	.byte 0x0
 6952 00b0 00          	.byte 0x0
 6953 00b1 0E          	.uleb128 0xe
 6954 00b2 13          	.uleb128 0x13
 6955 00b3 01          	.byte 0x1
MPLAB XC16 ASSEMBLY Listing:   			page 144


 6956 00b4 03          	.uleb128 0x3
 6957 00b5 08          	.uleb128 0x8
 6958 00b6 0B          	.uleb128 0xb
 6959 00b7 0B          	.uleb128 0xb
 6960 00b8 3A          	.uleb128 0x3a
 6961 00b9 0B          	.uleb128 0xb
 6962 00ba 3B          	.uleb128 0x3b
 6963 00bb 0B          	.uleb128 0xb
 6964 00bc 01          	.uleb128 0x1
 6965 00bd 13          	.uleb128 0x13
 6966 00be 00          	.byte 0x0
 6967 00bf 00          	.byte 0x0
 6968 00c0 0F          	.uleb128 0xf
 6969 00c1 0D          	.uleb128 0xd
 6970 00c2 00          	.byte 0x0
 6971 00c3 03          	.uleb128 0x3
 6972 00c4 08          	.uleb128 0x8
 6973 00c5 3A          	.uleb128 0x3a
 6974 00c6 0B          	.uleb128 0xb
 6975 00c7 3B          	.uleb128 0x3b
 6976 00c8 0B          	.uleb128 0xb
 6977 00c9 49          	.uleb128 0x49
 6978 00ca 13          	.uleb128 0x13
 6979 00cb 0B          	.uleb128 0xb
 6980 00cc 0B          	.uleb128 0xb
 6981 00cd 0D          	.uleb128 0xd
 6982 00ce 0B          	.uleb128 0xb
 6983 00cf 0C          	.uleb128 0xc
 6984 00d0 0B          	.uleb128 0xb
 6985 00d1 38          	.uleb128 0x38
 6986 00d2 0A          	.uleb128 0xa
 6987 00d3 00          	.byte 0x0
 6988 00d4 00          	.byte 0x0
 6989 00d5 10          	.uleb128 0x10
 6990 00d6 0D          	.uleb128 0xd
 6991 00d7 00          	.byte 0x0
 6992 00d8 03          	.uleb128 0x3
 6993 00d9 0E          	.uleb128 0xe
 6994 00da 3A          	.uleb128 0x3a
 6995 00db 0B          	.uleb128 0xb
 6996 00dc 3B          	.uleb128 0x3b
 6997 00dd 0B          	.uleb128 0xb
 6998 00de 49          	.uleb128 0x49
 6999 00df 13          	.uleb128 0x13
 7000 00e0 38          	.uleb128 0x38
 7001 00e1 0A          	.uleb128 0xa
 7002 00e2 00          	.byte 0x0
 7003 00e3 00          	.byte 0x0
 7004 00e4 11          	.uleb128 0x11
 7005 00e5 2E          	.uleb128 0x2e
 7006 00e6 00          	.byte 0x0
 7007 00e7 3F          	.uleb128 0x3f
 7008 00e8 0C          	.uleb128 0xc
 7009 00e9 03          	.uleb128 0x3
 7010 00ea 08          	.uleb128 0x8
 7011 00eb 3A          	.uleb128 0x3a
 7012 00ec 0B          	.uleb128 0xb
MPLAB XC16 ASSEMBLY Listing:   			page 145


 7013 00ed 3B          	.uleb128 0x3b
 7014 00ee 05          	.uleb128 0x5
 7015 00ef 27          	.uleb128 0x27
 7016 00f0 0C          	.uleb128 0xc
 7017 00f1 49          	.uleb128 0x49
 7018 00f2 13          	.uleb128 0x13
 7019 00f3 20          	.uleb128 0x20
 7020 00f4 0B          	.uleb128 0xb
 7021 00f5 00          	.byte 0x0
 7022 00f6 00          	.byte 0x0
 7023 00f7 12          	.uleb128 0x12
 7024 00f8 2E          	.uleb128 0x2e
 7025 00f9 01          	.byte 0x1
 7026 00fa 3F          	.uleb128 0x3f
 7027 00fb 0C          	.uleb128 0xc
 7028 00fc 03          	.uleb128 0x3
 7029 00fd 08          	.uleb128 0x8
 7030 00fe 3A          	.uleb128 0x3a
 7031 00ff 0B          	.uleb128 0xb
 7032 0100 3B          	.uleb128 0x3b
 7033 0101 0B          	.uleb128 0xb
 7034 0102 27          	.uleb128 0x27
 7035 0103 0C          	.uleb128 0xc
 7036 0104 49          	.uleb128 0x49
 7037 0105 13          	.uleb128 0x13
 7038 0106 11          	.uleb128 0x11
 7039 0107 01          	.uleb128 0x1
 7040 0108 12          	.uleb128 0x12
 7041 0109 01          	.uleb128 0x1
 7042 010a 40          	.uleb128 0x40
 7043 010b 0A          	.uleb128 0xa
 7044 010c 01          	.uleb128 0x1
 7045 010d 13          	.uleb128 0x13
 7046 010e 00          	.byte 0x0
 7047 010f 00          	.byte 0x0
 7048 0110 13          	.uleb128 0x13
 7049 0111 05          	.uleb128 0x5
 7050 0112 00          	.byte 0x0
 7051 0113 03          	.uleb128 0x3
 7052 0114 0E          	.uleb128 0xe
 7053 0115 3A          	.uleb128 0x3a
 7054 0116 0B          	.uleb128 0xb
 7055 0117 3B          	.uleb128 0x3b
 7056 0118 0B          	.uleb128 0xb
 7057 0119 49          	.uleb128 0x49
 7058 011a 13          	.uleb128 0x13
 7059 011b 02          	.uleb128 0x2
 7060 011c 0A          	.uleb128 0xa
 7061 011d 00          	.byte 0x0
 7062 011e 00          	.byte 0x0
 7063 011f 14          	.uleb128 0x14
 7064 0120 34          	.uleb128 0x34
 7065 0121 00          	.byte 0x0
 7066 0122 03          	.uleb128 0x3
 7067 0123 08          	.uleb128 0x8
 7068 0124 3A          	.uleb128 0x3a
 7069 0125 0B          	.uleb128 0xb
MPLAB XC16 ASSEMBLY Listing:   			page 146


 7070 0126 3B          	.uleb128 0x3b
 7071 0127 0B          	.uleb128 0xb
 7072 0128 49          	.uleb128 0x49
 7073 0129 13          	.uleb128 0x13
 7074 012a 02          	.uleb128 0x2
 7075 012b 0A          	.uleb128 0xa
 7076 012c 00          	.byte 0x0
 7077 012d 00          	.byte 0x0
 7078 012e 15          	.uleb128 0x15
 7079 012f 0F          	.uleb128 0xf
 7080 0130 00          	.byte 0x0
 7081 0131 0B          	.uleb128 0xb
 7082 0132 0B          	.uleb128 0xb
 7083 0133 49          	.uleb128 0x49
 7084 0134 13          	.uleb128 0x13
 7085 0135 00          	.byte 0x0
 7086 0136 00          	.byte 0x0
 7087 0137 16          	.uleb128 0x16
 7088 0138 01          	.uleb128 0x1
 7089 0139 01          	.byte 0x1
 7090 013a 49          	.uleb128 0x49
 7091 013b 13          	.uleb128 0x13
 7092 013c 01          	.uleb128 0x1
 7093 013d 13          	.uleb128 0x13
 7094 013e 00          	.byte 0x0
 7095 013f 00          	.byte 0x0
 7096 0140 17          	.uleb128 0x17
 7097 0141 21          	.uleb128 0x21
 7098 0142 00          	.byte 0x0
 7099 0143 49          	.uleb128 0x49
 7100 0144 13          	.uleb128 0x13
 7101 0145 2F          	.uleb128 0x2f
 7102 0146 0B          	.uleb128 0xb
 7103 0147 00          	.byte 0x0
 7104 0148 00          	.byte 0x0
 7105 0149 18          	.uleb128 0x18
 7106 014a 26          	.uleb128 0x26
 7107 014b 00          	.byte 0x0
 7108 014c 49          	.uleb128 0x49
 7109 014d 13          	.uleb128 0x13
 7110 014e 00          	.byte 0x0
 7111 014f 00          	.byte 0x0
 7112 0150 19          	.uleb128 0x19
 7113 0151 34          	.uleb128 0x34
 7114 0152 00          	.byte 0x0
 7115 0153 03          	.uleb128 0x3
 7116 0154 0E          	.uleb128 0xe
 7117 0155 3A          	.uleb128 0x3a
 7118 0156 0B          	.uleb128 0xb
 7119 0157 3B          	.uleb128 0x3b
 7120 0158 0B          	.uleb128 0xb
 7121 0159 49          	.uleb128 0x49
 7122 015a 13          	.uleb128 0x13
 7123 015b 02          	.uleb128 0x2
 7124 015c 0A          	.uleb128 0xa
 7125 015d 00          	.byte 0x0
 7126 015e 00          	.byte 0x0
MPLAB XC16 ASSEMBLY Listing:   			page 147


 7127 015f 1A          	.uleb128 0x1a
 7128 0160 2E          	.uleb128 0x2e
 7129 0161 00          	.byte 0x0
 7130 0162 3F          	.uleb128 0x3f
 7131 0163 0C          	.uleb128 0xc
 7132 0164 03          	.uleb128 0x3
 7133 0165 08          	.uleb128 0x8
 7134 0166 3A          	.uleb128 0x3a
 7135 0167 0B          	.uleb128 0xb
 7136 0168 3B          	.uleb128 0x3b
 7137 0169 0B          	.uleb128 0xb
 7138 016a 11          	.uleb128 0x11
 7139 016b 01          	.uleb128 0x1
 7140 016c 12          	.uleb128 0x12
 7141 016d 01          	.uleb128 0x1
 7142 016e 40          	.uleb128 0x40
 7143 016f 0A          	.uleb128 0xa
 7144 0170 00          	.byte 0x0
 7145 0171 00          	.byte 0x0
 7146 0172 1B          	.uleb128 0x1b
 7147 0173 2E          	.uleb128 0x2e
 7148 0174 00          	.byte 0x0
 7149 0175 3F          	.uleb128 0x3f
 7150 0176 0C          	.uleb128 0xc
 7151 0177 03          	.uleb128 0x3
 7152 0178 08          	.uleb128 0x8
 7153 0179 3A          	.uleb128 0x3a
 7154 017a 0B          	.uleb128 0xb
 7155 017b 3B          	.uleb128 0x3b
 7156 017c 05          	.uleb128 0x5
 7157 017d 11          	.uleb128 0x11
 7158 017e 01          	.uleb128 0x1
 7159 017f 12          	.uleb128 0x12
 7160 0180 01          	.uleb128 0x1
 7161 0181 40          	.uleb128 0x40
 7162 0182 0A          	.uleb128 0xa
 7163 0183 00          	.byte 0x0
 7164 0184 00          	.byte 0x0
 7165 0185 1C          	.uleb128 0x1c
 7166 0186 2E          	.uleb128 0x2e
 7167 0187 01          	.byte 0x1
 7168 0188 3F          	.uleb128 0x3f
 7169 0189 0C          	.uleb128 0xc
 7170 018a 03          	.uleb128 0x3
 7171 018b 08          	.uleb128 0x8
 7172 018c 3A          	.uleb128 0x3a
 7173 018d 0B          	.uleb128 0xb
 7174 018e 3B          	.uleb128 0x3b
 7175 018f 05          	.uleb128 0x5
 7176 0190 27          	.uleb128 0x27
 7177 0191 0C          	.uleb128 0xc
 7178 0192 11          	.uleb128 0x11
 7179 0193 01          	.uleb128 0x1
 7180 0194 12          	.uleb128 0x12
 7181 0195 01          	.uleb128 0x1
 7182 0196 40          	.uleb128 0x40
 7183 0197 0A          	.uleb128 0xa
MPLAB XC16 ASSEMBLY Listing:   			page 148


 7184 0198 01          	.uleb128 0x1
 7185 0199 13          	.uleb128 0x13
 7186 019a 00          	.byte 0x0
 7187 019b 00          	.byte 0x0
 7188 019c 1D          	.uleb128 0x1d
 7189 019d 05          	.uleb128 0x5
 7190 019e 00          	.byte 0x0
 7191 019f 03          	.uleb128 0x3
 7192 01a0 0E          	.uleb128 0xe
 7193 01a1 3A          	.uleb128 0x3a
 7194 01a2 0B          	.uleb128 0xb
 7195 01a3 3B          	.uleb128 0x3b
 7196 01a4 05          	.uleb128 0x5
 7197 01a5 49          	.uleb128 0x49
 7198 01a6 13          	.uleb128 0x13
 7199 01a7 02          	.uleb128 0x2
 7200 01a8 0A          	.uleb128 0xa
 7201 01a9 00          	.byte 0x0
 7202 01aa 00          	.byte 0x0
 7203 01ab 1E          	.uleb128 0x1e
 7204 01ac 1D          	.uleb128 0x1d
 7205 01ad 00          	.byte 0x0
 7206 01ae 31          	.uleb128 0x31
 7207 01af 13          	.uleb128 0x13
 7208 01b0 11          	.uleb128 0x11
 7209 01b1 01          	.uleb128 0x1
 7210 01b2 12          	.uleb128 0x12
 7211 01b3 01          	.uleb128 0x1
 7212 01b4 00          	.byte 0x0
 7213 01b5 00          	.byte 0x0
 7214 01b6 1F          	.uleb128 0x1f
 7215 01b7 2E          	.uleb128 0x2e
 7216 01b8 01          	.byte 0x1
 7217 01b9 3F          	.uleb128 0x3f
 7218 01ba 0C          	.uleb128 0xc
 7219 01bb 03          	.uleb128 0x3
 7220 01bc 08          	.uleb128 0x8
 7221 01bd 3A          	.uleb128 0x3a
 7222 01be 0B          	.uleb128 0xb
 7223 01bf 3B          	.uleb128 0x3b
 7224 01c0 05          	.uleb128 0x5
 7225 01c1 27          	.uleb128 0x27
 7226 01c2 0C          	.uleb128 0xc
 7227 01c3 49          	.uleb128 0x49
 7228 01c4 13          	.uleb128 0x13
 7229 01c5 11          	.uleb128 0x11
 7230 01c6 01          	.uleb128 0x1
 7231 01c7 12          	.uleb128 0x12
 7232 01c8 01          	.uleb128 0x1
 7233 01c9 40          	.uleb128 0x40
 7234 01ca 0A          	.uleb128 0xa
 7235 01cb 01          	.uleb128 0x1
 7236 01cc 13          	.uleb128 0x13
 7237 01cd 00          	.byte 0x0
 7238 01ce 00          	.byte 0x0
 7239 01cf 20          	.uleb128 0x20
 7240 01d0 34          	.uleb128 0x34
MPLAB XC16 ASSEMBLY Listing:   			page 149


 7241 01d1 00          	.byte 0x0
 7242 01d2 03          	.uleb128 0x3
 7243 01d3 0E          	.uleb128 0xe
 7244 01d4 3A          	.uleb128 0x3a
 7245 01d5 0B          	.uleb128 0xb
 7246 01d6 3B          	.uleb128 0x3b
 7247 01d7 05          	.uleb128 0x5
 7248 01d8 49          	.uleb128 0x49
 7249 01d9 13          	.uleb128 0x13
 7250 01da 02          	.uleb128 0x2
 7251 01db 0A          	.uleb128 0xa
 7252 01dc 00          	.byte 0x0
 7253 01dd 00          	.byte 0x0
 7254 01de 21          	.uleb128 0x21
 7255 01df 2E          	.uleb128 0x2e
 7256 01e0 00          	.byte 0x0
 7257 01e1 31          	.uleb128 0x31
 7258 01e2 13          	.uleb128 0x13
 7259 01e3 11          	.uleb128 0x11
 7260 01e4 01          	.uleb128 0x1
 7261 01e5 12          	.uleb128 0x12
 7262 01e6 01          	.uleb128 0x1
 7263 01e7 40          	.uleb128 0x40
 7264 01e8 0A          	.uleb128 0xa
 7265 01e9 00          	.byte 0x0
 7266 01ea 00          	.byte 0x0
 7267 01eb 22          	.uleb128 0x22
 7268 01ec 34          	.uleb128 0x34
 7269 01ed 00          	.byte 0x0
 7270 01ee 03          	.uleb128 0x3
 7271 01ef 0E          	.uleb128 0xe
 7272 01f0 3A          	.uleb128 0x3a
 7273 01f1 0B          	.uleb128 0xb
 7274 01f2 3B          	.uleb128 0x3b
 7275 01f3 05          	.uleb128 0x5
 7276 01f4 49          	.uleb128 0x49
 7277 01f5 13          	.uleb128 0x13
 7278 01f6 3F          	.uleb128 0x3f
 7279 01f7 0C          	.uleb128 0xc
 7280 01f8 3C          	.uleb128 0x3c
 7281 01f9 0C          	.uleb128 0xc
 7282 01fa 00          	.byte 0x0
 7283 01fb 00          	.byte 0x0
 7284 01fc 23          	.uleb128 0x23
 7285 01fd 35          	.uleb128 0x35
 7286 01fe 00          	.byte 0x0
 7287 01ff 49          	.uleb128 0x49
 7288 0200 13          	.uleb128 0x13
 7289 0201 00          	.byte 0x0
 7290 0202 00          	.byte 0x0
 7291 0203 24          	.uleb128 0x24
 7292 0204 34          	.uleb128 0x34
 7293 0205 00          	.byte 0x0
 7294 0206 03          	.uleb128 0x3
 7295 0207 08          	.uleb128 0x8
 7296 0208 3A          	.uleb128 0x3a
 7297 0209 0B          	.uleb128 0xb
MPLAB XC16 ASSEMBLY Listing:   			page 150


 7298 020a 3B          	.uleb128 0x3b
 7299 020b 05          	.uleb128 0x5
 7300 020c 49          	.uleb128 0x49
 7301 020d 13          	.uleb128 0x13
 7302 020e 3F          	.uleb128 0x3f
 7303 020f 0C          	.uleb128 0xc
 7304 0210 3C          	.uleb128 0x3c
 7305 0211 0C          	.uleb128 0xc
 7306 0212 00          	.byte 0x0
 7307 0213 00          	.byte 0x0
 7308 0214 00          	.byte 0x0
 7309                 	.section .debug_pubnames,info
 7310 0000 C8 01 00 00 	.4byte 0x1c8
 7311 0004 02 00       	.2byte 0x2
 7312 0006 00 00 00 00 	.4byte .Ldebug_info0
 7313 000a B5 1C 00 00 	.4byte 0x1cb5
 7314 000e DC 11 00 00 	.4byte 0x11dc
 7315 0012 67 65 74 5F 	.asciz "get_prescales"
 7315      70 72 65 73 
 7315      63 61 6C 65 
 7315      73 00 
 7316 0020 01 13 00 00 	.4byte 0x1301
 7317 0024 63 6F 6E 66 	.asciz "config_SPI1"
 7317      69 67 5F 53 
 7317      50 49 31 00 
 7318 0030 5D 13 00 00 	.4byte 0x135d
 7319 0034 63 6F 6E 66 	.asciz "config_SPI2"
 7319      69 67 5F 53 
 7319      50 49 32 00 
 7320 0040 B9 13 00 00 	.4byte 0x13b9
 7321 0044 63 6F 6E 66 	.asciz "config_SPI3"
 7321      69 67 5F 53 
 7321      50 49 33 00 
 7322 0050 15 14 00 00 	.4byte 0x1415
 7323 0054 63 6F 6E 66 	.asciz "config_SPI4"
 7323      69 67 5F 53 
 7323      50 49 34 00 
 7324 0060 71 14 00 00 	.4byte 0x1471
 7325 0064 53 50 49 31 	.asciz "SPI1_enable_module"
 7325      5F 65 6E 61 
 7325      62 6C 65 5F 
 7325      6D 6F 64 75 
 7325      6C 65 00 
 7326 0077 92 14 00 00 	.4byte 0x1492
 7327 007b 53 50 49 32 	.asciz "SPI2_enable_module"
 7327      5F 65 6E 61 
 7327      62 6C 65 5F 
 7327      6D 6F 64 75 
 7327      6C 65 00 
 7328 008e B3 14 00 00 	.4byte 0x14b3
 7329 0092 53 50 49 33 	.asciz "SPI3_enable_module"
 7329      5F 65 6E 61 
 7329      62 6C 65 5F 
 7329      6D 6F 64 75 
 7329      6C 65 00 
 7330 00a5 D5 14 00 00 	.4byte 0x14d5
 7331 00a9 53 50 49 34 	.asciz "SPI4_enable_module"
MPLAB XC16 ASSEMBLY Listing:   			page 151


 7331      5F 65 6E 61 
 7331      62 6C 65 5F 
 7331      6D 6F 64 75 
 7331      6C 65 00 
 7332 00bc F7 14 00 00 	.4byte 0x14f7
 7333 00c0 53 50 49 31 	.asciz "SPI1_Exchange"
 7333      5F 45 78 63 
 7333      68 61 6E 67 
 7333      65 00 
 7334 00ce 52 15 00 00 	.4byte 0x1552
 7335 00d2 53 50 49 32 	.asciz "SPI2_Exchange"
 7335      5F 45 78 63 
 7335      68 61 6E 67 
 7335      65 00 
 7336 00e0 AD 15 00 00 	.4byte 0x15ad
 7337 00e4 53 50 49 33 	.asciz "SPI3_Exchange"
 7337      5F 45 78 63 
 7337      68 61 6E 67 
 7337      65 00 
 7338 00f2 08 16 00 00 	.4byte 0x1608
 7339 00f6 53 50 49 34 	.asciz "SPI4_Exchange"
 7339      5F 45 78 63 
 7339      68 61 6E 67 
 7339      65 00 
 7340 0104 63 16 00 00 	.4byte 0x1663
 7341 0108 53 50 49 31 	.asciz "SPI1_ExchangeBuffer"
 7341      5F 45 78 63 
 7341      68 61 6E 67 
 7341      65 42 75 66 
 7341      66 65 72 00 
 7342 011c 60 17 00 00 	.4byte 0x1760
 7343 0120 53 50 49 32 	.asciz "SPI2_ExchangeBuffer"
 7343      5F 45 78 63 
 7343      68 61 6E 67 
 7343      65 42 75 66 
 7343      66 65 72 00 
 7344 0134 5D 18 00 00 	.4byte 0x185d
 7345 0138 53 50 49 33 	.asciz "SPI3_ExchangeBuffer"
 7345      5F 45 78 63 
 7345      68 61 6E 67 
 7345      65 42 75 66 
 7345      66 65 72 00 
 7346 014c 5A 19 00 00 	.4byte 0x195a
 7347 0150 53 50 49 34 	.asciz "SPI4_ExchangeBuffer"
 7347      5F 45 78 63 
 7347      68 61 6E 67 
 7347      65 42 75 66 
 7347      66 65 72 00 
 7348 0164 57 1A 00 00 	.4byte 0x1a57
 7349 0168 53 50 49 31 	.asciz "SPI1_TransferModeGet"
 7349      5F 54 72 61 
 7349      6E 73 66 65 
 7349      72 4D 6F 64 
 7349      65 47 65 74 
 7349      00 
 7350 017d 66 1A 00 00 	.4byte 0x1a66
 7351 0181 53 50 49 32 	.asciz "SPI2_TransferModeGet"
MPLAB XC16 ASSEMBLY Listing:   			page 152


 7351      5F 54 72 61 
 7351      6E 73 66 65 
 7351      72 4D 6F 64 
 7351      65 47 65 74 
 7351      00 
 7352 0196 75 1A 00 00 	.4byte 0x1a75
 7353 019a 53 50 49 33 	.asciz "SPI3_TransferModeGet"
 7353      5F 54 72 61 
 7353      6E 73 66 65 
 7353      72 4D 6F 64 
 7353      65 47 65 74 
 7353      00 
 7354 01af 84 1A 00 00 	.4byte 0x1a84
 7355 01b3 53 50 49 34 	.asciz "SPI4_TransferModeGet"
 7355      5F 54 72 61 
 7355      6E 73 66 65 
 7355      72 4D 6F 64 
 7355      65 47 65 74 
 7355      00 
 7356 01c8 00 00 00 00 	.4byte 0x0
 7357                 	.section .debug_pubtypes,info
 7358 0000 6F 02 00 00 	.4byte 0x26f
 7359 0004 02 00       	.2byte 0x2
 7360 0006 00 00 00 00 	.4byte .Ldebug_info0
 7361 000a B5 1C 00 00 	.4byte 0x1cb5
 7362 000e D7 00 00 00 	.4byte 0xd7
 7363 0012 75 69 6E 74 	.asciz "uint8_t"
 7363      38 5F 74 00 
 7364 001a F7 00 00 00 	.4byte 0xf7
 7365 001e 75 69 6E 74 	.asciz "uint16_t"
 7365      31 36 5F 74 
 7365      00 
 7366 0027 17 01 00 00 	.4byte 0x117
 7367 002b 75 69 6E 74 	.asciz "uint32_t"
 7367      33 32 5F 74 
 7367      00 
 7368 0034 8C 02 00 00 	.4byte 0x28c
 7369 0038 74 61 67 53 	.asciz "tagSPI1STATBITS"
 7369      50 49 31 53 
 7369      54 41 54 42 
 7369      49 54 53 00 
 7370 0048 AE 02 00 00 	.4byte 0x2ae
 7371 004c 53 50 49 31 	.asciz "SPI1STATBITS"
 7371      53 54 41 54 
 7371      42 49 54 53 
 7371      00 
 7372 0059 FC 03 00 00 	.4byte 0x3fc
 7373 005d 74 61 67 53 	.asciz "tagSPI1CON1BITS"
 7373      50 49 31 43 
 7373      4F 4E 31 42 
 7373      49 54 53 00 
 7374 006d 1E 04 00 00 	.4byte 0x41e
 7375 0071 53 50 49 31 	.asciz "SPI1CON1BITS"
 7375      43 4F 4E 31 
 7375      42 49 54 53 
 7375      00 
 7376 007e 33 04 00 00 	.4byte 0x433
MPLAB XC16 ASSEMBLY Listing:   			page 153


 7377 0082 74 61 67 53 	.asciz "tagSPI1CON2BITS"
 7377      50 49 31 43 
 7377      4F 4E 32 42 
 7377      49 54 53 00 
 7378 0092 A7 04 00 00 	.4byte 0x4a7
 7379 0096 53 50 49 31 	.asciz "SPI1CON2BITS"
 7379      43 4F 4E 32 
 7379      42 49 54 53 
 7379      00 
 7380 00a3 F2 05 00 00 	.4byte 0x5f2
 7381 00a7 74 61 67 53 	.asciz "tagSPI2STATBITS"
 7381      50 49 32 53 
 7381      54 41 54 42 
 7381      49 54 53 00 
 7382 00b7 14 06 00 00 	.4byte 0x614
 7383 00bb 53 50 49 32 	.asciz "SPI2STATBITS"
 7383      53 54 41 54 
 7383      42 49 54 53 
 7383      00 
 7384 00c8 62 07 00 00 	.4byte 0x762
 7385 00cc 74 61 67 53 	.asciz "tagSPI2CON1BITS"
 7385      50 49 32 43 
 7385      4F 4E 31 42 
 7385      49 54 53 00 
 7386 00dc 84 07 00 00 	.4byte 0x784
 7387 00e0 53 50 49 32 	.asciz "SPI2CON1BITS"
 7387      43 4F 4E 31 
 7387      42 49 54 53 
 7387      00 
 7388 00ed 99 07 00 00 	.4byte 0x799
 7389 00f1 74 61 67 53 	.asciz "tagSPI2CON2BITS"
 7389      50 49 32 43 
 7389      4F 4E 32 42 
 7389      49 54 53 00 
 7390 0101 0D 08 00 00 	.4byte 0x80d
 7391 0105 53 50 49 32 	.asciz "SPI2CON2BITS"
 7391      43 4F 4E 32 
 7391      42 49 54 53 
 7391      00 
 7392 0112 58 09 00 00 	.4byte 0x958
 7393 0116 74 61 67 53 	.asciz "tagSPI3STATBITS"
 7393      50 49 33 53 
 7393      54 41 54 42 
 7393      49 54 53 00 
 7394 0126 7A 09 00 00 	.4byte 0x97a
 7395 012a 53 50 49 33 	.asciz "SPI3STATBITS"
 7395      53 54 41 54 
 7395      42 49 54 53 
 7395      00 
 7396 0137 C8 0A 00 00 	.4byte 0xac8
 7397 013b 74 61 67 53 	.asciz "tagSPI3CON1BITS"
 7397      50 49 33 43 
 7397      4F 4E 31 42 
 7397      49 54 53 00 
 7398 014b EA 0A 00 00 	.4byte 0xaea
 7399 014f 53 50 49 33 	.asciz "SPI3CON1BITS"
 7399      43 4F 4E 31 
MPLAB XC16 ASSEMBLY Listing:   			page 154


 7399      42 49 54 53 
 7399      00 
 7400 015c FF 0A 00 00 	.4byte 0xaff
 7401 0160 74 61 67 53 	.asciz "tagSPI3CON2BITS"
 7401      50 49 33 43 
 7401      4F 4E 32 42 
 7401      49 54 53 00 
 7402 0170 73 0B 00 00 	.4byte 0xb73
 7403 0174 53 50 49 33 	.asciz "SPI3CON2BITS"
 7403      43 4F 4E 32 
 7403      42 49 54 53 
 7403      00 
 7404 0181 BE 0C 00 00 	.4byte 0xcbe
 7405 0185 74 61 67 53 	.asciz "tagSPI4STATBITS"
 7405      50 49 34 53 
 7405      54 41 54 42 
 7405      49 54 53 00 
 7406 0195 E0 0C 00 00 	.4byte 0xce0
 7407 0199 53 50 49 34 	.asciz "SPI4STATBITS"
 7407      53 54 41 54 
 7407      42 49 54 53 
 7407      00 
 7408 01a6 2E 0E 00 00 	.4byte 0xe2e
 7409 01aa 74 61 67 53 	.asciz "tagSPI4CON1BITS"
 7409      50 49 34 43 
 7409      4F 4E 31 42 
 7409      49 54 53 00 
 7410 01ba 50 0E 00 00 	.4byte 0xe50
 7411 01be 53 50 49 34 	.asciz "SPI4CON1BITS"
 7411      43 4F 4E 31 
 7411      42 49 54 53 
 7411      00 
 7412 01cb 65 0E 00 00 	.4byte 0xe65
 7413 01cf 74 61 67 53 	.asciz "tagSPI4CON2BITS"
 7413      50 49 34 43 
 7413      4F 4E 32 42 
 7413      49 54 53 00 
 7414 01df D9 0E 00 00 	.4byte 0xed9
 7415 01e3 53 50 49 34 	.asciz "SPI4CON2BITS"
 7415      43 4F 4E 32 
 7415      42 49 54 53 
 7415      00 
 7416 01f0 40 0F 00 00 	.4byte 0xf40
 7417 01f4 53 50 49 5F 	.asciz "SPI_TRANSFER_MODE"
 7417      54 52 41 4E 
 7417      53 46 45 52 
 7417      5F 4D 4F 44 
 7417      45 00 
 7418 0206 9D 0F 00 00 	.4byte 0xf9d
 7419 020a 53 50 49 5F 	.asciz "SPI_DATA_SAMPLE_PHASE"
 7419      44 41 54 41 
 7419      5F 53 41 4D 
 7419      50 4C 45 5F 
 7419      50 48 41 53 
 7419      45 00 
 7420 0220 F7 0F 00 00 	.4byte 0xff7
 7421 0224 53 50 49 5F 	.asciz "SPI_MODE"
MPLAB XC16 ASSEMBLY Listing:   			page 155


 7421      4D 4F 44 45 
 7421      00 
 7422 022d 07 10 00 00 	.4byte 0x1007
 7423 0231 53 50 49 5F 	.asciz "SPI_PERIPHERAL_CONFIGURATION"
 7423      50 45 52 49 
 7423      50 48 45 52 
 7423      41 4C 5F 43 
 7423      4F 4E 46 49 
 7423      47 55 52 41 
 7423      54 49 4F 4E 
 7423      00 
 7424 024e 38 11 00 00 	.4byte 0x1138
 7425 0252 53 50 49 5F 	.asciz "SPI_Peripheral_Configuration"
 7425      50 65 72 69 
 7425      70 68 65 72 
 7425      61 6C 5F 43 
 7425      6F 6E 66 69 
 7425      67 75 72 61 
 7425      74 69 6F 6E 
 7425      00 
 7426 026f 00 00 00 00 	.4byte 0x0
 7427                 	.section .debug_aranges,info
 7428 0000 14 00 00 00 	.4byte 0x14
 7429 0004 02 00       	.2byte 0x2
 7430 0006 00 00 00 00 	.4byte .Ldebug_info0
 7431 000a 04          	.byte 0x4
 7432 000b 00          	.byte 0x0
 7433 000c 00 00       	.2byte 0x0
 7434 000e 00 00       	.2byte 0x0
 7435 0010 00 00 00 00 	.4byte 0x0
 7436 0014 00 00 00 00 	.4byte 0x0
 7437                 	.section .debug_str,info
 7438                 	.LASF21:
 7439 0000 53 50 52 45 	.asciz "SPRE0"
 7439      30 00 
 7440                 	.LASF22:
 7441 0006 53 50 52 45 	.asciz "SPRE1"
 7441      31 00 
 7442                 	.LASF23:
 7443 000c 53 50 52 45 	.asciz "SPRE2"
 7443      32 00 
 7444                 	.LASF36:
 7445 0012 64 61 74 61 	.asciz "dataSentCount"
 7445      53 65 6E 74 
 7445      43 6F 75 6E 
 7445      74 00 
 7446                 	.LASF48:
 7447 0020 53 50 49 31 	.asciz "SPI1CON2bits"
 7447      43 4F 4E 32 
 7447      62 69 74 73 
 7447      00 
 7448                 	.LASF46:
 7449 002d 53 50 49 31 	.asciz "SPI1STATbits"
 7449      53 54 41 54 
 7449      62 69 74 73 
 7449      00 
 7450                 	.LASF3:
MPLAB XC16 ASSEMBLY Listing:   			page 156


 7451 003a 53 52 58 4D 	.asciz "SRXMPT"
 7451      50 54 00 
 7452                 	.LASF45:
 7453 0041 73 70 69 4D 	.asciz "spiModeStatus"
 7453      6F 64 65 53 
 7453      74 61 74 75 
 7453      73 00 
 7454                 	.LASF54:
 7455 004f 53 50 49 33 	.asciz "SPI3CON2bits"
 7455      43 4F 4E 32 
 7455      62 69 74 73 
 7455      00 
 7456                 	.LASF0:
 7457 005c 53 50 49 52 	.asciz "SPIRBF"
 7457      42 46 00 
 7458                 	.LASF57:
 7459 0063 53 50 49 34 	.asciz "SPI4CON2bits"
 7459      43 4F 4E 32 
 7459      62 69 74 73 
 7459      00 
 7460                 	.LASF8:
 7461 0070 53 50 49 45 	.asciz "SPIEN"
 7461      4E 00 
 7462                 	.LASF43:
 7463 0076 72 65 63 65 	.asciz "receiveAddressIncrement"
 7463      69 76 65 41 
 7463      64 64 72 65 
 7463      73 73 49 6E 
 7463      63 72 65 6D 
 7463      65 6E 74 00 
 7464                 	.LASF5:
 7465 008e 53 52 4D 50 	.asciz "SRMPT"
 7465      54 00 
 7466                 	.LASF26:
 7467 0094 46 52 4D 50 	.asciz "FRMPOL"
 7467      4F 4C 00 
 7468                 	.LASF32:
 7469 009b 63 6F 6E 66 	.asciz "config"
 7469      69 67 00 
 7470                 	.LASF41:
 7471 00a2 70 52 65 63 	.asciz "pReceived"
 7471      65 69 76 65 
 7471      64 00 
 7472                 	.LASF51:
 7473 00ac 53 50 49 32 	.asciz "SPI2CON2bits"
 7473      43 4F 4E 32 
 7473      62 69 74 73 
 7473      00 
 7474                 	.LASF12:
 7475 00b9 53 50 49 42 	.asciz "SPIBEC0"
 7475      45 43 30 00 
 7476                 	.LASF13:
 7477 00c1 53 50 49 42 	.asciz "SPIBEC1"
 7477      45 43 31 00 
 7478                 	.LASF30:
 7479 00c9 70 5F 70 72 	.asciz "p_presc"
 7479      65 73 63 00 
MPLAB XC16 ASSEMBLY Listing:   			page 157


 7480                 	.LASF9:
 7481 00d1 53 49 53 45 	.asciz "SISEL0"
 7481      4C 30 00 
 7482                 	.LASF10:
 7483 00d8 53 49 53 45 	.asciz "SISEL1"
 7483      4C 31 00 
 7484                 	.LASF11:
 7485 00df 53 49 53 45 	.asciz "SISEL2"
 7485      4C 32 00 
 7486                 	.LASF42:
 7487 00e6 61 64 64 72 	.asciz "addressIncrement"
 7487      65 73 73 49 
 7487      6E 63 72 65 
 7487      6D 65 6E 74 
 7487      00 
 7488                 	.LASF16:
 7489 00f7 4D 4F 44 45 	.asciz "MODE16"
 7489      31 36 00 
 7490                 	.LASF29:
 7491 00fe 62 61 75 64 	.asciz "baudrate"
 7491      72 61 74 65 
 7491      00 
 7492                 	.LASF38:
 7493 0107 64 75 6D 6D 	.asciz "dummyDataReceived"
 7493      79 44 61 74 
 7493      61 52 65 63 
 7493      65 69 76 65 
 7493      64 00 
 7494                 	.LASF25:
 7495 0119 46 52 4D 44 	.asciz "FRMDLY"
 7495      4C 59 00 
 7496                 	.LASF35:
 7497 0120 62 79 74 65 	.asciz "byteCount"
 7497      43 6F 75 6E 
 7497      74 00 
 7498                 	.LASF18:
 7499 012a 44 49 53 53 	.asciz "DISSCK"
 7499      43 4B 00 
 7500                 	.LASF15:
 7501 0131 4D 53 54 45 	.asciz "MSTEN"
 7501      4E 00 
 7502                 	.LASF44:
 7503 0137 73 65 6E 64 	.asciz "sendAddressIncrement"
 7503      41 64 64 72 
 7503      65 73 73 49 
 7503      6E 63 72 65 
 7503      6D 65 6E 74 
 7503      00 
 7504                 	.LASF7:
 7505 014c 53 50 49 53 	.asciz "SPISIDL"
 7505      49 44 4C 00 
 7506                 	.LASF39:
 7507 0154 64 75 6D 6D 	.asciz "dummyDataTransmit"
 7507      79 44 61 74 
 7507      61 54 72 61 
 7507      6E 73 6D 69 
 7507      74 00 
MPLAB XC16 ASSEMBLY Listing:   			page 158


 7508                 	.LASF27:
 7509 0166 53 50 49 46 	.asciz "SPIFSD"
 7509      53 44 00 
 7510                 	.LASF40:
 7511 016d 70 53 65 6E 	.asciz "pSend"
 7511      64 00 
 7512                 	.LASF28:
 7513 0173 46 52 4D 45 	.asciz "FRMEN"
 7513      4E 00 
 7514                 	.LASF4:
 7515 0179 53 50 49 52 	.asciz "SPIROV"
 7515      4F 56 00 
 7516                 	.LASF33:
 7517 0180 70 54 72 61 	.asciz "pTransmitData"
 7517      6E 73 6D 69 
 7517      74 44 61 74 
 7517      61 00 
 7518                 	.LASF47:
 7519 018e 53 50 49 31 	.asciz "SPI1CON1bits"
 7519      43 4F 4E 31 
 7519      62 69 74 73 
 7519      00 
 7520                 	.LASF50:
 7521 019b 53 50 49 32 	.asciz "SPI2CON1bits"
 7521      43 4F 4E 31 
 7521      62 69 74 73 
 7521      00 
 7522                 	.LASF2:
 7523 01a8 53 49 53 45 	.asciz "SISEL"
 7523      4C 00 
 7524                 	.LASF53:
 7525 01ae 53 50 49 33 	.asciz "SPI3CON1bits"
 7525      43 4F 4E 31 
 7525      62 69 74 73 
 7525      00 
 7526                 	.LASF6:
 7527 01bb 53 50 49 42 	.asciz "SPIBEC"
 7527      45 43 00 
 7528                 	.LASF14:
 7529 01c2 53 50 49 42 	.asciz "SPIBEC2"
 7529      45 43 32 00 
 7530                 	.LASF56:
 7531 01ca 53 50 49 34 	.asciz "SPI4CON1bits"
 7531      43 4F 4E 31 
 7531      62 69 74 73 
 7531      00 
 7532                 	.LASF37:
 7533 01d7 63 6F 75 6E 	.asciz "count"
 7533      74 00 
 7534                 	.LASF24:
 7535 01dd 53 50 49 42 	.asciz "SPIBEN"
 7535      45 4E 00 
 7536                 	.LASF17:
 7537 01e4 44 49 53 53 	.asciz "DISSDO"
 7537      44 4F 00 
 7538                 	.LASF52:
 7539 01eb 53 50 49 33 	.asciz "SPI3STATbits"
MPLAB XC16 ASSEMBLY Listing:   			page 159


 7539      53 54 41 54 
 7539      62 69 74 73 
 7539      00 
 7540                 	.LASF34:
 7541 01f8 70 52 65 63 	.asciz "pReceiveData"
 7541      65 69 76 65 
 7541      44 61 74 61 
 7541      00 
 7542                 	.LASF55:
 7543 0205 53 50 49 34 	.asciz "SPI4STATbits"
 7543      53 54 41 54 
 7543      62 69 74 73 
 7543      00 
 7544                 	.LASF1:
 7545 0212 53 50 49 54 	.asciz "SPITBF"
 7545      42 46 00 
 7546                 	.LASF49:
 7547 0219 53 50 49 32 	.asciz "SPI2STATbits"
 7547      53 54 41 54 
 7547      62 69 74 73 
 7547      00 
 7548                 	.LASF19:
 7549 0226 50 50 52 45 	.asciz "PPRE0"
 7549      30 00 
 7550                 	.LASF20:
 7551 022c 50 50 52 45 	.asciz "PPRE1"
 7551      31 00 
 7552                 	.LASF31:
 7553 0232 73 5F 70 72 	.asciz "s_presc"
 7553      65 73 63 00 
 7554                 	.section .text,code
 7555              	
 7556              	
 7557              	
 7558              	.section __c30_info,info,bss
 7559                 	__psv_trap_errata:
 7560                 	
 7561                 	.section __c30_signature,info,data
 7562 0000 01 00       	.word 0x0001
 7563 0002 00 00       	.word 0x0000
 7564 0004 00 00       	.word 0x0000
 7565                 	
 7566                 	
 7567                 	
 7568                 	.set ___PA___,0
 7569                 	.end
MPLAB XC16 ASSEMBLY Listing:   			page 160


DEFINED SYMBOLS
                            *ABS*:00000000 /home/user/Documents/FST/Programming/project_sub-zero/lib/lib_pic33e/SPI.c
                            *ABS*:00000001 __33EP256MU806
                            *ABS*:00000001 __dsPIC33E
                            *ABS*:00000001 __HAS_DSP
                            *ABS*:00000001 __HAS_DMAV2
                            *ABS*:00000001 __HAS_CODEGUARD
                            *ABS*:00000001 __HAS_PMP_ENHANCED
                            *ABS*:00000001 __HAS_EDS
                            *ABS*:00000001 __HAS_EP
                            *ABS*:00000011 __TARGET_DIVIDE_CYCLES
                            *ABS*:00000001 __C30ELF
    {standard input}:14     .text:00000000 _get_prescales
    {standard input}:18     *ABS*:00000000 ___PA___
    {standard input}:2950   .const:00000000 _C.68.18415
    {standard input}:2957   .const:00000004 _C.69.18416
    {standard input}:120    *ABS*:00000000 ___BP___
    {standard input}:202    .text:0000010a _config_SPI1
    {standard input}:457    .text:0000022e _config_SPI2
    {standard input}:712    .text:00000352 _config_SPI3
    {standard input}:967    .text:00000476 _config_SPI4
    {standard input}:1222   .text:0000059a _SPI1_enable_module
    {standard input}:1241   .text:000005a6 _SPI2_enable_module
    {standard input}:1260   .text:000005b2 _SPI3_enable_module
    {standard input}:1279   .text:000005be _SPI4_enable_module
    {standard input}:1298   .text:000005ca _SPI1_Exchange
    {standard input}:1400   .text:00000632 _SPI2_Exchange
    {standard input}:1502   .text:0000069a _SPI3_Exchange
    {standard input}:1604   .text:00000702 _SPI4_Exchange
    {standard input}:1706   .text:0000076a _SPI1_ExchangeBuffer
    {standard input}:1986   .text:00000898 _SPI2_ExchangeBuffer
    {standard input}:2266   .text:000009c6 _SPI3_ExchangeBuffer
    {standard input}:2546   .text:00000af4 _SPI4_ExchangeBuffer
    {standard input}:2826   .text:00000c22 _SPI1_TransferModeGet
    {standard input}:2857   .text:00000c3c _SPI2_TransferModeGet
    {standard input}:2888   .text:00000c56 _SPI3_TransferModeGet
    {standard input}:2919   .text:00000c70 _SPI4_TransferModeGet
                            *ABS*:00000008 __ext_attr_.const
    {standard input}:7559   __c30_info:00000000 __psv_trap_errata
    {standard input}:19     .text:00000000 .L0
                            .text:000000c4 .L2
                            .text:000000ba .L3
                            .text:0000009a .L4
                            .text:000000b2 .L5
                            .text:00000042 .L6
                            .text:0000003c .L7
                            .text:000005d0 .L17
                            .text:000005e6 .L18
                            .text:000005e8 .L19
                            .text:000005f4 .L20
                            .text:000005fc .L22
                            .text:00000614 .L23
                            .text:00000616 .L24
                            .text:00000622 .L25
                            .text:0000062a .L16
                            .text:00000638 .L28
                            .text:0000064e .L29
MPLAB XC16 ASSEMBLY Listing:   			page 161


                            .text:00000650 .L30
                            .text:0000065c .L31
                            .text:00000664 .L33
                            .text:0000067c .L34
                            .text:0000067e .L35
                            .text:0000068a .L36
                            .text:00000692 .L27
                            .text:000006a0 .L39
                            .text:000006b6 .L40
                            .text:000006b8 .L41
                            .text:000006c4 .L42
                            .text:000006cc .L44
                            .text:000006e4 .L45
                            .text:000006e6 .L46
                            .text:000006f2 .L47
                            .text:000006fa .L38
                            .text:00000708 .L50
                            .text:0000071e .L51
                            .text:00000720 .L52
                            .text:0000072c .L53
                            .text:00000734 .L55
                            .text:0000074c .L56
                            .text:0000074e .L57
                            .text:0000075a .L58
                            .text:00000762 .L49
                            .text:00000790 .L61
                            .text:00000792 .L62
                            .text:000007a6 .L63
                            .text:000007aa .L64
                            .text:000007ba .L65
                            .text:000007c2 .L66
                            .text:000007d2 .L67
                            .text:000007da .L69
                            .text:00000844 .L70
                            .text:00000810 .L71
                            .text:000007f8 .L72
                            .text:00000800 .L73
                            .text:00000828 .L74
                            .text:00000830 .L75
                            .text:000007e4 .L76
                            .text:00000886 .L77
                            .text:0000086a .L78
                            .text:00000872 .L79
                            .text:00000852 .L80
                            .text:000008be .L82
                            .text:000008c0 .L83
                            .text:000008d4 .L84
                            .text:000008d8 .L85
                            .text:000008e8 .L86
                            .text:000008f0 .L87
                            .text:00000900 .L88
                            .text:00000908 .L90
                            .text:00000972 .L91
                            .text:0000093e .L92
                            .text:00000926 .L93
                            .text:0000092e .L94
                            .text:00000956 .L95
MPLAB XC16 ASSEMBLY Listing:   			page 162


                            .text:0000095e .L96
                            .text:00000912 .L97
                            .text:000009b4 .L98
                            .text:00000998 .L99
                            .text:000009a0 .L100
                            .text:00000980 .L101
                            .text:000009ec .L103
                            .text:000009ee .L104
                            .text:00000a02 .L105
                            .text:00000a06 .L106
                            .text:00000a16 .L107
                            .text:00000a1e .L108
                            .text:00000a2e .L109
                            .text:00000a36 .L111
                            .text:00000aa0 .L112
                            .text:00000a6c .L113
                            .text:00000a54 .L114
                            .text:00000a5c .L115
                            .text:00000a84 .L116
                            .text:00000a8c .L117
                            .text:00000a40 .L118
                            .text:00000ae2 .L119
                            .text:00000ac6 .L120
                            .text:00000ace .L121
                            .text:00000aae .L122
                            .text:00000b1a .L124
                            .text:00000b1c .L125
                            .text:00000b30 .L126
                            .text:00000b34 .L127
                            .text:00000b44 .L128
                            .text:00000b4c .L129
                            .text:00000b5c .L130
                            .text:00000b64 .L132
                            .text:00000bce .L133
                            .text:00000b9a .L134
                            .text:00000b82 .L135
                            .text:00000b8a .L136
                            .text:00000bb2 .L137
                            .text:00000bba .L138
                            .text:00000b6e .L139
                            .text:00000c10 .L140
                            .text:00000bf4 .L141
                            .text:00000bfc .L142
                            .text:00000bdc .L143
                            .text:00000c32 .L145
                            .text:00000c34 .L146
                            .text:00000c4c .L148
                            .text:00000c4e .L149
                            .text:00000c66 .L151
                            .text:00000c68 .L152
                            .text:00000c80 .L154
                            .text:00000c82 .L155
                    .debug_abbrev:00000000 .Ldebug_abbrev0
                            .text:00000000 .Ltext0
                            .text:00000c8a .Letext0
                      .debug_line:00000000 .Ldebug_line0
                       .debug_str:0000005c .LASF0
MPLAB XC16 ASSEMBLY Listing:   			page 163


                       .debug_str:00000212 .LASF1
                       .debug_str:000001a8 .LASF2
                       .debug_str:0000003a .LASF3
                       .debug_str:00000179 .LASF4
                       .debug_str:0000008e .LASF5
                       .debug_str:000001bb .LASF6
                       .debug_str:0000014c .LASF7
                       .debug_str:00000070 .LASF8
                       .debug_str:000000d1 .LASF9
                       .debug_str:000000d8 .LASF10
                       .debug_str:000000df .LASF11
                       .debug_str:000000b9 .LASF12
                       .debug_str:000000c1 .LASF13
                       .debug_str:000001c2 .LASF14
                       .debug_str:00000131 .LASF15
                       .debug_str:000000f7 .LASF16
                       .debug_str:000001e4 .LASF17
                       .debug_str:0000012a .LASF18
                       .debug_str:00000226 .LASF19
                       .debug_str:0000022c .LASF20
                       .debug_str:00000000 .LASF21
                       .debug_str:00000006 .LASF22
                       .debug_str:0000000c .LASF23
                       .debug_str:000001dd .LASF24
                       .debug_str:00000119 .LASF25
                       .debug_str:00000094 .LASF26
                       .debug_str:00000166 .LASF27
                       .debug_str:00000173 .LASF28
                       .debug_str:000000fe .LASF29
                            .text:00000000 .LFB0
                            .text:0000010a .LFE0
                       .debug_str:000000c9 .LASF30
                       .debug_str:00000232 .LASF31
                            .text:0000010a .LFB1
                            .text:0000022e .LFE1
                       .debug_str:0000009b .LASF32
                            .text:0000022e .LFB2
                            .text:00000352 .LFE2
                            .text:00000352 .LFB3
                            .text:00000476 .LFE3
                            .text:00000476 .LFB4
                            .text:0000059a .LFE4
                            .text:0000059a .LFB5
                            .text:000005a6 .LFE5
                            .text:000005a6 .LFB6
                            .text:000005b2 .LFE6
                            .text:000005b2 .LFB7
                            .text:000005be .LFE7
                            .text:000005be .LFB8
                            .text:000005ca .LFE8
                            .text:000005ca .LFB9
                            .text:00000632 .LFE9
                       .debug_str:00000180 .LASF33
                       .debug_str:000001f8 .LASF34
                            .text:000005d8 .LBB26
                            .text:000005e8 .LBE26
                            .text:00000606 .LBB28
MPLAB XC16 ASSEMBLY Listing:   			page 164


                            .text:00000616 .LBE28
                            .text:00000632 .LFB10
                            .text:0000069a .LFE10
                            .text:00000640 .LBB30
                            .text:00000650 .LBE30
                            .text:0000066e .LBB32
                            .text:0000067e .LBE32
                            .text:0000069a .LFB11
                            .text:00000702 .LFE11
                            .text:000006a8 .LBB34
                            .text:000006b8 .LBE34
                            .text:000006d6 .LBB36
                            .text:000006e6 .LBE36
                            .text:00000702 .LFB12
                            .text:0000076a .LFE12
                            .text:00000710 .LBB38
                            .text:00000720 .LBE38
                            .text:0000073e .LBB40
                            .text:0000074e .LBE40
                            .text:0000076a .LFB13
                            .text:00000898 .LFE13
                       .debug_str:00000120 .LASF35
                       .debug_str:00000012 .LASF36
                       .debug_str:000001d7 .LASF37
                       .debug_str:00000107 .LASF38
                       .debug_str:00000154 .LASF39
                       .debug_str:0000016d .LASF40
                       .debug_str:000000a2 .LASF41
                       .debug_str:000000e6 .LASF42
                       .debug_str:00000076 .LASF43
                       .debug_str:00000137 .LASF44
                       .debug_str:00000041 .LASF45
                            .text:00000776 .LBB42
                            .text:0000077a .LBE42
                            .text:00000898 .LFB14
                            .text:000009c6 .LFE14
                            .text:000008a4 .LBB46
                            .text:000008a8 .LBE46
                            .text:000009c6 .LFB15
                            .text:00000af4 .LFE15
                            .text:000009d2 .LBB50
                            .text:000009d6 .LBE50
                            .text:00000af4 .LFB16
                            .text:00000c22 .LFE16
                            .text:00000b00 .LBB54
                            .text:00000b04 .LBE54
                            .text:00000c22 .LFB17
                            .text:00000c3c .LFE17
                            .text:00000c3c .LFB18
                            .text:00000c56 .LFE18
                            .text:00000c56 .LFB19
                            .text:00000c70 .LFE19
                            .text:00000c70 .LFB20
                            .text:00000c8a .LFE20
                       .debug_str:0000002d .LASF46
                       .debug_str:0000018e .LASF47
                       .debug_str:00000020 .LASF48
MPLAB XC16 ASSEMBLY Listing:   			page 165


                       .debug_str:00000219 .LASF49
                       .debug_str:0000019b .LASF50
                       .debug_str:000000ac .LASF51
                       .debug_str:000001eb .LASF52
                       .debug_str:000001ae .LASF53
                       .debug_str:0000004f .LASF54
                       .debug_str:00000205 .LASF55
                       .debug_str:000001ca .LASF56
                       .debug_str:00000063 .LASF57
                     .debug_frame:00000000 .Lframe0
                      .debug_info:00000000 .Ldebug_info0

UNDEFINED SYMBOLS
_memcpy
___udivsi3
___floatsisf
___mulsf3
___floatunsisf
___divsf3
___ltsf2
CORCON
_SPI1STATbits
_SPI1CON1bits
_SPI1CON2bits
_SPI2STATbits
_SPI2CON1bits
_SPI2CON2bits
_SPI3CON1bits
_SPI3CON2bits
_SPI3STATbits
_SPI4STATbits
_SPI4CON1bits
_SPI4CON2bits
_SPI1BUF
_SPI2BUF
_SPI3BUF
_SPI4BUF

EQUATE SYMBOLS
/home/user/Documents/FST/Programming/project_sub-zero/lib/lib_pic33e/SPI.c = 0x0
                  __33EP256MU806 = 0x1
                      __dsPIC33E = 0x1
                       __HAS_DSP = 0x1
                     __HAS_DMAV2 = 0x1
                 __HAS_CODEGUARD = 0x1
              __HAS_PMP_ENHANCED = 0x1
                       __HAS_EDS = 0x1
                        __HAS_EP = 0x1
          __TARGET_DIVIDE_CYCLES = 0x11
                        __C30ELF = 0x1
                        ___PA___ = 0x0
                        ___BP___ = 0x0
               __ext_attr_.const = 0x8
