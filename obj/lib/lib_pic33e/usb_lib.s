MPLAB XC16 ASSEMBLY Listing:   			page 1


   1              	.file "/home/user/Documents/FST/Programming/project_sub-zero/lib/lib_pic33e/usb_lib.c"
   2              	.section .debug_abbrev,info
   3                 	.Ldebug_abbrev0:
   4                 	.section .debug_info,info
   5                 	.Ldebug_info0:
   6                 	.section .debug_line,info
   7                 	.Ldebug_line0:
   8 0000 61 01 00 00 	.section .text,code
   8      02 00 14 01 
   8      00 00 01 01 
   8      FB 0E 0A 00 
   8      01 01 01 01 
   8      00 00 00 01 
   8      6C 69 62 2F 
   8      6C 69 62 5F 
   8      70 69 63 33 
   9              	.Ltext0:
  10              	.global _write_index_usb
  11              	.section .nbss,bss,near
  12                 	.align 2
  13                 	.type _write_index_usb,@object
  14                 	.size _write_index_usb,2
  15                 	_write_index_usb:
  16 0000 00 00       	.skip 2
  17                 	.global _read_index_usb
  18                 	.section .ndata,data,near
  19                 	.align 2
  20                 	.type _read_index_usb,@object
  21                 	.size _read_index_usb,2
  22                 	_read_index_usb:
  23 0000 3F 00       	.word 63
  24                 	.global _inner_write_index_usb
  25                 	.section .nbss,bss,near
  26                 	.align 2
  27                 	.type _inner_write_index_usb,@object
  28                 	.size _inner_write_index_usb,2
  29                 	_inner_write_index_usb:
  30 0002 00 00       	.skip 2
  31                 	.section .bss,bss
  32                 	.type _gpacket_buffer_usb,@object
  33                 	.global _gpacket_buffer_usb
  34 0000 00 00 00 00 	_gpacket_buffer_usb:.space 16384
  34      00 00 00 00 
  34      00 00 00 00 
  34      00 00 00 00 
  34      00 00 00 00 
  34      00 00 00 00 
  34      00 00 00 00 
  34      00 00 00 00 
  34      00 00 00 00 
  35                 	.type _gpacket_last_pos_usb,@object
  36                 	.global _gpacket_last_pos_usb
  37 4000 00 00 00 00 	_gpacket_last_pos_usb:.space 64
  37      00 00 00 00 
  37      00 00 00 00 
  37      00 00 00 00 
  37      00 00 00 00 
MPLAB XC16 ASSEMBLY Listing:   			page 2


  37      00 00 00 00 
  37      00 00 00 00 
  37      00 00 00 00 
  37      00 00 00 00 
  38                 	.section .text,code
  39              	.align 2
  40              	.weak _receive_handler_usb
  41              	.type _receive_handler_usb,@function
  42              	_receive_handler_usb:
  43              	.LFB0:
  44              	.file 1 "lib/lib_pic33e/usb_lib.c"
   1:lib/lib_pic33e/usb_lib.c **** #ifndef NOUSB
   2:lib/lib_pic33e/usb_lib.c **** 
   3:lib/lib_pic33e/usb_lib.c **** #include <string.h>
   4:lib/lib_pic33e/usb_lib.c **** #include <stdio.h>
   5:lib/lib_pic33e/usb_lib.c **** #include <stdlib.h>
   6:lib/lib_pic33e/usb_lib.c **** #include <stdarg.h>
   7:lib/lib_pic33e/usb_lib.c **** #include <stdbool.h>
   8:lib/lib_pic33e/usb_lib.c **** #include "usb_lib.h"
   9:lib/lib_pic33e/usb_lib.c **** #include "can.h"
  10:lib/lib_pic33e/usb_lib.c **** 
  11:lib/lib_pic33e/usb_lib.c **** unsigned volatile write_index_usb=0;
  12:lib/lib_pic33e/usb_lib.c **** unsigned volatile read_index_usb=BUFFER_SIZE_USB-1;
  13:lib/lib_pic33e/usb_lib.c **** unsigned volatile inner_write_index_usb = 0;
  14:lib/lib_pic33e/usb_lib.c **** 
  15:lib/lib_pic33e/usb_lib.c **** uint8_t gpacket_buffer_usb[BUFFER_SIZE_USB][BULK_SIZE_USB];
  16:lib/lib_pic33e/usb_lib.c **** uint8_t gpacket_last_pos_usb[BUFFER_SIZE_USB];
  17:lib/lib_pic33e/usb_lib.c **** 
  18:lib/lib_pic33e/usb_lib.c **** __attribute__((weak)) void receive_handler_usb(char *readString) {
  45              	.loc 1 18 0
  46              	.set ___PA___,1
  47 000000  02 00 FA 	lnk #2
  48              	.LCFI0:
  49 000002  00 0F 78 	mov w0,[w14]
  19:lib/lib_pic33e/usb_lib.c **** 	return;    
  20:lib/lib_pic33e/usb_lib.c **** }
  50              	.loc 1 20 0
  51 000004  8E 07 78 	mov w14,w15
  52 000006  4F 07 78 	mov [--w15],w14
  53 000008  00 40 A9 	bclr CORCON,#2
  54 00000a  00 00 06 	return 
  55              	.set ___PA___,0
  56              	.LFE0:
  57              	.size _receive_handler_usb,.-_receive_handler_usb
  58              	.align 2
  59              	.global _USBInit
  60              	.type _USBInit,@function
  61              	_USBInit:
  62              	.LFB1:
  21:lib/lib_pic33e/usb_lib.c **** 
  22:lib/lib_pic33e/usb_lib.c **** void USBInit(){
  63              	.loc 1 22 0
  64              	.set ___PA___,1
  65 00000c  00 00 FA 	lnk #0
  66              	.LCFI1:
  23:lib/lib_pic33e/usb_lib.c ****     USBDeviceInit();
  67              	.loc 1 23 0
MPLAB XC16 ASSEMBLY Listing:   			page 3


  68 00000e  00 00 07 	rcall _USBDeviceInit
  24:lib/lib_pic33e/usb_lib.c ****     USBDeviceAttach();
  69              	.loc 1 24 0
  70 000010  00 00 07 	rcall _USBDeviceAttach
  25:lib/lib_pic33e/usb_lib.c **** }
  71              	.loc 1 25 0
  72 000012  8E 07 78 	mov w14,w15
  73 000014  4F 07 78 	mov [--w15],w14
  74 000016  00 40 A9 	bclr CORCON,#2
  75 000018  00 00 06 	return 
  76              	.set ___PA___,0
  77              	.LFE1:
  78              	.size _USBInit,.-_USBInit
  79              	.align 2
  80              	.global _usb_full
  81              	.type _usb_full,@function
  82              	_usb_full:
  83              	.LFB2:
  26:lib/lib_pic33e/usb_lib.c **** 
  27:lib/lib_pic33e/usb_lib.c **** 
  28:lib/lib_pic33e/usb_lib.c **** /* Check if there is a packet to send 
  29:lib/lib_pic33e/usb_lib.c ****  * There is if the write_index_usb is 2 positions in front of read_index_usb
  30:lib/lib_pic33e/usb_lib.c ****  */
  31:lib/lib_pic33e/usb_lib.c **** bool usb_full() {
  84              	.loc 1 31 0
  85              	.set ___PA___,1
  86 00001a  00 00 FA 	lnk #0
  87              	.LCFI2:
  32:lib/lib_pic33e/usb_lib.c **** 	return !((read_index_usb+1) % BUFFER_SIZE_USB == write_index_usb);
  88              	.loc 1 32 0
  89 00001c  00 00 80 	mov _read_index_usb,w0
  90 00001e  F1 03 20 	mov #63,w1
  91 000020  00 01 E8 	inc w0,w2
  92 000022  00 00 80 	mov _write_index_usb,w0
  93 000024  81 00 61 	and w2,w1,w1
  94 000026  00 80 68 	xor w1,w0,w0
  95 000028  00 F0 A7 	btsc w0,#15
  96 00002a  00 00 EA 	neg w0,w0
  97 00002c  00 00 EA 	neg w0,w0
  98 00002e  4F 00 DE 	lsr w0,#15,w0
  99 000030  00 40 78 	mov.b w0,w0
  33:lib/lib_pic33e/usb_lib.c **** }
 100              	.loc 1 33 0
 101 000032  8E 07 78 	mov w14,w15
 102 000034  4F 07 78 	mov [--w15],w14
 103 000036  00 40 A9 	bclr CORCON,#2
 104 000038  00 00 06 	return 
 105              	.set ___PA___,0
 106              	.LFE2:
 107              	.size _usb_full,.-_usb_full
 108              	.align 2
 109              	.global _USBTasks
 110              	.type _USBTasks,@function
 111              	_USBTasks:
 112              	.LFB3:
  34:lib/lib_pic33e/usb_lib.c **** 
  35:lib/lib_pic33e/usb_lib.c **** void USBTasks(){
MPLAB XC16 ASSEMBLY Listing:   			page 4


 113              	.loc 1 35 0
 114              	.set ___PA___,1
 115 00003a  00 00 FA 	lnk #0
 116              	.LCFI3:
  36:lib/lib_pic33e/usb_lib.c ****     SYSTEM_Tasks();
  37:lib/lib_pic33e/usb_lib.c **** 
  38:lib/lib_pic33e/usb_lib.c ****     #if defined(USB_BLUE_LED)  
  39:lib/lib_pic33e/usb_lib.c ****     if (USBDeviceState==CONFIGURED_STATE){
 117              	.loc 1 39 0
 118 00003c  01 00 80 	mov _USBDeviceState,w1
 119 00003e  00 02 20 	mov #32,w0
 120 000040  80 8F 50 	sub w1,w0,[w15]
 121              	.set ___BP___,0
 122 000042  00 00 3A 	bra nz,.L5
  40:lib/lib_pic33e/usb_lib.c ****         LATEbits.LATE7=1;
 123              	.loc 1 40 0
 124 000044  00 E0 A8 	bset.b _LATEbits,#7
 125 000046  00 00 37 	bra .L6
 126              	.L5:
  41:lib/lib_pic33e/usb_lib.c ****     } 
  42:lib/lib_pic33e/usb_lib.c ****     else{
  43:lib/lib_pic33e/usb_lib.c ****         LATEbits.LATE7=0;
 127              	.loc 1 43 0
 128 000048  00 E0 A9 	bclr.b _LATEbits,#7
 129              	.L6:
  44:lib/lib_pic33e/usb_lib.c ****     }
  45:lib/lib_pic33e/usb_lib.c ****     #endif
  46:lib/lib_pic33e/usb_lib.c **** 
  47:lib/lib_pic33e/usb_lib.c ****     //Application specific tasks        
  48:lib/lib_pic33e/usb_lib.c ****     APP_DeviceCDCBasicDemoTasks();
 130              	.loc 1 48 0
 131 00004a  00 00 07 	rcall _APP_DeviceCDCBasicDemoTasks
  49:lib/lib_pic33e/usb_lib.c **** }
 132              	.loc 1 49 0
 133 00004c  8E 07 78 	mov w14,w15
 134 00004e  4F 07 78 	mov [--w15],w14
 135 000050  00 40 A9 	bclr CORCON,#2
 136 000052  00 00 06 	return 
 137              	.set ___PA___,0
 138              	.LFE3:
 139              	.size _USBTasks,.-_USBTasks
 140              	.align 2
 141              	.global _append_string_usb
 142              	.type _append_string_usb,@function
 143              	_append_string_usb:
 144              	.LFB4:
  50:lib/lib_pic33e/usb_lib.c **** 
  51:lib/lib_pic33e/usb_lib.c **** 
  52:lib/lib_pic33e/usb_lib.c **** void append_string_usb(uint8_t *buffer, size_t len){
 145              	.loc 1 52 0
 146              	.set ___PA___,1
 147 000054  06 00 FA 	lnk #6
 148              	.LCFI4:
 149              	.loc 1 52 0
 150 000056  10 07 98 	mov w0,[w14+2]
 151 000058  21 07 98 	mov w1,[w14+4]
  53:lib/lib_pic33e/usb_lib.c **** 	unsigned int i;
MPLAB XC16 ASSEMBLY Listing:   			page 5


  54:lib/lib_pic33e/usb_lib.c **** 	/* if it doesn't fit in a packet then fuck you */
  55:lib/lib_pic33e/usb_lib.c **** 	if (len > BULK_SIZE_USB) {
 152              	.loc 1 55 0
 153 00005a  00 10 20 	mov #256,w0
 154 00005c  AE 00 90 	mov [w14+4],w1
 155 00005e  80 8F 50 	sub w1,w0,[w15]
 156              	.set ___BP___,0
 157 000060  00 00 3E 	bra gtu,.L13
 158              	.L8:
  56:lib/lib_pic33e/usb_lib.c **** 		return;
  57:lib/lib_pic33e/usb_lib.c **** 	}
  58:lib/lib_pic33e/usb_lib.c **** 	
  59:lib/lib_pic33e/usb_lib.c **** 	/* If we have to much data to place on a buffer write it on the next one
  60:lib/lib_pic33e/usb_lib.c **** 	 * Save the last position on the buffer in another buffer.
  61:lib/lib_pic33e/usb_lib.c **** 	 * Update inner_write_index_usb to start from the beginning in the next buffer.
  62:lib/lib_pic33e/usb_lib.c **** 	 * Set the next buffer to zero so we don't send garbage.
  63:lib/lib_pic33e/usb_lib.c **** 	 */
  64:lib/lib_pic33e/usb_lib.c **** 	if (len > (BULK_SIZE_USB - inner_write_index_usb)) {
 159              	.loc 1 64 0
 160 000062  01 00 80 	mov _inner_write_index_usb,w1
 161 000064  02 10 20 	mov #256,w2
 162 000066  2E 00 90 	mov [w14+4],w0
 163 000068  81 00 51 	sub w2,w1,w1
 164 00006a  80 8F 50 	sub w1,w0,[w15]
 165              	.set ___BP___,0
 166 00006c  00 00 31 	bra geu,.L10
  65:lib/lib_pic33e/usb_lib.c **** 		gpacket_last_pos_usb[write_index_usb] = inner_write_index_usb;
 167              	.loc 1 65 0
 168 00006e  00 00 80 	mov _write_index_usb,w0
 169 000070  02 00 20 	mov #_gpacket_last_pos_usb,w2
 170 000072  01 00 80 	mov _inner_write_index_usb,w1
 171 000074  00 00 41 	add w2,w0,w0
  66:lib/lib_pic33e/usb_lib.c **** 		write_index_usb = (write_index_usb + 1) % BUFFER_SIZE_USB;
 172              	.loc 1 66 0
 173 000076  02 00 80 	mov _write_index_usb,w2
 174              	.loc 1 65 0
 175 000078  81 40 78 	mov.b w1,w1
 176              	.loc 1 66 0
 177 00007a  02 01 E8 	inc w2,w2
 178              	.loc 1 65 0
 179 00007c  01 48 78 	mov.b w1,[w0]
 180              	.loc 1 66 0
 181 00007e  F1 03 20 	mov #63,w1
  67:lib/lib_pic33e/usb_lib.c **** 		inner_write_index_usb = 0;
  68:lib/lib_pic33e/usb_lib.c **** 		memset(gpacket_buffer_usb[write_index_usb], 0, BULK_SIZE_USB);
 182              	.loc 1 68 0
 183 000080  00 00 20 	mov #_gpacket_buffer_usb,w0
 184              	.loc 1 66 0
 185 000082  81 00 61 	and w2,w1,w1
 186 000084  01 00 88 	mov w1,_write_index_usb
 187              	.loc 1 67 0
 188 000086  00 20 EF 	clr _inner_write_index_usb
 189              	.loc 1 68 0
 190 000088  01 00 80 	mov _write_index_usb,w1
 191 00008a  C8 08 DD 	sl w1,#8,w1
 192 00008c  00 80 40 	add w1,w0,w0
 193 00008e  02 10 20 	mov #256,w2
MPLAB XC16 ASSEMBLY Listing:   			page 6


 194 000090  80 00 EB 	clr w1
 195 000092  00 00 07 	rcall _memset
 196              	.L10:
  69:lib/lib_pic33e/usb_lib.c **** 	}
  70:lib/lib_pic33e/usb_lib.c **** 
  71:lib/lib_pic33e/usb_lib.c **** 	/* copy the buffer to the gpacket_buffer_usb */
  72:lib/lib_pic33e/usb_lib.c **** 	for (i = 0; i<len; i++, inner_write_index_usb++) {
 197              	.loc 1 72 0
 198 000094  00 00 EB 	clr w0
 199 000096  00 0F 78 	mov w0,[w14]
 200 000098  00 00 37 	bra .L11
 201              	.L12:
  73:lib/lib_pic33e/usb_lib.c **** 		gpacket_buffer_usb[write_index_usb][inner_write_index_usb] = buffer[i];
 202              	.loc 1 73 0
 203 00009a  00 00 00 	nop 
 204 00009c  1E 00 90 	mov [w14+2],w0
 205 00009e  00 00 00 	nop 
 206 0000a0  1E 00 40 	add w0,[w14],w0
 207 0000a2  04 00 80 	mov _write_index_usb,w4
 208 0000a4  90 40 78 	mov.b [w0],w1
 209              	.loc 1 72 0
 210 0000a6  1E 0F E8 	inc [w14],[w14]
 211              	.loc 1 73 0
 212 0000a8  00 00 80 	mov _inner_write_index_usb,w0
 213 0000aa  02 00 20 	mov #_gpacket_buffer_usb,w2
 214              	.loc 1 72 0
 215 0000ac  03 00 80 	mov _inner_write_index_usb,w3
 216              	.loc 1 73 0
 217 0000ae  48 22 DD 	sl w4,#8,w4
 218              	.loc 1 72 0
 219 0000b0  83 01 E8 	inc w3,w3
 220              	.loc 1 73 0
 221 0000b2  00 00 42 	add w4,w0,w0
 222              	.loc 1 72 0
 223 0000b4  03 00 88 	mov w3,_inner_write_index_usb
 224              	.loc 1 73 0
 225 0000b6  00 00 41 	add w2,w0,w0
 226 0000b8  01 48 78 	mov.b w1,[w0]
 227              	.L11:
 228              	.loc 1 72 0
 229 0000ba  9E 00 78 	mov [w14],w1
 230 0000bc  2E 00 90 	mov [w14+4],w0
 231 0000be  80 8F 50 	sub w1,w0,[w15]
 232              	.set ___BP___,0
 233 0000c0  00 00 39 	bra ltu,.L12
 234 0000c2  00 00 37 	bra .L7
 235              	.L13:
 236              	.L7:
  74:lib/lib_pic33e/usb_lib.c **** 	}
  75:lib/lib_pic33e/usb_lib.c **** }
 237              	.loc 1 75 0
 238 0000c4  8E 07 78 	mov w14,w15
 239 0000c6  4F 07 78 	mov [--w15],w14
 240 0000c8  00 40 A9 	bclr CORCON,#2
 241 0000ca  00 00 06 	return 
 242              	.set ___PA___,0
 243              	.LFE4:
MPLAB XC16 ASSEMBLY Listing:   			page 7


 244              	.size _append_string_usb,.-_append_string_usb
 245              	.align 2
 246              	.global _uprintf
 247              	.type _uprintf,@function
 248              	_uprintf:
 249              	.LFB5:
  76:lib/lib_pic33e/usb_lib.c **** 
  77:lib/lib_pic33e/usb_lib.c **** /**********************************************************************
  78:lib/lib_pic33e/usb_lib.c ****  * Name:    uprintf
  79:lib/lib_pic33e/usb_lib.c ****  * Args:	char *format and ... - same as printf
  80:lib/lib_pic33e/usb_lib.c ****  * Return:  Error signalling
  81:lib/lib_pic33e/usb_lib.c ****  *			0  - successful exit
  82:lib/lib_pic33e/usb_lib.c ****  *			-1 - string too long
  83:lib/lib_pic33e/usb_lib.c ****  * Desc:	Get CAN message from CAN 1 buffer
  84:lib/lib_pic33e/usb_lib.c ****  *			Returns Null if there aren't any messages available
  85:lib/lib_pic33e/usb_lib.c ****  **********************************************************************/
  86:lib/lib_pic33e/usb_lib.c **** int uprintf(char *format, ...){
 250              	.loc 1 86 0
 251              	.set ___PA___,1
 252 0000cc  04 01 FA 	lnk #260
 253              	.LCFI5:
  87:lib/lib_pic33e/usb_lib.c **** /*TODO: must increase cpu priority, to make sure this doesn't get interrupted*/
  88:lib/lib_pic33e/usb_lib.c ****     va_list aptr; 
  89:lib/lib_pic33e/usb_lib.c **** 
  90:lib/lib_pic33e/usb_lib.c ****     va_start(aptr, format);
 254              	.loc 1 90 0
 255 0000ce  68 00 57 	sub w14,#8,w0
 256 0000d0  00 0F 78 	mov w0,[w14]
  91:lib/lib_pic33e/usb_lib.c **** 
  92:lib/lib_pic33e/usb_lib.c ****     const char buffer[256];
  93:lib/lib_pic33e/usb_lib.c **** 
  94:lib/lib_pic33e/usb_lib.c ****     vsprintf(buffer,format, aptr);
 257              	.loc 1 94 0
 258 0000d2  CE B8 97 	mov [w14-8],w1
 259 0000d4  1E 01 78 	mov [w14],w2
 260 0000d6  64 00 47 	add w14,#4,w0
 261 0000d8  00 00 07 	rcall _vsprintf
  95:lib/lib_pic33e/usb_lib.c **** 
  96:lib/lib_pic33e/usb_lib.c ****     va_end(va);
  97:lib/lib_pic33e/usb_lib.c **** 
  98:lib/lib_pic33e/usb_lib.c ****     int bsize=strlen(buffer);
 262              	.loc 1 98 0
 263 0000da  64 00 47 	add w14,#4,w0
 264 0000dc  00 00 07 	rcall _strlen
  99:lib/lib_pic33e/usb_lib.c **** 
 100:lib/lib_pic33e/usb_lib.c ****     if (bsize>BULK_SIZE_USB-1)
 265              	.loc 1 100 0
 266 0000de  F1 0F 20 	mov #255,w1
 267              	.loc 1 98 0
 268 0000e0  10 07 98 	mov w0,[w14+2]
 269              	.loc 1 100 0
 270 0000e2  1E 00 90 	mov [w14+2],w0
 271 0000e4  81 0F 50 	sub w0,w1,[w15]
 272              	.set ___BP___,0
 273 0000e6  00 00 34 	bra le,.L15
 101:lib/lib_pic33e/usb_lib.c ****         return -1;
 274              	.loc 1 101 0
MPLAB XC16 ASSEMBLY Listing:   			page 8


 275 0000e8  00 80 EB 	setm w0
 276 0000ea  00 00 37 	bra .L16
 277              	.L15:
 102:lib/lib_pic33e/usb_lib.c **** 
 103:lib/lib_pic33e/usb_lib.c **** 	append_string_usb((uint8_t *) buffer, bsize);
 278              	.loc 1 103 0
 279 0000ec  9E 00 90 	mov [w14+2],w1
 280 0000ee  64 00 47 	add w14,#4,w0
 281 0000f0  00 00 07 	rcall _append_string_usb
 104:lib/lib_pic33e/usb_lib.c **** 
 105:lib/lib_pic33e/usb_lib.c **** 	return 0;
 282              	.loc 1 105 0
 283 0000f2  00 00 EB 	clr w0
 284              	.L16:
 106:lib/lib_pic33e/usb_lib.c **** }
 285              	.loc 1 106 0
 286 0000f4  8E 07 78 	mov w14,w15
 287 0000f6  4F 07 78 	mov [--w15],w14
 288 0000f8  00 40 A9 	bclr CORCON,#2
 289 0000fa  00 00 06 	return 
 290              	.set ___PA___,0
 291              	.LFE5:
 292              	.size _uprintf,.-_uprintf
 293              	.section .debug_frame,info
 294                 	.Lframe0:
 295 0000 10 00 00 00 	.4byte .LECIE0-.LSCIE0
 296                 	.LSCIE0:
 297 0004 FF FF FF FF 	.4byte 0xffffffff
 298 0008 01          	.byte 0x1
 299 0009 00          	.byte 0
 300 000a 01          	.uleb128 0x1
 301 000b 02          	.sleb128 2
 302 000c 25          	.byte 0x25
 303 000d 12          	.byte 0x12
 304 000e 0F          	.uleb128 0xf
 305 000f 7E          	.sleb128 -2
 306 0010 09          	.byte 0x9
 307 0011 25          	.uleb128 0x25
 308 0012 0F          	.uleb128 0xf
 309 0013 00          	.align 4
 310                 	.LECIE0:
 311                 	.LSFDE0:
 312 0014 18 00 00 00 	.4byte .LEFDE0-.LASFDE0
 313                 	.LASFDE0:
 314 0018 00 00 00 00 	.4byte .Lframe0
 315 001c 00 00 00 00 	.4byte .LFB0
 316 0020 0C 00 00 00 	.4byte .LFE0-.LFB0
 317 0024 04          	.byte 0x4
 318 0025 02 00 00 00 	.4byte .LCFI0-.LFB0
 319 0029 13          	.byte 0x13
 320 002a 7D          	.sleb128 -3
 321 002b 0D          	.byte 0xd
 322 002c 0E          	.uleb128 0xe
 323 002d 8E          	.byte 0x8e
 324 002e 02          	.uleb128 0x2
 325 002f 00          	.align 4
 326                 	.LEFDE0:
MPLAB XC16 ASSEMBLY Listing:   			page 9


 327                 	.LSFDE2:
 328 0030 18 00 00 00 	.4byte .LEFDE2-.LASFDE2
 329                 	.LASFDE2:
 330 0034 00 00 00 00 	.4byte .Lframe0
 331 0038 00 00 00 00 	.4byte .LFB1
 332 003c 0E 00 00 00 	.4byte .LFE1-.LFB1
 333 0040 04          	.byte 0x4
 334 0041 02 00 00 00 	.4byte .LCFI1-.LFB1
 335 0045 13          	.byte 0x13
 336 0046 7D          	.sleb128 -3
 337 0047 0D          	.byte 0xd
 338 0048 0E          	.uleb128 0xe
 339 0049 8E          	.byte 0x8e
 340 004a 02          	.uleb128 0x2
 341 004b 00          	.align 4
 342                 	.LEFDE2:
 343                 	.LSFDE4:
 344 004c 18 00 00 00 	.4byte .LEFDE4-.LASFDE4
 345                 	.LASFDE4:
 346 0050 00 00 00 00 	.4byte .Lframe0
 347 0054 00 00 00 00 	.4byte .LFB2
 348 0058 20 00 00 00 	.4byte .LFE2-.LFB2
 349 005c 04          	.byte 0x4
 350 005d 02 00 00 00 	.4byte .LCFI2-.LFB2
 351 0061 13          	.byte 0x13
 352 0062 7D          	.sleb128 -3
 353 0063 0D          	.byte 0xd
 354 0064 0E          	.uleb128 0xe
 355 0065 8E          	.byte 0x8e
 356 0066 02          	.uleb128 0x2
 357 0067 00          	.align 4
 358                 	.LEFDE4:
 359                 	.LSFDE6:
 360 0068 18 00 00 00 	.4byte .LEFDE6-.LASFDE6
 361                 	.LASFDE6:
 362 006c 00 00 00 00 	.4byte .Lframe0
 363 0070 00 00 00 00 	.4byte .LFB3
 364 0074 1A 00 00 00 	.4byte .LFE3-.LFB3
 365 0078 04          	.byte 0x4
 366 0079 02 00 00 00 	.4byte .LCFI3-.LFB3
 367 007d 13          	.byte 0x13
 368 007e 7D          	.sleb128 -3
 369 007f 0D          	.byte 0xd
 370 0080 0E          	.uleb128 0xe
 371 0081 8E          	.byte 0x8e
 372 0082 02          	.uleb128 0x2
 373 0083 00          	.align 4
 374                 	.LEFDE6:
 375                 	.LSFDE8:
 376 0084 18 00 00 00 	.4byte .LEFDE8-.LASFDE8
 377                 	.LASFDE8:
 378 0088 00 00 00 00 	.4byte .Lframe0
 379 008c 00 00 00 00 	.4byte .LFB4
 380 0090 78 00 00 00 	.4byte .LFE4-.LFB4
 381 0094 04          	.byte 0x4
 382 0095 02 00 00 00 	.4byte .LCFI4-.LFB4
 383 0099 13          	.byte 0x13
MPLAB XC16 ASSEMBLY Listing:   			page 10


 384 009a 7D          	.sleb128 -3
 385 009b 0D          	.byte 0xd
 386 009c 0E          	.uleb128 0xe
 387 009d 8E          	.byte 0x8e
 388 009e 02          	.uleb128 0x2
 389 009f 00          	.align 4
 390                 	.LEFDE8:
 391                 	.LSFDE10:
 392 00a0 18 00 00 00 	.4byte .LEFDE10-.LASFDE10
 393                 	.LASFDE10:
 394 00a4 00 00 00 00 	.4byte .Lframe0
 395 00a8 00 00 00 00 	.4byte .LFB5
 396 00ac 30 00 00 00 	.4byte .LFE5-.LFB5
 397 00b0 04          	.byte 0x4
 398 00b1 02 00 00 00 	.4byte .LCFI5-.LFB5
 399 00b5 13          	.byte 0x13
 400 00b6 7D          	.sleb128 -3
 401 00b7 0D          	.byte 0xd
 402 00b8 0E          	.uleb128 0xe
 403 00b9 8E          	.byte 0x8e
 404 00ba 02          	.uleb128 0x2
 405 00bb 00          	.align 4
 406                 	.LEFDE10:
 407                 	.section .text,code
 408              	.Letext0:
 409              	.file 2 "/opt/microchip/xc16/v1.36/bin/bin/../../include/lega-c/yvals.h"
 410              	.file 3 "/opt/microchip/xc16/v1.36/bin/bin/../../include/lega-c/string.h"
 411              	.file 4 "/opt/microchip/xc16/v1.36/bin/bin/../../include/lega-c/stdint.h"
 412              	.file 5 "/opt/microchip/xc16/v1.36/bin/bin/../../support/dsPIC33E/h/p33EP256MU806.h"
 413              	.file 6 "lib/lib_pic33e/usb/usb_common.h"
 414              	.file 7 "lib/lib_pic33e/usb/usb_device.h"
 415              	.file 8 "lib/lib_pic33e/usb_lib.h"
 416              	.section .debug_info,info
 417 0000 8B 08 00 00 	.4byte 0x88b
 418 0004 02 00       	.2byte 0x2
 419 0006 00 00 00 00 	.4byte .Ldebug_abbrev0
 420 000a 04          	.byte 0x4
 421 000b 01          	.uleb128 0x1
 422 000c 47 4E 55 20 	.asciz "GNU C 4.5.1 (XC16, Microchip v1.36) (A) Build date: Dec  5 2018"
 422      43 20 34 2E 
 422      35 2E 31 20 
 422      28 58 43 31 
 422      36 2C 20 4D 
 422      69 63 72 6F 
 422      63 68 69 70 
 422      20 76 31 2E 
 422      33 36 29 20 
 423 004c 01          	.byte 0x1
 424 004d 6C 69 62 2F 	.asciz "lib/lib_pic33e/usb_lib.c"
 424      6C 69 62 5F 
 424      70 69 63 33 
 424      33 65 2F 75 
 424      73 62 5F 6C 
 424      69 62 2E 63 
 424      00 
 425 0066 2F 68 6F 6D 	.asciz "/home/user/Documents/FST/Programming/project_sub-zero"
 425      65 2F 75 73 
MPLAB XC16 ASSEMBLY Listing:   			page 11


 425      65 72 2F 44 
 425      6F 63 75 6D 
 425      65 6E 74 73 
 425      2F 46 53 54 
 425      2F 50 72 6F 
 425      67 72 61 6D 
 425      6D 69 6E 67 
 426 009c 00 00 00 00 	.4byte .Ltext0
 427 00a0 00 00 00 00 	.4byte .Letext0
 428 00a4 00 00 00 00 	.4byte .Ldebug_line0
 429 00a8 02          	.uleb128 0x2
 430 00a9 76 61 5F 6C 	.asciz "va_list"
 430      69 73 74 00 
 431 00b1 02          	.byte 0x2
 432 00b2 56          	.byte 0x56
 433 00b3 B7 00 00 00 	.4byte 0xb7
 434 00b7 03          	.uleb128 0x3
 435 00b8 02          	.byte 0x2
 436 00b9 04          	.uleb128 0x4
 437 00ba 08          	.byte 0x8
 438 00bb 05          	.byte 0x5
 439 00bc 6C 6F 6E 67 	.asciz "long long int"
 439      20 6C 6F 6E 
 439      67 20 69 6E 
 439      74 00 
 440 00ca 04          	.uleb128 0x4
 441 00cb 08          	.byte 0x8
 442 00cc 07          	.byte 0x7
 443 00cd 6C 6F 6E 67 	.asciz "long long unsigned int"
 443      20 6C 6F 6E 
 443      67 20 75 6E 
 443      73 69 67 6E 
 443      65 64 20 69 
 443      6E 74 00 
 444 00e4 04          	.uleb128 0x4
 445 00e5 02          	.byte 0x2
 446 00e6 07          	.byte 0x7
 447 00e7 73 68 6F 72 	.asciz "short unsigned int"
 447      74 20 75 6E 
 447      73 69 67 6E 
 447      65 64 20 69 
 447      6E 74 00 
 448 00fa 04          	.uleb128 0x4
 449 00fb 02          	.byte 0x2
 450 00fc 07          	.byte 0x7
 451 00fd 75 6E 73 69 	.asciz "unsigned int"
 451      67 6E 65 64 
 451      20 69 6E 74 
 451      00 
 452 010a 04          	.uleb128 0x4
 453 010b 02          	.byte 0x2
 454 010c 05          	.byte 0x5
 455 010d 69 6E 74 00 	.asciz "int"
 456 0111 02          	.uleb128 0x2
 457 0112 5F 53 69 7A 	.asciz "_Sizet"
 457      65 74 00 
 458 0119 02          	.byte 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 12


 459 011a A2          	.byte 0xa2
 460 011b FA 00 00 00 	.4byte 0xfa
 461 011f 04          	.uleb128 0x4
 462 0120 04          	.byte 0x4
 463 0121 05          	.byte 0x5
 464 0122 6C 6F 6E 67 	.asciz "long int"
 464      20 69 6E 74 
 464      00 
 465 012b 02          	.uleb128 0x2
 466 012c 73 69 7A 65 	.asciz "size_t"
 466      5F 74 00 
 467 0133 03          	.byte 0x3
 468 0134 15          	.byte 0x15
 469 0135 11 01 00 00 	.4byte 0x111
 470 0139 04          	.uleb128 0x4
 471 013a 01          	.byte 0x1
 472 013b 08          	.byte 0x8
 473 013c 75 6E 73 69 	.asciz "unsigned char"
 473      67 6E 65 64 
 473      20 63 68 61 
 473      72 00 
 474 014a 04          	.uleb128 0x4
 475 014b 01          	.byte 0x1
 476 014c 06          	.byte 0x6
 477 014d 73 69 67 6E 	.asciz "signed char"
 477      65 64 20 63 
 477      68 61 72 00 
 478 0159 05          	.uleb128 0x5
 479 015a 02          	.byte 0x2
 480 015b 5F 01 00 00 	.4byte 0x15f
 481 015f 04          	.uleb128 0x4
 482 0160 01          	.byte 0x1
 483 0161 06          	.byte 0x6
 484 0162 63 68 61 72 	.asciz "char"
 484      00 
 485 0167 02          	.uleb128 0x2
 486 0168 75 69 6E 74 	.asciz "uint8_t"
 486      38 5F 74 00 
 487 0170 04          	.byte 0x4
 488 0171 2B          	.byte 0x2b
 489 0172 39 01 00 00 	.4byte 0x139
 490 0176 02          	.uleb128 0x2
 491 0177 75 69 6E 74 	.asciz "uint16_t"
 491      31 36 5F 74 
 491      00 
 492 0180 04          	.byte 0x4
 493 0181 31          	.byte 0x31
 494 0182 FA 00 00 00 	.4byte 0xfa
 495 0186 04          	.uleb128 0x4
 496 0187 04          	.byte 0x4
 497 0188 07          	.byte 0x7
 498 0189 6C 6F 6E 67 	.asciz "long unsigned int"
 498      20 75 6E 73 
 498      69 67 6E 65 
 498      64 20 69 6E 
 498      74 00 
 499 019b 06          	.uleb128 0x6
MPLAB XC16 ASSEMBLY Listing:   			page 13


 500 019c 74 61 67 4C 	.asciz "tagLATEBITS"
 500      41 54 45 42 
 500      49 54 53 00 
 501 01a8 02          	.byte 0x2
 502 01a9 05          	.byte 0x5
 503 01aa C0 3B       	.2byte 0x3bc0
 504 01ac 51 02 00 00 	.4byte 0x251
 505 01b0 07          	.uleb128 0x7
 506 01b1 4C 41 54 45 	.asciz "LATE0"
 506      30 00 
 507 01b7 05          	.byte 0x5
 508 01b8 C1 3B       	.2byte 0x3bc1
 509 01ba 76 01 00 00 	.4byte 0x176
 510 01be 02          	.byte 0x2
 511 01bf 01          	.byte 0x1
 512 01c0 0F          	.byte 0xf
 513 01c1 02          	.byte 0x2
 514 01c2 23          	.byte 0x23
 515 01c3 00          	.uleb128 0x0
 516 01c4 07          	.uleb128 0x7
 517 01c5 4C 41 54 45 	.asciz "LATE1"
 517      31 00 
 518 01cb 05          	.byte 0x5
 519 01cc C2 3B       	.2byte 0x3bc2
 520 01ce 76 01 00 00 	.4byte 0x176
 521 01d2 02          	.byte 0x2
 522 01d3 01          	.byte 0x1
 523 01d4 0E          	.byte 0xe
 524 01d5 02          	.byte 0x2
 525 01d6 23          	.byte 0x23
 526 01d7 00          	.uleb128 0x0
 527 01d8 07          	.uleb128 0x7
 528 01d9 4C 41 54 45 	.asciz "LATE2"
 528      32 00 
 529 01df 05          	.byte 0x5
 530 01e0 C3 3B       	.2byte 0x3bc3
 531 01e2 76 01 00 00 	.4byte 0x176
 532 01e6 02          	.byte 0x2
 533 01e7 01          	.byte 0x1
 534 01e8 0D          	.byte 0xd
 535 01e9 02          	.byte 0x2
 536 01ea 23          	.byte 0x23
 537 01eb 00          	.uleb128 0x0
 538 01ec 07          	.uleb128 0x7
 539 01ed 4C 41 54 45 	.asciz "LATE3"
 539      33 00 
 540 01f3 05          	.byte 0x5
 541 01f4 C4 3B       	.2byte 0x3bc4
 542 01f6 76 01 00 00 	.4byte 0x176
 543 01fa 02          	.byte 0x2
 544 01fb 01          	.byte 0x1
 545 01fc 0C          	.byte 0xc
 546 01fd 02          	.byte 0x2
 547 01fe 23          	.byte 0x23
 548 01ff 00          	.uleb128 0x0
 549 0200 07          	.uleb128 0x7
 550 0201 4C 41 54 45 	.asciz "LATE4"
MPLAB XC16 ASSEMBLY Listing:   			page 14


 550      34 00 
 551 0207 05          	.byte 0x5
 552 0208 C5 3B       	.2byte 0x3bc5
 553 020a 76 01 00 00 	.4byte 0x176
 554 020e 02          	.byte 0x2
 555 020f 01          	.byte 0x1
 556 0210 0B          	.byte 0xb
 557 0211 02          	.byte 0x2
 558 0212 23          	.byte 0x23
 559 0213 00          	.uleb128 0x0
 560 0214 07          	.uleb128 0x7
 561 0215 4C 41 54 45 	.asciz "LATE5"
 561      35 00 
 562 021b 05          	.byte 0x5
 563 021c C6 3B       	.2byte 0x3bc6
 564 021e 76 01 00 00 	.4byte 0x176
 565 0222 02          	.byte 0x2
 566 0223 01          	.byte 0x1
 567 0224 0A          	.byte 0xa
 568 0225 02          	.byte 0x2
 569 0226 23          	.byte 0x23
 570 0227 00          	.uleb128 0x0
 571 0228 07          	.uleb128 0x7
 572 0229 4C 41 54 45 	.asciz "LATE6"
 572      36 00 
 573 022f 05          	.byte 0x5
 574 0230 C7 3B       	.2byte 0x3bc7
 575 0232 76 01 00 00 	.4byte 0x176
 576 0236 02          	.byte 0x2
 577 0237 01          	.byte 0x1
 578 0238 09          	.byte 0x9
 579 0239 02          	.byte 0x2
 580 023a 23          	.byte 0x23
 581 023b 00          	.uleb128 0x0
 582 023c 07          	.uleb128 0x7
 583 023d 4C 41 54 45 	.asciz "LATE7"
 583      37 00 
 584 0243 05          	.byte 0x5
 585 0244 C8 3B       	.2byte 0x3bc8
 586 0246 76 01 00 00 	.4byte 0x176
 587 024a 02          	.byte 0x2
 588 024b 01          	.byte 0x1
 589 024c 08          	.byte 0x8
 590 024d 02          	.byte 0x2
 591 024e 23          	.byte 0x23
 592 024f 00          	.uleb128 0x0
 593 0250 00          	.byte 0x0
 594 0251 08          	.uleb128 0x8
 595 0252 4C 41 54 45 	.asciz "LATEBITS"
 595      42 49 54 53 
 595      00 
 596 025b 05          	.byte 0x5
 597 025c C9 3B       	.2byte 0x3bc9
 598 025e 9B 01 00 00 	.4byte 0x19b
 599 0262 09          	.uleb128 0x9
 600 0263 02          	.byte 0x2
 601 0264 06          	.byte 0x6
MPLAB XC16 ASSEMBLY Listing:   			page 15


 602 0265 CF          	.byte 0xcf
 603 0266 90 05 00 00 	.4byte 0x590
 604 026a 0A          	.uleb128 0xa
 605 026b 45 56 45 4E 	.asciz "EVENT_NONE"
 605      54 5F 4E 4F 
 605      4E 45 00 
 606 0276 00          	.sleb128 0
 607 0277 0A          	.uleb128 0xa
 608 0278 45 56 45 4E 	.asciz "EVENT_DEVICE_STACK_BASE"
 608      54 5F 44 45 
 608      56 49 43 45 
 608      5F 53 54 41 
 608      43 4B 5F 42 
 608      41 53 45 00 
 609 0290 01          	.sleb128 1
 610 0291 0A          	.uleb128 0xa
 611 0292 45 56 45 4E 	.asciz "EVENT_HOST_STACK_BASE"
 611      54 5F 48 4F 
 611      53 54 5F 53 
 611      54 41 43 4B 
 611      5F 42 41 53 
 611      45 00 
 612 02a8 E4 00       	.sleb128 100
 613 02aa 0A          	.uleb128 0xa
 614 02ab 45 56 45 4E 	.asciz "EVENT_HUB_ATTACH"
 614      54 5F 48 55 
 614      42 5F 41 54 
 614      54 41 43 48 
 614      00 
 615 02bc E5 00       	.sleb128 101
 616 02be 0A          	.uleb128 0xa
 617 02bf 45 56 45 4E 	.asciz "EVENT_STALL"
 617      54 5F 53 54 
 617      41 4C 4C 00 
 618 02cb E6 00       	.sleb128 102
 619 02cd 0A          	.uleb128 0xa
 620 02ce 45 56 45 4E 	.asciz "EVENT_VBUS_SES_REQUEST"
 620      54 5F 56 42 
 620      55 53 5F 53 
 620      45 53 5F 52 
 620      45 51 55 45 
 620      53 54 00 
 621 02e5 E7 00       	.sleb128 103
 622 02e7 0A          	.uleb128 0xa
 623 02e8 45 56 45 4E 	.asciz "EVENT_VBUS_OVERCURRENT"
 623      54 5F 56 42 
 623      55 53 5F 4F 
 623      56 45 52 43 
 623      55 52 52 45 
 623      4E 54 00 
 624 02ff E8 00       	.sleb128 104
 625 0301 0A          	.uleb128 0xa
 626 0302 45 56 45 4E 	.asciz "EVENT_VBUS_REQUEST_POWER"
 626      54 5F 56 42 
 626      55 53 5F 52 
 626      45 51 55 45 
 626      53 54 5F 50 
MPLAB XC16 ASSEMBLY Listing:   			page 16


 626      4F 57 45 52 
 626      00 
 627 031b E9 00       	.sleb128 105
 628 031d 0A          	.uleb128 0xa
 629 031e 45 56 45 4E 	.asciz "EVENT_VBUS_RELEASE_POWER"
 629      54 5F 56 42 
 629      55 53 5F 52 
 629      45 4C 45 41 
 629      53 45 5F 50 
 629      4F 57 45 52 
 629      00 
 630 0337 EA 00       	.sleb128 106
 631 0339 0A          	.uleb128 0xa
 632 033a 45 56 45 4E 	.asciz "EVENT_VBUS_POWER_AVAILABLE"
 632      54 5F 56 42 
 632      55 53 5F 50 
 632      4F 57 45 52 
 632      5F 41 56 41 
 632      49 4C 41 42 
 632      4C 45 00 
 633 0355 EB 00       	.sleb128 107
 634 0357 0A          	.uleb128 0xa
 635 0358 45 56 45 4E 	.asciz "EVENT_UNSUPPORTED_DEVICE"
 635      54 5F 55 4E 
 635      53 55 50 50 
 635      4F 52 54 45 
 635      44 5F 44 45 
 635      56 49 43 45 
 635      00 
 636 0371 EC 00       	.sleb128 108
 637 0373 0A          	.uleb128 0xa
 638 0374 45 56 45 4E 	.asciz "EVENT_CANNOT_ENUMERATE"
 638      54 5F 43 41 
 638      4E 4E 4F 54 
 638      5F 45 4E 55 
 638      4D 45 52 41 
 638      54 45 00 
 639 038b ED 00       	.sleb128 109
 640 038d 0A          	.uleb128 0xa
 641 038e 45 56 45 4E 	.asciz "EVENT_CLIENT_INIT_ERROR"
 641      54 5F 43 4C 
 641      49 45 4E 54 
 641      5F 49 4E 49 
 641      54 5F 45 52 
 641      52 4F 52 00 
 642 03a6 EE 00       	.sleb128 110
 643 03a8 0A          	.uleb128 0xa
 644 03a9 45 56 45 4E 	.asciz "EVENT_OUT_OF_MEMORY"
 644      54 5F 4F 55 
 644      54 5F 4F 46 
 644      5F 4D 45 4D 
 644      4F 52 59 00 
 645 03bd EF 00       	.sleb128 111
 646 03bf 0A          	.uleb128 0xa
 647 03c0 45 56 45 4E 	.asciz "EVENT_UNSPECIFIED_ERROR"
 647      54 5F 55 4E 
 647      53 50 45 43 
MPLAB XC16 ASSEMBLY Listing:   			page 17


 647      49 46 49 45 
 647      44 5F 45 52 
 647      52 4F 52 00 
 648 03d8 F0 00       	.sleb128 112
 649 03da 0A          	.uleb128 0xa
 650 03db 45 56 45 4E 	.asciz "EVENT_DETACH"
 650      54 5F 44 45 
 650      54 41 43 48 
 650      00 
 651 03e8 F1 00       	.sleb128 113
 652 03ea 0A          	.uleb128 0xa
 653 03eb 45 56 45 4E 	.asciz "EVENT_TRANSFER"
 653      54 5F 54 52 
 653      41 4E 53 46 
 653      45 52 00 
 654 03fa F2 00       	.sleb128 114
 655 03fc 0A          	.uleb128 0xa
 656 03fd 45 56 45 4E 	.asciz "EVENT_SOF"
 656      54 5F 53 4F 
 656      46 00 
 657 0407 F3 00       	.sleb128 115
 658 0409 0A          	.uleb128 0xa
 659 040a 45 56 45 4E 	.asciz "EVENT_RESUME"
 659      54 5F 52 45 
 659      53 55 4D 45 
 659      00 
 660 0417 F4 00       	.sleb128 116
 661 0419 0A          	.uleb128 0xa
 662 041a 45 56 45 4E 	.asciz "EVENT_SUSPEND"
 662      54 5F 53 55 
 662      53 50 45 4E 
 662      44 00 
 663 0428 F5 00       	.sleb128 117
 664 042a 0A          	.uleb128 0xa
 665 042b 45 56 45 4E 	.asciz "EVENT_RESET"
 665      54 5F 52 45 
 665      53 45 54 00 
 666 0437 F6 00       	.sleb128 118
 667 0439 0A          	.uleb128 0xa
 668 043a 45 56 45 4E 	.asciz "EVENT_DATA_ISOC_READ"
 668      54 5F 44 41 
 668      54 41 5F 49 
 668      53 4F 43 5F 
 668      52 45 41 44 
 668      00 
 669 044f F7 00       	.sleb128 119
 670 0451 0A          	.uleb128 0xa
 671 0452 45 56 45 4E 	.asciz "EVENT_DATA_ISOC_WRITE"
 671      54 5F 44 41 
 671      54 41 5F 49 
 671      53 4F 43 5F 
 671      57 52 49 54 
 671      45 00 
 672 0468 F8 00       	.sleb128 120
 673 046a 0A          	.uleb128 0xa
 674 046b 45 56 45 4E 	.asciz "EVENT_OVERRIDE_CLIENT_DRIVER_SELECTION"
 674      54 5F 4F 56 
MPLAB XC16 ASSEMBLY Listing:   			page 18


 674      45 52 52 49 
 674      44 45 5F 43 
 674      4C 49 45 4E 
 674      54 5F 44 52 
 674      49 56 45 52 
 674      5F 53 45 4C 
 674      45 43 54 49 
 675 0492 F9 00       	.sleb128 121
 676 0494 0A          	.uleb128 0xa
 677 0495 45 56 45 4E 	.asciz "EVENT_1MS"
 677      54 5F 31 4D 
 677      53 00 
 678 049f FA 00       	.sleb128 122
 679 04a1 0A          	.uleb128 0xa
 680 04a2 45 56 45 4E 	.asciz "EVENT_ALT_INTERFACE"
 680      54 5F 41 4C 
 680      54 5F 49 4E 
 680      54 45 52 46 
 680      41 43 45 00 
 681 04b6 FB 00       	.sleb128 123
 682 04b8 0A          	.uleb128 0xa
 683 04b9 45 56 45 4E 	.asciz "EVENT_HOLD_BEFORE_CONFIGURATION"
 683      54 5F 48 4F 
 683      4C 44 5F 42 
 683      45 46 4F 52 
 683      45 5F 43 4F 
 683      4E 46 49 47 
 683      55 52 41 54 
 683      49 4F 4E 00 
 684 04d9 FC 00       	.sleb128 124
 685 04db 0A          	.uleb128 0xa
 686 04dc 45 56 45 4E 	.asciz "EVENT_GENERIC_BASE"
 686      54 5F 47 45 
 686      4E 45 52 49 
 686      43 5F 42 41 
 686      53 45 00 
 687 04ef 90 03       	.sleb128 400
 688 04f1 0A          	.uleb128 0xa
 689 04f2 45 56 45 4E 	.asciz "EVENT_MSD_BASE"
 689      54 5F 4D 53 
 689      44 5F 42 41 
 689      53 45 00 
 690 0501 F4 03       	.sleb128 500
 691 0503 0A          	.uleb128 0xa
 692 0504 45 56 45 4E 	.asciz "EVENT_HID_BASE"
 692      54 5F 48 49 
 692      44 5F 42 41 
 692      53 45 00 
 693 0513 D8 04       	.sleb128 600
 694 0515 0A          	.uleb128 0xa
 695 0516 45 56 45 4E 	.asciz "EVENT_PRINTER_BASE"
 695      54 5F 50 52 
 695      49 4E 54 45 
 695      52 5F 42 41 
 695      53 45 00 
 696 0529 BC 05       	.sleb128 700
 697 052b 0A          	.uleb128 0xa
MPLAB XC16 ASSEMBLY Listing:   			page 19


 698 052c 45 56 45 4E 	.asciz "EVENT_CDC_BASE"
 698      54 5F 43 44 
 698      43 5F 42 41 
 698      53 45 00 
 699 053b A0 06       	.sleb128 800
 700 053d 0A          	.uleb128 0xa
 701 053e 45 56 45 4E 	.asciz "EVENT_CHARGER_BASE"
 701      54 5F 43 48 
 701      41 52 47 45 
 701      52 5F 42 41 
 701      53 45 00 
 702 0551 84 07       	.sleb128 900
 703 0553 0A          	.uleb128 0xa
 704 0554 45 56 45 4E 	.asciz "EVENT_AUDIO_BASE"
 704      54 5F 41 55 
 704      44 49 4F 5F 
 704      42 41 53 45 
 704      00 
 705 0565 E8 07       	.sleb128 1000
 706 0567 0A          	.uleb128 0xa
 707 0568 45 56 45 4E 	.asciz "EVENT_USER_BASE"
 707      54 5F 55 53 
 707      45 52 5F 42 
 707      41 53 45 00 
 708 0578 90 CE 00    	.sleb128 10000
 709 057b 0A          	.uleb128 0xa
 710 057c 45 56 45 4E 	.asciz "EVENT_BUS_ERROR"
 710      54 5F 42 55 
 710      53 5F 45 52 
 710      52 4F 52 00 
 711 058c FF FF 01    	.sleb128 32767
 712 058f 00          	.byte 0x0
 713 0590 04          	.uleb128 0x4
 714 0591 01          	.byte 0x1
 715 0592 02          	.byte 0x2
 716 0593 5F 42 6F 6F 	.asciz "_Bool"
 716      6C 00 
 717 0599 09          	.uleb128 0x9
 718 059a 02          	.byte 0x2
 719 059b 07          	.byte 0x7
 720 059c 4A          	.byte 0x4a
 721 059d 1B 06 00 00 	.4byte 0x61b
 722 05a1 0A          	.uleb128 0xa
 723 05a2 44 45 54 41 	.asciz "DETACHED_STATE"
 723      43 48 45 44 
 723      5F 53 54 41 
 723      54 45 00 
 724 05b1 00          	.sleb128 0
 725 05b2 0A          	.uleb128 0xa
 726 05b3 41 54 54 41 	.asciz "ATTACHED_STATE"
 726      43 48 45 44 
 726      5F 53 54 41 
 726      54 45 00 
 727 05c2 01          	.sleb128 1
 728 05c3 0A          	.uleb128 0xa
 729 05c4 50 4F 57 45 	.asciz "POWERED_STATE"
 729      52 45 44 5F 
MPLAB XC16 ASSEMBLY Listing:   			page 20


 729      53 54 41 54 
 729      45 00 
 730 05d2 02          	.sleb128 2
 731 05d3 0A          	.uleb128 0xa
 732 05d4 44 45 46 41 	.asciz "DEFAULT_STATE"
 732      55 4C 54 5F 
 732      53 54 41 54 
 732      45 00 
 733 05e2 04          	.sleb128 4
 734 05e3 0A          	.uleb128 0xa
 735 05e4 41 44 52 5F 	.asciz "ADR_PENDING_STATE"
 735      50 45 4E 44 
 735      49 4E 47 5F 
 735      53 54 41 54 
 735      45 00 
 736 05f6 08          	.sleb128 8
 737 05f7 0A          	.uleb128 0xa
 738 05f8 41 44 44 52 	.asciz "ADDRESS_STATE"
 738      45 53 53 5F 
 738      53 54 41 54 
 738      45 00 
 739 0606 10          	.sleb128 16
 740 0607 0A          	.uleb128 0xa
 741 0608 43 4F 4E 46 	.asciz "CONFIGURED_STATE"
 741      49 47 55 52 
 741      45 44 5F 53 
 741      54 41 54 45 
 741      00 
 742 0619 20          	.sleb128 32
 743 061a 00          	.byte 0x0
 744 061b 02          	.uleb128 0x2
 745 061c 55 53 42 5F 	.asciz "USB_DEVICE_STATE"
 745      44 45 56 49 
 745      43 45 5F 53 
 745      54 41 54 45 
 745      00 
 746 062d 07          	.byte 0x7
 747 062e 6E          	.byte 0x6e
 748 062f 99 05 00 00 	.4byte 0x599
 749 0633 05          	.uleb128 0x5
 750 0634 02          	.byte 0x2
 751 0635 67 01 00 00 	.4byte 0x167
 752 0639 0B          	.uleb128 0xb
 753 063a 01          	.byte 0x1
 754 063b 72 65 63 65 	.asciz "receive_handler_usb"
 754      69 76 65 5F 
 754      68 61 6E 64 
 754      6C 65 72 5F 
 754      75 73 62 00 
 755 064f 01          	.byte 0x1
 756 0650 12          	.byte 0x12
 757 0651 01          	.byte 0x1
 758 0652 00 00 00 00 	.4byte .LFB0
 759 0656 00 00 00 00 	.4byte .LFE0
 760 065a 01          	.byte 0x1
 761 065b 5E          	.byte 0x5e
 762 065c 76 06 00 00 	.4byte 0x676
MPLAB XC16 ASSEMBLY Listing:   			page 21


 763 0660 0C          	.uleb128 0xc
 764 0661 72 65 61 64 	.asciz "readString"
 764      53 74 72 69 
 764      6E 67 00 
 765 066c 01          	.byte 0x1
 766 066d 12          	.byte 0x12
 767 066e 59 01 00 00 	.4byte 0x159
 768 0672 02          	.byte 0x2
 769 0673 7E          	.byte 0x7e
 770 0674 00          	.sleb128 0
 771 0675 00          	.byte 0x0
 772 0676 0D          	.uleb128 0xd
 773 0677 01          	.byte 0x1
 774 0678 55 53 42 49 	.asciz "USBInit"
 774      6E 69 74 00 
 775 0680 01          	.byte 0x1
 776 0681 16          	.byte 0x16
 777 0682 00 00 00 00 	.4byte .LFB1
 778 0686 00 00 00 00 	.4byte .LFE1
 779 068a 01          	.byte 0x1
 780 068b 5E          	.byte 0x5e
 781 068c 0E          	.uleb128 0xe
 782 068d 01          	.byte 0x1
 783 068e 75 73 62 5F 	.asciz "usb_full"
 783      66 75 6C 6C 
 783      00 
 784 0697 01          	.byte 0x1
 785 0698 1F          	.byte 0x1f
 786 0699 90 05 00 00 	.4byte 0x590
 787 069d 00 00 00 00 	.4byte .LFB2
 788 06a1 00 00 00 00 	.4byte .LFE2
 789 06a5 01          	.byte 0x1
 790 06a6 5E          	.byte 0x5e
 791 06a7 0D          	.uleb128 0xd
 792 06a8 01          	.byte 0x1
 793 06a9 55 53 42 54 	.asciz "USBTasks"
 793      61 73 6B 73 
 793      00 
 794 06b2 01          	.byte 0x1
 795 06b3 23          	.byte 0x23
 796 06b4 00 00 00 00 	.4byte .LFB3
 797 06b8 00 00 00 00 	.4byte .LFE3
 798 06bc 01          	.byte 0x1
 799 06bd 5E          	.byte 0x5e
 800 06be 0B          	.uleb128 0xb
 801 06bf 01          	.byte 0x1
 802 06c0 61 70 70 65 	.asciz "append_string_usb"
 802      6E 64 5F 73 
 802      74 72 69 6E 
 802      67 5F 75 73 
 802      62 00 
 803 06d2 01          	.byte 0x1
 804 06d3 34          	.byte 0x34
 805 06d4 01          	.byte 0x1
 806 06d5 00 00 00 00 	.4byte .LFB4
 807 06d9 00 00 00 00 	.4byte .LFE4
 808 06dd 01          	.byte 0x1
MPLAB XC16 ASSEMBLY Listing:   			page 22


 809 06de 5E          	.byte 0x5e
 810 06df 0F 07 00 00 	.4byte 0x70f
 811 06e3 0C          	.uleb128 0xc
 812 06e4 62 75 66 66 	.asciz "buffer"
 812      65 72 00 
 813 06eb 01          	.byte 0x1
 814 06ec 34          	.byte 0x34
 815 06ed 33 06 00 00 	.4byte 0x633
 816 06f1 02          	.byte 0x2
 817 06f2 7E          	.byte 0x7e
 818 06f3 02          	.sleb128 2
 819 06f4 0C          	.uleb128 0xc
 820 06f5 6C 65 6E 00 	.asciz "len"
 821 06f9 01          	.byte 0x1
 822 06fa 34          	.byte 0x34
 823 06fb 2B 01 00 00 	.4byte 0x12b
 824 06ff 02          	.byte 0x2
 825 0700 7E          	.byte 0x7e
 826 0701 04          	.sleb128 4
 827 0702 0F          	.uleb128 0xf
 828 0703 69 00       	.asciz "i"
 829 0705 01          	.byte 0x1
 830 0706 35          	.byte 0x35
 831 0707 FA 00 00 00 	.4byte 0xfa
 832 070b 02          	.byte 0x2
 833 070c 7E          	.byte 0x7e
 834 070d 00          	.sleb128 0
 835 070e 00          	.byte 0x0
 836 070f 10          	.uleb128 0x10
 837 0710 01          	.byte 0x1
 838 0711 75 70 72 69 	.asciz "uprintf"
 838      6E 74 66 00 
 839 0719 01          	.byte 0x1
 840 071a 56          	.byte 0x56
 841 071b 01          	.byte 0x1
 842 071c 0A 01 00 00 	.4byte 0x10a
 843 0720 00 00 00 00 	.4byte .LFB5
 844 0724 00 00 00 00 	.4byte .LFE5
 845 0728 01          	.byte 0x1
 846 0729 5E          	.byte 0x5e
 847 072a 71 07 00 00 	.4byte 0x771
 848 072e 0C          	.uleb128 0xc
 849 072f 66 6F 72 6D 	.asciz "format"
 849      61 74 00 
 850 0736 01          	.byte 0x1
 851 0737 56          	.byte 0x56
 852 0738 59 01 00 00 	.4byte 0x159
 853 073c 02          	.byte 0x2
 854 073d 7E          	.byte 0x7e
 855 073e 78          	.sleb128 -8
 856 073f 11          	.uleb128 0x11
 857 0740 0F          	.uleb128 0xf
 858 0741 61 70 74 72 	.asciz "aptr"
 858      00 
 859 0746 01          	.byte 0x1
 860 0747 58          	.byte 0x58
 861 0748 A8 00 00 00 	.4byte 0xa8
MPLAB XC16 ASSEMBLY Listing:   			page 23


 862 074c 02          	.byte 0x2
 863 074d 7E          	.byte 0x7e
 864 074e 00          	.sleb128 0
 865 074f 0F          	.uleb128 0xf
 866 0750 62 75 66 66 	.asciz "buffer"
 866      65 72 00 
 867 0757 01          	.byte 0x1
 868 0758 5C          	.byte 0x5c
 869 0759 81 07 00 00 	.4byte 0x781
 870 075d 02          	.byte 0x2
 871 075e 7E          	.byte 0x7e
 872 075f 04          	.sleb128 4
 873 0760 0F          	.uleb128 0xf
 874 0761 62 73 69 7A 	.asciz "bsize"
 874      65 00 
 875 0767 01          	.byte 0x1
 876 0768 62          	.byte 0x62
 877 0769 0A 01 00 00 	.4byte 0x10a
 878 076d 02          	.byte 0x2
 879 076e 7E          	.byte 0x7e
 880 076f 02          	.sleb128 2
 881 0770 00          	.byte 0x0
 882 0771 12          	.uleb128 0x12
 883 0772 5F 01 00 00 	.4byte 0x15f
 884 0776 81 07 00 00 	.4byte 0x781
 885 077a 13          	.uleb128 0x13
 886 077b FA 00 00 00 	.4byte 0xfa
 887 077f FF          	.byte 0xff
 888 0780 00          	.byte 0x0
 889 0781 14          	.uleb128 0x14
 890 0782 71 07 00 00 	.4byte 0x771
 891 0786 15          	.uleb128 0x15
 892 0787 00 00 00 00 	.4byte .LASF0
 893 078b 05          	.byte 0x5
 894 078c CA 3B       	.2byte 0x3bca
 895 078e 94 07 00 00 	.4byte 0x794
 896 0792 01          	.byte 0x1
 897 0793 01          	.byte 0x1
 898 0794 16          	.uleb128 0x16
 899 0795 51 02 00 00 	.4byte 0x251
 900 0799 15          	.uleb128 0x15
 901 079a 00 00 00 00 	.4byte .LASF1
 902 079e 07          	.byte 0x7
 903 079f FE 07       	.2byte 0x7fe
 904 07a1 A7 07 00 00 	.4byte 0x7a7
 905 07a5 01          	.byte 0x1
 906 07a6 01          	.byte 0x1
 907 07a7 16          	.uleb128 0x16
 908 07a8 1B 06 00 00 	.4byte 0x61b
 909 07ac 12          	.uleb128 0x12
 910 07ad 67 01 00 00 	.4byte 0x167
 911 07b1 C2 07 00 00 	.4byte 0x7c2
 912 07b5 13          	.uleb128 0x13
 913 07b6 FA 00 00 00 	.4byte 0xfa
 914 07ba 3F          	.byte 0x3f
 915 07bb 13          	.uleb128 0x13
 916 07bc FA 00 00 00 	.4byte 0xfa
MPLAB XC16 ASSEMBLY Listing:   			page 24


 917 07c0 FF          	.byte 0xff
 918 07c1 00          	.byte 0x0
 919 07c2 17          	.uleb128 0x17
 920 07c3 00 00 00 00 	.4byte .LASF2
 921 07c7 08          	.byte 0x8
 922 07c8 12          	.byte 0x12
 923 07c9 AC 07 00 00 	.4byte 0x7ac
 924 07cd 01          	.byte 0x1
 925 07ce 01          	.byte 0x1
 926 07cf 12          	.uleb128 0x12
 927 07d0 67 01 00 00 	.4byte 0x167
 928 07d4 DF 07 00 00 	.4byte 0x7df
 929 07d8 13          	.uleb128 0x13
 930 07d9 FA 00 00 00 	.4byte 0xfa
 931 07dd 3F          	.byte 0x3f
 932 07de 00          	.byte 0x0
 933 07df 17          	.uleb128 0x17
 934 07e0 00 00 00 00 	.4byte .LASF3
 935 07e4 08          	.byte 0x8
 936 07e5 13          	.byte 0x13
 937 07e6 CF 07 00 00 	.4byte 0x7cf
 938 07ea 01          	.byte 0x1
 939 07eb 01          	.byte 0x1
 940 07ec 17          	.uleb128 0x17
 941 07ed 00 00 00 00 	.4byte .LASF4
 942 07f1 08          	.byte 0x8
 943 07f2 14          	.byte 0x14
 944 07f3 F9 07 00 00 	.4byte 0x7f9
 945 07f7 01          	.byte 0x1
 946 07f8 01          	.byte 0x1
 947 07f9 16          	.uleb128 0x16
 948 07fa FA 00 00 00 	.4byte 0xfa
 949 07fe 17          	.uleb128 0x17
 950 07ff 00 00 00 00 	.4byte .LASF5
 951 0803 08          	.byte 0x8
 952 0804 15          	.byte 0x15
 953 0805 F9 07 00 00 	.4byte 0x7f9
 954 0809 01          	.byte 0x1
 955 080a 01          	.byte 0x1
 956 080b 17          	.uleb128 0x17
 957 080c 00 00 00 00 	.4byte .LASF6
 958 0810 08          	.byte 0x8
 959 0811 16          	.byte 0x16
 960 0812 F9 07 00 00 	.4byte 0x7f9
 961 0816 01          	.byte 0x1
 962 0817 01          	.byte 0x1
 963 0818 15          	.uleb128 0x15
 964 0819 00 00 00 00 	.4byte .LASF0
 965 081d 05          	.byte 0x5
 966 081e CA 3B       	.2byte 0x3bca
 967 0820 94 07 00 00 	.4byte 0x794
 968 0824 01          	.byte 0x1
 969 0825 01          	.byte 0x1
 970 0826 15          	.uleb128 0x15
 971 0827 00 00 00 00 	.4byte .LASF1
 972 082b 07          	.byte 0x7
 973 082c FE 07       	.2byte 0x7fe
MPLAB XC16 ASSEMBLY Listing:   			page 25


 974 082e A7 07 00 00 	.4byte 0x7a7
 975 0832 01          	.byte 0x1
 976 0833 01          	.byte 0x1
 977 0834 18          	.uleb128 0x18
 978 0835 00 00 00 00 	.4byte .LASF2
 979 0839 01          	.byte 0x1
 980 083a 0F          	.byte 0xf
 981 083b AC 07 00 00 	.4byte 0x7ac
 982 083f 01          	.byte 0x1
 983 0840 05          	.byte 0x5
 984 0841 03          	.byte 0x3
 985 0842 00 00 00 00 	.4byte _gpacket_buffer_usb
 986 0846 18          	.uleb128 0x18
 987 0847 00 00 00 00 	.4byte .LASF3
 988 084b 01          	.byte 0x1
 989 084c 10          	.byte 0x10
 990 084d CF 07 00 00 	.4byte 0x7cf
 991 0851 01          	.byte 0x1
 992 0852 05          	.byte 0x5
 993 0853 03          	.byte 0x3
 994 0854 00 00 00 00 	.4byte _gpacket_last_pos_usb
 995 0858 18          	.uleb128 0x18
 996 0859 00 00 00 00 	.4byte .LASF4
 997 085d 01          	.byte 0x1
 998 085e 0B          	.byte 0xb
 999 085f F9 07 00 00 	.4byte 0x7f9
 1000 0863 01          	.byte 0x1
 1001 0864 05          	.byte 0x5
 1002 0865 03          	.byte 0x3
 1003 0866 00 00 00 00 	.4byte _write_index_usb
 1004 086a 18          	.uleb128 0x18
 1005 086b 00 00 00 00 	.4byte .LASF5
 1006 086f 01          	.byte 0x1
 1007 0870 0C          	.byte 0xc
 1008 0871 F9 07 00 00 	.4byte 0x7f9
 1009 0875 01          	.byte 0x1
 1010 0876 05          	.byte 0x5
 1011 0877 03          	.byte 0x3
 1012 0878 00 00 00 00 	.4byte _read_index_usb
 1013 087c 18          	.uleb128 0x18
 1014 087d 00 00 00 00 	.4byte .LASF6
 1015 0881 01          	.byte 0x1
 1016 0882 0D          	.byte 0xd
 1017 0883 F9 07 00 00 	.4byte 0x7f9
 1018 0887 01          	.byte 0x1
 1019 0888 05          	.byte 0x5
 1020 0889 03          	.byte 0x3
 1021 088a 00 00 00 00 	.4byte _inner_write_index_usb
 1022 088e 00          	.byte 0x0
 1023                 	.section .debug_abbrev,info
 1024 0000 01          	.uleb128 0x1
 1025 0001 11          	.uleb128 0x11
 1026 0002 01          	.byte 0x1
 1027 0003 25          	.uleb128 0x25
 1028 0004 08          	.uleb128 0x8
 1029 0005 13          	.uleb128 0x13
 1030 0006 0B          	.uleb128 0xb
MPLAB XC16 ASSEMBLY Listing:   			page 26


 1031 0007 03          	.uleb128 0x3
 1032 0008 08          	.uleb128 0x8
 1033 0009 1B          	.uleb128 0x1b
 1034 000a 08          	.uleb128 0x8
 1035 000b 11          	.uleb128 0x11
 1036 000c 01          	.uleb128 0x1
 1037 000d 12          	.uleb128 0x12
 1038 000e 01          	.uleb128 0x1
 1039 000f 10          	.uleb128 0x10
 1040 0010 06          	.uleb128 0x6
 1041 0011 00          	.byte 0x0
 1042 0012 00          	.byte 0x0
 1043 0013 02          	.uleb128 0x2
 1044 0014 16          	.uleb128 0x16
 1045 0015 00          	.byte 0x0
 1046 0016 03          	.uleb128 0x3
 1047 0017 08          	.uleb128 0x8
 1048 0018 3A          	.uleb128 0x3a
 1049 0019 0B          	.uleb128 0xb
 1050 001a 3B          	.uleb128 0x3b
 1051 001b 0B          	.uleb128 0xb
 1052 001c 49          	.uleb128 0x49
 1053 001d 13          	.uleb128 0x13
 1054 001e 00          	.byte 0x0
 1055 001f 00          	.byte 0x0
 1056 0020 03          	.uleb128 0x3
 1057 0021 0F          	.uleb128 0xf
 1058 0022 00          	.byte 0x0
 1059 0023 0B          	.uleb128 0xb
 1060 0024 0B          	.uleb128 0xb
 1061 0025 00          	.byte 0x0
 1062 0026 00          	.byte 0x0
 1063 0027 04          	.uleb128 0x4
 1064 0028 24          	.uleb128 0x24
 1065 0029 00          	.byte 0x0
 1066 002a 0B          	.uleb128 0xb
 1067 002b 0B          	.uleb128 0xb
 1068 002c 3E          	.uleb128 0x3e
 1069 002d 0B          	.uleb128 0xb
 1070 002e 03          	.uleb128 0x3
 1071 002f 08          	.uleb128 0x8
 1072 0030 00          	.byte 0x0
 1073 0031 00          	.byte 0x0
 1074 0032 05          	.uleb128 0x5
 1075 0033 0F          	.uleb128 0xf
 1076 0034 00          	.byte 0x0
 1077 0035 0B          	.uleb128 0xb
 1078 0036 0B          	.uleb128 0xb
 1079 0037 49          	.uleb128 0x49
 1080 0038 13          	.uleb128 0x13
 1081 0039 00          	.byte 0x0
 1082 003a 00          	.byte 0x0
 1083 003b 06          	.uleb128 0x6
 1084 003c 13          	.uleb128 0x13
 1085 003d 01          	.byte 0x1
 1086 003e 03          	.uleb128 0x3
 1087 003f 08          	.uleb128 0x8
MPLAB XC16 ASSEMBLY Listing:   			page 27


 1088 0040 0B          	.uleb128 0xb
 1089 0041 0B          	.uleb128 0xb
 1090 0042 3A          	.uleb128 0x3a
 1091 0043 0B          	.uleb128 0xb
 1092 0044 3B          	.uleb128 0x3b
 1093 0045 05          	.uleb128 0x5
 1094 0046 01          	.uleb128 0x1
 1095 0047 13          	.uleb128 0x13
 1096 0048 00          	.byte 0x0
 1097 0049 00          	.byte 0x0
 1098 004a 07          	.uleb128 0x7
 1099 004b 0D          	.uleb128 0xd
 1100 004c 00          	.byte 0x0
 1101 004d 03          	.uleb128 0x3
 1102 004e 08          	.uleb128 0x8
 1103 004f 3A          	.uleb128 0x3a
 1104 0050 0B          	.uleb128 0xb
 1105 0051 3B          	.uleb128 0x3b
 1106 0052 05          	.uleb128 0x5
 1107 0053 49          	.uleb128 0x49
 1108 0054 13          	.uleb128 0x13
 1109 0055 0B          	.uleb128 0xb
 1110 0056 0B          	.uleb128 0xb
 1111 0057 0D          	.uleb128 0xd
 1112 0058 0B          	.uleb128 0xb
 1113 0059 0C          	.uleb128 0xc
 1114 005a 0B          	.uleb128 0xb
 1115 005b 38          	.uleb128 0x38
 1116 005c 0A          	.uleb128 0xa
 1117 005d 00          	.byte 0x0
 1118 005e 00          	.byte 0x0
 1119 005f 08          	.uleb128 0x8
 1120 0060 16          	.uleb128 0x16
 1121 0061 00          	.byte 0x0
 1122 0062 03          	.uleb128 0x3
 1123 0063 08          	.uleb128 0x8
 1124 0064 3A          	.uleb128 0x3a
 1125 0065 0B          	.uleb128 0xb
 1126 0066 3B          	.uleb128 0x3b
 1127 0067 05          	.uleb128 0x5
 1128 0068 49          	.uleb128 0x49
 1129 0069 13          	.uleb128 0x13
 1130 006a 00          	.byte 0x0
 1131 006b 00          	.byte 0x0
 1132 006c 09          	.uleb128 0x9
 1133 006d 04          	.uleb128 0x4
 1134 006e 01          	.byte 0x1
 1135 006f 0B          	.uleb128 0xb
 1136 0070 0B          	.uleb128 0xb
 1137 0071 3A          	.uleb128 0x3a
 1138 0072 0B          	.uleb128 0xb
 1139 0073 3B          	.uleb128 0x3b
 1140 0074 0B          	.uleb128 0xb
 1141 0075 01          	.uleb128 0x1
 1142 0076 13          	.uleb128 0x13
 1143 0077 00          	.byte 0x0
 1144 0078 00          	.byte 0x0
MPLAB XC16 ASSEMBLY Listing:   			page 28


 1145 0079 0A          	.uleb128 0xa
 1146 007a 28          	.uleb128 0x28
 1147 007b 00          	.byte 0x0
 1148 007c 03          	.uleb128 0x3
 1149 007d 08          	.uleb128 0x8
 1150 007e 1C          	.uleb128 0x1c
 1151 007f 0D          	.uleb128 0xd
 1152 0080 00          	.byte 0x0
 1153 0081 00          	.byte 0x0
 1154 0082 0B          	.uleb128 0xb
 1155 0083 2E          	.uleb128 0x2e
 1156 0084 01          	.byte 0x1
 1157 0085 3F          	.uleb128 0x3f
 1158 0086 0C          	.uleb128 0xc
 1159 0087 03          	.uleb128 0x3
 1160 0088 08          	.uleb128 0x8
 1161 0089 3A          	.uleb128 0x3a
 1162 008a 0B          	.uleb128 0xb
 1163 008b 3B          	.uleb128 0x3b
 1164 008c 0B          	.uleb128 0xb
 1165 008d 27          	.uleb128 0x27
 1166 008e 0C          	.uleb128 0xc
 1167 008f 11          	.uleb128 0x11
 1168 0090 01          	.uleb128 0x1
 1169 0091 12          	.uleb128 0x12
 1170 0092 01          	.uleb128 0x1
 1171 0093 40          	.uleb128 0x40
 1172 0094 0A          	.uleb128 0xa
 1173 0095 01          	.uleb128 0x1
 1174 0096 13          	.uleb128 0x13
 1175 0097 00          	.byte 0x0
 1176 0098 00          	.byte 0x0
 1177 0099 0C          	.uleb128 0xc
 1178 009a 05          	.uleb128 0x5
 1179 009b 00          	.byte 0x0
 1180 009c 03          	.uleb128 0x3
 1181 009d 08          	.uleb128 0x8
 1182 009e 3A          	.uleb128 0x3a
 1183 009f 0B          	.uleb128 0xb
 1184 00a0 3B          	.uleb128 0x3b
 1185 00a1 0B          	.uleb128 0xb
 1186 00a2 49          	.uleb128 0x49
 1187 00a3 13          	.uleb128 0x13
 1188 00a4 02          	.uleb128 0x2
 1189 00a5 0A          	.uleb128 0xa
 1190 00a6 00          	.byte 0x0
 1191 00a7 00          	.byte 0x0
 1192 00a8 0D          	.uleb128 0xd
 1193 00a9 2E          	.uleb128 0x2e
 1194 00aa 00          	.byte 0x0
 1195 00ab 3F          	.uleb128 0x3f
 1196 00ac 0C          	.uleb128 0xc
 1197 00ad 03          	.uleb128 0x3
 1198 00ae 08          	.uleb128 0x8
 1199 00af 3A          	.uleb128 0x3a
 1200 00b0 0B          	.uleb128 0xb
 1201 00b1 3B          	.uleb128 0x3b
MPLAB XC16 ASSEMBLY Listing:   			page 29


 1202 00b2 0B          	.uleb128 0xb
 1203 00b3 11          	.uleb128 0x11
 1204 00b4 01          	.uleb128 0x1
 1205 00b5 12          	.uleb128 0x12
 1206 00b6 01          	.uleb128 0x1
 1207 00b7 40          	.uleb128 0x40
 1208 00b8 0A          	.uleb128 0xa
 1209 00b9 00          	.byte 0x0
 1210 00ba 00          	.byte 0x0
 1211 00bb 0E          	.uleb128 0xe
 1212 00bc 2E          	.uleb128 0x2e
 1213 00bd 00          	.byte 0x0
 1214 00be 3F          	.uleb128 0x3f
 1215 00bf 0C          	.uleb128 0xc
 1216 00c0 03          	.uleb128 0x3
 1217 00c1 08          	.uleb128 0x8
 1218 00c2 3A          	.uleb128 0x3a
 1219 00c3 0B          	.uleb128 0xb
 1220 00c4 3B          	.uleb128 0x3b
 1221 00c5 0B          	.uleb128 0xb
 1222 00c6 49          	.uleb128 0x49
 1223 00c7 13          	.uleb128 0x13
 1224 00c8 11          	.uleb128 0x11
 1225 00c9 01          	.uleb128 0x1
 1226 00ca 12          	.uleb128 0x12
 1227 00cb 01          	.uleb128 0x1
 1228 00cc 40          	.uleb128 0x40
 1229 00cd 0A          	.uleb128 0xa
 1230 00ce 00          	.byte 0x0
 1231 00cf 00          	.byte 0x0
 1232 00d0 0F          	.uleb128 0xf
 1233 00d1 34          	.uleb128 0x34
 1234 00d2 00          	.byte 0x0
 1235 00d3 03          	.uleb128 0x3
 1236 00d4 08          	.uleb128 0x8
 1237 00d5 3A          	.uleb128 0x3a
 1238 00d6 0B          	.uleb128 0xb
 1239 00d7 3B          	.uleb128 0x3b
 1240 00d8 0B          	.uleb128 0xb
 1241 00d9 49          	.uleb128 0x49
 1242 00da 13          	.uleb128 0x13
 1243 00db 02          	.uleb128 0x2
 1244 00dc 0A          	.uleb128 0xa
 1245 00dd 00          	.byte 0x0
 1246 00de 00          	.byte 0x0
 1247 00df 10          	.uleb128 0x10
 1248 00e0 2E          	.uleb128 0x2e
 1249 00e1 01          	.byte 0x1
 1250 00e2 3F          	.uleb128 0x3f
 1251 00e3 0C          	.uleb128 0xc
 1252 00e4 03          	.uleb128 0x3
 1253 00e5 08          	.uleb128 0x8
 1254 00e6 3A          	.uleb128 0x3a
 1255 00e7 0B          	.uleb128 0xb
 1256 00e8 3B          	.uleb128 0x3b
 1257 00e9 0B          	.uleb128 0xb
 1258 00ea 27          	.uleb128 0x27
MPLAB XC16 ASSEMBLY Listing:   			page 30


 1259 00eb 0C          	.uleb128 0xc
 1260 00ec 49          	.uleb128 0x49
 1261 00ed 13          	.uleb128 0x13
 1262 00ee 11          	.uleb128 0x11
 1263 00ef 01          	.uleb128 0x1
 1264 00f0 12          	.uleb128 0x12
 1265 00f1 01          	.uleb128 0x1
 1266 00f2 40          	.uleb128 0x40
 1267 00f3 0A          	.uleb128 0xa
 1268 00f4 01          	.uleb128 0x1
 1269 00f5 13          	.uleb128 0x13
 1270 00f6 00          	.byte 0x0
 1271 00f7 00          	.byte 0x0
 1272 00f8 11          	.uleb128 0x11
 1273 00f9 18          	.uleb128 0x18
 1274 00fa 00          	.byte 0x0
 1275 00fb 00          	.byte 0x0
 1276 00fc 00          	.byte 0x0
 1277 00fd 12          	.uleb128 0x12
 1278 00fe 01          	.uleb128 0x1
 1279 00ff 01          	.byte 0x1
 1280 0100 49          	.uleb128 0x49
 1281 0101 13          	.uleb128 0x13
 1282 0102 01          	.uleb128 0x1
 1283 0103 13          	.uleb128 0x13
 1284 0104 00          	.byte 0x0
 1285 0105 00          	.byte 0x0
 1286 0106 13          	.uleb128 0x13
 1287 0107 21          	.uleb128 0x21
 1288 0108 00          	.byte 0x0
 1289 0109 49          	.uleb128 0x49
 1290 010a 13          	.uleb128 0x13
 1291 010b 2F          	.uleb128 0x2f
 1292 010c 0B          	.uleb128 0xb
 1293 010d 00          	.byte 0x0
 1294 010e 00          	.byte 0x0
 1295 010f 14          	.uleb128 0x14
 1296 0110 26          	.uleb128 0x26
 1297 0111 00          	.byte 0x0
 1298 0112 49          	.uleb128 0x49
 1299 0113 13          	.uleb128 0x13
 1300 0114 00          	.byte 0x0
 1301 0115 00          	.byte 0x0
 1302 0116 15          	.uleb128 0x15
 1303 0117 34          	.uleb128 0x34
 1304 0118 00          	.byte 0x0
 1305 0119 03          	.uleb128 0x3
 1306 011a 0E          	.uleb128 0xe
 1307 011b 3A          	.uleb128 0x3a
 1308 011c 0B          	.uleb128 0xb
 1309 011d 3B          	.uleb128 0x3b
 1310 011e 05          	.uleb128 0x5
 1311 011f 49          	.uleb128 0x49
 1312 0120 13          	.uleb128 0x13
 1313 0121 3F          	.uleb128 0x3f
 1314 0122 0C          	.uleb128 0xc
 1315 0123 3C          	.uleb128 0x3c
MPLAB XC16 ASSEMBLY Listing:   			page 31


 1316 0124 0C          	.uleb128 0xc
 1317 0125 00          	.byte 0x0
 1318 0126 00          	.byte 0x0
 1319 0127 16          	.uleb128 0x16
 1320 0128 35          	.uleb128 0x35
 1321 0129 00          	.byte 0x0
 1322 012a 49          	.uleb128 0x49
 1323 012b 13          	.uleb128 0x13
 1324 012c 00          	.byte 0x0
 1325 012d 00          	.byte 0x0
 1326 012e 17          	.uleb128 0x17
 1327 012f 34          	.uleb128 0x34
 1328 0130 00          	.byte 0x0
 1329 0131 03          	.uleb128 0x3
 1330 0132 0E          	.uleb128 0xe
 1331 0133 3A          	.uleb128 0x3a
 1332 0134 0B          	.uleb128 0xb
 1333 0135 3B          	.uleb128 0x3b
 1334 0136 0B          	.uleb128 0xb
 1335 0137 49          	.uleb128 0x49
 1336 0138 13          	.uleb128 0x13
 1337 0139 3F          	.uleb128 0x3f
 1338 013a 0C          	.uleb128 0xc
 1339 013b 3C          	.uleb128 0x3c
 1340 013c 0C          	.uleb128 0xc
 1341 013d 00          	.byte 0x0
 1342 013e 00          	.byte 0x0
 1343 013f 18          	.uleb128 0x18
 1344 0140 34          	.uleb128 0x34
 1345 0141 00          	.byte 0x0
 1346 0142 03          	.uleb128 0x3
 1347 0143 0E          	.uleb128 0xe
 1348 0144 3A          	.uleb128 0x3a
 1349 0145 0B          	.uleb128 0xb
 1350 0146 3B          	.uleb128 0x3b
 1351 0147 0B          	.uleb128 0xb
 1352 0148 49          	.uleb128 0x49
 1353 0149 13          	.uleb128 0x13
 1354 014a 3F          	.uleb128 0x3f
 1355 014b 0C          	.uleb128 0xc
 1356 014c 02          	.uleb128 0x2
 1357 014d 0A          	.uleb128 0xa
 1358 014e 00          	.byte 0x0
 1359 014f 00          	.byte 0x0
 1360 0150 00          	.byte 0x0
 1361                 	.section .debug_pubnames,info
 1362 0000 DF 00 00 00 	.4byte 0xdf
 1363 0004 02 00       	.2byte 0x2
 1364 0006 00 00 00 00 	.4byte .Ldebug_info0
 1365 000a 8F 08 00 00 	.4byte 0x88f
 1366 000e 39 06 00 00 	.4byte 0x639
 1367 0012 72 65 63 65 	.asciz "receive_handler_usb"
 1367      69 76 65 5F 
 1367      68 61 6E 64 
 1367      6C 65 72 5F 
 1367      75 73 62 00 
 1368 0026 76 06 00 00 	.4byte 0x676
MPLAB XC16 ASSEMBLY Listing:   			page 32


 1369 002a 55 53 42 49 	.asciz "USBInit"
 1369      6E 69 74 00 
 1370 0032 8C 06 00 00 	.4byte 0x68c
 1371 0036 75 73 62 5F 	.asciz "usb_full"
 1371      66 75 6C 6C 
 1371      00 
 1372 003f A7 06 00 00 	.4byte 0x6a7
 1373 0043 55 53 42 54 	.asciz "USBTasks"
 1373      61 73 6B 73 
 1373      00 
 1374 004c BE 06 00 00 	.4byte 0x6be
 1375 0050 61 70 70 65 	.asciz "append_string_usb"
 1375      6E 64 5F 73 
 1375      74 72 69 6E 
 1375      67 5F 75 73 
 1375      62 00 
 1376 0062 0F 07 00 00 	.4byte 0x70f
 1377 0066 75 70 72 69 	.asciz "uprintf"
 1377      6E 74 66 00 
 1378 006e 34 08 00 00 	.4byte 0x834
 1379 0072 67 70 61 63 	.asciz "gpacket_buffer_usb"
 1379      6B 65 74 5F 
 1379      62 75 66 66 
 1379      65 72 5F 75 
 1379      73 62 00 
 1380 0085 46 08 00 00 	.4byte 0x846
 1381 0089 67 70 61 63 	.asciz "gpacket_last_pos_usb"
 1381      6B 65 74 5F 
 1381      6C 61 73 74 
 1381      5F 70 6F 73 
 1381      5F 75 73 62 
 1381      00 
 1382 009e 58 08 00 00 	.4byte 0x858
 1383 00a2 77 72 69 74 	.asciz "write_index_usb"
 1383      65 5F 69 6E 
 1383      64 65 78 5F 
 1383      75 73 62 00 
 1384 00b2 6A 08 00 00 	.4byte 0x86a
 1385 00b6 72 65 61 64 	.asciz "read_index_usb"
 1385      5F 69 6E 64 
 1385      65 78 5F 75 
 1385      73 62 00 
 1386 00c5 7C 08 00 00 	.4byte 0x87c
 1387 00c9 69 6E 6E 65 	.asciz "inner_write_index_usb"
 1387      72 5F 77 72 
 1387      69 74 65 5F 
 1387      69 6E 64 65 
 1387      78 5F 75 73 
 1387      62 00 
 1388 00df 00 00 00 00 	.4byte 0x0
 1389                 	.section .debug_pubtypes,info
 1390 0000 7B 00 00 00 	.4byte 0x7b
 1391 0004 02 00       	.2byte 0x2
 1392 0006 00 00 00 00 	.4byte .Ldebug_info0
 1393 000a 8F 08 00 00 	.4byte 0x88f
 1394 000e A8 00 00 00 	.4byte 0xa8
 1395 0012 76 61 5F 6C 	.asciz "va_list"
MPLAB XC16 ASSEMBLY Listing:   			page 33


 1395      69 73 74 00 
 1396 001a 11 01 00 00 	.4byte 0x111
 1397 001e 5F 53 69 7A 	.asciz "_Sizet"
 1397      65 74 00 
 1398 0025 2B 01 00 00 	.4byte 0x12b
 1399 0029 73 69 7A 65 	.asciz "size_t"
 1399      5F 74 00 
 1400 0030 67 01 00 00 	.4byte 0x167
 1401 0034 75 69 6E 74 	.asciz "uint8_t"
 1401      38 5F 74 00 
 1402 003c 76 01 00 00 	.4byte 0x176
 1403 0040 75 69 6E 74 	.asciz "uint16_t"
 1403      31 36 5F 74 
 1403      00 
 1404 0049 9B 01 00 00 	.4byte 0x19b
 1405 004d 74 61 67 4C 	.asciz "tagLATEBITS"
 1405      41 54 45 42 
 1405      49 54 53 00 
 1406 0059 51 02 00 00 	.4byte 0x251
 1407 005d 4C 41 54 45 	.asciz "LATEBITS"
 1407      42 49 54 53 
 1407      00 
 1408 0066 1B 06 00 00 	.4byte 0x61b
 1409 006a 55 53 42 5F 	.asciz "USB_DEVICE_STATE"
 1409      44 45 56 49 
 1409      43 45 5F 53 
 1409      54 41 54 45 
 1409      00 
 1410 007b 00 00 00 00 	.4byte 0x0
 1411                 	.section .debug_aranges,info
 1412 0000 14 00 00 00 	.4byte 0x14
 1413 0004 02 00       	.2byte 0x2
 1414 0006 00 00 00 00 	.4byte .Ldebug_info0
 1415 000a 04          	.byte 0x4
 1416 000b 00          	.byte 0x0
 1417 000c 00 00       	.2byte 0x0
 1418 000e 00 00       	.2byte 0x0
 1419 0010 00 00 00 00 	.4byte 0x0
 1420 0014 00 00 00 00 	.4byte 0x0
 1421                 	.section .debug_str,info
 1422                 	.LASF2:
 1423 0000 67 70 61 63 	.asciz "gpacket_buffer_usb"
 1423      6B 65 74 5F 
 1423      62 75 66 66 
 1423      65 72 5F 75 
 1423      73 62 00 
 1424                 	.LASF0:
 1425 0013 4C 41 54 45 	.asciz "LATEbits"
 1425      62 69 74 73 
 1425      00 
 1426                 	.LASF4:
 1427 001c 77 72 69 74 	.asciz "write_index_usb"
 1427      65 5F 69 6E 
 1427      64 65 78 5F 
 1427      75 73 62 00 
 1428                 	.LASF5:
 1429 002c 72 65 61 64 	.asciz "read_index_usb"
MPLAB XC16 ASSEMBLY Listing:   			page 34


 1429      5F 69 6E 64 
 1429      65 78 5F 75 
 1429      73 62 00 
 1430                 	.LASF1:
 1431 003b 55 53 42 44 	.asciz "USBDeviceState"
 1431      65 76 69 63 
 1431      65 53 74 61 
 1431      74 65 00 
 1432                 	.LASF3:
 1433 004a 67 70 61 63 	.asciz "gpacket_last_pos_usb"
 1433      6B 65 74 5F 
 1433      6C 61 73 74 
 1433      5F 70 6F 73 
 1433      5F 75 73 62 
 1433      00 
 1434                 	.LASF6:
 1435 005f 69 6E 6E 65 	.asciz "inner_write_index_usb"
 1435      72 5F 77 72 
 1435      69 74 65 5F 
 1435      69 6E 64 65 
 1435      78 5F 75 73 
 1435      62 00 
 1436                 	.section .text,code
 1437              	
 1438              	
 1439              	
 1440              	.section __c30_info,info,bss
 1441                 	__psv_trap_errata:
 1442                 	
 1443                 	.section __c30_signature,info,data
 1444 0000 01 00       	.word 0x0001
 1445 0002 01 00       	.word 0x0001
 1446 0004 00 00       	.word 0x0000
 1447                 	
 1448                 	
 1449                 	
 1450                 	.set ___PA___,0
 1451                 	.end
MPLAB XC16 ASSEMBLY Listing:   			page 35


DEFINED SYMBOLS
                            *ABS*:00000000 /home/user/Documents/FST/Programming/project_sub-zero/lib/lib_pic33e/usb_lib.c
                            *ABS*:00000001 __33EP256MU806
                            *ABS*:00000001 __dsPIC33E
                            *ABS*:00000001 __HAS_DSP
                            *ABS*:00000001 __HAS_DMAV2
                            *ABS*:00000001 __HAS_CODEGUARD
                            *ABS*:00000001 __HAS_PMP_ENHANCED
                            *ABS*:00000001 __HAS_EDS
                            *ABS*:00000001 __HAS_EP
                            *ABS*:00000011 __TARGET_DIVIDE_CYCLES
                            *ABS*:00000001 __C30ELF
    {standard input}:15     .nbss:00000000 _write_index_usb
    {standard input}:22     .ndata:00000000 _read_index_usb
    {standard input}:29     .nbss:00000002 _inner_write_index_usb
    {standard input}:34     .bss:00000000 _gpacket_buffer_usb
    {standard input}:37     .bss:00004000 _gpacket_last_pos_usb
    {standard input}:42     .text:00000000 _receive_handler_usb
    {standard input}:46     *ABS*:00000000 ___PA___
    {standard input}:61     .text:0000000c _USBInit
    {standard input}:82     .text:0000001a _usb_full
    {standard input}:111    .text:0000003a _USBTasks
    {standard input}:121    *ABS*:00000000 ___BP___
    {standard input}:143    .text:00000054 _append_string_usb
    {standard input}:248    .text:000000cc _uprintf
    {standard input}:1441   __c30_info:00000000 __psv_trap_errata
    {standard input}:47     .text:00000000 .L0
                            .text:00000048 .L5
                            .text:0000004a .L6
                            .text:000000c4 .L13
                            .text:00000094 .L10
                            .text:000000ba .L11
                            .text:0000009a .L12
                            .text:000000c4 .L7
                            .text:000000ec .L15
                            .text:000000f4 .L16
                    .debug_abbrev:00000000 .Ldebug_abbrev0
                            .text:00000000 .Ltext0
                            .text:000000fc .Letext0
                      .debug_line:00000000 .Ldebug_line0
                            .text:00000000 .LFB0
                            .text:0000000c .LFE0
                            .text:0000000c .LFB1
                            .text:0000001a .LFE1
                            .text:0000001a .LFB2
                            .text:0000003a .LFE2
                            .text:0000003a .LFB3
                            .text:00000054 .LFE3
                            .text:00000054 .LFB4
                            .text:000000cc .LFE4
                            .text:000000cc .LFB5
                            .text:000000fc .LFE5
                       .debug_str:00000013 .LASF0
                       .debug_str:0000003b .LASF1
                       .debug_str:00000000 .LASF2
                       .debug_str:0000004a .LASF3
                       .debug_str:0000001c .LASF4
MPLAB XC16 ASSEMBLY Listing:   			page 36


                       .debug_str:0000002c .LASF5
                       .debug_str:0000005f .LASF6
                     .debug_frame:00000000 .Lframe0
                      .debug_info:00000000 .Ldebug_info0

UNDEFINED SYMBOLS
CORCON
_USBDeviceInit
_USBDeviceAttach
_USBDeviceState
_LATEbits
_APP_DeviceCDCBasicDemoTasks
_memset
_vsprintf
_strlen

EQUATE SYMBOLS
/home/user/Documents/FST/Programming/project_sub-zero/lib/lib_pic33e/usb_lib.c = 0x0
                  __33EP256MU806 = 0x1
                      __dsPIC33E = 0x1
                       __HAS_DSP = 0x1
                     __HAS_DMAV2 = 0x1
                 __HAS_CODEGUARD = 0x1
              __HAS_PMP_ENHANCED = 0x1
                       __HAS_EDS = 0x1
                        __HAS_EP = 0x1
          __TARGET_DIVIDE_CYCLES = 0x11
                        __C30ELF = 0x1
                        ___PA___ = 0x0
                        ___BP___ = 0x0
