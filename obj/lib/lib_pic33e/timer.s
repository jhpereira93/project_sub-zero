MPLAB XC16 ASSEMBLY Listing:   			page 1


   1              	.file "/home/user/Documents/FST/Programming/project_sub-zero/lib/lib_pic33e/timer.c"
   2              	.section .debug_abbrev,info
   3                 	.Ldebug_abbrev0:
   4                 	.section .debug_info,info
   5                 	.Ldebug_info0:
   6                 	.section .debug_line,info
   7                 	.Ldebug_line0:
   8 0000 40 03 00 00 	.section .text,code
   8      02 00 BB 00 
   8      00 00 01 01 
   8      FB 0E 0A 00 
   8      01 01 01 01 
   8      00 00 00 01 
   8      6C 69 62 2F 
   8      6C 69 62 5F 
   8      70 69 63 33 
   9              	.Ltext0:
  10              	.section .nbss,bss,near
  11                 	.type _fun_ptr_timer1,@object
  12                 	.global _fun_ptr_timer1
  13                 	.align 2
  14 0000 00 00       	_fun_ptr_timer1:.space 2
  15                 	.type _fun_ptr_timer2,@object
  16                 	.global _fun_ptr_timer2
  17                 	.align 2
  18 0002 00 00       	_fun_ptr_timer2:.space 2
  19                 	.type _fun_ptr_timer3,@object
  20                 	.global _fun_ptr_timer3
  21                 	.align 2
  22 0004 00 00       	_fun_ptr_timer3:.space 2
  23                 	.type _fun_ptr_timer4,@object
  24                 	.global _fun_ptr_timer4
  25                 	.align 2
  26 0006 00 00       	_fun_ptr_timer4:.space 2
  27                 	.type _fun_ptr_timer5,@object
  28                 	.global _fun_ptr_timer5
  29                 	.align 2
  30 0008 00 00       	_fun_ptr_timer5:.space 2
  31                 	.type _fun_ptr_timer6,@object
  32                 	.global _fun_ptr_timer6
  33                 	.align 2
  34 000a 00 00       	_fun_ptr_timer6:.space 2
  35                 	.type _fun_ptr_timer7,@object
  36                 	.global _fun_ptr_timer7
  37                 	.align 2
  38 000c 00 00       	_fun_ptr_timer7:.space 2
  39                 	.type _fun_ptr_timer8,@object
  40                 	.global _fun_ptr_timer8
  41                 	.align 2
  42 000e 00 00       	_fun_ptr_timer8:.space 2
  43                 	.type _fun_ptr_timer9,@object
  44                 	.global _fun_ptr_timer9
  45                 	.align 2
  46 0010 00 00       	_fun_ptr_timer9:.space 2
  47                 	.section .text,code
  48              	.align 2
  49              	.global _default_timer
MPLAB XC16 ASSEMBLY Listing:   			page 2


  50              	.type _default_timer,@function
  51              	_default_timer:
  52              	.LFB0:
  53              	.file 1 "lib/lib_pic33e/timer.c"
   1:lib/lib_pic33e/timer.c **** #ifndef NOTIMER
   2:lib/lib_pic33e/timer.c **** 
   3:lib/lib_pic33e/timer.c **** /**********************************************************************
   4:lib/lib_pic33e/timer.c ****  *   lib_pic30f
   5:lib/lib_pic33e/timer.c ****  *
   6:lib/lib_pic33e/timer.c ****  *   Timer
   7:lib/lib_pic33e/timer.c ****  *      - module configuration
   8:lib/lib_pic33e/timer.c ****  *      - interruption assignment
   9:lib/lib_pic33e/timer.c ****  *   ______________________________________________________________
  10:lib/lib_pic33e/timer.c ****  *
  11:lib/lib_pic33e/timer.c ****  *   Copyright 2014 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
  12:lib/lib_pic33e/timer.c ****  *
  13:lib/lib_pic33e/timer.c ****  *   This program is free software; you can redistribute it and/or
  14:lib/lib_pic33e/timer.c ****  *   modify it under the terms of the GNU General Public License
  15:lib/lib_pic33e/timer.c ****  *   as published by the Free Software Foundation; version 2 of the
  16:lib/lib_pic33e/timer.c ****  *   License only.
  17:lib/lib_pic33e/timer.c ****  *
  18:lib/lib_pic33e/timer.c ****  *   This program is distributed in the hope that it will be useful,
  19:lib/lib_pic33e/timer.c ****  *   but WITHOUT ANY WARRANTY; without even the implied warranty of
  20:lib/lib_pic33e/timer.c ****  *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  21:lib/lib_pic33e/timer.c ****  *   GNU General Public License for more details.
  22:lib/lib_pic33e/timer.c ****  *
  23:lib/lib_pic33e/timer.c ****  *   You should have received a copy of the GNU General Public License
  24:lib/lib_pic33e/timer.c ****  *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
  25:lib/lib_pic33e/timer.c ****  *
  26:lib/lib_pic33e/timer.c ****  **********************************************************************/
  27:lib/lib_pic33e/timer.c **** 
  28:lib/lib_pic33e/timer.c **** 
  29:lib/lib_pic33e/timer.c **** #include <xc.h>
  30:lib/lib_pic33e/timer.c **** #include <limits.h>
  31:lib/lib_pic33e/timer.c **** #include <stdlib.h>
  32:lib/lib_pic33e/timer.c **** #include "timer.h"
  33:lib/lib_pic33e/timer.c **** 
  34:lib/lib_pic33e/timer.c **** void (*fun_ptr_timer1)(void);
  35:lib/lib_pic33e/timer.c **** void (*fun_ptr_timer2)(void);
  36:lib/lib_pic33e/timer.c **** void (*fun_ptr_timer3)(void);
  37:lib/lib_pic33e/timer.c **** void (*fun_ptr_timer4)(void);
  38:lib/lib_pic33e/timer.c **** void (*fun_ptr_timer5)(void);
  39:lib/lib_pic33e/timer.c **** void (*fun_ptr_timer6)(void);
  40:lib/lib_pic33e/timer.c **** void (*fun_ptr_timer7)(void);
  41:lib/lib_pic33e/timer.c **** void (*fun_ptr_timer8)(void);
  42:lib/lib_pic33e/timer.c **** void (*fun_ptr_timer9)(void);
  43:lib/lib_pic33e/timer.c **** 
  44:lib/lib_pic33e/timer.c **** void default_timer(void) {
  54              	.loc 1 44 0
  55              	.set ___PA___,1
  56 000000  00 00 FA 	lnk #0
  57              	.LCFI0:
  45:lib/lib_pic33e/timer.c **** 	return;
  46:lib/lib_pic33e/timer.c **** }
  58              	.loc 1 46 0
  59 000002  8E 07 78 	mov w14,w15
  60 000004  4F 07 78 	mov [--w15],w14
MPLAB XC16 ASSEMBLY Listing:   			page 3


  61 000006  00 40 A9 	bclr CORCON,#2
  62 000008  00 00 06 	return 
  63              	.set ___PA___,0
  64              	.LFE0:
  65              	.size _default_timer,.-_default_timer
  66              	.align 2
  67              	.global _config_timer1
  68              	.type _config_timer1,@function
  69              	_config_timer1:
  70              	.LFB1:
  47:lib/lib_pic33e/timer.c **** 
  48:lib/lib_pic33e/timer.c **** /**********************************************************************
  49:lib/lib_pic33e/timer.c ****  * Name:    config_timer1 (type A)
  50:lib/lib_pic33e/timer.c ****  * Args:    time - interrupt step in miliseconds; priority 0-7
  51:lib/lib_pic33e/timer.c ****  * Return:  -
  52:lib/lib_pic33e/timer.c ****  * Desc:    Configures and enables timer 1 module.
  53:lib/lib_pic33e/timer.c ****  **********************************************************************/
  54:lib/lib_pic33e/timer.c **** void config_timer1(unsigned int time, unsigned int priority, void
  55:lib/lib_pic33e/timer.c **** 		(*timer_function(void))){
  71              	.loc 1 55 0
  72              	.set ___PA___,1
  73 00000a  06 00 FA 	lnk #6
  74              	.LCFI1:
  75              	.loc 1 55 0
  76 00000c  00 0F 78 	mov w0,[w14]
  56:lib/lib_pic33e/timer.c **** 
  57:lib/lib_pic33e/timer.c **** 	T1CONbits.TCS   = 0;		/* use internal clock: Fcy               */
  77              	.loc 1 57 0
  78 00000e  00 20 A9 	bclr.b _T1CONbits,#1
  79              	.loc 1 55 0
  80 000010  11 07 98 	mov w1,[w14+2]
  58:lib/lib_pic33e/timer.c **** 	T1CONbits.TGATE = 0;		/* Gated mode off                        */
  81              	.loc 1 58 0
  82 000012  00 C0 A9 	bclr.b _T1CONbits,#6
  83              	.loc 1 55 0
  84 000014  22 07 98 	mov w2,[w14+4]
  59:lib/lib_pic33e/timer.c **** 	T1CONbits.TCKPS = 3;		/* prescale 1:256                        */
  85              	.loc 1 59 0
  86 000016  02 00 80 	mov _T1CONbits,w2
  87 000018  00 03 20 	mov #48,w0
  60:lib/lib_pic33e/timer.c **** 	T1CONbits.TSIDL = 0;		/* don't stop the timer in idle          */
  61:lib/lib_pic33e/timer.c **** 
  62:lib/lib_pic33e/timer.c **** 	TMR1 = 0;					/* clears the timer register             */
  63:lib/lib_pic33e/timer.c **** 	PR1 = (M_SEC/256)*time;		/* value at which the register overflows */
  88              	.loc 1 63 0
  89 00001a  9E 00 78 	mov [w14],w1
  90              	.loc 1 59 0
  91 00001c  02 01 70 	ior w0,w2,w2
  92              	.loc 1 63 0
  93 00001e  A0 0F 20 	mov #250,w0
  94              	.loc 1 59 0
  95 000020  02 00 88 	mov w2,_T1CONbits
  96              	.loc 1 63 0
  97 000022  80 88 B9 	mulw.ss w1,w0,w0
  98 000024  80 00 78 	mov w0,w1
  99              	.loc 1 60 0
 100 000026  01 A0 A9 	bclr.b _T1CONbits+1,#5
MPLAB XC16 ASSEMBLY Listing:   			page 4


  64:lib/lib_pic33e/timer.c **** 								/* and raises T1IF                       */
  65:lib/lib_pic33e/timer.c **** 
  66:lib/lib_pic33e/timer.c **** 	/* interruptions */
  67:lib/lib_pic33e/timer.c **** 	IPC0bits.T1IP = priority;	/* Timer 1 Interrupt Priority 0-7        */
 101              	.loc 1 67 0
 102 000028  1E 00 90 	mov [w14+2],w0
 103              	.loc 1 62 0
 104 00002a  00 20 EF 	clr _TMR1
 105              	.loc 1 67 0
 106 00002c  00 40 78 	mov.b w0,w0
 107              	.loc 1 63 0
 108 00002e  01 00 88 	mov w1,_PR1
 109              	.loc 1 67 0
 110 000030  67 40 60 	and.b w0,#7,w0
 111 000032  02 00 80 	mov _IPC0bits,w2
 112 000034  00 80 FB 	ze w0,w0
 113 000036  F1 FF 28 	mov #-28673,w1
 114 000038  67 00 60 	and w0,#7,w0
 115 00003a  01 01 61 	and w2,w1,w2
 116 00003c  CC 00 DD 	sl w0,#12,w1
  68:lib/lib_pic33e/timer.c **** 	IFS0bits.T1IF = 0;			/* clear interrupt flag                  */
  69:lib/lib_pic33e/timer.c **** 	IEC0bits.T1IE = 1;			/* Timer 1 Interrupt Enable              */
  70:lib/lib_pic33e/timer.c **** 
  71:lib/lib_pic33e/timer.c **** 	fun_ptr_timer1 = (timer_function == NULL) ? default_timer : timer_function;
 117              	.loc 1 71 0
 118 00003e  2E 00 90 	mov [w14+4],w0
 119              	.loc 1 67 0
 120 000040  82 80 70 	ior w1,w2,w1
 121 000042  01 00 88 	mov w1,_IPC0bits
 122              	.loc 1 68 0
 123 000044  00 60 A9 	bclr.b _IFS0bits,#3
 124              	.loc 1 69 0
 125 000046  00 60 A8 	bset.b _IEC0bits,#3
 126              	.loc 1 71 0
 127 000048  00 00 E0 	cp0 w0
 128              	.set ___BP___,0
 129 00004a  00 00 32 	bra z,.L3
 130 00004c  2E 00 90 	mov [w14+4],w0
 131 00004e  00 00 37 	bra .L4
 132              	.L3:
 133 000050  00 00 20 	mov #handle(_default_timer),w0
 134              	.L4:
 135 000052  00 00 88 	mov w0,_fun_ptr_timer1
  72:lib/lib_pic33e/timer.c **** 
  73:lib/lib_pic33e/timer.c **** 	T1CONbits.TON = 1;			/* starts the timer                      */
 136              	.loc 1 73 0
 137 000054  01 E0 A8 	bset.b _T1CONbits+1,#7
  74:lib/lib_pic33e/timer.c **** 	return;
  75:lib/lib_pic33e/timer.c **** }
 138              	.loc 1 75 0
 139 000056  8E 07 78 	mov w14,w15
 140 000058  4F 07 78 	mov [--w15],w14
 141 00005a  00 40 A9 	bclr CORCON,#2
 142 00005c  00 00 06 	return 
 143              	.set ___PA___,0
 144              	.LFE1:
 145              	.size _config_timer1,.-_config_timer1
MPLAB XC16 ASSEMBLY Listing:   			page 5


 146              	.align 2
 147              	.global _config_timer1_us
 148              	.type _config_timer1_us,@function
 149              	_config_timer1_us:
 150              	.LFB2:
  76:lib/lib_pic33e/timer.c **** 
  77:lib/lib_pic33e/timer.c **** /**********************************************************************
  78:lib/lib_pic33e/timer.c ****  * Name:    config_timer1 (type A)
  79:lib/lib_pic33e/timer.c ****  * Args:    time - interrupt step in microseconds; priority 0-7
  80:lib/lib_pic33e/timer.c ****  * Return:  -
  81:lib/lib_pic33e/timer.c ****  * Desc:    Configures timer 1 module.
  82:lib/lib_pic33e/timer.c ****  **********************************************************************/
  83:lib/lib_pic33e/timer.c **** void config_timer1_us(unsigned int time, unsigned int priority){
 151              	.loc 1 83 0
 152              	.set ___PA___,1
 153 00005e  04 00 FA 	lnk #4
 154              	.LCFI2:
 155              	.loc 1 83 0
 156 000060  00 0F 78 	mov w0,[w14]
  84:lib/lib_pic33e/timer.c **** 
  85:lib/lib_pic33e/timer.c **** 	T1CONbits.TCS   = 0;		/* use internal clock: Fcy               */
 157              	.loc 1 85 0
 158 000062  00 20 A9 	bclr.b _T1CONbits,#1
 159              	.loc 1 83 0
 160 000064  11 07 98 	mov w1,[w14+2]
  86:lib/lib_pic33e/timer.c **** 	T1CONbits.TGATE = 0;		/* Gated mode off                        */
 161              	.loc 1 86 0
 162 000066  00 C0 A9 	bclr.b _T1CONbits,#6
  87:lib/lib_pic33e/timer.c **** 	T1CONbits.TCKPS = 1;		/* prescale 1:8                          */
 163              	.loc 1 87 0
 164 000068  F0 FC 2F 	mov #-49,w0
 165 00006a  02 00 80 	mov _T1CONbits,w2
  88:lib/lib_pic33e/timer.c **** 	T1CONbits.TSIDL = 0;		/* don't stop the timer in idle          */
  89:lib/lib_pic33e/timer.c **** 
  90:lib/lib_pic33e/timer.c **** 	TMR1 = 0;					/* clears the timer register             */
  91:lib/lib_pic33e/timer.c **** 	PR1 = U_SEC*time/8; 		/* value at which the register overflows */
 166              	.loc 1 91 0
 167 00006c  9E 00 78 	mov [w14],w1
 168              	.loc 1 87 0
 169 00006e  00 00 61 	and w2,w0,w0
 170              	.loc 1 91 0
 171 000070  61 09 B8 	mul.uu w1,#1,w2
 172              	.loc 1 87 0
 173 000072  00 02 78 	mov w0,w4
 174 000074  04 40 A0 	bset w4,#4
 175              	.loc 1 91 0
 176 000076  46 18 DD 	sl w3,#6,w0
 177 000078  CA 10 DE 	lsr w2,#10,w1
 178 00007a  81 00 70 	ior w0,w1,w1
 179 00007c  46 10 DD 	sl w2,#6,w0
 180              	.loc 1 87 0
 181 00007e  04 00 88 	mov w4,_T1CONbits
 182              	.loc 1 91 0
 183 000080  4D 09 DD 	sl w1,#13,w2
 184 000082  43 00 DE 	lsr w0,#3,w0
 185 000084  00 00 71 	ior w2,w0,w0
 186 000086  C3 08 DE 	lsr w1,#3,w1
MPLAB XC16 ASSEMBLY Listing:   			page 6


 187              	.loc 1 88 0
 188 000088  01 A0 A9 	bclr.b _T1CONbits+1,#5
 189              	.loc 1 91 0
 190 00008a  80 00 78 	mov w0,w1
 191              	.loc 1 90 0
 192 00008c  00 20 EF 	clr _TMR1
  92:lib/lib_pic33e/timer.c **** 								/* and raises T1IF                       */
  93:lib/lib_pic33e/timer.c **** 
  94:lib/lib_pic33e/timer.c **** 	/* interruptions */
  95:lib/lib_pic33e/timer.c **** 	IPC0bits.T1IP = priority;	/* Timer 1 Interrupt Priority 0-7        */
 193              	.loc 1 95 0
 194 00008e  1E 00 90 	mov [w14+2],w0
 195              	.loc 1 91 0
 196 000090  01 00 88 	mov w1,_PR1
 197              	.loc 1 95 0
 198 000092  00 40 78 	mov.b w0,w0
 199 000094  02 00 80 	mov _IPC0bits,w2
 200 000096  67 40 60 	and.b w0,#7,w0
 201 000098  F1 FF 28 	mov #-28673,w1
 202 00009a  00 80 FB 	ze w0,w0
 203 00009c  81 00 61 	and w2,w1,w1
 204 00009e  67 00 60 	and w0,#7,w0
 205 0000a0  4C 00 DD 	sl w0,#12,w0
 206 0000a2  01 00 70 	ior w0,w1,w0
 207 0000a4  00 00 88 	mov w0,_IPC0bits
  96:lib/lib_pic33e/timer.c **** 	IFS0bits.T1IF = 0;			/* clear interrupt flag                  */
 208              	.loc 1 96 0
 209 0000a6  00 60 A9 	bclr.b _IFS0bits,#3
  97:lib/lib_pic33e/timer.c **** 	IEC0bits.T1IE = 1;			/* Timer 1 Interrupt Enable              */
 210              	.loc 1 97 0
 211 0000a8  00 60 A8 	bset.b _IEC0bits,#3
  98:lib/lib_pic33e/timer.c **** 
  99:lib/lib_pic33e/timer.c **** 	return;
 100:lib/lib_pic33e/timer.c **** }
 212              	.loc 1 100 0
 213 0000aa  8E 07 78 	mov w14,w15
 214 0000ac  4F 07 78 	mov [--w15],w14
 215 0000ae  00 40 A9 	bclr CORCON,#2
 216 0000b0  00 00 06 	return 
 217              	.set ___PA___,0
 218              	.LFE2:
 219              	.size _config_timer1_us,.-_config_timer1_us
 220              	.align 2
 221              	.global _config_timer2
 222              	.type _config_timer2,@function
 223              	_config_timer2:
 224              	.LFB3:
 101:lib/lib_pic33e/timer.c **** 
 102:lib/lib_pic33e/timer.c **** 
 103:lib/lib_pic33e/timer.c **** /**********************************************************************
 104:lib/lib_pic33e/timer.c ****  * Name:    config_timer2 (type B)
 105:lib/lib_pic33e/timer.c ****  * Args:    time - interrupt step in miliseconds; priority 0-7
 106:lib/lib_pic33e/timer.c ****  * Return:  -
 107:lib/lib_pic33e/timer.c ****  * Desc:    Configures and enables timer 1 module.
 108:lib/lib_pic33e/timer.c ****  **********************************************************************/
 109:lib/lib_pic33e/timer.c **** void config_timer2(unsigned int time, unsigned int priority, void
 110:lib/lib_pic33e/timer.c **** 		(*timer_function(void))){
MPLAB XC16 ASSEMBLY Listing:   			page 7


 225              	.loc 1 110 0
 226              	.set ___PA___,1
 227 0000b2  06 00 FA 	lnk #6
 228              	.LCFI3:
 229              	.loc 1 110 0
 230 0000b4  00 0F 78 	mov w0,[w14]
 111:lib/lib_pic33e/timer.c **** 
 112:lib/lib_pic33e/timer.c **** 	T2CONbits.TCS   = 0;		/* use internal clock: Fcy               */
 231              	.loc 1 112 0
 232 0000b6  00 20 A9 	bclr.b _T2CONbits,#1
 233              	.loc 1 110 0
 234 0000b8  11 07 98 	mov w1,[w14+2]
 113:lib/lib_pic33e/timer.c **** 	T2CONbits.TGATE = 0;		/* Gated mode off                        */
 235              	.loc 1 113 0
 236 0000ba  00 C0 A9 	bclr.b _T2CONbits,#6
 237              	.loc 1 110 0
 238 0000bc  22 07 98 	mov w2,[w14+4]
 114:lib/lib_pic33e/timer.c **** 	T2CONbits.TCKPS = 3;		/* prescale 1:256                        */
 239              	.loc 1 114 0
 240 0000be  02 00 80 	mov _T2CONbits,w2
 241 0000c0  00 03 20 	mov #48,w0
 115:lib/lib_pic33e/timer.c **** 	T2CONbits.TSIDL = 0;		/* don't stop the timer in idle          */
 116:lib/lib_pic33e/timer.c **** 	T2CONbits.T32 = 0;          /*TMRx and TMRy form 16-bit timer        */
 117:lib/lib_pic33e/timer.c **** 
 118:lib/lib_pic33e/timer.c **** 	TMR2 = 0;					/* clears the timer register             */
 119:lib/lib_pic33e/timer.c **** 	PR2 = (M_SEC/256)*time;		/* value at which the register overflows *
 242              	.loc 1 119 0
 243 0000c2  9E 00 78 	mov [w14],w1
 244              	.loc 1 114 0
 245 0000c4  02 01 70 	ior w0,w2,w2
 246              	.loc 1 119 0
 247 0000c6  A0 0F 20 	mov #250,w0
 248              	.loc 1 114 0
 249 0000c8  02 00 88 	mov w2,_T2CONbits
 250              	.loc 1 119 0
 251 0000ca  80 88 B9 	mulw.ss w1,w0,w0
 252 0000cc  80 00 78 	mov w0,w1
 253              	.loc 1 115 0
 254 0000ce  01 A0 A9 	bclr.b _T2CONbits+1,#5
 120:lib/lib_pic33e/timer.c **** 								 * and raises T1IF                       */
 121:lib/lib_pic33e/timer.c **** 
 122:lib/lib_pic33e/timer.c **** 	/* interruptions */
 123:lib/lib_pic33e/timer.c **** 	IPC1bits.T2IP = priority;	/* Timer 2 Interrupt Priority 0-7        */
 255              	.loc 1 123 0
 256 0000d0  1E 00 90 	mov [w14+2],w0
 257              	.loc 1 116 0
 258 0000d2  00 60 A9 	bclr.b _T2CONbits,#3
 259              	.loc 1 123 0
 260 0000d4  00 40 78 	mov.b w0,w0
 261              	.loc 1 118 0
 262 0000d6  00 20 EF 	clr _TMR2
 263              	.loc 1 123 0
 264 0000d8  67 40 60 	and.b w0,#7,w0
 265              	.loc 1 119 0
 266 0000da  01 00 88 	mov w1,_PR2
 267              	.loc 1 123 0
 268 0000dc  00 80 FB 	ze w0,w0
MPLAB XC16 ASSEMBLY Listing:   			page 8


 269 0000de  02 00 80 	mov _IPC1bits,w2
 270 0000e0  E7 00 60 	and w0,#7,w1
 271 0000e2  F0 FF 28 	mov #-28673,w0
 272 0000e4  CC 08 DD 	sl w1,#12,w1
 273 0000e6  00 01 61 	and w2,w0,w2
 124:lib/lib_pic33e/timer.c **** 	IFS0bits.T2IF = 0;			/* clear interrupt flag                  */
 125:lib/lib_pic33e/timer.c **** 	IEC0bits.T2IE = 1;			/* Timer 2 Interrupt Enable              */
 126:lib/lib_pic33e/timer.c **** 
 127:lib/lib_pic33e/timer.c **** 	fun_ptr_timer2 = (timer_function == NULL) ? default_timer : timer_function;
 274              	.loc 1 127 0
 275 0000e8  2E 00 90 	mov [w14+4],w0
 276              	.loc 1 123 0
 277 0000ea  82 80 70 	ior w1,w2,w1
 278 0000ec  01 00 88 	mov w1,_IPC1bits
 279              	.loc 1 124 0
 280 0000ee  00 E0 A9 	bclr.b _IFS0bits,#7
 281              	.loc 1 125 0
 282 0000f0  00 E0 A8 	bset.b _IEC0bits,#7
 283              	.loc 1 127 0
 284 0000f2  00 00 E0 	cp0 w0
 285              	.set ___BP___,0
 286 0000f4  00 00 32 	bra z,.L7
 287 0000f6  2E 00 90 	mov [w14+4],w0
 288 0000f8  00 00 37 	bra .L8
 289              	.L7:
 290 0000fa  00 00 20 	mov #handle(_default_timer),w0
 291              	.L8:
 292 0000fc  00 00 88 	mov w0,_fun_ptr_timer2
 128:lib/lib_pic33e/timer.c **** 
 129:lib/lib_pic33e/timer.c **** 	T2CONbits.TON = 1;			/* starts the timer                      */
 293              	.loc 1 129 0
 294 0000fe  01 E0 A8 	bset.b _T2CONbits+1,#7
 130:lib/lib_pic33e/timer.c **** 	return;
 131:lib/lib_pic33e/timer.c **** }
 295              	.loc 1 131 0
 296 000100  8E 07 78 	mov w14,w15
 297 000102  4F 07 78 	mov [--w15],w14
 298 000104  00 40 A9 	bclr CORCON,#2
 299 000106  00 00 06 	return 
 300              	.set ___PA___,0
 301              	.LFE3:
 302              	.size _config_timer2,.-_config_timer2
 303              	.align 2
 304              	.global _config_timer2_us
 305              	.type _config_timer2_us,@function
 306              	_config_timer2_us:
 307              	.LFB4:
 132:lib/lib_pic33e/timer.c **** 
 133:lib/lib_pic33e/timer.c **** /**********************************************************************
 134:lib/lib_pic33e/timer.c ****  * Name:    config_timer2 (type B)
 135:lib/lib_pic33e/timer.c ****  * Args:    time - interrupt step in microseconds; priority 0-7
 136:lib/lib_pic33e/timer.c ****  * Return:  -
 137:lib/lib_pic33e/timer.c ****  * Desc:    Configures and enables timer 1 module.
 138:lib/lib_pic33e/timer.c ****  **********************************************************************/
 139:lib/lib_pic33e/timer.c **** void config_timer2_us(unsigned int time, unsigned int priority){
 308              	.loc 1 139 0
 309              	.set ___PA___,1
MPLAB XC16 ASSEMBLY Listing:   			page 9


 310 000108  04 00 FA 	lnk #4
 311              	.LCFI4:
 312              	.loc 1 139 0
 313 00010a  00 0F 78 	mov w0,[w14]
 140:lib/lib_pic33e/timer.c **** 
 141:lib/lib_pic33e/timer.c **** 	T2CONbits.TCS   = 0;		/* use internal clock: Fcy               */
 314              	.loc 1 141 0
 315 00010c  00 20 A9 	bclr.b _T2CONbits,#1
 316              	.loc 1 139 0
 317 00010e  11 07 98 	mov w1,[w14+2]
 142:lib/lib_pic33e/timer.c **** 	T2CONbits.TGATE = 0;		/* Gated mode off                        */
 318              	.loc 1 142 0
 319 000110  00 C0 A9 	bclr.b _T2CONbits,#6
 143:lib/lib_pic33e/timer.c **** 	T2CONbits.TCKPS = 1;		/* prescale 1:8                          */
 320              	.loc 1 143 0
 321 000112  F0 FC 2F 	mov #-49,w0
 322 000114  02 00 80 	mov _T2CONbits,w2
 144:lib/lib_pic33e/timer.c **** 	T2CONbits.TSIDL = 0;		/* don't stop the timer in idle          */
 145:lib/lib_pic33e/timer.c **** 	T2CONbits.T32 = 0;          /*TMRx and TMRy form 16-bit timer        */
 146:lib/lib_pic33e/timer.c **** 
 147:lib/lib_pic33e/timer.c **** 	TMR2 = 0;					/* clears the timer register             */
 148:lib/lib_pic33e/timer.c **** 	PR2 = U_SEC*time/8; 		/* value at which the register overflows *
 323              	.loc 1 148 0
 324 000116  9E 00 78 	mov [w14],w1
 325              	.loc 1 143 0
 326 000118  00 00 61 	and w2,w0,w0
 327              	.loc 1 148 0
 328 00011a  61 09 B8 	mul.uu w1,#1,w2
 329              	.loc 1 143 0
 330 00011c  00 02 78 	mov w0,w4
 331 00011e  04 40 A0 	bset w4,#4
 332              	.loc 1 148 0
 333 000120  46 18 DD 	sl w3,#6,w0
 334 000122  CA 10 DE 	lsr w2,#10,w1
 335 000124  81 00 70 	ior w0,w1,w1
 336 000126  46 10 DD 	sl w2,#6,w0
 337              	.loc 1 143 0
 338 000128  04 00 88 	mov w4,_T2CONbits
 339              	.loc 1 148 0
 340 00012a  4D 09 DD 	sl w1,#13,w2
 341 00012c  43 00 DE 	lsr w0,#3,w0
 342 00012e  00 00 71 	ior w2,w0,w0
 343 000130  C3 08 DE 	lsr w1,#3,w1
 344              	.loc 1 144 0
 345 000132  01 A0 A9 	bclr.b _T2CONbits+1,#5
 346              	.loc 1 148 0
 347 000134  80 00 78 	mov w0,w1
 348              	.loc 1 145 0
 349 000136  00 60 A9 	bclr.b _T2CONbits,#3
 149:lib/lib_pic33e/timer.c **** 								 * and raises T1IF                       */
 150:lib/lib_pic33e/timer.c **** 
 151:lib/lib_pic33e/timer.c **** 	/* interruptions */
 152:lib/lib_pic33e/timer.c **** 	IPC1bits.T2IP = priority;	/* Timer 2 Interrupt Priority 0-7        */
 350              	.loc 1 152 0
 351 000138  1E 00 90 	mov [w14+2],w0
 352              	.loc 1 147 0
 353 00013a  00 20 EF 	clr _TMR2
MPLAB XC16 ASSEMBLY Listing:   			page 10


 354              	.loc 1 152 0
 355 00013c  00 40 78 	mov.b w0,w0
 356              	.loc 1 148 0
 357 00013e  01 00 88 	mov w1,_PR2
 358              	.loc 1 152 0
 359 000140  67 40 60 	and.b w0,#7,w0
 360 000142  02 00 80 	mov _IPC1bits,w2
 361 000144  00 80 FB 	ze w0,w0
 362 000146  F1 FF 28 	mov #-28673,w1
 363 000148  67 00 60 	and w0,#7,w0
 364 00014a  81 00 61 	and w2,w1,w1
 365 00014c  4C 00 DD 	sl w0,#12,w0
 366 00014e  01 00 70 	ior w0,w1,w0
 367 000150  00 00 88 	mov w0,_IPC1bits
 153:lib/lib_pic33e/timer.c **** 	IFS0bits.T2IF = 0;			/* clear interrupt flag                  */
 368              	.loc 1 153 0
 369 000152  00 E0 A9 	bclr.b _IFS0bits,#7
 154:lib/lib_pic33e/timer.c **** 	IEC0bits.T2IE = 1;			/* Timer 2 Interrupt Enable              */
 370              	.loc 1 154 0
 371 000154  00 E0 A8 	bset.b _IEC0bits,#7
 155:lib/lib_pic33e/timer.c **** 
 156:lib/lib_pic33e/timer.c **** 	return;
 157:lib/lib_pic33e/timer.c **** }
 372              	.loc 1 157 0
 373 000156  8E 07 78 	mov w14,w15
 374 000158  4F 07 78 	mov [--w15],w14
 375 00015a  00 40 A9 	bclr CORCON,#2
 376 00015c  00 00 06 	return 
 377              	.set ___PA___,0
 378              	.LFE4:
 379              	.size _config_timer2_us,.-_config_timer2_us
 380              	.align 2
 381              	.global _config_timer3
 382              	.type _config_timer3,@function
 383              	_config_timer3:
 384              	.LFB5:
 158:lib/lib_pic33e/timer.c **** 
 159:lib/lib_pic33e/timer.c **** 
 160:lib/lib_pic33e/timer.c **** /**********************************************************************
 161:lib/lib_pic33e/timer.c ****  * Name:    config_timer3		(type C)
 162:lib/lib_pic33e/timer.c ****  * Args:    time - interrupt step in miliseconds; priority 0-7
 163:lib/lib_pic33e/timer.c ****  * Return:  -
 164:lib/lib_pic33e/timer.c ****  * Desc:    Configures and enables timer 1 module.
 165:lib/lib_pic33e/timer.c ****  **********************************************************************/
 166:lib/lib_pic33e/timer.c **** void config_timer3(unsigned int time, unsigned int priority, void
 167:lib/lib_pic33e/timer.c **** 		(*timer_function(void))){
 385              	.loc 1 167 0
 386              	.set ___PA___,1
 387 00015e  06 00 FA 	lnk #6
 388              	.LCFI5:
 389              	.loc 1 167 0
 390 000160  00 0F 78 	mov w0,[w14]
 168:lib/lib_pic33e/timer.c **** 
 169:lib/lib_pic33e/timer.c **** 	T3CONbits.TCS   = 0;		/* use internal clock: Fcy               */
 391              	.loc 1 169 0
 392 000162  00 20 A9 	bclr.b _T3CONbits,#1
 393              	.loc 1 167 0
MPLAB XC16 ASSEMBLY Listing:   			page 11


 394 000164  11 07 98 	mov w1,[w14+2]
 170:lib/lib_pic33e/timer.c **** 	T3CONbits.TGATE = 0;		/* Gated mode off                        */
 395              	.loc 1 170 0
 396 000166  00 C0 A9 	bclr.b _T3CONbits,#6
 397              	.loc 1 167 0
 398 000168  22 07 98 	mov w2,[w14+4]
 171:lib/lib_pic33e/timer.c **** 	T3CONbits.TCKPS = 3;		/* prescale 1:256                        */
 399              	.loc 1 171 0
 400 00016a  02 00 80 	mov _T3CONbits,w2
 401 00016c  00 03 20 	mov #48,w0
 172:lib/lib_pic33e/timer.c **** 	T3CONbits.TSIDL = 0;		/* don't stop the timer in idle          */
 173:lib/lib_pic33e/timer.c **** 
 174:lib/lib_pic33e/timer.c **** 	TMR3 = 0;					/* clears the timer register             */
 175:lib/lib_pic33e/timer.c **** 	PR3 = (M_SEC/256)*time;		/* value at which the register overflows *
 402              	.loc 1 175 0
 403 00016e  9E 00 78 	mov [w14],w1
 404              	.loc 1 171 0
 405 000170  02 01 70 	ior w0,w2,w2
 406              	.loc 1 175 0
 407 000172  A0 0F 20 	mov #250,w0
 408              	.loc 1 171 0
 409 000174  02 00 88 	mov w2,_T3CONbits
 410              	.loc 1 175 0
 411 000176  80 88 B9 	mulw.ss w1,w0,w0
 412 000178  80 00 78 	mov w0,w1
 413              	.loc 1 172 0
 414 00017a  01 A0 A9 	bclr.b _T3CONbits+1,#5
 176:lib/lib_pic33e/timer.c **** 								 * and raises T1IF                       */
 177:lib/lib_pic33e/timer.c **** 
 178:lib/lib_pic33e/timer.c **** 	/* interruptions */
 179:lib/lib_pic33e/timer.c **** 	IPC2bits.T3IP = priority;	/* Timer 3 Interrupt Priority 0-7        */
 415              	.loc 1 179 0
 416 00017c  1E 00 90 	mov [w14+2],w0
 417              	.loc 1 174 0
 418 00017e  00 20 EF 	clr _TMR3
 419              	.loc 1 179 0
 420 000180  00 40 78 	mov.b w0,w0
 421              	.loc 1 175 0
 422 000182  01 00 88 	mov w1,_PR3
 423              	.loc 1 179 0
 424 000184  67 40 60 	and.b w0,#7,w0
 425 000186  02 00 80 	mov _IPC2bits,w2
 426 000188  80 80 FB 	ze w0,w1
 427 00018a  80 FF 2F 	mov #-8,w0
 428 00018c  E7 80 60 	and w1,#7,w1
 429 00018e  00 01 61 	and w2,w0,w2
 180:lib/lib_pic33e/timer.c **** 	IFS0bits.T3IF = 0;			/* clear interrupt flag                  */
 181:lib/lib_pic33e/timer.c **** 	IEC0bits.T3IE = 1;			/* Timer 3 Interrupt Enable              */
 182:lib/lib_pic33e/timer.c **** 
 183:lib/lib_pic33e/timer.c **** 	fun_ptr_timer3 = (timer_function == NULL) ? default_timer : timer_function;
 430              	.loc 1 183 0
 431 000190  2E 00 90 	mov [w14+4],w0
 432              	.loc 1 179 0
 433 000192  82 80 70 	ior w1,w2,w1
 434 000194  01 00 88 	mov w1,_IPC2bits
 435              	.loc 1 180 0
 436 000196  01 00 A9 	bclr.b _IFS0bits+1,#0
MPLAB XC16 ASSEMBLY Listing:   			page 12


 437              	.loc 1 181 0
 438 000198  01 00 A8 	bset.b _IEC0bits+1,#0
 439              	.loc 1 183 0
 440 00019a  00 00 E0 	cp0 w0
 441              	.set ___BP___,0
 442 00019c  00 00 32 	bra z,.L11
 443 00019e  2E 00 90 	mov [w14+4],w0
 444 0001a0  00 00 37 	bra .L12
 445              	.L11:
 446 0001a2  00 00 20 	mov #handle(_default_timer),w0
 447              	.L12:
 448 0001a4  00 00 88 	mov w0,_fun_ptr_timer3
 184:lib/lib_pic33e/timer.c **** 
 185:lib/lib_pic33e/timer.c **** 	T3CONbits.TON = 1;			/* starts the timer                      */
 449              	.loc 1 185 0
 450 0001a6  01 E0 A8 	bset.b _T3CONbits+1,#7
 186:lib/lib_pic33e/timer.c **** 	return;
 187:lib/lib_pic33e/timer.c **** }
 451              	.loc 1 187 0
 452 0001a8  8E 07 78 	mov w14,w15
 453 0001aa  4F 07 78 	mov [--w15],w14
 454 0001ac  00 40 A9 	bclr CORCON,#2
 455 0001ae  00 00 06 	return 
 456              	.set ___PA___,0
 457              	.LFE5:
 458              	.size _config_timer3,.-_config_timer3
 459              	.align 2
 460              	.global _config_timer3_us
 461              	.type _config_timer3_us,@function
 462              	_config_timer3_us:
 463              	.LFB6:
 188:lib/lib_pic33e/timer.c **** 
 189:lib/lib_pic33e/timer.c **** /**********************************************************************
 190:lib/lib_pic33e/timer.c ****  * Name:    config_timer3		(type C)
 191:lib/lib_pic33e/timer.c ****  * Args:    time - interrupt step in microseconds; priority 0-7
 192:lib/lib_pic33e/timer.c ****  * Return:  -
 193:lib/lib_pic33e/timer.c ****  * Desc:    Configures timer 1 module.
 194:lib/lib_pic33e/timer.c ****  **********************************************************************/
 195:lib/lib_pic33e/timer.c **** void config_timer3_us(unsigned int time, unsigned int priority){
 464              	.loc 1 195 0
 465              	.set ___PA___,1
 466 0001b0  04 00 FA 	lnk #4
 467              	.LCFI6:
 468              	.loc 1 195 0
 469 0001b2  00 0F 78 	mov w0,[w14]
 196:lib/lib_pic33e/timer.c **** 
 197:lib/lib_pic33e/timer.c **** 	T3CONbits.TCS   = 0;		/* use internal clock: Fcy               */
 470              	.loc 1 197 0
 471 0001b4  00 20 A9 	bclr.b _T3CONbits,#1
 472              	.loc 1 195 0
 473 0001b6  11 07 98 	mov w1,[w14+2]
 198:lib/lib_pic33e/timer.c **** 	T3CONbits.TGATE = 0;		/* Gated mode off                        */
 474              	.loc 1 198 0
 475 0001b8  00 C0 A9 	bclr.b _T3CONbits,#6
 199:lib/lib_pic33e/timer.c **** 	T3CONbits.TCKPS = 1;		/* prescale 1:8                          */
 476              	.loc 1 199 0
 477 0001ba  F0 FC 2F 	mov #-49,w0
MPLAB XC16 ASSEMBLY Listing:   			page 13


 478 0001bc  02 00 80 	mov _T3CONbits,w2
 200:lib/lib_pic33e/timer.c **** 	T3CONbits.TSIDL = 0;		/* don't stop the timer in idle          */
 201:lib/lib_pic33e/timer.c **** 
 202:lib/lib_pic33e/timer.c **** 	TMR3 = 0;					/* clears the timer register             */
 203:lib/lib_pic33e/timer.c **** 	PR3 = U_SEC*time/8; 		/* value at which the register overflows *
 479              	.loc 1 203 0
 480 0001be  9E 00 78 	mov [w14],w1
 481              	.loc 1 199 0
 482 0001c0  00 00 61 	and w2,w0,w0
 483              	.loc 1 203 0
 484 0001c2  61 09 B8 	mul.uu w1,#1,w2
 485              	.loc 1 199 0
 486 0001c4  00 02 78 	mov w0,w4
 487 0001c6  04 40 A0 	bset w4,#4
 488              	.loc 1 203 0
 489 0001c8  46 18 DD 	sl w3,#6,w0
 490 0001ca  CA 10 DE 	lsr w2,#10,w1
 491 0001cc  81 00 70 	ior w0,w1,w1
 492 0001ce  46 10 DD 	sl w2,#6,w0
 493              	.loc 1 199 0
 494 0001d0  04 00 88 	mov w4,_T3CONbits
 495              	.loc 1 203 0
 496 0001d2  4D 09 DD 	sl w1,#13,w2
 497 0001d4  43 00 DE 	lsr w0,#3,w0
 498 0001d6  00 00 71 	ior w2,w0,w0
 499 0001d8  C3 08 DE 	lsr w1,#3,w1
 500              	.loc 1 200 0
 501 0001da  01 A0 A9 	bclr.b _T3CONbits+1,#5
 502              	.loc 1 203 0
 503 0001dc  80 00 78 	mov w0,w1
 504              	.loc 1 202 0
 505 0001de  00 20 EF 	clr _TMR3
 204:lib/lib_pic33e/timer.c **** 								 * and raises T1IF                       */
 205:lib/lib_pic33e/timer.c **** 
 206:lib/lib_pic33e/timer.c **** 	/* interruptions */
 207:lib/lib_pic33e/timer.c **** 	IPC2bits.T3IP = priority;	/* Timer 3 Interrupt Priority 0-7        */
 506              	.loc 1 207 0
 507 0001e0  1E 00 90 	mov [w14+2],w0
 508              	.loc 1 203 0
 509 0001e2  01 00 88 	mov w1,_PR3
 510              	.loc 1 207 0
 511 0001e4  00 40 78 	mov.b w0,w0
 512 0001e6  02 00 80 	mov _IPC2bits,w2
 513 0001e8  67 40 60 	and.b w0,#7,w0
 514 0001ea  81 FF 2F 	mov #-8,w1
 515 0001ec  00 80 FB 	ze w0,w0
 516 0001ee  81 00 61 	and w2,w1,w1
 517 0001f0  67 00 60 	and w0,#7,w0
 518 0001f2  01 00 70 	ior w0,w1,w0
 519 0001f4  00 00 88 	mov w0,_IPC2bits
 208:lib/lib_pic33e/timer.c **** 	IFS0bits.T3IF = 0;			/* clear interrupt flag                  */
 520              	.loc 1 208 0
 521 0001f6  01 00 A9 	bclr.b _IFS0bits+1,#0
 209:lib/lib_pic33e/timer.c **** 	IEC0bits.T3IE = 1;			/* Timer 3 Interrupt Enable              */
 522              	.loc 1 209 0
 523 0001f8  01 00 A8 	bset.b _IEC0bits+1,#0
 210:lib/lib_pic33e/timer.c **** 
MPLAB XC16 ASSEMBLY Listing:   			page 14


 211:lib/lib_pic33e/timer.c **** 	return;
 212:lib/lib_pic33e/timer.c **** }
 524              	.loc 1 212 0
 525 0001fa  8E 07 78 	mov w14,w15
 526 0001fc  4F 07 78 	mov [--w15],w14
 527 0001fe  00 40 A9 	bclr CORCON,#2
 528 000200  00 00 06 	return 
 529              	.set ___PA___,0
 530              	.LFE6:
 531              	.size _config_timer3_us,.-_config_timer3_us
 532              	.align 2
 533              	.global _config_timer4
 534              	.type _config_timer4,@function
 535              	_config_timer4:
 536              	.LFB7:
 213:lib/lib_pic33e/timer.c **** 
 214:lib/lib_pic33e/timer.c **** /**********************************************************************
 215:lib/lib_pic33e/timer.c ****  * Name:    config_timer4   (type B)
 216:lib/lib_pic33e/timer.c ****  * Args:    time - interrupt step in miliseconds; priority 0-7
 217:lib/lib_pic33e/timer.c ****  * Return:  -
 218:lib/lib_pic33e/timer.c ****  * Desc:    Configures and enables timer 1 module.
 219:lib/lib_pic33e/timer.c ****  **********************************************************************/
 220:lib/lib_pic33e/timer.c **** void config_timer4(unsigned int time, unsigned int priority, void
 221:lib/lib_pic33e/timer.c **** 		(*timer_function(void))){
 537              	.loc 1 221 0
 538              	.set ___PA___,1
 539 000202  06 00 FA 	lnk #6
 540              	.LCFI7:
 541              	.loc 1 221 0
 542 000204  00 0F 78 	mov w0,[w14]
 222:lib/lib_pic33e/timer.c **** 
 223:lib/lib_pic33e/timer.c **** 	T4CONbits.TCS   = 0;		/* use internal clock: Fcy               */
 543              	.loc 1 223 0
 544 000206  00 20 A9 	bclr.b _T4CONbits,#1
 545              	.loc 1 221 0
 546 000208  11 07 98 	mov w1,[w14+2]
 224:lib/lib_pic33e/timer.c **** 	T4CONbits.TGATE = 0;		/* Gated mode off                        */
 547              	.loc 1 224 0
 548 00020a  00 C0 A9 	bclr.b _T4CONbits,#6
 549              	.loc 1 221 0
 550 00020c  22 07 98 	mov w2,[w14+4]
 225:lib/lib_pic33e/timer.c **** 	T4CONbits.TCKPS = 3;		/* prescale 1:256                        */
 551              	.loc 1 225 0
 552 00020e  02 00 80 	mov _T4CONbits,w2
 553 000210  00 03 20 	mov #48,w0
 226:lib/lib_pic33e/timer.c **** 	T4CONbits.TSIDL = 0;		/* don't stop the timer in idle          */
 227:lib/lib_pic33e/timer.c **** 	T2CONbits.T32 = 0;          /*TMRx and TMRy form 16-bit timer        */
 228:lib/lib_pic33e/timer.c **** 
 229:lib/lib_pic33e/timer.c **** 	TMR4 = 0;					/* clears the timer register             */
 230:lib/lib_pic33e/timer.c **** 	PR4 = (M_SEC/256)*time;	    /* value at which the register overflows *
 554              	.loc 1 230 0
 555 000212  9E 00 78 	mov [w14],w1
 556              	.loc 1 225 0
 557 000214  02 01 70 	ior w0,w2,w2
 558              	.loc 1 230 0
 559 000216  A0 0F 20 	mov #250,w0
 560              	.loc 1 225 0
MPLAB XC16 ASSEMBLY Listing:   			page 15


 561 000218  02 00 88 	mov w2,_T4CONbits
 562              	.loc 1 230 0
 563 00021a  80 88 B9 	mulw.ss w1,w0,w0
 564 00021c  80 00 78 	mov w0,w1
 565              	.loc 1 226 0
 566 00021e  01 A0 A9 	bclr.b _T4CONbits+1,#5
 231:lib/lib_pic33e/timer.c **** 								 * and raises T1IF                       */
 232:lib/lib_pic33e/timer.c **** 
 233:lib/lib_pic33e/timer.c **** 	/* interruptions */
 234:lib/lib_pic33e/timer.c **** 	IPC6bits.T4IP = priority;	/* Timer 4 Interrupt Priority 0-7        */
 567              	.loc 1 234 0
 568 000220  1E 00 90 	mov [w14+2],w0
 569              	.loc 1 227 0
 570 000222  00 60 A9 	bclr.b _T2CONbits,#3
 571              	.loc 1 234 0
 572 000224  00 40 78 	mov.b w0,w0
 573              	.loc 1 229 0
 574 000226  00 20 EF 	clr _TMR4
 575              	.loc 1 234 0
 576 000228  67 40 60 	and.b w0,#7,w0
 577              	.loc 1 230 0
 578 00022a  01 00 88 	mov w1,_PR4
 579              	.loc 1 234 0
 580 00022c  00 80 FB 	ze w0,w0
 581 00022e  02 00 80 	mov _IPC6bits,w2
 582 000230  E7 00 60 	and w0,#7,w1
 583 000232  F0 FF 28 	mov #-28673,w0
 584 000234  CC 08 DD 	sl w1,#12,w1
 585 000236  00 01 61 	and w2,w0,w2
 235:lib/lib_pic33e/timer.c **** 	IFS1bits.T4IF = 0;			/* clear interrupt flag                  */
 236:lib/lib_pic33e/timer.c **** 	IEC1bits.T4IE = 1;			/* Timer 4 Interrupt Enable              */
 237:lib/lib_pic33e/timer.c **** 
 238:lib/lib_pic33e/timer.c **** 	fun_ptr_timer4 = (timer_function == NULL) ? default_timer : timer_function;
 586              	.loc 1 238 0
 587 000238  2E 00 90 	mov [w14+4],w0
 588              	.loc 1 234 0
 589 00023a  82 80 70 	ior w1,w2,w1
 590 00023c  01 00 88 	mov w1,_IPC6bits
 591              	.loc 1 235 0
 592 00023e  01 60 A9 	bclr.b _IFS1bits+1,#3
 593              	.loc 1 236 0
 594 000240  01 60 A8 	bset.b _IEC1bits+1,#3
 595              	.loc 1 238 0
 596 000242  00 00 E0 	cp0 w0
 597              	.set ___BP___,0
 598 000244  00 00 32 	bra z,.L15
 599 000246  2E 00 90 	mov [w14+4],w0
 600 000248  00 00 37 	bra .L16
 601              	.L15:
 602 00024a  00 00 20 	mov #handle(_default_timer),w0
 603              	.L16:
 604 00024c  00 00 88 	mov w0,_fun_ptr_timer4
 239:lib/lib_pic33e/timer.c **** 
 240:lib/lib_pic33e/timer.c **** 	T4CONbits.TON = 1;			/* starts the timer                      */
 605              	.loc 1 240 0
 606 00024e  01 E0 A8 	bset.b _T4CONbits+1,#7
 241:lib/lib_pic33e/timer.c **** 	return;
MPLAB XC16 ASSEMBLY Listing:   			page 16


 242:lib/lib_pic33e/timer.c **** }
 607              	.loc 1 242 0
 608 000250  8E 07 78 	mov w14,w15
 609 000252  4F 07 78 	mov [--w15],w14
 610 000254  00 40 A9 	bclr CORCON,#2
 611 000256  00 00 06 	return 
 612              	.set ___PA___,0
 613              	.LFE7:
 614              	.size _config_timer4,.-_config_timer4
 615              	.align 2
 616              	.global _config_timer4_us
 617              	.type _config_timer4_us,@function
 618              	_config_timer4_us:
 619              	.LFB8:
 243:lib/lib_pic33e/timer.c **** 
 244:lib/lib_pic33e/timer.c **** /**********************************************************************
 245:lib/lib_pic33e/timer.c ****  * Name:    config_timer4   (type B)
 246:lib/lib_pic33e/timer.c ****  * Args:    time - interrupt step in microseconds; priority 0-7
 247:lib/lib_pic33e/timer.c ****  * Return:  -
 248:lib/lib_pic33e/timer.c ****  * Desc:    Configures and enables timer 1 module.
 249:lib/lib_pic33e/timer.c ****  **********************************************************************/
 250:lib/lib_pic33e/timer.c **** void config_timer4_us(unsigned int time, unsigned int priority){
 620              	.loc 1 250 0
 621              	.set ___PA___,1
 622 000258  04 00 FA 	lnk #4
 623              	.LCFI8:
 624              	.loc 1 250 0
 625 00025a  00 0F 78 	mov w0,[w14]
 251:lib/lib_pic33e/timer.c **** 
 252:lib/lib_pic33e/timer.c **** 	T4CONbits.TCS   = 0;		/* use internal clock: Fcy               */
 626              	.loc 1 252 0
 627 00025c  00 20 A9 	bclr.b _T4CONbits,#1
 628              	.loc 1 250 0
 629 00025e  11 07 98 	mov w1,[w14+2]
 253:lib/lib_pic33e/timer.c **** 	T4CONbits.TGATE = 0;		/* Gated mode off                        */
 630              	.loc 1 253 0
 631 000260  00 C0 A9 	bclr.b _T4CONbits,#6
 254:lib/lib_pic33e/timer.c **** 	T4CONbits.TCKPS = 1;		/* prescale 1:8                          */
 632              	.loc 1 254 0
 633 000262  F0 FC 2F 	mov #-49,w0
 634 000264  02 00 80 	mov _T4CONbits,w2
 255:lib/lib_pic33e/timer.c **** 	T4CONbits.TSIDL = 0;		/* don't stop the timer in idle          */
 256:lib/lib_pic33e/timer.c **** 	T2CONbits.T32 = 0;          /*TMRx and TMRy form 16-bit timer        */
 257:lib/lib_pic33e/timer.c **** 
 258:lib/lib_pic33e/timer.c **** 	TMR4 = 0;					/* clears the timer register             */
 259:lib/lib_pic33e/timer.c **** 	PR4 = U_SEC*time/8; 	    /* value at which the register overflows *
 635              	.loc 1 259 0
 636 000266  9E 00 78 	mov [w14],w1
 637              	.loc 1 254 0
 638 000268  00 00 61 	and w2,w0,w0
 639              	.loc 1 259 0
 640 00026a  61 09 B8 	mul.uu w1,#1,w2
 641              	.loc 1 254 0
 642 00026c  00 02 78 	mov w0,w4
 643 00026e  04 40 A0 	bset w4,#4
 644              	.loc 1 259 0
 645 000270  46 18 DD 	sl w3,#6,w0
MPLAB XC16 ASSEMBLY Listing:   			page 17


 646 000272  CA 10 DE 	lsr w2,#10,w1
 647 000274  81 00 70 	ior w0,w1,w1
 648 000276  46 10 DD 	sl w2,#6,w0
 649              	.loc 1 254 0
 650 000278  04 00 88 	mov w4,_T4CONbits
 651              	.loc 1 259 0
 652 00027a  4D 09 DD 	sl w1,#13,w2
 653 00027c  43 00 DE 	lsr w0,#3,w0
 654 00027e  00 00 71 	ior w2,w0,w0
 655 000280  C3 08 DE 	lsr w1,#3,w1
 656              	.loc 1 255 0
 657 000282  01 A0 A9 	bclr.b _T4CONbits+1,#5
 658              	.loc 1 259 0
 659 000284  80 00 78 	mov w0,w1
 660              	.loc 1 256 0
 661 000286  00 60 A9 	bclr.b _T2CONbits,#3
 260:lib/lib_pic33e/timer.c **** 								 * and raises T1IF                       */
 261:lib/lib_pic33e/timer.c **** 
 262:lib/lib_pic33e/timer.c **** 	/* interruptions */
 263:lib/lib_pic33e/timer.c **** 	IPC6bits.T4IP = priority;	/* Timer 4 Interrupt Priority 0-7        */
 662              	.loc 1 263 0
 663 000288  1E 00 90 	mov [w14+2],w0
 664              	.loc 1 258 0
 665 00028a  00 20 EF 	clr _TMR4
 666              	.loc 1 263 0
 667 00028c  00 40 78 	mov.b w0,w0
 668              	.loc 1 259 0
 669 00028e  01 00 88 	mov w1,_PR4
 670              	.loc 1 263 0
 671 000290  67 40 60 	and.b w0,#7,w0
 672 000292  02 00 80 	mov _IPC6bits,w2
 673 000294  00 80 FB 	ze w0,w0
 674 000296  F1 FF 28 	mov #-28673,w1
 675 000298  67 00 60 	and w0,#7,w0
 676 00029a  81 00 61 	and w2,w1,w1
 677 00029c  4C 00 DD 	sl w0,#12,w0
 678 00029e  01 00 70 	ior w0,w1,w0
 679 0002a0  00 00 88 	mov w0,_IPC6bits
 264:lib/lib_pic33e/timer.c **** 	IFS1bits.T4IF = 0;			/* clear interrupt flag                  */
 680              	.loc 1 264 0
 681 0002a2  01 60 A9 	bclr.b _IFS1bits+1,#3
 265:lib/lib_pic33e/timer.c **** 	IEC1bits.T4IE = 1;			/* Timer 4 Interrupt Enable              */
 682              	.loc 1 265 0
 683 0002a4  01 60 A8 	bset.b _IEC1bits+1,#3
 266:lib/lib_pic33e/timer.c **** 
 267:lib/lib_pic33e/timer.c **** 	return;
 268:lib/lib_pic33e/timer.c **** }
 684              	.loc 1 268 0
 685 0002a6  8E 07 78 	mov w14,w15
 686 0002a8  4F 07 78 	mov [--w15],w14
 687 0002aa  00 40 A9 	bclr CORCON,#2
 688 0002ac  00 00 06 	return 
 689              	.set ___PA___,0
 690              	.LFE8:
 691              	.size _config_timer4_us,.-_config_timer4_us
 692              	.align 2
 693              	.global _config_timer5
MPLAB XC16 ASSEMBLY Listing:   			page 18


 694              	.type _config_timer5,@function
 695              	_config_timer5:
 696              	.LFB9:
 269:lib/lib_pic33e/timer.c **** 
 270:lib/lib_pic33e/timer.c **** /**********************************************************************
 271:lib/lib_pic33e/timer.c ****  * Name:    config_timer5
 272:lib/lib_pic33e/timer.c ****  * Args:    time - interrupt step in miliseconds; priority 0-7
 273:lib/lib_pic33e/timer.c ****  * Return:  -
 274:lib/lib_pic33e/timer.c ****  * Desc:    Configures and enables timer 1 module.
 275:lib/lib_pic33e/timer.c ****  **********************************************************************/
 276:lib/lib_pic33e/timer.c **** void config_timer5(unsigned int time, unsigned int priority, void
 277:lib/lib_pic33e/timer.c **** 		(*timer_function(void))){
 697              	.loc 1 277 0
 698              	.set ___PA___,1
 699 0002ae  06 00 FA 	lnk #6
 700              	.LCFI9:
 701              	.loc 1 277 0
 702 0002b0  00 0F 78 	mov w0,[w14]
 278:lib/lib_pic33e/timer.c **** 
 279:lib/lib_pic33e/timer.c **** 	T5CONbits.TCS   = 0;		/* use internal clock: Fcy               */
 703              	.loc 1 279 0
 704 0002b2  00 20 A9 	bclr.b _T5CONbits,#1
 705              	.loc 1 277 0
 706 0002b4  11 07 98 	mov w1,[w14+2]
 280:lib/lib_pic33e/timer.c **** 	T5CONbits.TGATE = 0;		/* Gated mode off                        */
 707              	.loc 1 280 0
 708 0002b6  00 C0 A9 	bclr.b _T5CONbits,#6
 709              	.loc 1 277 0
 710 0002b8  22 07 98 	mov w2,[w14+4]
 281:lib/lib_pic33e/timer.c **** 	T5CONbits.TCKPS = 3;		/* prescale 1:256                        */
 711              	.loc 1 281 0
 712 0002ba  02 00 80 	mov _T5CONbits,w2
 713 0002bc  00 03 20 	mov #48,w0
 282:lib/lib_pic33e/timer.c **** 	T5CONbits.TSIDL = 0;		/* don't stop the timer in idle          */
 283:lib/lib_pic33e/timer.c **** 
 284:lib/lib_pic33e/timer.c **** 	TMR5 = 0;					/* clears the timer register             */
 285:lib/lib_pic33e/timer.c **** 	PR5 = (M_SEC/256)*time;		/* value at which the register overflows *
 714              	.loc 1 285 0
 715 0002be  9E 00 78 	mov [w14],w1
 716              	.loc 1 281 0
 717 0002c0  02 01 70 	ior w0,w2,w2
 718              	.loc 1 285 0
 719 0002c2  A0 0F 20 	mov #250,w0
 720              	.loc 1 281 0
 721 0002c4  02 00 88 	mov w2,_T5CONbits
 722              	.loc 1 285 0
 723 0002c6  80 88 B9 	mulw.ss w1,w0,w0
 724 0002c8  80 00 78 	mov w0,w1
 725              	.loc 1 282 0
 726 0002ca  01 A0 A9 	bclr.b _T5CONbits+1,#5
 286:lib/lib_pic33e/timer.c **** 							     * and raises T1IF                       */
 287:lib/lib_pic33e/timer.c **** 
 288:lib/lib_pic33e/timer.c **** 	/* interruptions */
 289:lib/lib_pic33e/timer.c **** 	IPC7bits.T5IP = priority;	/* Timer 5 Interrupt Priority 0-7        */
 727              	.loc 1 289 0
 728 0002cc  1E 00 90 	mov [w14+2],w0
 729              	.loc 1 284 0
MPLAB XC16 ASSEMBLY Listing:   			page 19


 730 0002ce  00 20 EF 	clr _TMR5
 731              	.loc 1 289 0
 732 0002d0  00 40 78 	mov.b w0,w0
 733              	.loc 1 285 0
 734 0002d2  01 00 88 	mov w1,_PR5
 735              	.loc 1 289 0
 736 0002d4  67 40 60 	and.b w0,#7,w0
 737 0002d6  02 00 80 	mov _IPC7bits,w2
 738 0002d8  80 80 FB 	ze w0,w1
 739 0002da  80 FF 2F 	mov #-8,w0
 740 0002dc  E7 80 60 	and w1,#7,w1
 741 0002de  00 01 61 	and w2,w0,w2
 290:lib/lib_pic33e/timer.c **** 	IFS1bits.T5IF = 0;			/* clear interrupt flag                  */
 291:lib/lib_pic33e/timer.c **** 	IEC1bits.T5IE = 1;			/* Timer 5 Interrupt Enable              */
 292:lib/lib_pic33e/timer.c **** 
 293:lib/lib_pic33e/timer.c **** 	fun_ptr_timer5 = (timer_function == NULL) ? default_timer : timer_function;
 742              	.loc 1 293 0
 743 0002e0  2E 00 90 	mov [w14+4],w0
 744              	.loc 1 289 0
 745 0002e2  82 80 70 	ior w1,w2,w1
 746 0002e4  01 00 88 	mov w1,_IPC7bits
 747              	.loc 1 290 0
 748 0002e6  01 80 A9 	bclr.b _IFS1bits+1,#4
 749              	.loc 1 291 0
 750 0002e8  01 80 A8 	bset.b _IEC1bits+1,#4
 751              	.loc 1 293 0
 752 0002ea  00 00 E0 	cp0 w0
 753              	.set ___BP___,0
 754 0002ec  00 00 32 	bra z,.L19
 755 0002ee  2E 00 90 	mov [w14+4],w0
 756 0002f0  00 00 37 	bra .L20
 757              	.L19:
 758 0002f2  00 00 20 	mov #handle(_default_timer),w0
 759              	.L20:
 760 0002f4  00 00 88 	mov w0,_fun_ptr_timer5
 294:lib/lib_pic33e/timer.c **** 
 295:lib/lib_pic33e/timer.c **** 	T5CONbits.TON = 1;			/* starts the timer                      */
 761              	.loc 1 295 0
 762 0002f6  01 E0 A8 	bset.b _T5CONbits+1,#7
 296:lib/lib_pic33e/timer.c **** 	return;
 297:lib/lib_pic33e/timer.c **** }
 763              	.loc 1 297 0
 764 0002f8  8E 07 78 	mov w14,w15
 765 0002fa  4F 07 78 	mov [--w15],w14
 766 0002fc  00 40 A9 	bclr CORCON,#2
 767 0002fe  00 00 06 	return 
 768              	.set ___PA___,0
 769              	.LFE9:
 770              	.size _config_timer5,.-_config_timer5
 771              	.align 2
 772              	.global _config_timer5_us
 773              	.type _config_timer5_us,@function
 774              	_config_timer5_us:
 775              	.LFB10:
 298:lib/lib_pic33e/timer.c **** 
 299:lib/lib_pic33e/timer.c **** /**********************************************************************
 300:lib/lib_pic33e/timer.c ****  * Name:    config_timer5
MPLAB XC16 ASSEMBLY Listing:   			page 20


 301:lib/lib_pic33e/timer.c ****  * Args:    time - interrupt step in microseconds; priority 0-7
 302:lib/lib_pic33e/timer.c ****  * Return:  -
 303:lib/lib_pic33e/timer.c ****  * Desc:    Configures timer 1 module.
 304:lib/lib_pic33e/timer.c ****  **********************************************************************/
 305:lib/lib_pic33e/timer.c **** void config_timer5_us(unsigned int time, unsigned int priority){
 776              	.loc 1 305 0
 777              	.set ___PA___,1
 778 000300  04 00 FA 	lnk #4
 779              	.LCFI10:
 780              	.loc 1 305 0
 781 000302  00 0F 78 	mov w0,[w14]
 306:lib/lib_pic33e/timer.c **** 
 307:lib/lib_pic33e/timer.c **** 	T5CONbits.TCS   = 0;		/* use internal clock: Fcy               */
 782              	.loc 1 307 0
 783 000304  00 20 A9 	bclr.b _T5CONbits,#1
 784              	.loc 1 305 0
 785 000306  11 07 98 	mov w1,[w14+2]
 308:lib/lib_pic33e/timer.c **** 	T5CONbits.TGATE = 0;		/* Gated mode off                        */
 786              	.loc 1 308 0
 787 000308  00 C0 A9 	bclr.b _T5CONbits,#6
 309:lib/lib_pic33e/timer.c **** 	T5CONbits.TCKPS = 1;		/* prescale 1:8                          */
 788              	.loc 1 309 0
 789 00030a  F0 FC 2F 	mov #-49,w0
 790 00030c  02 00 80 	mov _T5CONbits,w2
 310:lib/lib_pic33e/timer.c **** 	T5CONbits.TSIDL = 0;		/* don't stop the timer in idle          */
 311:lib/lib_pic33e/timer.c **** 
 312:lib/lib_pic33e/timer.c **** 	TMR5 = 0;					/* clears the timer register             */
 313:lib/lib_pic33e/timer.c **** 	PR5 = U_SEC*time/8; 		/* value at which the register overflows *
 791              	.loc 1 313 0
 792 00030e  9E 00 78 	mov [w14],w1
 793              	.loc 1 309 0
 794 000310  00 00 61 	and w2,w0,w0
 795              	.loc 1 313 0
 796 000312  61 09 B8 	mul.uu w1,#1,w2
 797              	.loc 1 309 0
 798 000314  00 02 78 	mov w0,w4
 799 000316  04 40 A0 	bset w4,#4
 800              	.loc 1 313 0
 801 000318  46 18 DD 	sl w3,#6,w0
 802 00031a  CA 10 DE 	lsr w2,#10,w1
 803 00031c  81 00 70 	ior w0,w1,w1
 804 00031e  46 10 DD 	sl w2,#6,w0
 805              	.loc 1 309 0
 806 000320  04 00 88 	mov w4,_T5CONbits
 807              	.loc 1 313 0
 808 000322  4D 09 DD 	sl w1,#13,w2
 809 000324  43 00 DE 	lsr w0,#3,w0
 810 000326  00 00 71 	ior w2,w0,w0
 811 000328  C3 08 DE 	lsr w1,#3,w1
 812              	.loc 1 310 0
 813 00032a  01 A0 A9 	bclr.b _T5CONbits+1,#5
 814              	.loc 1 313 0
 815 00032c  80 00 78 	mov w0,w1
 816              	.loc 1 312 0
 817 00032e  00 20 EF 	clr _TMR5
 314:lib/lib_pic33e/timer.c **** 							     * and raises T1IF                       */
 315:lib/lib_pic33e/timer.c **** 
MPLAB XC16 ASSEMBLY Listing:   			page 21


 316:lib/lib_pic33e/timer.c **** 	/* interruptions */
 317:lib/lib_pic33e/timer.c **** 	IPC7bits.T5IP = priority;	/* Timer 5 Interrupt Priority 0-7        */
 818              	.loc 1 317 0
 819 000330  1E 00 90 	mov [w14+2],w0
 820              	.loc 1 313 0
 821 000332  01 00 88 	mov w1,_PR5
 822              	.loc 1 317 0
 823 000334  00 40 78 	mov.b w0,w0
 824 000336  02 00 80 	mov _IPC7bits,w2
 825 000338  67 40 60 	and.b w0,#7,w0
 826 00033a  81 FF 2F 	mov #-8,w1
 827 00033c  00 80 FB 	ze w0,w0
 828 00033e  81 00 61 	and w2,w1,w1
 829 000340  67 00 60 	and w0,#7,w0
 830 000342  01 00 70 	ior w0,w1,w0
 831 000344  00 00 88 	mov w0,_IPC7bits
 318:lib/lib_pic33e/timer.c **** 	IFS1bits.T5IF = 0;			/* clear interrupt flag                  */
 832              	.loc 1 318 0
 833 000346  01 80 A9 	bclr.b _IFS1bits+1,#4
 319:lib/lib_pic33e/timer.c **** 	IEC1bits.T5IE = 1;			/* Timer 5 Interrupt Enable              */
 834              	.loc 1 319 0
 835 000348  01 80 A8 	bset.b _IEC1bits+1,#4
 320:lib/lib_pic33e/timer.c **** 
 321:lib/lib_pic33e/timer.c **** 	return;
 322:lib/lib_pic33e/timer.c **** }
 836              	.loc 1 322 0
 837 00034a  8E 07 78 	mov w14,w15
 838 00034c  4F 07 78 	mov [--w15],w14
 839 00034e  00 40 A9 	bclr CORCON,#2
 840 000350  00 00 06 	return 
 841              	.set ___PA___,0
 842              	.LFE10:
 843              	.size _config_timer5_us,.-_config_timer5_us
 844              	.align 2
 845              	.global _config_timer6
 846              	.type _config_timer6,@function
 847              	_config_timer6:
 848              	.LFB11:
 323:lib/lib_pic33e/timer.c **** 
 324:lib/lib_pic33e/timer.c **** /**********************************************************************
 325:lib/lib_pic33e/timer.c ****  * Name:    config_timer6 (type B)
 326:lib/lib_pic33e/timer.c ****  * Args:    time - interrupt step in miliseconds; priority 0-7
 327:lib/lib_pic33e/timer.c ****  * Return:  -
 328:lib/lib_pic33e/timer.c ****  * Desc:    Configures and enables timer 1 module.
 329:lib/lib_pic33e/timer.c ****  **********************************************************************/
 330:lib/lib_pic33e/timer.c **** void config_timer6(unsigned int time, unsigned int priority, void
 331:lib/lib_pic33e/timer.c **** 		(*timer_function(void))){
 849              	.loc 1 331 0
 850              	.set ___PA___,1
 851 000352  06 00 FA 	lnk #6
 852              	.LCFI11:
 853              	.loc 1 331 0
 854 000354  00 0F 78 	mov w0,[w14]
 332:lib/lib_pic33e/timer.c **** 
 333:lib/lib_pic33e/timer.c **** 	T6CONbits.TCS   = 0;		/* use internal clock: Fcy               */
 855              	.loc 1 333 0
 856 000356  00 20 A9 	bclr.b _T6CONbits,#1
MPLAB XC16 ASSEMBLY Listing:   			page 22


 857              	.loc 1 331 0
 858 000358  11 07 98 	mov w1,[w14+2]
 334:lib/lib_pic33e/timer.c **** 	T6CONbits.TGATE = 0;		/* Gated mode off                        */
 859              	.loc 1 334 0
 860 00035a  00 C0 A9 	bclr.b _T6CONbits,#6
 861              	.loc 1 331 0
 862 00035c  22 07 98 	mov w2,[w14+4]
 335:lib/lib_pic33e/timer.c **** 	T6CONbits.TCKPS = 3;		/* prescale 1:256                        */
 863              	.loc 1 335 0
 864 00035e  02 00 80 	mov _T6CONbits,w2
 865 000360  00 03 20 	mov #48,w0
 336:lib/lib_pic33e/timer.c **** 	T6CONbits.TSIDL = 0;		/* don't stop the timer in idle          */
 337:lib/lib_pic33e/timer.c **** 	T6CONbits.T32 = 0;          /*TMRx and TMRy form 16-bit timer        */
 338:lib/lib_pic33e/timer.c **** 
 339:lib/lib_pic33e/timer.c **** 	TMR6 = 0;					/* clears the timer register             */
 340:lib/lib_pic33e/timer.c **** 	PR6 = (M_SEC/256)*time;		/* value at which the register overflows *
 866              	.loc 1 340 0
 867 000362  9E 00 78 	mov [w14],w1
 868              	.loc 1 335 0
 869 000364  02 01 70 	ior w0,w2,w2
 870              	.loc 1 340 0
 871 000366  A0 0F 20 	mov #250,w0
 872              	.loc 1 335 0
 873 000368  02 00 88 	mov w2,_T6CONbits
 874              	.loc 1 340 0
 875 00036a  80 88 B9 	mulw.ss w1,w0,w0
 876 00036c  80 00 78 	mov w0,w1
 877              	.loc 1 336 0
 878 00036e  01 A0 A9 	bclr.b _T6CONbits+1,#5
 341:lib/lib_pic33e/timer.c **** 								 * and raises T1IF                       */
 342:lib/lib_pic33e/timer.c **** 
 343:lib/lib_pic33e/timer.c **** 	/* interruptions */
 344:lib/lib_pic33e/timer.c **** 	IPC11bits.T6IP = priority;	/* Timer 2 Interrupt Priority 0-7        */
 879              	.loc 1 344 0
 880 000370  1E 00 90 	mov [w14+2],w0
 881              	.loc 1 337 0
 882 000372  00 60 A9 	bclr.b _T6CONbits,#3
 883              	.loc 1 344 0
 884 000374  00 40 78 	mov.b w0,w0
 885              	.loc 1 339 0
 886 000376  00 20 EF 	clr _TMR6
 887              	.loc 1 344 0
 888 000378  67 40 60 	and.b w0,#7,w0
 889              	.loc 1 340 0
 890 00037a  01 00 88 	mov w1,_PR6
 891              	.loc 1 344 0
 892 00037c  00 80 FB 	ze w0,w0
 893 00037e  02 00 80 	mov _IPC11bits,w2
 894 000380  E7 00 60 	and w0,#7,w1
 895 000382  F0 FF 28 	mov #-28673,w0
 896 000384  CC 08 DD 	sl w1,#12,w1
 897 000386  00 01 61 	and w2,w0,w2
 345:lib/lib_pic33e/timer.c **** 	IFS2bits.T6IF = 0;			/* clear interrupt flag                  */
 346:lib/lib_pic33e/timer.c **** 	IEC2bits.T6IE = 1;			/* Timer 2 Interrupt Enable              */
 347:lib/lib_pic33e/timer.c **** 
 348:lib/lib_pic33e/timer.c **** 	fun_ptr_timer6 = (timer_function == NULL) ? default_timer : timer_function;
 898              	.loc 1 348 0
MPLAB XC16 ASSEMBLY Listing:   			page 23


 899 000388  2E 00 90 	mov [w14+4],w0
 900              	.loc 1 344 0
 901 00038a  82 80 70 	ior w1,w2,w1
 902 00038c  01 00 88 	mov w1,_IPC11bits
 903              	.loc 1 345 0
 904 00038e  01 E0 A9 	bclr.b _IFS2bits+1,#7
 905              	.loc 1 346 0
 906 000390  01 E0 A8 	bset.b _IEC2bits+1,#7
 907              	.loc 1 348 0
 908 000392  00 00 E0 	cp0 w0
 909              	.set ___BP___,0
 910 000394  00 00 32 	bra z,.L23
 911 000396  2E 00 90 	mov [w14+4],w0
 912 000398  00 00 37 	bra .L24
 913              	.L23:
 914 00039a  00 00 20 	mov #handle(_default_timer),w0
 915              	.L24:
 916 00039c  00 00 88 	mov w0,_fun_ptr_timer6
 349:lib/lib_pic33e/timer.c **** 
 350:lib/lib_pic33e/timer.c **** 	T6CONbits.TON = 1;			/* starts the timer                      */
 917              	.loc 1 350 0
 918 00039e  01 E0 A8 	bset.b _T6CONbits+1,#7
 351:lib/lib_pic33e/timer.c **** 	return;
 352:lib/lib_pic33e/timer.c **** }
 919              	.loc 1 352 0
 920 0003a0  8E 07 78 	mov w14,w15
 921 0003a2  4F 07 78 	mov [--w15],w14
 922 0003a4  00 40 A9 	bclr CORCON,#2
 923 0003a6  00 00 06 	return 
 924              	.set ___PA___,0
 925              	.LFE11:
 926              	.size _config_timer6,.-_config_timer6
 927              	.align 2
 928              	.global _config_timer6_us
 929              	.type _config_timer6_us,@function
 930              	_config_timer6_us:
 931              	.LFB12:
 353:lib/lib_pic33e/timer.c **** 
 354:lib/lib_pic33e/timer.c **** /**********************************************************************
 355:lib/lib_pic33e/timer.c ****  * Name:    config_timer6 (type B)
 356:lib/lib_pic33e/timer.c ****  * Args:    time - interrupt step in microseconds; priority 0-7
 357:lib/lib_pic33e/timer.c ****  * Return:  -
 358:lib/lib_pic33e/timer.c ****  * Desc:    Configures and enables timer 1 module.
 359:lib/lib_pic33e/timer.c ****  **********************************************************************/
 360:lib/lib_pic33e/timer.c **** void config_timer6_us(unsigned int time, unsigned int priority){
 932              	.loc 1 360 0
 933              	.set ___PA___,1
 934 0003a8  04 00 FA 	lnk #4
 935              	.LCFI12:
 936              	.loc 1 360 0
 937 0003aa  00 0F 78 	mov w0,[w14]
 361:lib/lib_pic33e/timer.c **** 
 362:lib/lib_pic33e/timer.c **** 	T6CONbits.TCS   = 0;		/* use internal clock: Fcy               */
 938              	.loc 1 362 0
 939 0003ac  00 20 A9 	bclr.b _T6CONbits,#1
 940              	.loc 1 360 0
 941 0003ae  11 07 98 	mov w1,[w14+2]
MPLAB XC16 ASSEMBLY Listing:   			page 24


 363:lib/lib_pic33e/timer.c **** 	T6CONbits.TGATE = 0;		/* Gated mode off                        */
 942              	.loc 1 363 0
 943 0003b0  00 C0 A9 	bclr.b _T6CONbits,#6
 364:lib/lib_pic33e/timer.c **** 	T6CONbits.TCKPS = 1;		/* prescale 1:8                          */
 944              	.loc 1 364 0
 945 0003b2  F0 FC 2F 	mov #-49,w0
 946 0003b4  02 00 80 	mov _T6CONbits,w2
 365:lib/lib_pic33e/timer.c **** 	T6CONbits.TSIDL = 0;		/* don't stop the timer in idle          */
 366:lib/lib_pic33e/timer.c **** 	T6CONbits.T32 = 0;          /*TMRx and TMRy form 16-bit timer        */
 367:lib/lib_pic33e/timer.c **** 
 368:lib/lib_pic33e/timer.c **** 	TMR6 = 0;					/* clears the timer register             */
 369:lib/lib_pic33e/timer.c **** 	PR6 = U_SEC*time/8; 		/* value at which the register overflows *
 947              	.loc 1 369 0
 948 0003b6  9E 00 78 	mov [w14],w1
 949              	.loc 1 364 0
 950 0003b8  00 00 61 	and w2,w0,w0
 951              	.loc 1 369 0
 952 0003ba  61 09 B8 	mul.uu w1,#1,w2
 953              	.loc 1 364 0
 954 0003bc  00 02 78 	mov w0,w4
 955 0003be  04 40 A0 	bset w4,#4
 956              	.loc 1 369 0
 957 0003c0  46 18 DD 	sl w3,#6,w0
 958 0003c2  CA 10 DE 	lsr w2,#10,w1
 959 0003c4  81 00 70 	ior w0,w1,w1
 960 0003c6  46 10 DD 	sl w2,#6,w0
 961              	.loc 1 364 0
 962 0003c8  04 00 88 	mov w4,_T6CONbits
 963              	.loc 1 369 0
 964 0003ca  4D 09 DD 	sl w1,#13,w2
 965 0003cc  43 00 DE 	lsr w0,#3,w0
 966 0003ce  00 00 71 	ior w2,w0,w0
 967 0003d0  C3 08 DE 	lsr w1,#3,w1
 968              	.loc 1 365 0
 969 0003d2  01 A0 A9 	bclr.b _T6CONbits+1,#5
 970              	.loc 1 369 0
 971 0003d4  80 00 78 	mov w0,w1
 972              	.loc 1 366 0
 973 0003d6  00 60 A9 	bclr.b _T6CONbits,#3
 370:lib/lib_pic33e/timer.c **** 								 * and raises T1IF                       */
 371:lib/lib_pic33e/timer.c **** 
 372:lib/lib_pic33e/timer.c **** 	/* interruptions */
 373:lib/lib_pic33e/timer.c **** 	IPC11bits.T6IP = priority;	/* Timer 2 Interrupt Priority 0-7        */
 974              	.loc 1 373 0
 975 0003d8  1E 00 90 	mov [w14+2],w0
 976              	.loc 1 368 0
 977 0003da  00 20 EF 	clr _TMR6
 978              	.loc 1 373 0
 979 0003dc  00 40 78 	mov.b w0,w0
 980              	.loc 1 369 0
 981 0003de  01 00 88 	mov w1,_PR6
 982              	.loc 1 373 0
 983 0003e0  67 40 60 	and.b w0,#7,w0
 984 0003e2  02 00 80 	mov _IPC11bits,w2
 985 0003e4  00 80 FB 	ze w0,w0
 986 0003e6  F1 FF 28 	mov #-28673,w1
 987 0003e8  67 00 60 	and w0,#7,w0
MPLAB XC16 ASSEMBLY Listing:   			page 25


 988 0003ea  81 00 61 	and w2,w1,w1
 989 0003ec  4C 00 DD 	sl w0,#12,w0
 990 0003ee  01 00 70 	ior w0,w1,w0
 991 0003f0  00 00 88 	mov w0,_IPC11bits
 374:lib/lib_pic33e/timer.c **** 	IFS2bits.T6IF = 0;			/* clear interrupt flag                  */
 992              	.loc 1 374 0
 993 0003f2  01 E0 A9 	bclr.b _IFS2bits+1,#7
 375:lib/lib_pic33e/timer.c **** 	IEC2bits.T6IE = 1;			/* Timer 2 Interrupt Enable              */
 994              	.loc 1 375 0
 995 0003f4  01 E0 A8 	bset.b _IEC2bits+1,#7
 376:lib/lib_pic33e/timer.c **** 
 377:lib/lib_pic33e/timer.c **** 	return;
 378:lib/lib_pic33e/timer.c **** }
 996              	.loc 1 378 0
 997 0003f6  8E 07 78 	mov w14,w15
 998 0003f8  4F 07 78 	mov [--w15],w14
 999 0003fa  00 40 A9 	bclr CORCON,#2
 1000 0003fc  00 00 06 	return 
 1001              	.set ___PA___,0
 1002              	.LFE12:
 1003              	.size _config_timer6_us,.-_config_timer6_us
 1004              	.align 2
 1005              	.global _config_timer7
 1006              	.type _config_timer7,@function
 1007              	_config_timer7:
 1008              	.LFB13:
 379:lib/lib_pic33e/timer.c **** 
 380:lib/lib_pic33e/timer.c **** /**********************************************************************
 381:lib/lib_pic33e/timer.c ****  * Name:    config_timer7		(type C)
 382:lib/lib_pic33e/timer.c ****  * Args:    time - interrupt step in miliseconds; priority 0-7
 383:lib/lib_pic33e/timer.c ****  * Return:  -
 384:lib/lib_pic33e/timer.c ****  * Desc:    Configures and enables timer 1 module.
 385:lib/lib_pic33e/timer.c ****  **********************************************************************/
 386:lib/lib_pic33e/timer.c **** void config_timer7(unsigned int time, unsigned int priority, void
 387:lib/lib_pic33e/timer.c **** 		(*timer_function(void))){
 1009              	.loc 1 387 0
 1010              	.set ___PA___,1
 1011 0003fe  06 00 FA 	lnk #6
 1012              	.LCFI13:
 1013              	.loc 1 387 0
 1014 000400  00 0F 78 	mov w0,[w14]
 388:lib/lib_pic33e/timer.c **** 
 389:lib/lib_pic33e/timer.c **** 	T7CONbits.TCS   = 0;		/* use internal clock: Fcy               */
 1015              	.loc 1 389 0
 1016 000402  00 20 A9 	bclr.b _T7CONbits,#1
 1017              	.loc 1 387 0
 1018 000404  11 07 98 	mov w1,[w14+2]
 390:lib/lib_pic33e/timer.c **** 	T7CONbits.TGATE = 0;		/* Gated mode off                        */
 1019              	.loc 1 390 0
 1020 000406  00 C0 A9 	bclr.b _T7CONbits,#6
 1021              	.loc 1 387 0
 1022 000408  22 07 98 	mov w2,[w14+4]
 391:lib/lib_pic33e/timer.c **** 	T7CONbits.TCKPS = 3;		/* prescale 1:256                        */
 1023              	.loc 1 391 0
 1024 00040a  02 00 80 	mov _T7CONbits,w2
 1025 00040c  00 03 20 	mov #48,w0
 392:lib/lib_pic33e/timer.c **** 	T7CONbits.TSIDL = 0;		/* don't stop the timer in idle          */
MPLAB XC16 ASSEMBLY Listing:   			page 26


 393:lib/lib_pic33e/timer.c **** 
 394:lib/lib_pic33e/timer.c **** 	TMR7 = 0;					/* clears the timer register             */
 395:lib/lib_pic33e/timer.c **** 	PR7 = (M_SEC/256)*time;		/* value at which the register overflows *
 1026              	.loc 1 395 0
 1027 00040e  9E 00 78 	mov [w14],w1
 1028              	.loc 1 391 0
 1029 000410  02 01 70 	ior w0,w2,w2
 1030              	.loc 1 395 0
 1031 000412  A0 0F 20 	mov #250,w0
 1032              	.loc 1 391 0
 1033 000414  02 00 88 	mov w2,_T7CONbits
 1034              	.loc 1 395 0
 1035 000416  80 88 B9 	mulw.ss w1,w0,w0
 1036 000418  80 00 78 	mov w0,w1
 1037              	.loc 1 392 0
 1038 00041a  01 A0 A9 	bclr.b _T7CONbits+1,#5
 396:lib/lib_pic33e/timer.c **** 								 * and raises T1IF                       */
 397:lib/lib_pic33e/timer.c **** 
 398:lib/lib_pic33e/timer.c **** 	/* interruptions */
 399:lib/lib_pic33e/timer.c **** 	IPC12bits.T7IP = priority;	/* Timer 3 Interrupt Priority 0-7        */
 1039              	.loc 1 399 0
 1040 00041c  1E 00 90 	mov [w14+2],w0
 1041              	.loc 1 394 0
 1042 00041e  00 20 EF 	clr _TMR7
 1043              	.loc 1 399 0
 1044 000420  00 40 78 	mov.b w0,w0
 1045              	.loc 1 395 0
 1046 000422  01 00 88 	mov w1,_PR7
 1047              	.loc 1 399 0
 1048 000424  67 40 60 	and.b w0,#7,w0
 1049 000426  02 00 80 	mov _IPC12bits,w2
 1050 000428  80 80 FB 	ze w0,w1
 1051 00042a  80 FF 2F 	mov #-8,w0
 1052 00042c  E7 80 60 	and w1,#7,w1
 1053 00042e  00 01 61 	and w2,w0,w2
 400:lib/lib_pic33e/timer.c **** 	IFS3bits.T7IF = 0;			/* clear interrupt flag                  */
 401:lib/lib_pic33e/timer.c **** 	IEC3bits.T7IE = 1;			/* Timer 3 Interrupt Enable              */
 402:lib/lib_pic33e/timer.c **** 
 403:lib/lib_pic33e/timer.c **** 	fun_ptr_timer7 = (timer_function == NULL) ? default_timer : timer_function;
 1054              	.loc 1 403 0
 1055 000430  2E 00 90 	mov [w14+4],w0
 1056              	.loc 1 399 0
 1057 000432  82 80 70 	ior w1,w2,w1
 1058 000434  01 00 88 	mov w1,_IPC12bits
 1059              	.loc 1 400 0
 1060 000436  00 00 A9 	bclr.b _IFS3bits,#0
 1061              	.loc 1 401 0
 1062 000438  00 00 A8 	bset.b _IEC3bits,#0
 1063              	.loc 1 403 0
 1064 00043a  00 00 E0 	cp0 w0
 1065              	.set ___BP___,0
 1066 00043c  00 00 32 	bra z,.L27
 1067 00043e  2E 00 90 	mov [w14+4],w0
 1068 000440  00 00 37 	bra .L28
 1069              	.L27:
 1070 000442  00 00 20 	mov #handle(_default_timer),w0
 1071              	.L28:
MPLAB XC16 ASSEMBLY Listing:   			page 27


 1072 000444  00 00 88 	mov w0,_fun_ptr_timer7
 404:lib/lib_pic33e/timer.c **** 
 405:lib/lib_pic33e/timer.c **** 	T7CONbits.TON = 1;			/* starts the timer                      */
 1073              	.loc 1 405 0
 1074 000446  01 E0 A8 	bset.b _T7CONbits+1,#7
 406:lib/lib_pic33e/timer.c **** 	return;
 407:lib/lib_pic33e/timer.c **** }
 1075              	.loc 1 407 0
 1076 000448  8E 07 78 	mov w14,w15
 1077 00044a  4F 07 78 	mov [--w15],w14
 1078 00044c  00 40 A9 	bclr CORCON,#2
 1079 00044e  00 00 06 	return 
 1080              	.set ___PA___,0
 1081              	.LFE13:
 1082              	.size _config_timer7,.-_config_timer7
 1083              	.align 2
 1084              	.global _config_timer7_us
 1085              	.type _config_timer7_us,@function
 1086              	_config_timer7_us:
 1087              	.LFB14:
 408:lib/lib_pic33e/timer.c **** 
 409:lib/lib_pic33e/timer.c **** /**********************************************************************
 410:lib/lib_pic33e/timer.c ****  * Name:    config_timer7		(type C)
 411:lib/lib_pic33e/timer.c ****  * Args:    time - interrupt step in microseconds; priority 0-7
 412:lib/lib_pic33e/timer.c ****  * Return:  -
 413:lib/lib_pic33e/timer.c ****  * Desc:    Configures timer 1 module.
 414:lib/lib_pic33e/timer.c ****  **********************************************************************/
 415:lib/lib_pic33e/timer.c **** void config_timer7_us(unsigned int time, unsigned int priority){
 1088              	.loc 1 415 0
 1089              	.set ___PA___,1
 1090 000450  04 00 FA 	lnk #4
 1091              	.LCFI14:
 1092              	.loc 1 415 0
 1093 000452  00 0F 78 	mov w0,[w14]
 416:lib/lib_pic33e/timer.c **** 
 417:lib/lib_pic33e/timer.c **** 	T7CONbits.TCS   = 0;		/* use internal clock: Fcy               */
 1094              	.loc 1 417 0
 1095 000454  00 20 A9 	bclr.b _T7CONbits,#1
 1096              	.loc 1 415 0
 1097 000456  11 07 98 	mov w1,[w14+2]
 418:lib/lib_pic33e/timer.c **** 	T7CONbits.TGATE = 0;		/* Gated mode off                        */
 1098              	.loc 1 418 0
 1099 000458  00 C0 A9 	bclr.b _T7CONbits,#6
 419:lib/lib_pic33e/timer.c **** 	T7CONbits.TCKPS = 1;		/* prescale 1:8                          */
 1100              	.loc 1 419 0
 1101 00045a  F0 FC 2F 	mov #-49,w0
 1102 00045c  02 00 80 	mov _T7CONbits,w2
 420:lib/lib_pic33e/timer.c **** 	T7CONbits.TSIDL = 0;		/* don't stop the timer in idle          */
 421:lib/lib_pic33e/timer.c **** 
 422:lib/lib_pic33e/timer.c **** 	TMR7 = 0;					/* clears the timer register             */
 423:lib/lib_pic33e/timer.c **** 	PR7 = U_SEC*time/8; 		/* value at which the register overflows *
 1103              	.loc 1 423 0
 1104 00045e  9E 00 78 	mov [w14],w1
 1105              	.loc 1 419 0
 1106 000460  00 00 61 	and w2,w0,w0
 1107              	.loc 1 423 0
 1108 000462  61 09 B8 	mul.uu w1,#1,w2
MPLAB XC16 ASSEMBLY Listing:   			page 28


 1109              	.loc 1 419 0
 1110 000464  00 02 78 	mov w0,w4
 1111 000466  04 40 A0 	bset w4,#4
 1112              	.loc 1 423 0
 1113 000468  46 18 DD 	sl w3,#6,w0
 1114 00046a  CA 10 DE 	lsr w2,#10,w1
 1115 00046c  81 00 70 	ior w0,w1,w1
 1116 00046e  46 10 DD 	sl w2,#6,w0
 1117              	.loc 1 419 0
 1118 000470  04 00 88 	mov w4,_T7CONbits
 1119              	.loc 1 423 0
 1120 000472  4D 09 DD 	sl w1,#13,w2
 1121 000474  43 00 DE 	lsr w0,#3,w0
 1122 000476  00 00 71 	ior w2,w0,w0
 1123 000478  C3 08 DE 	lsr w1,#3,w1
 1124              	.loc 1 420 0
 1125 00047a  01 A0 A9 	bclr.b _T7CONbits+1,#5
 1126              	.loc 1 423 0
 1127 00047c  80 00 78 	mov w0,w1
 1128              	.loc 1 422 0
 1129 00047e  00 20 EF 	clr _TMR7
 424:lib/lib_pic33e/timer.c **** 								 * and raises T1IF                       */
 425:lib/lib_pic33e/timer.c **** 
 426:lib/lib_pic33e/timer.c **** 	/* interruptions */
 427:lib/lib_pic33e/timer.c **** 	IPC12bits.T7IP = priority;	/* Timer 3 Interrupt Priority 0-7        */
 1130              	.loc 1 427 0
 1131 000480  1E 00 90 	mov [w14+2],w0
 1132              	.loc 1 423 0
 1133 000482  01 00 88 	mov w1,_PR7
 1134              	.loc 1 427 0
 1135 000484  00 40 78 	mov.b w0,w0
 1136 000486  02 00 80 	mov _IPC12bits,w2
 1137 000488  67 40 60 	and.b w0,#7,w0
 1138 00048a  81 FF 2F 	mov #-8,w1
 1139 00048c  00 80 FB 	ze w0,w0
 1140 00048e  81 00 61 	and w2,w1,w1
 1141 000490  67 00 60 	and w0,#7,w0
 1142 000492  01 00 70 	ior w0,w1,w0
 1143 000494  00 00 88 	mov w0,_IPC12bits
 428:lib/lib_pic33e/timer.c **** 	IFS3bits.T7IF = 0;			/* clear interrupt flag                  */
 1144              	.loc 1 428 0
 1145 000496  00 00 A9 	bclr.b _IFS3bits,#0
 429:lib/lib_pic33e/timer.c **** 	IEC3bits.T7IE = 1;			/* Timer 3 Interrupt Enable              */
 1146              	.loc 1 429 0
 1147 000498  00 00 A8 	bset.b _IEC3bits,#0
 430:lib/lib_pic33e/timer.c **** 
 431:lib/lib_pic33e/timer.c **** 	return;
 432:lib/lib_pic33e/timer.c **** }
 1148              	.loc 1 432 0
 1149 00049a  8E 07 78 	mov w14,w15
 1150 00049c  4F 07 78 	mov [--w15],w14
 1151 00049e  00 40 A9 	bclr CORCON,#2
 1152 0004a0  00 00 06 	return 
 1153              	.set ___PA___,0
 1154              	.LFE14:
 1155              	.size _config_timer7_us,.-_config_timer7_us
 1156              	.align 2
MPLAB XC16 ASSEMBLY Listing:   			page 29


 1157              	.global _config_timer8
 1158              	.type _config_timer8,@function
 1159              	_config_timer8:
 1160              	.LFB15:
 433:lib/lib_pic33e/timer.c **** 
 434:lib/lib_pic33e/timer.c **** /**********************************************************************
 435:lib/lib_pic33e/timer.c ****  * Name:    config_timer8 (type B)
 436:lib/lib_pic33e/timer.c ****  * Args:    time - interrupt step in miliseconds; priority 0-7
 437:lib/lib_pic33e/timer.c ****  * Return:  -
 438:lib/lib_pic33e/timer.c ****  * Desc:    Configures and enables timer 1 module.
 439:lib/lib_pic33e/timer.c ****  **********************************************************************/
 440:lib/lib_pic33e/timer.c **** void config_timer8(unsigned int time, unsigned int priority, void
 441:lib/lib_pic33e/timer.c **** 		(*timer_function(void))){
 1161              	.loc 1 441 0
 1162              	.set ___PA___,1
 1163 0004a2  06 00 FA 	lnk #6
 1164              	.LCFI15:
 1165              	.loc 1 441 0
 1166 0004a4  00 0F 78 	mov w0,[w14]
 442:lib/lib_pic33e/timer.c **** 
 443:lib/lib_pic33e/timer.c **** 	T8CONbits.TCS   = 0;		/* use internal clock: Fcy               */
 1167              	.loc 1 443 0
 1168 0004a6  00 20 A9 	bclr.b _T8CONbits,#1
 1169              	.loc 1 441 0
 1170 0004a8  11 07 98 	mov w1,[w14+2]
 444:lib/lib_pic33e/timer.c **** 	T8CONbits.TGATE = 0;		/* Gated mode off                        */
 1171              	.loc 1 444 0
 1172 0004aa  00 C0 A9 	bclr.b _T8CONbits,#6
 1173              	.loc 1 441 0
 1174 0004ac  22 07 98 	mov w2,[w14+4]
 445:lib/lib_pic33e/timer.c **** 	T8CONbits.TCKPS = 3;		/* prescale 1:256                        */
 1175              	.loc 1 445 0
 1176 0004ae  02 00 80 	mov _T8CONbits,w2
 1177 0004b0  00 03 20 	mov #48,w0
 446:lib/lib_pic33e/timer.c **** 	T8CONbits.TSIDL = 0;		/* don't stop the timer in idle          */
 447:lib/lib_pic33e/timer.c **** 	T8CONbits.T32 = 0;          /*TMRx and TMRy form 16-bit timer        */
 448:lib/lib_pic33e/timer.c **** 
 449:lib/lib_pic33e/timer.c **** 	TMR8 = 0;					/* clears the timer register             */
 450:lib/lib_pic33e/timer.c **** 	PR8 = (M_SEC/256)*time;		/* value at which the register overflows *
 1178              	.loc 1 450 0
 1179 0004b2  9E 00 78 	mov [w14],w1
 1180              	.loc 1 445 0
 1181 0004b4  02 01 70 	ior w0,w2,w2
 1182              	.loc 1 450 0
 1183 0004b6  A0 0F 20 	mov #250,w0
 1184              	.loc 1 445 0
 1185 0004b8  02 00 88 	mov w2,_T8CONbits
 1186              	.loc 1 450 0
 1187 0004ba  80 88 B9 	mulw.ss w1,w0,w0
 1188 0004bc  80 00 78 	mov w0,w1
 1189              	.loc 1 446 0
 1190 0004be  01 A0 A9 	bclr.b _T8CONbits+1,#5
 451:lib/lib_pic33e/timer.c **** 								 * and raises T1IF                       */
 452:lib/lib_pic33e/timer.c **** 
 453:lib/lib_pic33e/timer.c **** 	/* interruptions */
 454:lib/lib_pic33e/timer.c **** 	IPC12bits.T8IP = priority;	/* Timer 2 Interrupt Priority 0-7        */
 1191              	.loc 1 454 0
MPLAB XC16 ASSEMBLY Listing:   			page 30


 1192 0004c0  1E 00 90 	mov [w14+2],w0
 1193              	.loc 1 447 0
 1194 0004c2  00 60 A9 	bclr.b _T8CONbits,#3
 1195              	.loc 1 454 0
 1196 0004c4  00 40 78 	mov.b w0,w0
 1197              	.loc 1 449 0
 1198 0004c6  00 20 EF 	clr _TMR8
 1199              	.loc 1 454 0
 1200 0004c8  67 40 60 	and.b w0,#7,w0
 1201              	.loc 1 450 0
 1202 0004ca  01 00 88 	mov w1,_PR8
 1203              	.loc 1 454 0
 1204 0004cc  00 80 FB 	ze w0,w0
 1205 0004ce  02 00 80 	mov _IPC12bits,w2
 1206 0004d0  E7 00 60 	and w0,#7,w1
 1207 0004d2  F0 FF 28 	mov #-28673,w0
 1208 0004d4  CC 08 DD 	sl w1,#12,w1
 1209 0004d6  00 01 61 	and w2,w0,w2
 455:lib/lib_pic33e/timer.c **** 	IFS3bits.T8IF = 0;			/* clear interrupt flag                  */
 456:lib/lib_pic33e/timer.c **** 	IEC3bits.T8IE = 1;			/* Timer 2 Interrupt Enable              */
 457:lib/lib_pic33e/timer.c **** 
 458:lib/lib_pic33e/timer.c **** 	fun_ptr_timer8 = (timer_function == NULL) ? default_timer : timer_function;
 1210              	.loc 1 458 0
 1211 0004d8  2E 00 90 	mov [w14+4],w0
 1212              	.loc 1 454 0
 1213 0004da  82 80 70 	ior w1,w2,w1
 1214 0004dc  01 00 88 	mov w1,_IPC12bits
 1215              	.loc 1 455 0
 1216 0004de  00 60 A9 	bclr.b _IFS3bits,#3
 1217              	.loc 1 456 0
 1218 0004e0  00 60 A8 	bset.b _IEC3bits,#3
 1219              	.loc 1 458 0
 1220 0004e2  00 00 E0 	cp0 w0
 1221              	.set ___BP___,0
 1222 0004e4  00 00 32 	bra z,.L31
 1223 0004e6  2E 00 90 	mov [w14+4],w0
 1224 0004e8  00 00 37 	bra .L32
 1225              	.L31:
 1226 0004ea  00 00 20 	mov #handle(_default_timer),w0
 1227              	.L32:
 1228 0004ec  00 00 88 	mov w0,_fun_ptr_timer8
 459:lib/lib_pic33e/timer.c **** 
 460:lib/lib_pic33e/timer.c **** 	T8CONbits.TON = 1;			/* starts the timer                      */
 1229              	.loc 1 460 0
 1230 0004ee  01 E0 A8 	bset.b _T8CONbits+1,#7
 461:lib/lib_pic33e/timer.c **** 	return;
 462:lib/lib_pic33e/timer.c **** }
 1231              	.loc 1 462 0
 1232 0004f0  8E 07 78 	mov w14,w15
 1233 0004f2  4F 07 78 	mov [--w15],w14
 1234 0004f4  00 40 A9 	bclr CORCON,#2
 1235 0004f6  00 00 06 	return 
 1236              	.set ___PA___,0
 1237              	.LFE15:
 1238              	.size _config_timer8,.-_config_timer8
 1239              	.align 2
 1240              	.global _config_timer8_us
MPLAB XC16 ASSEMBLY Listing:   			page 31


 1241              	.type _config_timer8_us,@function
 1242              	_config_timer8_us:
 1243              	.LFB16:
 463:lib/lib_pic33e/timer.c **** 
 464:lib/lib_pic33e/timer.c **** /**********************************************************************
 465:lib/lib_pic33e/timer.c ****  * Name:    config_timer8 (type B)
 466:lib/lib_pic33e/timer.c ****  * Args:    time - interrupt step in microseconds; priority 0-7
 467:lib/lib_pic33e/timer.c ****  * Return:  -
 468:lib/lib_pic33e/timer.c ****  * Desc:    Configures and enables timer 1 module.
 469:lib/lib_pic33e/timer.c ****  **********************************************************************/
 470:lib/lib_pic33e/timer.c **** void config_timer8_us(unsigned int time, unsigned int priority){
 1244              	.loc 1 470 0
 1245              	.set ___PA___,1
 1246 0004f8  04 00 FA 	lnk #4
 1247              	.LCFI16:
 1248              	.loc 1 470 0
 1249 0004fa  00 0F 78 	mov w0,[w14]
 471:lib/lib_pic33e/timer.c **** 
 472:lib/lib_pic33e/timer.c **** 	T8CONbits.TCS   = 0;		/* use internal clock: Fcy               */
 1250              	.loc 1 472 0
 1251 0004fc  00 20 A9 	bclr.b _T8CONbits,#1
 1252              	.loc 1 470 0
 1253 0004fe  11 07 98 	mov w1,[w14+2]
 473:lib/lib_pic33e/timer.c **** 	T8CONbits.TGATE = 0;		/* Gated mode off                        */
 1254              	.loc 1 473 0
 1255 000500  00 C0 A9 	bclr.b _T8CONbits,#6
 474:lib/lib_pic33e/timer.c **** 	T8CONbits.TCKPS = 1;		/* prescale 1:8                          */
 1256              	.loc 1 474 0
 1257 000502  F0 FC 2F 	mov #-49,w0
 1258 000504  02 00 80 	mov _T8CONbits,w2
 475:lib/lib_pic33e/timer.c **** 	T8CONbits.TSIDL = 0;		/* don't stop the timer in idle          */
 476:lib/lib_pic33e/timer.c **** 	T8CONbits.T32 = 0;          /*TMRx and TMRy form 16-bit timer        */
 477:lib/lib_pic33e/timer.c **** 
 478:lib/lib_pic33e/timer.c **** 	TMR8 = 0;					/* clears the timer register             */
 479:lib/lib_pic33e/timer.c **** 	PR8 = U_SEC*time/8; 		/* value at which the register overflows *
 1259              	.loc 1 479 0
 1260 000506  9E 00 78 	mov [w14],w1
 1261              	.loc 1 474 0
 1262 000508  00 00 61 	and w2,w0,w0
 1263              	.loc 1 479 0
 1264 00050a  61 09 B8 	mul.uu w1,#1,w2
 1265              	.loc 1 474 0
 1266 00050c  00 02 78 	mov w0,w4
 1267 00050e  04 40 A0 	bset w4,#4
 1268              	.loc 1 479 0
 1269 000510  46 18 DD 	sl w3,#6,w0
 1270 000512  CA 10 DE 	lsr w2,#10,w1
 1271 000514  81 00 70 	ior w0,w1,w1
 1272 000516  46 10 DD 	sl w2,#6,w0
 1273              	.loc 1 474 0
 1274 000518  04 00 88 	mov w4,_T8CONbits
 1275              	.loc 1 479 0
 1276 00051a  4D 09 DD 	sl w1,#13,w2
 1277 00051c  43 00 DE 	lsr w0,#3,w0
 1278 00051e  00 00 71 	ior w2,w0,w0
 1279 000520  C3 08 DE 	lsr w1,#3,w1
 1280              	.loc 1 475 0
MPLAB XC16 ASSEMBLY Listing:   			page 32


 1281 000522  01 A0 A9 	bclr.b _T8CONbits+1,#5
 1282              	.loc 1 479 0
 1283 000524  80 00 78 	mov w0,w1
 1284              	.loc 1 476 0
 1285 000526  00 60 A9 	bclr.b _T8CONbits,#3
 480:lib/lib_pic33e/timer.c **** 								 * and raises T1IF                       */
 481:lib/lib_pic33e/timer.c **** 
 482:lib/lib_pic33e/timer.c **** 	/* interruptions */
 483:lib/lib_pic33e/timer.c **** 	IPC12bits.T8IP = priority;	/* Timer 2 Interrupt Priority 0-7        */
 1286              	.loc 1 483 0
 1287 000528  1E 00 90 	mov [w14+2],w0
 1288              	.loc 1 478 0
 1289 00052a  00 20 EF 	clr _TMR8
 1290              	.loc 1 483 0
 1291 00052c  00 40 78 	mov.b w0,w0
 1292              	.loc 1 479 0
 1293 00052e  01 00 88 	mov w1,_PR8
 1294              	.loc 1 483 0
 1295 000530  67 40 60 	and.b w0,#7,w0
 1296 000532  02 00 80 	mov _IPC12bits,w2
 1297 000534  00 80 FB 	ze w0,w0
 1298 000536  F1 FF 28 	mov #-28673,w1
 1299 000538  67 00 60 	and w0,#7,w0
 1300 00053a  81 00 61 	and w2,w1,w1
 1301 00053c  4C 00 DD 	sl w0,#12,w0
 1302 00053e  01 00 70 	ior w0,w1,w0
 1303 000540  00 00 88 	mov w0,_IPC12bits
 484:lib/lib_pic33e/timer.c **** 	IFS3bits.T8IF = 0;			/* clear interrupt flag                  */
 1304              	.loc 1 484 0
 1305 000542  00 60 A9 	bclr.b _IFS3bits,#3
 485:lib/lib_pic33e/timer.c **** 	IEC3bits.T8IE = 1;			/* Timer 2 Interrupt Enable              */
 1306              	.loc 1 485 0
 1307 000544  00 60 A8 	bset.b _IEC3bits,#3
 486:lib/lib_pic33e/timer.c **** 
 487:lib/lib_pic33e/timer.c **** 	return;
 488:lib/lib_pic33e/timer.c **** }
 1308              	.loc 1 488 0
 1309 000546  8E 07 78 	mov w14,w15
 1310 000548  4F 07 78 	mov [--w15],w14
 1311 00054a  00 40 A9 	bclr CORCON,#2
 1312 00054c  00 00 06 	return 
 1313              	.set ___PA___,0
 1314              	.LFE16:
 1315              	.size _config_timer8_us,.-_config_timer8_us
 1316              	.align 2
 1317              	.global _config_timer9
 1318              	.type _config_timer9,@function
 1319              	_config_timer9:
 1320              	.LFB17:
 489:lib/lib_pic33e/timer.c **** 
 490:lib/lib_pic33e/timer.c **** /**********************************************************************
 491:lib/lib_pic33e/timer.c ****  * Name:    config_timer3		(type C)
 492:lib/lib_pic33e/timer.c ****  * Args:    time - interrupt step in miliseconds; priority 0-7
 493:lib/lib_pic33e/timer.c ****  * Return:  -
 494:lib/lib_pic33e/timer.c ****  * Desc:    Configures and enables timer 1 module.
 495:lib/lib_pic33e/timer.c ****  **********************************************************************/
 496:lib/lib_pic33e/timer.c **** void config_timer9(unsigned int time, unsigned int priority, void
MPLAB XC16 ASSEMBLY Listing:   			page 33


 497:lib/lib_pic33e/timer.c **** 		(*timer_function(void))){
 1321              	.loc 1 497 0
 1322              	.set ___PA___,1
 1323 00054e  06 00 FA 	lnk #6
 1324              	.LCFI17:
 1325              	.loc 1 497 0
 1326 000550  00 0F 78 	mov w0,[w14]
 498:lib/lib_pic33e/timer.c **** 
 499:lib/lib_pic33e/timer.c **** 	T9CONbits.TCS   = 0;		/* use internal clock: Fcy               */
 1327              	.loc 1 499 0
 1328 000552  00 20 A9 	bclr.b _T9CONbits,#1
 1329              	.loc 1 497 0
 1330 000554  11 07 98 	mov w1,[w14+2]
 500:lib/lib_pic33e/timer.c **** 	T9CONbits.TGATE = 0;		/* Gated mode off                        */
 1331              	.loc 1 500 0
 1332 000556  00 C0 A9 	bclr.b _T9CONbits,#6
 1333              	.loc 1 497 0
 1334 000558  22 07 98 	mov w2,[w14+4]
 501:lib/lib_pic33e/timer.c **** 	T9CONbits.TCKPS = 3;		/* prescale 1:256                        */
 1335              	.loc 1 501 0
 1336 00055a  02 00 80 	mov _T9CONbits,w2
 1337 00055c  00 03 20 	mov #48,w0
 502:lib/lib_pic33e/timer.c **** 	T9CONbits.TSIDL = 0;		/* don't stop the timer in idle          */
 503:lib/lib_pic33e/timer.c **** 
 504:lib/lib_pic33e/timer.c **** 	TMR9 = 0;					/* clears the timer register             */
 505:lib/lib_pic33e/timer.c **** 	PR9 = (M_SEC/256)*time;		/* value at which the register overflows *
 1338              	.loc 1 505 0
 1339 00055e  9E 00 78 	mov [w14],w1
 1340              	.loc 1 501 0
 1341 000560  02 01 70 	ior w0,w2,w2
 1342              	.loc 1 505 0
 1343 000562  A0 0F 20 	mov #250,w0
 1344              	.loc 1 501 0
 1345 000564  02 00 88 	mov w2,_T9CONbits
 1346              	.loc 1 505 0
 1347 000566  80 88 B9 	mulw.ss w1,w0,w0
 1348 000568  80 00 78 	mov w0,w1
 1349              	.loc 1 502 0
 1350 00056a  01 A0 A9 	bclr.b _T9CONbits+1,#5
 506:lib/lib_pic33e/timer.c **** 								 * and raises T1IF                       */
 507:lib/lib_pic33e/timer.c **** 
 508:lib/lib_pic33e/timer.c **** 	/* interruptions */
 509:lib/lib_pic33e/timer.c **** 	IPC13bits.T9IP = priority;	/* Timer 3 Interrupt Priority 0-7        */
 1351              	.loc 1 509 0
 1352 00056c  1E 00 90 	mov [w14+2],w0
 1353              	.loc 1 504 0
 1354 00056e  00 20 EF 	clr _TMR9
 1355              	.loc 1 509 0
 1356 000570  00 40 78 	mov.b w0,w0
 1357              	.loc 1 505 0
 1358 000572  01 00 88 	mov w1,_PR9
 1359              	.loc 1 509 0
 1360 000574  67 40 60 	and.b w0,#7,w0
 1361 000576  02 00 80 	mov _IPC13bits,w2
 1362 000578  80 80 FB 	ze w0,w1
 1363 00057a  80 FF 2F 	mov #-8,w0
 1364 00057c  E7 80 60 	and w1,#7,w1
MPLAB XC16 ASSEMBLY Listing:   			page 34


 1365 00057e  00 01 61 	and w2,w0,w2
 510:lib/lib_pic33e/timer.c **** 	IFS3bits.T9IF = 0;			/* clear interrupt flag                  */
 511:lib/lib_pic33e/timer.c **** 	IEC3bits.T9IE = 1;			/* Timer 3 Interrupt Enable              */
 512:lib/lib_pic33e/timer.c **** 
 513:lib/lib_pic33e/timer.c **** 	fun_ptr_timer9 = (timer_function == NULL) ? default_timer : timer_function;
 1366              	.loc 1 513 0
 1367 000580  2E 00 90 	mov [w14+4],w0
 1368              	.loc 1 509 0
 1369 000582  82 80 70 	ior w1,w2,w1
 1370 000584  01 00 88 	mov w1,_IPC13bits
 1371              	.loc 1 510 0
 1372 000586  00 80 A9 	bclr.b _IFS3bits,#4
 1373              	.loc 1 511 0
 1374 000588  00 80 A8 	bset.b _IEC3bits,#4
 1375              	.loc 1 513 0
 1376 00058a  00 00 E0 	cp0 w0
 1377              	.set ___BP___,0
 1378 00058c  00 00 32 	bra z,.L35
 1379 00058e  2E 00 90 	mov [w14+4],w0
 1380 000590  00 00 37 	bra .L36
 1381              	.L35:
 1382 000592  00 00 20 	mov #handle(_default_timer),w0
 1383              	.L36:
 1384 000594  00 00 88 	mov w0,_fun_ptr_timer9
 514:lib/lib_pic33e/timer.c **** 
 515:lib/lib_pic33e/timer.c **** 	T9CONbits.TON = 1;			/* starts the timer                      */
 1385              	.loc 1 515 0
 1386 000596  01 E0 A8 	bset.b _T9CONbits+1,#7
 516:lib/lib_pic33e/timer.c **** 	return;
 517:lib/lib_pic33e/timer.c **** }
 1387              	.loc 1 517 0
 1388 000598  8E 07 78 	mov w14,w15
 1389 00059a  4F 07 78 	mov [--w15],w14
 1390 00059c  00 40 A9 	bclr CORCON,#2
 1391 00059e  00 00 06 	return 
 1392              	.set ___PA___,0
 1393              	.LFE17:
 1394              	.size _config_timer9,.-_config_timer9
 1395              	.align 2
 1396              	.global _config_timer9_us
 1397              	.type _config_timer9_us,@function
 1398              	_config_timer9_us:
 1399              	.LFB18:
 518:lib/lib_pic33e/timer.c **** 
 519:lib/lib_pic33e/timer.c **** /**********************************************************************
 520:lib/lib_pic33e/timer.c ****  * Name:    config_timer3		(type C)
 521:lib/lib_pic33e/timer.c ****  * Args:    time - interrupt step in microseconds; priority 0-7
 522:lib/lib_pic33e/timer.c ****  * Return:  -
 523:lib/lib_pic33e/timer.c ****  * Desc:    Configures timer 1 module.
 524:lib/lib_pic33e/timer.c ****  **********************************************************************/
 525:lib/lib_pic33e/timer.c **** void config_timer9_us(unsigned int time, unsigned int priority){
 1400              	.loc 1 525 0
 1401              	.set ___PA___,1
 1402 0005a0  04 00 FA 	lnk #4
 1403              	.LCFI18:
 1404              	.loc 1 525 0
 1405 0005a2  00 0F 78 	mov w0,[w14]
MPLAB XC16 ASSEMBLY Listing:   			page 35


 526:lib/lib_pic33e/timer.c **** 
 527:lib/lib_pic33e/timer.c **** 	T9CONbits.TCS   = 0;		/* use internal clock: Fcy               */
 1406              	.loc 1 527 0
 1407 0005a4  00 20 A9 	bclr.b _T9CONbits,#1
 1408              	.loc 1 525 0
 1409 0005a6  11 07 98 	mov w1,[w14+2]
 528:lib/lib_pic33e/timer.c **** 	T9CONbits.TGATE = 0;		/* Gated mode off                        */
 1410              	.loc 1 528 0
 1411 0005a8  00 C0 A9 	bclr.b _T9CONbits,#6
 529:lib/lib_pic33e/timer.c **** 	T9CONbits.TCKPS = 1;		/* prescale 1:8                          */
 1412              	.loc 1 529 0
 1413 0005aa  F0 FC 2F 	mov #-49,w0
 1414 0005ac  02 00 80 	mov _T9CONbits,w2
 530:lib/lib_pic33e/timer.c **** 	T9CONbits.TSIDL = 0;		/* don't stop the timer in idle          */
 531:lib/lib_pic33e/timer.c **** 
 532:lib/lib_pic33e/timer.c **** 	TMR9 = 0;					/* clears the timer register             */
 533:lib/lib_pic33e/timer.c **** 	PR9 = U_SEC*time/8; 		/* value at which the register overflows *
 1415              	.loc 1 533 0
 1416 0005ae  9E 00 78 	mov [w14],w1
 1417              	.loc 1 529 0
 1418 0005b0  00 00 61 	and w2,w0,w0
 1419              	.loc 1 533 0
 1420 0005b2  61 09 B8 	mul.uu w1,#1,w2
 1421              	.loc 1 529 0
 1422 0005b4  00 02 78 	mov w0,w4
 1423 0005b6  04 40 A0 	bset w4,#4
 1424              	.loc 1 533 0
 1425 0005b8  46 18 DD 	sl w3,#6,w0
 1426 0005ba  CA 10 DE 	lsr w2,#10,w1
 1427 0005bc  81 00 70 	ior w0,w1,w1
 1428 0005be  46 10 DD 	sl w2,#6,w0
 1429              	.loc 1 529 0
 1430 0005c0  04 00 88 	mov w4,_T9CONbits
 1431              	.loc 1 533 0
 1432 0005c2  4D 09 DD 	sl w1,#13,w2
 1433 0005c4  43 00 DE 	lsr w0,#3,w0
 1434 0005c6  00 00 71 	ior w2,w0,w0
 1435 0005c8  C3 08 DE 	lsr w1,#3,w1
 1436              	.loc 1 530 0
 1437 0005ca  01 A0 A9 	bclr.b _T9CONbits+1,#5
 1438              	.loc 1 533 0
 1439 0005cc  80 00 78 	mov w0,w1
 1440              	.loc 1 532 0
 1441 0005ce  00 20 EF 	clr _TMR9
 534:lib/lib_pic33e/timer.c **** 								 * and raises T1IF                       */
 535:lib/lib_pic33e/timer.c **** 
 536:lib/lib_pic33e/timer.c **** 	/* interruptions */
 537:lib/lib_pic33e/timer.c **** 	IPC13bits.T9IP = priority;	/* Timer 3 Interrupt Priority 0-7        */
 1442              	.loc 1 537 0
 1443 0005d0  1E 00 90 	mov [w14+2],w0
 1444              	.loc 1 533 0
 1445 0005d2  01 00 88 	mov w1,_PR9
 1446              	.loc 1 537 0
 1447 0005d4  00 40 78 	mov.b w0,w0
 1448 0005d6  02 00 80 	mov _IPC13bits,w2
 1449 0005d8  67 40 60 	and.b w0,#7,w0
 1450 0005da  81 FF 2F 	mov #-8,w1
MPLAB XC16 ASSEMBLY Listing:   			page 36


 1451 0005dc  00 80 FB 	ze w0,w0
 1452 0005de  81 00 61 	and w2,w1,w1
 1453 0005e0  67 00 60 	and w0,#7,w0
 1454 0005e2  01 00 70 	ior w0,w1,w0
 1455 0005e4  00 00 88 	mov w0,_IPC13bits
 538:lib/lib_pic33e/timer.c **** 	IFS3bits.T9IF = 0;			/* clear interrupt flag                  */
 1456              	.loc 1 538 0
 1457 0005e6  00 80 A9 	bclr.b _IFS3bits,#4
 539:lib/lib_pic33e/timer.c **** 	IEC3bits.T9IE = 1;			/* Timer 3 Interrupt Enable              */
 1458              	.loc 1 539 0
 1459 0005e8  00 80 A8 	bset.b _IEC3bits,#4
 540:lib/lib_pic33e/timer.c **** 
 541:lib/lib_pic33e/timer.c **** 	return;
 542:lib/lib_pic33e/timer.c **** }
 1460              	.loc 1 542 0
 1461 0005ea  8E 07 78 	mov w14,w15
 1462 0005ec  4F 07 78 	mov [--w15],w14
 1463 0005ee  00 40 A9 	bclr CORCON,#2
 1464 0005f0  00 00 06 	return 
 1465              	.set ___PA___,0
 1466              	.LFE18:
 1467              	.size _config_timer9_us,.-_config_timer9_us
 1468              	.align 2
 1469              	.global _enable_timer1
 1470              	.type _enable_timer1,@function
 1471              	_enable_timer1:
 1472              	.LFB19:
 543:lib/lib_pic33e/timer.c **** 
 544:lib/lib_pic33e/timer.c **** /**
 545:lib/lib_pic33e/timer.c ****  * @brief Enables timer 1
 546:lib/lib_pic33e/timer.c ****  */
 547:lib/lib_pic33e/timer.c **** void enable_timer1(void){
 1473              	.loc 1 547 0
 1474              	.set ___PA___,1
 1475 0005f2  00 00 FA 	lnk #0
 1476              	.LCFI19:
 548:lib/lib_pic33e/timer.c ****     T1CONbits.TON = 1;
 1477              	.loc 1 548 0
 1478 0005f4  01 E0 A8 	bset.b _T1CONbits+1,#7
 549:lib/lib_pic33e/timer.c **** }
 1479              	.loc 1 549 0
 1480 0005f6  8E 07 78 	mov w14,w15
 1481 0005f8  4F 07 78 	mov [--w15],w14
 1482 0005fa  00 40 A9 	bclr CORCON,#2
 1483 0005fc  00 00 06 	return 
 1484              	.set ___PA___,0
 1485              	.LFE19:
 1486              	.size _enable_timer1,.-_enable_timer1
 1487              	.align 2
 1488              	.global _enable_timer2
 1489              	.type _enable_timer2,@function
 1490              	_enable_timer2:
 1491              	.LFB20:
 550:lib/lib_pic33e/timer.c **** /**
 551:lib/lib_pic33e/timer.c ****  * @brief Enables timer 2
 552:lib/lib_pic33e/timer.c ****  */
 553:lib/lib_pic33e/timer.c **** void enable_timer2(void){
MPLAB XC16 ASSEMBLY Listing:   			page 37


 1492              	.loc 1 553 0
 1493              	.set ___PA___,1
 1494 0005fe  00 00 FA 	lnk #0
 1495              	.LCFI20:
 554:lib/lib_pic33e/timer.c ****     T2CONbits.TON = 1;
 1496              	.loc 1 554 0
 1497 000600  01 E0 A8 	bset.b _T2CONbits+1,#7
 555:lib/lib_pic33e/timer.c **** }
 1498              	.loc 1 555 0
 1499 000602  8E 07 78 	mov w14,w15
 1500 000604  4F 07 78 	mov [--w15],w14
 1501 000606  00 40 A9 	bclr CORCON,#2
 1502 000608  00 00 06 	return 
 1503              	.set ___PA___,0
 1504              	.LFE20:
 1505              	.size _enable_timer2,.-_enable_timer2
 1506              	.align 2
 1507              	.global _enable_timer3
 1508              	.type _enable_timer3,@function
 1509              	_enable_timer3:
 1510              	.LFB21:
 556:lib/lib_pic33e/timer.c **** /**
 557:lib/lib_pic33e/timer.c ****  * @brief Enables timer 3
 558:lib/lib_pic33e/timer.c ****  */
 559:lib/lib_pic33e/timer.c **** void enable_timer3(void){
 1511              	.loc 1 559 0
 1512              	.set ___PA___,1
 1513 00060a  00 00 FA 	lnk #0
 1514              	.LCFI21:
 560:lib/lib_pic33e/timer.c ****     T3CONbits.TON = 1;
 1515              	.loc 1 560 0
 1516 00060c  01 E0 A8 	bset.b _T3CONbits+1,#7
 561:lib/lib_pic33e/timer.c **** }
 1517              	.loc 1 561 0
 1518 00060e  8E 07 78 	mov w14,w15
 1519 000610  4F 07 78 	mov [--w15],w14
 1520 000612  00 40 A9 	bclr CORCON,#2
 1521 000614  00 00 06 	return 
 1522              	.set ___PA___,0
 1523              	.LFE21:
 1524              	.size _enable_timer3,.-_enable_timer3
 1525              	.align 2
 1526              	.global _enable_timer4
 1527              	.type _enable_timer4,@function
 1528              	_enable_timer4:
 1529              	.LFB22:
 562:lib/lib_pic33e/timer.c **** /**
 563:lib/lib_pic33e/timer.c ****  * @brief Enables timer 4
 564:lib/lib_pic33e/timer.c ****  */
 565:lib/lib_pic33e/timer.c **** void enable_timer4(void){
 1530              	.loc 1 565 0
 1531              	.set ___PA___,1
 1532 000616  00 00 FA 	lnk #0
 1533              	.LCFI22:
 566:lib/lib_pic33e/timer.c ****     T4CONbits.TON = 1;
 1534              	.loc 1 566 0
 1535 000618  01 E0 A8 	bset.b _T4CONbits+1,#7
MPLAB XC16 ASSEMBLY Listing:   			page 38


 567:lib/lib_pic33e/timer.c **** }
 1536              	.loc 1 567 0
 1537 00061a  8E 07 78 	mov w14,w15
 1538 00061c  4F 07 78 	mov [--w15],w14
 1539 00061e  00 40 A9 	bclr CORCON,#2
 1540 000620  00 00 06 	return 
 1541              	.set ___PA___,0
 1542              	.LFE22:
 1543              	.size _enable_timer4,.-_enable_timer4
 1544              	.align 2
 1545              	.global _enable_timer5
 1546              	.type _enable_timer5,@function
 1547              	_enable_timer5:
 1548              	.LFB23:
 568:lib/lib_pic33e/timer.c **** /**
 569:lib/lib_pic33e/timer.c ****  * @brief Enables timer 5
 570:lib/lib_pic33e/timer.c ****  */
 571:lib/lib_pic33e/timer.c **** void enable_timer5(void){
 1549              	.loc 1 571 0
 1550              	.set ___PA___,1
 1551 000622  00 00 FA 	lnk #0
 1552              	.LCFI23:
 572:lib/lib_pic33e/timer.c ****     T5CONbits.TON = 1;
 1553              	.loc 1 572 0
 1554 000624  01 E0 A8 	bset.b _T5CONbits+1,#7
 573:lib/lib_pic33e/timer.c **** }
 1555              	.loc 1 573 0
 1556 000626  8E 07 78 	mov w14,w15
 1557 000628  4F 07 78 	mov [--w15],w14
 1558 00062a  00 40 A9 	bclr CORCON,#2
 1559 00062c  00 00 06 	return 
 1560              	.set ___PA___,0
 1561              	.LFE23:
 1562              	.size _enable_timer5,.-_enable_timer5
 1563              	.align 2
 1564              	.global _enable_timer6
 1565              	.type _enable_timer6,@function
 1566              	_enable_timer6:
 1567              	.LFB24:
 574:lib/lib_pic33e/timer.c **** /**
 575:lib/lib_pic33e/timer.c ****  * @brief Enables timer 6
 576:lib/lib_pic33e/timer.c ****  */
 577:lib/lib_pic33e/timer.c **** void enable_timer6(void){
 1568              	.loc 1 577 0
 1569              	.set ___PA___,1
 1570 00062e  00 00 FA 	lnk #0
 1571              	.LCFI24:
 578:lib/lib_pic33e/timer.c ****     T6CONbits.TON = 1;
 1572              	.loc 1 578 0
 1573 000630  01 E0 A8 	bset.b _T6CONbits+1,#7
 579:lib/lib_pic33e/timer.c **** }
 1574              	.loc 1 579 0
 1575 000632  8E 07 78 	mov w14,w15
 1576 000634  4F 07 78 	mov [--w15],w14
 1577 000636  00 40 A9 	bclr CORCON,#2
 1578 000638  00 00 06 	return 
 1579              	.set ___PA___,0
MPLAB XC16 ASSEMBLY Listing:   			page 39


 1580              	.LFE24:
 1581              	.size _enable_timer6,.-_enable_timer6
 1582              	.align 2
 1583              	.global _enable_timer7
 1584              	.type _enable_timer7,@function
 1585              	_enable_timer7:
 1586              	.LFB25:
 580:lib/lib_pic33e/timer.c **** /**
 581:lib/lib_pic33e/timer.c ****  * @brief Enables timer 7
 582:lib/lib_pic33e/timer.c ****  */
 583:lib/lib_pic33e/timer.c **** void enable_timer7(void){
 1587              	.loc 1 583 0
 1588              	.set ___PA___,1
 1589 00063a  00 00 FA 	lnk #0
 1590              	.LCFI25:
 584:lib/lib_pic33e/timer.c ****     T7CONbits.TON = 1;
 1591              	.loc 1 584 0
 1592 00063c  01 E0 A8 	bset.b _T7CONbits+1,#7
 585:lib/lib_pic33e/timer.c **** }
 1593              	.loc 1 585 0
 1594 00063e  8E 07 78 	mov w14,w15
 1595 000640  4F 07 78 	mov [--w15],w14
 1596 000642  00 40 A9 	bclr CORCON,#2
 1597 000644  00 00 06 	return 
 1598              	.set ___PA___,0
 1599              	.LFE25:
 1600              	.size _enable_timer7,.-_enable_timer7
 1601              	.align 2
 1602              	.global _enable_timer8
 1603              	.type _enable_timer8,@function
 1604              	_enable_timer8:
 1605              	.LFB26:
 586:lib/lib_pic33e/timer.c **** /**
 587:lib/lib_pic33e/timer.c ****  * @brief Enables timer 8
 588:lib/lib_pic33e/timer.c ****  */
 589:lib/lib_pic33e/timer.c **** void enable_timer8(void){
 1606              	.loc 1 589 0
 1607              	.set ___PA___,1
 1608 000646  00 00 FA 	lnk #0
 1609              	.LCFI26:
 590:lib/lib_pic33e/timer.c ****     T8CONbits.TON = 1;
 1610              	.loc 1 590 0
 1611 000648  01 E0 A8 	bset.b _T8CONbits+1,#7
 591:lib/lib_pic33e/timer.c **** }
 1612              	.loc 1 591 0
 1613 00064a  8E 07 78 	mov w14,w15
 1614 00064c  4F 07 78 	mov [--w15],w14
 1615 00064e  00 40 A9 	bclr CORCON,#2
 1616 000650  00 00 06 	return 
 1617              	.set ___PA___,0
 1618              	.LFE26:
 1619              	.size _enable_timer8,.-_enable_timer8
 1620              	.align 2
 1621              	.global _enable_timer9
 1622              	.type _enable_timer9,@function
 1623              	_enable_timer9:
 1624              	.LFB27:
MPLAB XC16 ASSEMBLY Listing:   			page 40


 592:lib/lib_pic33e/timer.c **** /**
 593:lib/lib_pic33e/timer.c ****  * @brief Enables timer 9
 594:lib/lib_pic33e/timer.c ****  */
 595:lib/lib_pic33e/timer.c **** void enable_timer9(void){
 1625              	.loc 1 595 0
 1626              	.set ___PA___,1
 1627 000652  00 00 FA 	lnk #0
 1628              	.LCFI27:
 596:lib/lib_pic33e/timer.c ****     T9CONbits.TON = 1;
 1629              	.loc 1 596 0
 1630 000654  01 E0 A8 	bset.b _T9CONbits+1,#7
 597:lib/lib_pic33e/timer.c **** }
 1631              	.loc 1 597 0
 1632 000656  8E 07 78 	mov w14,w15
 1633 000658  4F 07 78 	mov [--w15],w14
 1634 00065a  00 40 A9 	bclr CORCON,#2
 1635 00065c  00 00 06 	return 
 1636              	.set ___PA___,0
 1637              	.LFE27:
 1638              	.size _enable_timer9,.-_enable_timer9
 1639              	.align 2
 1640              	.global _disable_timer1
 1641              	.type _disable_timer1,@function
 1642              	_disable_timer1:
 1643              	.LFB28:
 598:lib/lib_pic33e/timer.c **** 
 599:lib/lib_pic33e/timer.c **** /**
 600:lib/lib_pic33e/timer.c ****  * @brief Disables timer 1
 601:lib/lib_pic33e/timer.c ****  */
 602:lib/lib_pic33e/timer.c **** void disable_timer1(void){
 1644              	.loc 1 602 0
 1645              	.set ___PA___,1
 1646 00065e  00 00 FA 	lnk #0
 1647              	.LCFI28:
 603:lib/lib_pic33e/timer.c ****     T1CONbits.TON = 0;
 1648              	.loc 1 603 0
 1649 000660  01 E0 A9 	bclr.b _T1CONbits+1,#7
 604:lib/lib_pic33e/timer.c **** }
 1650              	.loc 1 604 0
 1651 000662  8E 07 78 	mov w14,w15
 1652 000664  4F 07 78 	mov [--w15],w14
 1653 000666  00 40 A9 	bclr CORCON,#2
 1654 000668  00 00 06 	return 
 1655              	.set ___PA___,0
 1656              	.LFE28:
 1657              	.size _disable_timer1,.-_disable_timer1
 1658              	.align 2
 1659              	.global _disable_timer2
 1660              	.type _disable_timer2,@function
 1661              	_disable_timer2:
 1662              	.LFB29:
 605:lib/lib_pic33e/timer.c **** /**
 606:lib/lib_pic33e/timer.c ****  * @brief Disables timer 2
 607:lib/lib_pic33e/timer.c ****  */
 608:lib/lib_pic33e/timer.c **** void disable_timer2(void){
 1663              	.loc 1 608 0
 1664              	.set ___PA___,1
MPLAB XC16 ASSEMBLY Listing:   			page 41


 1665 00066a  00 00 FA 	lnk #0
 1666              	.LCFI29:
 609:lib/lib_pic33e/timer.c ****     T2CONbits.TON = 0;
 1667              	.loc 1 609 0
 1668 00066c  01 E0 A9 	bclr.b _T2CONbits+1,#7
 610:lib/lib_pic33e/timer.c **** }
 1669              	.loc 1 610 0
 1670 00066e  8E 07 78 	mov w14,w15
 1671 000670  4F 07 78 	mov [--w15],w14
 1672 000672  00 40 A9 	bclr CORCON,#2
 1673 000674  00 00 06 	return 
 1674              	.set ___PA___,0
 1675              	.LFE29:
 1676              	.size _disable_timer2,.-_disable_timer2
 1677              	.align 2
 1678              	.global _disable_timer3
 1679              	.type _disable_timer3,@function
 1680              	_disable_timer3:
 1681              	.LFB30:
 611:lib/lib_pic33e/timer.c **** /**
 612:lib/lib_pic33e/timer.c ****  * @brief Disables timer 3
 613:lib/lib_pic33e/timer.c ****  */
 614:lib/lib_pic33e/timer.c **** void disable_timer3(void){
 1682              	.loc 1 614 0
 1683              	.set ___PA___,1
 1684 000676  00 00 FA 	lnk #0
 1685              	.LCFI30:
 615:lib/lib_pic33e/timer.c ****     T3CONbits.TON = 0;
 1686              	.loc 1 615 0
 1687 000678  01 E0 A9 	bclr.b _T3CONbits+1,#7
 616:lib/lib_pic33e/timer.c **** }
 1688              	.loc 1 616 0
 1689 00067a  8E 07 78 	mov w14,w15
 1690 00067c  4F 07 78 	mov [--w15],w14
 1691 00067e  00 40 A9 	bclr CORCON,#2
 1692 000680  00 00 06 	return 
 1693              	.set ___PA___,0
 1694              	.LFE30:
 1695              	.size _disable_timer3,.-_disable_timer3
 1696              	.align 2
 1697              	.global _disable_timer4
 1698              	.type _disable_timer4,@function
 1699              	_disable_timer4:
 1700              	.LFB31:
 617:lib/lib_pic33e/timer.c **** /**
 618:lib/lib_pic33e/timer.c ****  * @brief Disables timer 4
 619:lib/lib_pic33e/timer.c ****  */
 620:lib/lib_pic33e/timer.c **** void disable_timer4(void){
 1701              	.loc 1 620 0
 1702              	.set ___PA___,1
 1703 000682  00 00 FA 	lnk #0
 1704              	.LCFI31:
 621:lib/lib_pic33e/timer.c ****     T4CONbits.TON = 0;
 1705              	.loc 1 621 0
 1706 000684  01 E0 A9 	bclr.b _T4CONbits+1,#7
 622:lib/lib_pic33e/timer.c **** }
 1707              	.loc 1 622 0
MPLAB XC16 ASSEMBLY Listing:   			page 42


 1708 000686  8E 07 78 	mov w14,w15
 1709 000688  4F 07 78 	mov [--w15],w14
 1710 00068a  00 40 A9 	bclr CORCON,#2
 1711 00068c  00 00 06 	return 
 1712              	.set ___PA___,0
 1713              	.LFE31:
 1714              	.size _disable_timer4,.-_disable_timer4
 1715              	.align 2
 1716              	.global _disable_timer5
 1717              	.type _disable_timer5,@function
 1718              	_disable_timer5:
 1719              	.LFB32:
 623:lib/lib_pic33e/timer.c **** /**
 624:lib/lib_pic33e/timer.c ****  * @brief Disables timer 5
 625:lib/lib_pic33e/timer.c ****  */
 626:lib/lib_pic33e/timer.c **** void disable_timer5(void){
 1720              	.loc 1 626 0
 1721              	.set ___PA___,1
 1722 00068e  00 00 FA 	lnk #0
 1723              	.LCFI32:
 627:lib/lib_pic33e/timer.c ****     T5CONbits.TON = 0;
 1724              	.loc 1 627 0
 1725 000690  01 E0 A9 	bclr.b _T5CONbits+1,#7
 628:lib/lib_pic33e/timer.c **** }
 1726              	.loc 1 628 0
 1727 000692  8E 07 78 	mov w14,w15
 1728 000694  4F 07 78 	mov [--w15],w14
 1729 000696  00 40 A9 	bclr CORCON,#2
 1730 000698  00 00 06 	return 
 1731              	.set ___PA___,0
 1732              	.LFE32:
 1733              	.size _disable_timer5,.-_disable_timer5
 1734              	.align 2
 1735              	.global _disable_timer6
 1736              	.type _disable_timer6,@function
 1737              	_disable_timer6:
 1738              	.LFB33:
 629:lib/lib_pic33e/timer.c **** /**
 630:lib/lib_pic33e/timer.c ****  * @brief Disables timer 6
 631:lib/lib_pic33e/timer.c ****  */
 632:lib/lib_pic33e/timer.c **** void disable_timer6(void){
 1739              	.loc 1 632 0
 1740              	.set ___PA___,1
 1741 00069a  00 00 FA 	lnk #0
 1742              	.LCFI33:
 633:lib/lib_pic33e/timer.c ****     T6CONbits.TON = 0;
 1743              	.loc 1 633 0
 1744 00069c  01 E0 A9 	bclr.b _T6CONbits+1,#7
 634:lib/lib_pic33e/timer.c **** }
 1745              	.loc 1 634 0
 1746 00069e  8E 07 78 	mov w14,w15
 1747 0006a0  4F 07 78 	mov [--w15],w14
 1748 0006a2  00 40 A9 	bclr CORCON,#2
 1749 0006a4  00 00 06 	return 
 1750              	.set ___PA___,0
 1751              	.LFE33:
 1752              	.size _disable_timer6,.-_disable_timer6
MPLAB XC16 ASSEMBLY Listing:   			page 43


 1753              	.align 2
 1754              	.global _disable_timer7
 1755              	.type _disable_timer7,@function
 1756              	_disable_timer7:
 1757              	.LFB34:
 635:lib/lib_pic33e/timer.c **** /**
 636:lib/lib_pic33e/timer.c ****  * @brief Disables timer 7
 637:lib/lib_pic33e/timer.c ****  */
 638:lib/lib_pic33e/timer.c **** void disable_timer7(void){
 1758              	.loc 1 638 0
 1759              	.set ___PA___,1
 1760 0006a6  00 00 FA 	lnk #0
 1761              	.LCFI34:
 639:lib/lib_pic33e/timer.c ****     T7CONbits.TON = 0;
 1762              	.loc 1 639 0
 1763 0006a8  01 E0 A9 	bclr.b _T7CONbits+1,#7
 640:lib/lib_pic33e/timer.c **** }
 1764              	.loc 1 640 0
 1765 0006aa  8E 07 78 	mov w14,w15
 1766 0006ac  4F 07 78 	mov [--w15],w14
 1767 0006ae  00 40 A9 	bclr CORCON,#2
 1768 0006b0  00 00 06 	return 
 1769              	.set ___PA___,0
 1770              	.LFE34:
 1771              	.size _disable_timer7,.-_disable_timer7
 1772              	.align 2
 1773              	.global _disable_timer8
 1774              	.type _disable_timer8,@function
 1775              	_disable_timer8:
 1776              	.LFB35:
 641:lib/lib_pic33e/timer.c **** /**
 642:lib/lib_pic33e/timer.c ****  * @brief Disables timer 8
 643:lib/lib_pic33e/timer.c ****  */
 644:lib/lib_pic33e/timer.c **** void disable_timer8(void){
 1777              	.loc 1 644 0
 1778              	.set ___PA___,1
 1779 0006b2  00 00 FA 	lnk #0
 1780              	.LCFI35:
 645:lib/lib_pic33e/timer.c ****     T8CONbits.TON = 0;
 1781              	.loc 1 645 0
 1782 0006b4  01 E0 A9 	bclr.b _T8CONbits+1,#7
 646:lib/lib_pic33e/timer.c **** }
 1783              	.loc 1 646 0
 1784 0006b6  8E 07 78 	mov w14,w15
 1785 0006b8  4F 07 78 	mov [--w15],w14
 1786 0006ba  00 40 A9 	bclr CORCON,#2
 1787 0006bc  00 00 06 	return 
 1788              	.set ___PA___,0
 1789              	.LFE35:
 1790              	.size _disable_timer8,.-_disable_timer8
 1791              	.align 2
 1792              	.global _disable_timer9
 1793              	.type _disable_timer9,@function
 1794              	_disable_timer9:
 1795              	.LFB36:
 647:lib/lib_pic33e/timer.c **** /**
 648:lib/lib_pic33e/timer.c ****  * @brief Disables timer 9
MPLAB XC16 ASSEMBLY Listing:   			page 44


 649:lib/lib_pic33e/timer.c ****  */
 650:lib/lib_pic33e/timer.c **** void disable_timer9(void){
 1796              	.loc 1 650 0
 1797              	.set ___PA___,1
 1798 0006be  00 00 FA 	lnk #0
 1799              	.LCFI36:
 651:lib/lib_pic33e/timer.c ****     T9CONbits.TON = 0;
 1800              	.loc 1 651 0
 1801 0006c0  01 E0 A9 	bclr.b _T9CONbits+1,#7
 652:lib/lib_pic33e/timer.c **** }
 1802              	.loc 1 652 0
 1803 0006c2  8E 07 78 	mov w14,w15
 1804 0006c4  4F 07 78 	mov [--w15],w14
 1805 0006c6  00 40 A9 	bclr CORCON,#2
 1806 0006c8  00 00 06 	return 
 1807              	.set ___PA___,0
 1808              	.LFE36:
 1809              	.size _disable_timer9,.-_disable_timer9
 1810              	.align 2
 1811              	.global _reset_timer1
 1812              	.type _reset_timer1,@function
 1813              	_reset_timer1:
 1814              	.LFB37:
 653:lib/lib_pic33e/timer.c **** 
 654:lib/lib_pic33e/timer.c **** /**
 655:lib/lib_pic33e/timer.c ****  * @brief Resets timer 1
 656:lib/lib_pic33e/timer.c ****  */
 657:lib/lib_pic33e/timer.c **** void reset_timer1(void){
 1815              	.loc 1 657 0
 1816              	.set ___PA___,1
 1817 0006ca  00 00 FA 	lnk #0
 1818              	.LCFI37:
 658:lib/lib_pic33e/timer.c ****     TMR1 = 0;
 1819              	.loc 1 658 0
 1820 0006cc  00 20 EF 	clr _TMR1
 659:lib/lib_pic33e/timer.c **** }
 1821              	.loc 1 659 0
 1822 0006ce  8E 07 78 	mov w14,w15
 1823 0006d0  4F 07 78 	mov [--w15],w14
 1824 0006d2  00 40 A9 	bclr CORCON,#2
 1825 0006d4  00 00 06 	return 
 1826              	.set ___PA___,0
 1827              	.LFE37:
 1828              	.size _reset_timer1,.-_reset_timer1
 1829              	.align 2
 1830              	.global _reset_timer2
 1831              	.type _reset_timer2,@function
 1832              	_reset_timer2:
 1833              	.LFB38:
 660:lib/lib_pic33e/timer.c **** /**
 661:lib/lib_pic33e/timer.c ****  * @brief Resets timer 2
 662:lib/lib_pic33e/timer.c ****  */
 663:lib/lib_pic33e/timer.c **** void reset_timer2(void){
 1834              	.loc 1 663 0
 1835              	.set ___PA___,1
 1836 0006d6  00 00 FA 	lnk #0
 1837              	.LCFI38:
MPLAB XC16 ASSEMBLY Listing:   			page 45


 664:lib/lib_pic33e/timer.c ****     TMR2 = 0;
 1838              	.loc 1 664 0
 1839 0006d8  00 20 EF 	clr _TMR2
 665:lib/lib_pic33e/timer.c **** }
 1840              	.loc 1 665 0
 1841 0006da  8E 07 78 	mov w14,w15
 1842 0006dc  4F 07 78 	mov [--w15],w14
 1843 0006de  00 40 A9 	bclr CORCON,#2
 1844 0006e0  00 00 06 	return 
 1845              	.set ___PA___,0
 1846              	.LFE38:
 1847              	.size _reset_timer2,.-_reset_timer2
 1848              	.align 2
 1849              	.global _reset_timer3
 1850              	.type _reset_timer3,@function
 1851              	_reset_timer3:
 1852              	.LFB39:
 666:lib/lib_pic33e/timer.c **** /**
 667:lib/lib_pic33e/timer.c ****  * @brief Resets timer 3
 668:lib/lib_pic33e/timer.c ****  */
 669:lib/lib_pic33e/timer.c **** void reset_timer3(void){
 1853              	.loc 1 669 0
 1854              	.set ___PA___,1
 1855 0006e2  00 00 FA 	lnk #0
 1856              	.LCFI39:
 670:lib/lib_pic33e/timer.c ****     TMR3 = 0;
 1857              	.loc 1 670 0
 1858 0006e4  00 20 EF 	clr _TMR3
 671:lib/lib_pic33e/timer.c **** }
 1859              	.loc 1 671 0
 1860 0006e6  8E 07 78 	mov w14,w15
 1861 0006e8  4F 07 78 	mov [--w15],w14
 1862 0006ea  00 40 A9 	bclr CORCON,#2
 1863 0006ec  00 00 06 	return 
 1864              	.set ___PA___,0
 1865              	.LFE39:
 1866              	.size _reset_timer3,.-_reset_timer3
 1867              	.align 2
 1868              	.global _reset_timer4
 1869              	.type _reset_timer4,@function
 1870              	_reset_timer4:
 1871              	.LFB40:
 672:lib/lib_pic33e/timer.c **** /**
 673:lib/lib_pic33e/timer.c ****  * @brief Resets timer 4
 674:lib/lib_pic33e/timer.c ****  */
 675:lib/lib_pic33e/timer.c **** void reset_timer4(void){
 1872              	.loc 1 675 0
 1873              	.set ___PA___,1
 1874 0006ee  00 00 FA 	lnk #0
 1875              	.LCFI40:
 676:lib/lib_pic33e/timer.c ****     TMR4 = 0;
 1876              	.loc 1 676 0
 1877 0006f0  00 20 EF 	clr _TMR4
 677:lib/lib_pic33e/timer.c **** }
 1878              	.loc 1 677 0
 1879 0006f2  8E 07 78 	mov w14,w15
 1880 0006f4  4F 07 78 	mov [--w15],w14
MPLAB XC16 ASSEMBLY Listing:   			page 46


 1881 0006f6  00 40 A9 	bclr CORCON,#2
 1882 0006f8  00 00 06 	return 
 1883              	.set ___PA___,0
 1884              	.LFE40:
 1885              	.size _reset_timer4,.-_reset_timer4
 1886              	.align 2
 1887              	.global _reset_timer5
 1888              	.type _reset_timer5,@function
 1889              	_reset_timer5:
 1890              	.LFB41:
 678:lib/lib_pic33e/timer.c **** /**
 679:lib/lib_pic33e/timer.c ****  * @brief Resets timer 5
 680:lib/lib_pic33e/timer.c ****  */
 681:lib/lib_pic33e/timer.c **** void reset_timer5(void){
 1891              	.loc 1 681 0
 1892              	.set ___PA___,1
 1893 0006fa  00 00 FA 	lnk #0
 1894              	.LCFI41:
 682:lib/lib_pic33e/timer.c ****     TMR5 = 0;
 1895              	.loc 1 682 0
 1896 0006fc  00 20 EF 	clr _TMR5
 683:lib/lib_pic33e/timer.c **** }
 1897              	.loc 1 683 0
 1898 0006fe  8E 07 78 	mov w14,w15
 1899 000700  4F 07 78 	mov [--w15],w14
 1900 000702  00 40 A9 	bclr CORCON,#2
 1901 000704  00 00 06 	return 
 1902              	.set ___PA___,0
 1903              	.LFE41:
 1904              	.size _reset_timer5,.-_reset_timer5
 1905              	.align 2
 1906              	.global _reset_timer6
 1907              	.type _reset_timer6,@function
 1908              	_reset_timer6:
 1909              	.LFB42:
 684:lib/lib_pic33e/timer.c **** /**
 685:lib/lib_pic33e/timer.c ****  * @brief Resets timer 6
 686:lib/lib_pic33e/timer.c ****  */
 687:lib/lib_pic33e/timer.c **** void reset_timer6(void){
 1910              	.loc 1 687 0
 1911              	.set ___PA___,1
 1912 000706  00 00 FA 	lnk #0
 1913              	.LCFI42:
 688:lib/lib_pic33e/timer.c ****     TMR6 = 0;
 1914              	.loc 1 688 0
 1915 000708  00 20 EF 	clr _TMR6
 689:lib/lib_pic33e/timer.c **** }
 1916              	.loc 1 689 0
 1917 00070a  8E 07 78 	mov w14,w15
 1918 00070c  4F 07 78 	mov [--w15],w14
 1919 00070e  00 40 A9 	bclr CORCON,#2
 1920 000710  00 00 06 	return 
 1921              	.set ___PA___,0
 1922              	.LFE42:
 1923              	.size _reset_timer6,.-_reset_timer6
 1924              	.align 2
 1925              	.global _reset_timer7
MPLAB XC16 ASSEMBLY Listing:   			page 47


 1926              	.type _reset_timer7,@function
 1927              	_reset_timer7:
 1928              	.LFB43:
 690:lib/lib_pic33e/timer.c **** /**
 691:lib/lib_pic33e/timer.c ****  * @brief Resets timer 7
 692:lib/lib_pic33e/timer.c ****  */
 693:lib/lib_pic33e/timer.c **** void reset_timer7(void){
 1929              	.loc 1 693 0
 1930              	.set ___PA___,1
 1931 000712  00 00 FA 	lnk #0
 1932              	.LCFI43:
 694:lib/lib_pic33e/timer.c ****     TMR7 = 0;
 1933              	.loc 1 694 0
 1934 000714  00 20 EF 	clr _TMR7
 695:lib/lib_pic33e/timer.c **** }
 1935              	.loc 1 695 0
 1936 000716  8E 07 78 	mov w14,w15
 1937 000718  4F 07 78 	mov [--w15],w14
 1938 00071a  00 40 A9 	bclr CORCON,#2
 1939 00071c  00 00 06 	return 
 1940              	.set ___PA___,0
 1941              	.LFE43:
 1942              	.size _reset_timer7,.-_reset_timer7
 1943              	.align 2
 1944              	.global _reset_timer8
 1945              	.type _reset_timer8,@function
 1946              	_reset_timer8:
 1947              	.LFB44:
 696:lib/lib_pic33e/timer.c **** /**
 697:lib/lib_pic33e/timer.c ****  * @brief Resets timer 8
 698:lib/lib_pic33e/timer.c ****  */
 699:lib/lib_pic33e/timer.c **** void reset_timer8(void){
 1948              	.loc 1 699 0
 1949              	.set ___PA___,1
 1950 00071e  00 00 FA 	lnk #0
 1951              	.LCFI44:
 700:lib/lib_pic33e/timer.c ****     TMR8 = 0;
 1952              	.loc 1 700 0
 1953 000720  00 20 EF 	clr _TMR8
 701:lib/lib_pic33e/timer.c **** }
 1954              	.loc 1 701 0
 1955 000722  8E 07 78 	mov w14,w15
 1956 000724  4F 07 78 	mov [--w15],w14
 1957 000726  00 40 A9 	bclr CORCON,#2
 1958 000728  00 00 06 	return 
 1959              	.set ___PA___,0
 1960              	.LFE44:
 1961              	.size _reset_timer8,.-_reset_timer8
 1962              	.align 2
 1963              	.global _reset_timer9
 1964              	.type _reset_timer9,@function
 1965              	_reset_timer9:
 1966              	.LFB45:
 702:lib/lib_pic33e/timer.c **** /**
 703:lib/lib_pic33e/timer.c ****  * @brief Resets timer 9
 704:lib/lib_pic33e/timer.c ****  */
 705:lib/lib_pic33e/timer.c **** void reset_timer9(void){
MPLAB XC16 ASSEMBLY Listing:   			page 48


 1967              	.loc 1 705 0
 1968              	.set ___PA___,1
 1969 00072a  00 00 FA 	lnk #0
 1970              	.LCFI45:
 706:lib/lib_pic33e/timer.c ****     TMR9 = 0;
 1971              	.loc 1 706 0
 1972 00072c  00 20 EF 	clr _TMR9
 707:lib/lib_pic33e/timer.c **** }
 1973              	.loc 1 707 0
 1974 00072e  8E 07 78 	mov w14,w15
 1975 000730  4F 07 78 	mov [--w15],w14
 1976 000732  00 40 A9 	bclr CORCON,#2
 1977 000734  00 00 06 	return 
 1978              	.set ___PA___,0
 1979              	.LFE45:
 1980              	.size _reset_timer9,.-_reset_timer9
 1981              	.section .isr.text,code,keep
 1982              	.align 2
 1983              	.global __T1Interrupt
 1984              	.type __T1Interrupt,@function
 1985              	__T1Interrupt:
 1986              	.section .isr.text,code,keep
 1987              	.LFB46:
 1988              	.section .isr.text,code,keep
 708:lib/lib_pic33e/timer.c **** 
 709:lib/lib_pic33e/timer.c **** /**********************************************************************
 710:lib/lib_pic33e/timer.c ****  * Assign Timer 1 interruption
 711:lib/lib_pic33e/timer.c ****  **********************************************************************/
 712:lib/lib_pic33e/timer.c **** void __attribute__((interrupt, auto_psv, shadow)) _T1Interrupt (void){
 1989              	.loc 1 712 0
 1990              	.set ___PA___,1
 1991 000000  00 A0 FE 	push.s 
 1992 000002  00 00 F8 	push _RCOUNT
 1993              	.LCFI46:
 1994 000004  84 9F BE 	mov.d w4,[w15++]
 1995              	.LCFI47:
 1996 000006  86 9F BE 	mov.d w6,[w15++]
 1997              	.LCFI48:
 1998 000008  00 00 F8 	push _DSRPAG
 1999              	.LCFI49:
 2000 00000a  00 00 F8 	push _DSWPAG
 2001              	.LCFI50:
 2002 00000c  14 00 20 	mov #1,w4
 2003 00000e  04 00 88 	mov w4,_DSWPAG
 2004 000010  04 00 20 	mov #__const_psvpage,w4
 2005 000012  04 00 88 	mov w4,_DSRPAG
 2006 000014  00 00 00 	nop 
 2007 000016  00 00 FA 	lnk #0
 2008              	.LCFI51:
 2009              	.section .isr.text,code,keep
 713:lib/lib_pic33e/timer.c **** 	fun_ptr_timer1();
 2010              	.loc 1 713 0
 2011 000018  00 00 80 	mov _fun_ptr_timer1,w0
 2012 00001a  00 00 01 	call w0
 2013              	.section .isr.text,code,keep
 714:lib/lib_pic33e/timer.c **** 	IFS0bits.T1IF = 0;			/* clear interrupt flag                  */
 2014              	.loc 1 714 0
MPLAB XC16 ASSEMBLY Listing:   			page 49


 2015 00001c  00 60 A9 	bclr.b _IFS0bits,#3
 2016              	.section .isr.text,code,keep
 715:lib/lib_pic33e/timer.c **** 	return;
 716:lib/lib_pic33e/timer.c **** }
 2017              	.loc 1 716 0
 2018 00001e  8E 07 78 	mov w14,w15
 2019 000020  4F 07 78 	mov [--w15],w14
 2020 000022  00 40 A9 	bclr CORCON,#2
 2021 000024  00 00 F9 	pop _DSWPAG
 2022 000026  00 00 F9 	pop _DSRPAG
 2023 000028  80 1F 78 	mov w0,[w15++]
 2024 00002a  00 0E 20 	mov #224,w0
 2025 00002c  00 20 B7 	ior _SR
 2026 00002e  4F 00 78 	mov [--w15],w0
 2027 000030  4F 03 BE 	mov.d [--w15],w6
 2028 000032  4F 02 BE 	mov.d [--w15],w4
 2029 000034  00 00 F9 	pop _RCOUNT
 2030 000036  00 80 FE 	pop.s 
 2031 000038  00 40 06 	retfie 
 2032              	.set ___PA___,0
 2033              	.LFE46:
 2034              	.size __T1Interrupt,.-__T1Interrupt
 2035              	.section .isr.text,code,keep
 2036              	.align 2
 2037              	.global __T2Interrupt
 2038              	.type __T2Interrupt,@function
 2039              	__T2Interrupt:
 2040              	.section .isr.text,code,keep
 2041              	.LFB47:
 2042              	.section .isr.text,code,keep
 717:lib/lib_pic33e/timer.c **** 
 718:lib/lib_pic33e/timer.c **** /**********************************************************************
 719:lib/lib_pic33e/timer.c ****  * Assign Timer 2 interruption
 720:lib/lib_pic33e/timer.c ****  **********************************************************************/
 721:lib/lib_pic33e/timer.c **** void __attribute__((interrupt, auto_psv, shadow)) _T2Interrupt (void){
 2043              	.loc 1 721 0
 2044              	.set ___PA___,1
 2045 00003a  00 A0 FE 	push.s 
 2046 00003c  00 00 F8 	push _RCOUNT
 2047              	.LCFI52:
 2048 00003e  84 9F BE 	mov.d w4,[w15++]
 2049              	.LCFI53:
 2050 000040  86 9F BE 	mov.d w6,[w15++]
 2051              	.LCFI54:
 2052 000042  00 00 F8 	push _DSRPAG
 2053              	.LCFI55:
 2054 000044  00 00 F8 	push _DSWPAG
 2055              	.LCFI56:
 2056 000046  14 00 20 	mov #1,w4
 2057 000048  04 00 88 	mov w4,_DSWPAG
 2058 00004a  04 00 20 	mov #__const_psvpage,w4
 2059 00004c  04 00 88 	mov w4,_DSRPAG
 2060 00004e  00 00 00 	nop 
 2061 000050  00 00 FA 	lnk #0
 2062              	.LCFI57:
 2063              	.section .isr.text,code,keep
 722:lib/lib_pic33e/timer.c **** 	fun_ptr_timer2();
MPLAB XC16 ASSEMBLY Listing:   			page 50


 2064              	.loc 1 722 0
 2065 000052  00 00 80 	mov _fun_ptr_timer2,w0
 2066 000054  00 00 01 	call w0
 2067              	.section .isr.text,code,keep
 723:lib/lib_pic33e/timer.c **** 	IFS0bits.T2IF = 0;			/* clear interrupt flag                  */
 2068              	.loc 1 723 0
 2069 000056  00 E0 A9 	bclr.b _IFS0bits,#7
 2070              	.section .isr.text,code,keep
 724:lib/lib_pic33e/timer.c **** 
 725:lib/lib_pic33e/timer.c **** 	return;
 726:lib/lib_pic33e/timer.c **** }
 2071              	.loc 1 726 0
 2072 000058  8E 07 78 	mov w14,w15
 2073 00005a  4F 07 78 	mov [--w15],w14
 2074 00005c  00 40 A9 	bclr CORCON,#2
 2075 00005e  00 00 F9 	pop _DSWPAG
 2076 000060  00 00 F9 	pop _DSRPAG
 2077 000062  80 1F 78 	mov w0,[w15++]
 2078 000064  00 0E 20 	mov #224,w0
 2079 000066  00 20 B7 	ior _SR
 2080 000068  4F 00 78 	mov [--w15],w0
 2081 00006a  4F 03 BE 	mov.d [--w15],w6
 2082 00006c  4F 02 BE 	mov.d [--w15],w4
 2083 00006e  00 00 F9 	pop _RCOUNT
 2084 000070  00 80 FE 	pop.s 
 2085 000072  00 40 06 	retfie 
 2086              	.set ___PA___,0
 2087              	.LFE47:
 2088              	.size __T2Interrupt,.-__T2Interrupt
 2089              	.section .isr.text,code,keep
 2090              	.align 2
 2091              	.global __T3Interrupt
 2092              	.type __T3Interrupt,@function
 2093              	__T3Interrupt:
 2094              	.section .isr.text,code,keep
 2095              	.LFB48:
 2096              	.section .isr.text,code,keep
 727:lib/lib_pic33e/timer.c **** 
 728:lib/lib_pic33e/timer.c **** /**********************************************************************
 729:lib/lib_pic33e/timer.c ****  * Assign Timer 3 interruption
 730:lib/lib_pic33e/timer.c ****  **********************************************************************/
 731:lib/lib_pic33e/timer.c **** void __attribute__((interrupt, auto_psv, shadow)) _T3Interrupt (void){
 2097              	.loc 1 731 0
 2098              	.set ___PA___,1
 2099 000074  00 A0 FE 	push.s 
 2100 000076  00 00 F8 	push _RCOUNT
 2101              	.LCFI58:
 2102 000078  84 9F BE 	mov.d w4,[w15++]
 2103              	.LCFI59:
 2104 00007a  86 9F BE 	mov.d w6,[w15++]
 2105              	.LCFI60:
 2106 00007c  00 00 F8 	push _DSRPAG
 2107              	.LCFI61:
 2108 00007e  00 00 F8 	push _DSWPAG
 2109              	.LCFI62:
 2110 000080  14 00 20 	mov #1,w4
 2111 000082  04 00 88 	mov w4,_DSWPAG
MPLAB XC16 ASSEMBLY Listing:   			page 51


 2112 000084  04 00 20 	mov #__const_psvpage,w4
 2113 000086  04 00 88 	mov w4,_DSRPAG
 2114 000088  00 00 00 	nop 
 2115 00008a  00 00 FA 	lnk #0
 2116              	.LCFI63:
 2117              	.section .isr.text,code,keep
 732:lib/lib_pic33e/timer.c **** 	fun_ptr_timer3();
 2118              	.loc 1 732 0
 2119 00008c  00 00 80 	mov _fun_ptr_timer3,w0
 2120 00008e  00 00 01 	call w0
 2121              	.section .isr.text,code,keep
 733:lib/lib_pic33e/timer.c **** 	IFS0bits.T3IF = 0;			/* clear interrupt flag                  */
 2122              	.loc 1 733 0
 2123 000090  01 00 A9 	bclr.b _IFS0bits+1,#0
 2124              	.section .isr.text,code,keep
 734:lib/lib_pic33e/timer.c **** 
 735:lib/lib_pic33e/timer.c **** 	return;
 736:lib/lib_pic33e/timer.c **** }
 2125              	.loc 1 736 0
 2126 000092  8E 07 78 	mov w14,w15
 2127 000094  4F 07 78 	mov [--w15],w14
 2128 000096  00 40 A9 	bclr CORCON,#2
 2129 000098  00 00 F9 	pop _DSWPAG
 2130 00009a  00 00 F9 	pop _DSRPAG
 2131 00009c  80 1F 78 	mov w0,[w15++]
 2132 00009e  00 0E 20 	mov #224,w0
 2133 0000a0  00 20 B7 	ior _SR
 2134 0000a2  4F 00 78 	mov [--w15],w0
 2135 0000a4  4F 03 BE 	mov.d [--w15],w6
 2136 0000a6  4F 02 BE 	mov.d [--w15],w4
 2137 0000a8  00 00 F9 	pop _RCOUNT
 2138 0000aa  00 80 FE 	pop.s 
 2139 0000ac  00 40 06 	retfie 
 2140              	.set ___PA___,0
 2141              	.LFE48:
 2142              	.size __T3Interrupt,.-__T3Interrupt
 2143              	.section .isr.text,code,keep
 2144              	.align 2
 2145              	.global __T4Interrupt
 2146              	.type __T4Interrupt,@function
 2147              	__T4Interrupt:
 2148              	.section .isr.text,code,keep
 2149              	.LFB49:
 2150              	.section .isr.text,code,keep
 737:lib/lib_pic33e/timer.c **** /**********************************************************************
 738:lib/lib_pic33e/timer.c ****  * Assign Timer 4 interruption
 739:lib/lib_pic33e/timer.c ****  **********************************************************************/
 740:lib/lib_pic33e/timer.c **** void __attribute__((interrupt, auto_psv, shadow)) _T4Interrupt (void){
 2151              	.loc 1 740 0
 2152              	.set ___PA___,1
 2153 0000ae  00 A0 FE 	push.s 
 2154 0000b0  00 00 F8 	push _RCOUNT
 2155              	.LCFI64:
 2156 0000b2  84 9F BE 	mov.d w4,[w15++]
 2157              	.LCFI65:
 2158 0000b4  86 9F BE 	mov.d w6,[w15++]
 2159              	.LCFI66:
MPLAB XC16 ASSEMBLY Listing:   			page 52


 2160 0000b6  00 00 F8 	push _DSRPAG
 2161              	.LCFI67:
 2162 0000b8  00 00 F8 	push _DSWPAG
 2163              	.LCFI68:
 2164 0000ba  14 00 20 	mov #1,w4
 2165 0000bc  04 00 88 	mov w4,_DSWPAG
 2166 0000be  04 00 20 	mov #__const_psvpage,w4
 2167 0000c0  04 00 88 	mov w4,_DSRPAG
 2168 0000c2  00 00 00 	nop 
 2169 0000c4  00 00 FA 	lnk #0
 2170              	.LCFI69:
 2171              	.section .isr.text,code,keep
 741:lib/lib_pic33e/timer.c **** 	fun_ptr_timer4();
 2172              	.loc 1 741 0
 2173 0000c6  00 00 80 	mov _fun_ptr_timer4,w0
 2174 0000c8  00 00 01 	call w0
 2175              	.section .isr.text,code,keep
 742:lib/lib_pic33e/timer.c **** 	IFS1bits.T4IF = 0;			/* clear interrupt flag                  */
 2176              	.loc 1 742 0
 2177 0000ca  01 60 A9 	bclr.b _IFS1bits+1,#3
 2178              	.section .isr.text,code,keep
 743:lib/lib_pic33e/timer.c **** 
 744:lib/lib_pic33e/timer.c **** 	return;
 745:lib/lib_pic33e/timer.c **** }
 2179              	.loc 1 745 0
 2180 0000cc  8E 07 78 	mov w14,w15
 2181 0000ce  4F 07 78 	mov [--w15],w14
 2182 0000d0  00 40 A9 	bclr CORCON,#2
 2183 0000d2  00 00 F9 	pop _DSWPAG
 2184 0000d4  00 00 F9 	pop _DSRPAG
 2185 0000d6  80 1F 78 	mov w0,[w15++]
 2186 0000d8  00 0E 20 	mov #224,w0
 2187 0000da  00 20 B7 	ior _SR
 2188 0000dc  4F 00 78 	mov [--w15],w0
 2189 0000de  4F 03 BE 	mov.d [--w15],w6
 2190 0000e0  4F 02 BE 	mov.d [--w15],w4
 2191 0000e2  00 00 F9 	pop _RCOUNT
 2192 0000e4  00 80 FE 	pop.s 
 2193 0000e6  00 40 06 	retfie 
 2194              	.set ___PA___,0
 2195              	.LFE49:
 2196              	.size __T4Interrupt,.-__T4Interrupt
 2197              	.section .isr.text,code,keep
 2198              	.align 2
 2199              	.global __T5Interrupt
 2200              	.type __T5Interrupt,@function
 2201              	__T5Interrupt:
 2202              	.section .isr.text,code,keep
 2203              	.LFB50:
 2204              	.section .isr.text,code,keep
 746:lib/lib_pic33e/timer.c **** /**********************************************************************
 747:lib/lib_pic33e/timer.c ****  * Assign Timer 5 interruption
 748:lib/lib_pic33e/timer.c ****  **********************************************************************/
 749:lib/lib_pic33e/timer.c **** void __attribute__((interrupt, auto_psv, shadow)) _T5Interrupt (void){
 2205              	.loc 1 749 0
 2206              	.set ___PA___,1
 2207 0000e8  00 A0 FE 	push.s 
MPLAB XC16 ASSEMBLY Listing:   			page 53


 2208 0000ea  00 00 F8 	push _RCOUNT
 2209              	.LCFI70:
 2210 0000ec  84 9F BE 	mov.d w4,[w15++]
 2211              	.LCFI71:
 2212 0000ee  86 9F BE 	mov.d w6,[w15++]
 2213              	.LCFI72:
 2214 0000f0  00 00 F8 	push _DSRPAG
 2215              	.LCFI73:
 2216 0000f2  00 00 F8 	push _DSWPAG
 2217              	.LCFI74:
 2218 0000f4  14 00 20 	mov #1,w4
 2219 0000f6  04 00 88 	mov w4,_DSWPAG
 2220 0000f8  04 00 20 	mov #__const_psvpage,w4
 2221 0000fa  04 00 88 	mov w4,_DSRPAG
 2222 0000fc  00 00 00 	nop 
 2223 0000fe  00 00 FA 	lnk #0
 2224              	.LCFI75:
 2225              	.section .isr.text,code,keep
 750:lib/lib_pic33e/timer.c **** 	fun_ptr_timer5();
 2226              	.loc 1 750 0
 2227 000100  00 00 80 	mov _fun_ptr_timer5,w0
 2228 000102  00 00 01 	call w0
 2229              	.section .isr.text,code,keep
 751:lib/lib_pic33e/timer.c **** 	IFS1bits.T5IF = 0;			/* clear interrupt flag                  */
 2230              	.loc 1 751 0
 2231 000104  01 80 A9 	bclr.b _IFS1bits+1,#4
 2232              	.section .isr.text,code,keep
 752:lib/lib_pic33e/timer.c **** 
 753:lib/lib_pic33e/timer.c **** 	return;
 754:lib/lib_pic33e/timer.c **** }
 2233              	.loc 1 754 0
 2234 000106  8E 07 78 	mov w14,w15
 2235 000108  4F 07 78 	mov [--w15],w14
 2236 00010a  00 40 A9 	bclr CORCON,#2
 2237 00010c  00 00 F9 	pop _DSWPAG
 2238 00010e  00 00 F9 	pop _DSRPAG
 2239 000110  80 1F 78 	mov w0,[w15++]
 2240 000112  00 0E 20 	mov #224,w0
 2241 000114  00 20 B7 	ior _SR
 2242 000116  4F 00 78 	mov [--w15],w0
 2243 000118  4F 03 BE 	mov.d [--w15],w6
 2244 00011a  4F 02 BE 	mov.d [--w15],w4
 2245 00011c  00 00 F9 	pop _RCOUNT
 2246 00011e  00 80 FE 	pop.s 
 2247 000120  00 40 06 	retfie 
 2248              	.set ___PA___,0
 2249              	.LFE50:
 2250              	.size __T5Interrupt,.-__T5Interrupt
 2251              	.section .isr.text,code,keep
 2252              	.align 2
 2253              	.global __T6Interrupt
 2254              	.type __T6Interrupt,@function
 2255              	__T6Interrupt:
 2256              	.section .isr.text,code,keep
 2257              	.LFB51:
 2258              	.section .isr.text,code,keep
 755:lib/lib_pic33e/timer.c **** /**********************************************************************
MPLAB XC16 ASSEMBLY Listing:   			page 54


 756:lib/lib_pic33e/timer.c ****  * Assign Timer 6 interruption
 757:lib/lib_pic33e/timer.c ****  **********************************************************************/
 758:lib/lib_pic33e/timer.c **** void __attribute__((interrupt, auto_psv, shadow)) _T6Interrupt (void){
 2259              	.loc 1 758 0
 2260              	.set ___PA___,1
 2261 000122  00 A0 FE 	push.s 
 2262 000124  00 00 F8 	push _RCOUNT
 2263              	.LCFI76:
 2264 000126  84 9F BE 	mov.d w4,[w15++]
 2265              	.LCFI77:
 2266 000128  86 9F BE 	mov.d w6,[w15++]
 2267              	.LCFI78:
 2268 00012a  00 00 F8 	push _DSRPAG
 2269              	.LCFI79:
 2270 00012c  00 00 F8 	push _DSWPAG
 2271              	.LCFI80:
 2272 00012e  14 00 20 	mov #1,w4
 2273 000130  04 00 88 	mov w4,_DSWPAG
 2274 000132  04 00 20 	mov #__const_psvpage,w4
 2275 000134  04 00 88 	mov w4,_DSRPAG
 2276 000136  00 00 00 	nop 
 2277 000138  00 00 FA 	lnk #0
 2278              	.LCFI81:
 2279              	.section .isr.text,code,keep
 759:lib/lib_pic33e/timer.c **** 	fun_ptr_timer6();
 2280              	.loc 1 759 0
 2281 00013a  00 00 80 	mov _fun_ptr_timer6,w0
 2282 00013c  00 00 01 	call w0
 2283              	.section .isr.text,code,keep
 760:lib/lib_pic33e/timer.c **** 	IFS2bits.T6IF = 0;			/* clear interrupt flag                  */
 2284              	.loc 1 760 0
 2285 00013e  01 E0 A9 	bclr.b _IFS2bits+1,#7
 2286              	.section .isr.text,code,keep
 761:lib/lib_pic33e/timer.c **** 
 762:lib/lib_pic33e/timer.c **** 	return;
 763:lib/lib_pic33e/timer.c **** }
 2287              	.loc 1 763 0
 2288 000140  8E 07 78 	mov w14,w15
 2289 000142  4F 07 78 	mov [--w15],w14
 2290 000144  00 40 A9 	bclr CORCON,#2
 2291 000146  00 00 F9 	pop _DSWPAG
 2292 000148  00 00 F9 	pop _DSRPAG
 2293 00014a  80 1F 78 	mov w0,[w15++]
 2294 00014c  00 0E 20 	mov #224,w0
 2295 00014e  00 20 B7 	ior _SR
 2296 000150  4F 00 78 	mov [--w15],w0
 2297 000152  4F 03 BE 	mov.d [--w15],w6
 2298 000154  4F 02 BE 	mov.d [--w15],w4
 2299 000156  00 00 F9 	pop _RCOUNT
 2300 000158  00 80 FE 	pop.s 
 2301 00015a  00 40 06 	retfie 
 2302              	.set ___PA___,0
 2303              	.LFE51:
 2304              	.size __T6Interrupt,.-__T6Interrupt
 2305              	.section .isr.text,code,keep
 2306              	.align 2
 2307              	.global __T7Interrupt
MPLAB XC16 ASSEMBLY Listing:   			page 55


 2308              	.type __T7Interrupt,@function
 2309              	__T7Interrupt:
 2310              	.section .isr.text,code,keep
 2311              	.LFB52:
 2312              	.section .isr.text,code,keep
 764:lib/lib_pic33e/timer.c **** /**********************************************************************
 765:lib/lib_pic33e/timer.c ****  * Assign Timer 7 interruption
 766:lib/lib_pic33e/timer.c ****  **********************************************************************/
 767:lib/lib_pic33e/timer.c **** void __attribute__((interrupt, auto_psv, shadow)) _T7Interrupt (void){
 2313              	.loc 1 767 0
 2314              	.set ___PA___,1
 2315 00015c  00 A0 FE 	push.s 
 2316 00015e  00 00 F8 	push _RCOUNT
 2317              	.LCFI82:
 2318 000160  84 9F BE 	mov.d w4,[w15++]
 2319              	.LCFI83:
 2320 000162  86 9F BE 	mov.d w6,[w15++]
 2321              	.LCFI84:
 2322 000164  00 00 F8 	push _DSRPAG
 2323              	.LCFI85:
 2324 000166  00 00 F8 	push _DSWPAG
 2325              	.LCFI86:
 2326 000168  14 00 20 	mov #1,w4
 2327 00016a  04 00 88 	mov w4,_DSWPAG
 2328 00016c  04 00 20 	mov #__const_psvpage,w4
 2329 00016e  04 00 88 	mov w4,_DSRPAG
 2330 000170  00 00 00 	nop 
 2331 000172  00 00 FA 	lnk #0
 2332              	.LCFI87:
 2333              	.section .isr.text,code,keep
 768:lib/lib_pic33e/timer.c **** 	fun_ptr_timer7();
 2334              	.loc 1 768 0
 2335 000174  00 00 80 	mov _fun_ptr_timer7,w0
 2336 000176  00 00 01 	call w0
 2337              	.section .isr.text,code,keep
 769:lib/lib_pic33e/timer.c **** 	IFS3bits.T7IF = 0;			/* clear interrupt flag                  */
 2338              	.loc 1 769 0
 2339 000178  00 00 A9 	bclr.b _IFS3bits,#0
 2340              	.section .isr.text,code,keep
 770:lib/lib_pic33e/timer.c **** 
 771:lib/lib_pic33e/timer.c **** 	return;
 772:lib/lib_pic33e/timer.c **** }
 2341              	.loc 1 772 0
 2342 00017a  8E 07 78 	mov w14,w15
 2343 00017c  4F 07 78 	mov [--w15],w14
 2344 00017e  00 40 A9 	bclr CORCON,#2
 2345 000180  00 00 F9 	pop _DSWPAG
 2346 000182  00 00 F9 	pop _DSRPAG
 2347 000184  80 1F 78 	mov w0,[w15++]
 2348 000186  00 0E 20 	mov #224,w0
 2349 000188  00 20 B7 	ior _SR
 2350 00018a  4F 00 78 	mov [--w15],w0
 2351 00018c  4F 03 BE 	mov.d [--w15],w6
 2352 00018e  4F 02 BE 	mov.d [--w15],w4
 2353 000190  00 00 F9 	pop _RCOUNT
 2354 000192  00 80 FE 	pop.s 
 2355 000194  00 40 06 	retfie 
MPLAB XC16 ASSEMBLY Listing:   			page 56


 2356              	.set ___PA___,0
 2357              	.LFE52:
 2358              	.size __T7Interrupt,.-__T7Interrupt
 2359              	.section .isr.text,code,keep
 2360              	.align 2
 2361              	.global __T8Interrupt
 2362              	.type __T8Interrupt,@function
 2363              	__T8Interrupt:
 2364              	.section .isr.text,code,keep
 2365              	.LFB53:
 2366              	.section .isr.text,code,keep
 773:lib/lib_pic33e/timer.c **** /**********************************************************************
 774:lib/lib_pic33e/timer.c ****  * Assign Timer 8 interruption
 775:lib/lib_pic33e/timer.c ****  **********************************************************************/
 776:lib/lib_pic33e/timer.c **** void __attribute__((interrupt, auto_psv, shadow)) _T8Interrupt (void){
 2367              	.loc 1 776 0
 2368              	.set ___PA___,1
 2369 000196  00 A0 FE 	push.s 
 2370 000198  00 00 F8 	push _RCOUNT
 2371              	.LCFI88:
 2372 00019a  84 9F BE 	mov.d w4,[w15++]
 2373              	.LCFI89:
 2374 00019c  86 9F BE 	mov.d w6,[w15++]
 2375              	.LCFI90:
 2376 00019e  00 00 F8 	push _DSRPAG
 2377              	.LCFI91:
 2378 0001a0  00 00 F8 	push _DSWPAG
 2379              	.LCFI92:
 2380 0001a2  14 00 20 	mov #1,w4
 2381 0001a4  04 00 88 	mov w4,_DSWPAG
 2382 0001a6  04 00 20 	mov #__const_psvpage,w4
 2383 0001a8  04 00 88 	mov w4,_DSRPAG
 2384 0001aa  00 00 00 	nop 
 2385 0001ac  00 00 FA 	lnk #0
 2386              	.LCFI93:
 2387              	.section .isr.text,code,keep
 777:lib/lib_pic33e/timer.c **** 	fun_ptr_timer8();
 2388              	.loc 1 777 0
 2389 0001ae  00 00 80 	mov _fun_ptr_timer8,w0
 2390 0001b0  00 00 01 	call w0
 2391              	.section .isr.text,code,keep
 778:lib/lib_pic33e/timer.c **** 	IFS3bits.T8IF = 0;			/* clear interrupt flag                  */
 2392              	.loc 1 778 0
 2393 0001b2  00 60 A9 	bclr.b _IFS3bits,#3
 2394              	.section .isr.text,code,keep
 779:lib/lib_pic33e/timer.c **** 
 780:lib/lib_pic33e/timer.c **** 	return;
 781:lib/lib_pic33e/timer.c **** }
 2395              	.loc 1 781 0
 2396 0001b4  8E 07 78 	mov w14,w15
 2397 0001b6  4F 07 78 	mov [--w15],w14
 2398 0001b8  00 40 A9 	bclr CORCON,#2
 2399 0001ba  00 00 F9 	pop _DSWPAG
 2400 0001bc  00 00 F9 	pop _DSRPAG
 2401 0001be  80 1F 78 	mov w0,[w15++]
 2402 0001c0  00 0E 20 	mov #224,w0
 2403 0001c2  00 20 B7 	ior _SR
MPLAB XC16 ASSEMBLY Listing:   			page 57


 2404 0001c4  4F 00 78 	mov [--w15],w0
 2405 0001c6  4F 03 BE 	mov.d [--w15],w6
 2406 0001c8  4F 02 BE 	mov.d [--w15],w4
 2407 0001ca  00 00 F9 	pop _RCOUNT
 2408 0001cc  00 80 FE 	pop.s 
 2409 0001ce  00 40 06 	retfie 
 2410              	.set ___PA___,0
 2411              	.LFE53:
 2412              	.size __T8Interrupt,.-__T8Interrupt
 2413              	.section .isr.text,code,keep
 2414              	.align 2
 2415              	.global __T9Interrupt
 2416              	.type __T9Interrupt,@function
 2417              	__T9Interrupt:
 2418              	.section .isr.text,code,keep
 2419              	.LFB54:
 2420              	.section .isr.text,code,keep
 782:lib/lib_pic33e/timer.c **** /**********************************************************************
 783:lib/lib_pic33e/timer.c ****  * Assign Timer 9 interruption
 784:lib/lib_pic33e/timer.c ****  **********************************************************************/
 785:lib/lib_pic33e/timer.c **** void __attribute__((interrupt, auto_psv, shadow)) _T9Interrupt (void){
 2421              	.loc 1 785 0
 2422              	.set ___PA___,1
 2423 0001d0  00 A0 FE 	push.s 
 2424 0001d2  00 00 F8 	push _RCOUNT
 2425              	.LCFI94:
 2426 0001d4  84 9F BE 	mov.d w4,[w15++]
 2427              	.LCFI95:
 2428 0001d6  86 9F BE 	mov.d w6,[w15++]
 2429              	.LCFI96:
 2430 0001d8  00 00 F8 	push _DSRPAG
 2431              	.LCFI97:
 2432 0001da  00 00 F8 	push _DSWPAG
 2433              	.LCFI98:
 2434 0001dc  14 00 20 	mov #1,w4
 2435 0001de  04 00 88 	mov w4,_DSWPAG
 2436 0001e0  04 00 20 	mov #__const_psvpage,w4
 2437 0001e2  04 00 88 	mov w4,_DSRPAG
 2438 0001e4  00 00 00 	nop 
 2439 0001e6  00 00 FA 	lnk #0
 2440              	.LCFI99:
 2441              	.section .isr.text,code,keep
 786:lib/lib_pic33e/timer.c **** 	fun_ptr_timer9();
 2442              	.loc 1 786 0
 2443 0001e8  00 00 80 	mov _fun_ptr_timer9,w0
 2444 0001ea  00 00 01 	call w0
 2445              	.section .isr.text,code,keep
 787:lib/lib_pic33e/timer.c **** 	IFS3bits.T9IF = 0;			/* clear interrupt flag                  */
 2446              	.loc 1 787 0
 2447 0001ec  00 80 A9 	bclr.b _IFS3bits,#4
 2448              	.section .isr.text,code,keep
 788:lib/lib_pic33e/timer.c **** 
 789:lib/lib_pic33e/timer.c **** 	return;
 790:lib/lib_pic33e/timer.c **** }
 2449              	.loc 1 790 0
 2450 0001ee  8E 07 78 	mov w14,w15
 2451 0001f0  4F 07 78 	mov [--w15],w14
MPLAB XC16 ASSEMBLY Listing:   			page 58


 2452 0001f2  00 40 A9 	bclr CORCON,#2
 2453 0001f4  00 00 F9 	pop _DSWPAG
 2454 0001f6  00 00 F9 	pop _DSRPAG
 2455 0001f8  80 1F 78 	mov w0,[w15++]
 2456 0001fa  00 0E 20 	mov #224,w0
 2457 0001fc  00 20 B7 	ior _SR
 2458 0001fe  4F 00 78 	mov [--w15],w0
 2459 000200  4F 03 BE 	mov.d [--w15],w6
 2460 000202  4F 02 BE 	mov.d [--w15],w4
 2461 000204  00 00 F9 	pop _RCOUNT
 2462 000206  00 80 FE 	pop.s 
 2463 000208  00 40 06 	retfie 
 2464              	.set ___PA___,0
 2465              	.LFE54:
 2466              	.size __T9Interrupt,.-__T9Interrupt
 2467              	.section .debug_frame,info
 2468                 	.Lframe0:
 2469 0000 10 00 00 00 	.4byte .LECIE0-.LSCIE0
 2470                 	.LSCIE0:
 2471 0004 FF FF FF FF 	.4byte 0xffffffff
 2472 0008 01          	.byte 0x1
 2473 0009 00          	.byte 0
 2474 000a 01          	.uleb128 0x1
 2475 000b 02          	.sleb128 2
 2476 000c 25          	.byte 0x25
 2477 000d 12          	.byte 0x12
 2478 000e 0F          	.uleb128 0xf
 2479 000f 7E          	.sleb128 -2
 2480 0010 09          	.byte 0x9
 2481 0011 25          	.uleb128 0x25
 2482 0012 0F          	.uleb128 0xf
 2483 0013 00          	.align 4
 2484                 	.LECIE0:
 2485                 	.LSFDE0:
 2486 0014 18 00 00 00 	.4byte .LEFDE0-.LASFDE0
 2487                 	.LASFDE0:
 2488 0018 00 00 00 00 	.4byte .Lframe0
 2489 001c 00 00 00 00 	.4byte .LFB0
 2490 0020 0A 00 00 00 	.4byte .LFE0-.LFB0
 2491 0024 04          	.byte 0x4
 2492 0025 02 00 00 00 	.4byte .LCFI0-.LFB0
 2493 0029 13          	.byte 0x13
 2494 002a 7D          	.sleb128 -3
 2495 002b 0D          	.byte 0xd
 2496 002c 0E          	.uleb128 0xe
 2497 002d 8E          	.byte 0x8e
 2498 002e 02          	.uleb128 0x2
 2499 002f 00          	.align 4
 2500                 	.LEFDE0:
 2501                 	.LSFDE2:
 2502 0030 18 00 00 00 	.4byte .LEFDE2-.LASFDE2
 2503                 	.LASFDE2:
 2504 0034 00 00 00 00 	.4byte .Lframe0
 2505 0038 00 00 00 00 	.4byte .LFB1
 2506 003c 54 00 00 00 	.4byte .LFE1-.LFB1
 2507 0040 04          	.byte 0x4
 2508 0041 02 00 00 00 	.4byte .LCFI1-.LFB1
MPLAB XC16 ASSEMBLY Listing:   			page 59


 2509 0045 13          	.byte 0x13
 2510 0046 7D          	.sleb128 -3
 2511 0047 0D          	.byte 0xd
 2512 0048 0E          	.uleb128 0xe
 2513 0049 8E          	.byte 0x8e
 2514 004a 02          	.uleb128 0x2
 2515 004b 00          	.align 4
 2516                 	.LEFDE2:
 2517                 	.LSFDE4:
 2518 004c 18 00 00 00 	.4byte .LEFDE4-.LASFDE4
 2519                 	.LASFDE4:
 2520 0050 00 00 00 00 	.4byte .Lframe0
 2521 0054 00 00 00 00 	.4byte .LFB2
 2522 0058 54 00 00 00 	.4byte .LFE2-.LFB2
 2523 005c 04          	.byte 0x4
 2524 005d 02 00 00 00 	.4byte .LCFI2-.LFB2
 2525 0061 13          	.byte 0x13
 2526 0062 7D          	.sleb128 -3
 2527 0063 0D          	.byte 0xd
 2528 0064 0E          	.uleb128 0xe
 2529 0065 8E          	.byte 0x8e
 2530 0066 02          	.uleb128 0x2
 2531 0067 00          	.align 4
 2532                 	.LEFDE4:
 2533                 	.LSFDE6:
 2534 0068 18 00 00 00 	.4byte .LEFDE6-.LASFDE6
 2535                 	.LASFDE6:
 2536 006c 00 00 00 00 	.4byte .Lframe0
 2537 0070 00 00 00 00 	.4byte .LFB3
 2538 0074 56 00 00 00 	.4byte .LFE3-.LFB3
 2539 0078 04          	.byte 0x4
 2540 0079 02 00 00 00 	.4byte .LCFI3-.LFB3
 2541 007d 13          	.byte 0x13
 2542 007e 7D          	.sleb128 -3
 2543 007f 0D          	.byte 0xd
 2544 0080 0E          	.uleb128 0xe
 2545 0081 8E          	.byte 0x8e
 2546 0082 02          	.uleb128 0x2
 2547 0083 00          	.align 4
 2548                 	.LEFDE6:
 2549                 	.LSFDE8:
 2550 0084 18 00 00 00 	.4byte .LEFDE8-.LASFDE8
 2551                 	.LASFDE8:
 2552 0088 00 00 00 00 	.4byte .Lframe0
 2553 008c 00 00 00 00 	.4byte .LFB4
 2554 0090 56 00 00 00 	.4byte .LFE4-.LFB4
 2555 0094 04          	.byte 0x4
 2556 0095 02 00 00 00 	.4byte .LCFI4-.LFB4
 2557 0099 13          	.byte 0x13
 2558 009a 7D          	.sleb128 -3
 2559 009b 0D          	.byte 0xd
 2560 009c 0E          	.uleb128 0xe
 2561 009d 8E          	.byte 0x8e
 2562 009e 02          	.uleb128 0x2
 2563 009f 00          	.align 4
 2564                 	.LEFDE8:
 2565                 	.LSFDE10:
MPLAB XC16 ASSEMBLY Listing:   			page 60


 2566 00a0 18 00 00 00 	.4byte .LEFDE10-.LASFDE10
 2567                 	.LASFDE10:
 2568 00a4 00 00 00 00 	.4byte .Lframe0
 2569 00a8 00 00 00 00 	.4byte .LFB5
 2570 00ac 52 00 00 00 	.4byte .LFE5-.LFB5
 2571 00b0 04          	.byte 0x4
 2572 00b1 02 00 00 00 	.4byte .LCFI5-.LFB5
 2573 00b5 13          	.byte 0x13
 2574 00b6 7D          	.sleb128 -3
 2575 00b7 0D          	.byte 0xd
 2576 00b8 0E          	.uleb128 0xe
 2577 00b9 8E          	.byte 0x8e
 2578 00ba 02          	.uleb128 0x2
 2579 00bb 00          	.align 4
 2580                 	.LEFDE10:
 2581                 	.LSFDE12:
 2582 00bc 18 00 00 00 	.4byte .LEFDE12-.LASFDE12
 2583                 	.LASFDE12:
 2584 00c0 00 00 00 00 	.4byte .Lframe0
 2585 00c4 00 00 00 00 	.4byte .LFB6
 2586 00c8 52 00 00 00 	.4byte .LFE6-.LFB6
 2587 00cc 04          	.byte 0x4
 2588 00cd 02 00 00 00 	.4byte .LCFI6-.LFB6
 2589 00d1 13          	.byte 0x13
 2590 00d2 7D          	.sleb128 -3
 2591 00d3 0D          	.byte 0xd
 2592 00d4 0E          	.uleb128 0xe
 2593 00d5 8E          	.byte 0x8e
 2594 00d6 02          	.uleb128 0x2
 2595 00d7 00          	.align 4
 2596                 	.LEFDE12:
 2597                 	.LSFDE14:
 2598 00d8 18 00 00 00 	.4byte .LEFDE14-.LASFDE14
 2599                 	.LASFDE14:
 2600 00dc 00 00 00 00 	.4byte .Lframe0
 2601 00e0 00 00 00 00 	.4byte .LFB7
 2602 00e4 56 00 00 00 	.4byte .LFE7-.LFB7
 2603 00e8 04          	.byte 0x4
 2604 00e9 02 00 00 00 	.4byte .LCFI7-.LFB7
 2605 00ed 13          	.byte 0x13
 2606 00ee 7D          	.sleb128 -3
 2607 00ef 0D          	.byte 0xd
 2608 00f0 0E          	.uleb128 0xe
 2609 00f1 8E          	.byte 0x8e
 2610 00f2 02          	.uleb128 0x2
 2611 00f3 00          	.align 4
 2612                 	.LEFDE14:
 2613                 	.LSFDE16:
 2614 00f4 18 00 00 00 	.4byte .LEFDE16-.LASFDE16
 2615                 	.LASFDE16:
 2616 00f8 00 00 00 00 	.4byte .Lframe0
 2617 00fc 00 00 00 00 	.4byte .LFB8
 2618 0100 56 00 00 00 	.4byte .LFE8-.LFB8
 2619 0104 04          	.byte 0x4
 2620 0105 02 00 00 00 	.4byte .LCFI8-.LFB8
 2621 0109 13          	.byte 0x13
 2622 010a 7D          	.sleb128 -3
MPLAB XC16 ASSEMBLY Listing:   			page 61


 2623 010b 0D          	.byte 0xd
 2624 010c 0E          	.uleb128 0xe
 2625 010d 8E          	.byte 0x8e
 2626 010e 02          	.uleb128 0x2
 2627 010f 00          	.align 4
 2628                 	.LEFDE16:
 2629                 	.LSFDE18:
 2630 0110 18 00 00 00 	.4byte .LEFDE18-.LASFDE18
 2631                 	.LASFDE18:
 2632 0114 00 00 00 00 	.4byte .Lframe0
 2633 0118 00 00 00 00 	.4byte .LFB9
 2634 011c 52 00 00 00 	.4byte .LFE9-.LFB9
 2635 0120 04          	.byte 0x4
 2636 0121 02 00 00 00 	.4byte .LCFI9-.LFB9
 2637 0125 13          	.byte 0x13
 2638 0126 7D          	.sleb128 -3
 2639 0127 0D          	.byte 0xd
 2640 0128 0E          	.uleb128 0xe
 2641 0129 8E          	.byte 0x8e
 2642 012a 02          	.uleb128 0x2
 2643 012b 00          	.align 4
 2644                 	.LEFDE18:
 2645                 	.LSFDE20:
 2646 012c 18 00 00 00 	.4byte .LEFDE20-.LASFDE20
 2647                 	.LASFDE20:
 2648 0130 00 00 00 00 	.4byte .Lframe0
 2649 0134 00 00 00 00 	.4byte .LFB10
 2650 0138 52 00 00 00 	.4byte .LFE10-.LFB10
 2651 013c 04          	.byte 0x4
 2652 013d 02 00 00 00 	.4byte .LCFI10-.LFB10
 2653 0141 13          	.byte 0x13
 2654 0142 7D          	.sleb128 -3
 2655 0143 0D          	.byte 0xd
 2656 0144 0E          	.uleb128 0xe
 2657 0145 8E          	.byte 0x8e
 2658 0146 02          	.uleb128 0x2
 2659 0147 00          	.align 4
 2660                 	.LEFDE20:
 2661                 	.LSFDE22:
 2662 0148 18 00 00 00 	.4byte .LEFDE22-.LASFDE22
 2663                 	.LASFDE22:
 2664 014c 00 00 00 00 	.4byte .Lframe0
 2665 0150 00 00 00 00 	.4byte .LFB11
 2666 0154 56 00 00 00 	.4byte .LFE11-.LFB11
 2667 0158 04          	.byte 0x4
 2668 0159 02 00 00 00 	.4byte .LCFI11-.LFB11
 2669 015d 13          	.byte 0x13
 2670 015e 7D          	.sleb128 -3
 2671 015f 0D          	.byte 0xd
 2672 0160 0E          	.uleb128 0xe
 2673 0161 8E          	.byte 0x8e
 2674 0162 02          	.uleb128 0x2
 2675 0163 00          	.align 4
 2676                 	.LEFDE22:
 2677                 	.LSFDE24:
 2678 0164 18 00 00 00 	.4byte .LEFDE24-.LASFDE24
 2679                 	.LASFDE24:
MPLAB XC16 ASSEMBLY Listing:   			page 62


 2680 0168 00 00 00 00 	.4byte .Lframe0
 2681 016c 00 00 00 00 	.4byte .LFB12
 2682 0170 56 00 00 00 	.4byte .LFE12-.LFB12
 2683 0174 04          	.byte 0x4
 2684 0175 02 00 00 00 	.4byte .LCFI12-.LFB12
 2685 0179 13          	.byte 0x13
 2686 017a 7D          	.sleb128 -3
 2687 017b 0D          	.byte 0xd
 2688 017c 0E          	.uleb128 0xe
 2689 017d 8E          	.byte 0x8e
 2690 017e 02          	.uleb128 0x2
 2691 017f 00          	.align 4
 2692                 	.LEFDE24:
 2693                 	.LSFDE26:
 2694 0180 18 00 00 00 	.4byte .LEFDE26-.LASFDE26
 2695                 	.LASFDE26:
 2696 0184 00 00 00 00 	.4byte .Lframe0
 2697 0188 00 00 00 00 	.4byte .LFB13
 2698 018c 52 00 00 00 	.4byte .LFE13-.LFB13
 2699 0190 04          	.byte 0x4
 2700 0191 02 00 00 00 	.4byte .LCFI13-.LFB13
 2701 0195 13          	.byte 0x13
 2702 0196 7D          	.sleb128 -3
 2703 0197 0D          	.byte 0xd
 2704 0198 0E          	.uleb128 0xe
 2705 0199 8E          	.byte 0x8e
 2706 019a 02          	.uleb128 0x2
 2707 019b 00          	.align 4
 2708                 	.LEFDE26:
 2709                 	.LSFDE28:
 2710 019c 18 00 00 00 	.4byte .LEFDE28-.LASFDE28
 2711                 	.LASFDE28:
 2712 01a0 00 00 00 00 	.4byte .Lframe0
 2713 01a4 00 00 00 00 	.4byte .LFB14
 2714 01a8 52 00 00 00 	.4byte .LFE14-.LFB14
 2715 01ac 04          	.byte 0x4
 2716 01ad 02 00 00 00 	.4byte .LCFI14-.LFB14
 2717 01b1 13          	.byte 0x13
 2718 01b2 7D          	.sleb128 -3
 2719 01b3 0D          	.byte 0xd
 2720 01b4 0E          	.uleb128 0xe
 2721 01b5 8E          	.byte 0x8e
 2722 01b6 02          	.uleb128 0x2
 2723 01b7 00          	.align 4
 2724                 	.LEFDE28:
 2725                 	.LSFDE30:
 2726 01b8 18 00 00 00 	.4byte .LEFDE30-.LASFDE30
 2727                 	.LASFDE30:
 2728 01bc 00 00 00 00 	.4byte .Lframe0
 2729 01c0 00 00 00 00 	.4byte .LFB15
 2730 01c4 56 00 00 00 	.4byte .LFE15-.LFB15
 2731 01c8 04          	.byte 0x4
 2732 01c9 02 00 00 00 	.4byte .LCFI15-.LFB15
 2733 01cd 13          	.byte 0x13
 2734 01ce 7D          	.sleb128 -3
 2735 01cf 0D          	.byte 0xd
 2736 01d0 0E          	.uleb128 0xe
MPLAB XC16 ASSEMBLY Listing:   			page 63


 2737 01d1 8E          	.byte 0x8e
 2738 01d2 02          	.uleb128 0x2
 2739 01d3 00          	.align 4
 2740                 	.LEFDE30:
 2741                 	.LSFDE32:
 2742 01d4 18 00 00 00 	.4byte .LEFDE32-.LASFDE32
 2743                 	.LASFDE32:
 2744 01d8 00 00 00 00 	.4byte .Lframe0
 2745 01dc 00 00 00 00 	.4byte .LFB16
 2746 01e0 56 00 00 00 	.4byte .LFE16-.LFB16
 2747 01e4 04          	.byte 0x4
 2748 01e5 02 00 00 00 	.4byte .LCFI16-.LFB16
 2749 01e9 13          	.byte 0x13
 2750 01ea 7D          	.sleb128 -3
 2751 01eb 0D          	.byte 0xd
 2752 01ec 0E          	.uleb128 0xe
 2753 01ed 8E          	.byte 0x8e
 2754 01ee 02          	.uleb128 0x2
 2755 01ef 00          	.align 4
 2756                 	.LEFDE32:
 2757                 	.LSFDE34:
 2758 01f0 18 00 00 00 	.4byte .LEFDE34-.LASFDE34
 2759                 	.LASFDE34:
 2760 01f4 00 00 00 00 	.4byte .Lframe0
 2761 01f8 00 00 00 00 	.4byte .LFB17
 2762 01fc 52 00 00 00 	.4byte .LFE17-.LFB17
 2763 0200 04          	.byte 0x4
 2764 0201 02 00 00 00 	.4byte .LCFI17-.LFB17
 2765 0205 13          	.byte 0x13
 2766 0206 7D          	.sleb128 -3
 2767 0207 0D          	.byte 0xd
 2768 0208 0E          	.uleb128 0xe
 2769 0209 8E          	.byte 0x8e
 2770 020a 02          	.uleb128 0x2
 2771 020b 00          	.align 4
 2772                 	.LEFDE34:
 2773                 	.LSFDE36:
 2774 020c 18 00 00 00 	.4byte .LEFDE36-.LASFDE36
 2775                 	.LASFDE36:
 2776 0210 00 00 00 00 	.4byte .Lframe0
 2777 0214 00 00 00 00 	.4byte .LFB18
 2778 0218 52 00 00 00 	.4byte .LFE18-.LFB18
 2779 021c 04          	.byte 0x4
 2780 021d 02 00 00 00 	.4byte .LCFI18-.LFB18
 2781 0221 13          	.byte 0x13
 2782 0222 7D          	.sleb128 -3
 2783 0223 0D          	.byte 0xd
 2784 0224 0E          	.uleb128 0xe
 2785 0225 8E          	.byte 0x8e
 2786 0226 02          	.uleb128 0x2
 2787 0227 00          	.align 4
 2788                 	.LEFDE36:
 2789                 	.LSFDE38:
 2790 0228 18 00 00 00 	.4byte .LEFDE38-.LASFDE38
 2791                 	.LASFDE38:
 2792 022c 00 00 00 00 	.4byte .Lframe0
 2793 0230 00 00 00 00 	.4byte .LFB19
MPLAB XC16 ASSEMBLY Listing:   			page 64


 2794 0234 0C 00 00 00 	.4byte .LFE19-.LFB19
 2795 0238 04          	.byte 0x4
 2796 0239 02 00 00 00 	.4byte .LCFI19-.LFB19
 2797 023d 13          	.byte 0x13
 2798 023e 7D          	.sleb128 -3
 2799 023f 0D          	.byte 0xd
 2800 0240 0E          	.uleb128 0xe
 2801 0241 8E          	.byte 0x8e
 2802 0242 02          	.uleb128 0x2
 2803 0243 00          	.align 4
 2804                 	.LEFDE38:
 2805                 	.LSFDE40:
 2806 0244 18 00 00 00 	.4byte .LEFDE40-.LASFDE40
 2807                 	.LASFDE40:
 2808 0248 00 00 00 00 	.4byte .Lframe0
 2809 024c 00 00 00 00 	.4byte .LFB20
 2810 0250 0C 00 00 00 	.4byte .LFE20-.LFB20
 2811 0254 04          	.byte 0x4
 2812 0255 02 00 00 00 	.4byte .LCFI20-.LFB20
 2813 0259 13          	.byte 0x13
 2814 025a 7D          	.sleb128 -3
 2815 025b 0D          	.byte 0xd
 2816 025c 0E          	.uleb128 0xe
 2817 025d 8E          	.byte 0x8e
 2818 025e 02          	.uleb128 0x2
 2819 025f 00          	.align 4
 2820                 	.LEFDE40:
 2821                 	.LSFDE42:
 2822 0260 18 00 00 00 	.4byte .LEFDE42-.LASFDE42
 2823                 	.LASFDE42:
 2824 0264 00 00 00 00 	.4byte .Lframe0
 2825 0268 00 00 00 00 	.4byte .LFB21
 2826 026c 0C 00 00 00 	.4byte .LFE21-.LFB21
 2827 0270 04          	.byte 0x4
 2828 0271 02 00 00 00 	.4byte .LCFI21-.LFB21
 2829 0275 13          	.byte 0x13
 2830 0276 7D          	.sleb128 -3
 2831 0277 0D          	.byte 0xd
 2832 0278 0E          	.uleb128 0xe
 2833 0279 8E          	.byte 0x8e
 2834 027a 02          	.uleb128 0x2
 2835 027b 00          	.align 4
 2836                 	.LEFDE42:
 2837                 	.LSFDE44:
 2838 027c 18 00 00 00 	.4byte .LEFDE44-.LASFDE44
 2839                 	.LASFDE44:
 2840 0280 00 00 00 00 	.4byte .Lframe0
 2841 0284 00 00 00 00 	.4byte .LFB22
 2842 0288 0C 00 00 00 	.4byte .LFE22-.LFB22
 2843 028c 04          	.byte 0x4
 2844 028d 02 00 00 00 	.4byte .LCFI22-.LFB22
 2845 0291 13          	.byte 0x13
 2846 0292 7D          	.sleb128 -3
 2847 0293 0D          	.byte 0xd
 2848 0294 0E          	.uleb128 0xe
 2849 0295 8E          	.byte 0x8e
 2850 0296 02          	.uleb128 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 65


 2851 0297 00          	.align 4
 2852                 	.LEFDE44:
 2853                 	.LSFDE46:
 2854 0298 18 00 00 00 	.4byte .LEFDE46-.LASFDE46
 2855                 	.LASFDE46:
 2856 029c 00 00 00 00 	.4byte .Lframe0
 2857 02a0 00 00 00 00 	.4byte .LFB23
 2858 02a4 0C 00 00 00 	.4byte .LFE23-.LFB23
 2859 02a8 04          	.byte 0x4
 2860 02a9 02 00 00 00 	.4byte .LCFI23-.LFB23
 2861 02ad 13          	.byte 0x13
 2862 02ae 7D          	.sleb128 -3
 2863 02af 0D          	.byte 0xd
 2864 02b0 0E          	.uleb128 0xe
 2865 02b1 8E          	.byte 0x8e
 2866 02b2 02          	.uleb128 0x2
 2867 02b3 00          	.align 4
 2868                 	.LEFDE46:
 2869                 	.LSFDE48:
 2870 02b4 18 00 00 00 	.4byte .LEFDE48-.LASFDE48
 2871                 	.LASFDE48:
 2872 02b8 00 00 00 00 	.4byte .Lframe0
 2873 02bc 00 00 00 00 	.4byte .LFB24
 2874 02c0 0C 00 00 00 	.4byte .LFE24-.LFB24
 2875 02c4 04          	.byte 0x4
 2876 02c5 02 00 00 00 	.4byte .LCFI24-.LFB24
 2877 02c9 13          	.byte 0x13
 2878 02ca 7D          	.sleb128 -3
 2879 02cb 0D          	.byte 0xd
 2880 02cc 0E          	.uleb128 0xe
 2881 02cd 8E          	.byte 0x8e
 2882 02ce 02          	.uleb128 0x2
 2883 02cf 00          	.align 4
 2884                 	.LEFDE48:
 2885                 	.LSFDE50:
 2886 02d0 18 00 00 00 	.4byte .LEFDE50-.LASFDE50
 2887                 	.LASFDE50:
 2888 02d4 00 00 00 00 	.4byte .Lframe0
 2889 02d8 00 00 00 00 	.4byte .LFB25
 2890 02dc 0C 00 00 00 	.4byte .LFE25-.LFB25
 2891 02e0 04          	.byte 0x4
 2892 02e1 02 00 00 00 	.4byte .LCFI25-.LFB25
 2893 02e5 13          	.byte 0x13
 2894 02e6 7D          	.sleb128 -3
 2895 02e7 0D          	.byte 0xd
 2896 02e8 0E          	.uleb128 0xe
 2897 02e9 8E          	.byte 0x8e
 2898 02ea 02          	.uleb128 0x2
 2899 02eb 00          	.align 4
 2900                 	.LEFDE50:
 2901                 	.LSFDE52:
 2902 02ec 18 00 00 00 	.4byte .LEFDE52-.LASFDE52
 2903                 	.LASFDE52:
 2904 02f0 00 00 00 00 	.4byte .Lframe0
 2905 02f4 00 00 00 00 	.4byte .LFB26
 2906 02f8 0C 00 00 00 	.4byte .LFE26-.LFB26
 2907 02fc 04          	.byte 0x4
MPLAB XC16 ASSEMBLY Listing:   			page 66


 2908 02fd 02 00 00 00 	.4byte .LCFI26-.LFB26
 2909 0301 13          	.byte 0x13
 2910 0302 7D          	.sleb128 -3
 2911 0303 0D          	.byte 0xd
 2912 0304 0E          	.uleb128 0xe
 2913 0305 8E          	.byte 0x8e
 2914 0306 02          	.uleb128 0x2
 2915 0307 00          	.align 4
 2916                 	.LEFDE52:
 2917                 	.LSFDE54:
 2918 0308 18 00 00 00 	.4byte .LEFDE54-.LASFDE54
 2919                 	.LASFDE54:
 2920 030c 00 00 00 00 	.4byte .Lframe0
 2921 0310 00 00 00 00 	.4byte .LFB27
 2922 0314 0C 00 00 00 	.4byte .LFE27-.LFB27
 2923 0318 04          	.byte 0x4
 2924 0319 02 00 00 00 	.4byte .LCFI27-.LFB27
 2925 031d 13          	.byte 0x13
 2926 031e 7D          	.sleb128 -3
 2927 031f 0D          	.byte 0xd
 2928 0320 0E          	.uleb128 0xe
 2929 0321 8E          	.byte 0x8e
 2930 0322 02          	.uleb128 0x2
 2931 0323 00          	.align 4
 2932                 	.LEFDE54:
 2933                 	.LSFDE56:
 2934 0324 18 00 00 00 	.4byte .LEFDE56-.LASFDE56
 2935                 	.LASFDE56:
 2936 0328 00 00 00 00 	.4byte .Lframe0
 2937 032c 00 00 00 00 	.4byte .LFB28
 2938 0330 0C 00 00 00 	.4byte .LFE28-.LFB28
 2939 0334 04          	.byte 0x4
 2940 0335 02 00 00 00 	.4byte .LCFI28-.LFB28
 2941 0339 13          	.byte 0x13
 2942 033a 7D          	.sleb128 -3
 2943 033b 0D          	.byte 0xd
 2944 033c 0E          	.uleb128 0xe
 2945 033d 8E          	.byte 0x8e
 2946 033e 02          	.uleb128 0x2
 2947 033f 00          	.align 4
 2948                 	.LEFDE56:
 2949                 	.LSFDE58:
 2950 0340 18 00 00 00 	.4byte .LEFDE58-.LASFDE58
 2951                 	.LASFDE58:
 2952 0344 00 00 00 00 	.4byte .Lframe0
 2953 0348 00 00 00 00 	.4byte .LFB29
 2954 034c 0C 00 00 00 	.4byte .LFE29-.LFB29
 2955 0350 04          	.byte 0x4
 2956 0351 02 00 00 00 	.4byte .LCFI29-.LFB29
 2957 0355 13          	.byte 0x13
 2958 0356 7D          	.sleb128 -3
 2959 0357 0D          	.byte 0xd
 2960 0358 0E          	.uleb128 0xe
 2961 0359 8E          	.byte 0x8e
 2962 035a 02          	.uleb128 0x2
 2963 035b 00          	.align 4
 2964                 	.LEFDE58:
MPLAB XC16 ASSEMBLY Listing:   			page 67


 2965                 	.LSFDE60:
 2966 035c 18 00 00 00 	.4byte .LEFDE60-.LASFDE60
 2967                 	.LASFDE60:
 2968 0360 00 00 00 00 	.4byte .Lframe0
 2969 0364 00 00 00 00 	.4byte .LFB30
 2970 0368 0C 00 00 00 	.4byte .LFE30-.LFB30
 2971 036c 04          	.byte 0x4
 2972 036d 02 00 00 00 	.4byte .LCFI30-.LFB30
 2973 0371 13          	.byte 0x13
 2974 0372 7D          	.sleb128 -3
 2975 0373 0D          	.byte 0xd
 2976 0374 0E          	.uleb128 0xe
 2977 0375 8E          	.byte 0x8e
 2978 0376 02          	.uleb128 0x2
 2979 0377 00          	.align 4
 2980                 	.LEFDE60:
 2981                 	.LSFDE62:
 2982 0378 18 00 00 00 	.4byte .LEFDE62-.LASFDE62
 2983                 	.LASFDE62:
 2984 037c 00 00 00 00 	.4byte .Lframe0
 2985 0380 00 00 00 00 	.4byte .LFB31
 2986 0384 0C 00 00 00 	.4byte .LFE31-.LFB31
 2987 0388 04          	.byte 0x4
 2988 0389 02 00 00 00 	.4byte .LCFI31-.LFB31
 2989 038d 13          	.byte 0x13
 2990 038e 7D          	.sleb128 -3
 2991 038f 0D          	.byte 0xd
 2992 0390 0E          	.uleb128 0xe
 2993 0391 8E          	.byte 0x8e
 2994 0392 02          	.uleb128 0x2
 2995 0393 00          	.align 4
 2996                 	.LEFDE62:
 2997                 	.LSFDE64:
 2998 0394 18 00 00 00 	.4byte .LEFDE64-.LASFDE64
 2999                 	.LASFDE64:
 3000 0398 00 00 00 00 	.4byte .Lframe0
 3001 039c 00 00 00 00 	.4byte .LFB32
 3002 03a0 0C 00 00 00 	.4byte .LFE32-.LFB32
 3003 03a4 04          	.byte 0x4
 3004 03a5 02 00 00 00 	.4byte .LCFI32-.LFB32
 3005 03a9 13          	.byte 0x13
 3006 03aa 7D          	.sleb128 -3
 3007 03ab 0D          	.byte 0xd
 3008 03ac 0E          	.uleb128 0xe
 3009 03ad 8E          	.byte 0x8e
 3010 03ae 02          	.uleb128 0x2
 3011 03af 00          	.align 4
 3012                 	.LEFDE64:
 3013                 	.LSFDE66:
 3014 03b0 18 00 00 00 	.4byte .LEFDE66-.LASFDE66
 3015                 	.LASFDE66:
 3016 03b4 00 00 00 00 	.4byte .Lframe0
 3017 03b8 00 00 00 00 	.4byte .LFB33
 3018 03bc 0C 00 00 00 	.4byte .LFE33-.LFB33
 3019 03c0 04          	.byte 0x4
 3020 03c1 02 00 00 00 	.4byte .LCFI33-.LFB33
 3021 03c5 13          	.byte 0x13
MPLAB XC16 ASSEMBLY Listing:   			page 68


 3022 03c6 7D          	.sleb128 -3
 3023 03c7 0D          	.byte 0xd
 3024 03c8 0E          	.uleb128 0xe
 3025 03c9 8E          	.byte 0x8e
 3026 03ca 02          	.uleb128 0x2
 3027 03cb 00          	.align 4
 3028                 	.LEFDE66:
 3029                 	.LSFDE68:
 3030 03cc 18 00 00 00 	.4byte .LEFDE68-.LASFDE68
 3031                 	.LASFDE68:
 3032 03d0 00 00 00 00 	.4byte .Lframe0
 3033 03d4 00 00 00 00 	.4byte .LFB34
 3034 03d8 0C 00 00 00 	.4byte .LFE34-.LFB34
 3035 03dc 04          	.byte 0x4
 3036 03dd 02 00 00 00 	.4byte .LCFI34-.LFB34
 3037 03e1 13          	.byte 0x13
 3038 03e2 7D          	.sleb128 -3
 3039 03e3 0D          	.byte 0xd
 3040 03e4 0E          	.uleb128 0xe
 3041 03e5 8E          	.byte 0x8e
 3042 03e6 02          	.uleb128 0x2
 3043 03e7 00          	.align 4
 3044                 	.LEFDE68:
 3045                 	.LSFDE70:
 3046 03e8 18 00 00 00 	.4byte .LEFDE70-.LASFDE70
 3047                 	.LASFDE70:
 3048 03ec 00 00 00 00 	.4byte .Lframe0
 3049 03f0 00 00 00 00 	.4byte .LFB35
 3050 03f4 0C 00 00 00 	.4byte .LFE35-.LFB35
 3051 03f8 04          	.byte 0x4
 3052 03f9 02 00 00 00 	.4byte .LCFI35-.LFB35
 3053 03fd 13          	.byte 0x13
 3054 03fe 7D          	.sleb128 -3
 3055 03ff 0D          	.byte 0xd
 3056 0400 0E          	.uleb128 0xe
 3057 0401 8E          	.byte 0x8e
 3058 0402 02          	.uleb128 0x2
 3059 0403 00          	.align 4
 3060                 	.LEFDE70:
 3061                 	.LSFDE72:
 3062 0404 18 00 00 00 	.4byte .LEFDE72-.LASFDE72
 3063                 	.LASFDE72:
 3064 0408 00 00 00 00 	.4byte .Lframe0
 3065 040c 00 00 00 00 	.4byte .LFB36
 3066 0410 0C 00 00 00 	.4byte .LFE36-.LFB36
 3067 0414 04          	.byte 0x4
 3068 0415 02 00 00 00 	.4byte .LCFI36-.LFB36
 3069 0419 13          	.byte 0x13
 3070 041a 7D          	.sleb128 -3
 3071 041b 0D          	.byte 0xd
 3072 041c 0E          	.uleb128 0xe
 3073 041d 8E          	.byte 0x8e
 3074 041e 02          	.uleb128 0x2
 3075 041f 00          	.align 4
 3076                 	.LEFDE72:
 3077                 	.LSFDE74:
 3078 0420 18 00 00 00 	.4byte .LEFDE74-.LASFDE74
MPLAB XC16 ASSEMBLY Listing:   			page 69


 3079                 	.LASFDE74:
 3080 0424 00 00 00 00 	.4byte .Lframe0
 3081 0428 00 00 00 00 	.4byte .LFB37
 3082 042c 0C 00 00 00 	.4byte .LFE37-.LFB37
 3083 0430 04          	.byte 0x4
 3084 0431 02 00 00 00 	.4byte .LCFI37-.LFB37
 3085 0435 13          	.byte 0x13
 3086 0436 7D          	.sleb128 -3
 3087 0437 0D          	.byte 0xd
 3088 0438 0E          	.uleb128 0xe
 3089 0439 8E          	.byte 0x8e
 3090 043a 02          	.uleb128 0x2
 3091 043b 00          	.align 4
 3092                 	.LEFDE74:
 3093                 	.LSFDE76:
 3094 043c 18 00 00 00 	.4byte .LEFDE76-.LASFDE76
 3095                 	.LASFDE76:
 3096 0440 00 00 00 00 	.4byte .Lframe0
 3097 0444 00 00 00 00 	.4byte .LFB38
 3098 0448 0C 00 00 00 	.4byte .LFE38-.LFB38
 3099 044c 04          	.byte 0x4
 3100 044d 02 00 00 00 	.4byte .LCFI38-.LFB38
 3101 0451 13          	.byte 0x13
 3102 0452 7D          	.sleb128 -3
 3103 0453 0D          	.byte 0xd
 3104 0454 0E          	.uleb128 0xe
 3105 0455 8E          	.byte 0x8e
 3106 0456 02          	.uleb128 0x2
 3107 0457 00          	.align 4
 3108                 	.LEFDE76:
 3109                 	.LSFDE78:
 3110 0458 18 00 00 00 	.4byte .LEFDE78-.LASFDE78
 3111                 	.LASFDE78:
 3112 045c 00 00 00 00 	.4byte .Lframe0
 3113 0460 00 00 00 00 	.4byte .LFB39
 3114 0464 0C 00 00 00 	.4byte .LFE39-.LFB39
 3115 0468 04          	.byte 0x4
 3116 0469 02 00 00 00 	.4byte .LCFI39-.LFB39
 3117 046d 13          	.byte 0x13
 3118 046e 7D          	.sleb128 -3
 3119 046f 0D          	.byte 0xd
 3120 0470 0E          	.uleb128 0xe
 3121 0471 8E          	.byte 0x8e
 3122 0472 02          	.uleb128 0x2
 3123 0473 00          	.align 4
 3124                 	.LEFDE78:
 3125                 	.LSFDE80:
 3126 0474 18 00 00 00 	.4byte .LEFDE80-.LASFDE80
 3127                 	.LASFDE80:
 3128 0478 00 00 00 00 	.4byte .Lframe0
 3129 047c 00 00 00 00 	.4byte .LFB40
 3130 0480 0C 00 00 00 	.4byte .LFE40-.LFB40
 3131 0484 04          	.byte 0x4
 3132 0485 02 00 00 00 	.4byte .LCFI40-.LFB40
 3133 0489 13          	.byte 0x13
 3134 048a 7D          	.sleb128 -3
 3135 048b 0D          	.byte 0xd
MPLAB XC16 ASSEMBLY Listing:   			page 70


 3136 048c 0E          	.uleb128 0xe
 3137 048d 8E          	.byte 0x8e
 3138 048e 02          	.uleb128 0x2
 3139 048f 00          	.align 4
 3140                 	.LEFDE80:
 3141                 	.LSFDE82:
 3142 0490 18 00 00 00 	.4byte .LEFDE82-.LASFDE82
 3143                 	.LASFDE82:
 3144 0494 00 00 00 00 	.4byte .Lframe0
 3145 0498 00 00 00 00 	.4byte .LFB41
 3146 049c 0C 00 00 00 	.4byte .LFE41-.LFB41
 3147 04a0 04          	.byte 0x4
 3148 04a1 02 00 00 00 	.4byte .LCFI41-.LFB41
 3149 04a5 13          	.byte 0x13
 3150 04a6 7D          	.sleb128 -3
 3151 04a7 0D          	.byte 0xd
 3152 04a8 0E          	.uleb128 0xe
 3153 04a9 8E          	.byte 0x8e
 3154 04aa 02          	.uleb128 0x2
 3155 04ab 00          	.align 4
 3156                 	.LEFDE82:
 3157                 	.LSFDE84:
 3158 04ac 18 00 00 00 	.4byte .LEFDE84-.LASFDE84
 3159                 	.LASFDE84:
 3160 04b0 00 00 00 00 	.4byte .Lframe0
 3161 04b4 00 00 00 00 	.4byte .LFB42
 3162 04b8 0C 00 00 00 	.4byte .LFE42-.LFB42
 3163 04bc 04          	.byte 0x4
 3164 04bd 02 00 00 00 	.4byte .LCFI42-.LFB42
 3165 04c1 13          	.byte 0x13
 3166 04c2 7D          	.sleb128 -3
 3167 04c3 0D          	.byte 0xd
 3168 04c4 0E          	.uleb128 0xe
 3169 04c5 8E          	.byte 0x8e
 3170 04c6 02          	.uleb128 0x2
 3171 04c7 00          	.align 4
 3172                 	.LEFDE84:
 3173                 	.LSFDE86:
 3174 04c8 18 00 00 00 	.4byte .LEFDE86-.LASFDE86
 3175                 	.LASFDE86:
 3176 04cc 00 00 00 00 	.4byte .Lframe0
 3177 04d0 00 00 00 00 	.4byte .LFB43
 3178 04d4 0C 00 00 00 	.4byte .LFE43-.LFB43
 3179 04d8 04          	.byte 0x4
 3180 04d9 02 00 00 00 	.4byte .LCFI43-.LFB43
 3181 04dd 13          	.byte 0x13
 3182 04de 7D          	.sleb128 -3
 3183 04df 0D          	.byte 0xd
 3184 04e0 0E          	.uleb128 0xe
 3185 04e1 8E          	.byte 0x8e
 3186 04e2 02          	.uleb128 0x2
 3187 04e3 00          	.align 4
 3188                 	.LEFDE86:
 3189                 	.LSFDE88:
 3190 04e4 18 00 00 00 	.4byte .LEFDE88-.LASFDE88
 3191                 	.LASFDE88:
 3192 04e8 00 00 00 00 	.4byte .Lframe0
MPLAB XC16 ASSEMBLY Listing:   			page 71


 3193 04ec 00 00 00 00 	.4byte .LFB44
 3194 04f0 0C 00 00 00 	.4byte .LFE44-.LFB44
 3195 04f4 04          	.byte 0x4
 3196 04f5 02 00 00 00 	.4byte .LCFI44-.LFB44
 3197 04f9 13          	.byte 0x13
 3198 04fa 7D          	.sleb128 -3
 3199 04fb 0D          	.byte 0xd
 3200 04fc 0E          	.uleb128 0xe
 3201 04fd 8E          	.byte 0x8e
 3202 04fe 02          	.uleb128 0x2
 3203 04ff 00          	.align 4
 3204                 	.LEFDE88:
 3205                 	.LSFDE90:
 3206 0500 18 00 00 00 	.4byte .LEFDE90-.LASFDE90
 3207                 	.LASFDE90:
 3208 0504 00 00 00 00 	.4byte .Lframe0
 3209 0508 00 00 00 00 	.4byte .LFB45
 3210 050c 0C 00 00 00 	.4byte .LFE45-.LFB45
 3211 0510 04          	.byte 0x4
 3212 0511 02 00 00 00 	.4byte .LCFI45-.LFB45
 3213 0515 13          	.byte 0x13
 3214 0516 7D          	.sleb128 -3
 3215 0517 0D          	.byte 0xd
 3216 0518 0E          	.uleb128 0xe
 3217 0519 8E          	.byte 0x8e
 3218 051a 02          	.uleb128 0x2
 3219 051b 00          	.align 4
 3220                 	.LEFDE90:
 3221                 	.LSFDE92:
 3222 051c 2E 00 00 00 	.4byte .LEFDE92-.LASFDE92
 3223                 	.LASFDE92:
 3224 0520 00 00 00 00 	.4byte .Lframe0
 3225 0524 00 00 00 00 	.4byte .LFB46
 3226 0528 3A 00 00 00 	.4byte .LFE46-.LFB46
 3227 052c 04          	.byte 0x4
 3228 052d 06 00 00 00 	.4byte .LCFI47-.LFB46
 3229 0531 13          	.byte 0x13
 3230 0532 7B          	.sleb128 -5
 3231 0533 04          	.byte 0x4
 3232 0534 02 00 00 00 	.4byte .LCFI48-.LCFI47
 3233 0538 13          	.byte 0x13
 3234 0539 79          	.sleb128 -7
 3235 053a 04          	.byte 0x4
 3236 053b 04 00 00 00 	.4byte .LCFI50-.LCFI48
 3237 053f 86          	.byte 0x86
 3238 0540 05          	.uleb128 0x5
 3239 0541 84          	.byte 0x84
 3240 0542 03          	.uleb128 0x3
 3241 0543 04          	.byte 0x4
 3242 0544 0C 00 00 00 	.4byte .LCFI51-.LCFI50
 3243 0548 13          	.byte 0x13
 3244 0549 76          	.sleb128 -10
 3245 054a 0D          	.byte 0xd
 3246 054b 0E          	.uleb128 0xe
 3247 054c 8E          	.byte 0x8e
 3248 054d 09          	.uleb128 0x9
 3249                 	.align 4
MPLAB XC16 ASSEMBLY Listing:   			page 72


 3250                 	.LEFDE92:
 3251                 	.LSFDE94:
 3252 054e 2E 00 00 00 	.4byte .LEFDE94-.LASFDE94
 3253                 	.LASFDE94:
 3254 0552 00 00 00 00 	.4byte .Lframe0
 3255 0556 00 00 00 00 	.4byte .LFB47
 3256 055a 3A 00 00 00 	.4byte .LFE47-.LFB47
 3257 055e 04          	.byte 0x4
 3258 055f 06 00 00 00 	.4byte .LCFI53-.LFB47
 3259 0563 13          	.byte 0x13
 3260 0564 7B          	.sleb128 -5
 3261 0565 04          	.byte 0x4
 3262 0566 02 00 00 00 	.4byte .LCFI54-.LCFI53
 3263 056a 13          	.byte 0x13
 3264 056b 79          	.sleb128 -7
 3265 056c 04          	.byte 0x4
 3266 056d 04 00 00 00 	.4byte .LCFI56-.LCFI54
 3267 0571 86          	.byte 0x86
 3268 0572 05          	.uleb128 0x5
 3269 0573 84          	.byte 0x84
 3270 0574 03          	.uleb128 0x3
 3271 0575 04          	.byte 0x4
 3272 0576 0C 00 00 00 	.4byte .LCFI57-.LCFI56
 3273 057a 13          	.byte 0x13
 3274 057b 76          	.sleb128 -10
 3275 057c 0D          	.byte 0xd
 3276 057d 0E          	.uleb128 0xe
 3277 057e 8E          	.byte 0x8e
 3278 057f 09          	.uleb128 0x9
 3279                 	.align 4
 3280                 	.LEFDE94:
 3281                 	.LSFDE96:
 3282 0580 2E 00 00 00 	.4byte .LEFDE96-.LASFDE96
 3283                 	.LASFDE96:
 3284 0584 00 00 00 00 	.4byte .Lframe0
 3285 0588 00 00 00 00 	.4byte .LFB48
 3286 058c 3A 00 00 00 	.4byte .LFE48-.LFB48
 3287 0590 04          	.byte 0x4
 3288 0591 06 00 00 00 	.4byte .LCFI59-.LFB48
 3289 0595 13          	.byte 0x13
 3290 0596 7B          	.sleb128 -5
 3291 0597 04          	.byte 0x4
 3292 0598 02 00 00 00 	.4byte .LCFI60-.LCFI59
 3293 059c 13          	.byte 0x13
 3294 059d 79          	.sleb128 -7
 3295 059e 04          	.byte 0x4
 3296 059f 04 00 00 00 	.4byte .LCFI62-.LCFI60
 3297 05a3 86          	.byte 0x86
 3298 05a4 05          	.uleb128 0x5
 3299 05a5 84          	.byte 0x84
 3300 05a6 03          	.uleb128 0x3
 3301 05a7 04          	.byte 0x4
 3302 05a8 0C 00 00 00 	.4byte .LCFI63-.LCFI62
 3303 05ac 13          	.byte 0x13
 3304 05ad 76          	.sleb128 -10
 3305 05ae 0D          	.byte 0xd
 3306 05af 0E          	.uleb128 0xe
MPLAB XC16 ASSEMBLY Listing:   			page 73


 3307 05b0 8E          	.byte 0x8e
 3308 05b1 09          	.uleb128 0x9
 3309                 	.align 4
 3310                 	.LEFDE96:
 3311                 	.LSFDE98:
 3312 05b2 2E 00 00 00 	.4byte .LEFDE98-.LASFDE98
 3313                 	.LASFDE98:
 3314 05b6 00 00 00 00 	.4byte .Lframe0
 3315 05ba 00 00 00 00 	.4byte .LFB49
 3316 05be 3A 00 00 00 	.4byte .LFE49-.LFB49
 3317 05c2 04          	.byte 0x4
 3318 05c3 06 00 00 00 	.4byte .LCFI65-.LFB49
 3319 05c7 13          	.byte 0x13
 3320 05c8 7B          	.sleb128 -5
 3321 05c9 04          	.byte 0x4
 3322 05ca 02 00 00 00 	.4byte .LCFI66-.LCFI65
 3323 05ce 13          	.byte 0x13
 3324 05cf 79          	.sleb128 -7
 3325 05d0 04          	.byte 0x4
 3326 05d1 04 00 00 00 	.4byte .LCFI68-.LCFI66
 3327 05d5 86          	.byte 0x86
 3328 05d6 05          	.uleb128 0x5
 3329 05d7 84          	.byte 0x84
 3330 05d8 03          	.uleb128 0x3
 3331 05d9 04          	.byte 0x4
 3332 05da 0C 00 00 00 	.4byte .LCFI69-.LCFI68
 3333 05de 13          	.byte 0x13
 3334 05df 76          	.sleb128 -10
 3335 05e0 0D          	.byte 0xd
 3336 05e1 0E          	.uleb128 0xe
 3337 05e2 8E          	.byte 0x8e
 3338 05e3 09          	.uleb128 0x9
 3339                 	.align 4
 3340                 	.LEFDE98:
 3341                 	.LSFDE100:
 3342 05e4 2E 00 00 00 	.4byte .LEFDE100-.LASFDE100
 3343                 	.LASFDE100:
 3344 05e8 00 00 00 00 	.4byte .Lframe0
 3345 05ec 00 00 00 00 	.4byte .LFB50
 3346 05f0 3A 00 00 00 	.4byte .LFE50-.LFB50
 3347 05f4 04          	.byte 0x4
 3348 05f5 06 00 00 00 	.4byte .LCFI71-.LFB50
 3349 05f9 13          	.byte 0x13
 3350 05fa 7B          	.sleb128 -5
 3351 05fb 04          	.byte 0x4
 3352 05fc 02 00 00 00 	.4byte .LCFI72-.LCFI71
 3353 0600 13          	.byte 0x13
 3354 0601 79          	.sleb128 -7
 3355 0602 04          	.byte 0x4
 3356 0603 04 00 00 00 	.4byte .LCFI74-.LCFI72
 3357 0607 86          	.byte 0x86
 3358 0608 05          	.uleb128 0x5
 3359 0609 84          	.byte 0x84
 3360 060a 03          	.uleb128 0x3
 3361 060b 04          	.byte 0x4
 3362 060c 0C 00 00 00 	.4byte .LCFI75-.LCFI74
 3363 0610 13          	.byte 0x13
MPLAB XC16 ASSEMBLY Listing:   			page 74


 3364 0611 76          	.sleb128 -10
 3365 0612 0D          	.byte 0xd
 3366 0613 0E          	.uleb128 0xe
 3367 0614 8E          	.byte 0x8e
 3368 0615 09          	.uleb128 0x9
 3369                 	.align 4
 3370                 	.LEFDE100:
 3371                 	.LSFDE102:
 3372 0616 2E 00 00 00 	.4byte .LEFDE102-.LASFDE102
 3373                 	.LASFDE102:
 3374 061a 00 00 00 00 	.4byte .Lframe0
 3375 061e 00 00 00 00 	.4byte .LFB51
 3376 0622 3A 00 00 00 	.4byte .LFE51-.LFB51
 3377 0626 04          	.byte 0x4
 3378 0627 06 00 00 00 	.4byte .LCFI77-.LFB51
 3379 062b 13          	.byte 0x13
 3380 062c 7B          	.sleb128 -5
 3381 062d 04          	.byte 0x4
 3382 062e 02 00 00 00 	.4byte .LCFI78-.LCFI77
 3383 0632 13          	.byte 0x13
 3384 0633 79          	.sleb128 -7
 3385 0634 04          	.byte 0x4
 3386 0635 04 00 00 00 	.4byte .LCFI80-.LCFI78
 3387 0639 86          	.byte 0x86
 3388 063a 05          	.uleb128 0x5
 3389 063b 84          	.byte 0x84
 3390 063c 03          	.uleb128 0x3
 3391 063d 04          	.byte 0x4
 3392 063e 0C 00 00 00 	.4byte .LCFI81-.LCFI80
 3393 0642 13          	.byte 0x13
 3394 0643 76          	.sleb128 -10
 3395 0644 0D          	.byte 0xd
 3396 0645 0E          	.uleb128 0xe
 3397 0646 8E          	.byte 0x8e
 3398 0647 09          	.uleb128 0x9
 3399                 	.align 4
 3400                 	.LEFDE102:
 3401                 	.LSFDE104:
 3402 0648 2E 00 00 00 	.4byte .LEFDE104-.LASFDE104
 3403                 	.LASFDE104:
 3404 064c 00 00 00 00 	.4byte .Lframe0
 3405 0650 00 00 00 00 	.4byte .LFB52
 3406 0654 3A 00 00 00 	.4byte .LFE52-.LFB52
 3407 0658 04          	.byte 0x4
 3408 0659 06 00 00 00 	.4byte .LCFI83-.LFB52
 3409 065d 13          	.byte 0x13
 3410 065e 7B          	.sleb128 -5
 3411 065f 04          	.byte 0x4
 3412 0660 02 00 00 00 	.4byte .LCFI84-.LCFI83
 3413 0664 13          	.byte 0x13
 3414 0665 79          	.sleb128 -7
 3415 0666 04          	.byte 0x4
 3416 0667 04 00 00 00 	.4byte .LCFI86-.LCFI84
 3417 066b 86          	.byte 0x86
 3418 066c 05          	.uleb128 0x5
 3419 066d 84          	.byte 0x84
 3420 066e 03          	.uleb128 0x3
MPLAB XC16 ASSEMBLY Listing:   			page 75


 3421 066f 04          	.byte 0x4
 3422 0670 0C 00 00 00 	.4byte .LCFI87-.LCFI86
 3423 0674 13          	.byte 0x13
 3424 0675 76          	.sleb128 -10
 3425 0676 0D          	.byte 0xd
 3426 0677 0E          	.uleb128 0xe
 3427 0678 8E          	.byte 0x8e
 3428 0679 09          	.uleb128 0x9
 3429                 	.align 4
 3430                 	.LEFDE104:
 3431                 	.LSFDE106:
 3432 067a 2E 00 00 00 	.4byte .LEFDE106-.LASFDE106
 3433                 	.LASFDE106:
 3434 067e 00 00 00 00 	.4byte .Lframe0
 3435 0682 00 00 00 00 	.4byte .LFB53
 3436 0686 3A 00 00 00 	.4byte .LFE53-.LFB53
 3437 068a 04          	.byte 0x4
 3438 068b 06 00 00 00 	.4byte .LCFI89-.LFB53
 3439 068f 13          	.byte 0x13
 3440 0690 7B          	.sleb128 -5
 3441 0691 04          	.byte 0x4
 3442 0692 02 00 00 00 	.4byte .LCFI90-.LCFI89
 3443 0696 13          	.byte 0x13
 3444 0697 79          	.sleb128 -7
 3445 0698 04          	.byte 0x4
 3446 0699 04 00 00 00 	.4byte .LCFI92-.LCFI90
 3447 069d 86          	.byte 0x86
 3448 069e 05          	.uleb128 0x5
 3449 069f 84          	.byte 0x84
 3450 06a0 03          	.uleb128 0x3
 3451 06a1 04          	.byte 0x4
 3452 06a2 0C 00 00 00 	.4byte .LCFI93-.LCFI92
 3453 06a6 13          	.byte 0x13
 3454 06a7 76          	.sleb128 -10
 3455 06a8 0D          	.byte 0xd
 3456 06a9 0E          	.uleb128 0xe
 3457 06aa 8E          	.byte 0x8e
 3458 06ab 09          	.uleb128 0x9
 3459                 	.align 4
 3460                 	.LEFDE106:
 3461                 	.LSFDE108:
 3462 06ac 2E 00 00 00 	.4byte .LEFDE108-.LASFDE108
 3463                 	.LASFDE108:
 3464 06b0 00 00 00 00 	.4byte .Lframe0
 3465 06b4 00 00 00 00 	.4byte .LFB54
 3466 06b8 3A 00 00 00 	.4byte .LFE54-.LFB54
 3467 06bc 04          	.byte 0x4
 3468 06bd 06 00 00 00 	.4byte .LCFI95-.LFB54
 3469 06c1 13          	.byte 0x13
 3470 06c2 7B          	.sleb128 -5
 3471 06c3 04          	.byte 0x4
 3472 06c4 02 00 00 00 	.4byte .LCFI96-.LCFI95
 3473 06c8 13          	.byte 0x13
 3474 06c9 79          	.sleb128 -7
 3475 06ca 04          	.byte 0x4
 3476 06cb 04 00 00 00 	.4byte .LCFI98-.LCFI96
 3477 06cf 86          	.byte 0x86
MPLAB XC16 ASSEMBLY Listing:   			page 76


 3478 06d0 05          	.uleb128 0x5
 3479 06d1 84          	.byte 0x84
 3480 06d2 03          	.uleb128 0x3
 3481 06d3 04          	.byte 0x4
 3482 06d4 0C 00 00 00 	.4byte .LCFI99-.LCFI98
 3483 06d8 13          	.byte 0x13
 3484 06d9 76          	.sleb128 -10
 3485 06da 0D          	.byte 0xd
 3486 06db 0E          	.uleb128 0xe
 3487 06dc 8E          	.byte 0x8e
 3488 06dd 09          	.uleb128 0x9
 3489                 	.align 4
 3490                 	.LEFDE108:
 3491                 	.section .text,code
 3492              	.Letext0:
 3493              	.file 2 "/opt/microchip/xc16/v1.36/bin/bin/../../support/dsPIC33E/h/p33EP256MU806.h"
 3494              	.file 3 "/opt/microchip/xc16/v1.36/bin/bin/../../include/lega-c/stdint.h"
 3495              	.section .debug_info,info
 3496 0000 8C 31 00 00 	.4byte 0x318c
 3497 0004 02 00       	.2byte 0x2
 3498 0006 00 00 00 00 	.4byte .Ldebug_abbrev0
 3499 000a 04          	.byte 0x4
 3500 000b 01          	.uleb128 0x1
 3501 000c 47 4E 55 20 	.asciz "GNU C 4.5.1 (XC16, Microchip v1.36) (A) Build date: Dec  5 2018"
 3501      43 20 34 2E 
 3501      35 2E 31 20 
 3501      28 58 43 31 
 3501      36 2C 20 4D 
 3501      69 63 72 6F 
 3501      63 68 69 70 
 3501      20 76 31 2E 
 3501      33 36 29 20 
 3502 004c 01          	.byte 0x1
 3503 004d 6C 69 62 2F 	.asciz "lib/lib_pic33e/timer.c"
 3503      6C 69 62 5F 
 3503      70 69 63 33 
 3503      33 65 2F 74 
 3503      69 6D 65 72 
 3503      2E 63 00 
 3504 0064 2F 68 6F 6D 	.asciz "/home/user/Documents/FST/Programming/project_sub-zero"
 3504      65 2F 75 73 
 3504      65 72 2F 44 
 3504      6F 63 75 6D 
 3504      65 6E 74 73 
 3504      2F 46 53 54 
 3504      2F 50 72 6F 
 3504      67 72 61 6D 
 3504      6D 69 6E 67 
 3505 009a 00 00 00 00 	.4byte .Ltext0
 3506 009e 00 00 00 00 	.4byte .Letext0
 3507 00a2 00 00 00 00 	.4byte .Ldebug_line0
 3508 00a6 02          	.uleb128 0x2
 3509 00a7 01          	.byte 0x1
 3510 00a8 06          	.byte 0x6
 3511 00a9 73 69 67 6E 	.asciz "signed char"
 3511      65 64 20 63 
 3511      68 61 72 00 
MPLAB XC16 ASSEMBLY Listing:   			page 77


 3512 00b5 02          	.uleb128 0x2
 3513 00b6 02          	.byte 0x2
 3514 00b7 05          	.byte 0x5
 3515 00b8 69 6E 74 00 	.asciz "int"
 3516 00bc 02          	.uleb128 0x2
 3517 00bd 04          	.byte 0x4
 3518 00be 05          	.byte 0x5
 3519 00bf 6C 6F 6E 67 	.asciz "long int"
 3519      20 69 6E 74 
 3519      00 
 3520 00c8 02          	.uleb128 0x2
 3521 00c9 08          	.byte 0x8
 3522 00ca 05          	.byte 0x5
 3523 00cb 6C 6F 6E 67 	.asciz "long long int"
 3523      20 6C 6F 6E 
 3523      67 20 69 6E 
 3523      74 00 
 3524 00d9 02          	.uleb128 0x2
 3525 00da 01          	.byte 0x1
 3526 00db 08          	.byte 0x8
 3527 00dc 75 6E 73 69 	.asciz "unsigned char"
 3527      67 6E 65 64 
 3527      20 63 68 61 
 3527      72 00 
 3528 00ea 03          	.uleb128 0x3
 3529 00eb 75 69 6E 74 	.asciz "uint16_t"
 3529      31 36 5F 74 
 3529      00 
 3530 00f4 03          	.byte 0x3
 3531 00f5 31          	.byte 0x31
 3532 00f6 FA 00 00 00 	.4byte 0xfa
 3533 00fa 02          	.uleb128 0x2
 3534 00fb 02          	.byte 0x2
 3535 00fc 07          	.byte 0x7
 3536 00fd 75 6E 73 69 	.asciz "unsigned int"
 3536      67 6E 65 64 
 3536      20 69 6E 74 
 3536      00 
 3537 010a 02          	.uleb128 0x2
 3538 010b 04          	.byte 0x4
 3539 010c 07          	.byte 0x7
 3540 010d 6C 6F 6E 67 	.asciz "long unsigned int"
 3540      20 75 6E 73 
 3540      69 67 6E 65 
 3540      64 20 69 6E 
 3540      74 00 
 3541 011f 02          	.uleb128 0x2
 3542 0120 08          	.byte 0x8
 3543 0121 07          	.byte 0x7
 3544 0122 6C 6F 6E 67 	.asciz "long long unsigned int"
 3544      20 6C 6F 6E 
 3544      67 20 75 6E 
 3544      73 69 67 6E 
 3544      65 64 20 69 
 3544      6E 74 00 
 3545 0139 04          	.uleb128 0x4
 3546 013a 02          	.byte 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 78


 3547 013b 02          	.byte 0x2
 3548 013c 17 01       	.2byte 0x117
 3549 013e B1 01 00 00 	.4byte 0x1b1
 3550 0142 05          	.uleb128 0x5
 3551 0143 54 43 53 00 	.asciz "TCS"
 3552 0147 02          	.byte 0x2
 3553 0148 19 01       	.2byte 0x119
 3554 014a EA 00 00 00 	.4byte 0xea
 3555 014e 02          	.byte 0x2
 3556 014f 01          	.byte 0x1
 3557 0150 0E          	.byte 0xe
 3558 0151 02          	.byte 0x2
 3559 0152 23          	.byte 0x23
 3560 0153 00          	.uleb128 0x0
 3561 0154 05          	.uleb128 0x5
 3562 0155 54 53 59 4E 	.asciz "TSYNC"
 3562      43 00 
 3563 015b 02          	.byte 0x2
 3564 015c 1A 01       	.2byte 0x11a
 3565 015e EA 00 00 00 	.4byte 0xea
 3566 0162 02          	.byte 0x2
 3567 0163 01          	.byte 0x1
 3568 0164 0D          	.byte 0xd
 3569 0165 02          	.byte 0x2
 3570 0166 23          	.byte 0x23
 3571 0167 00          	.uleb128 0x0
 3572 0168 06          	.uleb128 0x6
 3573 0169 00 00 00 00 	.4byte .LASF0
 3574 016d 02          	.byte 0x2
 3575 016e 1C 01       	.2byte 0x11c
 3576 0170 EA 00 00 00 	.4byte 0xea
 3577 0174 02          	.byte 0x2
 3578 0175 02          	.byte 0x2
 3579 0176 0A          	.byte 0xa
 3580 0177 02          	.byte 0x2
 3581 0178 23          	.byte 0x23
 3582 0179 00          	.uleb128 0x0
 3583 017a 06          	.uleb128 0x6
 3584 017b 00 00 00 00 	.4byte .LASF1
 3585 017f 02          	.byte 0x2
 3586 0180 1D 01       	.2byte 0x11d
 3587 0182 EA 00 00 00 	.4byte 0xea
 3588 0186 02          	.byte 0x2
 3589 0187 01          	.byte 0x1
 3590 0188 09          	.byte 0x9
 3591 0189 02          	.byte 0x2
 3592 018a 23          	.byte 0x23
 3593 018b 00          	.uleb128 0x0
 3594 018c 06          	.uleb128 0x6
 3595 018d 00 00 00 00 	.4byte .LASF2
 3596 0191 02          	.byte 0x2
 3597 0192 1F 01       	.2byte 0x11f
 3598 0194 EA 00 00 00 	.4byte 0xea
 3599 0198 02          	.byte 0x2
 3600 0199 01          	.byte 0x1
 3601 019a 02          	.byte 0x2
 3602 019b 02          	.byte 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 79


 3603 019c 23          	.byte 0x23
 3604 019d 00          	.uleb128 0x0
 3605 019e 05          	.uleb128 0x5
 3606 019f 54 4F 4E 00 	.asciz "TON"
 3607 01a3 02          	.byte 0x2
 3608 01a4 21 01       	.2byte 0x121
 3609 01a6 EA 00 00 00 	.4byte 0xea
 3610 01aa 02          	.byte 0x2
 3611 01ab 01          	.byte 0x1
 3612 01ac 10          	.byte 0x10
 3613 01ad 02          	.byte 0x2
 3614 01ae 23          	.byte 0x23
 3615 01af 00          	.uleb128 0x0
 3616 01b0 00          	.byte 0x0
 3617 01b1 04          	.uleb128 0x4
 3618 01b2 02          	.byte 0x2
 3619 01b3 02          	.byte 0x2
 3620 01b4 23 01       	.2byte 0x123
 3621 01b6 DF 01 00 00 	.4byte 0x1df
 3622 01ba 06          	.uleb128 0x6
 3623 01bb 00 00 00 00 	.4byte .LASF3
 3624 01bf 02          	.byte 0x2
 3625 01c0 25 01       	.2byte 0x125
 3626 01c2 EA 00 00 00 	.4byte 0xea
 3627 01c6 02          	.byte 0x2
 3628 01c7 01          	.byte 0x1
 3629 01c8 0B          	.byte 0xb
 3630 01c9 02          	.byte 0x2
 3631 01ca 23          	.byte 0x23
 3632 01cb 00          	.uleb128 0x0
 3633 01cc 06          	.uleb128 0x6
 3634 01cd 00 00 00 00 	.4byte .LASF4
 3635 01d1 02          	.byte 0x2
 3636 01d2 26 01       	.2byte 0x126
 3637 01d4 EA 00 00 00 	.4byte 0xea
 3638 01d8 02          	.byte 0x2
 3639 01d9 01          	.byte 0x1
 3640 01da 0A          	.byte 0xa
 3641 01db 02          	.byte 0x2
 3642 01dc 23          	.byte 0x23
 3643 01dd 00          	.uleb128 0x0
 3644 01de 00          	.byte 0x0
 3645 01df 07          	.uleb128 0x7
 3646 01e0 02          	.byte 0x2
 3647 01e1 02          	.byte 0x2
 3648 01e2 16 01       	.2byte 0x116
 3649 01e4 F3 01 00 00 	.4byte 0x1f3
 3650 01e8 08          	.uleb128 0x8
 3651 01e9 39 01 00 00 	.4byte 0x139
 3652 01ed 08          	.uleb128 0x8
 3653 01ee B1 01 00 00 	.4byte 0x1b1
 3654 01f2 00          	.byte 0x0
 3655 01f3 09          	.uleb128 0x9
 3656 01f4 74 61 67 54 	.asciz "tagT1CONBITS"
 3656      31 43 4F 4E 
 3656      42 49 54 53 
 3656      00 
MPLAB XC16 ASSEMBLY Listing:   			page 80


 3657 0201 02          	.byte 0x2
 3658 0202 02          	.byte 0x2
 3659 0203 15 01       	.2byte 0x115
 3660 0205 12 02 00 00 	.4byte 0x212
 3661 0209 0A          	.uleb128 0xa
 3662 020a DF 01 00 00 	.4byte 0x1df
 3663 020e 02          	.byte 0x2
 3664 020f 23          	.byte 0x23
 3665 0210 00          	.uleb128 0x0
 3666 0211 00          	.byte 0x0
 3667 0212 0B          	.uleb128 0xb
 3668 0213 54 31 43 4F 	.asciz "T1CONBITS"
 3668      4E 42 49 54 
 3668      53 00 
 3669 021d 02          	.byte 0x2
 3670 021e 29 01       	.2byte 0x129
 3671 0220 F3 01 00 00 	.4byte 0x1f3
 3672 0224 04          	.uleb128 0x4
 3673 0225 02          	.byte 0x2
 3674 0226 02          	.byte 0x2
 3675 0227 3A 01       	.2byte 0x13a
 3676 0229 9A 02 00 00 	.4byte 0x29a
 3677 022d 05          	.uleb128 0x5
 3678 022e 54 43 53 00 	.asciz "TCS"
 3679 0232 02          	.byte 0x2
 3680 0233 3C 01       	.2byte 0x13c
 3681 0235 EA 00 00 00 	.4byte 0xea
 3682 0239 02          	.byte 0x2
 3683 023a 01          	.byte 0x1
 3684 023b 0E          	.byte 0xe
 3685 023c 02          	.byte 0x2
 3686 023d 23          	.byte 0x23
 3687 023e 00          	.uleb128 0x0
 3688 023f 05          	.uleb128 0x5
 3689 0240 54 33 32 00 	.asciz "T32"
 3690 0244 02          	.byte 0x2
 3691 0245 3E 01       	.2byte 0x13e
 3692 0247 EA 00 00 00 	.4byte 0xea
 3693 024b 02          	.byte 0x2
 3694 024c 01          	.byte 0x1
 3695 024d 0C          	.byte 0xc
 3696 024e 02          	.byte 0x2
 3697 024f 23          	.byte 0x23
 3698 0250 00          	.uleb128 0x0
 3699 0251 06          	.uleb128 0x6
 3700 0252 00 00 00 00 	.4byte .LASF0
 3701 0256 02          	.byte 0x2
 3702 0257 3F 01       	.2byte 0x13f
 3703 0259 EA 00 00 00 	.4byte 0xea
 3704 025d 02          	.byte 0x2
 3705 025e 02          	.byte 0x2
 3706 025f 0A          	.byte 0xa
 3707 0260 02          	.byte 0x2
 3708 0261 23          	.byte 0x23
 3709 0262 00          	.uleb128 0x0
 3710 0263 06          	.uleb128 0x6
 3711 0264 00 00 00 00 	.4byte .LASF1
MPLAB XC16 ASSEMBLY Listing:   			page 81


 3712 0268 02          	.byte 0x2
 3713 0269 40 01       	.2byte 0x140
 3714 026b EA 00 00 00 	.4byte 0xea
 3715 026f 02          	.byte 0x2
 3716 0270 01          	.byte 0x1
 3717 0271 09          	.byte 0x9
 3718 0272 02          	.byte 0x2
 3719 0273 23          	.byte 0x23
 3720 0274 00          	.uleb128 0x0
 3721 0275 06          	.uleb128 0x6
 3722 0276 00 00 00 00 	.4byte .LASF2
 3723 027a 02          	.byte 0x2
 3724 027b 42 01       	.2byte 0x142
 3725 027d EA 00 00 00 	.4byte 0xea
 3726 0281 02          	.byte 0x2
 3727 0282 01          	.byte 0x1
 3728 0283 02          	.byte 0x2
 3729 0284 02          	.byte 0x2
 3730 0285 23          	.byte 0x23
 3731 0286 00          	.uleb128 0x0
 3732 0287 05          	.uleb128 0x5
 3733 0288 54 4F 4E 00 	.asciz "TON"
 3734 028c 02          	.byte 0x2
 3735 028d 44 01       	.2byte 0x144
 3736 028f EA 00 00 00 	.4byte 0xea
 3737 0293 02          	.byte 0x2
 3738 0294 01          	.byte 0x1
 3739 0295 10          	.byte 0x10
 3740 0296 02          	.byte 0x2
 3741 0297 23          	.byte 0x23
 3742 0298 00          	.uleb128 0x0
 3743 0299 00          	.byte 0x0
 3744 029a 04          	.uleb128 0x4
 3745 029b 02          	.byte 0x2
 3746 029c 02          	.byte 0x2
 3747 029d 46 01       	.2byte 0x146
 3748 029f C8 02 00 00 	.4byte 0x2c8
 3749 02a3 06          	.uleb128 0x6
 3750 02a4 00 00 00 00 	.4byte .LASF3
 3751 02a8 02          	.byte 0x2
 3752 02a9 48 01       	.2byte 0x148
 3753 02ab EA 00 00 00 	.4byte 0xea
 3754 02af 02          	.byte 0x2
 3755 02b0 01          	.byte 0x1
 3756 02b1 0B          	.byte 0xb
 3757 02b2 02          	.byte 0x2
 3758 02b3 23          	.byte 0x23
 3759 02b4 00          	.uleb128 0x0
 3760 02b5 06          	.uleb128 0x6
 3761 02b6 00 00 00 00 	.4byte .LASF4
 3762 02ba 02          	.byte 0x2
 3763 02bb 49 01       	.2byte 0x149
 3764 02bd EA 00 00 00 	.4byte 0xea
 3765 02c1 02          	.byte 0x2
 3766 02c2 01          	.byte 0x1
 3767 02c3 0A          	.byte 0xa
 3768 02c4 02          	.byte 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 82


 3769 02c5 23          	.byte 0x23
 3770 02c6 00          	.uleb128 0x0
 3771 02c7 00          	.byte 0x0
 3772 02c8 07          	.uleb128 0x7
 3773 02c9 02          	.byte 0x2
 3774 02ca 02          	.byte 0x2
 3775 02cb 39 01       	.2byte 0x139
 3776 02cd DC 02 00 00 	.4byte 0x2dc
 3777 02d1 08          	.uleb128 0x8
 3778 02d2 24 02 00 00 	.4byte 0x224
 3779 02d6 08          	.uleb128 0x8
 3780 02d7 9A 02 00 00 	.4byte 0x29a
 3781 02db 00          	.byte 0x0
 3782 02dc 09          	.uleb128 0x9
 3783 02dd 74 61 67 54 	.asciz "tagT2CONBITS"
 3783      32 43 4F 4E 
 3783      42 49 54 53 
 3783      00 
 3784 02ea 02          	.byte 0x2
 3785 02eb 02          	.byte 0x2
 3786 02ec 38 01       	.2byte 0x138
 3787 02ee FB 02 00 00 	.4byte 0x2fb
 3788 02f2 0A          	.uleb128 0xa
 3789 02f3 C8 02 00 00 	.4byte 0x2c8
 3790 02f7 02          	.byte 0x2
 3791 02f8 23          	.byte 0x23
 3792 02f9 00          	.uleb128 0x0
 3793 02fa 00          	.byte 0x0
 3794 02fb 0B          	.uleb128 0xb
 3795 02fc 54 32 43 4F 	.asciz "T2CONBITS"
 3795      4E 42 49 54 
 3795      53 00 
 3796 0306 02          	.byte 0x2
 3797 0307 4C 01       	.2byte 0x14c
 3798 0309 DC 02 00 00 	.4byte 0x2dc
 3799 030d 04          	.uleb128 0x4
 3800 030e 02          	.byte 0x2
 3801 030f 02          	.byte 0x2
 3802 0310 53 01       	.2byte 0x153
 3803 0312 71 03 00 00 	.4byte 0x371
 3804 0316 05          	.uleb128 0x5
 3805 0317 54 43 53 00 	.asciz "TCS"
 3806 031b 02          	.byte 0x2
 3807 031c 55 01       	.2byte 0x155
 3808 031e EA 00 00 00 	.4byte 0xea
 3809 0322 02          	.byte 0x2
 3810 0323 01          	.byte 0x1
 3811 0324 0E          	.byte 0xe
 3812 0325 02          	.byte 0x2
 3813 0326 23          	.byte 0x23
 3814 0327 00          	.uleb128 0x0
 3815 0328 06          	.uleb128 0x6
 3816 0329 00 00 00 00 	.4byte .LASF0
 3817 032d 02          	.byte 0x2
 3818 032e 57 01       	.2byte 0x157
 3819 0330 EA 00 00 00 	.4byte 0xea
 3820 0334 02          	.byte 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 83


 3821 0335 02          	.byte 0x2
 3822 0336 0A          	.byte 0xa
 3823 0337 02          	.byte 0x2
 3824 0338 23          	.byte 0x23
 3825 0339 00          	.uleb128 0x0
 3826 033a 06          	.uleb128 0x6
 3827 033b 00 00 00 00 	.4byte .LASF1
 3828 033f 02          	.byte 0x2
 3829 0340 58 01       	.2byte 0x158
 3830 0342 EA 00 00 00 	.4byte 0xea
 3831 0346 02          	.byte 0x2
 3832 0347 01          	.byte 0x1
 3833 0348 09          	.byte 0x9
 3834 0349 02          	.byte 0x2
 3835 034a 23          	.byte 0x23
 3836 034b 00          	.uleb128 0x0
 3837 034c 06          	.uleb128 0x6
 3838 034d 00 00 00 00 	.4byte .LASF2
 3839 0351 02          	.byte 0x2
 3840 0352 5A 01       	.2byte 0x15a
 3841 0354 EA 00 00 00 	.4byte 0xea
 3842 0358 02          	.byte 0x2
 3843 0359 01          	.byte 0x1
 3844 035a 02          	.byte 0x2
 3845 035b 02          	.byte 0x2
 3846 035c 23          	.byte 0x23
 3847 035d 00          	.uleb128 0x0
 3848 035e 05          	.uleb128 0x5
 3849 035f 54 4F 4E 00 	.asciz "TON"
 3850 0363 02          	.byte 0x2
 3851 0364 5C 01       	.2byte 0x15c
 3852 0366 EA 00 00 00 	.4byte 0xea
 3853 036a 02          	.byte 0x2
 3854 036b 01          	.byte 0x1
 3855 036c 10          	.byte 0x10
 3856 036d 02          	.byte 0x2
 3857 036e 23          	.byte 0x23
 3858 036f 00          	.uleb128 0x0
 3859 0370 00          	.byte 0x0
 3860 0371 04          	.uleb128 0x4
 3861 0372 02          	.byte 0x2
 3862 0373 02          	.byte 0x2
 3863 0374 5E 01       	.2byte 0x15e
 3864 0376 9F 03 00 00 	.4byte 0x39f
 3865 037a 06          	.uleb128 0x6
 3866 037b 00 00 00 00 	.4byte .LASF3
 3867 037f 02          	.byte 0x2
 3868 0380 60 01       	.2byte 0x160
 3869 0382 EA 00 00 00 	.4byte 0xea
 3870 0386 02          	.byte 0x2
 3871 0387 01          	.byte 0x1
 3872 0388 0B          	.byte 0xb
 3873 0389 02          	.byte 0x2
 3874 038a 23          	.byte 0x23
 3875 038b 00          	.uleb128 0x0
 3876 038c 06          	.uleb128 0x6
 3877 038d 00 00 00 00 	.4byte .LASF4
MPLAB XC16 ASSEMBLY Listing:   			page 84


 3878 0391 02          	.byte 0x2
 3879 0392 61 01       	.2byte 0x161
 3880 0394 EA 00 00 00 	.4byte 0xea
 3881 0398 02          	.byte 0x2
 3882 0399 01          	.byte 0x1
 3883 039a 0A          	.byte 0xa
 3884 039b 02          	.byte 0x2
 3885 039c 23          	.byte 0x23
 3886 039d 00          	.uleb128 0x0
 3887 039e 00          	.byte 0x0
 3888 039f 07          	.uleb128 0x7
 3889 03a0 02          	.byte 0x2
 3890 03a1 02          	.byte 0x2
 3891 03a2 52 01       	.2byte 0x152
 3892 03a4 B3 03 00 00 	.4byte 0x3b3
 3893 03a8 08          	.uleb128 0x8
 3894 03a9 0D 03 00 00 	.4byte 0x30d
 3895 03ad 08          	.uleb128 0x8
 3896 03ae 71 03 00 00 	.4byte 0x371
 3897 03b2 00          	.byte 0x0
 3898 03b3 09          	.uleb128 0x9
 3899 03b4 74 61 67 54 	.asciz "tagT3CONBITS"
 3899      33 43 4F 4E 
 3899      42 49 54 53 
 3899      00 
 3900 03c1 02          	.byte 0x2
 3901 03c2 02          	.byte 0x2
 3902 03c3 51 01       	.2byte 0x151
 3903 03c5 D2 03 00 00 	.4byte 0x3d2
 3904 03c9 0A          	.uleb128 0xa
 3905 03ca 9F 03 00 00 	.4byte 0x39f
 3906 03ce 02          	.byte 0x2
 3907 03cf 23          	.byte 0x23
 3908 03d0 00          	.uleb128 0x0
 3909 03d1 00          	.byte 0x0
 3910 03d2 0B          	.uleb128 0xb
 3911 03d3 54 33 43 4F 	.asciz "T3CONBITS"
 3911      4E 42 49 54 
 3911      53 00 
 3912 03dd 02          	.byte 0x2
 3913 03de 64 01       	.2byte 0x164
 3914 03e0 B3 03 00 00 	.4byte 0x3b3
 3915 03e4 04          	.uleb128 0x4
 3916 03e5 02          	.byte 0x2
 3917 03e6 02          	.byte 0x2
 3918 03e7 75 01       	.2byte 0x175
 3919 03e9 5A 04 00 00 	.4byte 0x45a
 3920 03ed 05          	.uleb128 0x5
 3921 03ee 54 43 53 00 	.asciz "TCS"
 3922 03f2 02          	.byte 0x2
 3923 03f3 77 01       	.2byte 0x177
 3924 03f5 EA 00 00 00 	.4byte 0xea
 3925 03f9 02          	.byte 0x2
 3926 03fa 01          	.byte 0x1
 3927 03fb 0E          	.byte 0xe
 3928 03fc 02          	.byte 0x2
 3929 03fd 23          	.byte 0x23
MPLAB XC16 ASSEMBLY Listing:   			page 85


 3930 03fe 00          	.uleb128 0x0
 3931 03ff 05          	.uleb128 0x5
 3932 0400 54 33 32 00 	.asciz "T32"
 3933 0404 02          	.byte 0x2
 3934 0405 79 01       	.2byte 0x179
 3935 0407 EA 00 00 00 	.4byte 0xea
 3936 040b 02          	.byte 0x2
 3937 040c 01          	.byte 0x1
 3938 040d 0C          	.byte 0xc
 3939 040e 02          	.byte 0x2
 3940 040f 23          	.byte 0x23
 3941 0410 00          	.uleb128 0x0
 3942 0411 06          	.uleb128 0x6
 3943 0412 00 00 00 00 	.4byte .LASF0
 3944 0416 02          	.byte 0x2
 3945 0417 7A 01       	.2byte 0x17a
 3946 0419 EA 00 00 00 	.4byte 0xea
 3947 041d 02          	.byte 0x2
 3948 041e 02          	.byte 0x2
 3949 041f 0A          	.byte 0xa
 3950 0420 02          	.byte 0x2
 3951 0421 23          	.byte 0x23
 3952 0422 00          	.uleb128 0x0
 3953 0423 06          	.uleb128 0x6
 3954 0424 00 00 00 00 	.4byte .LASF1
 3955 0428 02          	.byte 0x2
 3956 0429 7B 01       	.2byte 0x17b
 3957 042b EA 00 00 00 	.4byte 0xea
 3958 042f 02          	.byte 0x2
 3959 0430 01          	.byte 0x1
 3960 0431 09          	.byte 0x9
 3961 0432 02          	.byte 0x2
 3962 0433 23          	.byte 0x23
 3963 0434 00          	.uleb128 0x0
 3964 0435 06          	.uleb128 0x6
 3965 0436 00 00 00 00 	.4byte .LASF2
 3966 043a 02          	.byte 0x2
 3967 043b 7D 01       	.2byte 0x17d
 3968 043d EA 00 00 00 	.4byte 0xea
 3969 0441 02          	.byte 0x2
 3970 0442 01          	.byte 0x1
 3971 0443 02          	.byte 0x2
 3972 0444 02          	.byte 0x2
 3973 0445 23          	.byte 0x23
 3974 0446 00          	.uleb128 0x0
 3975 0447 05          	.uleb128 0x5
 3976 0448 54 4F 4E 00 	.asciz "TON"
 3977 044c 02          	.byte 0x2
 3978 044d 7F 01       	.2byte 0x17f
 3979 044f EA 00 00 00 	.4byte 0xea
 3980 0453 02          	.byte 0x2
 3981 0454 01          	.byte 0x1
 3982 0455 10          	.byte 0x10
 3983 0456 02          	.byte 0x2
 3984 0457 23          	.byte 0x23
 3985 0458 00          	.uleb128 0x0
 3986 0459 00          	.byte 0x0
MPLAB XC16 ASSEMBLY Listing:   			page 86


 3987 045a 04          	.uleb128 0x4
 3988 045b 02          	.byte 0x2
 3989 045c 02          	.byte 0x2
 3990 045d 81 01       	.2byte 0x181
 3991 045f 88 04 00 00 	.4byte 0x488
 3992 0463 06          	.uleb128 0x6
 3993 0464 00 00 00 00 	.4byte .LASF3
 3994 0468 02          	.byte 0x2
 3995 0469 83 01       	.2byte 0x183
 3996 046b EA 00 00 00 	.4byte 0xea
 3997 046f 02          	.byte 0x2
 3998 0470 01          	.byte 0x1
 3999 0471 0B          	.byte 0xb
 4000 0472 02          	.byte 0x2
 4001 0473 23          	.byte 0x23
 4002 0474 00          	.uleb128 0x0
 4003 0475 06          	.uleb128 0x6
 4004 0476 00 00 00 00 	.4byte .LASF4
 4005 047a 02          	.byte 0x2
 4006 047b 84 01       	.2byte 0x184
 4007 047d EA 00 00 00 	.4byte 0xea
 4008 0481 02          	.byte 0x2
 4009 0482 01          	.byte 0x1
 4010 0483 0A          	.byte 0xa
 4011 0484 02          	.byte 0x2
 4012 0485 23          	.byte 0x23
 4013 0486 00          	.uleb128 0x0
 4014 0487 00          	.byte 0x0
 4015 0488 07          	.uleb128 0x7
 4016 0489 02          	.byte 0x2
 4017 048a 02          	.byte 0x2
 4018 048b 74 01       	.2byte 0x174
 4019 048d 9C 04 00 00 	.4byte 0x49c
 4020 0491 08          	.uleb128 0x8
 4021 0492 E4 03 00 00 	.4byte 0x3e4
 4022 0496 08          	.uleb128 0x8
 4023 0497 5A 04 00 00 	.4byte 0x45a
 4024 049b 00          	.byte 0x0
 4025 049c 09          	.uleb128 0x9
 4026 049d 74 61 67 54 	.asciz "tagT4CONBITS"
 4026      34 43 4F 4E 
 4026      42 49 54 53 
 4026      00 
 4027 04aa 02          	.byte 0x2
 4028 04ab 02          	.byte 0x2
 4029 04ac 73 01       	.2byte 0x173
 4030 04ae BB 04 00 00 	.4byte 0x4bb
 4031 04b2 0A          	.uleb128 0xa
 4032 04b3 88 04 00 00 	.4byte 0x488
 4033 04b7 02          	.byte 0x2
 4034 04b8 23          	.byte 0x23
 4035 04b9 00          	.uleb128 0x0
 4036 04ba 00          	.byte 0x0
 4037 04bb 0B          	.uleb128 0xb
 4038 04bc 54 34 43 4F 	.asciz "T4CONBITS"
 4038      4E 42 49 54 
 4038      53 00 
MPLAB XC16 ASSEMBLY Listing:   			page 87


 4039 04c6 02          	.byte 0x2
 4040 04c7 87 01       	.2byte 0x187
 4041 04c9 9C 04 00 00 	.4byte 0x49c
 4042 04cd 04          	.uleb128 0x4
 4043 04ce 02          	.byte 0x2
 4044 04cf 02          	.byte 0x2
 4045 04d0 8E 01       	.2byte 0x18e
 4046 04d2 31 05 00 00 	.4byte 0x531
 4047 04d6 05          	.uleb128 0x5
 4048 04d7 54 43 53 00 	.asciz "TCS"
 4049 04db 02          	.byte 0x2
 4050 04dc 90 01       	.2byte 0x190
 4051 04de EA 00 00 00 	.4byte 0xea
 4052 04e2 02          	.byte 0x2
 4053 04e3 01          	.byte 0x1
 4054 04e4 0E          	.byte 0xe
 4055 04e5 02          	.byte 0x2
 4056 04e6 23          	.byte 0x23
 4057 04e7 00          	.uleb128 0x0
 4058 04e8 06          	.uleb128 0x6
 4059 04e9 00 00 00 00 	.4byte .LASF0
 4060 04ed 02          	.byte 0x2
 4061 04ee 92 01       	.2byte 0x192
 4062 04f0 EA 00 00 00 	.4byte 0xea
 4063 04f4 02          	.byte 0x2
 4064 04f5 02          	.byte 0x2
 4065 04f6 0A          	.byte 0xa
 4066 04f7 02          	.byte 0x2
 4067 04f8 23          	.byte 0x23
 4068 04f9 00          	.uleb128 0x0
 4069 04fa 06          	.uleb128 0x6
 4070 04fb 00 00 00 00 	.4byte .LASF1
 4071 04ff 02          	.byte 0x2
 4072 0500 93 01       	.2byte 0x193
 4073 0502 EA 00 00 00 	.4byte 0xea
 4074 0506 02          	.byte 0x2
 4075 0507 01          	.byte 0x1
 4076 0508 09          	.byte 0x9
 4077 0509 02          	.byte 0x2
 4078 050a 23          	.byte 0x23
 4079 050b 00          	.uleb128 0x0
 4080 050c 06          	.uleb128 0x6
 4081 050d 00 00 00 00 	.4byte .LASF2
 4082 0511 02          	.byte 0x2
 4083 0512 95 01       	.2byte 0x195
 4084 0514 EA 00 00 00 	.4byte 0xea
 4085 0518 02          	.byte 0x2
 4086 0519 01          	.byte 0x1
 4087 051a 02          	.byte 0x2
 4088 051b 02          	.byte 0x2
 4089 051c 23          	.byte 0x23
 4090 051d 00          	.uleb128 0x0
 4091 051e 05          	.uleb128 0x5
 4092 051f 54 4F 4E 00 	.asciz "TON"
 4093 0523 02          	.byte 0x2
 4094 0524 97 01       	.2byte 0x197
 4095 0526 EA 00 00 00 	.4byte 0xea
MPLAB XC16 ASSEMBLY Listing:   			page 88


 4096 052a 02          	.byte 0x2
 4097 052b 01          	.byte 0x1
 4098 052c 10          	.byte 0x10
 4099 052d 02          	.byte 0x2
 4100 052e 23          	.byte 0x23
 4101 052f 00          	.uleb128 0x0
 4102 0530 00          	.byte 0x0
 4103 0531 04          	.uleb128 0x4
 4104 0532 02          	.byte 0x2
 4105 0533 02          	.byte 0x2
 4106 0534 99 01       	.2byte 0x199
 4107 0536 5F 05 00 00 	.4byte 0x55f
 4108 053a 06          	.uleb128 0x6
 4109 053b 00 00 00 00 	.4byte .LASF3
 4110 053f 02          	.byte 0x2
 4111 0540 9B 01       	.2byte 0x19b
 4112 0542 EA 00 00 00 	.4byte 0xea
 4113 0546 02          	.byte 0x2
 4114 0547 01          	.byte 0x1
 4115 0548 0B          	.byte 0xb
 4116 0549 02          	.byte 0x2
 4117 054a 23          	.byte 0x23
 4118 054b 00          	.uleb128 0x0
 4119 054c 06          	.uleb128 0x6
 4120 054d 00 00 00 00 	.4byte .LASF4
 4121 0551 02          	.byte 0x2
 4122 0552 9C 01       	.2byte 0x19c
 4123 0554 EA 00 00 00 	.4byte 0xea
 4124 0558 02          	.byte 0x2
 4125 0559 01          	.byte 0x1
 4126 055a 0A          	.byte 0xa
 4127 055b 02          	.byte 0x2
 4128 055c 23          	.byte 0x23
 4129 055d 00          	.uleb128 0x0
 4130 055e 00          	.byte 0x0
 4131 055f 07          	.uleb128 0x7
 4132 0560 02          	.byte 0x2
 4133 0561 02          	.byte 0x2
 4134 0562 8D 01       	.2byte 0x18d
 4135 0564 73 05 00 00 	.4byte 0x573
 4136 0568 08          	.uleb128 0x8
 4137 0569 CD 04 00 00 	.4byte 0x4cd
 4138 056d 08          	.uleb128 0x8
 4139 056e 31 05 00 00 	.4byte 0x531
 4140 0572 00          	.byte 0x0
 4141 0573 09          	.uleb128 0x9
 4142 0574 74 61 67 54 	.asciz "tagT5CONBITS"
 4142      35 43 4F 4E 
 4142      42 49 54 53 
 4142      00 
 4143 0581 02          	.byte 0x2
 4144 0582 02          	.byte 0x2
 4145 0583 8C 01       	.2byte 0x18c
 4146 0585 92 05 00 00 	.4byte 0x592
 4147 0589 0A          	.uleb128 0xa
 4148 058a 5F 05 00 00 	.4byte 0x55f
 4149 058e 02          	.byte 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 89


 4150 058f 23          	.byte 0x23
 4151 0590 00          	.uleb128 0x0
 4152 0591 00          	.byte 0x0
 4153 0592 0B          	.uleb128 0xb
 4154 0593 54 35 43 4F 	.asciz "T5CONBITS"
 4154      4E 42 49 54 
 4154      53 00 
 4155 059d 02          	.byte 0x2
 4156 059e 9F 01       	.2byte 0x19f
 4157 05a0 73 05 00 00 	.4byte 0x573
 4158 05a4 04          	.uleb128 0x4
 4159 05a5 02          	.byte 0x2
 4160 05a6 02          	.byte 0x2
 4161 05a7 B0 01       	.2byte 0x1b0
 4162 05a9 1A 06 00 00 	.4byte 0x61a
 4163 05ad 05          	.uleb128 0x5
 4164 05ae 54 43 53 00 	.asciz "TCS"
 4165 05b2 02          	.byte 0x2
 4166 05b3 B2 01       	.2byte 0x1b2
 4167 05b5 EA 00 00 00 	.4byte 0xea
 4168 05b9 02          	.byte 0x2
 4169 05ba 01          	.byte 0x1
 4170 05bb 0E          	.byte 0xe
 4171 05bc 02          	.byte 0x2
 4172 05bd 23          	.byte 0x23
 4173 05be 00          	.uleb128 0x0
 4174 05bf 05          	.uleb128 0x5
 4175 05c0 54 33 32 00 	.asciz "T32"
 4176 05c4 02          	.byte 0x2
 4177 05c5 B4 01       	.2byte 0x1b4
 4178 05c7 EA 00 00 00 	.4byte 0xea
 4179 05cb 02          	.byte 0x2
 4180 05cc 01          	.byte 0x1
 4181 05cd 0C          	.byte 0xc
 4182 05ce 02          	.byte 0x2
 4183 05cf 23          	.byte 0x23
 4184 05d0 00          	.uleb128 0x0
 4185 05d1 06          	.uleb128 0x6
 4186 05d2 00 00 00 00 	.4byte .LASF0
 4187 05d6 02          	.byte 0x2
 4188 05d7 B5 01       	.2byte 0x1b5
 4189 05d9 EA 00 00 00 	.4byte 0xea
 4190 05dd 02          	.byte 0x2
 4191 05de 02          	.byte 0x2
 4192 05df 0A          	.byte 0xa
 4193 05e0 02          	.byte 0x2
 4194 05e1 23          	.byte 0x23
 4195 05e2 00          	.uleb128 0x0
 4196 05e3 06          	.uleb128 0x6
 4197 05e4 00 00 00 00 	.4byte .LASF1
 4198 05e8 02          	.byte 0x2
 4199 05e9 B6 01       	.2byte 0x1b6
 4200 05eb EA 00 00 00 	.4byte 0xea
 4201 05ef 02          	.byte 0x2
 4202 05f0 01          	.byte 0x1
 4203 05f1 09          	.byte 0x9
 4204 05f2 02          	.byte 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 90


 4205 05f3 23          	.byte 0x23
 4206 05f4 00          	.uleb128 0x0
 4207 05f5 06          	.uleb128 0x6
 4208 05f6 00 00 00 00 	.4byte .LASF2
 4209 05fa 02          	.byte 0x2
 4210 05fb B8 01       	.2byte 0x1b8
 4211 05fd EA 00 00 00 	.4byte 0xea
 4212 0601 02          	.byte 0x2
 4213 0602 01          	.byte 0x1
 4214 0603 02          	.byte 0x2
 4215 0604 02          	.byte 0x2
 4216 0605 23          	.byte 0x23
 4217 0606 00          	.uleb128 0x0
 4218 0607 05          	.uleb128 0x5
 4219 0608 54 4F 4E 00 	.asciz "TON"
 4220 060c 02          	.byte 0x2
 4221 060d BA 01       	.2byte 0x1ba
 4222 060f EA 00 00 00 	.4byte 0xea
 4223 0613 02          	.byte 0x2
 4224 0614 01          	.byte 0x1
 4225 0615 10          	.byte 0x10
 4226 0616 02          	.byte 0x2
 4227 0617 23          	.byte 0x23
 4228 0618 00          	.uleb128 0x0
 4229 0619 00          	.byte 0x0
 4230 061a 04          	.uleb128 0x4
 4231 061b 02          	.byte 0x2
 4232 061c 02          	.byte 0x2
 4233 061d BC 01       	.2byte 0x1bc
 4234 061f 48 06 00 00 	.4byte 0x648
 4235 0623 06          	.uleb128 0x6
 4236 0624 00 00 00 00 	.4byte .LASF3
 4237 0628 02          	.byte 0x2
 4238 0629 BE 01       	.2byte 0x1be
 4239 062b EA 00 00 00 	.4byte 0xea
 4240 062f 02          	.byte 0x2
 4241 0630 01          	.byte 0x1
 4242 0631 0B          	.byte 0xb
 4243 0632 02          	.byte 0x2
 4244 0633 23          	.byte 0x23
 4245 0634 00          	.uleb128 0x0
 4246 0635 06          	.uleb128 0x6
 4247 0636 00 00 00 00 	.4byte .LASF4
 4248 063a 02          	.byte 0x2
 4249 063b BF 01       	.2byte 0x1bf
 4250 063d EA 00 00 00 	.4byte 0xea
 4251 0641 02          	.byte 0x2
 4252 0642 01          	.byte 0x1
 4253 0643 0A          	.byte 0xa
 4254 0644 02          	.byte 0x2
 4255 0645 23          	.byte 0x23
 4256 0646 00          	.uleb128 0x0
 4257 0647 00          	.byte 0x0
 4258 0648 07          	.uleb128 0x7
 4259 0649 02          	.byte 0x2
 4260 064a 02          	.byte 0x2
 4261 064b AF 01       	.2byte 0x1af
MPLAB XC16 ASSEMBLY Listing:   			page 91


 4262 064d 5C 06 00 00 	.4byte 0x65c
 4263 0651 08          	.uleb128 0x8
 4264 0652 A4 05 00 00 	.4byte 0x5a4
 4265 0656 08          	.uleb128 0x8
 4266 0657 1A 06 00 00 	.4byte 0x61a
 4267 065b 00          	.byte 0x0
 4268 065c 09          	.uleb128 0x9
 4269 065d 74 61 67 54 	.asciz "tagT6CONBITS"
 4269      36 43 4F 4E 
 4269      42 49 54 53 
 4269      00 
 4270 066a 02          	.byte 0x2
 4271 066b 02          	.byte 0x2
 4272 066c AE 01       	.2byte 0x1ae
 4273 066e 7B 06 00 00 	.4byte 0x67b
 4274 0672 0A          	.uleb128 0xa
 4275 0673 48 06 00 00 	.4byte 0x648
 4276 0677 02          	.byte 0x2
 4277 0678 23          	.byte 0x23
 4278 0679 00          	.uleb128 0x0
 4279 067a 00          	.byte 0x0
 4280 067b 0B          	.uleb128 0xb
 4281 067c 54 36 43 4F 	.asciz "T6CONBITS"
 4281      4E 42 49 54 
 4281      53 00 
 4282 0686 02          	.byte 0x2
 4283 0687 C2 01       	.2byte 0x1c2
 4284 0689 5C 06 00 00 	.4byte 0x65c
 4285 068d 04          	.uleb128 0x4
 4286 068e 02          	.byte 0x2
 4287 068f 02          	.byte 0x2
 4288 0690 C9 01       	.2byte 0x1c9
 4289 0692 F1 06 00 00 	.4byte 0x6f1
 4290 0696 05          	.uleb128 0x5
 4291 0697 54 43 53 00 	.asciz "TCS"
 4292 069b 02          	.byte 0x2
 4293 069c CB 01       	.2byte 0x1cb
 4294 069e EA 00 00 00 	.4byte 0xea
 4295 06a2 02          	.byte 0x2
 4296 06a3 01          	.byte 0x1
 4297 06a4 0E          	.byte 0xe
 4298 06a5 02          	.byte 0x2
 4299 06a6 23          	.byte 0x23
 4300 06a7 00          	.uleb128 0x0
 4301 06a8 06          	.uleb128 0x6
 4302 06a9 00 00 00 00 	.4byte .LASF0
 4303 06ad 02          	.byte 0x2
 4304 06ae CD 01       	.2byte 0x1cd
 4305 06b0 EA 00 00 00 	.4byte 0xea
 4306 06b4 02          	.byte 0x2
 4307 06b5 02          	.byte 0x2
 4308 06b6 0A          	.byte 0xa
 4309 06b7 02          	.byte 0x2
 4310 06b8 23          	.byte 0x23
 4311 06b9 00          	.uleb128 0x0
 4312 06ba 06          	.uleb128 0x6
 4313 06bb 00 00 00 00 	.4byte .LASF1
MPLAB XC16 ASSEMBLY Listing:   			page 92


 4314 06bf 02          	.byte 0x2
 4315 06c0 CE 01       	.2byte 0x1ce
 4316 06c2 EA 00 00 00 	.4byte 0xea
 4317 06c6 02          	.byte 0x2
 4318 06c7 01          	.byte 0x1
 4319 06c8 09          	.byte 0x9
 4320 06c9 02          	.byte 0x2
 4321 06ca 23          	.byte 0x23
 4322 06cb 00          	.uleb128 0x0
 4323 06cc 06          	.uleb128 0x6
 4324 06cd 00 00 00 00 	.4byte .LASF2
 4325 06d1 02          	.byte 0x2
 4326 06d2 D0 01       	.2byte 0x1d0
 4327 06d4 EA 00 00 00 	.4byte 0xea
 4328 06d8 02          	.byte 0x2
 4329 06d9 01          	.byte 0x1
 4330 06da 02          	.byte 0x2
 4331 06db 02          	.byte 0x2
 4332 06dc 23          	.byte 0x23
 4333 06dd 00          	.uleb128 0x0
 4334 06de 05          	.uleb128 0x5
 4335 06df 54 4F 4E 00 	.asciz "TON"
 4336 06e3 02          	.byte 0x2
 4337 06e4 D2 01       	.2byte 0x1d2
 4338 06e6 EA 00 00 00 	.4byte 0xea
 4339 06ea 02          	.byte 0x2
 4340 06eb 01          	.byte 0x1
 4341 06ec 10          	.byte 0x10
 4342 06ed 02          	.byte 0x2
 4343 06ee 23          	.byte 0x23
 4344 06ef 00          	.uleb128 0x0
 4345 06f0 00          	.byte 0x0
 4346 06f1 04          	.uleb128 0x4
 4347 06f2 02          	.byte 0x2
 4348 06f3 02          	.byte 0x2
 4349 06f4 D4 01       	.2byte 0x1d4
 4350 06f6 1F 07 00 00 	.4byte 0x71f
 4351 06fa 06          	.uleb128 0x6
 4352 06fb 00 00 00 00 	.4byte .LASF3
 4353 06ff 02          	.byte 0x2
 4354 0700 D6 01       	.2byte 0x1d6
 4355 0702 EA 00 00 00 	.4byte 0xea
 4356 0706 02          	.byte 0x2
 4357 0707 01          	.byte 0x1
 4358 0708 0B          	.byte 0xb
 4359 0709 02          	.byte 0x2
 4360 070a 23          	.byte 0x23
 4361 070b 00          	.uleb128 0x0
 4362 070c 06          	.uleb128 0x6
 4363 070d 00 00 00 00 	.4byte .LASF4
 4364 0711 02          	.byte 0x2
 4365 0712 D7 01       	.2byte 0x1d7
 4366 0714 EA 00 00 00 	.4byte 0xea
 4367 0718 02          	.byte 0x2
 4368 0719 01          	.byte 0x1
 4369 071a 0A          	.byte 0xa
 4370 071b 02          	.byte 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 93


 4371 071c 23          	.byte 0x23
 4372 071d 00          	.uleb128 0x0
 4373 071e 00          	.byte 0x0
 4374 071f 07          	.uleb128 0x7
 4375 0720 02          	.byte 0x2
 4376 0721 02          	.byte 0x2
 4377 0722 C8 01       	.2byte 0x1c8
 4378 0724 33 07 00 00 	.4byte 0x733
 4379 0728 08          	.uleb128 0x8
 4380 0729 8D 06 00 00 	.4byte 0x68d
 4381 072d 08          	.uleb128 0x8
 4382 072e F1 06 00 00 	.4byte 0x6f1
 4383 0732 00          	.byte 0x0
 4384 0733 09          	.uleb128 0x9
 4385 0734 74 61 67 54 	.asciz "tagT7CONBITS"
 4385      37 43 4F 4E 
 4385      42 49 54 53 
 4385      00 
 4386 0741 02          	.byte 0x2
 4387 0742 02          	.byte 0x2
 4388 0743 C7 01       	.2byte 0x1c7
 4389 0745 52 07 00 00 	.4byte 0x752
 4390 0749 0A          	.uleb128 0xa
 4391 074a 1F 07 00 00 	.4byte 0x71f
 4392 074e 02          	.byte 0x2
 4393 074f 23          	.byte 0x23
 4394 0750 00          	.uleb128 0x0
 4395 0751 00          	.byte 0x0
 4396 0752 0B          	.uleb128 0xb
 4397 0753 54 37 43 4F 	.asciz "T7CONBITS"
 4397      4E 42 49 54 
 4397      53 00 
 4398 075d 02          	.byte 0x2
 4399 075e DA 01       	.2byte 0x1da
 4400 0760 33 07 00 00 	.4byte 0x733
 4401 0764 04          	.uleb128 0x4
 4402 0765 02          	.byte 0x2
 4403 0766 02          	.byte 0x2
 4404 0767 EB 01       	.2byte 0x1eb
 4405 0769 DA 07 00 00 	.4byte 0x7da
 4406 076d 05          	.uleb128 0x5
 4407 076e 54 43 53 00 	.asciz "TCS"
 4408 0772 02          	.byte 0x2
 4409 0773 ED 01       	.2byte 0x1ed
 4410 0775 EA 00 00 00 	.4byte 0xea
 4411 0779 02          	.byte 0x2
 4412 077a 01          	.byte 0x1
 4413 077b 0E          	.byte 0xe
 4414 077c 02          	.byte 0x2
 4415 077d 23          	.byte 0x23
 4416 077e 00          	.uleb128 0x0
 4417 077f 05          	.uleb128 0x5
 4418 0780 54 33 32 00 	.asciz "T32"
 4419 0784 02          	.byte 0x2
 4420 0785 EF 01       	.2byte 0x1ef
 4421 0787 EA 00 00 00 	.4byte 0xea
 4422 078b 02          	.byte 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 94


 4423 078c 01          	.byte 0x1
 4424 078d 0C          	.byte 0xc
 4425 078e 02          	.byte 0x2
 4426 078f 23          	.byte 0x23
 4427 0790 00          	.uleb128 0x0
 4428 0791 06          	.uleb128 0x6
 4429 0792 00 00 00 00 	.4byte .LASF0
 4430 0796 02          	.byte 0x2
 4431 0797 F0 01       	.2byte 0x1f0
 4432 0799 EA 00 00 00 	.4byte 0xea
 4433 079d 02          	.byte 0x2
 4434 079e 02          	.byte 0x2
 4435 079f 0A          	.byte 0xa
 4436 07a0 02          	.byte 0x2
 4437 07a1 23          	.byte 0x23
 4438 07a2 00          	.uleb128 0x0
 4439 07a3 06          	.uleb128 0x6
 4440 07a4 00 00 00 00 	.4byte .LASF1
 4441 07a8 02          	.byte 0x2
 4442 07a9 F1 01       	.2byte 0x1f1
 4443 07ab EA 00 00 00 	.4byte 0xea
 4444 07af 02          	.byte 0x2
 4445 07b0 01          	.byte 0x1
 4446 07b1 09          	.byte 0x9
 4447 07b2 02          	.byte 0x2
 4448 07b3 23          	.byte 0x23
 4449 07b4 00          	.uleb128 0x0
 4450 07b5 06          	.uleb128 0x6
 4451 07b6 00 00 00 00 	.4byte .LASF2
 4452 07ba 02          	.byte 0x2
 4453 07bb F3 01       	.2byte 0x1f3
 4454 07bd EA 00 00 00 	.4byte 0xea
 4455 07c1 02          	.byte 0x2
 4456 07c2 01          	.byte 0x1
 4457 07c3 02          	.byte 0x2
 4458 07c4 02          	.byte 0x2
 4459 07c5 23          	.byte 0x23
 4460 07c6 00          	.uleb128 0x0
 4461 07c7 05          	.uleb128 0x5
 4462 07c8 54 4F 4E 00 	.asciz "TON"
 4463 07cc 02          	.byte 0x2
 4464 07cd F5 01       	.2byte 0x1f5
 4465 07cf EA 00 00 00 	.4byte 0xea
 4466 07d3 02          	.byte 0x2
 4467 07d4 01          	.byte 0x1
 4468 07d5 10          	.byte 0x10
 4469 07d6 02          	.byte 0x2
 4470 07d7 23          	.byte 0x23
 4471 07d8 00          	.uleb128 0x0
 4472 07d9 00          	.byte 0x0
 4473 07da 04          	.uleb128 0x4
 4474 07db 02          	.byte 0x2
 4475 07dc 02          	.byte 0x2
 4476 07dd F7 01       	.2byte 0x1f7
 4477 07df 08 08 00 00 	.4byte 0x808
 4478 07e3 06          	.uleb128 0x6
 4479 07e4 00 00 00 00 	.4byte .LASF3
MPLAB XC16 ASSEMBLY Listing:   			page 95


 4480 07e8 02          	.byte 0x2
 4481 07e9 F9 01       	.2byte 0x1f9
 4482 07eb EA 00 00 00 	.4byte 0xea
 4483 07ef 02          	.byte 0x2
 4484 07f0 01          	.byte 0x1
 4485 07f1 0B          	.byte 0xb
 4486 07f2 02          	.byte 0x2
 4487 07f3 23          	.byte 0x23
 4488 07f4 00          	.uleb128 0x0
 4489 07f5 06          	.uleb128 0x6
 4490 07f6 00 00 00 00 	.4byte .LASF4
 4491 07fa 02          	.byte 0x2
 4492 07fb FA 01       	.2byte 0x1fa
 4493 07fd EA 00 00 00 	.4byte 0xea
 4494 0801 02          	.byte 0x2
 4495 0802 01          	.byte 0x1
 4496 0803 0A          	.byte 0xa
 4497 0804 02          	.byte 0x2
 4498 0805 23          	.byte 0x23
 4499 0806 00          	.uleb128 0x0
 4500 0807 00          	.byte 0x0
 4501 0808 07          	.uleb128 0x7
 4502 0809 02          	.byte 0x2
 4503 080a 02          	.byte 0x2
 4504 080b EA 01       	.2byte 0x1ea
 4505 080d 1C 08 00 00 	.4byte 0x81c
 4506 0811 08          	.uleb128 0x8
 4507 0812 64 07 00 00 	.4byte 0x764
 4508 0816 08          	.uleb128 0x8
 4509 0817 DA 07 00 00 	.4byte 0x7da
 4510 081b 00          	.byte 0x0
 4511 081c 09          	.uleb128 0x9
 4512 081d 74 61 67 54 	.asciz "tagT8CONBITS"
 4512      38 43 4F 4E 
 4512      42 49 54 53 
 4512      00 
 4513 082a 02          	.byte 0x2
 4514 082b 02          	.byte 0x2
 4515 082c E9 01       	.2byte 0x1e9
 4516 082e 3B 08 00 00 	.4byte 0x83b
 4517 0832 0A          	.uleb128 0xa
 4518 0833 08 08 00 00 	.4byte 0x808
 4519 0837 02          	.byte 0x2
 4520 0838 23          	.byte 0x23
 4521 0839 00          	.uleb128 0x0
 4522 083a 00          	.byte 0x0
 4523 083b 0B          	.uleb128 0xb
 4524 083c 54 38 43 4F 	.asciz "T8CONBITS"
 4524      4E 42 49 54 
 4524      53 00 
 4525 0846 02          	.byte 0x2
 4526 0847 FD 01       	.2byte 0x1fd
 4527 0849 1C 08 00 00 	.4byte 0x81c
 4528 084d 04          	.uleb128 0x4
 4529 084e 02          	.byte 0x2
 4530 084f 02          	.byte 0x2
 4531 0850 04 02       	.2byte 0x204
MPLAB XC16 ASSEMBLY Listing:   			page 96


 4532 0852 B1 08 00 00 	.4byte 0x8b1
 4533 0856 05          	.uleb128 0x5
 4534 0857 54 43 53 00 	.asciz "TCS"
 4535 085b 02          	.byte 0x2
 4536 085c 06 02       	.2byte 0x206
 4537 085e EA 00 00 00 	.4byte 0xea
 4538 0862 02          	.byte 0x2
 4539 0863 01          	.byte 0x1
 4540 0864 0E          	.byte 0xe
 4541 0865 02          	.byte 0x2
 4542 0866 23          	.byte 0x23
 4543 0867 00          	.uleb128 0x0
 4544 0868 06          	.uleb128 0x6
 4545 0869 00 00 00 00 	.4byte .LASF0
 4546 086d 02          	.byte 0x2
 4547 086e 08 02       	.2byte 0x208
 4548 0870 EA 00 00 00 	.4byte 0xea
 4549 0874 02          	.byte 0x2
 4550 0875 02          	.byte 0x2
 4551 0876 0A          	.byte 0xa
 4552 0877 02          	.byte 0x2
 4553 0878 23          	.byte 0x23
 4554 0879 00          	.uleb128 0x0
 4555 087a 06          	.uleb128 0x6
 4556 087b 00 00 00 00 	.4byte .LASF1
 4557 087f 02          	.byte 0x2
 4558 0880 09 02       	.2byte 0x209
 4559 0882 EA 00 00 00 	.4byte 0xea
 4560 0886 02          	.byte 0x2
 4561 0887 01          	.byte 0x1
 4562 0888 09          	.byte 0x9
 4563 0889 02          	.byte 0x2
 4564 088a 23          	.byte 0x23
 4565 088b 00          	.uleb128 0x0
 4566 088c 06          	.uleb128 0x6
 4567 088d 00 00 00 00 	.4byte .LASF2
 4568 0891 02          	.byte 0x2
 4569 0892 0B 02       	.2byte 0x20b
 4570 0894 EA 00 00 00 	.4byte 0xea
 4571 0898 02          	.byte 0x2
 4572 0899 01          	.byte 0x1
 4573 089a 02          	.byte 0x2
 4574 089b 02          	.byte 0x2
 4575 089c 23          	.byte 0x23
 4576 089d 00          	.uleb128 0x0
 4577 089e 05          	.uleb128 0x5
 4578 089f 54 4F 4E 00 	.asciz "TON"
 4579 08a3 02          	.byte 0x2
 4580 08a4 0D 02       	.2byte 0x20d
 4581 08a6 EA 00 00 00 	.4byte 0xea
 4582 08aa 02          	.byte 0x2
 4583 08ab 01          	.byte 0x1
 4584 08ac 10          	.byte 0x10
 4585 08ad 02          	.byte 0x2
 4586 08ae 23          	.byte 0x23
 4587 08af 00          	.uleb128 0x0
 4588 08b0 00          	.byte 0x0
MPLAB XC16 ASSEMBLY Listing:   			page 97


 4589 08b1 04          	.uleb128 0x4
 4590 08b2 02          	.byte 0x2
 4591 08b3 02          	.byte 0x2
 4592 08b4 0F 02       	.2byte 0x20f
 4593 08b6 DF 08 00 00 	.4byte 0x8df
 4594 08ba 06          	.uleb128 0x6
 4595 08bb 00 00 00 00 	.4byte .LASF3
 4596 08bf 02          	.byte 0x2
 4597 08c0 11 02       	.2byte 0x211
 4598 08c2 EA 00 00 00 	.4byte 0xea
 4599 08c6 02          	.byte 0x2
 4600 08c7 01          	.byte 0x1
 4601 08c8 0B          	.byte 0xb
 4602 08c9 02          	.byte 0x2
 4603 08ca 23          	.byte 0x23
 4604 08cb 00          	.uleb128 0x0
 4605 08cc 06          	.uleb128 0x6
 4606 08cd 00 00 00 00 	.4byte .LASF4
 4607 08d1 02          	.byte 0x2
 4608 08d2 12 02       	.2byte 0x212
 4609 08d4 EA 00 00 00 	.4byte 0xea
 4610 08d8 02          	.byte 0x2
 4611 08d9 01          	.byte 0x1
 4612 08da 0A          	.byte 0xa
 4613 08db 02          	.byte 0x2
 4614 08dc 23          	.byte 0x23
 4615 08dd 00          	.uleb128 0x0
 4616 08de 00          	.byte 0x0
 4617 08df 07          	.uleb128 0x7
 4618 08e0 02          	.byte 0x2
 4619 08e1 02          	.byte 0x2
 4620 08e2 03 02       	.2byte 0x203
 4621 08e4 F3 08 00 00 	.4byte 0x8f3
 4622 08e8 08          	.uleb128 0x8
 4623 08e9 4D 08 00 00 	.4byte 0x84d
 4624 08ed 08          	.uleb128 0x8
 4625 08ee B1 08 00 00 	.4byte 0x8b1
 4626 08f2 00          	.byte 0x0
 4627 08f3 09          	.uleb128 0x9
 4628 08f4 74 61 67 54 	.asciz "tagT9CONBITS"
 4628      39 43 4F 4E 
 4628      42 49 54 53 
 4628      00 
 4629 0901 02          	.byte 0x2
 4630 0902 02          	.byte 0x2
 4631 0903 02 02       	.2byte 0x202
 4632 0905 12 09 00 00 	.4byte 0x912
 4633 0909 0A          	.uleb128 0xa
 4634 090a DF 08 00 00 	.4byte 0x8df
 4635 090e 02          	.byte 0x2
 4636 090f 23          	.byte 0x23
 4637 0910 00          	.uleb128 0x0
 4638 0911 00          	.byte 0x0
 4639 0912 0B          	.uleb128 0xb
 4640 0913 54 39 43 4F 	.asciz "T9CONBITS"
 4640      4E 42 49 54 
 4640      53 00 
MPLAB XC16 ASSEMBLY Listing:   			page 98


 4641 091d 02          	.byte 0x2
 4642 091e 15 02       	.2byte 0x215
 4643 0920 F3 08 00 00 	.4byte 0x8f3
 4644 0924 09          	.uleb128 0x9
 4645 0925 74 61 67 49 	.asciz "tagIFS0BITS"
 4645      46 53 30 42 
 4645      49 54 53 00 
 4646 0931 02          	.byte 0x2
 4647 0932 02          	.byte 0x2
 4648 0933 08 25       	.2byte 0x2508
 4649 0935 7F 0A 00 00 	.4byte 0xa7f
 4650 0939 05          	.uleb128 0x5
 4651 093a 49 4E 54 30 	.asciz "INT0IF"
 4651      49 46 00 
 4652 0941 02          	.byte 0x2
 4653 0942 09 25       	.2byte 0x2509
 4654 0944 EA 00 00 00 	.4byte 0xea
 4655 0948 02          	.byte 0x2
 4656 0949 01          	.byte 0x1
 4657 094a 0F          	.byte 0xf
 4658 094b 02          	.byte 0x2
 4659 094c 23          	.byte 0x23
 4660 094d 00          	.uleb128 0x0
 4661 094e 05          	.uleb128 0x5
 4662 094f 49 43 31 49 	.asciz "IC1IF"
 4662      46 00 
 4663 0955 02          	.byte 0x2
 4664 0956 0A 25       	.2byte 0x250a
 4665 0958 EA 00 00 00 	.4byte 0xea
 4666 095c 02          	.byte 0x2
 4667 095d 01          	.byte 0x1
 4668 095e 0E          	.byte 0xe
 4669 095f 02          	.byte 0x2
 4670 0960 23          	.byte 0x23
 4671 0961 00          	.uleb128 0x0
 4672 0962 05          	.uleb128 0x5
 4673 0963 4F 43 31 49 	.asciz "OC1IF"
 4673      46 00 
 4674 0969 02          	.byte 0x2
 4675 096a 0B 25       	.2byte 0x250b
 4676 096c EA 00 00 00 	.4byte 0xea
 4677 0970 02          	.byte 0x2
 4678 0971 01          	.byte 0x1
 4679 0972 0D          	.byte 0xd
 4680 0973 02          	.byte 0x2
 4681 0974 23          	.byte 0x23
 4682 0975 00          	.uleb128 0x0
 4683 0976 05          	.uleb128 0x5
 4684 0977 54 31 49 46 	.asciz "T1IF"
 4684      00 
 4685 097c 02          	.byte 0x2
 4686 097d 0C 25       	.2byte 0x250c
 4687 097f EA 00 00 00 	.4byte 0xea
 4688 0983 02          	.byte 0x2
 4689 0984 01          	.byte 0x1
 4690 0985 0C          	.byte 0xc
 4691 0986 02          	.byte 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 99


 4692 0987 23          	.byte 0x23
 4693 0988 00          	.uleb128 0x0
 4694 0989 05          	.uleb128 0x5
 4695 098a 44 4D 41 30 	.asciz "DMA0IF"
 4695      49 46 00 
 4696 0991 02          	.byte 0x2
 4697 0992 0D 25       	.2byte 0x250d
 4698 0994 EA 00 00 00 	.4byte 0xea
 4699 0998 02          	.byte 0x2
 4700 0999 01          	.byte 0x1
 4701 099a 0B          	.byte 0xb
 4702 099b 02          	.byte 0x2
 4703 099c 23          	.byte 0x23
 4704 099d 00          	.uleb128 0x0
 4705 099e 05          	.uleb128 0x5
 4706 099f 49 43 32 49 	.asciz "IC2IF"
 4706      46 00 
 4707 09a5 02          	.byte 0x2
 4708 09a6 0E 25       	.2byte 0x250e
 4709 09a8 EA 00 00 00 	.4byte 0xea
 4710 09ac 02          	.byte 0x2
 4711 09ad 01          	.byte 0x1
 4712 09ae 0A          	.byte 0xa
 4713 09af 02          	.byte 0x2
 4714 09b0 23          	.byte 0x23
 4715 09b1 00          	.uleb128 0x0
 4716 09b2 05          	.uleb128 0x5
 4717 09b3 4F 43 32 49 	.asciz "OC2IF"
 4717      46 00 
 4718 09b9 02          	.byte 0x2
 4719 09ba 0F 25       	.2byte 0x250f
 4720 09bc EA 00 00 00 	.4byte 0xea
 4721 09c0 02          	.byte 0x2
 4722 09c1 01          	.byte 0x1
 4723 09c2 09          	.byte 0x9
 4724 09c3 02          	.byte 0x2
 4725 09c4 23          	.byte 0x23
 4726 09c5 00          	.uleb128 0x0
 4727 09c6 05          	.uleb128 0x5
 4728 09c7 54 32 49 46 	.asciz "T2IF"
 4728      00 
 4729 09cc 02          	.byte 0x2
 4730 09cd 10 25       	.2byte 0x2510
 4731 09cf EA 00 00 00 	.4byte 0xea
 4732 09d3 02          	.byte 0x2
 4733 09d4 01          	.byte 0x1
 4734 09d5 08          	.byte 0x8
 4735 09d6 02          	.byte 0x2
 4736 09d7 23          	.byte 0x23
 4737 09d8 00          	.uleb128 0x0
 4738 09d9 05          	.uleb128 0x5
 4739 09da 54 33 49 46 	.asciz "T3IF"
 4739      00 
 4740 09df 02          	.byte 0x2
 4741 09e0 11 25       	.2byte 0x2511
 4742 09e2 EA 00 00 00 	.4byte 0xea
 4743 09e6 02          	.byte 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 100


 4744 09e7 01          	.byte 0x1
 4745 09e8 07          	.byte 0x7
 4746 09e9 02          	.byte 0x2
 4747 09ea 23          	.byte 0x23
 4748 09eb 00          	.uleb128 0x0
 4749 09ec 05          	.uleb128 0x5
 4750 09ed 53 50 49 31 	.asciz "SPI1EIF"
 4750      45 49 46 00 
 4751 09f5 02          	.byte 0x2
 4752 09f6 12 25       	.2byte 0x2512
 4753 09f8 EA 00 00 00 	.4byte 0xea
 4754 09fc 02          	.byte 0x2
 4755 09fd 01          	.byte 0x1
 4756 09fe 06          	.byte 0x6
 4757 09ff 02          	.byte 0x2
 4758 0a00 23          	.byte 0x23
 4759 0a01 00          	.uleb128 0x0
 4760 0a02 05          	.uleb128 0x5
 4761 0a03 53 50 49 31 	.asciz "SPI1IF"
 4761      49 46 00 
 4762 0a0a 02          	.byte 0x2
 4763 0a0b 13 25       	.2byte 0x2513
 4764 0a0d EA 00 00 00 	.4byte 0xea
 4765 0a11 02          	.byte 0x2
 4766 0a12 01          	.byte 0x1
 4767 0a13 05          	.byte 0x5
 4768 0a14 02          	.byte 0x2
 4769 0a15 23          	.byte 0x23
 4770 0a16 00          	.uleb128 0x0
 4771 0a17 05          	.uleb128 0x5
 4772 0a18 55 31 52 58 	.asciz "U1RXIF"
 4772      49 46 00 
 4773 0a1f 02          	.byte 0x2
 4774 0a20 14 25       	.2byte 0x2514
 4775 0a22 EA 00 00 00 	.4byte 0xea
 4776 0a26 02          	.byte 0x2
 4777 0a27 01          	.byte 0x1
 4778 0a28 04          	.byte 0x4
 4779 0a29 02          	.byte 0x2
 4780 0a2a 23          	.byte 0x23
 4781 0a2b 00          	.uleb128 0x0
 4782 0a2c 05          	.uleb128 0x5
 4783 0a2d 55 31 54 58 	.asciz "U1TXIF"
 4783      49 46 00 
 4784 0a34 02          	.byte 0x2
 4785 0a35 15 25       	.2byte 0x2515
 4786 0a37 EA 00 00 00 	.4byte 0xea
 4787 0a3b 02          	.byte 0x2
 4788 0a3c 01          	.byte 0x1
 4789 0a3d 03          	.byte 0x3
 4790 0a3e 02          	.byte 0x2
 4791 0a3f 23          	.byte 0x23
 4792 0a40 00          	.uleb128 0x0
 4793 0a41 05          	.uleb128 0x5
 4794 0a42 41 44 31 49 	.asciz "AD1IF"
 4794      46 00 
 4795 0a48 02          	.byte 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 101


 4796 0a49 16 25       	.2byte 0x2516
 4797 0a4b EA 00 00 00 	.4byte 0xea
 4798 0a4f 02          	.byte 0x2
 4799 0a50 01          	.byte 0x1
 4800 0a51 02          	.byte 0x2
 4801 0a52 02          	.byte 0x2
 4802 0a53 23          	.byte 0x23
 4803 0a54 00          	.uleb128 0x0
 4804 0a55 05          	.uleb128 0x5
 4805 0a56 44 4D 41 31 	.asciz "DMA1IF"
 4805      49 46 00 
 4806 0a5d 02          	.byte 0x2
 4807 0a5e 17 25       	.2byte 0x2517
 4808 0a60 EA 00 00 00 	.4byte 0xea
 4809 0a64 02          	.byte 0x2
 4810 0a65 01          	.byte 0x1
 4811 0a66 01          	.byte 0x1
 4812 0a67 02          	.byte 0x2
 4813 0a68 23          	.byte 0x23
 4814 0a69 00          	.uleb128 0x0
 4815 0a6a 05          	.uleb128 0x5
 4816 0a6b 4E 56 4D 49 	.asciz "NVMIF"
 4816      46 00 
 4817 0a71 02          	.byte 0x2
 4818 0a72 18 25       	.2byte 0x2518
 4819 0a74 EA 00 00 00 	.4byte 0xea
 4820 0a78 02          	.byte 0x2
 4821 0a79 01          	.byte 0x1
 4822 0a7a 10          	.byte 0x10
 4823 0a7b 02          	.byte 0x2
 4824 0a7c 23          	.byte 0x23
 4825 0a7d 00          	.uleb128 0x0
 4826 0a7e 00          	.byte 0x0
 4827 0a7f 0B          	.uleb128 0xb
 4828 0a80 49 46 53 30 	.asciz "IFS0BITS"
 4828      42 49 54 53 
 4828      00 
 4829 0a89 02          	.byte 0x2
 4830 0a8a 19 25       	.2byte 0x2519
 4831 0a8c 24 09 00 00 	.4byte 0x924
 4832 0a90 09          	.uleb128 0x9
 4833 0a91 74 61 67 49 	.asciz "tagIFS1BITS"
 4833      46 53 31 42 
 4833      49 54 53 00 
 4834 0a9d 02          	.byte 0x2
 4835 0a9e 02          	.byte 0x2
 4836 0a9f 1E 25       	.2byte 0x251e
 4837 0aa1 EB 0B 00 00 	.4byte 0xbeb
 4838 0aa5 05          	.uleb128 0x5
 4839 0aa6 53 49 32 43 	.asciz "SI2C1IF"
 4839      31 49 46 00 
 4840 0aae 02          	.byte 0x2
 4841 0aaf 1F 25       	.2byte 0x251f
 4842 0ab1 EA 00 00 00 	.4byte 0xea
 4843 0ab5 02          	.byte 0x2
 4844 0ab6 01          	.byte 0x1
 4845 0ab7 0F          	.byte 0xf
MPLAB XC16 ASSEMBLY Listing:   			page 102


 4846 0ab8 02          	.byte 0x2
 4847 0ab9 23          	.byte 0x23
 4848 0aba 00          	.uleb128 0x0
 4849 0abb 05          	.uleb128 0x5
 4850 0abc 4D 49 32 43 	.asciz "MI2C1IF"
 4850      31 49 46 00 
 4851 0ac4 02          	.byte 0x2
 4852 0ac5 20 25       	.2byte 0x2520
 4853 0ac7 EA 00 00 00 	.4byte 0xea
 4854 0acb 02          	.byte 0x2
 4855 0acc 01          	.byte 0x1
 4856 0acd 0E          	.byte 0xe
 4857 0ace 02          	.byte 0x2
 4858 0acf 23          	.byte 0x23
 4859 0ad0 00          	.uleb128 0x0
 4860 0ad1 05          	.uleb128 0x5
 4861 0ad2 43 4D 49 46 	.asciz "CMIF"
 4861      00 
 4862 0ad7 02          	.byte 0x2
 4863 0ad8 21 25       	.2byte 0x2521
 4864 0ada EA 00 00 00 	.4byte 0xea
 4865 0ade 02          	.byte 0x2
 4866 0adf 01          	.byte 0x1
 4867 0ae0 0D          	.byte 0xd
 4868 0ae1 02          	.byte 0x2
 4869 0ae2 23          	.byte 0x23
 4870 0ae3 00          	.uleb128 0x0
 4871 0ae4 05          	.uleb128 0x5
 4872 0ae5 43 4E 49 46 	.asciz "CNIF"
 4872      00 
 4873 0aea 02          	.byte 0x2
 4874 0aeb 22 25       	.2byte 0x2522
 4875 0aed EA 00 00 00 	.4byte 0xea
 4876 0af1 02          	.byte 0x2
 4877 0af2 01          	.byte 0x1
 4878 0af3 0C          	.byte 0xc
 4879 0af4 02          	.byte 0x2
 4880 0af5 23          	.byte 0x23
 4881 0af6 00          	.uleb128 0x0
 4882 0af7 05          	.uleb128 0x5
 4883 0af8 49 4E 54 31 	.asciz "INT1IF"
 4883      49 46 00 
 4884 0aff 02          	.byte 0x2
 4885 0b00 23 25       	.2byte 0x2523
 4886 0b02 EA 00 00 00 	.4byte 0xea
 4887 0b06 02          	.byte 0x2
 4888 0b07 01          	.byte 0x1
 4889 0b08 0B          	.byte 0xb
 4890 0b09 02          	.byte 0x2
 4891 0b0a 23          	.byte 0x23
 4892 0b0b 00          	.uleb128 0x0
 4893 0b0c 05          	.uleb128 0x5
 4894 0b0d 41 44 32 49 	.asciz "AD2IF"
 4894      46 00 
 4895 0b13 02          	.byte 0x2
 4896 0b14 24 25       	.2byte 0x2524
 4897 0b16 EA 00 00 00 	.4byte 0xea
MPLAB XC16 ASSEMBLY Listing:   			page 103


 4898 0b1a 02          	.byte 0x2
 4899 0b1b 01          	.byte 0x1
 4900 0b1c 0A          	.byte 0xa
 4901 0b1d 02          	.byte 0x2
 4902 0b1e 23          	.byte 0x23
 4903 0b1f 00          	.uleb128 0x0
 4904 0b20 05          	.uleb128 0x5
 4905 0b21 49 43 37 49 	.asciz "IC7IF"
 4905      46 00 
 4906 0b27 02          	.byte 0x2
 4907 0b28 25 25       	.2byte 0x2525
 4908 0b2a EA 00 00 00 	.4byte 0xea
 4909 0b2e 02          	.byte 0x2
 4910 0b2f 01          	.byte 0x1
 4911 0b30 09          	.byte 0x9
 4912 0b31 02          	.byte 0x2
 4913 0b32 23          	.byte 0x23
 4914 0b33 00          	.uleb128 0x0
 4915 0b34 05          	.uleb128 0x5
 4916 0b35 49 43 38 49 	.asciz "IC8IF"
 4916      46 00 
 4917 0b3b 02          	.byte 0x2
 4918 0b3c 26 25       	.2byte 0x2526
 4919 0b3e EA 00 00 00 	.4byte 0xea
 4920 0b42 02          	.byte 0x2
 4921 0b43 01          	.byte 0x1
 4922 0b44 08          	.byte 0x8
 4923 0b45 02          	.byte 0x2
 4924 0b46 23          	.byte 0x23
 4925 0b47 00          	.uleb128 0x0
 4926 0b48 05          	.uleb128 0x5
 4927 0b49 44 4D 41 32 	.asciz "DMA2IF"
 4927      49 46 00 
 4928 0b50 02          	.byte 0x2
 4929 0b51 27 25       	.2byte 0x2527
 4930 0b53 EA 00 00 00 	.4byte 0xea
 4931 0b57 02          	.byte 0x2
 4932 0b58 01          	.byte 0x1
 4933 0b59 07          	.byte 0x7
 4934 0b5a 02          	.byte 0x2
 4935 0b5b 23          	.byte 0x23
 4936 0b5c 00          	.uleb128 0x0
 4937 0b5d 05          	.uleb128 0x5
 4938 0b5e 4F 43 33 49 	.asciz "OC3IF"
 4938      46 00 
 4939 0b64 02          	.byte 0x2
 4940 0b65 28 25       	.2byte 0x2528
 4941 0b67 EA 00 00 00 	.4byte 0xea
 4942 0b6b 02          	.byte 0x2
 4943 0b6c 01          	.byte 0x1
 4944 0b6d 06          	.byte 0x6
 4945 0b6e 02          	.byte 0x2
 4946 0b6f 23          	.byte 0x23
 4947 0b70 00          	.uleb128 0x0
 4948 0b71 05          	.uleb128 0x5
 4949 0b72 4F 43 34 49 	.asciz "OC4IF"
 4949      46 00 
MPLAB XC16 ASSEMBLY Listing:   			page 104


 4950 0b78 02          	.byte 0x2
 4951 0b79 29 25       	.2byte 0x2529
 4952 0b7b EA 00 00 00 	.4byte 0xea
 4953 0b7f 02          	.byte 0x2
 4954 0b80 01          	.byte 0x1
 4955 0b81 05          	.byte 0x5
 4956 0b82 02          	.byte 0x2
 4957 0b83 23          	.byte 0x23
 4958 0b84 00          	.uleb128 0x0
 4959 0b85 05          	.uleb128 0x5
 4960 0b86 54 34 49 46 	.asciz "T4IF"
 4960      00 
 4961 0b8b 02          	.byte 0x2
 4962 0b8c 2A 25       	.2byte 0x252a
 4963 0b8e EA 00 00 00 	.4byte 0xea
 4964 0b92 02          	.byte 0x2
 4965 0b93 01          	.byte 0x1
 4966 0b94 04          	.byte 0x4
 4967 0b95 02          	.byte 0x2
 4968 0b96 23          	.byte 0x23
 4969 0b97 00          	.uleb128 0x0
 4970 0b98 05          	.uleb128 0x5
 4971 0b99 54 35 49 46 	.asciz "T5IF"
 4971      00 
 4972 0b9e 02          	.byte 0x2
 4973 0b9f 2B 25       	.2byte 0x252b
 4974 0ba1 EA 00 00 00 	.4byte 0xea
 4975 0ba5 02          	.byte 0x2
 4976 0ba6 01          	.byte 0x1
 4977 0ba7 03          	.byte 0x3
 4978 0ba8 02          	.byte 0x2
 4979 0ba9 23          	.byte 0x23
 4980 0baa 00          	.uleb128 0x0
 4981 0bab 05          	.uleb128 0x5
 4982 0bac 49 4E 54 32 	.asciz "INT2IF"
 4982      49 46 00 
 4983 0bb3 02          	.byte 0x2
 4984 0bb4 2C 25       	.2byte 0x252c
 4985 0bb6 EA 00 00 00 	.4byte 0xea
 4986 0bba 02          	.byte 0x2
 4987 0bbb 01          	.byte 0x1
 4988 0bbc 02          	.byte 0x2
 4989 0bbd 02          	.byte 0x2
 4990 0bbe 23          	.byte 0x23
 4991 0bbf 00          	.uleb128 0x0
 4992 0bc0 05          	.uleb128 0x5
 4993 0bc1 55 32 52 58 	.asciz "U2RXIF"
 4993      49 46 00 
 4994 0bc8 02          	.byte 0x2
 4995 0bc9 2D 25       	.2byte 0x252d
 4996 0bcb EA 00 00 00 	.4byte 0xea
 4997 0bcf 02          	.byte 0x2
 4998 0bd0 01          	.byte 0x1
 4999 0bd1 01          	.byte 0x1
 5000 0bd2 02          	.byte 0x2
 5001 0bd3 23          	.byte 0x23
 5002 0bd4 00          	.uleb128 0x0
MPLAB XC16 ASSEMBLY Listing:   			page 105


 5003 0bd5 05          	.uleb128 0x5
 5004 0bd6 55 32 54 58 	.asciz "U2TXIF"
 5004      49 46 00 
 5005 0bdd 02          	.byte 0x2
 5006 0bde 2E 25       	.2byte 0x252e
 5007 0be0 EA 00 00 00 	.4byte 0xea
 5008 0be4 02          	.byte 0x2
 5009 0be5 01          	.byte 0x1
 5010 0be6 10          	.byte 0x10
 5011 0be7 02          	.byte 0x2
 5012 0be8 23          	.byte 0x23
 5013 0be9 00          	.uleb128 0x0
 5014 0bea 00          	.byte 0x0
 5015 0beb 0B          	.uleb128 0xb
 5016 0bec 49 46 53 31 	.asciz "IFS1BITS"
 5016      42 49 54 53 
 5016      00 
 5017 0bf5 02          	.byte 0x2
 5018 0bf6 2F 25       	.2byte 0x252f
 5019 0bf8 90 0A 00 00 	.4byte 0xa90
 5020 0bfc 09          	.uleb128 0x9
 5021 0bfd 74 61 67 49 	.asciz "tagIFS2BITS"
 5021      46 53 32 42 
 5021      49 54 53 00 
 5022 0c09 02          	.byte 0x2
 5023 0c0a 02          	.byte 0x2
 5024 0c0b 34 25       	.2byte 0x2534
 5025 0c0d 56 0D 00 00 	.4byte 0xd56
 5026 0c11 05          	.uleb128 0x5
 5027 0c12 53 50 49 32 	.asciz "SPI2EIF"
 5027      45 49 46 00 
 5028 0c1a 02          	.byte 0x2
 5029 0c1b 35 25       	.2byte 0x2535
 5030 0c1d EA 00 00 00 	.4byte 0xea
 5031 0c21 02          	.byte 0x2
 5032 0c22 01          	.byte 0x1
 5033 0c23 0F          	.byte 0xf
 5034 0c24 02          	.byte 0x2
 5035 0c25 23          	.byte 0x23
 5036 0c26 00          	.uleb128 0x0
 5037 0c27 05          	.uleb128 0x5
 5038 0c28 53 50 49 32 	.asciz "SPI2IF"
 5038      49 46 00 
 5039 0c2f 02          	.byte 0x2
 5040 0c30 36 25       	.2byte 0x2536
 5041 0c32 EA 00 00 00 	.4byte 0xea
 5042 0c36 02          	.byte 0x2
 5043 0c37 01          	.byte 0x1
 5044 0c38 0E          	.byte 0xe
 5045 0c39 02          	.byte 0x2
 5046 0c3a 23          	.byte 0x23
 5047 0c3b 00          	.uleb128 0x0
 5048 0c3c 05          	.uleb128 0x5
 5049 0c3d 43 31 52 58 	.asciz "C1RXIF"
 5049      49 46 00 
 5050 0c44 02          	.byte 0x2
 5051 0c45 37 25       	.2byte 0x2537
MPLAB XC16 ASSEMBLY Listing:   			page 106


 5052 0c47 EA 00 00 00 	.4byte 0xea
 5053 0c4b 02          	.byte 0x2
 5054 0c4c 01          	.byte 0x1
 5055 0c4d 0D          	.byte 0xd
 5056 0c4e 02          	.byte 0x2
 5057 0c4f 23          	.byte 0x23
 5058 0c50 00          	.uleb128 0x0
 5059 0c51 05          	.uleb128 0x5
 5060 0c52 43 31 49 46 	.asciz "C1IF"
 5060      00 
 5061 0c57 02          	.byte 0x2
 5062 0c58 38 25       	.2byte 0x2538
 5063 0c5a EA 00 00 00 	.4byte 0xea
 5064 0c5e 02          	.byte 0x2
 5065 0c5f 01          	.byte 0x1
 5066 0c60 0C          	.byte 0xc
 5067 0c61 02          	.byte 0x2
 5068 0c62 23          	.byte 0x23
 5069 0c63 00          	.uleb128 0x0
 5070 0c64 05          	.uleb128 0x5
 5071 0c65 44 4D 41 33 	.asciz "DMA3IF"
 5071      49 46 00 
 5072 0c6c 02          	.byte 0x2
 5073 0c6d 39 25       	.2byte 0x2539
 5074 0c6f EA 00 00 00 	.4byte 0xea
 5075 0c73 02          	.byte 0x2
 5076 0c74 01          	.byte 0x1
 5077 0c75 0B          	.byte 0xb
 5078 0c76 02          	.byte 0x2
 5079 0c77 23          	.byte 0x23
 5080 0c78 00          	.uleb128 0x0
 5081 0c79 05          	.uleb128 0x5
 5082 0c7a 49 43 33 49 	.asciz "IC3IF"
 5082      46 00 
 5083 0c80 02          	.byte 0x2
 5084 0c81 3A 25       	.2byte 0x253a
 5085 0c83 EA 00 00 00 	.4byte 0xea
 5086 0c87 02          	.byte 0x2
 5087 0c88 01          	.byte 0x1
 5088 0c89 0A          	.byte 0xa
 5089 0c8a 02          	.byte 0x2
 5090 0c8b 23          	.byte 0x23
 5091 0c8c 00          	.uleb128 0x0
 5092 0c8d 05          	.uleb128 0x5
 5093 0c8e 49 43 34 49 	.asciz "IC4IF"
 5093      46 00 
 5094 0c94 02          	.byte 0x2
 5095 0c95 3B 25       	.2byte 0x253b
 5096 0c97 EA 00 00 00 	.4byte 0xea
 5097 0c9b 02          	.byte 0x2
 5098 0c9c 01          	.byte 0x1
 5099 0c9d 09          	.byte 0x9
 5100 0c9e 02          	.byte 0x2
 5101 0c9f 23          	.byte 0x23
 5102 0ca0 00          	.uleb128 0x0
 5103 0ca1 05          	.uleb128 0x5
 5104 0ca2 49 43 35 49 	.asciz "IC5IF"
MPLAB XC16 ASSEMBLY Listing:   			page 107


 5104      46 00 
 5105 0ca8 02          	.byte 0x2
 5106 0ca9 3C 25       	.2byte 0x253c
 5107 0cab EA 00 00 00 	.4byte 0xea
 5108 0caf 02          	.byte 0x2
 5109 0cb0 01          	.byte 0x1
 5110 0cb1 08          	.byte 0x8
 5111 0cb2 02          	.byte 0x2
 5112 0cb3 23          	.byte 0x23
 5113 0cb4 00          	.uleb128 0x0
 5114 0cb5 05          	.uleb128 0x5
 5115 0cb6 49 43 36 49 	.asciz "IC6IF"
 5115      46 00 
 5116 0cbc 02          	.byte 0x2
 5117 0cbd 3D 25       	.2byte 0x253d
 5118 0cbf EA 00 00 00 	.4byte 0xea
 5119 0cc3 02          	.byte 0x2
 5120 0cc4 01          	.byte 0x1
 5121 0cc5 07          	.byte 0x7
 5122 0cc6 02          	.byte 0x2
 5123 0cc7 23          	.byte 0x23
 5124 0cc8 00          	.uleb128 0x0
 5125 0cc9 05          	.uleb128 0x5
 5126 0cca 4F 43 35 49 	.asciz "OC5IF"
 5126      46 00 
 5127 0cd0 02          	.byte 0x2
 5128 0cd1 3E 25       	.2byte 0x253e
 5129 0cd3 EA 00 00 00 	.4byte 0xea
 5130 0cd7 02          	.byte 0x2
 5131 0cd8 01          	.byte 0x1
 5132 0cd9 06          	.byte 0x6
 5133 0cda 02          	.byte 0x2
 5134 0cdb 23          	.byte 0x23
 5135 0cdc 00          	.uleb128 0x0
 5136 0cdd 05          	.uleb128 0x5
 5137 0cde 4F 43 36 49 	.asciz "OC6IF"
 5137      46 00 
 5138 0ce4 02          	.byte 0x2
 5139 0ce5 3F 25       	.2byte 0x253f
 5140 0ce7 EA 00 00 00 	.4byte 0xea
 5141 0ceb 02          	.byte 0x2
 5142 0cec 01          	.byte 0x1
 5143 0ced 05          	.byte 0x5
 5144 0cee 02          	.byte 0x2
 5145 0cef 23          	.byte 0x23
 5146 0cf0 00          	.uleb128 0x0
 5147 0cf1 05          	.uleb128 0x5
 5148 0cf2 4F 43 37 49 	.asciz "OC7IF"
 5148      46 00 
 5149 0cf8 02          	.byte 0x2
 5150 0cf9 40 25       	.2byte 0x2540
 5151 0cfb EA 00 00 00 	.4byte 0xea
 5152 0cff 02          	.byte 0x2
 5153 0d00 01          	.byte 0x1
 5154 0d01 04          	.byte 0x4
 5155 0d02 02          	.byte 0x2
 5156 0d03 23          	.byte 0x23
MPLAB XC16 ASSEMBLY Listing:   			page 108


 5157 0d04 00          	.uleb128 0x0
 5158 0d05 05          	.uleb128 0x5
 5159 0d06 4F 43 38 49 	.asciz "OC8IF"
 5159      46 00 
 5160 0d0c 02          	.byte 0x2
 5161 0d0d 41 25       	.2byte 0x2541
 5162 0d0f EA 00 00 00 	.4byte 0xea
 5163 0d13 02          	.byte 0x2
 5164 0d14 01          	.byte 0x1
 5165 0d15 03          	.byte 0x3
 5166 0d16 02          	.byte 0x2
 5167 0d17 23          	.byte 0x23
 5168 0d18 00          	.uleb128 0x0
 5169 0d19 05          	.uleb128 0x5
 5170 0d1a 50 4D 50 49 	.asciz "PMPIF"
 5170      46 00 
 5171 0d20 02          	.byte 0x2
 5172 0d21 42 25       	.2byte 0x2542
 5173 0d23 EA 00 00 00 	.4byte 0xea
 5174 0d27 02          	.byte 0x2
 5175 0d28 01          	.byte 0x1
 5176 0d29 02          	.byte 0x2
 5177 0d2a 02          	.byte 0x2
 5178 0d2b 23          	.byte 0x23
 5179 0d2c 00          	.uleb128 0x0
 5180 0d2d 05          	.uleb128 0x5
 5181 0d2e 44 4D 41 34 	.asciz "DMA4IF"
 5181      49 46 00 
 5182 0d35 02          	.byte 0x2
 5183 0d36 43 25       	.2byte 0x2543
 5184 0d38 EA 00 00 00 	.4byte 0xea
 5185 0d3c 02          	.byte 0x2
 5186 0d3d 01          	.byte 0x1
 5187 0d3e 01          	.byte 0x1
 5188 0d3f 02          	.byte 0x2
 5189 0d40 23          	.byte 0x23
 5190 0d41 00          	.uleb128 0x0
 5191 0d42 05          	.uleb128 0x5
 5192 0d43 54 36 49 46 	.asciz "T6IF"
 5192      00 
 5193 0d48 02          	.byte 0x2
 5194 0d49 44 25       	.2byte 0x2544
 5195 0d4b EA 00 00 00 	.4byte 0xea
 5196 0d4f 02          	.byte 0x2
 5197 0d50 01          	.byte 0x1
 5198 0d51 10          	.byte 0x10
 5199 0d52 02          	.byte 0x2
 5200 0d53 23          	.byte 0x23
 5201 0d54 00          	.uleb128 0x0
 5202 0d55 00          	.byte 0x0
 5203 0d56 0B          	.uleb128 0xb
 5204 0d57 49 46 53 32 	.asciz "IFS2BITS"
 5204      42 49 54 53 
 5204      00 
 5205 0d60 02          	.byte 0x2
 5206 0d61 45 25       	.2byte 0x2545
 5207 0d63 FC 0B 00 00 	.4byte 0xbfc
MPLAB XC16 ASSEMBLY Listing:   			page 109


 5208 0d67 09          	.uleb128 0x9
 5209 0d68 74 61 67 49 	.asciz "tagIFS3BITS"
 5209      46 53 33 42 
 5209      49 54 53 00 
 5210 0d74 02          	.byte 0x2
 5211 0d75 02          	.byte 0x2
 5212 0d76 4A 25       	.2byte 0x254a
 5213 0d78 B0 0E 00 00 	.4byte 0xeb0
 5214 0d7c 05          	.uleb128 0x5
 5215 0d7d 54 37 49 46 	.asciz "T7IF"
 5215      00 
 5216 0d82 02          	.byte 0x2
 5217 0d83 4B 25       	.2byte 0x254b
 5218 0d85 EA 00 00 00 	.4byte 0xea
 5219 0d89 02          	.byte 0x2
 5220 0d8a 01          	.byte 0x1
 5221 0d8b 0F          	.byte 0xf
 5222 0d8c 02          	.byte 0x2
 5223 0d8d 23          	.byte 0x23
 5224 0d8e 00          	.uleb128 0x0
 5225 0d8f 05          	.uleb128 0x5
 5226 0d90 53 49 32 43 	.asciz "SI2C2IF"
 5226      32 49 46 00 
 5227 0d98 02          	.byte 0x2
 5228 0d99 4C 25       	.2byte 0x254c
 5229 0d9b EA 00 00 00 	.4byte 0xea
 5230 0d9f 02          	.byte 0x2
 5231 0da0 01          	.byte 0x1
 5232 0da1 0E          	.byte 0xe
 5233 0da2 02          	.byte 0x2
 5234 0da3 23          	.byte 0x23
 5235 0da4 00          	.uleb128 0x0
 5236 0da5 05          	.uleb128 0x5
 5237 0da6 4D 49 32 43 	.asciz "MI2C2IF"
 5237      32 49 46 00 
 5238 0dae 02          	.byte 0x2
 5239 0daf 4D 25       	.2byte 0x254d
 5240 0db1 EA 00 00 00 	.4byte 0xea
 5241 0db5 02          	.byte 0x2
 5242 0db6 01          	.byte 0x1
 5243 0db7 0D          	.byte 0xd
 5244 0db8 02          	.byte 0x2
 5245 0db9 23          	.byte 0x23
 5246 0dba 00          	.uleb128 0x0
 5247 0dbb 05          	.uleb128 0x5
 5248 0dbc 54 38 49 46 	.asciz "T8IF"
 5248      00 
 5249 0dc1 02          	.byte 0x2
 5250 0dc2 4E 25       	.2byte 0x254e
 5251 0dc4 EA 00 00 00 	.4byte 0xea
 5252 0dc8 02          	.byte 0x2
 5253 0dc9 01          	.byte 0x1
 5254 0dca 0C          	.byte 0xc
 5255 0dcb 02          	.byte 0x2
 5256 0dcc 23          	.byte 0x23
 5257 0dcd 00          	.uleb128 0x0
 5258 0dce 05          	.uleb128 0x5
MPLAB XC16 ASSEMBLY Listing:   			page 110


 5259 0dcf 54 39 49 46 	.asciz "T9IF"
 5259      00 
 5260 0dd4 02          	.byte 0x2
 5261 0dd5 4F 25       	.2byte 0x254f
 5262 0dd7 EA 00 00 00 	.4byte 0xea
 5263 0ddb 02          	.byte 0x2
 5264 0ddc 01          	.byte 0x1
 5265 0ddd 0B          	.byte 0xb
 5266 0dde 02          	.byte 0x2
 5267 0ddf 23          	.byte 0x23
 5268 0de0 00          	.uleb128 0x0
 5269 0de1 05          	.uleb128 0x5
 5270 0de2 49 4E 54 33 	.asciz "INT3IF"
 5270      49 46 00 
 5271 0de9 02          	.byte 0x2
 5272 0dea 50 25       	.2byte 0x2550
 5273 0dec EA 00 00 00 	.4byte 0xea
 5274 0df0 02          	.byte 0x2
 5275 0df1 01          	.byte 0x1
 5276 0df2 0A          	.byte 0xa
 5277 0df3 02          	.byte 0x2
 5278 0df4 23          	.byte 0x23
 5279 0df5 00          	.uleb128 0x0
 5280 0df6 05          	.uleb128 0x5
 5281 0df7 49 4E 54 34 	.asciz "INT4IF"
 5281      49 46 00 
 5282 0dfe 02          	.byte 0x2
 5283 0dff 51 25       	.2byte 0x2551
 5284 0e01 EA 00 00 00 	.4byte 0xea
 5285 0e05 02          	.byte 0x2
 5286 0e06 01          	.byte 0x1
 5287 0e07 09          	.byte 0x9
 5288 0e08 02          	.byte 0x2
 5289 0e09 23          	.byte 0x23
 5290 0e0a 00          	.uleb128 0x0
 5291 0e0b 05          	.uleb128 0x5
 5292 0e0c 43 32 52 58 	.asciz "C2RXIF"
 5292      49 46 00 
 5293 0e13 02          	.byte 0x2
 5294 0e14 52 25       	.2byte 0x2552
 5295 0e16 EA 00 00 00 	.4byte 0xea
 5296 0e1a 02          	.byte 0x2
 5297 0e1b 01          	.byte 0x1
 5298 0e1c 08          	.byte 0x8
 5299 0e1d 02          	.byte 0x2
 5300 0e1e 23          	.byte 0x23
 5301 0e1f 00          	.uleb128 0x0
 5302 0e20 05          	.uleb128 0x5
 5303 0e21 43 32 49 46 	.asciz "C2IF"
 5303      00 
 5304 0e26 02          	.byte 0x2
 5305 0e27 53 25       	.2byte 0x2553
 5306 0e29 EA 00 00 00 	.4byte 0xea
 5307 0e2d 02          	.byte 0x2
 5308 0e2e 01          	.byte 0x1
 5309 0e2f 07          	.byte 0x7
 5310 0e30 02          	.byte 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 111


 5311 0e31 23          	.byte 0x23
 5312 0e32 00          	.uleb128 0x0
 5313 0e33 05          	.uleb128 0x5
 5314 0e34 50 53 45 4D 	.asciz "PSEMIF"
 5314      49 46 00 
 5315 0e3b 02          	.byte 0x2
 5316 0e3c 54 25       	.2byte 0x2554
 5317 0e3e EA 00 00 00 	.4byte 0xea
 5318 0e42 02          	.byte 0x2
 5319 0e43 01          	.byte 0x1
 5320 0e44 06          	.byte 0x6
 5321 0e45 02          	.byte 0x2
 5322 0e46 23          	.byte 0x23
 5323 0e47 00          	.uleb128 0x0
 5324 0e48 05          	.uleb128 0x5
 5325 0e49 51 45 49 31 	.asciz "QEI1IF"
 5325      49 46 00 
 5326 0e50 02          	.byte 0x2
 5327 0e51 55 25       	.2byte 0x2555
 5328 0e53 EA 00 00 00 	.4byte 0xea
 5329 0e57 02          	.byte 0x2
 5330 0e58 01          	.byte 0x1
 5331 0e59 05          	.byte 0x5
 5332 0e5a 02          	.byte 0x2
 5333 0e5b 23          	.byte 0x23
 5334 0e5c 00          	.uleb128 0x0
 5335 0e5d 05          	.uleb128 0x5
 5336 0e5e 44 43 49 45 	.asciz "DCIEIF"
 5336      49 46 00 
 5337 0e65 02          	.byte 0x2
 5338 0e66 56 25       	.2byte 0x2556
 5339 0e68 EA 00 00 00 	.4byte 0xea
 5340 0e6c 02          	.byte 0x2
 5341 0e6d 01          	.byte 0x1
 5342 0e6e 04          	.byte 0x4
 5343 0e6f 02          	.byte 0x2
 5344 0e70 23          	.byte 0x23
 5345 0e71 00          	.uleb128 0x0
 5346 0e72 05          	.uleb128 0x5
 5347 0e73 44 43 49 49 	.asciz "DCIIF"
 5347      46 00 
 5348 0e79 02          	.byte 0x2
 5349 0e7a 57 25       	.2byte 0x2557
 5350 0e7c EA 00 00 00 	.4byte 0xea
 5351 0e80 02          	.byte 0x2
 5352 0e81 01          	.byte 0x1
 5353 0e82 03          	.byte 0x3
 5354 0e83 02          	.byte 0x2
 5355 0e84 23          	.byte 0x23
 5356 0e85 00          	.uleb128 0x0
 5357 0e86 05          	.uleb128 0x5
 5358 0e87 44 4D 41 35 	.asciz "DMA5IF"
 5358      49 46 00 
 5359 0e8e 02          	.byte 0x2
 5360 0e8f 58 25       	.2byte 0x2558
 5361 0e91 EA 00 00 00 	.4byte 0xea
 5362 0e95 02          	.byte 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 112


 5363 0e96 01          	.byte 0x1
 5364 0e97 02          	.byte 0x2
 5365 0e98 02          	.byte 0x2
 5366 0e99 23          	.byte 0x23
 5367 0e9a 00          	.uleb128 0x0
 5368 0e9b 05          	.uleb128 0x5
 5369 0e9c 52 54 43 49 	.asciz "RTCIF"
 5369      46 00 
 5370 0ea2 02          	.byte 0x2
 5371 0ea3 59 25       	.2byte 0x2559
 5372 0ea5 EA 00 00 00 	.4byte 0xea
 5373 0ea9 02          	.byte 0x2
 5374 0eaa 01          	.byte 0x1
 5375 0eab 01          	.byte 0x1
 5376 0eac 02          	.byte 0x2
 5377 0ead 23          	.byte 0x23
 5378 0eae 00          	.uleb128 0x0
 5379 0eaf 00          	.byte 0x0
 5380 0eb0 0B          	.uleb128 0xb
 5381 0eb1 49 46 53 33 	.asciz "IFS3BITS"
 5381      42 49 54 53 
 5381      00 
 5382 0eba 02          	.byte 0x2
 5383 0ebb 5A 25       	.2byte 0x255a
 5384 0ebd 67 0D 00 00 	.4byte 0xd67
 5385 0ec1 09          	.uleb128 0x9
 5386 0ec2 74 61 67 49 	.asciz "tagIEC0BITS"
 5386      45 43 30 42 
 5386      49 54 53 00 
 5387 0ece 02          	.byte 0x2
 5388 0ecf 02          	.byte 0x2
 5389 0ed0 B4 25       	.2byte 0x25b4
 5390 0ed2 1C 10 00 00 	.4byte 0x101c
 5391 0ed6 05          	.uleb128 0x5
 5392 0ed7 49 4E 54 30 	.asciz "INT0IE"
 5392      49 45 00 
 5393 0ede 02          	.byte 0x2
 5394 0edf B5 25       	.2byte 0x25b5
 5395 0ee1 EA 00 00 00 	.4byte 0xea
 5396 0ee5 02          	.byte 0x2
 5397 0ee6 01          	.byte 0x1
 5398 0ee7 0F          	.byte 0xf
 5399 0ee8 02          	.byte 0x2
 5400 0ee9 23          	.byte 0x23
 5401 0eea 00          	.uleb128 0x0
 5402 0eeb 05          	.uleb128 0x5
 5403 0eec 49 43 31 49 	.asciz "IC1IE"
 5403      45 00 
 5404 0ef2 02          	.byte 0x2
 5405 0ef3 B6 25       	.2byte 0x25b6
 5406 0ef5 EA 00 00 00 	.4byte 0xea
 5407 0ef9 02          	.byte 0x2
 5408 0efa 01          	.byte 0x1
 5409 0efb 0E          	.byte 0xe
 5410 0efc 02          	.byte 0x2
 5411 0efd 23          	.byte 0x23
 5412 0efe 00          	.uleb128 0x0
MPLAB XC16 ASSEMBLY Listing:   			page 113


 5413 0eff 05          	.uleb128 0x5
 5414 0f00 4F 43 31 49 	.asciz "OC1IE"
 5414      45 00 
 5415 0f06 02          	.byte 0x2
 5416 0f07 B7 25       	.2byte 0x25b7
 5417 0f09 EA 00 00 00 	.4byte 0xea
 5418 0f0d 02          	.byte 0x2
 5419 0f0e 01          	.byte 0x1
 5420 0f0f 0D          	.byte 0xd
 5421 0f10 02          	.byte 0x2
 5422 0f11 23          	.byte 0x23
 5423 0f12 00          	.uleb128 0x0
 5424 0f13 05          	.uleb128 0x5
 5425 0f14 54 31 49 45 	.asciz "T1IE"
 5425      00 
 5426 0f19 02          	.byte 0x2
 5427 0f1a B8 25       	.2byte 0x25b8
 5428 0f1c EA 00 00 00 	.4byte 0xea
 5429 0f20 02          	.byte 0x2
 5430 0f21 01          	.byte 0x1
 5431 0f22 0C          	.byte 0xc
 5432 0f23 02          	.byte 0x2
 5433 0f24 23          	.byte 0x23
 5434 0f25 00          	.uleb128 0x0
 5435 0f26 05          	.uleb128 0x5
 5436 0f27 44 4D 41 30 	.asciz "DMA0IE"
 5436      49 45 00 
 5437 0f2e 02          	.byte 0x2
 5438 0f2f B9 25       	.2byte 0x25b9
 5439 0f31 EA 00 00 00 	.4byte 0xea
 5440 0f35 02          	.byte 0x2
 5441 0f36 01          	.byte 0x1
 5442 0f37 0B          	.byte 0xb
 5443 0f38 02          	.byte 0x2
 5444 0f39 23          	.byte 0x23
 5445 0f3a 00          	.uleb128 0x0
 5446 0f3b 05          	.uleb128 0x5
 5447 0f3c 49 43 32 49 	.asciz "IC2IE"
 5447      45 00 
 5448 0f42 02          	.byte 0x2
 5449 0f43 BA 25       	.2byte 0x25ba
 5450 0f45 EA 00 00 00 	.4byte 0xea
 5451 0f49 02          	.byte 0x2
 5452 0f4a 01          	.byte 0x1
 5453 0f4b 0A          	.byte 0xa
 5454 0f4c 02          	.byte 0x2
 5455 0f4d 23          	.byte 0x23
 5456 0f4e 00          	.uleb128 0x0
 5457 0f4f 05          	.uleb128 0x5
 5458 0f50 4F 43 32 49 	.asciz "OC2IE"
 5458      45 00 
 5459 0f56 02          	.byte 0x2
 5460 0f57 BB 25       	.2byte 0x25bb
 5461 0f59 EA 00 00 00 	.4byte 0xea
 5462 0f5d 02          	.byte 0x2
 5463 0f5e 01          	.byte 0x1
 5464 0f5f 09          	.byte 0x9
MPLAB XC16 ASSEMBLY Listing:   			page 114


 5465 0f60 02          	.byte 0x2
 5466 0f61 23          	.byte 0x23
 5467 0f62 00          	.uleb128 0x0
 5468 0f63 05          	.uleb128 0x5
 5469 0f64 54 32 49 45 	.asciz "T2IE"
 5469      00 
 5470 0f69 02          	.byte 0x2
 5471 0f6a BC 25       	.2byte 0x25bc
 5472 0f6c EA 00 00 00 	.4byte 0xea
 5473 0f70 02          	.byte 0x2
 5474 0f71 01          	.byte 0x1
 5475 0f72 08          	.byte 0x8
 5476 0f73 02          	.byte 0x2
 5477 0f74 23          	.byte 0x23
 5478 0f75 00          	.uleb128 0x0
 5479 0f76 05          	.uleb128 0x5
 5480 0f77 54 33 49 45 	.asciz "T3IE"
 5480      00 
 5481 0f7c 02          	.byte 0x2
 5482 0f7d BD 25       	.2byte 0x25bd
 5483 0f7f EA 00 00 00 	.4byte 0xea
 5484 0f83 02          	.byte 0x2
 5485 0f84 01          	.byte 0x1
 5486 0f85 07          	.byte 0x7
 5487 0f86 02          	.byte 0x2
 5488 0f87 23          	.byte 0x23
 5489 0f88 00          	.uleb128 0x0
 5490 0f89 05          	.uleb128 0x5
 5491 0f8a 53 50 49 31 	.asciz "SPI1EIE"
 5491      45 49 45 00 
 5492 0f92 02          	.byte 0x2
 5493 0f93 BE 25       	.2byte 0x25be
 5494 0f95 EA 00 00 00 	.4byte 0xea
 5495 0f99 02          	.byte 0x2
 5496 0f9a 01          	.byte 0x1
 5497 0f9b 06          	.byte 0x6
 5498 0f9c 02          	.byte 0x2
 5499 0f9d 23          	.byte 0x23
 5500 0f9e 00          	.uleb128 0x0
 5501 0f9f 05          	.uleb128 0x5
 5502 0fa0 53 50 49 31 	.asciz "SPI1IE"
 5502      49 45 00 
 5503 0fa7 02          	.byte 0x2
 5504 0fa8 BF 25       	.2byte 0x25bf
 5505 0faa EA 00 00 00 	.4byte 0xea
 5506 0fae 02          	.byte 0x2
 5507 0faf 01          	.byte 0x1
 5508 0fb0 05          	.byte 0x5
 5509 0fb1 02          	.byte 0x2
 5510 0fb2 23          	.byte 0x23
 5511 0fb3 00          	.uleb128 0x0
 5512 0fb4 05          	.uleb128 0x5
 5513 0fb5 55 31 52 58 	.asciz "U1RXIE"
 5513      49 45 00 
 5514 0fbc 02          	.byte 0x2
 5515 0fbd C0 25       	.2byte 0x25c0
 5516 0fbf EA 00 00 00 	.4byte 0xea
MPLAB XC16 ASSEMBLY Listing:   			page 115


 5517 0fc3 02          	.byte 0x2
 5518 0fc4 01          	.byte 0x1
 5519 0fc5 04          	.byte 0x4
 5520 0fc6 02          	.byte 0x2
 5521 0fc7 23          	.byte 0x23
 5522 0fc8 00          	.uleb128 0x0
 5523 0fc9 05          	.uleb128 0x5
 5524 0fca 55 31 54 58 	.asciz "U1TXIE"
 5524      49 45 00 
 5525 0fd1 02          	.byte 0x2
 5526 0fd2 C1 25       	.2byte 0x25c1
 5527 0fd4 EA 00 00 00 	.4byte 0xea
 5528 0fd8 02          	.byte 0x2
 5529 0fd9 01          	.byte 0x1
 5530 0fda 03          	.byte 0x3
 5531 0fdb 02          	.byte 0x2
 5532 0fdc 23          	.byte 0x23
 5533 0fdd 00          	.uleb128 0x0
 5534 0fde 05          	.uleb128 0x5
 5535 0fdf 41 44 31 49 	.asciz "AD1IE"
 5535      45 00 
 5536 0fe5 02          	.byte 0x2
 5537 0fe6 C2 25       	.2byte 0x25c2
 5538 0fe8 EA 00 00 00 	.4byte 0xea
 5539 0fec 02          	.byte 0x2
 5540 0fed 01          	.byte 0x1
 5541 0fee 02          	.byte 0x2
 5542 0fef 02          	.byte 0x2
 5543 0ff0 23          	.byte 0x23
 5544 0ff1 00          	.uleb128 0x0
 5545 0ff2 05          	.uleb128 0x5
 5546 0ff3 44 4D 41 31 	.asciz "DMA1IE"
 5546      49 45 00 
 5547 0ffa 02          	.byte 0x2
 5548 0ffb C3 25       	.2byte 0x25c3
 5549 0ffd EA 00 00 00 	.4byte 0xea
 5550 1001 02          	.byte 0x2
 5551 1002 01          	.byte 0x1
 5552 1003 01          	.byte 0x1
 5553 1004 02          	.byte 0x2
 5554 1005 23          	.byte 0x23
 5555 1006 00          	.uleb128 0x0
 5556 1007 05          	.uleb128 0x5
 5557 1008 4E 56 4D 49 	.asciz "NVMIE"
 5557      45 00 
 5558 100e 02          	.byte 0x2
 5559 100f C4 25       	.2byte 0x25c4
 5560 1011 EA 00 00 00 	.4byte 0xea
 5561 1015 02          	.byte 0x2
 5562 1016 01          	.byte 0x1
 5563 1017 10          	.byte 0x10
 5564 1018 02          	.byte 0x2
 5565 1019 23          	.byte 0x23
 5566 101a 00          	.uleb128 0x0
 5567 101b 00          	.byte 0x0
 5568 101c 0B          	.uleb128 0xb
 5569 101d 49 45 43 30 	.asciz "IEC0BITS"
MPLAB XC16 ASSEMBLY Listing:   			page 116


 5569      42 49 54 53 
 5569      00 
 5570 1026 02          	.byte 0x2
 5571 1027 C5 25       	.2byte 0x25c5
 5572 1029 C1 0E 00 00 	.4byte 0xec1
 5573 102d 09          	.uleb128 0x9
 5574 102e 74 61 67 49 	.asciz "tagIEC1BITS"
 5574      45 43 31 42 
 5574      49 54 53 00 
 5575 103a 02          	.byte 0x2
 5576 103b 02          	.byte 0x2
 5577 103c CA 25       	.2byte 0x25ca
 5578 103e 88 11 00 00 	.4byte 0x1188
 5579 1042 05          	.uleb128 0x5
 5580 1043 53 49 32 43 	.asciz "SI2C1IE"
 5580      31 49 45 00 
 5581 104b 02          	.byte 0x2
 5582 104c CB 25       	.2byte 0x25cb
 5583 104e EA 00 00 00 	.4byte 0xea
 5584 1052 02          	.byte 0x2
 5585 1053 01          	.byte 0x1
 5586 1054 0F          	.byte 0xf
 5587 1055 02          	.byte 0x2
 5588 1056 23          	.byte 0x23
 5589 1057 00          	.uleb128 0x0
 5590 1058 05          	.uleb128 0x5
 5591 1059 4D 49 32 43 	.asciz "MI2C1IE"
 5591      31 49 45 00 
 5592 1061 02          	.byte 0x2
 5593 1062 CC 25       	.2byte 0x25cc
 5594 1064 EA 00 00 00 	.4byte 0xea
 5595 1068 02          	.byte 0x2
 5596 1069 01          	.byte 0x1
 5597 106a 0E          	.byte 0xe
 5598 106b 02          	.byte 0x2
 5599 106c 23          	.byte 0x23
 5600 106d 00          	.uleb128 0x0
 5601 106e 05          	.uleb128 0x5
 5602 106f 43 4D 49 45 	.asciz "CMIE"
 5602      00 
 5603 1074 02          	.byte 0x2
 5604 1075 CD 25       	.2byte 0x25cd
 5605 1077 EA 00 00 00 	.4byte 0xea
 5606 107b 02          	.byte 0x2
 5607 107c 01          	.byte 0x1
 5608 107d 0D          	.byte 0xd
 5609 107e 02          	.byte 0x2
 5610 107f 23          	.byte 0x23
 5611 1080 00          	.uleb128 0x0
 5612 1081 05          	.uleb128 0x5
 5613 1082 43 4E 49 45 	.asciz "CNIE"
 5613      00 
 5614 1087 02          	.byte 0x2
 5615 1088 CE 25       	.2byte 0x25ce
 5616 108a EA 00 00 00 	.4byte 0xea
 5617 108e 02          	.byte 0x2
 5618 108f 01          	.byte 0x1
MPLAB XC16 ASSEMBLY Listing:   			page 117


 5619 1090 0C          	.byte 0xc
 5620 1091 02          	.byte 0x2
 5621 1092 23          	.byte 0x23
 5622 1093 00          	.uleb128 0x0
 5623 1094 05          	.uleb128 0x5
 5624 1095 49 4E 54 31 	.asciz "INT1IE"
 5624      49 45 00 
 5625 109c 02          	.byte 0x2
 5626 109d CF 25       	.2byte 0x25cf
 5627 109f EA 00 00 00 	.4byte 0xea
 5628 10a3 02          	.byte 0x2
 5629 10a4 01          	.byte 0x1
 5630 10a5 0B          	.byte 0xb
 5631 10a6 02          	.byte 0x2
 5632 10a7 23          	.byte 0x23
 5633 10a8 00          	.uleb128 0x0
 5634 10a9 05          	.uleb128 0x5
 5635 10aa 41 44 32 49 	.asciz "AD2IE"
 5635      45 00 
 5636 10b0 02          	.byte 0x2
 5637 10b1 D0 25       	.2byte 0x25d0
 5638 10b3 EA 00 00 00 	.4byte 0xea
 5639 10b7 02          	.byte 0x2
 5640 10b8 01          	.byte 0x1
 5641 10b9 0A          	.byte 0xa
 5642 10ba 02          	.byte 0x2
 5643 10bb 23          	.byte 0x23
 5644 10bc 00          	.uleb128 0x0
 5645 10bd 05          	.uleb128 0x5
 5646 10be 49 43 37 49 	.asciz "IC7IE"
 5646      45 00 
 5647 10c4 02          	.byte 0x2
 5648 10c5 D1 25       	.2byte 0x25d1
 5649 10c7 EA 00 00 00 	.4byte 0xea
 5650 10cb 02          	.byte 0x2
 5651 10cc 01          	.byte 0x1
 5652 10cd 09          	.byte 0x9
 5653 10ce 02          	.byte 0x2
 5654 10cf 23          	.byte 0x23
 5655 10d0 00          	.uleb128 0x0
 5656 10d1 05          	.uleb128 0x5
 5657 10d2 49 43 38 49 	.asciz "IC8IE"
 5657      45 00 
 5658 10d8 02          	.byte 0x2
 5659 10d9 D2 25       	.2byte 0x25d2
 5660 10db EA 00 00 00 	.4byte 0xea
 5661 10df 02          	.byte 0x2
 5662 10e0 01          	.byte 0x1
 5663 10e1 08          	.byte 0x8
 5664 10e2 02          	.byte 0x2
 5665 10e3 23          	.byte 0x23
 5666 10e4 00          	.uleb128 0x0
 5667 10e5 05          	.uleb128 0x5
 5668 10e6 44 4D 41 32 	.asciz "DMA2IE"
 5668      49 45 00 
 5669 10ed 02          	.byte 0x2
 5670 10ee D3 25       	.2byte 0x25d3
MPLAB XC16 ASSEMBLY Listing:   			page 118


 5671 10f0 EA 00 00 00 	.4byte 0xea
 5672 10f4 02          	.byte 0x2
 5673 10f5 01          	.byte 0x1
 5674 10f6 07          	.byte 0x7
 5675 10f7 02          	.byte 0x2
 5676 10f8 23          	.byte 0x23
 5677 10f9 00          	.uleb128 0x0
 5678 10fa 05          	.uleb128 0x5
 5679 10fb 4F 43 33 49 	.asciz "OC3IE"
 5679      45 00 
 5680 1101 02          	.byte 0x2
 5681 1102 D4 25       	.2byte 0x25d4
 5682 1104 EA 00 00 00 	.4byte 0xea
 5683 1108 02          	.byte 0x2
 5684 1109 01          	.byte 0x1
 5685 110a 06          	.byte 0x6
 5686 110b 02          	.byte 0x2
 5687 110c 23          	.byte 0x23
 5688 110d 00          	.uleb128 0x0
 5689 110e 05          	.uleb128 0x5
 5690 110f 4F 43 34 49 	.asciz "OC4IE"
 5690      45 00 
 5691 1115 02          	.byte 0x2
 5692 1116 D5 25       	.2byte 0x25d5
 5693 1118 EA 00 00 00 	.4byte 0xea
 5694 111c 02          	.byte 0x2
 5695 111d 01          	.byte 0x1
 5696 111e 05          	.byte 0x5
 5697 111f 02          	.byte 0x2
 5698 1120 23          	.byte 0x23
 5699 1121 00          	.uleb128 0x0
 5700 1122 05          	.uleb128 0x5
 5701 1123 54 34 49 45 	.asciz "T4IE"
 5701      00 
 5702 1128 02          	.byte 0x2
 5703 1129 D6 25       	.2byte 0x25d6
 5704 112b EA 00 00 00 	.4byte 0xea
 5705 112f 02          	.byte 0x2
 5706 1130 01          	.byte 0x1
 5707 1131 04          	.byte 0x4
 5708 1132 02          	.byte 0x2
 5709 1133 23          	.byte 0x23
 5710 1134 00          	.uleb128 0x0
 5711 1135 05          	.uleb128 0x5
 5712 1136 54 35 49 45 	.asciz "T5IE"
 5712      00 
 5713 113b 02          	.byte 0x2
 5714 113c D7 25       	.2byte 0x25d7
 5715 113e EA 00 00 00 	.4byte 0xea
 5716 1142 02          	.byte 0x2
 5717 1143 01          	.byte 0x1
 5718 1144 03          	.byte 0x3
 5719 1145 02          	.byte 0x2
 5720 1146 23          	.byte 0x23
 5721 1147 00          	.uleb128 0x0
 5722 1148 05          	.uleb128 0x5
 5723 1149 49 4E 54 32 	.asciz "INT2IE"
MPLAB XC16 ASSEMBLY Listing:   			page 119


 5723      49 45 00 
 5724 1150 02          	.byte 0x2
 5725 1151 D8 25       	.2byte 0x25d8
 5726 1153 EA 00 00 00 	.4byte 0xea
 5727 1157 02          	.byte 0x2
 5728 1158 01          	.byte 0x1
 5729 1159 02          	.byte 0x2
 5730 115a 02          	.byte 0x2
 5731 115b 23          	.byte 0x23
 5732 115c 00          	.uleb128 0x0
 5733 115d 05          	.uleb128 0x5
 5734 115e 55 32 52 58 	.asciz "U2RXIE"
 5734      49 45 00 
 5735 1165 02          	.byte 0x2
 5736 1166 D9 25       	.2byte 0x25d9
 5737 1168 EA 00 00 00 	.4byte 0xea
 5738 116c 02          	.byte 0x2
 5739 116d 01          	.byte 0x1
 5740 116e 01          	.byte 0x1
 5741 116f 02          	.byte 0x2
 5742 1170 23          	.byte 0x23
 5743 1171 00          	.uleb128 0x0
 5744 1172 05          	.uleb128 0x5
 5745 1173 55 32 54 58 	.asciz "U2TXIE"
 5745      49 45 00 
 5746 117a 02          	.byte 0x2
 5747 117b DA 25       	.2byte 0x25da
 5748 117d EA 00 00 00 	.4byte 0xea
 5749 1181 02          	.byte 0x2
 5750 1182 01          	.byte 0x1
 5751 1183 10          	.byte 0x10
 5752 1184 02          	.byte 0x2
 5753 1185 23          	.byte 0x23
 5754 1186 00          	.uleb128 0x0
 5755 1187 00          	.byte 0x0
 5756 1188 0B          	.uleb128 0xb
 5757 1189 49 45 43 31 	.asciz "IEC1BITS"
 5757      42 49 54 53 
 5757      00 
 5758 1192 02          	.byte 0x2
 5759 1193 DB 25       	.2byte 0x25db
 5760 1195 2D 10 00 00 	.4byte 0x102d
 5761 1199 09          	.uleb128 0x9
 5762 119a 74 61 67 49 	.asciz "tagIEC2BITS"
 5762      45 43 32 42 
 5762      49 54 53 00 
 5763 11a6 02          	.byte 0x2
 5764 11a7 02          	.byte 0x2
 5765 11a8 E0 25       	.2byte 0x25e0
 5766 11aa F3 12 00 00 	.4byte 0x12f3
 5767 11ae 05          	.uleb128 0x5
 5768 11af 53 50 49 32 	.asciz "SPI2EIE"
 5768      45 49 45 00 
 5769 11b7 02          	.byte 0x2
 5770 11b8 E1 25       	.2byte 0x25e1
 5771 11ba EA 00 00 00 	.4byte 0xea
 5772 11be 02          	.byte 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 120


 5773 11bf 01          	.byte 0x1
 5774 11c0 0F          	.byte 0xf
 5775 11c1 02          	.byte 0x2
 5776 11c2 23          	.byte 0x23
 5777 11c3 00          	.uleb128 0x0
 5778 11c4 05          	.uleb128 0x5
 5779 11c5 53 50 49 32 	.asciz "SPI2IE"
 5779      49 45 00 
 5780 11cc 02          	.byte 0x2
 5781 11cd E2 25       	.2byte 0x25e2
 5782 11cf EA 00 00 00 	.4byte 0xea
 5783 11d3 02          	.byte 0x2
 5784 11d4 01          	.byte 0x1
 5785 11d5 0E          	.byte 0xe
 5786 11d6 02          	.byte 0x2
 5787 11d7 23          	.byte 0x23
 5788 11d8 00          	.uleb128 0x0
 5789 11d9 05          	.uleb128 0x5
 5790 11da 43 31 52 58 	.asciz "C1RXIE"
 5790      49 45 00 
 5791 11e1 02          	.byte 0x2
 5792 11e2 E3 25       	.2byte 0x25e3
 5793 11e4 EA 00 00 00 	.4byte 0xea
 5794 11e8 02          	.byte 0x2
 5795 11e9 01          	.byte 0x1
 5796 11ea 0D          	.byte 0xd
 5797 11eb 02          	.byte 0x2
 5798 11ec 23          	.byte 0x23
 5799 11ed 00          	.uleb128 0x0
 5800 11ee 05          	.uleb128 0x5
 5801 11ef 43 31 49 45 	.asciz "C1IE"
 5801      00 
 5802 11f4 02          	.byte 0x2
 5803 11f5 E4 25       	.2byte 0x25e4
 5804 11f7 EA 00 00 00 	.4byte 0xea
 5805 11fb 02          	.byte 0x2
 5806 11fc 01          	.byte 0x1
 5807 11fd 0C          	.byte 0xc
 5808 11fe 02          	.byte 0x2
 5809 11ff 23          	.byte 0x23
 5810 1200 00          	.uleb128 0x0
 5811 1201 05          	.uleb128 0x5
 5812 1202 44 4D 41 33 	.asciz "DMA3IE"
 5812      49 45 00 
 5813 1209 02          	.byte 0x2
 5814 120a E5 25       	.2byte 0x25e5
 5815 120c EA 00 00 00 	.4byte 0xea
 5816 1210 02          	.byte 0x2
 5817 1211 01          	.byte 0x1
 5818 1212 0B          	.byte 0xb
 5819 1213 02          	.byte 0x2
 5820 1214 23          	.byte 0x23
 5821 1215 00          	.uleb128 0x0
 5822 1216 05          	.uleb128 0x5
 5823 1217 49 43 33 49 	.asciz "IC3IE"
 5823      45 00 
 5824 121d 02          	.byte 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 121


 5825 121e E6 25       	.2byte 0x25e6
 5826 1220 EA 00 00 00 	.4byte 0xea
 5827 1224 02          	.byte 0x2
 5828 1225 01          	.byte 0x1
 5829 1226 0A          	.byte 0xa
 5830 1227 02          	.byte 0x2
 5831 1228 23          	.byte 0x23
 5832 1229 00          	.uleb128 0x0
 5833 122a 05          	.uleb128 0x5
 5834 122b 49 43 34 49 	.asciz "IC4IE"
 5834      45 00 
 5835 1231 02          	.byte 0x2
 5836 1232 E7 25       	.2byte 0x25e7
 5837 1234 EA 00 00 00 	.4byte 0xea
 5838 1238 02          	.byte 0x2
 5839 1239 01          	.byte 0x1
 5840 123a 09          	.byte 0x9
 5841 123b 02          	.byte 0x2
 5842 123c 23          	.byte 0x23
 5843 123d 00          	.uleb128 0x0
 5844 123e 05          	.uleb128 0x5
 5845 123f 49 43 35 49 	.asciz "IC5IE"
 5845      45 00 
 5846 1245 02          	.byte 0x2
 5847 1246 E8 25       	.2byte 0x25e8
 5848 1248 EA 00 00 00 	.4byte 0xea
 5849 124c 02          	.byte 0x2
 5850 124d 01          	.byte 0x1
 5851 124e 08          	.byte 0x8
 5852 124f 02          	.byte 0x2
 5853 1250 23          	.byte 0x23
 5854 1251 00          	.uleb128 0x0
 5855 1252 05          	.uleb128 0x5
 5856 1253 49 43 36 49 	.asciz "IC6IE"
 5856      45 00 
 5857 1259 02          	.byte 0x2
 5858 125a E9 25       	.2byte 0x25e9
 5859 125c EA 00 00 00 	.4byte 0xea
 5860 1260 02          	.byte 0x2
 5861 1261 01          	.byte 0x1
 5862 1262 07          	.byte 0x7
 5863 1263 02          	.byte 0x2
 5864 1264 23          	.byte 0x23
 5865 1265 00          	.uleb128 0x0
 5866 1266 05          	.uleb128 0x5
 5867 1267 4F 43 35 49 	.asciz "OC5IE"
 5867      45 00 
 5868 126d 02          	.byte 0x2
 5869 126e EA 25       	.2byte 0x25ea
 5870 1270 EA 00 00 00 	.4byte 0xea
 5871 1274 02          	.byte 0x2
 5872 1275 01          	.byte 0x1
 5873 1276 06          	.byte 0x6
 5874 1277 02          	.byte 0x2
 5875 1278 23          	.byte 0x23
 5876 1279 00          	.uleb128 0x0
 5877 127a 05          	.uleb128 0x5
MPLAB XC16 ASSEMBLY Listing:   			page 122


 5878 127b 4F 43 36 49 	.asciz "OC6IE"
 5878      45 00 
 5879 1281 02          	.byte 0x2
 5880 1282 EB 25       	.2byte 0x25eb
 5881 1284 EA 00 00 00 	.4byte 0xea
 5882 1288 02          	.byte 0x2
 5883 1289 01          	.byte 0x1
 5884 128a 05          	.byte 0x5
 5885 128b 02          	.byte 0x2
 5886 128c 23          	.byte 0x23
 5887 128d 00          	.uleb128 0x0
 5888 128e 05          	.uleb128 0x5
 5889 128f 4F 43 37 49 	.asciz "OC7IE"
 5889      45 00 
 5890 1295 02          	.byte 0x2
 5891 1296 EC 25       	.2byte 0x25ec
 5892 1298 EA 00 00 00 	.4byte 0xea
 5893 129c 02          	.byte 0x2
 5894 129d 01          	.byte 0x1
 5895 129e 04          	.byte 0x4
 5896 129f 02          	.byte 0x2
 5897 12a0 23          	.byte 0x23
 5898 12a1 00          	.uleb128 0x0
 5899 12a2 05          	.uleb128 0x5
 5900 12a3 4F 43 38 49 	.asciz "OC8IE"
 5900      45 00 
 5901 12a9 02          	.byte 0x2
 5902 12aa ED 25       	.2byte 0x25ed
 5903 12ac EA 00 00 00 	.4byte 0xea
 5904 12b0 02          	.byte 0x2
 5905 12b1 01          	.byte 0x1
 5906 12b2 03          	.byte 0x3
 5907 12b3 02          	.byte 0x2
 5908 12b4 23          	.byte 0x23
 5909 12b5 00          	.uleb128 0x0
 5910 12b6 05          	.uleb128 0x5
 5911 12b7 50 4D 50 49 	.asciz "PMPIE"
 5911      45 00 
 5912 12bd 02          	.byte 0x2
 5913 12be EE 25       	.2byte 0x25ee
 5914 12c0 EA 00 00 00 	.4byte 0xea
 5915 12c4 02          	.byte 0x2
 5916 12c5 01          	.byte 0x1
 5917 12c6 02          	.byte 0x2
 5918 12c7 02          	.byte 0x2
 5919 12c8 23          	.byte 0x23
 5920 12c9 00          	.uleb128 0x0
 5921 12ca 05          	.uleb128 0x5
 5922 12cb 44 4D 41 34 	.asciz "DMA4IE"
 5922      49 45 00 
 5923 12d2 02          	.byte 0x2
 5924 12d3 EF 25       	.2byte 0x25ef
 5925 12d5 EA 00 00 00 	.4byte 0xea
 5926 12d9 02          	.byte 0x2
 5927 12da 01          	.byte 0x1
 5928 12db 01          	.byte 0x1
 5929 12dc 02          	.byte 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 123


 5930 12dd 23          	.byte 0x23
 5931 12de 00          	.uleb128 0x0
 5932 12df 05          	.uleb128 0x5
 5933 12e0 54 36 49 45 	.asciz "T6IE"
 5933      00 
 5934 12e5 02          	.byte 0x2
 5935 12e6 F0 25       	.2byte 0x25f0
 5936 12e8 EA 00 00 00 	.4byte 0xea
 5937 12ec 02          	.byte 0x2
 5938 12ed 01          	.byte 0x1
 5939 12ee 10          	.byte 0x10
 5940 12ef 02          	.byte 0x2
 5941 12f0 23          	.byte 0x23
 5942 12f1 00          	.uleb128 0x0
 5943 12f2 00          	.byte 0x0
 5944 12f3 0B          	.uleb128 0xb
 5945 12f4 49 45 43 32 	.asciz "IEC2BITS"
 5945      42 49 54 53 
 5945      00 
 5946 12fd 02          	.byte 0x2
 5947 12fe F1 25       	.2byte 0x25f1
 5948 1300 99 11 00 00 	.4byte 0x1199
 5949 1304 09          	.uleb128 0x9
 5950 1305 74 61 67 49 	.asciz "tagIEC3BITS"
 5950      45 43 33 42 
 5950      49 54 53 00 
 5951 1311 02          	.byte 0x2
 5952 1312 02          	.byte 0x2
 5953 1313 F6 25       	.2byte 0x25f6
 5954 1315 4D 14 00 00 	.4byte 0x144d
 5955 1319 05          	.uleb128 0x5
 5956 131a 54 37 49 45 	.asciz "T7IE"
 5956      00 
 5957 131f 02          	.byte 0x2
 5958 1320 F7 25       	.2byte 0x25f7
 5959 1322 EA 00 00 00 	.4byte 0xea
 5960 1326 02          	.byte 0x2
 5961 1327 01          	.byte 0x1
 5962 1328 0F          	.byte 0xf
 5963 1329 02          	.byte 0x2
 5964 132a 23          	.byte 0x23
 5965 132b 00          	.uleb128 0x0
 5966 132c 05          	.uleb128 0x5
 5967 132d 53 49 32 43 	.asciz "SI2C2IE"
 5967      32 49 45 00 
 5968 1335 02          	.byte 0x2
 5969 1336 F8 25       	.2byte 0x25f8
 5970 1338 EA 00 00 00 	.4byte 0xea
 5971 133c 02          	.byte 0x2
 5972 133d 01          	.byte 0x1
 5973 133e 0E          	.byte 0xe
 5974 133f 02          	.byte 0x2
 5975 1340 23          	.byte 0x23
 5976 1341 00          	.uleb128 0x0
 5977 1342 05          	.uleb128 0x5
 5978 1343 4D 49 32 43 	.asciz "MI2C2IE"
 5978      32 49 45 00 
MPLAB XC16 ASSEMBLY Listing:   			page 124


 5979 134b 02          	.byte 0x2
 5980 134c F9 25       	.2byte 0x25f9
 5981 134e EA 00 00 00 	.4byte 0xea
 5982 1352 02          	.byte 0x2
 5983 1353 01          	.byte 0x1
 5984 1354 0D          	.byte 0xd
 5985 1355 02          	.byte 0x2
 5986 1356 23          	.byte 0x23
 5987 1357 00          	.uleb128 0x0
 5988 1358 05          	.uleb128 0x5
 5989 1359 54 38 49 45 	.asciz "T8IE"
 5989      00 
 5990 135e 02          	.byte 0x2
 5991 135f FA 25       	.2byte 0x25fa
 5992 1361 EA 00 00 00 	.4byte 0xea
 5993 1365 02          	.byte 0x2
 5994 1366 01          	.byte 0x1
 5995 1367 0C          	.byte 0xc
 5996 1368 02          	.byte 0x2
 5997 1369 23          	.byte 0x23
 5998 136a 00          	.uleb128 0x0
 5999 136b 05          	.uleb128 0x5
 6000 136c 54 39 49 45 	.asciz "T9IE"
 6000      00 
 6001 1371 02          	.byte 0x2
 6002 1372 FB 25       	.2byte 0x25fb
 6003 1374 EA 00 00 00 	.4byte 0xea
 6004 1378 02          	.byte 0x2
 6005 1379 01          	.byte 0x1
 6006 137a 0B          	.byte 0xb
 6007 137b 02          	.byte 0x2
 6008 137c 23          	.byte 0x23
 6009 137d 00          	.uleb128 0x0
 6010 137e 05          	.uleb128 0x5
 6011 137f 49 4E 54 33 	.asciz "INT3IE"
 6011      49 45 00 
 6012 1386 02          	.byte 0x2
 6013 1387 FC 25       	.2byte 0x25fc
 6014 1389 EA 00 00 00 	.4byte 0xea
 6015 138d 02          	.byte 0x2
 6016 138e 01          	.byte 0x1
 6017 138f 0A          	.byte 0xa
 6018 1390 02          	.byte 0x2
 6019 1391 23          	.byte 0x23
 6020 1392 00          	.uleb128 0x0
 6021 1393 05          	.uleb128 0x5
 6022 1394 49 4E 54 34 	.asciz "INT4IE"
 6022      49 45 00 
 6023 139b 02          	.byte 0x2
 6024 139c FD 25       	.2byte 0x25fd
 6025 139e EA 00 00 00 	.4byte 0xea
 6026 13a2 02          	.byte 0x2
 6027 13a3 01          	.byte 0x1
 6028 13a4 09          	.byte 0x9
 6029 13a5 02          	.byte 0x2
 6030 13a6 23          	.byte 0x23
 6031 13a7 00          	.uleb128 0x0
MPLAB XC16 ASSEMBLY Listing:   			page 125


 6032 13a8 05          	.uleb128 0x5
 6033 13a9 43 32 52 58 	.asciz "C2RXIE"
 6033      49 45 00 
 6034 13b0 02          	.byte 0x2
 6035 13b1 FE 25       	.2byte 0x25fe
 6036 13b3 EA 00 00 00 	.4byte 0xea
 6037 13b7 02          	.byte 0x2
 6038 13b8 01          	.byte 0x1
 6039 13b9 08          	.byte 0x8
 6040 13ba 02          	.byte 0x2
 6041 13bb 23          	.byte 0x23
 6042 13bc 00          	.uleb128 0x0
 6043 13bd 05          	.uleb128 0x5
 6044 13be 43 32 49 45 	.asciz "C2IE"
 6044      00 
 6045 13c3 02          	.byte 0x2
 6046 13c4 FF 25       	.2byte 0x25ff
 6047 13c6 EA 00 00 00 	.4byte 0xea
 6048 13ca 02          	.byte 0x2
 6049 13cb 01          	.byte 0x1
 6050 13cc 07          	.byte 0x7
 6051 13cd 02          	.byte 0x2
 6052 13ce 23          	.byte 0x23
 6053 13cf 00          	.uleb128 0x0
 6054 13d0 05          	.uleb128 0x5
 6055 13d1 50 53 45 4D 	.asciz "PSEMIE"
 6055      49 45 00 
 6056 13d8 02          	.byte 0x2
 6057 13d9 00 26       	.2byte 0x2600
 6058 13db EA 00 00 00 	.4byte 0xea
 6059 13df 02          	.byte 0x2
 6060 13e0 01          	.byte 0x1
 6061 13e1 06          	.byte 0x6
 6062 13e2 02          	.byte 0x2
 6063 13e3 23          	.byte 0x23
 6064 13e4 00          	.uleb128 0x0
 6065 13e5 05          	.uleb128 0x5
 6066 13e6 51 45 49 31 	.asciz "QEI1IE"
 6066      49 45 00 
 6067 13ed 02          	.byte 0x2
 6068 13ee 01 26       	.2byte 0x2601
 6069 13f0 EA 00 00 00 	.4byte 0xea
 6070 13f4 02          	.byte 0x2
 6071 13f5 01          	.byte 0x1
 6072 13f6 05          	.byte 0x5
 6073 13f7 02          	.byte 0x2
 6074 13f8 23          	.byte 0x23
 6075 13f9 00          	.uleb128 0x0
 6076 13fa 05          	.uleb128 0x5
 6077 13fb 44 43 49 45 	.asciz "DCIEIE"
 6077      49 45 00 
 6078 1402 02          	.byte 0x2
 6079 1403 02 26       	.2byte 0x2602
 6080 1405 EA 00 00 00 	.4byte 0xea
 6081 1409 02          	.byte 0x2
 6082 140a 01          	.byte 0x1
 6083 140b 04          	.byte 0x4
MPLAB XC16 ASSEMBLY Listing:   			page 126


 6084 140c 02          	.byte 0x2
 6085 140d 23          	.byte 0x23
 6086 140e 00          	.uleb128 0x0
 6087 140f 05          	.uleb128 0x5
 6088 1410 44 43 49 49 	.asciz "DCIIE"
 6088      45 00 
 6089 1416 02          	.byte 0x2
 6090 1417 03 26       	.2byte 0x2603
 6091 1419 EA 00 00 00 	.4byte 0xea
 6092 141d 02          	.byte 0x2
 6093 141e 01          	.byte 0x1
 6094 141f 03          	.byte 0x3
 6095 1420 02          	.byte 0x2
 6096 1421 23          	.byte 0x23
 6097 1422 00          	.uleb128 0x0
 6098 1423 05          	.uleb128 0x5
 6099 1424 44 4D 41 35 	.asciz "DMA5IE"
 6099      49 45 00 
 6100 142b 02          	.byte 0x2
 6101 142c 04 26       	.2byte 0x2604
 6102 142e EA 00 00 00 	.4byte 0xea
 6103 1432 02          	.byte 0x2
 6104 1433 01          	.byte 0x1
 6105 1434 02          	.byte 0x2
 6106 1435 02          	.byte 0x2
 6107 1436 23          	.byte 0x23
 6108 1437 00          	.uleb128 0x0
 6109 1438 05          	.uleb128 0x5
 6110 1439 52 54 43 49 	.asciz "RTCIE"
 6110      45 00 
 6111 143f 02          	.byte 0x2
 6112 1440 05 26       	.2byte 0x2605
 6113 1442 EA 00 00 00 	.4byte 0xea
 6114 1446 02          	.byte 0x2
 6115 1447 01          	.byte 0x1
 6116 1448 01          	.byte 0x1
 6117 1449 02          	.byte 0x2
 6118 144a 23          	.byte 0x23
 6119 144b 00          	.uleb128 0x0
 6120 144c 00          	.byte 0x0
 6121 144d 0B          	.uleb128 0xb
 6122 144e 49 45 43 33 	.asciz "IEC3BITS"
 6122      42 49 54 53 
 6122      00 
 6123 1457 02          	.byte 0x2
 6124 1458 06 26       	.2byte 0x2606
 6125 145a 04 13 00 00 	.4byte 0x1304
 6126 145e 04          	.uleb128 0x4
 6127 145f 02          	.byte 0x2
 6128 1460 02          	.byte 0x2
 6129 1461 62 26       	.2byte 0x2662
 6130 1463 B8 14 00 00 	.4byte 0x14b8
 6131 1467 05          	.uleb128 0x5
 6132 1468 49 4E 54 30 	.asciz "INT0IP"
 6132      49 50 00 
 6133 146f 02          	.byte 0x2
 6134 1470 63 26       	.2byte 0x2663
MPLAB XC16 ASSEMBLY Listing:   			page 127


 6135 1472 EA 00 00 00 	.4byte 0xea
 6136 1476 02          	.byte 0x2
 6137 1477 03          	.byte 0x3
 6138 1478 0D          	.byte 0xd
 6139 1479 02          	.byte 0x2
 6140 147a 23          	.byte 0x23
 6141 147b 00          	.uleb128 0x0
 6142 147c 05          	.uleb128 0x5
 6143 147d 49 43 31 49 	.asciz "IC1IP"
 6143      50 00 
 6144 1483 02          	.byte 0x2
 6145 1484 65 26       	.2byte 0x2665
 6146 1486 EA 00 00 00 	.4byte 0xea
 6147 148a 02          	.byte 0x2
 6148 148b 03          	.byte 0x3
 6149 148c 09          	.byte 0x9
 6150 148d 02          	.byte 0x2
 6151 148e 23          	.byte 0x23
 6152 148f 00          	.uleb128 0x0
 6153 1490 05          	.uleb128 0x5
 6154 1491 4F 43 31 49 	.asciz "OC1IP"
 6154      50 00 
 6155 1497 02          	.byte 0x2
 6156 1498 67 26       	.2byte 0x2667
 6157 149a EA 00 00 00 	.4byte 0xea
 6158 149e 02          	.byte 0x2
 6159 149f 03          	.byte 0x3
 6160 14a0 05          	.byte 0x5
 6161 14a1 02          	.byte 0x2
 6162 14a2 23          	.byte 0x23
 6163 14a3 00          	.uleb128 0x0
 6164 14a4 05          	.uleb128 0x5
 6165 14a5 54 31 49 50 	.asciz "T1IP"
 6165      00 
 6166 14aa 02          	.byte 0x2
 6167 14ab 69 26       	.2byte 0x2669
 6168 14ad EA 00 00 00 	.4byte 0xea
 6169 14b1 02          	.byte 0x2
 6170 14b2 03          	.byte 0x3
 6171 14b3 01          	.byte 0x1
 6172 14b4 02          	.byte 0x2
 6173 14b5 23          	.byte 0x23
 6174 14b6 00          	.uleb128 0x0
 6175 14b7 00          	.byte 0x0
 6176 14b8 04          	.uleb128 0x4
 6177 14b9 02          	.byte 0x2
 6178 14ba 02          	.byte 0x2
 6179 14bb 6B 26       	.2byte 0x266b
 6180 14bd BE 15 00 00 	.4byte 0x15be
 6181 14c1 05          	.uleb128 0x5
 6182 14c2 49 4E 54 30 	.asciz "INT0IP0"
 6182      49 50 30 00 
 6183 14ca 02          	.byte 0x2
 6184 14cb 6C 26       	.2byte 0x266c
 6185 14cd EA 00 00 00 	.4byte 0xea
 6186 14d1 02          	.byte 0x2
 6187 14d2 01          	.byte 0x1
MPLAB XC16 ASSEMBLY Listing:   			page 128


 6188 14d3 0F          	.byte 0xf
 6189 14d4 02          	.byte 0x2
 6190 14d5 23          	.byte 0x23
 6191 14d6 00          	.uleb128 0x0
 6192 14d7 05          	.uleb128 0x5
 6193 14d8 49 4E 54 30 	.asciz "INT0IP1"
 6193      49 50 31 00 
 6194 14e0 02          	.byte 0x2
 6195 14e1 6D 26       	.2byte 0x266d
 6196 14e3 EA 00 00 00 	.4byte 0xea
 6197 14e7 02          	.byte 0x2
 6198 14e8 01          	.byte 0x1
 6199 14e9 0E          	.byte 0xe
 6200 14ea 02          	.byte 0x2
 6201 14eb 23          	.byte 0x23
 6202 14ec 00          	.uleb128 0x0
 6203 14ed 05          	.uleb128 0x5
 6204 14ee 49 4E 54 30 	.asciz "INT0IP2"
 6204      49 50 32 00 
 6205 14f6 02          	.byte 0x2
 6206 14f7 6E 26       	.2byte 0x266e
 6207 14f9 EA 00 00 00 	.4byte 0xea
 6208 14fd 02          	.byte 0x2
 6209 14fe 01          	.byte 0x1
 6210 14ff 0D          	.byte 0xd
 6211 1500 02          	.byte 0x2
 6212 1501 23          	.byte 0x23
 6213 1502 00          	.uleb128 0x0
 6214 1503 05          	.uleb128 0x5
 6215 1504 49 43 31 49 	.asciz "IC1IP0"
 6215      50 30 00 
 6216 150b 02          	.byte 0x2
 6217 150c 70 26       	.2byte 0x2670
 6218 150e EA 00 00 00 	.4byte 0xea
 6219 1512 02          	.byte 0x2
 6220 1513 01          	.byte 0x1
 6221 1514 0B          	.byte 0xb
 6222 1515 02          	.byte 0x2
 6223 1516 23          	.byte 0x23
 6224 1517 00          	.uleb128 0x0
 6225 1518 05          	.uleb128 0x5
 6226 1519 49 43 31 49 	.asciz "IC1IP1"
 6226      50 31 00 
 6227 1520 02          	.byte 0x2
 6228 1521 71 26       	.2byte 0x2671
 6229 1523 EA 00 00 00 	.4byte 0xea
 6230 1527 02          	.byte 0x2
 6231 1528 01          	.byte 0x1
 6232 1529 0A          	.byte 0xa
 6233 152a 02          	.byte 0x2
 6234 152b 23          	.byte 0x23
 6235 152c 00          	.uleb128 0x0
 6236 152d 05          	.uleb128 0x5
 6237 152e 49 43 31 49 	.asciz "IC1IP2"
 6237      50 32 00 
 6238 1535 02          	.byte 0x2
 6239 1536 72 26       	.2byte 0x2672
MPLAB XC16 ASSEMBLY Listing:   			page 129


 6240 1538 EA 00 00 00 	.4byte 0xea
 6241 153c 02          	.byte 0x2
 6242 153d 01          	.byte 0x1
 6243 153e 09          	.byte 0x9
 6244 153f 02          	.byte 0x2
 6245 1540 23          	.byte 0x23
 6246 1541 00          	.uleb128 0x0
 6247 1542 05          	.uleb128 0x5
 6248 1543 4F 43 31 49 	.asciz "OC1IP0"
 6248      50 30 00 
 6249 154a 02          	.byte 0x2
 6250 154b 74 26       	.2byte 0x2674
 6251 154d EA 00 00 00 	.4byte 0xea
 6252 1551 02          	.byte 0x2
 6253 1552 01          	.byte 0x1
 6254 1553 07          	.byte 0x7
 6255 1554 02          	.byte 0x2
 6256 1555 23          	.byte 0x23
 6257 1556 00          	.uleb128 0x0
 6258 1557 05          	.uleb128 0x5
 6259 1558 4F 43 31 49 	.asciz "OC1IP1"
 6259      50 31 00 
 6260 155f 02          	.byte 0x2
 6261 1560 75 26       	.2byte 0x2675
 6262 1562 EA 00 00 00 	.4byte 0xea
 6263 1566 02          	.byte 0x2
 6264 1567 01          	.byte 0x1
 6265 1568 06          	.byte 0x6
 6266 1569 02          	.byte 0x2
 6267 156a 23          	.byte 0x23
 6268 156b 00          	.uleb128 0x0
 6269 156c 05          	.uleb128 0x5
 6270 156d 4F 43 31 49 	.asciz "OC1IP2"
 6270      50 32 00 
 6271 1574 02          	.byte 0x2
 6272 1575 76 26       	.2byte 0x2676
 6273 1577 EA 00 00 00 	.4byte 0xea
 6274 157b 02          	.byte 0x2
 6275 157c 01          	.byte 0x1
 6276 157d 05          	.byte 0x5
 6277 157e 02          	.byte 0x2
 6278 157f 23          	.byte 0x23
 6279 1580 00          	.uleb128 0x0
 6280 1581 05          	.uleb128 0x5
 6281 1582 54 31 49 50 	.asciz "T1IP0"
 6281      30 00 
 6282 1588 02          	.byte 0x2
 6283 1589 78 26       	.2byte 0x2678
 6284 158b EA 00 00 00 	.4byte 0xea
 6285 158f 02          	.byte 0x2
 6286 1590 01          	.byte 0x1
 6287 1591 03          	.byte 0x3
 6288 1592 02          	.byte 0x2
 6289 1593 23          	.byte 0x23
 6290 1594 00          	.uleb128 0x0
 6291 1595 05          	.uleb128 0x5
 6292 1596 54 31 49 50 	.asciz "T1IP1"
MPLAB XC16 ASSEMBLY Listing:   			page 130


 6292      31 00 
 6293 159c 02          	.byte 0x2
 6294 159d 79 26       	.2byte 0x2679
 6295 159f EA 00 00 00 	.4byte 0xea
 6296 15a3 02          	.byte 0x2
 6297 15a4 01          	.byte 0x1
 6298 15a5 02          	.byte 0x2
 6299 15a6 02          	.byte 0x2
 6300 15a7 23          	.byte 0x23
 6301 15a8 00          	.uleb128 0x0
 6302 15a9 05          	.uleb128 0x5
 6303 15aa 54 31 49 50 	.asciz "T1IP2"
 6303      32 00 
 6304 15b0 02          	.byte 0x2
 6305 15b1 7A 26       	.2byte 0x267a
 6306 15b3 EA 00 00 00 	.4byte 0xea
 6307 15b7 02          	.byte 0x2
 6308 15b8 01          	.byte 0x1
 6309 15b9 01          	.byte 0x1
 6310 15ba 02          	.byte 0x2
 6311 15bb 23          	.byte 0x23
 6312 15bc 00          	.uleb128 0x0
 6313 15bd 00          	.byte 0x0
 6314 15be 07          	.uleb128 0x7
 6315 15bf 02          	.byte 0x2
 6316 15c0 02          	.byte 0x2
 6317 15c1 61 26       	.2byte 0x2661
 6318 15c3 D2 15 00 00 	.4byte 0x15d2
 6319 15c7 08          	.uleb128 0x8
 6320 15c8 5E 14 00 00 	.4byte 0x145e
 6321 15cc 08          	.uleb128 0x8
 6322 15cd B8 14 00 00 	.4byte 0x14b8
 6323 15d1 00          	.byte 0x0
 6324 15d2 09          	.uleb128 0x9
 6325 15d3 74 61 67 49 	.asciz "tagIPC0BITS"
 6325      50 43 30 42 
 6325      49 54 53 00 
 6326 15df 02          	.byte 0x2
 6327 15e0 02          	.byte 0x2
 6328 15e1 60 26       	.2byte 0x2660
 6329 15e3 F0 15 00 00 	.4byte 0x15f0
 6330 15e7 0A          	.uleb128 0xa
 6331 15e8 BE 15 00 00 	.4byte 0x15be
 6332 15ec 02          	.byte 0x2
 6333 15ed 23          	.byte 0x23
 6334 15ee 00          	.uleb128 0x0
 6335 15ef 00          	.byte 0x0
 6336 15f0 0B          	.uleb128 0xb
 6337 15f1 49 50 43 30 	.asciz "IPC0BITS"
 6337      42 49 54 53 
 6337      00 
 6338 15fa 02          	.byte 0x2
 6339 15fb 7D 26       	.2byte 0x267d
 6340 15fd D2 15 00 00 	.4byte 0x15d2
 6341 1601 04          	.uleb128 0x4
 6342 1602 02          	.byte 0x2
 6343 1603 02          	.byte 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 131


 6344 1604 84 26       	.2byte 0x2684
 6345 1606 5B 16 00 00 	.4byte 0x165b
 6346 160a 05          	.uleb128 0x5
 6347 160b 44 4D 41 30 	.asciz "DMA0IP"
 6347      49 50 00 
 6348 1612 02          	.byte 0x2
 6349 1613 85 26       	.2byte 0x2685
 6350 1615 EA 00 00 00 	.4byte 0xea
 6351 1619 02          	.byte 0x2
 6352 161a 03          	.byte 0x3
 6353 161b 0D          	.byte 0xd
 6354 161c 02          	.byte 0x2
 6355 161d 23          	.byte 0x23
 6356 161e 00          	.uleb128 0x0
 6357 161f 05          	.uleb128 0x5
 6358 1620 49 43 32 49 	.asciz "IC2IP"
 6358      50 00 
 6359 1626 02          	.byte 0x2
 6360 1627 87 26       	.2byte 0x2687
 6361 1629 EA 00 00 00 	.4byte 0xea
 6362 162d 02          	.byte 0x2
 6363 162e 03          	.byte 0x3
 6364 162f 09          	.byte 0x9
 6365 1630 02          	.byte 0x2
 6366 1631 23          	.byte 0x23
 6367 1632 00          	.uleb128 0x0
 6368 1633 05          	.uleb128 0x5
 6369 1634 4F 43 32 49 	.asciz "OC2IP"
 6369      50 00 
 6370 163a 02          	.byte 0x2
 6371 163b 89 26       	.2byte 0x2689
 6372 163d EA 00 00 00 	.4byte 0xea
 6373 1641 02          	.byte 0x2
 6374 1642 03          	.byte 0x3
 6375 1643 05          	.byte 0x5
 6376 1644 02          	.byte 0x2
 6377 1645 23          	.byte 0x23
 6378 1646 00          	.uleb128 0x0
 6379 1647 05          	.uleb128 0x5
 6380 1648 54 32 49 50 	.asciz "T2IP"
 6380      00 
 6381 164d 02          	.byte 0x2
 6382 164e 8B 26       	.2byte 0x268b
 6383 1650 EA 00 00 00 	.4byte 0xea
 6384 1654 02          	.byte 0x2
 6385 1655 03          	.byte 0x3
 6386 1656 01          	.byte 0x1
 6387 1657 02          	.byte 0x2
 6388 1658 23          	.byte 0x23
 6389 1659 00          	.uleb128 0x0
 6390 165a 00          	.byte 0x0
 6391 165b 04          	.uleb128 0x4
 6392 165c 02          	.byte 0x2
 6393 165d 02          	.byte 0x2
 6394 165e 8D 26       	.2byte 0x268d
 6395 1660 61 17 00 00 	.4byte 0x1761
 6396 1664 05          	.uleb128 0x5
MPLAB XC16 ASSEMBLY Listing:   			page 132


 6397 1665 44 4D 41 30 	.asciz "DMA0IP0"
 6397      49 50 30 00 
 6398 166d 02          	.byte 0x2
 6399 166e 8E 26       	.2byte 0x268e
 6400 1670 EA 00 00 00 	.4byte 0xea
 6401 1674 02          	.byte 0x2
 6402 1675 01          	.byte 0x1
 6403 1676 0F          	.byte 0xf
 6404 1677 02          	.byte 0x2
 6405 1678 23          	.byte 0x23
 6406 1679 00          	.uleb128 0x0
 6407 167a 05          	.uleb128 0x5
 6408 167b 44 4D 41 30 	.asciz "DMA0IP1"
 6408      49 50 31 00 
 6409 1683 02          	.byte 0x2
 6410 1684 8F 26       	.2byte 0x268f
 6411 1686 EA 00 00 00 	.4byte 0xea
 6412 168a 02          	.byte 0x2
 6413 168b 01          	.byte 0x1
 6414 168c 0E          	.byte 0xe
 6415 168d 02          	.byte 0x2
 6416 168e 23          	.byte 0x23
 6417 168f 00          	.uleb128 0x0
 6418 1690 05          	.uleb128 0x5
 6419 1691 44 4D 41 30 	.asciz "DMA0IP2"
 6419      49 50 32 00 
 6420 1699 02          	.byte 0x2
 6421 169a 90 26       	.2byte 0x2690
 6422 169c EA 00 00 00 	.4byte 0xea
 6423 16a0 02          	.byte 0x2
 6424 16a1 01          	.byte 0x1
 6425 16a2 0D          	.byte 0xd
 6426 16a3 02          	.byte 0x2
 6427 16a4 23          	.byte 0x23
 6428 16a5 00          	.uleb128 0x0
 6429 16a6 05          	.uleb128 0x5
 6430 16a7 49 43 32 49 	.asciz "IC2IP0"
 6430      50 30 00 
 6431 16ae 02          	.byte 0x2
 6432 16af 92 26       	.2byte 0x2692
 6433 16b1 EA 00 00 00 	.4byte 0xea
 6434 16b5 02          	.byte 0x2
 6435 16b6 01          	.byte 0x1
 6436 16b7 0B          	.byte 0xb
 6437 16b8 02          	.byte 0x2
 6438 16b9 23          	.byte 0x23
 6439 16ba 00          	.uleb128 0x0
 6440 16bb 05          	.uleb128 0x5
 6441 16bc 49 43 32 49 	.asciz "IC2IP1"
 6441      50 31 00 
 6442 16c3 02          	.byte 0x2
 6443 16c4 93 26       	.2byte 0x2693
 6444 16c6 EA 00 00 00 	.4byte 0xea
 6445 16ca 02          	.byte 0x2
 6446 16cb 01          	.byte 0x1
 6447 16cc 0A          	.byte 0xa
 6448 16cd 02          	.byte 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 133


 6449 16ce 23          	.byte 0x23
 6450 16cf 00          	.uleb128 0x0
 6451 16d0 05          	.uleb128 0x5
 6452 16d1 49 43 32 49 	.asciz "IC2IP2"
 6452      50 32 00 
 6453 16d8 02          	.byte 0x2
 6454 16d9 94 26       	.2byte 0x2694
 6455 16db EA 00 00 00 	.4byte 0xea
 6456 16df 02          	.byte 0x2
 6457 16e0 01          	.byte 0x1
 6458 16e1 09          	.byte 0x9
 6459 16e2 02          	.byte 0x2
 6460 16e3 23          	.byte 0x23
 6461 16e4 00          	.uleb128 0x0
 6462 16e5 05          	.uleb128 0x5
 6463 16e6 4F 43 32 49 	.asciz "OC2IP0"
 6463      50 30 00 
 6464 16ed 02          	.byte 0x2
 6465 16ee 96 26       	.2byte 0x2696
 6466 16f0 EA 00 00 00 	.4byte 0xea
 6467 16f4 02          	.byte 0x2
 6468 16f5 01          	.byte 0x1
 6469 16f6 07          	.byte 0x7
 6470 16f7 02          	.byte 0x2
 6471 16f8 23          	.byte 0x23
 6472 16f9 00          	.uleb128 0x0
 6473 16fa 05          	.uleb128 0x5
 6474 16fb 4F 43 32 49 	.asciz "OC2IP1"
 6474      50 31 00 
 6475 1702 02          	.byte 0x2
 6476 1703 97 26       	.2byte 0x2697
 6477 1705 EA 00 00 00 	.4byte 0xea
 6478 1709 02          	.byte 0x2
 6479 170a 01          	.byte 0x1
 6480 170b 06          	.byte 0x6
 6481 170c 02          	.byte 0x2
 6482 170d 23          	.byte 0x23
 6483 170e 00          	.uleb128 0x0
 6484 170f 05          	.uleb128 0x5
 6485 1710 4F 43 32 49 	.asciz "OC2IP2"
 6485      50 32 00 
 6486 1717 02          	.byte 0x2
 6487 1718 98 26       	.2byte 0x2698
 6488 171a EA 00 00 00 	.4byte 0xea
 6489 171e 02          	.byte 0x2
 6490 171f 01          	.byte 0x1
 6491 1720 05          	.byte 0x5
 6492 1721 02          	.byte 0x2
 6493 1722 23          	.byte 0x23
 6494 1723 00          	.uleb128 0x0
 6495 1724 05          	.uleb128 0x5
 6496 1725 54 32 49 50 	.asciz "T2IP0"
 6496      30 00 
 6497 172b 02          	.byte 0x2
 6498 172c 9A 26       	.2byte 0x269a
 6499 172e EA 00 00 00 	.4byte 0xea
 6500 1732 02          	.byte 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 134


 6501 1733 01          	.byte 0x1
 6502 1734 03          	.byte 0x3
 6503 1735 02          	.byte 0x2
 6504 1736 23          	.byte 0x23
 6505 1737 00          	.uleb128 0x0
 6506 1738 05          	.uleb128 0x5
 6507 1739 54 32 49 50 	.asciz "T2IP1"
 6507      31 00 
 6508 173f 02          	.byte 0x2
 6509 1740 9B 26       	.2byte 0x269b
 6510 1742 EA 00 00 00 	.4byte 0xea
 6511 1746 02          	.byte 0x2
 6512 1747 01          	.byte 0x1
 6513 1748 02          	.byte 0x2
 6514 1749 02          	.byte 0x2
 6515 174a 23          	.byte 0x23
 6516 174b 00          	.uleb128 0x0
 6517 174c 05          	.uleb128 0x5
 6518 174d 54 32 49 50 	.asciz "T2IP2"
 6518      32 00 
 6519 1753 02          	.byte 0x2
 6520 1754 9C 26       	.2byte 0x269c
 6521 1756 EA 00 00 00 	.4byte 0xea
 6522 175a 02          	.byte 0x2
 6523 175b 01          	.byte 0x1
 6524 175c 01          	.byte 0x1
 6525 175d 02          	.byte 0x2
 6526 175e 23          	.byte 0x23
 6527 175f 00          	.uleb128 0x0
 6528 1760 00          	.byte 0x0
 6529 1761 07          	.uleb128 0x7
 6530 1762 02          	.byte 0x2
 6531 1763 02          	.byte 0x2
 6532 1764 83 26       	.2byte 0x2683
 6533 1766 75 17 00 00 	.4byte 0x1775
 6534 176a 08          	.uleb128 0x8
 6535 176b 01 16 00 00 	.4byte 0x1601
 6536 176f 08          	.uleb128 0x8
 6537 1770 5B 16 00 00 	.4byte 0x165b
 6538 1774 00          	.byte 0x0
 6539 1775 09          	.uleb128 0x9
 6540 1776 74 61 67 49 	.asciz "tagIPC1BITS"
 6540      50 43 31 42 
 6540      49 54 53 00 
 6541 1782 02          	.byte 0x2
 6542 1783 02          	.byte 0x2
 6543 1784 82 26       	.2byte 0x2682
 6544 1786 93 17 00 00 	.4byte 0x1793
 6545 178a 0A          	.uleb128 0xa
 6546 178b 61 17 00 00 	.4byte 0x1761
 6547 178f 02          	.byte 0x2
 6548 1790 23          	.byte 0x23
 6549 1791 00          	.uleb128 0x0
 6550 1792 00          	.byte 0x0
 6551 1793 0B          	.uleb128 0xb
 6552 1794 49 50 43 31 	.asciz "IPC1BITS"
 6552      42 49 54 53 
MPLAB XC16 ASSEMBLY Listing:   			page 135


 6552      00 
 6553 179d 02          	.byte 0x2
 6554 179e 9F 26       	.2byte 0x269f
 6555 17a0 75 17 00 00 	.4byte 0x1775
 6556 17a4 04          	.uleb128 0x4
 6557 17a5 02          	.byte 0x2
 6558 17a6 02          	.byte 0x2
 6559 17a7 A6 26       	.2byte 0x26a6
 6560 17a9 01 18 00 00 	.4byte 0x1801
 6561 17ad 05          	.uleb128 0x5
 6562 17ae 54 33 49 50 	.asciz "T3IP"
 6562      00 
 6563 17b3 02          	.byte 0x2
 6564 17b4 A7 26       	.2byte 0x26a7
 6565 17b6 EA 00 00 00 	.4byte 0xea
 6566 17ba 02          	.byte 0x2
 6567 17bb 03          	.byte 0x3
 6568 17bc 0D          	.byte 0xd
 6569 17bd 02          	.byte 0x2
 6570 17be 23          	.byte 0x23
 6571 17bf 00          	.uleb128 0x0
 6572 17c0 05          	.uleb128 0x5
 6573 17c1 53 50 49 31 	.asciz "SPI1EIP"
 6573      45 49 50 00 
 6574 17c9 02          	.byte 0x2
 6575 17ca A9 26       	.2byte 0x26a9
 6576 17cc EA 00 00 00 	.4byte 0xea
 6577 17d0 02          	.byte 0x2
 6578 17d1 03          	.byte 0x3
 6579 17d2 09          	.byte 0x9
 6580 17d3 02          	.byte 0x2
 6581 17d4 23          	.byte 0x23
 6582 17d5 00          	.uleb128 0x0
 6583 17d6 05          	.uleb128 0x5
 6584 17d7 53 50 49 31 	.asciz "SPI1IP"
 6584      49 50 00 
 6585 17de 02          	.byte 0x2
 6586 17df AB 26       	.2byte 0x26ab
 6587 17e1 EA 00 00 00 	.4byte 0xea
 6588 17e5 02          	.byte 0x2
 6589 17e6 03          	.byte 0x3
 6590 17e7 05          	.byte 0x5
 6591 17e8 02          	.byte 0x2
 6592 17e9 23          	.byte 0x23
 6593 17ea 00          	.uleb128 0x0
 6594 17eb 05          	.uleb128 0x5
 6595 17ec 55 31 52 58 	.asciz "U1RXIP"
 6595      49 50 00 
 6596 17f3 02          	.byte 0x2
 6597 17f4 AD 26       	.2byte 0x26ad
 6598 17f6 EA 00 00 00 	.4byte 0xea
 6599 17fa 02          	.byte 0x2
 6600 17fb 03          	.byte 0x3
 6601 17fc 01          	.byte 0x1
 6602 17fd 02          	.byte 0x2
 6603 17fe 23          	.byte 0x23
 6604 17ff 00          	.uleb128 0x0
MPLAB XC16 ASSEMBLY Listing:   			page 136


 6605 1800 00          	.byte 0x0
 6606 1801 04          	.uleb128 0x4
 6607 1802 02          	.byte 0x2
 6608 1803 02          	.byte 0x2
 6609 1804 AF 26       	.2byte 0x26af
 6610 1806 10 19 00 00 	.4byte 0x1910
 6611 180a 05          	.uleb128 0x5
 6612 180b 54 33 49 50 	.asciz "T3IP0"
 6612      30 00 
 6613 1811 02          	.byte 0x2
 6614 1812 B0 26       	.2byte 0x26b0
 6615 1814 EA 00 00 00 	.4byte 0xea
 6616 1818 02          	.byte 0x2
 6617 1819 01          	.byte 0x1
 6618 181a 0F          	.byte 0xf
 6619 181b 02          	.byte 0x2
 6620 181c 23          	.byte 0x23
 6621 181d 00          	.uleb128 0x0
 6622 181e 05          	.uleb128 0x5
 6623 181f 54 33 49 50 	.asciz "T3IP1"
 6623      31 00 
 6624 1825 02          	.byte 0x2
 6625 1826 B1 26       	.2byte 0x26b1
 6626 1828 EA 00 00 00 	.4byte 0xea
 6627 182c 02          	.byte 0x2
 6628 182d 01          	.byte 0x1
 6629 182e 0E          	.byte 0xe
 6630 182f 02          	.byte 0x2
 6631 1830 23          	.byte 0x23
 6632 1831 00          	.uleb128 0x0
 6633 1832 05          	.uleb128 0x5
 6634 1833 54 33 49 50 	.asciz "T3IP2"
 6634      32 00 
 6635 1839 02          	.byte 0x2
 6636 183a B2 26       	.2byte 0x26b2
 6637 183c EA 00 00 00 	.4byte 0xea
 6638 1840 02          	.byte 0x2
 6639 1841 01          	.byte 0x1
 6640 1842 0D          	.byte 0xd
 6641 1843 02          	.byte 0x2
 6642 1844 23          	.byte 0x23
 6643 1845 00          	.uleb128 0x0
 6644 1846 05          	.uleb128 0x5
 6645 1847 53 50 49 31 	.asciz "SPI1EIP0"
 6645      45 49 50 30 
 6645      00 
 6646 1850 02          	.byte 0x2
 6647 1851 B4 26       	.2byte 0x26b4
 6648 1853 EA 00 00 00 	.4byte 0xea
 6649 1857 02          	.byte 0x2
 6650 1858 01          	.byte 0x1
 6651 1859 0B          	.byte 0xb
 6652 185a 02          	.byte 0x2
 6653 185b 23          	.byte 0x23
 6654 185c 00          	.uleb128 0x0
 6655 185d 05          	.uleb128 0x5
 6656 185e 53 50 49 31 	.asciz "SPI1EIP1"
MPLAB XC16 ASSEMBLY Listing:   			page 137


 6656      45 49 50 31 
 6656      00 
 6657 1867 02          	.byte 0x2
 6658 1868 B5 26       	.2byte 0x26b5
 6659 186a EA 00 00 00 	.4byte 0xea
 6660 186e 02          	.byte 0x2
 6661 186f 01          	.byte 0x1
 6662 1870 0A          	.byte 0xa
 6663 1871 02          	.byte 0x2
 6664 1872 23          	.byte 0x23
 6665 1873 00          	.uleb128 0x0
 6666 1874 05          	.uleb128 0x5
 6667 1875 53 50 49 31 	.asciz "SPI1EIP2"
 6667      45 49 50 32 
 6667      00 
 6668 187e 02          	.byte 0x2
 6669 187f B6 26       	.2byte 0x26b6
 6670 1881 EA 00 00 00 	.4byte 0xea
 6671 1885 02          	.byte 0x2
 6672 1886 01          	.byte 0x1
 6673 1887 09          	.byte 0x9
 6674 1888 02          	.byte 0x2
 6675 1889 23          	.byte 0x23
 6676 188a 00          	.uleb128 0x0
 6677 188b 05          	.uleb128 0x5
 6678 188c 53 50 49 31 	.asciz "SPI1IP0"
 6678      49 50 30 00 
 6679 1894 02          	.byte 0x2
 6680 1895 B8 26       	.2byte 0x26b8
 6681 1897 EA 00 00 00 	.4byte 0xea
 6682 189b 02          	.byte 0x2
 6683 189c 01          	.byte 0x1
 6684 189d 07          	.byte 0x7
 6685 189e 02          	.byte 0x2
 6686 189f 23          	.byte 0x23
 6687 18a0 00          	.uleb128 0x0
 6688 18a1 05          	.uleb128 0x5
 6689 18a2 53 50 49 31 	.asciz "SPI1IP1"
 6689      49 50 31 00 
 6690 18aa 02          	.byte 0x2
 6691 18ab B9 26       	.2byte 0x26b9
 6692 18ad EA 00 00 00 	.4byte 0xea
 6693 18b1 02          	.byte 0x2
 6694 18b2 01          	.byte 0x1
 6695 18b3 06          	.byte 0x6
 6696 18b4 02          	.byte 0x2
 6697 18b5 23          	.byte 0x23
 6698 18b6 00          	.uleb128 0x0
 6699 18b7 05          	.uleb128 0x5
 6700 18b8 53 50 49 31 	.asciz "SPI1IP2"
 6700      49 50 32 00 
 6701 18c0 02          	.byte 0x2
 6702 18c1 BA 26       	.2byte 0x26ba
 6703 18c3 EA 00 00 00 	.4byte 0xea
 6704 18c7 02          	.byte 0x2
 6705 18c8 01          	.byte 0x1
 6706 18c9 05          	.byte 0x5
MPLAB XC16 ASSEMBLY Listing:   			page 138


 6707 18ca 02          	.byte 0x2
 6708 18cb 23          	.byte 0x23
 6709 18cc 00          	.uleb128 0x0
 6710 18cd 05          	.uleb128 0x5
 6711 18ce 55 31 52 58 	.asciz "U1RXIP0"
 6711      49 50 30 00 
 6712 18d6 02          	.byte 0x2
 6713 18d7 BC 26       	.2byte 0x26bc
 6714 18d9 EA 00 00 00 	.4byte 0xea
 6715 18dd 02          	.byte 0x2
 6716 18de 01          	.byte 0x1
 6717 18df 03          	.byte 0x3
 6718 18e0 02          	.byte 0x2
 6719 18e1 23          	.byte 0x23
 6720 18e2 00          	.uleb128 0x0
 6721 18e3 05          	.uleb128 0x5
 6722 18e4 55 31 52 58 	.asciz "U1RXIP1"
 6722      49 50 31 00 
 6723 18ec 02          	.byte 0x2
 6724 18ed BD 26       	.2byte 0x26bd
 6725 18ef EA 00 00 00 	.4byte 0xea
 6726 18f3 02          	.byte 0x2
 6727 18f4 01          	.byte 0x1
 6728 18f5 02          	.byte 0x2
 6729 18f6 02          	.byte 0x2
 6730 18f7 23          	.byte 0x23
 6731 18f8 00          	.uleb128 0x0
 6732 18f9 05          	.uleb128 0x5
 6733 18fa 55 31 52 58 	.asciz "U1RXIP2"
 6733      49 50 32 00 
 6734 1902 02          	.byte 0x2
 6735 1903 BE 26       	.2byte 0x26be
 6736 1905 EA 00 00 00 	.4byte 0xea
 6737 1909 02          	.byte 0x2
 6738 190a 01          	.byte 0x1
 6739 190b 01          	.byte 0x1
 6740 190c 02          	.byte 0x2
 6741 190d 23          	.byte 0x23
 6742 190e 00          	.uleb128 0x0
 6743 190f 00          	.byte 0x0
 6744 1910 07          	.uleb128 0x7
 6745 1911 02          	.byte 0x2
 6746 1912 02          	.byte 0x2
 6747 1913 A5 26       	.2byte 0x26a5
 6748 1915 24 19 00 00 	.4byte 0x1924
 6749 1919 08          	.uleb128 0x8
 6750 191a A4 17 00 00 	.4byte 0x17a4
 6751 191e 08          	.uleb128 0x8
 6752 191f 01 18 00 00 	.4byte 0x1801
 6753 1923 00          	.byte 0x0
 6754 1924 09          	.uleb128 0x9
 6755 1925 74 61 67 49 	.asciz "tagIPC2BITS"
 6755      50 43 32 42 
 6755      49 54 53 00 
 6756 1931 02          	.byte 0x2
 6757 1932 02          	.byte 0x2
 6758 1933 A4 26       	.2byte 0x26a4
MPLAB XC16 ASSEMBLY Listing:   			page 139


 6759 1935 42 19 00 00 	.4byte 0x1942
 6760 1939 0A          	.uleb128 0xa
 6761 193a 10 19 00 00 	.4byte 0x1910
 6762 193e 02          	.byte 0x2
 6763 193f 23          	.byte 0x23
 6764 1940 00          	.uleb128 0x0
 6765 1941 00          	.byte 0x0
 6766 1942 0B          	.uleb128 0xb
 6767 1943 49 50 43 32 	.asciz "IPC2BITS"
 6767      42 49 54 53 
 6767      00 
 6768 194c 02          	.byte 0x2
 6769 194d C1 26       	.2byte 0x26c1
 6770 194f 24 19 00 00 	.4byte 0x1924
 6771 1953 04          	.uleb128 0x4
 6772 1954 02          	.byte 0x2
 6773 1955 02          	.byte 0x2
 6774 1956 2E 27       	.2byte 0x272e
 6775 1958 AD 19 00 00 	.4byte 0x19ad
 6776 195c 05          	.uleb128 0x5
 6777 195d 44 4D 41 32 	.asciz "DMA2IP"
 6777      49 50 00 
 6778 1964 02          	.byte 0x2
 6779 1965 2F 27       	.2byte 0x272f
 6780 1967 EA 00 00 00 	.4byte 0xea
 6781 196b 02          	.byte 0x2
 6782 196c 03          	.byte 0x3
 6783 196d 0D          	.byte 0xd
 6784 196e 02          	.byte 0x2
 6785 196f 23          	.byte 0x23
 6786 1970 00          	.uleb128 0x0
 6787 1971 05          	.uleb128 0x5
 6788 1972 4F 43 33 49 	.asciz "OC3IP"
 6788      50 00 
 6789 1978 02          	.byte 0x2
 6790 1979 31 27       	.2byte 0x2731
 6791 197b EA 00 00 00 	.4byte 0xea
 6792 197f 02          	.byte 0x2
 6793 1980 03          	.byte 0x3
 6794 1981 09          	.byte 0x9
 6795 1982 02          	.byte 0x2
 6796 1983 23          	.byte 0x23
 6797 1984 00          	.uleb128 0x0
 6798 1985 05          	.uleb128 0x5
 6799 1986 4F 43 34 49 	.asciz "OC4IP"
 6799      50 00 
 6800 198c 02          	.byte 0x2
 6801 198d 33 27       	.2byte 0x2733
 6802 198f EA 00 00 00 	.4byte 0xea
 6803 1993 02          	.byte 0x2
 6804 1994 03          	.byte 0x3
 6805 1995 05          	.byte 0x5
 6806 1996 02          	.byte 0x2
 6807 1997 23          	.byte 0x23
 6808 1998 00          	.uleb128 0x0
 6809 1999 05          	.uleb128 0x5
 6810 199a 54 34 49 50 	.asciz "T4IP"
MPLAB XC16 ASSEMBLY Listing:   			page 140


 6810      00 
 6811 199f 02          	.byte 0x2
 6812 19a0 35 27       	.2byte 0x2735
 6813 19a2 EA 00 00 00 	.4byte 0xea
 6814 19a6 02          	.byte 0x2
 6815 19a7 03          	.byte 0x3
 6816 19a8 01          	.byte 0x1
 6817 19a9 02          	.byte 0x2
 6818 19aa 23          	.byte 0x23
 6819 19ab 00          	.uleb128 0x0
 6820 19ac 00          	.byte 0x0
 6821 19ad 04          	.uleb128 0x4
 6822 19ae 02          	.byte 0x2
 6823 19af 02          	.byte 0x2
 6824 19b0 37 27       	.2byte 0x2737
 6825 19b2 B3 1A 00 00 	.4byte 0x1ab3
 6826 19b6 05          	.uleb128 0x5
 6827 19b7 44 4D 41 32 	.asciz "DMA2IP0"
 6827      49 50 30 00 
 6828 19bf 02          	.byte 0x2
 6829 19c0 38 27       	.2byte 0x2738
 6830 19c2 EA 00 00 00 	.4byte 0xea
 6831 19c6 02          	.byte 0x2
 6832 19c7 01          	.byte 0x1
 6833 19c8 0F          	.byte 0xf
 6834 19c9 02          	.byte 0x2
 6835 19ca 23          	.byte 0x23
 6836 19cb 00          	.uleb128 0x0
 6837 19cc 05          	.uleb128 0x5
 6838 19cd 44 4D 41 32 	.asciz "DMA2IP1"
 6838      49 50 31 00 
 6839 19d5 02          	.byte 0x2
 6840 19d6 39 27       	.2byte 0x2739
 6841 19d8 EA 00 00 00 	.4byte 0xea
 6842 19dc 02          	.byte 0x2
 6843 19dd 01          	.byte 0x1
 6844 19de 0E          	.byte 0xe
 6845 19df 02          	.byte 0x2
 6846 19e0 23          	.byte 0x23
 6847 19e1 00          	.uleb128 0x0
 6848 19e2 05          	.uleb128 0x5
 6849 19e3 44 4D 41 32 	.asciz "DMA2IP2"
 6849      49 50 32 00 
 6850 19eb 02          	.byte 0x2
 6851 19ec 3A 27       	.2byte 0x273a
 6852 19ee EA 00 00 00 	.4byte 0xea
 6853 19f2 02          	.byte 0x2
 6854 19f3 01          	.byte 0x1
 6855 19f4 0D          	.byte 0xd
 6856 19f5 02          	.byte 0x2
 6857 19f6 23          	.byte 0x23
 6858 19f7 00          	.uleb128 0x0
 6859 19f8 05          	.uleb128 0x5
 6860 19f9 4F 43 33 49 	.asciz "OC3IP0"
 6860      50 30 00 
 6861 1a00 02          	.byte 0x2
 6862 1a01 3C 27       	.2byte 0x273c
MPLAB XC16 ASSEMBLY Listing:   			page 141


 6863 1a03 EA 00 00 00 	.4byte 0xea
 6864 1a07 02          	.byte 0x2
 6865 1a08 01          	.byte 0x1
 6866 1a09 0B          	.byte 0xb
 6867 1a0a 02          	.byte 0x2
 6868 1a0b 23          	.byte 0x23
 6869 1a0c 00          	.uleb128 0x0
 6870 1a0d 05          	.uleb128 0x5
 6871 1a0e 4F 43 33 49 	.asciz "OC3IP1"
 6871      50 31 00 
 6872 1a15 02          	.byte 0x2
 6873 1a16 3D 27       	.2byte 0x273d
 6874 1a18 EA 00 00 00 	.4byte 0xea
 6875 1a1c 02          	.byte 0x2
 6876 1a1d 01          	.byte 0x1
 6877 1a1e 0A          	.byte 0xa
 6878 1a1f 02          	.byte 0x2
 6879 1a20 23          	.byte 0x23
 6880 1a21 00          	.uleb128 0x0
 6881 1a22 05          	.uleb128 0x5
 6882 1a23 4F 43 33 49 	.asciz "OC3IP2"
 6882      50 32 00 
 6883 1a2a 02          	.byte 0x2
 6884 1a2b 3E 27       	.2byte 0x273e
 6885 1a2d EA 00 00 00 	.4byte 0xea
 6886 1a31 02          	.byte 0x2
 6887 1a32 01          	.byte 0x1
 6888 1a33 09          	.byte 0x9
 6889 1a34 02          	.byte 0x2
 6890 1a35 23          	.byte 0x23
 6891 1a36 00          	.uleb128 0x0
 6892 1a37 05          	.uleb128 0x5
 6893 1a38 4F 43 34 49 	.asciz "OC4IP0"
 6893      50 30 00 
 6894 1a3f 02          	.byte 0x2
 6895 1a40 40 27       	.2byte 0x2740
 6896 1a42 EA 00 00 00 	.4byte 0xea
 6897 1a46 02          	.byte 0x2
 6898 1a47 01          	.byte 0x1
 6899 1a48 07          	.byte 0x7
 6900 1a49 02          	.byte 0x2
 6901 1a4a 23          	.byte 0x23
 6902 1a4b 00          	.uleb128 0x0
 6903 1a4c 05          	.uleb128 0x5
 6904 1a4d 4F 43 34 49 	.asciz "OC4IP1"
 6904      50 31 00 
 6905 1a54 02          	.byte 0x2
 6906 1a55 41 27       	.2byte 0x2741
 6907 1a57 EA 00 00 00 	.4byte 0xea
 6908 1a5b 02          	.byte 0x2
 6909 1a5c 01          	.byte 0x1
 6910 1a5d 06          	.byte 0x6
 6911 1a5e 02          	.byte 0x2
 6912 1a5f 23          	.byte 0x23
 6913 1a60 00          	.uleb128 0x0
 6914 1a61 05          	.uleb128 0x5
 6915 1a62 4F 43 34 49 	.asciz "OC4IP2"
MPLAB XC16 ASSEMBLY Listing:   			page 142


 6915      50 32 00 
 6916 1a69 02          	.byte 0x2
 6917 1a6a 42 27       	.2byte 0x2742
 6918 1a6c EA 00 00 00 	.4byte 0xea
 6919 1a70 02          	.byte 0x2
 6920 1a71 01          	.byte 0x1
 6921 1a72 05          	.byte 0x5
 6922 1a73 02          	.byte 0x2
 6923 1a74 23          	.byte 0x23
 6924 1a75 00          	.uleb128 0x0
 6925 1a76 05          	.uleb128 0x5
 6926 1a77 54 34 49 50 	.asciz "T4IP0"
 6926      30 00 
 6927 1a7d 02          	.byte 0x2
 6928 1a7e 44 27       	.2byte 0x2744
 6929 1a80 EA 00 00 00 	.4byte 0xea
 6930 1a84 02          	.byte 0x2
 6931 1a85 01          	.byte 0x1
 6932 1a86 03          	.byte 0x3
 6933 1a87 02          	.byte 0x2
 6934 1a88 23          	.byte 0x23
 6935 1a89 00          	.uleb128 0x0
 6936 1a8a 05          	.uleb128 0x5
 6937 1a8b 54 34 49 50 	.asciz "T4IP1"
 6937      31 00 
 6938 1a91 02          	.byte 0x2
 6939 1a92 45 27       	.2byte 0x2745
 6940 1a94 EA 00 00 00 	.4byte 0xea
 6941 1a98 02          	.byte 0x2
 6942 1a99 01          	.byte 0x1
 6943 1a9a 02          	.byte 0x2
 6944 1a9b 02          	.byte 0x2
 6945 1a9c 23          	.byte 0x23
 6946 1a9d 00          	.uleb128 0x0
 6947 1a9e 05          	.uleb128 0x5
 6948 1a9f 54 34 49 50 	.asciz "T4IP2"
 6948      32 00 
 6949 1aa5 02          	.byte 0x2
 6950 1aa6 46 27       	.2byte 0x2746
 6951 1aa8 EA 00 00 00 	.4byte 0xea
 6952 1aac 02          	.byte 0x2
 6953 1aad 01          	.byte 0x1
 6954 1aae 01          	.byte 0x1
 6955 1aaf 02          	.byte 0x2
 6956 1ab0 23          	.byte 0x23
 6957 1ab1 00          	.uleb128 0x0
 6958 1ab2 00          	.byte 0x0
 6959 1ab3 07          	.uleb128 0x7
 6960 1ab4 02          	.byte 0x2
 6961 1ab5 02          	.byte 0x2
 6962 1ab6 2D 27       	.2byte 0x272d
 6963 1ab8 C7 1A 00 00 	.4byte 0x1ac7
 6964 1abc 08          	.uleb128 0x8
 6965 1abd 53 19 00 00 	.4byte 0x1953
 6966 1ac1 08          	.uleb128 0x8
 6967 1ac2 AD 19 00 00 	.4byte 0x19ad
 6968 1ac6 00          	.byte 0x0
MPLAB XC16 ASSEMBLY Listing:   			page 143


 6969 1ac7 09          	.uleb128 0x9
 6970 1ac8 74 61 67 49 	.asciz "tagIPC6BITS"
 6970      50 43 36 42 
 6970      49 54 53 00 
 6971 1ad4 02          	.byte 0x2
 6972 1ad5 02          	.byte 0x2
 6973 1ad6 2C 27       	.2byte 0x272c
 6974 1ad8 E5 1A 00 00 	.4byte 0x1ae5
 6975 1adc 0A          	.uleb128 0xa
 6976 1add B3 1A 00 00 	.4byte 0x1ab3
 6977 1ae1 02          	.byte 0x2
 6978 1ae2 23          	.byte 0x23
 6979 1ae3 00          	.uleb128 0x0
 6980 1ae4 00          	.byte 0x0
 6981 1ae5 0B          	.uleb128 0xb
 6982 1ae6 49 50 43 36 	.asciz "IPC6BITS"
 6982      42 49 54 53 
 6982      00 
 6983 1aef 02          	.byte 0x2
 6984 1af0 49 27       	.2byte 0x2749
 6985 1af2 C7 1A 00 00 	.4byte 0x1ac7
 6986 1af6 04          	.uleb128 0x4
 6987 1af7 02          	.byte 0x2
 6988 1af8 02          	.byte 0x2
 6989 1af9 50 27       	.2byte 0x2750
 6990 1afb 52 1B 00 00 	.4byte 0x1b52
 6991 1aff 05          	.uleb128 0x5
 6992 1b00 54 35 49 50 	.asciz "T5IP"
 6992      00 
 6993 1b05 02          	.byte 0x2
 6994 1b06 51 27       	.2byte 0x2751
 6995 1b08 EA 00 00 00 	.4byte 0xea
 6996 1b0c 02          	.byte 0x2
 6997 1b0d 03          	.byte 0x3
 6998 1b0e 0D          	.byte 0xd
 6999 1b0f 02          	.byte 0x2
 7000 1b10 23          	.byte 0x23
 7001 1b11 00          	.uleb128 0x0
 7002 1b12 05          	.uleb128 0x5
 7003 1b13 49 4E 54 32 	.asciz "INT2IP"
 7003      49 50 00 
 7004 1b1a 02          	.byte 0x2
 7005 1b1b 53 27       	.2byte 0x2753
 7006 1b1d EA 00 00 00 	.4byte 0xea
 7007 1b21 02          	.byte 0x2
 7008 1b22 03          	.byte 0x3
 7009 1b23 09          	.byte 0x9
 7010 1b24 02          	.byte 0x2
 7011 1b25 23          	.byte 0x23
 7012 1b26 00          	.uleb128 0x0
 7013 1b27 05          	.uleb128 0x5
 7014 1b28 55 32 52 58 	.asciz "U2RXIP"
 7014      49 50 00 
 7015 1b2f 02          	.byte 0x2
 7016 1b30 55 27       	.2byte 0x2755
 7017 1b32 EA 00 00 00 	.4byte 0xea
 7018 1b36 02          	.byte 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 144


 7019 1b37 03          	.byte 0x3
 7020 1b38 05          	.byte 0x5
 7021 1b39 02          	.byte 0x2
 7022 1b3a 23          	.byte 0x23
 7023 1b3b 00          	.uleb128 0x0
 7024 1b3c 05          	.uleb128 0x5
 7025 1b3d 55 32 54 58 	.asciz "U2TXIP"
 7025      49 50 00 
 7026 1b44 02          	.byte 0x2
 7027 1b45 57 27       	.2byte 0x2757
 7028 1b47 EA 00 00 00 	.4byte 0xea
 7029 1b4b 02          	.byte 0x2
 7030 1b4c 03          	.byte 0x3
 7031 1b4d 01          	.byte 0x1
 7032 1b4e 02          	.byte 0x2
 7033 1b4f 23          	.byte 0x23
 7034 1b50 00          	.uleb128 0x0
 7035 1b51 00          	.byte 0x0
 7036 1b52 04          	.uleb128 0x4
 7037 1b53 02          	.byte 0x2
 7038 1b54 02          	.byte 0x2
 7039 1b55 59 27       	.2byte 0x2759
 7040 1b57 5E 1C 00 00 	.4byte 0x1c5e
 7041 1b5b 05          	.uleb128 0x5
 7042 1b5c 54 35 49 50 	.asciz "T5IP0"
 7042      30 00 
 7043 1b62 02          	.byte 0x2
 7044 1b63 5A 27       	.2byte 0x275a
 7045 1b65 EA 00 00 00 	.4byte 0xea
 7046 1b69 02          	.byte 0x2
 7047 1b6a 01          	.byte 0x1
 7048 1b6b 0F          	.byte 0xf
 7049 1b6c 02          	.byte 0x2
 7050 1b6d 23          	.byte 0x23
 7051 1b6e 00          	.uleb128 0x0
 7052 1b6f 05          	.uleb128 0x5
 7053 1b70 54 35 49 50 	.asciz "T5IP1"
 7053      31 00 
 7054 1b76 02          	.byte 0x2
 7055 1b77 5B 27       	.2byte 0x275b
 7056 1b79 EA 00 00 00 	.4byte 0xea
 7057 1b7d 02          	.byte 0x2
 7058 1b7e 01          	.byte 0x1
 7059 1b7f 0E          	.byte 0xe
 7060 1b80 02          	.byte 0x2
 7061 1b81 23          	.byte 0x23
 7062 1b82 00          	.uleb128 0x0
 7063 1b83 05          	.uleb128 0x5
 7064 1b84 54 35 49 50 	.asciz "T5IP2"
 7064      32 00 
 7065 1b8a 02          	.byte 0x2
 7066 1b8b 5C 27       	.2byte 0x275c
 7067 1b8d EA 00 00 00 	.4byte 0xea
 7068 1b91 02          	.byte 0x2
 7069 1b92 01          	.byte 0x1
 7070 1b93 0D          	.byte 0xd
 7071 1b94 02          	.byte 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 145


 7072 1b95 23          	.byte 0x23
 7073 1b96 00          	.uleb128 0x0
 7074 1b97 05          	.uleb128 0x5
 7075 1b98 49 4E 54 32 	.asciz "INT2IP0"
 7075      49 50 30 00 
 7076 1ba0 02          	.byte 0x2
 7077 1ba1 5E 27       	.2byte 0x275e
 7078 1ba3 EA 00 00 00 	.4byte 0xea
 7079 1ba7 02          	.byte 0x2
 7080 1ba8 01          	.byte 0x1
 7081 1ba9 0B          	.byte 0xb
 7082 1baa 02          	.byte 0x2
 7083 1bab 23          	.byte 0x23
 7084 1bac 00          	.uleb128 0x0
 7085 1bad 05          	.uleb128 0x5
 7086 1bae 49 4E 54 32 	.asciz "INT2IP1"
 7086      49 50 31 00 
 7087 1bb6 02          	.byte 0x2
 7088 1bb7 5F 27       	.2byte 0x275f
 7089 1bb9 EA 00 00 00 	.4byte 0xea
 7090 1bbd 02          	.byte 0x2
 7091 1bbe 01          	.byte 0x1
 7092 1bbf 0A          	.byte 0xa
 7093 1bc0 02          	.byte 0x2
 7094 1bc1 23          	.byte 0x23
 7095 1bc2 00          	.uleb128 0x0
 7096 1bc3 05          	.uleb128 0x5
 7097 1bc4 49 4E 54 32 	.asciz "INT2IP2"
 7097      49 50 32 00 
 7098 1bcc 02          	.byte 0x2
 7099 1bcd 60 27       	.2byte 0x2760
 7100 1bcf EA 00 00 00 	.4byte 0xea
 7101 1bd3 02          	.byte 0x2
 7102 1bd4 01          	.byte 0x1
 7103 1bd5 09          	.byte 0x9
 7104 1bd6 02          	.byte 0x2
 7105 1bd7 23          	.byte 0x23
 7106 1bd8 00          	.uleb128 0x0
 7107 1bd9 05          	.uleb128 0x5
 7108 1bda 55 32 52 58 	.asciz "U2RXIP0"
 7108      49 50 30 00 
 7109 1be2 02          	.byte 0x2
 7110 1be3 62 27       	.2byte 0x2762
 7111 1be5 EA 00 00 00 	.4byte 0xea
 7112 1be9 02          	.byte 0x2
 7113 1bea 01          	.byte 0x1
 7114 1beb 07          	.byte 0x7
 7115 1bec 02          	.byte 0x2
 7116 1bed 23          	.byte 0x23
 7117 1bee 00          	.uleb128 0x0
 7118 1bef 05          	.uleb128 0x5
 7119 1bf0 55 32 52 58 	.asciz "U2RXIP1"
 7119      49 50 31 00 
 7120 1bf8 02          	.byte 0x2
 7121 1bf9 63 27       	.2byte 0x2763
 7122 1bfb EA 00 00 00 	.4byte 0xea
 7123 1bff 02          	.byte 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 146


 7124 1c00 01          	.byte 0x1
 7125 1c01 06          	.byte 0x6
 7126 1c02 02          	.byte 0x2
 7127 1c03 23          	.byte 0x23
 7128 1c04 00          	.uleb128 0x0
 7129 1c05 05          	.uleb128 0x5
 7130 1c06 55 32 52 58 	.asciz "U2RXIP2"
 7130      49 50 32 00 
 7131 1c0e 02          	.byte 0x2
 7132 1c0f 64 27       	.2byte 0x2764
 7133 1c11 EA 00 00 00 	.4byte 0xea
 7134 1c15 02          	.byte 0x2
 7135 1c16 01          	.byte 0x1
 7136 1c17 05          	.byte 0x5
 7137 1c18 02          	.byte 0x2
 7138 1c19 23          	.byte 0x23
 7139 1c1a 00          	.uleb128 0x0
 7140 1c1b 05          	.uleb128 0x5
 7141 1c1c 55 32 54 58 	.asciz "U2TXIP0"
 7141      49 50 30 00 
 7142 1c24 02          	.byte 0x2
 7143 1c25 66 27       	.2byte 0x2766
 7144 1c27 EA 00 00 00 	.4byte 0xea
 7145 1c2b 02          	.byte 0x2
 7146 1c2c 01          	.byte 0x1
 7147 1c2d 03          	.byte 0x3
 7148 1c2e 02          	.byte 0x2
 7149 1c2f 23          	.byte 0x23
 7150 1c30 00          	.uleb128 0x0
 7151 1c31 05          	.uleb128 0x5
 7152 1c32 55 32 54 58 	.asciz "U2TXIP1"
 7152      49 50 31 00 
 7153 1c3a 02          	.byte 0x2
 7154 1c3b 67 27       	.2byte 0x2767
 7155 1c3d EA 00 00 00 	.4byte 0xea
 7156 1c41 02          	.byte 0x2
 7157 1c42 01          	.byte 0x1
 7158 1c43 02          	.byte 0x2
 7159 1c44 02          	.byte 0x2
 7160 1c45 23          	.byte 0x23
 7161 1c46 00          	.uleb128 0x0
 7162 1c47 05          	.uleb128 0x5
 7163 1c48 55 32 54 58 	.asciz "U2TXIP2"
 7163      49 50 32 00 
 7164 1c50 02          	.byte 0x2
 7165 1c51 68 27       	.2byte 0x2768
 7166 1c53 EA 00 00 00 	.4byte 0xea
 7167 1c57 02          	.byte 0x2
 7168 1c58 01          	.byte 0x1
 7169 1c59 01          	.byte 0x1
 7170 1c5a 02          	.byte 0x2
 7171 1c5b 23          	.byte 0x23
 7172 1c5c 00          	.uleb128 0x0
 7173 1c5d 00          	.byte 0x0
 7174 1c5e 07          	.uleb128 0x7
 7175 1c5f 02          	.byte 0x2
 7176 1c60 02          	.byte 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 147


 7177 1c61 4F 27       	.2byte 0x274f
 7178 1c63 72 1C 00 00 	.4byte 0x1c72
 7179 1c67 08          	.uleb128 0x8
 7180 1c68 F6 1A 00 00 	.4byte 0x1af6
 7181 1c6c 08          	.uleb128 0x8
 7182 1c6d 52 1B 00 00 	.4byte 0x1b52
 7183 1c71 00          	.byte 0x0
 7184 1c72 09          	.uleb128 0x9
 7185 1c73 74 61 67 49 	.asciz "tagIPC7BITS"
 7185      50 43 37 42 
 7185      49 54 53 00 
 7186 1c7f 02          	.byte 0x2
 7187 1c80 02          	.byte 0x2
 7188 1c81 4E 27       	.2byte 0x274e
 7189 1c83 90 1C 00 00 	.4byte 0x1c90
 7190 1c87 0A          	.uleb128 0xa
 7191 1c88 5E 1C 00 00 	.4byte 0x1c5e
 7192 1c8c 02          	.byte 0x2
 7193 1c8d 23          	.byte 0x23
 7194 1c8e 00          	.uleb128 0x0
 7195 1c8f 00          	.byte 0x0
 7196 1c90 0B          	.uleb128 0xb
 7197 1c91 49 50 43 37 	.asciz "IPC7BITS"
 7197      42 49 54 53 
 7197      00 
 7198 1c9a 02          	.byte 0x2
 7199 1c9b 6B 27       	.2byte 0x276b
 7200 1c9d 72 1C 00 00 	.4byte 0x1c72
 7201 1ca1 04          	.uleb128 0x4
 7202 1ca2 02          	.byte 0x2
 7203 1ca3 02          	.byte 0x2
 7204 1ca4 D8 27       	.2byte 0x27d8
 7205 1ca6 FB 1C 00 00 	.4byte 0x1cfb
 7206 1caa 05          	.uleb128 0x5
 7207 1cab 4F 43 38 49 	.asciz "OC8IP"
 7207      50 00 
 7208 1cb1 02          	.byte 0x2
 7209 1cb2 D9 27       	.2byte 0x27d9
 7210 1cb4 EA 00 00 00 	.4byte 0xea
 7211 1cb8 02          	.byte 0x2
 7212 1cb9 03          	.byte 0x3
 7213 1cba 0D          	.byte 0xd
 7214 1cbb 02          	.byte 0x2
 7215 1cbc 23          	.byte 0x23
 7216 1cbd 00          	.uleb128 0x0
 7217 1cbe 05          	.uleb128 0x5
 7218 1cbf 50 4D 50 49 	.asciz "PMPIP"
 7218      50 00 
 7219 1cc5 02          	.byte 0x2
 7220 1cc6 DB 27       	.2byte 0x27db
 7221 1cc8 EA 00 00 00 	.4byte 0xea
 7222 1ccc 02          	.byte 0x2
 7223 1ccd 03          	.byte 0x3
 7224 1cce 09          	.byte 0x9
 7225 1ccf 02          	.byte 0x2
 7226 1cd0 23          	.byte 0x23
 7227 1cd1 00          	.uleb128 0x0
MPLAB XC16 ASSEMBLY Listing:   			page 148


 7228 1cd2 05          	.uleb128 0x5
 7229 1cd3 44 4D 41 34 	.asciz "DMA4IP"
 7229      49 50 00 
 7230 1cda 02          	.byte 0x2
 7231 1cdb DD 27       	.2byte 0x27dd
 7232 1cdd EA 00 00 00 	.4byte 0xea
 7233 1ce1 02          	.byte 0x2
 7234 1ce2 03          	.byte 0x3
 7235 1ce3 05          	.byte 0x5
 7236 1ce4 02          	.byte 0x2
 7237 1ce5 23          	.byte 0x23
 7238 1ce6 00          	.uleb128 0x0
 7239 1ce7 05          	.uleb128 0x5
 7240 1ce8 54 36 49 50 	.asciz "T6IP"
 7240      00 
 7241 1ced 02          	.byte 0x2
 7242 1cee DF 27       	.2byte 0x27df
 7243 1cf0 EA 00 00 00 	.4byte 0xea
 7244 1cf4 02          	.byte 0x2
 7245 1cf5 03          	.byte 0x3
 7246 1cf6 01          	.byte 0x1
 7247 1cf7 02          	.byte 0x2
 7248 1cf8 23          	.byte 0x23
 7249 1cf9 00          	.uleb128 0x0
 7250 1cfa 00          	.byte 0x0
 7251 1cfb 04          	.uleb128 0x4
 7252 1cfc 02          	.byte 0x2
 7253 1cfd 02          	.byte 0x2
 7254 1cfe E1 27       	.2byte 0x27e1
 7255 1d00 01 1E 00 00 	.4byte 0x1e01
 7256 1d04 05          	.uleb128 0x5
 7257 1d05 4F 43 38 49 	.asciz "OC8IP0"
 7257      50 30 00 
 7258 1d0c 02          	.byte 0x2
 7259 1d0d E2 27       	.2byte 0x27e2
 7260 1d0f EA 00 00 00 	.4byte 0xea
 7261 1d13 02          	.byte 0x2
 7262 1d14 01          	.byte 0x1
 7263 1d15 0F          	.byte 0xf
 7264 1d16 02          	.byte 0x2
 7265 1d17 23          	.byte 0x23
 7266 1d18 00          	.uleb128 0x0
 7267 1d19 05          	.uleb128 0x5
 7268 1d1a 4F 43 38 49 	.asciz "OC8IP1"
 7268      50 31 00 
 7269 1d21 02          	.byte 0x2
 7270 1d22 E3 27       	.2byte 0x27e3
 7271 1d24 EA 00 00 00 	.4byte 0xea
 7272 1d28 02          	.byte 0x2
 7273 1d29 01          	.byte 0x1
 7274 1d2a 0E          	.byte 0xe
 7275 1d2b 02          	.byte 0x2
 7276 1d2c 23          	.byte 0x23
 7277 1d2d 00          	.uleb128 0x0
 7278 1d2e 05          	.uleb128 0x5
 7279 1d2f 4F 43 38 49 	.asciz "OC8IP2"
 7279      50 32 00 
MPLAB XC16 ASSEMBLY Listing:   			page 149


 7280 1d36 02          	.byte 0x2
 7281 1d37 E4 27       	.2byte 0x27e4
 7282 1d39 EA 00 00 00 	.4byte 0xea
 7283 1d3d 02          	.byte 0x2
 7284 1d3e 01          	.byte 0x1
 7285 1d3f 0D          	.byte 0xd
 7286 1d40 02          	.byte 0x2
 7287 1d41 23          	.byte 0x23
 7288 1d42 00          	.uleb128 0x0
 7289 1d43 05          	.uleb128 0x5
 7290 1d44 50 4D 50 49 	.asciz "PMPIP0"
 7290      50 30 00 
 7291 1d4b 02          	.byte 0x2
 7292 1d4c E6 27       	.2byte 0x27e6
 7293 1d4e EA 00 00 00 	.4byte 0xea
 7294 1d52 02          	.byte 0x2
 7295 1d53 01          	.byte 0x1
 7296 1d54 0B          	.byte 0xb
 7297 1d55 02          	.byte 0x2
 7298 1d56 23          	.byte 0x23
 7299 1d57 00          	.uleb128 0x0
 7300 1d58 05          	.uleb128 0x5
 7301 1d59 50 4D 50 49 	.asciz "PMPIP1"
 7301      50 31 00 
 7302 1d60 02          	.byte 0x2
 7303 1d61 E7 27       	.2byte 0x27e7
 7304 1d63 EA 00 00 00 	.4byte 0xea
 7305 1d67 02          	.byte 0x2
 7306 1d68 01          	.byte 0x1
 7307 1d69 0A          	.byte 0xa
 7308 1d6a 02          	.byte 0x2
 7309 1d6b 23          	.byte 0x23
 7310 1d6c 00          	.uleb128 0x0
 7311 1d6d 05          	.uleb128 0x5
 7312 1d6e 50 4D 50 49 	.asciz "PMPIP2"
 7312      50 32 00 
 7313 1d75 02          	.byte 0x2
 7314 1d76 E8 27       	.2byte 0x27e8
 7315 1d78 EA 00 00 00 	.4byte 0xea
 7316 1d7c 02          	.byte 0x2
 7317 1d7d 01          	.byte 0x1
 7318 1d7e 09          	.byte 0x9
 7319 1d7f 02          	.byte 0x2
 7320 1d80 23          	.byte 0x23
 7321 1d81 00          	.uleb128 0x0
 7322 1d82 05          	.uleb128 0x5
 7323 1d83 44 4D 41 34 	.asciz "DMA4IP0"
 7323      49 50 30 00 
 7324 1d8b 02          	.byte 0x2
 7325 1d8c EA 27       	.2byte 0x27ea
 7326 1d8e EA 00 00 00 	.4byte 0xea
 7327 1d92 02          	.byte 0x2
 7328 1d93 01          	.byte 0x1
 7329 1d94 07          	.byte 0x7
 7330 1d95 02          	.byte 0x2
 7331 1d96 23          	.byte 0x23
 7332 1d97 00          	.uleb128 0x0
MPLAB XC16 ASSEMBLY Listing:   			page 150


 7333 1d98 05          	.uleb128 0x5
 7334 1d99 44 4D 41 34 	.asciz "DMA4IP1"
 7334      49 50 31 00 
 7335 1da1 02          	.byte 0x2
 7336 1da2 EB 27       	.2byte 0x27eb
 7337 1da4 EA 00 00 00 	.4byte 0xea
 7338 1da8 02          	.byte 0x2
 7339 1da9 01          	.byte 0x1
 7340 1daa 06          	.byte 0x6
 7341 1dab 02          	.byte 0x2
 7342 1dac 23          	.byte 0x23
 7343 1dad 00          	.uleb128 0x0
 7344 1dae 05          	.uleb128 0x5
 7345 1daf 44 4D 41 34 	.asciz "DMA4IP2"
 7345      49 50 32 00 
 7346 1db7 02          	.byte 0x2
 7347 1db8 EC 27       	.2byte 0x27ec
 7348 1dba EA 00 00 00 	.4byte 0xea
 7349 1dbe 02          	.byte 0x2
 7350 1dbf 01          	.byte 0x1
 7351 1dc0 05          	.byte 0x5
 7352 1dc1 02          	.byte 0x2
 7353 1dc2 23          	.byte 0x23
 7354 1dc3 00          	.uleb128 0x0
 7355 1dc4 05          	.uleb128 0x5
 7356 1dc5 54 36 49 50 	.asciz "T6IP0"
 7356      30 00 
 7357 1dcb 02          	.byte 0x2
 7358 1dcc EE 27       	.2byte 0x27ee
 7359 1dce EA 00 00 00 	.4byte 0xea
 7360 1dd2 02          	.byte 0x2
 7361 1dd3 01          	.byte 0x1
 7362 1dd4 03          	.byte 0x3
 7363 1dd5 02          	.byte 0x2
 7364 1dd6 23          	.byte 0x23
 7365 1dd7 00          	.uleb128 0x0
 7366 1dd8 05          	.uleb128 0x5
 7367 1dd9 54 36 49 50 	.asciz "T6IP1"
 7367      31 00 
 7368 1ddf 02          	.byte 0x2
 7369 1de0 EF 27       	.2byte 0x27ef
 7370 1de2 EA 00 00 00 	.4byte 0xea
 7371 1de6 02          	.byte 0x2
 7372 1de7 01          	.byte 0x1
 7373 1de8 02          	.byte 0x2
 7374 1de9 02          	.byte 0x2
 7375 1dea 23          	.byte 0x23
 7376 1deb 00          	.uleb128 0x0
 7377 1dec 05          	.uleb128 0x5
 7378 1ded 54 36 49 50 	.asciz "T6IP2"
 7378      32 00 
 7379 1df3 02          	.byte 0x2
 7380 1df4 F0 27       	.2byte 0x27f0
 7381 1df6 EA 00 00 00 	.4byte 0xea
 7382 1dfa 02          	.byte 0x2
 7383 1dfb 01          	.byte 0x1
 7384 1dfc 01          	.byte 0x1
MPLAB XC16 ASSEMBLY Listing:   			page 151


 7385 1dfd 02          	.byte 0x2
 7386 1dfe 23          	.byte 0x23
 7387 1dff 00          	.uleb128 0x0
 7388 1e00 00          	.byte 0x0
 7389 1e01 07          	.uleb128 0x7
 7390 1e02 02          	.byte 0x2
 7391 1e03 02          	.byte 0x2
 7392 1e04 D7 27       	.2byte 0x27d7
 7393 1e06 15 1E 00 00 	.4byte 0x1e15
 7394 1e0a 08          	.uleb128 0x8
 7395 1e0b A1 1C 00 00 	.4byte 0x1ca1
 7396 1e0f 08          	.uleb128 0x8
 7397 1e10 FB 1C 00 00 	.4byte 0x1cfb
 7398 1e14 00          	.byte 0x0
 7399 1e15 09          	.uleb128 0x9
 7400 1e16 74 61 67 49 	.asciz "tagIPC11BITS"
 7400      50 43 31 31 
 7400      42 49 54 53 
 7400      00 
 7401 1e23 02          	.byte 0x2
 7402 1e24 02          	.byte 0x2
 7403 1e25 D6 27       	.2byte 0x27d6
 7404 1e27 34 1E 00 00 	.4byte 0x1e34
 7405 1e2b 0A          	.uleb128 0xa
 7406 1e2c 01 1E 00 00 	.4byte 0x1e01
 7407 1e30 02          	.byte 0x2
 7408 1e31 23          	.byte 0x23
 7409 1e32 00          	.uleb128 0x0
 7410 1e33 00          	.byte 0x0
 7411 1e34 0B          	.uleb128 0xb
 7412 1e35 49 50 43 31 	.asciz "IPC11BITS"
 7412      31 42 49 54 
 7412      53 00 
 7413 1e3f 02          	.byte 0x2
 7414 1e40 F3 27       	.2byte 0x27f3
 7415 1e42 15 1E 00 00 	.4byte 0x1e15
 7416 1e46 04          	.uleb128 0x4
 7417 1e47 02          	.byte 0x2
 7418 1e48 02          	.byte 0x2
 7419 1e49 FA 27       	.2byte 0x27fa
 7420 1e4b A2 1E 00 00 	.4byte 0x1ea2
 7421 1e4f 05          	.uleb128 0x5
 7422 1e50 54 37 49 50 	.asciz "T7IP"
 7422      00 
 7423 1e55 02          	.byte 0x2
 7424 1e56 FB 27       	.2byte 0x27fb
 7425 1e58 EA 00 00 00 	.4byte 0xea
 7426 1e5c 02          	.byte 0x2
 7427 1e5d 03          	.byte 0x3
 7428 1e5e 0D          	.byte 0xd
 7429 1e5f 02          	.byte 0x2
 7430 1e60 23          	.byte 0x23
 7431 1e61 00          	.uleb128 0x0
 7432 1e62 05          	.uleb128 0x5
 7433 1e63 53 49 32 43 	.asciz "SI2C2IP"
 7433      32 49 50 00 
 7434 1e6b 02          	.byte 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 152


 7435 1e6c FD 27       	.2byte 0x27fd
 7436 1e6e EA 00 00 00 	.4byte 0xea
 7437 1e72 02          	.byte 0x2
 7438 1e73 03          	.byte 0x3
 7439 1e74 09          	.byte 0x9
 7440 1e75 02          	.byte 0x2
 7441 1e76 23          	.byte 0x23
 7442 1e77 00          	.uleb128 0x0
 7443 1e78 05          	.uleb128 0x5
 7444 1e79 4D 49 32 43 	.asciz "MI2C2IP"
 7444      32 49 50 00 
 7445 1e81 02          	.byte 0x2
 7446 1e82 FF 27       	.2byte 0x27ff
 7447 1e84 EA 00 00 00 	.4byte 0xea
 7448 1e88 02          	.byte 0x2
 7449 1e89 03          	.byte 0x3
 7450 1e8a 05          	.byte 0x5
 7451 1e8b 02          	.byte 0x2
 7452 1e8c 23          	.byte 0x23
 7453 1e8d 00          	.uleb128 0x0
 7454 1e8e 05          	.uleb128 0x5
 7455 1e8f 54 38 49 50 	.asciz "T8IP"
 7455      00 
 7456 1e94 02          	.byte 0x2
 7457 1e95 01 28       	.2byte 0x2801
 7458 1e97 EA 00 00 00 	.4byte 0xea
 7459 1e9b 02          	.byte 0x2
 7460 1e9c 03          	.byte 0x3
 7461 1e9d 01          	.byte 0x1
 7462 1e9e 02          	.byte 0x2
 7463 1e9f 23          	.byte 0x23
 7464 1ea0 00          	.uleb128 0x0
 7465 1ea1 00          	.byte 0x0
 7466 1ea2 04          	.uleb128 0x4
 7467 1ea3 02          	.byte 0x2
 7468 1ea4 02          	.byte 0x2
 7469 1ea5 03 28       	.2byte 0x2803
 7470 1ea7 AE 1F 00 00 	.4byte 0x1fae
 7471 1eab 05          	.uleb128 0x5
 7472 1eac 54 37 49 50 	.asciz "T7IP0"
 7472      30 00 
 7473 1eb2 02          	.byte 0x2
 7474 1eb3 04 28       	.2byte 0x2804
 7475 1eb5 EA 00 00 00 	.4byte 0xea
 7476 1eb9 02          	.byte 0x2
 7477 1eba 01          	.byte 0x1
 7478 1ebb 0F          	.byte 0xf
 7479 1ebc 02          	.byte 0x2
 7480 1ebd 23          	.byte 0x23
 7481 1ebe 00          	.uleb128 0x0
 7482 1ebf 05          	.uleb128 0x5
 7483 1ec0 54 37 49 50 	.asciz "T7IP1"
 7483      31 00 
 7484 1ec6 02          	.byte 0x2
 7485 1ec7 05 28       	.2byte 0x2805
 7486 1ec9 EA 00 00 00 	.4byte 0xea
 7487 1ecd 02          	.byte 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 153


 7488 1ece 01          	.byte 0x1
 7489 1ecf 0E          	.byte 0xe
 7490 1ed0 02          	.byte 0x2
 7491 1ed1 23          	.byte 0x23
 7492 1ed2 00          	.uleb128 0x0
 7493 1ed3 05          	.uleb128 0x5
 7494 1ed4 54 37 49 50 	.asciz "T7IP2"
 7494      32 00 
 7495 1eda 02          	.byte 0x2
 7496 1edb 06 28       	.2byte 0x2806
 7497 1edd EA 00 00 00 	.4byte 0xea
 7498 1ee1 02          	.byte 0x2
 7499 1ee2 01          	.byte 0x1
 7500 1ee3 0D          	.byte 0xd
 7501 1ee4 02          	.byte 0x2
 7502 1ee5 23          	.byte 0x23
 7503 1ee6 00          	.uleb128 0x0
 7504 1ee7 05          	.uleb128 0x5
 7505 1ee8 53 49 32 43 	.asciz "SI2C2IP0"
 7505      32 49 50 30 
 7505      00 
 7506 1ef1 02          	.byte 0x2
 7507 1ef2 08 28       	.2byte 0x2808
 7508 1ef4 EA 00 00 00 	.4byte 0xea
 7509 1ef8 02          	.byte 0x2
 7510 1ef9 01          	.byte 0x1
 7511 1efa 0B          	.byte 0xb
 7512 1efb 02          	.byte 0x2
 7513 1efc 23          	.byte 0x23
 7514 1efd 00          	.uleb128 0x0
 7515 1efe 05          	.uleb128 0x5
 7516 1eff 53 49 32 43 	.asciz "SI2C2IP1"
 7516      32 49 50 31 
 7516      00 
 7517 1f08 02          	.byte 0x2
 7518 1f09 09 28       	.2byte 0x2809
 7519 1f0b EA 00 00 00 	.4byte 0xea
 7520 1f0f 02          	.byte 0x2
 7521 1f10 01          	.byte 0x1
 7522 1f11 0A          	.byte 0xa
 7523 1f12 02          	.byte 0x2
 7524 1f13 23          	.byte 0x23
 7525 1f14 00          	.uleb128 0x0
 7526 1f15 05          	.uleb128 0x5
 7527 1f16 53 49 32 43 	.asciz "SI2C2IP2"
 7527      32 49 50 32 
 7527      00 
 7528 1f1f 02          	.byte 0x2
 7529 1f20 0A 28       	.2byte 0x280a
 7530 1f22 EA 00 00 00 	.4byte 0xea
 7531 1f26 02          	.byte 0x2
 7532 1f27 01          	.byte 0x1
 7533 1f28 09          	.byte 0x9
 7534 1f29 02          	.byte 0x2
 7535 1f2a 23          	.byte 0x23
 7536 1f2b 00          	.uleb128 0x0
 7537 1f2c 05          	.uleb128 0x5
MPLAB XC16 ASSEMBLY Listing:   			page 154


 7538 1f2d 4D 49 32 43 	.asciz "MI2C2IP0"
 7538      32 49 50 30 
 7538      00 
 7539 1f36 02          	.byte 0x2
 7540 1f37 0C 28       	.2byte 0x280c
 7541 1f39 EA 00 00 00 	.4byte 0xea
 7542 1f3d 02          	.byte 0x2
 7543 1f3e 01          	.byte 0x1
 7544 1f3f 07          	.byte 0x7
 7545 1f40 02          	.byte 0x2
 7546 1f41 23          	.byte 0x23
 7547 1f42 00          	.uleb128 0x0
 7548 1f43 05          	.uleb128 0x5
 7549 1f44 4D 49 32 43 	.asciz "MI2C2IP1"
 7549      32 49 50 31 
 7549      00 
 7550 1f4d 02          	.byte 0x2
 7551 1f4e 0D 28       	.2byte 0x280d
 7552 1f50 EA 00 00 00 	.4byte 0xea
 7553 1f54 02          	.byte 0x2
 7554 1f55 01          	.byte 0x1
 7555 1f56 06          	.byte 0x6
 7556 1f57 02          	.byte 0x2
 7557 1f58 23          	.byte 0x23
 7558 1f59 00          	.uleb128 0x0
 7559 1f5a 05          	.uleb128 0x5
 7560 1f5b 4D 49 32 43 	.asciz "MI2C2IP2"
 7560      32 49 50 32 
 7560      00 
 7561 1f64 02          	.byte 0x2
 7562 1f65 0E 28       	.2byte 0x280e
 7563 1f67 EA 00 00 00 	.4byte 0xea
 7564 1f6b 02          	.byte 0x2
 7565 1f6c 01          	.byte 0x1
 7566 1f6d 05          	.byte 0x5
 7567 1f6e 02          	.byte 0x2
 7568 1f6f 23          	.byte 0x23
 7569 1f70 00          	.uleb128 0x0
 7570 1f71 05          	.uleb128 0x5
 7571 1f72 54 38 49 50 	.asciz "T8IP0"
 7571      30 00 
 7572 1f78 02          	.byte 0x2
 7573 1f79 10 28       	.2byte 0x2810
 7574 1f7b EA 00 00 00 	.4byte 0xea
 7575 1f7f 02          	.byte 0x2
 7576 1f80 01          	.byte 0x1
 7577 1f81 03          	.byte 0x3
 7578 1f82 02          	.byte 0x2
 7579 1f83 23          	.byte 0x23
 7580 1f84 00          	.uleb128 0x0
 7581 1f85 05          	.uleb128 0x5
 7582 1f86 54 38 49 50 	.asciz "T8IP1"
 7582      31 00 
 7583 1f8c 02          	.byte 0x2
 7584 1f8d 11 28       	.2byte 0x2811
 7585 1f8f EA 00 00 00 	.4byte 0xea
 7586 1f93 02          	.byte 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 155


 7587 1f94 01          	.byte 0x1
 7588 1f95 02          	.byte 0x2
 7589 1f96 02          	.byte 0x2
 7590 1f97 23          	.byte 0x23
 7591 1f98 00          	.uleb128 0x0
 7592 1f99 05          	.uleb128 0x5
 7593 1f9a 54 38 49 50 	.asciz "T8IP2"
 7593      32 00 
 7594 1fa0 02          	.byte 0x2
 7595 1fa1 12 28       	.2byte 0x2812
 7596 1fa3 EA 00 00 00 	.4byte 0xea
 7597 1fa7 02          	.byte 0x2
 7598 1fa8 01          	.byte 0x1
 7599 1fa9 01          	.byte 0x1
 7600 1faa 02          	.byte 0x2
 7601 1fab 23          	.byte 0x23
 7602 1fac 00          	.uleb128 0x0
 7603 1fad 00          	.byte 0x0
 7604 1fae 07          	.uleb128 0x7
 7605 1faf 02          	.byte 0x2
 7606 1fb0 02          	.byte 0x2
 7607 1fb1 F9 27       	.2byte 0x27f9
 7608 1fb3 C2 1F 00 00 	.4byte 0x1fc2
 7609 1fb7 08          	.uleb128 0x8
 7610 1fb8 46 1E 00 00 	.4byte 0x1e46
 7611 1fbc 08          	.uleb128 0x8
 7612 1fbd A2 1E 00 00 	.4byte 0x1ea2
 7613 1fc1 00          	.byte 0x0
 7614 1fc2 09          	.uleb128 0x9
 7615 1fc3 74 61 67 49 	.asciz "tagIPC12BITS"
 7615      50 43 31 32 
 7615      42 49 54 53 
 7615      00 
 7616 1fd0 02          	.byte 0x2
 7617 1fd1 02          	.byte 0x2
 7618 1fd2 F8 27       	.2byte 0x27f8
 7619 1fd4 E1 1F 00 00 	.4byte 0x1fe1
 7620 1fd8 0A          	.uleb128 0xa
 7621 1fd9 AE 1F 00 00 	.4byte 0x1fae
 7622 1fdd 02          	.byte 0x2
 7623 1fde 23          	.byte 0x23
 7624 1fdf 00          	.uleb128 0x0
 7625 1fe0 00          	.byte 0x0
 7626 1fe1 0B          	.uleb128 0xb
 7627 1fe2 49 50 43 31 	.asciz "IPC12BITS"
 7627      32 42 49 54 
 7627      53 00 
 7628 1fec 02          	.byte 0x2
 7629 1fed 15 28       	.2byte 0x2815
 7630 1fef C2 1F 00 00 	.4byte 0x1fc2
 7631 1ff3 04          	.uleb128 0x4
 7632 1ff4 02          	.byte 0x2
 7633 1ff5 02          	.byte 0x2
 7634 1ff6 1C 28       	.2byte 0x281c
 7635 1ff8 4F 20 00 00 	.4byte 0x204f
 7636 1ffc 05          	.uleb128 0x5
 7637 1ffd 54 39 49 50 	.asciz "T9IP"
MPLAB XC16 ASSEMBLY Listing:   			page 156


 7637      00 
 7638 2002 02          	.byte 0x2
 7639 2003 1D 28       	.2byte 0x281d
 7640 2005 EA 00 00 00 	.4byte 0xea
 7641 2009 02          	.byte 0x2
 7642 200a 03          	.byte 0x3
 7643 200b 0D          	.byte 0xd
 7644 200c 02          	.byte 0x2
 7645 200d 23          	.byte 0x23
 7646 200e 00          	.uleb128 0x0
 7647 200f 05          	.uleb128 0x5
 7648 2010 49 4E 54 33 	.asciz "INT3IP"
 7648      49 50 00 
 7649 2017 02          	.byte 0x2
 7650 2018 1F 28       	.2byte 0x281f
 7651 201a EA 00 00 00 	.4byte 0xea
 7652 201e 02          	.byte 0x2
 7653 201f 03          	.byte 0x3
 7654 2020 09          	.byte 0x9
 7655 2021 02          	.byte 0x2
 7656 2022 23          	.byte 0x23
 7657 2023 00          	.uleb128 0x0
 7658 2024 05          	.uleb128 0x5
 7659 2025 49 4E 54 34 	.asciz "INT4IP"
 7659      49 50 00 
 7660 202c 02          	.byte 0x2
 7661 202d 21 28       	.2byte 0x2821
 7662 202f EA 00 00 00 	.4byte 0xea
 7663 2033 02          	.byte 0x2
 7664 2034 03          	.byte 0x3
 7665 2035 05          	.byte 0x5
 7666 2036 02          	.byte 0x2
 7667 2037 23          	.byte 0x23
 7668 2038 00          	.uleb128 0x0
 7669 2039 05          	.uleb128 0x5
 7670 203a 43 32 52 58 	.asciz "C2RXIP"
 7670      49 50 00 
 7671 2041 02          	.byte 0x2
 7672 2042 23 28       	.2byte 0x2823
 7673 2044 EA 00 00 00 	.4byte 0xea
 7674 2048 02          	.byte 0x2
 7675 2049 03          	.byte 0x3
 7676 204a 01          	.byte 0x1
 7677 204b 02          	.byte 0x2
 7678 204c 23          	.byte 0x23
 7679 204d 00          	.uleb128 0x0
 7680 204e 00          	.byte 0x0
 7681 204f 04          	.uleb128 0x4
 7682 2050 02          	.byte 0x2
 7683 2051 02          	.byte 0x2
 7684 2052 25 28       	.2byte 0x2825
 7685 2054 5B 21 00 00 	.4byte 0x215b
 7686 2058 05          	.uleb128 0x5
 7687 2059 54 39 49 50 	.asciz "T9IP0"
 7687      30 00 
 7688 205f 02          	.byte 0x2
 7689 2060 26 28       	.2byte 0x2826
MPLAB XC16 ASSEMBLY Listing:   			page 157


 7690 2062 EA 00 00 00 	.4byte 0xea
 7691 2066 02          	.byte 0x2
 7692 2067 01          	.byte 0x1
 7693 2068 0F          	.byte 0xf
 7694 2069 02          	.byte 0x2
 7695 206a 23          	.byte 0x23
 7696 206b 00          	.uleb128 0x0
 7697 206c 05          	.uleb128 0x5
 7698 206d 54 39 49 50 	.asciz "T9IP1"
 7698      31 00 
 7699 2073 02          	.byte 0x2
 7700 2074 27 28       	.2byte 0x2827
 7701 2076 EA 00 00 00 	.4byte 0xea
 7702 207a 02          	.byte 0x2
 7703 207b 01          	.byte 0x1
 7704 207c 0E          	.byte 0xe
 7705 207d 02          	.byte 0x2
 7706 207e 23          	.byte 0x23
 7707 207f 00          	.uleb128 0x0
 7708 2080 05          	.uleb128 0x5
 7709 2081 54 39 49 50 	.asciz "T9IP2"
 7709      32 00 
 7710 2087 02          	.byte 0x2
 7711 2088 28 28       	.2byte 0x2828
 7712 208a EA 00 00 00 	.4byte 0xea
 7713 208e 02          	.byte 0x2
 7714 208f 01          	.byte 0x1
 7715 2090 0D          	.byte 0xd
 7716 2091 02          	.byte 0x2
 7717 2092 23          	.byte 0x23
 7718 2093 00          	.uleb128 0x0
 7719 2094 05          	.uleb128 0x5
 7720 2095 49 4E 54 33 	.asciz "INT3IP0"
 7720      49 50 30 00 
 7721 209d 02          	.byte 0x2
 7722 209e 2A 28       	.2byte 0x282a
 7723 20a0 EA 00 00 00 	.4byte 0xea
 7724 20a4 02          	.byte 0x2
 7725 20a5 01          	.byte 0x1
 7726 20a6 0B          	.byte 0xb
 7727 20a7 02          	.byte 0x2
 7728 20a8 23          	.byte 0x23
 7729 20a9 00          	.uleb128 0x0
 7730 20aa 05          	.uleb128 0x5
 7731 20ab 49 4E 54 33 	.asciz "INT3IP1"
 7731      49 50 31 00 
 7732 20b3 02          	.byte 0x2
 7733 20b4 2B 28       	.2byte 0x282b
 7734 20b6 EA 00 00 00 	.4byte 0xea
 7735 20ba 02          	.byte 0x2
 7736 20bb 01          	.byte 0x1
 7737 20bc 0A          	.byte 0xa
 7738 20bd 02          	.byte 0x2
 7739 20be 23          	.byte 0x23
 7740 20bf 00          	.uleb128 0x0
 7741 20c0 05          	.uleb128 0x5
 7742 20c1 49 4E 54 33 	.asciz "INT3IP2"
MPLAB XC16 ASSEMBLY Listing:   			page 158


 7742      49 50 32 00 
 7743 20c9 02          	.byte 0x2
 7744 20ca 2C 28       	.2byte 0x282c
 7745 20cc EA 00 00 00 	.4byte 0xea
 7746 20d0 02          	.byte 0x2
 7747 20d1 01          	.byte 0x1
 7748 20d2 09          	.byte 0x9
 7749 20d3 02          	.byte 0x2
 7750 20d4 23          	.byte 0x23
 7751 20d5 00          	.uleb128 0x0
 7752 20d6 05          	.uleb128 0x5
 7753 20d7 49 4E 54 34 	.asciz "INT4IP0"
 7753      49 50 30 00 
 7754 20df 02          	.byte 0x2
 7755 20e0 2E 28       	.2byte 0x282e
 7756 20e2 EA 00 00 00 	.4byte 0xea
 7757 20e6 02          	.byte 0x2
 7758 20e7 01          	.byte 0x1
 7759 20e8 07          	.byte 0x7
 7760 20e9 02          	.byte 0x2
 7761 20ea 23          	.byte 0x23
 7762 20eb 00          	.uleb128 0x0
 7763 20ec 05          	.uleb128 0x5
 7764 20ed 49 4E 54 34 	.asciz "INT4IP1"
 7764      49 50 31 00 
 7765 20f5 02          	.byte 0x2
 7766 20f6 2F 28       	.2byte 0x282f
 7767 20f8 EA 00 00 00 	.4byte 0xea
 7768 20fc 02          	.byte 0x2
 7769 20fd 01          	.byte 0x1
 7770 20fe 06          	.byte 0x6
 7771 20ff 02          	.byte 0x2
 7772 2100 23          	.byte 0x23
 7773 2101 00          	.uleb128 0x0
 7774 2102 05          	.uleb128 0x5
 7775 2103 49 4E 54 34 	.asciz "INT4IP2"
 7775      49 50 32 00 
 7776 210b 02          	.byte 0x2
 7777 210c 30 28       	.2byte 0x2830
 7778 210e EA 00 00 00 	.4byte 0xea
 7779 2112 02          	.byte 0x2
 7780 2113 01          	.byte 0x1
 7781 2114 05          	.byte 0x5
 7782 2115 02          	.byte 0x2
 7783 2116 23          	.byte 0x23
 7784 2117 00          	.uleb128 0x0
 7785 2118 05          	.uleb128 0x5
 7786 2119 43 32 52 58 	.asciz "C2RXIP0"
 7786      49 50 30 00 
 7787 2121 02          	.byte 0x2
 7788 2122 32 28       	.2byte 0x2832
 7789 2124 EA 00 00 00 	.4byte 0xea
 7790 2128 02          	.byte 0x2
 7791 2129 01          	.byte 0x1
 7792 212a 03          	.byte 0x3
 7793 212b 02          	.byte 0x2
 7794 212c 23          	.byte 0x23
MPLAB XC16 ASSEMBLY Listing:   			page 159


 7795 212d 00          	.uleb128 0x0
 7796 212e 05          	.uleb128 0x5
 7797 212f 43 32 52 58 	.asciz "C2RXIP1"
 7797      49 50 31 00 
 7798 2137 02          	.byte 0x2
 7799 2138 33 28       	.2byte 0x2833
 7800 213a EA 00 00 00 	.4byte 0xea
 7801 213e 02          	.byte 0x2
 7802 213f 01          	.byte 0x1
 7803 2140 02          	.byte 0x2
 7804 2141 02          	.byte 0x2
 7805 2142 23          	.byte 0x23
 7806 2143 00          	.uleb128 0x0
 7807 2144 05          	.uleb128 0x5
 7808 2145 43 32 52 58 	.asciz "C2RXIP2"
 7808      49 50 32 00 
 7809 214d 02          	.byte 0x2
 7810 214e 34 28       	.2byte 0x2834
 7811 2150 EA 00 00 00 	.4byte 0xea
 7812 2154 02          	.byte 0x2
 7813 2155 01          	.byte 0x1
 7814 2156 01          	.byte 0x1
 7815 2157 02          	.byte 0x2
 7816 2158 23          	.byte 0x23
 7817 2159 00          	.uleb128 0x0
 7818 215a 00          	.byte 0x0
 7819 215b 07          	.uleb128 0x7
 7820 215c 02          	.byte 0x2
 7821 215d 02          	.byte 0x2
 7822 215e 1B 28       	.2byte 0x281b
 7823 2160 6F 21 00 00 	.4byte 0x216f
 7824 2164 08          	.uleb128 0x8
 7825 2165 F3 1F 00 00 	.4byte 0x1ff3
 7826 2169 08          	.uleb128 0x8
 7827 216a 4F 20 00 00 	.4byte 0x204f
 7828 216e 00          	.byte 0x0
 7829 216f 09          	.uleb128 0x9
 7830 2170 74 61 67 49 	.asciz "tagIPC13BITS"
 7830      50 43 31 33 
 7830      42 49 54 53 
 7830      00 
 7831 217d 02          	.byte 0x2
 7832 217e 02          	.byte 0x2
 7833 217f 1A 28       	.2byte 0x281a
 7834 2181 8E 21 00 00 	.4byte 0x218e
 7835 2185 0A          	.uleb128 0xa
 7836 2186 5B 21 00 00 	.4byte 0x215b
 7837 218a 02          	.byte 0x2
 7838 218b 23          	.byte 0x23
 7839 218c 00          	.uleb128 0x0
 7840 218d 00          	.byte 0x0
 7841 218e 0B          	.uleb128 0xb
 7842 218f 49 50 43 31 	.asciz "IPC13BITS"
 7842      33 42 49 54 
 7842      53 00 
 7843 2199 02          	.byte 0x2
 7844 219a 37 28       	.2byte 0x2837
MPLAB XC16 ASSEMBLY Listing:   			page 160


 7845 219c 6F 21 00 00 	.4byte 0x216f
 7846 21a0 0C          	.uleb128 0xc
 7847 21a1 02          	.byte 0x2
 7848 21a2 02          	.uleb128 0x2
 7849 21a3 02          	.byte 0x2
 7850 21a4 07          	.byte 0x7
 7851 21a5 73 68 6F 72 	.asciz "short unsigned int"
 7851      74 20 75 6E 
 7851      73 69 67 6E 
 7851      65 64 20 69 
 7851      6E 74 00 
 7852 21b8 0D          	.uleb128 0xd
 7853 21b9 01          	.byte 0x1
 7854 21ba 64 65 66 61 	.asciz "default_timer"
 7854      75 6C 74 5F 
 7854      74 69 6D 65 
 7854      72 00 
 7855 21c8 01          	.byte 0x1
 7856 21c9 2C          	.byte 0x2c
 7857 21ca 01          	.byte 0x1
 7858 21cb 00 00 00 00 	.4byte .LFB0
 7859 21cf 00 00 00 00 	.4byte .LFE0
 7860 21d3 01          	.byte 0x1
 7861 21d4 5E          	.byte 0x5e
 7862 21d5 0E          	.uleb128 0xe
 7863 21d6 01          	.byte 0x1
 7864 21d7 63 6F 6E 66 	.asciz "config_timer1"
 7864      69 67 5F 74 
 7864      69 6D 65 72 
 7864      31 00 
 7865 21e5 01          	.byte 0x1
 7866 21e6 36          	.byte 0x36
 7867 21e7 01          	.byte 0x1
 7868 21e8 00 00 00 00 	.4byte .LFB1
 7869 21ec 00 00 00 00 	.4byte .LFE1
 7870 21f0 01          	.byte 0x1
 7871 21f1 5E          	.byte 0x5e
 7872 21f2 21 22 00 00 	.4byte 0x2221
 7873 21f6 0F          	.uleb128 0xf
 7874 21f7 00 00 00 00 	.4byte .LASF5
 7875 21fb 01          	.byte 0x1
 7876 21fc 36          	.byte 0x36
 7877 21fd FA 00 00 00 	.4byte 0xfa
 7878 2201 02          	.byte 0x2
 7879 2202 7E          	.byte 0x7e
 7880 2203 00          	.sleb128 0
 7881 2204 0F          	.uleb128 0xf
 7882 2205 00 00 00 00 	.4byte .LASF6
 7883 2209 01          	.byte 0x1
 7884 220a 36          	.byte 0x36
 7885 220b FA 00 00 00 	.4byte 0xfa
 7886 220f 02          	.byte 0x2
 7887 2210 7E          	.byte 0x7e
 7888 2211 02          	.sleb128 2
 7889 2212 0F          	.uleb128 0xf
 7890 2213 00 00 00 00 	.4byte .LASF7
 7891 2217 01          	.byte 0x1
MPLAB XC16 ASSEMBLY Listing:   			page 161


 7892 2218 37          	.byte 0x37
 7893 2219 27 22 00 00 	.4byte 0x2227
 7894 221d 02          	.byte 0x2
 7895 221e 7E          	.byte 0x7e
 7896 221f 04          	.sleb128 4
 7897 2220 00          	.byte 0x0
 7898 2221 10          	.uleb128 0x10
 7899 2222 01          	.byte 0x1
 7900 2223 A0 21 00 00 	.4byte 0x21a0
 7901 2227 11          	.uleb128 0x11
 7902 2228 02          	.byte 0x2
 7903 2229 21 22 00 00 	.4byte 0x2221
 7904 222d 0E          	.uleb128 0xe
 7905 222e 01          	.byte 0x1
 7906 222f 63 6F 6E 66 	.asciz "config_timer1_us"
 7906      69 67 5F 74 
 7906      69 6D 65 72 
 7906      31 5F 75 73 
 7906      00 
 7907 2240 01          	.byte 0x1
 7908 2241 53          	.byte 0x53
 7909 2242 01          	.byte 0x1
 7910 2243 00 00 00 00 	.4byte .LFB2
 7911 2247 00 00 00 00 	.4byte .LFE2
 7912 224b 01          	.byte 0x1
 7913 224c 5E          	.byte 0x5e
 7914 224d 6E 22 00 00 	.4byte 0x226e
 7915 2251 0F          	.uleb128 0xf
 7916 2252 00 00 00 00 	.4byte .LASF5
 7917 2256 01          	.byte 0x1
 7918 2257 53          	.byte 0x53
 7919 2258 FA 00 00 00 	.4byte 0xfa
 7920 225c 02          	.byte 0x2
 7921 225d 7E          	.byte 0x7e
 7922 225e 00          	.sleb128 0
 7923 225f 0F          	.uleb128 0xf
 7924 2260 00 00 00 00 	.4byte .LASF6
 7925 2264 01          	.byte 0x1
 7926 2265 53          	.byte 0x53
 7927 2266 FA 00 00 00 	.4byte 0xfa
 7928 226a 02          	.byte 0x2
 7929 226b 7E          	.byte 0x7e
 7930 226c 02          	.sleb128 2
 7931 226d 00          	.byte 0x0
 7932 226e 0E          	.uleb128 0xe
 7933 226f 01          	.byte 0x1
 7934 2270 63 6F 6E 66 	.asciz "config_timer2"
 7934      69 67 5F 74 
 7934      69 6D 65 72 
 7934      32 00 
 7935 227e 01          	.byte 0x1
 7936 227f 6D          	.byte 0x6d
 7937 2280 01          	.byte 0x1
 7938 2281 00 00 00 00 	.4byte .LFB3
 7939 2285 00 00 00 00 	.4byte .LFE3
 7940 2289 01          	.byte 0x1
 7941 228a 5E          	.byte 0x5e
MPLAB XC16 ASSEMBLY Listing:   			page 162


 7942 228b BA 22 00 00 	.4byte 0x22ba
 7943 228f 0F          	.uleb128 0xf
 7944 2290 00 00 00 00 	.4byte .LASF5
 7945 2294 01          	.byte 0x1
 7946 2295 6D          	.byte 0x6d
 7947 2296 FA 00 00 00 	.4byte 0xfa
 7948 229a 02          	.byte 0x2
 7949 229b 7E          	.byte 0x7e
 7950 229c 00          	.sleb128 0
 7951 229d 0F          	.uleb128 0xf
 7952 229e 00 00 00 00 	.4byte .LASF6
 7953 22a2 01          	.byte 0x1
 7954 22a3 6D          	.byte 0x6d
 7955 22a4 FA 00 00 00 	.4byte 0xfa
 7956 22a8 02          	.byte 0x2
 7957 22a9 7E          	.byte 0x7e
 7958 22aa 02          	.sleb128 2
 7959 22ab 0F          	.uleb128 0xf
 7960 22ac 00 00 00 00 	.4byte .LASF7
 7961 22b0 01          	.byte 0x1
 7962 22b1 6E          	.byte 0x6e
 7963 22b2 27 22 00 00 	.4byte 0x2227
 7964 22b6 02          	.byte 0x2
 7965 22b7 7E          	.byte 0x7e
 7966 22b8 04          	.sleb128 4
 7967 22b9 00          	.byte 0x0
 7968 22ba 0E          	.uleb128 0xe
 7969 22bb 01          	.byte 0x1
 7970 22bc 63 6F 6E 66 	.asciz "config_timer2_us"
 7970      69 67 5F 74 
 7970      69 6D 65 72 
 7970      32 5F 75 73 
 7970      00 
 7971 22cd 01          	.byte 0x1
 7972 22ce 8B          	.byte 0x8b
 7973 22cf 01          	.byte 0x1
 7974 22d0 00 00 00 00 	.4byte .LFB4
 7975 22d4 00 00 00 00 	.4byte .LFE4
 7976 22d8 01          	.byte 0x1
 7977 22d9 5E          	.byte 0x5e
 7978 22da FB 22 00 00 	.4byte 0x22fb
 7979 22de 0F          	.uleb128 0xf
 7980 22df 00 00 00 00 	.4byte .LASF5
 7981 22e3 01          	.byte 0x1
 7982 22e4 8B          	.byte 0x8b
 7983 22e5 FA 00 00 00 	.4byte 0xfa
 7984 22e9 02          	.byte 0x2
 7985 22ea 7E          	.byte 0x7e
 7986 22eb 00          	.sleb128 0
 7987 22ec 0F          	.uleb128 0xf
 7988 22ed 00 00 00 00 	.4byte .LASF6
 7989 22f1 01          	.byte 0x1
 7990 22f2 8B          	.byte 0x8b
 7991 22f3 FA 00 00 00 	.4byte 0xfa
 7992 22f7 02          	.byte 0x2
 7993 22f8 7E          	.byte 0x7e
 7994 22f9 02          	.sleb128 2
MPLAB XC16 ASSEMBLY Listing:   			page 163


 7995 22fa 00          	.byte 0x0
 7996 22fb 0E          	.uleb128 0xe
 7997 22fc 01          	.byte 0x1
 7998 22fd 63 6F 6E 66 	.asciz "config_timer3"
 7998      69 67 5F 74 
 7998      69 6D 65 72 
 7998      33 00 
 7999 230b 01          	.byte 0x1
 8000 230c A6          	.byte 0xa6
 8001 230d 01          	.byte 0x1
 8002 230e 00 00 00 00 	.4byte .LFB5
 8003 2312 00 00 00 00 	.4byte .LFE5
 8004 2316 01          	.byte 0x1
 8005 2317 5E          	.byte 0x5e
 8006 2318 47 23 00 00 	.4byte 0x2347
 8007 231c 0F          	.uleb128 0xf
 8008 231d 00 00 00 00 	.4byte .LASF5
 8009 2321 01          	.byte 0x1
 8010 2322 A6          	.byte 0xa6
 8011 2323 FA 00 00 00 	.4byte 0xfa
 8012 2327 02          	.byte 0x2
 8013 2328 7E          	.byte 0x7e
 8014 2329 00          	.sleb128 0
 8015 232a 0F          	.uleb128 0xf
 8016 232b 00 00 00 00 	.4byte .LASF6
 8017 232f 01          	.byte 0x1
 8018 2330 A6          	.byte 0xa6
 8019 2331 FA 00 00 00 	.4byte 0xfa
 8020 2335 02          	.byte 0x2
 8021 2336 7E          	.byte 0x7e
 8022 2337 02          	.sleb128 2
 8023 2338 0F          	.uleb128 0xf
 8024 2339 00 00 00 00 	.4byte .LASF7
 8025 233d 01          	.byte 0x1
 8026 233e A7          	.byte 0xa7
 8027 233f 27 22 00 00 	.4byte 0x2227
 8028 2343 02          	.byte 0x2
 8029 2344 7E          	.byte 0x7e
 8030 2345 04          	.sleb128 4
 8031 2346 00          	.byte 0x0
 8032 2347 0E          	.uleb128 0xe
 8033 2348 01          	.byte 0x1
 8034 2349 63 6F 6E 66 	.asciz "config_timer3_us"
 8034      69 67 5F 74 
 8034      69 6D 65 72 
 8034      33 5F 75 73 
 8034      00 
 8035 235a 01          	.byte 0x1
 8036 235b C3          	.byte 0xc3
 8037 235c 01          	.byte 0x1
 8038 235d 00 00 00 00 	.4byte .LFB6
 8039 2361 00 00 00 00 	.4byte .LFE6
 8040 2365 01          	.byte 0x1
 8041 2366 5E          	.byte 0x5e
 8042 2367 88 23 00 00 	.4byte 0x2388
 8043 236b 0F          	.uleb128 0xf
 8044 236c 00 00 00 00 	.4byte .LASF5
MPLAB XC16 ASSEMBLY Listing:   			page 164


 8045 2370 01          	.byte 0x1
 8046 2371 C3          	.byte 0xc3
 8047 2372 FA 00 00 00 	.4byte 0xfa
 8048 2376 02          	.byte 0x2
 8049 2377 7E          	.byte 0x7e
 8050 2378 00          	.sleb128 0
 8051 2379 0F          	.uleb128 0xf
 8052 237a 00 00 00 00 	.4byte .LASF6
 8053 237e 01          	.byte 0x1
 8054 237f C3          	.byte 0xc3
 8055 2380 FA 00 00 00 	.4byte 0xfa
 8056 2384 02          	.byte 0x2
 8057 2385 7E          	.byte 0x7e
 8058 2386 02          	.sleb128 2
 8059 2387 00          	.byte 0x0
 8060 2388 0E          	.uleb128 0xe
 8061 2389 01          	.byte 0x1
 8062 238a 63 6F 6E 66 	.asciz "config_timer4"
 8062      69 67 5F 74 
 8062      69 6D 65 72 
 8062      34 00 
 8063 2398 01          	.byte 0x1
 8064 2399 DC          	.byte 0xdc
 8065 239a 01          	.byte 0x1
 8066 239b 00 00 00 00 	.4byte .LFB7
 8067 239f 00 00 00 00 	.4byte .LFE7
 8068 23a3 01          	.byte 0x1
 8069 23a4 5E          	.byte 0x5e
 8070 23a5 D4 23 00 00 	.4byte 0x23d4
 8071 23a9 0F          	.uleb128 0xf
 8072 23aa 00 00 00 00 	.4byte .LASF5
 8073 23ae 01          	.byte 0x1
 8074 23af DC          	.byte 0xdc
 8075 23b0 FA 00 00 00 	.4byte 0xfa
 8076 23b4 02          	.byte 0x2
 8077 23b5 7E          	.byte 0x7e
 8078 23b6 00          	.sleb128 0
 8079 23b7 0F          	.uleb128 0xf
 8080 23b8 00 00 00 00 	.4byte .LASF6
 8081 23bc 01          	.byte 0x1
 8082 23bd DC          	.byte 0xdc
 8083 23be FA 00 00 00 	.4byte 0xfa
 8084 23c2 02          	.byte 0x2
 8085 23c3 7E          	.byte 0x7e
 8086 23c4 02          	.sleb128 2
 8087 23c5 0F          	.uleb128 0xf
 8088 23c6 00 00 00 00 	.4byte .LASF7
 8089 23ca 01          	.byte 0x1
 8090 23cb DD          	.byte 0xdd
 8091 23cc 27 22 00 00 	.4byte 0x2227
 8092 23d0 02          	.byte 0x2
 8093 23d1 7E          	.byte 0x7e
 8094 23d2 04          	.sleb128 4
 8095 23d3 00          	.byte 0x0
 8096 23d4 0E          	.uleb128 0xe
 8097 23d5 01          	.byte 0x1
 8098 23d6 63 6F 6E 66 	.asciz "config_timer4_us"
MPLAB XC16 ASSEMBLY Listing:   			page 165


 8098      69 67 5F 74 
 8098      69 6D 65 72 
 8098      34 5F 75 73 
 8098      00 
 8099 23e7 01          	.byte 0x1
 8100 23e8 FA          	.byte 0xfa
 8101 23e9 01          	.byte 0x1
 8102 23ea 00 00 00 00 	.4byte .LFB8
 8103 23ee 00 00 00 00 	.4byte .LFE8
 8104 23f2 01          	.byte 0x1
 8105 23f3 5E          	.byte 0x5e
 8106 23f4 15 24 00 00 	.4byte 0x2415
 8107 23f8 0F          	.uleb128 0xf
 8108 23f9 00 00 00 00 	.4byte .LASF5
 8109 23fd 01          	.byte 0x1
 8110 23fe FA          	.byte 0xfa
 8111 23ff FA 00 00 00 	.4byte 0xfa
 8112 2403 02          	.byte 0x2
 8113 2404 7E          	.byte 0x7e
 8114 2405 00          	.sleb128 0
 8115 2406 0F          	.uleb128 0xf
 8116 2407 00 00 00 00 	.4byte .LASF6
 8117 240b 01          	.byte 0x1
 8118 240c FA          	.byte 0xfa
 8119 240d FA 00 00 00 	.4byte 0xfa
 8120 2411 02          	.byte 0x2
 8121 2412 7E          	.byte 0x7e
 8122 2413 02          	.sleb128 2
 8123 2414 00          	.byte 0x0
 8124 2415 12          	.uleb128 0x12
 8125 2416 01          	.byte 0x1
 8126 2417 63 6F 6E 66 	.asciz "config_timer5"
 8126      69 67 5F 74 
 8126      69 6D 65 72 
 8126      35 00 
 8127 2425 01          	.byte 0x1
 8128 2426 14 01       	.2byte 0x114
 8129 2428 01          	.byte 0x1
 8130 2429 00 00 00 00 	.4byte .LFB9
 8131 242d 00 00 00 00 	.4byte .LFE9
 8132 2431 01          	.byte 0x1
 8133 2432 5E          	.byte 0x5e
 8134 2433 65 24 00 00 	.4byte 0x2465
 8135 2437 13          	.uleb128 0x13
 8136 2438 00 00 00 00 	.4byte .LASF5
 8137 243c 01          	.byte 0x1
 8138 243d 14 01       	.2byte 0x114
 8139 243f FA 00 00 00 	.4byte 0xfa
 8140 2443 02          	.byte 0x2
 8141 2444 7E          	.byte 0x7e
 8142 2445 00          	.sleb128 0
 8143 2446 13          	.uleb128 0x13
 8144 2447 00 00 00 00 	.4byte .LASF6
 8145 244b 01          	.byte 0x1
 8146 244c 14 01       	.2byte 0x114
 8147 244e FA 00 00 00 	.4byte 0xfa
 8148 2452 02          	.byte 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 166


 8149 2453 7E          	.byte 0x7e
 8150 2454 02          	.sleb128 2
 8151 2455 13          	.uleb128 0x13
 8152 2456 00 00 00 00 	.4byte .LASF7
 8153 245a 01          	.byte 0x1
 8154 245b 15 01       	.2byte 0x115
 8155 245d 27 22 00 00 	.4byte 0x2227
 8156 2461 02          	.byte 0x2
 8157 2462 7E          	.byte 0x7e
 8158 2463 04          	.sleb128 4
 8159 2464 00          	.byte 0x0
 8160 2465 12          	.uleb128 0x12
 8161 2466 01          	.byte 0x1
 8162 2467 63 6F 6E 66 	.asciz "config_timer5_us"
 8162      69 67 5F 74 
 8162      69 6D 65 72 
 8162      35 5F 75 73 
 8162      00 
 8163 2478 01          	.byte 0x1
 8164 2479 31 01       	.2byte 0x131
 8165 247b 01          	.byte 0x1
 8166 247c 00 00 00 00 	.4byte .LFB10
 8167 2480 00 00 00 00 	.4byte .LFE10
 8168 2484 01          	.byte 0x1
 8169 2485 5E          	.byte 0x5e
 8170 2486 A9 24 00 00 	.4byte 0x24a9
 8171 248a 13          	.uleb128 0x13
 8172 248b 00 00 00 00 	.4byte .LASF5
 8173 248f 01          	.byte 0x1
 8174 2490 31 01       	.2byte 0x131
 8175 2492 FA 00 00 00 	.4byte 0xfa
 8176 2496 02          	.byte 0x2
 8177 2497 7E          	.byte 0x7e
 8178 2498 00          	.sleb128 0
 8179 2499 13          	.uleb128 0x13
 8180 249a 00 00 00 00 	.4byte .LASF6
 8181 249e 01          	.byte 0x1
 8182 249f 31 01       	.2byte 0x131
 8183 24a1 FA 00 00 00 	.4byte 0xfa
 8184 24a5 02          	.byte 0x2
 8185 24a6 7E          	.byte 0x7e
 8186 24a7 02          	.sleb128 2
 8187 24a8 00          	.byte 0x0
 8188 24a9 12          	.uleb128 0x12
 8189 24aa 01          	.byte 0x1
 8190 24ab 63 6F 6E 66 	.asciz "config_timer6"
 8190      69 67 5F 74 
 8190      69 6D 65 72 
 8190      36 00 
 8191 24b9 01          	.byte 0x1
 8192 24ba 4A 01       	.2byte 0x14a
 8193 24bc 01          	.byte 0x1
 8194 24bd 00 00 00 00 	.4byte .LFB11
 8195 24c1 00 00 00 00 	.4byte .LFE11
 8196 24c5 01          	.byte 0x1
 8197 24c6 5E          	.byte 0x5e
 8198 24c7 F9 24 00 00 	.4byte 0x24f9
MPLAB XC16 ASSEMBLY Listing:   			page 167


 8199 24cb 13          	.uleb128 0x13
 8200 24cc 00 00 00 00 	.4byte .LASF5
 8201 24d0 01          	.byte 0x1
 8202 24d1 4A 01       	.2byte 0x14a
 8203 24d3 FA 00 00 00 	.4byte 0xfa
 8204 24d7 02          	.byte 0x2
 8205 24d8 7E          	.byte 0x7e
 8206 24d9 00          	.sleb128 0
 8207 24da 13          	.uleb128 0x13
 8208 24db 00 00 00 00 	.4byte .LASF6
 8209 24df 01          	.byte 0x1
 8210 24e0 4A 01       	.2byte 0x14a
 8211 24e2 FA 00 00 00 	.4byte 0xfa
 8212 24e6 02          	.byte 0x2
 8213 24e7 7E          	.byte 0x7e
 8214 24e8 02          	.sleb128 2
 8215 24e9 13          	.uleb128 0x13
 8216 24ea 00 00 00 00 	.4byte .LASF7
 8217 24ee 01          	.byte 0x1
 8218 24ef 4B 01       	.2byte 0x14b
 8219 24f1 27 22 00 00 	.4byte 0x2227
 8220 24f5 02          	.byte 0x2
 8221 24f6 7E          	.byte 0x7e
 8222 24f7 04          	.sleb128 4
 8223 24f8 00          	.byte 0x0
 8224 24f9 12          	.uleb128 0x12
 8225 24fa 01          	.byte 0x1
 8226 24fb 63 6F 6E 66 	.asciz "config_timer6_us"
 8226      69 67 5F 74 
 8226      69 6D 65 72 
 8226      36 5F 75 73 
 8226      00 
 8227 250c 01          	.byte 0x1
 8228 250d 68 01       	.2byte 0x168
 8229 250f 01          	.byte 0x1
 8230 2510 00 00 00 00 	.4byte .LFB12
 8231 2514 00 00 00 00 	.4byte .LFE12
 8232 2518 01          	.byte 0x1
 8233 2519 5E          	.byte 0x5e
 8234 251a 3D 25 00 00 	.4byte 0x253d
 8235 251e 13          	.uleb128 0x13
 8236 251f 00 00 00 00 	.4byte .LASF5
 8237 2523 01          	.byte 0x1
 8238 2524 68 01       	.2byte 0x168
 8239 2526 FA 00 00 00 	.4byte 0xfa
 8240 252a 02          	.byte 0x2
 8241 252b 7E          	.byte 0x7e
 8242 252c 00          	.sleb128 0
 8243 252d 13          	.uleb128 0x13
 8244 252e 00 00 00 00 	.4byte .LASF6
 8245 2532 01          	.byte 0x1
 8246 2533 68 01       	.2byte 0x168
 8247 2535 FA 00 00 00 	.4byte 0xfa
 8248 2539 02          	.byte 0x2
 8249 253a 7E          	.byte 0x7e
 8250 253b 02          	.sleb128 2
 8251 253c 00          	.byte 0x0
MPLAB XC16 ASSEMBLY Listing:   			page 168


 8252 253d 12          	.uleb128 0x12
 8253 253e 01          	.byte 0x1
 8254 253f 63 6F 6E 66 	.asciz "config_timer7"
 8254      69 67 5F 74 
 8254      69 6D 65 72 
 8254      37 00 
 8255 254d 01          	.byte 0x1
 8256 254e 82 01       	.2byte 0x182
 8257 2550 01          	.byte 0x1
 8258 2551 00 00 00 00 	.4byte .LFB13
 8259 2555 00 00 00 00 	.4byte .LFE13
 8260 2559 01          	.byte 0x1
 8261 255a 5E          	.byte 0x5e
 8262 255b 8D 25 00 00 	.4byte 0x258d
 8263 255f 13          	.uleb128 0x13
 8264 2560 00 00 00 00 	.4byte .LASF5
 8265 2564 01          	.byte 0x1
 8266 2565 82 01       	.2byte 0x182
 8267 2567 FA 00 00 00 	.4byte 0xfa
 8268 256b 02          	.byte 0x2
 8269 256c 7E          	.byte 0x7e
 8270 256d 00          	.sleb128 0
 8271 256e 13          	.uleb128 0x13
 8272 256f 00 00 00 00 	.4byte .LASF6
 8273 2573 01          	.byte 0x1
 8274 2574 82 01       	.2byte 0x182
 8275 2576 FA 00 00 00 	.4byte 0xfa
 8276 257a 02          	.byte 0x2
 8277 257b 7E          	.byte 0x7e
 8278 257c 02          	.sleb128 2
 8279 257d 13          	.uleb128 0x13
 8280 257e 00 00 00 00 	.4byte .LASF7
 8281 2582 01          	.byte 0x1
 8282 2583 83 01       	.2byte 0x183
 8283 2585 27 22 00 00 	.4byte 0x2227
 8284 2589 02          	.byte 0x2
 8285 258a 7E          	.byte 0x7e
 8286 258b 04          	.sleb128 4
 8287 258c 00          	.byte 0x0
 8288 258d 12          	.uleb128 0x12
 8289 258e 01          	.byte 0x1
 8290 258f 63 6F 6E 66 	.asciz "config_timer7_us"
 8290      69 67 5F 74 
 8290      69 6D 65 72 
 8290      37 5F 75 73 
 8290      00 
 8291 25a0 01          	.byte 0x1
 8292 25a1 9F 01       	.2byte 0x19f
 8293 25a3 01          	.byte 0x1
 8294 25a4 00 00 00 00 	.4byte .LFB14
 8295 25a8 00 00 00 00 	.4byte .LFE14
 8296 25ac 01          	.byte 0x1
 8297 25ad 5E          	.byte 0x5e
 8298 25ae D1 25 00 00 	.4byte 0x25d1
 8299 25b2 13          	.uleb128 0x13
 8300 25b3 00 00 00 00 	.4byte .LASF5
 8301 25b7 01          	.byte 0x1
MPLAB XC16 ASSEMBLY Listing:   			page 169


 8302 25b8 9F 01       	.2byte 0x19f
 8303 25ba FA 00 00 00 	.4byte 0xfa
 8304 25be 02          	.byte 0x2
 8305 25bf 7E          	.byte 0x7e
 8306 25c0 00          	.sleb128 0
 8307 25c1 13          	.uleb128 0x13
 8308 25c2 00 00 00 00 	.4byte .LASF6
 8309 25c6 01          	.byte 0x1
 8310 25c7 9F 01       	.2byte 0x19f
 8311 25c9 FA 00 00 00 	.4byte 0xfa
 8312 25cd 02          	.byte 0x2
 8313 25ce 7E          	.byte 0x7e
 8314 25cf 02          	.sleb128 2
 8315 25d0 00          	.byte 0x0
 8316 25d1 12          	.uleb128 0x12
 8317 25d2 01          	.byte 0x1
 8318 25d3 63 6F 6E 66 	.asciz "config_timer8"
 8318      69 67 5F 74 
 8318      69 6D 65 72 
 8318      38 00 
 8319 25e1 01          	.byte 0x1
 8320 25e2 B8 01       	.2byte 0x1b8
 8321 25e4 01          	.byte 0x1
 8322 25e5 00 00 00 00 	.4byte .LFB15
 8323 25e9 00 00 00 00 	.4byte .LFE15
 8324 25ed 01          	.byte 0x1
 8325 25ee 5E          	.byte 0x5e
 8326 25ef 21 26 00 00 	.4byte 0x2621
 8327 25f3 13          	.uleb128 0x13
 8328 25f4 00 00 00 00 	.4byte .LASF5
 8329 25f8 01          	.byte 0x1
 8330 25f9 B8 01       	.2byte 0x1b8
 8331 25fb FA 00 00 00 	.4byte 0xfa
 8332 25ff 02          	.byte 0x2
 8333 2600 7E          	.byte 0x7e
 8334 2601 00          	.sleb128 0
 8335 2602 13          	.uleb128 0x13
 8336 2603 00 00 00 00 	.4byte .LASF6
 8337 2607 01          	.byte 0x1
 8338 2608 B8 01       	.2byte 0x1b8
 8339 260a FA 00 00 00 	.4byte 0xfa
 8340 260e 02          	.byte 0x2
 8341 260f 7E          	.byte 0x7e
 8342 2610 02          	.sleb128 2
 8343 2611 13          	.uleb128 0x13
 8344 2612 00 00 00 00 	.4byte .LASF7
 8345 2616 01          	.byte 0x1
 8346 2617 B9 01       	.2byte 0x1b9
 8347 2619 27 22 00 00 	.4byte 0x2227
 8348 261d 02          	.byte 0x2
 8349 261e 7E          	.byte 0x7e
 8350 261f 04          	.sleb128 4
 8351 2620 00          	.byte 0x0
 8352 2621 12          	.uleb128 0x12
 8353 2622 01          	.byte 0x1
 8354 2623 63 6F 6E 66 	.asciz "config_timer8_us"
 8354      69 67 5F 74 
MPLAB XC16 ASSEMBLY Listing:   			page 170


 8354      69 6D 65 72 
 8354      38 5F 75 73 
 8354      00 
 8355 2634 01          	.byte 0x1
 8356 2635 D6 01       	.2byte 0x1d6
 8357 2637 01          	.byte 0x1
 8358 2638 00 00 00 00 	.4byte .LFB16
 8359 263c 00 00 00 00 	.4byte .LFE16
 8360 2640 01          	.byte 0x1
 8361 2641 5E          	.byte 0x5e
 8362 2642 65 26 00 00 	.4byte 0x2665
 8363 2646 13          	.uleb128 0x13
 8364 2647 00 00 00 00 	.4byte .LASF5
 8365 264b 01          	.byte 0x1
 8366 264c D6 01       	.2byte 0x1d6
 8367 264e FA 00 00 00 	.4byte 0xfa
 8368 2652 02          	.byte 0x2
 8369 2653 7E          	.byte 0x7e
 8370 2654 00          	.sleb128 0
 8371 2655 13          	.uleb128 0x13
 8372 2656 00 00 00 00 	.4byte .LASF6
 8373 265a 01          	.byte 0x1
 8374 265b D6 01       	.2byte 0x1d6
 8375 265d FA 00 00 00 	.4byte 0xfa
 8376 2661 02          	.byte 0x2
 8377 2662 7E          	.byte 0x7e
 8378 2663 02          	.sleb128 2
 8379 2664 00          	.byte 0x0
 8380 2665 12          	.uleb128 0x12
 8381 2666 01          	.byte 0x1
 8382 2667 63 6F 6E 66 	.asciz "config_timer9"
 8382      69 67 5F 74 
 8382      69 6D 65 72 
 8382      39 00 
 8383 2675 01          	.byte 0x1
 8384 2676 F0 01       	.2byte 0x1f0
 8385 2678 01          	.byte 0x1
 8386 2679 00 00 00 00 	.4byte .LFB17
 8387 267d 00 00 00 00 	.4byte .LFE17
 8388 2681 01          	.byte 0x1
 8389 2682 5E          	.byte 0x5e
 8390 2683 B5 26 00 00 	.4byte 0x26b5
 8391 2687 13          	.uleb128 0x13
 8392 2688 00 00 00 00 	.4byte .LASF5
 8393 268c 01          	.byte 0x1
 8394 268d F0 01       	.2byte 0x1f0
 8395 268f FA 00 00 00 	.4byte 0xfa
 8396 2693 02          	.byte 0x2
 8397 2694 7E          	.byte 0x7e
 8398 2695 00          	.sleb128 0
 8399 2696 13          	.uleb128 0x13
 8400 2697 00 00 00 00 	.4byte .LASF6
 8401 269b 01          	.byte 0x1
 8402 269c F0 01       	.2byte 0x1f0
 8403 269e FA 00 00 00 	.4byte 0xfa
 8404 26a2 02          	.byte 0x2
 8405 26a3 7E          	.byte 0x7e
MPLAB XC16 ASSEMBLY Listing:   			page 171


 8406 26a4 02          	.sleb128 2
 8407 26a5 13          	.uleb128 0x13
 8408 26a6 00 00 00 00 	.4byte .LASF7
 8409 26aa 01          	.byte 0x1
 8410 26ab F1 01       	.2byte 0x1f1
 8411 26ad 27 22 00 00 	.4byte 0x2227
 8412 26b1 02          	.byte 0x2
 8413 26b2 7E          	.byte 0x7e
 8414 26b3 04          	.sleb128 4
 8415 26b4 00          	.byte 0x0
 8416 26b5 12          	.uleb128 0x12
 8417 26b6 01          	.byte 0x1
 8418 26b7 63 6F 6E 66 	.asciz "config_timer9_us"
 8418      69 67 5F 74 
 8418      69 6D 65 72 
 8418      39 5F 75 73 
 8418      00 
 8419 26c8 01          	.byte 0x1
 8420 26c9 0D 02       	.2byte 0x20d
 8421 26cb 01          	.byte 0x1
 8422 26cc 00 00 00 00 	.4byte .LFB18
 8423 26d0 00 00 00 00 	.4byte .LFE18
 8424 26d4 01          	.byte 0x1
 8425 26d5 5E          	.byte 0x5e
 8426 26d6 F9 26 00 00 	.4byte 0x26f9
 8427 26da 13          	.uleb128 0x13
 8428 26db 00 00 00 00 	.4byte .LASF5
 8429 26df 01          	.byte 0x1
 8430 26e0 0D 02       	.2byte 0x20d
 8431 26e2 FA 00 00 00 	.4byte 0xfa
 8432 26e6 02          	.byte 0x2
 8433 26e7 7E          	.byte 0x7e
 8434 26e8 00          	.sleb128 0
 8435 26e9 13          	.uleb128 0x13
 8436 26ea 00 00 00 00 	.4byte .LASF6
 8437 26ee 01          	.byte 0x1
 8438 26ef 0D 02       	.2byte 0x20d
 8439 26f1 FA 00 00 00 	.4byte 0xfa
 8440 26f5 02          	.byte 0x2
 8441 26f6 7E          	.byte 0x7e
 8442 26f7 02          	.sleb128 2
 8443 26f8 00          	.byte 0x0
 8444 26f9 14          	.uleb128 0x14
 8445 26fa 01          	.byte 0x1
 8446 26fb 65 6E 61 62 	.asciz "enable_timer1"
 8446      6C 65 5F 74 
 8446      69 6D 65 72 
 8446      31 00 
 8447 2709 01          	.byte 0x1
 8448 270a 23 02       	.2byte 0x223
 8449 270c 01          	.byte 0x1
 8450 270d 00 00 00 00 	.4byte .LFB19
 8451 2711 00 00 00 00 	.4byte .LFE19
 8452 2715 01          	.byte 0x1
 8453 2716 5E          	.byte 0x5e
 8454 2717 14          	.uleb128 0x14
 8455 2718 01          	.byte 0x1
MPLAB XC16 ASSEMBLY Listing:   			page 172


 8456 2719 65 6E 61 62 	.asciz "enable_timer2"
 8456      6C 65 5F 74 
 8456      69 6D 65 72 
 8456      32 00 
 8457 2727 01          	.byte 0x1
 8458 2728 29 02       	.2byte 0x229
 8459 272a 01          	.byte 0x1
 8460 272b 00 00 00 00 	.4byte .LFB20
 8461 272f 00 00 00 00 	.4byte .LFE20
 8462 2733 01          	.byte 0x1
 8463 2734 5E          	.byte 0x5e
 8464 2735 14          	.uleb128 0x14
 8465 2736 01          	.byte 0x1
 8466 2737 65 6E 61 62 	.asciz "enable_timer3"
 8466      6C 65 5F 74 
 8466      69 6D 65 72 
 8466      33 00 
 8467 2745 01          	.byte 0x1
 8468 2746 2F 02       	.2byte 0x22f
 8469 2748 01          	.byte 0x1
 8470 2749 00 00 00 00 	.4byte .LFB21
 8471 274d 00 00 00 00 	.4byte .LFE21
 8472 2751 01          	.byte 0x1
 8473 2752 5E          	.byte 0x5e
 8474 2753 14          	.uleb128 0x14
 8475 2754 01          	.byte 0x1
 8476 2755 65 6E 61 62 	.asciz "enable_timer4"
 8476      6C 65 5F 74 
 8476      69 6D 65 72 
 8476      34 00 
 8477 2763 01          	.byte 0x1
 8478 2764 35 02       	.2byte 0x235
 8479 2766 01          	.byte 0x1
 8480 2767 00 00 00 00 	.4byte .LFB22
 8481 276b 00 00 00 00 	.4byte .LFE22
 8482 276f 01          	.byte 0x1
 8483 2770 5E          	.byte 0x5e
 8484 2771 14          	.uleb128 0x14
 8485 2772 01          	.byte 0x1
 8486 2773 65 6E 61 62 	.asciz "enable_timer5"
 8486      6C 65 5F 74 
 8486      69 6D 65 72 
 8486      35 00 
 8487 2781 01          	.byte 0x1
 8488 2782 3B 02       	.2byte 0x23b
 8489 2784 01          	.byte 0x1
 8490 2785 00 00 00 00 	.4byte .LFB23
 8491 2789 00 00 00 00 	.4byte .LFE23
 8492 278d 01          	.byte 0x1
 8493 278e 5E          	.byte 0x5e
 8494 278f 14          	.uleb128 0x14
 8495 2790 01          	.byte 0x1
 8496 2791 65 6E 61 62 	.asciz "enable_timer6"
 8496      6C 65 5F 74 
 8496      69 6D 65 72 
 8496      36 00 
 8497 279f 01          	.byte 0x1
MPLAB XC16 ASSEMBLY Listing:   			page 173


 8498 27a0 41 02       	.2byte 0x241
 8499 27a2 01          	.byte 0x1
 8500 27a3 00 00 00 00 	.4byte .LFB24
 8501 27a7 00 00 00 00 	.4byte .LFE24
 8502 27ab 01          	.byte 0x1
 8503 27ac 5E          	.byte 0x5e
 8504 27ad 14          	.uleb128 0x14
 8505 27ae 01          	.byte 0x1
 8506 27af 65 6E 61 62 	.asciz "enable_timer7"
 8506      6C 65 5F 74 
 8506      69 6D 65 72 
 8506      37 00 
 8507 27bd 01          	.byte 0x1
 8508 27be 47 02       	.2byte 0x247
 8509 27c0 01          	.byte 0x1
 8510 27c1 00 00 00 00 	.4byte .LFB25
 8511 27c5 00 00 00 00 	.4byte .LFE25
 8512 27c9 01          	.byte 0x1
 8513 27ca 5E          	.byte 0x5e
 8514 27cb 14          	.uleb128 0x14
 8515 27cc 01          	.byte 0x1
 8516 27cd 65 6E 61 62 	.asciz "enable_timer8"
 8516      6C 65 5F 74 
 8516      69 6D 65 72 
 8516      38 00 
 8517 27db 01          	.byte 0x1
 8518 27dc 4D 02       	.2byte 0x24d
 8519 27de 01          	.byte 0x1
 8520 27df 00 00 00 00 	.4byte .LFB26
 8521 27e3 00 00 00 00 	.4byte .LFE26
 8522 27e7 01          	.byte 0x1
 8523 27e8 5E          	.byte 0x5e
 8524 27e9 14          	.uleb128 0x14
 8525 27ea 01          	.byte 0x1
 8526 27eb 65 6E 61 62 	.asciz "enable_timer9"
 8526      6C 65 5F 74 
 8526      69 6D 65 72 
 8526      39 00 
 8527 27f9 01          	.byte 0x1
 8528 27fa 53 02       	.2byte 0x253
 8529 27fc 01          	.byte 0x1
 8530 27fd 00 00 00 00 	.4byte .LFB27
 8531 2801 00 00 00 00 	.4byte .LFE27
 8532 2805 01          	.byte 0x1
 8533 2806 5E          	.byte 0x5e
 8534 2807 14          	.uleb128 0x14
 8535 2808 01          	.byte 0x1
 8536 2809 64 69 73 61 	.asciz "disable_timer1"
 8536      62 6C 65 5F 
 8536      74 69 6D 65 
 8536      72 31 00 
 8537 2818 01          	.byte 0x1
 8538 2819 5A 02       	.2byte 0x25a
 8539 281b 01          	.byte 0x1
 8540 281c 00 00 00 00 	.4byte .LFB28
 8541 2820 00 00 00 00 	.4byte .LFE28
 8542 2824 01          	.byte 0x1
MPLAB XC16 ASSEMBLY Listing:   			page 174


 8543 2825 5E          	.byte 0x5e
 8544 2826 14          	.uleb128 0x14
 8545 2827 01          	.byte 0x1
 8546 2828 64 69 73 61 	.asciz "disable_timer2"
 8546      62 6C 65 5F 
 8546      74 69 6D 65 
 8546      72 32 00 
 8547 2837 01          	.byte 0x1
 8548 2838 60 02       	.2byte 0x260
 8549 283a 01          	.byte 0x1
 8550 283b 00 00 00 00 	.4byte .LFB29
 8551 283f 00 00 00 00 	.4byte .LFE29
 8552 2843 01          	.byte 0x1
 8553 2844 5E          	.byte 0x5e
 8554 2845 14          	.uleb128 0x14
 8555 2846 01          	.byte 0x1
 8556 2847 64 69 73 61 	.asciz "disable_timer3"
 8556      62 6C 65 5F 
 8556      74 69 6D 65 
 8556      72 33 00 
 8557 2856 01          	.byte 0x1
 8558 2857 66 02       	.2byte 0x266
 8559 2859 01          	.byte 0x1
 8560 285a 00 00 00 00 	.4byte .LFB30
 8561 285e 00 00 00 00 	.4byte .LFE30
 8562 2862 01          	.byte 0x1
 8563 2863 5E          	.byte 0x5e
 8564 2864 14          	.uleb128 0x14
 8565 2865 01          	.byte 0x1
 8566 2866 64 69 73 61 	.asciz "disable_timer4"
 8566      62 6C 65 5F 
 8566      74 69 6D 65 
 8566      72 34 00 
 8567 2875 01          	.byte 0x1
 8568 2876 6C 02       	.2byte 0x26c
 8569 2878 01          	.byte 0x1
 8570 2879 00 00 00 00 	.4byte .LFB31
 8571 287d 00 00 00 00 	.4byte .LFE31
 8572 2881 01          	.byte 0x1
 8573 2882 5E          	.byte 0x5e
 8574 2883 14          	.uleb128 0x14
 8575 2884 01          	.byte 0x1
 8576 2885 64 69 73 61 	.asciz "disable_timer5"
 8576      62 6C 65 5F 
 8576      74 69 6D 65 
 8576      72 35 00 
 8577 2894 01          	.byte 0x1
 8578 2895 72 02       	.2byte 0x272
 8579 2897 01          	.byte 0x1
 8580 2898 00 00 00 00 	.4byte .LFB32
 8581 289c 00 00 00 00 	.4byte .LFE32
 8582 28a0 01          	.byte 0x1
 8583 28a1 5E          	.byte 0x5e
 8584 28a2 14          	.uleb128 0x14
 8585 28a3 01          	.byte 0x1
 8586 28a4 64 69 73 61 	.asciz "disable_timer6"
 8586      62 6C 65 5F 
MPLAB XC16 ASSEMBLY Listing:   			page 175


 8586      74 69 6D 65 
 8586      72 36 00 
 8587 28b3 01          	.byte 0x1
 8588 28b4 78 02       	.2byte 0x278
 8589 28b6 01          	.byte 0x1
 8590 28b7 00 00 00 00 	.4byte .LFB33
 8591 28bb 00 00 00 00 	.4byte .LFE33
 8592 28bf 01          	.byte 0x1
 8593 28c0 5E          	.byte 0x5e
 8594 28c1 14          	.uleb128 0x14
 8595 28c2 01          	.byte 0x1
 8596 28c3 64 69 73 61 	.asciz "disable_timer7"
 8596      62 6C 65 5F 
 8596      74 69 6D 65 
 8596      72 37 00 
 8597 28d2 01          	.byte 0x1
 8598 28d3 7E 02       	.2byte 0x27e
 8599 28d5 01          	.byte 0x1
 8600 28d6 00 00 00 00 	.4byte .LFB34
 8601 28da 00 00 00 00 	.4byte .LFE34
 8602 28de 01          	.byte 0x1
 8603 28df 5E          	.byte 0x5e
 8604 28e0 14          	.uleb128 0x14
 8605 28e1 01          	.byte 0x1
 8606 28e2 64 69 73 61 	.asciz "disable_timer8"
 8606      62 6C 65 5F 
 8606      74 69 6D 65 
 8606      72 38 00 
 8607 28f1 01          	.byte 0x1
 8608 28f2 84 02       	.2byte 0x284
 8609 28f4 01          	.byte 0x1
 8610 28f5 00 00 00 00 	.4byte .LFB35
 8611 28f9 00 00 00 00 	.4byte .LFE35
 8612 28fd 01          	.byte 0x1
 8613 28fe 5E          	.byte 0x5e
 8614 28ff 14          	.uleb128 0x14
 8615 2900 01          	.byte 0x1
 8616 2901 64 69 73 61 	.asciz "disable_timer9"
 8616      62 6C 65 5F 
 8616      74 69 6D 65 
 8616      72 39 00 
 8617 2910 01          	.byte 0x1
 8618 2911 8A 02       	.2byte 0x28a
 8619 2913 01          	.byte 0x1
 8620 2914 00 00 00 00 	.4byte .LFB36
 8621 2918 00 00 00 00 	.4byte .LFE36
 8622 291c 01          	.byte 0x1
 8623 291d 5E          	.byte 0x5e
 8624 291e 14          	.uleb128 0x14
 8625 291f 01          	.byte 0x1
 8626 2920 72 65 73 65 	.asciz "reset_timer1"
 8626      74 5F 74 69 
 8626      6D 65 72 31 
 8626      00 
 8627 292d 01          	.byte 0x1
 8628 292e 91 02       	.2byte 0x291
 8629 2930 01          	.byte 0x1
MPLAB XC16 ASSEMBLY Listing:   			page 176


 8630 2931 00 00 00 00 	.4byte .LFB37
 8631 2935 00 00 00 00 	.4byte .LFE37
 8632 2939 01          	.byte 0x1
 8633 293a 5E          	.byte 0x5e
 8634 293b 14          	.uleb128 0x14
 8635 293c 01          	.byte 0x1
 8636 293d 72 65 73 65 	.asciz "reset_timer2"
 8636      74 5F 74 69 
 8636      6D 65 72 32 
 8636      00 
 8637 294a 01          	.byte 0x1
 8638 294b 97 02       	.2byte 0x297
 8639 294d 01          	.byte 0x1
 8640 294e 00 00 00 00 	.4byte .LFB38
 8641 2952 00 00 00 00 	.4byte .LFE38
 8642 2956 01          	.byte 0x1
 8643 2957 5E          	.byte 0x5e
 8644 2958 14          	.uleb128 0x14
 8645 2959 01          	.byte 0x1
 8646 295a 72 65 73 65 	.asciz "reset_timer3"
 8646      74 5F 74 69 
 8646      6D 65 72 33 
 8646      00 
 8647 2967 01          	.byte 0x1
 8648 2968 9D 02       	.2byte 0x29d
 8649 296a 01          	.byte 0x1
 8650 296b 00 00 00 00 	.4byte .LFB39
 8651 296f 00 00 00 00 	.4byte .LFE39
 8652 2973 01          	.byte 0x1
 8653 2974 5E          	.byte 0x5e
 8654 2975 14          	.uleb128 0x14
 8655 2976 01          	.byte 0x1
 8656 2977 72 65 73 65 	.asciz "reset_timer4"
 8656      74 5F 74 69 
 8656      6D 65 72 34 
 8656      00 
 8657 2984 01          	.byte 0x1
 8658 2985 A3 02       	.2byte 0x2a3
 8659 2987 01          	.byte 0x1
 8660 2988 00 00 00 00 	.4byte .LFB40
 8661 298c 00 00 00 00 	.4byte .LFE40
 8662 2990 01          	.byte 0x1
 8663 2991 5E          	.byte 0x5e
 8664 2992 14          	.uleb128 0x14
 8665 2993 01          	.byte 0x1
 8666 2994 72 65 73 65 	.asciz "reset_timer5"
 8666      74 5F 74 69 
 8666      6D 65 72 35 
 8666      00 
 8667 29a1 01          	.byte 0x1
 8668 29a2 A9 02       	.2byte 0x2a9
 8669 29a4 01          	.byte 0x1
 8670 29a5 00 00 00 00 	.4byte .LFB41
 8671 29a9 00 00 00 00 	.4byte .LFE41
 8672 29ad 01          	.byte 0x1
 8673 29ae 5E          	.byte 0x5e
 8674 29af 14          	.uleb128 0x14
MPLAB XC16 ASSEMBLY Listing:   			page 177


 8675 29b0 01          	.byte 0x1
 8676 29b1 72 65 73 65 	.asciz "reset_timer6"
 8676      74 5F 74 69 
 8676      6D 65 72 36 
 8676      00 
 8677 29be 01          	.byte 0x1
 8678 29bf AF 02       	.2byte 0x2af
 8679 29c1 01          	.byte 0x1
 8680 29c2 00 00 00 00 	.4byte .LFB42
 8681 29c6 00 00 00 00 	.4byte .LFE42
 8682 29ca 01          	.byte 0x1
 8683 29cb 5E          	.byte 0x5e
 8684 29cc 14          	.uleb128 0x14
 8685 29cd 01          	.byte 0x1
 8686 29ce 72 65 73 65 	.asciz "reset_timer7"
 8686      74 5F 74 69 
 8686      6D 65 72 37 
 8686      00 
 8687 29db 01          	.byte 0x1
 8688 29dc B5 02       	.2byte 0x2b5
 8689 29de 01          	.byte 0x1
 8690 29df 00 00 00 00 	.4byte .LFB43
 8691 29e3 00 00 00 00 	.4byte .LFE43
 8692 29e7 01          	.byte 0x1
 8693 29e8 5E          	.byte 0x5e
 8694 29e9 14          	.uleb128 0x14
 8695 29ea 01          	.byte 0x1
 8696 29eb 72 65 73 65 	.asciz "reset_timer8"
 8696      74 5F 74 69 
 8696      6D 65 72 38 
 8696      00 
 8697 29f8 01          	.byte 0x1
 8698 29f9 BB 02       	.2byte 0x2bb
 8699 29fb 01          	.byte 0x1
 8700 29fc 00 00 00 00 	.4byte .LFB44
 8701 2a00 00 00 00 00 	.4byte .LFE44
 8702 2a04 01          	.byte 0x1
 8703 2a05 5E          	.byte 0x5e
 8704 2a06 14          	.uleb128 0x14
 8705 2a07 01          	.byte 0x1
 8706 2a08 72 65 73 65 	.asciz "reset_timer9"
 8706      74 5F 74 69 
 8706      6D 65 72 39 
 8706      00 
 8707 2a15 01          	.byte 0x1
 8708 2a16 C1 02       	.2byte 0x2c1
 8709 2a18 01          	.byte 0x1
 8710 2a19 00 00 00 00 	.4byte .LFB45
 8711 2a1d 00 00 00 00 	.4byte .LFE45
 8712 2a21 01          	.byte 0x1
 8713 2a22 5E          	.byte 0x5e
 8714 2a23 14          	.uleb128 0x14
 8715 2a24 01          	.byte 0x1
 8716 2a25 5F 54 31 49 	.asciz "_T1Interrupt"
 8716      6E 74 65 72 
 8716      72 75 70 74 
 8716      00 
MPLAB XC16 ASSEMBLY Listing:   			page 178


 8717 2a32 01          	.byte 0x1
 8718 2a33 C8 02       	.2byte 0x2c8
 8719 2a35 01          	.byte 0x1
 8720 2a36 00 00 00 00 	.4byte .LFB46
 8721 2a3a 00 00 00 00 	.4byte .LFE46
 8722 2a3e 01          	.byte 0x1
 8723 2a3f 5E          	.byte 0x5e
 8724 2a40 14          	.uleb128 0x14
 8725 2a41 01          	.byte 0x1
 8726 2a42 5F 54 32 49 	.asciz "_T2Interrupt"
 8726      6E 74 65 72 
 8726      72 75 70 74 
 8726      00 
 8727 2a4f 01          	.byte 0x1
 8728 2a50 D1 02       	.2byte 0x2d1
 8729 2a52 01          	.byte 0x1
 8730 2a53 00 00 00 00 	.4byte .LFB47
 8731 2a57 00 00 00 00 	.4byte .LFE47
 8732 2a5b 01          	.byte 0x1
 8733 2a5c 5E          	.byte 0x5e
 8734 2a5d 14          	.uleb128 0x14
 8735 2a5e 01          	.byte 0x1
 8736 2a5f 5F 54 33 49 	.asciz "_T3Interrupt"
 8736      6E 74 65 72 
 8736      72 75 70 74 
 8736      00 
 8737 2a6c 01          	.byte 0x1
 8738 2a6d DB 02       	.2byte 0x2db
 8739 2a6f 01          	.byte 0x1
 8740 2a70 00 00 00 00 	.4byte .LFB48
 8741 2a74 00 00 00 00 	.4byte .LFE48
 8742 2a78 01          	.byte 0x1
 8743 2a79 5E          	.byte 0x5e
 8744 2a7a 14          	.uleb128 0x14
 8745 2a7b 01          	.byte 0x1
 8746 2a7c 5F 54 34 49 	.asciz "_T4Interrupt"
 8746      6E 74 65 72 
 8746      72 75 70 74 
 8746      00 
 8747 2a89 01          	.byte 0x1
 8748 2a8a E4 02       	.2byte 0x2e4
 8749 2a8c 01          	.byte 0x1
 8750 2a8d 00 00 00 00 	.4byte .LFB49
 8751 2a91 00 00 00 00 	.4byte .LFE49
 8752 2a95 01          	.byte 0x1
 8753 2a96 5E          	.byte 0x5e
 8754 2a97 14          	.uleb128 0x14
 8755 2a98 01          	.byte 0x1
 8756 2a99 5F 54 35 49 	.asciz "_T5Interrupt"
 8756      6E 74 65 72 
 8756      72 75 70 74 
 8756      00 
 8757 2aa6 01          	.byte 0x1
 8758 2aa7 ED 02       	.2byte 0x2ed
 8759 2aa9 01          	.byte 0x1
 8760 2aaa 00 00 00 00 	.4byte .LFB50
 8761 2aae 00 00 00 00 	.4byte .LFE50
MPLAB XC16 ASSEMBLY Listing:   			page 179


 8762 2ab2 01          	.byte 0x1
 8763 2ab3 5E          	.byte 0x5e
 8764 2ab4 14          	.uleb128 0x14
 8765 2ab5 01          	.byte 0x1
 8766 2ab6 5F 54 36 49 	.asciz "_T6Interrupt"
 8766      6E 74 65 72 
 8766      72 75 70 74 
 8766      00 
 8767 2ac3 01          	.byte 0x1
 8768 2ac4 F6 02       	.2byte 0x2f6
 8769 2ac6 01          	.byte 0x1
 8770 2ac7 00 00 00 00 	.4byte .LFB51
 8771 2acb 00 00 00 00 	.4byte .LFE51
 8772 2acf 01          	.byte 0x1
 8773 2ad0 5E          	.byte 0x5e
 8774 2ad1 14          	.uleb128 0x14
 8775 2ad2 01          	.byte 0x1
 8776 2ad3 5F 54 37 49 	.asciz "_T7Interrupt"
 8776      6E 74 65 72 
 8776      72 75 70 74 
 8776      00 
 8777 2ae0 01          	.byte 0x1
 8778 2ae1 FF 02       	.2byte 0x2ff
 8779 2ae3 01          	.byte 0x1
 8780 2ae4 00 00 00 00 	.4byte .LFB52
 8781 2ae8 00 00 00 00 	.4byte .LFE52
 8782 2aec 01          	.byte 0x1
 8783 2aed 5E          	.byte 0x5e
 8784 2aee 14          	.uleb128 0x14
 8785 2aef 01          	.byte 0x1
 8786 2af0 5F 54 38 49 	.asciz "_T8Interrupt"
 8786      6E 74 65 72 
 8786      72 75 70 74 
 8786      00 
 8787 2afd 01          	.byte 0x1
 8788 2afe 08 03       	.2byte 0x308
 8789 2b00 01          	.byte 0x1
 8790 2b01 00 00 00 00 	.4byte .LFB53
 8791 2b05 00 00 00 00 	.4byte .LFE53
 8792 2b09 01          	.byte 0x1
 8793 2b0a 5E          	.byte 0x5e
 8794 2b0b 14          	.uleb128 0x14
 8795 2b0c 01          	.byte 0x1
 8796 2b0d 5F 54 39 49 	.asciz "_T9Interrupt"
 8796      6E 74 65 72 
 8796      72 75 70 74 
 8796      00 
 8797 2b1a 01          	.byte 0x1
 8798 2b1b 11 03       	.2byte 0x311
 8799 2b1d 01          	.byte 0x1
 8800 2b1e 00 00 00 00 	.4byte .LFB54
 8801 2b22 00 00 00 00 	.4byte .LFE54
 8802 2b26 01          	.byte 0x1
 8803 2b27 5E          	.byte 0x5e
 8804 2b28 15          	.uleb128 0x15
 8805 2b29 54 4D 52 31 	.asciz "TMR1"
 8805      00 
MPLAB XC16 ASSEMBLY Listing:   			page 180


 8806 2b2e 02          	.byte 0x2
 8807 2b2f 10 01       	.2byte 0x110
 8808 2b31 37 2B 00 00 	.4byte 0x2b37
 8809 2b35 01          	.byte 0x1
 8810 2b36 01          	.byte 0x1
 8811 2b37 16          	.uleb128 0x16
 8812 2b38 EA 00 00 00 	.4byte 0xea
 8813 2b3c 15          	.uleb128 0x15
 8814 2b3d 50 52 31 00 	.asciz "PR1"
 8815 2b41 02          	.byte 0x2
 8816 2b42 12 01       	.2byte 0x112
 8817 2b44 37 2B 00 00 	.4byte 0x2b37
 8818 2b48 01          	.byte 0x1
 8819 2b49 01          	.byte 0x1
 8820 2b4a 17          	.uleb128 0x17
 8821 2b4b 00 00 00 00 	.4byte .LASF8
 8822 2b4f 02          	.byte 0x2
 8823 2b50 2A 01       	.2byte 0x12a
 8824 2b52 58 2B 00 00 	.4byte 0x2b58
 8825 2b56 01          	.byte 0x1
 8826 2b57 01          	.byte 0x1
 8827 2b58 16          	.uleb128 0x16
 8828 2b59 12 02 00 00 	.4byte 0x212
 8829 2b5d 15          	.uleb128 0x15
 8830 2b5e 54 4D 52 32 	.asciz "TMR2"
 8830      00 
 8831 2b63 02          	.byte 0x2
 8832 2b64 2D 01       	.2byte 0x12d
 8833 2b66 37 2B 00 00 	.4byte 0x2b37
 8834 2b6a 01          	.byte 0x1
 8835 2b6b 01          	.byte 0x1
 8836 2b6c 15          	.uleb128 0x15
 8837 2b6d 54 4D 52 33 	.asciz "TMR3"
 8837      00 
 8838 2b72 02          	.byte 0x2
 8839 2b73 31 01       	.2byte 0x131
 8840 2b75 37 2B 00 00 	.4byte 0x2b37
 8841 2b79 01          	.byte 0x1
 8842 2b7a 01          	.byte 0x1
 8843 2b7b 15          	.uleb128 0x15
 8844 2b7c 50 52 32 00 	.asciz "PR2"
 8845 2b80 02          	.byte 0x2
 8846 2b81 33 01       	.2byte 0x133
 8847 2b83 37 2B 00 00 	.4byte 0x2b37
 8848 2b87 01          	.byte 0x1
 8849 2b88 01          	.byte 0x1
 8850 2b89 15          	.uleb128 0x15
 8851 2b8a 50 52 33 00 	.asciz "PR3"
 8852 2b8e 02          	.byte 0x2
 8853 2b8f 35 01       	.2byte 0x135
 8854 2b91 37 2B 00 00 	.4byte 0x2b37
 8855 2b95 01          	.byte 0x1
 8856 2b96 01          	.byte 0x1
 8857 2b97 17          	.uleb128 0x17
 8858 2b98 00 00 00 00 	.4byte .LASF9
 8859 2b9c 02          	.byte 0x2
 8860 2b9d 4D 01       	.2byte 0x14d
MPLAB XC16 ASSEMBLY Listing:   			page 181


 8861 2b9f A5 2B 00 00 	.4byte 0x2ba5
 8862 2ba3 01          	.byte 0x1
 8863 2ba4 01          	.byte 0x1
 8864 2ba5 16          	.uleb128 0x16
 8865 2ba6 FB 02 00 00 	.4byte 0x2fb
 8866 2baa 17          	.uleb128 0x17
 8867 2bab 00 00 00 00 	.4byte .LASF10
 8868 2baf 02          	.byte 0x2
 8869 2bb0 65 01       	.2byte 0x165
 8870 2bb2 B8 2B 00 00 	.4byte 0x2bb8
 8871 2bb6 01          	.byte 0x1
 8872 2bb7 01          	.byte 0x1
 8873 2bb8 16          	.uleb128 0x16
 8874 2bb9 D2 03 00 00 	.4byte 0x3d2
 8875 2bbd 15          	.uleb128 0x15
 8876 2bbe 54 4D 52 34 	.asciz "TMR4"
 8876      00 
 8877 2bc3 02          	.byte 0x2
 8878 2bc4 68 01       	.2byte 0x168
 8879 2bc6 37 2B 00 00 	.4byte 0x2b37
 8880 2bca 01          	.byte 0x1
 8881 2bcb 01          	.byte 0x1
 8882 2bcc 15          	.uleb128 0x15
 8883 2bcd 54 4D 52 35 	.asciz "TMR5"
 8883      00 
 8884 2bd2 02          	.byte 0x2
 8885 2bd3 6C 01       	.2byte 0x16c
 8886 2bd5 37 2B 00 00 	.4byte 0x2b37
 8887 2bd9 01          	.byte 0x1
 8888 2bda 01          	.byte 0x1
 8889 2bdb 15          	.uleb128 0x15
 8890 2bdc 50 52 34 00 	.asciz "PR4"
 8891 2be0 02          	.byte 0x2
 8892 2be1 6E 01       	.2byte 0x16e
 8893 2be3 37 2B 00 00 	.4byte 0x2b37
 8894 2be7 01          	.byte 0x1
 8895 2be8 01          	.byte 0x1
 8896 2be9 15          	.uleb128 0x15
 8897 2bea 50 52 35 00 	.asciz "PR5"
 8898 2bee 02          	.byte 0x2
 8899 2bef 70 01       	.2byte 0x170
 8900 2bf1 37 2B 00 00 	.4byte 0x2b37
 8901 2bf5 01          	.byte 0x1
 8902 2bf6 01          	.byte 0x1
 8903 2bf7 17          	.uleb128 0x17
 8904 2bf8 00 00 00 00 	.4byte .LASF11
 8905 2bfc 02          	.byte 0x2
 8906 2bfd 88 01       	.2byte 0x188
 8907 2bff 05 2C 00 00 	.4byte 0x2c05
 8908 2c03 01          	.byte 0x1
 8909 2c04 01          	.byte 0x1
 8910 2c05 16          	.uleb128 0x16
 8911 2c06 BB 04 00 00 	.4byte 0x4bb
 8912 2c0a 17          	.uleb128 0x17
 8913 2c0b 00 00 00 00 	.4byte .LASF12
 8914 2c0f 02          	.byte 0x2
 8915 2c10 A0 01       	.2byte 0x1a0
MPLAB XC16 ASSEMBLY Listing:   			page 182


 8916 2c12 18 2C 00 00 	.4byte 0x2c18
 8917 2c16 01          	.byte 0x1
 8918 2c17 01          	.byte 0x1
 8919 2c18 16          	.uleb128 0x16
 8920 2c19 92 05 00 00 	.4byte 0x592
 8921 2c1d 15          	.uleb128 0x15
 8922 2c1e 54 4D 52 36 	.asciz "TMR6"
 8922      00 
 8923 2c23 02          	.byte 0x2
 8924 2c24 A3 01       	.2byte 0x1a3
 8925 2c26 37 2B 00 00 	.4byte 0x2b37
 8926 2c2a 01          	.byte 0x1
 8927 2c2b 01          	.byte 0x1
 8928 2c2c 15          	.uleb128 0x15
 8929 2c2d 54 4D 52 37 	.asciz "TMR7"
 8929      00 
 8930 2c32 02          	.byte 0x2
 8931 2c33 A7 01       	.2byte 0x1a7
 8932 2c35 37 2B 00 00 	.4byte 0x2b37
 8933 2c39 01          	.byte 0x1
 8934 2c3a 01          	.byte 0x1
 8935 2c3b 15          	.uleb128 0x15
 8936 2c3c 50 52 36 00 	.asciz "PR6"
 8937 2c40 02          	.byte 0x2
 8938 2c41 A9 01       	.2byte 0x1a9
 8939 2c43 37 2B 00 00 	.4byte 0x2b37
 8940 2c47 01          	.byte 0x1
 8941 2c48 01          	.byte 0x1
 8942 2c49 15          	.uleb128 0x15
 8943 2c4a 50 52 37 00 	.asciz "PR7"
 8944 2c4e 02          	.byte 0x2
 8945 2c4f AB 01       	.2byte 0x1ab
 8946 2c51 37 2B 00 00 	.4byte 0x2b37
 8947 2c55 01          	.byte 0x1
 8948 2c56 01          	.byte 0x1
 8949 2c57 17          	.uleb128 0x17
 8950 2c58 00 00 00 00 	.4byte .LASF13
 8951 2c5c 02          	.byte 0x2
 8952 2c5d C3 01       	.2byte 0x1c3
 8953 2c5f 65 2C 00 00 	.4byte 0x2c65
 8954 2c63 01          	.byte 0x1
 8955 2c64 01          	.byte 0x1
 8956 2c65 16          	.uleb128 0x16
 8957 2c66 7B 06 00 00 	.4byte 0x67b
 8958 2c6a 17          	.uleb128 0x17
 8959 2c6b 00 00 00 00 	.4byte .LASF14
 8960 2c6f 02          	.byte 0x2
 8961 2c70 DB 01       	.2byte 0x1db
 8962 2c72 78 2C 00 00 	.4byte 0x2c78
 8963 2c76 01          	.byte 0x1
 8964 2c77 01          	.byte 0x1
 8965 2c78 16          	.uleb128 0x16
 8966 2c79 52 07 00 00 	.4byte 0x752
 8967 2c7d 15          	.uleb128 0x15
 8968 2c7e 54 4D 52 38 	.asciz "TMR8"
 8968      00 
 8969 2c83 02          	.byte 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 183


 8970 2c84 DE 01       	.2byte 0x1de
 8971 2c86 37 2B 00 00 	.4byte 0x2b37
 8972 2c8a 01          	.byte 0x1
 8973 2c8b 01          	.byte 0x1
 8974 2c8c 15          	.uleb128 0x15
 8975 2c8d 54 4D 52 39 	.asciz "TMR9"
 8975      00 
 8976 2c92 02          	.byte 0x2
 8977 2c93 E2 01       	.2byte 0x1e2
 8978 2c95 37 2B 00 00 	.4byte 0x2b37
 8979 2c99 01          	.byte 0x1
 8980 2c9a 01          	.byte 0x1
 8981 2c9b 15          	.uleb128 0x15
 8982 2c9c 50 52 38 00 	.asciz "PR8"
 8983 2ca0 02          	.byte 0x2
 8984 2ca1 E4 01       	.2byte 0x1e4
 8985 2ca3 37 2B 00 00 	.4byte 0x2b37
 8986 2ca7 01          	.byte 0x1
 8987 2ca8 01          	.byte 0x1
 8988 2ca9 15          	.uleb128 0x15
 8989 2caa 50 52 39 00 	.asciz "PR9"
 8990 2cae 02          	.byte 0x2
 8991 2caf E6 01       	.2byte 0x1e6
 8992 2cb1 37 2B 00 00 	.4byte 0x2b37
 8993 2cb5 01          	.byte 0x1
 8994 2cb6 01          	.byte 0x1
 8995 2cb7 17          	.uleb128 0x17
 8996 2cb8 00 00 00 00 	.4byte .LASF15
 8997 2cbc 02          	.byte 0x2
 8998 2cbd FE 01       	.2byte 0x1fe
 8999 2cbf C5 2C 00 00 	.4byte 0x2cc5
 9000 2cc3 01          	.byte 0x1
 9001 2cc4 01          	.byte 0x1
 9002 2cc5 16          	.uleb128 0x16
 9003 2cc6 3B 08 00 00 	.4byte 0x83b
 9004 2cca 17          	.uleb128 0x17
 9005 2ccb 00 00 00 00 	.4byte .LASF16
 9006 2ccf 02          	.byte 0x2
 9007 2cd0 16 02       	.2byte 0x216
 9008 2cd2 D8 2C 00 00 	.4byte 0x2cd8
 9009 2cd6 01          	.byte 0x1
 9010 2cd7 01          	.byte 0x1
 9011 2cd8 16          	.uleb128 0x16
 9012 2cd9 12 09 00 00 	.4byte 0x912
 9013 2cdd 17          	.uleb128 0x17
 9014 2cde 00 00 00 00 	.4byte .LASF17
 9015 2ce2 02          	.byte 0x2
 9016 2ce3 1A 25       	.2byte 0x251a
 9017 2ce5 EB 2C 00 00 	.4byte 0x2ceb
 9018 2ce9 01          	.byte 0x1
 9019 2cea 01          	.byte 0x1
 9020 2ceb 16          	.uleb128 0x16
 9021 2cec 7F 0A 00 00 	.4byte 0xa7f
 9022 2cf0 17          	.uleb128 0x17
 9023 2cf1 00 00 00 00 	.4byte .LASF18
 9024 2cf5 02          	.byte 0x2
 9025 2cf6 30 25       	.2byte 0x2530
MPLAB XC16 ASSEMBLY Listing:   			page 184


 9026 2cf8 FE 2C 00 00 	.4byte 0x2cfe
 9027 2cfc 01          	.byte 0x1
 9028 2cfd 01          	.byte 0x1
 9029 2cfe 16          	.uleb128 0x16
 9030 2cff EB 0B 00 00 	.4byte 0xbeb
 9031 2d03 17          	.uleb128 0x17
 9032 2d04 00 00 00 00 	.4byte .LASF19
 9033 2d08 02          	.byte 0x2
 9034 2d09 46 25       	.2byte 0x2546
 9035 2d0b 11 2D 00 00 	.4byte 0x2d11
 9036 2d0f 01          	.byte 0x1
 9037 2d10 01          	.byte 0x1
 9038 2d11 16          	.uleb128 0x16
 9039 2d12 56 0D 00 00 	.4byte 0xd56
 9040 2d16 17          	.uleb128 0x17
 9041 2d17 00 00 00 00 	.4byte .LASF20
 9042 2d1b 02          	.byte 0x2
 9043 2d1c 5B 25       	.2byte 0x255b
 9044 2d1e 24 2D 00 00 	.4byte 0x2d24
 9045 2d22 01          	.byte 0x1
 9046 2d23 01          	.byte 0x1
 9047 2d24 16          	.uleb128 0x16
 9048 2d25 B0 0E 00 00 	.4byte 0xeb0
 9049 2d29 17          	.uleb128 0x17
 9050 2d2a 00 00 00 00 	.4byte .LASF21
 9051 2d2e 02          	.byte 0x2
 9052 2d2f C6 25       	.2byte 0x25c6
 9053 2d31 37 2D 00 00 	.4byte 0x2d37
 9054 2d35 01          	.byte 0x1
 9055 2d36 01          	.byte 0x1
 9056 2d37 16          	.uleb128 0x16
 9057 2d38 1C 10 00 00 	.4byte 0x101c
 9058 2d3c 17          	.uleb128 0x17
 9059 2d3d 00 00 00 00 	.4byte .LASF22
 9060 2d41 02          	.byte 0x2
 9061 2d42 DC 25       	.2byte 0x25dc
 9062 2d44 4A 2D 00 00 	.4byte 0x2d4a
 9063 2d48 01          	.byte 0x1
 9064 2d49 01          	.byte 0x1
 9065 2d4a 16          	.uleb128 0x16
 9066 2d4b 88 11 00 00 	.4byte 0x1188
 9067 2d4f 17          	.uleb128 0x17
 9068 2d50 00 00 00 00 	.4byte .LASF23
 9069 2d54 02          	.byte 0x2
 9070 2d55 F2 25       	.2byte 0x25f2
 9071 2d57 5D 2D 00 00 	.4byte 0x2d5d
 9072 2d5b 01          	.byte 0x1
 9073 2d5c 01          	.byte 0x1
 9074 2d5d 16          	.uleb128 0x16
 9075 2d5e F3 12 00 00 	.4byte 0x12f3
 9076 2d62 17          	.uleb128 0x17
 9077 2d63 00 00 00 00 	.4byte .LASF24
 9078 2d67 02          	.byte 0x2
 9079 2d68 07 26       	.2byte 0x2607
 9080 2d6a 70 2D 00 00 	.4byte 0x2d70
 9081 2d6e 01          	.byte 0x1
 9082 2d6f 01          	.byte 0x1
MPLAB XC16 ASSEMBLY Listing:   			page 185


 9083 2d70 16          	.uleb128 0x16
 9084 2d71 4D 14 00 00 	.4byte 0x144d
 9085 2d75 17          	.uleb128 0x17
 9086 2d76 00 00 00 00 	.4byte .LASF25
 9087 2d7a 02          	.byte 0x2
 9088 2d7b 7E 26       	.2byte 0x267e
 9089 2d7d 83 2D 00 00 	.4byte 0x2d83
 9090 2d81 01          	.byte 0x1
 9091 2d82 01          	.byte 0x1
 9092 2d83 16          	.uleb128 0x16
 9093 2d84 F0 15 00 00 	.4byte 0x15f0
 9094 2d88 17          	.uleb128 0x17
 9095 2d89 00 00 00 00 	.4byte .LASF26
 9096 2d8d 02          	.byte 0x2
 9097 2d8e A0 26       	.2byte 0x26a0
 9098 2d90 96 2D 00 00 	.4byte 0x2d96
 9099 2d94 01          	.byte 0x1
 9100 2d95 01          	.byte 0x1
 9101 2d96 16          	.uleb128 0x16
 9102 2d97 93 17 00 00 	.4byte 0x1793
 9103 2d9b 17          	.uleb128 0x17
 9104 2d9c 00 00 00 00 	.4byte .LASF27
 9105 2da0 02          	.byte 0x2
 9106 2da1 C2 26       	.2byte 0x26c2
 9107 2da3 A9 2D 00 00 	.4byte 0x2da9
 9108 2da7 01          	.byte 0x1
 9109 2da8 01          	.byte 0x1
 9110 2da9 16          	.uleb128 0x16
 9111 2daa 42 19 00 00 	.4byte 0x1942
 9112 2dae 17          	.uleb128 0x17
 9113 2daf 00 00 00 00 	.4byte .LASF28
 9114 2db3 02          	.byte 0x2
 9115 2db4 4A 27       	.2byte 0x274a
 9116 2db6 BC 2D 00 00 	.4byte 0x2dbc
 9117 2dba 01          	.byte 0x1
 9118 2dbb 01          	.byte 0x1
 9119 2dbc 16          	.uleb128 0x16
 9120 2dbd E5 1A 00 00 	.4byte 0x1ae5
 9121 2dc1 17          	.uleb128 0x17
 9122 2dc2 00 00 00 00 	.4byte .LASF29
 9123 2dc6 02          	.byte 0x2
 9124 2dc7 6C 27       	.2byte 0x276c
 9125 2dc9 CF 2D 00 00 	.4byte 0x2dcf
 9126 2dcd 01          	.byte 0x1
 9127 2dce 01          	.byte 0x1
 9128 2dcf 16          	.uleb128 0x16
 9129 2dd0 90 1C 00 00 	.4byte 0x1c90
 9130 2dd4 17          	.uleb128 0x17
 9131 2dd5 00 00 00 00 	.4byte .LASF30
 9132 2dd9 02          	.byte 0x2
 9133 2dda F4 27       	.2byte 0x27f4
 9134 2ddc E2 2D 00 00 	.4byte 0x2de2
 9135 2de0 01          	.byte 0x1
 9136 2de1 01          	.byte 0x1
 9137 2de2 16          	.uleb128 0x16
 9138 2de3 34 1E 00 00 	.4byte 0x1e34
 9139 2de7 17          	.uleb128 0x17
MPLAB XC16 ASSEMBLY Listing:   			page 186


 9140 2de8 00 00 00 00 	.4byte .LASF31
 9141 2dec 02          	.byte 0x2
 9142 2ded 16 28       	.2byte 0x2816
 9143 2def F5 2D 00 00 	.4byte 0x2df5
 9144 2df3 01          	.byte 0x1
 9145 2df4 01          	.byte 0x1
 9146 2df5 16          	.uleb128 0x16
 9147 2df6 E1 1F 00 00 	.4byte 0x1fe1
 9148 2dfa 17          	.uleb128 0x17
 9149 2dfb 00 00 00 00 	.4byte .LASF32
 9150 2dff 02          	.byte 0x2
 9151 2e00 38 28       	.2byte 0x2838
 9152 2e02 08 2E 00 00 	.4byte 0x2e08
 9153 2e06 01          	.byte 0x1
 9154 2e07 01          	.byte 0x1
 9155 2e08 16          	.uleb128 0x16
 9156 2e09 8E 21 00 00 	.4byte 0x218e
 9157 2e0d 18          	.uleb128 0x18
 9158 2e0e 01          	.byte 0x1
 9159 2e0f 19          	.uleb128 0x19
 9160 2e10 00 00 00 00 	.4byte .LASF33
 9161 2e14 01          	.byte 0x1
 9162 2e15 22          	.byte 0x22
 9163 2e16 1C 2E 00 00 	.4byte 0x2e1c
 9164 2e1a 01          	.byte 0x1
 9165 2e1b 01          	.byte 0x1
 9166 2e1c 11          	.uleb128 0x11
 9167 2e1d 02          	.byte 0x2
 9168 2e1e 0D 2E 00 00 	.4byte 0x2e0d
 9169 2e22 19          	.uleb128 0x19
 9170 2e23 00 00 00 00 	.4byte .LASF34
 9171 2e27 01          	.byte 0x1
 9172 2e28 23          	.byte 0x23
 9173 2e29 1C 2E 00 00 	.4byte 0x2e1c
 9174 2e2d 01          	.byte 0x1
 9175 2e2e 01          	.byte 0x1
 9176 2e2f 19          	.uleb128 0x19
 9177 2e30 00 00 00 00 	.4byte .LASF35
 9178 2e34 01          	.byte 0x1
 9179 2e35 24          	.byte 0x24
 9180 2e36 1C 2E 00 00 	.4byte 0x2e1c
 9181 2e3a 01          	.byte 0x1
 9182 2e3b 01          	.byte 0x1
 9183 2e3c 19          	.uleb128 0x19
 9184 2e3d 00 00 00 00 	.4byte .LASF36
 9185 2e41 01          	.byte 0x1
 9186 2e42 25          	.byte 0x25
 9187 2e43 1C 2E 00 00 	.4byte 0x2e1c
 9188 2e47 01          	.byte 0x1
 9189 2e48 01          	.byte 0x1
 9190 2e49 19          	.uleb128 0x19
 9191 2e4a 00 00 00 00 	.4byte .LASF37
 9192 2e4e 01          	.byte 0x1
 9193 2e4f 26          	.byte 0x26
 9194 2e50 1C 2E 00 00 	.4byte 0x2e1c
 9195 2e54 01          	.byte 0x1
 9196 2e55 01          	.byte 0x1
MPLAB XC16 ASSEMBLY Listing:   			page 187


 9197 2e56 19          	.uleb128 0x19
 9198 2e57 00 00 00 00 	.4byte .LASF38
 9199 2e5b 01          	.byte 0x1
 9200 2e5c 27          	.byte 0x27
 9201 2e5d 1C 2E 00 00 	.4byte 0x2e1c
 9202 2e61 01          	.byte 0x1
 9203 2e62 01          	.byte 0x1
 9204 2e63 19          	.uleb128 0x19
 9205 2e64 00 00 00 00 	.4byte .LASF39
 9206 2e68 01          	.byte 0x1
 9207 2e69 28          	.byte 0x28
 9208 2e6a 1C 2E 00 00 	.4byte 0x2e1c
 9209 2e6e 01          	.byte 0x1
 9210 2e6f 01          	.byte 0x1
 9211 2e70 19          	.uleb128 0x19
 9212 2e71 00 00 00 00 	.4byte .LASF40
 9213 2e75 01          	.byte 0x1
 9214 2e76 29          	.byte 0x29
 9215 2e77 1C 2E 00 00 	.4byte 0x2e1c
 9216 2e7b 01          	.byte 0x1
 9217 2e7c 01          	.byte 0x1
 9218 2e7d 19          	.uleb128 0x19
 9219 2e7e 00 00 00 00 	.4byte .LASF41
 9220 2e82 01          	.byte 0x1
 9221 2e83 2A          	.byte 0x2a
 9222 2e84 1C 2E 00 00 	.4byte 0x2e1c
 9223 2e88 01          	.byte 0x1
 9224 2e89 01          	.byte 0x1
 9225 2e8a 15          	.uleb128 0x15
 9226 2e8b 54 4D 52 31 	.asciz "TMR1"
 9226      00 
 9227 2e90 02          	.byte 0x2
 9228 2e91 10 01       	.2byte 0x110
 9229 2e93 37 2B 00 00 	.4byte 0x2b37
 9230 2e97 01          	.byte 0x1
 9231 2e98 01          	.byte 0x1
 9232 2e99 15          	.uleb128 0x15
 9233 2e9a 50 52 31 00 	.asciz "PR1"
 9234 2e9e 02          	.byte 0x2
 9235 2e9f 12 01       	.2byte 0x112
 9236 2ea1 37 2B 00 00 	.4byte 0x2b37
 9237 2ea5 01          	.byte 0x1
 9238 2ea6 01          	.byte 0x1
 9239 2ea7 17          	.uleb128 0x17
 9240 2ea8 00 00 00 00 	.4byte .LASF8
 9241 2eac 02          	.byte 0x2
 9242 2ead 2A 01       	.2byte 0x12a
 9243 2eaf 58 2B 00 00 	.4byte 0x2b58
 9244 2eb3 01          	.byte 0x1
 9245 2eb4 01          	.byte 0x1
 9246 2eb5 15          	.uleb128 0x15
 9247 2eb6 54 4D 52 32 	.asciz "TMR2"
 9247      00 
 9248 2ebb 02          	.byte 0x2
 9249 2ebc 2D 01       	.2byte 0x12d
 9250 2ebe 37 2B 00 00 	.4byte 0x2b37
 9251 2ec2 01          	.byte 0x1
MPLAB XC16 ASSEMBLY Listing:   			page 188


 9252 2ec3 01          	.byte 0x1
 9253 2ec4 15          	.uleb128 0x15
 9254 2ec5 54 4D 52 33 	.asciz "TMR3"
 9254      00 
 9255 2eca 02          	.byte 0x2
 9256 2ecb 31 01       	.2byte 0x131
 9257 2ecd 37 2B 00 00 	.4byte 0x2b37
 9258 2ed1 01          	.byte 0x1
 9259 2ed2 01          	.byte 0x1
 9260 2ed3 15          	.uleb128 0x15
 9261 2ed4 50 52 32 00 	.asciz "PR2"
 9262 2ed8 02          	.byte 0x2
 9263 2ed9 33 01       	.2byte 0x133
 9264 2edb 37 2B 00 00 	.4byte 0x2b37
 9265 2edf 01          	.byte 0x1
 9266 2ee0 01          	.byte 0x1
 9267 2ee1 15          	.uleb128 0x15
 9268 2ee2 50 52 33 00 	.asciz "PR3"
 9269 2ee6 02          	.byte 0x2
 9270 2ee7 35 01       	.2byte 0x135
 9271 2ee9 37 2B 00 00 	.4byte 0x2b37
 9272 2eed 01          	.byte 0x1
 9273 2eee 01          	.byte 0x1
 9274 2eef 17          	.uleb128 0x17
 9275 2ef0 00 00 00 00 	.4byte .LASF9
 9276 2ef4 02          	.byte 0x2
 9277 2ef5 4D 01       	.2byte 0x14d
 9278 2ef7 A5 2B 00 00 	.4byte 0x2ba5
 9279 2efb 01          	.byte 0x1
 9280 2efc 01          	.byte 0x1
 9281 2efd 17          	.uleb128 0x17
 9282 2efe 00 00 00 00 	.4byte .LASF10
 9283 2f02 02          	.byte 0x2
 9284 2f03 65 01       	.2byte 0x165
 9285 2f05 B8 2B 00 00 	.4byte 0x2bb8
 9286 2f09 01          	.byte 0x1
 9287 2f0a 01          	.byte 0x1
 9288 2f0b 15          	.uleb128 0x15
 9289 2f0c 54 4D 52 34 	.asciz "TMR4"
 9289      00 
 9290 2f11 02          	.byte 0x2
 9291 2f12 68 01       	.2byte 0x168
 9292 2f14 37 2B 00 00 	.4byte 0x2b37
 9293 2f18 01          	.byte 0x1
 9294 2f19 01          	.byte 0x1
 9295 2f1a 15          	.uleb128 0x15
 9296 2f1b 54 4D 52 35 	.asciz "TMR5"
 9296      00 
 9297 2f20 02          	.byte 0x2
 9298 2f21 6C 01       	.2byte 0x16c
 9299 2f23 37 2B 00 00 	.4byte 0x2b37
 9300 2f27 01          	.byte 0x1
 9301 2f28 01          	.byte 0x1
 9302 2f29 15          	.uleb128 0x15
 9303 2f2a 50 52 34 00 	.asciz "PR4"
 9304 2f2e 02          	.byte 0x2
 9305 2f2f 6E 01       	.2byte 0x16e
MPLAB XC16 ASSEMBLY Listing:   			page 189


 9306 2f31 37 2B 00 00 	.4byte 0x2b37
 9307 2f35 01          	.byte 0x1
 9308 2f36 01          	.byte 0x1
 9309 2f37 15          	.uleb128 0x15
 9310 2f38 50 52 35 00 	.asciz "PR5"
 9311 2f3c 02          	.byte 0x2
 9312 2f3d 70 01       	.2byte 0x170
 9313 2f3f 37 2B 00 00 	.4byte 0x2b37
 9314 2f43 01          	.byte 0x1
 9315 2f44 01          	.byte 0x1
 9316 2f45 17          	.uleb128 0x17
 9317 2f46 00 00 00 00 	.4byte .LASF11
 9318 2f4a 02          	.byte 0x2
 9319 2f4b 88 01       	.2byte 0x188
 9320 2f4d 05 2C 00 00 	.4byte 0x2c05
 9321 2f51 01          	.byte 0x1
 9322 2f52 01          	.byte 0x1
 9323 2f53 17          	.uleb128 0x17
 9324 2f54 00 00 00 00 	.4byte .LASF12
 9325 2f58 02          	.byte 0x2
 9326 2f59 A0 01       	.2byte 0x1a0
 9327 2f5b 18 2C 00 00 	.4byte 0x2c18
 9328 2f5f 01          	.byte 0x1
 9329 2f60 01          	.byte 0x1
 9330 2f61 15          	.uleb128 0x15
 9331 2f62 54 4D 52 36 	.asciz "TMR6"
 9331      00 
 9332 2f67 02          	.byte 0x2
 9333 2f68 A3 01       	.2byte 0x1a3
 9334 2f6a 37 2B 00 00 	.4byte 0x2b37
 9335 2f6e 01          	.byte 0x1
 9336 2f6f 01          	.byte 0x1
 9337 2f70 15          	.uleb128 0x15
 9338 2f71 54 4D 52 37 	.asciz "TMR7"
 9338      00 
 9339 2f76 02          	.byte 0x2
 9340 2f77 A7 01       	.2byte 0x1a7
 9341 2f79 37 2B 00 00 	.4byte 0x2b37
 9342 2f7d 01          	.byte 0x1
 9343 2f7e 01          	.byte 0x1
 9344 2f7f 15          	.uleb128 0x15
 9345 2f80 50 52 36 00 	.asciz "PR6"
 9346 2f84 02          	.byte 0x2
 9347 2f85 A9 01       	.2byte 0x1a9
 9348 2f87 37 2B 00 00 	.4byte 0x2b37
 9349 2f8b 01          	.byte 0x1
 9350 2f8c 01          	.byte 0x1
 9351 2f8d 15          	.uleb128 0x15
 9352 2f8e 50 52 37 00 	.asciz "PR7"
 9353 2f92 02          	.byte 0x2
 9354 2f93 AB 01       	.2byte 0x1ab
 9355 2f95 37 2B 00 00 	.4byte 0x2b37
 9356 2f99 01          	.byte 0x1
 9357 2f9a 01          	.byte 0x1
 9358 2f9b 17          	.uleb128 0x17
 9359 2f9c 00 00 00 00 	.4byte .LASF13
 9360 2fa0 02          	.byte 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 190


 9361 2fa1 C3 01       	.2byte 0x1c3
 9362 2fa3 65 2C 00 00 	.4byte 0x2c65
 9363 2fa7 01          	.byte 0x1
 9364 2fa8 01          	.byte 0x1
 9365 2fa9 17          	.uleb128 0x17
 9366 2faa 00 00 00 00 	.4byte .LASF14
 9367 2fae 02          	.byte 0x2
 9368 2faf DB 01       	.2byte 0x1db
 9369 2fb1 78 2C 00 00 	.4byte 0x2c78
 9370 2fb5 01          	.byte 0x1
 9371 2fb6 01          	.byte 0x1
 9372 2fb7 15          	.uleb128 0x15
 9373 2fb8 54 4D 52 38 	.asciz "TMR8"
 9373      00 
 9374 2fbd 02          	.byte 0x2
 9375 2fbe DE 01       	.2byte 0x1de
 9376 2fc0 37 2B 00 00 	.4byte 0x2b37
 9377 2fc4 01          	.byte 0x1
 9378 2fc5 01          	.byte 0x1
 9379 2fc6 15          	.uleb128 0x15
 9380 2fc7 54 4D 52 39 	.asciz "TMR9"
 9380      00 
 9381 2fcc 02          	.byte 0x2
 9382 2fcd E2 01       	.2byte 0x1e2
 9383 2fcf 37 2B 00 00 	.4byte 0x2b37
 9384 2fd3 01          	.byte 0x1
 9385 2fd4 01          	.byte 0x1
 9386 2fd5 15          	.uleb128 0x15
 9387 2fd6 50 52 38 00 	.asciz "PR8"
 9388 2fda 02          	.byte 0x2
 9389 2fdb E4 01       	.2byte 0x1e4
 9390 2fdd 37 2B 00 00 	.4byte 0x2b37
 9391 2fe1 01          	.byte 0x1
 9392 2fe2 01          	.byte 0x1
 9393 2fe3 15          	.uleb128 0x15
 9394 2fe4 50 52 39 00 	.asciz "PR9"
 9395 2fe8 02          	.byte 0x2
 9396 2fe9 E6 01       	.2byte 0x1e6
 9397 2feb 37 2B 00 00 	.4byte 0x2b37
 9398 2fef 01          	.byte 0x1
 9399 2ff0 01          	.byte 0x1
 9400 2ff1 17          	.uleb128 0x17
 9401 2ff2 00 00 00 00 	.4byte .LASF15
 9402 2ff6 02          	.byte 0x2
 9403 2ff7 FE 01       	.2byte 0x1fe
 9404 2ff9 C5 2C 00 00 	.4byte 0x2cc5
 9405 2ffd 01          	.byte 0x1
 9406 2ffe 01          	.byte 0x1
 9407 2fff 17          	.uleb128 0x17
 9408 3000 00 00 00 00 	.4byte .LASF16
 9409 3004 02          	.byte 0x2
 9410 3005 16 02       	.2byte 0x216
 9411 3007 D8 2C 00 00 	.4byte 0x2cd8
 9412 300b 01          	.byte 0x1
 9413 300c 01          	.byte 0x1
 9414 300d 17          	.uleb128 0x17
 9415 300e 00 00 00 00 	.4byte .LASF17
MPLAB XC16 ASSEMBLY Listing:   			page 191


 9416 3012 02          	.byte 0x2
 9417 3013 1A 25       	.2byte 0x251a
 9418 3015 EB 2C 00 00 	.4byte 0x2ceb
 9419 3019 01          	.byte 0x1
 9420 301a 01          	.byte 0x1
 9421 301b 17          	.uleb128 0x17
 9422 301c 00 00 00 00 	.4byte .LASF18
 9423 3020 02          	.byte 0x2
 9424 3021 30 25       	.2byte 0x2530
 9425 3023 FE 2C 00 00 	.4byte 0x2cfe
 9426 3027 01          	.byte 0x1
 9427 3028 01          	.byte 0x1
 9428 3029 17          	.uleb128 0x17
 9429 302a 00 00 00 00 	.4byte .LASF19
 9430 302e 02          	.byte 0x2
 9431 302f 46 25       	.2byte 0x2546
 9432 3031 11 2D 00 00 	.4byte 0x2d11
 9433 3035 01          	.byte 0x1
 9434 3036 01          	.byte 0x1
 9435 3037 17          	.uleb128 0x17
 9436 3038 00 00 00 00 	.4byte .LASF20
 9437 303c 02          	.byte 0x2
 9438 303d 5B 25       	.2byte 0x255b
 9439 303f 24 2D 00 00 	.4byte 0x2d24
 9440 3043 01          	.byte 0x1
 9441 3044 01          	.byte 0x1
 9442 3045 17          	.uleb128 0x17
 9443 3046 00 00 00 00 	.4byte .LASF21
 9444 304a 02          	.byte 0x2
 9445 304b C6 25       	.2byte 0x25c6
 9446 304d 37 2D 00 00 	.4byte 0x2d37
 9447 3051 01          	.byte 0x1
 9448 3052 01          	.byte 0x1
 9449 3053 17          	.uleb128 0x17
 9450 3054 00 00 00 00 	.4byte .LASF22
 9451 3058 02          	.byte 0x2
 9452 3059 DC 25       	.2byte 0x25dc
 9453 305b 4A 2D 00 00 	.4byte 0x2d4a
 9454 305f 01          	.byte 0x1
 9455 3060 01          	.byte 0x1
 9456 3061 17          	.uleb128 0x17
 9457 3062 00 00 00 00 	.4byte .LASF23
 9458 3066 02          	.byte 0x2
 9459 3067 F2 25       	.2byte 0x25f2
 9460 3069 5D 2D 00 00 	.4byte 0x2d5d
 9461 306d 01          	.byte 0x1
 9462 306e 01          	.byte 0x1
 9463 306f 17          	.uleb128 0x17
 9464 3070 00 00 00 00 	.4byte .LASF24
 9465 3074 02          	.byte 0x2
 9466 3075 07 26       	.2byte 0x2607
 9467 3077 70 2D 00 00 	.4byte 0x2d70
 9468 307b 01          	.byte 0x1
 9469 307c 01          	.byte 0x1
 9470 307d 17          	.uleb128 0x17
 9471 307e 00 00 00 00 	.4byte .LASF25
 9472 3082 02          	.byte 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 192


 9473 3083 7E 26       	.2byte 0x267e
 9474 3085 83 2D 00 00 	.4byte 0x2d83
 9475 3089 01          	.byte 0x1
 9476 308a 01          	.byte 0x1
 9477 308b 17          	.uleb128 0x17
 9478 308c 00 00 00 00 	.4byte .LASF26
 9479 3090 02          	.byte 0x2
 9480 3091 A0 26       	.2byte 0x26a0
 9481 3093 96 2D 00 00 	.4byte 0x2d96
 9482 3097 01          	.byte 0x1
 9483 3098 01          	.byte 0x1
 9484 3099 17          	.uleb128 0x17
 9485 309a 00 00 00 00 	.4byte .LASF27
 9486 309e 02          	.byte 0x2
 9487 309f C2 26       	.2byte 0x26c2
 9488 30a1 A9 2D 00 00 	.4byte 0x2da9
 9489 30a5 01          	.byte 0x1
 9490 30a6 01          	.byte 0x1
 9491 30a7 17          	.uleb128 0x17
 9492 30a8 00 00 00 00 	.4byte .LASF28
 9493 30ac 02          	.byte 0x2
 9494 30ad 4A 27       	.2byte 0x274a
 9495 30af BC 2D 00 00 	.4byte 0x2dbc
 9496 30b3 01          	.byte 0x1
 9497 30b4 01          	.byte 0x1
 9498 30b5 17          	.uleb128 0x17
 9499 30b6 00 00 00 00 	.4byte .LASF29
 9500 30ba 02          	.byte 0x2
 9501 30bb 6C 27       	.2byte 0x276c
 9502 30bd CF 2D 00 00 	.4byte 0x2dcf
 9503 30c1 01          	.byte 0x1
 9504 30c2 01          	.byte 0x1
 9505 30c3 17          	.uleb128 0x17
 9506 30c4 00 00 00 00 	.4byte .LASF30
 9507 30c8 02          	.byte 0x2
 9508 30c9 F4 27       	.2byte 0x27f4
 9509 30cb E2 2D 00 00 	.4byte 0x2de2
 9510 30cf 01          	.byte 0x1
 9511 30d0 01          	.byte 0x1
 9512 30d1 17          	.uleb128 0x17
 9513 30d2 00 00 00 00 	.4byte .LASF31
 9514 30d6 02          	.byte 0x2
 9515 30d7 16 28       	.2byte 0x2816
 9516 30d9 F5 2D 00 00 	.4byte 0x2df5
 9517 30dd 01          	.byte 0x1
 9518 30de 01          	.byte 0x1
 9519 30df 17          	.uleb128 0x17
 9520 30e0 00 00 00 00 	.4byte .LASF32
 9521 30e4 02          	.byte 0x2
 9522 30e5 38 28       	.2byte 0x2838
 9523 30e7 08 2E 00 00 	.4byte 0x2e08
 9524 30eb 01          	.byte 0x1
 9525 30ec 01          	.byte 0x1
 9526 30ed 1A          	.uleb128 0x1a
 9527 30ee 00 00 00 00 	.4byte .LASF33
 9528 30f2 01          	.byte 0x1
 9529 30f3 22          	.byte 0x22
MPLAB XC16 ASSEMBLY Listing:   			page 193


 9530 30f4 1C 2E 00 00 	.4byte 0x2e1c
 9531 30f8 01          	.byte 0x1
 9532 30f9 05          	.byte 0x5
 9533 30fa 03          	.byte 0x3
 9534 30fb 00 00 00 00 	.4byte _fun_ptr_timer1
 9535 30ff 1A          	.uleb128 0x1a
 9536 3100 00 00 00 00 	.4byte .LASF34
 9537 3104 01          	.byte 0x1
 9538 3105 23          	.byte 0x23
 9539 3106 1C 2E 00 00 	.4byte 0x2e1c
 9540 310a 01          	.byte 0x1
 9541 310b 05          	.byte 0x5
 9542 310c 03          	.byte 0x3
 9543 310d 00 00 00 00 	.4byte _fun_ptr_timer2
 9544 3111 1A          	.uleb128 0x1a
 9545 3112 00 00 00 00 	.4byte .LASF35
 9546 3116 01          	.byte 0x1
 9547 3117 24          	.byte 0x24
 9548 3118 1C 2E 00 00 	.4byte 0x2e1c
 9549 311c 01          	.byte 0x1
 9550 311d 05          	.byte 0x5
 9551 311e 03          	.byte 0x3
 9552 311f 00 00 00 00 	.4byte _fun_ptr_timer3
 9553 3123 1A          	.uleb128 0x1a
 9554 3124 00 00 00 00 	.4byte .LASF36
 9555 3128 01          	.byte 0x1
 9556 3129 25          	.byte 0x25
 9557 312a 1C 2E 00 00 	.4byte 0x2e1c
 9558 312e 01          	.byte 0x1
 9559 312f 05          	.byte 0x5
 9560 3130 03          	.byte 0x3
 9561 3131 00 00 00 00 	.4byte _fun_ptr_timer4
 9562 3135 1A          	.uleb128 0x1a
 9563 3136 00 00 00 00 	.4byte .LASF37
 9564 313a 01          	.byte 0x1
 9565 313b 26          	.byte 0x26
 9566 313c 1C 2E 00 00 	.4byte 0x2e1c
 9567 3140 01          	.byte 0x1
 9568 3141 05          	.byte 0x5
 9569 3142 03          	.byte 0x3
 9570 3143 00 00 00 00 	.4byte _fun_ptr_timer5
 9571 3147 1A          	.uleb128 0x1a
 9572 3148 00 00 00 00 	.4byte .LASF38
 9573 314c 01          	.byte 0x1
 9574 314d 27          	.byte 0x27
 9575 314e 1C 2E 00 00 	.4byte 0x2e1c
 9576 3152 01          	.byte 0x1
 9577 3153 05          	.byte 0x5
 9578 3154 03          	.byte 0x3
 9579 3155 00 00 00 00 	.4byte _fun_ptr_timer6
 9580 3159 1A          	.uleb128 0x1a
 9581 315a 00 00 00 00 	.4byte .LASF39
 9582 315e 01          	.byte 0x1
 9583 315f 28          	.byte 0x28
 9584 3160 1C 2E 00 00 	.4byte 0x2e1c
 9585 3164 01          	.byte 0x1
 9586 3165 05          	.byte 0x5
MPLAB XC16 ASSEMBLY Listing:   			page 194


 9587 3166 03          	.byte 0x3
 9588 3167 00 00 00 00 	.4byte _fun_ptr_timer7
 9589 316b 1A          	.uleb128 0x1a
 9590 316c 00 00 00 00 	.4byte .LASF40
 9591 3170 01          	.byte 0x1
 9592 3171 29          	.byte 0x29
 9593 3172 1C 2E 00 00 	.4byte 0x2e1c
 9594 3176 01          	.byte 0x1
 9595 3177 05          	.byte 0x5
 9596 3178 03          	.byte 0x3
 9597 3179 00 00 00 00 	.4byte _fun_ptr_timer8
 9598 317d 1A          	.uleb128 0x1a
 9599 317e 00 00 00 00 	.4byte .LASF41
 9600 3182 01          	.byte 0x1
 9601 3183 2A          	.byte 0x2a
 9602 3184 1C 2E 00 00 	.4byte 0x2e1c
 9603 3188 01          	.byte 0x1
 9604 3189 05          	.byte 0x5
 9605 318a 03          	.byte 0x3
 9606 318b 00 00 00 00 	.4byte _fun_ptr_timer9
 9607 318f 00          	.byte 0x0
 9608                 	.section .debug_abbrev,info
 9609 0000 01          	.uleb128 0x1
 9610 0001 11          	.uleb128 0x11
 9611 0002 01          	.byte 0x1
 9612 0003 25          	.uleb128 0x25
 9613 0004 08          	.uleb128 0x8
 9614 0005 13          	.uleb128 0x13
 9615 0006 0B          	.uleb128 0xb
 9616 0007 03          	.uleb128 0x3
 9617 0008 08          	.uleb128 0x8
 9618 0009 1B          	.uleb128 0x1b
 9619 000a 08          	.uleb128 0x8
 9620 000b 11          	.uleb128 0x11
 9621 000c 01          	.uleb128 0x1
 9622 000d 12          	.uleb128 0x12
 9623 000e 01          	.uleb128 0x1
 9624 000f 10          	.uleb128 0x10
 9625 0010 06          	.uleb128 0x6
 9626 0011 00          	.byte 0x0
 9627 0012 00          	.byte 0x0
 9628 0013 02          	.uleb128 0x2
 9629 0014 24          	.uleb128 0x24
 9630 0015 00          	.byte 0x0
 9631 0016 0B          	.uleb128 0xb
 9632 0017 0B          	.uleb128 0xb
 9633 0018 3E          	.uleb128 0x3e
 9634 0019 0B          	.uleb128 0xb
 9635 001a 03          	.uleb128 0x3
 9636 001b 08          	.uleb128 0x8
 9637 001c 00          	.byte 0x0
 9638 001d 00          	.byte 0x0
 9639 001e 03          	.uleb128 0x3
 9640 001f 16          	.uleb128 0x16
 9641 0020 00          	.byte 0x0
 9642 0021 03          	.uleb128 0x3
 9643 0022 08          	.uleb128 0x8
MPLAB XC16 ASSEMBLY Listing:   			page 195


 9644 0023 3A          	.uleb128 0x3a
 9645 0024 0B          	.uleb128 0xb
 9646 0025 3B          	.uleb128 0x3b
 9647 0026 0B          	.uleb128 0xb
 9648 0027 49          	.uleb128 0x49
 9649 0028 13          	.uleb128 0x13
 9650 0029 00          	.byte 0x0
 9651 002a 00          	.byte 0x0
 9652 002b 04          	.uleb128 0x4
 9653 002c 13          	.uleb128 0x13
 9654 002d 01          	.byte 0x1
 9655 002e 0B          	.uleb128 0xb
 9656 002f 0B          	.uleb128 0xb
 9657 0030 3A          	.uleb128 0x3a
 9658 0031 0B          	.uleb128 0xb
 9659 0032 3B          	.uleb128 0x3b
 9660 0033 05          	.uleb128 0x5
 9661 0034 01          	.uleb128 0x1
 9662 0035 13          	.uleb128 0x13
 9663 0036 00          	.byte 0x0
 9664 0037 00          	.byte 0x0
 9665 0038 05          	.uleb128 0x5
 9666 0039 0D          	.uleb128 0xd
 9667 003a 00          	.byte 0x0
 9668 003b 03          	.uleb128 0x3
 9669 003c 08          	.uleb128 0x8
 9670 003d 3A          	.uleb128 0x3a
 9671 003e 0B          	.uleb128 0xb
 9672 003f 3B          	.uleb128 0x3b
 9673 0040 05          	.uleb128 0x5
 9674 0041 49          	.uleb128 0x49
 9675 0042 13          	.uleb128 0x13
 9676 0043 0B          	.uleb128 0xb
 9677 0044 0B          	.uleb128 0xb
 9678 0045 0D          	.uleb128 0xd
 9679 0046 0B          	.uleb128 0xb
 9680 0047 0C          	.uleb128 0xc
 9681 0048 0B          	.uleb128 0xb
 9682 0049 38          	.uleb128 0x38
 9683 004a 0A          	.uleb128 0xa
 9684 004b 00          	.byte 0x0
 9685 004c 00          	.byte 0x0
 9686 004d 06          	.uleb128 0x6
 9687 004e 0D          	.uleb128 0xd
 9688 004f 00          	.byte 0x0
 9689 0050 03          	.uleb128 0x3
 9690 0051 0E          	.uleb128 0xe
 9691 0052 3A          	.uleb128 0x3a
 9692 0053 0B          	.uleb128 0xb
 9693 0054 3B          	.uleb128 0x3b
 9694 0055 05          	.uleb128 0x5
 9695 0056 49          	.uleb128 0x49
 9696 0057 13          	.uleb128 0x13
 9697 0058 0B          	.uleb128 0xb
 9698 0059 0B          	.uleb128 0xb
 9699 005a 0D          	.uleb128 0xd
 9700 005b 0B          	.uleb128 0xb
MPLAB XC16 ASSEMBLY Listing:   			page 196


 9701 005c 0C          	.uleb128 0xc
 9702 005d 0B          	.uleb128 0xb
 9703 005e 38          	.uleb128 0x38
 9704 005f 0A          	.uleb128 0xa
 9705 0060 00          	.byte 0x0
 9706 0061 00          	.byte 0x0
 9707 0062 07          	.uleb128 0x7
 9708 0063 17          	.uleb128 0x17
 9709 0064 01          	.byte 0x1
 9710 0065 0B          	.uleb128 0xb
 9711 0066 0B          	.uleb128 0xb
 9712 0067 3A          	.uleb128 0x3a
 9713 0068 0B          	.uleb128 0xb
 9714 0069 3B          	.uleb128 0x3b
 9715 006a 05          	.uleb128 0x5
 9716 006b 01          	.uleb128 0x1
 9717 006c 13          	.uleb128 0x13
 9718 006d 00          	.byte 0x0
 9719 006e 00          	.byte 0x0
 9720 006f 08          	.uleb128 0x8
 9721 0070 0D          	.uleb128 0xd
 9722 0071 00          	.byte 0x0
 9723 0072 49          	.uleb128 0x49
 9724 0073 13          	.uleb128 0x13
 9725 0074 00          	.byte 0x0
 9726 0075 00          	.byte 0x0
 9727 0076 09          	.uleb128 0x9
 9728 0077 13          	.uleb128 0x13
 9729 0078 01          	.byte 0x1
 9730 0079 03          	.uleb128 0x3
 9731 007a 08          	.uleb128 0x8
 9732 007b 0B          	.uleb128 0xb
 9733 007c 0B          	.uleb128 0xb
 9734 007d 3A          	.uleb128 0x3a
 9735 007e 0B          	.uleb128 0xb
 9736 007f 3B          	.uleb128 0x3b
 9737 0080 05          	.uleb128 0x5
 9738 0081 01          	.uleb128 0x1
 9739 0082 13          	.uleb128 0x13
 9740 0083 00          	.byte 0x0
 9741 0084 00          	.byte 0x0
 9742 0085 0A          	.uleb128 0xa
 9743 0086 0D          	.uleb128 0xd
 9744 0087 00          	.byte 0x0
 9745 0088 49          	.uleb128 0x49
 9746 0089 13          	.uleb128 0x13
 9747 008a 38          	.uleb128 0x38
 9748 008b 0A          	.uleb128 0xa
 9749 008c 00          	.byte 0x0
 9750 008d 00          	.byte 0x0
 9751 008e 0B          	.uleb128 0xb
 9752 008f 16          	.uleb128 0x16
 9753 0090 00          	.byte 0x0
 9754 0091 03          	.uleb128 0x3
 9755 0092 08          	.uleb128 0x8
 9756 0093 3A          	.uleb128 0x3a
 9757 0094 0B          	.uleb128 0xb
MPLAB XC16 ASSEMBLY Listing:   			page 197


 9758 0095 3B          	.uleb128 0x3b
 9759 0096 05          	.uleb128 0x5
 9760 0097 49          	.uleb128 0x49
 9761 0098 13          	.uleb128 0x13
 9762 0099 00          	.byte 0x0
 9763 009a 00          	.byte 0x0
 9764 009b 0C          	.uleb128 0xc
 9765 009c 0F          	.uleb128 0xf
 9766 009d 00          	.byte 0x0
 9767 009e 0B          	.uleb128 0xb
 9768 009f 0B          	.uleb128 0xb
 9769 00a0 00          	.byte 0x0
 9770 00a1 00          	.byte 0x0
 9771 00a2 0D          	.uleb128 0xd
 9772 00a3 2E          	.uleb128 0x2e
 9773 00a4 00          	.byte 0x0
 9774 00a5 3F          	.uleb128 0x3f
 9775 00a6 0C          	.uleb128 0xc
 9776 00a7 03          	.uleb128 0x3
 9777 00a8 08          	.uleb128 0x8
 9778 00a9 3A          	.uleb128 0x3a
 9779 00aa 0B          	.uleb128 0xb
 9780 00ab 3B          	.uleb128 0x3b
 9781 00ac 0B          	.uleb128 0xb
 9782 00ad 27          	.uleb128 0x27
 9783 00ae 0C          	.uleb128 0xc
 9784 00af 11          	.uleb128 0x11
 9785 00b0 01          	.uleb128 0x1
 9786 00b1 12          	.uleb128 0x12
 9787 00b2 01          	.uleb128 0x1
 9788 00b3 40          	.uleb128 0x40
 9789 00b4 0A          	.uleb128 0xa
 9790 00b5 00          	.byte 0x0
 9791 00b6 00          	.byte 0x0
 9792 00b7 0E          	.uleb128 0xe
 9793 00b8 2E          	.uleb128 0x2e
 9794 00b9 01          	.byte 0x1
 9795 00ba 3F          	.uleb128 0x3f
 9796 00bb 0C          	.uleb128 0xc
 9797 00bc 03          	.uleb128 0x3
 9798 00bd 08          	.uleb128 0x8
 9799 00be 3A          	.uleb128 0x3a
 9800 00bf 0B          	.uleb128 0xb
 9801 00c0 3B          	.uleb128 0x3b
 9802 00c1 0B          	.uleb128 0xb
 9803 00c2 27          	.uleb128 0x27
 9804 00c3 0C          	.uleb128 0xc
 9805 00c4 11          	.uleb128 0x11
 9806 00c5 01          	.uleb128 0x1
 9807 00c6 12          	.uleb128 0x12
 9808 00c7 01          	.uleb128 0x1
 9809 00c8 40          	.uleb128 0x40
 9810 00c9 0A          	.uleb128 0xa
 9811 00ca 01          	.uleb128 0x1
 9812 00cb 13          	.uleb128 0x13
 9813 00cc 00          	.byte 0x0
 9814 00cd 00          	.byte 0x0
MPLAB XC16 ASSEMBLY Listing:   			page 198


 9815 00ce 0F          	.uleb128 0xf
 9816 00cf 05          	.uleb128 0x5
 9817 00d0 00          	.byte 0x0
 9818 00d1 03          	.uleb128 0x3
 9819 00d2 0E          	.uleb128 0xe
 9820 00d3 3A          	.uleb128 0x3a
 9821 00d4 0B          	.uleb128 0xb
 9822 00d5 3B          	.uleb128 0x3b
 9823 00d6 0B          	.uleb128 0xb
 9824 00d7 49          	.uleb128 0x49
 9825 00d8 13          	.uleb128 0x13
 9826 00d9 02          	.uleb128 0x2
 9827 00da 0A          	.uleb128 0xa
 9828 00db 00          	.byte 0x0
 9829 00dc 00          	.byte 0x0
 9830 00dd 10          	.uleb128 0x10
 9831 00de 15          	.uleb128 0x15
 9832 00df 00          	.byte 0x0
 9833 00e0 27          	.uleb128 0x27
 9834 00e1 0C          	.uleb128 0xc
 9835 00e2 49          	.uleb128 0x49
 9836 00e3 13          	.uleb128 0x13
 9837 00e4 00          	.byte 0x0
 9838 00e5 00          	.byte 0x0
 9839 00e6 11          	.uleb128 0x11
 9840 00e7 0F          	.uleb128 0xf
 9841 00e8 00          	.byte 0x0
 9842 00e9 0B          	.uleb128 0xb
 9843 00ea 0B          	.uleb128 0xb
 9844 00eb 49          	.uleb128 0x49
 9845 00ec 13          	.uleb128 0x13
 9846 00ed 00          	.byte 0x0
 9847 00ee 00          	.byte 0x0
 9848 00ef 12          	.uleb128 0x12
 9849 00f0 2E          	.uleb128 0x2e
 9850 00f1 01          	.byte 0x1
 9851 00f2 3F          	.uleb128 0x3f
 9852 00f3 0C          	.uleb128 0xc
 9853 00f4 03          	.uleb128 0x3
 9854 00f5 08          	.uleb128 0x8
 9855 00f6 3A          	.uleb128 0x3a
 9856 00f7 0B          	.uleb128 0xb
 9857 00f8 3B          	.uleb128 0x3b
 9858 00f9 05          	.uleb128 0x5
 9859 00fa 27          	.uleb128 0x27
 9860 00fb 0C          	.uleb128 0xc
 9861 00fc 11          	.uleb128 0x11
 9862 00fd 01          	.uleb128 0x1
 9863 00fe 12          	.uleb128 0x12
 9864 00ff 01          	.uleb128 0x1
 9865 0100 40          	.uleb128 0x40
 9866 0101 0A          	.uleb128 0xa
 9867 0102 01          	.uleb128 0x1
 9868 0103 13          	.uleb128 0x13
 9869 0104 00          	.byte 0x0
 9870 0105 00          	.byte 0x0
 9871 0106 13          	.uleb128 0x13
MPLAB XC16 ASSEMBLY Listing:   			page 199


 9872 0107 05          	.uleb128 0x5
 9873 0108 00          	.byte 0x0
 9874 0109 03          	.uleb128 0x3
 9875 010a 0E          	.uleb128 0xe
 9876 010b 3A          	.uleb128 0x3a
 9877 010c 0B          	.uleb128 0xb
 9878 010d 3B          	.uleb128 0x3b
 9879 010e 05          	.uleb128 0x5
 9880 010f 49          	.uleb128 0x49
 9881 0110 13          	.uleb128 0x13
 9882 0111 02          	.uleb128 0x2
 9883 0112 0A          	.uleb128 0xa
 9884 0113 00          	.byte 0x0
 9885 0114 00          	.byte 0x0
 9886 0115 14          	.uleb128 0x14
 9887 0116 2E          	.uleb128 0x2e
 9888 0117 00          	.byte 0x0
 9889 0118 3F          	.uleb128 0x3f
 9890 0119 0C          	.uleb128 0xc
 9891 011a 03          	.uleb128 0x3
 9892 011b 08          	.uleb128 0x8
 9893 011c 3A          	.uleb128 0x3a
 9894 011d 0B          	.uleb128 0xb
 9895 011e 3B          	.uleb128 0x3b
 9896 011f 05          	.uleb128 0x5
 9897 0120 27          	.uleb128 0x27
 9898 0121 0C          	.uleb128 0xc
 9899 0122 11          	.uleb128 0x11
 9900 0123 01          	.uleb128 0x1
 9901 0124 12          	.uleb128 0x12
 9902 0125 01          	.uleb128 0x1
 9903 0126 40          	.uleb128 0x40
 9904 0127 0A          	.uleb128 0xa
 9905 0128 00          	.byte 0x0
 9906 0129 00          	.byte 0x0
 9907 012a 15          	.uleb128 0x15
 9908 012b 34          	.uleb128 0x34
 9909 012c 00          	.byte 0x0
 9910 012d 03          	.uleb128 0x3
 9911 012e 08          	.uleb128 0x8
 9912 012f 3A          	.uleb128 0x3a
 9913 0130 0B          	.uleb128 0xb
 9914 0131 3B          	.uleb128 0x3b
 9915 0132 05          	.uleb128 0x5
 9916 0133 49          	.uleb128 0x49
 9917 0134 13          	.uleb128 0x13
 9918 0135 3F          	.uleb128 0x3f
 9919 0136 0C          	.uleb128 0xc
 9920 0137 3C          	.uleb128 0x3c
 9921 0138 0C          	.uleb128 0xc
 9922 0139 00          	.byte 0x0
 9923 013a 00          	.byte 0x0
 9924 013b 16          	.uleb128 0x16
 9925 013c 35          	.uleb128 0x35
 9926 013d 00          	.byte 0x0
 9927 013e 49          	.uleb128 0x49
 9928 013f 13          	.uleb128 0x13
MPLAB XC16 ASSEMBLY Listing:   			page 200


 9929 0140 00          	.byte 0x0
 9930 0141 00          	.byte 0x0
 9931 0142 17          	.uleb128 0x17
 9932 0143 34          	.uleb128 0x34
 9933 0144 00          	.byte 0x0
 9934 0145 03          	.uleb128 0x3
 9935 0146 0E          	.uleb128 0xe
 9936 0147 3A          	.uleb128 0x3a
 9937 0148 0B          	.uleb128 0xb
 9938 0149 3B          	.uleb128 0x3b
 9939 014a 05          	.uleb128 0x5
 9940 014b 49          	.uleb128 0x49
 9941 014c 13          	.uleb128 0x13
 9942 014d 3F          	.uleb128 0x3f
 9943 014e 0C          	.uleb128 0xc
 9944 014f 3C          	.uleb128 0x3c
 9945 0150 0C          	.uleb128 0xc
 9946 0151 00          	.byte 0x0
 9947 0152 00          	.byte 0x0
 9948 0153 18          	.uleb128 0x18
 9949 0154 15          	.uleb128 0x15
 9950 0155 00          	.byte 0x0
 9951 0156 27          	.uleb128 0x27
 9952 0157 0C          	.uleb128 0xc
 9953 0158 00          	.byte 0x0
 9954 0159 00          	.byte 0x0
 9955 015a 19          	.uleb128 0x19
 9956 015b 34          	.uleb128 0x34
 9957 015c 00          	.byte 0x0
 9958 015d 03          	.uleb128 0x3
 9959 015e 0E          	.uleb128 0xe
 9960 015f 3A          	.uleb128 0x3a
 9961 0160 0B          	.uleb128 0xb
 9962 0161 3B          	.uleb128 0x3b
 9963 0162 0B          	.uleb128 0xb
 9964 0163 49          	.uleb128 0x49
 9965 0164 13          	.uleb128 0x13
 9966 0165 3F          	.uleb128 0x3f
 9967 0166 0C          	.uleb128 0xc
 9968 0167 3C          	.uleb128 0x3c
 9969 0168 0C          	.uleb128 0xc
 9970 0169 00          	.byte 0x0
 9971 016a 00          	.byte 0x0
 9972 016b 1A          	.uleb128 0x1a
 9973 016c 34          	.uleb128 0x34
 9974 016d 00          	.byte 0x0
 9975 016e 03          	.uleb128 0x3
 9976 016f 0E          	.uleb128 0xe
 9977 0170 3A          	.uleb128 0x3a
 9978 0171 0B          	.uleb128 0xb
 9979 0172 3B          	.uleb128 0x3b
 9980 0173 0B          	.uleb128 0xb
 9981 0174 49          	.uleb128 0x49
 9982 0175 13          	.uleb128 0x13
 9983 0176 3F          	.uleb128 0x3f
 9984 0177 0C          	.uleb128 0xc
 9985 0178 02          	.uleb128 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 201


 9986 0179 0A          	.uleb128 0xa
 9987 017a 00          	.byte 0x0
 9988 017b 00          	.byte 0x0
 9989 017c 00          	.byte 0x0
 9990                 	.section .debug_pubnames,info
 9991 0000 A9 04 00 00 	.4byte 0x4a9
 9992 0004 02 00       	.2byte 0x2
 9993 0006 00 00 00 00 	.4byte .Ldebug_info0
 9994 000a 90 31 00 00 	.4byte 0x3190
 9995 000e B8 21 00 00 	.4byte 0x21b8
 9996 0012 64 65 66 61 	.asciz "default_timer"
 9996      75 6C 74 5F 
 9996      74 69 6D 65 
 9996      72 00 
 9997 0020 D5 21 00 00 	.4byte 0x21d5
 9998 0024 63 6F 6E 66 	.asciz "config_timer1"
 9998      69 67 5F 74 
 9998      69 6D 65 72 
 9998      31 00 
 9999 0032 2D 22 00 00 	.4byte 0x222d
 10000 0036 63 6F 6E 66 	.asciz "config_timer1_us"
 10000      69 67 5F 74 
 10000      69 6D 65 72 
 10000      31 5F 75 73 
 10000      00 
 10001 0047 6E 22 00 00 	.4byte 0x226e
 10002 004b 63 6F 6E 66 	.asciz "config_timer2"
 10002      69 67 5F 74 
 10002      69 6D 65 72 
 10002      32 00 
 10003 0059 BA 22 00 00 	.4byte 0x22ba
 10004 005d 63 6F 6E 66 	.asciz "config_timer2_us"
 10004      69 67 5F 74 
 10004      69 6D 65 72 
 10004      32 5F 75 73 
 10004      00 
 10005 006e FB 22 00 00 	.4byte 0x22fb
 10006 0072 63 6F 6E 66 	.asciz "config_timer3"
 10006      69 67 5F 74 
 10006      69 6D 65 72 
 10006      33 00 
 10007 0080 47 23 00 00 	.4byte 0x2347
 10008 0084 63 6F 6E 66 	.asciz "config_timer3_us"
 10008      69 67 5F 74 
 10008      69 6D 65 72 
 10008      33 5F 75 73 
 10008      00 
 10009 0095 88 23 00 00 	.4byte 0x2388
 10010 0099 63 6F 6E 66 	.asciz "config_timer4"
 10010      69 67 5F 74 
 10010      69 6D 65 72 
 10010      34 00 
 10011 00a7 D4 23 00 00 	.4byte 0x23d4
 10012 00ab 63 6F 6E 66 	.asciz "config_timer4_us"
 10012      69 67 5F 74 
 10012      69 6D 65 72 
 10012      34 5F 75 73 
MPLAB XC16 ASSEMBLY Listing:   			page 202


 10012      00 
 10013 00bc 15 24 00 00 	.4byte 0x2415
 10014 00c0 63 6F 6E 66 	.asciz "config_timer5"
 10014      69 67 5F 74 
 10014      69 6D 65 72 
 10014      35 00 
 10015 00ce 65 24 00 00 	.4byte 0x2465
 10016 00d2 63 6F 6E 66 	.asciz "config_timer5_us"
 10016      69 67 5F 74 
 10016      69 6D 65 72 
 10016      35 5F 75 73 
 10016      00 
 10017 00e3 A9 24 00 00 	.4byte 0x24a9
 10018 00e7 63 6F 6E 66 	.asciz "config_timer6"
 10018      69 67 5F 74 
 10018      69 6D 65 72 
 10018      36 00 
 10019 00f5 F9 24 00 00 	.4byte 0x24f9
 10020 00f9 63 6F 6E 66 	.asciz "config_timer6_us"
 10020      69 67 5F 74 
 10020      69 6D 65 72 
 10020      36 5F 75 73 
 10020      00 
 10021 010a 3D 25 00 00 	.4byte 0x253d
 10022 010e 63 6F 6E 66 	.asciz "config_timer7"
 10022      69 67 5F 74 
 10022      69 6D 65 72 
 10022      37 00 
 10023 011c 8D 25 00 00 	.4byte 0x258d
 10024 0120 63 6F 6E 66 	.asciz "config_timer7_us"
 10024      69 67 5F 74 
 10024      69 6D 65 72 
 10024      37 5F 75 73 
 10024      00 
 10025 0131 D1 25 00 00 	.4byte 0x25d1
 10026 0135 63 6F 6E 66 	.asciz "config_timer8"
 10026      69 67 5F 74 
 10026      69 6D 65 72 
 10026      38 00 
 10027 0143 21 26 00 00 	.4byte 0x2621
 10028 0147 63 6F 6E 66 	.asciz "config_timer8_us"
 10028      69 67 5F 74 
 10028      69 6D 65 72 
 10028      38 5F 75 73 
 10028      00 
 10029 0158 65 26 00 00 	.4byte 0x2665
 10030 015c 63 6F 6E 66 	.asciz "config_timer9"
 10030      69 67 5F 74 
 10030      69 6D 65 72 
 10030      39 00 
 10031 016a B5 26 00 00 	.4byte 0x26b5
 10032 016e 63 6F 6E 66 	.asciz "config_timer9_us"
 10032      69 67 5F 74 
 10032      69 6D 65 72 
 10032      39 5F 75 73 
 10032      00 
 10033 017f F9 26 00 00 	.4byte 0x26f9
MPLAB XC16 ASSEMBLY Listing:   			page 203


 10034 0183 65 6E 61 62 	.asciz "enable_timer1"
 10034      6C 65 5F 74 
 10034      69 6D 65 72 
 10034      31 00 
 10035 0191 17 27 00 00 	.4byte 0x2717
 10036 0195 65 6E 61 62 	.asciz "enable_timer2"
 10036      6C 65 5F 74 
 10036      69 6D 65 72 
 10036      32 00 
 10037 01a3 35 27 00 00 	.4byte 0x2735
 10038 01a7 65 6E 61 62 	.asciz "enable_timer3"
 10038      6C 65 5F 74 
 10038      69 6D 65 72 
 10038      33 00 
 10039 01b5 53 27 00 00 	.4byte 0x2753
 10040 01b9 65 6E 61 62 	.asciz "enable_timer4"
 10040      6C 65 5F 74 
 10040      69 6D 65 72 
 10040      34 00 
 10041 01c7 71 27 00 00 	.4byte 0x2771
 10042 01cb 65 6E 61 62 	.asciz "enable_timer5"
 10042      6C 65 5F 74 
 10042      69 6D 65 72 
 10042      35 00 
 10043 01d9 8F 27 00 00 	.4byte 0x278f
 10044 01dd 65 6E 61 62 	.asciz "enable_timer6"
 10044      6C 65 5F 74 
 10044      69 6D 65 72 
 10044      36 00 
 10045 01eb AD 27 00 00 	.4byte 0x27ad
 10046 01ef 65 6E 61 62 	.asciz "enable_timer7"
 10046      6C 65 5F 74 
 10046      69 6D 65 72 
 10046      37 00 
 10047 01fd CB 27 00 00 	.4byte 0x27cb
 10048 0201 65 6E 61 62 	.asciz "enable_timer8"
 10048      6C 65 5F 74 
 10048      69 6D 65 72 
 10048      38 00 
 10049 020f E9 27 00 00 	.4byte 0x27e9
 10050 0213 65 6E 61 62 	.asciz "enable_timer9"
 10050      6C 65 5F 74 
 10050      69 6D 65 72 
 10050      39 00 
 10051 0221 07 28 00 00 	.4byte 0x2807
 10052 0225 64 69 73 61 	.asciz "disable_timer1"
 10052      62 6C 65 5F 
 10052      74 69 6D 65 
 10052      72 31 00 
 10053 0234 26 28 00 00 	.4byte 0x2826
 10054 0238 64 69 73 61 	.asciz "disable_timer2"
 10054      62 6C 65 5F 
 10054      74 69 6D 65 
 10054      72 32 00 
 10055 0247 45 28 00 00 	.4byte 0x2845
 10056 024b 64 69 73 61 	.asciz "disable_timer3"
 10056      62 6C 65 5F 
MPLAB XC16 ASSEMBLY Listing:   			page 204


 10056      74 69 6D 65 
 10056      72 33 00 
 10057 025a 64 28 00 00 	.4byte 0x2864
 10058 025e 64 69 73 61 	.asciz "disable_timer4"
 10058      62 6C 65 5F 
 10058      74 69 6D 65 
 10058      72 34 00 
 10059 026d 83 28 00 00 	.4byte 0x2883
 10060 0271 64 69 73 61 	.asciz "disable_timer5"
 10060      62 6C 65 5F 
 10060      74 69 6D 65 
 10060      72 35 00 
 10061 0280 A2 28 00 00 	.4byte 0x28a2
 10062 0284 64 69 73 61 	.asciz "disable_timer6"
 10062      62 6C 65 5F 
 10062      74 69 6D 65 
 10062      72 36 00 
 10063 0293 C1 28 00 00 	.4byte 0x28c1
 10064 0297 64 69 73 61 	.asciz "disable_timer7"
 10064      62 6C 65 5F 
 10064      74 69 6D 65 
 10064      72 37 00 
 10065 02a6 E0 28 00 00 	.4byte 0x28e0
 10066 02aa 64 69 73 61 	.asciz "disable_timer8"
 10066      62 6C 65 5F 
 10066      74 69 6D 65 
 10066      72 38 00 
 10067 02b9 FF 28 00 00 	.4byte 0x28ff
 10068 02bd 64 69 73 61 	.asciz "disable_timer9"
 10068      62 6C 65 5F 
 10068      74 69 6D 65 
 10068      72 39 00 
 10069 02cc 1E 29 00 00 	.4byte 0x291e
 10070 02d0 72 65 73 65 	.asciz "reset_timer1"
 10070      74 5F 74 69 
 10070      6D 65 72 31 
 10070      00 
 10071 02dd 3B 29 00 00 	.4byte 0x293b
 10072 02e1 72 65 73 65 	.asciz "reset_timer2"
 10072      74 5F 74 69 
 10072      6D 65 72 32 
 10072      00 
 10073 02ee 58 29 00 00 	.4byte 0x2958
 10074 02f2 72 65 73 65 	.asciz "reset_timer3"
 10074      74 5F 74 69 
 10074      6D 65 72 33 
 10074      00 
 10075 02ff 75 29 00 00 	.4byte 0x2975
 10076 0303 72 65 73 65 	.asciz "reset_timer4"
 10076      74 5F 74 69 
 10076      6D 65 72 34 
 10076      00 
 10077 0310 92 29 00 00 	.4byte 0x2992
 10078 0314 72 65 73 65 	.asciz "reset_timer5"
 10078      74 5F 74 69 
 10078      6D 65 72 35 
 10078      00 
MPLAB XC16 ASSEMBLY Listing:   			page 205


 10079 0321 AF 29 00 00 	.4byte 0x29af
 10080 0325 72 65 73 65 	.asciz "reset_timer6"
 10080      74 5F 74 69 
 10080      6D 65 72 36 
 10080      00 
 10081 0332 CC 29 00 00 	.4byte 0x29cc
 10082 0336 72 65 73 65 	.asciz "reset_timer7"
 10082      74 5F 74 69 
 10082      6D 65 72 37 
 10082      00 
 10083 0343 E9 29 00 00 	.4byte 0x29e9
 10084 0347 72 65 73 65 	.asciz "reset_timer8"
 10084      74 5F 74 69 
 10084      6D 65 72 38 
 10084      00 
 10085 0354 06 2A 00 00 	.4byte 0x2a06
 10086 0358 72 65 73 65 	.asciz "reset_timer9"
 10086      74 5F 74 69 
 10086      6D 65 72 39 
 10086      00 
 10087 0365 23 2A 00 00 	.4byte 0x2a23
 10088 0369 5F 54 31 49 	.asciz "_T1Interrupt"
 10088      6E 74 65 72 
 10088      72 75 70 74 
 10088      00 
 10089 0376 40 2A 00 00 	.4byte 0x2a40
 10090 037a 5F 54 32 49 	.asciz "_T2Interrupt"
 10090      6E 74 65 72 
 10090      72 75 70 74 
 10090      00 
 10091 0387 5D 2A 00 00 	.4byte 0x2a5d
 10092 038b 5F 54 33 49 	.asciz "_T3Interrupt"
 10092      6E 74 65 72 
 10092      72 75 70 74 
 10092      00 
 10093 0398 7A 2A 00 00 	.4byte 0x2a7a
 10094 039c 5F 54 34 49 	.asciz "_T4Interrupt"
 10094      6E 74 65 72 
 10094      72 75 70 74 
 10094      00 
 10095 03a9 97 2A 00 00 	.4byte 0x2a97
 10096 03ad 5F 54 35 49 	.asciz "_T5Interrupt"
 10096      6E 74 65 72 
 10096      72 75 70 74 
 10096      00 
 10097 03ba B4 2A 00 00 	.4byte 0x2ab4
 10098 03be 5F 54 36 49 	.asciz "_T6Interrupt"
 10098      6E 74 65 72 
 10098      72 75 70 74 
 10098      00 
 10099 03cb D1 2A 00 00 	.4byte 0x2ad1
 10100 03cf 5F 54 37 49 	.asciz "_T7Interrupt"
 10100      6E 74 65 72 
 10100      72 75 70 74 
 10100      00 
 10101 03dc EE 2A 00 00 	.4byte 0x2aee
 10102 03e0 5F 54 38 49 	.asciz "_T8Interrupt"
MPLAB XC16 ASSEMBLY Listing:   			page 206


 10102      6E 74 65 72 
 10102      72 75 70 74 
 10102      00 
 10103 03ed 0B 2B 00 00 	.4byte 0x2b0b
 10104 03f1 5F 54 39 49 	.asciz "_T9Interrupt"
 10104      6E 74 65 72 
 10104      72 75 70 74 
 10104      00 
 10105 03fe ED 30 00 00 	.4byte 0x30ed
 10106 0402 66 75 6E 5F 	.asciz "fun_ptr_timer1"
 10106      70 74 72 5F 
 10106      74 69 6D 65 
 10106      72 31 00 
 10107 0411 FF 30 00 00 	.4byte 0x30ff
 10108 0415 66 75 6E 5F 	.asciz "fun_ptr_timer2"
 10108      70 74 72 5F 
 10108      74 69 6D 65 
 10108      72 32 00 
 10109 0424 11 31 00 00 	.4byte 0x3111
 10110 0428 66 75 6E 5F 	.asciz "fun_ptr_timer3"
 10110      70 74 72 5F 
 10110      74 69 6D 65 
 10110      72 33 00 
 10111 0437 23 31 00 00 	.4byte 0x3123
 10112 043b 66 75 6E 5F 	.asciz "fun_ptr_timer4"
 10112      70 74 72 5F 
 10112      74 69 6D 65 
 10112      72 34 00 
 10113 044a 35 31 00 00 	.4byte 0x3135
 10114 044e 66 75 6E 5F 	.asciz "fun_ptr_timer5"
 10114      70 74 72 5F 
 10114      74 69 6D 65 
 10114      72 35 00 
 10115 045d 47 31 00 00 	.4byte 0x3147
 10116 0461 66 75 6E 5F 	.asciz "fun_ptr_timer6"
 10116      70 74 72 5F 
 10116      74 69 6D 65 
 10116      72 36 00 
 10117 0470 59 31 00 00 	.4byte 0x3159
 10118 0474 66 75 6E 5F 	.asciz "fun_ptr_timer7"
 10118      70 74 72 5F 
 10118      74 69 6D 65 
 10118      72 37 00 
 10119 0483 6B 31 00 00 	.4byte 0x316b
 10120 0487 66 75 6E 5F 	.asciz "fun_ptr_timer8"
 10120      70 74 72 5F 
 10120      74 69 6D 65 
 10120      72 38 00 
 10121 0496 7D 31 00 00 	.4byte 0x317d
 10122 049a 66 75 6E 5F 	.asciz "fun_ptr_timer9"
 10122      70 74 72 5F 
 10122      74 69 6D 65 
 10122      72 39 00 
 10123 04a9 00 00 00 00 	.4byte 0x0
 10124                 	.section .debug_pubtypes,info
 10125 0000 08 03 00 00 	.4byte 0x308
 10126 0004 02 00       	.2byte 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 207


 10127 0006 00 00 00 00 	.4byte .Ldebug_info0
 10128 000a 90 31 00 00 	.4byte 0x3190
 10129 000e EA 00 00 00 	.4byte 0xea
 10130 0012 75 69 6E 74 	.asciz "uint16_t"
 10130      31 36 5F 74 
 10130      00 
 10131 001b F3 01 00 00 	.4byte 0x1f3
 10132 001f 74 61 67 54 	.asciz "tagT1CONBITS"
 10132      31 43 4F 4E 
 10132      42 49 54 53 
 10132      00 
 10133 002c 12 02 00 00 	.4byte 0x212
 10134 0030 54 31 43 4F 	.asciz "T1CONBITS"
 10134      4E 42 49 54 
 10134      53 00 
 10135 003a DC 02 00 00 	.4byte 0x2dc
 10136 003e 74 61 67 54 	.asciz "tagT2CONBITS"
 10136      32 43 4F 4E 
 10136      42 49 54 53 
 10136      00 
 10137 004b FB 02 00 00 	.4byte 0x2fb
 10138 004f 54 32 43 4F 	.asciz "T2CONBITS"
 10138      4E 42 49 54 
 10138      53 00 
 10139 0059 B3 03 00 00 	.4byte 0x3b3
 10140 005d 74 61 67 54 	.asciz "tagT3CONBITS"
 10140      33 43 4F 4E 
 10140      42 49 54 53 
 10140      00 
 10141 006a D2 03 00 00 	.4byte 0x3d2
 10142 006e 54 33 43 4F 	.asciz "T3CONBITS"
 10142      4E 42 49 54 
 10142      53 00 
 10143 0078 9C 04 00 00 	.4byte 0x49c
 10144 007c 74 61 67 54 	.asciz "tagT4CONBITS"
 10144      34 43 4F 4E 
 10144      42 49 54 53 
 10144      00 
 10145 0089 BB 04 00 00 	.4byte 0x4bb
 10146 008d 54 34 43 4F 	.asciz "T4CONBITS"
 10146      4E 42 49 54 
 10146      53 00 
 10147 0097 73 05 00 00 	.4byte 0x573
 10148 009b 74 61 67 54 	.asciz "tagT5CONBITS"
 10148      35 43 4F 4E 
 10148      42 49 54 53 
 10148      00 
 10149 00a8 92 05 00 00 	.4byte 0x592
 10150 00ac 54 35 43 4F 	.asciz "T5CONBITS"
 10150      4E 42 49 54 
 10150      53 00 
 10151 00b6 5C 06 00 00 	.4byte 0x65c
 10152 00ba 74 61 67 54 	.asciz "tagT6CONBITS"
 10152      36 43 4F 4E 
 10152      42 49 54 53 
 10152      00 
 10153 00c7 7B 06 00 00 	.4byte 0x67b
MPLAB XC16 ASSEMBLY Listing:   			page 208


 10154 00cb 54 36 43 4F 	.asciz "T6CONBITS"
 10154      4E 42 49 54 
 10154      53 00 
 10155 00d5 33 07 00 00 	.4byte 0x733
 10156 00d9 74 61 67 54 	.asciz "tagT7CONBITS"
 10156      37 43 4F 4E 
 10156      42 49 54 53 
 10156      00 
 10157 00e6 52 07 00 00 	.4byte 0x752
 10158 00ea 54 37 43 4F 	.asciz "T7CONBITS"
 10158      4E 42 49 54 
 10158      53 00 
 10159 00f4 1C 08 00 00 	.4byte 0x81c
 10160 00f8 74 61 67 54 	.asciz "tagT8CONBITS"
 10160      38 43 4F 4E 
 10160      42 49 54 53 
 10160      00 
 10161 0105 3B 08 00 00 	.4byte 0x83b
 10162 0109 54 38 43 4F 	.asciz "T8CONBITS"
 10162      4E 42 49 54 
 10162      53 00 
 10163 0113 F3 08 00 00 	.4byte 0x8f3
 10164 0117 74 61 67 54 	.asciz "tagT9CONBITS"
 10164      39 43 4F 4E 
 10164      42 49 54 53 
 10164      00 
 10165 0124 12 09 00 00 	.4byte 0x912
 10166 0128 54 39 43 4F 	.asciz "T9CONBITS"
 10166      4E 42 49 54 
 10166      53 00 
 10167 0132 24 09 00 00 	.4byte 0x924
 10168 0136 74 61 67 49 	.asciz "tagIFS0BITS"
 10168      46 53 30 42 
 10168      49 54 53 00 
 10169 0142 7F 0A 00 00 	.4byte 0xa7f
 10170 0146 49 46 53 30 	.asciz "IFS0BITS"
 10170      42 49 54 53 
 10170      00 
 10171 014f 90 0A 00 00 	.4byte 0xa90
 10172 0153 74 61 67 49 	.asciz "tagIFS1BITS"
 10172      46 53 31 42 
 10172      49 54 53 00 
 10173 015f EB 0B 00 00 	.4byte 0xbeb
 10174 0163 49 46 53 31 	.asciz "IFS1BITS"
 10174      42 49 54 53 
 10174      00 
 10175 016c FC 0B 00 00 	.4byte 0xbfc
 10176 0170 74 61 67 49 	.asciz "tagIFS2BITS"
 10176      46 53 32 42 
 10176      49 54 53 00 
 10177 017c 56 0D 00 00 	.4byte 0xd56
 10178 0180 49 46 53 32 	.asciz "IFS2BITS"
 10178      42 49 54 53 
 10178      00 
 10179 0189 67 0D 00 00 	.4byte 0xd67
 10180 018d 74 61 67 49 	.asciz "tagIFS3BITS"
 10180      46 53 33 42 
MPLAB XC16 ASSEMBLY Listing:   			page 209


 10180      49 54 53 00 
 10181 0199 B0 0E 00 00 	.4byte 0xeb0
 10182 019d 49 46 53 33 	.asciz "IFS3BITS"
 10182      42 49 54 53 
 10182      00 
 10183 01a6 C1 0E 00 00 	.4byte 0xec1
 10184 01aa 74 61 67 49 	.asciz "tagIEC0BITS"
 10184      45 43 30 42 
 10184      49 54 53 00 
 10185 01b6 1C 10 00 00 	.4byte 0x101c
 10186 01ba 49 45 43 30 	.asciz "IEC0BITS"
 10186      42 49 54 53 
 10186      00 
 10187 01c3 2D 10 00 00 	.4byte 0x102d
 10188 01c7 74 61 67 49 	.asciz "tagIEC1BITS"
 10188      45 43 31 42 
 10188      49 54 53 00 
 10189 01d3 88 11 00 00 	.4byte 0x1188
 10190 01d7 49 45 43 31 	.asciz "IEC1BITS"
 10190      42 49 54 53 
 10190      00 
 10191 01e0 99 11 00 00 	.4byte 0x1199
 10192 01e4 74 61 67 49 	.asciz "tagIEC2BITS"
 10192      45 43 32 42 
 10192      49 54 53 00 
 10193 01f0 F3 12 00 00 	.4byte 0x12f3
 10194 01f4 49 45 43 32 	.asciz "IEC2BITS"
 10194      42 49 54 53 
 10194      00 
 10195 01fd 04 13 00 00 	.4byte 0x1304
 10196 0201 74 61 67 49 	.asciz "tagIEC3BITS"
 10196      45 43 33 42 
 10196      49 54 53 00 
 10197 020d 4D 14 00 00 	.4byte 0x144d
 10198 0211 49 45 43 33 	.asciz "IEC3BITS"
 10198      42 49 54 53 
 10198      00 
 10199 021a D2 15 00 00 	.4byte 0x15d2
 10200 021e 74 61 67 49 	.asciz "tagIPC0BITS"
 10200      50 43 30 42 
 10200      49 54 53 00 
 10201 022a F0 15 00 00 	.4byte 0x15f0
 10202 022e 49 50 43 30 	.asciz "IPC0BITS"
 10202      42 49 54 53 
 10202      00 
 10203 0237 75 17 00 00 	.4byte 0x1775
 10204 023b 74 61 67 49 	.asciz "tagIPC1BITS"
 10204      50 43 31 42 
 10204      49 54 53 00 
 10205 0247 93 17 00 00 	.4byte 0x1793
 10206 024b 49 50 43 31 	.asciz "IPC1BITS"
 10206      42 49 54 53 
 10206      00 
 10207 0254 24 19 00 00 	.4byte 0x1924
 10208 0258 74 61 67 49 	.asciz "tagIPC2BITS"
 10208      50 43 32 42 
 10208      49 54 53 00 
MPLAB XC16 ASSEMBLY Listing:   			page 210


 10209 0264 42 19 00 00 	.4byte 0x1942
 10210 0268 49 50 43 32 	.asciz "IPC2BITS"
 10210      42 49 54 53 
 10210      00 
 10211 0271 C7 1A 00 00 	.4byte 0x1ac7
 10212 0275 74 61 67 49 	.asciz "tagIPC6BITS"
 10212      50 43 36 42 
 10212      49 54 53 00 
 10213 0281 E5 1A 00 00 	.4byte 0x1ae5
 10214 0285 49 50 43 36 	.asciz "IPC6BITS"
 10214      42 49 54 53 
 10214      00 
 10215 028e 72 1C 00 00 	.4byte 0x1c72
 10216 0292 74 61 67 49 	.asciz "tagIPC7BITS"
 10216      50 43 37 42 
 10216      49 54 53 00 
 10217 029e 90 1C 00 00 	.4byte 0x1c90
 10218 02a2 49 50 43 37 	.asciz "IPC7BITS"
 10218      42 49 54 53 
 10218      00 
 10219 02ab 15 1E 00 00 	.4byte 0x1e15
 10220 02af 74 61 67 49 	.asciz "tagIPC11BITS"
 10220      50 43 31 31 
 10220      42 49 54 53 
 10220      00 
 10221 02bc 34 1E 00 00 	.4byte 0x1e34
 10222 02c0 49 50 43 31 	.asciz "IPC11BITS"
 10222      31 42 49 54 
 10222      53 00 
 10223 02ca C2 1F 00 00 	.4byte 0x1fc2
 10224 02ce 74 61 67 49 	.asciz "tagIPC12BITS"
 10224      50 43 31 32 
 10224      42 49 54 53 
 10224      00 
 10225 02db E1 1F 00 00 	.4byte 0x1fe1
 10226 02df 49 50 43 31 	.asciz "IPC12BITS"
 10226      32 42 49 54 
 10226      53 00 
 10227 02e9 6F 21 00 00 	.4byte 0x216f
 10228 02ed 74 61 67 49 	.asciz "tagIPC13BITS"
 10228      50 43 31 33 
 10228      42 49 54 53 
 10228      00 
 10229 02fa 8E 21 00 00 	.4byte 0x218e
 10230 02fe 49 50 43 31 	.asciz "IPC13BITS"
 10230      33 42 49 54 
 10230      53 00 
 10231 0308 00 00 00 00 	.4byte 0x0
 10232                 	.section .debug_aranges,info
 10233 0000 14 00 00 00 	.4byte 0x14
 10234 0004 02 00       	.2byte 0x2
 10235 0006 00 00 00 00 	.4byte .Ldebug_info0
 10236 000a 04          	.byte 0x4
 10237 000b 00          	.byte 0x0
 10238 000c 00 00       	.2byte 0x0
 10239 000e 00 00       	.2byte 0x0
 10240 0010 00 00 00 00 	.4byte 0x0
MPLAB XC16 ASSEMBLY Listing:   			page 211


 10241 0014 00 00 00 00 	.4byte 0x0
 10242                 	.section .debug_str,info
 10243                 	.LASF18:
 10244 0000 49 46 53 31 	.asciz "IFS1bits"
 10244      62 69 74 73 
 10244      00 
 10245                 	.LASF10:
 10246 0009 54 33 43 4F 	.asciz "T3CONbits"
 10246      4E 62 69 74 
 10246      73 00 
 10247                 	.LASF15:
 10248 0013 54 38 43 4F 	.asciz "T8CONbits"
 10248      4E 62 69 74 
 10248      73 00 
 10249                 	.LASF31:
 10250 001d 49 50 43 31 	.asciz "IPC12bits"
 10250      32 62 69 74 
 10250      73 00 
 10251                 	.LASF25:
 10252 0027 49 50 43 30 	.asciz "IPC0bits"
 10252      62 69 74 73 
 10252      00 
 10253                 	.LASF1:
 10254 0030 54 47 41 54 	.asciz "TGATE"
 10254      45 00 
 10255                 	.LASF21:
 10256 0036 49 45 43 30 	.asciz "IEC0bits"
 10256      62 69 74 73 
 10256      00 
 10257                 	.LASF9:
 10258 003f 54 32 43 4F 	.asciz "T2CONbits"
 10258      4E 62 69 74 
 10258      73 00 
 10259                 	.LASF14:
 10260 0049 54 37 43 4F 	.asciz "T7CONbits"
 10260      4E 62 69 74 
 10260      73 00 
 10261                 	.LASF19:
 10262 0053 49 46 53 32 	.asciz "IFS2bits"
 10262      62 69 74 73 
 10262      00 
 10263                 	.LASF17:
 10264 005c 49 46 53 30 	.asciz "IFS0bits"
 10264      62 69 74 73 
 10264      00 
 10265                 	.LASF32:
 10266 0065 49 50 43 31 	.asciz "IPC13bits"
 10266      33 62 69 74 
 10266      73 00 
 10267                 	.LASF26:
 10268 006f 49 50 43 31 	.asciz "IPC1bits"
 10268      62 69 74 73 
 10268      00 
 10269                 	.LASF8:
 10270 0078 54 31 43 4F 	.asciz "T1CONbits"
 10270      4E 62 69 74 
 10270      73 00 
MPLAB XC16 ASSEMBLY Listing:   			page 212


 10271                 	.LASF22:
 10272 0082 49 45 43 31 	.asciz "IEC1bits"
 10272      62 69 74 73 
 10272      00 
 10273                 	.LASF13:
 10274 008b 54 36 43 4F 	.asciz "T6CONbits"
 10274      4E 62 69 74 
 10274      73 00 
 10275                 	.LASF6:
 10276 0095 70 72 69 6F 	.asciz "priority"
 10276      72 69 74 79 
 10276      00 
 10277                 	.LASF37:
 10278 009e 66 75 6E 5F 	.asciz "fun_ptr_timer5"
 10278      70 74 72 5F 
 10278      74 69 6D 65 
 10278      72 35 00 
 10279                 	.LASF20:
 10280 00ad 49 46 53 33 	.asciz "IFS3bits"
 10280      62 69 74 73 
 10280      00 
 10281                 	.LASF0:
 10282 00b6 54 43 4B 50 	.asciz "TCKPS"
 10282      53 00 
 10283                 	.LASF34:
 10284 00bc 66 75 6E 5F 	.asciz "fun_ptr_timer2"
 10284      70 74 72 5F 
 10284      74 69 6D 65 
 10284      72 32 00 
 10285                 	.LASF28:
 10286 00cb 49 50 43 36 	.asciz "IPC6bits"
 10286      62 69 74 73 
 10286      00 
 10287                 	.LASF39:
 10288 00d4 66 75 6E 5F 	.asciz "fun_ptr_timer7"
 10288      70 74 72 5F 
 10288      74 69 6D 65 
 10288      72 37 00 
 10289                 	.LASF27:
 10290 00e3 49 50 43 32 	.asciz "IPC2bits"
 10290      62 69 74 73 
 10290      00 
 10291                 	.LASF12:
 10292 00ec 54 35 43 4F 	.asciz "T5CONbits"
 10292      4E 62 69 74 
 10292      73 00 
 10293                 	.LASF38:
 10294 00f6 66 75 6E 5F 	.asciz "fun_ptr_timer6"
 10294      70 74 72 5F 
 10294      74 69 6D 65 
 10294      72 36 00 
 10295                 	.LASF33:
 10296 0105 66 75 6E 5F 	.asciz "fun_ptr_timer1"
 10296      70 74 72 5F 
 10296      74 69 6D 65 
 10296      72 31 00 
 10297                 	.LASF36:
MPLAB XC16 ASSEMBLY Listing:   			page 213


 10298 0114 66 75 6E 5F 	.asciz "fun_ptr_timer4"
 10298      70 74 72 5F 
 10298      74 69 6D 65 
 10298      72 34 00 
 10299                 	.LASF23:
 10300 0123 49 45 43 32 	.asciz "IEC2bits"
 10300      62 69 74 73 
 10300      00 
 10301                 	.LASF35:
 10302 012c 66 75 6E 5F 	.asciz "fun_ptr_timer3"
 10302      70 74 72 5F 
 10302      74 69 6D 65 
 10302      72 33 00 
 10303                 	.LASF40:
 10304 013b 66 75 6E 5F 	.asciz "fun_ptr_timer8"
 10304      70 74 72 5F 
 10304      74 69 6D 65 
 10304      72 38 00 
 10305                 	.LASF41:
 10306 014a 66 75 6E 5F 	.asciz "fun_ptr_timer9"
 10306      70 74 72 5F 
 10306      74 69 6D 65 
 10306      72 39 00 
 10307                 	.LASF11:
 10308 0159 54 34 43 4F 	.asciz "T4CONbits"
 10308      4E 62 69 74 
 10308      73 00 
 10309                 	.LASF7:
 10310 0163 74 69 6D 65 	.asciz "timer_function"
 10310      72 5F 66 75 
 10310      6E 63 74 69 
 10310      6F 6E 00 
 10311                 	.LASF29:
 10312 0172 49 50 43 37 	.asciz "IPC7bits"
 10312      62 69 74 73 
 10312      00 
 10313                 	.LASF2:
 10314 017b 54 53 49 44 	.asciz "TSIDL"
 10314      4C 00 
 10315                 	.LASF30:
 10316 0181 49 50 43 31 	.asciz "IPC11bits"
 10316      31 62 69 74 
 10316      73 00 
 10317                 	.LASF24:
 10318 018b 49 45 43 33 	.asciz "IEC3bits"
 10318      62 69 74 73 
 10318      00 
 10319                 	.LASF16:
 10320 0194 54 39 43 4F 	.asciz "T9CONbits"
 10320      4E 62 69 74 
 10320      73 00 
 10321                 	.LASF3:
 10322 019e 54 43 4B 50 	.asciz "TCKPS0"
 10322      53 30 00 
 10323                 	.LASF4:
 10324 01a5 54 43 4B 50 	.asciz "TCKPS1"
 10324      53 31 00 
MPLAB XC16 ASSEMBLY Listing:   			page 214


 10325                 	.LASF5:
 10326 01ac 74 69 6D 65 	.asciz "time"
 10326      00 
 10327                 	.section .text,code
 10328              	
 10329              	
 10330              	
 10331              	.section __c30_info,info,bss
 10332                 	__psv_trap_errata:
 10333                 	
 10334                 	.section __c30_signature,info,data
 10335 0000 01 00       	.word 0x0001
 10336 0002 00 00       	.word 0x0000
 10337 0004 00 00       	.word 0x0000
 10338                 	
 10339                 	
 10340                 	
 10341                 	.set ___PA___,0
 10342                 	.end
MPLAB XC16 ASSEMBLY Listing:   			page 215


DEFINED SYMBOLS
                            *ABS*:00000000 /home/user/Documents/FST/Programming/project_sub-zero/lib/lib_pic33e/timer.c
                            *ABS*:00000001 __33EP256MU806
                            *ABS*:00000001 __dsPIC33E
                            *ABS*:00000001 __HAS_DSP
                            *ABS*:00000001 __HAS_DMAV2
                            *ABS*:00000001 __HAS_CODEGUARD
                            *ABS*:00000001 __HAS_PMP_ENHANCED
                            *ABS*:00000001 __HAS_EDS
                            *ABS*:00000001 __HAS_EP
                            *ABS*:00000011 __TARGET_DIVIDE_CYCLES
                            *ABS*:00000001 __C30ELF
    {standard input}:14     .nbss:00000000 _fun_ptr_timer1
    {standard input}:18     .nbss:00000002 _fun_ptr_timer2
    {standard input}:22     .nbss:00000004 _fun_ptr_timer3
    {standard input}:26     .nbss:00000006 _fun_ptr_timer4
    {standard input}:30     .nbss:00000008 _fun_ptr_timer5
    {standard input}:34     .nbss:0000000a _fun_ptr_timer6
    {standard input}:38     .nbss:0000000c _fun_ptr_timer7
    {standard input}:42     .nbss:0000000e _fun_ptr_timer8
    {standard input}:46     .nbss:00000010 _fun_ptr_timer9
    {standard input}:51     .text:00000000 _default_timer
    {standard input}:55     *ABS*:00000000 ___PA___
    {standard input}:69     .text:0000000a _config_timer1
    {standard input}:128    *ABS*:00000000 ___BP___
    {standard input}:149    .text:0000005e _config_timer1_us
    {standard input}:223    .text:000000b2 _config_timer2
    {standard input}:306    .text:00000108 _config_timer2_us
    {standard input}:383    .text:0000015e _config_timer3
    {standard input}:462    .text:000001b0 _config_timer3_us
    {standard input}:535    .text:00000202 _config_timer4
    {standard input}:618    .text:00000258 _config_timer4_us
    {standard input}:695    .text:000002ae _config_timer5
    {standard input}:774    .text:00000300 _config_timer5_us
    {standard input}:847    .text:00000352 _config_timer6
    {standard input}:930    .text:000003a8 _config_timer6_us
    {standard input}:1007   .text:000003fe _config_timer7
    {standard input}:1086   .text:00000450 _config_timer7_us
    {standard input}:1159   .text:000004a2 _config_timer8
    {standard input}:1242   .text:000004f8 _config_timer8_us
    {standard input}:1319   .text:0000054e _config_timer9
    {standard input}:1398   .text:000005a0 _config_timer9_us
    {standard input}:1471   .text:000005f2 _enable_timer1
    {standard input}:1490   .text:000005fe _enable_timer2
    {standard input}:1509   .text:0000060a _enable_timer3
    {standard input}:1528   .text:00000616 _enable_timer4
    {standard input}:1547   .text:00000622 _enable_timer5
    {standard input}:1566   .text:0000062e _enable_timer6
    {standard input}:1585   .text:0000063a _enable_timer7
    {standard input}:1604   .text:00000646 _enable_timer8
    {standard input}:1623   .text:00000652 _enable_timer9
    {standard input}:1642   .text:0000065e _disable_timer1
    {standard input}:1661   .text:0000066a _disable_timer2
    {standard input}:1680   .text:00000676 _disable_timer3
    {standard input}:1699   .text:00000682 _disable_timer4
    {standard input}:1718   .text:0000068e _disable_timer5
    {standard input}:1737   .text:0000069a _disable_timer6
MPLAB XC16 ASSEMBLY Listing:   			page 216


    {standard input}:1756   .text:000006a6 _disable_timer7
    {standard input}:1775   .text:000006b2 _disable_timer8
    {standard input}:1794   .text:000006be _disable_timer9
    {standard input}:1813   .text:000006ca _reset_timer1
    {standard input}:1832   .text:000006d6 _reset_timer2
    {standard input}:1851   .text:000006e2 _reset_timer3
    {standard input}:1870   .text:000006ee _reset_timer4
    {standard input}:1889   .text:000006fa _reset_timer5
    {standard input}:1908   .text:00000706 _reset_timer6
    {standard input}:1927   .text:00000712 _reset_timer7
    {standard input}:1946   .text:0000071e _reset_timer8
    {standard input}:1965   .text:0000072a _reset_timer9
                            *ABS*:00000040 __ext_attr_.isr.text
    {standard input}:1985   .isr.text:00000000 __T1Interrupt
    {standard input}:2039   .isr.text:0000003a __T2Interrupt
    {standard input}:2093   .isr.text:00000074 __T3Interrupt
    {standard input}:2147   .isr.text:000000ae __T4Interrupt
    {standard input}:2201   .isr.text:000000e8 __T5Interrupt
    {standard input}:2255   .isr.text:00000122 __T6Interrupt
    {standard input}:2309   .isr.text:0000015c __T7Interrupt
    {standard input}:2363   .isr.text:00000196 __T8Interrupt
    {standard input}:2417   .isr.text:000001d0 __T9Interrupt
    {standard input}:10332  __c30_info:00000000 __psv_trap_errata
    {standard input}:1991   .isr.text:00000000 .L0
    {standard input}:56     .text:00000000 .L0
                            .text:00000050 .L3
                            .text:00000052 .L4
                            .text:000000fa .L7
                            .text:000000fc .L8
                            .text:000001a2 .L11
                            .text:000001a4 .L12
                            .text:0000024a .L15
                            .text:0000024c .L16
                            .text:000002f2 .L19
                            .text:000002f4 .L20
                            .text:0000039a .L23
                            .text:0000039c .L24
                            .text:00000442 .L27
                            .text:00000444 .L28
                            .text:000004ea .L31
                            .text:000004ec .L32
                            .text:00000592 .L35
                            .text:00000594 .L36
                    .debug_abbrev:00000000 .Ldebug_abbrev0
                            .text:00000000 .Ltext0
                            .text:00000736 .Letext0
                      .debug_line:00000000 .Ldebug_line0
                       .debug_str:000000b6 .LASF0
                       .debug_str:00000030 .LASF1
                       .debug_str:0000017b .LASF2
                       .debug_str:0000019e .LASF3
                       .debug_str:000001a5 .LASF4
                            .text:00000000 .LFB0
                            .text:0000000a .LFE0
                            .text:0000000a .LFB1
                            .text:0000005e .LFE1
                       .debug_str:000001ac .LASF5
MPLAB XC16 ASSEMBLY Listing:   			page 217


                       .debug_str:00000095 .LASF6
                       .debug_str:00000163 .LASF7
                            .text:0000005e .LFB2
                            .text:000000b2 .LFE2
                            .text:000000b2 .LFB3
                            .text:00000108 .LFE3
                            .text:00000108 .LFB4
                            .text:0000015e .LFE4
                            .text:0000015e .LFB5
                            .text:000001b0 .LFE5
                            .text:000001b0 .LFB6
                            .text:00000202 .LFE6
                            .text:00000202 .LFB7
                            .text:00000258 .LFE7
                            .text:00000258 .LFB8
                            .text:000002ae .LFE8
                            .text:000002ae .LFB9
                            .text:00000300 .LFE9
                            .text:00000300 .LFB10
                            .text:00000352 .LFE10
                            .text:00000352 .LFB11
                            .text:000003a8 .LFE11
                            .text:000003a8 .LFB12
                            .text:000003fe .LFE12
                            .text:000003fe .LFB13
                            .text:00000450 .LFE13
                            .text:00000450 .LFB14
                            .text:000004a2 .LFE14
                            .text:000004a2 .LFB15
                            .text:000004f8 .LFE15
                            .text:000004f8 .LFB16
                            .text:0000054e .LFE16
                            .text:0000054e .LFB17
                            .text:000005a0 .LFE17
                            .text:000005a0 .LFB18
                            .text:000005f2 .LFE18
                            .text:000005f2 .LFB19
                            .text:000005fe .LFE19
                            .text:000005fe .LFB20
                            .text:0000060a .LFE20
                            .text:0000060a .LFB21
                            .text:00000616 .LFE21
                            .text:00000616 .LFB22
                            .text:00000622 .LFE22
                            .text:00000622 .LFB23
                            .text:0000062e .LFE23
                            .text:0000062e .LFB24
                            .text:0000063a .LFE24
                            .text:0000063a .LFB25
                            .text:00000646 .LFE25
                            .text:00000646 .LFB26
                            .text:00000652 .LFE26
                            .text:00000652 .LFB27
                            .text:0000065e .LFE27
                            .text:0000065e .LFB28
                            .text:0000066a .LFE28
                            .text:0000066a .LFB29
MPLAB XC16 ASSEMBLY Listing:   			page 218


                            .text:00000676 .LFE29
                            .text:00000676 .LFB30
                            .text:00000682 .LFE30
                            .text:00000682 .LFB31
                            .text:0000068e .LFE31
                            .text:0000068e .LFB32
                            .text:0000069a .LFE32
                            .text:0000069a .LFB33
                            .text:000006a6 .LFE33
                            .text:000006a6 .LFB34
                            .text:000006b2 .LFE34
                            .text:000006b2 .LFB35
                            .text:000006be .LFE35
                            .text:000006be .LFB36
                            .text:000006ca .LFE36
                            .text:000006ca .LFB37
                            .text:000006d6 .LFE37
                            .text:000006d6 .LFB38
                            .text:000006e2 .LFE38
                            .text:000006e2 .LFB39
                            .text:000006ee .LFE39
                            .text:000006ee .LFB40
                            .text:000006fa .LFE40
                            .text:000006fa .LFB41
                            .text:00000706 .LFE41
                            .text:00000706 .LFB42
                            .text:00000712 .LFE42
                            .text:00000712 .LFB43
                            .text:0000071e .LFE43
                            .text:0000071e .LFB44
                            .text:0000072a .LFE44
                            .text:0000072a .LFB45
                            .text:00000736 .LFE45
                        .isr.text:00000000 .LFB46
                        .isr.text:0000003a .LFE46
                        .isr.text:0000003a .LFB47
                        .isr.text:00000074 .LFE47
                        .isr.text:00000074 .LFB48
                        .isr.text:000000ae .LFE48
                        .isr.text:000000ae .LFB49
                        .isr.text:000000e8 .LFE49
                        .isr.text:000000e8 .LFB50
                        .isr.text:00000122 .LFE50
                        .isr.text:00000122 .LFB51
                        .isr.text:0000015c .LFE51
                        .isr.text:0000015c .LFB52
                        .isr.text:00000196 .LFE52
                        .isr.text:00000196 .LFB53
                        .isr.text:000001d0 .LFE53
                        .isr.text:000001d0 .LFB54
                        .isr.text:0000020a .LFE54
                       .debug_str:00000078 .LASF8
                       .debug_str:0000003f .LASF9
                       .debug_str:00000009 .LASF10
                       .debug_str:00000159 .LASF11
                       .debug_str:000000ec .LASF12
                       .debug_str:0000008b .LASF13
MPLAB XC16 ASSEMBLY Listing:   			page 219


                       .debug_str:00000049 .LASF14
                       .debug_str:00000013 .LASF15
                       .debug_str:00000194 .LASF16
                       .debug_str:0000005c .LASF17
                       .debug_str:00000000 .LASF18
                       .debug_str:00000053 .LASF19
                       .debug_str:000000ad .LASF20
                       .debug_str:00000036 .LASF21
                       .debug_str:00000082 .LASF22
                       .debug_str:00000123 .LASF23
                       .debug_str:0000018b .LASF24
                       .debug_str:00000027 .LASF25
                       .debug_str:0000006f .LASF26
                       .debug_str:000000e3 .LASF27
                       .debug_str:000000cb .LASF28
                       .debug_str:00000172 .LASF29
                       .debug_str:00000181 .LASF30
                       .debug_str:0000001d .LASF31
                       .debug_str:00000065 .LASF32
                       .debug_str:00000105 .LASF33
                       .debug_str:000000bc .LASF34
                       .debug_str:0000012c .LASF35
                       .debug_str:00000114 .LASF36
                       .debug_str:0000009e .LASF37
                       .debug_str:000000f6 .LASF38
                       .debug_str:000000d4 .LASF39
                       .debug_str:0000013b .LASF40
                       .debug_str:0000014a .LASF41
                     .debug_frame:00000000 .Lframe0
                      .debug_info:00000000 .Ldebug_info0

UNDEFINED SYMBOLS
CORCON
_T1CONbits
_TMR1
_PR1
_IPC0bits
_IFS0bits
_IEC0bits
_T2CONbits
_TMR2
_PR2
_IPC1bits
_T3CONbits
_TMR3
_PR3
_IPC2bits
_T4CONbits
_TMR4
_PR4
_IPC6bits
_IFS1bits
_IEC1bits
_T5CONbits
_TMR5
_PR5
_IPC7bits
MPLAB XC16 ASSEMBLY Listing:   			page 220


_T6CONbits
_TMR6
_PR6
_IPC11bits
_IFS2bits
_IEC2bits
_T7CONbits
_TMR7
_PR7
_IPC12bits
_IFS3bits
_IEC3bits
_T8CONbits
_TMR8
_PR8
_T9CONbits
_TMR9
_PR9
_IPC13bits
_RCOUNT
_DSRPAG
_DSWPAG
__const_psvpage
_SR

EQUATE SYMBOLS
/home/user/Documents/FST/Programming/project_sub-zero/lib/lib_pic33e/timer.c = 0x0
                  __33EP256MU806 = 0x1
                      __dsPIC33E = 0x1
                       __HAS_DSP = 0x1
                     __HAS_DMAV2 = 0x1
                 __HAS_CODEGUARD = 0x1
              __HAS_PMP_ENHANCED = 0x1
                       __HAS_EDS = 0x1
                        __HAS_EP = 0x1
          __TARGET_DIVIDE_CYCLES = 0x11
                        __C30ELF = 0x1
                        ___PA___ = 0x0
                        ___BP___ = 0x0
            __ext_attr_.isr.text = 0x40
