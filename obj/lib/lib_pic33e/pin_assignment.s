MPLAB XC16 ASSEMBLY Listing:   			page 1


   1              	.file "/home/user/Documents/FST/Programming/project_sub-zero/lib/lib_pic33e/pin_assignment.c"
   2              	.section .debug_abbrev,info
   3                 	.Ldebug_abbrev0:
   4                 	.section .debug_info,info
   5                 	.Ldebug_info0:
   6                 	.section .debug_line,info
   7                 	.Ldebug_line0:
   8 0000 DF 00 00 00 	.section .text,code
   8      02 00 C4 00 
   8      00 00 01 01 
   8      FB 0E 0A 00 
   8      01 01 01 01 
   8      00 00 00 01 
   8      6C 69 62 2F 
   8      6C 69 62 5F 
   8      70 69 63 33 
   9              	.Ltext0:
  10              	
  11              	.pushsection .user_init,code,keep
  12 000000  00 00 07 	rcall _config_pin_assignment
  13              	.popsection
  14              	
  15              	.section .text,code
  16              	.align 2
  17              	.global _config_pin_assignment
  18              	.type _config_pin_assignment,@function
  19              	_config_pin_assignment:
  20              	.LFB0:
  21              	.file 1 "lib/lib_pic33e/pin_assignment.c"
   1:lib/lib_pic33e/pin_assignment.c **** #ifndef NOPINASSIGNMENT
   2:lib/lib_pic33e/pin_assignment.c **** 
   3:lib/lib_pic33e/pin_assignment.c **** #include <xc.h>
   4:lib/lib_pic33e/pin_assignment.c **** #include "pps.h"
   5:lib/lib_pic33e/pin_assignment.c **** 
   6:lib/lib_pic33e/pin_assignment.c **** /*
   7:lib/lib_pic33e/pin_assignment.c ****  * See section 10. IO/Ports, subsection 4 of the family reference.
   8:lib/lib_pic33e/pin_assignment.c ****  * And section 11.4 IO/Ports of the datasheet
   9:lib/lib_pic33e/pin_assignment.c ****  */
  10:lib/lib_pic33e/pin_assignment.c **** 
  11:lib/lib_pic33e/pin_assignment.c **** void __attribute__((user_init)) config_pin_assignment(void)
  12:lib/lib_pic33e/pin_assignment.c **** {
  22              	.loc 1 12 0
  23              	.set ___PA___,1
  24 000000  00 00 FA 	lnk #0
  25              	.LCFI0:
  13:lib/lib_pic33e/pin_assignment.c ****     /*
  14:lib/lib_pic33e/pin_assignment.c ****      * Set all ANSELx registers to '0' so the digital ports are enabled by
  15:lib/lib_pic33e/pin_assignment.c ****      * default. Analog modules should set the needed ANSEL bits to '1'.
  16:lib/lib_pic33e/pin_assignment.c ****      */
  17:lib/lib_pic33e/pin_assignment.c ****     ANSELB = 0;
  26              	.loc 1 17 0
  27 000002  00 20 EF 	clr _ANSELB
  18:lib/lib_pic33e/pin_assignment.c ****     ANSELC = 0;
  28              	.loc 1 18 0
  29 000004  00 20 EF 	clr _ANSELC
  19:lib/lib_pic33e/pin_assignment.c ****     ANSELD = 0;
  30              	.loc 1 19 0
MPLAB XC16 ASSEMBLY Listing:   			page 2


  31 000006  00 20 EF 	clr _ANSELD
  20:lib/lib_pic33e/pin_assignment.c ****     ANSELE = 0;
  32              	.loc 1 20 0
  33 000008  00 20 EF 	clr _ANSELE
  21:lib/lib_pic33e/pin_assignment.c ****     ANSELG = 0;
  34              	.loc 1 21 0
  35 00000a  00 20 EF 	clr _ANSELG
  22:lib/lib_pic33e/pin_assignment.c **** 
  23:lib/lib_pic33e/pin_assignment.c ****     return;
  24:lib/lib_pic33e/pin_assignment.c **** }
  36              	.loc 1 24 0
  37 00000c  8E 07 78 	mov w14,w15
  38 00000e  4F 07 78 	mov [--w15],w14
  39 000010  00 40 A9 	bclr CORCON,#2
  40 000012  00 00 06 	return 
  41              	.set ___PA___,0
  42              	.LFE0:
  43              	.size _config_pin_assignment,.-_config_pin_assignment
  44              	.section .debug_frame,info
  45                 	.Lframe0:
  46 0000 10 00 00 00 	.4byte .LECIE0-.LSCIE0
  47                 	.LSCIE0:
  48 0004 FF FF FF FF 	.4byte 0xffffffff
  49 0008 01          	.byte 0x1
  50 0009 00          	.byte 0
  51 000a 01          	.uleb128 0x1
  52 000b 02          	.sleb128 2
  53 000c 25          	.byte 0x25
  54 000d 12          	.byte 0x12
  55 000e 0F          	.uleb128 0xf
  56 000f 7E          	.sleb128 -2
  57 0010 09          	.byte 0x9
  58 0011 25          	.uleb128 0x25
  59 0012 0F          	.uleb128 0xf
  60 0013 00          	.align 4
  61                 	.LECIE0:
  62                 	.LSFDE0:
  63 0014 18 00 00 00 	.4byte .LEFDE0-.LASFDE0
  64                 	.LASFDE0:
  65 0018 00 00 00 00 	.4byte .Lframe0
  66 001c 00 00 00 00 	.4byte .LFB0
  67 0020 14 00 00 00 	.4byte .LFE0-.LFB0
  68 0024 04          	.byte 0x4
  69 0025 02 00 00 00 	.4byte .LCFI0-.LFB0
  70 0029 13          	.byte 0x13
  71 002a 7D          	.sleb128 -3
  72 002b 0D          	.byte 0xd
  73 002c 0E          	.uleb128 0xe
  74 002d 8E          	.byte 0x8e
  75 002e 02          	.uleb128 0x2
  76 002f 00          	.align 4
  77                 	.LEFDE0:
  78                 	.section .text,code
  79              	.Letext0:
  80              	.file 2 "/opt/microchip/xc16/v1.36/bin/bin/../../support/dsPIC33E/h/p33EP256MU806.h"
  81              	.file 3 "/opt/microchip/xc16/v1.36/bin/bin/../../include/lega-c/stdint.h"
  82              	.section .debug_info,info
MPLAB XC16 ASSEMBLY Listing:   			page 3


  83 0000 13 02 00 00 	.4byte 0x213
  84 0004 02 00       	.2byte 0x2
  85 0006 00 00 00 00 	.4byte .Ldebug_abbrev0
  86 000a 04          	.byte 0x4
  87 000b 01          	.uleb128 0x1
  88 000c 47 4E 55 20 	.asciz "GNU C 4.5.1 (XC16, Microchip v1.36) (A) Build date: Dec  5 2018"
  88      43 20 34 2E 
  88      35 2E 31 20 
  88      28 58 43 31 
  88      36 2C 20 4D 
  88      69 63 72 6F 
  88      63 68 69 70 
  88      20 76 31 2E 
  88      33 36 29 20 
  89 004c 01          	.byte 0x1
  90 004d 6C 69 62 2F 	.asciz "lib/lib_pic33e/pin_assignment.c"
  90      6C 69 62 5F 
  90      70 69 63 33 
  90      33 65 2F 70 
  90      69 6E 5F 61 
  90      73 73 69 67 
  90      6E 6D 65 6E 
  90      74 2E 63 00 
  91 006d 2F 68 6F 6D 	.asciz "/home/user/Documents/FST/Programming/project_sub-zero"
  91      65 2F 75 73 
  91      65 72 2F 44 
  91      6F 63 75 6D 
  91      65 6E 74 73 
  91      2F 46 53 54 
  91      2F 50 72 6F 
  91      67 72 61 6D 
  91      6D 69 6E 67 
  92 00a3 00 00 00 00 	.4byte .Ltext0
  93 00a7 00 00 00 00 	.4byte .Letext0
  94 00ab 00 00 00 00 	.4byte .Ldebug_line0
  95 00af 02          	.uleb128 0x2
  96 00b0 01          	.byte 0x1
  97 00b1 06          	.byte 0x6
  98 00b2 73 69 67 6E 	.asciz "signed char"
  98      65 64 20 63 
  98      68 61 72 00 
  99 00be 02          	.uleb128 0x2
 100 00bf 02          	.byte 0x2
 101 00c0 05          	.byte 0x5
 102 00c1 69 6E 74 00 	.asciz "int"
 103 00c5 02          	.uleb128 0x2
 104 00c6 04          	.byte 0x4
 105 00c7 05          	.byte 0x5
 106 00c8 6C 6F 6E 67 	.asciz "long int"
 106      20 69 6E 74 
 106      00 
 107 00d1 02          	.uleb128 0x2
 108 00d2 08          	.byte 0x8
 109 00d3 05          	.byte 0x5
 110 00d4 6C 6F 6E 67 	.asciz "long long int"
 110      20 6C 6F 6E 
 110      67 20 69 6E 
MPLAB XC16 ASSEMBLY Listing:   			page 4


 110      74 00 
 111 00e2 02          	.uleb128 0x2
 112 00e3 01          	.byte 0x1
 113 00e4 08          	.byte 0x8
 114 00e5 75 6E 73 69 	.asciz "unsigned char"
 114      67 6E 65 64 
 114      20 63 68 61 
 114      72 00 
 115 00f3 03          	.uleb128 0x3
 116 00f4 75 69 6E 74 	.asciz "uint16_t"
 116      31 36 5F 74 
 116      00 
 117 00fd 03          	.byte 0x3
 118 00fe 31          	.byte 0x31
 119 00ff 03 01 00 00 	.4byte 0x103
 120 0103 02          	.uleb128 0x2
 121 0104 02          	.byte 0x2
 122 0105 07          	.byte 0x7
 123 0106 75 6E 73 69 	.asciz "unsigned int"
 123      67 6E 65 64 
 123      20 69 6E 74 
 123      00 
 124 0113 02          	.uleb128 0x2
 125 0114 04          	.byte 0x4
 126 0115 07          	.byte 0x7
 127 0116 6C 6F 6E 67 	.asciz "long unsigned int"
 127      20 75 6E 73 
 127      69 67 6E 65 
 127      64 20 69 6E 
 127      74 00 
 128 0128 02          	.uleb128 0x2
 129 0129 08          	.byte 0x8
 130 012a 07          	.byte 0x7
 131 012b 6C 6F 6E 67 	.asciz "long long unsigned int"
 131      20 6C 6F 6E 
 131      67 20 75 6E 
 131      73 69 67 6E 
 131      65 64 20 69 
 131      6E 74 00 
 132 0142 04          	.uleb128 0x4
 133 0143 01          	.byte 0x1
 134 0144 63 6F 6E 66 	.asciz "config_pin_assignment"
 134      69 67 5F 70 
 134      69 6E 5F 61 
 134      73 73 69 67 
 134      6E 6D 65 6E 
 134      74 00 
 135 015a 01          	.byte 0x1
 136 015b 0B          	.byte 0xb
 137 015c 01          	.byte 0x1
 138 015d 00 00 00 00 	.4byte .LFB0
 139 0161 00 00 00 00 	.4byte .LFE0
 140 0165 01          	.byte 0x1
 141 0166 5E          	.byte 0x5e
 142 0167 05          	.uleb128 0x5
 143 0168 41 4E 53 45 	.asciz "ANSELB"
 143      4C 42 00 
MPLAB XC16 ASSEMBLY Listing:   			page 5


 144 016f 02          	.byte 0x2
 145 0170 BC 3A       	.2byte 0x3abc
 146 0172 78 01 00 00 	.4byte 0x178
 147 0176 01          	.byte 0x1
 148 0177 01          	.byte 0x1
 149 0178 06          	.uleb128 0x6
 150 0179 F3 00 00 00 	.4byte 0xf3
 151 017d 05          	.uleb128 0x5
 152 017e 41 4E 53 45 	.asciz "ANSELC"
 152      4C 43 00 
 153 0185 02          	.byte 0x2
 154 0186 14 3B       	.2byte 0x3b14
 155 0188 78 01 00 00 	.4byte 0x178
 156 018c 01          	.byte 0x1
 157 018d 01          	.byte 0x1
 158 018e 05          	.uleb128 0x5
 159 018f 41 4E 53 45 	.asciz "ANSELD"
 159      4C 44 00 
 160 0196 02          	.byte 0x2
 161 0197 9A 3B       	.2byte 0x3b9a
 162 0199 78 01 00 00 	.4byte 0x178
 163 019d 01          	.byte 0x1
 164 019e 01          	.byte 0x1
 165 019f 05          	.uleb128 0x5
 166 01a0 41 4E 53 45 	.asciz "ANSELE"
 166      4C 45 00 
 167 01a7 02          	.byte 0x2
 168 01a8 F7 3B       	.2byte 0x3bf7
 169 01aa 78 01 00 00 	.4byte 0x178
 170 01ae 01          	.byte 0x1
 171 01af 01          	.byte 0x1
 172 01b0 05          	.uleb128 0x5
 173 01b1 41 4E 53 45 	.asciz "ANSELG"
 173      4C 47 00 
 174 01b8 02          	.byte 0x2
 175 01b9 A3 3C       	.2byte 0x3ca3
 176 01bb 78 01 00 00 	.4byte 0x178
 177 01bf 01          	.byte 0x1
 178 01c0 01          	.byte 0x1
 179 01c1 05          	.uleb128 0x5
 180 01c2 41 4E 53 45 	.asciz "ANSELB"
 180      4C 42 00 
 181 01c9 02          	.byte 0x2
 182 01ca BC 3A       	.2byte 0x3abc
 183 01cc 78 01 00 00 	.4byte 0x178
 184 01d0 01          	.byte 0x1
 185 01d1 01          	.byte 0x1
 186 01d2 05          	.uleb128 0x5
 187 01d3 41 4E 53 45 	.asciz "ANSELC"
 187      4C 43 00 
 188 01da 02          	.byte 0x2
 189 01db 14 3B       	.2byte 0x3b14
 190 01dd 78 01 00 00 	.4byte 0x178
 191 01e1 01          	.byte 0x1
 192 01e2 01          	.byte 0x1
 193 01e3 05          	.uleb128 0x5
 194 01e4 41 4E 53 45 	.asciz "ANSELD"
MPLAB XC16 ASSEMBLY Listing:   			page 6


 194      4C 44 00 
 195 01eb 02          	.byte 0x2
 196 01ec 9A 3B       	.2byte 0x3b9a
 197 01ee 78 01 00 00 	.4byte 0x178
 198 01f2 01          	.byte 0x1
 199 01f3 01          	.byte 0x1
 200 01f4 05          	.uleb128 0x5
 201 01f5 41 4E 53 45 	.asciz "ANSELE"
 201      4C 45 00 
 202 01fc 02          	.byte 0x2
 203 01fd F7 3B       	.2byte 0x3bf7
 204 01ff 78 01 00 00 	.4byte 0x178
 205 0203 01          	.byte 0x1
 206 0204 01          	.byte 0x1
 207 0205 05          	.uleb128 0x5
 208 0206 41 4E 53 45 	.asciz "ANSELG"
 208      4C 47 00 
 209 020d 02          	.byte 0x2
 210 020e A3 3C       	.2byte 0x3ca3
 211 0210 78 01 00 00 	.4byte 0x178
 212 0214 01          	.byte 0x1
 213 0215 01          	.byte 0x1
 214 0216 00          	.byte 0x0
 215                 	.section .debug_abbrev,info
 216 0000 01          	.uleb128 0x1
 217 0001 11          	.uleb128 0x11
 218 0002 01          	.byte 0x1
 219 0003 25          	.uleb128 0x25
 220 0004 08          	.uleb128 0x8
 221 0005 13          	.uleb128 0x13
 222 0006 0B          	.uleb128 0xb
 223 0007 03          	.uleb128 0x3
 224 0008 08          	.uleb128 0x8
 225 0009 1B          	.uleb128 0x1b
 226 000a 08          	.uleb128 0x8
 227 000b 11          	.uleb128 0x11
 228 000c 01          	.uleb128 0x1
 229 000d 12          	.uleb128 0x12
 230 000e 01          	.uleb128 0x1
 231 000f 10          	.uleb128 0x10
 232 0010 06          	.uleb128 0x6
 233 0011 00          	.byte 0x0
 234 0012 00          	.byte 0x0
 235 0013 02          	.uleb128 0x2
 236 0014 24          	.uleb128 0x24
 237 0015 00          	.byte 0x0
 238 0016 0B          	.uleb128 0xb
 239 0017 0B          	.uleb128 0xb
 240 0018 3E          	.uleb128 0x3e
 241 0019 0B          	.uleb128 0xb
 242 001a 03          	.uleb128 0x3
 243 001b 08          	.uleb128 0x8
 244 001c 00          	.byte 0x0
 245 001d 00          	.byte 0x0
 246 001e 03          	.uleb128 0x3
 247 001f 16          	.uleb128 0x16
 248 0020 00          	.byte 0x0
MPLAB XC16 ASSEMBLY Listing:   			page 7


 249 0021 03          	.uleb128 0x3
 250 0022 08          	.uleb128 0x8
 251 0023 3A          	.uleb128 0x3a
 252 0024 0B          	.uleb128 0xb
 253 0025 3B          	.uleb128 0x3b
 254 0026 0B          	.uleb128 0xb
 255 0027 49          	.uleb128 0x49
 256 0028 13          	.uleb128 0x13
 257 0029 00          	.byte 0x0
 258 002a 00          	.byte 0x0
 259 002b 04          	.uleb128 0x4
 260 002c 2E          	.uleb128 0x2e
 261 002d 00          	.byte 0x0
 262 002e 3F          	.uleb128 0x3f
 263 002f 0C          	.uleb128 0xc
 264 0030 03          	.uleb128 0x3
 265 0031 08          	.uleb128 0x8
 266 0032 3A          	.uleb128 0x3a
 267 0033 0B          	.uleb128 0xb
 268 0034 3B          	.uleb128 0x3b
 269 0035 0B          	.uleb128 0xb
 270 0036 27          	.uleb128 0x27
 271 0037 0C          	.uleb128 0xc
 272 0038 11          	.uleb128 0x11
 273 0039 01          	.uleb128 0x1
 274 003a 12          	.uleb128 0x12
 275 003b 01          	.uleb128 0x1
 276 003c 40          	.uleb128 0x40
 277 003d 0A          	.uleb128 0xa
 278 003e 00          	.byte 0x0
 279 003f 00          	.byte 0x0
 280 0040 05          	.uleb128 0x5
 281 0041 34          	.uleb128 0x34
 282 0042 00          	.byte 0x0
 283 0043 03          	.uleb128 0x3
 284 0044 08          	.uleb128 0x8
 285 0045 3A          	.uleb128 0x3a
 286 0046 0B          	.uleb128 0xb
 287 0047 3B          	.uleb128 0x3b
 288 0048 05          	.uleb128 0x5
 289 0049 49          	.uleb128 0x49
 290 004a 13          	.uleb128 0x13
 291 004b 3F          	.uleb128 0x3f
 292 004c 0C          	.uleb128 0xc
 293 004d 3C          	.uleb128 0x3c
 294 004e 0C          	.uleb128 0xc
 295 004f 00          	.byte 0x0
 296 0050 00          	.byte 0x0
 297 0051 06          	.uleb128 0x6
 298 0052 35          	.uleb128 0x35
 299 0053 00          	.byte 0x0
 300 0054 49          	.uleb128 0x49
 301 0055 13          	.uleb128 0x13
 302 0056 00          	.byte 0x0
 303 0057 00          	.byte 0x0
 304 0058 00          	.byte 0x0
 305                 	.section .debug_pubnames,info
MPLAB XC16 ASSEMBLY Listing:   			page 8


 306 0000 28 00 00 00 	.4byte 0x28
 307 0004 02 00       	.2byte 0x2
 308 0006 00 00 00 00 	.4byte .Ldebug_info0
 309 000a 17 02 00 00 	.4byte 0x217
 310 000e 42 01 00 00 	.4byte 0x142
 311 0012 63 6F 6E 66 	.asciz "config_pin_assignment"
 311      69 67 5F 70 
 311      69 6E 5F 61 
 311      73 73 69 67 
 311      6E 6D 65 6E 
 311      74 00 
 312 0028 00 00 00 00 	.4byte 0x0
 313                 	.section .debug_pubtypes,info
 314 0000 1B 00 00 00 	.4byte 0x1b
 315 0004 02 00       	.2byte 0x2
 316 0006 00 00 00 00 	.4byte .Ldebug_info0
 317 000a 17 02 00 00 	.4byte 0x217
 318 000e F3 00 00 00 	.4byte 0xf3
 319 0012 75 69 6E 74 	.asciz "uint16_t"
 319      31 36 5F 74 
 319      00 
 320 001b 00 00 00 00 	.4byte 0x0
 321                 	.section .debug_aranges,info
 322 0000 14 00 00 00 	.4byte 0x14
 323 0004 02 00       	.2byte 0x2
 324 0006 00 00 00 00 	.4byte .Ldebug_info0
 325 000a 04          	.byte 0x4
 326 000b 00          	.byte 0x0
 327 000c 00 00       	.2byte 0x0
 328 000e 00 00       	.2byte 0x0
 329 0010 00 00 00 00 	.4byte 0x0
 330 0014 00 00 00 00 	.4byte 0x0
 331                 	.section .debug_str,info
 332                 	.section .text,code
 333              	
 334              	
 335              	
 336              	.section __c30_info,info,bss
 337                 	__psv_trap_errata:
 338                 	
 339                 	.section __c30_signature,info,data
 340 0000 01 00       	.word 0x0001
 341 0002 00 00       	.word 0x0000
 342 0004 00 00       	.word 0x0000
 343                 	
 344                 	
 345                 	
 346                 	.set ___PA___,0
 347                 	.end
MPLAB XC16 ASSEMBLY Listing:   			page 9


DEFINED SYMBOLS
                            *ABS*:00000000 /home/user/Documents/FST/Programming/project_sub-zero/lib/lib_pic33e/pin_assignment.c
                            *ABS*:00000001 __33EP256MU806
                            *ABS*:00000001 __dsPIC33E
                            *ABS*:00000001 __HAS_DSP
                            *ABS*:00000001 __HAS_DMAV2
                            *ABS*:00000001 __HAS_CODEGUARD
                            *ABS*:00000001 __HAS_PMP_ENHANCED
                            *ABS*:00000001 __HAS_EDS
                            *ABS*:00000001 __HAS_EP
                            *ABS*:00000011 __TARGET_DIVIDE_CYCLES
                            *ABS*:00000001 __C30ELF
                            *ABS*:00000040 __ext_attr_.user_init
    {standard input}:19     .text:00000000 _config_pin_assignment
    {standard input}:23     *ABS*:00000000 ___PA___
    {standard input}:337    __c30_info:00000000 __psv_trap_errata
    {standard input}:24     .text:00000000 .L0
                    .debug_abbrev:00000000 .Ldebug_abbrev0
                            .text:00000000 .Ltext0
                            .text:00000014 .Letext0
                      .debug_line:00000000 .Ldebug_line0
                            .text:00000000 .LFB0
                            .text:00000014 .LFE0
                     .debug_frame:00000000 .Lframe0
                      .debug_info:00000000 .Ldebug_info0

UNDEFINED SYMBOLS
_ANSELB
_ANSELC
_ANSELD
_ANSELE
_ANSELG
CORCON

EQUATE SYMBOLS
/home/user/Documents/FST/Programming/project_sub-zero/lib/lib_pic33e/pin_assignment.c = 0x0
                  __33EP256MU806 = 0x1
                      __dsPIC33E = 0x1
                       __HAS_DSP = 0x1
                     __HAS_DMAV2 = 0x1
                 __HAS_CODEGUARD = 0x1
              __HAS_PMP_ENHANCED = 0x1
                       __HAS_EDS = 0x1
                        __HAS_EP = 0x1
          __TARGET_DIVIDE_CYCLES = 0x11
                        __C30ELF = 0x1
           __ext_attr_.user_init = 0x40
                        ___PA___ = 0x0
