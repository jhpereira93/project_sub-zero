MPLAB XC16 ASSEMBLY Listing:   			page 1


   1              	.file "/home/user/Documents/FST/Programming/project_sub-zero/lib/lib_pic33e/utils.c"
   2              	.section .debug_abbrev,info
   3                 	.Ldebug_abbrev0:
   4                 	.section .debug_info,info
   5                 	.Ldebug_info0:
   6                 	.section .debug_line,info
   7                 	.Ldebug_line0:
   8 0000 87 00 00 00 	.section .text,code
   8      02 00 6D 00 
   8      00 00 01 01 
   8      FB 0E 0A 00 
   8      01 01 01 01 
   8      00 00 00 01 
   8      6C 69 62 2F 
   8      6C 69 62 5F 
   8      70 69 63 33 
   9              	.Ltext0:
  10              	.section .text,code
  11              	.align 2
  12              	.global _popcount
  13              	.type _popcount,@function
  14              	_popcount:
  15              	.LFB0:
  16              	.file 1 "lib/lib_pic33e/utils.c"
   1:lib/lib_pic33e/utils.c **** #include <stdint.h>
   2:lib/lib_pic33e/utils.c **** 
   3:lib/lib_pic33e/utils.c **** uint16_t popcount(uint16_t x)
   4:lib/lib_pic33e/utils.c **** {
  17              	.loc 1 4 0
  18              	.set ___PA___,1
  19 000000  04 00 FA 	lnk #4
  20              	.LCFI0:
   5:lib/lib_pic33e/utils.c **** uint16_t c = 0;
  21              	.loc 1 5 0
  22 000002  80 00 EB 	clr w1
  23              	.loc 1 4 0
  24 000004  10 07 98 	mov w0,[w14+2]
  25              	.loc 1 5 0
  26 000006  01 0F 78 	mov w1,[w14]
   6:lib/lib_pic33e/utils.c **** for (; x > 0; x &= x -1) c++;
  27              	.loc 1 6 0
  28 000008  00 00 37 	bra .L2
  29              	.L3:
  30 00000a  1E 0F E8 	inc [w14],[w14]
  31 00000c  1E 00 90 	mov [w14+2],w0
  32 00000e  9E 00 90 	mov [w14+2],w1
  33 000010  00 00 E9 	dec w0,w0
  34 000012  00 80 60 	and w1,w0,w0
  35 000014  10 07 98 	mov w0,[w14+2]
  36              	.L2:
  37 000016  1E 00 90 	mov [w14+2],w0
  38 000018  00 00 E0 	cp0 w0
  39              	.set ___BP___,0
  40 00001a  00 00 3A 	bra nz,.L3
   7:lib/lib_pic33e/utils.c **** return c;
  41              	.loc 1 7 0
  42 00001c  1E 00 78 	mov [w14],w0
MPLAB XC16 ASSEMBLY Listing:   			page 2


   8:lib/lib_pic33e/utils.c **** }
  43              	.loc 1 8 0
  44 00001e  8E 07 78 	mov w14,w15
  45 000020  4F 07 78 	mov [--w15],w14
  46 000022  00 40 A9 	bclr CORCON,#2
  47 000024  00 00 06 	return 
  48              	.set ___PA___,0
  49              	.LFE0:
  50              	.size _popcount,.-_popcount
  51              	.section .debug_frame,info
  52                 	.Lframe0:
  53 0000 10 00 00 00 	.4byte .LECIE0-.LSCIE0
  54                 	.LSCIE0:
  55 0004 FF FF FF FF 	.4byte 0xffffffff
  56 0008 01          	.byte 0x1
  57 0009 00          	.byte 0
  58 000a 01          	.uleb128 0x1
  59 000b 02          	.sleb128 2
  60 000c 25          	.byte 0x25
  61 000d 12          	.byte 0x12
  62 000e 0F          	.uleb128 0xf
  63 000f 7E          	.sleb128 -2
  64 0010 09          	.byte 0x9
  65 0011 25          	.uleb128 0x25
  66 0012 0F          	.uleb128 0xf
  67 0013 00          	.align 4
  68                 	.LECIE0:
  69                 	.LSFDE0:
  70 0014 18 00 00 00 	.4byte .LEFDE0-.LASFDE0
  71                 	.LASFDE0:
  72 0018 00 00 00 00 	.4byte .Lframe0
  73 001c 00 00 00 00 	.4byte .LFB0
  74 0020 26 00 00 00 	.4byte .LFE0-.LFB0
  75 0024 04          	.byte 0x4
  76 0025 02 00 00 00 	.4byte .LCFI0-.LFB0
  77 0029 13          	.byte 0x13
  78 002a 7D          	.sleb128 -3
  79 002b 0D          	.byte 0xd
  80 002c 0E          	.uleb128 0xe
  81 002d 8E          	.byte 0x8e
  82 002e 02          	.uleb128 0x2
  83 002f 00          	.align 4
  84                 	.LEFDE0:
  85                 	.section .text,code
  86              	.Letext0:
  87              	.file 2 "/opt/microchip/xc16/v1.36/bin/bin/../../include/lega-c/stdint.h"
  88              	.section .debug_info,info
  89 0000 6B 01 00 00 	.4byte 0x16b
  90 0004 02 00       	.2byte 0x2
  91 0006 00 00 00 00 	.4byte .Ldebug_abbrev0
  92 000a 04          	.byte 0x4
  93 000b 01          	.uleb128 0x1
  94 000c 47 4E 55 20 	.asciz "GNU C 4.5.1 (XC16, Microchip v1.36) (A) Build date: Dec  5 2018"
  94      43 20 34 2E 
  94      35 2E 31 20 
  94      28 58 43 31 
  94      36 2C 20 4D 
MPLAB XC16 ASSEMBLY Listing:   			page 3


  94      69 63 72 6F 
  94      63 68 69 70 
  94      20 76 31 2E 
  94      33 36 29 20 
  95 004c 01          	.byte 0x1
  96 004d 6C 69 62 2F 	.asciz "lib/lib_pic33e/utils.c"
  96      6C 69 62 5F 
  96      70 69 63 33 
  96      33 65 2F 75 
  96      74 69 6C 73 
  96      2E 63 00 
  97 0064 2F 68 6F 6D 	.asciz "/home/user/Documents/FST/Programming/project_sub-zero"
  97      65 2F 75 73 
  97      65 72 2F 44 
  97      6F 63 75 6D 
  97      65 6E 74 73 
  97      2F 46 53 54 
  97      2F 50 72 6F 
  97      67 72 61 6D 
  97      6D 69 6E 67 
  98 009a 00 00 00 00 	.4byte .Ltext0
  99 009e 00 00 00 00 	.4byte .Letext0
 100 00a2 00 00 00 00 	.4byte .Ldebug_line0
 101 00a6 02          	.uleb128 0x2
 102 00a7 01          	.byte 0x1
 103 00a8 06          	.byte 0x6
 104 00a9 73 69 67 6E 	.asciz "signed char"
 104      65 64 20 63 
 104      68 61 72 00 
 105 00b5 02          	.uleb128 0x2
 106 00b6 02          	.byte 0x2
 107 00b7 05          	.byte 0x5
 108 00b8 69 6E 74 00 	.asciz "int"
 109 00bc 02          	.uleb128 0x2
 110 00bd 04          	.byte 0x4
 111 00be 05          	.byte 0x5
 112 00bf 6C 6F 6E 67 	.asciz "long int"
 112      20 69 6E 74 
 112      00 
 113 00c8 02          	.uleb128 0x2
 114 00c9 08          	.byte 0x8
 115 00ca 05          	.byte 0x5
 116 00cb 6C 6F 6E 67 	.asciz "long long int"
 116      20 6C 6F 6E 
 116      67 20 69 6E 
 116      74 00 
 117 00d9 02          	.uleb128 0x2
 118 00da 01          	.byte 0x1
 119 00db 08          	.byte 0x8
 120 00dc 75 6E 73 69 	.asciz "unsigned char"
 120      67 6E 65 64 
 120      20 63 68 61 
 120      72 00 
 121 00ea 03          	.uleb128 0x3
 122 00eb 75 69 6E 74 	.asciz "uint16_t"
 122      31 36 5F 74 
 122      00 
MPLAB XC16 ASSEMBLY Listing:   			page 4


 123 00f4 02          	.byte 0x2
 124 00f5 31          	.byte 0x31
 125 00f6 FA 00 00 00 	.4byte 0xfa
 126 00fa 02          	.uleb128 0x2
 127 00fb 02          	.byte 0x2
 128 00fc 07          	.byte 0x7
 129 00fd 75 6E 73 69 	.asciz "unsigned int"
 129      67 6E 65 64 
 129      20 69 6E 74 
 129      00 
 130 010a 02          	.uleb128 0x2
 131 010b 04          	.byte 0x4
 132 010c 07          	.byte 0x7
 133 010d 6C 6F 6E 67 	.asciz "long unsigned int"
 133      20 75 6E 73 
 133      69 67 6E 65 
 133      64 20 69 6E 
 133      74 00 
 134 011f 02          	.uleb128 0x2
 135 0120 08          	.byte 0x8
 136 0121 07          	.byte 0x7
 137 0122 6C 6F 6E 67 	.asciz "long long unsigned int"
 137      20 6C 6F 6E 
 137      67 20 75 6E 
 137      73 69 67 6E 
 137      65 64 20 69 
 137      6E 74 00 
 138 0139 04          	.uleb128 0x4
 139 013a 01          	.byte 0x1
 140 013b 70 6F 70 63 	.asciz "popcount"
 140      6F 75 6E 74 
 140      00 
 141 0144 01          	.byte 0x1
 142 0145 03          	.byte 0x3
 143 0146 01          	.byte 0x1
 144 0147 EA 00 00 00 	.4byte 0xea
 145 014b 00 00 00 00 	.4byte .LFB0
 146 014f 00 00 00 00 	.4byte .LFE0
 147 0153 01          	.byte 0x1
 148 0154 5E          	.byte 0x5e
 149 0155 05          	.uleb128 0x5
 150 0156 78 00       	.asciz "x"
 151 0158 01          	.byte 0x1
 152 0159 03          	.byte 0x3
 153 015a EA 00 00 00 	.4byte 0xea
 154 015e 02          	.byte 0x2
 155 015f 7E          	.byte 0x7e
 156 0160 02          	.sleb128 2
 157 0161 06          	.uleb128 0x6
 158 0162 63 00       	.asciz "c"
 159 0164 01          	.byte 0x1
 160 0165 05          	.byte 0x5
 161 0166 EA 00 00 00 	.4byte 0xea
 162 016a 02          	.byte 0x2
 163 016b 7E          	.byte 0x7e
 164 016c 00          	.sleb128 0
 165 016d 00          	.byte 0x0
MPLAB XC16 ASSEMBLY Listing:   			page 5


 166 016e 00          	.byte 0x0
 167                 	.section .debug_abbrev,info
 168 0000 01          	.uleb128 0x1
 169 0001 11          	.uleb128 0x11
 170 0002 01          	.byte 0x1
 171 0003 25          	.uleb128 0x25
 172 0004 08          	.uleb128 0x8
 173 0005 13          	.uleb128 0x13
 174 0006 0B          	.uleb128 0xb
 175 0007 03          	.uleb128 0x3
 176 0008 08          	.uleb128 0x8
 177 0009 1B          	.uleb128 0x1b
 178 000a 08          	.uleb128 0x8
 179 000b 11          	.uleb128 0x11
 180 000c 01          	.uleb128 0x1
 181 000d 12          	.uleb128 0x12
 182 000e 01          	.uleb128 0x1
 183 000f 10          	.uleb128 0x10
 184 0010 06          	.uleb128 0x6
 185 0011 00          	.byte 0x0
 186 0012 00          	.byte 0x0
 187 0013 02          	.uleb128 0x2
 188 0014 24          	.uleb128 0x24
 189 0015 00          	.byte 0x0
 190 0016 0B          	.uleb128 0xb
 191 0017 0B          	.uleb128 0xb
 192 0018 3E          	.uleb128 0x3e
 193 0019 0B          	.uleb128 0xb
 194 001a 03          	.uleb128 0x3
 195 001b 08          	.uleb128 0x8
 196 001c 00          	.byte 0x0
 197 001d 00          	.byte 0x0
 198 001e 03          	.uleb128 0x3
 199 001f 16          	.uleb128 0x16
 200 0020 00          	.byte 0x0
 201 0021 03          	.uleb128 0x3
 202 0022 08          	.uleb128 0x8
 203 0023 3A          	.uleb128 0x3a
 204 0024 0B          	.uleb128 0xb
 205 0025 3B          	.uleb128 0x3b
 206 0026 0B          	.uleb128 0xb
 207 0027 49          	.uleb128 0x49
 208 0028 13          	.uleb128 0x13
 209 0029 00          	.byte 0x0
 210 002a 00          	.byte 0x0
 211 002b 04          	.uleb128 0x4
 212 002c 2E          	.uleb128 0x2e
 213 002d 01          	.byte 0x1
 214 002e 3F          	.uleb128 0x3f
 215 002f 0C          	.uleb128 0xc
 216 0030 03          	.uleb128 0x3
 217 0031 08          	.uleb128 0x8
 218 0032 3A          	.uleb128 0x3a
 219 0033 0B          	.uleb128 0xb
 220 0034 3B          	.uleb128 0x3b
 221 0035 0B          	.uleb128 0xb
 222 0036 27          	.uleb128 0x27
MPLAB XC16 ASSEMBLY Listing:   			page 6


 223 0037 0C          	.uleb128 0xc
 224 0038 49          	.uleb128 0x49
 225 0039 13          	.uleb128 0x13
 226 003a 11          	.uleb128 0x11
 227 003b 01          	.uleb128 0x1
 228 003c 12          	.uleb128 0x12
 229 003d 01          	.uleb128 0x1
 230 003e 40          	.uleb128 0x40
 231 003f 0A          	.uleb128 0xa
 232 0040 00          	.byte 0x0
 233 0041 00          	.byte 0x0
 234 0042 05          	.uleb128 0x5
 235 0043 05          	.uleb128 0x5
 236 0044 00          	.byte 0x0
 237 0045 03          	.uleb128 0x3
 238 0046 08          	.uleb128 0x8
 239 0047 3A          	.uleb128 0x3a
 240 0048 0B          	.uleb128 0xb
 241 0049 3B          	.uleb128 0x3b
 242 004a 0B          	.uleb128 0xb
 243 004b 49          	.uleb128 0x49
 244 004c 13          	.uleb128 0x13
 245 004d 02          	.uleb128 0x2
 246 004e 0A          	.uleb128 0xa
 247 004f 00          	.byte 0x0
 248 0050 00          	.byte 0x0
 249 0051 06          	.uleb128 0x6
 250 0052 34          	.uleb128 0x34
 251 0053 00          	.byte 0x0
 252 0054 03          	.uleb128 0x3
 253 0055 08          	.uleb128 0x8
 254 0056 3A          	.uleb128 0x3a
 255 0057 0B          	.uleb128 0xb
 256 0058 3B          	.uleb128 0x3b
 257 0059 0B          	.uleb128 0xb
 258 005a 49          	.uleb128 0x49
 259 005b 13          	.uleb128 0x13
 260 005c 02          	.uleb128 0x2
 261 005d 0A          	.uleb128 0xa
 262 005e 00          	.byte 0x0
 263 005f 00          	.byte 0x0
 264 0060 00          	.byte 0x0
 265                 	.section .debug_pubnames,info
 266 0000 1B 00 00 00 	.4byte 0x1b
 267 0004 02 00       	.2byte 0x2
 268 0006 00 00 00 00 	.4byte .Ldebug_info0
 269 000a 6F 01 00 00 	.4byte 0x16f
 270 000e 39 01 00 00 	.4byte 0x139
 271 0012 70 6F 70 63 	.asciz "popcount"
 271      6F 75 6E 74 
 271      00 
 272 001b 00 00 00 00 	.4byte 0x0
 273                 	.section .debug_pubtypes,info
 274 0000 1B 00 00 00 	.4byte 0x1b
 275 0004 02 00       	.2byte 0x2
 276 0006 00 00 00 00 	.4byte .Ldebug_info0
 277 000a 6F 01 00 00 	.4byte 0x16f
MPLAB XC16 ASSEMBLY Listing:   			page 7


 278 000e EA 00 00 00 	.4byte 0xea
 279 0012 75 69 6E 74 	.asciz "uint16_t"
 279      31 36 5F 74 
 279      00 
 280 001b 00 00 00 00 	.4byte 0x0
 281                 	.section .debug_aranges,info
 282 0000 14 00 00 00 	.4byte 0x14
 283 0004 02 00       	.2byte 0x2
 284 0006 00 00 00 00 	.4byte .Ldebug_info0
 285 000a 04          	.byte 0x4
 286 000b 00          	.byte 0x0
 287 000c 00 00       	.2byte 0x0
 288 000e 00 00       	.2byte 0x0
 289 0010 00 00 00 00 	.4byte 0x0
 290 0014 00 00 00 00 	.4byte 0x0
 291                 	.section .debug_str,info
 292                 	.section .text,code
 293              	
 294              	
 295              	
 296              	.section __c30_info,info,bss
 297                 	__psv_trap_errata:
 298                 	
 299                 	.section __c30_signature,info,data
 300 0000 01 00       	.word 0x0001
 301 0002 00 00       	.word 0x0000
 302 0004 00 00       	.word 0x0000
 303                 	
 304                 	
 305                 	
 306                 	.set ___PA___,0
 307                 	.end
MPLAB XC16 ASSEMBLY Listing:   			page 8


DEFINED SYMBOLS
                            *ABS*:00000000 /home/user/Documents/FST/Programming/project_sub-zero/lib/lib_pic33e/utils.c
                            *ABS*:00000001 __33EP256MU806
                            *ABS*:00000001 __dsPIC33E
                            *ABS*:00000001 __HAS_DSP
                            *ABS*:00000001 __HAS_DMAV2
                            *ABS*:00000001 __HAS_CODEGUARD
                            *ABS*:00000001 __HAS_PMP_ENHANCED
                            *ABS*:00000001 __HAS_EDS
                            *ABS*:00000001 __HAS_EP
                            *ABS*:00000011 __TARGET_DIVIDE_CYCLES
                            *ABS*:00000001 __C30ELF
    {standard input}:14     .text:00000000 _popcount
    {standard input}:18     *ABS*:00000000 ___PA___
    {standard input}:39     *ABS*:00000000 ___BP___
    {standard input}:297    __c30_info:00000000 __psv_trap_errata
    {standard input}:19     .text:00000000 .L0
                            .text:00000016 .L2
                            .text:0000000a .L3
                    .debug_abbrev:00000000 .Ldebug_abbrev0
                            .text:00000000 .Ltext0
                            .text:00000026 .Letext0
                      .debug_line:00000000 .Ldebug_line0
                            .text:00000000 .LFB0
                            .text:00000026 .LFE0
                     .debug_frame:00000000 .Lframe0
                      .debug_info:00000000 .Ldebug_info0

UNDEFINED SYMBOLS
CORCON

EQUATE SYMBOLS
/home/user/Documents/FST/Programming/project_sub-zero/lib/lib_pic33e/utils.c = 0x0
                  __33EP256MU806 = 0x1
                      __dsPIC33E = 0x1
                       __HAS_DSP = 0x1
                     __HAS_DMAV2 = 0x1
                 __HAS_CODEGUARD = 0x1
              __HAS_PMP_ENHANCED = 0x1
                       __HAS_EDS = 0x1
                        __HAS_EP = 0x1
          __TARGET_DIVIDE_CYCLES = 0x11
                        __C30ELF = 0x1
                        ___PA___ = 0x0
                        ___BP___ = 0x0
