MPLAB XC16 ASSEMBLY Listing:   			page 1


   1              	.file "/home/user/Documents/FST/Programming/project_sub-zero/lib/lib_pic33e/UART.c"
   2              	.section .debug_abbrev,info
   3                 	.Ldebug_abbrev0:
   4                 	.section .debug_info,info
   5                 	.Ldebug_info0:
   6                 	.section .debug_line,info
   7                 	.Ldebug_line0:
   8 0000 5D 02 00 00 	.section .text,code
   8      02 00 C4 00 
   8      00 00 01 01 
   8      FB 0E 0A 00 
   8      01 01 01 01 
   8      00 00 00 01 
   8      6C 69 62 2F 
   8      6C 69 62 5F 
   8      70 69 63 33 
   9              	.Ltext0:
  10              	.section .bss,bss
  11                 	.type _UART1_BUFFER,@object
  12                 	.global _UART1_BUFFER
  13                 	.align 2
  14 0000 00 00 00 00 	_UART1_BUFFER:.space 140
  14      00 00 00 00 
  14      00 00 00 00 
  14      00 00 00 00 
  14      00 00 00 00 
  14      00 00 00 00 
  14      00 00 00 00 
  14      00 00 00 00 
  14      00 00 00 00 
  15                 	.type _UART2_BUFFER,@object
  16                 	.global _UART2_BUFFER
  17                 	.align 2
  18 008c 00 00 00 00 	_UART2_BUFFER:.space 140
  18      00 00 00 00 
  18      00 00 00 00 
  18      00 00 00 00 
  18      00 00 00 00 
  18      00 00 00 00 
  18      00 00 00 00 
  18      00 00 00 00 
  18      00 00 00 00 
  19                 	.global _uart1_read
  20                 	.section .nbss,bss,near
  21                 	.align 2
  22                 	.type _uart1_read,@object
  23                 	.size _uart1_read,2
  24                 	_uart1_read:
  25 0000 00 00       	.skip 2
  26                 	.global _uart1_write
  27                 	.align 2
  28                 	.type _uart1_write,@object
  29                 	.size _uart1_write,2
  30                 	_uart1_write:
  31 0002 00 00       	.skip 2
  32                 	.global _uart2_read
  33                 	.align 2
MPLAB XC16 ASSEMBLY Listing:   			page 2


  34                 	.type _uart2_read,@object
  35                 	.size _uart2_read,2
  36                 	_uart2_read:
  37 0004 00 00       	.skip 2
  38                 	.global _uart2_write
  39                 	.align 2
  40                 	.type _uart2_write,@object
  41                 	.size _uart2_write,2
  42                 	_uart2_write:
  43 0006 00 00       	.skip 2
  44                 	.section .bss,bss
  45                 	.type _uart1_errors_status,@object
  46                 	.global _uart1_errors_status
  47                 	.align 2
  48 0118 00 00 00    	_uart1_errors_status:.space 3
  49                 	.type _uart2_errors_status,@object
  50                 	.global _uart2_errors_status
  51 011b 00          	.align 2
  52 011c 00 00 00    	_uart2_errors_status:.space 3
  53 011f 00          	.section .text,code
  54              	.align 2
  55              	.global _baudrate_generator
  56              	.type _baudrate_generator,@function
  57              	_baudrate_generator:
  58              	.LFB0:
  59              	.file 1 "lib/lib_pic33e/UART.c"
   1:lib/lib_pic33e/UART.c **** #ifndef NOUART
   2:lib/lib_pic33e/UART.c **** 
   3:lib/lib_pic33e/UART.c **** #include "UART.h"
   4:lib/lib_pic33e/UART.c **** 
   5:lib/lib_pic33e/UART.c **** unsigned int UART1_BUFFER[UART_BUFFER_LENGHT];
   6:lib/lib_pic33e/UART.c **** unsigned int UART2_BUFFER[UART_BUFFER_LENGHT];
   7:lib/lib_pic33e/UART.c **** 
   8:lib/lib_pic33e/UART.c **** unsigned int uart1_read = 0 ;
   9:lib/lib_pic33e/UART.c **** unsigned int uart1_write = 0;
  10:lib/lib_pic33e/UART.c **** unsigned int uart2_read = 0;
  11:lib/lib_pic33e/UART.c **** unsigned int uart2_write = 0;
  12:lib/lib_pic33e/UART.c **** 
  13:lib/lib_pic33e/UART.c **** UART_errors uart1_errors_status;
  14:lib/lib_pic33e/UART.c **** UART_errors uart2_errors_status;
  15:lib/lib_pic33e/UART.c **** 
  16:lib/lib_pic33e/UART.c **** /**********************************************************************
  17:lib/lib_pic33e/UART.c ****  * Name:    baudrate_generator
  18:lib/lib_pic33e/UART.c ****  * Args:    *parameters (refer to the .h for more information on the data)
  19:lib/lib_pic33e/UART.c ****  * Return:  -1 if you get a error of more then 1% with your desired baudrate/FOSC
  20:lib/lib_pic33e/UART.c ****  *          0 if the parameters struct was nicely filled
  21:lib/lib_pic33e/UART.c ****  * Desc:    determines how to configure the BRGH bit and the UxBRG acording t
  22:lib/lib_pic33e/UART.c ****  *          o the wanted baudrate
  23:lib/lib_pic33e/UART.c ****  **********************************************************************/
  24:lib/lib_pic33e/UART.c **** int baudrate_generator(UART_parameters *parameters){
  60              	.loc 1 24 0
  61              	.set ___PA___,1
  62 000000  12 00 FA 	lnk #18
  63              	.LCFI0:
  64 000002  88 1F 78 	mov w8,[w15++]
  65              	.LCFI1:
  66              	.loc 1 24 0
MPLAB XC16 ASSEMBLY Listing:   			page 3


  67 000004  00 0F 98 	mov w0,[w14+16]
  25:lib/lib_pic33e/UART.c ****     unsigned int BRG_16;
  26:lib/lib_pic33e/UART.c ****     unsigned int BRG_4;
  27:lib/lib_pic33e/UART.c ****     unsigned int BR_16;
  28:lib/lib_pic33e/UART.c ****     unsigned int BR_4;
  29:lib/lib_pic33e/UART.c ****     float error_16;
  30:lib/lib_pic33e/UART.c ****     float error_4;
  31:lib/lib_pic33e/UART.c **** 
  32:lib/lib_pic33e/UART.c ****     //calculate UxBRG  -  Baud Rate Divisor
  33:lib/lib_pic33e/UART.c ****     BRG_16 = ((Fp/parameters->BaudRate)/16) - 1;
  68              	.loc 1 33 0
  69 000006  0E 09 90 	mov [w14+16],w2
  70 000008  00 40 22 	mov #9216,w0
  71 00000a  41 C7 24 	mov #19572,w1
  72 00000c  C2 01 90 	mov [w2+8],w3
  73 00000e  32 01 90 	mov [w2+6],w2
  74 000010  00 00 07 	rcall ___divsf3
  75 000012  02 00 20 	mov #0,w2
  76 000014  03 18 24 	mov #16768,w3
  77 000016  00 00 07 	rcall ___divsf3
  78 000018  02 00 20 	mov #0,w2
  79 00001a  03 F8 23 	mov #16256,w3
  80 00001c  00 00 07 	rcall ___subsf3
  81 00001e  00 00 07 	rcall ___fixunssfsi
  34:lib/lib_pic33e/UART.c ****     BRG_4 = ((Fp/parameters->BaudRate)/4) - 1;
  82              	.loc 1 34 0
  83 000020  0E 09 90 	mov [w14+16],w2
  84              	.loc 1 33 0
  85 000022  00 0F 78 	mov w0,[w14]
  86              	.loc 1 34 0
  87 000024  C2 01 90 	mov [w2+8],w3
  88 000026  32 01 90 	mov [w2+6],w2
  89 000028  00 40 22 	mov #9216,w0
  90 00002a  41 C7 24 	mov #19572,w1
  91 00002c  00 00 07 	rcall ___divsf3
  92 00002e  02 00 20 	mov #0,w2
  93 000030  03 08 24 	mov #16512,w3
  94 000032  00 00 07 	rcall ___divsf3
  95 000034  02 00 20 	mov #0,w2
  96 000036  03 F8 23 	mov #16256,w3
  97 000038  00 00 07 	rcall ___subsf3
  98 00003a  00 00 07 	rcall ___fixunssfsi
  35:lib/lib_pic33e/UART.c **** 
  36:lib/lib_pic33e/UART.c ****     //calculate baudrate that you get by using the previous BRG
  37:lib/lib_pic33e/UART.c ****     BR_16 = (Fp/(BRG_16 + 1))/16;
  99              	.loc 1 37 0
 100 00003c  1E 01 E8 	inc [w14],w2
 101              	.loc 1 34 0
 102 00003e  10 07 98 	mov w0,[w14+2]
 103              	.loc 1 37 0
 104 000040  80 01 EB 	clr w3
 105 000042  00 90 20 	mov #2304,w0
 106 000044  D1 03 20 	mov #61,w1
 107 000046  00 00 07 	rcall ___udivsi3
  38:lib/lib_pic33e/UART.c ****     BR_4 = (Fp/(BRG_4 + 1))/4;
 108              	.loc 1 38 0
 109 000048  1E 01 90 	mov [w14+2],w2
MPLAB XC16 ASSEMBLY Listing:   			page 4


 110 00004a  02 01 E8 	inc w2,w2
 111              	.loc 1 37 0
 112 00004c  20 07 98 	mov w0,[w14+4]
 113              	.loc 1 38 0
 114 00004e  80 01 EB 	clr w3
 115 000050  00 40 22 	mov #9216,w0
 116 000052  41 0F 20 	mov #244,w1
 117 000054  00 00 07 	rcall ___udivsi3
  39:lib/lib_pic33e/UART.c **** 
  40:lib/lib_pic33e/UART.c ****     //calculate the error that you get by using the previous baudrate
  41:lib/lib_pic33e/UART.c ****     error_16 = ((BR_16 - parameters->BaudRate)/parameters->BaudRate) * 100;
 118              	.loc 1 41 0
 119 000056  2E 02 90 	mov [w14+4],w4
 120              	.loc 1 38 0
 121 000058  00 01 BE 	mov.d w0,w2
 122              	.loc 1 41 0
 123 00005a  61 20 B8 	mul.uu w4,#1,w0
 124              	.loc 1 38 0
 125 00005c  32 07 98 	mov w2,[w14+6]
 126              	.loc 1 41 0
 127 00005e  00 00 07 	rcall ___floatunsisf
 128 000060  0E 09 90 	mov [w14+16],w2
 129 000062  C2 01 90 	mov [w2+8],w3
 130 000064  32 01 90 	mov [w2+6],w2
 131 000066  00 00 07 	rcall ___subsf3
 132 000068  0E 09 90 	mov [w14+16],w2
 133 00006a  C2 01 90 	mov [w2+8],w3
 134 00006c  32 01 90 	mov [w2+6],w2
 135 00006e  00 00 07 	rcall ___divsf3
 136 000070  02 00 20 	mov #0,w2
 137 000072  83 2C 24 	mov #17096,w3
 138 000074  00 00 07 	rcall ___mulsf3
  42:lib/lib_pic33e/UART.c ****     error_4 = ((BR_4 - parameters->BaudRate)/parameters->BaudRate) * 100;
 139              	.loc 1 42 0
 140 000076  3E 01 90 	mov [w14+6],w2
 141              	.loc 1 41 0
 142 000078  40 07 98 	mov w0,[w14+8]
 143 00007a  51 07 98 	mov w1,[w14+10]
 144              	.loc 1 42 0
 145 00007c  61 10 B8 	mul.uu w2,#1,w0
 146 00007e  00 00 07 	rcall ___floatunsisf
 147 000080  0E 09 90 	mov [w14+16],w2
 148 000082  C2 01 90 	mov [w2+8],w3
 149 000084  32 01 90 	mov [w2+6],w2
 150 000086  00 00 07 	rcall ___subsf3
 151 000088  0E 09 90 	mov [w14+16],w2
 152 00008a  C2 01 90 	mov [w2+8],w3
 153 00008c  32 01 90 	mov [w2+6],w2
  43:lib/lib_pic33e/UART.c **** 
  44:lib/lib_pic33e/UART.c ****     if((error_16 < 1) || (error_4 < 1)){
 154              	.loc 1 44 0
 155 00008e  18 C0 B3 	mov.b #1,w8
 156              	.loc 1 42 0
 157 000090  00 00 07 	rcall ___divsf3
 158 000092  02 00 20 	mov #0,w2
 159 000094  83 2C 24 	mov #17096,w3
 160 000096  00 00 07 	rcall ___mulsf3
MPLAB XC16 ASSEMBLY Listing:   			page 5


 161              	.loc 1 44 0
 162 000098  02 00 20 	mov #0,w2
 163 00009a  03 F8 23 	mov #16256,w3
 164              	.loc 1 42 0
 165 00009c  00 00 00 	nop 
 166 00009e  60 07 98 	mov w0,[w14+12]
 167 0000a0  71 07 98 	mov w1,[w14+14]
 168              	.loc 1 44 0
 169 0000a2  4E 00 90 	mov [w14+8],w0
 170 0000a4  DE 00 90 	mov [w14+10],w1
 171 0000a6  00 00 07 	rcall ___ltsf2
 172 0000a8  00 00 E0 	cp0 w0
 173              	.set ___BP___,0
 174 0000aa  00 00 35 	bra lt,.L2
 175 0000ac  00 44 EB 	clr.b w8
 176              	.L2:
 177 0000ae  08 04 E0 	cp0.b w8
 178              	.set ___BP___,0
 179 0000b0  00 00 3A 	bra nz,.L3
 180 0000b2  02 00 20 	mov #0,w2
 181 0000b4  03 F8 23 	mov #16256,w3
 182 0000b6  6E 00 90 	mov [w14+12],w0
 183 0000b8  FE 00 90 	mov [w14+14],w1
 184 0000ba  18 C0 B3 	mov.b #1,w8
 185 0000bc  00 00 07 	rcall ___ltsf2
 186 0000be  00 00 E0 	cp0 w0
 187              	.set ___BP___,0
 188 0000c0  00 00 35 	bra lt,.L4
 189 0000c2  00 44 EB 	clr.b w8
 190              	.L4:
 191 0000c4  08 04 E0 	cp0.b w8
 192              	.set ___BP___,0
 193 0000c6  00 00 32 	bra z,.L5
 194              	.L3:
  45:lib/lib_pic33e/UART.c ****         if(error_16 < error_4){
 195              	.loc 1 45 0
 196 0000c8  00 00 00 	nop 
 197 0000ca  6E 01 90 	mov [w14+12],w2
 198 0000cc  FE 01 90 	mov [w14+14],w3
 199 0000ce  00 00 00 	nop 
 200 0000d0  4E 00 90 	mov [w14+8],w0
 201 0000d2  DE 00 90 	mov [w14+10],w1
 202 0000d4  18 C0 B3 	mov.b #1,w8
 203 0000d6  00 00 07 	rcall ___ltsf2
 204 0000d8  00 00 E0 	cp0 w0
 205              	.set ___BP___,0
 206 0000da  00 00 35 	bra lt,.L6
 207 0000dc  00 44 EB 	clr.b w8
 208              	.L6:
 209 0000de  08 04 E0 	cp0.b w8
 210              	.set ___BP___,0
 211 0000e0  00 00 32 	bra z,.L7
  46:lib/lib_pic33e/UART.c ****             parameters->UxBRG = BRG_16;
  47:lib/lib_pic33e/UART.c ****             parameters->BRGH =  0;
 212              	.loc 1 47 0
 213 0000e2  8E 08 90 	mov [w14+16],w1
 214              	.loc 1 46 0
MPLAB XC16 ASSEMBLY Listing:   			page 6


 215 0000e4  0E 09 90 	mov [w14+16],w2
  48:lib/lib_pic33e/UART.c ****             return 0;
 216              	.loc 1 48 0
 217 0000e6  00 00 EB 	clr w0
 218              	.loc 1 46 0
 219 0000e8  9E 01 78 	mov [w14],w3
 220 0000ea  63 01 98 	mov w3,[w2+12]
 221              	.loc 1 47 0
 222 0000ec  71 01 90 	mov [w1+14],w2
 223 0000ee  02 00 A1 	bclr w2,#0
 224 0000f0  F2 00 98 	mov w2,[w1+14]
 225              	.loc 1 48 0
 226 0000f2  00 00 37 	bra .L8
 227              	.L7:
  49:lib/lib_pic33e/UART.c ****         }else{
  50:lib/lib_pic33e/UART.c ****                 parameters->UxBRG = BRG_4;
  51:lib/lib_pic33e/UART.c ****                 parameters->BRGH =  1;
 228              	.loc 1 51 0
 229 0000f4  00 00 00 	nop 
 230 0000f6  8E 08 90 	mov [w14+16],w1
 231              	.loc 1 50 0
 232 0000f8  00 00 00 	nop 
 233 0000fa  0E 09 90 	mov [w14+16],w2
  52:lib/lib_pic33e/UART.c ****                 return 0;
 234              	.loc 1 52 0
 235 0000fc  00 00 EB 	clr w0
 236              	.loc 1 50 0
 237 0000fe  9E 01 90 	mov [w14+2],w3
 238 000100  63 01 98 	mov w3,[w2+12]
 239              	.loc 1 51 0
 240 000102  71 01 90 	mov [w1+14],w2
 241 000104  02 00 A0 	bset w2,#0
 242 000106  F2 00 98 	mov w2,[w1+14]
 243              	.loc 1 52 0
 244 000108  00 00 37 	bra .L8
 245              	.L5:
  53:lib/lib_pic33e/UART.c ****         }
  54:lib/lib_pic33e/UART.c ****     }
  55:lib/lib_pic33e/UART.c ****     return -1;
 246              	.loc 1 55 0
 247 00010a  00 80 EB 	setm w0
 248              	.L8:
  56:lib/lib_pic33e/UART.c **** 
  57:lib/lib_pic33e/UART.c **** }
 249              	.loc 1 57 0
 250 00010c  00 00 00 	nop 
 251 00010e  4F 04 78 	mov [--w15],w8
 252 000110  8E 07 78 	mov w14,w15
 253 000112  4F 07 78 	mov [--w15],w14
 254 000114  00 40 A9 	bclr CORCON,#2
 255 000116  00 00 06 	return 
 256              	.set ___PA___,0
 257              	.LFE0:
 258              	.size _baudrate_generator,.-_baudrate_generator
 259              	.align 2
 260              	.global _config_uart1
 261              	.type _config_uart1,@function
MPLAB XC16 ASSEMBLY Listing:   			page 7


 262              	_config_uart1:
 263              	.LFB1:
  58:lib/lib_pic33e/UART.c **** 
  59:lib/lib_pic33e/UART.c **** /**********************************************************************
  60:lib/lib_pic33e/UART.c ****  * Name:    config_uart1
  61:lib/lib_pic33e/UART.c ****  * Args:    parameters (refer to the .h for more information on the data)
  62:lib/lib_pic33e/UART.c ****  * Return:  -
  63:lib/lib_pic33e/UART.c ****  * Desc:    uses the parameters struct to configurate the UART1 module
  64:lib/lib_pic33e/UART.c ****  **********************************************************************/
  65:lib/lib_pic33e/UART.c **** int config_uart1(UART_parameters *parameters){
 264              	.loc 1 65 0
 265              	.set ___PA___,1
 266 000118  02 00 FA 	lnk #2
 267              	.LCFI2:
 268 00011a  88 9F BE 	mov.d w8,[w15++]
 269              	.LCFI3:
 270              	.loc 1 65 0
 271 00011c  00 0F 78 	mov w0,[w14]
  66:lib/lib_pic33e/UART.c **** 
  67:lib/lib_pic33e/UART.c ****     if(baudrate_generator(parameters) == -1){
 272              	.loc 1 67 0
 273 00011e  1E 00 78 	mov [w14],w0
 274 000120  00 00 07 	rcall _baudrate_generator
 275 000122  E1 0F 40 	add w0,#1,[w15]
 276              	.set ___BP___,0
 277 000124  00 00 3A 	bra nz,.L10
  68:lib/lib_pic33e/UART.c ****         return -1;
 278              	.loc 1 68 0
 279 000126  00 80 EB 	setm w0
 280 000128  00 00 37 	bra .L11
 281              	.L10:
  69:lib/lib_pic33e/UART.c ****     }
  70:lib/lib_pic33e/UART.c ****     U1MODEbits.UARTEN = 1;                              /*UART enable                              
  71:lib/lib_pic33e/UART.c ****     U1MODEbits.USIDL = 0;                               /*continue operation in idle               
  72:lib/lib_pic33e/UART.c ****     U1MODEbits.IREN = 0;                                /*IrDA disabled                            
  73:lib/lib_pic33e/UART.c ****     U1MODEbits.RTSMD = 0;                               /*Simplex mode                             
  74:lib/lib_pic33e/UART.c ****     U1MODEbits.UEN0 = 0;                                 /*UxTX and UxRX are enabled and UxCTS and 
  75:lib/lib_pic33e/UART.c ****     U1MODEbits.UEN1 = 0;
  76:lib/lib_pic33e/UART.c ****     U1MODEbits.WAKE = 1;                                /*wake from sleep mode when star bit is det
  77:lib/lib_pic33e/UART.c ****     U1MODEbits.LPBACK = 0;                              /*Loopback mode is disabled                
  78:lib/lib_pic33e/UART.c ****     U1MODEbits.ABAUD = 0;                               /*baud rate measurement is disabled        
  79:lib/lib_pic33e/UART.c ****     U1MODEbits.URXINV = !(parameters->UxRX_idle_state); /*UxRX idle state is '1'                   
 282              	.loc 1 79 0
 283 00012a  9E 01 78 	mov [w14],w3
  80:lib/lib_pic33e/UART.c ****     U1MODEbits.BRGH = parameters->BRGH;                 /*BRG generates 16/4 clocks per bit  second
 284              	.loc 1 80 0
 285 00012c  1E 01 78 	mov [w14],w2
  81:lib/lib_pic33e/UART.c ****     U1MODEbits.PDSEL0 = (parameters->pdsel & 0b001);    /*8 bit data, no parity                    
 286              	.loc 1 81 0
 287 00012e  1E 00 78 	mov [w14],w0
  82:lib/lib_pic33e/UART.c ****     U1MODEbits.PDSEL1 = ((parameters->pdsel & 0b010) >> 1);
 288              	.loc 1 82 0
 289 000130  9E 00 78 	mov [w14],w1
 290              	.loc 1 70 0
 291 000132  01 E0 A8 	bset.b _U1MODEbits+1,#7
 292              	.loc 1 71 0
 293 000134  01 A0 A9 	bclr.b _U1MODEbits+1,#5
MPLAB XC16 ASSEMBLY Listing:   			page 8


 294              	.loc 1 72 0
 295 000136  01 80 A9 	bclr.b _U1MODEbits+1,#4
 296              	.loc 1 73 0
 297 000138  01 60 A9 	bclr.b _U1MODEbits+1,#3
 298              	.loc 1 74 0
 299 00013a  01 00 A9 	bclr.b _U1MODEbits+1,#0
 300              	.loc 1 75 0
 301 00013c  01 20 A9 	bclr.b _U1MODEbits+1,#1
 302              	.loc 1 76 0
 303 00013e  00 E0 A8 	bset.b _U1MODEbits,#7
 304              	.loc 1 77 0
 305 000140  00 C0 A9 	bclr.b _U1MODEbits,#6
 306              	.loc 1 78 0
 307 000142  00 A0 A9 	bclr.b _U1MODEbits,#5
 308              	.loc 1 79 0
 309 000144  04 00 80 	mov _U1MODEbits,w4
 310 000146  D3 01 90 	mov [w3+10],w3
 311 000148  04 40 A1 	bclr w4,#4
 312 00014a  E1 81 61 	and w3,#1,w3
 313 00014c  03 F0 A7 	btsc w3,#15
 314 00014e  83 01 EA 	neg w3,w3
 315 000150  83 01 E9 	dec w3,w3
 316 000152  CF 19 DE 	lsr w3,#15,w3
 317 000154  00 00 00 	nop 
 318 000156  83 41 78 	mov.b w3,w3
 319 000158  83 81 FB 	ze w3,w3
 320 00015a  00 00 00 	nop 
 321 00015c  E1 81 61 	and w3,#1,w3
 322 00015e  C4 19 DD 	sl w3,#4,w3
 323 000160  84 81 71 	ior w3,w4,w3
 324 000162  03 00 88 	mov w3,_U1MODEbits
 325              	.loc 1 80 0
 326 000164  03 00 80 	mov _U1MODEbits,w3
 327 000166  72 01 90 	mov [w2+14],w2
 328 000168  03 30 A1 	bclr w3,#3
 329 00016a  61 41 61 	and.b w2,#1,w2
 330 00016c  02 81 FB 	ze w2,w2
 331 00016e  00 00 00 	nop 
 332 000170  61 01 61 	and w2,#1,w2
 333 000172  43 11 DD 	sl w2,#3,w2
 334 000174  03 01 71 	ior w2,w3,w2
 335 000176  02 00 88 	mov w2,_U1MODEbits
 336              	.loc 1 81 0
 337 000178  02 00 80 	mov _U1MODEbits,w2
 338 00017a  10 00 78 	mov [w0],w0
 339 00017c  02 10 A1 	bclr w2,#1
 340 00017e  00 40 78 	mov.b w0,w0
 341 000180  61 40 60 	and.b w0,#1,w0
 342 000182  61 40 60 	and.b w0,#1,w0
 343 000184  61 40 60 	and.b w0,#1,w0
 344 000186  00 80 FB 	ze w0,w0
 345 000188  61 00 60 	and w0,#1,w0
 346 00018a  00 00 40 	add w0,w0,w0
 347 00018c  02 00 70 	ior w0,w2,w0
 348 00018e  00 00 88 	mov w0,_U1MODEbits
 349              	.loc 1 82 0
 350 000190  11 00 78 	mov [w1],w0
MPLAB XC16 ASSEMBLY Listing:   			page 9


 351 000192  01 00 80 	mov _U1MODEbits,w1
 352 000194  62 00 60 	and w0,#2,w0
 353 000196  01 20 A1 	bclr w1,#2
 354 000198  00 00 D1 	lsr w0,w0
 355 00019a  00 40 78 	mov.b w0,w0
 356 00019c  61 40 60 	and.b w0,#1,w0
 357 00019e  00 80 FB 	ze w0,w0
 358 0001a0  61 00 60 	and w0,#1,w0
 359 0001a2  42 00 DD 	sl w0,#2,w0
 360 0001a4  01 00 70 	ior w0,w1,w0
 361 0001a6  00 00 88 	mov w0,_U1MODEbits
  83:lib/lib_pic33e/UART.c ****     U1MODEbits.STSEL = (parameters->number_stop_bits-1); /*1 stop bit                              
 362              	.loc 1 83 0
 363 0001a8  1E 00 78 	mov [w14],w0
  84:lib/lib_pic33e/UART.c **** 
  85:lib/lib_pic33e/UART.c ****     U1STAbits.UTXISEL0 = 0;                             /*interrupt is generated when any charater 
  86:lib/lib_pic33e/UART.c ****     U1STAbits.UTXISEL1 = 0;
  87:lib/lib_pic33e/UART.c ****     U1STAbits.UTXINV = !(parameters->UxTX_idle_state);  /*UxTX idle state 0 for 1 and 1 for 0      
 364              	.loc 1 87 0
 365 0001aa  9E 03 78 	mov [w14],w7
  88:lib/lib_pic33e/UART.c ****     U1STAbits.UTXEN = (!parameters->transmit);           /*transmiter is enabled, UxTX is controled
 366              	.loc 1 88 0
 367 0001ac  1E 03 78 	mov [w14],w6
  89:lib/lib_pic33e/UART.c ****     U1STAbits.UTXBRK = 0;                               /*Sync Break transmission is disabled or co
  90:lib/lib_pic33e/UART.c **** 
  91:lib/lib_pic33e/UART.c ****     /*receive configiuration*/                         
  92:lib/lib_pic33e/UART.c ****     U1STAbits.ADDEN = 0;                                /*bit has no effect - 9-bit mode not select
  93:lib/lib_pic33e/UART.c **** 
  94:lib/lib_pic33e/UART.c ****     /*defining baudrate*/
  95:lib/lib_pic33e/UART.c ****     U1BRG = parameters->UxBRG;
 368              	.loc 1 95 0
 369 0001ae  9E 02 78 	mov [w14],w5
 370              	.loc 1 83 0
 371 0001b0  01 00 80 	mov _U1MODEbits,w1
 372 0001b2  10 00 90 	mov [w0+2],w0
 373 0001b4  81 04 78 	mov w1,w9
 374 0001b6  09 00 A1 	bclr w9,#0
 375 0001b8  00 40 78 	mov.b w0,w0
  96:lib/lib_pic33e/UART.c **** 
  97:lib/lib_pic33e/UART.c ****     /*receive interrupt configuration*/
  98:lib/lib_pic33e/UART.c ****     U1STAbits.URXISEL = 0;                              /*interrupt flag bit is set when  caracter 
 376              	.loc 1 98 0
 377 0001ba  F4 F3 2F 	mov #-193,w4
 378              	.loc 1 83 0
 379 0001bc  00 40 E9 	dec.b w0,w0
  99:lib/lib_pic33e/UART.c ****     IFS0bits.U1RXIF = 0;                                /*clear interrupt flag                     
 100:lib/lib_pic33e/UART.c ****     IEC0bits.U1RXIE = 1;                                /*interrupt request enable                 
 101:lib/lib_pic33e/UART.c ****     IPC2bits.U1RXIP = 5;                                /*interrupt priority to 5                  
 380              	.loc 1 101 0
 381 0001be  F3 FF 28 	mov #-28673,w3
 382              	.loc 1 83 0
 383 0001c0  61 40 60 	and.b w0,#1,w0
 384              	.loc 1 101 0
 385 0001c2  02 00 25 	mov #20480,w2
 386              	.loc 1 83 0
 387 0001c4  00 80 FB 	ze w0,w0
 102:lib/lib_pic33e/UART.c **** 
MPLAB XC16 ASSEMBLY Listing:   			page 10


 103:lib/lib_pic33e/UART.c ****     /*receive error interrupt configuration*/
 104:lib/lib_pic33e/UART.c ****     IFS4bits.U1EIF = 0;                                 /*clear interrupt flag                     
 105:lib/lib_pic33e/UART.c ****     IEC4bits.U1EIE = 1;                                 /*interrupt enable                         
 106:lib/lib_pic33e/UART.c ****     IPC16bits.U1EIP = 7;                                /*interrupt priority - KEEP IT HIGH        
 388              	.loc 1 106 0
 389 0001c6  01 07 20 	mov #112,w1
 390              	.loc 1 83 0
 391 0001c8  61 04 60 	and w0,#1,w8
 107:lib/lib_pic33e/UART.c **** 
 108:lib/lib_pic33e/UART.c ****     return 0;
 392              	.loc 1 108 0
 393 0001ca  00 00 EB 	clr w0
 394              	.loc 1 83 0
 395 0001cc  09 04 74 	ior w8,w9,w8
 396 0001ce  08 00 88 	mov w8,_U1MODEbits
 397              	.loc 1 85 0
 398 0001d0  01 A0 A9 	bclr.b _U1STAbits+1,#5
 399              	.loc 1 86 0
 400 0001d2  01 E0 A9 	bclr.b _U1STAbits+1,#7
 401              	.loc 1 87 0
 402 0001d4  08 00 80 	mov _U1STAbits,w8
 403 0001d6  D7 03 90 	mov [w7+10],w7
 404 0001d8  08 E0 A1 	bclr w8,#14
 405 0001da  E2 83 63 	and w7,#2,w7
 406 0001dc  07 F0 A7 	btsc w7,#15
 407 0001de  87 03 EA 	neg w7,w7
 408 0001e0  87 03 E9 	dec w7,w7
 409 0001e2  CF 3B DE 	lsr w7,#15,w7
 410 0001e4  00 00 00 	nop 
 411 0001e6  87 43 78 	mov.b w7,w7
 412 0001e8  87 83 FB 	ze w7,w7
 413 0001ea  00 00 00 	nop 
 414 0001ec  E1 83 63 	and w7,#1,w7
 415 0001ee  CE 3B DD 	sl w7,#14,w7
 416 0001f0  88 83 73 	ior w7,w8,w7
 417 0001f2  07 00 88 	mov w7,_U1STAbits
 418              	.loc 1 88 0
 419 0001f4  07 00 80 	mov _U1STAbits,w7
 420 0001f6  46 43 90 	mov.b [w6+4],w6
 421 0001f8  07 A0 A1 	bclr w7,#10
 422 0001fa  06 04 A2 	btg.b w6,#0
 423 0001fc  06 83 FB 	ze w6,w6
 424 0001fe  00 00 00 	nop 
 425 000200  06 43 78 	mov.b w6,w6
 426 000202  61 43 63 	and.b w6,#1,w6
 427 000204  06 83 FB 	ze w6,w6
 428 000206  00 00 00 	nop 
 429 000208  61 03 63 	and w6,#1,w6
 430 00020a  4A 33 DD 	sl w6,#10,w6
 431 00020c  07 03 73 	ior w6,w7,w6
 432 00020e  06 00 88 	mov w6,_U1STAbits
 433              	.loc 1 89 0
 434 000210  01 60 A9 	bclr.b _U1STAbits+1,#3
 435              	.loc 1 92 0
 436 000212  00 A0 A9 	bclr.b _U1STAbits,#5
 437              	.loc 1 95 0
 438 000214  E5 02 90 	mov [w5+12],w5
MPLAB XC16 ASSEMBLY Listing:   			page 11


 439 000216  05 00 88 	mov w5,_U1BRG
 440              	.loc 1 98 0
 441 000218  05 00 80 	mov _U1STAbits,w5
 442 00021a  04 82 62 	and w5,w4,w4
 443 00021c  04 00 88 	mov w4,_U1STAbits
 444              	.loc 1 99 0
 445 00021e  01 60 A9 	bclr.b _IFS0bits+1,#3
 446              	.loc 1 100 0
 447 000220  01 60 A8 	bset.b _IEC0bits+1,#3
 448              	.loc 1 101 0
 449 000222  04 00 80 	mov _IPC2bits,w4
 450 000224  83 01 62 	and w4,w3,w3
 451 000226  03 01 71 	ior w2,w3,w2
 452 000228  02 00 88 	mov w2,_IPC2bits
 453              	.loc 1 104 0
 454 00022a  00 20 A9 	bclr.b _IFS4bits,#1
 455              	.loc 1 105 0
 456 00022c  00 20 A8 	bset.b _IEC4bits,#1
 457              	.loc 1 106 0
 458 00022e  02 00 80 	mov _IPC16bits,w2
 459 000230  82 80 70 	ior w1,w2,w1
 460 000232  01 00 88 	mov w1,_IPC16bits
 461              	.L11:
 109:lib/lib_pic33e/UART.c **** 
 110:lib/lib_pic33e/UART.c **** }
 462              	.loc 1 110 0
 463 000234  00 00 00 	nop 
 464 000236  4F 04 BE 	mov.d [--w15],w8
 465 000238  8E 07 78 	mov w14,w15
 466 00023a  4F 07 78 	mov [--w15],w14
 467 00023c  00 40 A9 	bclr CORCON,#2
 468 00023e  00 00 06 	return 
 469              	.set ___PA___,0
 470              	.LFE1:
 471              	.size _config_uart1,.-_config_uart1
 472              	.align 2
 473              	.global _config_uart2
 474              	.type _config_uart2,@function
 475              	_config_uart2:
 476              	.LFB2:
 111:lib/lib_pic33e/UART.c **** 
 112:lib/lib_pic33e/UART.c **** /**********************************************************************
 113:lib/lib_pic33e/UART.c ****  * Name:    config_uart2
 114:lib/lib_pic33e/UART.c ****  * Args:    parameters (refer to the .h for more information on the data)
 115:lib/lib_pic33e/UART.c ****  * Return:  -
 116:lib/lib_pic33e/UART.c ****  * Desc:    uses the parameters struct to configurate the UART2 module
 117:lib/lib_pic33e/UART.c ****  **********************************************************************/
 118:lib/lib_pic33e/UART.c **** int config_uart2(UART_parameters *parameters){
 477              	.loc 1 118 0
 478              	.set ___PA___,1
 479 000240  02 00 FA 	lnk #2
 480              	.LCFI4:
 481 000242  88 9F BE 	mov.d w8,[w15++]
 482              	.LCFI5:
 483              	.loc 1 118 0
 484 000244  00 0F 78 	mov w0,[w14]
 119:lib/lib_pic33e/UART.c **** 
MPLAB XC16 ASSEMBLY Listing:   			page 12


 120:lib/lib_pic33e/UART.c ****      if(baudrate_generator(parameters) == -1){
 485              	.loc 1 120 0
 486 000246  1E 00 78 	mov [w14],w0
 487 000248  00 00 07 	rcall _baudrate_generator
 488 00024a  E1 0F 40 	add w0,#1,[w15]
 489              	.set ___BP___,0
 490 00024c  00 00 3A 	bra nz,.L13
 121:lib/lib_pic33e/UART.c ****         return -1;
 491              	.loc 1 121 0
 492 00024e  00 80 EB 	setm w0
 493 000250  00 00 37 	bra .L14
 494              	.L13:
 122:lib/lib_pic33e/UART.c ****     }
 123:lib/lib_pic33e/UART.c ****     U2MODEbits.UARTEN = 1;                              /*UART enable                              
 124:lib/lib_pic33e/UART.c ****     U2MODEbits.USIDL = 0;                               /*continue operation in idle               
 125:lib/lib_pic33e/UART.c ****     U2MODEbits.IREN = 0;                                /*IrDA disabled                            
 126:lib/lib_pic33e/UART.c ****     U2MODEbits.RTSMD = 0;                               /*Simplex mode                             
 127:lib/lib_pic33e/UART.c ****     U2MODEbits.UEN0 = 0;                                 /*UxTX and UxRX are enabled and UxCTS and 
 128:lib/lib_pic33e/UART.c ****     U2MODEbits.UEN1 = 0;
 129:lib/lib_pic33e/UART.c ****     U2MODEbits.WAKE = 1;                                /*wake from sleep mode when star bit is det
 130:lib/lib_pic33e/UART.c ****     U2MODEbits.LPBACK = 0;                              /*Loopback mode is disabled                
 131:lib/lib_pic33e/UART.c ****     U2MODEbits.ABAUD = 0;                               /*baud rate measurement is disabled        
 132:lib/lib_pic33e/UART.c ****     U2MODEbits.URXINV = !(parameters->UxRX_idle_state); /*UxRX idle state is '1'                   
 495              	.loc 1 132 0
 496 000252  9E 01 78 	mov [w14],w3
 133:lib/lib_pic33e/UART.c ****     U2MODEbits.BRGH = parameters->BRGH;                 /*BRG generates 16/4 clocks per bit  second
 497              	.loc 1 133 0
 498 000254  1E 01 78 	mov [w14],w2
 134:lib/lib_pic33e/UART.c ****     U2MODEbits.PDSEL0 = (parameters->pdsel & 0b001);    /*8 bit data, no parity                    
 499              	.loc 1 134 0
 500 000256  1E 00 78 	mov [w14],w0
 135:lib/lib_pic33e/UART.c ****     U2MODEbits.PDSEL1 = ((parameters->pdsel & 0b010) >> 1);
 501              	.loc 1 135 0
 502 000258  9E 00 78 	mov [w14],w1
 503              	.loc 1 123 0
 504 00025a  01 E0 A8 	bset.b _U2MODEbits+1,#7
 505              	.loc 1 124 0
 506 00025c  01 A0 A9 	bclr.b _U2MODEbits+1,#5
 507              	.loc 1 125 0
 508 00025e  01 80 A9 	bclr.b _U2MODEbits+1,#4
 509              	.loc 1 126 0
 510 000260  01 60 A9 	bclr.b _U2MODEbits+1,#3
 511              	.loc 1 127 0
 512 000262  01 00 A9 	bclr.b _U2MODEbits+1,#0
 513              	.loc 1 128 0
 514 000264  01 20 A9 	bclr.b _U2MODEbits+1,#1
 515              	.loc 1 129 0
 516 000266  00 E0 A8 	bset.b _U2MODEbits,#7
 517              	.loc 1 130 0
 518 000268  00 C0 A9 	bclr.b _U2MODEbits,#6
 519              	.loc 1 131 0
 520 00026a  00 A0 A9 	bclr.b _U2MODEbits,#5
 521              	.loc 1 132 0
 522 00026c  04 00 80 	mov _U2MODEbits,w4
 523 00026e  D3 01 90 	mov [w3+10],w3
 524 000270  04 40 A1 	bclr w4,#4
 525 000272  E1 81 61 	and w3,#1,w3
MPLAB XC16 ASSEMBLY Listing:   			page 13


 526 000274  03 F0 A7 	btsc w3,#15
 527 000276  83 01 EA 	neg w3,w3
 528 000278  83 01 E9 	dec w3,w3
 529 00027a  CF 19 DE 	lsr w3,#15,w3
 530 00027c  00 00 00 	nop 
 531 00027e  83 41 78 	mov.b w3,w3
 532 000280  83 81 FB 	ze w3,w3
 533 000282  00 00 00 	nop 
 534 000284  E1 81 61 	and w3,#1,w3
 535 000286  C4 19 DD 	sl w3,#4,w3
 536 000288  84 81 71 	ior w3,w4,w3
 537 00028a  03 00 88 	mov w3,_U2MODEbits
 538              	.loc 1 133 0
 539 00028c  03 00 80 	mov _U2MODEbits,w3
 540 00028e  72 01 90 	mov [w2+14],w2
 541 000290  03 30 A1 	bclr w3,#3
 542 000292  61 41 61 	and.b w2,#1,w2
 543 000294  02 81 FB 	ze w2,w2
 544 000296  00 00 00 	nop 
 545 000298  61 01 61 	and w2,#1,w2
 546 00029a  43 11 DD 	sl w2,#3,w2
 547 00029c  03 01 71 	ior w2,w3,w2
 548 00029e  02 00 88 	mov w2,_U2MODEbits
 549              	.loc 1 134 0
 550 0002a0  02 00 80 	mov _U2MODEbits,w2
 551 0002a2  10 00 78 	mov [w0],w0
 552 0002a4  02 10 A1 	bclr w2,#1
 553 0002a6  00 40 78 	mov.b w0,w0
 554 0002a8  61 40 60 	and.b w0,#1,w0
 555 0002aa  61 40 60 	and.b w0,#1,w0
 556 0002ac  61 40 60 	and.b w0,#1,w0
 557 0002ae  00 80 FB 	ze w0,w0
 558 0002b0  61 00 60 	and w0,#1,w0
 559 0002b2  00 00 40 	add w0,w0,w0
 560 0002b4  02 00 70 	ior w0,w2,w0
 561 0002b6  00 00 88 	mov w0,_U2MODEbits
 562              	.loc 1 135 0
 563 0002b8  11 00 78 	mov [w1],w0
 564 0002ba  01 00 80 	mov _U2MODEbits,w1
 565 0002bc  62 00 60 	and w0,#2,w0
 566 0002be  01 20 A1 	bclr w1,#2
 567 0002c0  00 00 D1 	lsr w0,w0
 568 0002c2  00 40 78 	mov.b w0,w0
 569 0002c4  61 40 60 	and.b w0,#1,w0
 570 0002c6  00 80 FB 	ze w0,w0
 571 0002c8  61 00 60 	and w0,#1,w0
 572 0002ca  42 00 DD 	sl w0,#2,w0
 573 0002cc  01 00 70 	ior w0,w1,w0
 574 0002ce  00 00 88 	mov w0,_U2MODEbits
 136:lib/lib_pic33e/UART.c ****     U2MODEbits.STSEL = (parameters->number_stop_bits-1); /*1 stop bit                              
 575              	.loc 1 136 0
 576 0002d0  1E 00 78 	mov [w14],w0
 137:lib/lib_pic33e/UART.c **** 
 138:lib/lib_pic33e/UART.c ****     U2STAbits.UTXISEL0 = 0;                             /*interrupt is generated when any charater 
 139:lib/lib_pic33e/UART.c ****     U2STAbits.UTXISEL1 = 0;
 140:lib/lib_pic33e/UART.c ****     U2STAbits.UTXINV = !(parameters->UxTX_idle_state);  /*UxTX idle state 0 for 1 and 1 for 0      
 577              	.loc 1 140 0
MPLAB XC16 ASSEMBLY Listing:   			page 14


 578 0002d2  9E 03 78 	mov [w14],w7
 141:lib/lib_pic33e/UART.c ****     U2STAbits.UTXEN = (!parameters->transmit);           /*transmiter is enabled, UxTX is controled
 579              	.loc 1 141 0
 580 0002d4  1E 03 78 	mov [w14],w6
 142:lib/lib_pic33e/UART.c ****     U2STAbits.UTXBRK = 0;                               /*Sync Break transmission is disabled or co
 143:lib/lib_pic33e/UART.c **** 
 144:lib/lib_pic33e/UART.c ****     /*receive configiuration*/                         
 145:lib/lib_pic33e/UART.c ****     U2STAbits.ADDEN = 0;                                /*bit has no effect - 9-bit mode not select
 146:lib/lib_pic33e/UART.c **** 
 147:lib/lib_pic33e/UART.c ****     /*defining baudrate*/
 148:lib/lib_pic33e/UART.c ****     U2BRG = parameters->UxBRG;
 581              	.loc 1 148 0
 582 0002d6  9E 02 78 	mov [w14],w5
 583              	.loc 1 136 0
 584 0002d8  01 00 80 	mov _U2MODEbits,w1
 585 0002da  10 00 90 	mov [w0+2],w0
 586 0002dc  81 04 78 	mov w1,w9
 587 0002de  09 00 A1 	bclr w9,#0
 588 0002e0  00 40 78 	mov.b w0,w0
 149:lib/lib_pic33e/UART.c **** 
 150:lib/lib_pic33e/UART.c ****     /*receive interrupt configuration*/
 151:lib/lib_pic33e/UART.c ****     U2STAbits.URXISEL = 0;                              /*interrupt flag bit is set when  caracter 
 589              	.loc 1 151 0
 590 0002e2  F4 F3 2F 	mov #-193,w4
 591              	.loc 1 136 0
 592 0002e4  00 40 E9 	dec.b w0,w0
 152:lib/lib_pic33e/UART.c ****     IFS1bits.U2RXIF = 0;                                /*clear interrupt flag                     
 153:lib/lib_pic33e/UART.c ****     IEC1bits.U2RXIE = 1;                                /*interrupt request enable                 
 154:lib/lib_pic33e/UART.c ****     IPC7bits.U2RXIP = 5;                                /*interrupt priority to 5                  
 593              	.loc 1 154 0
 594 0002e6  F3 8F 2F 	mov #-1793,w3
 595              	.loc 1 136 0
 596 0002e8  61 40 60 	and.b w0,#1,w0
 597              	.loc 1 154 0
 598 0002ea  02 50 20 	mov #1280,w2
 599              	.loc 1 136 0
 600 0002ec  00 80 FB 	ze w0,w0
 155:lib/lib_pic33e/UART.c **** 
 156:lib/lib_pic33e/UART.c ****     /*receive error interrupt configuration*/
 157:lib/lib_pic33e/UART.c ****     IFS4bits.U2EIF = 0;                                 /*clear interrupt flag                     
 158:lib/lib_pic33e/UART.c ****     IEC4bits.U2EIE = 1;                                 /*interrupt enable                         
 159:lib/lib_pic33e/UART.c ****     IPC16bits.U2EIP = 7;                                /*interrupt priority - KEEP IT HIGH        
 601              	.loc 1 159 0
 602 0002ee  01 70 20 	mov #1792,w1
 603              	.loc 1 136 0
 604 0002f0  61 04 60 	and w0,#1,w8
 160:lib/lib_pic33e/UART.c **** 
 161:lib/lib_pic33e/UART.c ****     return 0;
 605              	.loc 1 161 0
 606 0002f2  00 00 EB 	clr w0
 607              	.loc 1 136 0
 608 0002f4  09 04 74 	ior w8,w9,w8
 609 0002f6  08 00 88 	mov w8,_U2MODEbits
 610              	.loc 1 138 0
 611 0002f8  01 A0 A9 	bclr.b _U2STAbits+1,#5
 612              	.loc 1 139 0
 613 0002fa  01 E0 A9 	bclr.b _U2STAbits+1,#7
MPLAB XC16 ASSEMBLY Listing:   			page 15


 614              	.loc 1 140 0
 615 0002fc  08 00 80 	mov _U2STAbits,w8
 616 0002fe  D7 03 90 	mov [w7+10],w7
 617 000300  08 E0 A1 	bclr w8,#14
 618 000302  E2 83 63 	and w7,#2,w7
 619 000304  07 F0 A7 	btsc w7,#15
 620 000306  87 03 EA 	neg w7,w7
 621 000308  87 03 E9 	dec w7,w7
 622 00030a  CF 3B DE 	lsr w7,#15,w7
 623 00030c  00 00 00 	nop 
 624 00030e  87 43 78 	mov.b w7,w7
 625 000310  87 83 FB 	ze w7,w7
 626 000312  00 00 00 	nop 
 627 000314  E1 83 63 	and w7,#1,w7
 628 000316  CE 3B DD 	sl w7,#14,w7
 629 000318  88 83 73 	ior w7,w8,w7
 630 00031a  07 00 88 	mov w7,_U2STAbits
 631              	.loc 1 141 0
 632 00031c  07 00 80 	mov _U2STAbits,w7
 633 00031e  46 43 90 	mov.b [w6+4],w6
 634 000320  07 A0 A1 	bclr w7,#10
 635 000322  06 04 A2 	btg.b w6,#0
 636 000324  06 83 FB 	ze w6,w6
 637 000326  00 00 00 	nop 
 638 000328  06 43 78 	mov.b w6,w6
 639 00032a  61 43 63 	and.b w6,#1,w6
 640 00032c  06 83 FB 	ze w6,w6
 641 00032e  00 00 00 	nop 
 642 000330  61 03 63 	and w6,#1,w6
 643 000332  4A 33 DD 	sl w6,#10,w6
 644 000334  07 03 73 	ior w6,w7,w6
 645 000336  06 00 88 	mov w6,_U2STAbits
 646              	.loc 1 142 0
 647 000338  01 60 A9 	bclr.b _U2STAbits+1,#3
 648              	.loc 1 145 0
 649 00033a  00 A0 A9 	bclr.b _U2STAbits,#5
 650              	.loc 1 148 0
 651 00033c  E5 02 90 	mov [w5+12],w5
 652 00033e  05 00 88 	mov w5,_U2BRG
 653              	.loc 1 151 0
 654 000340  05 00 80 	mov _U2STAbits,w5
 655 000342  04 82 62 	and w5,w4,w4
 656 000344  04 00 88 	mov w4,_U2STAbits
 657              	.loc 1 152 0
 658 000346  01 C0 A9 	bclr.b _IFS1bits+1,#6
 659              	.loc 1 153 0
 660 000348  01 C0 A8 	bset.b _IEC1bits+1,#6
 661              	.loc 1 154 0
 662 00034a  04 00 80 	mov _IPC7bits,w4
 663 00034c  83 01 62 	and w4,w3,w3
 664 00034e  03 01 71 	ior w2,w3,w2
 665 000350  02 00 88 	mov w2,_IPC7bits
 666              	.loc 1 157 0
 667 000352  00 40 A9 	bclr.b _IFS4bits,#2
 668              	.loc 1 158 0
 669 000354  00 40 A8 	bset.b _IEC4bits,#2
 670              	.loc 1 159 0
MPLAB XC16 ASSEMBLY Listing:   			page 16


 671 000356  02 00 80 	mov _IPC16bits,w2
 672 000358  82 80 70 	ior w1,w2,w1
 673 00035a  01 00 88 	mov w1,_IPC16bits
 674              	.L14:
 162:lib/lib_pic33e/UART.c **** 
 163:lib/lib_pic33e/UART.c **** }
 675              	.loc 1 163 0
 676 00035c  00 00 00 	nop 
 677 00035e  4F 04 BE 	mov.d [--w15],w8
 678 000360  8E 07 78 	mov w14,w15
 679 000362  4F 07 78 	mov [--w15],w14
 680 000364  00 40 A9 	bclr CORCON,#2
 681 000366  00 00 06 	return 
 682              	.set ___PA___,0
 683              	.LFE2:
 684              	.size _config_uart2,.-_config_uart2
 685              	.align 2
 686              	.global _enable_uart1
 687              	.type _enable_uart1,@function
 688              	_enable_uart1:
 689              	.LFB3:
 164:lib/lib_pic33e/UART.c **** 
 165:lib/lib_pic33e/UART.c **** /**********************************************************************
 166:lib/lib_pic33e/UART.c ****  * Name:    enable_uart1
 167:lib/lib_pic33e/UART.c ****  * Args:    -
 168:lib/lib_pic33e/UART.c ****  * Return:  -
 169:lib/lib_pic33e/UART.c ****  * Desc:    enables the UART1 module
 170:lib/lib_pic33e/UART.c ****  **********************************************************************/
 171:lib/lib_pic33e/UART.c **** void enable_uart1(void){
 690              	.loc 1 171 0
 691              	.set ___PA___,1
 692 000368  00 00 FA 	lnk #0
 693              	.LCFI6:
 172:lib/lib_pic33e/UART.c ****     U1MODEbits.UARTEN = 1; /*UART1 enable*/
 694              	.loc 1 172 0
 695 00036a  01 E0 A8 	bset.b _U1MODEbits+1,#7
 173:lib/lib_pic33e/UART.c ****     return;
 174:lib/lib_pic33e/UART.c **** }
 696              	.loc 1 174 0
 697 00036c  8E 07 78 	mov w14,w15
 698 00036e  4F 07 78 	mov [--w15],w14
 699 000370  00 40 A9 	bclr CORCON,#2
 700 000372  00 00 06 	return 
 701              	.set ___PA___,0
 702              	.LFE3:
 703              	.size _enable_uart1,.-_enable_uart1
 704              	.align 2
 705              	.global _enable_uart2
 706              	.type _enable_uart2,@function
 707              	_enable_uart2:
 708              	.LFB4:
 175:lib/lib_pic33e/UART.c **** 
 176:lib/lib_pic33e/UART.c **** /**********************************************************************
 177:lib/lib_pic33e/UART.c ****  * Name:    enable_uart2
 178:lib/lib_pic33e/UART.c ****  * Args:    -
 179:lib/lib_pic33e/UART.c ****  * Return:  -
 180:lib/lib_pic33e/UART.c ****  * Desc:    enables the UART2 module
MPLAB XC16 ASSEMBLY Listing:   			page 17


 181:lib/lib_pic33e/UART.c ****  **********************************************************************/
 182:lib/lib_pic33e/UART.c **** void enable_uart2(void){
 709              	.loc 1 182 0
 710              	.set ___PA___,1
 711 000374  00 00 FA 	lnk #0
 712              	.LCFI7:
 183:lib/lib_pic33e/UART.c ****     U2MODEbits.UARTEN = 1; /*UART2 enable*/
 713              	.loc 1 183 0
 714 000376  01 E0 A8 	bset.b _U2MODEbits+1,#7
 184:lib/lib_pic33e/UART.c ****     return;
 185:lib/lib_pic33e/UART.c **** }
 715              	.loc 1 185 0
 716 000378  8E 07 78 	mov w14,w15
 717 00037a  4F 07 78 	mov [--w15],w14
 718 00037c  00 40 A9 	bclr CORCON,#2
 719 00037e  00 00 06 	return 
 720              	.set ___PA___,0
 721              	.LFE4:
 722              	.size _enable_uart2,.-_enable_uart2
 723              	.align 2
 724              	.global _disable_uart1
 725              	.type _disable_uart1,@function
 726              	_disable_uart1:
 727              	.LFB5:
 186:lib/lib_pic33e/UART.c **** 
 187:lib/lib_pic33e/UART.c **** /**********************************************************************
 188:lib/lib_pic33e/UART.c ****  * Name:    disable_uart1
 189:lib/lib_pic33e/UART.c ****  * Args:    -
 190:lib/lib_pic33e/UART.c ****  * Return:  -
 191:lib/lib_pic33e/UART.c ****  * Desc:    disables the UART1 module
 192:lib/lib_pic33e/UART.c ****  **********************************************************************/
 193:lib/lib_pic33e/UART.c **** void disable_uart1(void){
 728              	.loc 1 193 0
 729              	.set ___PA___,1
 730 000380  00 00 FA 	lnk #0
 731              	.LCFI8:
 194:lib/lib_pic33e/UART.c ****     U1MODEbits.UARTEN = 0; /*UART1 disable*/
 732              	.loc 1 194 0
 733 000382  01 E0 A9 	bclr.b _U1MODEbits+1,#7
 195:lib/lib_pic33e/UART.c ****     return;
 196:lib/lib_pic33e/UART.c **** }
 734              	.loc 1 196 0
 735 000384  8E 07 78 	mov w14,w15
 736 000386  4F 07 78 	mov [--w15],w14
 737 000388  00 40 A9 	bclr CORCON,#2
 738 00038a  00 00 06 	return 
 739              	.set ___PA___,0
 740              	.LFE5:
 741              	.size _disable_uart1,.-_disable_uart1
 742              	.align 2
 743              	.global _disable_uart2
 744              	.type _disable_uart2,@function
 745              	_disable_uart2:
 746              	.LFB6:
 197:lib/lib_pic33e/UART.c **** 
 198:lib/lib_pic33e/UART.c **** /**********************************************************************
 199:lib/lib_pic33e/UART.c ****  * Name:    disable_uart2
MPLAB XC16 ASSEMBLY Listing:   			page 18


 200:lib/lib_pic33e/UART.c ****  * Args:    -
 201:lib/lib_pic33e/UART.c ****  * Return:  -
 202:lib/lib_pic33e/UART.c ****  * Desc:    disables the UART2 module
 203:lib/lib_pic33e/UART.c ****  **********************************************************************/
 204:lib/lib_pic33e/UART.c **** void disable_uart2(void){
 747              	.loc 1 204 0
 748              	.set ___PA___,1
 749 00038c  00 00 FA 	lnk #0
 750              	.LCFI9:
 205:lib/lib_pic33e/UART.c ****     U2MODEbits.UARTEN = 0; /*UART2 disable*/
 751              	.loc 1 205 0
 752 00038e  01 E0 A9 	bclr.b _U2MODEbits+1,#7
 206:lib/lib_pic33e/UART.c ****     return;
 207:lib/lib_pic33e/UART.c **** }
 753              	.loc 1 207 0
 754 000390  8E 07 78 	mov w14,w15
 755 000392  4F 07 78 	mov [--w15],w14
 756 000394  00 40 A9 	bclr CORCON,#2
 757 000396  00 00 06 	return 
 758              	.set ___PA___,0
 759              	.LFE6:
 760              	.size _disable_uart2,.-_disable_uart2
 761              	.align 2
 762              	.global _receive_uart1_buffer_empty
 763              	.type _receive_uart1_buffer_empty,@function
 764              	_receive_uart1_buffer_empty:
 765              	.LFB7:
 208:lib/lib_pic33e/UART.c **** 
 209:lib/lib_pic33e/UART.c **** /**********************************************************************
 210:lib/lib_pic33e/UART.c ****  * Name:    receive_uart1_buffer_empty
 211:lib/lib_pic33e/UART.c ****  * Args:    -
 212:lib/lib_pic33e/UART.c ****  * Return:  bool (TRUE - buffer is empty)
 213:lib/lib_pic33e/UART.c ****  * Desc:    checks if the receive buffer is empty. Use this fuction before
 214:lib/lib_pic33e/UART.c ****  *          disabling the module if you dont want to lose any characters
 215:lib/lib_pic33e/UART.c ****  **********************************************************************/
 216:lib/lib_pic33e/UART.c **** bool receive_uart1_buffer_empty(void){
 766              	.loc 1 216 0
 767              	.set ___PA___,1
 768 000398  00 00 FA 	lnk #0
 769              	.LCFI10:
 217:lib/lib_pic33e/UART.c ****     if(U1STAbits.URXDA == 1){
 770              	.loc 1 217 0
 771 00039a  00 00 80 	mov _U1STAbits,w0
 772 00039c  61 00 60 	and w0,#1,w0
 773 00039e  00 00 E0 	cp0 w0
 774              	.set ___BP___,0
 775 0003a0  00 00 32 	bra z,.L20
 218:lib/lib_pic33e/UART.c ****         return false;
 776              	.loc 1 218 0
 777 0003a2  00 40 EB 	clr.b w0
 778 0003a4  00 00 37 	bra .L21
 779              	.L20:
 219:lib/lib_pic33e/UART.c ****     }
 220:lib/lib_pic33e/UART.c ****     return true;
 780              	.loc 1 220 0
 781 0003a6  10 C0 B3 	mov.b #1,w0
 782              	.L21:
MPLAB XC16 ASSEMBLY Listing:   			page 19


 221:lib/lib_pic33e/UART.c **** }
 783              	.loc 1 221 0
 784 0003a8  8E 07 78 	mov w14,w15
 785 0003aa  4F 07 78 	mov [--w15],w14
 786 0003ac  00 40 A9 	bclr CORCON,#2
 787 0003ae  00 00 06 	return 
 788              	.set ___PA___,0
 789              	.LFE7:
 790              	.size _receive_uart1_buffer_empty,.-_receive_uart1_buffer_empty
 791              	.align 2
 792              	.global _receive_uart2_buffer_empty
 793              	.type _receive_uart2_buffer_empty,@function
 794              	_receive_uart2_buffer_empty:
 795              	.LFB8:
 222:lib/lib_pic33e/UART.c **** 
 223:lib/lib_pic33e/UART.c **** /**********************************************************************
 224:lib/lib_pic33e/UART.c ****  * Name:    receive_uart2_buffer_empty
 225:lib/lib_pic33e/UART.c ****  * Args:    -
 226:lib/lib_pic33e/UART.c ****  * Return:  bool (TRUE - buffer is empty)
 227:lib/lib_pic33e/UART.c ****  * Desc:    checks if the receive buffer is empty. Use this fuction before
 228:lib/lib_pic33e/UART.c ****  *          disabling the module if you dont want to lose any characters
 229:lib/lib_pic33e/UART.c ****  **********************************************************************/
 230:lib/lib_pic33e/UART.c **** bool receive_uart2_buffer_empty(void){
 796              	.loc 1 230 0
 797              	.set ___PA___,1
 798 0003b0  00 00 FA 	lnk #0
 799              	.LCFI11:
 231:lib/lib_pic33e/UART.c ****     if(U2STAbits.URXDA == 1){
 800              	.loc 1 231 0
 801 0003b2  00 00 80 	mov _U2STAbits,w0
 802 0003b4  61 00 60 	and w0,#1,w0
 803 0003b6  00 00 E0 	cp0 w0
 804              	.set ___BP___,0
 805 0003b8  00 00 32 	bra z,.L23
 232:lib/lib_pic33e/UART.c ****         return false;
 806              	.loc 1 232 0
 807 0003ba  00 40 EB 	clr.b w0
 808 0003bc  00 00 37 	bra .L24
 809              	.L23:
 233:lib/lib_pic33e/UART.c ****     }
 234:lib/lib_pic33e/UART.c ****     return true;
 810              	.loc 1 234 0
 811 0003be  10 C0 B3 	mov.b #1,w0
 812              	.L24:
 235:lib/lib_pic33e/UART.c **** }
 813              	.loc 1 235 0
 814 0003c0  8E 07 78 	mov w14,w15
 815 0003c2  4F 07 78 	mov [--w15],w14
 816 0003c4  00 40 A9 	bclr CORCON,#2
 817 0003c6  00 00 06 	return 
 818              	.set ___PA___,0
 819              	.LFE8:
 820              	.size _receive_uart2_buffer_empty,.-_receive_uart2_buffer_empty
 821              	.align 2
 822              	.global _get_FIFO_data
 823              	.type _get_FIFO_data,@function
 824              	_get_FIFO_data:
MPLAB XC16 ASSEMBLY Listing:   			page 20


 825              	.LFB9:
 236:lib/lib_pic33e/UART.c **** 
 237:lib/lib_pic33e/UART.c **** /**********************************************************************
 238:lib/lib_pic33e/UART.c ****  * Name:    get_FIFO_data
 239:lib/lib_pic33e/UART.c ****  * Args:    uart_number - refers to the what uart you are using (uart1/uart2)
 240:lib/lib_pic33e/UART.c ****  * Return:  char - the first vharacter in the FIFO
 241:lib/lib_pic33e/UART.c ****  * Desc:    gets the first caracter from the FIFO
 242:lib/lib_pic33e/UART.c ****  **********************************************************************/
 243:lib/lib_pic33e/UART.c **** char get_FIFO_data(unsigned int uart_number){
 826              	.loc 1 243 0
 827              	.set ___PA___,1
 828 0003c8  04 00 FA 	lnk #4
 829              	.LCFI12:
 830              	.loc 1 243 0
 831 0003ca  10 07 98 	mov w0,[w14+2]
 244:lib/lib_pic33e/UART.c ****     char aux;
 245:lib/lib_pic33e/UART.c ****     switch(uart_number){
 832              	.loc 1 245 0
 833 0003cc  1E 00 90 	mov [w14+2],w0
 834 0003ce  E1 0F 50 	sub w0,#1,[w15]
 835              	.set ___BP___,0
 836 0003d0  00 00 32 	bra z,.L27
 837 0003d2  E2 0F 50 	sub w0,#2,[w15]
 838              	.set ___BP___,0
 839 0003d4  00 00 32 	bra z,.L28
 840 0003d6  00 00 37 	bra .L30
 841              	.L27:
 246:lib/lib_pic33e/UART.c ****         case 1:
 247:lib/lib_pic33e/UART.c ****             aux = U1RXREG;
 842              	.loc 1 247 0
 843 0003d8  00 00 80 	mov _U1RXREG,w0
 844 0003da  00 4F 78 	mov.b w0,[w14]
 248:lib/lib_pic33e/UART.c ****             return aux;
 845              	.loc 1 248 0
 846 0003dc  1E 40 78 	mov.b [w14],w0
 847 0003de  00 00 37 	bra .L29
 848              	.L28:
 249:lib/lib_pic33e/UART.c ****             break;
 250:lib/lib_pic33e/UART.c ****         case 2:
 251:lib/lib_pic33e/UART.c ****             aux = U2RXREG;
 849              	.loc 1 251 0
 850 0003e0  00 00 80 	mov _U2RXREG,w0
 851 0003e2  00 4F 78 	mov.b w0,[w14]
 252:lib/lib_pic33e/UART.c ****             return aux;
 852              	.loc 1 252 0
 853 0003e4  1E 40 78 	mov.b [w14],w0
 854 0003e6  00 00 37 	bra .L29
 855              	.L30:
 253:lib/lib_pic33e/UART.c ****             break;
 254:lib/lib_pic33e/UART.c ****     }
 255:lib/lib_pic33e/UART.c ****     return 0; 
 856              	.loc 1 255 0
 857 0003e8  00 40 EB 	clr.b w0
 858              	.L29:
 256:lib/lib_pic33e/UART.c **** }
 859              	.loc 1 256 0
 860 0003ea  8E 07 78 	mov w14,w15
MPLAB XC16 ASSEMBLY Listing:   			page 21


 861 0003ec  4F 07 78 	mov [--w15],w14
 862 0003ee  00 40 A9 	bclr CORCON,#2
 863 0003f0  00 00 06 	return 
 864              	.set ___PA___,0
 865              	.LFE9:
 866              	.size _get_FIFO_data,.-_get_FIFO_data
 867              	.section .isr.text,code,keep
 868              	.align 2
 869              	.global __U1RXInterrupt
 870              	.type __U1RXInterrupt,@function
 871              	__U1RXInterrupt:
 872              	.section .isr.text,code,keep
 873              	.LFB10:
 874              	.section .isr.text,code,keep
 257:lib/lib_pic33e/UART.c **** 
 258:lib/lib_pic33e/UART.c **** /**********************************************************************
 259:lib/lib_pic33e/UART.c ****  * Name:    U1RX receive interrupt
 260:lib/lib_pic33e/UART.c ****  * Args:    -
 261:lib/lib_pic33e/UART.c ****  * Return:  -
 262:lib/lib_pic33e/UART.c ****  * Desc:    call upon UART1 receiving (interrupts every char received)
 263:lib/lib_pic33e/UART.c ****  **********************************************************************/
 264:lib/lib_pic33e/UART.c **** void __attribute__((interrupt, auto_psv, shadow)) _U1RXInterrupt (void){
 875              	.loc 1 264 0
 876              	.set ___PA___,1
 877 000000  00 A0 FE 	push.s 
 878 000002  00 00 F8 	push _RCOUNT
 879              	.LCFI13:
 880 000004  84 9F BE 	mov.d w4,[w15++]
 881              	.LCFI14:
 882 000006  86 9F BE 	mov.d w6,[w15++]
 883              	.LCFI15:
 884 000008  88 1F 78 	mov w8,[w15++]
 885              	.LCFI16:
 886 00000a  00 00 F8 	push _DSRPAG
 887              	.LCFI17:
 888 00000c  00 00 F8 	push _DSWPAG
 889              	.LCFI18:
 890 00000e  14 00 20 	mov #1,w4
 891 000010  04 00 88 	mov w4,_DSWPAG
 892 000012  04 00 20 	mov #__const_psvpage,w4
 893 000014  04 00 88 	mov w4,_DSRPAG
 894 000016  00 00 00 	nop 
 895 000018  00 00 FA 	lnk #0
 896              	.LCFI19:
 897              	.section .isr.text,code,keep
 265:lib/lib_pic33e/UART.c ****     //check for parity errors
 266:lib/lib_pic33e/UART.c ****     if(U1STAbits.PERR){
 898              	.loc 1 266 0
 899 00001a  00 00 80 	mov _U1STAbits,w0
 900 00001c  68 00 60 	and w0,#8,w0
 901 00001e  00 00 E0 	cp0 w0
 902              	.set ___BP___,0
 903 000020  00 00 32 	bra z,.L32
 904              	.section .isr.text,code,keep
 267:lib/lib_pic33e/UART.c ****         uart1_errors_status.parity = 1;
 905              	.loc 1 267 0
 906 000022  10 00 20 	mov #_uart1_errors_status+1,w0
MPLAB XC16 ASSEMBLY Listing:   			page 22


 907 000024  11 C0 B3 	mov.b #1,w1
 908 000026  01 48 78 	mov.b w1,[w0]
 909              	.L32:
 910              	.section .isr.text,code,keep
 268:lib/lib_pic33e/UART.c ****     }
 269:lib/lib_pic33e/UART.c ****     //check for framing error - stop bits
 270:lib/lib_pic33e/UART.c ****     if(U1STAbits.FERR){
 911              	.loc 1 270 0
 912 000028  00 00 80 	mov _U1STAbits,w0
 913 00002a  64 00 60 	and w0,#4,w0
 914 00002c  00 00 E0 	cp0 w0
 915              	.set ___BP___,0
 916 00002e  00 00 32 	bra z,.L33
 917              	.section .isr.text,code,keep
 271:lib/lib_pic33e/UART.c ****         uart1_errors_status.framing = 1;
 918              	.loc 1 271 0
 919 000030  20 00 20 	mov #_uart1_errors_status+2,w0
 920 000032  11 C0 B3 	mov.b #1,w1
 921 000034  01 48 78 	mov.b w1,[w0]
 922              	.L33:
 923              	.section .isr.text,code,keep
 272:lib/lib_pic33e/UART.c ****     }
 273:lib/lib_pic33e/UART.c **** 
 274:lib/lib_pic33e/UART.c ****     //get message from the fifo and put it in the UART1_BUFFER
 275:lib/lib_pic33e/UART.c ****     UART1_BUFFER[uart1_write] = get_FIFO_data(1);
 924              	.loc 1 275 0
 925 000036  08 00 80 	mov _uart1_write,w8
 926 000038  10 00 20 	mov #1,w0
 927 00003a  00 00 07 	rcall _get_FIFO_data
 928              	.section .isr.text,code,keep
 276:lib/lib_pic33e/UART.c ****     uart1_write = (uart1_write+1) % UART_BUFFER_LENGHT;
 277:lib/lib_pic33e/UART.c **** 
 278:lib/lib_pic33e/UART.c ****     IFS0bits.U1RXIF = 0;                                /*clear interrupt flag                     
 929              	.loc 1 278 0
 930 00003c  01 60 A9 	bclr.b _IFS0bits+1,#3
 931              	.section .isr.text,code,keep
 932              	.loc 1 276 0
 933 00003e  01 00 80 	mov _uart1_write,w1
 934              	.section .isr.text,code,keep
 935              	.loc 1 275 0
 936 000040  80 02 FB 	se w0,w5
 937              	.section .isr.text,code,keep
 938              	.loc 1 276 0
 939 000042  01 01 E8 	inc w1,w2
 940              	.section .isr.text,code,keep
 941              	.loc 1 275 0
 942 000044  88 01 44 	add w8,w8,w3
 943 000046  01 00 20 	mov #_UART1_BUFFER,w1
 944              	.section .isr.text,code,keep
 945              	.loc 1 276 0
 946 000048  F0 A0 2E 	mov #-5617,w0
 947              	.section .isr.text,code,keep
 948              	.loc 1 275 0
 949 00004a  01 82 41 	add w3,w1,w4
 950              	.section .isr.text,code,keep
 951              	.loc 1 276 0
 952 00004c  00 10 B8 	mul.uu w2,w0,w0
MPLAB XC16 ASSEMBLY Listing:   			page 23


 953 00004e  63 04 20 	mov #70,w3
 954 000050  46 08 DE 	lsr w1,#6,w0
 955              	.section .isr.text,code,keep
 956              	.loc 1 275 0
 957 000052  05 0A 78 	mov w5,[w4]
 958              	.section .isr.text,code,keep
 959              	.loc 1 276 0
 960 000054  83 80 B9 	mulw.ss w0,w3,w0
 961 000056  00 00 51 	sub w2,w0,w0
 962 000058  00 00 88 	mov w0,_uart1_write
 963              	.section .isr.text,code,keep
 279:lib/lib_pic33e/UART.c **** 
 280:lib/lib_pic33e/UART.c **** 
 281:lib/lib_pic33e/UART.c **** 
 282:lib/lib_pic33e/UART.c **** }
 964              	.loc 1 282 0
 965 00005a  8E 07 78 	mov w14,w15
 966 00005c  4F 07 78 	mov [--w15],w14
 967 00005e  00 40 A9 	bclr CORCON,#2
 968 000060  00 00 F9 	pop _DSWPAG
 969 000062  00 00 F9 	pop _DSRPAG
 970 000064  80 1F 78 	mov w0,[w15++]
 971 000066  00 0E 20 	mov #224,w0
 972 000068  00 20 B7 	ior _SR
 973 00006a  4F 00 78 	mov [--w15],w0
 974 00006c  4F 04 78 	mov [--w15],w8
 975 00006e  4F 03 BE 	mov.d [--w15],w6
 976 000070  4F 02 BE 	mov.d [--w15],w4
 977 000072  00 00 F9 	pop _RCOUNT
 978 000074  00 80 FE 	pop.s 
 979 000076  00 40 06 	retfie 
 980              	.set ___PA___,0
 981              	.LFE10:
 982              	.size __U1RXInterrupt,.-__U1RXInterrupt
 983              	.section .isr.text,code,keep
 984              	.align 2
 985              	.global __U2RXInterrupt
 986              	.type __U2RXInterrupt,@function
 987              	__U2RXInterrupt:
 988              	.section .isr.text,code,keep
 989              	.LFB11:
 990              	.section .isr.text,code,keep
 283:lib/lib_pic33e/UART.c **** 
 284:lib/lib_pic33e/UART.c **** /**********************************************************************
 285:lib/lib_pic33e/UART.c ****  * Name:    U2RX receive interrupt
 286:lib/lib_pic33e/UART.c ****  * Args:    -
 287:lib/lib_pic33e/UART.c ****  * Return:  -
 288:lib/lib_pic33e/UART.c ****  * Desc:    call upon UART2 receiving (interrupts every char received)
 289:lib/lib_pic33e/UART.c ****  **********************************************************************/
 290:lib/lib_pic33e/UART.c **** void __attribute__((interrupt, auto_psv, shadow)) _U2RXInterrupt (void){
 991              	.loc 1 290 0
 992              	.set ___PA___,1
 993 000078  00 A0 FE 	push.s 
 994 00007a  00 00 F8 	push _RCOUNT
 995              	.LCFI20:
 996 00007c  84 9F BE 	mov.d w4,[w15++]
 997              	.LCFI21:
MPLAB XC16 ASSEMBLY Listing:   			page 24


 998 00007e  86 9F BE 	mov.d w6,[w15++]
 999              	.LCFI22:
 1000 000080  88 1F 78 	mov w8,[w15++]
 1001              	.LCFI23:
 1002 000082  00 00 F8 	push _DSRPAG
 1003              	.LCFI24:
 1004 000084  00 00 F8 	push _DSWPAG
 1005              	.LCFI25:
 1006 000086  14 00 20 	mov #1,w4
 1007 000088  04 00 88 	mov w4,_DSWPAG
 1008 00008a  04 00 20 	mov #__const_psvpage,w4
 1009 00008c  04 00 88 	mov w4,_DSRPAG
 1010 00008e  00 00 00 	nop 
 1011 000090  00 00 FA 	lnk #0
 1012              	.LCFI26:
 1013              	.section .isr.text,code,keep
 291:lib/lib_pic33e/UART.c ****     //check for parity errors
 292:lib/lib_pic33e/UART.c ****     if(U2STAbits.PERR){
 1014              	.loc 1 292 0
 1015 000092  00 00 80 	mov _U2STAbits,w0
 1016 000094  68 00 60 	and w0,#8,w0
 1017 000096  00 00 E0 	cp0 w0
 1018              	.set ___BP___,0
 1019 000098  00 00 32 	bra z,.L35
 1020              	.section .isr.text,code,keep
 293:lib/lib_pic33e/UART.c ****         uart2_errors_status.parity = 1;
 1021              	.loc 1 293 0
 1022 00009a  10 00 20 	mov #_uart2_errors_status+1,w0
 1023 00009c  11 C0 B3 	mov.b #1,w1
 1024 00009e  01 48 78 	mov.b w1,[w0]
 1025              	.L35:
 1026              	.section .isr.text,code,keep
 294:lib/lib_pic33e/UART.c ****     }
 295:lib/lib_pic33e/UART.c ****     //check for framing error - stop bits
 296:lib/lib_pic33e/UART.c ****     if(U2STAbits.FERR){
 1027              	.loc 1 296 0
 1028 0000a0  00 00 80 	mov _U2STAbits,w0
 1029 0000a2  64 00 60 	and w0,#4,w0
 1030 0000a4  00 00 E0 	cp0 w0
 1031              	.set ___BP___,0
 1032 0000a6  00 00 32 	bra z,.L36
 1033              	.section .isr.text,code,keep
 297:lib/lib_pic33e/UART.c ****         uart2_errors_status.framing = 1;
 1034              	.loc 1 297 0
 1035 0000a8  20 00 20 	mov #_uart2_errors_status+2,w0
 1036 0000aa  11 C0 B3 	mov.b #1,w1
 1037 0000ac  01 48 78 	mov.b w1,[w0]
 1038              	.L36:
 1039              	.section .isr.text,code,keep
 298:lib/lib_pic33e/UART.c ****     }
 299:lib/lib_pic33e/UART.c ****     //get message from the fifo and put it in the UART2_BUFFER
 300:lib/lib_pic33e/UART.c ****     UART2_BUFFER[uart2_write] = get_FIFO_data(2);
 1040              	.loc 1 300 0
 1041 0000ae  08 00 80 	mov _uart2_write,w8
 1042 0000b0  20 00 20 	mov #2,w0
 1043 0000b2  00 00 07 	rcall _get_FIFO_data
 1044              	.section .isr.text,code,keep
MPLAB XC16 ASSEMBLY Listing:   			page 25


 301:lib/lib_pic33e/UART.c ****     uart2_write = (uart2_write+1) % UART_BUFFER_LENGHT;
 302:lib/lib_pic33e/UART.c **** 
 303:lib/lib_pic33e/UART.c ****     IFS1bits.U2RXIF = 0;                                /*clear interrupt flag                     
 1045              	.loc 1 303 0
 1046 0000b4  01 C0 A9 	bclr.b _IFS1bits+1,#6
 1047              	.section .isr.text,code,keep
 1048              	.loc 1 301 0
 1049 0000b6  01 00 80 	mov _uart2_write,w1
 1050              	.section .isr.text,code,keep
 1051              	.loc 1 300 0
 1052 0000b8  80 02 FB 	se w0,w5
 1053              	.section .isr.text,code,keep
 1054              	.loc 1 301 0
 1055 0000ba  01 01 E8 	inc w1,w2
 1056              	.section .isr.text,code,keep
 1057              	.loc 1 300 0
 1058 0000bc  88 01 44 	add w8,w8,w3
 1059 0000be  01 00 20 	mov #_UART2_BUFFER,w1
 1060              	.section .isr.text,code,keep
 1061              	.loc 1 301 0
 1062 0000c0  F0 A0 2E 	mov #-5617,w0
 1063              	.section .isr.text,code,keep
 1064              	.loc 1 300 0
 1065 0000c2  01 82 41 	add w3,w1,w4
 1066              	.section .isr.text,code,keep
 1067              	.loc 1 301 0
 1068 0000c4  00 10 B8 	mul.uu w2,w0,w0
 1069 0000c6  63 04 20 	mov #70,w3
 1070 0000c8  46 08 DE 	lsr w1,#6,w0
 1071              	.section .isr.text,code,keep
 1072              	.loc 1 300 0
 1073 0000ca  05 0A 78 	mov w5,[w4]
 1074              	.section .isr.text,code,keep
 1075              	.loc 1 301 0
 1076 0000cc  83 80 B9 	mulw.ss w0,w3,w0
 1077 0000ce  00 00 51 	sub w2,w0,w0
 1078 0000d0  00 00 88 	mov w0,_uart2_write
 1079              	.section .isr.text,code,keep
 304:lib/lib_pic33e/UART.c **** 
 305:lib/lib_pic33e/UART.c **** }
 1080              	.loc 1 305 0
 1081 0000d2  8E 07 78 	mov w14,w15
 1082 0000d4  4F 07 78 	mov [--w15],w14
 1083 0000d6  00 40 A9 	bclr CORCON,#2
 1084 0000d8  00 00 F9 	pop _DSWPAG
 1085 0000da  00 00 F9 	pop _DSRPAG
 1086 0000dc  80 1F 78 	mov w0,[w15++]
 1087 0000de  00 0E 20 	mov #224,w0
 1088 0000e0  00 20 B7 	ior _SR
 1089 0000e2  4F 00 78 	mov [--w15],w0
 1090 0000e4  4F 04 78 	mov [--w15],w8
 1091 0000e6  4F 03 BE 	mov.d [--w15],w6
 1092 0000e8  4F 02 BE 	mov.d [--w15],w4
 1093 0000ea  00 00 F9 	pop _RCOUNT
 1094 0000ec  00 80 FE 	pop.s 
 1095 0000ee  00 40 06 	retfie 
 1096              	.set ___PA___,0
MPLAB XC16 ASSEMBLY Listing:   			page 26


 1097              	.LFE11:
 1098              	.size __U2RXInterrupt,.-__U2RXInterrupt
 1099              	.section .isr.text,code,keep
 1100              	.align 2
 1101              	.global __U1ErrInterrupt
 1102              	.type __U1ErrInterrupt,@function
 1103              	__U1ErrInterrupt:
 1104              	.section .isr.text,code,keep
 1105              	.LFB12:
 1106              	.section .isr.text,code,keep
 306:lib/lib_pic33e/UART.c **** 
 307:lib/lib_pic33e/UART.c **** /**********************************************************************
 308:lib/lib_pic33e/UART.c ****  * Name:    U1RX receive error interrupt
 309:lib/lib_pic33e/UART.c ****  * Args:    -
 310:lib/lib_pic33e/UART.c ****  * Return:  -
 311:lib/lib_pic33e/UART.c ****  * Desc:    called upon uart1 receiving error
 312:lib/lib_pic33e/UART.c ****  **********************************************************************/
 313:lib/lib_pic33e/UART.c **** void __attribute__((interrupt, auto_psv, shadow)) _U1ErrInterrupt (void){
 1107              	.loc 1 313 0
 1108              	.set ___PA___,1
 1109 0000f0  00 A0 FE 	push.s 
 1110 0000f2  00 00 F8 	push _RCOUNT
 1111              	.LCFI27:
 1112 0000f4  84 9F BE 	mov.d w4,[w15++]
 1113              	.LCFI28:
 1114 0000f6  86 9F BE 	mov.d w6,[w15++]
 1115              	.LCFI29:
 1116 0000f8  88 1F 78 	mov w8,[w15++]
 1117              	.LCFI30:
 1118 0000fa  00 00 F8 	push _DSRPAG
 1119              	.LCFI31:
 1120 0000fc  00 00 F8 	push _DSWPAG
 1121              	.LCFI32:
 1122 0000fe  14 00 20 	mov #1,w4
 1123 000100  04 00 88 	mov w4,_DSWPAG
 1124 000102  04 00 20 	mov #__const_psvpage,w4
 1125 000104  04 00 88 	mov w4,_DSRPAG
 1126 000106  00 00 00 	nop 
 1127 000108  02 00 FA 	lnk #2
 1128              	.LCFI33:
 1129              	.section .isr.text,code,keep
 314:lib/lib_pic33e/UART.c ****     int i;
 315:lib/lib_pic33e/UART.c ****     //check for overflow error
 316:lib/lib_pic33e/UART.c ****     if(U1STAbits.OERR == 1){
 1130              	.loc 1 316 0
 1131 00010a  00 00 80 	mov _U1STAbits,w0
 1132 00010c  62 00 60 	and w0,#2,w0
 1133 00010e  00 00 E0 	cp0 w0
 1134              	.set ___BP___,0
 1135 000110  00 00 32 	bra z,.L38
 1136              	.section .isr.text,code,keep
 317:lib/lib_pic33e/UART.c ****         uart1_errors_status.overflow = 1;
 1137              	.loc 1 317 0
 1138 000112  01 00 20 	mov #_uart1_errors_status,w1
 1139 000114  12 C0 B3 	mov.b #1,w2
 1140              	.section .isr.text,code,keep
 318:lib/lib_pic33e/UART.c ****         //in case of overflow get every message from the FIFO before continuing
MPLAB XC16 ASSEMBLY Listing:   			page 27


 319:lib/lib_pic33e/UART.c ****         for(i=0; i<4; i++){
 1141              	.loc 1 319 0
 1142 000116  00 00 EB 	clr w0
 1143              	.section .isr.text,code,keep
 1144              	.loc 1 317 0
 1145 000118  82 48 78 	mov.b w2,[w1]
 1146              	.section .isr.text,code,keep
 1147              	.loc 1 319 0
 1148 00011a  00 0F 78 	mov w0,[w14]
 1149 00011c  00 00 37 	bra .L39
 1150              	.L40:
 1151              	.section .isr.text,code,keep
 320:lib/lib_pic33e/UART.c ****             UART1_BUFFER[uart1_write] = get_FIFO_data(1);
 1152              	.loc 1 320 0
 1153 00011e  08 00 80 	mov _uart1_write,w8
 1154 000120  10 00 20 	mov #1,w0
 1155 000122  00 00 07 	rcall _get_FIFO_data
 1156              	.section .isr.text,code,keep
 1157              	.loc 1 319 0
 1158 000124  1E 0F E8 	inc [w14],[w14]
 1159              	.section .isr.text,code,keep
 321:lib/lib_pic33e/UART.c ****             uart1_write = (uart1_write+1) % UART_BUFFER_LENGHT;
 1160              	.loc 1 321 0
 1161 000126  01 00 80 	mov _uart1_write,w1
 1162              	.section .isr.text,code,keep
 1163              	.loc 1 320 0
 1164 000128  80 02 FB 	se w0,w5
 1165              	.section .isr.text,code,keep
 1166              	.loc 1 321 0
 1167 00012a  01 01 E8 	inc w1,w2
 1168              	.section .isr.text,code,keep
 1169              	.loc 1 320 0
 1170 00012c  88 01 44 	add w8,w8,w3
 1171 00012e  01 00 20 	mov #_UART1_BUFFER,w1
 1172              	.section .isr.text,code,keep
 1173              	.loc 1 321 0
 1174 000130  F0 A0 2E 	mov #-5617,w0
 1175              	.section .isr.text,code,keep
 1176              	.loc 1 320 0
 1177 000132  01 82 41 	add w3,w1,w4
 1178              	.section .isr.text,code,keep
 1179              	.loc 1 321 0
 1180 000134  00 10 B8 	mul.uu w2,w0,w0
 1181 000136  63 04 20 	mov #70,w3
 1182 000138  46 08 DE 	lsr w1,#6,w0
 1183              	.section .isr.text,code,keep
 1184              	.loc 1 320 0
 1185 00013a  05 0A 78 	mov w5,[w4]
 1186              	.section .isr.text,code,keep
 1187              	.loc 1 321 0
 1188 00013c  83 80 B9 	mulw.ss w0,w3,w0
 1189 00013e  00 00 51 	sub w2,w0,w0
 1190 000140  00 00 88 	mov w0,_uart1_write
 1191              	.L39:
 1192              	.section .isr.text,code,keep
 1193              	.loc 1 319 0
 1194 000142  1E 00 78 	mov [w14],w0
MPLAB XC16 ASSEMBLY Listing:   			page 28


 1195 000144  E3 0F 50 	sub w0,#3,[w15]
 1196              	.set ___BP___,0
 1197 000146  00 00 34 	bra le,.L40
 1198              	.section .isr.text,code,keep
 322:lib/lib_pic33e/UART.c ****         } 
 323:lib/lib_pic33e/UART.c ****         //clear error - THIS NEEDS TO BE DONE MANUALLY
 324:lib/lib_pic33e/UART.c ****         U1STAbits.OERR = 0;  
 1199              	.loc 1 324 0
 1200 000148  00 20 A9 	bclr.b _U1STAbits,#1
 1201              	.L38:
 1202              	.section .isr.text,code,keep
 325:lib/lib_pic33e/UART.c ****     }
 326:lib/lib_pic33e/UART.c ****     IFS4bits.U1EIF = 0;                                 /*clear interrupt flag                     
 1203              	.loc 1 326 0
 1204 00014a  00 20 A9 	bclr.b _IFS4bits,#1
 1205              	.section .isr.text,code,keep
 327:lib/lib_pic33e/UART.c **** 
 328:lib/lib_pic33e/UART.c **** }
 1206              	.loc 1 328 0
 1207 00014c  8E 07 78 	mov w14,w15
 1208 00014e  4F 07 78 	mov [--w15],w14
 1209 000150  00 40 A9 	bclr CORCON,#2
 1210 000152  00 00 F9 	pop _DSWPAG
 1211 000154  00 00 F9 	pop _DSRPAG
 1212 000156  80 1F 78 	mov w0,[w15++]
 1213 000158  00 0E 20 	mov #224,w0
 1214 00015a  00 20 B7 	ior _SR
 1215 00015c  4F 00 78 	mov [--w15],w0
 1216 00015e  4F 04 78 	mov [--w15],w8
 1217 000160  4F 03 BE 	mov.d [--w15],w6
 1218 000162  4F 02 BE 	mov.d [--w15],w4
 1219 000164  00 00 F9 	pop _RCOUNT
 1220 000166  00 80 FE 	pop.s 
 1221 000168  00 40 06 	retfie 
 1222              	.set ___PA___,0
 1223              	.LFE12:
 1224              	.size __U1ErrInterrupt,.-__U1ErrInterrupt
 1225              	.section .isr.text,code,keep
 1226              	.align 2
 1227              	.global __U2ErrInterrupt
 1228              	.type __U2ErrInterrupt,@function
 1229              	__U2ErrInterrupt:
 1230              	.section .isr.text,code,keep
 1231              	.LFB13:
 1232              	.section .isr.text,code,keep
 329:lib/lib_pic33e/UART.c **** 
 330:lib/lib_pic33e/UART.c **** /**********************************************************************
 331:lib/lib_pic33e/UART.c ****  * Name:    U2RX receive error interrupt
 332:lib/lib_pic33e/UART.c ****  * Args:    -
 333:lib/lib_pic33e/UART.c ****  * Return:  -
 334:lib/lib_pic33e/UART.c ****  * Desc:    called upon uart1 receiving error
 335:lib/lib_pic33e/UART.c ****  **********************************************************************/
 336:lib/lib_pic33e/UART.c **** void __attribute__((interrupt, auto_psv, shadow)) _U2ErrInterrupt (void){
 1233              	.loc 1 336 0
 1234              	.set ___PA___,1
 1235 00016a  00 A0 FE 	push.s 
 1236 00016c  00 00 F8 	push _RCOUNT
MPLAB XC16 ASSEMBLY Listing:   			page 29


 1237              	.LCFI34:
 1238 00016e  84 9F BE 	mov.d w4,[w15++]
 1239              	.LCFI35:
 1240 000170  86 9F BE 	mov.d w6,[w15++]
 1241              	.LCFI36:
 1242 000172  88 1F 78 	mov w8,[w15++]
 1243              	.LCFI37:
 1244 000174  00 00 F8 	push _DSRPAG
 1245              	.LCFI38:
 1246 000176  00 00 F8 	push _DSWPAG
 1247              	.LCFI39:
 1248 000178  14 00 20 	mov #1,w4
 1249 00017a  04 00 88 	mov w4,_DSWPAG
 1250 00017c  04 00 20 	mov #__const_psvpage,w4
 1251 00017e  04 00 88 	mov w4,_DSRPAG
 1252 000180  00 00 00 	nop 
 1253 000182  02 00 FA 	lnk #2
 1254              	.LCFI40:
 1255              	.section .isr.text,code,keep
 337:lib/lib_pic33e/UART.c ****     int i;
 338:lib/lib_pic33e/UART.c ****     //check for overflow error
 339:lib/lib_pic33e/UART.c ****     if(U2STAbits.OERR == 1){
 1256              	.loc 1 339 0
 1257 000184  00 00 80 	mov _U2STAbits,w0
 1258 000186  62 00 60 	and w0,#2,w0
 1259 000188  00 00 E0 	cp0 w0
 1260              	.set ___BP___,0
 1261 00018a  00 00 32 	bra z,.L42
 1262              	.section .isr.text,code,keep
 340:lib/lib_pic33e/UART.c ****         uart2_errors_status.overflow = 1;
 1263              	.loc 1 340 0
 1264 00018c  01 00 20 	mov #_uart2_errors_status,w1
 1265 00018e  12 C0 B3 	mov.b #1,w2
 1266              	.section .isr.text,code,keep
 341:lib/lib_pic33e/UART.c ****         //in case of overflow get every message from the FIFO before continuing
 342:lib/lib_pic33e/UART.c ****         for(i=0; i<4; i++){
 1267              	.loc 1 342 0
 1268 000190  00 00 EB 	clr w0
 1269              	.section .isr.text,code,keep
 1270              	.loc 1 340 0
 1271 000192  82 48 78 	mov.b w2,[w1]
 1272              	.section .isr.text,code,keep
 1273              	.loc 1 342 0
 1274 000194  00 0F 78 	mov w0,[w14]
 1275 000196  00 00 37 	bra .L43
 1276              	.L44:
 1277              	.section .isr.text,code,keep
 343:lib/lib_pic33e/UART.c ****             UART2_BUFFER[uart2_write] = get_FIFO_data(2);
 1278              	.loc 1 343 0
 1279 000198  08 00 80 	mov _uart2_write,w8
 1280 00019a  20 00 20 	mov #2,w0
 1281 00019c  00 00 07 	rcall _get_FIFO_data
 1282              	.section .isr.text,code,keep
 1283              	.loc 1 342 0
 1284 00019e  1E 0F E8 	inc [w14],[w14]
 1285              	.section .isr.text,code,keep
 344:lib/lib_pic33e/UART.c ****             uart2_write = (uart2_write+1) % UART_BUFFER_LENGHT;
MPLAB XC16 ASSEMBLY Listing:   			page 30


 1286              	.loc 1 344 0
 1287 0001a0  01 00 80 	mov _uart2_write,w1
 1288              	.section .isr.text,code,keep
 1289              	.loc 1 343 0
 1290 0001a2  80 02 FB 	se w0,w5
 1291              	.section .isr.text,code,keep
 1292              	.loc 1 344 0
 1293 0001a4  01 01 E8 	inc w1,w2
 1294              	.section .isr.text,code,keep
 1295              	.loc 1 343 0
 1296 0001a6  88 01 44 	add w8,w8,w3
 1297 0001a8  01 00 20 	mov #_UART2_BUFFER,w1
 1298              	.section .isr.text,code,keep
 1299              	.loc 1 344 0
 1300 0001aa  F0 A0 2E 	mov #-5617,w0
 1301              	.section .isr.text,code,keep
 1302              	.loc 1 343 0
 1303 0001ac  01 82 41 	add w3,w1,w4
 1304              	.section .isr.text,code,keep
 1305              	.loc 1 344 0
 1306 0001ae  00 10 B8 	mul.uu w2,w0,w0
 1307 0001b0  63 04 20 	mov #70,w3
 1308 0001b2  46 08 DE 	lsr w1,#6,w0
 1309              	.section .isr.text,code,keep
 1310              	.loc 1 343 0
 1311 0001b4  05 0A 78 	mov w5,[w4]
 1312              	.section .isr.text,code,keep
 1313              	.loc 1 344 0
 1314 0001b6  83 80 B9 	mulw.ss w0,w3,w0
 1315 0001b8  00 00 51 	sub w2,w0,w0
 1316 0001ba  00 00 88 	mov w0,_uart2_write
 1317              	.L43:
 1318              	.section .isr.text,code,keep
 1319              	.loc 1 342 0
 1320 0001bc  1E 00 78 	mov [w14],w0
 1321 0001be  E3 0F 50 	sub w0,#3,[w15]
 1322              	.set ___BP___,0
 1323 0001c0  00 00 34 	bra le,.L44
 1324              	.section .isr.text,code,keep
 345:lib/lib_pic33e/UART.c ****         } 
 346:lib/lib_pic33e/UART.c ****         //clear error - THIS NEEDS TO BE DONE MANUALLY
 347:lib/lib_pic33e/UART.c ****         U2STAbits.OERR = 0;  
 1325              	.loc 1 347 0
 1326 0001c2  00 20 A9 	bclr.b _U2STAbits,#1
 1327              	.L42:
 1328              	.section .isr.text,code,keep
 348:lib/lib_pic33e/UART.c ****     }
 349:lib/lib_pic33e/UART.c ****     IFS4bits.U2EIF = 0;                                 /*clear interrupt flag                     
 1329              	.loc 1 349 0
 1330 0001c4  00 40 A9 	bclr.b _IFS4bits,#2
 1331              	.section .isr.text,code,keep
 350:lib/lib_pic33e/UART.c **** 
 351:lib/lib_pic33e/UART.c **** }
 1332              	.loc 1 351 0
 1333 0001c6  8E 07 78 	mov w14,w15
 1334 0001c8  4F 07 78 	mov [--w15],w14
 1335 0001ca  00 40 A9 	bclr CORCON,#2
MPLAB XC16 ASSEMBLY Listing:   			page 31


 1336 0001cc  00 00 F9 	pop _DSWPAG
 1337 0001ce  00 00 F9 	pop _DSRPAG
 1338 0001d0  80 1F 78 	mov w0,[w15++]
 1339 0001d2  00 0E 20 	mov #224,w0
 1340 0001d4  00 20 B7 	ior _SR
 1341 0001d6  4F 00 78 	mov [--w15],w0
 1342 0001d8  4F 04 78 	mov [--w15],w8
 1343 0001da  4F 03 BE 	mov.d [--w15],w6
 1344 0001dc  4F 02 BE 	mov.d [--w15],w4
 1345 0001de  00 00 F9 	pop _RCOUNT
 1346 0001e0  00 80 FE 	pop.s 
 1347 0001e2  00 40 06 	retfie 
 1348              	.set ___PA___,0
 1349              	.LFE13:
 1350              	.size __U2ErrInterrupt,.-__U2ErrInterrupt
 1351              	.section .text,code
 1352              	.align 2
 1353              	.global _get_uart1_errors
 1354              	.type _get_uart1_errors,@function
 1355              	_get_uart1_errors:
 1356              	.LFB14:
 352:lib/lib_pic33e/UART.c **** 
 353:lib/lib_pic33e/UART.c **** /**********************************************************************
 354:lib/lib_pic33e/UART.c ****  * Name:    get_uart1_errors
 355:lib/lib_pic33e/UART.c ****  * Args:    -
 356:lib/lib_pic33e/UART.c ****  * Return:  uart erros struct
 357:lib/lib_pic33e/UART.c ****  * Desc:    -
 358:lib/lib_pic33e/UART.c ****  **********************************************************************/
 359:lib/lib_pic33e/UART.c **** UART_errors get_uart1_errors(void){
 1357              	.loc 1 359 0
 1358              	.set ___PA___,1
 1359 0003f2  00 00 FA 	lnk #0
 1360              	.LCFI41:
 360:lib/lib_pic33e/UART.c ****     return uart1_errors_status;
 1361              	.loc 1 360 0
 1362 0003f4  01 00 20 	mov #_uart1_errors_status,w1
 1363              	.loc 1 359 0
 1364 0003f6  00 01 78 	mov w0,w2
 1365              	.loc 1 360 0
 1366 0003f8  02 00 78 	mov w2,w0
 1367 0003fa  11 48 78 	mov.b [w1],[w0]
 1368 0003fc  81 00 E8 	inc w1,w1
 1369 0003fe  00 00 E8 	inc w0,w0
 1370 000400  11 48 78 	mov.b [w1],[w0]
 1371 000402  81 00 E8 	inc w1,w1
 1372 000404  00 00 E8 	inc w0,w0
 1373 000406  11 48 78 	mov.b [w1],[w0]
 361:lib/lib_pic33e/UART.c **** }
 1374              	.loc 1 361 0
 1375 000408  02 00 78 	mov w2,w0
 1376 00040a  8E 07 78 	mov w14,w15
 1377 00040c  4F 07 78 	mov [--w15],w14
 1378 00040e  00 40 A9 	bclr CORCON,#2
 1379 000410  00 00 06 	return 
 1380              	.set ___PA___,0
 1381              	.LFE14:
 1382              	.size _get_uart1_errors,.-_get_uart1_errors
MPLAB XC16 ASSEMBLY Listing:   			page 32


 1383              	.align 2
 1384              	.global _get_uart2_errors
 1385              	.type _get_uart2_errors,@function
 1386              	_get_uart2_errors:
 1387              	.LFB15:
 362:lib/lib_pic33e/UART.c **** 
 363:lib/lib_pic33e/UART.c **** /**********************************************************************
 364:lib/lib_pic33e/UART.c ****  * Name:    get_uart2_errors
 365:lib/lib_pic33e/UART.c ****  * Args:    -
 366:lib/lib_pic33e/UART.c ****  * Return:  uart erros struct
 367:lib/lib_pic33e/UART.c ****  * Desc:    -
 368:lib/lib_pic33e/UART.c ****  **********************************************************************/
 369:lib/lib_pic33e/UART.c **** UART_errors get_uart2_errors(void){
 1388              	.loc 1 369 0
 1389              	.set ___PA___,1
 1390 000412  00 00 FA 	lnk #0
 1391              	.LCFI42:
 370:lib/lib_pic33e/UART.c ****     return uart2_errors_status;
 1392              	.loc 1 370 0
 1393 000414  01 00 20 	mov #_uart2_errors_status,w1
 1394              	.loc 1 369 0
 1395 000416  00 01 78 	mov w0,w2
 1396              	.loc 1 370 0
 1397 000418  02 00 78 	mov w2,w0
 1398 00041a  11 48 78 	mov.b [w1],[w0]
 1399 00041c  81 00 E8 	inc w1,w1
 1400 00041e  00 00 E8 	inc w0,w0
 1401 000420  11 48 78 	mov.b [w1],[w0]
 1402 000422  81 00 E8 	inc w1,w1
 1403 000424  00 00 E8 	inc w0,w0
 1404 000426  11 48 78 	mov.b [w1],[w0]
 371:lib/lib_pic33e/UART.c **** }
 1405              	.loc 1 371 0
 1406 000428  02 00 78 	mov w2,w0
 1407 00042a  8E 07 78 	mov w14,w15
 1408 00042c  4F 07 78 	mov [--w15],w14
 1409 00042e  00 40 A9 	bclr CORCON,#2
 1410 000430  00 00 06 	return 
 1411              	.set ___PA___,0
 1412              	.LFE15:
 1413              	.size _get_uart2_errors,.-_get_uart2_errors
 1414              	.align 2
 1415              	.global _pop_uart1
 1416              	.type _pop_uart1,@function
 1417              	_pop_uart1:
 1418              	.LFB16:
 372:lib/lib_pic33e/UART.c **** 
 373:lib/lib_pic33e/UART.c **** /**********************************************************************
 374:lib/lib_pic33e/UART.c ****  * Name:    pop_uart1
 375:lib/lib_pic33e/UART.c ****  * Args:    -
 376:lib/lib_pic33e/UART.c ****  * Return:  char (uart message from the UART1_BUFFER)
 377:lib/lib_pic33e/UART.c ****  * Desc:    -
 378:lib/lib_pic33e/UART.c ****  **********************************************************************/
 379:lib/lib_pic33e/UART.c **** char pop_uart1(void){
 1419              	.loc 1 379 0
 1420              	.set ___PA___,1
 1421 000432  02 00 FA 	lnk #2
MPLAB XC16 ASSEMBLY Listing:   			page 33


 1422              	.LCFI43:
 380:lib/lib_pic33e/UART.c ****     unsigned int aux;
 381:lib/lib_pic33e/UART.c ****     aux = uart1_read;
 1423              	.loc 1 381 0
 1424 000434  01 00 80 	mov _uart1_read,w1
 1425 000436  01 0F 78 	mov w1,[w14]
 382:lib/lib_pic33e/UART.c ****     uart1_read = (uart1_read + 1) % UART_BUFFER_LENGHT;
 383:lib/lib_pic33e/UART.c **** 	return UART1_BUFFER[aux];
 1426              	.loc 1 383 0
 1427 000438  01 00 20 	mov #_UART1_BUFFER,w1
 1428 00043a  1E 00 78 	mov [w14],w0
 1429 00043c  00 00 40 	add w0,w0,w0
 1430 00043e  01 02 40 	add w0,w1,w4
 1431              	.loc 1 382 0
 1432 000440  01 00 80 	mov _uart1_read,w1
 1433 000442  F0 A0 2E 	mov #-5617,w0
 1434 000444  01 01 E8 	inc w1,w2
 1435 000446  63 04 20 	mov #70,w3
 1436 000448  00 10 B8 	mul.uu w2,w0,w0
 1437              	.loc 1 383 0
 1438 00044a  14 02 78 	mov [w4],w4
 1439              	.loc 1 382 0
 1440 00044c  46 08 DE 	lsr w1,#6,w0
 1441              	.loc 1 383 0
 1442 00044e  84 40 78 	mov.b w4,w1
 1443              	.loc 1 382 0
 1444 000450  83 80 B9 	mulw.ss w0,w3,w0
 1445 000452  00 00 51 	sub w2,w0,w0
 1446 000454  00 00 88 	mov w0,_uart1_read
 384:lib/lib_pic33e/UART.c **** }
 1447              	.loc 1 384 0
 1448 000456  01 40 78 	mov.b w1,w0
 1449 000458  8E 07 78 	mov w14,w15
 1450 00045a  4F 07 78 	mov [--w15],w14
 1451 00045c  00 40 A9 	bclr CORCON,#2
 1452 00045e  00 00 06 	return 
 1453              	.set ___PA___,0
 1454              	.LFE16:
 1455              	.size _pop_uart1,.-_pop_uart1
 1456              	.align 2
 1457              	.global _pop_uart2
 1458              	.type _pop_uart2,@function
 1459              	_pop_uart2:
 1460              	.LFB17:
 385:lib/lib_pic33e/UART.c **** 
 386:lib/lib_pic33e/UART.c **** /**********************************************************************
 387:lib/lib_pic33e/UART.c ****  * Name:    pop_uart2
 388:lib/lib_pic33e/UART.c ****  * Args:    -
 389:lib/lib_pic33e/UART.c ****  * Return:  char (uart message from the UART2_BUFFER)
 390:lib/lib_pic33e/UART.c ****  * Desc:    -
 391:lib/lib_pic33e/UART.c ****  **********************************************************************/
 392:lib/lib_pic33e/UART.c **** char pop_uart2(void){
 1461              	.loc 1 392 0
 1462              	.set ___PA___,1
 1463 000460  02 00 FA 	lnk #2
 1464              	.LCFI44:
 393:lib/lib_pic33e/UART.c ****     unsigned int aux;
MPLAB XC16 ASSEMBLY Listing:   			page 34


 394:lib/lib_pic33e/UART.c ****     aux = uart2_read;
 1465              	.loc 1 394 0
 1466 000462  01 00 80 	mov _uart2_read,w1
 1467 000464  01 0F 78 	mov w1,[w14]
 395:lib/lib_pic33e/UART.c ****     uart2_read = (uart2_read + 1) % UART_BUFFER_LENGHT;
 396:lib/lib_pic33e/UART.c **** 	return UART2_BUFFER[aux];
 1468              	.loc 1 396 0
 1469 000466  01 00 20 	mov #_UART2_BUFFER,w1
 1470 000468  1E 00 78 	mov [w14],w0
 1471 00046a  00 00 40 	add w0,w0,w0
 1472 00046c  01 02 40 	add w0,w1,w4
 1473              	.loc 1 395 0
 1474 00046e  01 00 80 	mov _uart2_read,w1
 1475 000470  F0 A0 2E 	mov #-5617,w0
 1476 000472  01 01 E8 	inc w1,w2
 1477 000474  63 04 20 	mov #70,w3
 1478 000476  00 10 B8 	mul.uu w2,w0,w0
 1479              	.loc 1 396 0
 1480 000478  14 02 78 	mov [w4],w4
 1481              	.loc 1 395 0
 1482 00047a  46 08 DE 	lsr w1,#6,w0
 1483              	.loc 1 396 0
 1484 00047c  84 40 78 	mov.b w4,w1
 1485              	.loc 1 395 0
 1486 00047e  83 80 B9 	mulw.ss w0,w3,w0
 1487 000480  00 00 51 	sub w2,w0,w0
 1488 000482  00 00 88 	mov w0,_uart2_read
 397:lib/lib_pic33e/UART.c **** }
 1489              	.loc 1 397 0
 1490 000484  01 40 78 	mov.b w1,w0
 1491 000486  8E 07 78 	mov w14,w15
 1492 000488  4F 07 78 	mov [--w15],w14
 1493 00048a  00 40 A9 	bclr CORCON,#2
 1494 00048c  00 00 06 	return 
 1495              	.set ___PA___,0
 1496              	.LFE17:
 1497              	.size _pop_uart2,.-_pop_uart2
 1498              	.align 2
 1499              	.global _uartRX1_empty
 1500              	.type _uartRX1_empty,@function
 1501              	_uartRX1_empty:
 1502              	.LFB18:
 398:lib/lib_pic33e/UART.c **** 
 399:lib/lib_pic33e/UART.c **** /**********************************************************************
 400:lib/lib_pic33e/UART.c ****  * Name:    uartRX1_empty
 401:lib/lib_pic33e/UART.c ****  * Args:    -
 402:lib/lib_pic33e/UART.c ****  * Return:  bool (TRUE if UART1_BUFFER is empty)
 403:lib/lib_pic33e/UART.c ****  * Desc:    checks if there is anything to read in the UART1_BUFFER
 404:lib/lib_pic33e/UART.c ****  **********************************************************************/
 405:lib/lib_pic33e/UART.c **** bool uartRX1_empty(void){
 1503              	.loc 1 405 0
 1504              	.set ___PA___,1
 1505 00048e  00 00 FA 	lnk #0
 1506              	.LCFI45:
 406:lib/lib_pic33e/UART.c ****     if(uart1_read == uart1_write){
 1507              	.loc 1 406 0
 1508 000490  01 00 80 	mov _uart1_read,w1
MPLAB XC16 ASSEMBLY Listing:   			page 35


 1509 000492  00 00 80 	mov _uart1_write,w0
 1510 000494  80 8F 50 	sub w1,w0,[w15]
 1511              	.set ___BP___,0
 1512 000496  00 00 3A 	bra nz,.L50
 407:lib/lib_pic33e/UART.c ****         return true;
 1513              	.loc 1 407 0
 1514 000498  10 C0 B3 	mov.b #1,w0
 1515 00049a  00 00 37 	bra .L51
 1516              	.L50:
 408:lib/lib_pic33e/UART.c ****     }else{
 409:lib/lib_pic33e/UART.c ****         return false;
 1517              	.loc 1 409 0
 1518 00049c  00 40 EB 	clr.b w0
 1519              	.L51:
 410:lib/lib_pic33e/UART.c ****         }
 411:lib/lib_pic33e/UART.c **** }
 1520              	.loc 1 411 0
 1521 00049e  8E 07 78 	mov w14,w15
 1522 0004a0  4F 07 78 	mov [--w15],w14
 1523 0004a2  00 40 A9 	bclr CORCON,#2
 1524 0004a4  00 00 06 	return 
 1525              	.set ___PA___,0
 1526              	.LFE18:
 1527              	.size _uartRX1_empty,.-_uartRX1_empty
 1528              	.align 2
 1529              	.global _uartRX2_empty
 1530              	.type _uartRX2_empty,@function
 1531              	_uartRX2_empty:
 1532              	.LFB19:
 412:lib/lib_pic33e/UART.c **** 
 413:lib/lib_pic33e/UART.c **** /**********************************************************************
 414:lib/lib_pic33e/UART.c ****  * Name:    uartRX2_empty
 415:lib/lib_pic33e/UART.c ****  * Args:    -
 416:lib/lib_pic33e/UART.c ****  * Return:  bool (TRUE if UART2_BUFFER is empty)
 417:lib/lib_pic33e/UART.c ****  * Desc:    checks if there is anything to read in the UART2_BUFFER
 418:lib/lib_pic33e/UART.c ****  **********************************************************************/
 419:lib/lib_pic33e/UART.c **** bool uartRX2_empty(void){
 1533              	.loc 1 419 0
 1534              	.set ___PA___,1
 1535 0004a6  00 00 FA 	lnk #0
 1536              	.LCFI46:
 420:lib/lib_pic33e/UART.c ****     if(uart2_read == uart2_write){
 1537              	.loc 1 420 0
 1538 0004a8  01 00 80 	mov _uart2_read,w1
 1539 0004aa  00 00 80 	mov _uart2_write,w0
 1540 0004ac  80 8F 50 	sub w1,w0,[w15]
 1541              	.set ___BP___,0
 1542 0004ae  00 00 3A 	bra nz,.L53
 421:lib/lib_pic33e/UART.c ****         return true;
 1543              	.loc 1 421 0
 1544 0004b0  10 C0 B3 	mov.b #1,w0
 1545 0004b2  00 00 37 	bra .L54
 1546              	.L53:
 422:lib/lib_pic33e/UART.c ****     }else{
 423:lib/lib_pic33e/UART.c ****         return false;
 1547              	.loc 1 423 0
 1548 0004b4  00 40 EB 	clr.b w0
MPLAB XC16 ASSEMBLY Listing:   			page 36


 1549              	.L54:
 424:lib/lib_pic33e/UART.c ****         }
 425:lib/lib_pic33e/UART.c **** }
 1550              	.loc 1 425 0
 1551 0004b6  8E 07 78 	mov w14,w15
 1552 0004b8  4F 07 78 	mov [--w15],w14
 1553 0004ba  00 40 A9 	bclr CORCON,#2
 1554 0004bc  00 00 06 	return 
 1555              	.set ___PA___,0
 1556              	.LFE19:
 1557              	.size _uartRX2_empty,.-_uartRX2_empty
 1558              	.section .debug_frame,info
 1559                 	.Lframe0:
 1560 0000 10 00 00 00 	.4byte .LECIE0-.LSCIE0
 1561                 	.LSCIE0:
 1562 0004 FF FF FF FF 	.4byte 0xffffffff
 1563 0008 01          	.byte 0x1
 1564 0009 00          	.byte 0
 1565 000a 01          	.uleb128 0x1
 1566 000b 02          	.sleb128 2
 1567 000c 25          	.byte 0x25
 1568 000d 12          	.byte 0x12
 1569 000e 0F          	.uleb128 0xf
 1570 000f 7E          	.sleb128 -2
 1571 0010 09          	.byte 0x9
 1572 0011 25          	.uleb128 0x25
 1573 0012 0F          	.uleb128 0xf
 1574 0013 00          	.align 4
 1575                 	.LECIE0:
 1576                 	.LSFDE0:
 1577 0014 1E 00 00 00 	.4byte .LEFDE0-.LASFDE0
 1578                 	.LASFDE0:
 1579 0018 00 00 00 00 	.4byte .Lframe0
 1580 001c 00 00 00 00 	.4byte .LFB0
 1581 0020 18 01 00 00 	.4byte .LFE0-.LFB0
 1582 0024 04          	.byte 0x4
 1583 0025 02 00 00 00 	.4byte .LCFI0-.LFB0
 1584 0029 13          	.byte 0x13
 1585 002a 7D          	.sleb128 -3
 1586 002b 0D          	.byte 0xd
 1587 002c 0E          	.uleb128 0xe
 1588 002d 8E          	.byte 0x8e
 1589 002e 02          	.uleb128 0x2
 1590 002f 04          	.byte 0x4
 1591 0030 02 00 00 00 	.4byte .LCFI1-.LCFI0
 1592 0034 88          	.byte 0x88
 1593 0035 0C          	.uleb128 0xc
 1594                 	.align 4
 1595                 	.LEFDE0:
 1596                 	.LSFDE2:
 1597 0036 1E 00 00 00 	.4byte .LEFDE2-.LASFDE2
 1598                 	.LASFDE2:
 1599 003a 00 00 00 00 	.4byte .Lframe0
 1600 003e 00 00 00 00 	.4byte .LFB1
 1601 0042 28 01 00 00 	.4byte .LFE1-.LFB1
 1602 0046 04          	.byte 0x4
 1603 0047 02 00 00 00 	.4byte .LCFI2-.LFB1
MPLAB XC16 ASSEMBLY Listing:   			page 37


 1604 004b 13          	.byte 0x13
 1605 004c 7D          	.sleb128 -3
 1606 004d 0D          	.byte 0xd
 1607 004e 0E          	.uleb128 0xe
 1608 004f 8E          	.byte 0x8e
 1609 0050 02          	.uleb128 0x2
 1610 0051 04          	.byte 0x4
 1611 0052 02 00 00 00 	.4byte .LCFI3-.LCFI2
 1612 0056 88          	.byte 0x88
 1613 0057 04          	.uleb128 0x4
 1614                 	.align 4
 1615                 	.LEFDE2:
 1616                 	.LSFDE4:
 1617 0058 1E 00 00 00 	.4byte .LEFDE4-.LASFDE4
 1618                 	.LASFDE4:
 1619 005c 00 00 00 00 	.4byte .Lframe0
 1620 0060 00 00 00 00 	.4byte .LFB2
 1621 0064 28 01 00 00 	.4byte .LFE2-.LFB2
 1622 0068 04          	.byte 0x4
 1623 0069 02 00 00 00 	.4byte .LCFI4-.LFB2
 1624 006d 13          	.byte 0x13
 1625 006e 7D          	.sleb128 -3
 1626 006f 0D          	.byte 0xd
 1627 0070 0E          	.uleb128 0xe
 1628 0071 8E          	.byte 0x8e
 1629 0072 02          	.uleb128 0x2
 1630 0073 04          	.byte 0x4
 1631 0074 02 00 00 00 	.4byte .LCFI5-.LCFI4
 1632 0078 88          	.byte 0x88
 1633 0079 04          	.uleb128 0x4
 1634                 	.align 4
 1635                 	.LEFDE4:
 1636                 	.LSFDE6:
 1637 007a 18 00 00 00 	.4byte .LEFDE6-.LASFDE6
 1638                 	.LASFDE6:
 1639 007e 00 00 00 00 	.4byte .Lframe0
 1640 0082 00 00 00 00 	.4byte .LFB3
 1641 0086 0C 00 00 00 	.4byte .LFE3-.LFB3
 1642 008a 04          	.byte 0x4
 1643 008b 02 00 00 00 	.4byte .LCFI6-.LFB3
 1644 008f 13          	.byte 0x13
 1645 0090 7D          	.sleb128 -3
 1646 0091 0D          	.byte 0xd
 1647 0092 0E          	.uleb128 0xe
 1648 0093 8E          	.byte 0x8e
 1649 0094 02          	.uleb128 0x2
 1650 0095 00          	.align 4
 1651                 	.LEFDE6:
 1652                 	.LSFDE8:
 1653 0096 18 00 00 00 	.4byte .LEFDE8-.LASFDE8
 1654                 	.LASFDE8:
 1655 009a 00 00 00 00 	.4byte .Lframe0
 1656 009e 00 00 00 00 	.4byte .LFB4
 1657 00a2 0C 00 00 00 	.4byte .LFE4-.LFB4
 1658 00a6 04          	.byte 0x4
 1659 00a7 02 00 00 00 	.4byte .LCFI7-.LFB4
 1660 00ab 13          	.byte 0x13
MPLAB XC16 ASSEMBLY Listing:   			page 38


 1661 00ac 7D          	.sleb128 -3
 1662 00ad 0D          	.byte 0xd
 1663 00ae 0E          	.uleb128 0xe
 1664 00af 8E          	.byte 0x8e
 1665 00b0 02          	.uleb128 0x2
 1666 00b1 00          	.align 4
 1667                 	.LEFDE8:
 1668                 	.LSFDE10:
 1669 00b2 18 00 00 00 	.4byte .LEFDE10-.LASFDE10
 1670                 	.LASFDE10:
 1671 00b6 00 00 00 00 	.4byte .Lframe0
 1672 00ba 00 00 00 00 	.4byte .LFB5
 1673 00be 0C 00 00 00 	.4byte .LFE5-.LFB5
 1674 00c2 04          	.byte 0x4
 1675 00c3 02 00 00 00 	.4byte .LCFI8-.LFB5
 1676 00c7 13          	.byte 0x13
 1677 00c8 7D          	.sleb128 -3
 1678 00c9 0D          	.byte 0xd
 1679 00ca 0E          	.uleb128 0xe
 1680 00cb 8E          	.byte 0x8e
 1681 00cc 02          	.uleb128 0x2
 1682 00cd 00          	.align 4
 1683                 	.LEFDE10:
 1684                 	.LSFDE12:
 1685 00ce 18 00 00 00 	.4byte .LEFDE12-.LASFDE12
 1686                 	.LASFDE12:
 1687 00d2 00 00 00 00 	.4byte .Lframe0
 1688 00d6 00 00 00 00 	.4byte .LFB6
 1689 00da 0C 00 00 00 	.4byte .LFE6-.LFB6
 1690 00de 04          	.byte 0x4
 1691 00df 02 00 00 00 	.4byte .LCFI9-.LFB6
 1692 00e3 13          	.byte 0x13
 1693 00e4 7D          	.sleb128 -3
 1694 00e5 0D          	.byte 0xd
 1695 00e6 0E          	.uleb128 0xe
 1696 00e7 8E          	.byte 0x8e
 1697 00e8 02          	.uleb128 0x2
 1698 00e9 00          	.align 4
 1699                 	.LEFDE12:
 1700                 	.LSFDE14:
 1701 00ea 18 00 00 00 	.4byte .LEFDE14-.LASFDE14
 1702                 	.LASFDE14:
 1703 00ee 00 00 00 00 	.4byte .Lframe0
 1704 00f2 00 00 00 00 	.4byte .LFB7
 1705 00f6 18 00 00 00 	.4byte .LFE7-.LFB7
 1706 00fa 04          	.byte 0x4
 1707 00fb 02 00 00 00 	.4byte .LCFI10-.LFB7
 1708 00ff 13          	.byte 0x13
 1709 0100 7D          	.sleb128 -3
 1710 0101 0D          	.byte 0xd
 1711 0102 0E          	.uleb128 0xe
 1712 0103 8E          	.byte 0x8e
 1713 0104 02          	.uleb128 0x2
 1714 0105 00          	.align 4
 1715                 	.LEFDE14:
 1716                 	.LSFDE16:
 1717 0106 18 00 00 00 	.4byte .LEFDE16-.LASFDE16
MPLAB XC16 ASSEMBLY Listing:   			page 39


 1718                 	.LASFDE16:
 1719 010a 00 00 00 00 	.4byte .Lframe0
 1720 010e 00 00 00 00 	.4byte .LFB8
 1721 0112 18 00 00 00 	.4byte .LFE8-.LFB8
 1722 0116 04          	.byte 0x4
 1723 0117 02 00 00 00 	.4byte .LCFI11-.LFB8
 1724 011b 13          	.byte 0x13
 1725 011c 7D          	.sleb128 -3
 1726 011d 0D          	.byte 0xd
 1727 011e 0E          	.uleb128 0xe
 1728 011f 8E          	.byte 0x8e
 1729 0120 02          	.uleb128 0x2
 1730 0121 00          	.align 4
 1731                 	.LEFDE16:
 1732                 	.LSFDE18:
 1733 0122 18 00 00 00 	.4byte .LEFDE18-.LASFDE18
 1734                 	.LASFDE18:
 1735 0126 00 00 00 00 	.4byte .Lframe0
 1736 012a 00 00 00 00 	.4byte .LFB9
 1737 012e 2A 00 00 00 	.4byte .LFE9-.LFB9
 1738 0132 04          	.byte 0x4
 1739 0133 02 00 00 00 	.4byte .LCFI12-.LFB9
 1740 0137 13          	.byte 0x13
 1741 0138 7D          	.sleb128 -3
 1742 0139 0D          	.byte 0xd
 1743 013a 0E          	.uleb128 0xe
 1744 013b 8E          	.byte 0x8e
 1745 013c 02          	.uleb128 0x2
 1746 013d 00          	.align 4
 1747                 	.LEFDE18:
 1748                 	.LSFDE20:
 1749 013e 38 00 00 00 	.4byte .LEFDE20-.LASFDE20
 1750                 	.LASFDE20:
 1751 0142 00 00 00 00 	.4byte .Lframe0
 1752 0146 00 00 00 00 	.4byte .LFB10
 1753 014a 78 00 00 00 	.4byte .LFE10-.LFB10
 1754 014e 04          	.byte 0x4
 1755 014f 06 00 00 00 	.4byte .LCFI14-.LFB10
 1756 0153 13          	.byte 0x13
 1757 0154 7B          	.sleb128 -5
 1758 0155 04          	.byte 0x4
 1759 0156 02 00 00 00 	.4byte .LCFI15-.LCFI14
 1760 015a 13          	.byte 0x13
 1761 015b 79          	.sleb128 -7
 1762 015c 04          	.byte 0x4
 1763 015d 02 00 00 00 	.4byte .LCFI16-.LCFI15
 1764 0161 13          	.byte 0x13
 1765 0162 78          	.sleb128 -8
 1766 0163 04          	.byte 0x4
 1767 0164 04 00 00 00 	.4byte .LCFI18-.LCFI16
 1768 0168 88          	.byte 0x88
 1769 0169 07          	.uleb128 0x7
 1770 016a 86          	.byte 0x86
 1771 016b 05          	.uleb128 0x5
 1772 016c 84          	.byte 0x84
 1773 016d 03          	.uleb128 0x3
 1774 016e 04          	.byte 0x4
MPLAB XC16 ASSEMBLY Listing:   			page 40


 1775 016f 0C 00 00 00 	.4byte .LCFI19-.LCFI18
 1776 0173 13          	.byte 0x13
 1777 0174 75          	.sleb128 -11
 1778 0175 0D          	.byte 0xd
 1779 0176 0E          	.uleb128 0xe
 1780 0177 8E          	.byte 0x8e
 1781 0178 0A          	.uleb128 0xa
 1782 0179 00          	.align 4
 1783                 	.LEFDE20:
 1784                 	.LSFDE22:
 1785 017a 38 00 00 00 	.4byte .LEFDE22-.LASFDE22
 1786                 	.LASFDE22:
 1787 017e 00 00 00 00 	.4byte .Lframe0
 1788 0182 00 00 00 00 	.4byte .LFB11
 1789 0186 78 00 00 00 	.4byte .LFE11-.LFB11
 1790 018a 04          	.byte 0x4
 1791 018b 06 00 00 00 	.4byte .LCFI21-.LFB11
 1792 018f 13          	.byte 0x13
 1793 0190 7B          	.sleb128 -5
 1794 0191 04          	.byte 0x4
 1795 0192 02 00 00 00 	.4byte .LCFI22-.LCFI21
 1796 0196 13          	.byte 0x13
 1797 0197 79          	.sleb128 -7
 1798 0198 04          	.byte 0x4
 1799 0199 02 00 00 00 	.4byte .LCFI23-.LCFI22
 1800 019d 13          	.byte 0x13
 1801 019e 78          	.sleb128 -8
 1802 019f 04          	.byte 0x4
 1803 01a0 04 00 00 00 	.4byte .LCFI25-.LCFI23
 1804 01a4 88          	.byte 0x88
 1805 01a5 07          	.uleb128 0x7
 1806 01a6 86          	.byte 0x86
 1807 01a7 05          	.uleb128 0x5
 1808 01a8 84          	.byte 0x84
 1809 01a9 03          	.uleb128 0x3
 1810 01aa 04          	.byte 0x4
 1811 01ab 0C 00 00 00 	.4byte .LCFI26-.LCFI25
 1812 01af 13          	.byte 0x13
 1813 01b0 75          	.sleb128 -11
 1814 01b1 0D          	.byte 0xd
 1815 01b2 0E          	.uleb128 0xe
 1816 01b3 8E          	.byte 0x8e
 1817 01b4 0A          	.uleb128 0xa
 1818 01b5 00          	.align 4
 1819                 	.LEFDE22:
 1820                 	.LSFDE24:
 1821 01b6 38 00 00 00 	.4byte .LEFDE24-.LASFDE24
 1822                 	.LASFDE24:
 1823 01ba 00 00 00 00 	.4byte .Lframe0
 1824 01be 00 00 00 00 	.4byte .LFB12
 1825 01c2 7A 00 00 00 	.4byte .LFE12-.LFB12
 1826 01c6 04          	.byte 0x4
 1827 01c7 06 00 00 00 	.4byte .LCFI28-.LFB12
 1828 01cb 13          	.byte 0x13
 1829 01cc 7B          	.sleb128 -5
 1830 01cd 04          	.byte 0x4
 1831 01ce 02 00 00 00 	.4byte .LCFI29-.LCFI28
MPLAB XC16 ASSEMBLY Listing:   			page 41


 1832 01d2 13          	.byte 0x13
 1833 01d3 79          	.sleb128 -7
 1834 01d4 04          	.byte 0x4
 1835 01d5 02 00 00 00 	.4byte .LCFI30-.LCFI29
 1836 01d9 13          	.byte 0x13
 1837 01da 78          	.sleb128 -8
 1838 01db 04          	.byte 0x4
 1839 01dc 04 00 00 00 	.4byte .LCFI32-.LCFI30
 1840 01e0 88          	.byte 0x88
 1841 01e1 07          	.uleb128 0x7
 1842 01e2 86          	.byte 0x86
 1843 01e3 05          	.uleb128 0x5
 1844 01e4 84          	.byte 0x84
 1845 01e5 03          	.uleb128 0x3
 1846 01e6 04          	.byte 0x4
 1847 01e7 0C 00 00 00 	.4byte .LCFI33-.LCFI32
 1848 01eb 13          	.byte 0x13
 1849 01ec 75          	.sleb128 -11
 1850 01ed 0D          	.byte 0xd
 1851 01ee 0E          	.uleb128 0xe
 1852 01ef 8E          	.byte 0x8e
 1853 01f0 0A          	.uleb128 0xa
 1854 01f1 00          	.align 4
 1855                 	.LEFDE24:
 1856                 	.LSFDE26:
 1857 01f2 38 00 00 00 	.4byte .LEFDE26-.LASFDE26
 1858                 	.LASFDE26:
 1859 01f6 00 00 00 00 	.4byte .Lframe0
 1860 01fa 00 00 00 00 	.4byte .LFB13
 1861 01fe 7A 00 00 00 	.4byte .LFE13-.LFB13
 1862 0202 04          	.byte 0x4
 1863 0203 06 00 00 00 	.4byte .LCFI35-.LFB13
 1864 0207 13          	.byte 0x13
 1865 0208 7B          	.sleb128 -5
 1866 0209 04          	.byte 0x4
 1867 020a 02 00 00 00 	.4byte .LCFI36-.LCFI35
 1868 020e 13          	.byte 0x13
 1869 020f 79          	.sleb128 -7
 1870 0210 04          	.byte 0x4
 1871 0211 02 00 00 00 	.4byte .LCFI37-.LCFI36
 1872 0215 13          	.byte 0x13
 1873 0216 78          	.sleb128 -8
 1874 0217 04          	.byte 0x4
 1875 0218 04 00 00 00 	.4byte .LCFI39-.LCFI37
 1876 021c 88          	.byte 0x88
 1877 021d 07          	.uleb128 0x7
 1878 021e 86          	.byte 0x86
 1879 021f 05          	.uleb128 0x5
 1880 0220 84          	.byte 0x84
 1881 0221 03          	.uleb128 0x3
 1882 0222 04          	.byte 0x4
 1883 0223 0C 00 00 00 	.4byte .LCFI40-.LCFI39
 1884 0227 13          	.byte 0x13
 1885 0228 75          	.sleb128 -11
 1886 0229 0D          	.byte 0xd
 1887 022a 0E          	.uleb128 0xe
 1888 022b 8E          	.byte 0x8e
MPLAB XC16 ASSEMBLY Listing:   			page 42


 1889 022c 0A          	.uleb128 0xa
 1890 022d 00          	.align 4
 1891                 	.LEFDE26:
 1892                 	.LSFDE28:
 1893 022e 18 00 00 00 	.4byte .LEFDE28-.LASFDE28
 1894                 	.LASFDE28:
 1895 0232 00 00 00 00 	.4byte .Lframe0
 1896 0236 00 00 00 00 	.4byte .LFB14
 1897 023a 20 00 00 00 	.4byte .LFE14-.LFB14
 1898 023e 04          	.byte 0x4
 1899 023f 02 00 00 00 	.4byte .LCFI41-.LFB14
 1900 0243 13          	.byte 0x13
 1901 0244 7D          	.sleb128 -3
 1902 0245 0D          	.byte 0xd
 1903 0246 0E          	.uleb128 0xe
 1904 0247 8E          	.byte 0x8e
 1905 0248 02          	.uleb128 0x2
 1906 0249 00          	.align 4
 1907                 	.LEFDE28:
 1908                 	.LSFDE30:
 1909 024a 18 00 00 00 	.4byte .LEFDE30-.LASFDE30
 1910                 	.LASFDE30:
 1911 024e 00 00 00 00 	.4byte .Lframe0
 1912 0252 00 00 00 00 	.4byte .LFB15
 1913 0256 20 00 00 00 	.4byte .LFE15-.LFB15
 1914 025a 04          	.byte 0x4
 1915 025b 02 00 00 00 	.4byte .LCFI42-.LFB15
 1916 025f 13          	.byte 0x13
 1917 0260 7D          	.sleb128 -3
 1918 0261 0D          	.byte 0xd
 1919 0262 0E          	.uleb128 0xe
 1920 0263 8E          	.byte 0x8e
 1921 0264 02          	.uleb128 0x2
 1922 0265 00          	.align 4
 1923                 	.LEFDE30:
 1924                 	.LSFDE32:
 1925 0266 18 00 00 00 	.4byte .LEFDE32-.LASFDE32
 1926                 	.LASFDE32:
 1927 026a 00 00 00 00 	.4byte .Lframe0
 1928 026e 00 00 00 00 	.4byte .LFB16
 1929 0272 2E 00 00 00 	.4byte .LFE16-.LFB16
 1930 0276 04          	.byte 0x4
 1931 0277 02 00 00 00 	.4byte .LCFI43-.LFB16
 1932 027b 13          	.byte 0x13
 1933 027c 7D          	.sleb128 -3
 1934 027d 0D          	.byte 0xd
 1935 027e 0E          	.uleb128 0xe
 1936 027f 8E          	.byte 0x8e
 1937 0280 02          	.uleb128 0x2
 1938 0281 00          	.align 4
 1939                 	.LEFDE32:
 1940                 	.LSFDE34:
 1941 0282 18 00 00 00 	.4byte .LEFDE34-.LASFDE34
 1942                 	.LASFDE34:
 1943 0286 00 00 00 00 	.4byte .Lframe0
 1944 028a 00 00 00 00 	.4byte .LFB17
 1945 028e 2E 00 00 00 	.4byte .LFE17-.LFB17
MPLAB XC16 ASSEMBLY Listing:   			page 43


 1946 0292 04          	.byte 0x4
 1947 0293 02 00 00 00 	.4byte .LCFI44-.LFB17
 1948 0297 13          	.byte 0x13
 1949 0298 7D          	.sleb128 -3
 1950 0299 0D          	.byte 0xd
 1951 029a 0E          	.uleb128 0xe
 1952 029b 8E          	.byte 0x8e
 1953 029c 02          	.uleb128 0x2
 1954 029d 00          	.align 4
 1955                 	.LEFDE34:
 1956                 	.LSFDE36:
 1957 029e 18 00 00 00 	.4byte .LEFDE36-.LASFDE36
 1958                 	.LASFDE36:
 1959 02a2 00 00 00 00 	.4byte .Lframe0
 1960 02a6 00 00 00 00 	.4byte .LFB18
 1961 02aa 18 00 00 00 	.4byte .LFE18-.LFB18
 1962 02ae 04          	.byte 0x4
 1963 02af 02 00 00 00 	.4byte .LCFI45-.LFB18
 1964 02b3 13          	.byte 0x13
 1965 02b4 7D          	.sleb128 -3
 1966 02b5 0D          	.byte 0xd
 1967 02b6 0E          	.uleb128 0xe
 1968 02b7 8E          	.byte 0x8e
 1969 02b8 02          	.uleb128 0x2
 1970 02b9 00          	.align 4
 1971                 	.LEFDE36:
 1972                 	.LSFDE38:
 1973 02ba 18 00 00 00 	.4byte .LEFDE38-.LASFDE38
 1974                 	.LASFDE38:
 1975 02be 00 00 00 00 	.4byte .Lframe0
 1976 02c2 00 00 00 00 	.4byte .LFB19
 1977 02c6 18 00 00 00 	.4byte .LFE19-.LFB19
 1978 02ca 04          	.byte 0x4
 1979 02cb 02 00 00 00 	.4byte .LCFI46-.LFB19
 1980 02cf 13          	.byte 0x13
 1981 02d0 7D          	.sleb128 -3
 1982 02d1 0D          	.byte 0xd
 1983 02d2 0E          	.uleb128 0xe
 1984 02d3 8E          	.byte 0x8e
 1985 02d4 02          	.uleb128 0x2
 1986 02d5 00          	.align 4
 1987                 	.LEFDE38:
 1988                 	.section .text,code
 1989              	.Letext0:
 1990              	.file 2 "/opt/microchip/xc16/v1.36/bin/bin/../../support/dsPIC33E/h/p33EP256MU806.h"
 1991              	.file 3 "/opt/microchip/xc16/v1.36/bin/bin/../../include/lega-c/stdint.h"
 1992              	.file 4 "lib/lib_pic33e/UART.h"
 1993              	.section .debug_info,info
 1994 0000 96 1C 00 00 	.4byte 0x1c96
 1995 0004 02 00       	.2byte 0x2
 1996 0006 00 00 00 00 	.4byte .Ldebug_abbrev0
 1997 000a 04          	.byte 0x4
 1998 000b 01          	.uleb128 0x1
 1999 000c 47 4E 55 20 	.asciz "GNU C 4.5.1 (XC16, Microchip v1.36) (A) Build date: Dec  5 2018"
 1999      43 20 34 2E 
 1999      35 2E 31 20 
 1999      28 58 43 31 
MPLAB XC16 ASSEMBLY Listing:   			page 44


 1999      36 2C 20 4D 
 1999      69 63 72 6F 
 1999      63 68 69 70 
 1999      20 76 31 2E 
 1999      33 36 29 20 
 2000 004c 01          	.byte 0x1
 2001 004d 6C 69 62 2F 	.asciz "lib/lib_pic33e/UART.c"
 2001      6C 69 62 5F 
 2001      70 69 63 33 
 2001      33 65 2F 55 
 2001      41 52 54 2E 
 2001      63 00 
 2002 0063 2F 68 6F 6D 	.asciz "/home/user/Documents/FST/Programming/project_sub-zero"
 2002      65 2F 75 73 
 2002      65 72 2F 44 
 2002      6F 63 75 6D 
 2002      65 6E 74 73 
 2002      2F 46 53 54 
 2002      2F 50 72 6F 
 2002      67 72 61 6D 
 2002      6D 69 6E 67 
 2003 0099 00 00 00 00 	.4byte .Ltext0
 2004 009d 00 00 00 00 	.4byte .Letext0
 2005 00a1 00 00 00 00 	.4byte .Ldebug_line0
 2006 00a5 02          	.uleb128 0x2
 2007 00a6 01          	.byte 0x1
 2008 00a7 06          	.byte 0x6
 2009 00a8 73 69 67 6E 	.asciz "signed char"
 2009      65 64 20 63 
 2009      68 61 72 00 
 2010 00b4 02          	.uleb128 0x2
 2011 00b5 02          	.byte 0x2
 2012 00b6 05          	.byte 0x5
 2013 00b7 69 6E 74 00 	.asciz "int"
 2014 00bb 02          	.uleb128 0x2
 2015 00bc 04          	.byte 0x4
 2016 00bd 05          	.byte 0x5
 2017 00be 6C 6F 6E 67 	.asciz "long int"
 2017      20 69 6E 74 
 2017      00 
 2018 00c7 02          	.uleb128 0x2
 2019 00c8 08          	.byte 0x8
 2020 00c9 05          	.byte 0x5
 2021 00ca 6C 6F 6E 67 	.asciz "long long int"
 2021      20 6C 6F 6E 
 2021      67 20 69 6E 
 2021      74 00 
 2022 00d8 02          	.uleb128 0x2
 2023 00d9 01          	.byte 0x1
 2024 00da 08          	.byte 0x8
 2025 00db 75 6E 73 69 	.asciz "unsigned char"
 2025      67 6E 65 64 
 2025      20 63 68 61 
 2025      72 00 
 2026 00e9 03          	.uleb128 0x3
 2027 00ea 75 69 6E 74 	.asciz "uint16_t"
 2027      31 36 5F 74 
MPLAB XC16 ASSEMBLY Listing:   			page 45


 2027      00 
 2028 00f3 03          	.byte 0x3
 2029 00f4 31          	.byte 0x31
 2030 00f5 F9 00 00 00 	.4byte 0xf9
 2031 00f9 02          	.uleb128 0x2
 2032 00fa 02          	.byte 0x2
 2033 00fb 07          	.byte 0x7
 2034 00fc 75 6E 73 69 	.asciz "unsigned int"
 2034      67 6E 65 64 
 2034      20 69 6E 74 
 2034      00 
 2035 0109 02          	.uleb128 0x2
 2036 010a 04          	.byte 0x4
 2037 010b 07          	.byte 0x7
 2038 010c 6C 6F 6E 67 	.asciz "long unsigned int"
 2038      20 75 6E 73 
 2038      69 67 6E 65 
 2038      64 20 69 6E 
 2038      74 00 
 2039 011e 02          	.uleb128 0x2
 2040 011f 08          	.byte 0x8
 2041 0120 07          	.byte 0x7
 2042 0121 6C 6F 6E 67 	.asciz "long long unsigned int"
 2042      20 6C 6F 6E 
 2042      67 20 75 6E 
 2042      73 69 67 6E 
 2042      65 64 20 69 
 2042      6E 74 00 
 2043 0138 04          	.uleb128 0x4
 2044 0139 02          	.byte 0x2
 2045 013a 02          	.byte 0x2
 2046 013b 8C 06       	.2byte 0x68c
 2047 013d 30 02 00 00 	.4byte 0x230
 2048 0141 05          	.uleb128 0x5
 2049 0142 53 54 53 45 	.asciz "STSEL"
 2049      4C 00 
 2050 0148 02          	.byte 0x2
 2051 0149 8D 06       	.2byte 0x68d
 2052 014b E9 00 00 00 	.4byte 0xe9
 2053 014f 02          	.byte 0x2
 2054 0150 01          	.byte 0x1
 2055 0151 0F          	.byte 0xf
 2056 0152 02          	.byte 0x2
 2057 0153 23          	.byte 0x23
 2058 0154 00          	.uleb128 0x0
 2059 0155 05          	.uleb128 0x5
 2060 0156 50 44 53 45 	.asciz "PDSEL"
 2060      4C 00 
 2061 015c 02          	.byte 0x2
 2062 015d 8E 06       	.2byte 0x68e
 2063 015f E9 00 00 00 	.4byte 0xe9
 2064 0163 02          	.byte 0x2
 2065 0164 02          	.byte 0x2
 2066 0165 0D          	.byte 0xd
 2067 0166 02          	.byte 0x2
 2068 0167 23          	.byte 0x23
 2069 0168 00          	.uleb128 0x0
MPLAB XC16 ASSEMBLY Listing:   			page 46


 2070 0169 05          	.uleb128 0x5
 2071 016a 42 52 47 48 	.asciz "BRGH"
 2071      00 
 2072 016f 02          	.byte 0x2
 2073 0170 8F 06       	.2byte 0x68f
 2074 0172 E9 00 00 00 	.4byte 0xe9
 2075 0176 02          	.byte 0x2
 2076 0177 01          	.byte 0x1
 2077 0178 0C          	.byte 0xc
 2078 0179 02          	.byte 0x2
 2079 017a 23          	.byte 0x23
 2080 017b 00          	.uleb128 0x0
 2081 017c 05          	.uleb128 0x5
 2082 017d 55 52 58 49 	.asciz "URXINV"
 2082      4E 56 00 
 2083 0184 02          	.byte 0x2
 2084 0185 90 06       	.2byte 0x690
 2085 0187 E9 00 00 00 	.4byte 0xe9
 2086 018b 02          	.byte 0x2
 2087 018c 01          	.byte 0x1
 2088 018d 0B          	.byte 0xb
 2089 018e 02          	.byte 0x2
 2090 018f 23          	.byte 0x23
 2091 0190 00          	.uleb128 0x0
 2092 0191 05          	.uleb128 0x5
 2093 0192 41 42 41 55 	.asciz "ABAUD"
 2093      44 00 
 2094 0198 02          	.byte 0x2
 2095 0199 91 06       	.2byte 0x691
 2096 019b E9 00 00 00 	.4byte 0xe9
 2097 019f 02          	.byte 0x2
 2098 01a0 01          	.byte 0x1
 2099 01a1 0A          	.byte 0xa
 2100 01a2 02          	.byte 0x2
 2101 01a3 23          	.byte 0x23
 2102 01a4 00          	.uleb128 0x0
 2103 01a5 05          	.uleb128 0x5
 2104 01a6 4C 50 42 41 	.asciz "LPBACK"
 2104      43 4B 00 
 2105 01ad 02          	.byte 0x2
 2106 01ae 92 06       	.2byte 0x692
 2107 01b0 E9 00 00 00 	.4byte 0xe9
 2108 01b4 02          	.byte 0x2
 2109 01b5 01          	.byte 0x1
 2110 01b6 09          	.byte 0x9
 2111 01b7 02          	.byte 0x2
 2112 01b8 23          	.byte 0x23
 2113 01b9 00          	.uleb128 0x0
 2114 01ba 05          	.uleb128 0x5
 2115 01bb 57 41 4B 45 	.asciz "WAKE"
 2115      00 
 2116 01c0 02          	.byte 0x2
 2117 01c1 93 06       	.2byte 0x693
 2118 01c3 E9 00 00 00 	.4byte 0xe9
 2119 01c7 02          	.byte 0x2
 2120 01c8 01          	.byte 0x1
 2121 01c9 08          	.byte 0x8
MPLAB XC16 ASSEMBLY Listing:   			page 47


 2122 01ca 02          	.byte 0x2
 2123 01cb 23          	.byte 0x23
 2124 01cc 00          	.uleb128 0x0
 2125 01cd 05          	.uleb128 0x5
 2126 01ce 55 45 4E 00 	.asciz "UEN"
 2127 01d2 02          	.byte 0x2
 2128 01d3 94 06       	.2byte 0x694
 2129 01d5 E9 00 00 00 	.4byte 0xe9
 2130 01d9 02          	.byte 0x2
 2131 01da 02          	.byte 0x2
 2132 01db 06          	.byte 0x6
 2133 01dc 02          	.byte 0x2
 2134 01dd 23          	.byte 0x23
 2135 01de 00          	.uleb128 0x0
 2136 01df 05          	.uleb128 0x5
 2137 01e0 52 54 53 4D 	.asciz "RTSMD"
 2137      44 00 
 2138 01e6 02          	.byte 0x2
 2139 01e7 96 06       	.2byte 0x696
 2140 01e9 E9 00 00 00 	.4byte 0xe9
 2141 01ed 02          	.byte 0x2
 2142 01ee 01          	.byte 0x1
 2143 01ef 04          	.byte 0x4
 2144 01f0 02          	.byte 0x2
 2145 01f1 23          	.byte 0x23
 2146 01f2 00          	.uleb128 0x0
 2147 01f3 05          	.uleb128 0x5
 2148 01f4 49 52 45 4E 	.asciz "IREN"
 2148      00 
 2149 01f9 02          	.byte 0x2
 2150 01fa 97 06       	.2byte 0x697
 2151 01fc E9 00 00 00 	.4byte 0xe9
 2152 0200 02          	.byte 0x2
 2153 0201 01          	.byte 0x1
 2154 0202 03          	.byte 0x3
 2155 0203 02          	.byte 0x2
 2156 0204 23          	.byte 0x23
 2157 0205 00          	.uleb128 0x0
 2158 0206 05          	.uleb128 0x5
 2159 0207 55 53 49 44 	.asciz "USIDL"
 2159      4C 00 
 2160 020d 02          	.byte 0x2
 2161 020e 98 06       	.2byte 0x698
 2162 0210 E9 00 00 00 	.4byte 0xe9
 2163 0214 02          	.byte 0x2
 2164 0215 01          	.byte 0x1
 2165 0216 02          	.byte 0x2
 2166 0217 02          	.byte 0x2
 2167 0218 23          	.byte 0x23
 2168 0219 00          	.uleb128 0x0
 2169 021a 05          	.uleb128 0x5
 2170 021b 55 41 52 54 	.asciz "UARTEN"
 2170      45 4E 00 
 2171 0222 02          	.byte 0x2
 2172 0223 9A 06       	.2byte 0x69a
 2173 0225 E9 00 00 00 	.4byte 0xe9
 2174 0229 02          	.byte 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 48


 2175 022a 01          	.byte 0x1
 2176 022b 10          	.byte 0x10
 2177 022c 02          	.byte 0x2
 2178 022d 23          	.byte 0x23
 2179 022e 00          	.uleb128 0x0
 2180 022f 00          	.byte 0x0
 2181 0230 04          	.uleb128 0x4
 2182 0231 02          	.byte 0x2
 2183 0232 02          	.byte 0x2
 2184 0233 9C 06       	.2byte 0x69c
 2185 0235 9E 02 00 00 	.4byte 0x29e
 2186 0239 05          	.uleb128 0x5
 2187 023a 50 44 53 45 	.asciz "PDSEL0"
 2187      4C 30 00 
 2188 0241 02          	.byte 0x2
 2189 0242 9E 06       	.2byte 0x69e
 2190 0244 E9 00 00 00 	.4byte 0xe9
 2191 0248 02          	.byte 0x2
 2192 0249 01          	.byte 0x1
 2193 024a 0E          	.byte 0xe
 2194 024b 02          	.byte 0x2
 2195 024c 23          	.byte 0x23
 2196 024d 00          	.uleb128 0x0
 2197 024e 05          	.uleb128 0x5
 2198 024f 50 44 53 45 	.asciz "PDSEL1"
 2198      4C 31 00 
 2199 0256 02          	.byte 0x2
 2200 0257 9F 06       	.2byte 0x69f
 2201 0259 E9 00 00 00 	.4byte 0xe9
 2202 025d 02          	.byte 0x2
 2203 025e 01          	.byte 0x1
 2204 025f 0D          	.byte 0xd
 2205 0260 02          	.byte 0x2
 2206 0261 23          	.byte 0x23
 2207 0262 00          	.uleb128 0x0
 2208 0263 05          	.uleb128 0x5
 2209 0264 52 58 49 4E 	.asciz "RXINV"
 2209      56 00 
 2210 026a 02          	.byte 0x2
 2211 026b A1 06       	.2byte 0x6a1
 2212 026d E9 00 00 00 	.4byte 0xe9
 2213 0271 02          	.byte 0x2
 2214 0272 01          	.byte 0x1
 2215 0273 0B          	.byte 0xb
 2216 0274 02          	.byte 0x2
 2217 0275 23          	.byte 0x23
 2218 0276 00          	.uleb128 0x0
 2219 0277 05          	.uleb128 0x5
 2220 0278 55 45 4E 30 	.asciz "UEN0"
 2220      00 
 2221 027d 02          	.byte 0x2
 2222 027e A3 06       	.2byte 0x6a3
 2223 0280 E9 00 00 00 	.4byte 0xe9
 2224 0284 02          	.byte 0x2
 2225 0285 01          	.byte 0x1
 2226 0286 07          	.byte 0x7
 2227 0287 02          	.byte 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 49


 2228 0288 23          	.byte 0x23
 2229 0289 00          	.uleb128 0x0
 2230 028a 05          	.uleb128 0x5
 2231 028b 55 45 4E 31 	.asciz "UEN1"
 2231      00 
 2232 0290 02          	.byte 0x2
 2233 0291 A4 06       	.2byte 0x6a4
 2234 0293 E9 00 00 00 	.4byte 0xe9
 2235 0297 02          	.byte 0x2
 2236 0298 01          	.byte 0x1
 2237 0299 06          	.byte 0x6
 2238 029a 02          	.byte 0x2
 2239 029b 23          	.byte 0x23
 2240 029c 00          	.uleb128 0x0
 2241 029d 00          	.byte 0x0
 2242 029e 06          	.uleb128 0x6
 2243 029f 02          	.byte 0x2
 2244 02a0 02          	.byte 0x2
 2245 02a1 8B 06       	.2byte 0x68b
 2246 02a3 B2 02 00 00 	.4byte 0x2b2
 2247 02a7 07          	.uleb128 0x7
 2248 02a8 38 01 00 00 	.4byte 0x138
 2249 02ac 07          	.uleb128 0x7
 2250 02ad 30 02 00 00 	.4byte 0x230
 2251 02b1 00          	.byte 0x0
 2252 02b2 08          	.uleb128 0x8
 2253 02b3 74 61 67 55 	.asciz "tagU1MODEBITS"
 2253      31 4D 4F 44 
 2253      45 42 49 54 
 2253      53 00 
 2254 02c1 02          	.byte 0x2
 2255 02c2 02          	.byte 0x2
 2256 02c3 8A 06       	.2byte 0x68a
 2257 02c5 D2 02 00 00 	.4byte 0x2d2
 2258 02c9 09          	.uleb128 0x9
 2259 02ca 9E 02 00 00 	.4byte 0x29e
 2260 02ce 02          	.byte 0x2
 2261 02cf 23          	.byte 0x23
 2262 02d0 00          	.uleb128 0x0
 2263 02d1 00          	.byte 0x0
 2264 02d2 0A          	.uleb128 0xa
 2265 02d3 55 31 4D 4F 	.asciz "U1MODEBITS"
 2265      44 45 42 49 
 2265      54 53 00 
 2266 02de 02          	.byte 0x2
 2267 02df A7 06       	.2byte 0x6a7
 2268 02e1 B2 02 00 00 	.4byte 0x2b2
 2269 02e5 04          	.uleb128 0x4
 2270 02e6 02          	.byte 0x2
 2271 02e7 02          	.byte 0x2
 2272 02e8 AE 06       	.2byte 0x6ae
 2273 02ea 03 04 00 00 	.4byte 0x403
 2274 02ee 05          	.uleb128 0x5
 2275 02ef 55 52 58 44 	.asciz "URXDA"
 2275      41 00 
 2276 02f5 02          	.byte 0x2
 2277 02f6 AF 06       	.2byte 0x6af
MPLAB XC16 ASSEMBLY Listing:   			page 50


 2278 02f8 E9 00 00 00 	.4byte 0xe9
 2279 02fc 02          	.byte 0x2
 2280 02fd 01          	.byte 0x1
 2281 02fe 0F          	.byte 0xf
 2282 02ff 02          	.byte 0x2
 2283 0300 23          	.byte 0x23
 2284 0301 00          	.uleb128 0x0
 2285 0302 05          	.uleb128 0x5
 2286 0303 4F 45 52 52 	.asciz "OERR"
 2286      00 
 2287 0308 02          	.byte 0x2
 2288 0309 B0 06       	.2byte 0x6b0
 2289 030b E9 00 00 00 	.4byte 0xe9
 2290 030f 02          	.byte 0x2
 2291 0310 01          	.byte 0x1
 2292 0311 0E          	.byte 0xe
 2293 0312 02          	.byte 0x2
 2294 0313 23          	.byte 0x23
 2295 0314 00          	.uleb128 0x0
 2296 0315 05          	.uleb128 0x5
 2297 0316 46 45 52 52 	.asciz "FERR"
 2297      00 
 2298 031b 02          	.byte 0x2
 2299 031c B1 06       	.2byte 0x6b1
 2300 031e E9 00 00 00 	.4byte 0xe9
 2301 0322 02          	.byte 0x2
 2302 0323 01          	.byte 0x1
 2303 0324 0D          	.byte 0xd
 2304 0325 02          	.byte 0x2
 2305 0326 23          	.byte 0x23
 2306 0327 00          	.uleb128 0x0
 2307 0328 05          	.uleb128 0x5
 2308 0329 50 45 52 52 	.asciz "PERR"
 2308      00 
 2309 032e 02          	.byte 0x2
 2310 032f B2 06       	.2byte 0x6b2
 2311 0331 E9 00 00 00 	.4byte 0xe9
 2312 0335 02          	.byte 0x2
 2313 0336 01          	.byte 0x1
 2314 0337 0C          	.byte 0xc
 2315 0338 02          	.byte 0x2
 2316 0339 23          	.byte 0x23
 2317 033a 00          	.uleb128 0x0
 2318 033b 05          	.uleb128 0x5
 2319 033c 52 49 44 4C 	.asciz "RIDLE"
 2319      45 00 
 2320 0342 02          	.byte 0x2
 2321 0343 B3 06       	.2byte 0x6b3
 2322 0345 E9 00 00 00 	.4byte 0xe9
 2323 0349 02          	.byte 0x2
 2324 034a 01          	.byte 0x1
 2325 034b 0B          	.byte 0xb
 2326 034c 02          	.byte 0x2
 2327 034d 23          	.byte 0x23
 2328 034e 00          	.uleb128 0x0
 2329 034f 05          	.uleb128 0x5
 2330 0350 41 44 44 45 	.asciz "ADDEN"
MPLAB XC16 ASSEMBLY Listing:   			page 51


 2330      4E 00 
 2331 0356 02          	.byte 0x2
 2332 0357 B4 06       	.2byte 0x6b4
 2333 0359 E9 00 00 00 	.4byte 0xe9
 2334 035d 02          	.byte 0x2
 2335 035e 01          	.byte 0x1
 2336 035f 0A          	.byte 0xa
 2337 0360 02          	.byte 0x2
 2338 0361 23          	.byte 0x23
 2339 0362 00          	.uleb128 0x0
 2340 0363 05          	.uleb128 0x5
 2341 0364 55 52 58 49 	.asciz "URXISEL"
 2341      53 45 4C 00 
 2342 036c 02          	.byte 0x2
 2343 036d B5 06       	.2byte 0x6b5
 2344 036f E9 00 00 00 	.4byte 0xe9
 2345 0373 02          	.byte 0x2
 2346 0374 02          	.byte 0x2
 2347 0375 08          	.byte 0x8
 2348 0376 02          	.byte 0x2
 2349 0377 23          	.byte 0x23
 2350 0378 00          	.uleb128 0x0
 2351 0379 05          	.uleb128 0x5
 2352 037a 54 52 4D 54 	.asciz "TRMT"
 2352      00 
 2353 037f 02          	.byte 0x2
 2354 0380 B6 06       	.2byte 0x6b6
 2355 0382 E9 00 00 00 	.4byte 0xe9
 2356 0386 02          	.byte 0x2
 2357 0387 01          	.byte 0x1
 2358 0388 07          	.byte 0x7
 2359 0389 02          	.byte 0x2
 2360 038a 23          	.byte 0x23
 2361 038b 00          	.uleb128 0x0
 2362 038c 05          	.uleb128 0x5
 2363 038d 55 54 58 42 	.asciz "UTXBF"
 2363      46 00 
 2364 0393 02          	.byte 0x2
 2365 0394 B7 06       	.2byte 0x6b7
 2366 0396 E9 00 00 00 	.4byte 0xe9
 2367 039a 02          	.byte 0x2
 2368 039b 01          	.byte 0x1
 2369 039c 06          	.byte 0x6
 2370 039d 02          	.byte 0x2
 2371 039e 23          	.byte 0x23
 2372 039f 00          	.uleb128 0x0
 2373 03a0 05          	.uleb128 0x5
 2374 03a1 55 54 58 45 	.asciz "UTXEN"
 2374      4E 00 
 2375 03a7 02          	.byte 0x2
 2376 03a8 B8 06       	.2byte 0x6b8
 2377 03aa E9 00 00 00 	.4byte 0xe9
 2378 03ae 02          	.byte 0x2
 2379 03af 01          	.byte 0x1
 2380 03b0 05          	.byte 0x5
 2381 03b1 02          	.byte 0x2
 2382 03b2 23          	.byte 0x23
MPLAB XC16 ASSEMBLY Listing:   			page 52


 2383 03b3 00          	.uleb128 0x0
 2384 03b4 05          	.uleb128 0x5
 2385 03b5 55 54 58 42 	.asciz "UTXBRK"
 2385      52 4B 00 
 2386 03bc 02          	.byte 0x2
 2387 03bd B9 06       	.2byte 0x6b9
 2388 03bf E9 00 00 00 	.4byte 0xe9
 2389 03c3 02          	.byte 0x2
 2390 03c4 01          	.byte 0x1
 2391 03c5 04          	.byte 0x4
 2392 03c6 02          	.byte 0x2
 2393 03c7 23          	.byte 0x23
 2394 03c8 00          	.uleb128 0x0
 2395 03c9 0B          	.uleb128 0xb
 2396 03ca 00 00 00 00 	.4byte .LASF0
 2397 03ce 02          	.byte 0x2
 2398 03cf BB 06       	.2byte 0x6bb
 2399 03d1 E9 00 00 00 	.4byte 0xe9
 2400 03d5 02          	.byte 0x2
 2401 03d6 01          	.byte 0x1
 2402 03d7 02          	.byte 0x2
 2403 03d8 02          	.byte 0x2
 2404 03d9 23          	.byte 0x23
 2405 03da 00          	.uleb128 0x0
 2406 03db 05          	.uleb128 0x5
 2407 03dc 55 54 58 49 	.asciz "UTXINV"
 2407      4E 56 00 
 2408 03e3 02          	.byte 0x2
 2409 03e4 BC 06       	.2byte 0x6bc
 2410 03e6 E9 00 00 00 	.4byte 0xe9
 2411 03ea 02          	.byte 0x2
 2412 03eb 01          	.byte 0x1
 2413 03ec 01          	.byte 0x1
 2414 03ed 02          	.byte 0x2
 2415 03ee 23          	.byte 0x23
 2416 03ef 00          	.uleb128 0x0
 2417 03f0 0B          	.uleb128 0xb
 2418 03f1 00 00 00 00 	.4byte .LASF1
 2419 03f5 02          	.byte 0x2
 2420 03f6 BD 06       	.2byte 0x6bd
 2421 03f8 E9 00 00 00 	.4byte 0xe9
 2422 03fc 02          	.byte 0x2
 2423 03fd 01          	.byte 0x1
 2424 03fe 10          	.byte 0x10
 2425 03ff 02          	.byte 0x2
 2426 0400 23          	.byte 0x23
 2427 0401 00          	.uleb128 0x0
 2428 0402 00          	.byte 0x0
 2429 0403 04          	.uleb128 0x4
 2430 0404 02          	.byte 0x2
 2431 0405 02          	.byte 0x2
 2432 0406 BF 06       	.2byte 0x6bf
 2433 0408 45 04 00 00 	.4byte 0x445
 2434 040c 0B          	.uleb128 0xb
 2435 040d 00 00 00 00 	.4byte .LASF2
 2436 0411 02          	.byte 0x2
 2437 0412 C1 06       	.2byte 0x6c1
MPLAB XC16 ASSEMBLY Listing:   			page 53


 2438 0414 E9 00 00 00 	.4byte 0xe9
 2439 0418 02          	.byte 0x2
 2440 0419 01          	.byte 0x1
 2441 041a 09          	.byte 0x9
 2442 041b 02          	.byte 0x2
 2443 041c 23          	.byte 0x23
 2444 041d 00          	.uleb128 0x0
 2445 041e 0B          	.uleb128 0xb
 2446 041f 00 00 00 00 	.4byte .LASF3
 2447 0423 02          	.byte 0x2
 2448 0424 C2 06       	.2byte 0x6c2
 2449 0426 E9 00 00 00 	.4byte 0xe9
 2450 042a 02          	.byte 0x2
 2451 042b 01          	.byte 0x1
 2452 042c 08          	.byte 0x8
 2453 042d 02          	.byte 0x2
 2454 042e 23          	.byte 0x23
 2455 042f 00          	.uleb128 0x0
 2456 0430 05          	.uleb128 0x5
 2457 0431 54 58 49 4E 	.asciz "TXINV"
 2457      56 00 
 2458 0437 02          	.byte 0x2
 2459 0438 C4 06       	.2byte 0x6c4
 2460 043a E9 00 00 00 	.4byte 0xe9
 2461 043e 02          	.byte 0x2
 2462 043f 01          	.byte 0x1
 2463 0440 01          	.byte 0x1
 2464 0441 02          	.byte 0x2
 2465 0442 23          	.byte 0x23
 2466 0443 00          	.uleb128 0x0
 2467 0444 00          	.byte 0x0
 2468 0445 06          	.uleb128 0x6
 2469 0446 02          	.byte 0x2
 2470 0447 02          	.byte 0x2
 2471 0448 AD 06       	.2byte 0x6ad
 2472 044a 59 04 00 00 	.4byte 0x459
 2473 044e 07          	.uleb128 0x7
 2474 044f E5 02 00 00 	.4byte 0x2e5
 2475 0453 07          	.uleb128 0x7
 2476 0454 03 04 00 00 	.4byte 0x403
 2477 0458 00          	.byte 0x0
 2478 0459 08          	.uleb128 0x8
 2479 045a 74 61 67 55 	.asciz "tagU1STABITS"
 2479      31 53 54 41 
 2479      42 49 54 53 
 2479      00 
 2480 0467 02          	.byte 0x2
 2481 0468 02          	.byte 0x2
 2482 0469 AC 06       	.2byte 0x6ac
 2483 046b 78 04 00 00 	.4byte 0x478
 2484 046f 09          	.uleb128 0x9
 2485 0470 45 04 00 00 	.4byte 0x445
 2486 0474 02          	.byte 0x2
 2487 0475 23          	.byte 0x23
 2488 0476 00          	.uleb128 0x0
 2489 0477 00          	.byte 0x0
 2490 0478 0A          	.uleb128 0xa
MPLAB XC16 ASSEMBLY Listing:   			page 54


 2491 0479 55 31 53 54 	.asciz "U1STABITS"
 2491      41 42 49 54 
 2491      53 00 
 2492 0483 02          	.byte 0x2
 2493 0484 C7 06       	.2byte 0x6c7
 2494 0486 59 04 00 00 	.4byte 0x459
 2495 048a 04          	.uleb128 0x4
 2496 048b 02          	.byte 0x2
 2497 048c 02          	.byte 0x2
 2498 048d D4 06       	.2byte 0x6d4
 2499 048f 82 05 00 00 	.4byte 0x582
 2500 0493 05          	.uleb128 0x5
 2501 0494 53 54 53 45 	.asciz "STSEL"
 2501      4C 00 
 2502 049a 02          	.byte 0x2
 2503 049b D5 06       	.2byte 0x6d5
 2504 049d E9 00 00 00 	.4byte 0xe9
 2505 04a1 02          	.byte 0x2
 2506 04a2 01          	.byte 0x1
 2507 04a3 0F          	.byte 0xf
 2508 04a4 02          	.byte 0x2
 2509 04a5 23          	.byte 0x23
 2510 04a6 00          	.uleb128 0x0
 2511 04a7 05          	.uleb128 0x5
 2512 04a8 50 44 53 45 	.asciz "PDSEL"
 2512      4C 00 
 2513 04ae 02          	.byte 0x2
 2514 04af D6 06       	.2byte 0x6d6
 2515 04b1 E9 00 00 00 	.4byte 0xe9
 2516 04b5 02          	.byte 0x2
 2517 04b6 02          	.byte 0x2
 2518 04b7 0D          	.byte 0xd
 2519 04b8 02          	.byte 0x2
 2520 04b9 23          	.byte 0x23
 2521 04ba 00          	.uleb128 0x0
 2522 04bb 05          	.uleb128 0x5
 2523 04bc 42 52 47 48 	.asciz "BRGH"
 2523      00 
 2524 04c1 02          	.byte 0x2
 2525 04c2 D7 06       	.2byte 0x6d7
 2526 04c4 E9 00 00 00 	.4byte 0xe9
 2527 04c8 02          	.byte 0x2
 2528 04c9 01          	.byte 0x1
 2529 04ca 0C          	.byte 0xc
 2530 04cb 02          	.byte 0x2
 2531 04cc 23          	.byte 0x23
 2532 04cd 00          	.uleb128 0x0
 2533 04ce 05          	.uleb128 0x5
 2534 04cf 55 52 58 49 	.asciz "URXINV"
 2534      4E 56 00 
 2535 04d6 02          	.byte 0x2
 2536 04d7 D8 06       	.2byte 0x6d8
 2537 04d9 E9 00 00 00 	.4byte 0xe9
 2538 04dd 02          	.byte 0x2
 2539 04de 01          	.byte 0x1
 2540 04df 0B          	.byte 0xb
 2541 04e0 02          	.byte 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 55


 2542 04e1 23          	.byte 0x23
 2543 04e2 00          	.uleb128 0x0
 2544 04e3 05          	.uleb128 0x5
 2545 04e4 41 42 41 55 	.asciz "ABAUD"
 2545      44 00 
 2546 04ea 02          	.byte 0x2
 2547 04eb D9 06       	.2byte 0x6d9
 2548 04ed E9 00 00 00 	.4byte 0xe9
 2549 04f1 02          	.byte 0x2
 2550 04f2 01          	.byte 0x1
 2551 04f3 0A          	.byte 0xa
 2552 04f4 02          	.byte 0x2
 2553 04f5 23          	.byte 0x23
 2554 04f6 00          	.uleb128 0x0
 2555 04f7 05          	.uleb128 0x5
 2556 04f8 4C 50 42 41 	.asciz "LPBACK"
 2556      43 4B 00 
 2557 04ff 02          	.byte 0x2
 2558 0500 DA 06       	.2byte 0x6da
 2559 0502 E9 00 00 00 	.4byte 0xe9
 2560 0506 02          	.byte 0x2
 2561 0507 01          	.byte 0x1
 2562 0508 09          	.byte 0x9
 2563 0509 02          	.byte 0x2
 2564 050a 23          	.byte 0x23
 2565 050b 00          	.uleb128 0x0
 2566 050c 05          	.uleb128 0x5
 2567 050d 57 41 4B 45 	.asciz "WAKE"
 2567      00 
 2568 0512 02          	.byte 0x2
 2569 0513 DB 06       	.2byte 0x6db
 2570 0515 E9 00 00 00 	.4byte 0xe9
 2571 0519 02          	.byte 0x2
 2572 051a 01          	.byte 0x1
 2573 051b 08          	.byte 0x8
 2574 051c 02          	.byte 0x2
 2575 051d 23          	.byte 0x23
 2576 051e 00          	.uleb128 0x0
 2577 051f 05          	.uleb128 0x5
 2578 0520 55 45 4E 00 	.asciz "UEN"
 2579 0524 02          	.byte 0x2
 2580 0525 DC 06       	.2byte 0x6dc
 2581 0527 E9 00 00 00 	.4byte 0xe9
 2582 052b 02          	.byte 0x2
 2583 052c 02          	.byte 0x2
 2584 052d 06          	.byte 0x6
 2585 052e 02          	.byte 0x2
 2586 052f 23          	.byte 0x23
 2587 0530 00          	.uleb128 0x0
 2588 0531 05          	.uleb128 0x5
 2589 0532 52 54 53 4D 	.asciz "RTSMD"
 2589      44 00 
 2590 0538 02          	.byte 0x2
 2591 0539 DE 06       	.2byte 0x6de
 2592 053b E9 00 00 00 	.4byte 0xe9
 2593 053f 02          	.byte 0x2
 2594 0540 01          	.byte 0x1
MPLAB XC16 ASSEMBLY Listing:   			page 56


 2595 0541 04          	.byte 0x4
 2596 0542 02          	.byte 0x2
 2597 0543 23          	.byte 0x23
 2598 0544 00          	.uleb128 0x0
 2599 0545 05          	.uleb128 0x5
 2600 0546 49 52 45 4E 	.asciz "IREN"
 2600      00 
 2601 054b 02          	.byte 0x2
 2602 054c DF 06       	.2byte 0x6df
 2603 054e E9 00 00 00 	.4byte 0xe9
 2604 0552 02          	.byte 0x2
 2605 0553 01          	.byte 0x1
 2606 0554 03          	.byte 0x3
 2607 0555 02          	.byte 0x2
 2608 0556 23          	.byte 0x23
 2609 0557 00          	.uleb128 0x0
 2610 0558 05          	.uleb128 0x5
 2611 0559 55 53 49 44 	.asciz "USIDL"
 2611      4C 00 
 2612 055f 02          	.byte 0x2
 2613 0560 E0 06       	.2byte 0x6e0
 2614 0562 E9 00 00 00 	.4byte 0xe9
 2615 0566 02          	.byte 0x2
 2616 0567 01          	.byte 0x1
 2617 0568 02          	.byte 0x2
 2618 0569 02          	.byte 0x2
 2619 056a 23          	.byte 0x23
 2620 056b 00          	.uleb128 0x0
 2621 056c 05          	.uleb128 0x5
 2622 056d 55 41 52 54 	.asciz "UARTEN"
 2622      45 4E 00 
 2623 0574 02          	.byte 0x2
 2624 0575 E2 06       	.2byte 0x6e2
 2625 0577 E9 00 00 00 	.4byte 0xe9
 2626 057b 02          	.byte 0x2
 2627 057c 01          	.byte 0x1
 2628 057d 10          	.byte 0x10
 2629 057e 02          	.byte 0x2
 2630 057f 23          	.byte 0x23
 2631 0580 00          	.uleb128 0x0
 2632 0581 00          	.byte 0x0
 2633 0582 04          	.uleb128 0x4
 2634 0583 02          	.byte 0x2
 2635 0584 02          	.byte 0x2
 2636 0585 E4 06       	.2byte 0x6e4
 2637 0587 F0 05 00 00 	.4byte 0x5f0
 2638 058b 05          	.uleb128 0x5
 2639 058c 50 44 53 45 	.asciz "PDSEL0"
 2639      4C 30 00 
 2640 0593 02          	.byte 0x2
 2641 0594 E6 06       	.2byte 0x6e6
 2642 0596 E9 00 00 00 	.4byte 0xe9
 2643 059a 02          	.byte 0x2
 2644 059b 01          	.byte 0x1
 2645 059c 0E          	.byte 0xe
 2646 059d 02          	.byte 0x2
 2647 059e 23          	.byte 0x23
MPLAB XC16 ASSEMBLY Listing:   			page 57


 2648 059f 00          	.uleb128 0x0
 2649 05a0 05          	.uleb128 0x5
 2650 05a1 50 44 53 45 	.asciz "PDSEL1"
 2650      4C 31 00 
 2651 05a8 02          	.byte 0x2
 2652 05a9 E7 06       	.2byte 0x6e7
 2653 05ab E9 00 00 00 	.4byte 0xe9
 2654 05af 02          	.byte 0x2
 2655 05b0 01          	.byte 0x1
 2656 05b1 0D          	.byte 0xd
 2657 05b2 02          	.byte 0x2
 2658 05b3 23          	.byte 0x23
 2659 05b4 00          	.uleb128 0x0
 2660 05b5 05          	.uleb128 0x5
 2661 05b6 52 58 49 4E 	.asciz "RXINV"
 2661      56 00 
 2662 05bc 02          	.byte 0x2
 2663 05bd E9 06       	.2byte 0x6e9
 2664 05bf E9 00 00 00 	.4byte 0xe9
 2665 05c3 02          	.byte 0x2
 2666 05c4 01          	.byte 0x1
 2667 05c5 0B          	.byte 0xb
 2668 05c6 02          	.byte 0x2
 2669 05c7 23          	.byte 0x23
 2670 05c8 00          	.uleb128 0x0
 2671 05c9 05          	.uleb128 0x5
 2672 05ca 55 45 4E 30 	.asciz "UEN0"
 2672      00 
 2673 05cf 02          	.byte 0x2
 2674 05d0 EB 06       	.2byte 0x6eb
 2675 05d2 E9 00 00 00 	.4byte 0xe9
 2676 05d6 02          	.byte 0x2
 2677 05d7 01          	.byte 0x1
 2678 05d8 07          	.byte 0x7
 2679 05d9 02          	.byte 0x2
 2680 05da 23          	.byte 0x23
 2681 05db 00          	.uleb128 0x0
 2682 05dc 05          	.uleb128 0x5
 2683 05dd 55 45 4E 31 	.asciz "UEN1"
 2683      00 
 2684 05e2 02          	.byte 0x2
 2685 05e3 EC 06       	.2byte 0x6ec
 2686 05e5 E9 00 00 00 	.4byte 0xe9
 2687 05e9 02          	.byte 0x2
 2688 05ea 01          	.byte 0x1
 2689 05eb 06          	.byte 0x6
 2690 05ec 02          	.byte 0x2
 2691 05ed 23          	.byte 0x23
 2692 05ee 00          	.uleb128 0x0
 2693 05ef 00          	.byte 0x0
 2694 05f0 06          	.uleb128 0x6
 2695 05f1 02          	.byte 0x2
 2696 05f2 02          	.byte 0x2
 2697 05f3 D3 06       	.2byte 0x6d3
 2698 05f5 04 06 00 00 	.4byte 0x604
 2699 05f9 07          	.uleb128 0x7
 2700 05fa 8A 04 00 00 	.4byte 0x48a
MPLAB XC16 ASSEMBLY Listing:   			page 58


 2701 05fe 07          	.uleb128 0x7
 2702 05ff 82 05 00 00 	.4byte 0x582
 2703 0603 00          	.byte 0x0
 2704 0604 08          	.uleb128 0x8
 2705 0605 74 61 67 55 	.asciz "tagU2MODEBITS"
 2705      32 4D 4F 44 
 2705      45 42 49 54 
 2705      53 00 
 2706 0613 02          	.byte 0x2
 2707 0614 02          	.byte 0x2
 2708 0615 D2 06       	.2byte 0x6d2
 2709 0617 24 06 00 00 	.4byte 0x624
 2710 061b 09          	.uleb128 0x9
 2711 061c F0 05 00 00 	.4byte 0x5f0
 2712 0620 02          	.byte 0x2
 2713 0621 23          	.byte 0x23
 2714 0622 00          	.uleb128 0x0
 2715 0623 00          	.byte 0x0
 2716 0624 0A          	.uleb128 0xa
 2717 0625 55 32 4D 4F 	.asciz "U2MODEBITS"
 2717      44 45 42 49 
 2717      54 53 00 
 2718 0630 02          	.byte 0x2
 2719 0631 EF 06       	.2byte 0x6ef
 2720 0633 04 06 00 00 	.4byte 0x604
 2721 0637 04          	.uleb128 0x4
 2722 0638 02          	.byte 0x2
 2723 0639 02          	.byte 0x2
 2724 063a F6 06       	.2byte 0x6f6
 2725 063c 55 07 00 00 	.4byte 0x755
 2726 0640 05          	.uleb128 0x5
 2727 0641 55 52 58 44 	.asciz "URXDA"
 2727      41 00 
 2728 0647 02          	.byte 0x2
 2729 0648 F7 06       	.2byte 0x6f7
 2730 064a E9 00 00 00 	.4byte 0xe9
 2731 064e 02          	.byte 0x2
 2732 064f 01          	.byte 0x1
 2733 0650 0F          	.byte 0xf
 2734 0651 02          	.byte 0x2
 2735 0652 23          	.byte 0x23
 2736 0653 00          	.uleb128 0x0
 2737 0654 05          	.uleb128 0x5
 2738 0655 4F 45 52 52 	.asciz "OERR"
 2738      00 
 2739 065a 02          	.byte 0x2
 2740 065b F8 06       	.2byte 0x6f8
 2741 065d E9 00 00 00 	.4byte 0xe9
 2742 0661 02          	.byte 0x2
 2743 0662 01          	.byte 0x1
 2744 0663 0E          	.byte 0xe
 2745 0664 02          	.byte 0x2
 2746 0665 23          	.byte 0x23
 2747 0666 00          	.uleb128 0x0
 2748 0667 05          	.uleb128 0x5
 2749 0668 46 45 52 52 	.asciz "FERR"
 2749      00 
MPLAB XC16 ASSEMBLY Listing:   			page 59


 2750 066d 02          	.byte 0x2
 2751 066e F9 06       	.2byte 0x6f9
 2752 0670 E9 00 00 00 	.4byte 0xe9
 2753 0674 02          	.byte 0x2
 2754 0675 01          	.byte 0x1
 2755 0676 0D          	.byte 0xd
 2756 0677 02          	.byte 0x2
 2757 0678 23          	.byte 0x23
 2758 0679 00          	.uleb128 0x0
 2759 067a 05          	.uleb128 0x5
 2760 067b 50 45 52 52 	.asciz "PERR"
 2760      00 
 2761 0680 02          	.byte 0x2
 2762 0681 FA 06       	.2byte 0x6fa
 2763 0683 E9 00 00 00 	.4byte 0xe9
 2764 0687 02          	.byte 0x2
 2765 0688 01          	.byte 0x1
 2766 0689 0C          	.byte 0xc
 2767 068a 02          	.byte 0x2
 2768 068b 23          	.byte 0x23
 2769 068c 00          	.uleb128 0x0
 2770 068d 05          	.uleb128 0x5
 2771 068e 52 49 44 4C 	.asciz "RIDLE"
 2771      45 00 
 2772 0694 02          	.byte 0x2
 2773 0695 FB 06       	.2byte 0x6fb
 2774 0697 E9 00 00 00 	.4byte 0xe9
 2775 069b 02          	.byte 0x2
 2776 069c 01          	.byte 0x1
 2777 069d 0B          	.byte 0xb
 2778 069e 02          	.byte 0x2
 2779 069f 23          	.byte 0x23
 2780 06a0 00          	.uleb128 0x0
 2781 06a1 05          	.uleb128 0x5
 2782 06a2 41 44 44 45 	.asciz "ADDEN"
 2782      4E 00 
 2783 06a8 02          	.byte 0x2
 2784 06a9 FC 06       	.2byte 0x6fc
 2785 06ab E9 00 00 00 	.4byte 0xe9
 2786 06af 02          	.byte 0x2
 2787 06b0 01          	.byte 0x1
 2788 06b1 0A          	.byte 0xa
 2789 06b2 02          	.byte 0x2
 2790 06b3 23          	.byte 0x23
 2791 06b4 00          	.uleb128 0x0
 2792 06b5 05          	.uleb128 0x5
 2793 06b6 55 52 58 49 	.asciz "URXISEL"
 2793      53 45 4C 00 
 2794 06be 02          	.byte 0x2
 2795 06bf FD 06       	.2byte 0x6fd
 2796 06c1 E9 00 00 00 	.4byte 0xe9
 2797 06c5 02          	.byte 0x2
 2798 06c6 02          	.byte 0x2
 2799 06c7 08          	.byte 0x8
 2800 06c8 02          	.byte 0x2
 2801 06c9 23          	.byte 0x23
 2802 06ca 00          	.uleb128 0x0
MPLAB XC16 ASSEMBLY Listing:   			page 60


 2803 06cb 05          	.uleb128 0x5
 2804 06cc 54 52 4D 54 	.asciz "TRMT"
 2804      00 
 2805 06d1 02          	.byte 0x2
 2806 06d2 FE 06       	.2byte 0x6fe
 2807 06d4 E9 00 00 00 	.4byte 0xe9
 2808 06d8 02          	.byte 0x2
 2809 06d9 01          	.byte 0x1
 2810 06da 07          	.byte 0x7
 2811 06db 02          	.byte 0x2
 2812 06dc 23          	.byte 0x23
 2813 06dd 00          	.uleb128 0x0
 2814 06de 05          	.uleb128 0x5
 2815 06df 55 54 58 42 	.asciz "UTXBF"
 2815      46 00 
 2816 06e5 02          	.byte 0x2
 2817 06e6 FF 06       	.2byte 0x6ff
 2818 06e8 E9 00 00 00 	.4byte 0xe9
 2819 06ec 02          	.byte 0x2
 2820 06ed 01          	.byte 0x1
 2821 06ee 06          	.byte 0x6
 2822 06ef 02          	.byte 0x2
 2823 06f0 23          	.byte 0x23
 2824 06f1 00          	.uleb128 0x0
 2825 06f2 05          	.uleb128 0x5
 2826 06f3 55 54 58 45 	.asciz "UTXEN"
 2826      4E 00 
 2827 06f9 02          	.byte 0x2
 2828 06fa 00 07       	.2byte 0x700
 2829 06fc E9 00 00 00 	.4byte 0xe9
 2830 0700 02          	.byte 0x2
 2831 0701 01          	.byte 0x1
 2832 0702 05          	.byte 0x5
 2833 0703 02          	.byte 0x2
 2834 0704 23          	.byte 0x23
 2835 0705 00          	.uleb128 0x0
 2836 0706 05          	.uleb128 0x5
 2837 0707 55 54 58 42 	.asciz "UTXBRK"
 2837      52 4B 00 
 2838 070e 02          	.byte 0x2
 2839 070f 01 07       	.2byte 0x701
 2840 0711 E9 00 00 00 	.4byte 0xe9
 2841 0715 02          	.byte 0x2
 2842 0716 01          	.byte 0x1
 2843 0717 04          	.byte 0x4
 2844 0718 02          	.byte 0x2
 2845 0719 23          	.byte 0x23
 2846 071a 00          	.uleb128 0x0
 2847 071b 0B          	.uleb128 0xb
 2848 071c 00 00 00 00 	.4byte .LASF0
 2849 0720 02          	.byte 0x2
 2850 0721 03 07       	.2byte 0x703
 2851 0723 E9 00 00 00 	.4byte 0xe9
 2852 0727 02          	.byte 0x2
 2853 0728 01          	.byte 0x1
 2854 0729 02          	.byte 0x2
 2855 072a 02          	.byte 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 61


 2856 072b 23          	.byte 0x23
 2857 072c 00          	.uleb128 0x0
 2858 072d 05          	.uleb128 0x5
 2859 072e 55 54 58 49 	.asciz "UTXINV"
 2859      4E 56 00 
 2860 0735 02          	.byte 0x2
 2861 0736 04 07       	.2byte 0x704
 2862 0738 E9 00 00 00 	.4byte 0xe9
 2863 073c 02          	.byte 0x2
 2864 073d 01          	.byte 0x1
 2865 073e 01          	.byte 0x1
 2866 073f 02          	.byte 0x2
 2867 0740 23          	.byte 0x23
 2868 0741 00          	.uleb128 0x0
 2869 0742 0B          	.uleb128 0xb
 2870 0743 00 00 00 00 	.4byte .LASF1
 2871 0747 02          	.byte 0x2
 2872 0748 05 07       	.2byte 0x705
 2873 074a E9 00 00 00 	.4byte 0xe9
 2874 074e 02          	.byte 0x2
 2875 074f 01          	.byte 0x1
 2876 0750 10          	.byte 0x10
 2877 0751 02          	.byte 0x2
 2878 0752 23          	.byte 0x23
 2879 0753 00          	.uleb128 0x0
 2880 0754 00          	.byte 0x0
 2881 0755 04          	.uleb128 0x4
 2882 0756 02          	.byte 0x2
 2883 0757 02          	.byte 0x2
 2884 0758 07 07       	.2byte 0x707
 2885 075a 97 07 00 00 	.4byte 0x797
 2886 075e 0B          	.uleb128 0xb
 2887 075f 00 00 00 00 	.4byte .LASF2
 2888 0763 02          	.byte 0x2
 2889 0764 09 07       	.2byte 0x709
 2890 0766 E9 00 00 00 	.4byte 0xe9
 2891 076a 02          	.byte 0x2
 2892 076b 01          	.byte 0x1
 2893 076c 09          	.byte 0x9
 2894 076d 02          	.byte 0x2
 2895 076e 23          	.byte 0x23
 2896 076f 00          	.uleb128 0x0
 2897 0770 0B          	.uleb128 0xb
 2898 0771 00 00 00 00 	.4byte .LASF3
 2899 0775 02          	.byte 0x2
 2900 0776 0A 07       	.2byte 0x70a
 2901 0778 E9 00 00 00 	.4byte 0xe9
 2902 077c 02          	.byte 0x2
 2903 077d 01          	.byte 0x1
 2904 077e 08          	.byte 0x8
 2905 077f 02          	.byte 0x2
 2906 0780 23          	.byte 0x23
 2907 0781 00          	.uleb128 0x0
 2908 0782 05          	.uleb128 0x5
 2909 0783 54 58 49 4E 	.asciz "TXINV"
 2909      56 00 
 2910 0789 02          	.byte 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 62


 2911 078a 0C 07       	.2byte 0x70c
 2912 078c E9 00 00 00 	.4byte 0xe9
 2913 0790 02          	.byte 0x2
 2914 0791 01          	.byte 0x1
 2915 0792 01          	.byte 0x1
 2916 0793 02          	.byte 0x2
 2917 0794 23          	.byte 0x23
 2918 0795 00          	.uleb128 0x0
 2919 0796 00          	.byte 0x0
 2920 0797 06          	.uleb128 0x6
 2921 0798 02          	.byte 0x2
 2922 0799 02          	.byte 0x2
 2923 079a F5 06       	.2byte 0x6f5
 2924 079c AB 07 00 00 	.4byte 0x7ab
 2925 07a0 07          	.uleb128 0x7
 2926 07a1 37 06 00 00 	.4byte 0x637
 2927 07a5 07          	.uleb128 0x7
 2928 07a6 55 07 00 00 	.4byte 0x755
 2929 07aa 00          	.byte 0x0
 2930 07ab 08          	.uleb128 0x8
 2931 07ac 74 61 67 55 	.asciz "tagU2STABITS"
 2931      32 53 54 41 
 2931      42 49 54 53 
 2931      00 
 2932 07b9 02          	.byte 0x2
 2933 07ba 02          	.byte 0x2
 2934 07bb F4 06       	.2byte 0x6f4
 2935 07bd CA 07 00 00 	.4byte 0x7ca
 2936 07c1 09          	.uleb128 0x9
 2937 07c2 97 07 00 00 	.4byte 0x797
 2938 07c6 02          	.byte 0x2
 2939 07c7 23          	.byte 0x23
 2940 07c8 00          	.uleb128 0x0
 2941 07c9 00          	.byte 0x0
 2942 07ca 0A          	.uleb128 0xa
 2943 07cb 55 32 53 54 	.asciz "U2STABITS"
 2943      41 42 49 54 
 2943      53 00 
 2944 07d5 02          	.byte 0x2
 2945 07d6 0F 07       	.2byte 0x70f
 2946 07d8 AB 07 00 00 	.4byte 0x7ab
 2947 07dc 08          	.uleb128 0x8
 2948 07dd 74 61 67 49 	.asciz "tagIFS0BITS"
 2948      46 53 30 42 
 2948      49 54 53 00 
 2949 07e9 02          	.byte 0x2
 2950 07ea 02          	.byte 0x2
 2951 07eb 08 25       	.2byte 0x2508
 2952 07ed 37 09 00 00 	.4byte 0x937
 2953 07f1 05          	.uleb128 0x5
 2954 07f2 49 4E 54 30 	.asciz "INT0IF"
 2954      49 46 00 
 2955 07f9 02          	.byte 0x2
 2956 07fa 09 25       	.2byte 0x2509
 2957 07fc E9 00 00 00 	.4byte 0xe9
 2958 0800 02          	.byte 0x2
 2959 0801 01          	.byte 0x1
MPLAB XC16 ASSEMBLY Listing:   			page 63


 2960 0802 0F          	.byte 0xf
 2961 0803 02          	.byte 0x2
 2962 0804 23          	.byte 0x23
 2963 0805 00          	.uleb128 0x0
 2964 0806 05          	.uleb128 0x5
 2965 0807 49 43 31 49 	.asciz "IC1IF"
 2965      46 00 
 2966 080d 02          	.byte 0x2
 2967 080e 0A 25       	.2byte 0x250a
 2968 0810 E9 00 00 00 	.4byte 0xe9
 2969 0814 02          	.byte 0x2
 2970 0815 01          	.byte 0x1
 2971 0816 0E          	.byte 0xe
 2972 0817 02          	.byte 0x2
 2973 0818 23          	.byte 0x23
 2974 0819 00          	.uleb128 0x0
 2975 081a 05          	.uleb128 0x5
 2976 081b 4F 43 31 49 	.asciz "OC1IF"
 2976      46 00 
 2977 0821 02          	.byte 0x2
 2978 0822 0B 25       	.2byte 0x250b
 2979 0824 E9 00 00 00 	.4byte 0xe9
 2980 0828 02          	.byte 0x2
 2981 0829 01          	.byte 0x1
 2982 082a 0D          	.byte 0xd
 2983 082b 02          	.byte 0x2
 2984 082c 23          	.byte 0x23
 2985 082d 00          	.uleb128 0x0
 2986 082e 05          	.uleb128 0x5
 2987 082f 54 31 49 46 	.asciz "T1IF"
 2987      00 
 2988 0834 02          	.byte 0x2
 2989 0835 0C 25       	.2byte 0x250c
 2990 0837 E9 00 00 00 	.4byte 0xe9
 2991 083b 02          	.byte 0x2
 2992 083c 01          	.byte 0x1
 2993 083d 0C          	.byte 0xc
 2994 083e 02          	.byte 0x2
 2995 083f 23          	.byte 0x23
 2996 0840 00          	.uleb128 0x0
 2997 0841 05          	.uleb128 0x5
 2998 0842 44 4D 41 30 	.asciz "DMA0IF"
 2998      49 46 00 
 2999 0849 02          	.byte 0x2
 3000 084a 0D 25       	.2byte 0x250d
 3001 084c E9 00 00 00 	.4byte 0xe9
 3002 0850 02          	.byte 0x2
 3003 0851 01          	.byte 0x1
 3004 0852 0B          	.byte 0xb
 3005 0853 02          	.byte 0x2
 3006 0854 23          	.byte 0x23
 3007 0855 00          	.uleb128 0x0
 3008 0856 05          	.uleb128 0x5
 3009 0857 49 43 32 49 	.asciz "IC2IF"
 3009      46 00 
 3010 085d 02          	.byte 0x2
 3011 085e 0E 25       	.2byte 0x250e
MPLAB XC16 ASSEMBLY Listing:   			page 64


 3012 0860 E9 00 00 00 	.4byte 0xe9
 3013 0864 02          	.byte 0x2
 3014 0865 01          	.byte 0x1
 3015 0866 0A          	.byte 0xa
 3016 0867 02          	.byte 0x2
 3017 0868 23          	.byte 0x23
 3018 0869 00          	.uleb128 0x0
 3019 086a 05          	.uleb128 0x5
 3020 086b 4F 43 32 49 	.asciz "OC2IF"
 3020      46 00 
 3021 0871 02          	.byte 0x2
 3022 0872 0F 25       	.2byte 0x250f
 3023 0874 E9 00 00 00 	.4byte 0xe9
 3024 0878 02          	.byte 0x2
 3025 0879 01          	.byte 0x1
 3026 087a 09          	.byte 0x9
 3027 087b 02          	.byte 0x2
 3028 087c 23          	.byte 0x23
 3029 087d 00          	.uleb128 0x0
 3030 087e 05          	.uleb128 0x5
 3031 087f 54 32 49 46 	.asciz "T2IF"
 3031      00 
 3032 0884 02          	.byte 0x2
 3033 0885 10 25       	.2byte 0x2510
 3034 0887 E9 00 00 00 	.4byte 0xe9
 3035 088b 02          	.byte 0x2
 3036 088c 01          	.byte 0x1
 3037 088d 08          	.byte 0x8
 3038 088e 02          	.byte 0x2
 3039 088f 23          	.byte 0x23
 3040 0890 00          	.uleb128 0x0
 3041 0891 05          	.uleb128 0x5
 3042 0892 54 33 49 46 	.asciz "T3IF"
 3042      00 
 3043 0897 02          	.byte 0x2
 3044 0898 11 25       	.2byte 0x2511
 3045 089a E9 00 00 00 	.4byte 0xe9
 3046 089e 02          	.byte 0x2
 3047 089f 01          	.byte 0x1
 3048 08a0 07          	.byte 0x7
 3049 08a1 02          	.byte 0x2
 3050 08a2 23          	.byte 0x23
 3051 08a3 00          	.uleb128 0x0
 3052 08a4 05          	.uleb128 0x5
 3053 08a5 53 50 49 31 	.asciz "SPI1EIF"
 3053      45 49 46 00 
 3054 08ad 02          	.byte 0x2
 3055 08ae 12 25       	.2byte 0x2512
 3056 08b0 E9 00 00 00 	.4byte 0xe9
 3057 08b4 02          	.byte 0x2
 3058 08b5 01          	.byte 0x1
 3059 08b6 06          	.byte 0x6
 3060 08b7 02          	.byte 0x2
 3061 08b8 23          	.byte 0x23
 3062 08b9 00          	.uleb128 0x0
 3063 08ba 05          	.uleb128 0x5
 3064 08bb 53 50 49 31 	.asciz "SPI1IF"
MPLAB XC16 ASSEMBLY Listing:   			page 65


 3064      49 46 00 
 3065 08c2 02          	.byte 0x2
 3066 08c3 13 25       	.2byte 0x2513
 3067 08c5 E9 00 00 00 	.4byte 0xe9
 3068 08c9 02          	.byte 0x2
 3069 08ca 01          	.byte 0x1
 3070 08cb 05          	.byte 0x5
 3071 08cc 02          	.byte 0x2
 3072 08cd 23          	.byte 0x23
 3073 08ce 00          	.uleb128 0x0
 3074 08cf 05          	.uleb128 0x5
 3075 08d0 55 31 52 58 	.asciz "U1RXIF"
 3075      49 46 00 
 3076 08d7 02          	.byte 0x2
 3077 08d8 14 25       	.2byte 0x2514
 3078 08da E9 00 00 00 	.4byte 0xe9
 3079 08de 02          	.byte 0x2
 3080 08df 01          	.byte 0x1
 3081 08e0 04          	.byte 0x4
 3082 08e1 02          	.byte 0x2
 3083 08e2 23          	.byte 0x23
 3084 08e3 00          	.uleb128 0x0
 3085 08e4 05          	.uleb128 0x5
 3086 08e5 55 31 54 58 	.asciz "U1TXIF"
 3086      49 46 00 
 3087 08ec 02          	.byte 0x2
 3088 08ed 15 25       	.2byte 0x2515
 3089 08ef E9 00 00 00 	.4byte 0xe9
 3090 08f3 02          	.byte 0x2
 3091 08f4 01          	.byte 0x1
 3092 08f5 03          	.byte 0x3
 3093 08f6 02          	.byte 0x2
 3094 08f7 23          	.byte 0x23
 3095 08f8 00          	.uleb128 0x0
 3096 08f9 05          	.uleb128 0x5
 3097 08fa 41 44 31 49 	.asciz "AD1IF"
 3097      46 00 
 3098 0900 02          	.byte 0x2
 3099 0901 16 25       	.2byte 0x2516
 3100 0903 E9 00 00 00 	.4byte 0xe9
 3101 0907 02          	.byte 0x2
 3102 0908 01          	.byte 0x1
 3103 0909 02          	.byte 0x2
 3104 090a 02          	.byte 0x2
 3105 090b 23          	.byte 0x23
 3106 090c 00          	.uleb128 0x0
 3107 090d 05          	.uleb128 0x5
 3108 090e 44 4D 41 31 	.asciz "DMA1IF"
 3108      49 46 00 
 3109 0915 02          	.byte 0x2
 3110 0916 17 25       	.2byte 0x2517
 3111 0918 E9 00 00 00 	.4byte 0xe9
 3112 091c 02          	.byte 0x2
 3113 091d 01          	.byte 0x1
 3114 091e 01          	.byte 0x1
 3115 091f 02          	.byte 0x2
 3116 0920 23          	.byte 0x23
MPLAB XC16 ASSEMBLY Listing:   			page 66


 3117 0921 00          	.uleb128 0x0
 3118 0922 05          	.uleb128 0x5
 3119 0923 4E 56 4D 49 	.asciz "NVMIF"
 3119      46 00 
 3120 0929 02          	.byte 0x2
 3121 092a 18 25       	.2byte 0x2518
 3122 092c E9 00 00 00 	.4byte 0xe9
 3123 0930 02          	.byte 0x2
 3124 0931 01          	.byte 0x1
 3125 0932 10          	.byte 0x10
 3126 0933 02          	.byte 0x2
 3127 0934 23          	.byte 0x23
 3128 0935 00          	.uleb128 0x0
 3129 0936 00          	.byte 0x0
 3130 0937 0A          	.uleb128 0xa
 3131 0938 49 46 53 30 	.asciz "IFS0BITS"
 3131      42 49 54 53 
 3131      00 
 3132 0941 02          	.byte 0x2
 3133 0942 19 25       	.2byte 0x2519
 3134 0944 DC 07 00 00 	.4byte 0x7dc
 3135 0948 08          	.uleb128 0x8
 3136 0949 74 61 67 49 	.asciz "tagIFS1BITS"
 3136      46 53 31 42 
 3136      49 54 53 00 
 3137 0955 02          	.byte 0x2
 3138 0956 02          	.byte 0x2
 3139 0957 1E 25       	.2byte 0x251e
 3140 0959 A3 0A 00 00 	.4byte 0xaa3
 3141 095d 05          	.uleb128 0x5
 3142 095e 53 49 32 43 	.asciz "SI2C1IF"
 3142      31 49 46 00 
 3143 0966 02          	.byte 0x2
 3144 0967 1F 25       	.2byte 0x251f
 3145 0969 E9 00 00 00 	.4byte 0xe9
 3146 096d 02          	.byte 0x2
 3147 096e 01          	.byte 0x1
 3148 096f 0F          	.byte 0xf
 3149 0970 02          	.byte 0x2
 3150 0971 23          	.byte 0x23
 3151 0972 00          	.uleb128 0x0
 3152 0973 05          	.uleb128 0x5
 3153 0974 4D 49 32 43 	.asciz "MI2C1IF"
 3153      31 49 46 00 
 3154 097c 02          	.byte 0x2
 3155 097d 20 25       	.2byte 0x2520
 3156 097f E9 00 00 00 	.4byte 0xe9
 3157 0983 02          	.byte 0x2
 3158 0984 01          	.byte 0x1
 3159 0985 0E          	.byte 0xe
 3160 0986 02          	.byte 0x2
 3161 0987 23          	.byte 0x23
 3162 0988 00          	.uleb128 0x0
 3163 0989 05          	.uleb128 0x5
 3164 098a 43 4D 49 46 	.asciz "CMIF"
 3164      00 
 3165 098f 02          	.byte 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 67


 3166 0990 21 25       	.2byte 0x2521
 3167 0992 E9 00 00 00 	.4byte 0xe9
 3168 0996 02          	.byte 0x2
 3169 0997 01          	.byte 0x1
 3170 0998 0D          	.byte 0xd
 3171 0999 02          	.byte 0x2
 3172 099a 23          	.byte 0x23
 3173 099b 00          	.uleb128 0x0
 3174 099c 05          	.uleb128 0x5
 3175 099d 43 4E 49 46 	.asciz "CNIF"
 3175      00 
 3176 09a2 02          	.byte 0x2
 3177 09a3 22 25       	.2byte 0x2522
 3178 09a5 E9 00 00 00 	.4byte 0xe9
 3179 09a9 02          	.byte 0x2
 3180 09aa 01          	.byte 0x1
 3181 09ab 0C          	.byte 0xc
 3182 09ac 02          	.byte 0x2
 3183 09ad 23          	.byte 0x23
 3184 09ae 00          	.uleb128 0x0
 3185 09af 05          	.uleb128 0x5
 3186 09b0 49 4E 54 31 	.asciz "INT1IF"
 3186      49 46 00 
 3187 09b7 02          	.byte 0x2
 3188 09b8 23 25       	.2byte 0x2523
 3189 09ba E9 00 00 00 	.4byte 0xe9
 3190 09be 02          	.byte 0x2
 3191 09bf 01          	.byte 0x1
 3192 09c0 0B          	.byte 0xb
 3193 09c1 02          	.byte 0x2
 3194 09c2 23          	.byte 0x23
 3195 09c3 00          	.uleb128 0x0
 3196 09c4 05          	.uleb128 0x5
 3197 09c5 41 44 32 49 	.asciz "AD2IF"
 3197      46 00 
 3198 09cb 02          	.byte 0x2
 3199 09cc 24 25       	.2byte 0x2524
 3200 09ce E9 00 00 00 	.4byte 0xe9
 3201 09d2 02          	.byte 0x2
 3202 09d3 01          	.byte 0x1
 3203 09d4 0A          	.byte 0xa
 3204 09d5 02          	.byte 0x2
 3205 09d6 23          	.byte 0x23
 3206 09d7 00          	.uleb128 0x0
 3207 09d8 05          	.uleb128 0x5
 3208 09d9 49 43 37 49 	.asciz "IC7IF"
 3208      46 00 
 3209 09df 02          	.byte 0x2
 3210 09e0 25 25       	.2byte 0x2525
 3211 09e2 E9 00 00 00 	.4byte 0xe9
 3212 09e6 02          	.byte 0x2
 3213 09e7 01          	.byte 0x1
 3214 09e8 09          	.byte 0x9
 3215 09e9 02          	.byte 0x2
 3216 09ea 23          	.byte 0x23
 3217 09eb 00          	.uleb128 0x0
 3218 09ec 05          	.uleb128 0x5
MPLAB XC16 ASSEMBLY Listing:   			page 68


 3219 09ed 49 43 38 49 	.asciz "IC8IF"
 3219      46 00 
 3220 09f3 02          	.byte 0x2
 3221 09f4 26 25       	.2byte 0x2526
 3222 09f6 E9 00 00 00 	.4byte 0xe9
 3223 09fa 02          	.byte 0x2
 3224 09fb 01          	.byte 0x1
 3225 09fc 08          	.byte 0x8
 3226 09fd 02          	.byte 0x2
 3227 09fe 23          	.byte 0x23
 3228 09ff 00          	.uleb128 0x0
 3229 0a00 05          	.uleb128 0x5
 3230 0a01 44 4D 41 32 	.asciz "DMA2IF"
 3230      49 46 00 
 3231 0a08 02          	.byte 0x2
 3232 0a09 27 25       	.2byte 0x2527
 3233 0a0b E9 00 00 00 	.4byte 0xe9
 3234 0a0f 02          	.byte 0x2
 3235 0a10 01          	.byte 0x1
 3236 0a11 07          	.byte 0x7
 3237 0a12 02          	.byte 0x2
 3238 0a13 23          	.byte 0x23
 3239 0a14 00          	.uleb128 0x0
 3240 0a15 05          	.uleb128 0x5
 3241 0a16 4F 43 33 49 	.asciz "OC3IF"
 3241      46 00 
 3242 0a1c 02          	.byte 0x2
 3243 0a1d 28 25       	.2byte 0x2528
 3244 0a1f E9 00 00 00 	.4byte 0xe9
 3245 0a23 02          	.byte 0x2
 3246 0a24 01          	.byte 0x1
 3247 0a25 06          	.byte 0x6
 3248 0a26 02          	.byte 0x2
 3249 0a27 23          	.byte 0x23
 3250 0a28 00          	.uleb128 0x0
 3251 0a29 05          	.uleb128 0x5
 3252 0a2a 4F 43 34 49 	.asciz "OC4IF"
 3252      46 00 
 3253 0a30 02          	.byte 0x2
 3254 0a31 29 25       	.2byte 0x2529
 3255 0a33 E9 00 00 00 	.4byte 0xe9
 3256 0a37 02          	.byte 0x2
 3257 0a38 01          	.byte 0x1
 3258 0a39 05          	.byte 0x5
 3259 0a3a 02          	.byte 0x2
 3260 0a3b 23          	.byte 0x23
 3261 0a3c 00          	.uleb128 0x0
 3262 0a3d 05          	.uleb128 0x5
 3263 0a3e 54 34 49 46 	.asciz "T4IF"
 3263      00 
 3264 0a43 02          	.byte 0x2
 3265 0a44 2A 25       	.2byte 0x252a
 3266 0a46 E9 00 00 00 	.4byte 0xe9
 3267 0a4a 02          	.byte 0x2
 3268 0a4b 01          	.byte 0x1
 3269 0a4c 04          	.byte 0x4
 3270 0a4d 02          	.byte 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 69


 3271 0a4e 23          	.byte 0x23
 3272 0a4f 00          	.uleb128 0x0
 3273 0a50 05          	.uleb128 0x5
 3274 0a51 54 35 49 46 	.asciz "T5IF"
 3274      00 
 3275 0a56 02          	.byte 0x2
 3276 0a57 2B 25       	.2byte 0x252b
 3277 0a59 E9 00 00 00 	.4byte 0xe9
 3278 0a5d 02          	.byte 0x2
 3279 0a5e 01          	.byte 0x1
 3280 0a5f 03          	.byte 0x3
 3281 0a60 02          	.byte 0x2
 3282 0a61 23          	.byte 0x23
 3283 0a62 00          	.uleb128 0x0
 3284 0a63 05          	.uleb128 0x5
 3285 0a64 49 4E 54 32 	.asciz "INT2IF"
 3285      49 46 00 
 3286 0a6b 02          	.byte 0x2
 3287 0a6c 2C 25       	.2byte 0x252c
 3288 0a6e E9 00 00 00 	.4byte 0xe9
 3289 0a72 02          	.byte 0x2
 3290 0a73 01          	.byte 0x1
 3291 0a74 02          	.byte 0x2
 3292 0a75 02          	.byte 0x2
 3293 0a76 23          	.byte 0x23
 3294 0a77 00          	.uleb128 0x0
 3295 0a78 05          	.uleb128 0x5
 3296 0a79 55 32 52 58 	.asciz "U2RXIF"
 3296      49 46 00 
 3297 0a80 02          	.byte 0x2
 3298 0a81 2D 25       	.2byte 0x252d
 3299 0a83 E9 00 00 00 	.4byte 0xe9
 3300 0a87 02          	.byte 0x2
 3301 0a88 01          	.byte 0x1
 3302 0a89 01          	.byte 0x1
 3303 0a8a 02          	.byte 0x2
 3304 0a8b 23          	.byte 0x23
 3305 0a8c 00          	.uleb128 0x0
 3306 0a8d 05          	.uleb128 0x5
 3307 0a8e 55 32 54 58 	.asciz "U2TXIF"
 3307      49 46 00 
 3308 0a95 02          	.byte 0x2
 3309 0a96 2E 25       	.2byte 0x252e
 3310 0a98 E9 00 00 00 	.4byte 0xe9
 3311 0a9c 02          	.byte 0x2
 3312 0a9d 01          	.byte 0x1
 3313 0a9e 10          	.byte 0x10
 3314 0a9f 02          	.byte 0x2
 3315 0aa0 23          	.byte 0x23
 3316 0aa1 00          	.uleb128 0x0
 3317 0aa2 00          	.byte 0x0
 3318 0aa3 0A          	.uleb128 0xa
 3319 0aa4 49 46 53 31 	.asciz "IFS1BITS"
 3319      42 49 54 53 
 3319      00 
 3320 0aad 02          	.byte 0x2
 3321 0aae 2F 25       	.2byte 0x252f
MPLAB XC16 ASSEMBLY Listing:   			page 70


 3322 0ab0 48 09 00 00 	.4byte 0x948
 3323 0ab4 08          	.uleb128 0x8
 3324 0ab5 74 61 67 49 	.asciz "tagIFS4BITS"
 3324      46 53 34 42 
 3324      49 54 53 00 
 3325 0ac1 02          	.byte 0x2
 3326 0ac2 02          	.byte 0x2
 3327 0ac3 5F 25       	.2byte 0x255f
 3328 0ac5 85 0B 00 00 	.4byte 0xb85
 3329 0ac9 05          	.uleb128 0x5
 3330 0aca 55 31 45 49 	.asciz "U1EIF"
 3330      46 00 
 3331 0ad0 02          	.byte 0x2
 3332 0ad1 61 25       	.2byte 0x2561
 3333 0ad3 E9 00 00 00 	.4byte 0xe9
 3334 0ad7 02          	.byte 0x2
 3335 0ad8 01          	.byte 0x1
 3336 0ad9 0E          	.byte 0xe
 3337 0ada 02          	.byte 0x2
 3338 0adb 23          	.byte 0x23
 3339 0adc 00          	.uleb128 0x0
 3340 0add 05          	.uleb128 0x5
 3341 0ade 55 32 45 49 	.asciz "U2EIF"
 3341      46 00 
 3342 0ae4 02          	.byte 0x2
 3343 0ae5 62 25       	.2byte 0x2562
 3344 0ae7 E9 00 00 00 	.4byte 0xe9
 3345 0aeb 02          	.byte 0x2
 3346 0aec 01          	.byte 0x1
 3347 0aed 0D          	.byte 0xd
 3348 0aee 02          	.byte 0x2
 3349 0aef 23          	.byte 0x23
 3350 0af0 00          	.uleb128 0x0
 3351 0af1 05          	.uleb128 0x5
 3352 0af2 43 52 43 49 	.asciz "CRCIF"
 3352      46 00 
 3353 0af8 02          	.byte 0x2
 3354 0af9 63 25       	.2byte 0x2563
 3355 0afb E9 00 00 00 	.4byte 0xe9
 3356 0aff 02          	.byte 0x2
 3357 0b00 01          	.byte 0x1
 3358 0b01 0C          	.byte 0xc
 3359 0b02 02          	.byte 0x2
 3360 0b03 23          	.byte 0x23
 3361 0b04 00          	.uleb128 0x0
 3362 0b05 05          	.uleb128 0x5
 3363 0b06 44 4D 41 36 	.asciz "DMA6IF"
 3363      49 46 00 
 3364 0b0d 02          	.byte 0x2
 3365 0b0e 64 25       	.2byte 0x2564
 3366 0b10 E9 00 00 00 	.4byte 0xe9
 3367 0b14 02          	.byte 0x2
 3368 0b15 01          	.byte 0x1
 3369 0b16 0B          	.byte 0xb
 3370 0b17 02          	.byte 0x2
 3371 0b18 23          	.byte 0x23
 3372 0b19 00          	.uleb128 0x0
MPLAB XC16 ASSEMBLY Listing:   			page 71


 3373 0b1a 05          	.uleb128 0x5
 3374 0b1b 44 4D 41 37 	.asciz "DMA7IF"
 3374      49 46 00 
 3375 0b22 02          	.byte 0x2
 3376 0b23 65 25       	.2byte 0x2565
 3377 0b25 E9 00 00 00 	.4byte 0xe9
 3378 0b29 02          	.byte 0x2
 3379 0b2a 01          	.byte 0x1
 3380 0b2b 0A          	.byte 0xa
 3381 0b2c 02          	.byte 0x2
 3382 0b2d 23          	.byte 0x23
 3383 0b2e 00          	.uleb128 0x0
 3384 0b2f 05          	.uleb128 0x5
 3385 0b30 43 31 54 58 	.asciz "C1TXIF"
 3385      49 46 00 
 3386 0b37 02          	.byte 0x2
 3387 0b38 66 25       	.2byte 0x2566
 3388 0b3a E9 00 00 00 	.4byte 0xe9
 3389 0b3e 02          	.byte 0x2
 3390 0b3f 01          	.byte 0x1
 3391 0b40 09          	.byte 0x9
 3392 0b41 02          	.byte 0x2
 3393 0b42 23          	.byte 0x23
 3394 0b43 00          	.uleb128 0x0
 3395 0b44 05          	.uleb128 0x5
 3396 0b45 43 32 54 58 	.asciz "C2TXIF"
 3396      49 46 00 
 3397 0b4c 02          	.byte 0x2
 3398 0b4d 67 25       	.2byte 0x2567
 3399 0b4f E9 00 00 00 	.4byte 0xe9
 3400 0b53 02          	.byte 0x2
 3401 0b54 01          	.byte 0x1
 3402 0b55 08          	.byte 0x8
 3403 0b56 02          	.byte 0x2
 3404 0b57 23          	.byte 0x23
 3405 0b58 00          	.uleb128 0x0
 3406 0b59 05          	.uleb128 0x5
 3407 0b5a 50 53 45 53 	.asciz "PSESMIF"
 3407      4D 49 46 00 
 3408 0b62 02          	.byte 0x2
 3409 0b63 69 25       	.2byte 0x2569
 3410 0b65 E9 00 00 00 	.4byte 0xe9
 3411 0b69 02          	.byte 0x2
 3412 0b6a 01          	.byte 0x1
 3413 0b6b 06          	.byte 0x6
 3414 0b6c 02          	.byte 0x2
 3415 0b6d 23          	.byte 0x23
 3416 0b6e 00          	.uleb128 0x0
 3417 0b6f 05          	.uleb128 0x5
 3418 0b70 51 45 49 32 	.asciz "QEI2IF"
 3418      49 46 00 
 3419 0b77 02          	.byte 0x2
 3420 0b78 6B 25       	.2byte 0x256b
 3421 0b7a E9 00 00 00 	.4byte 0xe9
 3422 0b7e 02          	.byte 0x2
 3423 0b7f 01          	.byte 0x1
 3424 0b80 04          	.byte 0x4
MPLAB XC16 ASSEMBLY Listing:   			page 72


 3425 0b81 02          	.byte 0x2
 3426 0b82 23          	.byte 0x23
 3427 0b83 00          	.uleb128 0x0
 3428 0b84 00          	.byte 0x0
 3429 0b85 0A          	.uleb128 0xa
 3430 0b86 49 46 53 34 	.asciz "IFS4BITS"
 3430      42 49 54 53 
 3430      00 
 3431 0b8f 02          	.byte 0x2
 3432 0b90 6C 25       	.2byte 0x256c
 3433 0b92 B4 0A 00 00 	.4byte 0xab4
 3434 0b96 08          	.uleb128 0x8
 3435 0b97 74 61 67 49 	.asciz "tagIEC0BITS"
 3435      45 43 30 42 
 3435      49 54 53 00 
 3436 0ba3 02          	.byte 0x2
 3437 0ba4 02          	.byte 0x2
 3438 0ba5 B4 25       	.2byte 0x25b4
 3439 0ba7 F1 0C 00 00 	.4byte 0xcf1
 3440 0bab 05          	.uleb128 0x5
 3441 0bac 49 4E 54 30 	.asciz "INT0IE"
 3441      49 45 00 
 3442 0bb3 02          	.byte 0x2
 3443 0bb4 B5 25       	.2byte 0x25b5
 3444 0bb6 E9 00 00 00 	.4byte 0xe9
 3445 0bba 02          	.byte 0x2
 3446 0bbb 01          	.byte 0x1
 3447 0bbc 0F          	.byte 0xf
 3448 0bbd 02          	.byte 0x2
 3449 0bbe 23          	.byte 0x23
 3450 0bbf 00          	.uleb128 0x0
 3451 0bc0 05          	.uleb128 0x5
 3452 0bc1 49 43 31 49 	.asciz "IC1IE"
 3452      45 00 
 3453 0bc7 02          	.byte 0x2
 3454 0bc8 B6 25       	.2byte 0x25b6
 3455 0bca E9 00 00 00 	.4byte 0xe9
 3456 0bce 02          	.byte 0x2
 3457 0bcf 01          	.byte 0x1
 3458 0bd0 0E          	.byte 0xe
 3459 0bd1 02          	.byte 0x2
 3460 0bd2 23          	.byte 0x23
 3461 0bd3 00          	.uleb128 0x0
 3462 0bd4 05          	.uleb128 0x5
 3463 0bd5 4F 43 31 49 	.asciz "OC1IE"
 3463      45 00 
 3464 0bdb 02          	.byte 0x2
 3465 0bdc B7 25       	.2byte 0x25b7
 3466 0bde E9 00 00 00 	.4byte 0xe9
 3467 0be2 02          	.byte 0x2
 3468 0be3 01          	.byte 0x1
 3469 0be4 0D          	.byte 0xd
 3470 0be5 02          	.byte 0x2
 3471 0be6 23          	.byte 0x23
 3472 0be7 00          	.uleb128 0x0
 3473 0be8 05          	.uleb128 0x5
 3474 0be9 54 31 49 45 	.asciz "T1IE"
MPLAB XC16 ASSEMBLY Listing:   			page 73


 3474      00 
 3475 0bee 02          	.byte 0x2
 3476 0bef B8 25       	.2byte 0x25b8
 3477 0bf1 E9 00 00 00 	.4byte 0xe9
 3478 0bf5 02          	.byte 0x2
 3479 0bf6 01          	.byte 0x1
 3480 0bf7 0C          	.byte 0xc
 3481 0bf8 02          	.byte 0x2
 3482 0bf9 23          	.byte 0x23
 3483 0bfa 00          	.uleb128 0x0
 3484 0bfb 05          	.uleb128 0x5
 3485 0bfc 44 4D 41 30 	.asciz "DMA0IE"
 3485      49 45 00 
 3486 0c03 02          	.byte 0x2
 3487 0c04 B9 25       	.2byte 0x25b9
 3488 0c06 E9 00 00 00 	.4byte 0xe9
 3489 0c0a 02          	.byte 0x2
 3490 0c0b 01          	.byte 0x1
 3491 0c0c 0B          	.byte 0xb
 3492 0c0d 02          	.byte 0x2
 3493 0c0e 23          	.byte 0x23
 3494 0c0f 00          	.uleb128 0x0
 3495 0c10 05          	.uleb128 0x5
 3496 0c11 49 43 32 49 	.asciz "IC2IE"
 3496      45 00 
 3497 0c17 02          	.byte 0x2
 3498 0c18 BA 25       	.2byte 0x25ba
 3499 0c1a E9 00 00 00 	.4byte 0xe9
 3500 0c1e 02          	.byte 0x2
 3501 0c1f 01          	.byte 0x1
 3502 0c20 0A          	.byte 0xa
 3503 0c21 02          	.byte 0x2
 3504 0c22 23          	.byte 0x23
 3505 0c23 00          	.uleb128 0x0
 3506 0c24 05          	.uleb128 0x5
 3507 0c25 4F 43 32 49 	.asciz "OC2IE"
 3507      45 00 
 3508 0c2b 02          	.byte 0x2
 3509 0c2c BB 25       	.2byte 0x25bb
 3510 0c2e E9 00 00 00 	.4byte 0xe9
 3511 0c32 02          	.byte 0x2
 3512 0c33 01          	.byte 0x1
 3513 0c34 09          	.byte 0x9
 3514 0c35 02          	.byte 0x2
 3515 0c36 23          	.byte 0x23
 3516 0c37 00          	.uleb128 0x0
 3517 0c38 05          	.uleb128 0x5
 3518 0c39 54 32 49 45 	.asciz "T2IE"
 3518      00 
 3519 0c3e 02          	.byte 0x2
 3520 0c3f BC 25       	.2byte 0x25bc
 3521 0c41 E9 00 00 00 	.4byte 0xe9
 3522 0c45 02          	.byte 0x2
 3523 0c46 01          	.byte 0x1
 3524 0c47 08          	.byte 0x8
 3525 0c48 02          	.byte 0x2
 3526 0c49 23          	.byte 0x23
MPLAB XC16 ASSEMBLY Listing:   			page 74


 3527 0c4a 00          	.uleb128 0x0
 3528 0c4b 05          	.uleb128 0x5
 3529 0c4c 54 33 49 45 	.asciz "T3IE"
 3529      00 
 3530 0c51 02          	.byte 0x2
 3531 0c52 BD 25       	.2byte 0x25bd
 3532 0c54 E9 00 00 00 	.4byte 0xe9
 3533 0c58 02          	.byte 0x2
 3534 0c59 01          	.byte 0x1
 3535 0c5a 07          	.byte 0x7
 3536 0c5b 02          	.byte 0x2
 3537 0c5c 23          	.byte 0x23
 3538 0c5d 00          	.uleb128 0x0
 3539 0c5e 05          	.uleb128 0x5
 3540 0c5f 53 50 49 31 	.asciz "SPI1EIE"
 3540      45 49 45 00 
 3541 0c67 02          	.byte 0x2
 3542 0c68 BE 25       	.2byte 0x25be
 3543 0c6a E9 00 00 00 	.4byte 0xe9
 3544 0c6e 02          	.byte 0x2
 3545 0c6f 01          	.byte 0x1
 3546 0c70 06          	.byte 0x6
 3547 0c71 02          	.byte 0x2
 3548 0c72 23          	.byte 0x23
 3549 0c73 00          	.uleb128 0x0
 3550 0c74 05          	.uleb128 0x5
 3551 0c75 53 50 49 31 	.asciz "SPI1IE"
 3551      49 45 00 
 3552 0c7c 02          	.byte 0x2
 3553 0c7d BF 25       	.2byte 0x25bf
 3554 0c7f E9 00 00 00 	.4byte 0xe9
 3555 0c83 02          	.byte 0x2
 3556 0c84 01          	.byte 0x1
 3557 0c85 05          	.byte 0x5
 3558 0c86 02          	.byte 0x2
 3559 0c87 23          	.byte 0x23
 3560 0c88 00          	.uleb128 0x0
 3561 0c89 05          	.uleb128 0x5
 3562 0c8a 55 31 52 58 	.asciz "U1RXIE"
 3562      49 45 00 
 3563 0c91 02          	.byte 0x2
 3564 0c92 C0 25       	.2byte 0x25c0
 3565 0c94 E9 00 00 00 	.4byte 0xe9
 3566 0c98 02          	.byte 0x2
 3567 0c99 01          	.byte 0x1
 3568 0c9a 04          	.byte 0x4
 3569 0c9b 02          	.byte 0x2
 3570 0c9c 23          	.byte 0x23
 3571 0c9d 00          	.uleb128 0x0
 3572 0c9e 05          	.uleb128 0x5
 3573 0c9f 55 31 54 58 	.asciz "U1TXIE"
 3573      49 45 00 
 3574 0ca6 02          	.byte 0x2
 3575 0ca7 C1 25       	.2byte 0x25c1
 3576 0ca9 E9 00 00 00 	.4byte 0xe9
 3577 0cad 02          	.byte 0x2
 3578 0cae 01          	.byte 0x1
MPLAB XC16 ASSEMBLY Listing:   			page 75


 3579 0caf 03          	.byte 0x3
 3580 0cb0 02          	.byte 0x2
 3581 0cb1 23          	.byte 0x23
 3582 0cb2 00          	.uleb128 0x0
 3583 0cb3 05          	.uleb128 0x5
 3584 0cb4 41 44 31 49 	.asciz "AD1IE"
 3584      45 00 
 3585 0cba 02          	.byte 0x2
 3586 0cbb C2 25       	.2byte 0x25c2
 3587 0cbd E9 00 00 00 	.4byte 0xe9
 3588 0cc1 02          	.byte 0x2
 3589 0cc2 01          	.byte 0x1
 3590 0cc3 02          	.byte 0x2
 3591 0cc4 02          	.byte 0x2
 3592 0cc5 23          	.byte 0x23
 3593 0cc6 00          	.uleb128 0x0
 3594 0cc7 05          	.uleb128 0x5
 3595 0cc8 44 4D 41 31 	.asciz "DMA1IE"
 3595      49 45 00 
 3596 0ccf 02          	.byte 0x2
 3597 0cd0 C3 25       	.2byte 0x25c3
 3598 0cd2 E9 00 00 00 	.4byte 0xe9
 3599 0cd6 02          	.byte 0x2
 3600 0cd7 01          	.byte 0x1
 3601 0cd8 01          	.byte 0x1
 3602 0cd9 02          	.byte 0x2
 3603 0cda 23          	.byte 0x23
 3604 0cdb 00          	.uleb128 0x0
 3605 0cdc 05          	.uleb128 0x5
 3606 0cdd 4E 56 4D 49 	.asciz "NVMIE"
 3606      45 00 
 3607 0ce3 02          	.byte 0x2
 3608 0ce4 C4 25       	.2byte 0x25c4
 3609 0ce6 E9 00 00 00 	.4byte 0xe9
 3610 0cea 02          	.byte 0x2
 3611 0ceb 01          	.byte 0x1
 3612 0cec 10          	.byte 0x10
 3613 0ced 02          	.byte 0x2
 3614 0cee 23          	.byte 0x23
 3615 0cef 00          	.uleb128 0x0
 3616 0cf0 00          	.byte 0x0
 3617 0cf1 0A          	.uleb128 0xa
 3618 0cf2 49 45 43 30 	.asciz "IEC0BITS"
 3618      42 49 54 53 
 3618      00 
 3619 0cfb 02          	.byte 0x2
 3620 0cfc C5 25       	.2byte 0x25c5
 3621 0cfe 96 0B 00 00 	.4byte 0xb96
 3622 0d02 08          	.uleb128 0x8
 3623 0d03 74 61 67 49 	.asciz "tagIEC1BITS"
 3623      45 43 31 42 
 3623      49 54 53 00 
 3624 0d0f 02          	.byte 0x2
 3625 0d10 02          	.byte 0x2
 3626 0d11 CA 25       	.2byte 0x25ca
 3627 0d13 5D 0E 00 00 	.4byte 0xe5d
 3628 0d17 05          	.uleb128 0x5
MPLAB XC16 ASSEMBLY Listing:   			page 76


 3629 0d18 53 49 32 43 	.asciz "SI2C1IE"
 3629      31 49 45 00 
 3630 0d20 02          	.byte 0x2
 3631 0d21 CB 25       	.2byte 0x25cb
 3632 0d23 E9 00 00 00 	.4byte 0xe9
 3633 0d27 02          	.byte 0x2
 3634 0d28 01          	.byte 0x1
 3635 0d29 0F          	.byte 0xf
 3636 0d2a 02          	.byte 0x2
 3637 0d2b 23          	.byte 0x23
 3638 0d2c 00          	.uleb128 0x0
 3639 0d2d 05          	.uleb128 0x5
 3640 0d2e 4D 49 32 43 	.asciz "MI2C1IE"
 3640      31 49 45 00 
 3641 0d36 02          	.byte 0x2
 3642 0d37 CC 25       	.2byte 0x25cc
 3643 0d39 E9 00 00 00 	.4byte 0xe9
 3644 0d3d 02          	.byte 0x2
 3645 0d3e 01          	.byte 0x1
 3646 0d3f 0E          	.byte 0xe
 3647 0d40 02          	.byte 0x2
 3648 0d41 23          	.byte 0x23
 3649 0d42 00          	.uleb128 0x0
 3650 0d43 05          	.uleb128 0x5
 3651 0d44 43 4D 49 45 	.asciz "CMIE"
 3651      00 
 3652 0d49 02          	.byte 0x2
 3653 0d4a CD 25       	.2byte 0x25cd
 3654 0d4c E9 00 00 00 	.4byte 0xe9
 3655 0d50 02          	.byte 0x2
 3656 0d51 01          	.byte 0x1
 3657 0d52 0D          	.byte 0xd
 3658 0d53 02          	.byte 0x2
 3659 0d54 23          	.byte 0x23
 3660 0d55 00          	.uleb128 0x0
 3661 0d56 05          	.uleb128 0x5
 3662 0d57 43 4E 49 45 	.asciz "CNIE"
 3662      00 
 3663 0d5c 02          	.byte 0x2
 3664 0d5d CE 25       	.2byte 0x25ce
 3665 0d5f E9 00 00 00 	.4byte 0xe9
 3666 0d63 02          	.byte 0x2
 3667 0d64 01          	.byte 0x1
 3668 0d65 0C          	.byte 0xc
 3669 0d66 02          	.byte 0x2
 3670 0d67 23          	.byte 0x23
 3671 0d68 00          	.uleb128 0x0
 3672 0d69 05          	.uleb128 0x5
 3673 0d6a 49 4E 54 31 	.asciz "INT1IE"
 3673      49 45 00 
 3674 0d71 02          	.byte 0x2
 3675 0d72 CF 25       	.2byte 0x25cf
 3676 0d74 E9 00 00 00 	.4byte 0xe9
 3677 0d78 02          	.byte 0x2
 3678 0d79 01          	.byte 0x1
 3679 0d7a 0B          	.byte 0xb
 3680 0d7b 02          	.byte 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 77


 3681 0d7c 23          	.byte 0x23
 3682 0d7d 00          	.uleb128 0x0
 3683 0d7e 05          	.uleb128 0x5
 3684 0d7f 41 44 32 49 	.asciz "AD2IE"
 3684      45 00 
 3685 0d85 02          	.byte 0x2
 3686 0d86 D0 25       	.2byte 0x25d0
 3687 0d88 E9 00 00 00 	.4byte 0xe9
 3688 0d8c 02          	.byte 0x2
 3689 0d8d 01          	.byte 0x1
 3690 0d8e 0A          	.byte 0xa
 3691 0d8f 02          	.byte 0x2
 3692 0d90 23          	.byte 0x23
 3693 0d91 00          	.uleb128 0x0
 3694 0d92 05          	.uleb128 0x5
 3695 0d93 49 43 37 49 	.asciz "IC7IE"
 3695      45 00 
 3696 0d99 02          	.byte 0x2
 3697 0d9a D1 25       	.2byte 0x25d1
 3698 0d9c E9 00 00 00 	.4byte 0xe9
 3699 0da0 02          	.byte 0x2
 3700 0da1 01          	.byte 0x1
 3701 0da2 09          	.byte 0x9
 3702 0da3 02          	.byte 0x2
 3703 0da4 23          	.byte 0x23
 3704 0da5 00          	.uleb128 0x0
 3705 0da6 05          	.uleb128 0x5
 3706 0da7 49 43 38 49 	.asciz "IC8IE"
 3706      45 00 
 3707 0dad 02          	.byte 0x2
 3708 0dae D2 25       	.2byte 0x25d2
 3709 0db0 E9 00 00 00 	.4byte 0xe9
 3710 0db4 02          	.byte 0x2
 3711 0db5 01          	.byte 0x1
 3712 0db6 08          	.byte 0x8
 3713 0db7 02          	.byte 0x2
 3714 0db8 23          	.byte 0x23
 3715 0db9 00          	.uleb128 0x0
 3716 0dba 05          	.uleb128 0x5
 3717 0dbb 44 4D 41 32 	.asciz "DMA2IE"
 3717      49 45 00 
 3718 0dc2 02          	.byte 0x2
 3719 0dc3 D3 25       	.2byte 0x25d3
 3720 0dc5 E9 00 00 00 	.4byte 0xe9
 3721 0dc9 02          	.byte 0x2
 3722 0dca 01          	.byte 0x1
 3723 0dcb 07          	.byte 0x7
 3724 0dcc 02          	.byte 0x2
 3725 0dcd 23          	.byte 0x23
 3726 0dce 00          	.uleb128 0x0
 3727 0dcf 05          	.uleb128 0x5
 3728 0dd0 4F 43 33 49 	.asciz "OC3IE"
 3728      45 00 
 3729 0dd6 02          	.byte 0x2
 3730 0dd7 D4 25       	.2byte 0x25d4
 3731 0dd9 E9 00 00 00 	.4byte 0xe9
 3732 0ddd 02          	.byte 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 78


 3733 0dde 01          	.byte 0x1
 3734 0ddf 06          	.byte 0x6
 3735 0de0 02          	.byte 0x2
 3736 0de1 23          	.byte 0x23
 3737 0de2 00          	.uleb128 0x0
 3738 0de3 05          	.uleb128 0x5
 3739 0de4 4F 43 34 49 	.asciz "OC4IE"
 3739      45 00 
 3740 0dea 02          	.byte 0x2
 3741 0deb D5 25       	.2byte 0x25d5
 3742 0ded E9 00 00 00 	.4byte 0xe9
 3743 0df1 02          	.byte 0x2
 3744 0df2 01          	.byte 0x1
 3745 0df3 05          	.byte 0x5
 3746 0df4 02          	.byte 0x2
 3747 0df5 23          	.byte 0x23
 3748 0df6 00          	.uleb128 0x0
 3749 0df7 05          	.uleb128 0x5
 3750 0df8 54 34 49 45 	.asciz "T4IE"
 3750      00 
 3751 0dfd 02          	.byte 0x2
 3752 0dfe D6 25       	.2byte 0x25d6
 3753 0e00 E9 00 00 00 	.4byte 0xe9
 3754 0e04 02          	.byte 0x2
 3755 0e05 01          	.byte 0x1
 3756 0e06 04          	.byte 0x4
 3757 0e07 02          	.byte 0x2
 3758 0e08 23          	.byte 0x23
 3759 0e09 00          	.uleb128 0x0
 3760 0e0a 05          	.uleb128 0x5
 3761 0e0b 54 35 49 45 	.asciz "T5IE"
 3761      00 
 3762 0e10 02          	.byte 0x2
 3763 0e11 D7 25       	.2byte 0x25d7
 3764 0e13 E9 00 00 00 	.4byte 0xe9
 3765 0e17 02          	.byte 0x2
 3766 0e18 01          	.byte 0x1
 3767 0e19 03          	.byte 0x3
 3768 0e1a 02          	.byte 0x2
 3769 0e1b 23          	.byte 0x23
 3770 0e1c 00          	.uleb128 0x0
 3771 0e1d 05          	.uleb128 0x5
 3772 0e1e 49 4E 54 32 	.asciz "INT2IE"
 3772      49 45 00 
 3773 0e25 02          	.byte 0x2
 3774 0e26 D8 25       	.2byte 0x25d8
 3775 0e28 E9 00 00 00 	.4byte 0xe9
 3776 0e2c 02          	.byte 0x2
 3777 0e2d 01          	.byte 0x1
 3778 0e2e 02          	.byte 0x2
 3779 0e2f 02          	.byte 0x2
 3780 0e30 23          	.byte 0x23
 3781 0e31 00          	.uleb128 0x0
 3782 0e32 05          	.uleb128 0x5
 3783 0e33 55 32 52 58 	.asciz "U2RXIE"
 3783      49 45 00 
 3784 0e3a 02          	.byte 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 79


 3785 0e3b D9 25       	.2byte 0x25d9
 3786 0e3d E9 00 00 00 	.4byte 0xe9
 3787 0e41 02          	.byte 0x2
 3788 0e42 01          	.byte 0x1
 3789 0e43 01          	.byte 0x1
 3790 0e44 02          	.byte 0x2
 3791 0e45 23          	.byte 0x23
 3792 0e46 00          	.uleb128 0x0
 3793 0e47 05          	.uleb128 0x5
 3794 0e48 55 32 54 58 	.asciz "U2TXIE"
 3794      49 45 00 
 3795 0e4f 02          	.byte 0x2
 3796 0e50 DA 25       	.2byte 0x25da
 3797 0e52 E9 00 00 00 	.4byte 0xe9
 3798 0e56 02          	.byte 0x2
 3799 0e57 01          	.byte 0x1
 3800 0e58 10          	.byte 0x10
 3801 0e59 02          	.byte 0x2
 3802 0e5a 23          	.byte 0x23
 3803 0e5b 00          	.uleb128 0x0
 3804 0e5c 00          	.byte 0x0
 3805 0e5d 0A          	.uleb128 0xa
 3806 0e5e 49 45 43 31 	.asciz "IEC1BITS"
 3806      42 49 54 53 
 3806      00 
 3807 0e67 02          	.byte 0x2
 3808 0e68 DB 25       	.2byte 0x25db
 3809 0e6a 02 0D 00 00 	.4byte 0xd02
 3810 0e6e 08          	.uleb128 0x8
 3811 0e6f 74 61 67 49 	.asciz "tagIEC4BITS"
 3811      45 43 34 42 
 3811      49 54 53 00 
 3812 0e7b 02          	.byte 0x2
 3813 0e7c 02          	.byte 0x2
 3814 0e7d 0B 26       	.2byte 0x260b
 3815 0e7f 3F 0F 00 00 	.4byte 0xf3f
 3816 0e83 05          	.uleb128 0x5
 3817 0e84 55 31 45 49 	.asciz "U1EIE"
 3817      45 00 
 3818 0e8a 02          	.byte 0x2
 3819 0e8b 0D 26       	.2byte 0x260d
 3820 0e8d E9 00 00 00 	.4byte 0xe9
 3821 0e91 02          	.byte 0x2
 3822 0e92 01          	.byte 0x1
 3823 0e93 0E          	.byte 0xe
 3824 0e94 02          	.byte 0x2
 3825 0e95 23          	.byte 0x23
 3826 0e96 00          	.uleb128 0x0
 3827 0e97 05          	.uleb128 0x5
 3828 0e98 55 32 45 49 	.asciz "U2EIE"
 3828      45 00 
 3829 0e9e 02          	.byte 0x2
 3830 0e9f 0E 26       	.2byte 0x260e
 3831 0ea1 E9 00 00 00 	.4byte 0xe9
 3832 0ea5 02          	.byte 0x2
 3833 0ea6 01          	.byte 0x1
 3834 0ea7 0D          	.byte 0xd
MPLAB XC16 ASSEMBLY Listing:   			page 80


 3835 0ea8 02          	.byte 0x2
 3836 0ea9 23          	.byte 0x23
 3837 0eaa 00          	.uleb128 0x0
 3838 0eab 05          	.uleb128 0x5
 3839 0eac 43 52 43 49 	.asciz "CRCIE"
 3839      45 00 
 3840 0eb2 02          	.byte 0x2
 3841 0eb3 0F 26       	.2byte 0x260f
 3842 0eb5 E9 00 00 00 	.4byte 0xe9
 3843 0eb9 02          	.byte 0x2
 3844 0eba 01          	.byte 0x1
 3845 0ebb 0C          	.byte 0xc
 3846 0ebc 02          	.byte 0x2
 3847 0ebd 23          	.byte 0x23
 3848 0ebe 00          	.uleb128 0x0
 3849 0ebf 05          	.uleb128 0x5
 3850 0ec0 44 4D 41 36 	.asciz "DMA6IE"
 3850      49 45 00 
 3851 0ec7 02          	.byte 0x2
 3852 0ec8 10 26       	.2byte 0x2610
 3853 0eca E9 00 00 00 	.4byte 0xe9
 3854 0ece 02          	.byte 0x2
 3855 0ecf 01          	.byte 0x1
 3856 0ed0 0B          	.byte 0xb
 3857 0ed1 02          	.byte 0x2
 3858 0ed2 23          	.byte 0x23
 3859 0ed3 00          	.uleb128 0x0
 3860 0ed4 05          	.uleb128 0x5
 3861 0ed5 44 4D 41 37 	.asciz "DMA7IE"
 3861      49 45 00 
 3862 0edc 02          	.byte 0x2
 3863 0edd 11 26       	.2byte 0x2611
 3864 0edf E9 00 00 00 	.4byte 0xe9
 3865 0ee3 02          	.byte 0x2
 3866 0ee4 01          	.byte 0x1
 3867 0ee5 0A          	.byte 0xa
 3868 0ee6 02          	.byte 0x2
 3869 0ee7 23          	.byte 0x23
 3870 0ee8 00          	.uleb128 0x0
 3871 0ee9 05          	.uleb128 0x5
 3872 0eea 43 31 54 58 	.asciz "C1TXIE"
 3872      49 45 00 
 3873 0ef1 02          	.byte 0x2
 3874 0ef2 12 26       	.2byte 0x2612
 3875 0ef4 E9 00 00 00 	.4byte 0xe9
 3876 0ef8 02          	.byte 0x2
 3877 0ef9 01          	.byte 0x1
 3878 0efa 09          	.byte 0x9
 3879 0efb 02          	.byte 0x2
 3880 0efc 23          	.byte 0x23
 3881 0efd 00          	.uleb128 0x0
 3882 0efe 05          	.uleb128 0x5
 3883 0eff 43 32 54 58 	.asciz "C2TXIE"
 3883      49 45 00 
 3884 0f06 02          	.byte 0x2
 3885 0f07 13 26       	.2byte 0x2613
 3886 0f09 E9 00 00 00 	.4byte 0xe9
MPLAB XC16 ASSEMBLY Listing:   			page 81


 3887 0f0d 02          	.byte 0x2
 3888 0f0e 01          	.byte 0x1
 3889 0f0f 08          	.byte 0x8
 3890 0f10 02          	.byte 0x2
 3891 0f11 23          	.byte 0x23
 3892 0f12 00          	.uleb128 0x0
 3893 0f13 05          	.uleb128 0x5
 3894 0f14 50 53 45 53 	.asciz "PSESMIE"
 3894      4D 49 45 00 
 3895 0f1c 02          	.byte 0x2
 3896 0f1d 15 26       	.2byte 0x2615
 3897 0f1f E9 00 00 00 	.4byte 0xe9
 3898 0f23 02          	.byte 0x2
 3899 0f24 01          	.byte 0x1
 3900 0f25 06          	.byte 0x6
 3901 0f26 02          	.byte 0x2
 3902 0f27 23          	.byte 0x23
 3903 0f28 00          	.uleb128 0x0
 3904 0f29 05          	.uleb128 0x5
 3905 0f2a 51 45 49 32 	.asciz "QEI2IE"
 3905      49 45 00 
 3906 0f31 02          	.byte 0x2
 3907 0f32 17 26       	.2byte 0x2617
 3908 0f34 E9 00 00 00 	.4byte 0xe9
 3909 0f38 02          	.byte 0x2
 3910 0f39 01          	.byte 0x1
 3911 0f3a 04          	.byte 0x4
 3912 0f3b 02          	.byte 0x2
 3913 0f3c 23          	.byte 0x23
 3914 0f3d 00          	.uleb128 0x0
 3915 0f3e 00          	.byte 0x0
 3916 0f3f 0A          	.uleb128 0xa
 3917 0f40 49 45 43 34 	.asciz "IEC4BITS"
 3917      42 49 54 53 
 3917      00 
 3918 0f49 02          	.byte 0x2
 3919 0f4a 18 26       	.2byte 0x2618
 3920 0f4c 6E 0E 00 00 	.4byte 0xe6e
 3921 0f50 04          	.uleb128 0x4
 3922 0f51 02          	.byte 0x2
 3923 0f52 02          	.byte 0x2
 3924 0f53 A6 26       	.2byte 0x26a6
 3925 0f55 AD 0F 00 00 	.4byte 0xfad
 3926 0f59 05          	.uleb128 0x5
 3927 0f5a 54 33 49 50 	.asciz "T3IP"
 3927      00 
 3928 0f5f 02          	.byte 0x2
 3929 0f60 A7 26       	.2byte 0x26a7
 3930 0f62 E9 00 00 00 	.4byte 0xe9
 3931 0f66 02          	.byte 0x2
 3932 0f67 03          	.byte 0x3
 3933 0f68 0D          	.byte 0xd
 3934 0f69 02          	.byte 0x2
 3935 0f6a 23          	.byte 0x23
 3936 0f6b 00          	.uleb128 0x0
 3937 0f6c 05          	.uleb128 0x5
 3938 0f6d 53 50 49 31 	.asciz "SPI1EIP"
MPLAB XC16 ASSEMBLY Listing:   			page 82


 3938      45 49 50 00 
 3939 0f75 02          	.byte 0x2
 3940 0f76 A9 26       	.2byte 0x26a9
 3941 0f78 E9 00 00 00 	.4byte 0xe9
 3942 0f7c 02          	.byte 0x2
 3943 0f7d 03          	.byte 0x3
 3944 0f7e 09          	.byte 0x9
 3945 0f7f 02          	.byte 0x2
 3946 0f80 23          	.byte 0x23
 3947 0f81 00          	.uleb128 0x0
 3948 0f82 05          	.uleb128 0x5
 3949 0f83 53 50 49 31 	.asciz "SPI1IP"
 3949      49 50 00 
 3950 0f8a 02          	.byte 0x2
 3951 0f8b AB 26       	.2byte 0x26ab
 3952 0f8d E9 00 00 00 	.4byte 0xe9
 3953 0f91 02          	.byte 0x2
 3954 0f92 03          	.byte 0x3
 3955 0f93 05          	.byte 0x5
 3956 0f94 02          	.byte 0x2
 3957 0f95 23          	.byte 0x23
 3958 0f96 00          	.uleb128 0x0
 3959 0f97 05          	.uleb128 0x5
 3960 0f98 55 31 52 58 	.asciz "U1RXIP"
 3960      49 50 00 
 3961 0f9f 02          	.byte 0x2
 3962 0fa0 AD 26       	.2byte 0x26ad
 3963 0fa2 E9 00 00 00 	.4byte 0xe9
 3964 0fa6 02          	.byte 0x2
 3965 0fa7 03          	.byte 0x3
 3966 0fa8 01          	.byte 0x1
 3967 0fa9 02          	.byte 0x2
 3968 0faa 23          	.byte 0x23
 3969 0fab 00          	.uleb128 0x0
 3970 0fac 00          	.byte 0x0
 3971 0fad 04          	.uleb128 0x4
 3972 0fae 02          	.byte 0x2
 3973 0faf 02          	.byte 0x2
 3974 0fb0 AF 26       	.2byte 0x26af
 3975 0fb2 BC 10 00 00 	.4byte 0x10bc
 3976 0fb6 05          	.uleb128 0x5
 3977 0fb7 54 33 49 50 	.asciz "T3IP0"
 3977      30 00 
 3978 0fbd 02          	.byte 0x2
 3979 0fbe B0 26       	.2byte 0x26b0
 3980 0fc0 E9 00 00 00 	.4byte 0xe9
 3981 0fc4 02          	.byte 0x2
 3982 0fc5 01          	.byte 0x1
 3983 0fc6 0F          	.byte 0xf
 3984 0fc7 02          	.byte 0x2
 3985 0fc8 23          	.byte 0x23
 3986 0fc9 00          	.uleb128 0x0
 3987 0fca 05          	.uleb128 0x5
 3988 0fcb 54 33 49 50 	.asciz "T3IP1"
 3988      31 00 
 3989 0fd1 02          	.byte 0x2
 3990 0fd2 B1 26       	.2byte 0x26b1
MPLAB XC16 ASSEMBLY Listing:   			page 83


 3991 0fd4 E9 00 00 00 	.4byte 0xe9
 3992 0fd8 02          	.byte 0x2
 3993 0fd9 01          	.byte 0x1
 3994 0fda 0E          	.byte 0xe
 3995 0fdb 02          	.byte 0x2
 3996 0fdc 23          	.byte 0x23
 3997 0fdd 00          	.uleb128 0x0
 3998 0fde 05          	.uleb128 0x5
 3999 0fdf 54 33 49 50 	.asciz "T3IP2"
 3999      32 00 
 4000 0fe5 02          	.byte 0x2
 4001 0fe6 B2 26       	.2byte 0x26b2
 4002 0fe8 E9 00 00 00 	.4byte 0xe9
 4003 0fec 02          	.byte 0x2
 4004 0fed 01          	.byte 0x1
 4005 0fee 0D          	.byte 0xd
 4006 0fef 02          	.byte 0x2
 4007 0ff0 23          	.byte 0x23
 4008 0ff1 00          	.uleb128 0x0
 4009 0ff2 05          	.uleb128 0x5
 4010 0ff3 53 50 49 31 	.asciz "SPI1EIP0"
 4010      45 49 50 30 
 4010      00 
 4011 0ffc 02          	.byte 0x2
 4012 0ffd B4 26       	.2byte 0x26b4
 4013 0fff E9 00 00 00 	.4byte 0xe9
 4014 1003 02          	.byte 0x2
 4015 1004 01          	.byte 0x1
 4016 1005 0B          	.byte 0xb
 4017 1006 02          	.byte 0x2
 4018 1007 23          	.byte 0x23
 4019 1008 00          	.uleb128 0x0
 4020 1009 05          	.uleb128 0x5
 4021 100a 53 50 49 31 	.asciz "SPI1EIP1"
 4021      45 49 50 31 
 4021      00 
 4022 1013 02          	.byte 0x2
 4023 1014 B5 26       	.2byte 0x26b5
 4024 1016 E9 00 00 00 	.4byte 0xe9
 4025 101a 02          	.byte 0x2
 4026 101b 01          	.byte 0x1
 4027 101c 0A          	.byte 0xa
 4028 101d 02          	.byte 0x2
 4029 101e 23          	.byte 0x23
 4030 101f 00          	.uleb128 0x0
 4031 1020 05          	.uleb128 0x5
 4032 1021 53 50 49 31 	.asciz "SPI1EIP2"
 4032      45 49 50 32 
 4032      00 
 4033 102a 02          	.byte 0x2
 4034 102b B6 26       	.2byte 0x26b6
 4035 102d E9 00 00 00 	.4byte 0xe9
 4036 1031 02          	.byte 0x2
 4037 1032 01          	.byte 0x1
 4038 1033 09          	.byte 0x9
 4039 1034 02          	.byte 0x2
 4040 1035 23          	.byte 0x23
MPLAB XC16 ASSEMBLY Listing:   			page 84


 4041 1036 00          	.uleb128 0x0
 4042 1037 05          	.uleb128 0x5
 4043 1038 53 50 49 31 	.asciz "SPI1IP0"
 4043      49 50 30 00 
 4044 1040 02          	.byte 0x2
 4045 1041 B8 26       	.2byte 0x26b8
 4046 1043 E9 00 00 00 	.4byte 0xe9
 4047 1047 02          	.byte 0x2
 4048 1048 01          	.byte 0x1
 4049 1049 07          	.byte 0x7
 4050 104a 02          	.byte 0x2
 4051 104b 23          	.byte 0x23
 4052 104c 00          	.uleb128 0x0
 4053 104d 05          	.uleb128 0x5
 4054 104e 53 50 49 31 	.asciz "SPI1IP1"
 4054      49 50 31 00 
 4055 1056 02          	.byte 0x2
 4056 1057 B9 26       	.2byte 0x26b9
 4057 1059 E9 00 00 00 	.4byte 0xe9
 4058 105d 02          	.byte 0x2
 4059 105e 01          	.byte 0x1
 4060 105f 06          	.byte 0x6
 4061 1060 02          	.byte 0x2
 4062 1061 23          	.byte 0x23
 4063 1062 00          	.uleb128 0x0
 4064 1063 05          	.uleb128 0x5
 4065 1064 53 50 49 31 	.asciz "SPI1IP2"
 4065      49 50 32 00 
 4066 106c 02          	.byte 0x2
 4067 106d BA 26       	.2byte 0x26ba
 4068 106f E9 00 00 00 	.4byte 0xe9
 4069 1073 02          	.byte 0x2
 4070 1074 01          	.byte 0x1
 4071 1075 05          	.byte 0x5
 4072 1076 02          	.byte 0x2
 4073 1077 23          	.byte 0x23
 4074 1078 00          	.uleb128 0x0
 4075 1079 05          	.uleb128 0x5
 4076 107a 55 31 52 58 	.asciz "U1RXIP0"
 4076      49 50 30 00 
 4077 1082 02          	.byte 0x2
 4078 1083 BC 26       	.2byte 0x26bc
 4079 1085 E9 00 00 00 	.4byte 0xe9
 4080 1089 02          	.byte 0x2
 4081 108a 01          	.byte 0x1
 4082 108b 03          	.byte 0x3
 4083 108c 02          	.byte 0x2
 4084 108d 23          	.byte 0x23
 4085 108e 00          	.uleb128 0x0
 4086 108f 05          	.uleb128 0x5
 4087 1090 55 31 52 58 	.asciz "U1RXIP1"
 4087      49 50 31 00 
 4088 1098 02          	.byte 0x2
 4089 1099 BD 26       	.2byte 0x26bd
 4090 109b E9 00 00 00 	.4byte 0xe9
 4091 109f 02          	.byte 0x2
 4092 10a0 01          	.byte 0x1
MPLAB XC16 ASSEMBLY Listing:   			page 85


 4093 10a1 02          	.byte 0x2
 4094 10a2 02          	.byte 0x2
 4095 10a3 23          	.byte 0x23
 4096 10a4 00          	.uleb128 0x0
 4097 10a5 05          	.uleb128 0x5
 4098 10a6 55 31 52 58 	.asciz "U1RXIP2"
 4098      49 50 32 00 
 4099 10ae 02          	.byte 0x2
 4100 10af BE 26       	.2byte 0x26be
 4101 10b1 E9 00 00 00 	.4byte 0xe9
 4102 10b5 02          	.byte 0x2
 4103 10b6 01          	.byte 0x1
 4104 10b7 01          	.byte 0x1
 4105 10b8 02          	.byte 0x2
 4106 10b9 23          	.byte 0x23
 4107 10ba 00          	.uleb128 0x0
 4108 10bb 00          	.byte 0x0
 4109 10bc 06          	.uleb128 0x6
 4110 10bd 02          	.byte 0x2
 4111 10be 02          	.byte 0x2
 4112 10bf A5 26       	.2byte 0x26a5
 4113 10c1 D0 10 00 00 	.4byte 0x10d0
 4114 10c5 07          	.uleb128 0x7
 4115 10c6 50 0F 00 00 	.4byte 0xf50
 4116 10ca 07          	.uleb128 0x7
 4117 10cb AD 0F 00 00 	.4byte 0xfad
 4118 10cf 00          	.byte 0x0
 4119 10d0 08          	.uleb128 0x8
 4120 10d1 74 61 67 49 	.asciz "tagIPC2BITS"
 4120      50 43 32 42 
 4120      49 54 53 00 
 4121 10dd 02          	.byte 0x2
 4122 10de 02          	.byte 0x2
 4123 10df A4 26       	.2byte 0x26a4
 4124 10e1 EE 10 00 00 	.4byte 0x10ee
 4125 10e5 09          	.uleb128 0x9
 4126 10e6 BC 10 00 00 	.4byte 0x10bc
 4127 10ea 02          	.byte 0x2
 4128 10eb 23          	.byte 0x23
 4129 10ec 00          	.uleb128 0x0
 4130 10ed 00          	.byte 0x0
 4131 10ee 0A          	.uleb128 0xa
 4132 10ef 49 50 43 32 	.asciz "IPC2BITS"
 4132      42 49 54 53 
 4132      00 
 4133 10f8 02          	.byte 0x2
 4134 10f9 C1 26       	.2byte 0x26c1
 4135 10fb D0 10 00 00 	.4byte 0x10d0
 4136 10ff 04          	.uleb128 0x4
 4137 1100 02          	.byte 0x2
 4138 1101 02          	.byte 0x2
 4139 1102 50 27       	.2byte 0x2750
 4140 1104 5B 11 00 00 	.4byte 0x115b
 4141 1108 05          	.uleb128 0x5
 4142 1109 54 35 49 50 	.asciz "T5IP"
 4142      00 
 4143 110e 02          	.byte 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 86


 4144 110f 51 27       	.2byte 0x2751
 4145 1111 E9 00 00 00 	.4byte 0xe9
 4146 1115 02          	.byte 0x2
 4147 1116 03          	.byte 0x3
 4148 1117 0D          	.byte 0xd
 4149 1118 02          	.byte 0x2
 4150 1119 23          	.byte 0x23
 4151 111a 00          	.uleb128 0x0
 4152 111b 05          	.uleb128 0x5
 4153 111c 49 4E 54 32 	.asciz "INT2IP"
 4153      49 50 00 
 4154 1123 02          	.byte 0x2
 4155 1124 53 27       	.2byte 0x2753
 4156 1126 E9 00 00 00 	.4byte 0xe9
 4157 112a 02          	.byte 0x2
 4158 112b 03          	.byte 0x3
 4159 112c 09          	.byte 0x9
 4160 112d 02          	.byte 0x2
 4161 112e 23          	.byte 0x23
 4162 112f 00          	.uleb128 0x0
 4163 1130 05          	.uleb128 0x5
 4164 1131 55 32 52 58 	.asciz "U2RXIP"
 4164      49 50 00 
 4165 1138 02          	.byte 0x2
 4166 1139 55 27       	.2byte 0x2755
 4167 113b E9 00 00 00 	.4byte 0xe9
 4168 113f 02          	.byte 0x2
 4169 1140 03          	.byte 0x3
 4170 1141 05          	.byte 0x5
 4171 1142 02          	.byte 0x2
 4172 1143 23          	.byte 0x23
 4173 1144 00          	.uleb128 0x0
 4174 1145 05          	.uleb128 0x5
 4175 1146 55 32 54 58 	.asciz "U2TXIP"
 4175      49 50 00 
 4176 114d 02          	.byte 0x2
 4177 114e 57 27       	.2byte 0x2757
 4178 1150 E9 00 00 00 	.4byte 0xe9
 4179 1154 02          	.byte 0x2
 4180 1155 03          	.byte 0x3
 4181 1156 01          	.byte 0x1
 4182 1157 02          	.byte 0x2
 4183 1158 23          	.byte 0x23
 4184 1159 00          	.uleb128 0x0
 4185 115a 00          	.byte 0x0
 4186 115b 04          	.uleb128 0x4
 4187 115c 02          	.byte 0x2
 4188 115d 02          	.byte 0x2
 4189 115e 59 27       	.2byte 0x2759
 4190 1160 67 12 00 00 	.4byte 0x1267
 4191 1164 05          	.uleb128 0x5
 4192 1165 54 35 49 50 	.asciz "T5IP0"
 4192      30 00 
 4193 116b 02          	.byte 0x2
 4194 116c 5A 27       	.2byte 0x275a
 4195 116e E9 00 00 00 	.4byte 0xe9
 4196 1172 02          	.byte 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 87


 4197 1173 01          	.byte 0x1
 4198 1174 0F          	.byte 0xf
 4199 1175 02          	.byte 0x2
 4200 1176 23          	.byte 0x23
 4201 1177 00          	.uleb128 0x0
 4202 1178 05          	.uleb128 0x5
 4203 1179 54 35 49 50 	.asciz "T5IP1"
 4203      31 00 
 4204 117f 02          	.byte 0x2
 4205 1180 5B 27       	.2byte 0x275b
 4206 1182 E9 00 00 00 	.4byte 0xe9
 4207 1186 02          	.byte 0x2
 4208 1187 01          	.byte 0x1
 4209 1188 0E          	.byte 0xe
 4210 1189 02          	.byte 0x2
 4211 118a 23          	.byte 0x23
 4212 118b 00          	.uleb128 0x0
 4213 118c 05          	.uleb128 0x5
 4214 118d 54 35 49 50 	.asciz "T5IP2"
 4214      32 00 
 4215 1193 02          	.byte 0x2
 4216 1194 5C 27       	.2byte 0x275c
 4217 1196 E9 00 00 00 	.4byte 0xe9
 4218 119a 02          	.byte 0x2
 4219 119b 01          	.byte 0x1
 4220 119c 0D          	.byte 0xd
 4221 119d 02          	.byte 0x2
 4222 119e 23          	.byte 0x23
 4223 119f 00          	.uleb128 0x0
 4224 11a0 05          	.uleb128 0x5
 4225 11a1 49 4E 54 32 	.asciz "INT2IP0"
 4225      49 50 30 00 
 4226 11a9 02          	.byte 0x2
 4227 11aa 5E 27       	.2byte 0x275e
 4228 11ac E9 00 00 00 	.4byte 0xe9
 4229 11b0 02          	.byte 0x2
 4230 11b1 01          	.byte 0x1
 4231 11b2 0B          	.byte 0xb
 4232 11b3 02          	.byte 0x2
 4233 11b4 23          	.byte 0x23
 4234 11b5 00          	.uleb128 0x0
 4235 11b6 05          	.uleb128 0x5
 4236 11b7 49 4E 54 32 	.asciz "INT2IP1"
 4236      49 50 31 00 
 4237 11bf 02          	.byte 0x2
 4238 11c0 5F 27       	.2byte 0x275f
 4239 11c2 E9 00 00 00 	.4byte 0xe9
 4240 11c6 02          	.byte 0x2
 4241 11c7 01          	.byte 0x1
 4242 11c8 0A          	.byte 0xa
 4243 11c9 02          	.byte 0x2
 4244 11ca 23          	.byte 0x23
 4245 11cb 00          	.uleb128 0x0
 4246 11cc 05          	.uleb128 0x5
 4247 11cd 49 4E 54 32 	.asciz "INT2IP2"
 4247      49 50 32 00 
 4248 11d5 02          	.byte 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 88


 4249 11d6 60 27       	.2byte 0x2760
 4250 11d8 E9 00 00 00 	.4byte 0xe9
 4251 11dc 02          	.byte 0x2
 4252 11dd 01          	.byte 0x1
 4253 11de 09          	.byte 0x9
 4254 11df 02          	.byte 0x2
 4255 11e0 23          	.byte 0x23
 4256 11e1 00          	.uleb128 0x0
 4257 11e2 05          	.uleb128 0x5
 4258 11e3 55 32 52 58 	.asciz "U2RXIP0"
 4258      49 50 30 00 
 4259 11eb 02          	.byte 0x2
 4260 11ec 62 27       	.2byte 0x2762
 4261 11ee E9 00 00 00 	.4byte 0xe9
 4262 11f2 02          	.byte 0x2
 4263 11f3 01          	.byte 0x1
 4264 11f4 07          	.byte 0x7
 4265 11f5 02          	.byte 0x2
 4266 11f6 23          	.byte 0x23
 4267 11f7 00          	.uleb128 0x0
 4268 11f8 05          	.uleb128 0x5
 4269 11f9 55 32 52 58 	.asciz "U2RXIP1"
 4269      49 50 31 00 
 4270 1201 02          	.byte 0x2
 4271 1202 63 27       	.2byte 0x2763
 4272 1204 E9 00 00 00 	.4byte 0xe9
 4273 1208 02          	.byte 0x2
 4274 1209 01          	.byte 0x1
 4275 120a 06          	.byte 0x6
 4276 120b 02          	.byte 0x2
 4277 120c 23          	.byte 0x23
 4278 120d 00          	.uleb128 0x0
 4279 120e 05          	.uleb128 0x5
 4280 120f 55 32 52 58 	.asciz "U2RXIP2"
 4280      49 50 32 00 
 4281 1217 02          	.byte 0x2
 4282 1218 64 27       	.2byte 0x2764
 4283 121a E9 00 00 00 	.4byte 0xe9
 4284 121e 02          	.byte 0x2
 4285 121f 01          	.byte 0x1
 4286 1220 05          	.byte 0x5
 4287 1221 02          	.byte 0x2
 4288 1222 23          	.byte 0x23
 4289 1223 00          	.uleb128 0x0
 4290 1224 05          	.uleb128 0x5
 4291 1225 55 32 54 58 	.asciz "U2TXIP0"
 4291      49 50 30 00 
 4292 122d 02          	.byte 0x2
 4293 122e 66 27       	.2byte 0x2766
 4294 1230 E9 00 00 00 	.4byte 0xe9
 4295 1234 02          	.byte 0x2
 4296 1235 01          	.byte 0x1
 4297 1236 03          	.byte 0x3
 4298 1237 02          	.byte 0x2
 4299 1238 23          	.byte 0x23
 4300 1239 00          	.uleb128 0x0
 4301 123a 05          	.uleb128 0x5
MPLAB XC16 ASSEMBLY Listing:   			page 89


 4302 123b 55 32 54 58 	.asciz "U2TXIP1"
 4302      49 50 31 00 
 4303 1243 02          	.byte 0x2
 4304 1244 67 27       	.2byte 0x2767
 4305 1246 E9 00 00 00 	.4byte 0xe9
 4306 124a 02          	.byte 0x2
 4307 124b 01          	.byte 0x1
 4308 124c 02          	.byte 0x2
 4309 124d 02          	.byte 0x2
 4310 124e 23          	.byte 0x23
 4311 124f 00          	.uleb128 0x0
 4312 1250 05          	.uleb128 0x5
 4313 1251 55 32 54 58 	.asciz "U2TXIP2"
 4313      49 50 32 00 
 4314 1259 02          	.byte 0x2
 4315 125a 68 27       	.2byte 0x2768
 4316 125c E9 00 00 00 	.4byte 0xe9
 4317 1260 02          	.byte 0x2
 4318 1261 01          	.byte 0x1
 4319 1262 01          	.byte 0x1
 4320 1263 02          	.byte 0x2
 4321 1264 23          	.byte 0x23
 4322 1265 00          	.uleb128 0x0
 4323 1266 00          	.byte 0x0
 4324 1267 06          	.uleb128 0x6
 4325 1268 02          	.byte 0x2
 4326 1269 02          	.byte 0x2
 4327 126a 4F 27       	.2byte 0x274f
 4328 126c 7B 12 00 00 	.4byte 0x127b
 4329 1270 07          	.uleb128 0x7
 4330 1271 FF 10 00 00 	.4byte 0x10ff
 4331 1275 07          	.uleb128 0x7
 4332 1276 5B 11 00 00 	.4byte 0x115b
 4333 127a 00          	.byte 0x0
 4334 127b 08          	.uleb128 0x8
 4335 127c 74 61 67 49 	.asciz "tagIPC7BITS"
 4335      50 43 37 42 
 4335      49 54 53 00 
 4336 1288 02          	.byte 0x2
 4337 1289 02          	.byte 0x2
 4338 128a 4E 27       	.2byte 0x274e
 4339 128c 99 12 00 00 	.4byte 0x1299
 4340 1290 09          	.uleb128 0x9
 4341 1291 67 12 00 00 	.4byte 0x1267
 4342 1295 02          	.byte 0x2
 4343 1296 23          	.byte 0x23
 4344 1297 00          	.uleb128 0x0
 4345 1298 00          	.byte 0x0
 4346 1299 0A          	.uleb128 0xa
 4347 129a 49 50 43 37 	.asciz "IPC7BITS"
 4347      42 49 54 53 
 4347      00 
 4348 12a3 02          	.byte 0x2
 4349 12a4 6B 27       	.2byte 0x276b
 4350 12a6 7B 12 00 00 	.4byte 0x127b
 4351 12aa 04          	.uleb128 0x4
 4352 12ab 02          	.byte 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 90


 4353 12ac 02          	.byte 0x2
 4354 12ad 7C 28       	.2byte 0x287c
 4355 12af F0 12 00 00 	.4byte 0x12f0
 4356 12b3 05          	.uleb128 0x5
 4357 12b4 55 31 45 49 	.asciz "U1EIP"
 4357      50 00 
 4358 12ba 02          	.byte 0x2
 4359 12bb 7E 28       	.2byte 0x287e
 4360 12bd E9 00 00 00 	.4byte 0xe9
 4361 12c1 02          	.byte 0x2
 4362 12c2 03          	.byte 0x3
 4363 12c3 09          	.byte 0x9
 4364 12c4 02          	.byte 0x2
 4365 12c5 23          	.byte 0x23
 4366 12c6 00          	.uleb128 0x0
 4367 12c7 05          	.uleb128 0x5
 4368 12c8 55 32 45 49 	.asciz "U2EIP"
 4368      50 00 
 4369 12ce 02          	.byte 0x2
 4370 12cf 80 28       	.2byte 0x2880
 4371 12d1 E9 00 00 00 	.4byte 0xe9
 4372 12d5 02          	.byte 0x2
 4373 12d6 03          	.byte 0x3
 4374 12d7 05          	.byte 0x5
 4375 12d8 02          	.byte 0x2
 4376 12d9 23          	.byte 0x23
 4377 12da 00          	.uleb128 0x0
 4378 12db 05          	.uleb128 0x5
 4379 12dc 43 52 43 49 	.asciz "CRCIP"
 4379      50 00 
 4380 12e2 02          	.byte 0x2
 4381 12e3 82 28       	.2byte 0x2882
 4382 12e5 E9 00 00 00 	.4byte 0xe9
 4383 12e9 02          	.byte 0x2
 4384 12ea 03          	.byte 0x3
 4385 12eb 01          	.byte 0x1
 4386 12ec 02          	.byte 0x2
 4387 12ed 23          	.byte 0x23
 4388 12ee 00          	.uleb128 0x0
 4389 12ef 00          	.byte 0x0
 4390 12f0 04          	.uleb128 0x4
 4391 12f1 02          	.byte 0x2
 4392 12f2 02          	.byte 0x2
 4393 12f3 84 28       	.2byte 0x2884
 4394 12f5 B7 13 00 00 	.4byte 0x13b7
 4395 12f9 05          	.uleb128 0x5
 4396 12fa 55 31 45 49 	.asciz "U1EIP0"
 4396      50 30 00 
 4397 1301 02          	.byte 0x2
 4398 1302 86 28       	.2byte 0x2886
 4399 1304 E9 00 00 00 	.4byte 0xe9
 4400 1308 02          	.byte 0x2
 4401 1309 01          	.byte 0x1
 4402 130a 0B          	.byte 0xb
 4403 130b 02          	.byte 0x2
 4404 130c 23          	.byte 0x23
 4405 130d 00          	.uleb128 0x0
MPLAB XC16 ASSEMBLY Listing:   			page 91


 4406 130e 05          	.uleb128 0x5
 4407 130f 55 31 45 49 	.asciz "U1EIP1"
 4407      50 31 00 
 4408 1316 02          	.byte 0x2
 4409 1317 87 28       	.2byte 0x2887
 4410 1319 E9 00 00 00 	.4byte 0xe9
 4411 131d 02          	.byte 0x2
 4412 131e 01          	.byte 0x1
 4413 131f 0A          	.byte 0xa
 4414 1320 02          	.byte 0x2
 4415 1321 23          	.byte 0x23
 4416 1322 00          	.uleb128 0x0
 4417 1323 05          	.uleb128 0x5
 4418 1324 55 31 45 49 	.asciz "U1EIP2"
 4418      50 32 00 
 4419 132b 02          	.byte 0x2
 4420 132c 88 28       	.2byte 0x2888
 4421 132e E9 00 00 00 	.4byte 0xe9
 4422 1332 02          	.byte 0x2
 4423 1333 01          	.byte 0x1
 4424 1334 09          	.byte 0x9
 4425 1335 02          	.byte 0x2
 4426 1336 23          	.byte 0x23
 4427 1337 00          	.uleb128 0x0
 4428 1338 05          	.uleb128 0x5
 4429 1339 55 32 45 49 	.asciz "U2EIP0"
 4429      50 30 00 
 4430 1340 02          	.byte 0x2
 4431 1341 8A 28       	.2byte 0x288a
 4432 1343 E9 00 00 00 	.4byte 0xe9
 4433 1347 02          	.byte 0x2
 4434 1348 01          	.byte 0x1
 4435 1349 07          	.byte 0x7
 4436 134a 02          	.byte 0x2
 4437 134b 23          	.byte 0x23
 4438 134c 00          	.uleb128 0x0
 4439 134d 05          	.uleb128 0x5
 4440 134e 55 32 45 49 	.asciz "U2EIP1"
 4440      50 31 00 
 4441 1355 02          	.byte 0x2
 4442 1356 8B 28       	.2byte 0x288b
 4443 1358 E9 00 00 00 	.4byte 0xe9
 4444 135c 02          	.byte 0x2
 4445 135d 01          	.byte 0x1
 4446 135e 06          	.byte 0x6
 4447 135f 02          	.byte 0x2
 4448 1360 23          	.byte 0x23
 4449 1361 00          	.uleb128 0x0
 4450 1362 05          	.uleb128 0x5
 4451 1363 55 32 45 49 	.asciz "U2EIP2"
 4451      50 32 00 
 4452 136a 02          	.byte 0x2
 4453 136b 8C 28       	.2byte 0x288c
 4454 136d E9 00 00 00 	.4byte 0xe9
 4455 1371 02          	.byte 0x2
 4456 1372 01          	.byte 0x1
 4457 1373 05          	.byte 0x5
MPLAB XC16 ASSEMBLY Listing:   			page 92


 4458 1374 02          	.byte 0x2
 4459 1375 23          	.byte 0x23
 4460 1376 00          	.uleb128 0x0
 4461 1377 05          	.uleb128 0x5
 4462 1378 43 52 43 49 	.asciz "CRCIP0"
 4462      50 30 00 
 4463 137f 02          	.byte 0x2
 4464 1380 8E 28       	.2byte 0x288e
 4465 1382 E9 00 00 00 	.4byte 0xe9
 4466 1386 02          	.byte 0x2
 4467 1387 01          	.byte 0x1
 4468 1388 03          	.byte 0x3
 4469 1389 02          	.byte 0x2
 4470 138a 23          	.byte 0x23
 4471 138b 00          	.uleb128 0x0
 4472 138c 05          	.uleb128 0x5
 4473 138d 43 52 43 49 	.asciz "CRCIP1"
 4473      50 31 00 
 4474 1394 02          	.byte 0x2
 4475 1395 8F 28       	.2byte 0x288f
 4476 1397 E9 00 00 00 	.4byte 0xe9
 4477 139b 02          	.byte 0x2
 4478 139c 01          	.byte 0x1
 4479 139d 02          	.byte 0x2
 4480 139e 02          	.byte 0x2
 4481 139f 23          	.byte 0x23
 4482 13a0 00          	.uleb128 0x0
 4483 13a1 05          	.uleb128 0x5
 4484 13a2 43 52 43 49 	.asciz "CRCIP2"
 4484      50 32 00 
 4485 13a9 02          	.byte 0x2
 4486 13aa 90 28       	.2byte 0x2890
 4487 13ac E9 00 00 00 	.4byte 0xe9
 4488 13b0 02          	.byte 0x2
 4489 13b1 01          	.byte 0x1
 4490 13b2 01          	.byte 0x1
 4491 13b3 02          	.byte 0x2
 4492 13b4 23          	.byte 0x23
 4493 13b5 00          	.uleb128 0x0
 4494 13b6 00          	.byte 0x0
 4495 13b7 06          	.uleb128 0x6
 4496 13b8 02          	.byte 0x2
 4497 13b9 02          	.byte 0x2
 4498 13ba 7B 28       	.2byte 0x287b
 4499 13bc CB 13 00 00 	.4byte 0x13cb
 4500 13c0 07          	.uleb128 0x7
 4501 13c1 AA 12 00 00 	.4byte 0x12aa
 4502 13c5 07          	.uleb128 0x7
 4503 13c6 F0 12 00 00 	.4byte 0x12f0
 4504 13ca 00          	.byte 0x0
 4505 13cb 08          	.uleb128 0x8
 4506 13cc 74 61 67 49 	.asciz "tagIPC16BITS"
 4506      50 43 31 36 
 4506      42 49 54 53 
 4506      00 
 4507 13d9 02          	.byte 0x2
 4508 13da 02          	.byte 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 93


 4509 13db 7A 28       	.2byte 0x287a
 4510 13dd EA 13 00 00 	.4byte 0x13ea
 4511 13e1 09          	.uleb128 0x9
 4512 13e2 B7 13 00 00 	.4byte 0x13b7
 4513 13e6 02          	.byte 0x2
 4514 13e7 23          	.byte 0x23
 4515 13e8 00          	.uleb128 0x0
 4516 13e9 00          	.byte 0x0
 4517 13ea 0A          	.uleb128 0xa
 4518 13eb 49 50 43 31 	.asciz "IPC16BITS"
 4518      36 42 49 54 
 4518      53 00 
 4519 13f5 02          	.byte 0x2
 4520 13f6 93 28       	.2byte 0x2893
 4521 13f8 CB 13 00 00 	.4byte 0x13cb
 4522 13fc 0C          	.uleb128 0xc
 4523 13fd 02          	.byte 0x2
 4524 13fe 04          	.byte 0x4
 4525 13ff 0C          	.byte 0xc
 4526 1400 48 14 00 00 	.4byte 0x1448
 4527 1404 0D          	.uleb128 0xd
 4528 1405 5F 38 5F 42 	.asciz "_8_BIT_NO_PAR"
 4528      49 54 5F 4E 
 4528      4F 5F 50 41 
 4528      52 00 
 4529 1413 00          	.sleb128 0
 4530 1414 0D          	.uleb128 0xd
 4531 1415 5F 38 5F 42 	.asciz "_8_BIT_EVEN_PAR"
 4531      49 54 5F 45 
 4531      56 45 4E 5F 
 4531      50 41 52 00 
 4532 1425 01          	.sleb128 1
 4533 1426 0D          	.uleb128 0xd
 4534 1427 5F 38 5F 42 	.asciz "_8_BIT_ODD_PAR"
 4534      49 54 5F 4F 
 4534      44 44 5F 50 
 4534      41 52 00 
 4535 1436 02          	.sleb128 2
 4536 1437 0D          	.uleb128 0xd
 4537 1438 5F 39 5F 42 	.asciz "_9_BIT_NO_PAR"
 4537      49 54 5F 4E 
 4537      4F 5F 50 41 
 4537      52 00 
 4538 1446 03          	.sleb128 3
 4539 1447 00          	.byte 0x0
 4540 1448 03          	.uleb128 0x3
 4541 1449 44 61 74 61 	.asciz "Data_Parity"
 4541      5F 50 61 72 
 4541      69 74 79 00 
 4542 1455 04          	.byte 0x4
 4543 1456 0C          	.byte 0xc
 4544 1457 FC 13 00 00 	.4byte 0x13fc
 4545 145b 0E          	.uleb128 0xe
 4546 145c 10          	.byte 0x10
 4547 145d 04          	.byte 0x4
 4548 145e 0F          	.byte 0xf
 4549 145f 14 15 00 00 	.4byte 0x1514
MPLAB XC16 ASSEMBLY Listing:   			page 94


 4550 1463 0F          	.uleb128 0xf
 4551 1464 70 64 73 65 	.asciz "pdsel"
 4551      6C 00 
 4552 146a 04          	.byte 0x4
 4553 146b 10          	.byte 0x10
 4554 146c 48 14 00 00 	.4byte 0x1448
 4555 1470 02          	.byte 0x2
 4556 1471 23          	.byte 0x23
 4557 1472 00          	.uleb128 0x0
 4558 1473 0F          	.uleb128 0xf
 4559 1474 6E 75 6D 62 	.asciz "number_stop_bits"
 4559      65 72 5F 73 
 4559      74 6F 70 5F 
 4559      62 69 74 73 
 4559      00 
 4560 1485 04          	.byte 0x4
 4561 1486 11          	.byte 0x11
 4562 1487 F9 00 00 00 	.4byte 0xf9
 4563 148b 02          	.byte 0x2
 4564 148c 23          	.byte 0x23
 4565 148d 02          	.uleb128 0x2
 4566 148e 0F          	.uleb128 0xf
 4567 148f 74 72 61 6E 	.asciz "transmit"
 4567      73 6D 69 74 
 4567      00 
 4568 1498 04          	.byte 0x4
 4569 1499 12          	.byte 0x12
 4570 149a 14 15 00 00 	.4byte 0x1514
 4571 149e 02          	.byte 0x2
 4572 149f 23          	.byte 0x23
 4573 14a0 04          	.uleb128 0x4
 4574 14a1 0F          	.uleb128 0xf
 4575 14a2 42 61 75 64 	.asciz "BaudRate"
 4575      52 61 74 65 
 4575      00 
 4576 14ab 04          	.byte 0x4
 4577 14ac 13          	.byte 0x13
 4578 14ad 1D 15 00 00 	.4byte 0x151d
 4579 14b1 02          	.byte 0x2
 4580 14b2 23          	.byte 0x23
 4581 14b3 06          	.uleb128 0x6
 4582 14b4 10          	.uleb128 0x10
 4583 14b5 55 78 52 58 	.asciz "UxRX_idle_state"
 4583      5F 69 64 6C 
 4583      65 5F 73 74 
 4583      61 74 65 00 
 4584 14c5 04          	.byte 0x4
 4585 14c6 14          	.byte 0x14
 4586 14c7 F9 00 00 00 	.4byte 0xf9
 4587 14cb 02          	.byte 0x2
 4588 14cc 01          	.byte 0x1
 4589 14cd 0F          	.byte 0xf
 4590 14ce 02          	.byte 0x2
 4591 14cf 23          	.byte 0x23
 4592 14d0 0A          	.uleb128 0xa
 4593 14d1 10          	.uleb128 0x10
 4594 14d2 55 78 54 58 	.asciz "UxTX_idle_state"
MPLAB XC16 ASSEMBLY Listing:   			page 95


 4594      5F 69 64 6C 
 4594      65 5F 73 74 
 4594      61 74 65 00 
 4595 14e2 04          	.byte 0x4
 4596 14e3 15          	.byte 0x15
 4597 14e4 F9 00 00 00 	.4byte 0xf9
 4598 14e8 02          	.byte 0x2
 4599 14e9 01          	.byte 0x1
 4600 14ea 0E          	.byte 0xe
 4601 14eb 02          	.byte 0x2
 4602 14ec 23          	.byte 0x23
 4603 14ed 0A          	.uleb128 0xa
 4604 14ee 10          	.uleb128 0x10
 4605 14ef 55 78 42 52 	.asciz "UxBRG"
 4605      47 00 
 4606 14f5 04          	.byte 0x4
 4607 14f6 16          	.byte 0x16
 4608 14f7 F9 00 00 00 	.4byte 0xf9
 4609 14fb 02          	.byte 0x2
 4610 14fc 10          	.byte 0x10
 4611 14fd 10          	.byte 0x10
 4612 14fe 02          	.byte 0x2
 4613 14ff 23          	.byte 0x23
 4614 1500 0C          	.uleb128 0xc
 4615 1501 10          	.uleb128 0x10
 4616 1502 42 52 47 48 	.asciz "BRGH"
 4616      00 
 4617 1507 04          	.byte 0x4
 4618 1508 17          	.byte 0x17
 4619 1509 F9 00 00 00 	.4byte 0xf9
 4620 150d 02          	.byte 0x2
 4621 150e 01          	.byte 0x1
 4622 150f 0F          	.byte 0xf
 4623 1510 02          	.byte 0x2
 4624 1511 23          	.byte 0x23
 4625 1512 0E          	.uleb128 0xe
 4626 1513 00          	.byte 0x0
 4627 1514 02          	.uleb128 0x2
 4628 1515 01          	.byte 0x1
 4629 1516 02          	.byte 0x2
 4630 1517 5F 42 6F 6F 	.asciz "_Bool"
 4630      6C 00 
 4631 151d 02          	.uleb128 0x2
 4632 151e 04          	.byte 0x4
 4633 151f 04          	.byte 0x4
 4634 1520 64 6F 75 62 	.asciz "double"
 4634      6C 65 00 
 4635 1527 03          	.uleb128 0x3
 4636 1528 55 41 52 54 	.asciz "UART_parameters"
 4636      5F 70 61 72 
 4636      61 6D 65 74 
 4636      65 72 73 00 
 4637 1538 04          	.byte 0x4
 4638 1539 18          	.byte 0x18
 4639 153a 5B 14 00 00 	.4byte 0x145b
 4640 153e 0E          	.uleb128 0xe
 4641 153f 03          	.byte 0x3
MPLAB XC16 ASSEMBLY Listing:   			page 96


 4642 1540 04          	.byte 0x4
 4643 1541 1A          	.byte 0x1a
 4644 1542 7D 15 00 00 	.4byte 0x157d
 4645 1546 0F          	.uleb128 0xf
 4646 1547 6F 76 65 72 	.asciz "overflow"
 4646      66 6C 6F 77 
 4646      00 
 4647 1550 04          	.byte 0x4
 4648 1551 1B          	.byte 0x1b
 4649 1552 14 15 00 00 	.4byte 0x1514
 4650 1556 02          	.byte 0x2
 4651 1557 23          	.byte 0x23
 4652 1558 00          	.uleb128 0x0
 4653 1559 0F          	.uleb128 0xf
 4654 155a 70 61 72 69 	.asciz "parity"
 4654      74 79 00 
 4655 1561 04          	.byte 0x4
 4656 1562 1C          	.byte 0x1c
 4657 1563 14 15 00 00 	.4byte 0x1514
 4658 1567 02          	.byte 0x2
 4659 1568 23          	.byte 0x23
 4660 1569 01          	.uleb128 0x1
 4661 156a 0F          	.uleb128 0xf
 4662 156b 66 72 61 6D 	.asciz "framing"
 4662      69 6E 67 00 
 4663 1573 04          	.byte 0x4
 4664 1574 1D          	.byte 0x1d
 4665 1575 14 15 00 00 	.4byte 0x1514
 4666 1579 02          	.byte 0x2
 4667 157a 23          	.byte 0x23
 4668 157b 02          	.uleb128 0x2
 4669 157c 00          	.byte 0x0
 4670 157d 03          	.uleb128 0x3
 4671 157e 55 41 52 54 	.asciz "UART_errors"
 4671      5F 65 72 72 
 4671      6F 72 73 00 
 4672 158a 04          	.byte 0x4
 4673 158b 1E          	.byte 0x1e
 4674 158c 3E 15 00 00 	.4byte 0x153e
 4675 1590 11          	.uleb128 0x11
 4676 1591 01          	.byte 0x1
 4677 1592 62 61 75 64 	.asciz "baudrate_generator"
 4677      72 61 74 65 
 4677      5F 67 65 6E 
 4677      65 72 61 74 
 4677      6F 72 00 
 4678 15a5 01          	.byte 0x1
 4679 15a6 18          	.byte 0x18
 4680 15a7 01          	.byte 0x1
 4681 15a8 B4 00 00 00 	.4byte 0xb4
 4682 15ac 00 00 00 00 	.4byte .LFB0
 4683 15b0 00 00 00 00 	.4byte .LFE0
 4684 15b4 01          	.byte 0x1
 4685 15b5 5E          	.byte 0x5e
 4686 15b6 2E 16 00 00 	.4byte 0x162e
 4687 15ba 12          	.uleb128 0x12
 4688 15bb 00 00 00 00 	.4byte .LASF4
MPLAB XC16 ASSEMBLY Listing:   			page 97


 4689 15bf 01          	.byte 0x1
 4690 15c0 18          	.byte 0x18
 4691 15c1 2E 16 00 00 	.4byte 0x162e
 4692 15c5 02          	.byte 0x2
 4693 15c6 7E          	.byte 0x7e
 4694 15c7 10          	.sleb128 16
 4695 15c8 13          	.uleb128 0x13
 4696 15c9 42 52 47 5F 	.asciz "BRG_16"
 4696      31 36 00 
 4697 15d0 01          	.byte 0x1
 4698 15d1 19          	.byte 0x19
 4699 15d2 F9 00 00 00 	.4byte 0xf9
 4700 15d6 02          	.byte 0x2
 4701 15d7 7E          	.byte 0x7e
 4702 15d8 00          	.sleb128 0
 4703 15d9 13          	.uleb128 0x13
 4704 15da 42 52 47 5F 	.asciz "BRG_4"
 4704      34 00 
 4705 15e0 01          	.byte 0x1
 4706 15e1 1A          	.byte 0x1a
 4707 15e2 F9 00 00 00 	.4byte 0xf9
 4708 15e6 02          	.byte 0x2
 4709 15e7 7E          	.byte 0x7e
 4710 15e8 02          	.sleb128 2
 4711 15e9 13          	.uleb128 0x13
 4712 15ea 42 52 5F 31 	.asciz "BR_16"
 4712      36 00 
 4713 15f0 01          	.byte 0x1
 4714 15f1 1B          	.byte 0x1b
 4715 15f2 F9 00 00 00 	.4byte 0xf9
 4716 15f6 02          	.byte 0x2
 4717 15f7 7E          	.byte 0x7e
 4718 15f8 04          	.sleb128 4
 4719 15f9 13          	.uleb128 0x13
 4720 15fa 42 52 5F 34 	.asciz "BR_4"
 4720      00 
 4721 15ff 01          	.byte 0x1
 4722 1600 1C          	.byte 0x1c
 4723 1601 F9 00 00 00 	.4byte 0xf9
 4724 1605 02          	.byte 0x2
 4725 1606 7E          	.byte 0x7e
 4726 1607 06          	.sleb128 6
 4727 1608 13          	.uleb128 0x13
 4728 1609 65 72 72 6F 	.asciz "error_16"
 4728      72 5F 31 36 
 4728      00 
 4729 1612 01          	.byte 0x1
 4730 1613 1D          	.byte 0x1d
 4731 1614 34 16 00 00 	.4byte 0x1634
 4732 1618 02          	.byte 0x2
 4733 1619 7E          	.byte 0x7e
 4734 161a 08          	.sleb128 8
 4735 161b 13          	.uleb128 0x13
 4736 161c 65 72 72 6F 	.asciz "error_4"
 4736      72 5F 34 00 
 4737 1624 01          	.byte 0x1
 4738 1625 1E          	.byte 0x1e
MPLAB XC16 ASSEMBLY Listing:   			page 98


 4739 1626 34 16 00 00 	.4byte 0x1634
 4740 162a 02          	.byte 0x2
 4741 162b 7E          	.byte 0x7e
 4742 162c 0C          	.sleb128 12
 4743 162d 00          	.byte 0x0
 4744 162e 14          	.uleb128 0x14
 4745 162f 02          	.byte 0x2
 4746 1630 27 15 00 00 	.4byte 0x1527
 4747 1634 02          	.uleb128 0x2
 4748 1635 04          	.byte 0x4
 4749 1636 04          	.byte 0x4
 4750 1637 66 6C 6F 61 	.asciz "float"
 4750      74 00 
 4751 163d 11          	.uleb128 0x11
 4752 163e 01          	.byte 0x1
 4753 163f 63 6F 6E 66 	.asciz "config_uart1"
 4753      69 67 5F 75 
 4753      61 72 74 31 
 4753      00 
 4754 164c 01          	.byte 0x1
 4755 164d 41          	.byte 0x41
 4756 164e 01          	.byte 0x1
 4757 164f B4 00 00 00 	.4byte 0xb4
 4758 1653 00 00 00 00 	.4byte .LFB1
 4759 1657 00 00 00 00 	.4byte .LFE1
 4760 165b 01          	.byte 0x1
 4761 165c 5E          	.byte 0x5e
 4762 165d 70 16 00 00 	.4byte 0x1670
 4763 1661 12          	.uleb128 0x12
 4764 1662 00 00 00 00 	.4byte .LASF4
 4765 1666 01          	.byte 0x1
 4766 1667 41          	.byte 0x41
 4767 1668 2E 16 00 00 	.4byte 0x162e
 4768 166c 02          	.byte 0x2
 4769 166d 7E          	.byte 0x7e
 4770 166e 00          	.sleb128 0
 4771 166f 00          	.byte 0x0
 4772 1670 11          	.uleb128 0x11
 4773 1671 01          	.byte 0x1
 4774 1672 63 6F 6E 66 	.asciz "config_uart2"
 4774      69 67 5F 75 
 4774      61 72 74 32 
 4774      00 
 4775 167f 01          	.byte 0x1
 4776 1680 76          	.byte 0x76
 4777 1681 01          	.byte 0x1
 4778 1682 B4 00 00 00 	.4byte 0xb4
 4779 1686 00 00 00 00 	.4byte .LFB2
 4780 168a 00 00 00 00 	.4byte .LFE2
 4781 168e 01          	.byte 0x1
 4782 168f 5E          	.byte 0x5e
 4783 1690 A3 16 00 00 	.4byte 0x16a3
 4784 1694 12          	.uleb128 0x12
 4785 1695 00 00 00 00 	.4byte .LASF4
 4786 1699 01          	.byte 0x1
 4787 169a 76          	.byte 0x76
 4788 169b 2E 16 00 00 	.4byte 0x162e
MPLAB XC16 ASSEMBLY Listing:   			page 99


 4789 169f 02          	.byte 0x2
 4790 16a0 7E          	.byte 0x7e
 4791 16a1 00          	.sleb128 0
 4792 16a2 00          	.byte 0x0
 4793 16a3 15          	.uleb128 0x15
 4794 16a4 01          	.byte 0x1
 4795 16a5 65 6E 61 62 	.asciz "enable_uart1"
 4795      6C 65 5F 75 
 4795      61 72 74 31 
 4795      00 
 4796 16b2 01          	.byte 0x1
 4797 16b3 AB          	.byte 0xab
 4798 16b4 01          	.byte 0x1
 4799 16b5 00 00 00 00 	.4byte .LFB3
 4800 16b9 00 00 00 00 	.4byte .LFE3
 4801 16bd 01          	.byte 0x1
 4802 16be 5E          	.byte 0x5e
 4803 16bf 15          	.uleb128 0x15
 4804 16c0 01          	.byte 0x1
 4805 16c1 65 6E 61 62 	.asciz "enable_uart2"
 4805      6C 65 5F 75 
 4805      61 72 74 32 
 4805      00 
 4806 16ce 01          	.byte 0x1
 4807 16cf B6          	.byte 0xb6
 4808 16d0 01          	.byte 0x1
 4809 16d1 00 00 00 00 	.4byte .LFB4
 4810 16d5 00 00 00 00 	.4byte .LFE4
 4811 16d9 01          	.byte 0x1
 4812 16da 5E          	.byte 0x5e
 4813 16db 15          	.uleb128 0x15
 4814 16dc 01          	.byte 0x1
 4815 16dd 64 69 73 61 	.asciz "disable_uart1"
 4815      62 6C 65 5F 
 4815      75 61 72 74 
 4815      31 00 
 4816 16eb 01          	.byte 0x1
 4817 16ec C1          	.byte 0xc1
 4818 16ed 01          	.byte 0x1
 4819 16ee 00 00 00 00 	.4byte .LFB5
 4820 16f2 00 00 00 00 	.4byte .LFE5
 4821 16f6 01          	.byte 0x1
 4822 16f7 5E          	.byte 0x5e
 4823 16f8 15          	.uleb128 0x15
 4824 16f9 01          	.byte 0x1
 4825 16fa 64 69 73 61 	.asciz "disable_uart2"
 4825      62 6C 65 5F 
 4825      75 61 72 74 
 4825      32 00 
 4826 1708 01          	.byte 0x1
 4827 1709 CC          	.byte 0xcc
 4828 170a 01          	.byte 0x1
 4829 170b 00 00 00 00 	.4byte .LFB6
 4830 170f 00 00 00 00 	.4byte .LFE6
 4831 1713 01          	.byte 0x1
 4832 1714 5E          	.byte 0x5e
 4833 1715 16          	.uleb128 0x16
MPLAB XC16 ASSEMBLY Listing:   			page 100


 4834 1716 01          	.byte 0x1
 4835 1717 72 65 63 65 	.asciz "receive_uart1_buffer_empty"
 4835      69 76 65 5F 
 4835      75 61 72 74 
 4835      31 5F 62 75 
 4835      66 66 65 72 
 4835      5F 65 6D 70 
 4835      74 79 00 
 4836 1732 01          	.byte 0x1
 4837 1733 D8          	.byte 0xd8
 4838 1734 01          	.byte 0x1
 4839 1735 14 15 00 00 	.4byte 0x1514
 4840 1739 00 00 00 00 	.4byte .LFB7
 4841 173d 00 00 00 00 	.4byte .LFE7
 4842 1741 01          	.byte 0x1
 4843 1742 5E          	.byte 0x5e
 4844 1743 16          	.uleb128 0x16
 4845 1744 01          	.byte 0x1
 4846 1745 72 65 63 65 	.asciz "receive_uart2_buffer_empty"
 4846      69 76 65 5F 
 4846      75 61 72 74 
 4846      32 5F 62 75 
 4846      66 66 65 72 
 4846      5F 65 6D 70 
 4846      74 79 00 
 4847 1760 01          	.byte 0x1
 4848 1761 E6          	.byte 0xe6
 4849 1762 01          	.byte 0x1
 4850 1763 14 15 00 00 	.4byte 0x1514
 4851 1767 00 00 00 00 	.4byte .LFB8
 4852 176b 00 00 00 00 	.4byte .LFE8
 4853 176f 01          	.byte 0x1
 4854 1770 5E          	.byte 0x5e
 4855 1771 11          	.uleb128 0x11
 4856 1772 01          	.byte 0x1
 4857 1773 67 65 74 5F 	.asciz "get_FIFO_data"
 4857      46 49 46 4F 
 4857      5F 64 61 74 
 4857      61 00 
 4858 1781 01          	.byte 0x1
 4859 1782 F3          	.byte 0xf3
 4860 1783 01          	.byte 0x1
 4861 1784 BB 17 00 00 	.4byte 0x17bb
 4862 1788 00 00 00 00 	.4byte .LFB9
 4863 178c 00 00 00 00 	.4byte .LFE9
 4864 1790 01          	.byte 0x1
 4865 1791 5E          	.byte 0x5e
 4866 1792 BB 17 00 00 	.4byte 0x17bb
 4867 1796 17          	.uleb128 0x17
 4868 1797 75 61 72 74 	.asciz "uart_number"
 4868      5F 6E 75 6D 
 4868      62 65 72 00 
 4869 17a3 01          	.byte 0x1
 4870 17a4 F3          	.byte 0xf3
 4871 17a5 F9 00 00 00 	.4byte 0xf9
 4872 17a9 02          	.byte 0x2
 4873 17aa 7E          	.byte 0x7e
MPLAB XC16 ASSEMBLY Listing:   			page 101


 4874 17ab 02          	.sleb128 2
 4875 17ac 13          	.uleb128 0x13
 4876 17ad 61 75 78 00 	.asciz "aux"
 4877 17b1 01          	.byte 0x1
 4878 17b2 F4          	.byte 0xf4
 4879 17b3 BB 17 00 00 	.4byte 0x17bb
 4880 17b7 02          	.byte 0x2
 4881 17b8 7E          	.byte 0x7e
 4882 17b9 00          	.sleb128 0
 4883 17ba 00          	.byte 0x0
 4884 17bb 02          	.uleb128 0x2
 4885 17bc 01          	.byte 0x1
 4886 17bd 06          	.byte 0x6
 4887 17be 63 68 61 72 	.asciz "char"
 4887      00 
 4888 17c3 18          	.uleb128 0x18
 4889 17c4 01          	.byte 0x1
 4890 17c5 5F 55 31 52 	.asciz "_U1RXInterrupt"
 4890      58 49 6E 74 
 4890      65 72 72 75 
 4890      70 74 00 
 4891 17d4 01          	.byte 0x1
 4892 17d5 08 01       	.2byte 0x108
 4893 17d7 01          	.byte 0x1
 4894 17d8 00 00 00 00 	.4byte .LFB10
 4895 17dc 00 00 00 00 	.4byte .LFE10
 4896 17e0 01          	.byte 0x1
 4897 17e1 5E          	.byte 0x5e
 4898 17e2 18          	.uleb128 0x18
 4899 17e3 01          	.byte 0x1
 4900 17e4 5F 55 32 52 	.asciz "_U2RXInterrupt"
 4900      58 49 6E 74 
 4900      65 72 72 75 
 4900      70 74 00 
 4901 17f3 01          	.byte 0x1
 4902 17f4 22 01       	.2byte 0x122
 4903 17f6 01          	.byte 0x1
 4904 17f7 00 00 00 00 	.4byte .LFB11
 4905 17fb 00 00 00 00 	.4byte .LFE11
 4906 17ff 01          	.byte 0x1
 4907 1800 5E          	.byte 0x5e
 4908 1801 19          	.uleb128 0x19
 4909 1802 01          	.byte 0x1
 4910 1803 5F 55 31 45 	.asciz "_U1ErrInterrupt"
 4910      72 72 49 6E 
 4910      74 65 72 72 
 4910      75 70 74 00 
 4911 1813 01          	.byte 0x1
 4912 1814 39 01       	.2byte 0x139
 4913 1816 01          	.byte 0x1
 4914 1817 00 00 00 00 	.4byte .LFB12
 4915 181b 00 00 00 00 	.4byte .LFE12
 4916 181f 01          	.byte 0x1
 4917 1820 5E          	.byte 0x5e
 4918 1821 33 18 00 00 	.4byte 0x1833
 4919 1825 1A          	.uleb128 0x1a
 4920 1826 69 00       	.asciz "i"
MPLAB XC16 ASSEMBLY Listing:   			page 102


 4921 1828 01          	.byte 0x1
 4922 1829 3A 01       	.2byte 0x13a
 4923 182b B4 00 00 00 	.4byte 0xb4
 4924 182f 02          	.byte 0x2
 4925 1830 7E          	.byte 0x7e
 4926 1831 00          	.sleb128 0
 4927 1832 00          	.byte 0x0
 4928 1833 19          	.uleb128 0x19
 4929 1834 01          	.byte 0x1
 4930 1835 5F 55 32 45 	.asciz "_U2ErrInterrupt"
 4930      72 72 49 6E 
 4930      74 65 72 72 
 4930      75 70 74 00 
 4931 1845 01          	.byte 0x1
 4932 1846 50 01       	.2byte 0x150
 4933 1848 01          	.byte 0x1
 4934 1849 00 00 00 00 	.4byte .LFB13
 4935 184d 00 00 00 00 	.4byte .LFE13
 4936 1851 01          	.byte 0x1
 4937 1852 5E          	.byte 0x5e
 4938 1853 65 18 00 00 	.4byte 0x1865
 4939 1857 1A          	.uleb128 0x1a
 4940 1858 69 00       	.asciz "i"
 4941 185a 01          	.byte 0x1
 4942 185b 51 01       	.2byte 0x151
 4943 185d B4 00 00 00 	.4byte 0xb4
 4944 1861 02          	.byte 0x2
 4945 1862 7E          	.byte 0x7e
 4946 1863 00          	.sleb128 0
 4947 1864 00          	.byte 0x0
 4948 1865 1B          	.uleb128 0x1b
 4949 1866 01          	.byte 0x1
 4950 1867 67 65 74 5F 	.asciz "get_uart1_errors"
 4950      75 61 72 74 
 4950      31 5F 65 72 
 4950      72 6F 72 73 
 4950      00 
 4951 1878 01          	.byte 0x1
 4952 1879 67 01       	.2byte 0x167
 4953 187b 01          	.byte 0x1
 4954 187c 7D 15 00 00 	.4byte 0x157d
 4955 1880 00 00 00 00 	.4byte .LFB14
 4956 1884 00 00 00 00 	.4byte .LFE14
 4957 1888 01          	.byte 0x1
 4958 1889 5E          	.byte 0x5e
 4959 188a 1B          	.uleb128 0x1b
 4960 188b 01          	.byte 0x1
 4961 188c 67 65 74 5F 	.asciz "get_uart2_errors"
 4961      75 61 72 74 
 4961      32 5F 65 72 
 4961      72 6F 72 73 
 4961      00 
 4962 189d 01          	.byte 0x1
 4963 189e 71 01       	.2byte 0x171
 4964 18a0 01          	.byte 0x1
 4965 18a1 7D 15 00 00 	.4byte 0x157d
 4966 18a5 00 00 00 00 	.4byte .LFB15
MPLAB XC16 ASSEMBLY Listing:   			page 103


 4967 18a9 00 00 00 00 	.4byte .LFE15
 4968 18ad 01          	.byte 0x1
 4969 18ae 5E          	.byte 0x5e
 4970 18af 1C          	.uleb128 0x1c
 4971 18b0 01          	.byte 0x1
 4972 18b1 70 6F 70 5F 	.asciz "pop_uart1"
 4972      75 61 72 74 
 4972      31 00 
 4973 18bb 01          	.byte 0x1
 4974 18bc 7B 01       	.2byte 0x17b
 4975 18be 01          	.byte 0x1
 4976 18bf BB 17 00 00 	.4byte 0x17bb
 4977 18c3 00 00 00 00 	.4byte .LFB16
 4978 18c7 00 00 00 00 	.4byte .LFE16
 4979 18cb 01          	.byte 0x1
 4980 18cc 5E          	.byte 0x5e
 4981 18cd E1 18 00 00 	.4byte 0x18e1
 4982 18d1 1A          	.uleb128 0x1a
 4983 18d2 61 75 78 00 	.asciz "aux"
 4984 18d6 01          	.byte 0x1
 4985 18d7 7C 01       	.2byte 0x17c
 4986 18d9 F9 00 00 00 	.4byte 0xf9
 4987 18dd 02          	.byte 0x2
 4988 18de 7E          	.byte 0x7e
 4989 18df 00          	.sleb128 0
 4990 18e0 00          	.byte 0x0
 4991 18e1 1C          	.uleb128 0x1c
 4992 18e2 01          	.byte 0x1
 4993 18e3 70 6F 70 5F 	.asciz "pop_uart2"
 4993      75 61 72 74 
 4993      32 00 
 4994 18ed 01          	.byte 0x1
 4995 18ee 88 01       	.2byte 0x188
 4996 18f0 01          	.byte 0x1
 4997 18f1 BB 17 00 00 	.4byte 0x17bb
 4998 18f5 00 00 00 00 	.4byte .LFB17
 4999 18f9 00 00 00 00 	.4byte .LFE17
 5000 18fd 01          	.byte 0x1
 5001 18fe 5E          	.byte 0x5e
 5002 18ff 13 19 00 00 	.4byte 0x1913
 5003 1903 1A          	.uleb128 0x1a
 5004 1904 61 75 78 00 	.asciz "aux"
 5005 1908 01          	.byte 0x1
 5006 1909 89 01       	.2byte 0x189
 5007 190b F9 00 00 00 	.4byte 0xf9
 5008 190f 02          	.byte 0x2
 5009 1910 7E          	.byte 0x7e
 5010 1911 00          	.sleb128 0
 5011 1912 00          	.byte 0x0
 5012 1913 1B          	.uleb128 0x1b
 5013 1914 01          	.byte 0x1
 5014 1915 75 61 72 74 	.asciz "uartRX1_empty"
 5014      52 58 31 5F 
 5014      65 6D 70 74 
 5014      79 00 
 5015 1923 01          	.byte 0x1
 5016 1924 95 01       	.2byte 0x195
MPLAB XC16 ASSEMBLY Listing:   			page 104


 5017 1926 01          	.byte 0x1
 5018 1927 14 15 00 00 	.4byte 0x1514
 5019 192b 00 00 00 00 	.4byte .LFB18
 5020 192f 00 00 00 00 	.4byte .LFE18
 5021 1933 01          	.byte 0x1
 5022 1934 5E          	.byte 0x5e
 5023 1935 1B          	.uleb128 0x1b
 5024 1936 01          	.byte 0x1
 5025 1937 75 61 72 74 	.asciz "uartRX2_empty"
 5025      52 58 32 5F 
 5025      65 6D 70 74 
 5025      79 00 
 5026 1945 01          	.byte 0x1
 5027 1946 A3 01       	.2byte 0x1a3
 5028 1948 01          	.byte 0x1
 5029 1949 14 15 00 00 	.4byte 0x1514
 5030 194d 00 00 00 00 	.4byte .LFB19
 5031 1951 00 00 00 00 	.4byte .LFE19
 5032 1955 01          	.byte 0x1
 5033 1956 5E          	.byte 0x5e
 5034 1957 1D          	.uleb128 0x1d
 5035 1958 00 00 00 00 	.4byte .LASF5
 5036 195c 02          	.byte 0x2
 5037 195d A8 06       	.2byte 0x6a8
 5038 195f 65 19 00 00 	.4byte 0x1965
 5039 1963 01          	.byte 0x1
 5040 1964 01          	.byte 0x1
 5041 1965 1E          	.uleb128 0x1e
 5042 1966 D2 02 00 00 	.4byte 0x2d2
 5043 196a 1D          	.uleb128 0x1d
 5044 196b 00 00 00 00 	.4byte .LASF6
 5045 196f 02          	.byte 0x2
 5046 1970 C8 06       	.2byte 0x6c8
 5047 1972 78 19 00 00 	.4byte 0x1978
 5048 1976 01          	.byte 0x1
 5049 1977 01          	.byte 0x1
 5050 1978 1E          	.uleb128 0x1e
 5051 1979 78 04 00 00 	.4byte 0x478
 5052 197d 1F          	.uleb128 0x1f
 5053 197e 55 31 52 58 	.asciz "U1RXREG"
 5053      52 45 47 00 
 5054 1986 02          	.byte 0x2
 5055 1987 CD 06       	.2byte 0x6cd
 5056 1989 8F 19 00 00 	.4byte 0x198f
 5057 198d 01          	.byte 0x1
 5058 198e 01          	.byte 0x1
 5059 198f 1E          	.uleb128 0x1e
 5060 1990 E9 00 00 00 	.4byte 0xe9
 5061 1994 1F          	.uleb128 0x1f
 5062 1995 55 31 42 52 	.asciz "U1BRG"
 5062      47 00 
 5063 199b 02          	.byte 0x2
 5064 199c CF 06       	.2byte 0x6cf
 5065 199e 8F 19 00 00 	.4byte 0x198f
 5066 19a2 01          	.byte 0x1
 5067 19a3 01          	.byte 0x1
 5068 19a4 1D          	.uleb128 0x1d
MPLAB XC16 ASSEMBLY Listing:   			page 105


 5069 19a5 00 00 00 00 	.4byte .LASF7
 5070 19a9 02          	.byte 0x2
 5071 19aa F0 06       	.2byte 0x6f0
 5072 19ac B2 19 00 00 	.4byte 0x19b2
 5073 19b0 01          	.byte 0x1
 5074 19b1 01          	.byte 0x1
 5075 19b2 1E          	.uleb128 0x1e
 5076 19b3 24 06 00 00 	.4byte 0x624
 5077 19b7 1D          	.uleb128 0x1d
 5078 19b8 00 00 00 00 	.4byte .LASF8
 5079 19bc 02          	.byte 0x2
 5080 19bd 10 07       	.2byte 0x710
 5081 19bf C5 19 00 00 	.4byte 0x19c5
 5082 19c3 01          	.byte 0x1
 5083 19c4 01          	.byte 0x1
 5084 19c5 1E          	.uleb128 0x1e
 5085 19c6 CA 07 00 00 	.4byte 0x7ca
 5086 19ca 1F          	.uleb128 0x1f
 5087 19cb 55 32 52 58 	.asciz "U2RXREG"
 5087      52 45 47 00 
 5088 19d3 02          	.byte 0x2
 5089 19d4 15 07       	.2byte 0x715
 5090 19d6 8F 19 00 00 	.4byte 0x198f
 5091 19da 01          	.byte 0x1
 5092 19db 01          	.byte 0x1
 5093 19dc 1F          	.uleb128 0x1f
 5094 19dd 55 32 42 52 	.asciz "U2BRG"
 5094      47 00 
 5095 19e3 02          	.byte 0x2
 5096 19e4 17 07       	.2byte 0x717
 5097 19e6 8F 19 00 00 	.4byte 0x198f
 5098 19ea 01          	.byte 0x1
 5099 19eb 01          	.byte 0x1
 5100 19ec 1D          	.uleb128 0x1d
 5101 19ed 00 00 00 00 	.4byte .LASF9
 5102 19f1 02          	.byte 0x2
 5103 19f2 1A 25       	.2byte 0x251a
 5104 19f4 FA 19 00 00 	.4byte 0x19fa
 5105 19f8 01          	.byte 0x1
 5106 19f9 01          	.byte 0x1
 5107 19fa 1E          	.uleb128 0x1e
 5108 19fb 37 09 00 00 	.4byte 0x937
 5109 19ff 1D          	.uleb128 0x1d
 5110 1a00 00 00 00 00 	.4byte .LASF10
 5111 1a04 02          	.byte 0x2
 5112 1a05 30 25       	.2byte 0x2530
 5113 1a07 0D 1A 00 00 	.4byte 0x1a0d
 5114 1a0b 01          	.byte 0x1
 5115 1a0c 01          	.byte 0x1
 5116 1a0d 1E          	.uleb128 0x1e
 5117 1a0e A3 0A 00 00 	.4byte 0xaa3
 5118 1a12 1D          	.uleb128 0x1d
 5119 1a13 00 00 00 00 	.4byte .LASF11
 5120 1a17 02          	.byte 0x2
 5121 1a18 6D 25       	.2byte 0x256d
 5122 1a1a 20 1A 00 00 	.4byte 0x1a20
 5123 1a1e 01          	.byte 0x1
MPLAB XC16 ASSEMBLY Listing:   			page 106


 5124 1a1f 01          	.byte 0x1
 5125 1a20 1E          	.uleb128 0x1e
 5126 1a21 85 0B 00 00 	.4byte 0xb85
 5127 1a25 1D          	.uleb128 0x1d
 5128 1a26 00 00 00 00 	.4byte .LASF12
 5129 1a2a 02          	.byte 0x2
 5130 1a2b C6 25       	.2byte 0x25c6
 5131 1a2d 33 1A 00 00 	.4byte 0x1a33
 5132 1a31 01          	.byte 0x1
 5133 1a32 01          	.byte 0x1
 5134 1a33 1E          	.uleb128 0x1e
 5135 1a34 F1 0C 00 00 	.4byte 0xcf1
 5136 1a38 1D          	.uleb128 0x1d
 5137 1a39 00 00 00 00 	.4byte .LASF13
 5138 1a3d 02          	.byte 0x2
 5139 1a3e DC 25       	.2byte 0x25dc
 5140 1a40 46 1A 00 00 	.4byte 0x1a46
 5141 1a44 01          	.byte 0x1
 5142 1a45 01          	.byte 0x1
 5143 1a46 1E          	.uleb128 0x1e
 5144 1a47 5D 0E 00 00 	.4byte 0xe5d
 5145 1a4b 1D          	.uleb128 0x1d
 5146 1a4c 00 00 00 00 	.4byte .LASF14
 5147 1a50 02          	.byte 0x2
 5148 1a51 19 26       	.2byte 0x2619
 5149 1a53 59 1A 00 00 	.4byte 0x1a59
 5150 1a57 01          	.byte 0x1
 5151 1a58 01          	.byte 0x1
 5152 1a59 1E          	.uleb128 0x1e
 5153 1a5a 3F 0F 00 00 	.4byte 0xf3f
 5154 1a5e 1D          	.uleb128 0x1d
 5155 1a5f 00 00 00 00 	.4byte .LASF15
 5156 1a63 02          	.byte 0x2
 5157 1a64 C2 26       	.2byte 0x26c2
 5158 1a66 6C 1A 00 00 	.4byte 0x1a6c
 5159 1a6a 01          	.byte 0x1
 5160 1a6b 01          	.byte 0x1
 5161 1a6c 1E          	.uleb128 0x1e
 5162 1a6d EE 10 00 00 	.4byte 0x10ee
 5163 1a71 1D          	.uleb128 0x1d
 5164 1a72 00 00 00 00 	.4byte .LASF16
 5165 1a76 02          	.byte 0x2
 5166 1a77 6C 27       	.2byte 0x276c
 5167 1a79 7F 1A 00 00 	.4byte 0x1a7f
 5168 1a7d 01          	.byte 0x1
 5169 1a7e 01          	.byte 0x1
 5170 1a7f 1E          	.uleb128 0x1e
 5171 1a80 99 12 00 00 	.4byte 0x1299
 5172 1a84 1D          	.uleb128 0x1d
 5173 1a85 00 00 00 00 	.4byte .LASF17
 5174 1a89 02          	.byte 0x2
 5175 1a8a 94 28       	.2byte 0x2894
 5176 1a8c 92 1A 00 00 	.4byte 0x1a92
 5177 1a90 01          	.byte 0x1
 5178 1a91 01          	.byte 0x1
 5179 1a92 1E          	.uleb128 0x1e
 5180 1a93 EA 13 00 00 	.4byte 0x13ea
MPLAB XC16 ASSEMBLY Listing:   			page 107


 5181 1a97 20          	.uleb128 0x20
 5182 1a98 F9 00 00 00 	.4byte 0xf9
 5183 1a9c A7 1A 00 00 	.4byte 0x1aa7
 5184 1aa0 21          	.uleb128 0x21
 5185 1aa1 F9 00 00 00 	.4byte 0xf9
 5186 1aa5 45          	.byte 0x45
 5187 1aa6 00          	.byte 0x0
 5188 1aa7 22          	.uleb128 0x22
 5189 1aa8 00 00 00 00 	.4byte .LASF18
 5190 1aac 01          	.byte 0x1
 5191 1aad 05          	.byte 0x5
 5192 1aae 97 1A 00 00 	.4byte 0x1a97
 5193 1ab2 01          	.byte 0x1
 5194 1ab3 01          	.byte 0x1
 5195 1ab4 22          	.uleb128 0x22
 5196 1ab5 00 00 00 00 	.4byte .LASF19
 5197 1ab9 01          	.byte 0x1
 5198 1aba 06          	.byte 0x6
 5199 1abb 97 1A 00 00 	.4byte 0x1a97
 5200 1abf 01          	.byte 0x1
 5201 1ac0 01          	.byte 0x1
 5202 1ac1 22          	.uleb128 0x22
 5203 1ac2 00 00 00 00 	.4byte .LASF20
 5204 1ac6 01          	.byte 0x1
 5205 1ac7 08          	.byte 0x8
 5206 1ac8 F9 00 00 00 	.4byte 0xf9
 5207 1acc 01          	.byte 0x1
 5208 1acd 01          	.byte 0x1
 5209 1ace 22          	.uleb128 0x22
 5210 1acf 00 00 00 00 	.4byte .LASF21
 5211 1ad3 01          	.byte 0x1
 5212 1ad4 09          	.byte 0x9
 5213 1ad5 F9 00 00 00 	.4byte 0xf9
 5214 1ad9 01          	.byte 0x1
 5215 1ada 01          	.byte 0x1
 5216 1adb 22          	.uleb128 0x22
 5217 1adc 00 00 00 00 	.4byte .LASF22
 5218 1ae0 01          	.byte 0x1
 5219 1ae1 0A          	.byte 0xa
 5220 1ae2 F9 00 00 00 	.4byte 0xf9
 5221 1ae6 01          	.byte 0x1
 5222 1ae7 01          	.byte 0x1
 5223 1ae8 22          	.uleb128 0x22
 5224 1ae9 00 00 00 00 	.4byte .LASF23
 5225 1aed 01          	.byte 0x1
 5226 1aee 0B          	.byte 0xb
 5227 1aef F9 00 00 00 	.4byte 0xf9
 5228 1af3 01          	.byte 0x1
 5229 1af4 01          	.byte 0x1
 5230 1af5 22          	.uleb128 0x22
 5231 1af6 00 00 00 00 	.4byte .LASF24
 5232 1afa 01          	.byte 0x1
 5233 1afb 0D          	.byte 0xd
 5234 1afc 7D 15 00 00 	.4byte 0x157d
 5235 1b00 01          	.byte 0x1
 5236 1b01 01          	.byte 0x1
 5237 1b02 22          	.uleb128 0x22
MPLAB XC16 ASSEMBLY Listing:   			page 108


 5238 1b03 00 00 00 00 	.4byte .LASF25
 5239 1b07 01          	.byte 0x1
 5240 1b08 0E          	.byte 0xe
 5241 1b09 7D 15 00 00 	.4byte 0x157d
 5242 1b0d 01          	.byte 0x1
 5243 1b0e 01          	.byte 0x1
 5244 1b0f 1D          	.uleb128 0x1d
 5245 1b10 00 00 00 00 	.4byte .LASF5
 5246 1b14 02          	.byte 0x2
 5247 1b15 A8 06       	.2byte 0x6a8
 5248 1b17 65 19 00 00 	.4byte 0x1965
 5249 1b1b 01          	.byte 0x1
 5250 1b1c 01          	.byte 0x1
 5251 1b1d 1D          	.uleb128 0x1d
 5252 1b1e 00 00 00 00 	.4byte .LASF6
 5253 1b22 02          	.byte 0x2
 5254 1b23 C8 06       	.2byte 0x6c8
 5255 1b25 78 19 00 00 	.4byte 0x1978
 5256 1b29 01          	.byte 0x1
 5257 1b2a 01          	.byte 0x1
 5258 1b2b 1F          	.uleb128 0x1f
 5259 1b2c 55 31 52 58 	.asciz "U1RXREG"
 5259      52 45 47 00 
 5260 1b34 02          	.byte 0x2
 5261 1b35 CD 06       	.2byte 0x6cd
 5262 1b37 8F 19 00 00 	.4byte 0x198f
 5263 1b3b 01          	.byte 0x1
 5264 1b3c 01          	.byte 0x1
 5265 1b3d 1F          	.uleb128 0x1f
 5266 1b3e 55 31 42 52 	.asciz "U1BRG"
 5266      47 00 
 5267 1b44 02          	.byte 0x2
 5268 1b45 CF 06       	.2byte 0x6cf
 5269 1b47 8F 19 00 00 	.4byte 0x198f
 5270 1b4b 01          	.byte 0x1
 5271 1b4c 01          	.byte 0x1
 5272 1b4d 1D          	.uleb128 0x1d
 5273 1b4e 00 00 00 00 	.4byte .LASF7
 5274 1b52 02          	.byte 0x2
 5275 1b53 F0 06       	.2byte 0x6f0
 5276 1b55 B2 19 00 00 	.4byte 0x19b2
 5277 1b59 01          	.byte 0x1
 5278 1b5a 01          	.byte 0x1
 5279 1b5b 1D          	.uleb128 0x1d
 5280 1b5c 00 00 00 00 	.4byte .LASF8
 5281 1b60 02          	.byte 0x2
 5282 1b61 10 07       	.2byte 0x710
 5283 1b63 C5 19 00 00 	.4byte 0x19c5
 5284 1b67 01          	.byte 0x1
 5285 1b68 01          	.byte 0x1
 5286 1b69 1F          	.uleb128 0x1f
 5287 1b6a 55 32 52 58 	.asciz "U2RXREG"
 5287      52 45 47 00 
 5288 1b72 02          	.byte 0x2
 5289 1b73 15 07       	.2byte 0x715
 5290 1b75 8F 19 00 00 	.4byte 0x198f
 5291 1b79 01          	.byte 0x1
MPLAB XC16 ASSEMBLY Listing:   			page 109


 5292 1b7a 01          	.byte 0x1
 5293 1b7b 1F          	.uleb128 0x1f
 5294 1b7c 55 32 42 52 	.asciz "U2BRG"
 5294      47 00 
 5295 1b82 02          	.byte 0x2
 5296 1b83 17 07       	.2byte 0x717
 5297 1b85 8F 19 00 00 	.4byte 0x198f
 5298 1b89 01          	.byte 0x1
 5299 1b8a 01          	.byte 0x1
 5300 1b8b 1D          	.uleb128 0x1d
 5301 1b8c 00 00 00 00 	.4byte .LASF9
 5302 1b90 02          	.byte 0x2
 5303 1b91 1A 25       	.2byte 0x251a
 5304 1b93 FA 19 00 00 	.4byte 0x19fa
 5305 1b97 01          	.byte 0x1
 5306 1b98 01          	.byte 0x1
 5307 1b99 1D          	.uleb128 0x1d
 5308 1b9a 00 00 00 00 	.4byte .LASF10
 5309 1b9e 02          	.byte 0x2
 5310 1b9f 30 25       	.2byte 0x2530
 5311 1ba1 0D 1A 00 00 	.4byte 0x1a0d
 5312 1ba5 01          	.byte 0x1
 5313 1ba6 01          	.byte 0x1
 5314 1ba7 1D          	.uleb128 0x1d
 5315 1ba8 00 00 00 00 	.4byte .LASF11
 5316 1bac 02          	.byte 0x2
 5317 1bad 6D 25       	.2byte 0x256d
 5318 1baf 20 1A 00 00 	.4byte 0x1a20
 5319 1bb3 01          	.byte 0x1
 5320 1bb4 01          	.byte 0x1
 5321 1bb5 1D          	.uleb128 0x1d
 5322 1bb6 00 00 00 00 	.4byte .LASF12
 5323 1bba 02          	.byte 0x2
 5324 1bbb C6 25       	.2byte 0x25c6
 5325 1bbd 33 1A 00 00 	.4byte 0x1a33
 5326 1bc1 01          	.byte 0x1
 5327 1bc2 01          	.byte 0x1
 5328 1bc3 1D          	.uleb128 0x1d
 5329 1bc4 00 00 00 00 	.4byte .LASF13
 5330 1bc8 02          	.byte 0x2
 5331 1bc9 DC 25       	.2byte 0x25dc
 5332 1bcb 46 1A 00 00 	.4byte 0x1a46
 5333 1bcf 01          	.byte 0x1
 5334 1bd0 01          	.byte 0x1
 5335 1bd1 1D          	.uleb128 0x1d
 5336 1bd2 00 00 00 00 	.4byte .LASF14
 5337 1bd6 02          	.byte 0x2
 5338 1bd7 19 26       	.2byte 0x2619
 5339 1bd9 59 1A 00 00 	.4byte 0x1a59
 5340 1bdd 01          	.byte 0x1
 5341 1bde 01          	.byte 0x1
 5342 1bdf 1D          	.uleb128 0x1d
 5343 1be0 00 00 00 00 	.4byte .LASF15
 5344 1be4 02          	.byte 0x2
 5345 1be5 C2 26       	.2byte 0x26c2
 5346 1be7 6C 1A 00 00 	.4byte 0x1a6c
 5347 1beb 01          	.byte 0x1
MPLAB XC16 ASSEMBLY Listing:   			page 110


 5348 1bec 01          	.byte 0x1
 5349 1bed 1D          	.uleb128 0x1d
 5350 1bee 00 00 00 00 	.4byte .LASF16
 5351 1bf2 02          	.byte 0x2
 5352 1bf3 6C 27       	.2byte 0x276c
 5353 1bf5 7F 1A 00 00 	.4byte 0x1a7f
 5354 1bf9 01          	.byte 0x1
 5355 1bfa 01          	.byte 0x1
 5356 1bfb 1D          	.uleb128 0x1d
 5357 1bfc 00 00 00 00 	.4byte .LASF17
 5358 1c00 02          	.byte 0x2
 5359 1c01 94 28       	.2byte 0x2894
 5360 1c03 92 1A 00 00 	.4byte 0x1a92
 5361 1c07 01          	.byte 0x1
 5362 1c08 01          	.byte 0x1
 5363 1c09 23          	.uleb128 0x23
 5364 1c0a 00 00 00 00 	.4byte .LASF18
 5365 1c0e 01          	.byte 0x1
 5366 1c0f 05          	.byte 0x5
 5367 1c10 97 1A 00 00 	.4byte 0x1a97
 5368 1c14 01          	.byte 0x1
 5369 1c15 05          	.byte 0x5
 5370 1c16 03          	.byte 0x3
 5371 1c17 00 00 00 00 	.4byte _UART1_BUFFER
 5372 1c1b 23          	.uleb128 0x23
 5373 1c1c 00 00 00 00 	.4byte .LASF19
 5374 1c20 01          	.byte 0x1
 5375 1c21 06          	.byte 0x6
 5376 1c22 97 1A 00 00 	.4byte 0x1a97
 5377 1c26 01          	.byte 0x1
 5378 1c27 05          	.byte 0x5
 5379 1c28 03          	.byte 0x3
 5380 1c29 00 00 00 00 	.4byte _UART2_BUFFER
 5381 1c2d 23          	.uleb128 0x23
 5382 1c2e 00 00 00 00 	.4byte .LASF20
 5383 1c32 01          	.byte 0x1
 5384 1c33 08          	.byte 0x8
 5385 1c34 F9 00 00 00 	.4byte 0xf9
 5386 1c38 01          	.byte 0x1
 5387 1c39 05          	.byte 0x5
 5388 1c3a 03          	.byte 0x3
 5389 1c3b 00 00 00 00 	.4byte _uart1_read
 5390 1c3f 23          	.uleb128 0x23
 5391 1c40 00 00 00 00 	.4byte .LASF21
 5392 1c44 01          	.byte 0x1
 5393 1c45 09          	.byte 0x9
 5394 1c46 F9 00 00 00 	.4byte 0xf9
 5395 1c4a 01          	.byte 0x1
 5396 1c4b 05          	.byte 0x5
 5397 1c4c 03          	.byte 0x3
 5398 1c4d 00 00 00 00 	.4byte _uart1_write
 5399 1c51 23          	.uleb128 0x23
 5400 1c52 00 00 00 00 	.4byte .LASF22
 5401 1c56 01          	.byte 0x1
 5402 1c57 0A          	.byte 0xa
 5403 1c58 F9 00 00 00 	.4byte 0xf9
 5404 1c5c 01          	.byte 0x1
MPLAB XC16 ASSEMBLY Listing:   			page 111


 5405 1c5d 05          	.byte 0x5
 5406 1c5e 03          	.byte 0x3
 5407 1c5f 00 00 00 00 	.4byte _uart2_read
 5408 1c63 23          	.uleb128 0x23
 5409 1c64 00 00 00 00 	.4byte .LASF23
 5410 1c68 01          	.byte 0x1
 5411 1c69 0B          	.byte 0xb
 5412 1c6a F9 00 00 00 	.4byte 0xf9
 5413 1c6e 01          	.byte 0x1
 5414 1c6f 05          	.byte 0x5
 5415 1c70 03          	.byte 0x3
 5416 1c71 00 00 00 00 	.4byte _uart2_write
 5417 1c75 23          	.uleb128 0x23
 5418 1c76 00 00 00 00 	.4byte .LASF24
 5419 1c7a 01          	.byte 0x1
 5420 1c7b 0D          	.byte 0xd
 5421 1c7c 7D 15 00 00 	.4byte 0x157d
 5422 1c80 01          	.byte 0x1
 5423 1c81 05          	.byte 0x5
 5424 1c82 03          	.byte 0x3
 5425 1c83 00 00 00 00 	.4byte _uart1_errors_status
 5426 1c87 23          	.uleb128 0x23
 5427 1c88 00 00 00 00 	.4byte .LASF25
 5428 1c8c 01          	.byte 0x1
 5429 1c8d 0E          	.byte 0xe
 5430 1c8e 7D 15 00 00 	.4byte 0x157d
 5431 1c92 01          	.byte 0x1
 5432 1c93 05          	.byte 0x5
 5433 1c94 03          	.byte 0x3
 5434 1c95 00 00 00 00 	.4byte _uart2_errors_status
 5435 1c99 00          	.byte 0x0
 5436                 	.section .debug_abbrev,info
 5437 0000 01          	.uleb128 0x1
 5438 0001 11          	.uleb128 0x11
 5439 0002 01          	.byte 0x1
 5440 0003 25          	.uleb128 0x25
 5441 0004 08          	.uleb128 0x8
 5442 0005 13          	.uleb128 0x13
 5443 0006 0B          	.uleb128 0xb
 5444 0007 03          	.uleb128 0x3
 5445 0008 08          	.uleb128 0x8
 5446 0009 1B          	.uleb128 0x1b
 5447 000a 08          	.uleb128 0x8
 5448 000b 11          	.uleb128 0x11
 5449 000c 01          	.uleb128 0x1
 5450 000d 12          	.uleb128 0x12
 5451 000e 01          	.uleb128 0x1
 5452 000f 10          	.uleb128 0x10
 5453 0010 06          	.uleb128 0x6
 5454 0011 00          	.byte 0x0
 5455 0012 00          	.byte 0x0
 5456 0013 02          	.uleb128 0x2
 5457 0014 24          	.uleb128 0x24
 5458 0015 00          	.byte 0x0
 5459 0016 0B          	.uleb128 0xb
 5460 0017 0B          	.uleb128 0xb
 5461 0018 3E          	.uleb128 0x3e
MPLAB XC16 ASSEMBLY Listing:   			page 112


 5462 0019 0B          	.uleb128 0xb
 5463 001a 03          	.uleb128 0x3
 5464 001b 08          	.uleb128 0x8
 5465 001c 00          	.byte 0x0
 5466 001d 00          	.byte 0x0
 5467 001e 03          	.uleb128 0x3
 5468 001f 16          	.uleb128 0x16
 5469 0020 00          	.byte 0x0
 5470 0021 03          	.uleb128 0x3
 5471 0022 08          	.uleb128 0x8
 5472 0023 3A          	.uleb128 0x3a
 5473 0024 0B          	.uleb128 0xb
 5474 0025 3B          	.uleb128 0x3b
 5475 0026 0B          	.uleb128 0xb
 5476 0027 49          	.uleb128 0x49
 5477 0028 13          	.uleb128 0x13
 5478 0029 00          	.byte 0x0
 5479 002a 00          	.byte 0x0
 5480 002b 04          	.uleb128 0x4
 5481 002c 13          	.uleb128 0x13
 5482 002d 01          	.byte 0x1
 5483 002e 0B          	.uleb128 0xb
 5484 002f 0B          	.uleb128 0xb
 5485 0030 3A          	.uleb128 0x3a
 5486 0031 0B          	.uleb128 0xb
 5487 0032 3B          	.uleb128 0x3b
 5488 0033 05          	.uleb128 0x5
 5489 0034 01          	.uleb128 0x1
 5490 0035 13          	.uleb128 0x13
 5491 0036 00          	.byte 0x0
 5492 0037 00          	.byte 0x0
 5493 0038 05          	.uleb128 0x5
 5494 0039 0D          	.uleb128 0xd
 5495 003a 00          	.byte 0x0
 5496 003b 03          	.uleb128 0x3
 5497 003c 08          	.uleb128 0x8
 5498 003d 3A          	.uleb128 0x3a
 5499 003e 0B          	.uleb128 0xb
 5500 003f 3B          	.uleb128 0x3b
 5501 0040 05          	.uleb128 0x5
 5502 0041 49          	.uleb128 0x49
 5503 0042 13          	.uleb128 0x13
 5504 0043 0B          	.uleb128 0xb
 5505 0044 0B          	.uleb128 0xb
 5506 0045 0D          	.uleb128 0xd
 5507 0046 0B          	.uleb128 0xb
 5508 0047 0C          	.uleb128 0xc
 5509 0048 0B          	.uleb128 0xb
 5510 0049 38          	.uleb128 0x38
 5511 004a 0A          	.uleb128 0xa
 5512 004b 00          	.byte 0x0
 5513 004c 00          	.byte 0x0
 5514 004d 06          	.uleb128 0x6
 5515 004e 17          	.uleb128 0x17
 5516 004f 01          	.byte 0x1
 5517 0050 0B          	.uleb128 0xb
 5518 0051 0B          	.uleb128 0xb
MPLAB XC16 ASSEMBLY Listing:   			page 113


 5519 0052 3A          	.uleb128 0x3a
 5520 0053 0B          	.uleb128 0xb
 5521 0054 3B          	.uleb128 0x3b
 5522 0055 05          	.uleb128 0x5
 5523 0056 01          	.uleb128 0x1
 5524 0057 13          	.uleb128 0x13
 5525 0058 00          	.byte 0x0
 5526 0059 00          	.byte 0x0
 5527 005a 07          	.uleb128 0x7
 5528 005b 0D          	.uleb128 0xd
 5529 005c 00          	.byte 0x0
 5530 005d 49          	.uleb128 0x49
 5531 005e 13          	.uleb128 0x13
 5532 005f 00          	.byte 0x0
 5533 0060 00          	.byte 0x0
 5534 0061 08          	.uleb128 0x8
 5535 0062 13          	.uleb128 0x13
 5536 0063 01          	.byte 0x1
 5537 0064 03          	.uleb128 0x3
 5538 0065 08          	.uleb128 0x8
 5539 0066 0B          	.uleb128 0xb
 5540 0067 0B          	.uleb128 0xb
 5541 0068 3A          	.uleb128 0x3a
 5542 0069 0B          	.uleb128 0xb
 5543 006a 3B          	.uleb128 0x3b
 5544 006b 05          	.uleb128 0x5
 5545 006c 01          	.uleb128 0x1
 5546 006d 13          	.uleb128 0x13
 5547 006e 00          	.byte 0x0
 5548 006f 00          	.byte 0x0
 5549 0070 09          	.uleb128 0x9
 5550 0071 0D          	.uleb128 0xd
 5551 0072 00          	.byte 0x0
 5552 0073 49          	.uleb128 0x49
 5553 0074 13          	.uleb128 0x13
 5554 0075 38          	.uleb128 0x38
 5555 0076 0A          	.uleb128 0xa
 5556 0077 00          	.byte 0x0
 5557 0078 00          	.byte 0x0
 5558 0079 0A          	.uleb128 0xa
 5559 007a 16          	.uleb128 0x16
 5560 007b 00          	.byte 0x0
 5561 007c 03          	.uleb128 0x3
 5562 007d 08          	.uleb128 0x8
 5563 007e 3A          	.uleb128 0x3a
 5564 007f 0B          	.uleb128 0xb
 5565 0080 3B          	.uleb128 0x3b
 5566 0081 05          	.uleb128 0x5
 5567 0082 49          	.uleb128 0x49
 5568 0083 13          	.uleb128 0x13
 5569 0084 00          	.byte 0x0
 5570 0085 00          	.byte 0x0
 5571 0086 0B          	.uleb128 0xb
 5572 0087 0D          	.uleb128 0xd
 5573 0088 00          	.byte 0x0
 5574 0089 03          	.uleb128 0x3
 5575 008a 0E          	.uleb128 0xe
MPLAB XC16 ASSEMBLY Listing:   			page 114


 5576 008b 3A          	.uleb128 0x3a
 5577 008c 0B          	.uleb128 0xb
 5578 008d 3B          	.uleb128 0x3b
 5579 008e 05          	.uleb128 0x5
 5580 008f 49          	.uleb128 0x49
 5581 0090 13          	.uleb128 0x13
 5582 0091 0B          	.uleb128 0xb
 5583 0092 0B          	.uleb128 0xb
 5584 0093 0D          	.uleb128 0xd
 5585 0094 0B          	.uleb128 0xb
 5586 0095 0C          	.uleb128 0xc
 5587 0096 0B          	.uleb128 0xb
 5588 0097 38          	.uleb128 0x38
 5589 0098 0A          	.uleb128 0xa
 5590 0099 00          	.byte 0x0
 5591 009a 00          	.byte 0x0
 5592 009b 0C          	.uleb128 0xc
 5593 009c 04          	.uleb128 0x4
 5594 009d 01          	.byte 0x1
 5595 009e 0B          	.uleb128 0xb
 5596 009f 0B          	.uleb128 0xb
 5597 00a0 3A          	.uleb128 0x3a
 5598 00a1 0B          	.uleb128 0xb
 5599 00a2 3B          	.uleb128 0x3b
 5600 00a3 0B          	.uleb128 0xb
 5601 00a4 01          	.uleb128 0x1
 5602 00a5 13          	.uleb128 0x13
 5603 00a6 00          	.byte 0x0
 5604 00a7 00          	.byte 0x0
 5605 00a8 0D          	.uleb128 0xd
 5606 00a9 28          	.uleb128 0x28
 5607 00aa 00          	.byte 0x0
 5608 00ab 03          	.uleb128 0x3
 5609 00ac 08          	.uleb128 0x8
 5610 00ad 1C          	.uleb128 0x1c
 5611 00ae 0D          	.uleb128 0xd
 5612 00af 00          	.byte 0x0
 5613 00b0 00          	.byte 0x0
 5614 00b1 0E          	.uleb128 0xe
 5615 00b2 13          	.uleb128 0x13
 5616 00b3 01          	.byte 0x1
 5617 00b4 0B          	.uleb128 0xb
 5618 00b5 0B          	.uleb128 0xb
 5619 00b6 3A          	.uleb128 0x3a
 5620 00b7 0B          	.uleb128 0xb
 5621 00b8 3B          	.uleb128 0x3b
 5622 00b9 0B          	.uleb128 0xb
 5623 00ba 01          	.uleb128 0x1
 5624 00bb 13          	.uleb128 0x13
 5625 00bc 00          	.byte 0x0
 5626 00bd 00          	.byte 0x0
 5627 00be 0F          	.uleb128 0xf
 5628 00bf 0D          	.uleb128 0xd
 5629 00c0 00          	.byte 0x0
 5630 00c1 03          	.uleb128 0x3
 5631 00c2 08          	.uleb128 0x8
 5632 00c3 3A          	.uleb128 0x3a
MPLAB XC16 ASSEMBLY Listing:   			page 115


 5633 00c4 0B          	.uleb128 0xb
 5634 00c5 3B          	.uleb128 0x3b
 5635 00c6 0B          	.uleb128 0xb
 5636 00c7 49          	.uleb128 0x49
 5637 00c8 13          	.uleb128 0x13
 5638 00c9 38          	.uleb128 0x38
 5639 00ca 0A          	.uleb128 0xa
 5640 00cb 00          	.byte 0x0
 5641 00cc 00          	.byte 0x0
 5642 00cd 10          	.uleb128 0x10
 5643 00ce 0D          	.uleb128 0xd
 5644 00cf 00          	.byte 0x0
 5645 00d0 03          	.uleb128 0x3
 5646 00d1 08          	.uleb128 0x8
 5647 00d2 3A          	.uleb128 0x3a
 5648 00d3 0B          	.uleb128 0xb
 5649 00d4 3B          	.uleb128 0x3b
 5650 00d5 0B          	.uleb128 0xb
 5651 00d6 49          	.uleb128 0x49
 5652 00d7 13          	.uleb128 0x13
 5653 00d8 0B          	.uleb128 0xb
 5654 00d9 0B          	.uleb128 0xb
 5655 00da 0D          	.uleb128 0xd
 5656 00db 0B          	.uleb128 0xb
 5657 00dc 0C          	.uleb128 0xc
 5658 00dd 0B          	.uleb128 0xb
 5659 00de 38          	.uleb128 0x38
 5660 00df 0A          	.uleb128 0xa
 5661 00e0 00          	.byte 0x0
 5662 00e1 00          	.byte 0x0
 5663 00e2 11          	.uleb128 0x11
 5664 00e3 2E          	.uleb128 0x2e
 5665 00e4 01          	.byte 0x1
 5666 00e5 3F          	.uleb128 0x3f
 5667 00e6 0C          	.uleb128 0xc
 5668 00e7 03          	.uleb128 0x3
 5669 00e8 08          	.uleb128 0x8
 5670 00e9 3A          	.uleb128 0x3a
 5671 00ea 0B          	.uleb128 0xb
 5672 00eb 3B          	.uleb128 0x3b
 5673 00ec 0B          	.uleb128 0xb
 5674 00ed 27          	.uleb128 0x27
 5675 00ee 0C          	.uleb128 0xc
 5676 00ef 49          	.uleb128 0x49
 5677 00f0 13          	.uleb128 0x13
 5678 00f1 11          	.uleb128 0x11
 5679 00f2 01          	.uleb128 0x1
 5680 00f3 12          	.uleb128 0x12
 5681 00f4 01          	.uleb128 0x1
 5682 00f5 40          	.uleb128 0x40
 5683 00f6 0A          	.uleb128 0xa
 5684 00f7 01          	.uleb128 0x1
 5685 00f8 13          	.uleb128 0x13
 5686 00f9 00          	.byte 0x0
 5687 00fa 00          	.byte 0x0
 5688 00fb 12          	.uleb128 0x12
 5689 00fc 05          	.uleb128 0x5
MPLAB XC16 ASSEMBLY Listing:   			page 116


 5690 00fd 00          	.byte 0x0
 5691 00fe 03          	.uleb128 0x3
 5692 00ff 0E          	.uleb128 0xe
 5693 0100 3A          	.uleb128 0x3a
 5694 0101 0B          	.uleb128 0xb
 5695 0102 3B          	.uleb128 0x3b
 5696 0103 0B          	.uleb128 0xb
 5697 0104 49          	.uleb128 0x49
 5698 0105 13          	.uleb128 0x13
 5699 0106 02          	.uleb128 0x2
 5700 0107 0A          	.uleb128 0xa
 5701 0108 00          	.byte 0x0
 5702 0109 00          	.byte 0x0
 5703 010a 13          	.uleb128 0x13
 5704 010b 34          	.uleb128 0x34
 5705 010c 00          	.byte 0x0
 5706 010d 03          	.uleb128 0x3
 5707 010e 08          	.uleb128 0x8
 5708 010f 3A          	.uleb128 0x3a
 5709 0110 0B          	.uleb128 0xb
 5710 0111 3B          	.uleb128 0x3b
 5711 0112 0B          	.uleb128 0xb
 5712 0113 49          	.uleb128 0x49
 5713 0114 13          	.uleb128 0x13
 5714 0115 02          	.uleb128 0x2
 5715 0116 0A          	.uleb128 0xa
 5716 0117 00          	.byte 0x0
 5717 0118 00          	.byte 0x0
 5718 0119 14          	.uleb128 0x14
 5719 011a 0F          	.uleb128 0xf
 5720 011b 00          	.byte 0x0
 5721 011c 0B          	.uleb128 0xb
 5722 011d 0B          	.uleb128 0xb
 5723 011e 49          	.uleb128 0x49
 5724 011f 13          	.uleb128 0x13
 5725 0120 00          	.byte 0x0
 5726 0121 00          	.byte 0x0
 5727 0122 15          	.uleb128 0x15
 5728 0123 2E          	.uleb128 0x2e
 5729 0124 00          	.byte 0x0
 5730 0125 3F          	.uleb128 0x3f
 5731 0126 0C          	.uleb128 0xc
 5732 0127 03          	.uleb128 0x3
 5733 0128 08          	.uleb128 0x8
 5734 0129 3A          	.uleb128 0x3a
 5735 012a 0B          	.uleb128 0xb
 5736 012b 3B          	.uleb128 0x3b
 5737 012c 0B          	.uleb128 0xb
 5738 012d 27          	.uleb128 0x27
 5739 012e 0C          	.uleb128 0xc
 5740 012f 11          	.uleb128 0x11
 5741 0130 01          	.uleb128 0x1
 5742 0131 12          	.uleb128 0x12
 5743 0132 01          	.uleb128 0x1
 5744 0133 40          	.uleb128 0x40
 5745 0134 0A          	.uleb128 0xa
 5746 0135 00          	.byte 0x0
MPLAB XC16 ASSEMBLY Listing:   			page 117


 5747 0136 00          	.byte 0x0
 5748 0137 16          	.uleb128 0x16
 5749 0138 2E          	.uleb128 0x2e
 5750 0139 00          	.byte 0x0
 5751 013a 3F          	.uleb128 0x3f
 5752 013b 0C          	.uleb128 0xc
 5753 013c 03          	.uleb128 0x3
 5754 013d 08          	.uleb128 0x8
 5755 013e 3A          	.uleb128 0x3a
 5756 013f 0B          	.uleb128 0xb
 5757 0140 3B          	.uleb128 0x3b
 5758 0141 0B          	.uleb128 0xb
 5759 0142 27          	.uleb128 0x27
 5760 0143 0C          	.uleb128 0xc
 5761 0144 49          	.uleb128 0x49
 5762 0145 13          	.uleb128 0x13
 5763 0146 11          	.uleb128 0x11
 5764 0147 01          	.uleb128 0x1
 5765 0148 12          	.uleb128 0x12
 5766 0149 01          	.uleb128 0x1
 5767 014a 40          	.uleb128 0x40
 5768 014b 0A          	.uleb128 0xa
 5769 014c 00          	.byte 0x0
 5770 014d 00          	.byte 0x0
 5771 014e 17          	.uleb128 0x17
 5772 014f 05          	.uleb128 0x5
 5773 0150 00          	.byte 0x0
 5774 0151 03          	.uleb128 0x3
 5775 0152 08          	.uleb128 0x8
 5776 0153 3A          	.uleb128 0x3a
 5777 0154 0B          	.uleb128 0xb
 5778 0155 3B          	.uleb128 0x3b
 5779 0156 0B          	.uleb128 0xb
 5780 0157 49          	.uleb128 0x49
 5781 0158 13          	.uleb128 0x13
 5782 0159 02          	.uleb128 0x2
 5783 015a 0A          	.uleb128 0xa
 5784 015b 00          	.byte 0x0
 5785 015c 00          	.byte 0x0
 5786 015d 18          	.uleb128 0x18
 5787 015e 2E          	.uleb128 0x2e
 5788 015f 00          	.byte 0x0
 5789 0160 3F          	.uleb128 0x3f
 5790 0161 0C          	.uleb128 0xc
 5791 0162 03          	.uleb128 0x3
 5792 0163 08          	.uleb128 0x8
 5793 0164 3A          	.uleb128 0x3a
 5794 0165 0B          	.uleb128 0xb
 5795 0166 3B          	.uleb128 0x3b
 5796 0167 05          	.uleb128 0x5
 5797 0168 27          	.uleb128 0x27
 5798 0169 0C          	.uleb128 0xc
 5799 016a 11          	.uleb128 0x11
 5800 016b 01          	.uleb128 0x1
 5801 016c 12          	.uleb128 0x12
 5802 016d 01          	.uleb128 0x1
 5803 016e 40          	.uleb128 0x40
MPLAB XC16 ASSEMBLY Listing:   			page 118


 5804 016f 0A          	.uleb128 0xa
 5805 0170 00          	.byte 0x0
 5806 0171 00          	.byte 0x0
 5807 0172 19          	.uleb128 0x19
 5808 0173 2E          	.uleb128 0x2e
 5809 0174 01          	.byte 0x1
 5810 0175 3F          	.uleb128 0x3f
 5811 0176 0C          	.uleb128 0xc
 5812 0177 03          	.uleb128 0x3
 5813 0178 08          	.uleb128 0x8
 5814 0179 3A          	.uleb128 0x3a
 5815 017a 0B          	.uleb128 0xb
 5816 017b 3B          	.uleb128 0x3b
 5817 017c 05          	.uleb128 0x5
 5818 017d 27          	.uleb128 0x27
 5819 017e 0C          	.uleb128 0xc
 5820 017f 11          	.uleb128 0x11
 5821 0180 01          	.uleb128 0x1
 5822 0181 12          	.uleb128 0x12
 5823 0182 01          	.uleb128 0x1
 5824 0183 40          	.uleb128 0x40
 5825 0184 0A          	.uleb128 0xa
 5826 0185 01          	.uleb128 0x1
 5827 0186 13          	.uleb128 0x13
 5828 0187 00          	.byte 0x0
 5829 0188 00          	.byte 0x0
 5830 0189 1A          	.uleb128 0x1a
 5831 018a 34          	.uleb128 0x34
 5832 018b 00          	.byte 0x0
 5833 018c 03          	.uleb128 0x3
 5834 018d 08          	.uleb128 0x8
 5835 018e 3A          	.uleb128 0x3a
 5836 018f 0B          	.uleb128 0xb
 5837 0190 3B          	.uleb128 0x3b
 5838 0191 05          	.uleb128 0x5
 5839 0192 49          	.uleb128 0x49
 5840 0193 13          	.uleb128 0x13
 5841 0194 02          	.uleb128 0x2
 5842 0195 0A          	.uleb128 0xa
 5843 0196 00          	.byte 0x0
 5844 0197 00          	.byte 0x0
 5845 0198 1B          	.uleb128 0x1b
 5846 0199 2E          	.uleb128 0x2e
 5847 019a 00          	.byte 0x0
 5848 019b 3F          	.uleb128 0x3f
 5849 019c 0C          	.uleb128 0xc
 5850 019d 03          	.uleb128 0x3
 5851 019e 08          	.uleb128 0x8
 5852 019f 3A          	.uleb128 0x3a
 5853 01a0 0B          	.uleb128 0xb
 5854 01a1 3B          	.uleb128 0x3b
 5855 01a2 05          	.uleb128 0x5
 5856 01a3 27          	.uleb128 0x27
 5857 01a4 0C          	.uleb128 0xc
 5858 01a5 49          	.uleb128 0x49
 5859 01a6 13          	.uleb128 0x13
 5860 01a7 11          	.uleb128 0x11
MPLAB XC16 ASSEMBLY Listing:   			page 119


 5861 01a8 01          	.uleb128 0x1
 5862 01a9 12          	.uleb128 0x12
 5863 01aa 01          	.uleb128 0x1
 5864 01ab 40          	.uleb128 0x40
 5865 01ac 0A          	.uleb128 0xa
 5866 01ad 00          	.byte 0x0
 5867 01ae 00          	.byte 0x0
 5868 01af 1C          	.uleb128 0x1c
 5869 01b0 2E          	.uleb128 0x2e
 5870 01b1 01          	.byte 0x1
 5871 01b2 3F          	.uleb128 0x3f
 5872 01b3 0C          	.uleb128 0xc
 5873 01b4 03          	.uleb128 0x3
 5874 01b5 08          	.uleb128 0x8
 5875 01b6 3A          	.uleb128 0x3a
 5876 01b7 0B          	.uleb128 0xb
 5877 01b8 3B          	.uleb128 0x3b
 5878 01b9 05          	.uleb128 0x5
 5879 01ba 27          	.uleb128 0x27
 5880 01bb 0C          	.uleb128 0xc
 5881 01bc 49          	.uleb128 0x49
 5882 01bd 13          	.uleb128 0x13
 5883 01be 11          	.uleb128 0x11
 5884 01bf 01          	.uleb128 0x1
 5885 01c0 12          	.uleb128 0x12
 5886 01c1 01          	.uleb128 0x1
 5887 01c2 40          	.uleb128 0x40
 5888 01c3 0A          	.uleb128 0xa
 5889 01c4 01          	.uleb128 0x1
 5890 01c5 13          	.uleb128 0x13
 5891 01c6 00          	.byte 0x0
 5892 01c7 00          	.byte 0x0
 5893 01c8 1D          	.uleb128 0x1d
 5894 01c9 34          	.uleb128 0x34
 5895 01ca 00          	.byte 0x0
 5896 01cb 03          	.uleb128 0x3
 5897 01cc 0E          	.uleb128 0xe
 5898 01cd 3A          	.uleb128 0x3a
 5899 01ce 0B          	.uleb128 0xb
 5900 01cf 3B          	.uleb128 0x3b
 5901 01d0 05          	.uleb128 0x5
 5902 01d1 49          	.uleb128 0x49
 5903 01d2 13          	.uleb128 0x13
 5904 01d3 3F          	.uleb128 0x3f
 5905 01d4 0C          	.uleb128 0xc
 5906 01d5 3C          	.uleb128 0x3c
 5907 01d6 0C          	.uleb128 0xc
 5908 01d7 00          	.byte 0x0
 5909 01d8 00          	.byte 0x0
 5910 01d9 1E          	.uleb128 0x1e
 5911 01da 35          	.uleb128 0x35
 5912 01db 00          	.byte 0x0
 5913 01dc 49          	.uleb128 0x49
 5914 01dd 13          	.uleb128 0x13
 5915 01de 00          	.byte 0x0
 5916 01df 00          	.byte 0x0
 5917 01e0 1F          	.uleb128 0x1f
MPLAB XC16 ASSEMBLY Listing:   			page 120


 5918 01e1 34          	.uleb128 0x34
 5919 01e2 00          	.byte 0x0
 5920 01e3 03          	.uleb128 0x3
 5921 01e4 08          	.uleb128 0x8
 5922 01e5 3A          	.uleb128 0x3a
 5923 01e6 0B          	.uleb128 0xb
 5924 01e7 3B          	.uleb128 0x3b
 5925 01e8 05          	.uleb128 0x5
 5926 01e9 49          	.uleb128 0x49
 5927 01ea 13          	.uleb128 0x13
 5928 01eb 3F          	.uleb128 0x3f
 5929 01ec 0C          	.uleb128 0xc
 5930 01ed 3C          	.uleb128 0x3c
 5931 01ee 0C          	.uleb128 0xc
 5932 01ef 00          	.byte 0x0
 5933 01f0 00          	.byte 0x0
 5934 01f1 20          	.uleb128 0x20
 5935 01f2 01          	.uleb128 0x1
 5936 01f3 01          	.byte 0x1
 5937 01f4 49          	.uleb128 0x49
 5938 01f5 13          	.uleb128 0x13
 5939 01f6 01          	.uleb128 0x1
 5940 01f7 13          	.uleb128 0x13
 5941 01f8 00          	.byte 0x0
 5942 01f9 00          	.byte 0x0
 5943 01fa 21          	.uleb128 0x21
 5944 01fb 21          	.uleb128 0x21
 5945 01fc 00          	.byte 0x0
 5946 01fd 49          	.uleb128 0x49
 5947 01fe 13          	.uleb128 0x13
 5948 01ff 2F          	.uleb128 0x2f
 5949 0200 0B          	.uleb128 0xb
 5950 0201 00          	.byte 0x0
 5951 0202 00          	.byte 0x0
 5952 0203 22          	.uleb128 0x22
 5953 0204 34          	.uleb128 0x34
 5954 0205 00          	.byte 0x0
 5955 0206 03          	.uleb128 0x3
 5956 0207 0E          	.uleb128 0xe
 5957 0208 3A          	.uleb128 0x3a
 5958 0209 0B          	.uleb128 0xb
 5959 020a 3B          	.uleb128 0x3b
 5960 020b 0B          	.uleb128 0xb
 5961 020c 49          	.uleb128 0x49
 5962 020d 13          	.uleb128 0x13
 5963 020e 3F          	.uleb128 0x3f
 5964 020f 0C          	.uleb128 0xc
 5965 0210 3C          	.uleb128 0x3c
 5966 0211 0C          	.uleb128 0xc
 5967 0212 00          	.byte 0x0
 5968 0213 00          	.byte 0x0
 5969 0214 23          	.uleb128 0x23
 5970 0215 34          	.uleb128 0x34
 5971 0216 00          	.byte 0x0
 5972 0217 03          	.uleb128 0x3
 5973 0218 0E          	.uleb128 0xe
 5974 0219 3A          	.uleb128 0x3a
MPLAB XC16 ASSEMBLY Listing:   			page 121


 5975 021a 0B          	.uleb128 0xb
 5976 021b 3B          	.uleb128 0x3b
 5977 021c 0B          	.uleb128 0xb
 5978 021d 49          	.uleb128 0x49
 5979 021e 13          	.uleb128 0x13
 5980 021f 3F          	.uleb128 0x3f
 5981 0220 0C          	.uleb128 0xc
 5982 0221 02          	.uleb128 0x2
 5983 0222 0A          	.uleb128 0xa
 5984 0223 00          	.byte 0x0
 5985 0224 00          	.byte 0x0
 5986 0225 00          	.byte 0x0
 5987                 	.section .debug_pubnames,info
 5988 0000 25 02 00 00 	.4byte 0x225
 5989 0004 02 00       	.2byte 0x2
 5990 0006 00 00 00 00 	.4byte .Ldebug_info0
 5991 000a 9A 1C 00 00 	.4byte 0x1c9a
 5992 000e 90 15 00 00 	.4byte 0x1590
 5993 0012 62 61 75 64 	.asciz "baudrate_generator"
 5993      72 61 74 65 
 5993      5F 67 65 6E 
 5993      65 72 61 74 
 5993      6F 72 00 
 5994 0025 3D 16 00 00 	.4byte 0x163d
 5995 0029 63 6F 6E 66 	.asciz "config_uart1"
 5995      69 67 5F 75 
 5995      61 72 74 31 
 5995      00 
 5996 0036 70 16 00 00 	.4byte 0x1670
 5997 003a 63 6F 6E 66 	.asciz "config_uart2"
 5997      69 67 5F 75 
 5997      61 72 74 32 
 5997      00 
 5998 0047 A3 16 00 00 	.4byte 0x16a3
 5999 004b 65 6E 61 62 	.asciz "enable_uart1"
 5999      6C 65 5F 75 
 5999      61 72 74 31 
 5999      00 
 6000 0058 BF 16 00 00 	.4byte 0x16bf
 6001 005c 65 6E 61 62 	.asciz "enable_uart2"
 6001      6C 65 5F 75 
 6001      61 72 74 32 
 6001      00 
 6002 0069 DB 16 00 00 	.4byte 0x16db
 6003 006d 64 69 73 61 	.asciz "disable_uart1"
 6003      62 6C 65 5F 
 6003      75 61 72 74 
 6003      31 00 
 6004 007b F8 16 00 00 	.4byte 0x16f8
 6005 007f 64 69 73 61 	.asciz "disable_uart2"
 6005      62 6C 65 5F 
 6005      75 61 72 74 
 6005      32 00 
 6006 008d 15 17 00 00 	.4byte 0x1715
 6007 0091 72 65 63 65 	.asciz "receive_uart1_buffer_empty"
 6007      69 76 65 5F 
 6007      75 61 72 74 
MPLAB XC16 ASSEMBLY Listing:   			page 122


 6007      31 5F 62 75 
 6007      66 66 65 72 
 6007      5F 65 6D 70 
 6007      74 79 00 
 6008 00ac 43 17 00 00 	.4byte 0x1743
 6009 00b0 72 65 63 65 	.asciz "receive_uart2_buffer_empty"
 6009      69 76 65 5F 
 6009      75 61 72 74 
 6009      32 5F 62 75 
 6009      66 66 65 72 
 6009      5F 65 6D 70 
 6009      74 79 00 
 6010 00cb 71 17 00 00 	.4byte 0x1771
 6011 00cf 67 65 74 5F 	.asciz "get_FIFO_data"
 6011      46 49 46 4F 
 6011      5F 64 61 74 
 6011      61 00 
 6012 00dd C3 17 00 00 	.4byte 0x17c3
 6013 00e1 5F 55 31 52 	.asciz "_U1RXInterrupt"
 6013      58 49 6E 74 
 6013      65 72 72 75 
 6013      70 74 00 
 6014 00f0 E2 17 00 00 	.4byte 0x17e2
 6015 00f4 5F 55 32 52 	.asciz "_U2RXInterrupt"
 6015      58 49 6E 74 
 6015      65 72 72 75 
 6015      70 74 00 
 6016 0103 01 18 00 00 	.4byte 0x1801
 6017 0107 5F 55 31 45 	.asciz "_U1ErrInterrupt"
 6017      72 72 49 6E 
 6017      74 65 72 72 
 6017      75 70 74 00 
 6018 0117 33 18 00 00 	.4byte 0x1833
 6019 011b 5F 55 32 45 	.asciz "_U2ErrInterrupt"
 6019      72 72 49 6E 
 6019      74 65 72 72 
 6019      75 70 74 00 
 6020 012b 65 18 00 00 	.4byte 0x1865
 6021 012f 67 65 74 5F 	.asciz "get_uart1_errors"
 6021      75 61 72 74 
 6021      31 5F 65 72 
 6021      72 6F 72 73 
 6021      00 
 6022 0140 8A 18 00 00 	.4byte 0x188a
 6023 0144 67 65 74 5F 	.asciz "get_uart2_errors"
 6023      75 61 72 74 
 6023      32 5F 65 72 
 6023      72 6F 72 73 
 6023      00 
 6024 0155 AF 18 00 00 	.4byte 0x18af
 6025 0159 70 6F 70 5F 	.asciz "pop_uart1"
 6025      75 61 72 74 
 6025      31 00 
 6026 0163 E1 18 00 00 	.4byte 0x18e1
 6027 0167 70 6F 70 5F 	.asciz "pop_uart2"
 6027      75 61 72 74 
 6027      32 00 
MPLAB XC16 ASSEMBLY Listing:   			page 123


 6028 0171 13 19 00 00 	.4byte 0x1913
 6029 0175 75 61 72 74 	.asciz "uartRX1_empty"
 6029      52 58 31 5F 
 6029      65 6D 70 74 
 6029      79 00 
 6030 0183 35 19 00 00 	.4byte 0x1935
 6031 0187 75 61 72 74 	.asciz "uartRX2_empty"
 6031      52 58 32 5F 
 6031      65 6D 70 74 
 6031      79 00 
 6032 0195 09 1C 00 00 	.4byte 0x1c09
 6033 0199 55 41 52 54 	.asciz "UART1_BUFFER"
 6033      31 5F 42 55 
 6033      46 46 45 52 
 6033      00 
 6034 01a6 1B 1C 00 00 	.4byte 0x1c1b
 6035 01aa 55 41 52 54 	.asciz "UART2_BUFFER"
 6035      32 5F 42 55 
 6035      46 46 45 52 
 6035      00 
 6036 01b7 2D 1C 00 00 	.4byte 0x1c2d
 6037 01bb 75 61 72 74 	.asciz "uart1_read"
 6037      31 5F 72 65 
 6037      61 64 00 
 6038 01c6 3F 1C 00 00 	.4byte 0x1c3f
 6039 01ca 75 61 72 74 	.asciz "uart1_write"
 6039      31 5F 77 72 
 6039      69 74 65 00 
 6040 01d6 51 1C 00 00 	.4byte 0x1c51
 6041 01da 75 61 72 74 	.asciz "uart2_read"
 6041      32 5F 72 65 
 6041      61 64 00 
 6042 01e5 63 1C 00 00 	.4byte 0x1c63
 6043 01e9 75 61 72 74 	.asciz "uart2_write"
 6043      32 5F 77 72 
 6043      69 74 65 00 
 6044 01f5 75 1C 00 00 	.4byte 0x1c75
 6045 01f9 75 61 72 74 	.asciz "uart1_errors_status"
 6045      31 5F 65 72 
 6045      72 6F 72 73 
 6045      5F 73 74 61 
 6045      74 75 73 00 
 6046 020d 87 1C 00 00 	.4byte 0x1c87
 6047 0211 75 61 72 74 	.asciz "uart2_errors_status"
 6047      32 5F 65 72 
 6047      72 6F 72 73 
 6047      5F 73 74 61 
 6047      74 75 73 00 
 6048 0225 00 00 00 00 	.4byte 0x0
 6049                 	.section .debug_pubtypes,info
 6050 0000 D6 01 00 00 	.4byte 0x1d6
 6051 0004 02 00       	.2byte 0x2
 6052 0006 00 00 00 00 	.4byte .Ldebug_info0
 6053 000a 9A 1C 00 00 	.4byte 0x1c9a
 6054 000e E9 00 00 00 	.4byte 0xe9
 6055 0012 75 69 6E 74 	.asciz "uint16_t"
 6055      31 36 5F 74 
MPLAB XC16 ASSEMBLY Listing:   			page 124


 6055      00 
 6056 001b B2 02 00 00 	.4byte 0x2b2
 6057 001f 74 61 67 55 	.asciz "tagU1MODEBITS"
 6057      31 4D 4F 44 
 6057      45 42 49 54 
 6057      53 00 
 6058 002d D2 02 00 00 	.4byte 0x2d2
 6059 0031 55 31 4D 4F 	.asciz "U1MODEBITS"
 6059      44 45 42 49 
 6059      54 53 00 
 6060 003c 59 04 00 00 	.4byte 0x459
 6061 0040 74 61 67 55 	.asciz "tagU1STABITS"
 6061      31 53 54 41 
 6061      42 49 54 53 
 6061      00 
 6062 004d 78 04 00 00 	.4byte 0x478
 6063 0051 55 31 53 54 	.asciz "U1STABITS"
 6063      41 42 49 54 
 6063      53 00 
 6064 005b 04 06 00 00 	.4byte 0x604
 6065 005f 74 61 67 55 	.asciz "tagU2MODEBITS"
 6065      32 4D 4F 44 
 6065      45 42 49 54 
 6065      53 00 
 6066 006d 24 06 00 00 	.4byte 0x624
 6067 0071 55 32 4D 4F 	.asciz "U2MODEBITS"
 6067      44 45 42 49 
 6067      54 53 00 
 6068 007c AB 07 00 00 	.4byte 0x7ab
 6069 0080 74 61 67 55 	.asciz "tagU2STABITS"
 6069      32 53 54 41 
 6069      42 49 54 53 
 6069      00 
 6070 008d CA 07 00 00 	.4byte 0x7ca
 6071 0091 55 32 53 54 	.asciz "U2STABITS"
 6071      41 42 49 54 
 6071      53 00 
 6072 009b DC 07 00 00 	.4byte 0x7dc
 6073 009f 74 61 67 49 	.asciz "tagIFS0BITS"
 6073      46 53 30 42 
 6073      49 54 53 00 
 6074 00ab 37 09 00 00 	.4byte 0x937
 6075 00af 49 46 53 30 	.asciz "IFS0BITS"
 6075      42 49 54 53 
 6075      00 
 6076 00b8 48 09 00 00 	.4byte 0x948
 6077 00bc 74 61 67 49 	.asciz "tagIFS1BITS"
 6077      46 53 31 42 
 6077      49 54 53 00 
 6078 00c8 A3 0A 00 00 	.4byte 0xaa3
 6079 00cc 49 46 53 31 	.asciz "IFS1BITS"
 6079      42 49 54 53 
 6079      00 
 6080 00d5 B4 0A 00 00 	.4byte 0xab4
 6081 00d9 74 61 67 49 	.asciz "tagIFS4BITS"
 6081      46 53 34 42 
 6081      49 54 53 00 
MPLAB XC16 ASSEMBLY Listing:   			page 125


 6082 00e5 85 0B 00 00 	.4byte 0xb85
 6083 00e9 49 46 53 34 	.asciz "IFS4BITS"
 6083      42 49 54 53 
 6083      00 
 6084 00f2 96 0B 00 00 	.4byte 0xb96
 6085 00f6 74 61 67 49 	.asciz "tagIEC0BITS"
 6085      45 43 30 42 
 6085      49 54 53 00 
 6086 0102 F1 0C 00 00 	.4byte 0xcf1
 6087 0106 49 45 43 30 	.asciz "IEC0BITS"
 6087      42 49 54 53 
 6087      00 
 6088 010f 02 0D 00 00 	.4byte 0xd02
 6089 0113 74 61 67 49 	.asciz "tagIEC1BITS"
 6089      45 43 31 42 
 6089      49 54 53 00 
 6090 011f 5D 0E 00 00 	.4byte 0xe5d
 6091 0123 49 45 43 31 	.asciz "IEC1BITS"
 6091      42 49 54 53 
 6091      00 
 6092 012c 6E 0E 00 00 	.4byte 0xe6e
 6093 0130 74 61 67 49 	.asciz "tagIEC4BITS"
 6093      45 43 34 42 
 6093      49 54 53 00 
 6094 013c 3F 0F 00 00 	.4byte 0xf3f
 6095 0140 49 45 43 34 	.asciz "IEC4BITS"
 6095      42 49 54 53 
 6095      00 
 6096 0149 D0 10 00 00 	.4byte 0x10d0
 6097 014d 74 61 67 49 	.asciz "tagIPC2BITS"
 6097      50 43 32 42 
 6097      49 54 53 00 
 6098 0159 EE 10 00 00 	.4byte 0x10ee
 6099 015d 49 50 43 32 	.asciz "IPC2BITS"
 6099      42 49 54 53 
 6099      00 
 6100 0166 7B 12 00 00 	.4byte 0x127b
 6101 016a 74 61 67 49 	.asciz "tagIPC7BITS"
 6101      50 43 37 42 
 6101      49 54 53 00 
 6102 0176 99 12 00 00 	.4byte 0x1299
 6103 017a 49 50 43 37 	.asciz "IPC7BITS"
 6103      42 49 54 53 
 6103      00 
 6104 0183 CB 13 00 00 	.4byte 0x13cb
 6105 0187 74 61 67 49 	.asciz "tagIPC16BITS"
 6105      50 43 31 36 
 6105      42 49 54 53 
 6105      00 
 6106 0194 EA 13 00 00 	.4byte 0x13ea
 6107 0198 49 50 43 31 	.asciz "IPC16BITS"
 6107      36 42 49 54 
 6107      53 00 
 6108 01a2 48 14 00 00 	.4byte 0x1448
 6109 01a6 44 61 74 61 	.asciz "Data_Parity"
 6109      5F 50 61 72 
 6109      69 74 79 00 
MPLAB XC16 ASSEMBLY Listing:   			page 126


 6110 01b2 27 15 00 00 	.4byte 0x1527
 6111 01b6 55 41 52 54 	.asciz "UART_parameters"
 6111      5F 70 61 72 
 6111      61 6D 65 74 
 6111      65 72 73 00 
 6112 01c6 7D 15 00 00 	.4byte 0x157d
 6113 01ca 55 41 52 54 	.asciz "UART_errors"
 6113      5F 65 72 72 
 6113      6F 72 73 00 
 6114 01d6 00 00 00 00 	.4byte 0x0
 6115                 	.section .debug_aranges,info
 6116 0000 14 00 00 00 	.4byte 0x14
 6117 0004 02 00       	.2byte 0x2
 6118 0006 00 00 00 00 	.4byte .Ldebug_info0
 6119 000a 04          	.byte 0x4
 6120 000b 00          	.byte 0x0
 6121 000c 00 00       	.2byte 0x0
 6122 000e 00 00       	.2byte 0x0
 6123 0010 00 00 00 00 	.4byte 0x0
 6124 0014 00 00 00 00 	.4byte 0x0
 6125                 	.section .debug_str,info
 6126                 	.LASF17:
 6127 0000 49 50 43 31 	.asciz "IPC16bits"
 6127      36 62 69 74 
 6127      73 00 
 6128                 	.LASF19:
 6129 000a 55 41 52 54 	.asciz "UART2_BUFFER"
 6129      32 5F 42 55 
 6129      46 46 45 52 
 6129      00 
 6130                 	.LASF16:
 6131 0017 49 50 43 37 	.asciz "IPC7bits"
 6131      62 69 74 73 
 6131      00 
 6132                 	.LASF14:
 6133 0020 49 45 43 34 	.asciz "IEC4bits"
 6133      62 69 74 73 
 6133      00 
 6134                 	.LASF23:
 6135 0029 75 61 72 74 	.asciz "uart2_write"
 6135      32 5F 77 72 
 6135      69 74 65 00 
 6136                 	.LASF12:
 6137 0035 49 45 43 30 	.asciz "IEC0bits"
 6137      62 69 74 73 
 6137      00 
 6138                 	.LASF7:
 6139 003e 55 32 4D 4F 	.asciz "U2MODEbits"
 6139      44 45 62 69 
 6139      74 73 00 
 6140                 	.LASF9:
 6141 0049 49 46 53 30 	.asciz "IFS0bits"
 6141      62 69 74 73 
 6141      00 
 6142                 	.LASF8:
 6143 0052 55 32 53 54 	.asciz "U2STAbits"
 6143      41 62 69 74 
MPLAB XC16 ASSEMBLY Listing:   			page 127


 6143      73 00 
 6144                 	.LASF4:
 6145 005c 70 61 72 61 	.asciz "parameters"
 6145      6D 65 74 65 
 6145      72 73 00 
 6146                 	.LASF13:
 6147 0067 49 45 43 31 	.asciz "IEC1bits"
 6147      62 69 74 73 
 6147      00 
 6148                 	.LASF2:
 6149 0070 55 52 58 49 	.asciz "URXISEL0"
 6149      53 45 4C 30 
 6149      00 
 6150                 	.LASF3:
 6151 0079 55 52 58 49 	.asciz "URXISEL1"
 6151      53 45 4C 31 
 6151      00 
 6152                 	.LASF20:
 6153 0082 75 61 72 74 	.asciz "uart1_read"
 6153      31 5F 72 65 
 6153      61 64 00 
 6154                 	.LASF0:
 6155 008d 55 54 58 49 	.asciz "UTXISEL0"
 6155      53 45 4C 30 
 6155      00 
 6156                 	.LASF6:
 6157 0096 55 31 53 54 	.asciz "U1STAbits"
 6157      41 62 69 74 
 6157      73 00 
 6158                 	.LASF10:
 6159 00a0 49 46 53 31 	.asciz "IFS1bits"
 6159      62 69 74 73 
 6159      00 
 6160                 	.LASF1:
 6161 00a9 55 54 58 49 	.asciz "UTXISEL1"
 6161      53 45 4C 31 
 6161      00 
 6162                 	.LASF15:
 6163 00b2 49 50 43 32 	.asciz "IPC2bits"
 6163      62 69 74 73 
 6163      00 
 6164                 	.LASF11:
 6165 00bb 49 46 53 34 	.asciz "IFS4bits"
 6165      62 69 74 73 
 6165      00 
 6166                 	.LASF22:
 6167 00c4 75 61 72 74 	.asciz "uart2_read"
 6167      32 5F 72 65 
 6167      61 64 00 
 6168                 	.LASF5:
 6169 00cf 55 31 4D 4F 	.asciz "U1MODEbits"
 6169      44 45 62 69 
 6169      74 73 00 
 6170                 	.LASF21:
 6171 00da 75 61 72 74 	.asciz "uart1_write"
 6171      31 5F 77 72 
 6171      69 74 65 00 
MPLAB XC16 ASSEMBLY Listing:   			page 128


 6172                 	.LASF18:
 6173 00e6 55 41 52 54 	.asciz "UART1_BUFFER"
 6173      31 5F 42 55 
 6173      46 46 45 52 
 6173      00 
 6174                 	.LASF25:
 6175 00f3 75 61 72 74 	.asciz "uart2_errors_status"
 6175      32 5F 65 72 
 6175      72 6F 72 73 
 6175      5F 73 74 61 
 6175      74 75 73 00 
 6176                 	.LASF24:
 6177 0107 75 61 72 74 	.asciz "uart1_errors_status"
 6177      31 5F 65 72 
 6177      72 6F 72 73 
 6177      5F 73 74 61 
 6177      74 75 73 00 
 6178                 	.section .text,code
 6179              	
 6180              	
 6181              	
 6182              	.section __c30_info,info,bss
 6183                 	__psv_trap_errata:
 6184                 	
 6185                 	.section __c30_signature,info,data
 6186 0000 01 00       	.word 0x0001
 6187 0002 00 00       	.word 0x0000
 6188 0004 00 00       	.word 0x0000
 6189                 	
 6190                 	
 6191                 	
 6192                 	.set ___PA___,0
 6193                 	.end
MPLAB XC16 ASSEMBLY Listing:   			page 129


DEFINED SYMBOLS
                            *ABS*:00000000 /home/user/Documents/FST/Programming/project_sub-zero/lib/lib_pic33e/UART.c
                            *ABS*:00000001 __33EP256MU806
                            *ABS*:00000001 __dsPIC33E
                            *ABS*:00000001 __HAS_DSP
                            *ABS*:00000001 __HAS_DMAV2
                            *ABS*:00000001 __HAS_CODEGUARD
                            *ABS*:00000001 __HAS_PMP_ENHANCED
                            *ABS*:00000001 __HAS_EDS
                            *ABS*:00000001 __HAS_EP
                            *ABS*:00000011 __TARGET_DIVIDE_CYCLES
                            *ABS*:00000001 __C30ELF
    {standard input}:14     .bss:00000000 _UART1_BUFFER
    {standard input}:18     .bss:0000008c _UART2_BUFFER
    {standard input}:24     .nbss:00000000 _uart1_read
    {standard input}:30     .nbss:00000002 _uart1_write
    {standard input}:36     .nbss:00000004 _uart2_read
    {standard input}:42     .nbss:00000006 _uart2_write
    {standard input}:48     .bss:00000118 _uart1_errors_status
    {standard input}:52     .bss:0000011c _uart2_errors_status
    {standard input}:57     .text:00000000 _baudrate_generator
    {standard input}:61     *ABS*:00000000 ___PA___
    {standard input}:173    *ABS*:00000000 ___BP___
    {standard input}:262    .text:00000118 _config_uart1
    {standard input}:475    .text:00000240 _config_uart2
    {standard input}:688    .text:00000368 _enable_uart1
    {standard input}:707    .text:00000374 _enable_uart2
    {standard input}:726    .text:00000380 _disable_uart1
    {standard input}:745    .text:0000038c _disable_uart2
    {standard input}:764    .text:00000398 _receive_uart1_buffer_empty
    {standard input}:794    .text:000003b0 _receive_uart2_buffer_empty
    {standard input}:824    .text:000003c8 _get_FIFO_data
                            *ABS*:00000040 __ext_attr_.isr.text
    {standard input}:871    .isr.text:00000000 __U1RXInterrupt
    {standard input}:987    .isr.text:00000078 __U2RXInterrupt
    {standard input}:1103   .isr.text:000000f0 __U1ErrInterrupt
    {standard input}:1229   .isr.text:0000016a __U2ErrInterrupt
    {standard input}:1355   .text:000003f2 _get_uart1_errors
    {standard input}:1386   .text:00000412 _get_uart2_errors
    {standard input}:1417   .text:00000432 _pop_uart1
    {standard input}:1459   .text:00000460 _pop_uart2
    {standard input}:1501   .text:0000048e _uartRX1_empty
    {standard input}:1531   .text:000004a6 _uartRX2_empty
    {standard input}:6183   __c30_info:00000000 __psv_trap_errata
    {standard input}:877    .isr.text:00000000 .L0
    {standard input}:62     .text:00000000 .L0
                            .text:000000ae .L2
                            .text:000000c8 .L3
                            .text:000000c4 .L4
                            .text:0000010a .L5
                            .text:000000de .L6
                            .text:000000f4 .L7
                            .text:0000010c .L8
                            .text:0000012a .L10
                            .text:00000234 .L11
                            .text:00000252 .L13
                            .text:0000035c .L14
MPLAB XC16 ASSEMBLY Listing:   			page 130


                            .text:000003a6 .L20
                            .text:000003a8 .L21
                            .text:000003be .L23
                            .text:000003c0 .L24
                            .text:000003d8 .L27
                            .text:000003e0 .L28
                            .text:000003e8 .L30
                            .text:000003ea .L29
                            .text:0000049c .L50
                            .text:0000049e .L51
                            .text:000004b4 .L53
                            .text:000004b6 .L54
                    .debug_abbrev:00000000 .Ldebug_abbrev0
                            .text:00000000 .Ltext0
                            .text:000004be .Letext0
                      .debug_line:00000000 .Ldebug_line0
                       .debug_str:0000008d .LASF0
                       .debug_str:000000a9 .LASF1
                       .debug_str:00000070 .LASF2
                       .debug_str:00000079 .LASF3
                            .text:00000000 .LFB0
                            .text:00000118 .LFE0
                       .debug_str:0000005c .LASF4
                            .text:00000118 .LFB1
                            .text:00000240 .LFE1
                            .text:00000240 .LFB2
                            .text:00000368 .LFE2
                            .text:00000368 .LFB3
                            .text:00000374 .LFE3
                            .text:00000374 .LFB4
                            .text:00000380 .LFE4
                            .text:00000380 .LFB5
                            .text:0000038c .LFE5
                            .text:0000038c .LFB6
                            .text:00000398 .LFE6
                            .text:00000398 .LFB7
                            .text:000003b0 .LFE7
                            .text:000003b0 .LFB8
                            .text:000003c8 .LFE8
                            .text:000003c8 .LFB9
                            .text:000003f2 .LFE9
                        .isr.text:00000000 .LFB10
                        .isr.text:00000078 .LFE10
                        .isr.text:00000078 .LFB11
                        .isr.text:000000f0 .LFE11
                        .isr.text:000000f0 .LFB12
                        .isr.text:0000016a .LFE12
                        .isr.text:0000016a .LFB13
                        .isr.text:000001e4 .LFE13
                            .text:000003f2 .LFB14
                            .text:00000412 .LFE14
                            .text:00000412 .LFB15
                            .text:00000432 .LFE15
                            .text:00000432 .LFB16
                            .text:00000460 .LFE16
                            .text:00000460 .LFB17
                            .text:0000048e .LFE17
MPLAB XC16 ASSEMBLY Listing:   			page 131


                            .text:0000048e .LFB18
                            .text:000004a6 .LFE18
                            .text:000004a6 .LFB19
                            .text:000004be .LFE19
                       .debug_str:000000cf .LASF5
                       .debug_str:00000096 .LASF6
                       .debug_str:0000003e .LASF7
                       .debug_str:00000052 .LASF8
                       .debug_str:00000049 .LASF9
                       .debug_str:000000a0 .LASF10
                       .debug_str:000000bb .LASF11
                       .debug_str:00000035 .LASF12
                       .debug_str:00000067 .LASF13
                       .debug_str:00000020 .LASF14
                       .debug_str:000000b2 .LASF15
                       .debug_str:00000017 .LASF16
                       .debug_str:00000000 .LASF17
                       .debug_str:000000e6 .LASF18
                       .debug_str:0000000a .LASF19
                       .debug_str:00000082 .LASF20
                       .debug_str:000000da .LASF21
                       .debug_str:000000c4 .LASF22
                       .debug_str:00000029 .LASF23
                       .debug_str:00000107 .LASF24
                       .debug_str:000000f3 .LASF25
                        .isr.text:00000028 .L32
                        .isr.text:00000036 .L33
                        .isr.text:000000a0 .L35
                        .isr.text:000000ae .L36
                        .isr.text:0000014a .L38
                        .isr.text:00000142 .L39
                        .isr.text:0000011e .L40
                        .isr.text:000001c4 .L42
                        .isr.text:000001bc .L43
                        .isr.text:00000198 .L44
                     .debug_frame:00000000 .Lframe0
                      .debug_info:00000000 .Ldebug_info0

UNDEFINED SYMBOLS
___divsf3
___subsf3
___fixunssfsi
___udivsi3
___floatunsisf
___mulsf3
___ltsf2
CORCON
_U1MODEbits
_U1STAbits
_U1BRG
_IFS0bits
_IEC0bits
_IPC2bits
_IFS4bits
_IEC4bits
_IPC16bits
_U2MODEbits
MPLAB XC16 ASSEMBLY Listing:   			page 132


_U2STAbits
_U2BRG
_IFS1bits
_IEC1bits
_IPC7bits
_U1RXREG
_U2RXREG
_RCOUNT
_DSRPAG
_DSWPAG
__const_psvpage
_SR

EQUATE SYMBOLS
/home/user/Documents/FST/Programming/project_sub-zero/lib/lib_pic33e/UART.c = 0x0
                  __33EP256MU806 = 0x1
                      __dsPIC33E = 0x1
                       __HAS_DSP = 0x1
                     __HAS_DMAV2 = 0x1
                 __HAS_CODEGUARD = 0x1
              __HAS_PMP_ENHANCED = 0x1
                       __HAS_EDS = 0x1
                        __HAS_EP = 0x1
          __TARGET_DIVIDE_CYCLES = 0x11
                        __C30ELF = 0x1
                        ___PA___ = 0x0
                        ___BP___ = 0x0
            __ext_attr_.isr.text = 0x40
