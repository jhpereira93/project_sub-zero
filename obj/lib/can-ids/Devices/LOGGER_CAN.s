MPLAB XC16 ASSEMBLY Listing:   			page 1


   1              	.file "/home/user/Documents/FST/Programming/project_sub-zero/lib/can-ids/Devices/LOGGER_CAN.c"
   2              	.section .debug_abbrev,info
   3                 	.Ldebug_abbrev0:
   4                 	.section .debug_info,info
   5                 	.Ldebug_info0:
   6                 	.section .debug_line,info
   7                 	.Ldebug_line0:
   8 0000 BE 00 00 00 	.section .text,code
   8      02 00 A0 00 
   8      00 00 01 01 
   8      FB 0E 0A 00 
   8      01 01 01 01 
   8      00 00 00 01 
   8      6C 69 62 2F 
   8      63 61 6E 2D 
   8      69 64 73 2F 
   9              	.Ltext0:
  10              	.section .text,code
  11              	.align 2
  12              	.global _parse_logger_message_status
  13              	.type _parse_logger_message_status,@function
  14              	_parse_logger_message_status:
  15              	.LFB0:
  16              	.file 1 "lib/can-ids/Devices/LOGGER_CAN.c"
   1:lib/can-ids/Devices/LOGGER_CAN.c **** #include <stdbool.h>
   2:lib/can-ids/Devices/LOGGER_CAN.c **** #include <stdint.h>
   3:lib/can-ids/Devices/LOGGER_CAN.c **** #include "can-ids/CAN_IDs.h"
   4:lib/can-ids/Devices/LOGGER_CAN.c **** #include "LOGGER_CAN.h"
   5:lib/can-ids/Devices/LOGGER_CAN.c **** #include "COMMON_CAN.h"
   6:lib/can-ids/Devices/LOGGER_CAN.c **** 
   7:lib/can-ids/Devices/LOGGER_CAN.c **** #define MSG_ID_LOGGER_STATUS 32
   8:lib/can-ids/Devices/LOGGER_CAN.c **** 
   9:lib/can-ids/Devices/LOGGER_CAN.c **** void parse_logger_message_status(uint16_t data[4], LOGGER_MSG_STATUS *status){
  17              	.loc 1 9 0
  18              	.set ___PA___,1
  19 000000  04 00 FA 	lnk #4
  20              	.LCFI0:
  21              	.loc 1 9 0
  22 000002  00 0F 78 	mov w0,[w14]
  23 000004  11 07 98 	mov w1,[w14+2]
  10:lib/can-ids/Devices/LOGGER_CAN.c **** 
  11:lib/can-ids/Devices/LOGGER_CAN.c ****     status->state = (LOG_STATE)data[0];
  24              	.loc 1 11 0
  25 000006  9E 00 78 	mov [w14],w1
  26 000008  1E 00 90 	mov [w14+2],w0
  27 00000a  91 00 78 	mov [w1],w1
  28 00000c  01 08 78 	mov w1,[w0]
  12:lib/can-ids/Devices/LOGGER_CAN.c **** 
  13:lib/can-ids/Devices/LOGGER_CAN.c ****     return;
  14:lib/can-ids/Devices/LOGGER_CAN.c **** }
  29              	.loc 1 14 0
  30 00000e  8E 07 78 	mov w14,w15
  31 000010  4F 07 78 	mov [--w15],w14
  32 000012  00 40 A9 	bclr CORCON,#2
  33 000014  00 00 06 	return 
  34              	.set ___PA___,0
  35              	.LFE0:
MPLAB XC16 ASSEMBLY Listing:   			page 2


  36              	.size _parse_logger_message_status,.-_parse_logger_message_status
  37              	.align 2
  38              	.global _parse_can_logger
  39              	.type _parse_can_logger,@function
  40              	_parse_can_logger:
  41              	.LFB1:
  15:lib/can-ids/Devices/LOGGER_CAN.c **** void parse_can_logger(CANdata message, LOGGER_CAN_Data *data){
  42              	.loc 1 15 0
  43              	.set ___PA___,1
  44 000016  0E 00 FA 	lnk #14
  45              	.LCFI1:
  46              	.loc 1 15 0
  47 000018  00 0F 78 	mov w0,[w14]
  48 00001a  11 07 98 	mov w1,[w14+2]
  16:lib/can-ids/Devices/LOGGER_CAN.c ****     if (message.dev_id != DEVICE_ID_LOGGER) {
  49              	.loc 1 16 0
  50 00001c  1E 00 78 	mov [w14],w0
  51              	.loc 1 15 0
  52 00001e  22 07 98 	mov w2,[w14+4]
  53 000020  33 07 98 	mov w3,[w14+6]
  54 000022  44 07 98 	mov w4,[w14+8]
  55 000024  55 07 98 	mov w5,[w14+10]
  56 000026  66 07 98 	mov w6,[w14+12]
  57              	.loc 1 16 0
  58 000028  7F 00 60 	and w0,#31,w0
  59 00002a  EB 0F 50 	sub w0,#11,[w15]
  60              	.set ___BP___,0
  61 00002c  00 00 3A 	bra nz,.L6
  62              	.L3:
  17:lib/can-ids/Devices/LOGGER_CAN.c ****         //make_parse_error_msg(DEVICE_ID_LOGGER, message.dev_id, DEVICE_ID_LOGGER);
  18:lib/can-ids/Devices/LOGGER_CAN.c ****         return;
  19:lib/can-ids/Devices/LOGGER_CAN.c ****     }
  20:lib/can-ids/Devices/LOGGER_CAN.c ****     
  21:lib/can-ids/Devices/LOGGER_CAN.c ****     switch (message.msg_id) {
  63              	.loc 1 21 0
  64 00002e  9E 00 78 	mov [w14],w1
  65 000030  00 02 20 	mov #32,w0
  66 000032  C5 08 DE 	lsr w1,#5,w1
  67 000034  F1 43 B2 	and.b #63,w1
  68 000036  81 80 FB 	ze w1,w1
  69 000038  80 8F 50 	sub w1,w0,[w15]
  70              	.set ___BP___,0
  71 00003a  00 00 3A 	bra nz,.L2
  72              	.L5:
  22:lib/can-ids/Devices/LOGGER_CAN.c ****         case MSG_ID_LOGGER_STATUS:
  23:lib/can-ids/Devices/LOGGER_CAN.c ****             parse_logger_message_status(message.data, &(data->status));
  73              	.loc 1 23 0
  74 00003c  EE 00 90 	mov [w14+12],w1
  75 00003e  64 00 47 	add w14,#4,w0
  76 000040  00 00 07 	rcall _parse_logger_message_status
  77 000042  00 00 37 	bra .L2
  78              	.L6:
  79              	.L2:
  24:lib/can-ids/Devices/LOGGER_CAN.c ****             break;    
  25:lib/can-ids/Devices/LOGGER_CAN.c ****     }
  26:lib/can-ids/Devices/LOGGER_CAN.c **** 
  27:lib/can-ids/Devices/LOGGER_CAN.c **** }
MPLAB XC16 ASSEMBLY Listing:   			page 3


  80              	.loc 1 27 0
  81 000044  8E 07 78 	mov w14,w15
  82 000046  4F 07 78 	mov [--w15],w14
  83 000048  00 40 A9 	bclr CORCON,#2
  84 00004a  00 00 06 	return 
  85              	.set ___PA___,0
  86              	.LFE1:
  87              	.size _parse_can_logger,.-_parse_can_logger
  88              	.section .debug_frame,info
  89                 	.Lframe0:
  90 0000 10 00 00 00 	.4byte .LECIE0-.LSCIE0
  91                 	.LSCIE0:
  92 0004 FF FF FF FF 	.4byte 0xffffffff
  93 0008 01          	.byte 0x1
  94 0009 00          	.byte 0
  95 000a 01          	.uleb128 0x1
  96 000b 02          	.sleb128 2
  97 000c 25          	.byte 0x25
  98 000d 12          	.byte 0x12
  99 000e 0F          	.uleb128 0xf
 100 000f 7E          	.sleb128 -2
 101 0010 09          	.byte 0x9
 102 0011 25          	.uleb128 0x25
 103 0012 0F          	.uleb128 0xf
 104 0013 00          	.align 4
 105                 	.LECIE0:
 106                 	.LSFDE0:
 107 0014 18 00 00 00 	.4byte .LEFDE0-.LASFDE0
 108                 	.LASFDE0:
 109 0018 00 00 00 00 	.4byte .Lframe0
 110 001c 00 00 00 00 	.4byte .LFB0
 111 0020 16 00 00 00 	.4byte .LFE0-.LFB0
 112 0024 04          	.byte 0x4
 113 0025 02 00 00 00 	.4byte .LCFI0-.LFB0
 114 0029 13          	.byte 0x13
 115 002a 7D          	.sleb128 -3
 116 002b 0D          	.byte 0xd
 117 002c 0E          	.uleb128 0xe
 118 002d 8E          	.byte 0x8e
 119 002e 02          	.uleb128 0x2
 120 002f 00          	.align 4
 121                 	.LEFDE0:
 122                 	.LSFDE2:
 123 0030 18 00 00 00 	.4byte .LEFDE2-.LASFDE2
 124                 	.LASFDE2:
 125 0034 00 00 00 00 	.4byte .Lframe0
 126 0038 00 00 00 00 	.4byte .LFB1
 127 003c 36 00 00 00 	.4byte .LFE1-.LFB1
 128 0040 04          	.byte 0x4
 129 0041 02 00 00 00 	.4byte .LCFI1-.LFB1
 130 0045 13          	.byte 0x13
 131 0046 7D          	.sleb128 -3
 132 0047 0D          	.byte 0xd
 133 0048 0E          	.uleb128 0xe
 134 0049 8E          	.byte 0x8e
 135 004a 02          	.uleb128 0x2
 136 004b 00          	.align 4
MPLAB XC16 ASSEMBLY Listing:   			page 4


 137                 	.LEFDE2:
 138                 	.section .text,code
 139              	.Letext0:
 140              	.file 2 "lib/can-ids/CAN_IDs.h"
 141              	.file 3 "/opt/microchip/xc16/v1.36/bin/bin/../../include/lega-c/stdint.h"
 142              	.file 4 "lib/can-ids/Devices/LOGGER_CAN.h"
 143              	.section .debug_info,info
 144 0000 27 03 00 00 	.4byte 0x327
 145 0004 02 00       	.2byte 0x2
 146 0006 00 00 00 00 	.4byte .Ldebug_abbrev0
 147 000a 04          	.byte 0x4
 148 000b 01          	.uleb128 0x1
 149 000c 47 4E 55 20 	.asciz "GNU C 4.5.1 (XC16, Microchip v1.36) (A) Build date: Dec  5 2018"
 149      43 20 34 2E 
 149      35 2E 31 20 
 149      28 58 43 31 
 149      36 2C 20 4D 
 149      69 63 72 6F 
 149      63 68 69 70 
 149      20 76 31 2E 
 149      33 36 29 20 
 150 004c 01          	.byte 0x1
 151 004d 6C 69 62 2F 	.asciz "lib/can-ids/Devices/LOGGER_CAN.c"
 151      63 61 6E 2D 
 151      69 64 73 2F 
 151      44 65 76 69 
 151      63 65 73 2F 
 151      4C 4F 47 47 
 151      45 52 5F 43 
 151      41 4E 2E 63 
 151      00 
 152 006e 2F 68 6F 6D 	.asciz "/home/user/Documents/FST/Programming/project_sub-zero"
 152      65 2F 75 73 
 152      65 72 2F 44 
 152      6F 63 75 6D 
 152      65 6E 74 73 
 152      2F 46 53 54 
 152      2F 50 72 6F 
 152      67 72 61 6D 
 152      6D 69 6E 67 
 153 00a4 00 00 00 00 	.4byte .Ltext0
 154 00a8 00 00 00 00 	.4byte .Letext0
 155 00ac 00 00 00 00 	.4byte .Ldebug_line0
 156 00b0 02          	.uleb128 0x2
 157 00b1 01          	.byte 0x1
 158 00b2 06          	.byte 0x6
 159 00b3 73 69 67 6E 	.asciz "signed char"
 159      65 64 20 63 
 159      68 61 72 00 
 160 00bf 02          	.uleb128 0x2
 161 00c0 02          	.byte 0x2
 162 00c1 05          	.byte 0x5
 163 00c2 69 6E 74 00 	.asciz "int"
 164 00c6 02          	.uleb128 0x2
 165 00c7 04          	.byte 0x4
 166 00c8 05          	.byte 0x5
 167 00c9 6C 6F 6E 67 	.asciz "long int"
MPLAB XC16 ASSEMBLY Listing:   			page 5


 167      20 69 6E 74 
 167      00 
 168 00d2 02          	.uleb128 0x2
 169 00d3 08          	.byte 0x8
 170 00d4 05          	.byte 0x5
 171 00d5 6C 6F 6E 67 	.asciz "long long int"
 171      20 6C 6F 6E 
 171      67 20 69 6E 
 171      74 00 
 172 00e3 02          	.uleb128 0x2
 173 00e4 01          	.byte 0x1
 174 00e5 08          	.byte 0x8
 175 00e6 75 6E 73 69 	.asciz "unsigned char"
 175      67 6E 65 64 
 175      20 63 68 61 
 175      72 00 
 176 00f4 03          	.uleb128 0x3
 177 00f5 75 69 6E 74 	.asciz "uint16_t"
 177      31 36 5F 74 
 177      00 
 178 00fe 03          	.byte 0x3
 179 00ff 31          	.byte 0x31
 180 0100 04 01 00 00 	.4byte 0x104
 181 0104 02          	.uleb128 0x2
 182 0105 02          	.byte 0x2
 183 0106 07          	.byte 0x7
 184 0107 75 6E 73 69 	.asciz "unsigned int"
 184      67 6E 65 64 
 184      20 69 6E 74 
 184      00 
 185 0114 02          	.uleb128 0x2
 186 0115 04          	.byte 0x4
 187 0116 07          	.byte 0x7
 188 0117 6C 6F 6E 67 	.asciz "long unsigned int"
 188      20 75 6E 73 
 188      69 67 6E 65 
 188      64 20 69 6E 
 188      74 00 
 189 0129 02          	.uleb128 0x2
 190 012a 08          	.byte 0x8
 191 012b 07          	.byte 0x7
 192 012c 6C 6F 6E 67 	.asciz "long long unsigned int"
 192      20 6C 6F 6E 
 192      67 20 75 6E 
 192      73 69 67 6E 
 192      65 64 20 69 
 192      6E 74 00 
 193 0143 04          	.uleb128 0x4
 194 0144 02          	.byte 0x2
 195 0145 02          	.byte 0x2
 196 0146 12          	.byte 0x12
 197 0147 74 01 00 00 	.4byte 0x174
 198 014b 05          	.uleb128 0x5
 199 014c 64 65 76 5F 	.asciz "dev_id"
 199      69 64 00 
 200 0153 02          	.byte 0x2
 201 0154 13          	.byte 0x13
MPLAB XC16 ASSEMBLY Listing:   			page 6


 202 0155 F4 00 00 00 	.4byte 0xf4
 203 0159 02          	.byte 0x2
 204 015a 05          	.byte 0x5
 205 015b 0B          	.byte 0xb
 206 015c 02          	.byte 0x2
 207 015d 23          	.byte 0x23
 208 015e 00          	.uleb128 0x0
 209 015f 05          	.uleb128 0x5
 210 0160 6D 73 67 5F 	.asciz "msg_id"
 210      69 64 00 
 211 0167 02          	.byte 0x2
 212 0168 16          	.byte 0x16
 213 0169 F4 00 00 00 	.4byte 0xf4
 214 016d 02          	.byte 0x2
 215 016e 06          	.byte 0x6
 216 016f 05          	.byte 0x5
 217 0170 02          	.byte 0x2
 218 0171 23          	.byte 0x23
 219 0172 00          	.uleb128 0x0
 220 0173 00          	.byte 0x0
 221 0174 06          	.uleb128 0x6
 222 0175 02          	.byte 0x2
 223 0176 02          	.byte 0x2
 224 0177 11          	.byte 0x11
 225 0178 8D 01 00 00 	.4byte 0x18d
 226 017c 07          	.uleb128 0x7
 227 017d 43 01 00 00 	.4byte 0x143
 228 0181 08          	.uleb128 0x8
 229 0182 73 69 64 00 	.asciz "sid"
 230 0186 02          	.byte 0x2
 231 0187 18          	.byte 0x18
 232 0188 F4 00 00 00 	.4byte 0xf4
 233 018c 00          	.byte 0x0
 234 018d 04          	.uleb128 0x4
 235 018e 0C          	.byte 0xc
 236 018f 02          	.byte 0x2
 237 0190 10          	.byte 0x10
 238 0191 BE 01 00 00 	.4byte 0x1be
 239 0195 09          	.uleb128 0x9
 240 0196 74 01 00 00 	.4byte 0x174
 241 019a 02          	.byte 0x2
 242 019b 23          	.byte 0x23
 243 019c 00          	.uleb128 0x0
 244 019d 05          	.uleb128 0x5
 245 019e 64 6C 63 00 	.asciz "dlc"
 246 01a2 02          	.byte 0x2
 247 01a3 1A          	.byte 0x1a
 248 01a4 F4 00 00 00 	.4byte 0xf4
 249 01a8 02          	.byte 0x2
 250 01a9 04          	.byte 0x4
 251 01aa 0C          	.byte 0xc
 252 01ab 02          	.byte 0x2
 253 01ac 23          	.byte 0x23
 254 01ad 02          	.uleb128 0x2
 255 01ae 0A          	.uleb128 0xa
 256 01af 64 61 74 61 	.asciz "data"
 256      00 
MPLAB XC16 ASSEMBLY Listing:   			page 7


 257 01b4 02          	.byte 0x2
 258 01b5 1B          	.byte 0x1b
 259 01b6 BE 01 00 00 	.4byte 0x1be
 260 01ba 02          	.byte 0x2
 261 01bb 23          	.byte 0x23
 262 01bc 04          	.uleb128 0x4
 263 01bd 00          	.byte 0x0
 264 01be 0B          	.uleb128 0xb
 265 01bf F4 00 00 00 	.4byte 0xf4
 266 01c3 CE 01 00 00 	.4byte 0x1ce
 267 01c7 0C          	.uleb128 0xc
 268 01c8 04 01 00 00 	.4byte 0x104
 269 01cc 03          	.byte 0x3
 270 01cd 00          	.byte 0x0
 271 01ce 03          	.uleb128 0x3
 272 01cf 43 41 4E 64 	.asciz "CANdata"
 272      61 74 61 00 
 273 01d7 02          	.byte 0x2
 274 01d8 1C          	.byte 0x1c
 275 01d9 8D 01 00 00 	.4byte 0x18d
 276 01dd 0D          	.uleb128 0xd
 277 01de 02          	.byte 0x2
 278 01df 04          	.byte 0x4
 279 01e0 06          	.byte 0x6
 280 01e1 0E 02 00 00 	.4byte 0x20e
 281 01e5 0E          	.uleb128 0xe
 282 01e6 4E 4F 5F 43 	.asciz "NO_CARD"
 282      41 52 44 00 
 283 01ee 00          	.sleb128 0
 284 01ef 0E          	.uleb128 0xe
 285 01f0 57 41 49 54 	.asciz "WAITING_FOR_START"
 285      49 4E 47 5F 
 285      46 4F 52 5F 
 285      53 54 41 52 
 285      54 00 
 286 0202 01          	.sleb128 1
 287 0203 0E          	.uleb128 0xe
 288 0204 57 52 49 54 	.asciz "WRITING"
 288      49 4E 47 00 
 289 020c 02          	.sleb128 2
 290 020d 00          	.byte 0x0
 291 020e 03          	.uleb128 0x3
 292 020f 4C 4F 47 5F 	.asciz "LOG_STATE"
 292      53 54 41 54 
 292      45 00 
 293 0219 04          	.byte 0x4
 294 021a 0A          	.byte 0xa
 295 021b DD 01 00 00 	.4byte 0x1dd
 296 021f 04          	.uleb128 0x4
 297 0220 02          	.byte 0x2
 298 0221 04          	.byte 0x4
 299 0222 0C          	.byte 0xc
 300 0223 38 02 00 00 	.4byte 0x238
 301 0227 0A          	.uleb128 0xa
 302 0228 73 74 61 74 	.asciz "state"
 302      65 00 
 303 022e 04          	.byte 0x4
MPLAB XC16 ASSEMBLY Listing:   			page 8


 304 022f 0D          	.byte 0xd
 305 0230 0E 02 00 00 	.4byte 0x20e
 306 0234 02          	.byte 0x2
 307 0235 23          	.byte 0x23
 308 0236 00          	.uleb128 0x0
 309 0237 00          	.byte 0x0
 310 0238 03          	.uleb128 0x3
 311 0239 4C 4F 47 47 	.asciz "LOGGER_MSG_STATUS"
 311      45 52 5F 4D 
 311      53 47 5F 53 
 311      54 41 54 55 
 311      53 00 
 312 024b 04          	.byte 0x4
 313 024c 0E          	.byte 0xe
 314 024d 1F 02 00 00 	.4byte 0x21f
 315 0251 04          	.uleb128 0x4
 316 0252 02          	.byte 0x2
 317 0253 04          	.byte 0x4
 318 0254 10          	.byte 0x10
 319 0255 6B 02 00 00 	.4byte 0x26b
 320 0259 0A          	.uleb128 0xa
 321 025a 73 74 61 74 	.asciz "status"
 321      75 73 00 
 322 0261 04          	.byte 0x4
 323 0262 11          	.byte 0x11
 324 0263 38 02 00 00 	.4byte 0x238
 325 0267 02          	.byte 0x2
 326 0268 23          	.byte 0x23
 327 0269 00          	.uleb128 0x0
 328 026a 00          	.byte 0x0
 329 026b 03          	.uleb128 0x3
 330 026c 4C 4F 47 47 	.asciz "LOGGER_CAN_Data"
 330      45 52 5F 43 
 330      41 4E 5F 44 
 330      61 74 61 00 
 331 027c 04          	.byte 0x4
 332 027d 12          	.byte 0x12
 333 027e 51 02 00 00 	.4byte 0x251
 334 0282 0F          	.uleb128 0xf
 335 0283 01          	.byte 0x1
 336 0284 70 61 72 73 	.asciz "parse_logger_message_status"
 336      65 5F 6C 6F 
 336      67 67 65 72 
 336      5F 6D 65 73 
 336      73 61 67 65 
 336      5F 73 74 61 
 336      74 75 73 00 
 337 02a0 01          	.byte 0x1
 338 02a1 09          	.byte 0x9
 339 02a2 01          	.byte 0x1
 340 02a3 00 00 00 00 	.4byte .LFB0
 341 02a7 00 00 00 00 	.4byte .LFE0
 342 02ab 01          	.byte 0x1
 343 02ac 5E          	.byte 0x5e
 344 02ad D2 02 00 00 	.4byte 0x2d2
 345 02b1 10          	.uleb128 0x10
 346 02b2 64 61 74 61 	.asciz "data"
MPLAB XC16 ASSEMBLY Listing:   			page 9


 346      00 
 347 02b7 01          	.byte 0x1
 348 02b8 09          	.byte 0x9
 349 02b9 D2 02 00 00 	.4byte 0x2d2
 350 02bd 02          	.byte 0x2
 351 02be 7E          	.byte 0x7e
 352 02bf 00          	.sleb128 0
 353 02c0 10          	.uleb128 0x10
 354 02c1 73 74 61 74 	.asciz "status"
 354      75 73 00 
 355 02c8 01          	.byte 0x1
 356 02c9 09          	.byte 0x9
 357 02ca D8 02 00 00 	.4byte 0x2d8
 358 02ce 02          	.byte 0x2
 359 02cf 7E          	.byte 0x7e
 360 02d0 02          	.sleb128 2
 361 02d1 00          	.byte 0x0
 362 02d2 11          	.uleb128 0x11
 363 02d3 02          	.byte 0x2
 364 02d4 F4 00 00 00 	.4byte 0xf4
 365 02d8 11          	.uleb128 0x11
 366 02d9 02          	.byte 0x2
 367 02da 38 02 00 00 	.4byte 0x238
 368 02de 0F          	.uleb128 0xf
 369 02df 01          	.byte 0x1
 370 02e0 70 61 72 73 	.asciz "parse_can_logger"
 370      65 5F 63 61 
 370      6E 5F 6C 6F 
 370      67 67 65 72 
 370      00 
 371 02f1 01          	.byte 0x1
 372 02f2 0F          	.byte 0xf
 373 02f3 01          	.byte 0x1
 374 02f4 00 00 00 00 	.4byte .LFB1
 375 02f8 00 00 00 00 	.4byte .LFE1
 376 02fc 01          	.byte 0x1
 377 02fd 5E          	.byte 0x5e
 378 02fe 24 03 00 00 	.4byte 0x324
 379 0302 10          	.uleb128 0x10
 380 0303 6D 65 73 73 	.asciz "message"
 380      61 67 65 00 
 381 030b 01          	.byte 0x1
 382 030c 0F          	.byte 0xf
 383 030d CE 01 00 00 	.4byte 0x1ce
 384 0311 02          	.byte 0x2
 385 0312 7E          	.byte 0x7e
 386 0313 00          	.sleb128 0
 387 0314 10          	.uleb128 0x10
 388 0315 64 61 74 61 	.asciz "data"
 388      00 
 389 031a 01          	.byte 0x1
 390 031b 0F          	.byte 0xf
 391 031c 24 03 00 00 	.4byte 0x324
 392 0320 02          	.byte 0x2
 393 0321 7E          	.byte 0x7e
 394 0322 0C          	.sleb128 12
 395 0323 00          	.byte 0x0
MPLAB XC16 ASSEMBLY Listing:   			page 10


 396 0324 11          	.uleb128 0x11
 397 0325 02          	.byte 0x2
 398 0326 6B 02 00 00 	.4byte 0x26b
 399 032a 00          	.byte 0x0
 400                 	.section .debug_abbrev,info
 401 0000 01          	.uleb128 0x1
 402 0001 11          	.uleb128 0x11
 403 0002 01          	.byte 0x1
 404 0003 25          	.uleb128 0x25
 405 0004 08          	.uleb128 0x8
 406 0005 13          	.uleb128 0x13
 407 0006 0B          	.uleb128 0xb
 408 0007 03          	.uleb128 0x3
 409 0008 08          	.uleb128 0x8
 410 0009 1B          	.uleb128 0x1b
 411 000a 08          	.uleb128 0x8
 412 000b 11          	.uleb128 0x11
 413 000c 01          	.uleb128 0x1
 414 000d 12          	.uleb128 0x12
 415 000e 01          	.uleb128 0x1
 416 000f 10          	.uleb128 0x10
 417 0010 06          	.uleb128 0x6
 418 0011 00          	.byte 0x0
 419 0012 00          	.byte 0x0
 420 0013 02          	.uleb128 0x2
 421 0014 24          	.uleb128 0x24
 422 0015 00          	.byte 0x0
 423 0016 0B          	.uleb128 0xb
 424 0017 0B          	.uleb128 0xb
 425 0018 3E          	.uleb128 0x3e
 426 0019 0B          	.uleb128 0xb
 427 001a 03          	.uleb128 0x3
 428 001b 08          	.uleb128 0x8
 429 001c 00          	.byte 0x0
 430 001d 00          	.byte 0x0
 431 001e 03          	.uleb128 0x3
 432 001f 16          	.uleb128 0x16
 433 0020 00          	.byte 0x0
 434 0021 03          	.uleb128 0x3
 435 0022 08          	.uleb128 0x8
 436 0023 3A          	.uleb128 0x3a
 437 0024 0B          	.uleb128 0xb
 438 0025 3B          	.uleb128 0x3b
 439 0026 0B          	.uleb128 0xb
 440 0027 49          	.uleb128 0x49
 441 0028 13          	.uleb128 0x13
 442 0029 00          	.byte 0x0
 443 002a 00          	.byte 0x0
 444 002b 04          	.uleb128 0x4
 445 002c 13          	.uleb128 0x13
 446 002d 01          	.byte 0x1
 447 002e 0B          	.uleb128 0xb
 448 002f 0B          	.uleb128 0xb
 449 0030 3A          	.uleb128 0x3a
 450 0031 0B          	.uleb128 0xb
 451 0032 3B          	.uleb128 0x3b
 452 0033 0B          	.uleb128 0xb
MPLAB XC16 ASSEMBLY Listing:   			page 11


 453 0034 01          	.uleb128 0x1
 454 0035 13          	.uleb128 0x13
 455 0036 00          	.byte 0x0
 456 0037 00          	.byte 0x0
 457 0038 05          	.uleb128 0x5
 458 0039 0D          	.uleb128 0xd
 459 003a 00          	.byte 0x0
 460 003b 03          	.uleb128 0x3
 461 003c 08          	.uleb128 0x8
 462 003d 3A          	.uleb128 0x3a
 463 003e 0B          	.uleb128 0xb
 464 003f 3B          	.uleb128 0x3b
 465 0040 0B          	.uleb128 0xb
 466 0041 49          	.uleb128 0x49
 467 0042 13          	.uleb128 0x13
 468 0043 0B          	.uleb128 0xb
 469 0044 0B          	.uleb128 0xb
 470 0045 0D          	.uleb128 0xd
 471 0046 0B          	.uleb128 0xb
 472 0047 0C          	.uleb128 0xc
 473 0048 0B          	.uleb128 0xb
 474 0049 38          	.uleb128 0x38
 475 004a 0A          	.uleb128 0xa
 476 004b 00          	.byte 0x0
 477 004c 00          	.byte 0x0
 478 004d 06          	.uleb128 0x6
 479 004e 17          	.uleb128 0x17
 480 004f 01          	.byte 0x1
 481 0050 0B          	.uleb128 0xb
 482 0051 0B          	.uleb128 0xb
 483 0052 3A          	.uleb128 0x3a
 484 0053 0B          	.uleb128 0xb
 485 0054 3B          	.uleb128 0x3b
 486 0055 0B          	.uleb128 0xb
 487 0056 01          	.uleb128 0x1
 488 0057 13          	.uleb128 0x13
 489 0058 00          	.byte 0x0
 490 0059 00          	.byte 0x0
 491 005a 07          	.uleb128 0x7
 492 005b 0D          	.uleb128 0xd
 493 005c 00          	.byte 0x0
 494 005d 49          	.uleb128 0x49
 495 005e 13          	.uleb128 0x13
 496 005f 00          	.byte 0x0
 497 0060 00          	.byte 0x0
 498 0061 08          	.uleb128 0x8
 499 0062 0D          	.uleb128 0xd
 500 0063 00          	.byte 0x0
 501 0064 03          	.uleb128 0x3
 502 0065 08          	.uleb128 0x8
 503 0066 3A          	.uleb128 0x3a
 504 0067 0B          	.uleb128 0xb
 505 0068 3B          	.uleb128 0x3b
 506 0069 0B          	.uleb128 0xb
 507 006a 49          	.uleb128 0x49
 508 006b 13          	.uleb128 0x13
 509 006c 00          	.byte 0x0
MPLAB XC16 ASSEMBLY Listing:   			page 12


 510 006d 00          	.byte 0x0
 511 006e 09          	.uleb128 0x9
 512 006f 0D          	.uleb128 0xd
 513 0070 00          	.byte 0x0
 514 0071 49          	.uleb128 0x49
 515 0072 13          	.uleb128 0x13
 516 0073 38          	.uleb128 0x38
 517 0074 0A          	.uleb128 0xa
 518 0075 00          	.byte 0x0
 519 0076 00          	.byte 0x0
 520 0077 0A          	.uleb128 0xa
 521 0078 0D          	.uleb128 0xd
 522 0079 00          	.byte 0x0
 523 007a 03          	.uleb128 0x3
 524 007b 08          	.uleb128 0x8
 525 007c 3A          	.uleb128 0x3a
 526 007d 0B          	.uleb128 0xb
 527 007e 3B          	.uleb128 0x3b
 528 007f 0B          	.uleb128 0xb
 529 0080 49          	.uleb128 0x49
 530 0081 13          	.uleb128 0x13
 531 0082 38          	.uleb128 0x38
 532 0083 0A          	.uleb128 0xa
 533 0084 00          	.byte 0x0
 534 0085 00          	.byte 0x0
 535 0086 0B          	.uleb128 0xb
 536 0087 01          	.uleb128 0x1
 537 0088 01          	.byte 0x1
 538 0089 49          	.uleb128 0x49
 539 008a 13          	.uleb128 0x13
 540 008b 01          	.uleb128 0x1
 541 008c 13          	.uleb128 0x13
 542 008d 00          	.byte 0x0
 543 008e 00          	.byte 0x0
 544 008f 0C          	.uleb128 0xc
 545 0090 21          	.uleb128 0x21
 546 0091 00          	.byte 0x0
 547 0092 49          	.uleb128 0x49
 548 0093 13          	.uleb128 0x13
 549 0094 2F          	.uleb128 0x2f
 550 0095 0B          	.uleb128 0xb
 551 0096 00          	.byte 0x0
 552 0097 00          	.byte 0x0
 553 0098 0D          	.uleb128 0xd
 554 0099 04          	.uleb128 0x4
 555 009a 01          	.byte 0x1
 556 009b 0B          	.uleb128 0xb
 557 009c 0B          	.uleb128 0xb
 558 009d 3A          	.uleb128 0x3a
 559 009e 0B          	.uleb128 0xb
 560 009f 3B          	.uleb128 0x3b
 561 00a0 0B          	.uleb128 0xb
 562 00a1 01          	.uleb128 0x1
 563 00a2 13          	.uleb128 0x13
 564 00a3 00          	.byte 0x0
 565 00a4 00          	.byte 0x0
 566 00a5 0E          	.uleb128 0xe
MPLAB XC16 ASSEMBLY Listing:   			page 13


 567 00a6 28          	.uleb128 0x28
 568 00a7 00          	.byte 0x0
 569 00a8 03          	.uleb128 0x3
 570 00a9 08          	.uleb128 0x8
 571 00aa 1C          	.uleb128 0x1c
 572 00ab 0D          	.uleb128 0xd
 573 00ac 00          	.byte 0x0
 574 00ad 00          	.byte 0x0
 575 00ae 0F          	.uleb128 0xf
 576 00af 2E          	.uleb128 0x2e
 577 00b0 01          	.byte 0x1
 578 00b1 3F          	.uleb128 0x3f
 579 00b2 0C          	.uleb128 0xc
 580 00b3 03          	.uleb128 0x3
 581 00b4 08          	.uleb128 0x8
 582 00b5 3A          	.uleb128 0x3a
 583 00b6 0B          	.uleb128 0xb
 584 00b7 3B          	.uleb128 0x3b
 585 00b8 0B          	.uleb128 0xb
 586 00b9 27          	.uleb128 0x27
 587 00ba 0C          	.uleb128 0xc
 588 00bb 11          	.uleb128 0x11
 589 00bc 01          	.uleb128 0x1
 590 00bd 12          	.uleb128 0x12
 591 00be 01          	.uleb128 0x1
 592 00bf 40          	.uleb128 0x40
 593 00c0 0A          	.uleb128 0xa
 594 00c1 01          	.uleb128 0x1
 595 00c2 13          	.uleb128 0x13
 596 00c3 00          	.byte 0x0
 597 00c4 00          	.byte 0x0
 598 00c5 10          	.uleb128 0x10
 599 00c6 05          	.uleb128 0x5
 600 00c7 00          	.byte 0x0
 601 00c8 03          	.uleb128 0x3
 602 00c9 08          	.uleb128 0x8
 603 00ca 3A          	.uleb128 0x3a
 604 00cb 0B          	.uleb128 0xb
 605 00cc 3B          	.uleb128 0x3b
 606 00cd 0B          	.uleb128 0xb
 607 00ce 49          	.uleb128 0x49
 608 00cf 13          	.uleb128 0x13
 609 00d0 02          	.uleb128 0x2
 610 00d1 0A          	.uleb128 0xa
 611 00d2 00          	.byte 0x0
 612 00d3 00          	.byte 0x0
 613 00d4 11          	.uleb128 0x11
 614 00d5 0F          	.uleb128 0xf
 615 00d6 00          	.byte 0x0
 616 00d7 0B          	.uleb128 0xb
 617 00d8 0B          	.uleb128 0xb
 618 00d9 49          	.uleb128 0x49
 619 00da 13          	.uleb128 0x13
 620 00db 00          	.byte 0x0
 621 00dc 00          	.byte 0x0
 622 00dd 00          	.byte 0x0
 623                 	.section .debug_pubnames,info
MPLAB XC16 ASSEMBLY Listing:   			page 14


 624 0000 43 00 00 00 	.4byte 0x43
 625 0004 02 00       	.2byte 0x2
 626 0006 00 00 00 00 	.4byte .Ldebug_info0
 627 000a 2B 03 00 00 	.4byte 0x32b
 628 000e 82 02 00 00 	.4byte 0x282
 629 0012 70 61 72 73 	.asciz "parse_logger_message_status"
 629      65 5F 6C 6F 
 629      67 67 65 72 
 629      5F 6D 65 73 
 629      73 61 67 65 
 629      5F 73 74 61 
 629      74 75 73 00 
 630 002e DE 02 00 00 	.4byte 0x2de
 631 0032 70 61 72 73 	.asciz "parse_can_logger"
 631      65 5F 63 61 
 631      6E 5F 6C 6F 
 631      67 67 65 72 
 631      00 
 632 0043 00 00 00 00 	.4byte 0x0
 633                 	.section .debug_pubtypes,info
 634 0000 5F 00 00 00 	.4byte 0x5f
 635 0004 02 00       	.2byte 0x2
 636 0006 00 00 00 00 	.4byte .Ldebug_info0
 637 000a 2B 03 00 00 	.4byte 0x32b
 638 000e F4 00 00 00 	.4byte 0xf4
 639 0012 75 69 6E 74 	.asciz "uint16_t"
 639      31 36 5F 74 
 639      00 
 640 001b CE 01 00 00 	.4byte 0x1ce
 641 001f 43 41 4E 64 	.asciz "CANdata"
 641      61 74 61 00 
 642 0027 0E 02 00 00 	.4byte 0x20e
 643 002b 4C 4F 47 5F 	.asciz "LOG_STATE"
 643      53 54 41 54 
 643      45 00 
 644 0035 38 02 00 00 	.4byte 0x238
 645 0039 4C 4F 47 47 	.asciz "LOGGER_MSG_STATUS"
 645      45 52 5F 4D 
 645      53 47 5F 53 
 645      54 41 54 55 
 645      53 00 
 646 004b 6B 02 00 00 	.4byte 0x26b
 647 004f 4C 4F 47 47 	.asciz "LOGGER_CAN_Data"
 647      45 52 5F 43 
 647      41 4E 5F 44 
 647      61 74 61 00 
 648 005f 00 00 00 00 	.4byte 0x0
 649                 	.section .debug_aranges,info
 650 0000 14 00 00 00 	.4byte 0x14
 651 0004 02 00       	.2byte 0x2
 652 0006 00 00 00 00 	.4byte .Ldebug_info0
 653 000a 04          	.byte 0x4
 654 000b 00          	.byte 0x0
 655 000c 00 00       	.2byte 0x0
 656 000e 00 00       	.2byte 0x0
 657 0010 00 00 00 00 	.4byte 0x0
 658 0014 00 00 00 00 	.4byte 0x0
MPLAB XC16 ASSEMBLY Listing:   			page 15


 659                 	.section .debug_str,info
 660                 	.section .text,code
 661              	
 662              	
 663              	
 664              	.section __c30_info,info,bss
 665                 	__psv_trap_errata:
 666                 	
 667                 	.section __c30_signature,info,data
 668 0000 01 00       	.word 0x0001
 669 0002 00 00       	.word 0x0000
 670 0004 00 00       	.word 0x0000
 671                 	
 672                 	
 673                 	
 674                 	.set ___PA___,0
 675                 	.end
MPLAB XC16 ASSEMBLY Listing:   			page 16


DEFINED SYMBOLS
                            *ABS*:00000000 /home/user/Documents/FST/Programming/project_sub-zero/lib/can-ids/Devices/LOGGER_CAN.c
                            *ABS*:00000001 __33EP256MU806
                            *ABS*:00000001 __dsPIC33E
                            *ABS*:00000001 __HAS_DSP
                            *ABS*:00000001 __HAS_DMAV2
                            *ABS*:00000001 __HAS_CODEGUARD
                            *ABS*:00000001 __HAS_PMP_ENHANCED
                            *ABS*:00000001 __HAS_EDS
                            *ABS*:00000001 __HAS_EP
                            *ABS*:00000011 __TARGET_DIVIDE_CYCLES
                            *ABS*:00000001 __C30ELF
    {standard input}:14     .text:00000000 _parse_logger_message_status
    {standard input}:18     *ABS*:00000000 ___PA___
    {standard input}:40     .text:00000016 _parse_can_logger
    {standard input}:60     *ABS*:00000000 ___BP___
    {standard input}:665    __c30_info:00000000 __psv_trap_errata
    {standard input}:19     .text:00000000 .L0
                            .text:00000044 .L6
                            .text:00000044 .L2
                    .debug_abbrev:00000000 .Ldebug_abbrev0
                            .text:00000000 .Ltext0
                            .text:0000004c .Letext0
                      .debug_line:00000000 .Ldebug_line0
                            .text:00000000 .LFB0
                            .text:00000016 .LFE0
                            .text:00000016 .LFB1
                            .text:0000004c .LFE1
                     .debug_frame:00000000 .Lframe0
                      .debug_info:00000000 .Ldebug_info0

UNDEFINED SYMBOLS
CORCON

EQUATE SYMBOLS
/home/user/Documents/FST/Programming/project_sub-zero/lib/can-ids/Devices/LOGGER_CAN.c = 0x0
                  __33EP256MU806 = 0x1
                      __dsPIC33E = 0x1
                       __HAS_DSP = 0x1
                     __HAS_DMAV2 = 0x1
                 __HAS_CODEGUARD = 0x1
              __HAS_PMP_ENHANCED = 0x1
                       __HAS_EDS = 0x1
                        __HAS_EP = 0x1
          __TARGET_DIVIDE_CYCLES = 0x11
                        __C30ELF = 0x1
                        ___PA___ = 0x0
                        ___BP___ = 0x0
