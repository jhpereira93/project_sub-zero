MPLAB XC16 ASSEMBLY Listing:   			page 1


   1              	.file "/home/user/Documents/FST/Programming/project_sub-zero/lib/can-ids/Devices/INTERFACE_CAN.c"
   2              	.section .debug_abbrev,info
   3                 	.Ldebug_abbrev0:
   4                 	.section .debug_info,info
   5                 	.Ldebug_info0:
   6                 	.section .debug_line,info
   7                 	.Ldebug_line0:
   8 0000 43 01 00 00 	.section .text,code
   8      02 00 C1 00 
   8      00 00 01 01 
   8      FB 0E 0A 00 
   8      01 01 01 01 
   8      00 00 00 01 
   8      6C 69 62 2F 
   8      63 61 6E 2D 
   8      69 64 73 2F 
   9              	.Ltext0:
  10              	.section .text,code
  11              	.align 2
  12              	.global _parse_can_message_dcu_toggle
  13              	.type _parse_can_message_dcu_toggle,@function
  14              	_parse_can_message_dcu_toggle:
  15              	.LFB0:
  16              	.file 1 "lib/can-ids/Devices/INTERFACE_CAN.c"
   1:lib/can-ids/Devices/INTERFACE_CAN.c **** #include <stdbool.h>
   2:lib/can-ids/Devices/INTERFACE_CAN.c **** #include <stdint.h>
   3:lib/can-ids/Devices/INTERFACE_CAN.c **** #include "can-ids/CAN_IDs.h"
   4:lib/can-ids/Devices/INTERFACE_CAN.c **** #include "INTERFACE_CAN.h"
   5:lib/can-ids/Devices/INTERFACE_CAN.c **** 
   6:lib/can-ids/Devices/INTERFACE_CAN.c **** #include <stdio.h>
   7:lib/can-ids/Devices/INTERFACE_CAN.c **** 
   8:lib/can-ids/Devices/INTERFACE_CAN.c **** 
   9:lib/can-ids/Devices/INTERFACE_CAN.c **** void parse_can_message_dcu_toggle(uint16_t data[4], INTERFACE_MSG_DCU_TOGGLE *dcu_toggle)
  10:lib/can-ids/Devices/INTERFACE_CAN.c **** {
  17              	.loc 1 10 0
  18              	.set ___PA___,1
  19 000000  04 00 FA 	lnk #4
  20              	.LCFI0:
  21              	.loc 1 10 0
  22 000002  00 0F 78 	mov w0,[w14]
  23 000004  11 07 98 	mov w1,[w14+2]
  11:lib/can-ids/Devices/INTERFACE_CAN.c ****     dcu_toggle->code = (DCU_TOGGLE) (data[0] & 0b1111);
  24              	.loc 1 11 0
  25 000006  9E 00 78 	mov [w14],w1
  26 000008  1E 00 90 	mov [w14+2],w0
  27 00000a  91 00 78 	mov [w1],w1
  28 00000c  EF 80 60 	and w1,#15,w1
  29 00000e  01 08 78 	mov w1,[w0]
  12:lib/can-ids/Devices/INTERFACE_CAN.c **** }
  30              	.loc 1 12 0
  31 000010  8E 07 78 	mov w14,w15
  32 000012  4F 07 78 	mov [--w15],w14
  33 000014  00 40 A9 	bclr CORCON,#2
  34 000016  00 00 06 	return 
  35              	.set ___PA___,0
  36              	.LFE0:
  37              	.size _parse_can_message_dcu_toggle,.-_parse_can_message_dcu_toggle
MPLAB XC16 ASSEMBLY Listing:   			page 2


  38              	.align 2
  39              	.global _parse_can_message_debug_toggle
  40              	.type _parse_can_message_debug_toggle,@function
  41              	_parse_can_message_debug_toggle:
  42              	.LFB1:
  13:lib/can-ids/Devices/INTERFACE_CAN.c **** 
  14:lib/can-ids/Devices/INTERFACE_CAN.c **** void parse_can_message_debug_toggle(uint16_t data0, INTERFACE_MSG_DEBUG_TOGGLE *debug_toggle)
  15:lib/can-ids/Devices/INTERFACE_CAN.c **** {
  43              	.loc 1 15 0
  44              	.set ___PA___,1
  45 000018  04 00 FA 	lnk #4
  46              	.LCFI1:
  47              	.loc 1 15 0
  48 00001a  00 0F 78 	mov w0,[w14]
  49 00001c  11 07 98 	mov w1,[w14+2]
  16:lib/can-ids/Devices/INTERFACE_CAN.c ****     debug_toggle->device = (DEBUG_TOGGLE) (data0 & 0b1111);
  50              	.loc 1 16 0
  51 00001e  1E 01 78 	mov [w14],w2
  52 000020  EF 00 61 	and w2,#15,w1
  53 000022  1E 00 90 	mov [w14+2],w0
  54 000024  01 08 78 	mov w1,[w0]
  17:lib/can-ids/Devices/INTERFACE_CAN.c **** }
  55              	.loc 1 17 0
  56 000026  8E 07 78 	mov w14,w15
  57 000028  4F 07 78 	mov [--w15],w14
  58 00002a  00 40 A9 	bclr CORCON,#2
  59 00002c  00 00 06 	return 
  60              	.set ___PA___,0
  61              	.LFE1:
  62              	.size _parse_can_message_debug_toggle,.-_parse_can_message_debug_toggle
  63              	.align 2
  64              	.global _parse_can_message_pedal_thresholds
  65              	.type _parse_can_message_pedal_thresholds,@function
  66              	_parse_can_message_pedal_thresholds:
  67              	.LFB2:
  18:lib/can-ids/Devices/INTERFACE_CAN.c **** 
  19:lib/can-ids/Devices/INTERFACE_CAN.c **** 
  20:lib/can-ids/Devices/INTERFACE_CAN.c **** void parse_can_message_pedal_thresholds (CANdata *message, INTERFACE_CAN_Data *data){
  68              	.loc 1 20 0
  69              	.set ___PA___,1
  70 00002e  04 00 FA 	lnk #4
  71              	.LCFI2:
  72              	.loc 1 20 0
  73 000030  00 0F 78 	mov w0,[w14]
  74 000032  11 07 98 	mov w1,[w14+2]
  21:lib/can-ids/Devices/INTERFACE_CAN.c **** 
  22:lib/can-ids/Devices/INTERFACE_CAN.c ****     data->pedal_thresholds = (INTERFACE_MSG_PEDAL_THRESHOLDS) message->data[0];
  75              	.loc 1 22 0
  76 000034  9E 00 78 	mov [w14],w1
  77 000036  1E 00 90 	mov [w14+2],w0
  78 000038  A1 00 90 	mov [w1+4],w1
  79 00003a  21 00 98 	mov w1,[w0+4]
  23:lib/can-ids/Devices/INTERFACE_CAN.c **** 
  24:lib/can-ids/Devices/INTERFACE_CAN.c ****     return;
  25:lib/can-ids/Devices/INTERFACE_CAN.c **** }
  80              	.loc 1 25 0
  81 00003c  8E 07 78 	mov w14,w15
MPLAB XC16 ASSEMBLY Listing:   			page 3


  82 00003e  4F 07 78 	mov [--w15],w14
  83 000040  00 40 A9 	bclr CORCON,#2
  84 000042  00 00 06 	return 
  85              	.set ___PA___,0
  86              	.LFE2:
  87              	.size _parse_can_message_pedal_thresholds,.-_parse_can_message_pedal_thresholds
  88              	.align 2
  89              	.global _parse_can_message_interface_inverters
  90              	.type _parse_can_message_interface_inverters,@function
  91              	_parse_can_message_interface_inverters:
  92              	.LFB3:
  26:lib/can-ids/Devices/INTERFACE_CAN.c **** 
  27:lib/can-ids/Devices/INTERFACE_CAN.c **** void parse_can_message_interface_inverters (uint16_t data[4], INTERFACE_MSG_INVERTERS *inverters){
  93              	.loc 1 27 0
  94              	.set ___PA___,1
  95 000044  04 00 FA 	lnk #4
  96              	.LCFI3:
  97              	.loc 1 27 0
  98 000046  00 0F 78 	mov w0,[w14]
  28:lib/can-ids/Devices/INTERFACE_CAN.c ****    
  29:lib/can-ids/Devices/INTERFACE_CAN.c ****     inverters->max_rpm = data[0];
  30:lib/can-ids/Devices/INTERFACE_CAN.c **** 
  31:lib/can-ids/Devices/INTERFACE_CAN.c ****     inverters->max_torque_rear = data[1];
  32:lib/can-ids/Devices/INTERFACE_CAN.c **** 
  33:lib/can-ids/Devices/INTERFACE_CAN.c ****     inverters->max_torque_front = data[2];
  99              	.loc 1 33 0
 100 000048  1E 01 78 	mov [w14],w2
 101 00004a  E4 01 41 	add w2,#4,w3
 102              	.loc 1 29 0
 103 00004c  1E 02 78 	mov [w14],w4
 104              	.loc 1 31 0
 105 00004e  1E 80 E8 	inc2 [w14],w0
  34:lib/can-ids/Devices/INTERFACE_CAN.c **** 
  35:lib/can-ids/Devices/INTERFACE_CAN.c ****     inverters->reset = data[3];
 106              	.loc 1 35 0
 107 000050  9E 02 78 	mov [w14],w5
 108 000052  66 81 42 	add w5,#6,w2
 109              	.loc 1 27 0
 110 000054  11 07 98 	mov w1,[w14+2]
 111              	.loc 1 29 0
 112 000056  94 02 78 	mov [w4],w5
 113 000058  9E 00 90 	mov [w14+2],w1
 114              	.loc 1 31 0
 115 00005a  1E 02 90 	mov [w14+2],w4
 116              	.loc 1 29 0
 117 00005c  85 08 78 	mov w5,[w1]
 118              	.loc 1 33 0
 119 00005e  9E 00 90 	mov [w14+2],w1
 120              	.loc 1 31 0
 121 000060  90 02 78 	mov [w0],w5
 122              	.loc 1 35 0
 123 000062  1E 00 90 	mov [w14+2],w0
 124              	.loc 1 31 0
 125 000064  25 02 98 	mov w5,[w4+4]
 126              	.loc 1 33 0
 127 000066  93 01 78 	mov [w3],w3
 128 000068  93 00 98 	mov w3,[w1+2]
MPLAB XC16 ASSEMBLY Listing:   			page 4


 129              	.loc 1 35 0
 130 00006a  92 00 78 	mov [w2],w1
 131 00006c  31 00 98 	mov w1,[w0+6]
  36:lib/can-ids/Devices/INTERFACE_CAN.c **** }
 132              	.loc 1 36 0
 133 00006e  8E 07 78 	mov w14,w15
 134 000070  4F 07 78 	mov [--w15],w14
 135 000072  00 40 A9 	bclr CORCON,#2
 136 000074  00 00 06 	return 
 137              	.set ___PA___,0
 138              	.LFE3:
 139              	.size _parse_can_message_interface_inverters,.-_parse_can_message_interface_inverters
 140              	.align 2
 141              	.global _parse_can_message_interface_arm1
 142              	.type _parse_can_message_interface_arm1,@function
 143              	_parse_can_message_interface_arm1:
 144              	.LFB4:
  37:lib/can-ids/Devices/INTERFACE_CAN.c **** 
  38:lib/can-ids/Devices/INTERFACE_CAN.c **** void parse_can_message_interface_arm1 (CANdata msg, INTERFACE_MSG_ARM1 *arm1){
 145              	.loc 1 38 0
 146              	.set ___PA___,1
 147 000076  0E 00 FA 	lnk #14
 148              	.LCFI4:
 149              	.loc 1 38 0
 150 000078  00 0F 78 	mov w0,[w14]
 151 00007a  66 07 98 	mov w6,[w14+12]
 152 00007c  22 07 98 	mov w2,[w14+4]
 153 00007e  33 07 98 	mov w3,[w14+6]
 154 000080  44 07 98 	mov w4,[w14+8]
 155 000082  11 07 98 	mov w1,[w14+2]
 156 000084  55 07 98 	mov w5,[w14+10]
  39:lib/can-ids/Devices/INTERFACE_CAN.c ****    
  40:lib/can-ids/Devices/INTERFACE_CAN.c ****     arm1->mode = msg.data[0];
 157              	.loc 1 40 0
 158 000086  2E 02 90 	mov [w14+4],w4
 159 000088  EE 00 90 	mov [w14+12],w1
  41:lib/can-ids/Devices/INTERFACE_CAN.c **** 
  42:lib/can-ids/Devices/INTERFACE_CAN.c ****     arm1->gain = msg.data[1];
 160              	.loc 1 42 0
 161 00008a  3E 00 90 	mov [w14+6],w0
 162 00008c  6E 01 90 	mov [w14+12],w2
 163 00008e  80 01 78 	mov w0,w3
  43:lib/can-ids/Devices/INTERFACE_CAN.c **** 
  44:lib/can-ids/Devices/INTERFACE_CAN.c ****     arm1->tl = (msg.data[2] /100.0);
 164              	.loc 1 44 0
 165 000090  4E 00 90 	mov [w14+8],w0
 166              	.loc 1 40 0
 167 000092  84 08 78 	mov w4,[w1]
 168              	.loc 1 44 0
 169 000094  80 00 EB 	clr w1
 170              	.loc 1 42 0
 171 000096  13 01 98 	mov w3,[w2+2]
 172              	.loc 1 44 0
 173 000098  00 00 07 	rcall ___floatunsisf
 174 00009a  02 00 20 	mov #0,w2
 175 00009c  83 2C 24 	mov #17096,w3
 176 00009e  00 00 07 	rcall ___divsf3
MPLAB XC16 ASSEMBLY Listing:   			page 5


 177 0000a0  6E 01 90 	mov [w14+12],w2
 178 0000a2  20 01 98 	mov w0,[w2+4]
 179 0000a4  31 01 98 	mov w1,[w2+6]
  45:lib/can-ids/Devices/INTERFACE_CAN.c **** 
  46:lib/can-ids/Devices/INTERFACE_CAN.c ****     arm1->power = msg.data[3];
 180              	.loc 1 46 0
 181 0000a6  DE 00 90 	mov [w14+10],w1
 182 0000a8  6E 00 90 	mov [w14+12],w0
 183 0000aa  41 00 98 	mov w1,[w0+8]
  47:lib/can-ids/Devices/INTERFACE_CAN.c **** }
 184              	.loc 1 47 0
 185 0000ac  8E 07 78 	mov w14,w15
 186 0000ae  4F 07 78 	mov [--w15],w14
 187 0000b0  00 40 A9 	bclr CORCON,#2
 188 0000b2  00 00 06 	return 
 189              	.set ___PA___,0
 190              	.LFE4:
 191              	.size _parse_can_message_interface_arm1,.-_parse_can_message_interface_arm1
 192              	.align 2
 193              	.global _parse_can_message_set_steer_thresholds
 194              	.type _parse_can_message_set_steer_thresholds,@function
 195              	_parse_can_message_set_steer_thresholds:
 196              	.LFB5:
  48:lib/can-ids/Devices/INTERFACE_CAN.c **** 
  49:lib/can-ids/Devices/INTERFACE_CAN.c **** void parse_can_message_set_steer_thresholds(CANdata message, INTERFACE_CAN_Data *data){
 197              	.loc 1 49 0
 198              	.set ___PA___,1
 199 0000b4  0E 00 FA 	lnk #14
 200              	.LCFI5:
 201              	.loc 1 49 0
 202 0000b6  00 0F 78 	mov w0,[w14]
 203 0000b8  22 07 98 	mov w2,[w14+4]
 204 0000ba  66 07 98 	mov w6,[w14+12]
  50:lib/can-ids/Devices/INTERFACE_CAN.c **** 
  51:lib/can-ids/Devices/INTERFACE_CAN.c ****     data->steer_thresholds = (INTERFACE_MSG_STEER_THRESHOLDS) message.data[0];
 205              	.loc 1 51 0
 206 0000bc  2E 01 90 	mov [w14+4],w2
 207 0000be  6E 00 90 	mov [w14+12],w0
 208              	.loc 1 49 0
 209 0000c0  11 07 98 	mov w1,[w14+2]
 210 0000c2  33 07 98 	mov w3,[w14+6]
 211 0000c4  44 07 98 	mov w4,[w14+8]
 212 0000c6  55 07 98 	mov w5,[w14+10]
 213              	.loc 1 51 0
 214 0000c8  72 00 98 	mov w2,[w0+14]
  52:lib/can-ids/Devices/INTERFACE_CAN.c **** 
  53:lib/can-ids/Devices/INTERFACE_CAN.c **** }
 215              	.loc 1 53 0
 216 0000ca  8E 07 78 	mov w14,w15
 217 0000cc  4F 07 78 	mov [--w15],w14
 218 0000ce  00 40 A9 	bclr CORCON,#2
 219 0000d0  00 00 06 	return 
 220              	.set ___PA___,0
 221              	.LFE5:
 222              	.size _parse_can_message_set_steer_thresholds,.-_parse_can_message_set_steer_thresholds
 223              	.align 2
 224              	.global _parse_can_message_interface_start_log
MPLAB XC16 ASSEMBLY Listing:   			page 6


 225              	.type _parse_can_message_interface_start_log,@function
 226              	_parse_can_message_interface_start_log:
 227              	.LFB6:
  54:lib/can-ids/Devices/INTERFACE_CAN.c **** 
  55:lib/can-ids/Devices/INTERFACE_CAN.c **** void parse_can_message_interface_start_log(CANdata message, INTERFACE_CAN_Data *data){
 228              	.loc 1 55 0
 229              	.set ___PA___,1
 230 0000d2  0E 00 FA 	lnk #14
 231              	.LCFI6:
 232              	.loc 1 55 0
 233 0000d4  00 0F 78 	mov w0,[w14]
 234 0000d6  66 07 98 	mov w6,[w14+12]
 235 0000d8  22 07 98 	mov w2,[w14+4]
 236 0000da  33 07 98 	mov w3,[w14+6]
 237 0000dc  44 07 98 	mov w4,[w14+8]
  56:lib/can-ids/Devices/INTERFACE_CAN.c **** 
  57:lib/can-ids/Devices/INTERFACE_CAN.c ****     data->start_log.year = message.data[0] & 0x7FF;
  58:lib/can-ids/Devices/INTERFACE_CAN.c ****     data->start_log.day  = message.data[0] >>11;
 238              	.loc 1 58 0
 239 0000de  2E 01 90 	mov [w14+4],w2
 240 0000e0  6E 00 90 	mov [w14+12],w0
 241 0000e2  4B 11 DE 	lsr w2,#11,w2
 242 0000e4  02 41 78 	mov.b w2,w2
 243 0000e6  32 50 98 	mov.b w2,[w0+19]
  59:lib/can-ids/Devices/INTERFACE_CAN.c ****     data->start_log.month = message.data[1] & 0xF;
 244              	.loc 1 59 0
 245 0000e8  3E 01 90 	mov [w14+6],w2
 246 0000ea  6E 00 90 	mov [w14+12],w0
 247 0000ec  02 41 78 	mov.b w2,w2
 248 0000ee  6F 41 61 	and.b w2,#15,w2
 249 0000f0  22 50 98 	mov.b w2,[w0+18]
  60:lib/can-ids/Devices/INTERFACE_CAN.c ****     data->start_log.hour = message.data[1] >> 4;
 250              	.loc 1 60 0
 251 0000f2  3E 01 90 	mov [w14+6],w2
 252 0000f4  6E 00 90 	mov [w14+12],w0
 253 0000f6  44 11 DE 	lsr w2,#4,w2
 254 0000f8  02 41 78 	mov.b w2,w2
 255 0000fa  42 50 98 	mov.b w2,[w0+20]
  61:lib/can-ids/Devices/INTERFACE_CAN.c ****     data->start_log.minute = message.data[2] & 0x3F;
 256              	.loc 1 61 0
 257 0000fc  4E 01 90 	mov [w14+8],w2
 258 0000fe  6E 00 90 	mov [w14+12],w0
 259 000100  02 41 78 	mov.b w2,w2
 260 000102  F2 43 B2 	and.b #63,w2
 261 000104  52 50 98 	mov.b w2,[w0+21]
  62:lib/can-ids/Devices/INTERFACE_CAN.c ****     data->start_log.second = message.data[2] >> 6;
 262              	.loc 1 62 0
 263 000106  4E 01 90 	mov [w14+8],w2
 264 000108  6E 00 90 	mov [w14+12],w0
 265 00010a  46 11 DE 	lsr w2,#6,w2
 266 00010c  02 41 78 	mov.b w2,w2
 267 00010e  62 50 98 	mov.b w2,[w0+22]
 268              	.loc 1 57 0
 269 000110  AE 01 90 	mov [w14+4],w3
 270 000112  F2 7F 20 	mov #2047,w2
 271 000114  6E 00 90 	mov [w14+12],w0
 272 000116  02 81 61 	and w3,w2,w2
MPLAB XC16 ASSEMBLY Listing:   			page 7


 273              	.loc 1 55 0
 274 000118  11 07 98 	mov w1,[w14+2]
 275 00011a  55 07 98 	mov w5,[w14+10]
 276              	.loc 1 57 0
 277 00011c  02 08 98 	mov w2,[w0+16]
  63:lib/can-ids/Devices/INTERFACE_CAN.c **** 
  64:lib/can-ids/Devices/INTERFACE_CAN.c ****     return;
  65:lib/can-ids/Devices/INTERFACE_CAN.c **** }
 278              	.loc 1 65 0
 279 00011e  8E 07 78 	mov w14,w15
 280 000120  4F 07 78 	mov [--w15],w14
 281 000122  00 40 A9 	bclr CORCON,#2
 282 000124  00 00 06 	return 
 283              	.set ___PA___,0
 284              	.LFE6:
 285              	.size _parse_can_message_interface_start_log,.-_parse_can_message_interface_start_log
 286              	.align 2
 287              	.global _parse_can_message_interface_set_drs
 288              	.type _parse_can_message_interface_set_drs,@function
 289              	_parse_can_message_interface_set_drs:
 290              	.LFB7:
  66:lib/can-ids/Devices/INTERFACE_CAN.c **** 
  67:lib/can-ids/Devices/INTERFACE_CAN.c **** void parse_can_message_interface_set_drs(CANdata message, INTERFACE_CAN_Data * data){
 291              	.loc 1 67 0
 292              	.set ___PA___,1
 293 000126  0E 00 FA 	lnk #14
 294              	.LCFI7:
 295              	.loc 1 67 0
 296 000128  00 0F 78 	mov w0,[w14]
 297 00012a  22 07 98 	mov w2,[w14+4]
 298 00012c  66 07 98 	mov w6,[w14+12]
  68:lib/can-ids/Devices/INTERFACE_CAN.c **** 
  69:lib/can-ids/Devices/INTERFACE_CAN.c ****     data->set_drs.drs_sig = message.data[0];
 299              	.loc 1 69 0
 300 00012e  2E 01 90 	mov [w14+4],w2
 301 000130  6E 00 90 	mov [w14+12],w0
 302              	.loc 1 67 0
 303 000132  11 07 98 	mov w1,[w14+2]
 304 000134  33 07 98 	mov w3,[w14+6]
 305 000136  44 07 98 	mov w4,[w14+8]
 306 000138  55 07 98 	mov w5,[w14+10]
 307              	.loc 1 69 0
 308 00013a  42 08 98 	mov w2,[w0+24]
  70:lib/can-ids/Devices/INTERFACE_CAN.c ****     return;
  71:lib/can-ids/Devices/INTERFACE_CAN.c **** }
 309              	.loc 1 71 0
 310 00013c  8E 07 78 	mov w14,w15
 311 00013e  4F 07 78 	mov [--w15],w14
 312 000140  00 40 A9 	bclr CORCON,#2
 313 000142  00 00 06 	return 
 314              	.set ___PA___,0
 315              	.LFE7:
 316              	.size _parse_can_message_interface_set_drs,.-_parse_can_message_interface_set_drs
 317              	.align 2
 318              	.global _parse_can_message_interface_te_limits
 319              	.type _parse_can_message_interface_te_limits,@function
 320              	_parse_can_message_interface_te_limits:
MPLAB XC16 ASSEMBLY Listing:   			page 8


 321              	.LFB8:
  72:lib/can-ids/Devices/INTERFACE_CAN.c **** 
  73:lib/can-ids/Devices/INTERFACE_CAN.c **** void parse_can_message_interface_te_limits(CANdata message, INTERFACE_CAN_Data *data) {
 322              	.loc 1 73 0
 323              	.set ___PA___,1
 324 000144  0E 00 FA 	lnk #14
 325              	.LCFI8:
 326              	.loc 1 73 0
 327 000146  00 0F 78 	mov w0,[w14]
 328 000148  22 07 98 	mov w2,[w14+4]
 329 00014a  66 07 98 	mov w6,[w14+12]
  74:lib/can-ids/Devices/INTERFACE_CAN.c ****     data->limit_request.limit= message.data[0] & 0x7;
 330              	.loc 1 74 0
 331 00014c  2E 00 90 	mov [w14+4],w0
 332 00014e  6E 01 90 	mov [w14+12],w2
 333 000150  67 03 60 	and w0,#7,w6
  75:lib/can-ids/Devices/INTERFACE_CAN.c ****     data->limit_request.sensor= (message.data[0]>>3) & 0x7;
 334              	.loc 1 75 0
 335 000152  AE 03 90 	mov [w14+4],w7
 336 000154  6E 00 90 	mov [w14+12],w0
 337 000156  C3 3B DE 	lsr w7,#3,w7
 338              	.loc 1 73 0
 339 000158  11 07 98 	mov w1,[w14+2]
 340              	.loc 1 75 0
 341 00015a  E7 80 63 	and w7,#7,w1
 342              	.loc 1 73 0
 343 00015c  33 07 98 	mov w3,[w14+6]
 344 00015e  44 07 98 	mov w4,[w14+8]
 345 000160  55 07 98 	mov w5,[w14+10]
 346              	.loc 1 74 0
 347 000162  26 11 98 	mov w6,[w2+36]
 348              	.loc 1 75 0
 349 000164  31 10 98 	mov w1,[w0+38]
  76:lib/can-ids/Devices/INTERFACE_CAN.c ****     
  77:lib/can-ids/Devices/INTERFACE_CAN.c **** }
 350              	.loc 1 77 0
 351 000166  8E 07 78 	mov w14,w15
 352 000168  4F 07 78 	mov [--w15],w14
 353 00016a  00 40 A9 	bclr CORCON,#2
 354 00016c  00 00 06 	return 
 355              	.set ___PA___,0
 356              	.LFE8:
 357              	.size _parse_can_message_interface_te_limits,.-_parse_can_message_interface_te_limits
 358              	.align 2
 359              	.global _parse_can_message_hard_breaking_thresh
 360              	.type _parse_can_message_hard_breaking_thresh,@function
 361              	_parse_can_message_hard_breaking_thresh:
 362              	.LFB9:
  78:lib/can-ids/Devices/INTERFACE_CAN.c **** 
  79:lib/can-ids/Devices/INTERFACE_CAN.c **** void parse_can_message_hard_breaking_thresh(CANdata message, INTERFACE_CAN_Data *data){
 363              	.loc 1 79 0
 364              	.set ___PA___,1
 365 00016e  0E 00 FA 	lnk #14
 366              	.LCFI9:
 367              	.loc 1 79 0
 368 000170  00 0F 78 	mov w0,[w14]
 369 000172  44 07 98 	mov w4,[w14+8]
MPLAB XC16 ASSEMBLY Listing:   			page 9


 370 000174  66 07 98 	mov w6,[w14+12]
  80:lib/can-ids/Devices/INTERFACE_CAN.c ****     
  81:lib/can-ids/Devices/INTERFACE_CAN.c ****     data->hard_breaking.hard_breaking_press_limit = message.data[2];
 371              	.loc 1 81 0
 372 000176  4E 02 90 	mov [w14+8],w4
 373 000178  6E 00 90 	mov [w14+12],w0
 374              	.loc 1 79 0
 375 00017a  11 07 98 	mov w1,[w14+2]
 376 00017c  22 07 98 	mov w2,[w14+4]
 377 00017e  33 07 98 	mov w3,[w14+6]
 378 000180  55 07 98 	mov w5,[w14+10]
 379              	.loc 1 81 0
 380 000182  44 10 98 	mov w4,[w0+40]
  82:lib/can-ids/Devices/INTERFACE_CAN.c **** }
 381              	.loc 1 82 0
 382 000184  8E 07 78 	mov w14,w15
 383 000186  4F 07 78 	mov [--w15],w14
 384 000188  00 40 A9 	bclr CORCON,#2
 385 00018a  00 00 06 	return 
 386              	.set ___PA___,0
 387              	.LFE9:
 388              	.size _parse_can_message_hard_breaking_thresh,.-_parse_can_message_hard_breaking_thresh
 389              	.align 2
 390              	.global _parse_can_interface
 391              	.type _parse_can_interface,@function
 392              	_parse_can_interface:
 393              	.LFB10:
  83:lib/can-ids/Devices/INTERFACE_CAN.c **** 
  84:lib/can-ids/Devices/INTERFACE_CAN.c **** 
  85:lib/can-ids/Devices/INTERFACE_CAN.c **** 
  86:lib/can-ids/Devices/INTERFACE_CAN.c **** 
  87:lib/can-ids/Devices/INTERFACE_CAN.c **** void parse_can_interface(CANdata message, INTERFACE_CAN_Data *data){
 394              	.loc 1 87 0
 395              	.set ___PA___,1
 396 00018c  0E 00 FA 	lnk #14
 397              	.LCFI10:
 398              	.loc 1 87 0
 399 00018e  00 0F 78 	mov w0,[w14]
 400 000190  11 07 98 	mov w1,[w14+2]
  88:lib/can-ids/Devices/INTERFACE_CAN.c **** 
  89:lib/can-ids/Devices/INTERFACE_CAN.c **** 
  90:lib/can-ids/Devices/INTERFACE_CAN.c **** 	if (message.dev_id != DEVICE_ID_INTERFACE) {
 401              	.loc 1 90 0
 402 000192  1E 00 78 	mov [w14],w0
 403              	.loc 1 87 0
 404 000194  22 07 98 	mov w2,[w14+4]
 405 000196  33 07 98 	mov w3,[w14+6]
 406 000198  44 07 98 	mov w4,[w14+8]
 407 00019a  55 07 98 	mov w5,[w14+10]
 408 00019c  66 07 98 	mov w6,[w14+12]
 409              	.loc 1 90 0
 410 00019e  7F 00 60 	and w0,#31,w0
 411 0001a0  FF 0F 50 	sub w0,#31,[w15]
 412              	.set ___BP___,0
 413 0001a2  00 00 3A 	bra nz,.L25
 414              	.L12:
  91:lib/can-ids/Devices/INTERFACE_CAN.c **** 		/*FIXME: send info for error logging*/
MPLAB XC16 ASSEMBLY Listing:   			page 10


  92:lib/can-ids/Devices/INTERFACE_CAN.c ****         return;
  93:lib/can-ids/Devices/INTERFACE_CAN.c **** 	}
  94:lib/can-ids/Devices/INTERFACE_CAN.c **** 
  95:lib/can-ids/Devices/INTERFACE_CAN.c **** 	switch (message.msg_id) {
 415              	.loc 1 95 0
 416 0001a4  1E 00 78 	mov [w14],w0
 417 0001a6  91 02 20 	mov #41,w1
 418 0001a8  45 00 DE 	lsr w0,#5,w0
 419 0001aa  F0 43 B2 	and.b #63,w0
 420 0001ac  00 80 FB 	ze w0,w0
 421 0001ae  81 0F 50 	sub w0,w1,[w15]
 422              	.set ___BP___,0
 423 0001b0  00 00 32 	bra z,.L18
 424 0001b2  91 02 20 	mov #41,w1
 425 0001b4  81 0F 50 	sub w0,w1,[w15]
 426              	.set ___BP___,0
 427 0001b6  00 00 3C 	bra gt,.L23
 428 0001b8  01 02 20 	mov #32,w1
 429 0001ba  81 0F 50 	sub w0,w1,[w15]
 430              	.set ___BP___,0
 431 0001bc  00 00 32 	bra z,.L15
 432 0001be  01 02 20 	mov #32,w1
 433 0001c0  81 0F 50 	sub w0,w1,[w15]
 434              	.set ___BP___,0
 435 0001c2  00 00 3C 	bra gt,.L24
 436 0001c4  FB 0F 50 	sub w0,#27,[w15]
 437              	.set ___BP___,0
 438 0001c6  00 00 32 	bra z,.L14
 439 0001c8  00 00 37 	bra .L11
 440              	.L24:
 441 0001ca  11 02 20 	mov #33,w1
 442 0001cc  81 0F 50 	sub w0,w1,[w15]
 443              	.set ___BP___,0
 444 0001ce  00 00 32 	bra z,.L16
 445 0001d0  21 02 20 	mov #34,w1
 446 0001d2  81 0F 50 	sub w0,w1,[w15]
 447              	.set ___BP___,0
 448 0001d4  00 00 32 	bra z,.L17
 449 0001d6  00 00 37 	bra .L11
 450              	.L23:
 451 0001d8  B1 02 20 	mov #43,w1
 452 0001da  81 0F 50 	sub w0,w1,[w15]
 453              	.set ___BP___,0
 454 0001dc  00 00 32 	bra z,.L20
 455 0001de  B1 02 20 	mov #43,w1
 456 0001e0  81 0F 50 	sub w0,w1,[w15]
 457              	.set ___BP___,0
 458 0001e2  00 00 35 	bra lt,.L19
 459 0001e4  D1 02 20 	mov #45,w1
 460 0001e6  81 0F 50 	sub w0,w1,[w15]
 461              	.set ___BP___,0
 462 0001e8  00 00 32 	bra z,.L21
 463 0001ea  E1 02 20 	mov #46,w1
 464 0001ec  81 0F 50 	sub w0,w1,[w15]
 465              	.set ___BP___,0
 466 0001ee  00 00 32 	bra z,.L22
 467 0001f0  00 00 37 	bra .L11
MPLAB XC16 ASSEMBLY Listing:   			page 11


 468              	.L20:
  96:lib/can-ids/Devices/INTERFACE_CAN.c ****         case CMD_ID_INTERFACE_START_LOG:
  97:lib/can-ids/Devices/INTERFACE_CAN.c ****             parse_can_message_interface_start_log(message, data);
 469              	.loc 1 97 0
 470 0001f2  6E 03 90 	mov [w14+12],w6
 471 0001f4  1E 00 78 	mov [w14],w0
 472 0001f6  9E 00 90 	mov [w14+2],w1
 473 0001f8  2E 01 90 	mov [w14+4],w2
 474 0001fa  BE 01 90 	mov [w14+6],w3
 475 0001fc  4E 02 90 	mov [w14+8],w4
 476 0001fe  DE 02 90 	mov [w14+10],w5
 477 000200  00 00 07 	rcall _parse_can_message_interface_start_log
  98:lib/can-ids/Devices/INTERFACE_CAN.c ****             break;
 478              	.loc 1 98 0
 479 000202  00 00 37 	bra .L11
 480              	.L15:
  99:lib/can-ids/Devices/INTERFACE_CAN.c **** 
 100:lib/can-ids/Devices/INTERFACE_CAN.c ****         case CMD_ID_INTERFACE_DCU_TOGGLE:
 101:lib/can-ids/Devices/INTERFACE_CAN.c **** 			parse_can_message_dcu_toggle(message.data, &(data->dcu_toggle));
 481              	.loc 1 101 0
 482 000204  EE 00 90 	mov [w14+12],w1
 483 000206  64 00 47 	add w14,#4,w0
 484 000208  00 00 07 	rcall _parse_can_message_dcu_toggle
 102:lib/can-ids/Devices/INTERFACE_CAN.c **** 			break;
 485              	.loc 1 102 0
 486 00020a  00 00 37 	bra .L11
 487              	.L16:
 103:lib/can-ids/Devices/INTERFACE_CAN.c ****         case CMD_ID_INTERFACE_DEBUG_TOGGLE:
 104:lib/can-ids/Devices/INTERFACE_CAN.c ****             parse_can_message_debug_toggle(message.data[0], &(data->debug_toggle));
 488              	.loc 1 104 0
 489 00020c  EE 00 90 	mov [w14+12],w1
 490 00020e  2E 00 90 	mov [w14+4],w0
 491 000210  81 80 E8 	inc2 w1,w1
 492 000212  00 00 07 	rcall _parse_can_message_debug_toggle
 105:lib/can-ids/Devices/INTERFACE_CAN.c ****             break;
 493              	.loc 1 105 0
 494 000214  00 00 37 	bra .L11
 495              	.L17:
 106:lib/can-ids/Devices/INTERFACE_CAN.c ****         case CMD_ID_INTERFACE_SET_PEDAL_THRESHOLD:
 107:lib/can-ids/Devices/INTERFACE_CAN.c ****             parse_can_message_pedal_thresholds(&message, data);
 496              	.loc 1 107 0
 497 000216  EE 00 90 	mov [w14+12],w1
 498 000218  0E 00 78 	mov w14,w0
 499 00021a  00 00 07 	rcall _parse_can_message_pedal_thresholds
 108:lib/can-ids/Devices/INTERFACE_CAN.c ****             break;
 500              	.loc 1 108 0
 501 00021c  00 00 37 	bra .L11
 502              	.L18:
 109:lib/can-ids/Devices/INTERFACE_CAN.c ****         case MSG_ID_INTERFACE_INVERTERS:
 110:lib/can-ids/Devices/INTERFACE_CAN.c ****             parse_can_message_interface_inverters(message.data, &(data->inverters_limitation));
 503              	.loc 1 110 0
 504 00021e  EE 00 90 	mov [w14+12],w1
 505 000220  64 00 47 	add w14,#4,w0
 506 000222  E6 80 40 	add w1,#6,w1
 507 000224  00 00 07 	rcall _parse_can_message_interface_inverters
 111:lib/can-ids/Devices/INTERFACE_CAN.c ****             break;
 508              	.loc 1 111 0
MPLAB XC16 ASSEMBLY Listing:   			page 12


 509 000226  00 00 37 	bra .L11
 510              	.L19:
 112:lib/can-ids/Devices/INTERFACE_CAN.c ****         case CMD_ID_INTERFACE_SET_STEER_THRESHOLD:
 113:lib/can-ids/Devices/INTERFACE_CAN.c ****             parse_can_message_set_steer_thresholds(message, data);
 511              	.loc 1 113 0
 512 000228  6E 03 90 	mov [w14+12],w6
 513 00022a  1E 00 78 	mov [w14],w0
 514 00022c  9E 00 90 	mov [w14+2],w1
 515 00022e  2E 01 90 	mov [w14+4],w2
 516 000230  BE 01 90 	mov [w14+6],w3
 517 000232  4E 02 90 	mov [w14+8],w4
 518 000234  DE 02 90 	mov [w14+10],w5
 519 000236  00 00 07 	rcall _parse_can_message_set_steer_thresholds
 114:lib/can-ids/Devices/INTERFACE_CAN.c ****             break;
 520              	.loc 1 114 0
 521 000238  00 00 37 	bra .L11
 522              	.L21:
 115:lib/can-ids/Devices/INTERFACE_CAN.c ****         case MSG_ID_INTERFACE_ARM1:
 116:lib/can-ids/Devices/INTERFACE_CAN.c ****             parse_can_message_interface_arm1(message, &(data->arm1));
 523              	.loc 1 116 0
 524 00023a  6E 00 90 	mov [w14+12],w0
 525 00023c  9E 00 90 	mov [w14+2],w1
 526 00023e  7A 01 40 	add w0,#26,w2
 527 000240  1E 00 78 	mov [w14],w0
 528 000242  02 03 78 	mov w2,w6
 529 000244  2E 01 90 	mov [w14+4],w2
 530 000246  BE 01 90 	mov [w14+6],w3
 531 000248  4E 02 90 	mov [w14+8],w4
 532 00024a  DE 02 90 	mov [w14+10],w5
 533 00024c  00 00 07 	rcall _parse_can_message_interface_arm1
 117:lib/can-ids/Devices/INTERFACE_CAN.c ****             break;
 534              	.loc 1 117 0
 535 00024e  00 00 37 	bra .L11
 536              	.L22:
 118:lib/can-ids/Devices/INTERFACE_CAN.c ****         case MSG_ID_INTERFACE_TE_LIMITS_REQUEST:
 119:lib/can-ids/Devices/INTERFACE_CAN.c ****             parse_can_message_interface_te_limits(message, data);
 537              	.loc 1 119 0
 538 000250  00 00 00 	nop 
 539 000252  6E 03 90 	mov [w14+12],w6
 540 000254  00 00 00 	nop 
 541 000256  1E 00 78 	mov [w14],w0
 542 000258  00 00 00 	nop 
 543 00025a  9E 00 90 	mov [w14+2],w1
 544 00025c  00 00 00 	nop 
 545 00025e  2E 01 90 	mov [w14+4],w2
 546 000260  00 00 00 	nop 
 547 000262  BE 01 90 	mov [w14+6],w3
 548 000264  00 00 00 	nop 
 549 000266  4E 02 90 	mov [w14+8],w4
 550 000268  00 00 00 	nop 
 551 00026a  DE 02 90 	mov [w14+10],w5
 552 00026c  00 00 07 	rcall _parse_can_message_interface_te_limits
 120:lib/can-ids/Devices/INTERFACE_CAN.c ****             break;
 553              	.loc 1 120 0
 554 00026e  00 00 37 	bra .L11
 555              	.L14:
 121:lib/can-ids/Devices/INTERFACE_CAN.c ****         case CMD_ID_COMMON_SET:
MPLAB XC16 ASSEMBLY Listing:   			page 13


 122:lib/can-ids/Devices/INTERFACE_CAN.c ****             parse_can_message_hard_breaking_thresh(message, data); // this needs to change
 556              	.loc 1 122 0
 557 000270  00 00 00 	nop 
 558 000272  6E 03 90 	mov [w14+12],w6
 559 000274  00 00 00 	nop 
 560 000276  1E 00 78 	mov [w14],w0
 561 000278  00 00 00 	nop 
 562 00027a  9E 00 90 	mov [w14+2],w1
 563 00027c  00 00 00 	nop 
 564 00027e  2E 01 90 	mov [w14+4],w2
 565 000280  00 00 00 	nop 
 566 000282  BE 01 90 	mov [w14+6],w3
 567 000284  00 00 00 	nop 
 568 000286  4E 02 90 	mov [w14+8],w4
 569 000288  00 00 00 	nop 
 570 00028a  DE 02 90 	mov [w14+10],w5
 571 00028c  00 00 07 	rcall _parse_can_message_hard_breaking_thresh
 572 00028e  00 00 37 	bra .L11
 573              	.L25:
 574              	.L11:
 123:lib/can-ids/Devices/INTERFACE_CAN.c **** 	}
 124:lib/can-ids/Devices/INTERFACE_CAN.c **** }
 575              	.loc 1 124 0
 576 000290  8E 07 78 	mov w14,w15
 577 000292  4F 07 78 	mov [--w15],w14
 578 000294  00 40 A9 	bclr CORCON,#2
 579 000296  00 00 06 	return 
 580              	.set ___PA___,0
 581              	.LFE10:
 582              	.size _parse_can_interface,.-_parse_can_interface
 583              	.section .debug_frame,info
 584                 	.Lframe0:
 585 0000 10 00 00 00 	.4byte .LECIE0-.LSCIE0
 586                 	.LSCIE0:
 587 0004 FF FF FF FF 	.4byte 0xffffffff
 588 0008 01          	.byte 0x1
 589 0009 00          	.byte 0
 590 000a 01          	.uleb128 0x1
 591 000b 02          	.sleb128 2
 592 000c 25          	.byte 0x25
 593 000d 12          	.byte 0x12
 594 000e 0F          	.uleb128 0xf
 595 000f 7E          	.sleb128 -2
 596 0010 09          	.byte 0x9
 597 0011 25          	.uleb128 0x25
 598 0012 0F          	.uleb128 0xf
 599 0013 00          	.align 4
 600                 	.LECIE0:
 601                 	.LSFDE0:
 602 0014 18 00 00 00 	.4byte .LEFDE0-.LASFDE0
 603                 	.LASFDE0:
 604 0018 00 00 00 00 	.4byte .Lframe0
 605 001c 00 00 00 00 	.4byte .LFB0
 606 0020 18 00 00 00 	.4byte .LFE0-.LFB0
 607 0024 04          	.byte 0x4
 608 0025 02 00 00 00 	.4byte .LCFI0-.LFB0
 609 0029 13          	.byte 0x13
MPLAB XC16 ASSEMBLY Listing:   			page 14


 610 002a 7D          	.sleb128 -3
 611 002b 0D          	.byte 0xd
 612 002c 0E          	.uleb128 0xe
 613 002d 8E          	.byte 0x8e
 614 002e 02          	.uleb128 0x2
 615 002f 00          	.align 4
 616                 	.LEFDE0:
 617                 	.LSFDE2:
 618 0030 18 00 00 00 	.4byte .LEFDE2-.LASFDE2
 619                 	.LASFDE2:
 620 0034 00 00 00 00 	.4byte .Lframe0
 621 0038 00 00 00 00 	.4byte .LFB1
 622 003c 16 00 00 00 	.4byte .LFE1-.LFB1
 623 0040 04          	.byte 0x4
 624 0041 02 00 00 00 	.4byte .LCFI1-.LFB1
 625 0045 13          	.byte 0x13
 626 0046 7D          	.sleb128 -3
 627 0047 0D          	.byte 0xd
 628 0048 0E          	.uleb128 0xe
 629 0049 8E          	.byte 0x8e
 630 004a 02          	.uleb128 0x2
 631 004b 00          	.align 4
 632                 	.LEFDE2:
 633                 	.LSFDE4:
 634 004c 18 00 00 00 	.4byte .LEFDE4-.LASFDE4
 635                 	.LASFDE4:
 636 0050 00 00 00 00 	.4byte .Lframe0
 637 0054 00 00 00 00 	.4byte .LFB2
 638 0058 16 00 00 00 	.4byte .LFE2-.LFB2
 639 005c 04          	.byte 0x4
 640 005d 02 00 00 00 	.4byte .LCFI2-.LFB2
 641 0061 13          	.byte 0x13
 642 0062 7D          	.sleb128 -3
 643 0063 0D          	.byte 0xd
 644 0064 0E          	.uleb128 0xe
 645 0065 8E          	.byte 0x8e
 646 0066 02          	.uleb128 0x2
 647 0067 00          	.align 4
 648                 	.LEFDE4:
 649                 	.LSFDE6:
 650 0068 18 00 00 00 	.4byte .LEFDE6-.LASFDE6
 651                 	.LASFDE6:
 652 006c 00 00 00 00 	.4byte .Lframe0
 653 0070 00 00 00 00 	.4byte .LFB3
 654 0074 32 00 00 00 	.4byte .LFE3-.LFB3
 655 0078 04          	.byte 0x4
 656 0079 02 00 00 00 	.4byte .LCFI3-.LFB3
 657 007d 13          	.byte 0x13
 658 007e 7D          	.sleb128 -3
 659 007f 0D          	.byte 0xd
 660 0080 0E          	.uleb128 0xe
 661 0081 8E          	.byte 0x8e
 662 0082 02          	.uleb128 0x2
 663 0083 00          	.align 4
 664                 	.LEFDE6:
 665                 	.LSFDE8:
 666 0084 18 00 00 00 	.4byte .LEFDE8-.LASFDE8
MPLAB XC16 ASSEMBLY Listing:   			page 15


 667                 	.LASFDE8:
 668 0088 00 00 00 00 	.4byte .Lframe0
 669 008c 00 00 00 00 	.4byte .LFB4
 670 0090 3E 00 00 00 	.4byte .LFE4-.LFB4
 671 0094 04          	.byte 0x4
 672 0095 02 00 00 00 	.4byte .LCFI4-.LFB4
 673 0099 13          	.byte 0x13
 674 009a 7D          	.sleb128 -3
 675 009b 0D          	.byte 0xd
 676 009c 0E          	.uleb128 0xe
 677 009d 8E          	.byte 0x8e
 678 009e 02          	.uleb128 0x2
 679 009f 00          	.align 4
 680                 	.LEFDE8:
 681                 	.LSFDE10:
 682 00a0 18 00 00 00 	.4byte .LEFDE10-.LASFDE10
 683                 	.LASFDE10:
 684 00a4 00 00 00 00 	.4byte .Lframe0
 685 00a8 00 00 00 00 	.4byte .LFB5
 686 00ac 1E 00 00 00 	.4byte .LFE5-.LFB5
 687 00b0 04          	.byte 0x4
 688 00b1 02 00 00 00 	.4byte .LCFI5-.LFB5
 689 00b5 13          	.byte 0x13
 690 00b6 7D          	.sleb128 -3
 691 00b7 0D          	.byte 0xd
 692 00b8 0E          	.uleb128 0xe
 693 00b9 8E          	.byte 0x8e
 694 00ba 02          	.uleb128 0x2
 695 00bb 00          	.align 4
 696                 	.LEFDE10:
 697                 	.LSFDE12:
 698 00bc 18 00 00 00 	.4byte .LEFDE12-.LASFDE12
 699                 	.LASFDE12:
 700 00c0 00 00 00 00 	.4byte .Lframe0
 701 00c4 00 00 00 00 	.4byte .LFB6
 702 00c8 54 00 00 00 	.4byte .LFE6-.LFB6
 703 00cc 04          	.byte 0x4
 704 00cd 02 00 00 00 	.4byte .LCFI6-.LFB6
 705 00d1 13          	.byte 0x13
 706 00d2 7D          	.sleb128 -3
 707 00d3 0D          	.byte 0xd
 708 00d4 0E          	.uleb128 0xe
 709 00d5 8E          	.byte 0x8e
 710 00d6 02          	.uleb128 0x2
 711 00d7 00          	.align 4
 712                 	.LEFDE12:
 713                 	.LSFDE14:
 714 00d8 18 00 00 00 	.4byte .LEFDE14-.LASFDE14
 715                 	.LASFDE14:
 716 00dc 00 00 00 00 	.4byte .Lframe0
 717 00e0 00 00 00 00 	.4byte .LFB7
 718 00e4 1E 00 00 00 	.4byte .LFE7-.LFB7
 719 00e8 04          	.byte 0x4
 720 00e9 02 00 00 00 	.4byte .LCFI7-.LFB7
 721 00ed 13          	.byte 0x13
 722 00ee 7D          	.sleb128 -3
 723 00ef 0D          	.byte 0xd
MPLAB XC16 ASSEMBLY Listing:   			page 16


 724 00f0 0E          	.uleb128 0xe
 725 00f1 8E          	.byte 0x8e
 726 00f2 02          	.uleb128 0x2
 727 00f3 00          	.align 4
 728                 	.LEFDE14:
 729                 	.LSFDE16:
 730 00f4 18 00 00 00 	.4byte .LEFDE16-.LASFDE16
 731                 	.LASFDE16:
 732 00f8 00 00 00 00 	.4byte .Lframe0
 733 00fc 00 00 00 00 	.4byte .LFB8
 734 0100 2A 00 00 00 	.4byte .LFE8-.LFB8
 735 0104 04          	.byte 0x4
 736 0105 02 00 00 00 	.4byte .LCFI8-.LFB8
 737 0109 13          	.byte 0x13
 738 010a 7D          	.sleb128 -3
 739 010b 0D          	.byte 0xd
 740 010c 0E          	.uleb128 0xe
 741 010d 8E          	.byte 0x8e
 742 010e 02          	.uleb128 0x2
 743 010f 00          	.align 4
 744                 	.LEFDE16:
 745                 	.LSFDE18:
 746 0110 18 00 00 00 	.4byte .LEFDE18-.LASFDE18
 747                 	.LASFDE18:
 748 0114 00 00 00 00 	.4byte .Lframe0
 749 0118 00 00 00 00 	.4byte .LFB9
 750 011c 1E 00 00 00 	.4byte .LFE9-.LFB9
 751 0120 04          	.byte 0x4
 752 0121 02 00 00 00 	.4byte .LCFI9-.LFB9
 753 0125 13          	.byte 0x13
 754 0126 7D          	.sleb128 -3
 755 0127 0D          	.byte 0xd
 756 0128 0E          	.uleb128 0xe
 757 0129 8E          	.byte 0x8e
 758 012a 02          	.uleb128 0x2
 759 012b 00          	.align 4
 760                 	.LEFDE18:
 761                 	.LSFDE20:
 762 012c 18 00 00 00 	.4byte .LEFDE20-.LASFDE20
 763                 	.LASFDE20:
 764 0130 00 00 00 00 	.4byte .Lframe0
 765 0134 00 00 00 00 	.4byte .LFB10
 766 0138 0C 01 00 00 	.4byte .LFE10-.LFB10
 767 013c 04          	.byte 0x4
 768 013d 02 00 00 00 	.4byte .LCFI10-.LFB10
 769 0141 13          	.byte 0x13
 770 0142 7D          	.sleb128 -3
 771 0143 0D          	.byte 0xd
 772 0144 0E          	.uleb128 0xe
 773 0145 8E          	.byte 0x8e
 774 0146 02          	.uleb128 0x2
 775 0147 00          	.align 4
 776                 	.LEFDE20:
 777                 	.section .text,code
 778              	.Letext0:
 779              	.file 2 "/opt/microchip/xc16/v1.36/bin/bin/../../include/lega-c/stdint.h"
 780              	.file 3 "lib/can-ids/CAN_IDs.h"
MPLAB XC16 ASSEMBLY Listing:   			page 17


 781              	.file 4 "lib/can-ids/Devices/TE_CAN.h"
 782              	.file 5 "lib/can-ids/Devices/STEER_CAN.h"
 783              	.file 6 "lib/can-ids/Devices/INTERFACE_CAN.h"
 784              	.section .debug_info,info
 785 0000 2A 0B 00 00 	.4byte 0xb2a
 786 0004 02 00       	.2byte 0x2
 787 0006 00 00 00 00 	.4byte .Ldebug_abbrev0
 788 000a 04          	.byte 0x4
 789 000b 01          	.uleb128 0x1
 790 000c 47 4E 55 20 	.asciz "GNU C 4.5.1 (XC16, Microchip v1.36) (A) Build date: Dec  5 2018"
 790      43 20 34 2E 
 790      35 2E 31 20 
 790      28 58 43 31 
 790      36 2C 20 4D 
 790      69 63 72 6F 
 790      63 68 69 70 
 790      20 76 31 2E 
 790      33 36 29 20 
 791 004c 01          	.byte 0x1
 792 004d 6C 69 62 2F 	.asciz "lib/can-ids/Devices/INTERFACE_CAN.c"
 792      63 61 6E 2D 
 792      69 64 73 2F 
 792      44 65 76 69 
 792      63 65 73 2F 
 792      49 4E 54 45 
 792      52 46 41 43 
 792      45 5F 43 41 
 792      4E 2E 63 00 
 793 0071 2F 68 6F 6D 	.asciz "/home/user/Documents/FST/Programming/project_sub-zero"
 793      65 2F 75 73 
 793      65 72 2F 44 
 793      6F 63 75 6D 
 793      65 6E 74 73 
 793      2F 46 53 54 
 793      2F 50 72 6F 
 793      67 72 61 6D 
 793      6D 69 6E 67 
 794 00a7 00 00 00 00 	.4byte .Ltext0
 795 00ab 00 00 00 00 	.4byte .Letext0
 796 00af 00 00 00 00 	.4byte .Ldebug_line0
 797 00b3 02          	.uleb128 0x2
 798 00b4 01          	.byte 0x1
 799 00b5 06          	.byte 0x6
 800 00b6 73 69 67 6E 	.asciz "signed char"
 800      65 64 20 63 
 800      68 61 72 00 
 801 00c2 03          	.uleb128 0x3
 802 00c3 69 6E 74 31 	.asciz "int16_t"
 802      36 5F 74 00 
 803 00cb 02          	.byte 0x2
 804 00cc 14          	.byte 0x14
 805 00cd D1 00 00 00 	.4byte 0xd1
 806 00d1 02          	.uleb128 0x2
 807 00d2 02          	.byte 0x2
 808 00d3 05          	.byte 0x5
 809 00d4 69 6E 74 00 	.asciz "int"
 810 00d8 02          	.uleb128 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 18


 811 00d9 04          	.byte 0x4
 812 00da 05          	.byte 0x5
 813 00db 6C 6F 6E 67 	.asciz "long int"
 813      20 69 6E 74 
 813      00 
 814 00e4 02          	.uleb128 0x2
 815 00e5 08          	.byte 0x8
 816 00e6 05          	.byte 0x5
 817 00e7 6C 6F 6E 67 	.asciz "long long int"
 817      20 6C 6F 6E 
 817      67 20 69 6E 
 817      74 00 
 818 00f5 03          	.uleb128 0x3
 819 00f6 75 69 6E 74 	.asciz "uint8_t"
 819      38 5F 74 00 
 820 00fe 02          	.byte 0x2
 821 00ff 2B          	.byte 0x2b
 822 0100 04 01 00 00 	.4byte 0x104
 823 0104 02          	.uleb128 0x2
 824 0105 01          	.byte 0x1
 825 0106 08          	.byte 0x8
 826 0107 75 6E 73 69 	.asciz "unsigned char"
 826      67 6E 65 64 
 826      20 63 68 61 
 826      72 00 
 827 0115 03          	.uleb128 0x3
 828 0116 75 69 6E 74 	.asciz "uint16_t"
 828      31 36 5F 74 
 828      00 
 829 011f 02          	.byte 0x2
 830 0120 31          	.byte 0x31
 831 0121 25 01 00 00 	.4byte 0x125
 832 0125 02          	.uleb128 0x2
 833 0126 02          	.byte 0x2
 834 0127 07          	.byte 0x7
 835 0128 75 6E 73 69 	.asciz "unsigned int"
 835      67 6E 65 64 
 835      20 69 6E 74 
 835      00 
 836 0135 02          	.uleb128 0x2
 837 0136 04          	.byte 0x4
 838 0137 07          	.byte 0x7
 839 0138 6C 6F 6E 67 	.asciz "long unsigned int"
 839      20 75 6E 73 
 839      69 67 6E 65 
 839      64 20 69 6E 
 839      74 00 
 840 014a 02          	.uleb128 0x2
 841 014b 08          	.byte 0x8
 842 014c 07          	.byte 0x7
 843 014d 6C 6F 6E 67 	.asciz "long long unsigned int"
 843      20 6C 6F 6E 
 843      67 20 75 6E 
 843      73 69 67 6E 
 843      65 64 20 69 
 843      6E 74 00 
 844 0164 04          	.uleb128 0x4
MPLAB XC16 ASSEMBLY Listing:   			page 19


 845 0165 02          	.byte 0x2
 846 0166 03          	.byte 0x3
 847 0167 12          	.byte 0x12
 848 0168 95 01 00 00 	.4byte 0x195
 849 016c 05          	.uleb128 0x5
 850 016d 64 65 76 5F 	.asciz "dev_id"
 850      69 64 00 
 851 0174 03          	.byte 0x3
 852 0175 13          	.byte 0x13
 853 0176 15 01 00 00 	.4byte 0x115
 854 017a 02          	.byte 0x2
 855 017b 05          	.byte 0x5
 856 017c 0B          	.byte 0xb
 857 017d 02          	.byte 0x2
 858 017e 23          	.byte 0x23
 859 017f 00          	.uleb128 0x0
 860 0180 05          	.uleb128 0x5
 861 0181 6D 73 67 5F 	.asciz "msg_id"
 861      69 64 00 
 862 0188 03          	.byte 0x3
 863 0189 16          	.byte 0x16
 864 018a 15 01 00 00 	.4byte 0x115
 865 018e 02          	.byte 0x2
 866 018f 06          	.byte 0x6
 867 0190 05          	.byte 0x5
 868 0191 02          	.byte 0x2
 869 0192 23          	.byte 0x23
 870 0193 00          	.uleb128 0x0
 871 0194 00          	.byte 0x0
 872 0195 06          	.uleb128 0x6
 873 0196 02          	.byte 0x2
 874 0197 03          	.byte 0x3
 875 0198 11          	.byte 0x11
 876 0199 AE 01 00 00 	.4byte 0x1ae
 877 019d 07          	.uleb128 0x7
 878 019e 64 01 00 00 	.4byte 0x164
 879 01a2 08          	.uleb128 0x8
 880 01a3 73 69 64 00 	.asciz "sid"
 881 01a7 03          	.byte 0x3
 882 01a8 18          	.byte 0x18
 883 01a9 15 01 00 00 	.4byte 0x115
 884 01ad 00          	.byte 0x0
 885 01ae 04          	.uleb128 0x4
 886 01af 0C          	.byte 0xc
 887 01b0 03          	.byte 0x3
 888 01b1 10          	.byte 0x10
 889 01b2 DE 01 00 00 	.4byte 0x1de
 890 01b6 09          	.uleb128 0x9
 891 01b7 95 01 00 00 	.4byte 0x195
 892 01bb 02          	.byte 0x2
 893 01bc 23          	.byte 0x23
 894 01bd 00          	.uleb128 0x0
 895 01be 05          	.uleb128 0x5
 896 01bf 64 6C 63 00 	.asciz "dlc"
 897 01c3 03          	.byte 0x3
 898 01c4 1A          	.byte 0x1a
 899 01c5 15 01 00 00 	.4byte 0x115
MPLAB XC16 ASSEMBLY Listing:   			page 20


 900 01c9 02          	.byte 0x2
 901 01ca 04          	.byte 0x4
 902 01cb 0C          	.byte 0xc
 903 01cc 02          	.byte 0x2
 904 01cd 23          	.byte 0x23
 905 01ce 02          	.uleb128 0x2
 906 01cf 0A          	.uleb128 0xa
 907 01d0 00 00 00 00 	.4byte .LASF0
 908 01d4 03          	.byte 0x3
 909 01d5 1B          	.byte 0x1b
 910 01d6 DE 01 00 00 	.4byte 0x1de
 911 01da 02          	.byte 0x2
 912 01db 23          	.byte 0x23
 913 01dc 04          	.uleb128 0x4
 914 01dd 00          	.byte 0x0
 915 01de 0B          	.uleb128 0xb
 916 01df 15 01 00 00 	.4byte 0x115
 917 01e3 EE 01 00 00 	.4byte 0x1ee
 918 01e7 0C          	.uleb128 0xc
 919 01e8 25 01 00 00 	.4byte 0x125
 920 01ec 03          	.byte 0x3
 921 01ed 00          	.byte 0x0
 922 01ee 03          	.uleb128 0x3
 923 01ef 43 41 4E 64 	.asciz "CANdata"
 923      61 74 61 00 
 924 01f7 03          	.byte 0x3
 925 01f8 1C          	.byte 0x1c
 926 01f9 AE 01 00 00 	.4byte 0x1ae
 927 01fd 02          	.uleb128 0x2
 928 01fe 01          	.byte 0x1
 929 01ff 02          	.byte 0x2
 930 0200 5F 42 6F 6F 	.asciz "_Bool"
 930      6C 00 
 931 0206 0D          	.uleb128 0xd
 932 0207 02          	.byte 0x2
 933 0208 04          	.byte 0x4
 934 0209 3B          	.byte 0x3b
 935 020a 4D 02 00 00 	.4byte 0x24d
 936 020e 0E          	.uleb128 0xe
 937 020f 42 52 41 4B 	.asciz "BRAKE_ZERO"
 937      45 5F 5A 45 
 937      52 4F 00 
 938 021a 00          	.sleb128 0
 939 021b 0E          	.uleb128 0xe
 940 021c 42 52 41 4B 	.asciz "BRAKE_MAX"
 940      45 5F 4D 41 
 940      58 00 
 941 0226 01          	.sleb128 1
 942 0227 0E          	.uleb128 0xe
 943 0228 41 43 43 45 	.asciz "ACCELERATOR_ZERO"
 943      4C 45 52 41 
 943      54 4F 52 5F 
 943      5A 45 52 4F 
 943      00 
 944 0239 02          	.sleb128 2
 945 023a 0E          	.uleb128 0xe
 946 023b 41 43 43 45 	.asciz "ACCELERATOR_MAX"
MPLAB XC16 ASSEMBLY Listing:   			page 21


 946      4C 45 52 41 
 946      54 4F 52 5F 
 946      4D 41 58 00 
 947 024b 03          	.sleb128 3
 948 024c 00          	.byte 0x0
 949 024d 03          	.uleb128 0x3
 950 024e 49 4E 54 45 	.asciz "INTERFACE_MSG_PEDAL_THRESHOLDS"
 950      52 46 41 43 
 950      45 5F 4D 53 
 950      47 5F 50 45 
 950      44 41 4C 5F 
 950      54 48 52 45 
 950      53 48 4F 4C 
 950      44 53 00 
 951 026d 04          	.byte 0x4
 952 026e 41          	.byte 0x41
 953 026f 06 02 00 00 	.4byte 0x206
 954 0273 0D          	.uleb128 0xd
 955 0274 02          	.byte 0x2
 956 0275 05          	.byte 0x5
 957 0276 0B          	.byte 0xb
 958 0277 A6 02 00 00 	.4byte 0x2a6
 959 027b 0E          	.uleb128 0xe
 960 027c 53 54 45 45 	.asciz "STEER_LEFT"
 960      52 5F 4C 45 
 960      46 54 00 
 961 0287 00          	.sleb128 0
 962 0288 0E          	.uleb128 0xe
 963 0289 53 54 45 45 	.asciz "STEER_CENTRE"
 963      52 5F 43 45 
 963      4E 54 52 45 
 963      00 
 964 0296 01          	.sleb128 1
 965 0297 0E          	.uleb128 0xe
 966 0298 53 54 45 45 	.asciz "STEER_RIGHT"
 966      52 5F 52 49 
 966      47 48 54 00 
 967 02a4 02          	.sleb128 2
 968 02a5 00          	.byte 0x0
 969 02a6 03          	.uleb128 0x3
 970 02a7 49 4E 54 45 	.asciz "INTERFACE_MSG_STEER_THRESHOLDS"
 970      52 46 41 43 
 970      45 5F 4D 53 
 970      47 5F 53 54 
 970      45 45 52 5F 
 970      54 48 52 45 
 970      53 48 4F 4C 
 970      44 53 00 
 971 02c6 05          	.byte 0x5
 972 02c7 10          	.byte 0x10
 973 02c8 73 02 00 00 	.4byte 0x273
 974 02cc 0D          	.uleb128 0xd
 975 02cd 02          	.byte 0x2
 976 02ce 06          	.byte 0x6
 977 02cf 1A          	.byte 0x1a
 978 02d0 36 03 00 00 	.4byte 0x336
 979 02d4 0E          	.uleb128 0xe
MPLAB XC16 ASSEMBLY Listing:   			page 22


 980 02d5 44 43 55 5F 	.asciz "DCU_FANS"
 980      46 41 4E 53 
 980      00 
 981 02de 00          	.sleb128 0
 982 02df 0E          	.uleb128 0xe
 983 02e0 44 43 55 5F 	.asciz "DCU_PUMPS1"
 983      50 55 4D 50 
 983      53 31 00 
 984 02eb 01          	.sleb128 1
 985 02ec 0E          	.uleb128 0xe
 986 02ed 44 43 55 5F 	.asciz "DCU_PUMPS2"
 986      50 55 4D 50 
 986      53 32 00 
 987 02f8 02          	.sleb128 2
 988 02f9 0E          	.uleb128 0xe
 989 02fa 44 43 55 5F 	.asciz "DCU_CUA"
 989      43 55 41 00 
 990 0302 03          	.sleb128 3
 991 0303 0E          	.uleb128 0xe
 992 0304 44 43 55 5F 	.asciz "DCU_CUB"
 992      43 55 42 00 
 993 030c 04          	.sleb128 4
 994 030d 0E          	.uleb128 0xe
 995 030e 44 43 55 5F 	.asciz "DCU_SC"
 995      53 43 00 
 996 0315 05          	.sleb128 5
 997 0316 0E          	.uleb128 0xe
 998 0317 44 43 55 5F 	.asciz "DCU_BLUE_TSAL"
 998      42 4C 55 45 
 998      5F 54 53 41 
 998      4C 00 
 999 0325 06          	.sleb128 6
 1000 0326 0E          	.uleb128 0xe
 1001 0327 44 43 55 5F 	.asciz "DCU_DCDC_DRS"
 1001      44 43 44 43 
 1001      5F 44 52 53 
 1001      00 
 1002 0334 07          	.sleb128 7
 1003 0335 00          	.byte 0x0
 1004 0336 03          	.uleb128 0x3
 1005 0337 44 43 55 5F 	.asciz "DCU_TOGGLE"
 1005      54 4F 47 47 
 1005      4C 45 00 
 1006 0342 06          	.byte 0x6
 1007 0343 23          	.byte 0x23
 1008 0344 CC 02 00 00 	.4byte 0x2cc
 1009 0348 0D          	.uleb128 0xd
 1010 0349 02          	.byte 0x2
 1011 034a 06          	.byte 0x6
 1012 034b 25          	.byte 0x25
 1013 034c 96 03 00 00 	.4byte 0x396
 1014 0350 0E          	.uleb128 0xe
 1015 0351 44 45 42 55 	.asciz "DEBUG_TOGGLE_TORQUE_ENCODER"
 1015      47 5F 54 4F 
 1015      47 47 4C 45 
 1015      5F 54 4F 52 
 1015      51 55 45 5F 
MPLAB XC16 ASSEMBLY Listing:   			page 23


 1015      45 4E 43 4F 
 1015      44 45 52 00 
 1016 036d 00          	.sleb128 0
 1017 036e 0E          	.uleb128 0xe
 1018 036f 44 45 42 55 	.asciz "DEBUG_TOGGLE_DCU"
 1018      47 5F 54 4F 
 1018      47 47 4C 45 
 1018      5F 44 43 55 
 1018      00 
 1019 0380 01          	.sleb128 1
 1020 0381 0E          	.uleb128 0xe
 1021 0382 44 45 42 55 	.asciz "DEBUG_TOGGLE_DASH"
 1021      47 5F 54 4F 
 1021      47 47 4C 45 
 1021      5F 44 41 53 
 1021      48 00 
 1022 0394 02          	.sleb128 2
 1023 0395 00          	.byte 0x0
 1024 0396 03          	.uleb128 0x3
 1025 0397 44 45 42 55 	.asciz "DEBUG_TOGGLE"
 1025      47 5F 54 4F 
 1025      47 47 4C 45 
 1025      00 
 1026 03a4 06          	.byte 0x6
 1027 03a5 29          	.byte 0x29
 1028 03a6 48 03 00 00 	.4byte 0x348
 1029 03aa 04          	.uleb128 0x4
 1030 03ab 02          	.byte 0x2
 1031 03ac 06          	.byte 0x6
 1032 03ad 2C          	.byte 0x2c
 1033 03ae C2 03 00 00 	.4byte 0x3c2
 1034 03b2 0F          	.uleb128 0xf
 1035 03b3 63 6F 64 65 	.asciz "code"
 1035      00 
 1036 03b8 06          	.byte 0x6
 1037 03b9 2D          	.byte 0x2d
 1038 03ba 36 03 00 00 	.4byte 0x336
 1039 03be 02          	.byte 0x2
 1040 03bf 23          	.byte 0x23
 1041 03c0 00          	.uleb128 0x0
 1042 03c1 00          	.byte 0x0
 1043 03c2 03          	.uleb128 0x3
 1044 03c3 49 4E 54 45 	.asciz "INTERFACE_MSG_DCU_TOGGLE"
 1044      52 46 41 43 
 1044      45 5F 4D 53 
 1044      47 5F 44 43 
 1044      55 5F 54 4F 
 1044      47 47 4C 45 
 1044      00 
 1045 03dc 06          	.byte 0x6
 1046 03dd 2F          	.byte 0x2f
 1047 03de AA 03 00 00 	.4byte 0x3aa
 1048 03e2 04          	.uleb128 0x4
 1049 03e3 02          	.byte 0x2
 1050 03e4 06          	.byte 0x6
 1051 03e5 32          	.byte 0x32
 1052 03e6 FC 03 00 00 	.4byte 0x3fc
MPLAB XC16 ASSEMBLY Listing:   			page 24


 1053 03ea 0F          	.uleb128 0xf
 1054 03eb 64 65 76 69 	.asciz "device"
 1054      63 65 00 
 1055 03f2 06          	.byte 0x6
 1056 03f3 33          	.byte 0x33
 1057 03f4 96 03 00 00 	.4byte 0x396
 1058 03f8 02          	.byte 0x2
 1059 03f9 23          	.byte 0x23
 1060 03fa 00          	.uleb128 0x0
 1061 03fb 00          	.byte 0x0
 1062 03fc 03          	.uleb128 0x3
 1063 03fd 49 4E 54 45 	.asciz "INTERFACE_MSG_DEBUG_TOGGLE"
 1063      52 46 41 43 
 1063      45 5F 4D 53 
 1063      47 5F 44 45 
 1063      42 55 47 5F 
 1063      54 4F 47 47 
 1063      4C 45 00 
 1064 0418 06          	.byte 0x6
 1065 0419 34          	.byte 0x34
 1066 041a E2 03 00 00 	.4byte 0x3e2
 1067 041e 04          	.uleb128 0x4
 1068 041f 08          	.byte 0x8
 1069 0420 06          	.byte 0x6
 1070 0421 36          	.byte 0x36
 1071 0422 7E 04 00 00 	.4byte 0x47e
 1072 0426 0F          	.uleb128 0xf
 1073 0427 6D 61 78 5F 	.asciz "max_rpm"
 1073      72 70 6D 00 
 1074 042f 06          	.byte 0x6
 1075 0430 37          	.byte 0x37
 1076 0431 15 01 00 00 	.4byte 0x115
 1077 0435 02          	.byte 0x2
 1078 0436 23          	.byte 0x23
 1079 0437 00          	.uleb128 0x0
 1080 0438 0F          	.uleb128 0xf
 1081 0439 6D 61 78 5F 	.asciz "max_torque_front"
 1081      74 6F 72 71 
 1081      75 65 5F 66 
 1081      72 6F 6E 74 
 1081      00 
 1082 044a 06          	.byte 0x6
 1083 044b 38          	.byte 0x38
 1084 044c C2 00 00 00 	.4byte 0xc2
 1085 0450 02          	.byte 0x2
 1086 0451 23          	.byte 0x23
 1087 0452 02          	.uleb128 0x2
 1088 0453 0F          	.uleb128 0xf
 1089 0454 6D 61 78 5F 	.asciz "max_torque_rear"
 1089      74 6F 72 71 
 1089      75 65 5F 72 
 1089      65 61 72 00 
 1090 0464 06          	.byte 0x6
 1091 0465 39          	.byte 0x39
 1092 0466 C2 00 00 00 	.4byte 0xc2
 1093 046a 02          	.byte 0x2
 1094 046b 23          	.byte 0x23
MPLAB XC16 ASSEMBLY Listing:   			page 25


 1095 046c 04          	.uleb128 0x4
 1096 046d 0F          	.uleb128 0xf
 1097 046e 72 65 73 65 	.asciz "reset"
 1097      74 00 
 1098 0474 06          	.byte 0x6
 1099 0475 3A          	.byte 0x3a
 1100 0476 15 01 00 00 	.4byte 0x115
 1101 047a 02          	.byte 0x2
 1102 047b 23          	.byte 0x23
 1103 047c 06          	.uleb128 0x6
 1104 047d 00          	.byte 0x0
 1105 047e 03          	.uleb128 0x3
 1106 047f 49 4E 54 45 	.asciz "INTERFACE_MSG_INVERTERS"
 1106      52 46 41 43 
 1106      45 5F 4D 53 
 1106      47 5F 49 4E 
 1106      56 45 52 54 
 1106      45 52 53 00 
 1107 0497 06          	.byte 0x6
 1108 0498 3B          	.byte 0x3b
 1109 0499 1E 04 00 00 	.4byte 0x41e
 1110 049d 04          	.uleb128 0x4
 1111 049e 0A          	.byte 0xa
 1112 049f 06          	.byte 0x6
 1113 04a0 3D          	.byte 0x3d
 1114 04a1 E1 04 00 00 	.4byte 0x4e1
 1115 04a5 0F          	.uleb128 0xf
 1116 04a6 6D 6F 64 65 	.asciz "mode"
 1116      00 
 1117 04ab 06          	.byte 0x6
 1118 04ac 3E          	.byte 0x3e
 1119 04ad 15 01 00 00 	.4byte 0x115
 1120 04b1 02          	.byte 0x2
 1121 04b2 23          	.byte 0x23
 1122 04b3 00          	.uleb128 0x0
 1123 04b4 0F          	.uleb128 0xf
 1124 04b5 67 61 69 6E 	.asciz "gain"
 1124      00 
 1125 04ba 06          	.byte 0x6
 1126 04bb 3F          	.byte 0x3f
 1127 04bc C2 00 00 00 	.4byte 0xc2
 1128 04c0 02          	.byte 0x2
 1129 04c1 23          	.byte 0x23
 1130 04c2 02          	.uleb128 0x2
 1131 04c3 0F          	.uleb128 0xf
 1132 04c4 74 6C 00    	.asciz "tl"
 1133 04c7 06          	.byte 0x6
 1134 04c8 40          	.byte 0x40
 1135 04c9 E1 04 00 00 	.4byte 0x4e1
 1136 04cd 02          	.byte 0x2
 1137 04ce 23          	.byte 0x23
 1138 04cf 04          	.uleb128 0x4
 1139 04d0 0F          	.uleb128 0xf
 1140 04d1 70 6F 77 65 	.asciz "power"
 1140      72 00 
 1141 04d7 06          	.byte 0x6
 1142 04d8 41          	.byte 0x41
MPLAB XC16 ASSEMBLY Listing:   			page 26


 1143 04d9 15 01 00 00 	.4byte 0x115
 1144 04dd 02          	.byte 0x2
 1145 04de 23          	.byte 0x23
 1146 04df 08          	.uleb128 0x8
 1147 04e0 00          	.byte 0x0
 1148 04e1 02          	.uleb128 0x2
 1149 04e2 04          	.byte 0x4
 1150 04e3 04          	.byte 0x4
 1151 04e4 66 6C 6F 61 	.asciz "float"
 1151      74 00 
 1152 04ea 03          	.uleb128 0x3
 1153 04eb 49 4E 54 45 	.asciz "INTERFACE_MSG_ARM1"
 1153      52 46 41 43 
 1153      45 5F 4D 53 
 1153      47 5F 41 52 
 1153      4D 31 00 
 1154 04fe 06          	.byte 0x6
 1155 04ff 42          	.byte 0x42
 1156 0500 9D 04 00 00 	.4byte 0x49d
 1157 0504 04          	.uleb128 0x4
 1158 0505 08          	.byte 0x8
 1159 0506 06          	.byte 0x6
 1160 0507 44          	.byte 0x44
 1161 0508 6B 05 00 00 	.4byte 0x56b
 1162 050c 0F          	.uleb128 0xf
 1163 050d 79 65 61 72 	.asciz "year"
 1163      00 
 1164 0512 06          	.byte 0x6
 1165 0513 45          	.byte 0x45
 1166 0514 15 01 00 00 	.4byte 0x115
 1167 0518 02          	.byte 0x2
 1168 0519 23          	.byte 0x23
 1169 051a 00          	.uleb128 0x0
 1170 051b 0F          	.uleb128 0xf
 1171 051c 6D 6F 6E 74 	.asciz "month"
 1171      68 00 
 1172 0522 06          	.byte 0x6
 1173 0523 46          	.byte 0x46
 1174 0524 F5 00 00 00 	.4byte 0xf5
 1175 0528 02          	.byte 0x2
 1176 0529 23          	.byte 0x23
 1177 052a 02          	.uleb128 0x2
 1178 052b 0F          	.uleb128 0xf
 1179 052c 64 61 79 00 	.asciz "day"
 1180 0530 06          	.byte 0x6
 1181 0531 47          	.byte 0x47
 1182 0532 F5 00 00 00 	.4byte 0xf5
 1183 0536 02          	.byte 0x2
 1184 0537 23          	.byte 0x23
 1185 0538 03          	.uleb128 0x3
 1186 0539 0F          	.uleb128 0xf
 1187 053a 68 6F 75 72 	.asciz "hour"
 1187      00 
 1188 053f 06          	.byte 0x6
 1189 0540 48          	.byte 0x48
 1190 0541 F5 00 00 00 	.4byte 0xf5
 1191 0545 02          	.byte 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 27


 1192 0546 23          	.byte 0x23
 1193 0547 04          	.uleb128 0x4
 1194 0548 0F          	.uleb128 0xf
 1195 0549 6D 69 6E 75 	.asciz "minute"
 1195      74 65 00 
 1196 0550 06          	.byte 0x6
 1197 0551 49          	.byte 0x49
 1198 0552 F5 00 00 00 	.4byte 0xf5
 1199 0556 02          	.byte 0x2
 1200 0557 23          	.byte 0x23
 1201 0558 05          	.uleb128 0x5
 1202 0559 0F          	.uleb128 0xf
 1203 055a 73 65 63 6F 	.asciz "second"
 1203      6E 64 00 
 1204 0561 06          	.byte 0x6
 1205 0562 4A          	.byte 0x4a
 1206 0563 F5 00 00 00 	.4byte 0xf5
 1207 0567 02          	.byte 0x2
 1208 0568 23          	.byte 0x23
 1209 0569 06          	.uleb128 0x6
 1210 056a 00          	.byte 0x0
 1211 056b 03          	.uleb128 0x3
 1212 056c 49 4E 54 45 	.asciz "INTERFACE_MSG_START_LOG_DATE"
 1212      52 46 41 43 
 1212      45 5F 4D 53 
 1212      47 5F 53 54 
 1212      41 52 54 5F 
 1212      4C 4F 47 5F 
 1212      44 41 54 45 
 1212      00 
 1213 0589 06          	.byte 0x6
 1214 058a 4B          	.byte 0x4b
 1215 058b 04 05 00 00 	.4byte 0x504
 1216 058f 04          	.uleb128 0x4
 1217 0590 02          	.byte 0x2
 1218 0591 06          	.byte 0x6
 1219 0592 4E          	.byte 0x4e
 1220 0593 AA 05 00 00 	.4byte 0x5aa
 1221 0597 0F          	.uleb128 0xf
 1222 0598 64 72 73 5F 	.asciz "drs_sig"
 1222      73 69 67 00 
 1223 05a0 06          	.byte 0x6
 1224 05a1 4F          	.byte 0x4f
 1225 05a2 15 01 00 00 	.4byte 0x115
 1226 05a6 02          	.byte 0x2
 1227 05a7 23          	.byte 0x23
 1228 05a8 00          	.uleb128 0x0
 1229 05a9 00          	.byte 0x0
 1230 05aa 03          	.uleb128 0x3
 1231 05ab 49 4E 54 45 	.asciz "INTERFACE_MSG_SET_DRS"
 1231      52 46 41 43 
 1231      45 5F 4D 53 
 1231      47 5F 53 45 
 1231      54 5F 44 52 
 1231      53 00 
 1232 05c1 06          	.byte 0x6
 1233 05c2 50          	.byte 0x50
MPLAB XC16 ASSEMBLY Listing:   			page 28


 1234 05c3 8F 05 00 00 	.4byte 0x58f
 1235 05c7 04          	.uleb128 0x4
 1236 05c8 04          	.byte 0x4
 1237 05c9 06          	.byte 0x6
 1238 05ca 52          	.byte 0x52
 1239 05cb F1 05 00 00 	.4byte 0x5f1
 1240 05cf 0F          	.uleb128 0xf
 1241 05d0 6C 69 6D 69 	.asciz "limit"
 1241      74 00 
 1242 05d6 06          	.byte 0x6
 1243 05d7 53          	.byte 0x53
 1244 05d8 15 01 00 00 	.4byte 0x115
 1245 05dc 02          	.byte 0x2
 1246 05dd 23          	.byte 0x23
 1247 05de 00          	.uleb128 0x0
 1248 05df 0F          	.uleb128 0xf
 1249 05e0 73 65 6E 73 	.asciz "sensor"
 1249      6F 72 00 
 1250 05e7 06          	.byte 0x6
 1251 05e8 54          	.byte 0x54
 1252 05e9 15 01 00 00 	.4byte 0x115
 1253 05ed 02          	.byte 0x2
 1254 05ee 23          	.byte 0x23
 1255 05ef 02          	.uleb128 0x2
 1256 05f0 00          	.byte 0x0
 1257 05f1 03          	.uleb128 0x3
 1258 05f2 49 4E 54 45 	.asciz "INTERFACE_MSG_TE_LIMIT"
 1258      52 46 41 43 
 1258      45 5F 4D 53 
 1258      47 5F 54 45 
 1258      5F 4C 49 4D 
 1258      49 54 00 
 1259 0609 06          	.byte 0x6
 1260 060a 55          	.byte 0x55
 1261 060b C7 05 00 00 	.4byte 0x5c7
 1262 060f 04          	.uleb128 0x4
 1263 0610 02          	.byte 0x2
 1264 0611 06          	.byte 0x6
 1265 0612 58          	.byte 0x58
 1266 0613 3C 06 00 00 	.4byte 0x63c
 1267 0617 0F          	.uleb128 0xf
 1268 0618 68 61 72 64 	.asciz "hard_breaking_press_limit"
 1268      5F 62 72 65 
 1268      61 6B 69 6E 
 1268      67 5F 70 72 
 1268      65 73 73 5F 
 1268      6C 69 6D 69 
 1268      74 00 
 1269 0632 06          	.byte 0x6
 1270 0633 5A          	.byte 0x5a
 1271 0634 15 01 00 00 	.4byte 0x115
 1272 0638 02          	.byte 0x2
 1273 0639 23          	.byte 0x23
 1274 063a 00          	.uleb128 0x0
 1275 063b 00          	.byte 0x0
 1276 063c 03          	.uleb128 0x3
 1277 063d 49 4E 54 45 	.asciz "INTERFACE_MSG_HARD_BREAKING"
MPLAB XC16 ASSEMBLY Listing:   			page 29


 1277      52 46 41 43 
 1277      45 5F 4D 53 
 1277      47 5F 48 41 
 1277      52 44 5F 42 
 1277      52 45 41 4B 
 1277      49 4E 47 00 
 1278 0659 06          	.byte 0x6
 1279 065a 5B          	.byte 0x5b
 1280 065b 0F 06 00 00 	.4byte 0x60f
 1281 065f 04          	.uleb128 0x4
 1282 0660 2A          	.byte 0x2a
 1283 0661 06          	.byte 0x6
 1284 0662 5E          	.byte 0x5e
 1285 0663 3E 07 00 00 	.4byte 0x73e
 1286 0667 0A          	.uleb128 0xa
 1287 0668 00 00 00 00 	.4byte .LASF1
 1288 066c 06          	.byte 0x6
 1289 066d 5F          	.byte 0x5f
 1290 066e C2 03 00 00 	.4byte 0x3c2
 1291 0672 02          	.byte 0x2
 1292 0673 23          	.byte 0x23
 1293 0674 00          	.uleb128 0x0
 1294 0675 0A          	.uleb128 0xa
 1295 0676 00 00 00 00 	.4byte .LASF2
 1296 067a 06          	.byte 0x6
 1297 067b 60          	.byte 0x60
 1298 067c FC 03 00 00 	.4byte 0x3fc
 1299 0680 02          	.byte 0x2
 1300 0681 23          	.byte 0x23
 1301 0682 02          	.uleb128 0x2
 1302 0683 0F          	.uleb128 0xf
 1303 0684 70 65 64 61 	.asciz "pedal_thresholds"
 1303      6C 5F 74 68 
 1303      72 65 73 68 
 1303      6F 6C 64 73 
 1303      00 
 1304 0695 06          	.byte 0x6
 1305 0696 61          	.byte 0x61
 1306 0697 4D 02 00 00 	.4byte 0x24d
 1307 069b 02          	.byte 0x2
 1308 069c 23          	.byte 0x23
 1309 069d 04          	.uleb128 0x4
 1310 069e 0F          	.uleb128 0xf
 1311 069f 69 6E 76 65 	.asciz "inverters_limitation"
 1311      72 74 65 72 
 1311      73 5F 6C 69 
 1311      6D 69 74 61 
 1311      74 69 6F 6E 
 1311      00 
 1312 06b4 06          	.byte 0x6
 1313 06b5 62          	.byte 0x62
 1314 06b6 7E 04 00 00 	.4byte 0x47e
 1315 06ba 02          	.byte 0x2
 1316 06bb 23          	.byte 0x23
 1317 06bc 06          	.uleb128 0x6
 1318 06bd 0F          	.uleb128 0xf
 1319 06be 73 74 65 65 	.asciz "steer_thresholds"
MPLAB XC16 ASSEMBLY Listing:   			page 30


 1319      72 5F 74 68 
 1319      72 65 73 68 
 1319      6F 6C 64 73 
 1319      00 
 1320 06cf 06          	.byte 0x6
 1321 06d0 63          	.byte 0x63
 1322 06d1 A6 02 00 00 	.4byte 0x2a6
 1323 06d5 02          	.byte 0x2
 1324 06d6 23          	.byte 0x23
 1325 06d7 0E          	.uleb128 0xe
 1326 06d8 0F          	.uleb128 0xf
 1327 06d9 73 74 61 72 	.asciz "start_log"
 1327      74 5F 6C 6F 
 1327      67 00 
 1328 06e3 06          	.byte 0x6
 1329 06e4 64          	.byte 0x64
 1330 06e5 6B 05 00 00 	.4byte 0x56b
 1331 06e9 02          	.byte 0x2
 1332 06ea 23          	.byte 0x23
 1333 06eb 10          	.uleb128 0x10
 1334 06ec 0F          	.uleb128 0xf
 1335 06ed 73 65 74 5F 	.asciz "set_drs"
 1335      64 72 73 00 
 1336 06f5 06          	.byte 0x6
 1337 06f6 65          	.byte 0x65
 1338 06f7 AA 05 00 00 	.4byte 0x5aa
 1339 06fb 02          	.byte 0x2
 1340 06fc 23          	.byte 0x23
 1341 06fd 18          	.uleb128 0x18
 1342 06fe 0F          	.uleb128 0xf
 1343 06ff 61 72 6D 31 	.asciz "arm1"
 1343      00 
 1344 0704 06          	.byte 0x6
 1345 0705 66          	.byte 0x66
 1346 0706 EA 04 00 00 	.4byte 0x4ea
 1347 070a 02          	.byte 0x2
 1348 070b 23          	.byte 0x23
 1349 070c 1A          	.uleb128 0x1a
 1350 070d 0F          	.uleb128 0xf
 1351 070e 6C 69 6D 69 	.asciz "limit_request"
 1351      74 5F 72 65 
 1351      71 75 65 73 
 1351      74 00 
 1352 071c 06          	.byte 0x6
 1353 071d 67          	.byte 0x67
 1354 071e F1 05 00 00 	.4byte 0x5f1
 1355 0722 02          	.byte 0x2
 1356 0723 23          	.byte 0x23
 1357 0724 24          	.uleb128 0x24
 1358 0725 0F          	.uleb128 0xf
 1359 0726 68 61 72 64 	.asciz "hard_breaking"
 1359      5F 62 72 65 
 1359      61 6B 69 6E 
 1359      67 00 
 1360 0734 06          	.byte 0x6
 1361 0735 68          	.byte 0x68
 1362 0736 3C 06 00 00 	.4byte 0x63c
MPLAB XC16 ASSEMBLY Listing:   			page 31


 1363 073a 02          	.byte 0x2
 1364 073b 23          	.byte 0x23
 1365 073c 28          	.uleb128 0x28
 1366 073d 00          	.byte 0x0
 1367 073e 03          	.uleb128 0x3
 1368 073f 49 4E 54 45 	.asciz "INTERFACE_CAN_Data"
 1368      52 46 41 43 
 1368      45 5F 43 41 
 1368      4E 5F 44 61 
 1368      74 61 00 
 1369 0752 06          	.byte 0x6
 1370 0753 69          	.byte 0x69
 1371 0754 5F 06 00 00 	.4byte 0x65f
 1372 0758 02          	.uleb128 0x2
 1373 0759 02          	.byte 0x2
 1374 075a 07          	.byte 0x7
 1375 075b 73 68 6F 72 	.asciz "short unsigned int"
 1375      74 20 75 6E 
 1375      73 69 67 6E 
 1375      65 64 20 69 
 1375      6E 74 00 
 1376 076e 02          	.uleb128 0x2
 1377 076f 01          	.byte 0x1
 1378 0770 06          	.byte 0x6
 1379 0771 63 68 61 72 	.asciz "char"
 1379      00 
 1380 0776 10          	.uleb128 0x10
 1381 0777 01          	.byte 0x1
 1382 0778 70 61 72 73 	.asciz "parse_can_message_dcu_toggle"
 1382      65 5F 63 61 
 1382      6E 5F 6D 65 
 1382      73 73 61 67 
 1382      65 5F 64 63 
 1382      75 5F 74 6F 
 1382      67 67 6C 65 
 1382      00 
 1383 0795 01          	.byte 0x1
 1384 0796 09          	.byte 0x9
 1385 0797 01          	.byte 0x1
 1386 0798 00 00 00 00 	.4byte .LFB0
 1387 079c 00 00 00 00 	.4byte .LFE0
 1388 07a0 01          	.byte 0x1
 1389 07a1 5E          	.byte 0x5e
 1390 07a2 C3 07 00 00 	.4byte 0x7c3
 1391 07a6 11          	.uleb128 0x11
 1392 07a7 00 00 00 00 	.4byte .LASF0
 1393 07ab 01          	.byte 0x1
 1394 07ac 09          	.byte 0x9
 1395 07ad C3 07 00 00 	.4byte 0x7c3
 1396 07b1 02          	.byte 0x2
 1397 07b2 7E          	.byte 0x7e
 1398 07b3 00          	.sleb128 0
 1399 07b4 11          	.uleb128 0x11
 1400 07b5 00 00 00 00 	.4byte .LASF1
 1401 07b9 01          	.byte 0x1
 1402 07ba 09          	.byte 0x9
 1403 07bb C9 07 00 00 	.4byte 0x7c9
MPLAB XC16 ASSEMBLY Listing:   			page 32


 1404 07bf 02          	.byte 0x2
 1405 07c0 7E          	.byte 0x7e
 1406 07c1 02          	.sleb128 2
 1407 07c2 00          	.byte 0x0
 1408 07c3 12          	.uleb128 0x12
 1409 07c4 02          	.byte 0x2
 1410 07c5 15 01 00 00 	.4byte 0x115
 1411 07c9 12          	.uleb128 0x12
 1412 07ca 02          	.byte 0x2
 1413 07cb C2 03 00 00 	.4byte 0x3c2
 1414 07cf 10          	.uleb128 0x10
 1415 07d0 01          	.byte 0x1
 1416 07d1 70 61 72 73 	.asciz "parse_can_message_debug_toggle"
 1416      65 5F 63 61 
 1416      6E 5F 6D 65 
 1416      73 73 61 67 
 1416      65 5F 64 65 
 1416      62 75 67 5F 
 1416      74 6F 67 67 
 1416      6C 65 00 
 1417 07f0 01          	.byte 0x1
 1418 07f1 0E          	.byte 0xe
 1419 07f2 01          	.byte 0x1
 1420 07f3 00 00 00 00 	.4byte .LFB1
 1421 07f7 00 00 00 00 	.4byte .LFE1
 1422 07fb 01          	.byte 0x1
 1423 07fc 5E          	.byte 0x5e
 1424 07fd 20 08 00 00 	.4byte 0x820
 1425 0801 13          	.uleb128 0x13
 1426 0802 64 61 74 61 	.asciz "data0"
 1426      30 00 
 1427 0808 01          	.byte 0x1
 1428 0809 0E          	.byte 0xe
 1429 080a 15 01 00 00 	.4byte 0x115
 1430 080e 02          	.byte 0x2
 1431 080f 7E          	.byte 0x7e
 1432 0810 00          	.sleb128 0
 1433 0811 11          	.uleb128 0x11
 1434 0812 00 00 00 00 	.4byte .LASF2
 1435 0816 01          	.byte 0x1
 1436 0817 0E          	.byte 0xe
 1437 0818 20 08 00 00 	.4byte 0x820
 1438 081c 02          	.byte 0x2
 1439 081d 7E          	.byte 0x7e
 1440 081e 02          	.sleb128 2
 1441 081f 00          	.byte 0x0
 1442 0820 12          	.uleb128 0x12
 1443 0821 02          	.byte 0x2
 1444 0822 FC 03 00 00 	.4byte 0x3fc
 1445 0826 10          	.uleb128 0x10
 1446 0827 01          	.byte 0x1
 1447 0828 70 61 72 73 	.asciz "parse_can_message_pedal_thresholds"
 1447      65 5F 63 61 
 1447      6E 5F 6D 65 
 1447      73 73 61 67 
 1447      65 5F 70 65 
 1447      64 61 6C 5F 
MPLAB XC16 ASSEMBLY Listing:   			page 33


 1447      74 68 72 65 
 1447      73 68 6F 6C 
 1447      64 73 00 
 1448 084b 01          	.byte 0x1
 1449 084c 14          	.byte 0x14
 1450 084d 01          	.byte 0x1
 1451 084e 00 00 00 00 	.4byte .LFB2
 1452 0852 00 00 00 00 	.4byte .LFE2
 1453 0856 01          	.byte 0x1
 1454 0857 5E          	.byte 0x5e
 1455 0858 79 08 00 00 	.4byte 0x879
 1456 085c 11          	.uleb128 0x11
 1457 085d 00 00 00 00 	.4byte .LASF3
 1458 0861 01          	.byte 0x1
 1459 0862 14          	.byte 0x14
 1460 0863 79 08 00 00 	.4byte 0x879
 1461 0867 02          	.byte 0x2
 1462 0868 7E          	.byte 0x7e
 1463 0869 00          	.sleb128 0
 1464 086a 11          	.uleb128 0x11
 1465 086b 00 00 00 00 	.4byte .LASF0
 1466 086f 01          	.byte 0x1
 1467 0870 14          	.byte 0x14
 1468 0871 7F 08 00 00 	.4byte 0x87f
 1469 0875 02          	.byte 0x2
 1470 0876 7E          	.byte 0x7e
 1471 0877 02          	.sleb128 2
 1472 0878 00          	.byte 0x0
 1473 0879 12          	.uleb128 0x12
 1474 087a 02          	.byte 0x2
 1475 087b EE 01 00 00 	.4byte 0x1ee
 1476 087f 12          	.uleb128 0x12
 1477 0880 02          	.byte 0x2
 1478 0881 3E 07 00 00 	.4byte 0x73e
 1479 0885 10          	.uleb128 0x10
 1480 0886 01          	.byte 0x1
 1481 0887 70 61 72 73 	.asciz "parse_can_message_interface_inverters"
 1481      65 5F 63 61 
 1481      6E 5F 6D 65 
 1481      73 73 61 67 
 1481      65 5F 69 6E 
 1481      74 65 72 66 
 1481      61 63 65 5F 
 1481      69 6E 76 65 
 1481      72 74 65 72 
 1482 08ad 01          	.byte 0x1
 1483 08ae 1B          	.byte 0x1b
 1484 08af 01          	.byte 0x1
 1485 08b0 00 00 00 00 	.4byte .LFB3
 1486 08b4 00 00 00 00 	.4byte .LFE3
 1487 08b8 01          	.byte 0x1
 1488 08b9 5E          	.byte 0x5e
 1489 08ba E1 08 00 00 	.4byte 0x8e1
 1490 08be 11          	.uleb128 0x11
 1491 08bf 00 00 00 00 	.4byte .LASF0
 1492 08c3 01          	.byte 0x1
 1493 08c4 1B          	.byte 0x1b
MPLAB XC16 ASSEMBLY Listing:   			page 34


 1494 08c5 C3 07 00 00 	.4byte 0x7c3
 1495 08c9 02          	.byte 0x2
 1496 08ca 7E          	.byte 0x7e
 1497 08cb 00          	.sleb128 0
 1498 08cc 13          	.uleb128 0x13
 1499 08cd 69 6E 76 65 	.asciz "inverters"
 1499      72 74 65 72 
 1499      73 00 
 1500 08d7 01          	.byte 0x1
 1501 08d8 1B          	.byte 0x1b
 1502 08d9 E1 08 00 00 	.4byte 0x8e1
 1503 08dd 02          	.byte 0x2
 1504 08de 7E          	.byte 0x7e
 1505 08df 02          	.sleb128 2
 1506 08e0 00          	.byte 0x0
 1507 08e1 12          	.uleb128 0x12
 1508 08e2 02          	.byte 0x2
 1509 08e3 7E 04 00 00 	.4byte 0x47e
 1510 08e7 10          	.uleb128 0x10
 1511 08e8 01          	.byte 0x1
 1512 08e9 70 61 72 73 	.asciz "parse_can_message_interface_arm1"
 1512      65 5F 63 61 
 1512      6E 5F 6D 65 
 1512      73 73 61 67 
 1512      65 5F 69 6E 
 1512      74 65 72 66 
 1512      61 63 65 5F 
 1512      61 72 6D 31 
 1512      00 
 1513 090a 01          	.byte 0x1
 1514 090b 26          	.byte 0x26
 1515 090c 01          	.byte 0x1
 1516 090d 00 00 00 00 	.4byte .LFB4
 1517 0911 00 00 00 00 	.4byte .LFE4
 1518 0915 01          	.byte 0x1
 1519 0916 5E          	.byte 0x5e
 1520 0917 39 09 00 00 	.4byte 0x939
 1521 091b 13          	.uleb128 0x13
 1522 091c 6D 73 67 00 	.asciz "msg"
 1523 0920 01          	.byte 0x1
 1524 0921 26          	.byte 0x26
 1525 0922 EE 01 00 00 	.4byte 0x1ee
 1526 0926 02          	.byte 0x2
 1527 0927 7E          	.byte 0x7e
 1528 0928 00          	.sleb128 0
 1529 0929 13          	.uleb128 0x13
 1530 092a 61 72 6D 31 	.asciz "arm1"
 1530      00 
 1531 092f 01          	.byte 0x1
 1532 0930 26          	.byte 0x26
 1533 0931 39 09 00 00 	.4byte 0x939
 1534 0935 02          	.byte 0x2
 1535 0936 7E          	.byte 0x7e
 1536 0937 0C          	.sleb128 12
 1537 0938 00          	.byte 0x0
 1538 0939 12          	.uleb128 0x12
 1539 093a 02          	.byte 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 35


 1540 093b EA 04 00 00 	.4byte 0x4ea
 1541 093f 10          	.uleb128 0x10
 1542 0940 01          	.byte 0x1
 1543 0941 70 61 72 73 	.asciz "parse_can_message_set_steer_thresholds"
 1543      65 5F 63 61 
 1543      6E 5F 6D 65 
 1543      73 73 61 67 
 1543      65 5F 73 65 
 1543      74 5F 73 74 
 1543      65 65 72 5F 
 1543      74 68 72 65 
 1543      73 68 6F 6C 
 1544 0968 01          	.byte 0x1
 1545 0969 31          	.byte 0x31
 1546 096a 01          	.byte 0x1
 1547 096b 00 00 00 00 	.4byte .LFB5
 1548 096f 00 00 00 00 	.4byte .LFE5
 1549 0973 01          	.byte 0x1
 1550 0974 5E          	.byte 0x5e
 1551 0975 96 09 00 00 	.4byte 0x996
 1552 0979 11          	.uleb128 0x11
 1553 097a 00 00 00 00 	.4byte .LASF3
 1554 097e 01          	.byte 0x1
 1555 097f 31          	.byte 0x31
 1556 0980 EE 01 00 00 	.4byte 0x1ee
 1557 0984 02          	.byte 0x2
 1558 0985 7E          	.byte 0x7e
 1559 0986 00          	.sleb128 0
 1560 0987 11          	.uleb128 0x11
 1561 0988 00 00 00 00 	.4byte .LASF0
 1562 098c 01          	.byte 0x1
 1563 098d 31          	.byte 0x31
 1564 098e 7F 08 00 00 	.4byte 0x87f
 1565 0992 02          	.byte 0x2
 1566 0993 7E          	.byte 0x7e
 1567 0994 0C          	.sleb128 12
 1568 0995 00          	.byte 0x0
 1569 0996 10          	.uleb128 0x10
 1570 0997 01          	.byte 0x1
 1571 0998 70 61 72 73 	.asciz "parse_can_message_interface_start_log"
 1571      65 5F 63 61 
 1571      6E 5F 6D 65 
 1571      73 73 61 67 
 1571      65 5F 69 6E 
 1571      74 65 72 66 
 1571      61 63 65 5F 
 1571      73 74 61 72 
 1571      74 5F 6C 6F 
 1572 09be 01          	.byte 0x1
 1573 09bf 37          	.byte 0x37
 1574 09c0 01          	.byte 0x1
 1575 09c1 00 00 00 00 	.4byte .LFB6
 1576 09c5 00 00 00 00 	.4byte .LFE6
 1577 09c9 01          	.byte 0x1
 1578 09ca 5E          	.byte 0x5e
 1579 09cb EC 09 00 00 	.4byte 0x9ec
 1580 09cf 11          	.uleb128 0x11
MPLAB XC16 ASSEMBLY Listing:   			page 36


 1581 09d0 00 00 00 00 	.4byte .LASF3
 1582 09d4 01          	.byte 0x1
 1583 09d5 37          	.byte 0x37
 1584 09d6 EE 01 00 00 	.4byte 0x1ee
 1585 09da 02          	.byte 0x2
 1586 09db 7E          	.byte 0x7e
 1587 09dc 00          	.sleb128 0
 1588 09dd 11          	.uleb128 0x11
 1589 09de 00 00 00 00 	.4byte .LASF0
 1590 09e2 01          	.byte 0x1
 1591 09e3 37          	.byte 0x37
 1592 09e4 7F 08 00 00 	.4byte 0x87f
 1593 09e8 02          	.byte 0x2
 1594 09e9 7E          	.byte 0x7e
 1595 09ea 0C          	.sleb128 12
 1596 09eb 00          	.byte 0x0
 1597 09ec 10          	.uleb128 0x10
 1598 09ed 01          	.byte 0x1
 1599 09ee 70 61 72 73 	.asciz "parse_can_message_interface_set_drs"
 1599      65 5F 63 61 
 1599      6E 5F 6D 65 
 1599      73 73 61 67 
 1599      65 5F 69 6E 
 1599      74 65 72 66 
 1599      61 63 65 5F 
 1599      73 65 74 5F 
 1599      64 72 73 00 
 1600 0a12 01          	.byte 0x1
 1601 0a13 43          	.byte 0x43
 1602 0a14 01          	.byte 0x1
 1603 0a15 00 00 00 00 	.4byte .LFB7
 1604 0a19 00 00 00 00 	.4byte .LFE7
 1605 0a1d 01          	.byte 0x1
 1606 0a1e 5E          	.byte 0x5e
 1607 0a1f 40 0A 00 00 	.4byte 0xa40
 1608 0a23 11          	.uleb128 0x11
 1609 0a24 00 00 00 00 	.4byte .LASF3
 1610 0a28 01          	.byte 0x1
 1611 0a29 43          	.byte 0x43
 1612 0a2a EE 01 00 00 	.4byte 0x1ee
 1613 0a2e 02          	.byte 0x2
 1614 0a2f 7E          	.byte 0x7e
 1615 0a30 00          	.sleb128 0
 1616 0a31 11          	.uleb128 0x11
 1617 0a32 00 00 00 00 	.4byte .LASF0
 1618 0a36 01          	.byte 0x1
 1619 0a37 43          	.byte 0x43
 1620 0a38 7F 08 00 00 	.4byte 0x87f
 1621 0a3c 02          	.byte 0x2
 1622 0a3d 7E          	.byte 0x7e
 1623 0a3e 0C          	.sleb128 12
 1624 0a3f 00          	.byte 0x0
 1625 0a40 10          	.uleb128 0x10
 1626 0a41 01          	.byte 0x1
 1627 0a42 70 61 72 73 	.asciz "parse_can_message_interface_te_limits"
 1627      65 5F 63 61 
 1627      6E 5F 6D 65 
MPLAB XC16 ASSEMBLY Listing:   			page 37


 1627      73 73 61 67 
 1627      65 5F 69 6E 
 1627      74 65 72 66 
 1627      61 63 65 5F 
 1627      74 65 5F 6C 
 1627      69 6D 69 74 
 1628 0a68 01          	.byte 0x1
 1629 0a69 49          	.byte 0x49
 1630 0a6a 01          	.byte 0x1
 1631 0a6b 00 00 00 00 	.4byte .LFB8
 1632 0a6f 00 00 00 00 	.4byte .LFE8
 1633 0a73 01          	.byte 0x1
 1634 0a74 5E          	.byte 0x5e
 1635 0a75 96 0A 00 00 	.4byte 0xa96
 1636 0a79 11          	.uleb128 0x11
 1637 0a7a 00 00 00 00 	.4byte .LASF3
 1638 0a7e 01          	.byte 0x1
 1639 0a7f 49          	.byte 0x49
 1640 0a80 EE 01 00 00 	.4byte 0x1ee
 1641 0a84 02          	.byte 0x2
 1642 0a85 7E          	.byte 0x7e
 1643 0a86 00          	.sleb128 0
 1644 0a87 11          	.uleb128 0x11
 1645 0a88 00 00 00 00 	.4byte .LASF0
 1646 0a8c 01          	.byte 0x1
 1647 0a8d 49          	.byte 0x49
 1648 0a8e 7F 08 00 00 	.4byte 0x87f
 1649 0a92 02          	.byte 0x2
 1650 0a93 7E          	.byte 0x7e
 1651 0a94 0C          	.sleb128 12
 1652 0a95 00          	.byte 0x0
 1653 0a96 10          	.uleb128 0x10
 1654 0a97 01          	.byte 0x1
 1655 0a98 70 61 72 73 	.asciz "parse_can_message_hard_breaking_thresh"
 1655      65 5F 63 61 
 1655      6E 5F 6D 65 
 1655      73 73 61 67 
 1655      65 5F 68 61 
 1655      72 64 5F 62 
 1655      72 65 61 6B 
 1655      69 6E 67 5F 
 1655      74 68 72 65 
 1656 0abf 01          	.byte 0x1
 1657 0ac0 4F          	.byte 0x4f
 1658 0ac1 01          	.byte 0x1
 1659 0ac2 00 00 00 00 	.4byte .LFB9
 1660 0ac6 00 00 00 00 	.4byte .LFE9
 1661 0aca 01          	.byte 0x1
 1662 0acb 5E          	.byte 0x5e
 1663 0acc ED 0A 00 00 	.4byte 0xaed
 1664 0ad0 11          	.uleb128 0x11
 1665 0ad1 00 00 00 00 	.4byte .LASF3
 1666 0ad5 01          	.byte 0x1
 1667 0ad6 4F          	.byte 0x4f
 1668 0ad7 EE 01 00 00 	.4byte 0x1ee
 1669 0adb 02          	.byte 0x2
 1670 0adc 7E          	.byte 0x7e
MPLAB XC16 ASSEMBLY Listing:   			page 38


 1671 0add 00          	.sleb128 0
 1672 0ade 11          	.uleb128 0x11
 1673 0adf 00 00 00 00 	.4byte .LASF0
 1674 0ae3 01          	.byte 0x1
 1675 0ae4 4F          	.byte 0x4f
 1676 0ae5 7F 08 00 00 	.4byte 0x87f
 1677 0ae9 02          	.byte 0x2
 1678 0aea 7E          	.byte 0x7e
 1679 0aeb 0C          	.sleb128 12
 1680 0aec 00          	.byte 0x0
 1681 0aed 14          	.uleb128 0x14
 1682 0aee 01          	.byte 0x1
 1683 0aef 70 61 72 73 	.asciz "parse_can_interface"
 1683      65 5F 63 61 
 1683      6E 5F 69 6E 
 1683      74 65 72 66 
 1683      61 63 65 00 
 1684 0b03 01          	.byte 0x1
 1685 0b04 57          	.byte 0x57
 1686 0b05 01          	.byte 0x1
 1687 0b06 00 00 00 00 	.4byte .LFB10
 1688 0b0a 00 00 00 00 	.4byte .LFE10
 1689 0b0e 01          	.byte 0x1
 1690 0b0f 5E          	.byte 0x5e
 1691 0b10 11          	.uleb128 0x11
 1692 0b11 00 00 00 00 	.4byte .LASF3
 1693 0b15 01          	.byte 0x1
 1694 0b16 57          	.byte 0x57
 1695 0b17 EE 01 00 00 	.4byte 0x1ee
 1696 0b1b 02          	.byte 0x2
 1697 0b1c 7E          	.byte 0x7e
 1698 0b1d 00          	.sleb128 0
 1699 0b1e 11          	.uleb128 0x11
 1700 0b1f 00 00 00 00 	.4byte .LASF0
 1701 0b23 01          	.byte 0x1
 1702 0b24 57          	.byte 0x57
 1703 0b25 7F 08 00 00 	.4byte 0x87f
 1704 0b29 02          	.byte 0x2
 1705 0b2a 7E          	.byte 0x7e
 1706 0b2b 0C          	.sleb128 12
 1707 0b2c 00          	.byte 0x0
 1708 0b2d 00          	.byte 0x0
 1709                 	.section .debug_abbrev,info
 1710 0000 01          	.uleb128 0x1
 1711 0001 11          	.uleb128 0x11
 1712 0002 01          	.byte 0x1
 1713 0003 25          	.uleb128 0x25
 1714 0004 08          	.uleb128 0x8
 1715 0005 13          	.uleb128 0x13
 1716 0006 0B          	.uleb128 0xb
 1717 0007 03          	.uleb128 0x3
 1718 0008 08          	.uleb128 0x8
 1719 0009 1B          	.uleb128 0x1b
 1720 000a 08          	.uleb128 0x8
 1721 000b 11          	.uleb128 0x11
 1722 000c 01          	.uleb128 0x1
 1723 000d 12          	.uleb128 0x12
MPLAB XC16 ASSEMBLY Listing:   			page 39


 1724 000e 01          	.uleb128 0x1
 1725 000f 10          	.uleb128 0x10
 1726 0010 06          	.uleb128 0x6
 1727 0011 00          	.byte 0x0
 1728 0012 00          	.byte 0x0
 1729 0013 02          	.uleb128 0x2
 1730 0014 24          	.uleb128 0x24
 1731 0015 00          	.byte 0x0
 1732 0016 0B          	.uleb128 0xb
 1733 0017 0B          	.uleb128 0xb
 1734 0018 3E          	.uleb128 0x3e
 1735 0019 0B          	.uleb128 0xb
 1736 001a 03          	.uleb128 0x3
 1737 001b 08          	.uleb128 0x8
 1738 001c 00          	.byte 0x0
 1739 001d 00          	.byte 0x0
 1740 001e 03          	.uleb128 0x3
 1741 001f 16          	.uleb128 0x16
 1742 0020 00          	.byte 0x0
 1743 0021 03          	.uleb128 0x3
 1744 0022 08          	.uleb128 0x8
 1745 0023 3A          	.uleb128 0x3a
 1746 0024 0B          	.uleb128 0xb
 1747 0025 3B          	.uleb128 0x3b
 1748 0026 0B          	.uleb128 0xb
 1749 0027 49          	.uleb128 0x49
 1750 0028 13          	.uleb128 0x13
 1751 0029 00          	.byte 0x0
 1752 002a 00          	.byte 0x0
 1753 002b 04          	.uleb128 0x4
 1754 002c 13          	.uleb128 0x13
 1755 002d 01          	.byte 0x1
 1756 002e 0B          	.uleb128 0xb
 1757 002f 0B          	.uleb128 0xb
 1758 0030 3A          	.uleb128 0x3a
 1759 0031 0B          	.uleb128 0xb
 1760 0032 3B          	.uleb128 0x3b
 1761 0033 0B          	.uleb128 0xb
 1762 0034 01          	.uleb128 0x1
 1763 0035 13          	.uleb128 0x13
 1764 0036 00          	.byte 0x0
 1765 0037 00          	.byte 0x0
 1766 0038 05          	.uleb128 0x5
 1767 0039 0D          	.uleb128 0xd
 1768 003a 00          	.byte 0x0
 1769 003b 03          	.uleb128 0x3
 1770 003c 08          	.uleb128 0x8
 1771 003d 3A          	.uleb128 0x3a
 1772 003e 0B          	.uleb128 0xb
 1773 003f 3B          	.uleb128 0x3b
 1774 0040 0B          	.uleb128 0xb
 1775 0041 49          	.uleb128 0x49
 1776 0042 13          	.uleb128 0x13
 1777 0043 0B          	.uleb128 0xb
 1778 0044 0B          	.uleb128 0xb
 1779 0045 0D          	.uleb128 0xd
 1780 0046 0B          	.uleb128 0xb
MPLAB XC16 ASSEMBLY Listing:   			page 40


 1781 0047 0C          	.uleb128 0xc
 1782 0048 0B          	.uleb128 0xb
 1783 0049 38          	.uleb128 0x38
 1784 004a 0A          	.uleb128 0xa
 1785 004b 00          	.byte 0x0
 1786 004c 00          	.byte 0x0
 1787 004d 06          	.uleb128 0x6
 1788 004e 17          	.uleb128 0x17
 1789 004f 01          	.byte 0x1
 1790 0050 0B          	.uleb128 0xb
 1791 0051 0B          	.uleb128 0xb
 1792 0052 3A          	.uleb128 0x3a
 1793 0053 0B          	.uleb128 0xb
 1794 0054 3B          	.uleb128 0x3b
 1795 0055 0B          	.uleb128 0xb
 1796 0056 01          	.uleb128 0x1
 1797 0057 13          	.uleb128 0x13
 1798 0058 00          	.byte 0x0
 1799 0059 00          	.byte 0x0
 1800 005a 07          	.uleb128 0x7
 1801 005b 0D          	.uleb128 0xd
 1802 005c 00          	.byte 0x0
 1803 005d 49          	.uleb128 0x49
 1804 005e 13          	.uleb128 0x13
 1805 005f 00          	.byte 0x0
 1806 0060 00          	.byte 0x0
 1807 0061 08          	.uleb128 0x8
 1808 0062 0D          	.uleb128 0xd
 1809 0063 00          	.byte 0x0
 1810 0064 03          	.uleb128 0x3
 1811 0065 08          	.uleb128 0x8
 1812 0066 3A          	.uleb128 0x3a
 1813 0067 0B          	.uleb128 0xb
 1814 0068 3B          	.uleb128 0x3b
 1815 0069 0B          	.uleb128 0xb
 1816 006a 49          	.uleb128 0x49
 1817 006b 13          	.uleb128 0x13
 1818 006c 00          	.byte 0x0
 1819 006d 00          	.byte 0x0
 1820 006e 09          	.uleb128 0x9
 1821 006f 0D          	.uleb128 0xd
 1822 0070 00          	.byte 0x0
 1823 0071 49          	.uleb128 0x49
 1824 0072 13          	.uleb128 0x13
 1825 0073 38          	.uleb128 0x38
 1826 0074 0A          	.uleb128 0xa
 1827 0075 00          	.byte 0x0
 1828 0076 00          	.byte 0x0
 1829 0077 0A          	.uleb128 0xa
 1830 0078 0D          	.uleb128 0xd
 1831 0079 00          	.byte 0x0
 1832 007a 03          	.uleb128 0x3
 1833 007b 0E          	.uleb128 0xe
 1834 007c 3A          	.uleb128 0x3a
 1835 007d 0B          	.uleb128 0xb
 1836 007e 3B          	.uleb128 0x3b
 1837 007f 0B          	.uleb128 0xb
MPLAB XC16 ASSEMBLY Listing:   			page 41


 1838 0080 49          	.uleb128 0x49
 1839 0081 13          	.uleb128 0x13
 1840 0082 38          	.uleb128 0x38
 1841 0083 0A          	.uleb128 0xa
 1842 0084 00          	.byte 0x0
 1843 0085 00          	.byte 0x0
 1844 0086 0B          	.uleb128 0xb
 1845 0087 01          	.uleb128 0x1
 1846 0088 01          	.byte 0x1
 1847 0089 49          	.uleb128 0x49
 1848 008a 13          	.uleb128 0x13
 1849 008b 01          	.uleb128 0x1
 1850 008c 13          	.uleb128 0x13
 1851 008d 00          	.byte 0x0
 1852 008e 00          	.byte 0x0
 1853 008f 0C          	.uleb128 0xc
 1854 0090 21          	.uleb128 0x21
 1855 0091 00          	.byte 0x0
 1856 0092 49          	.uleb128 0x49
 1857 0093 13          	.uleb128 0x13
 1858 0094 2F          	.uleb128 0x2f
 1859 0095 0B          	.uleb128 0xb
 1860 0096 00          	.byte 0x0
 1861 0097 00          	.byte 0x0
 1862 0098 0D          	.uleb128 0xd
 1863 0099 04          	.uleb128 0x4
 1864 009a 01          	.byte 0x1
 1865 009b 0B          	.uleb128 0xb
 1866 009c 0B          	.uleb128 0xb
 1867 009d 3A          	.uleb128 0x3a
 1868 009e 0B          	.uleb128 0xb
 1869 009f 3B          	.uleb128 0x3b
 1870 00a0 0B          	.uleb128 0xb
 1871 00a1 01          	.uleb128 0x1
 1872 00a2 13          	.uleb128 0x13
 1873 00a3 00          	.byte 0x0
 1874 00a4 00          	.byte 0x0
 1875 00a5 0E          	.uleb128 0xe
 1876 00a6 28          	.uleb128 0x28
 1877 00a7 00          	.byte 0x0
 1878 00a8 03          	.uleb128 0x3
 1879 00a9 08          	.uleb128 0x8
 1880 00aa 1C          	.uleb128 0x1c
 1881 00ab 0D          	.uleb128 0xd
 1882 00ac 00          	.byte 0x0
 1883 00ad 00          	.byte 0x0
 1884 00ae 0F          	.uleb128 0xf
 1885 00af 0D          	.uleb128 0xd
 1886 00b0 00          	.byte 0x0
 1887 00b1 03          	.uleb128 0x3
 1888 00b2 08          	.uleb128 0x8
 1889 00b3 3A          	.uleb128 0x3a
 1890 00b4 0B          	.uleb128 0xb
 1891 00b5 3B          	.uleb128 0x3b
 1892 00b6 0B          	.uleb128 0xb
 1893 00b7 49          	.uleb128 0x49
 1894 00b8 13          	.uleb128 0x13
MPLAB XC16 ASSEMBLY Listing:   			page 42


 1895 00b9 38          	.uleb128 0x38
 1896 00ba 0A          	.uleb128 0xa
 1897 00bb 00          	.byte 0x0
 1898 00bc 00          	.byte 0x0
 1899 00bd 10          	.uleb128 0x10
 1900 00be 2E          	.uleb128 0x2e
 1901 00bf 01          	.byte 0x1
 1902 00c0 3F          	.uleb128 0x3f
 1903 00c1 0C          	.uleb128 0xc
 1904 00c2 03          	.uleb128 0x3
 1905 00c3 08          	.uleb128 0x8
 1906 00c4 3A          	.uleb128 0x3a
 1907 00c5 0B          	.uleb128 0xb
 1908 00c6 3B          	.uleb128 0x3b
 1909 00c7 0B          	.uleb128 0xb
 1910 00c8 27          	.uleb128 0x27
 1911 00c9 0C          	.uleb128 0xc
 1912 00ca 11          	.uleb128 0x11
 1913 00cb 01          	.uleb128 0x1
 1914 00cc 12          	.uleb128 0x12
 1915 00cd 01          	.uleb128 0x1
 1916 00ce 40          	.uleb128 0x40
 1917 00cf 0A          	.uleb128 0xa
 1918 00d0 01          	.uleb128 0x1
 1919 00d1 13          	.uleb128 0x13
 1920 00d2 00          	.byte 0x0
 1921 00d3 00          	.byte 0x0
 1922 00d4 11          	.uleb128 0x11
 1923 00d5 05          	.uleb128 0x5
 1924 00d6 00          	.byte 0x0
 1925 00d7 03          	.uleb128 0x3
 1926 00d8 0E          	.uleb128 0xe
 1927 00d9 3A          	.uleb128 0x3a
 1928 00da 0B          	.uleb128 0xb
 1929 00db 3B          	.uleb128 0x3b
 1930 00dc 0B          	.uleb128 0xb
 1931 00dd 49          	.uleb128 0x49
 1932 00de 13          	.uleb128 0x13
 1933 00df 02          	.uleb128 0x2
 1934 00e0 0A          	.uleb128 0xa
 1935 00e1 00          	.byte 0x0
 1936 00e2 00          	.byte 0x0
 1937 00e3 12          	.uleb128 0x12
 1938 00e4 0F          	.uleb128 0xf
 1939 00e5 00          	.byte 0x0
 1940 00e6 0B          	.uleb128 0xb
 1941 00e7 0B          	.uleb128 0xb
 1942 00e8 49          	.uleb128 0x49
 1943 00e9 13          	.uleb128 0x13
 1944 00ea 00          	.byte 0x0
 1945 00eb 00          	.byte 0x0
 1946 00ec 13          	.uleb128 0x13
 1947 00ed 05          	.uleb128 0x5
 1948 00ee 00          	.byte 0x0
 1949 00ef 03          	.uleb128 0x3
 1950 00f0 08          	.uleb128 0x8
 1951 00f1 3A          	.uleb128 0x3a
MPLAB XC16 ASSEMBLY Listing:   			page 43


 1952 00f2 0B          	.uleb128 0xb
 1953 00f3 3B          	.uleb128 0x3b
 1954 00f4 0B          	.uleb128 0xb
 1955 00f5 49          	.uleb128 0x49
 1956 00f6 13          	.uleb128 0x13
 1957 00f7 02          	.uleb128 0x2
 1958 00f8 0A          	.uleb128 0xa
 1959 00f9 00          	.byte 0x0
 1960 00fa 00          	.byte 0x0
 1961 00fb 14          	.uleb128 0x14
 1962 00fc 2E          	.uleb128 0x2e
 1963 00fd 01          	.byte 0x1
 1964 00fe 3F          	.uleb128 0x3f
 1965 00ff 0C          	.uleb128 0xc
 1966 0100 03          	.uleb128 0x3
 1967 0101 08          	.uleb128 0x8
 1968 0102 3A          	.uleb128 0x3a
 1969 0103 0B          	.uleb128 0xb
 1970 0104 3B          	.uleb128 0x3b
 1971 0105 0B          	.uleb128 0xb
 1972 0106 27          	.uleb128 0x27
 1973 0107 0C          	.uleb128 0xc
 1974 0108 11          	.uleb128 0x11
 1975 0109 01          	.uleb128 0x1
 1976 010a 12          	.uleb128 0x12
 1977 010b 01          	.uleb128 0x1
 1978 010c 40          	.uleb128 0x40
 1979 010d 0A          	.uleb128 0xa
 1980 010e 00          	.byte 0x0
 1981 010f 00          	.byte 0x0
 1982 0110 00          	.byte 0x0
 1983                 	.section .debug_pubnames,info
 1984 0000 B2 01 00 00 	.4byte 0x1b2
 1985 0004 02 00       	.2byte 0x2
 1986 0006 00 00 00 00 	.4byte .Ldebug_info0
 1987 000a 2E 0B 00 00 	.4byte 0xb2e
 1988 000e 76 07 00 00 	.4byte 0x776
 1989 0012 70 61 72 73 	.asciz "parse_can_message_dcu_toggle"
 1989      65 5F 63 61 
 1989      6E 5F 6D 65 
 1989      73 73 61 67 
 1989      65 5F 64 63 
 1989      75 5F 74 6F 
 1989      67 67 6C 65 
 1989      00 
 1990 002f CF 07 00 00 	.4byte 0x7cf
 1991 0033 70 61 72 73 	.asciz "parse_can_message_debug_toggle"
 1991      65 5F 63 61 
 1991      6E 5F 6D 65 
 1991      73 73 61 67 
 1991      65 5F 64 65 
 1991      62 75 67 5F 
 1991      74 6F 67 67 
 1991      6C 65 00 
 1992 0052 26 08 00 00 	.4byte 0x826
 1993 0056 70 61 72 73 	.asciz "parse_can_message_pedal_thresholds"
 1993      65 5F 63 61 
MPLAB XC16 ASSEMBLY Listing:   			page 44


 1993      6E 5F 6D 65 
 1993      73 73 61 67 
 1993      65 5F 70 65 
 1993      64 61 6C 5F 
 1993      74 68 72 65 
 1993      73 68 6F 6C 
 1993      64 73 00 
 1994 0079 85 08 00 00 	.4byte 0x885
 1995 007d 70 61 72 73 	.asciz "parse_can_message_interface_inverters"
 1995      65 5F 63 61 
 1995      6E 5F 6D 65 
 1995      73 73 61 67 
 1995      65 5F 69 6E 
 1995      74 65 72 66 
 1995      61 63 65 5F 
 1995      69 6E 76 65 
 1995      72 74 65 72 
 1996 00a3 E7 08 00 00 	.4byte 0x8e7
 1997 00a7 70 61 72 73 	.asciz "parse_can_message_interface_arm1"
 1997      65 5F 63 61 
 1997      6E 5F 6D 65 
 1997      73 73 61 67 
 1997      65 5F 69 6E 
 1997      74 65 72 66 
 1997      61 63 65 5F 
 1997      61 72 6D 31 
 1997      00 
 1998 00c8 3F 09 00 00 	.4byte 0x93f
 1999 00cc 70 61 72 73 	.asciz "parse_can_message_set_steer_thresholds"
 1999      65 5F 63 61 
 1999      6E 5F 6D 65 
 1999      73 73 61 67 
 1999      65 5F 73 65 
 1999      74 5F 73 74 
 1999      65 65 72 5F 
 1999      74 68 72 65 
 1999      73 68 6F 6C 
 2000 00f3 96 09 00 00 	.4byte 0x996
 2001 00f7 70 61 72 73 	.asciz "parse_can_message_interface_start_log"
 2001      65 5F 63 61 
 2001      6E 5F 6D 65 
 2001      73 73 61 67 
 2001      65 5F 69 6E 
 2001      74 65 72 66 
 2001      61 63 65 5F 
 2001      73 74 61 72 
 2001      74 5F 6C 6F 
 2002 011d EC 09 00 00 	.4byte 0x9ec
 2003 0121 70 61 72 73 	.asciz "parse_can_message_interface_set_drs"
 2003      65 5F 63 61 
 2003      6E 5F 6D 65 
 2003      73 73 61 67 
 2003      65 5F 69 6E 
 2003      74 65 72 66 
 2003      61 63 65 5F 
 2003      73 65 74 5F 
 2003      64 72 73 00 
MPLAB XC16 ASSEMBLY Listing:   			page 45


 2004 0145 40 0A 00 00 	.4byte 0xa40
 2005 0149 70 61 72 73 	.asciz "parse_can_message_interface_te_limits"
 2005      65 5F 63 61 
 2005      6E 5F 6D 65 
 2005      73 73 61 67 
 2005      65 5F 69 6E 
 2005      74 65 72 66 
 2005      61 63 65 5F 
 2005      74 65 5F 6C 
 2005      69 6D 69 74 
 2006 016f 96 0A 00 00 	.4byte 0xa96
 2007 0173 70 61 72 73 	.asciz "parse_can_message_hard_breaking_thresh"
 2007      65 5F 63 61 
 2007      6E 5F 6D 65 
 2007      73 73 61 67 
 2007      65 5F 68 61 
 2007      72 64 5F 62 
 2007      72 65 61 6B 
 2007      69 6E 67 5F 
 2007      74 68 72 65 
 2008 019a ED 0A 00 00 	.4byte 0xaed
 2009 019e 70 61 72 73 	.asciz "parse_can_interface"
 2009      65 5F 63 61 
 2009      6E 5F 69 6E 
 2009      74 65 72 66 
 2009      61 63 65 00 
 2010 01b2 00 00 00 00 	.4byte 0x0
 2011                 	.section .debug_pubtypes,info
 2012 0000 A1 01 00 00 	.4byte 0x1a1
 2013 0004 02 00       	.2byte 0x2
 2014 0006 00 00 00 00 	.4byte .Ldebug_info0
 2015 000a 2E 0B 00 00 	.4byte 0xb2e
 2016 000e C2 00 00 00 	.4byte 0xc2
 2017 0012 69 6E 74 31 	.asciz "int16_t"
 2017      36 5F 74 00 
 2018 001a F5 00 00 00 	.4byte 0xf5
 2019 001e 75 69 6E 74 	.asciz "uint8_t"
 2019      38 5F 74 00 
 2020 0026 15 01 00 00 	.4byte 0x115
 2021 002a 75 69 6E 74 	.asciz "uint16_t"
 2021      31 36 5F 74 
 2021      00 
 2022 0033 EE 01 00 00 	.4byte 0x1ee
 2023 0037 43 41 4E 64 	.asciz "CANdata"
 2023      61 74 61 00 
 2024 003f 4D 02 00 00 	.4byte 0x24d
 2025 0043 49 4E 54 45 	.asciz "INTERFACE_MSG_PEDAL_THRESHOLDS"
 2025      52 46 41 43 
 2025      45 5F 4D 53 
 2025      47 5F 50 45 
 2025      44 41 4C 5F 
 2025      54 48 52 45 
 2025      53 48 4F 4C 
 2025      44 53 00 
 2026 0062 A6 02 00 00 	.4byte 0x2a6
 2027 0066 49 4E 54 45 	.asciz "INTERFACE_MSG_STEER_THRESHOLDS"
 2027      52 46 41 43 
MPLAB XC16 ASSEMBLY Listing:   			page 46


 2027      45 5F 4D 53 
 2027      47 5F 53 54 
 2027      45 45 52 5F 
 2027      54 48 52 45 
 2027      53 48 4F 4C 
 2027      44 53 00 
 2028 0085 36 03 00 00 	.4byte 0x336
 2029 0089 44 43 55 5F 	.asciz "DCU_TOGGLE"
 2029      54 4F 47 47 
 2029      4C 45 00 
 2030 0094 96 03 00 00 	.4byte 0x396
 2031 0098 44 45 42 55 	.asciz "DEBUG_TOGGLE"
 2031      47 5F 54 4F 
 2031      47 47 4C 45 
 2031      00 
 2032 00a5 C2 03 00 00 	.4byte 0x3c2
 2033 00a9 49 4E 54 45 	.asciz "INTERFACE_MSG_DCU_TOGGLE"
 2033      52 46 41 43 
 2033      45 5F 4D 53 
 2033      47 5F 44 43 
 2033      55 5F 54 4F 
 2033      47 47 4C 45 
 2033      00 
 2034 00c2 FC 03 00 00 	.4byte 0x3fc
 2035 00c6 49 4E 54 45 	.asciz "INTERFACE_MSG_DEBUG_TOGGLE"
 2035      52 46 41 43 
 2035      45 5F 4D 53 
 2035      47 5F 44 45 
 2035      42 55 47 5F 
 2035      54 4F 47 47 
 2035      4C 45 00 
 2036 00e1 7E 04 00 00 	.4byte 0x47e
 2037 00e5 49 4E 54 45 	.asciz "INTERFACE_MSG_INVERTERS"
 2037      52 46 41 43 
 2037      45 5F 4D 53 
 2037      47 5F 49 4E 
 2037      56 45 52 54 
 2037      45 52 53 00 
 2038 00fd EA 04 00 00 	.4byte 0x4ea
 2039 0101 49 4E 54 45 	.asciz "INTERFACE_MSG_ARM1"
 2039      52 46 41 43 
 2039      45 5F 4D 53 
 2039      47 5F 41 52 
 2039      4D 31 00 
 2040 0114 6B 05 00 00 	.4byte 0x56b
 2041 0118 49 4E 54 45 	.asciz "INTERFACE_MSG_START_LOG_DATE"
 2041      52 46 41 43 
 2041      45 5F 4D 53 
 2041      47 5F 53 54 
 2041      41 52 54 5F 
 2041      4C 4F 47 5F 
 2041      44 41 54 45 
 2041      00 
 2042 0135 AA 05 00 00 	.4byte 0x5aa
 2043 0139 49 4E 54 45 	.asciz "INTERFACE_MSG_SET_DRS"
 2043      52 46 41 43 
 2043      45 5F 4D 53 
MPLAB XC16 ASSEMBLY Listing:   			page 47


 2043      47 5F 53 45 
 2043      54 5F 44 52 
 2043      53 00 
 2044 014f F1 05 00 00 	.4byte 0x5f1
 2045 0153 49 4E 54 45 	.asciz "INTERFACE_MSG_TE_LIMIT"
 2045      52 46 41 43 
 2045      45 5F 4D 53 
 2045      47 5F 54 45 
 2045      5F 4C 49 4D 
 2045      49 54 00 
 2046 016a 3C 06 00 00 	.4byte 0x63c
 2047 016e 49 4E 54 45 	.asciz "INTERFACE_MSG_HARD_BREAKING"
 2047      52 46 41 43 
 2047      45 5F 4D 53 
 2047      47 5F 48 41 
 2047      52 44 5F 42 
 2047      52 45 41 4B 
 2047      49 4E 47 00 
 2048 018a 3E 07 00 00 	.4byte 0x73e
 2049 018e 49 4E 54 45 	.asciz "INTERFACE_CAN_Data"
 2049      52 46 41 43 
 2049      45 5F 43 41 
 2049      4E 5F 44 61 
 2049      74 61 00 
 2050 01a1 00 00 00 00 	.4byte 0x0
 2051                 	.section .debug_aranges,info
 2052 0000 14 00 00 00 	.4byte 0x14
 2053 0004 02 00       	.2byte 0x2
 2054 0006 00 00 00 00 	.4byte .Ldebug_info0
 2055 000a 04          	.byte 0x4
 2056 000b 00          	.byte 0x0
 2057 000c 00 00       	.2byte 0x0
 2058 000e 00 00       	.2byte 0x0
 2059 0010 00 00 00 00 	.4byte 0x0
 2060 0014 00 00 00 00 	.4byte 0x0
 2061                 	.section .debug_str,info
 2062                 	.LASF0:
 2063 0000 64 61 74 61 	.asciz "data"
 2063      00 
 2064                 	.LASF2:
 2065 0005 64 65 62 75 	.asciz "debug_toggle"
 2065      67 5F 74 6F 
 2065      67 67 6C 65 
 2065      00 
 2066                 	.LASF3:
 2067 0012 6D 65 73 73 	.asciz "message"
 2067      61 67 65 00 
 2068                 	.LASF1:
 2069 001a 64 63 75 5F 	.asciz "dcu_toggle"
 2069      74 6F 67 67 
 2069      6C 65 00 
 2070                 	.section .text,code
 2071              	
 2072              	
 2073              	
 2074              	.section __c30_info,info,bss
 2075                 	__psv_trap_errata:
MPLAB XC16 ASSEMBLY Listing:   			page 48


 2076                 	
 2077                 	.section __c30_signature,info,data
 2078 0000 01 00       	.word 0x0001
 2079 0002 00 00       	.word 0x0000
 2080 0004 00 00       	.word 0x0000
 2081                 	
 2082                 	
 2083                 	
 2084                 	.set ___PA___,0
 2085                 	.end
MPLAB XC16 ASSEMBLY Listing:   			page 49


DEFINED SYMBOLS
                            *ABS*:00000000 /home/user/Documents/FST/Programming/project_sub-zero/lib/can-ids/Devices/INTERFACE_CAN.c
                            *ABS*:00000001 __33EP256MU806
                            *ABS*:00000001 __dsPIC33E
                            *ABS*:00000001 __HAS_DSP
                            *ABS*:00000001 __HAS_DMAV2
                            *ABS*:00000001 __HAS_CODEGUARD
                            *ABS*:00000001 __HAS_PMP_ENHANCED
                            *ABS*:00000001 __HAS_EDS
                            *ABS*:00000001 __HAS_EP
                            *ABS*:00000011 __TARGET_DIVIDE_CYCLES
                            *ABS*:00000001 __C30ELF
    {standard input}:14     .text:00000000 _parse_can_message_dcu_toggle
    {standard input}:18     *ABS*:00000000 ___PA___
    {standard input}:41     .text:00000018 _parse_can_message_debug_toggle
    {standard input}:66     .text:0000002e _parse_can_message_pedal_thresholds
    {standard input}:91     .text:00000044 _parse_can_message_interface_inverters
    {standard input}:143    .text:00000076 _parse_can_message_interface_arm1
    {standard input}:195    .text:000000b4 _parse_can_message_set_steer_thresholds
    {standard input}:226    .text:000000d2 _parse_can_message_interface_start_log
    {standard input}:289    .text:00000126 _parse_can_message_interface_set_drs
    {standard input}:320    .text:00000144 _parse_can_message_interface_te_limits
    {standard input}:361    .text:0000016e _parse_can_message_hard_breaking_thresh
    {standard input}:392    .text:0000018c _parse_can_interface
    {standard input}:412    *ABS*:00000000 ___BP___
    {standard input}:2075   __c30_info:00000000 __psv_trap_errata
    {standard input}:19     .text:00000000 .L0
                            .text:00000290 .L25
                            .text:0000021e .L18
                            .text:000001d8 .L23
                            .text:00000204 .L15
                            .text:000001ca .L24
                            .text:00000270 .L14
                            .text:00000290 .L11
                            .text:0000020c .L16
                            .text:00000216 .L17
                            .text:000001f2 .L20
                            .text:00000228 .L19
                            .text:0000023a .L21
                            .text:00000250 .L22
                    .debug_abbrev:00000000 .Ldebug_abbrev0
                            .text:00000000 .Ltext0
                            .text:00000298 .Letext0
                      .debug_line:00000000 .Ldebug_line0
                       .debug_str:00000000 .LASF0
                       .debug_str:0000001a .LASF1
                       .debug_str:00000005 .LASF2
                            .text:00000000 .LFB0
                            .text:00000018 .LFE0
                            .text:00000018 .LFB1
                            .text:0000002e .LFE1
                            .text:0000002e .LFB2
                            .text:00000044 .LFE2
                       .debug_str:00000012 .LASF3
                            .text:00000044 .LFB3
                            .text:00000076 .LFE3
                            .text:00000076 .LFB4
MPLAB XC16 ASSEMBLY Listing:   			page 50


                            .text:000000b4 .LFE4
                            .text:000000b4 .LFB5
                            .text:000000d2 .LFE5
                            .text:000000d2 .LFB6
                            .text:00000126 .LFE6
                            .text:00000126 .LFB7
                            .text:00000144 .LFE7
                            .text:00000144 .LFB8
                            .text:0000016e .LFE8
                            .text:0000016e .LFB9
                            .text:0000018c .LFE9
                            .text:0000018c .LFB10
                            .text:00000298 .LFE10
                     .debug_frame:00000000 .Lframe0
                      .debug_info:00000000 .Ldebug_info0

UNDEFINED SYMBOLS
CORCON
___floatunsisf
___divsf3

EQUATE SYMBOLS
/home/user/Documents/FST/Programming/project_sub-zero/lib/can-ids/Devices/INTERFACE_CAN.c = 0x0
                  __33EP256MU806 = 0x1
                      __dsPIC33E = 0x1
                       __HAS_DSP = 0x1
                     __HAS_DMAV2 = 0x1
                 __HAS_CODEGUARD = 0x1
              __HAS_PMP_ENHANCED = 0x1
                       __HAS_EDS = 0x1
                        __HAS_EP = 0x1
          __TARGET_DIVIDE_CYCLES = 0x11
                        __C30ELF = 0x1
                        ___PA___ = 0x0
                        ___BP___ = 0x0
