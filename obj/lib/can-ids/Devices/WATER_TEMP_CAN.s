MPLAB XC16 ASSEMBLY Listing:   			page 1


   1              	.file "/home/user/Documents/FST/Programming/project_sub-zero/lib/can-ids/Devices/WATER_TEMP_CAN.c"
   2              	.section .debug_abbrev,info
   3                 	.Ldebug_abbrev0:
   4                 	.section .debug_info,info
   5                 	.Ldebug_info0:
   6                 	.section .debug_line,info
   7                 	.Ldebug_line0:
   8 0000 E1 00 00 00 	.section .text,code
   8      02 00 A8 00 
   8      00 00 01 01 
   8      FB 0E 0A 00 
   8      01 01 01 01 
   8      00 00 00 01 
   8      6C 69 62 2F 
   8      63 61 6E 2D 
   8      69 64 73 2F 
   9              	.Ltext0:
  10              	.section .text,code
  11              	.align 2
  12              	.global _parse_can_message_wt_temperature
  13              	.type _parse_can_message_wt_temperature,@function
  14              	_parse_can_message_wt_temperature:
  15              	.LFB0:
  16              	.file 1 "lib/can-ids/Devices/WATER_TEMP_CAN.c"
   1:lib/can-ids/Devices/WATER_TEMP_CAN.c **** #include <stdint.h>
   2:lib/can-ids/Devices/WATER_TEMP_CAN.c **** #include "can-ids/CAN_IDs.h"
   3:lib/can-ids/Devices/WATER_TEMP_CAN.c **** #include "can-ids/Devices/WATER_TEMP_CAN.h"
   4:lib/can-ids/Devices/WATER_TEMP_CAN.c **** 
   5:lib/can-ids/Devices/WATER_TEMP_CAN.c **** void parse_can_message_wt_temperature(CANdata msg, WT_Temperature *temperature) {
  17              	.loc 1 5 0
  18              	.set ___PA___,1
  19 000000  0E 00 FA 	lnk #14
  20              	.LCFI0:
  21 000002  88 1F 78 	mov w8,[w15++]
  22              	.LCFI1:
  23              	.loc 1 5 0
  24 000004  00 0F 78 	mov w0,[w14]
  25 000006  66 07 98 	mov w6,[w14+12]
  26 000008  22 07 98 	mov w2,[w14+4]
  27 00000a  33 07 98 	mov w3,[w14+6]
  28 00000c  44 07 98 	mov w4,[w14+8]
  29 00000e  55 07 98 	mov w5,[w14+10]
   6:lib/can-ids/Devices/WATER_TEMP_CAN.c **** 	temperature->timestamp = msg.data[0];
  30              	.loc 1 6 0
  31 000010  2E 04 90 	mov [w14+4],w8
  32 000012  EE 03 90 	mov [w14+12],w7
   7:lib/can-ids/Devices/WATER_TEMP_CAN.c **** 	temperature->temp1 = msg.data[1];
  33              	.loc 1 7 0
  34 000014  3E 03 90 	mov [w14+6],w6
  35 000016  EE 02 90 	mov [w14+12],w5
   8:lib/can-ids/Devices/WATER_TEMP_CAN.c **** 	temperature->temp2 = msg.data[2];
  36              	.loc 1 8 0
  37 000018  4E 02 90 	mov [w14+8],w4
  38 00001a  EE 01 90 	mov [w14+12],w3
   9:lib/can-ids/Devices/WATER_TEMP_CAN.c **** 	temperature->temp3 = msg.data[3];
  39              	.loc 1 9 0
  40 00001c  5E 01 90 	mov [w14+10],w2
MPLAB XC16 ASSEMBLY Listing:   			page 2


  41 00001e  6E 00 90 	mov [w14+12],w0
  42              	.loc 1 5 0
  43 000020  11 07 98 	mov w1,[w14+2]
  44              	.loc 1 6 0
  45 000022  88 0B 78 	mov w8,[w7]
  46              	.loc 1 7 0
  47 000024  96 02 98 	mov w6,[w5+2]
  48              	.loc 1 8 0
  49 000026  A4 01 98 	mov w4,[w3+4]
  50              	.loc 1 9 0
  51 000028  32 00 98 	mov w2,[w0+6]
  10:lib/can-ids/Devices/WATER_TEMP_CAN.c **** }
  52              	.loc 1 10 0
  53 00002a  4F 04 78 	mov [--w15],w8
  54 00002c  8E 07 78 	mov w14,w15
  55 00002e  4F 07 78 	mov [--w15],w14
  56 000030  00 40 A9 	bclr CORCON,#2
  57 000032  00 00 06 	return 
  58              	.set ___PA___,0
  59              	.LFE0:
  60              	.size _parse_can_message_wt_temperature,.-_parse_can_message_wt_temperature
  61              	.align 2
  62              	.global _parse_can_message_wt_flow_rate
  63              	.type _parse_can_message_wt_flow_rate,@function
  64              	_parse_can_message_wt_flow_rate:
  65              	.LFB1:
  11:lib/can-ids/Devices/WATER_TEMP_CAN.c **** 
  12:lib/can-ids/Devices/WATER_TEMP_CAN.c **** void parse_can_message_wt_flow_rate(CANdata msg, WT_Flow_Rate *flow_rate) {
  66              	.loc 1 12 0
  67              	.set ___PA___,1
  68 000034  0E 00 FA 	lnk #14
  69              	.LCFI2:
  70              	.loc 1 12 0
  71 000036  00 0F 78 	mov w0,[w14]
  72 000038  66 07 98 	mov w6,[w14+12]
  73 00003a  22 07 98 	mov w2,[w14+4]
  74 00003c  33 07 98 	mov w3,[w14+6]
  13:lib/can-ids/Devices/WATER_TEMP_CAN.c **** 	flow_rate->flow_rate_1 = msg.data[0];
  75              	.loc 1 13 0
  76 00003e  2E 03 90 	mov [w14+4],w6
  77 000040  EE 01 90 	mov [w14+12],w3
  14:lib/can-ids/Devices/WATER_TEMP_CAN.c **** 	flow_rate->flow_rate_2 = msg.data[1];
  78              	.loc 1 14 0
  79 000042  3E 01 90 	mov [w14+6],w2
  80 000044  6E 00 90 	mov [w14+12],w0
  81              	.loc 1 12 0
  82 000046  11 07 98 	mov w1,[w14+2]
  83 000048  44 07 98 	mov w4,[w14+8]
  84 00004a  55 07 98 	mov w5,[w14+10]
  85              	.loc 1 13 0
  86 00004c  86 09 78 	mov w6,[w3]
  87              	.loc 1 14 0
  88 00004e  12 00 98 	mov w2,[w0+2]
  15:lib/can-ids/Devices/WATER_TEMP_CAN.c **** }
  89              	.loc 1 15 0
  90 000050  8E 07 78 	mov w14,w15
  91 000052  4F 07 78 	mov [--w15],w14
MPLAB XC16 ASSEMBLY Listing:   			page 3


  92 000054  00 40 A9 	bclr CORCON,#2
  93 000056  00 00 06 	return 
  94              	.set ___PA___,0
  95              	.LFE1:
  96              	.size _parse_can_message_wt_flow_rate,.-_parse_can_message_wt_flow_rate
  97              	.align 2
  98              	.global _parse_can_message_wt
  99              	.type _parse_can_message_wt,@function
 100              	_parse_can_message_wt:
 101              	.LFB2:
  16:lib/can-ids/Devices/WATER_TEMP_CAN.c **** 
  17:lib/can-ids/Devices/WATER_TEMP_CAN.c **** void parse_can_message_wt(CANdata msg, WT_Data *data) {
 102              	.loc 1 17 0
 103              	.set ___PA___,1
 104 000058  0E 00 FA 	lnk #14
 105              	.LCFI3:
 106              	.loc 1 17 0
 107 00005a  00 0F 78 	mov w0,[w14]
 108 00005c  11 07 98 	mov w1,[w14+2]
  18:lib/can-ids/Devices/WATER_TEMP_CAN.c **** 	switch(msg.msg_id){
 109              	.loc 1 18 0
 110 00005e  1E 00 78 	mov [w14],w0
 111              	.loc 1 17 0
 112 000060  22 07 98 	mov w2,[w14+4]
 113              	.loc 1 18 0
 114 000062  45 00 DE 	lsr w0,#5,w0
 115              	.loc 1 17 0
 116 000064  33 07 98 	mov w3,[w14+6]
 117              	.loc 1 18 0
 118 000066  F0 43 B2 	and.b #63,w0
 119              	.loc 1 17 0
 120 000068  44 07 98 	mov w4,[w14+8]
 121 00006a  55 07 98 	mov w5,[w14+10]
 122 00006c  66 07 98 	mov w6,[w14+12]
 123              	.loc 1 18 0
 124 00006e  00 80 FB 	ze w0,w0
 125 000070  81 03 20 	mov #56,w1
 126 000072  81 0F 50 	sub w0,w1,[w15]
 127              	.set ___BP___,0
 128 000074  00 00 32 	bra z,.L6
 129 000076  91 03 20 	mov #57,w1
 130 000078  81 0F 50 	sub w0,w1,[w15]
 131              	.set ___BP___,0
 132 00007a  00 00 32 	bra z,.L7
 133 00007c  61 03 20 	mov #54,w1
 134 00007e  81 0F 50 	sub w0,w1,[w15]
 135              	.set ___BP___,0
 136 000080  00 00 32 	bra z,.L5
 137 000082  00 00 37 	bra .L3
 138              	.L7:
  19:lib/can-ids/Devices/WATER_TEMP_CAN.c **** 		case MSG_ID_WATER_TEMP_TEMPERATURE1:
  20:lib/can-ids/Devices/WATER_TEMP_CAN.c **** 			parse_can_message_wt_temperature(msg,&(data->temperature1));
 139              	.loc 1 20 0
 140 000084  6E 00 90 	mov [w14+12],w0
 141 000086  9E 00 90 	mov [w14+2],w1
 142 000088  00 03 78 	mov w0,w6
 143 00008a  1E 00 78 	mov [w14],w0
MPLAB XC16 ASSEMBLY Listing:   			page 4


 144 00008c  2E 01 90 	mov [w14+4],w2
 145 00008e  BE 01 90 	mov [w14+6],w3
 146 000090  4E 02 90 	mov [w14+8],w4
 147 000092  DE 02 90 	mov [w14+10],w5
 148 000094  00 00 07 	rcall _parse_can_message_wt_temperature
  21:lib/can-ids/Devices/WATER_TEMP_CAN.c **** 			break;
 149              	.loc 1 21 0
 150 000096  00 00 37 	bra .L3
 151              	.L6:
  22:lib/can-ids/Devices/WATER_TEMP_CAN.c **** 		case MSG_ID_WATER_TEMP_TEMPERATURE2:
  23:lib/can-ids/Devices/WATER_TEMP_CAN.c **** 			parse_can_message_wt_temperature(msg,&(data->temperature2));
 152              	.loc 1 23 0
 153 000098  00 00 00 	nop 
 154 00009a  6E 00 90 	mov [w14+12],w0
 155 00009c  00 00 00 	nop 
 156 00009e  9E 00 90 	mov [w14+2],w1
 157 0000a0  68 01 40 	add w0,#8,w2
 158 0000a2  1E 00 78 	mov [w14],w0
 159 0000a4  02 03 78 	mov w2,w6
 160 0000a6  2E 01 90 	mov [w14+4],w2
 161 0000a8  BE 01 90 	mov [w14+6],w3
 162 0000aa  4E 02 90 	mov [w14+8],w4
 163 0000ac  DE 02 90 	mov [w14+10],w5
 164 0000ae  00 00 07 	rcall _parse_can_message_wt_temperature
  24:lib/can-ids/Devices/WATER_TEMP_CAN.c **** 			break;
 165              	.loc 1 24 0
 166 0000b0  00 00 37 	bra .L3
 167              	.L5:
  25:lib/can-ids/Devices/WATER_TEMP_CAN.c **** 		case MSG_ID_WATER_TEMP_FLOW_RATE:
  26:lib/can-ids/Devices/WATER_TEMP_CAN.c **** 			parse_can_message_wt_flow_rate(msg,&(data->flow_rate));
 168              	.loc 1 26 0
 169 0000b2  00 00 00 	nop 
 170 0000b4  6E 00 90 	mov [w14+12],w0
 171 0000b6  00 00 00 	nop 
 172 0000b8  9E 00 90 	mov [w14+2],w1
 173 0000ba  70 01 40 	add w0,#16,w2
 174 0000bc  1E 00 78 	mov [w14],w0
 175 0000be  02 03 78 	mov w2,w6
 176 0000c0  2E 01 90 	mov [w14+4],w2
 177 0000c2  BE 01 90 	mov [w14+6],w3
 178 0000c4  4E 02 90 	mov [w14+8],w4
 179 0000c6  DE 02 90 	mov [w14+10],w5
 180 0000c8  00 00 07 	rcall _parse_can_message_wt_flow_rate
 181              	.L3:
  27:lib/can-ids/Devices/WATER_TEMP_CAN.c **** 			break;
  28:lib/can-ids/Devices/WATER_TEMP_CAN.c **** 	}
  29:lib/can-ids/Devices/WATER_TEMP_CAN.c **** }
 182              	.loc 1 29 0
 183 0000ca  8E 07 78 	mov w14,w15
 184 0000cc  4F 07 78 	mov [--w15],w14
 185 0000ce  00 40 A9 	bclr CORCON,#2
 186 0000d0  00 00 06 	return 
 187              	.set ___PA___,0
 188              	.LFE2:
 189              	.size _parse_can_message_wt,.-_parse_can_message_wt
 190              	.section .debug_frame,info
 191                 	.Lframe0:
MPLAB XC16 ASSEMBLY Listing:   			page 5


 192 0000 10 00 00 00 	.4byte .LECIE0-.LSCIE0
 193                 	.LSCIE0:
 194 0004 FF FF FF FF 	.4byte 0xffffffff
 195 0008 01          	.byte 0x1
 196 0009 00          	.byte 0
 197 000a 01          	.uleb128 0x1
 198 000b 02          	.sleb128 2
 199 000c 25          	.byte 0x25
 200 000d 12          	.byte 0x12
 201 000e 0F          	.uleb128 0xf
 202 000f 7E          	.sleb128 -2
 203 0010 09          	.byte 0x9
 204 0011 25          	.uleb128 0x25
 205 0012 0F          	.uleb128 0xf
 206 0013 00          	.align 4
 207                 	.LECIE0:
 208                 	.LSFDE0:
 209 0014 1E 00 00 00 	.4byte .LEFDE0-.LASFDE0
 210                 	.LASFDE0:
 211 0018 00 00 00 00 	.4byte .Lframe0
 212 001c 00 00 00 00 	.4byte .LFB0
 213 0020 34 00 00 00 	.4byte .LFE0-.LFB0
 214 0024 04          	.byte 0x4
 215 0025 02 00 00 00 	.4byte .LCFI0-.LFB0
 216 0029 13          	.byte 0x13
 217 002a 7D          	.sleb128 -3
 218 002b 0D          	.byte 0xd
 219 002c 0E          	.uleb128 0xe
 220 002d 8E          	.byte 0x8e
 221 002e 02          	.uleb128 0x2
 222 002f 04          	.byte 0x4
 223 0030 02 00 00 00 	.4byte .LCFI1-.LCFI0
 224 0034 88          	.byte 0x88
 225 0035 0A          	.uleb128 0xa
 226                 	.align 4
 227                 	.LEFDE0:
 228                 	.LSFDE2:
 229 0036 18 00 00 00 	.4byte .LEFDE2-.LASFDE2
 230                 	.LASFDE2:
 231 003a 00 00 00 00 	.4byte .Lframe0
 232 003e 00 00 00 00 	.4byte .LFB1
 233 0042 24 00 00 00 	.4byte .LFE1-.LFB1
 234 0046 04          	.byte 0x4
 235 0047 02 00 00 00 	.4byte .LCFI2-.LFB1
 236 004b 13          	.byte 0x13
 237 004c 7D          	.sleb128 -3
 238 004d 0D          	.byte 0xd
 239 004e 0E          	.uleb128 0xe
 240 004f 8E          	.byte 0x8e
 241 0050 02          	.uleb128 0x2
 242 0051 00          	.align 4
 243                 	.LEFDE2:
 244                 	.LSFDE4:
 245 0052 18 00 00 00 	.4byte .LEFDE4-.LASFDE4
 246                 	.LASFDE4:
 247 0056 00 00 00 00 	.4byte .Lframe0
 248 005a 00 00 00 00 	.4byte .LFB2
MPLAB XC16 ASSEMBLY Listing:   			page 6


 249 005e 7A 00 00 00 	.4byte .LFE2-.LFB2
 250 0062 04          	.byte 0x4
 251 0063 02 00 00 00 	.4byte .LCFI3-.LFB2
 252 0067 13          	.byte 0x13
 253 0068 7D          	.sleb128 -3
 254 0069 0D          	.byte 0xd
 255 006a 0E          	.uleb128 0xe
 256 006b 8E          	.byte 0x8e
 257 006c 02          	.uleb128 0x2
 258 006d 00          	.align 4
 259                 	.LEFDE4:
 260                 	.section .text,code
 261              	.Letext0:
 262              	.file 2 "lib/can-ids/CAN_IDs.h"
 263              	.file 3 "/opt/microchip/xc16/v1.36/bin/bin/../../include/lega-c/stdint.h"
 264              	.file 4 "lib/can-ids/Devices/WATER_TEMP_CAN.h"
 265              	.section .debug_info,info
 266 0000 DE 03 00 00 	.4byte 0x3de
 267 0004 02 00       	.2byte 0x2
 268 0006 00 00 00 00 	.4byte .Ldebug_abbrev0
 269 000a 04          	.byte 0x4
 270 000b 01          	.uleb128 0x1
 271 000c 47 4E 55 20 	.asciz "GNU C 4.5.1 (XC16, Microchip v1.36) (A) Build date: Dec  5 2018"
 271      43 20 34 2E 
 271      35 2E 31 20 
 271      28 58 43 31 
 271      36 2C 20 4D 
 271      69 63 72 6F 
 271      63 68 69 70 
 271      20 76 31 2E 
 271      33 36 29 20 
 272 004c 01          	.byte 0x1
 273 004d 6C 69 62 2F 	.asciz "lib/can-ids/Devices/WATER_TEMP_CAN.c"
 273      63 61 6E 2D 
 273      69 64 73 2F 
 273      44 65 76 69 
 273      63 65 73 2F 
 273      57 41 54 45 
 273      52 5F 54 45 
 273      4D 50 5F 43 
 273      41 4E 2E 63 
 274 0072 2F 68 6F 6D 	.asciz "/home/user/Documents/FST/Programming/project_sub-zero"
 274      65 2F 75 73 
 274      65 72 2F 44 
 274      6F 63 75 6D 
 274      65 6E 74 73 
 274      2F 46 53 54 
 274      2F 50 72 6F 
 274      67 72 61 6D 
 274      6D 69 6E 67 
 275 00a8 00 00 00 00 	.4byte .Ltext0
 276 00ac 00 00 00 00 	.4byte .Letext0
 277 00b0 00 00 00 00 	.4byte .Ldebug_line0
 278 00b4 02          	.uleb128 0x2
 279 00b5 01          	.byte 0x1
 280 00b6 06          	.byte 0x6
 281 00b7 73 69 67 6E 	.asciz "signed char"
MPLAB XC16 ASSEMBLY Listing:   			page 7


 281      65 64 20 63 
 281      68 61 72 00 
 282 00c3 02          	.uleb128 0x2
 283 00c4 02          	.byte 0x2
 284 00c5 05          	.byte 0x5
 285 00c6 69 6E 74 00 	.asciz "int"
 286 00ca 02          	.uleb128 0x2
 287 00cb 04          	.byte 0x4
 288 00cc 05          	.byte 0x5
 289 00cd 6C 6F 6E 67 	.asciz "long int"
 289      20 69 6E 74 
 289      00 
 290 00d6 02          	.uleb128 0x2
 291 00d7 08          	.byte 0x8
 292 00d8 05          	.byte 0x5
 293 00d9 6C 6F 6E 67 	.asciz "long long int"
 293      20 6C 6F 6E 
 293      67 20 69 6E 
 293      74 00 
 294 00e7 02          	.uleb128 0x2
 295 00e8 01          	.byte 0x1
 296 00e9 08          	.byte 0x8
 297 00ea 75 6E 73 69 	.asciz "unsigned char"
 297      67 6E 65 64 
 297      20 63 68 61 
 297      72 00 
 298 00f8 03          	.uleb128 0x3
 299 00f9 75 69 6E 74 	.asciz "uint16_t"
 299      31 36 5F 74 
 299      00 
 300 0102 03          	.byte 0x3
 301 0103 31          	.byte 0x31
 302 0104 08 01 00 00 	.4byte 0x108
 303 0108 02          	.uleb128 0x2
 304 0109 02          	.byte 0x2
 305 010a 07          	.byte 0x7
 306 010b 75 6E 73 69 	.asciz "unsigned int"
 306      67 6E 65 64 
 306      20 69 6E 74 
 306      00 
 307 0118 02          	.uleb128 0x2
 308 0119 04          	.byte 0x4
 309 011a 07          	.byte 0x7
 310 011b 6C 6F 6E 67 	.asciz "long unsigned int"
 310      20 75 6E 73 
 310      69 67 6E 65 
 310      64 20 69 6E 
 310      74 00 
 311 012d 02          	.uleb128 0x2
 312 012e 08          	.byte 0x8
 313 012f 07          	.byte 0x7
 314 0130 6C 6F 6E 67 	.asciz "long long unsigned int"
 314      20 6C 6F 6E 
 314      67 20 75 6E 
 314      73 69 67 6E 
 314      65 64 20 69 
 314      6E 74 00 
MPLAB XC16 ASSEMBLY Listing:   			page 8


 315 0147 04          	.uleb128 0x4
 316 0148 02          	.byte 0x2
 317 0149 02          	.byte 0x2
 318 014a 12          	.byte 0x12
 319 014b 78 01 00 00 	.4byte 0x178
 320 014f 05          	.uleb128 0x5
 321 0150 64 65 76 5F 	.asciz "dev_id"
 321      69 64 00 
 322 0157 02          	.byte 0x2
 323 0158 13          	.byte 0x13
 324 0159 F8 00 00 00 	.4byte 0xf8
 325 015d 02          	.byte 0x2
 326 015e 05          	.byte 0x5
 327 015f 0B          	.byte 0xb
 328 0160 02          	.byte 0x2
 329 0161 23          	.byte 0x23
 330 0162 00          	.uleb128 0x0
 331 0163 05          	.uleb128 0x5
 332 0164 6D 73 67 5F 	.asciz "msg_id"
 332      69 64 00 
 333 016b 02          	.byte 0x2
 334 016c 16          	.byte 0x16
 335 016d F8 00 00 00 	.4byte 0xf8
 336 0171 02          	.byte 0x2
 337 0172 06          	.byte 0x6
 338 0173 05          	.byte 0x5
 339 0174 02          	.byte 0x2
 340 0175 23          	.byte 0x23
 341 0176 00          	.uleb128 0x0
 342 0177 00          	.byte 0x0
 343 0178 06          	.uleb128 0x6
 344 0179 02          	.byte 0x2
 345 017a 02          	.byte 0x2
 346 017b 11          	.byte 0x11
 347 017c 91 01 00 00 	.4byte 0x191
 348 0180 07          	.uleb128 0x7
 349 0181 47 01 00 00 	.4byte 0x147
 350 0185 08          	.uleb128 0x8
 351 0186 73 69 64 00 	.asciz "sid"
 352 018a 02          	.byte 0x2
 353 018b 18          	.byte 0x18
 354 018c F8 00 00 00 	.4byte 0xf8
 355 0190 00          	.byte 0x0
 356 0191 04          	.uleb128 0x4
 357 0192 0C          	.byte 0xc
 358 0193 02          	.byte 0x2
 359 0194 10          	.byte 0x10
 360 0195 C2 01 00 00 	.4byte 0x1c2
 361 0199 09          	.uleb128 0x9
 362 019a 78 01 00 00 	.4byte 0x178
 363 019e 02          	.byte 0x2
 364 019f 23          	.byte 0x23
 365 01a0 00          	.uleb128 0x0
 366 01a1 05          	.uleb128 0x5
 367 01a2 64 6C 63 00 	.asciz "dlc"
 368 01a6 02          	.byte 0x2
 369 01a7 1A          	.byte 0x1a
MPLAB XC16 ASSEMBLY Listing:   			page 9


 370 01a8 F8 00 00 00 	.4byte 0xf8
 371 01ac 02          	.byte 0x2
 372 01ad 04          	.byte 0x4
 373 01ae 0C          	.byte 0xc
 374 01af 02          	.byte 0x2
 375 01b0 23          	.byte 0x23
 376 01b1 02          	.uleb128 0x2
 377 01b2 0A          	.uleb128 0xa
 378 01b3 64 61 74 61 	.asciz "data"
 378      00 
 379 01b8 02          	.byte 0x2
 380 01b9 1B          	.byte 0x1b
 381 01ba C2 01 00 00 	.4byte 0x1c2
 382 01be 02          	.byte 0x2
 383 01bf 23          	.byte 0x23
 384 01c0 04          	.uleb128 0x4
 385 01c1 00          	.byte 0x0
 386 01c2 0B          	.uleb128 0xb
 387 01c3 F8 00 00 00 	.4byte 0xf8
 388 01c7 D2 01 00 00 	.4byte 0x1d2
 389 01cb 0C          	.uleb128 0xc
 390 01cc 08 01 00 00 	.4byte 0x108
 391 01d0 03          	.byte 0x3
 392 01d1 00          	.byte 0x0
 393 01d2 03          	.uleb128 0x3
 394 01d3 43 41 4E 64 	.asciz "CANdata"
 394      61 74 61 00 
 395 01db 02          	.byte 0x2
 396 01dc 1C          	.byte 0x1c
 397 01dd 91 01 00 00 	.4byte 0x191
 398 01e1 04          	.uleb128 0x4
 399 01e2 08          	.byte 0x8
 400 01e3 04          	.byte 0x4
 401 01e4 0D          	.byte 0xd
 402 01e5 2E 02 00 00 	.4byte 0x22e
 403 01e9 0A          	.uleb128 0xa
 404 01ea 74 69 6D 65 	.asciz "timestamp"
 404      73 74 61 6D 
 404      70 00 
 405 01f4 04          	.byte 0x4
 406 01f5 0E          	.byte 0xe
 407 01f6 08 01 00 00 	.4byte 0x108
 408 01fa 02          	.byte 0x2
 409 01fb 23          	.byte 0x23
 410 01fc 00          	.uleb128 0x0
 411 01fd 0A          	.uleb128 0xa
 412 01fe 74 65 6D 70 	.asciz "temp1"
 412      31 00 
 413 0204 04          	.byte 0x4
 414 0205 0F          	.byte 0xf
 415 0206 08 01 00 00 	.4byte 0x108
 416 020a 02          	.byte 0x2
 417 020b 23          	.byte 0x23
 418 020c 02          	.uleb128 0x2
 419 020d 0A          	.uleb128 0xa
 420 020e 74 65 6D 70 	.asciz "temp2"
 420      32 00 
MPLAB XC16 ASSEMBLY Listing:   			page 10


 421 0214 04          	.byte 0x4
 422 0215 10          	.byte 0x10
 423 0216 08 01 00 00 	.4byte 0x108
 424 021a 02          	.byte 0x2
 425 021b 23          	.byte 0x23
 426 021c 04          	.uleb128 0x4
 427 021d 0A          	.uleb128 0xa
 428 021e 74 65 6D 70 	.asciz "temp3"
 428      33 00 
 429 0224 04          	.byte 0x4
 430 0225 11          	.byte 0x11
 431 0226 08 01 00 00 	.4byte 0x108
 432 022a 02          	.byte 0x2
 433 022b 23          	.byte 0x23
 434 022c 06          	.uleb128 0x6
 435 022d 00          	.byte 0x0
 436 022e 03          	.uleb128 0x3
 437 022f 57 54 5F 54 	.asciz "WT_Temperature"
 437      65 6D 70 65 
 437      72 61 74 75 
 437      72 65 00 
 438 023e 04          	.byte 0x4
 439 023f 12          	.byte 0x12
 440 0240 E1 01 00 00 	.4byte 0x1e1
 441 0244 04          	.uleb128 0x4
 442 0245 04          	.byte 0x4
 443 0246 04          	.byte 0x4
 444 0247 14          	.byte 0x14
 445 0248 79 02 00 00 	.4byte 0x279
 446 024c 0A          	.uleb128 0xa
 447 024d 66 6C 6F 77 	.asciz "flow_rate_1"
 447      5F 72 61 74 
 447      65 5F 31 00 
 448 0259 04          	.byte 0x4
 449 025a 15          	.byte 0x15
 450 025b 08 01 00 00 	.4byte 0x108
 451 025f 02          	.byte 0x2
 452 0260 23          	.byte 0x23
 453 0261 00          	.uleb128 0x0
 454 0262 0A          	.uleb128 0xa
 455 0263 66 6C 6F 77 	.asciz "flow_rate_2"
 455      5F 72 61 74 
 455      65 5F 32 00 
 456 026f 04          	.byte 0x4
 457 0270 16          	.byte 0x16
 458 0271 08 01 00 00 	.4byte 0x108
 459 0275 02          	.byte 0x2
 460 0276 23          	.byte 0x23
 461 0277 02          	.uleb128 0x2
 462 0278 00          	.byte 0x0
 463 0279 03          	.uleb128 0x3
 464 027a 57 54 5F 46 	.asciz "WT_Flow_Rate"
 464      6C 6F 77 5F 
 464      52 61 74 65 
 464      00 
 465 0287 04          	.byte 0x4
 466 0288 17          	.byte 0x17
MPLAB XC16 ASSEMBLY Listing:   			page 11


 467 0289 44 02 00 00 	.4byte 0x244
 468 028d 04          	.uleb128 0x4
 469 028e 14          	.byte 0x14
 470 028f 04          	.byte 0x4
 471 0290 19          	.byte 0x19
 472 0291 D2 02 00 00 	.4byte 0x2d2
 473 0295 0A          	.uleb128 0xa
 474 0296 74 65 6D 70 	.asciz "temperature1"
 474      65 72 61 74 
 474      75 72 65 31 
 474      00 
 475 02a3 04          	.byte 0x4
 476 02a4 1A          	.byte 0x1a
 477 02a5 2E 02 00 00 	.4byte 0x22e
 478 02a9 02          	.byte 0x2
 479 02aa 23          	.byte 0x23
 480 02ab 00          	.uleb128 0x0
 481 02ac 0A          	.uleb128 0xa
 482 02ad 74 65 6D 70 	.asciz "temperature2"
 482      65 72 61 74 
 482      75 72 65 32 
 482      00 
 483 02ba 04          	.byte 0x4
 484 02bb 1B          	.byte 0x1b
 485 02bc 2E 02 00 00 	.4byte 0x22e
 486 02c0 02          	.byte 0x2
 487 02c1 23          	.byte 0x23
 488 02c2 08          	.uleb128 0x8
 489 02c3 0D          	.uleb128 0xd
 490 02c4 00 00 00 00 	.4byte .LASF0
 491 02c8 04          	.byte 0x4
 492 02c9 1C          	.byte 0x1c
 493 02ca 79 02 00 00 	.4byte 0x279
 494 02ce 02          	.byte 0x2
 495 02cf 23          	.byte 0x23
 496 02d0 10          	.uleb128 0x10
 497 02d1 00          	.byte 0x0
 498 02d2 03          	.uleb128 0x3
 499 02d3 57 54 5F 44 	.asciz "WT_Data"
 499      61 74 61 00 
 500 02db 04          	.byte 0x4
 501 02dc 1D          	.byte 0x1d
 502 02dd 8D 02 00 00 	.4byte 0x28d
 503 02e1 0E          	.uleb128 0xe
 504 02e2 01          	.byte 0x1
 505 02e3 70 61 72 73 	.asciz "parse_can_message_wt_temperature"
 505      65 5F 63 61 
 505      6E 5F 6D 65 
 505      73 73 61 67 
 505      65 5F 77 74 
 505      5F 74 65 6D 
 505      70 65 72 61 
 505      74 75 72 65 
 505      00 
 506 0304 01          	.byte 0x1
 507 0305 05          	.byte 0x5
 508 0306 01          	.byte 0x1
MPLAB XC16 ASSEMBLY Listing:   			page 12


 509 0307 00 00 00 00 	.4byte .LFB0
 510 030b 00 00 00 00 	.4byte .LFE0
 511 030f 01          	.byte 0x1
 512 0310 5E          	.byte 0x5e
 513 0311 3A 03 00 00 	.4byte 0x33a
 514 0315 0F          	.uleb128 0xf
 515 0316 6D 73 67 00 	.asciz "msg"
 516 031a 01          	.byte 0x1
 517 031b 05          	.byte 0x5
 518 031c D2 01 00 00 	.4byte 0x1d2
 519 0320 02          	.byte 0x2
 520 0321 7E          	.byte 0x7e
 521 0322 00          	.sleb128 0
 522 0323 0F          	.uleb128 0xf
 523 0324 74 65 6D 70 	.asciz "temperature"
 523      65 72 61 74 
 523      75 72 65 00 
 524 0330 01          	.byte 0x1
 525 0331 05          	.byte 0x5
 526 0332 3A 03 00 00 	.4byte 0x33a
 527 0336 02          	.byte 0x2
 528 0337 7E          	.byte 0x7e
 529 0338 0C          	.sleb128 12
 530 0339 00          	.byte 0x0
 531 033a 10          	.uleb128 0x10
 532 033b 02          	.byte 0x2
 533 033c 2E 02 00 00 	.4byte 0x22e
 534 0340 0E          	.uleb128 0xe
 535 0341 01          	.byte 0x1
 536 0342 70 61 72 73 	.asciz "parse_can_message_wt_flow_rate"
 536      65 5F 63 61 
 536      6E 5F 6D 65 
 536      73 73 61 67 
 536      65 5F 77 74 
 536      5F 66 6C 6F 
 536      77 5F 72 61 
 536      74 65 00 
 537 0361 01          	.byte 0x1
 538 0362 0C          	.byte 0xc
 539 0363 01          	.byte 0x1
 540 0364 00 00 00 00 	.4byte .LFB1
 541 0368 00 00 00 00 	.4byte .LFE1
 542 036c 01          	.byte 0x1
 543 036d 5E          	.byte 0x5e
 544 036e 8F 03 00 00 	.4byte 0x38f
 545 0372 0F          	.uleb128 0xf
 546 0373 6D 73 67 00 	.asciz "msg"
 547 0377 01          	.byte 0x1
 548 0378 0C          	.byte 0xc
 549 0379 D2 01 00 00 	.4byte 0x1d2
 550 037d 02          	.byte 0x2
 551 037e 7E          	.byte 0x7e
 552 037f 00          	.sleb128 0
 553 0380 11          	.uleb128 0x11
 554 0381 00 00 00 00 	.4byte .LASF0
 555 0385 01          	.byte 0x1
 556 0386 0C          	.byte 0xc
MPLAB XC16 ASSEMBLY Listing:   			page 13


 557 0387 8F 03 00 00 	.4byte 0x38f
 558 038b 02          	.byte 0x2
 559 038c 7E          	.byte 0x7e
 560 038d 0C          	.sleb128 12
 561 038e 00          	.byte 0x0
 562 038f 10          	.uleb128 0x10
 563 0390 02          	.byte 0x2
 564 0391 79 02 00 00 	.4byte 0x279
 565 0395 0E          	.uleb128 0xe
 566 0396 01          	.byte 0x1
 567 0397 70 61 72 73 	.asciz "parse_can_message_wt"
 567      65 5F 63 61 
 567      6E 5F 6D 65 
 567      73 73 61 67 
 567      65 5F 77 74 
 567      00 
 568 03ac 01          	.byte 0x1
 569 03ad 11          	.byte 0x11
 570 03ae 01          	.byte 0x1
 571 03af 00 00 00 00 	.4byte .LFB2
 572 03b3 00 00 00 00 	.4byte .LFE2
 573 03b7 01          	.byte 0x1
 574 03b8 5E          	.byte 0x5e
 575 03b9 DB 03 00 00 	.4byte 0x3db
 576 03bd 0F          	.uleb128 0xf
 577 03be 6D 73 67 00 	.asciz "msg"
 578 03c2 01          	.byte 0x1
 579 03c3 11          	.byte 0x11
 580 03c4 D2 01 00 00 	.4byte 0x1d2
 581 03c8 02          	.byte 0x2
 582 03c9 7E          	.byte 0x7e
 583 03ca 00          	.sleb128 0
 584 03cb 0F          	.uleb128 0xf
 585 03cc 64 61 74 61 	.asciz "data"
 585      00 
 586 03d1 01          	.byte 0x1
 587 03d2 11          	.byte 0x11
 588 03d3 DB 03 00 00 	.4byte 0x3db
 589 03d7 02          	.byte 0x2
 590 03d8 7E          	.byte 0x7e
 591 03d9 0C          	.sleb128 12
 592 03da 00          	.byte 0x0
 593 03db 10          	.uleb128 0x10
 594 03dc 02          	.byte 0x2
 595 03dd D2 02 00 00 	.4byte 0x2d2
 596 03e1 00          	.byte 0x0
 597                 	.section .debug_abbrev,info
 598 0000 01          	.uleb128 0x1
 599 0001 11          	.uleb128 0x11
 600 0002 01          	.byte 0x1
 601 0003 25          	.uleb128 0x25
 602 0004 08          	.uleb128 0x8
 603 0005 13          	.uleb128 0x13
 604 0006 0B          	.uleb128 0xb
 605 0007 03          	.uleb128 0x3
 606 0008 08          	.uleb128 0x8
 607 0009 1B          	.uleb128 0x1b
MPLAB XC16 ASSEMBLY Listing:   			page 14


 608 000a 08          	.uleb128 0x8
 609 000b 11          	.uleb128 0x11
 610 000c 01          	.uleb128 0x1
 611 000d 12          	.uleb128 0x12
 612 000e 01          	.uleb128 0x1
 613 000f 10          	.uleb128 0x10
 614 0010 06          	.uleb128 0x6
 615 0011 00          	.byte 0x0
 616 0012 00          	.byte 0x0
 617 0013 02          	.uleb128 0x2
 618 0014 24          	.uleb128 0x24
 619 0015 00          	.byte 0x0
 620 0016 0B          	.uleb128 0xb
 621 0017 0B          	.uleb128 0xb
 622 0018 3E          	.uleb128 0x3e
 623 0019 0B          	.uleb128 0xb
 624 001a 03          	.uleb128 0x3
 625 001b 08          	.uleb128 0x8
 626 001c 00          	.byte 0x0
 627 001d 00          	.byte 0x0
 628 001e 03          	.uleb128 0x3
 629 001f 16          	.uleb128 0x16
 630 0020 00          	.byte 0x0
 631 0021 03          	.uleb128 0x3
 632 0022 08          	.uleb128 0x8
 633 0023 3A          	.uleb128 0x3a
 634 0024 0B          	.uleb128 0xb
 635 0025 3B          	.uleb128 0x3b
 636 0026 0B          	.uleb128 0xb
 637 0027 49          	.uleb128 0x49
 638 0028 13          	.uleb128 0x13
 639 0029 00          	.byte 0x0
 640 002a 00          	.byte 0x0
 641 002b 04          	.uleb128 0x4
 642 002c 13          	.uleb128 0x13
 643 002d 01          	.byte 0x1
 644 002e 0B          	.uleb128 0xb
 645 002f 0B          	.uleb128 0xb
 646 0030 3A          	.uleb128 0x3a
 647 0031 0B          	.uleb128 0xb
 648 0032 3B          	.uleb128 0x3b
 649 0033 0B          	.uleb128 0xb
 650 0034 01          	.uleb128 0x1
 651 0035 13          	.uleb128 0x13
 652 0036 00          	.byte 0x0
 653 0037 00          	.byte 0x0
 654 0038 05          	.uleb128 0x5
 655 0039 0D          	.uleb128 0xd
 656 003a 00          	.byte 0x0
 657 003b 03          	.uleb128 0x3
 658 003c 08          	.uleb128 0x8
 659 003d 3A          	.uleb128 0x3a
 660 003e 0B          	.uleb128 0xb
 661 003f 3B          	.uleb128 0x3b
 662 0040 0B          	.uleb128 0xb
 663 0041 49          	.uleb128 0x49
 664 0042 13          	.uleb128 0x13
MPLAB XC16 ASSEMBLY Listing:   			page 15


 665 0043 0B          	.uleb128 0xb
 666 0044 0B          	.uleb128 0xb
 667 0045 0D          	.uleb128 0xd
 668 0046 0B          	.uleb128 0xb
 669 0047 0C          	.uleb128 0xc
 670 0048 0B          	.uleb128 0xb
 671 0049 38          	.uleb128 0x38
 672 004a 0A          	.uleb128 0xa
 673 004b 00          	.byte 0x0
 674 004c 00          	.byte 0x0
 675 004d 06          	.uleb128 0x6
 676 004e 17          	.uleb128 0x17
 677 004f 01          	.byte 0x1
 678 0050 0B          	.uleb128 0xb
 679 0051 0B          	.uleb128 0xb
 680 0052 3A          	.uleb128 0x3a
 681 0053 0B          	.uleb128 0xb
 682 0054 3B          	.uleb128 0x3b
 683 0055 0B          	.uleb128 0xb
 684 0056 01          	.uleb128 0x1
 685 0057 13          	.uleb128 0x13
 686 0058 00          	.byte 0x0
 687 0059 00          	.byte 0x0
 688 005a 07          	.uleb128 0x7
 689 005b 0D          	.uleb128 0xd
 690 005c 00          	.byte 0x0
 691 005d 49          	.uleb128 0x49
 692 005e 13          	.uleb128 0x13
 693 005f 00          	.byte 0x0
 694 0060 00          	.byte 0x0
 695 0061 08          	.uleb128 0x8
 696 0062 0D          	.uleb128 0xd
 697 0063 00          	.byte 0x0
 698 0064 03          	.uleb128 0x3
 699 0065 08          	.uleb128 0x8
 700 0066 3A          	.uleb128 0x3a
 701 0067 0B          	.uleb128 0xb
 702 0068 3B          	.uleb128 0x3b
 703 0069 0B          	.uleb128 0xb
 704 006a 49          	.uleb128 0x49
 705 006b 13          	.uleb128 0x13
 706 006c 00          	.byte 0x0
 707 006d 00          	.byte 0x0
 708 006e 09          	.uleb128 0x9
 709 006f 0D          	.uleb128 0xd
 710 0070 00          	.byte 0x0
 711 0071 49          	.uleb128 0x49
 712 0072 13          	.uleb128 0x13
 713 0073 38          	.uleb128 0x38
 714 0074 0A          	.uleb128 0xa
 715 0075 00          	.byte 0x0
 716 0076 00          	.byte 0x0
 717 0077 0A          	.uleb128 0xa
 718 0078 0D          	.uleb128 0xd
 719 0079 00          	.byte 0x0
 720 007a 03          	.uleb128 0x3
 721 007b 08          	.uleb128 0x8
MPLAB XC16 ASSEMBLY Listing:   			page 16


 722 007c 3A          	.uleb128 0x3a
 723 007d 0B          	.uleb128 0xb
 724 007e 3B          	.uleb128 0x3b
 725 007f 0B          	.uleb128 0xb
 726 0080 49          	.uleb128 0x49
 727 0081 13          	.uleb128 0x13
 728 0082 38          	.uleb128 0x38
 729 0083 0A          	.uleb128 0xa
 730 0084 00          	.byte 0x0
 731 0085 00          	.byte 0x0
 732 0086 0B          	.uleb128 0xb
 733 0087 01          	.uleb128 0x1
 734 0088 01          	.byte 0x1
 735 0089 49          	.uleb128 0x49
 736 008a 13          	.uleb128 0x13
 737 008b 01          	.uleb128 0x1
 738 008c 13          	.uleb128 0x13
 739 008d 00          	.byte 0x0
 740 008e 00          	.byte 0x0
 741 008f 0C          	.uleb128 0xc
 742 0090 21          	.uleb128 0x21
 743 0091 00          	.byte 0x0
 744 0092 49          	.uleb128 0x49
 745 0093 13          	.uleb128 0x13
 746 0094 2F          	.uleb128 0x2f
 747 0095 0B          	.uleb128 0xb
 748 0096 00          	.byte 0x0
 749 0097 00          	.byte 0x0
 750 0098 0D          	.uleb128 0xd
 751 0099 0D          	.uleb128 0xd
 752 009a 00          	.byte 0x0
 753 009b 03          	.uleb128 0x3
 754 009c 0E          	.uleb128 0xe
 755 009d 3A          	.uleb128 0x3a
 756 009e 0B          	.uleb128 0xb
 757 009f 3B          	.uleb128 0x3b
 758 00a0 0B          	.uleb128 0xb
 759 00a1 49          	.uleb128 0x49
 760 00a2 13          	.uleb128 0x13
 761 00a3 38          	.uleb128 0x38
 762 00a4 0A          	.uleb128 0xa
 763 00a5 00          	.byte 0x0
 764 00a6 00          	.byte 0x0
 765 00a7 0E          	.uleb128 0xe
 766 00a8 2E          	.uleb128 0x2e
 767 00a9 01          	.byte 0x1
 768 00aa 3F          	.uleb128 0x3f
 769 00ab 0C          	.uleb128 0xc
 770 00ac 03          	.uleb128 0x3
 771 00ad 08          	.uleb128 0x8
 772 00ae 3A          	.uleb128 0x3a
 773 00af 0B          	.uleb128 0xb
 774 00b0 3B          	.uleb128 0x3b
 775 00b1 0B          	.uleb128 0xb
 776 00b2 27          	.uleb128 0x27
 777 00b3 0C          	.uleb128 0xc
 778 00b4 11          	.uleb128 0x11
MPLAB XC16 ASSEMBLY Listing:   			page 17


 779 00b5 01          	.uleb128 0x1
 780 00b6 12          	.uleb128 0x12
 781 00b7 01          	.uleb128 0x1
 782 00b8 40          	.uleb128 0x40
 783 00b9 0A          	.uleb128 0xa
 784 00ba 01          	.uleb128 0x1
 785 00bb 13          	.uleb128 0x13
 786 00bc 00          	.byte 0x0
 787 00bd 00          	.byte 0x0
 788 00be 0F          	.uleb128 0xf
 789 00bf 05          	.uleb128 0x5
 790 00c0 00          	.byte 0x0
 791 00c1 03          	.uleb128 0x3
 792 00c2 08          	.uleb128 0x8
 793 00c3 3A          	.uleb128 0x3a
 794 00c4 0B          	.uleb128 0xb
 795 00c5 3B          	.uleb128 0x3b
 796 00c6 0B          	.uleb128 0xb
 797 00c7 49          	.uleb128 0x49
 798 00c8 13          	.uleb128 0x13
 799 00c9 02          	.uleb128 0x2
 800 00ca 0A          	.uleb128 0xa
 801 00cb 00          	.byte 0x0
 802 00cc 00          	.byte 0x0
 803 00cd 10          	.uleb128 0x10
 804 00ce 0F          	.uleb128 0xf
 805 00cf 00          	.byte 0x0
 806 00d0 0B          	.uleb128 0xb
 807 00d1 0B          	.uleb128 0xb
 808 00d2 49          	.uleb128 0x49
 809 00d3 13          	.uleb128 0x13
 810 00d4 00          	.byte 0x0
 811 00d5 00          	.byte 0x0
 812 00d6 11          	.uleb128 0x11
 813 00d7 05          	.uleb128 0x5
 814 00d8 00          	.byte 0x0
 815 00d9 03          	.uleb128 0x3
 816 00da 0E          	.uleb128 0xe
 817 00db 3A          	.uleb128 0x3a
 818 00dc 0B          	.uleb128 0xb
 819 00dd 3B          	.uleb128 0x3b
 820 00de 0B          	.uleb128 0xb
 821 00df 49          	.uleb128 0x49
 822 00e0 13          	.uleb128 0x13
 823 00e1 02          	.uleb128 0x2
 824 00e2 0A          	.uleb128 0xa
 825 00e3 00          	.byte 0x0
 826 00e4 00          	.byte 0x0
 827 00e5 00          	.byte 0x0
 828                 	.section .debug_pubnames,info
 829 0000 6F 00 00 00 	.4byte 0x6f
 830 0004 02 00       	.2byte 0x2
 831 0006 00 00 00 00 	.4byte .Ldebug_info0
 832 000a E2 03 00 00 	.4byte 0x3e2
 833 000e E1 02 00 00 	.4byte 0x2e1
 834 0012 70 61 72 73 	.asciz "parse_can_message_wt_temperature"
 834      65 5F 63 61 
MPLAB XC16 ASSEMBLY Listing:   			page 18


 834      6E 5F 6D 65 
 834      73 73 61 67 
 834      65 5F 77 74 
 834      5F 74 65 6D 
 834      70 65 72 61 
 834      74 75 72 65 
 834      00 
 835 0033 40 03 00 00 	.4byte 0x340
 836 0037 70 61 72 73 	.asciz "parse_can_message_wt_flow_rate"
 836      65 5F 63 61 
 836      6E 5F 6D 65 
 836      73 73 61 67 
 836      65 5F 77 74 
 836      5F 66 6C 6F 
 836      77 5F 72 61 
 836      74 65 00 
 837 0056 95 03 00 00 	.4byte 0x395
 838 005a 70 61 72 73 	.asciz "parse_can_message_wt"
 838      65 5F 63 61 
 838      6E 5F 6D 65 
 838      73 73 61 67 
 838      65 5F 77 74 
 838      00 
 839 006f 00 00 00 00 	.4byte 0x0
 840                 	.section .debug_pubtypes,info
 841 0000 57 00 00 00 	.4byte 0x57
 842 0004 02 00       	.2byte 0x2
 843 0006 00 00 00 00 	.4byte .Ldebug_info0
 844 000a E2 03 00 00 	.4byte 0x3e2
 845 000e F8 00 00 00 	.4byte 0xf8
 846 0012 75 69 6E 74 	.asciz "uint16_t"
 846      31 36 5F 74 
 846      00 
 847 001b D2 01 00 00 	.4byte 0x1d2
 848 001f 43 41 4E 64 	.asciz "CANdata"
 848      61 74 61 00 
 849 0027 2E 02 00 00 	.4byte 0x22e
 850 002b 57 54 5F 54 	.asciz "WT_Temperature"
 850      65 6D 70 65 
 850      72 61 74 75 
 850      72 65 00 
 851 003a 79 02 00 00 	.4byte 0x279
 852 003e 57 54 5F 46 	.asciz "WT_Flow_Rate"
 852      6C 6F 77 5F 
 852      52 61 74 65 
 852      00 
 853 004b D2 02 00 00 	.4byte 0x2d2
 854 004f 57 54 5F 44 	.asciz "WT_Data"
 854      61 74 61 00 
 855 0057 00 00 00 00 	.4byte 0x0
 856                 	.section .debug_aranges,info
 857 0000 14 00 00 00 	.4byte 0x14
 858 0004 02 00       	.2byte 0x2
 859 0006 00 00 00 00 	.4byte .Ldebug_info0
 860 000a 04          	.byte 0x4
 861 000b 00          	.byte 0x0
 862 000c 00 00       	.2byte 0x0
MPLAB XC16 ASSEMBLY Listing:   			page 19


 863 000e 00 00       	.2byte 0x0
 864 0010 00 00 00 00 	.4byte 0x0
 865 0014 00 00 00 00 	.4byte 0x0
 866                 	.section .debug_str,info
 867                 	.LASF0:
 868 0000 66 6C 6F 77 	.asciz "flow_rate"
 868      5F 72 61 74 
 868      65 00 
 869                 	.section .text,code
 870              	
 871              	
 872              	
 873              	.section __c30_info,info,bss
 874                 	__psv_trap_errata:
 875                 	
 876                 	.section __c30_signature,info,data
 877 0000 01 00       	.word 0x0001
 878 0002 00 00       	.word 0x0000
 879 0004 00 00       	.word 0x0000
 880                 	
 881                 	
 882                 	
 883                 	.set ___PA___,0
 884                 	.end
MPLAB XC16 ASSEMBLY Listing:   			page 20


DEFINED SYMBOLS
                            *ABS*:00000000 /home/user/Documents/FST/Programming/project_sub-zero/lib/can-ids/Devices/WATER_TEMP_CAN.c
                            *ABS*:00000001 __33EP256MU806
                            *ABS*:00000001 __dsPIC33E
                            *ABS*:00000001 __HAS_DSP
                            *ABS*:00000001 __HAS_DMAV2
                            *ABS*:00000001 __HAS_CODEGUARD
                            *ABS*:00000001 __HAS_PMP_ENHANCED
                            *ABS*:00000001 __HAS_EDS
                            *ABS*:00000001 __HAS_EP
                            *ABS*:00000011 __TARGET_DIVIDE_CYCLES
                            *ABS*:00000001 __C30ELF
    {standard input}:14     .text:00000000 _parse_can_message_wt_temperature
    {standard input}:18     *ABS*:00000000 ___PA___
    {standard input}:64     .text:00000034 _parse_can_message_wt_flow_rate
    {standard input}:100    .text:00000058 _parse_can_message_wt
    {standard input}:127    *ABS*:00000000 ___BP___
    {standard input}:874    __c30_info:00000000 __psv_trap_errata
    {standard input}:19     .text:00000000 .L0
                            .text:00000098 .L6
                            .text:00000084 .L7
                            .text:000000b2 .L5
                            .text:000000ca .L3
                    .debug_abbrev:00000000 .Ldebug_abbrev0
                            .text:00000000 .Ltext0
                            .text:000000d2 .Letext0
                      .debug_line:00000000 .Ldebug_line0
                       .debug_str:00000000 .LASF0
                            .text:00000000 .LFB0
                            .text:00000034 .LFE0
                            .text:00000034 .LFB1
                            .text:00000058 .LFE1
                            .text:00000058 .LFB2
                            .text:000000d2 .LFE2
                     .debug_frame:00000000 .Lframe0
                      .debug_info:00000000 .Ldebug_info0

UNDEFINED SYMBOLS
CORCON

EQUATE SYMBOLS
/home/user/Documents/FST/Programming/project_sub-zero/lib/can-ids/Devices/WATER_TEMP_CAN.c = 0x0
                  __33EP256MU806 = 0x1
                      __dsPIC33E = 0x1
                       __HAS_DSP = 0x1
                     __HAS_DMAV2 = 0x1
                 __HAS_CODEGUARD = 0x1
              __HAS_PMP_ENHANCED = 0x1
                       __HAS_EDS = 0x1
                        __HAS_EP = 0x1
          __TARGET_DIVIDE_CYCLES = 0x11
                        __C30ELF = 0x1
                        ___PA___ = 0x0
                        ___BP___ = 0x0
