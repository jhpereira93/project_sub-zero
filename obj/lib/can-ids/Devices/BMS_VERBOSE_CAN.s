MPLAB XC16 ASSEMBLY Listing:   			page 1


   1              	.file "/home/user/Documents/FST/Programming/project_sub-zero/lib/can-ids/Devices/BMS_VERBOSE_CAN.c"
   2              	.section .debug_abbrev,info
   3                 	.Ldebug_abbrev0:
   4                 	.section .debug_info,info
   5                 	.Ldebug_info0:
   6                 	.section .debug_line,info
   7                 	.Ldebug_line0:
   8 0000 03 01 00 00 	.section .text,code
   8      02 00 AA 00 
   8      00 00 01 01 
   8      FB 0E 0A 00 
   8      01 01 01 01 
   8      00 00 00 01 
   8      6C 69 62 2F 
   8      63 61 6E 2D 
   8      69 64 73 2F 
   9              	.Ltext0:
  10              	.section .text,code
  11              	.align 2
  12              	.global _parse_can_message_master_verbose
  13              	.type _parse_can_message_master_verbose,@function
  14              	_parse_can_message_master_verbose:
  15              	.LFB0:
  16              	.file 1 "lib/can-ids/Devices/BMS_VERBOSE_CAN.c"
   1:lib/can-ids/Devices/BMS_VERBOSE_CAN.c **** #ifndef __BMS_VERBOSE
   2:lib/can-ids/Devices/BMS_VERBOSE_CAN.c **** #define __BMS_VERBOSE
   3:lib/can-ids/Devices/BMS_VERBOSE_CAN.c **** 
   4:lib/can-ids/Devices/BMS_VERBOSE_CAN.c **** #include "can-ids/Devices/BMS_VERBOSE_CAN.h"
   5:lib/can-ids/Devices/BMS_VERBOSE_CAN.c **** #include "can-ids/CAN_IDs.h"
   6:lib/can-ids/Devices/BMS_VERBOSE_CAN.c **** 
   7:lib/can-ids/Devices/BMS_VERBOSE_CAN.c **** /**
   8:lib/can-ids/Devices/BMS_VERBOSE_CAN.c ****  * Per message parse functions are hard to apply here due to array of equal
   9:lib/can-ids/Devices/BMS_VERBOSE_CAN.c ****  * things
  10:lib/can-ids/Devices/BMS_VERBOSE_CAN.c ****  */
  11:lib/can-ids/Devices/BMS_VERBOSE_CAN.c **** #if 0
  12:lib/can-ids/Devices/BMS_VERBOSE_CAN.c **** void parse_can_message_master_cell_info(CANdata msg, MASTER_MSG_Cell_Info *info) {
  13:lib/can-ids/Devices/BMS_VERBOSE_CAN.c **** 	uint16_t id = msg.data[0];
  14:lib/can-ids/Devices/BMS_VERBOSE_CAN.c **** 	if (id > 143) {
  15:lib/can-ids/Devices/BMS_VERBOSE_CAN.c **** 		return;
  16:lib/can-ids/Devices/BMS_VERBOSE_CAN.c **** 	}
  17:lib/can-ids/Devices/BMS_VERBOSE_CAN.c **** 	info[id].cell_id = id;
  18:lib/can-ids/Devices/BMS_VERBOSE_CAN.c **** 	info[id].voltage = msg.data[1];
  19:lib/can-ids/Devices/BMS_VERBOSE_CAN.c **** 	info[id].temperature = msg.data[2];
  20:lib/can-ids/Devices/BMS_VERBOSE_CAN.c **** 	info[id].SoC = msg.data[3];
  21:lib/can-ids/Devices/BMS_VERBOSE_CAN.c **** 
  22:lib/can-ids/Devices/BMS_VERBOSE_CAN.c **** 	return;
  23:lib/can-ids/Devices/BMS_VERBOSE_CAN.c **** }
  24:lib/can-ids/Devices/BMS_VERBOSE_CAN.c **** 
  25:lib/can-ids/Devices/BMS_VERBOSE_CAN.c **** void parse_can_message_master_slave_info_1(CANdata msg, MASTER_MSG_Slave_Info_1 *slave) {
  26:lib/can-ids/Devices/BMS_VERBOSE_CAN.c **** 	uint16_t id = msg.data[0];
  27:lib/can-ids/Devices/BMS_VERBOSE_CAN.c **** 	if (id > 11) {
  28:lib/can-ids/Devices/BMS_VERBOSE_CAN.c **** 		return;
  29:lib/can-ids/Devices/BMS_VERBOSE_CAN.c **** 	}
  30:lib/can-ids/Devices/BMS_VERBOSE_CAN.c **** 	info.slave_id = id;
  31:lib/can-ids/Devices/BMS_VERBOSE_CAN.c **** 	info.discharge_temperature_connection_OK_mask = msg.data[1];
  32:lib/can-ids/Devices/BMS_VERBOSE_CAN.c **** 
  33:lib/can-ids/Devices/BMS_VERBOSE_CAN.c **** 	return;
MPLAB XC16 ASSEMBLY Listing:   			page 2


  34:lib/can-ids/Devices/BMS_VERBOSE_CAN.c **** }
  35:lib/can-ids/Devices/BMS_VERBOSE_CAN.c **** #endif
  36:lib/can-ids/Devices/BMS_VERBOSE_CAN.c **** 
  37:lib/can-ids/Devices/BMS_VERBOSE_CAN.c **** void parse_can_message_master_verbose(CANdata msg, MASTER_VERBOSE_MSG_Data *master){
  17              	.loc 1 37 0
  18              	.set ___PA___,1
  19 000000  10 00 FA 	lnk #16
  20              	.LCFI0:
  21 000002  88 1F 78 	mov w8,[w15++]
  22              	.LCFI1:
  23              	.loc 1 37 0
  24 000004  10 07 98 	mov w0,[w14+2]
  25 000006  21 07 98 	mov w1,[w14+4]
  38:lib/can-ids/Devices/BMS_VERBOSE_CAN.c ****     uint16_t id;
  39:lib/can-ids/Devices/BMS_VERBOSE_CAN.c ****     switch(msg.msg_id){
  26              	.loc 1 39 0
  27 000008  1E 00 90 	mov [w14+2],w0
  28              	.loc 1 37 0
  29 00000a  32 07 98 	mov w2,[w14+6]
  30              	.loc 1 39 0
  31 00000c  45 00 DE 	lsr w0,#5,w0
  32              	.loc 1 37 0
  33 00000e  43 07 98 	mov w3,[w14+8]
  34              	.loc 1 39 0
  35 000010  F0 43 B2 	and.b #63,w0
  36              	.loc 1 37 0
  37 000012  54 07 98 	mov w4,[w14+10]
  38 000014  65 07 98 	mov w5,[w14+12]
  39 000016  76 07 98 	mov w6,[w14+14]
  40              	.loc 1 39 0
  41 000018  00 80 FB 	ze w0,w0
  42 00001a  D1 03 20 	mov #61,w1
  43 00001c  81 0F 50 	sub w0,w1,[w15]
  44              	.set ___BP___,0
  45 00001e  00 00 32 	bra z,.L4
  46 000020  D1 03 20 	mov #61,w1
  47 000022  81 0F 50 	sub w0,w1,[w15]
  48              	.set ___BP___,0
  49 000024  00 00 3C 	bra gt,.L7
  50 000026  C1 03 20 	mov #60,w1
  51 000028  81 0F 50 	sub w0,w1,[w15]
  52              	.set ___BP___,0
  53 00002a  00 00 32 	bra z,.L3
  54 00002c  00 00 37 	bra .L1
  55              	.L7:
  56 00002e  E1 03 20 	mov #62,w1
  57 000030  81 0F 50 	sub w0,w1,[w15]
  58              	.set ___BP___,0
  59 000032  00 00 32 	bra z,.L5
  60 000034  F1 03 20 	mov #63,w1
  61 000036  81 0F 50 	sub w0,w1,[w15]
  62              	.set ___BP___,0
  63 000038  00 00 32 	bra z,.L6
  64 00003a  00 00 37 	bra .L1
  65              	.L5:
  40:lib/can-ids/Devices/BMS_VERBOSE_CAN.c **** 
  41:lib/can-ids/Devices/BMS_VERBOSE_CAN.c ****         case MSG_ID_MASTER_SLAVE_INFO_1:
MPLAB XC16 ASSEMBLY Listing:   			page 3


  42:lib/can-ids/Devices/BMS_VERBOSE_CAN.c ****             id = msg.data[0];
  66              	.loc 1 42 0
  67 00003c  BE 00 90 	mov [w14+6],w1
  68 00003e  01 0F 78 	mov w1,[w14]
  43:lib/can-ids/Devices/BMS_VERBOSE_CAN.c ****             if (id >= 12){
  69              	.loc 1 43 0
  70 000040  1E 00 78 	mov [w14],w0
  71 000042  EB 0F 50 	sub w0,#11,[w15]
  72              	.set ___BP___,0
  73 000044  00 00 3E 	bra gtu,.L16
  74              	.L8:
  44:lib/can-ids/Devices/BMS_VERBOSE_CAN.c ****                 return;
  45:lib/can-ids/Devices/BMS_VERBOSE_CAN.c ****             }
  46:lib/can-ids/Devices/BMS_VERBOSE_CAN.c ****             master->slave.slave_info_1.slave_id = msg.data[0];
  75              	.loc 1 46 0
  76 000046  BE 03 90 	mov [w14+6],w7
  77 000048  7E 03 90 	mov [w14+14],w6
  47:lib/can-ids/Devices/BMS_VERBOSE_CAN.c ****             master->slave.slave_info_1.discharge_channel_fault_mask = msg.data[1];
  78              	.loc 1 47 0
  79 00004a  CE 02 90 	mov [w14+8],w5
  80 00004c  7E 02 90 	mov [w14+14],w4
  48:lib/can-ids/Devices/BMS_VERBOSE_CAN.c ****             master->slave.slave_info_1.cell_connection_fault_mask = msg.data[2];
  81              	.loc 1 48 0
  82 00004e  DE 01 90 	mov [w14+10],w3
  83 000050  7E 01 90 	mov [w14+14],w2
  49:lib/can-ids/Devices/BMS_VERBOSE_CAN.c ****             master->slave.slave_info_1.cell_temp_fault_mask = msg.data[3] & 0xFFF;
  84              	.loc 1 49 0
  85 000052  6E 04 90 	mov [w14+12],w8
  86 000054  F1 FF 20 	mov #4095,w1
  87 000056  7E 00 90 	mov [w14+14],w0
  88 000058  81 00 64 	and w8,w1,w1
  89              	.loc 1 46 0
  90 00005a  07 0B 78 	mov w7,[w6]
  91              	.loc 1 47 0
  92 00005c  15 02 98 	mov w5,[w4+2]
  93              	.loc 1 48 0
  94 00005e  23 01 98 	mov w3,[w2+4]
  95              	.loc 1 49 0
  96 000060  31 00 98 	mov w1,[w0+6]
  50:lib/can-ids/Devices/BMS_VERBOSE_CAN.c **** 			master->slave.slave_info_1.slave_temp1_fault = (msg.data[3] > 11)& 1;
  97              	.loc 1 50 0
  98 000062  6E 00 90 	mov [w14+12],w0
  99 000064  11 C0 B3 	mov.b #1,w1
 100 000066  EB 0F 50 	sub w0,#11,[w15]
 101              	.set ___BP___,0
 102 000068  00 00 3E 	bra gtu,.L9
 103 00006a  80 40 EB 	clr.b w1
 104              	.L9:
 105 00006c  00 00 00 	nop 
 106 00006e  7E 01 90 	mov [w14+14],w2
  51:lib/can-ids/Devices/BMS_VERBOSE_CAN.c **** 			master->slave.slave_info_1.slave_temp2_fault = (msg.data[3] > 12)& 1;
 107              	.loc 1 51 0
 108 000070  10 C0 B3 	mov.b #1,w0
 109              	.loc 1 50 0
 110 000072  01 49 98 	mov.b w1,[w2+8]
 111              	.loc 1 51 0
 112 000074  EE 00 90 	mov [w14+12],w1
MPLAB XC16 ASSEMBLY Listing:   			page 4


 113 000076  EC 8F 50 	sub w1,#12,[w15]
 114              	.set ___BP___,0
 115 000078  00 00 3E 	bra gtu,.L10
 116 00007a  00 40 EB 	clr.b w0
 117              	.L10:
 118 00007c  00 00 00 	nop 
 119 00007e  7E 01 90 	mov [w14+14],w2
  52:lib/can-ids/Devices/BMS_VERBOSE_CAN.c **** 			master->slave.slave_info_1.heatsink_temp1_fault = (msg.data[3] > 14)& 1;
 120              	.loc 1 52 0
 121 000080  11 C0 B3 	mov.b #1,w1
 122              	.loc 1 51 0
 123 000082  10 49 98 	mov.b w0,[w2+9]
 124              	.loc 1 52 0
 125 000084  6E 00 90 	mov [w14+12],w0
 126 000086  EE 0F 50 	sub w0,#14,[w15]
 127              	.set ___BP___,0
 128 000088  00 00 3E 	bra gtu,.L11
 129 00008a  80 40 EB 	clr.b w1
 130              	.L11:
 131 00008c  00 00 00 	nop 
 132 00008e  7E 01 90 	mov [w14+14],w2
  53:lib/can-ids/Devices/BMS_VERBOSE_CAN.c **** 			master->slave.slave_info_1.heatsink_temp2_fault = (msg.data[3] > 15)& 1;
 133              	.loc 1 53 0
 134 000090  10 C0 B3 	mov.b #1,w0
 135              	.loc 1 52 0
 136 000092  21 49 98 	mov.b w1,[w2+10]
 137              	.loc 1 53 0
 138 000094  EE 00 90 	mov [w14+12],w1
 139 000096  EF 8F 50 	sub w1,#15,[w15]
 140              	.set ___BP___,0
 141 000098  00 00 3E 	bra gtu,.L12
 142 00009a  00 40 EB 	clr.b w0
 143              	.L12:
 144 00009c  00 00 00 	nop 
 145 00009e  FE 00 90 	mov [w14+14],w1
 146 0000a0  00 00 00 	nop 
 147 0000a2  B0 48 98 	mov.b w0,[w1+11]
  54:lib/can-ids/Devices/BMS_VERBOSE_CAN.c **** 
  55:lib/can-ids/Devices/BMS_VERBOSE_CAN.c ****             break;
 148              	.loc 1 55 0
 149 0000a4  00 00 37 	bra .L1
 150              	.L4:
  56:lib/can-ids/Devices/BMS_VERBOSE_CAN.c **** 
  57:lib/can-ids/Devices/BMS_VERBOSE_CAN.c ****         case MSG_ID_MASTER_SLAVE_INFO_2:
  58:lib/can-ids/Devices/BMS_VERBOSE_CAN.c ****             id = msg.data[0];
 151              	.loc 1 58 0
 152 0000a6  00 00 00 	nop 
 153 0000a8  BE 00 90 	mov [w14+6],w1
 154 0000aa  01 0F 78 	mov w1,[w14]
  59:lib/can-ids/Devices/BMS_VERBOSE_CAN.c ****             if (id >= 12){
 155              	.loc 1 59 0
 156 0000ac  1E 00 78 	mov [w14],w0
 157 0000ae  EB 0F 50 	sub w0,#11,[w15]
 158              	.set ___BP___,0
 159 0000b0  00 00 3E 	bra gtu,.L17
 160              	.L13:
  60:lib/can-ids/Devices/BMS_VERBOSE_CAN.c ****                 return;
MPLAB XC16 ASSEMBLY Listing:   			page 5


  61:lib/can-ids/Devices/BMS_VERBOSE_CAN.c ****             }
  62:lib/can-ids/Devices/BMS_VERBOSE_CAN.c ****             master->slave.slave_info_2.slave_id = msg.data[0];
 161              	.loc 1 62 0
 162 0000b2  00 00 00 	nop 
 163 0000b4  BE 03 90 	mov [w14+6],w7
 164 0000b6  00 00 00 	nop 
 165 0000b8  7E 03 90 	mov [w14+14],w6
  63:lib/can-ids/Devices/BMS_VERBOSE_CAN.c ****             master->slave.slave_info_2.discharge_channel_state = msg.data[1];
 166              	.loc 1 63 0
 167 0000ba  00 00 00 	nop 
 168 0000bc  CE 02 90 	mov [w14+8],w5
 169 0000be  00 00 00 	nop 
 170 0000c0  7E 02 90 	mov [w14+14],w4
  64:lib/can-ids/Devices/BMS_VERBOSE_CAN.c ****             master->slave.slave_info_2.heatsink_temperature1 = msg.data[2];
 171              	.loc 1 64 0
 172 0000c2  00 00 00 	nop 
 173 0000c4  DE 01 90 	mov [w14+10],w3
 174 0000c6  00 00 00 	nop 
 175 0000c8  7E 01 90 	mov [w14+14],w2
  65:lib/can-ids/Devices/BMS_VERBOSE_CAN.c ****             master->slave.slave_info_2.heatsink_temperature2 = msg.data[3];
 176              	.loc 1 65 0
 177 0000ca  00 00 00 	nop 
 178 0000cc  EE 00 90 	mov [w14+12],w1
 179 0000ce  00 00 00 	nop 
 180 0000d0  7E 00 90 	mov [w14+14],w0
 181              	.loc 1 62 0
 182 0000d2  67 03 98 	mov w7,[w6+12]
 183              	.loc 1 63 0
 184 0000d4  75 02 98 	mov w5,[w4+14]
 185              	.loc 1 64 0
 186 0000d6  03 09 98 	mov w3,[w2+16]
 187              	.loc 1 65 0
 188 0000d8  11 08 98 	mov w1,[w0+18]
  66:lib/can-ids/Devices/BMS_VERBOSE_CAN.c ****             break;
 189              	.loc 1 66 0
 190 0000da  00 00 37 	bra .L1
 191              	.L3:
  67:lib/can-ids/Devices/BMS_VERBOSE_CAN.c **** 
  68:lib/can-ids/Devices/BMS_VERBOSE_CAN.c ****         case MSG_ID_MASTER_SLAVE_INFO_3:
  69:lib/can-ids/Devices/BMS_VERBOSE_CAN.c ****             id = msg.data[0];
 192              	.loc 1 69 0
 193 0000dc  00 00 00 	nop 
 194 0000de  BE 00 90 	mov [w14+6],w1
 195 0000e0  01 0F 78 	mov w1,[w14]
  70:lib/can-ids/Devices/BMS_VERBOSE_CAN.c ****             if (id >= 12){
 196              	.loc 1 70 0
 197 0000e2  1E 00 78 	mov [w14],w0
 198 0000e4  EB 0F 50 	sub w0,#11,[w15]
 199              	.set ___BP___,0
 200 0000e6  00 00 3E 	bra gtu,.L18
 201              	.L14:
  71:lib/can-ids/Devices/BMS_VERBOSE_CAN.c ****                 return;
  72:lib/can-ids/Devices/BMS_VERBOSE_CAN.c ****             }
  73:lib/can-ids/Devices/BMS_VERBOSE_CAN.c ****             master->slave.slave_info_3.slave_id = msg.data[0];
 202              	.loc 1 73 0
 203 0000e8  00 00 00 	nop 
 204 0000ea  BE 03 90 	mov [w14+6],w7
MPLAB XC16 ASSEMBLY Listing:   			page 6


 205 0000ec  00 00 00 	nop 
 206 0000ee  7E 03 90 	mov [w14+14],w6
  74:lib/can-ids/Devices/BMS_VERBOSE_CAN.c ****             master->slave.slave_info_3.slave_temperature1 = msg.data[1];
 207              	.loc 1 74 0
 208 0000f0  00 00 00 	nop 
 209 0000f2  CE 02 90 	mov [w14+8],w5
 210 0000f4  00 00 00 	nop 
 211 0000f6  7E 02 90 	mov [w14+14],w4
  75:lib/can-ids/Devices/BMS_VERBOSE_CAN.c ****             master->slave.slave_info_3.slave_temperature2 = msg.data[2];
 212              	.loc 1 75 0
 213 0000f8  00 00 00 	nop 
 214 0000fa  DE 01 90 	mov [w14+10],w3
 215 0000fc  00 00 00 	nop 
 216 0000fe  7E 01 90 	mov [w14+14],w2
  76:lib/can-ids/Devices/BMS_VERBOSE_CAN.c ****             master->slave.slave_info_3.BM_temperature = msg.data[3];
 217              	.loc 1 76 0
 218 000100  00 00 00 	nop 
 219 000102  EE 00 90 	mov [w14+12],w1
 220 000104  00 00 00 	nop 
 221 000106  7E 00 90 	mov [w14+14],w0
 222              	.loc 1 73 0
 223 000108  27 0B 98 	mov w7,[w6+20]
 224              	.loc 1 74 0
 225 00010a  35 0A 98 	mov w5,[w4+22]
 226              	.loc 1 75 0
 227 00010c  43 09 98 	mov w3,[w2+24]
 228              	.loc 1 76 0
 229 00010e  51 08 98 	mov w1,[w0+26]
  77:lib/can-ids/Devices/BMS_VERBOSE_CAN.c ****             break;
 230              	.loc 1 77 0
 231 000110  00 00 37 	bra .L1
 232              	.L6:
  78:lib/can-ids/Devices/BMS_VERBOSE_CAN.c **** 
  79:lib/can-ids/Devices/BMS_VERBOSE_CAN.c ****         case MSG_ID_MASTER_CELL_INFO:
  80:lib/can-ids/Devices/BMS_VERBOSE_CAN.c ****             id = msg.data[0] & 0xFF;
 233              	.loc 1 80 0
 234 000112  00 00 00 	nop 
 235 000114  BE 00 90 	mov [w14+6],w1
 236 000116  F0 0F 20 	mov #255,w0
 237 000118  00 8F 60 	and w1,w0,[w14]
  81:lib/can-ids/Devices/BMS_VERBOSE_CAN.c ****             if (id >= 144){
 238              	.loc 1 81 0
 239 00011a  F0 08 20 	mov #143,w0
 240 00011c  9E 00 78 	mov [w14],w1
 241 00011e  80 8F 50 	sub w1,w0,[w15]
 242              	.set ___BP___,0
 243 000120  00 00 3E 	bra gtu,.L19
 244              	.L15:
  82:lib/can-ids/Devices/BMS_VERBOSE_CAN.c ****                 return;
  83:lib/can-ids/Devices/BMS_VERBOSE_CAN.c ****             }
  84:lib/can-ids/Devices/BMS_VERBOSE_CAN.c ****             master->cell.cell_id = (uint8_t)msg.data[0];
 245              	.loc 1 84 0
 246 000122  00 00 00 	nop 
 247 000124  BE 00 90 	mov [w14+6],w1
 248 000126  00 00 00 	nop 
 249 000128  7E 00 90 	mov [w14+14],w0
 250 00012a  00 00 00 	nop 
MPLAB XC16 ASSEMBLY Listing:   			page 7


 251 00012c  81 40 78 	mov.b w1,w1
 252 00012e  00 00 00 	nop 
 253 000130  41 58 98 	mov.b w1,[w0+28]
  85:lib/can-ids/Devices/BMS_VERBOSE_CAN.c ****             master->cell.voltage_state = (msg.data[0]>>8)&0b1111;
 254              	.loc 1 85 0
 255 000132  00 00 00 	nop 
 256 000134  FE 00 90 	mov [w14+14],w1
 257 000136  00 00 00 	nop 
 258 000138  3E 00 90 	mov [w14+6],w0
 259 00013a  00 00 00 	nop 
 260 00013c  E1 09 90 	mov [w1+28],w3
 261 00013e  48 00 DE 	lsr w0,#8,w0
 262 000140  F2 0F 2F 	mov #-3841,w2
 263 000142  00 40 78 	mov.b w0,w0
 264 000144  02 81 61 	and w3,w2,w2
 265 000146  6F 40 60 	and.b w0,#15,w0
 266 000148  6F 40 60 	and.b w0,#15,w0
 267 00014a  6F 40 60 	and.b w0,#15,w0
 268 00014c  00 80 FB 	ze w0,w0
 269 00014e  6F 00 60 	and w0,#15,w0
 270 000150  48 00 DD 	sl w0,#8,w0
 271 000152  02 00 70 	ior w0,w2,w0
 272 000154  E0 08 98 	mov w0,[w1+28]
  86:lib/can-ids/Devices/BMS_VERBOSE_CAN.c ****             master->cell.temperature_state = (msg.data[0]>>12)&0b1111;
 273              	.loc 1 86 0
 274 000156  7E 00 90 	mov [w14+14],w0
 275 000158  BE 00 90 	mov [w14+6],w1
 276 00015a  E0 09 90 	mov [w0+28],w3
 277 00015c  CC 08 DE 	lsr w1,#12,w1
 278 00015e  F2 FF 20 	mov #4095,w2
 279 000160  81 40 78 	mov.b w1,w1
 280 000162  02 81 61 	and w3,w2,w2
 281 000164  EF C0 60 	and.b w1,#15,w1
 282 000166  81 80 FB 	ze w1,w1
 283 000168  CC 08 DD 	sl w1,#12,w1
 284 00016a  82 80 70 	ior w1,w2,w1
 285 00016c  61 08 98 	mov w1,[w0+28]
  87:lib/can-ids/Devices/BMS_VERBOSE_CAN.c ****             master->cell.voltage = msg.data[1];
 286              	.loc 1 87 0
 287 00016e  CE 02 90 	mov [w14+8],w5
 288 000170  7E 02 90 	mov [w14+14],w4
  88:lib/can-ids/Devices/BMS_VERBOSE_CAN.c ****             master->cell.temperature = msg.data[2];
 289              	.loc 1 88 0
 290 000172  DE 01 90 	mov [w14+10],w3
 291 000174  7E 01 90 	mov [w14+14],w2
  89:lib/can-ids/Devices/BMS_VERBOSE_CAN.c ****             master->cell.SoC = msg.data[3];
 292              	.loc 1 89 0
 293 000176  EE 00 90 	mov [w14+12],w1
 294 000178  7E 00 90 	mov [w14+14],w0
 295              	.loc 1 87 0
 296 00017a  75 0A 98 	mov w5,[w4+30]
 297              	.loc 1 88 0
 298 00017c  03 11 98 	mov w3,[w2+32]
 299              	.loc 1 89 0
 300 00017e  11 10 98 	mov w1,[w0+34]
 301 000180  00 00 37 	bra .L1
 302              	.L16:
MPLAB XC16 ASSEMBLY Listing:   			page 8


 303 000182  00 00 37 	bra .L1
 304              	.L17:
 305 000184  00 00 37 	bra .L1
 306              	.L18:
 307 000186  00 00 37 	bra .L1
 308              	.L19:
 309              	.L1:
  90:lib/can-ids/Devices/BMS_VERBOSE_CAN.c ****             break;
  91:lib/can-ids/Devices/BMS_VERBOSE_CAN.c **** 
  92:lib/can-ids/Devices/BMS_VERBOSE_CAN.c ****     }
  93:lib/can-ids/Devices/BMS_VERBOSE_CAN.c **** }
 310              	.loc 1 93 0
 311 000188  00 00 00 	nop 
 312 00018a  4F 04 78 	mov [--w15],w8
 313 00018c  8E 07 78 	mov w14,w15
 314 00018e  4F 07 78 	mov [--w15],w14
 315 000190  00 40 A9 	bclr CORCON,#2
 316 000192  00 00 06 	return 
 317              	.set ___PA___,0
 318              	.LFE0:
 319              	.size _parse_can_message_master_verbose,.-_parse_can_message_master_verbose
 320              	.section .debug_frame,info
 321                 	.Lframe0:
 322 0000 10 00 00 00 	.4byte .LECIE0-.LSCIE0
 323                 	.LSCIE0:
 324 0004 FF FF FF FF 	.4byte 0xffffffff
 325 0008 01          	.byte 0x1
 326 0009 00          	.byte 0
 327 000a 01          	.uleb128 0x1
 328 000b 02          	.sleb128 2
 329 000c 25          	.byte 0x25
 330 000d 12          	.byte 0x12
 331 000e 0F          	.uleb128 0xf
 332 000f 7E          	.sleb128 -2
 333 0010 09          	.byte 0x9
 334 0011 25          	.uleb128 0x25
 335 0012 0F          	.uleb128 0xf
 336 0013 00          	.align 4
 337                 	.LECIE0:
 338                 	.LSFDE0:
 339 0014 1E 00 00 00 	.4byte .LEFDE0-.LASFDE0
 340                 	.LASFDE0:
 341 0018 00 00 00 00 	.4byte .Lframe0
 342 001c 00 00 00 00 	.4byte .LFB0
 343 0020 94 01 00 00 	.4byte .LFE0-.LFB0
 344 0024 04          	.byte 0x4
 345 0025 02 00 00 00 	.4byte .LCFI0-.LFB0
 346 0029 13          	.byte 0x13
 347 002a 7D          	.sleb128 -3
 348 002b 0D          	.byte 0xd
 349 002c 0E          	.uleb128 0xe
 350 002d 8E          	.byte 0x8e
 351 002e 02          	.uleb128 0x2
 352 002f 04          	.byte 0x4
 353 0030 02 00 00 00 	.4byte .LCFI1-.LCFI0
 354 0034 88          	.byte 0x88
 355 0035 0B          	.uleb128 0xb
MPLAB XC16 ASSEMBLY Listing:   			page 9


 356                 	.align 4
 357                 	.LEFDE0:
 358                 	.section .text,code
 359              	.Letext0:
 360              	.file 2 "/opt/microchip/xc16/v1.36/bin/bin/../../include/lega-c/stdint.h"
 361              	.file 3 "lib/can-ids/CAN_IDs.h"
 362              	.file 4 "lib/can-ids/Devices/BMS_VERBOSE_CAN.h"
 363              	.section .debug_info,info
 364 0000 D2 05 00 00 	.4byte 0x5d2
 365 0004 02 00       	.2byte 0x2
 366 0006 00 00 00 00 	.4byte .Ldebug_abbrev0
 367 000a 04          	.byte 0x4
 368 000b 01          	.uleb128 0x1
 369 000c 47 4E 55 20 	.asciz "GNU C 4.5.1 (XC16, Microchip v1.36) (A) Build date: Dec  5 2018"
 369      43 20 34 2E 
 369      35 2E 31 20 
 369      28 58 43 31 
 369      36 2C 20 4D 
 369      69 63 72 6F 
 369      63 68 69 70 
 369      20 76 31 2E 
 369      33 36 29 20 
 370 004c 01          	.byte 0x1
 371 004d 6C 69 62 2F 	.asciz "lib/can-ids/Devices/BMS_VERBOSE_CAN.c"
 371      63 61 6E 2D 
 371      69 64 73 2F 
 371      44 65 76 69 
 371      63 65 73 2F 
 371      42 4D 53 5F 
 371      56 45 52 42 
 371      4F 53 45 5F 
 371      43 41 4E 2E 
 372 0073 2F 68 6F 6D 	.asciz "/home/user/Documents/FST/Programming/project_sub-zero"
 372      65 2F 75 73 
 372      65 72 2F 44 
 372      6F 63 75 6D 
 372      65 6E 74 73 
 372      2F 46 53 54 
 372      2F 50 72 6F 
 372      67 72 61 6D 
 372      6D 69 6E 67 
 373 00a9 00 00 00 00 	.4byte .Ltext0
 374 00ad 00 00 00 00 	.4byte .Letext0
 375 00b1 00 00 00 00 	.4byte .Ldebug_line0
 376 00b5 02          	.uleb128 0x2
 377 00b6 01          	.byte 0x1
 378 00b7 06          	.byte 0x6
 379 00b8 73 69 67 6E 	.asciz "signed char"
 379      65 64 20 63 
 379      68 61 72 00 
 380 00c4 02          	.uleb128 0x2
 381 00c5 02          	.byte 0x2
 382 00c6 05          	.byte 0x5
 383 00c7 69 6E 74 00 	.asciz "int"
 384 00cb 02          	.uleb128 0x2
 385 00cc 04          	.byte 0x4
 386 00cd 05          	.byte 0x5
MPLAB XC16 ASSEMBLY Listing:   			page 10


 387 00ce 6C 6F 6E 67 	.asciz "long int"
 387      20 69 6E 74 
 387      00 
 388 00d7 02          	.uleb128 0x2
 389 00d8 08          	.byte 0x8
 390 00d9 05          	.byte 0x5
 391 00da 6C 6F 6E 67 	.asciz "long long int"
 391      20 6C 6F 6E 
 391      67 20 69 6E 
 391      74 00 
 392 00e8 03          	.uleb128 0x3
 393 00e9 75 69 6E 74 	.asciz "uint8_t"
 393      38 5F 74 00 
 394 00f1 02          	.byte 0x2
 395 00f2 2B          	.byte 0x2b
 396 00f3 F7 00 00 00 	.4byte 0xf7
 397 00f7 02          	.uleb128 0x2
 398 00f8 01          	.byte 0x1
 399 00f9 08          	.byte 0x8
 400 00fa 75 6E 73 69 	.asciz "unsigned char"
 400      67 6E 65 64 
 400      20 63 68 61 
 400      72 00 
 401 0108 03          	.uleb128 0x3
 402 0109 75 69 6E 74 	.asciz "uint16_t"
 402      31 36 5F 74 
 402      00 
 403 0112 02          	.byte 0x2
 404 0113 31          	.byte 0x31
 405 0114 18 01 00 00 	.4byte 0x118
 406 0118 02          	.uleb128 0x2
 407 0119 02          	.byte 0x2
 408 011a 07          	.byte 0x7
 409 011b 75 6E 73 69 	.asciz "unsigned int"
 409      67 6E 65 64 
 409      20 69 6E 74 
 409      00 
 410 0128 02          	.uleb128 0x2
 411 0129 04          	.byte 0x4
 412 012a 07          	.byte 0x7
 413 012b 6C 6F 6E 67 	.asciz "long unsigned int"
 413      20 75 6E 73 
 413      69 67 6E 65 
 413      64 20 69 6E 
 413      74 00 
 414 013d 02          	.uleb128 0x2
 415 013e 08          	.byte 0x8
 416 013f 07          	.byte 0x7
 417 0140 6C 6F 6E 67 	.asciz "long long unsigned int"
 417      20 6C 6F 6E 
 417      67 20 75 6E 
 417      73 69 67 6E 
 417      65 64 20 69 
 417      6E 74 00 
 418 0157 04          	.uleb128 0x4
 419 0158 02          	.byte 0x2
 420 0159 03          	.byte 0x3
MPLAB XC16 ASSEMBLY Listing:   			page 11


 421 015a 12          	.byte 0x12
 422 015b 88 01 00 00 	.4byte 0x188
 423 015f 05          	.uleb128 0x5
 424 0160 64 65 76 5F 	.asciz "dev_id"
 424      69 64 00 
 425 0167 03          	.byte 0x3
 426 0168 13          	.byte 0x13
 427 0169 08 01 00 00 	.4byte 0x108
 428 016d 02          	.byte 0x2
 429 016e 05          	.byte 0x5
 430 016f 0B          	.byte 0xb
 431 0170 02          	.byte 0x2
 432 0171 23          	.byte 0x23
 433 0172 00          	.uleb128 0x0
 434 0173 05          	.uleb128 0x5
 435 0174 6D 73 67 5F 	.asciz "msg_id"
 435      69 64 00 
 436 017b 03          	.byte 0x3
 437 017c 16          	.byte 0x16
 438 017d 08 01 00 00 	.4byte 0x108
 439 0181 02          	.byte 0x2
 440 0182 06          	.byte 0x6
 441 0183 05          	.byte 0x5
 442 0184 02          	.byte 0x2
 443 0185 23          	.byte 0x23
 444 0186 00          	.uleb128 0x0
 445 0187 00          	.byte 0x0
 446 0188 06          	.uleb128 0x6
 447 0189 02          	.byte 0x2
 448 018a 03          	.byte 0x3
 449 018b 11          	.byte 0x11
 450 018c A1 01 00 00 	.4byte 0x1a1
 451 0190 07          	.uleb128 0x7
 452 0191 57 01 00 00 	.4byte 0x157
 453 0195 08          	.uleb128 0x8
 454 0196 73 69 64 00 	.asciz "sid"
 455 019a 03          	.byte 0x3
 456 019b 18          	.byte 0x18
 457 019c 08 01 00 00 	.4byte 0x108
 458 01a0 00          	.byte 0x0
 459 01a1 04          	.uleb128 0x4
 460 01a2 0C          	.byte 0xc
 461 01a3 03          	.byte 0x3
 462 01a4 10          	.byte 0x10
 463 01a5 D2 01 00 00 	.4byte 0x1d2
 464 01a9 09          	.uleb128 0x9
 465 01aa 88 01 00 00 	.4byte 0x188
 466 01ae 02          	.byte 0x2
 467 01af 23          	.byte 0x23
 468 01b0 00          	.uleb128 0x0
 469 01b1 05          	.uleb128 0x5
 470 01b2 64 6C 63 00 	.asciz "dlc"
 471 01b6 03          	.byte 0x3
 472 01b7 1A          	.byte 0x1a
 473 01b8 08 01 00 00 	.4byte 0x108
 474 01bc 02          	.byte 0x2
 475 01bd 04          	.byte 0x4
MPLAB XC16 ASSEMBLY Listing:   			page 12


 476 01be 0C          	.byte 0xc
 477 01bf 02          	.byte 0x2
 478 01c0 23          	.byte 0x23
 479 01c1 02          	.uleb128 0x2
 480 01c2 0A          	.uleb128 0xa
 481 01c3 64 61 74 61 	.asciz "data"
 481      00 
 482 01c8 03          	.byte 0x3
 483 01c9 1B          	.byte 0x1b
 484 01ca D2 01 00 00 	.4byte 0x1d2
 485 01ce 02          	.byte 0x2
 486 01cf 23          	.byte 0x23
 487 01d0 04          	.uleb128 0x4
 488 01d1 00          	.byte 0x0
 489 01d2 0B          	.uleb128 0xb
 490 01d3 08 01 00 00 	.4byte 0x108
 491 01d7 E2 01 00 00 	.4byte 0x1e2
 492 01db 0C          	.uleb128 0xc
 493 01dc 18 01 00 00 	.4byte 0x118
 494 01e0 03          	.byte 0x3
 495 01e1 00          	.byte 0x0
 496 01e2 03          	.uleb128 0x3
 497 01e3 43 41 4E 64 	.asciz "CANdata"
 497      61 74 61 00 
 498 01eb 03          	.byte 0x3
 499 01ec 1C          	.byte 0x1c
 500 01ed A1 01 00 00 	.4byte 0x1a1
 501 01f1 02          	.uleb128 0x2
 502 01f2 01          	.byte 0x1
 503 01f3 02          	.byte 0x2
 504 01f4 5F 42 6F 6F 	.asciz "_Bool"
 504      6C 00 
 505 01fa 04          	.uleb128 0x4
 506 01fb 08          	.byte 0x8
 507 01fc 04          	.byte 0x4
 508 01fd 12          	.byte 0x12
 509 01fe 85 02 00 00 	.4byte 0x285
 510 0202 0A          	.uleb128 0xa
 511 0203 63 65 6C 6C 	.asciz "cell_id"
 511      5F 69 64 00 
 512 020b 04          	.byte 0x4
 513 020c 13          	.byte 0x13
 514 020d E8 00 00 00 	.4byte 0xe8
 515 0211 02          	.byte 0x2
 516 0212 23          	.byte 0x23
 517 0213 00          	.uleb128 0x0
 518 0214 05          	.uleb128 0x5
 519 0215 76 6F 6C 74 	.asciz "voltage_state"
 519      61 67 65 5F 
 519      73 74 61 74 
 519      65 00 
 520 0223 04          	.byte 0x4
 521 0224 14          	.byte 0x14
 522 0225 E8 00 00 00 	.4byte 0xe8
 523 0229 01          	.byte 0x1
 524 022a 04          	.byte 0x4
 525 022b 04          	.byte 0x4
MPLAB XC16 ASSEMBLY Listing:   			page 13


 526 022c 02          	.byte 0x2
 527 022d 23          	.byte 0x23
 528 022e 01          	.uleb128 0x1
 529 022f 05          	.uleb128 0x5
 530 0230 74 65 6D 70 	.asciz "temperature_state"
 530      65 72 61 74 
 530      75 72 65 5F 
 530      73 74 61 74 
 530      65 00 
 531 0242 04          	.byte 0x4
 532 0243 15          	.byte 0x15
 533 0244 E8 00 00 00 	.4byte 0xe8
 534 0248 01          	.byte 0x1
 535 0249 04          	.byte 0x4
 536 024a 08          	.byte 0x8
 537 024b 02          	.byte 0x2
 538 024c 23          	.byte 0x23
 539 024d 01          	.uleb128 0x1
 540 024e 0A          	.uleb128 0xa
 541 024f 76 6F 6C 74 	.asciz "voltage"
 541      61 67 65 00 
 542 0257 04          	.byte 0x4
 543 0258 16          	.byte 0x16
 544 0259 08 01 00 00 	.4byte 0x108
 545 025d 02          	.byte 0x2
 546 025e 23          	.byte 0x23
 547 025f 02          	.uleb128 0x2
 548 0260 0A          	.uleb128 0xa
 549 0261 74 65 6D 70 	.asciz "temperature"
 549      65 72 61 74 
 549      75 72 65 00 
 550 026d 04          	.byte 0x4
 551 026e 17          	.byte 0x17
 552 026f 08 01 00 00 	.4byte 0x108
 553 0273 02          	.byte 0x2
 554 0274 23          	.byte 0x23
 555 0275 04          	.uleb128 0x4
 556 0276 0A          	.uleb128 0xa
 557 0277 53 6F 43 00 	.asciz "SoC"
 558 027b 04          	.byte 0x4
 559 027c 18          	.byte 0x18
 560 027d 08 01 00 00 	.4byte 0x108
 561 0281 02          	.byte 0x2
 562 0282 23          	.byte 0x23
 563 0283 06          	.uleb128 0x6
 564 0284 00          	.byte 0x0
 565 0285 03          	.uleb128 0x3
 566 0286 4D 41 53 54 	.asciz "MASTER_MSG_Cell_Info"
 566      45 52 5F 4D 
 566      53 47 5F 43 
 566      65 6C 6C 5F 
 566      49 6E 66 6F 
 566      00 
 567 029b 04          	.byte 0x4
 568 029c 19          	.byte 0x19
 569 029d FA 01 00 00 	.4byte 0x1fa
 570 02a1 04          	.uleb128 0x4
MPLAB XC16 ASSEMBLY Listing:   			page 14


 571 02a2 0C          	.byte 0xc
 572 02a3 04          	.byte 0x4
 573 02a4 21          	.byte 0x21
 574 02a5 99 03 00 00 	.4byte 0x399
 575 02a9 0D          	.uleb128 0xd
 576 02aa 00 00 00 00 	.4byte .LASF0
 577 02ae 04          	.byte 0x4
 578 02af 22          	.byte 0x22
 579 02b0 08 01 00 00 	.4byte 0x108
 580 02b4 02          	.byte 0x2
 581 02b5 23          	.byte 0x23
 582 02b6 00          	.uleb128 0x0
 583 02b7 0A          	.uleb128 0xa
 584 02b8 64 69 73 63 	.asciz "discharge_channel_fault_mask"
 584      68 61 72 67 
 584      65 5F 63 68 
 584      61 6E 6E 65 
 584      6C 5F 66 61 
 584      75 6C 74 5F 
 584      6D 61 73 6B 
 584      00 
 585 02d5 04          	.byte 0x4
 586 02d6 23          	.byte 0x23
 587 02d7 08 01 00 00 	.4byte 0x108
 588 02db 02          	.byte 0x2
 589 02dc 23          	.byte 0x23
 590 02dd 02          	.uleb128 0x2
 591 02de 0A          	.uleb128 0xa
 592 02df 63 65 6C 6C 	.asciz "cell_connection_fault_mask"
 592      5F 63 6F 6E 
 592      6E 65 63 74 
 592      69 6F 6E 5F 
 592      66 61 75 6C 
 592      74 5F 6D 61 
 592      73 6B 00 
 593 02fa 04          	.byte 0x4
 594 02fb 24          	.byte 0x24
 595 02fc 08 01 00 00 	.4byte 0x108
 596 0300 02          	.byte 0x2
 597 0301 23          	.byte 0x23
 598 0302 04          	.uleb128 0x4
 599 0303 0A          	.uleb128 0xa
 600 0304 63 65 6C 6C 	.asciz "cell_temp_fault_mask"
 600      5F 74 65 6D 
 600      70 5F 66 61 
 600      75 6C 74 5F 
 600      6D 61 73 6B 
 600      00 
 601 0319 04          	.byte 0x4
 602 031a 25          	.byte 0x25
 603 031b 08 01 00 00 	.4byte 0x108
 604 031f 02          	.byte 0x2
 605 0320 23          	.byte 0x23
 606 0321 06          	.uleb128 0x6
 607 0322 0A          	.uleb128 0xa
 608 0323 73 6C 61 76 	.asciz "slave_temp1_fault"
 608      65 5F 74 65 
MPLAB XC16 ASSEMBLY Listing:   			page 15


 608      6D 70 31 5F 
 608      66 61 75 6C 
 608      74 00 
 609 0335 04          	.byte 0x4
 610 0336 26          	.byte 0x26
 611 0337 F1 01 00 00 	.4byte 0x1f1
 612 033b 02          	.byte 0x2
 613 033c 23          	.byte 0x23
 614 033d 08          	.uleb128 0x8
 615 033e 0A          	.uleb128 0xa
 616 033f 73 6C 61 76 	.asciz "slave_temp2_fault"
 616      65 5F 74 65 
 616      6D 70 32 5F 
 616      66 61 75 6C 
 616      74 00 
 617 0351 04          	.byte 0x4
 618 0352 27          	.byte 0x27
 619 0353 F1 01 00 00 	.4byte 0x1f1
 620 0357 02          	.byte 0x2
 621 0358 23          	.byte 0x23
 622 0359 09          	.uleb128 0x9
 623 035a 0A          	.uleb128 0xa
 624 035b 68 65 61 74 	.asciz "heatsink_temp1_fault"
 624      73 69 6E 6B 
 624      5F 74 65 6D 
 624      70 31 5F 66 
 624      61 75 6C 74 
 624      00 
 625 0370 04          	.byte 0x4
 626 0371 28          	.byte 0x28
 627 0372 F1 01 00 00 	.4byte 0x1f1
 628 0376 02          	.byte 0x2
 629 0377 23          	.byte 0x23
 630 0378 0A          	.uleb128 0xa
 631 0379 0A          	.uleb128 0xa
 632 037a 68 65 61 74 	.asciz "heatsink_temp2_fault"
 632      73 69 6E 6B 
 632      5F 74 65 6D 
 632      70 32 5F 66 
 632      61 75 6C 74 
 632      00 
 633 038f 04          	.byte 0x4
 634 0390 29          	.byte 0x29
 635 0391 F1 01 00 00 	.4byte 0x1f1
 636 0395 02          	.byte 0x2
 637 0396 23          	.byte 0x23
 638 0397 0B          	.uleb128 0xb
 639 0398 00          	.byte 0x0
 640 0399 03          	.uleb128 0x3
 641 039a 4D 41 53 54 	.asciz "MASTER_MSG_Slave_Info_1"
 641      45 52 5F 4D 
 641      53 47 5F 53 
 641      6C 61 76 65 
 641      5F 49 6E 66 
 641      6F 5F 31 00 
 642 03b2 04          	.byte 0x4
 643 03b3 2A          	.byte 0x2a
MPLAB XC16 ASSEMBLY Listing:   			page 16


 644 03b4 A1 02 00 00 	.4byte 0x2a1
 645 03b8 04          	.uleb128 0x4
 646 03b9 08          	.byte 0x8
 647 03ba 04          	.byte 0x4
 648 03bb 32          	.byte 0x32
 649 03bc 31 04 00 00 	.4byte 0x431
 650 03c0 0D          	.uleb128 0xd
 651 03c1 00 00 00 00 	.4byte .LASF0
 652 03c5 04          	.byte 0x4
 653 03c6 33          	.byte 0x33
 654 03c7 08 01 00 00 	.4byte 0x108
 655 03cb 02          	.byte 0x2
 656 03cc 23          	.byte 0x23
 657 03cd 00          	.uleb128 0x0
 658 03ce 0A          	.uleb128 0xa
 659 03cf 64 69 73 63 	.asciz "discharge_channel_state"
 659      68 61 72 67 
 659      65 5F 63 68 
 659      61 6E 6E 65 
 659      6C 5F 73 74 
 659      61 74 65 00 
 660 03e7 04          	.byte 0x4
 661 03e8 34          	.byte 0x34
 662 03e9 08 01 00 00 	.4byte 0x108
 663 03ed 02          	.byte 0x2
 664 03ee 23          	.byte 0x23
 665 03ef 02          	.uleb128 0x2
 666 03f0 0A          	.uleb128 0xa
 667 03f1 68 65 61 74 	.asciz "heatsink_temperature1"
 667      73 69 6E 6B 
 667      5F 74 65 6D 
 667      70 65 72 61 
 667      74 75 72 65 
 667      31 00 
 668 0407 04          	.byte 0x4
 669 0408 35          	.byte 0x35
 670 0409 08 01 00 00 	.4byte 0x108
 671 040d 02          	.byte 0x2
 672 040e 23          	.byte 0x23
 673 040f 04          	.uleb128 0x4
 674 0410 0A          	.uleb128 0xa
 675 0411 68 65 61 74 	.asciz "heatsink_temperature2"
 675      73 69 6E 6B 
 675      5F 74 65 6D 
 675      70 65 72 61 
 675      74 75 72 65 
 675      32 00 
 676 0427 04          	.byte 0x4
 677 0428 36          	.byte 0x36
 678 0429 08 01 00 00 	.4byte 0x108
 679 042d 02          	.byte 0x2
 680 042e 23          	.byte 0x23
 681 042f 06          	.uleb128 0x6
 682 0430 00          	.byte 0x0
 683 0431 03          	.uleb128 0x3
 684 0432 4D 41 53 54 	.asciz "MASTER_MSG_Slave_Info_2"
 684      45 52 5F 4D 
MPLAB XC16 ASSEMBLY Listing:   			page 17


 684      53 47 5F 53 
 684      6C 61 76 65 
 684      5F 49 6E 66 
 684      6F 5F 32 00 
 685 044a 04          	.byte 0x4
 686 044b 37          	.byte 0x37
 687 044c B8 03 00 00 	.4byte 0x3b8
 688 0450 04          	.uleb128 0x4
 689 0451 08          	.byte 0x8
 690 0452 04          	.byte 0x4
 691 0453 3F          	.byte 0x3f
 692 0454 BA 04 00 00 	.4byte 0x4ba
 693 0458 0D          	.uleb128 0xd
 694 0459 00 00 00 00 	.4byte .LASF0
 695 045d 04          	.byte 0x4
 696 045e 40          	.byte 0x40
 697 045f 08 01 00 00 	.4byte 0x108
 698 0463 02          	.byte 0x2
 699 0464 23          	.byte 0x23
 700 0465 00          	.uleb128 0x0
 701 0466 0A          	.uleb128 0xa
 702 0467 73 6C 61 76 	.asciz "slave_temperature1"
 702      65 5F 74 65 
 702      6D 70 65 72 
 702      61 74 75 72 
 702      65 31 00 
 703 047a 04          	.byte 0x4
 704 047b 41          	.byte 0x41
 705 047c 08 01 00 00 	.4byte 0x108
 706 0480 02          	.byte 0x2
 707 0481 23          	.byte 0x23
 708 0482 02          	.uleb128 0x2
 709 0483 0A          	.uleb128 0xa
 710 0484 73 6C 61 76 	.asciz "slave_temperature2"
 710      65 5F 74 65 
 710      6D 70 65 72 
 710      61 74 75 72 
 710      65 32 00 
 711 0497 04          	.byte 0x4
 712 0498 42          	.byte 0x42
 713 0499 08 01 00 00 	.4byte 0x108
 714 049d 02          	.byte 0x2
 715 049e 23          	.byte 0x23
 716 049f 04          	.uleb128 0x4
 717 04a0 0A          	.uleb128 0xa
 718 04a1 42 4D 5F 74 	.asciz "BM_temperature"
 718      65 6D 70 65 
 718      72 61 74 75 
 718      72 65 00 
 719 04b0 04          	.byte 0x4
 720 04b1 43          	.byte 0x43
 721 04b2 08 01 00 00 	.4byte 0x108
 722 04b6 02          	.byte 0x2
 723 04b7 23          	.byte 0x23
 724 04b8 06          	.uleb128 0x6
 725 04b9 00          	.byte 0x0
 726 04ba 03          	.uleb128 0x3
MPLAB XC16 ASSEMBLY Listing:   			page 18


 727 04bb 4D 41 53 54 	.asciz "MASTER_MSG_Slave_Info_3"
 727      45 52 5F 4D 
 727      53 47 5F 53 
 727      6C 61 76 65 
 727      5F 49 6E 66 
 727      6F 5F 33 00 
 728 04d3 04          	.byte 0x4
 729 04d4 44          	.byte 0x44
 730 04d5 50 04 00 00 	.4byte 0x450
 731 04d9 04          	.uleb128 0x4
 732 04da 1C          	.byte 0x1c
 733 04db 04          	.byte 0x4
 734 04dc 4D          	.byte 0x4d
 735 04dd 27 05 00 00 	.4byte 0x527
 736 04e1 0A          	.uleb128 0xa
 737 04e2 73 6C 61 76 	.asciz "slave_info_1"
 737      65 5F 69 6E 
 737      66 6F 5F 31 
 737      00 
 738 04ef 04          	.byte 0x4
 739 04f0 4E          	.byte 0x4e
 740 04f1 99 03 00 00 	.4byte 0x399
 741 04f5 02          	.byte 0x2
 742 04f6 23          	.byte 0x23
 743 04f7 00          	.uleb128 0x0
 744 04f8 0A          	.uleb128 0xa
 745 04f9 73 6C 61 76 	.asciz "slave_info_2"
 745      65 5F 69 6E 
 745      66 6F 5F 32 
 745      00 
 746 0506 04          	.byte 0x4
 747 0507 4F          	.byte 0x4f
 748 0508 31 04 00 00 	.4byte 0x431
 749 050c 02          	.byte 0x2
 750 050d 23          	.byte 0x23
 751 050e 0C          	.uleb128 0xc
 752 050f 0A          	.uleb128 0xa
 753 0510 73 6C 61 76 	.asciz "slave_info_3"
 753      65 5F 69 6E 
 753      66 6F 5F 33 
 753      00 
 754 051d 04          	.byte 0x4
 755 051e 50          	.byte 0x50
 756 051f BA 04 00 00 	.4byte 0x4ba
 757 0523 02          	.byte 0x2
 758 0524 23          	.byte 0x23
 759 0525 14          	.uleb128 0x14
 760 0526 00          	.byte 0x0
 761 0527 04          	.uleb128 0x4
 762 0528 24          	.byte 0x24
 763 0529 04          	.byte 0x4
 764 052a 4C          	.byte 0x4c
 765 052b 4F 05 00 00 	.4byte 0x54f
 766 052f 0A          	.uleb128 0xa
 767 0530 73 6C 61 76 	.asciz "slave"
 767      65 00 
 768 0536 04          	.byte 0x4
MPLAB XC16 ASSEMBLY Listing:   			page 19


 769 0537 51          	.byte 0x51
 770 0538 D9 04 00 00 	.4byte 0x4d9
 771 053c 02          	.byte 0x2
 772 053d 23          	.byte 0x23
 773 053e 00          	.uleb128 0x0
 774 053f 0A          	.uleb128 0xa
 775 0540 63 65 6C 6C 	.asciz "cell"
 775      00 
 776 0545 04          	.byte 0x4
 777 0546 52          	.byte 0x52
 778 0547 85 02 00 00 	.4byte 0x285
 779 054b 02          	.byte 0x2
 780 054c 23          	.byte 0x23
 781 054d 1C          	.uleb128 0x1c
 782 054e 00          	.byte 0x0
 783 054f 03          	.uleb128 0x3
 784 0550 4D 41 53 54 	.asciz "MASTER_VERBOSE_MSG_Data"
 784      45 52 5F 56 
 784      45 52 42 4F 
 784      53 45 5F 4D 
 784      53 47 5F 44 
 784      61 74 61 00 
 785 0568 04          	.byte 0x4
 786 0569 54          	.byte 0x54
 787 056a 27 05 00 00 	.4byte 0x527
 788 056e 0E          	.uleb128 0xe
 789 056f 01          	.byte 0x1
 790 0570 70 61 72 73 	.asciz "parse_can_message_master_verbose"
 790      65 5F 63 61 
 790      6E 5F 6D 65 
 790      73 73 61 67 
 790      65 5F 6D 61 
 790      73 74 65 72 
 790      5F 76 65 72 
 790      62 6F 73 65 
 790      00 
 791 0591 01          	.byte 0x1
 792 0592 25          	.byte 0x25
 793 0593 01          	.byte 0x1
 794 0594 00 00 00 00 	.4byte .LFB0
 795 0598 00 00 00 00 	.4byte .LFE0
 796 059c 01          	.byte 0x1
 797 059d 5E          	.byte 0x5e
 798 059e CF 05 00 00 	.4byte 0x5cf
 799 05a2 0F          	.uleb128 0xf
 800 05a3 6D 73 67 00 	.asciz "msg"
 801 05a7 01          	.byte 0x1
 802 05a8 25          	.byte 0x25
 803 05a9 E2 01 00 00 	.4byte 0x1e2
 804 05ad 02          	.byte 0x2
 805 05ae 7E          	.byte 0x7e
 806 05af 02          	.sleb128 2
 807 05b0 0F          	.uleb128 0xf
 808 05b1 6D 61 73 74 	.asciz "master"
 808      65 72 00 
 809 05b8 01          	.byte 0x1
 810 05b9 25          	.byte 0x25
MPLAB XC16 ASSEMBLY Listing:   			page 20


 811 05ba CF 05 00 00 	.4byte 0x5cf
 812 05be 02          	.byte 0x2
 813 05bf 7E          	.byte 0x7e
 814 05c0 0E          	.sleb128 14
 815 05c1 10          	.uleb128 0x10
 816 05c2 69 64 00    	.asciz "id"
 817 05c5 01          	.byte 0x1
 818 05c6 26          	.byte 0x26
 819 05c7 08 01 00 00 	.4byte 0x108
 820 05cb 02          	.byte 0x2
 821 05cc 7E          	.byte 0x7e
 822 05cd 00          	.sleb128 0
 823 05ce 00          	.byte 0x0
 824 05cf 11          	.uleb128 0x11
 825 05d0 02          	.byte 0x2
 826 05d1 4F 05 00 00 	.4byte 0x54f
 827 05d5 00          	.byte 0x0
 828                 	.section .debug_abbrev,info
 829 0000 01          	.uleb128 0x1
 830 0001 11          	.uleb128 0x11
 831 0002 01          	.byte 0x1
 832 0003 25          	.uleb128 0x25
 833 0004 08          	.uleb128 0x8
 834 0005 13          	.uleb128 0x13
 835 0006 0B          	.uleb128 0xb
 836 0007 03          	.uleb128 0x3
 837 0008 08          	.uleb128 0x8
 838 0009 1B          	.uleb128 0x1b
 839 000a 08          	.uleb128 0x8
 840 000b 11          	.uleb128 0x11
 841 000c 01          	.uleb128 0x1
 842 000d 12          	.uleb128 0x12
 843 000e 01          	.uleb128 0x1
 844 000f 10          	.uleb128 0x10
 845 0010 06          	.uleb128 0x6
 846 0011 00          	.byte 0x0
 847 0012 00          	.byte 0x0
 848 0013 02          	.uleb128 0x2
 849 0014 24          	.uleb128 0x24
 850 0015 00          	.byte 0x0
 851 0016 0B          	.uleb128 0xb
 852 0017 0B          	.uleb128 0xb
 853 0018 3E          	.uleb128 0x3e
 854 0019 0B          	.uleb128 0xb
 855 001a 03          	.uleb128 0x3
 856 001b 08          	.uleb128 0x8
 857 001c 00          	.byte 0x0
 858 001d 00          	.byte 0x0
 859 001e 03          	.uleb128 0x3
 860 001f 16          	.uleb128 0x16
 861 0020 00          	.byte 0x0
 862 0021 03          	.uleb128 0x3
 863 0022 08          	.uleb128 0x8
 864 0023 3A          	.uleb128 0x3a
 865 0024 0B          	.uleb128 0xb
 866 0025 3B          	.uleb128 0x3b
 867 0026 0B          	.uleb128 0xb
MPLAB XC16 ASSEMBLY Listing:   			page 21


 868 0027 49          	.uleb128 0x49
 869 0028 13          	.uleb128 0x13
 870 0029 00          	.byte 0x0
 871 002a 00          	.byte 0x0
 872 002b 04          	.uleb128 0x4
 873 002c 13          	.uleb128 0x13
 874 002d 01          	.byte 0x1
 875 002e 0B          	.uleb128 0xb
 876 002f 0B          	.uleb128 0xb
 877 0030 3A          	.uleb128 0x3a
 878 0031 0B          	.uleb128 0xb
 879 0032 3B          	.uleb128 0x3b
 880 0033 0B          	.uleb128 0xb
 881 0034 01          	.uleb128 0x1
 882 0035 13          	.uleb128 0x13
 883 0036 00          	.byte 0x0
 884 0037 00          	.byte 0x0
 885 0038 05          	.uleb128 0x5
 886 0039 0D          	.uleb128 0xd
 887 003a 00          	.byte 0x0
 888 003b 03          	.uleb128 0x3
 889 003c 08          	.uleb128 0x8
 890 003d 3A          	.uleb128 0x3a
 891 003e 0B          	.uleb128 0xb
 892 003f 3B          	.uleb128 0x3b
 893 0040 0B          	.uleb128 0xb
 894 0041 49          	.uleb128 0x49
 895 0042 13          	.uleb128 0x13
 896 0043 0B          	.uleb128 0xb
 897 0044 0B          	.uleb128 0xb
 898 0045 0D          	.uleb128 0xd
 899 0046 0B          	.uleb128 0xb
 900 0047 0C          	.uleb128 0xc
 901 0048 0B          	.uleb128 0xb
 902 0049 38          	.uleb128 0x38
 903 004a 0A          	.uleb128 0xa
 904 004b 00          	.byte 0x0
 905 004c 00          	.byte 0x0
 906 004d 06          	.uleb128 0x6
 907 004e 17          	.uleb128 0x17
 908 004f 01          	.byte 0x1
 909 0050 0B          	.uleb128 0xb
 910 0051 0B          	.uleb128 0xb
 911 0052 3A          	.uleb128 0x3a
 912 0053 0B          	.uleb128 0xb
 913 0054 3B          	.uleb128 0x3b
 914 0055 0B          	.uleb128 0xb
 915 0056 01          	.uleb128 0x1
 916 0057 13          	.uleb128 0x13
 917 0058 00          	.byte 0x0
 918 0059 00          	.byte 0x0
 919 005a 07          	.uleb128 0x7
 920 005b 0D          	.uleb128 0xd
 921 005c 00          	.byte 0x0
 922 005d 49          	.uleb128 0x49
 923 005e 13          	.uleb128 0x13
 924 005f 00          	.byte 0x0
MPLAB XC16 ASSEMBLY Listing:   			page 22


 925 0060 00          	.byte 0x0
 926 0061 08          	.uleb128 0x8
 927 0062 0D          	.uleb128 0xd
 928 0063 00          	.byte 0x0
 929 0064 03          	.uleb128 0x3
 930 0065 08          	.uleb128 0x8
 931 0066 3A          	.uleb128 0x3a
 932 0067 0B          	.uleb128 0xb
 933 0068 3B          	.uleb128 0x3b
 934 0069 0B          	.uleb128 0xb
 935 006a 49          	.uleb128 0x49
 936 006b 13          	.uleb128 0x13
 937 006c 00          	.byte 0x0
 938 006d 00          	.byte 0x0
 939 006e 09          	.uleb128 0x9
 940 006f 0D          	.uleb128 0xd
 941 0070 00          	.byte 0x0
 942 0071 49          	.uleb128 0x49
 943 0072 13          	.uleb128 0x13
 944 0073 38          	.uleb128 0x38
 945 0074 0A          	.uleb128 0xa
 946 0075 00          	.byte 0x0
 947 0076 00          	.byte 0x0
 948 0077 0A          	.uleb128 0xa
 949 0078 0D          	.uleb128 0xd
 950 0079 00          	.byte 0x0
 951 007a 03          	.uleb128 0x3
 952 007b 08          	.uleb128 0x8
 953 007c 3A          	.uleb128 0x3a
 954 007d 0B          	.uleb128 0xb
 955 007e 3B          	.uleb128 0x3b
 956 007f 0B          	.uleb128 0xb
 957 0080 49          	.uleb128 0x49
 958 0081 13          	.uleb128 0x13
 959 0082 38          	.uleb128 0x38
 960 0083 0A          	.uleb128 0xa
 961 0084 00          	.byte 0x0
 962 0085 00          	.byte 0x0
 963 0086 0B          	.uleb128 0xb
 964 0087 01          	.uleb128 0x1
 965 0088 01          	.byte 0x1
 966 0089 49          	.uleb128 0x49
 967 008a 13          	.uleb128 0x13
 968 008b 01          	.uleb128 0x1
 969 008c 13          	.uleb128 0x13
 970 008d 00          	.byte 0x0
 971 008e 00          	.byte 0x0
 972 008f 0C          	.uleb128 0xc
 973 0090 21          	.uleb128 0x21
 974 0091 00          	.byte 0x0
 975 0092 49          	.uleb128 0x49
 976 0093 13          	.uleb128 0x13
 977 0094 2F          	.uleb128 0x2f
 978 0095 0B          	.uleb128 0xb
 979 0096 00          	.byte 0x0
 980 0097 00          	.byte 0x0
 981 0098 0D          	.uleb128 0xd
MPLAB XC16 ASSEMBLY Listing:   			page 23


 982 0099 0D          	.uleb128 0xd
 983 009a 00          	.byte 0x0
 984 009b 03          	.uleb128 0x3
 985 009c 0E          	.uleb128 0xe
 986 009d 3A          	.uleb128 0x3a
 987 009e 0B          	.uleb128 0xb
 988 009f 3B          	.uleb128 0x3b
 989 00a0 0B          	.uleb128 0xb
 990 00a1 49          	.uleb128 0x49
 991 00a2 13          	.uleb128 0x13
 992 00a3 38          	.uleb128 0x38
 993 00a4 0A          	.uleb128 0xa
 994 00a5 00          	.byte 0x0
 995 00a6 00          	.byte 0x0
 996 00a7 0E          	.uleb128 0xe
 997 00a8 2E          	.uleb128 0x2e
 998 00a9 01          	.byte 0x1
 999 00aa 3F          	.uleb128 0x3f
 1000 00ab 0C          	.uleb128 0xc
 1001 00ac 03          	.uleb128 0x3
 1002 00ad 08          	.uleb128 0x8
 1003 00ae 3A          	.uleb128 0x3a
 1004 00af 0B          	.uleb128 0xb
 1005 00b0 3B          	.uleb128 0x3b
 1006 00b1 0B          	.uleb128 0xb
 1007 00b2 27          	.uleb128 0x27
 1008 00b3 0C          	.uleb128 0xc
 1009 00b4 11          	.uleb128 0x11
 1010 00b5 01          	.uleb128 0x1
 1011 00b6 12          	.uleb128 0x12
 1012 00b7 01          	.uleb128 0x1
 1013 00b8 40          	.uleb128 0x40
 1014 00b9 0A          	.uleb128 0xa
 1015 00ba 01          	.uleb128 0x1
 1016 00bb 13          	.uleb128 0x13
 1017 00bc 00          	.byte 0x0
 1018 00bd 00          	.byte 0x0
 1019 00be 0F          	.uleb128 0xf
 1020 00bf 05          	.uleb128 0x5
 1021 00c0 00          	.byte 0x0
 1022 00c1 03          	.uleb128 0x3
 1023 00c2 08          	.uleb128 0x8
 1024 00c3 3A          	.uleb128 0x3a
 1025 00c4 0B          	.uleb128 0xb
 1026 00c5 3B          	.uleb128 0x3b
 1027 00c6 0B          	.uleb128 0xb
 1028 00c7 49          	.uleb128 0x49
 1029 00c8 13          	.uleb128 0x13
 1030 00c9 02          	.uleb128 0x2
 1031 00ca 0A          	.uleb128 0xa
 1032 00cb 00          	.byte 0x0
 1033 00cc 00          	.byte 0x0
 1034 00cd 10          	.uleb128 0x10
 1035 00ce 34          	.uleb128 0x34
 1036 00cf 00          	.byte 0x0
 1037 00d0 03          	.uleb128 0x3
 1038 00d1 08          	.uleb128 0x8
MPLAB XC16 ASSEMBLY Listing:   			page 24


 1039 00d2 3A          	.uleb128 0x3a
 1040 00d3 0B          	.uleb128 0xb
 1041 00d4 3B          	.uleb128 0x3b
 1042 00d5 0B          	.uleb128 0xb
 1043 00d6 49          	.uleb128 0x49
 1044 00d7 13          	.uleb128 0x13
 1045 00d8 02          	.uleb128 0x2
 1046 00d9 0A          	.uleb128 0xa
 1047 00da 00          	.byte 0x0
 1048 00db 00          	.byte 0x0
 1049 00dc 11          	.uleb128 0x11
 1050 00dd 0F          	.uleb128 0xf
 1051 00de 00          	.byte 0x0
 1052 00df 0B          	.uleb128 0xb
 1053 00e0 0B          	.uleb128 0xb
 1054 00e1 49          	.uleb128 0x49
 1055 00e2 13          	.uleb128 0x13
 1056 00e3 00          	.byte 0x0
 1057 00e4 00          	.byte 0x0
 1058 00e5 00          	.byte 0x0
 1059                 	.section .debug_pubnames,info
 1060 0000 33 00 00 00 	.4byte 0x33
 1061 0004 02 00       	.2byte 0x2
 1062 0006 00 00 00 00 	.4byte .Ldebug_info0
 1063 000a D6 05 00 00 	.4byte 0x5d6
 1064 000e 6E 05 00 00 	.4byte 0x56e
 1065 0012 70 61 72 73 	.asciz "parse_can_message_master_verbose"
 1065      65 5F 63 61 
 1065      6E 5F 6D 65 
 1065      73 73 61 67 
 1065      65 5F 6D 61 
 1065      73 74 65 72 
 1065      5F 76 65 72 
 1065      62 6F 73 65 
 1065      00 
 1066 0033 00 00 00 00 	.4byte 0x0
 1067                 	.section .debug_pubtypes,info
 1068 0000 BC 00 00 00 	.4byte 0xbc
 1069 0004 02 00       	.2byte 0x2
 1070 0006 00 00 00 00 	.4byte .Ldebug_info0
 1071 000a D6 05 00 00 	.4byte 0x5d6
 1072 000e E8 00 00 00 	.4byte 0xe8
 1073 0012 75 69 6E 74 	.asciz "uint8_t"
 1073      38 5F 74 00 
 1074 001a 08 01 00 00 	.4byte 0x108
 1075 001e 75 69 6E 74 	.asciz "uint16_t"
 1075      31 36 5F 74 
 1075      00 
 1076 0027 E2 01 00 00 	.4byte 0x1e2
 1077 002b 43 41 4E 64 	.asciz "CANdata"
 1077      61 74 61 00 
 1078 0033 85 02 00 00 	.4byte 0x285
 1079 0037 4D 41 53 54 	.asciz "MASTER_MSG_Cell_Info"
 1079      45 52 5F 4D 
 1079      53 47 5F 43 
 1079      65 6C 6C 5F 
 1079      49 6E 66 6F 
MPLAB XC16 ASSEMBLY Listing:   			page 25


 1079      00 
 1080 004c 99 03 00 00 	.4byte 0x399
 1081 0050 4D 41 53 54 	.asciz "MASTER_MSG_Slave_Info_1"
 1081      45 52 5F 4D 
 1081      53 47 5F 53 
 1081      6C 61 76 65 
 1081      5F 49 6E 66 
 1081      6F 5F 31 00 
 1082 0068 31 04 00 00 	.4byte 0x431
 1083 006c 4D 41 53 54 	.asciz "MASTER_MSG_Slave_Info_2"
 1083      45 52 5F 4D 
 1083      53 47 5F 53 
 1083      6C 61 76 65 
 1083      5F 49 6E 66 
 1083      6F 5F 32 00 
 1084 0084 BA 04 00 00 	.4byte 0x4ba
 1085 0088 4D 41 53 54 	.asciz "MASTER_MSG_Slave_Info_3"
 1085      45 52 5F 4D 
 1085      53 47 5F 53 
 1085      6C 61 76 65 
 1085      5F 49 6E 66 
 1085      6F 5F 33 00 
 1086 00a0 4F 05 00 00 	.4byte 0x54f
 1087 00a4 4D 41 53 54 	.asciz "MASTER_VERBOSE_MSG_Data"
 1087      45 52 5F 56 
 1087      45 52 42 4F 
 1087      53 45 5F 4D 
 1087      53 47 5F 44 
 1087      61 74 61 00 
 1088 00bc 00 00 00 00 	.4byte 0x0
 1089                 	.section .debug_aranges,info
 1090 0000 14 00 00 00 	.4byte 0x14
 1091 0004 02 00       	.2byte 0x2
 1092 0006 00 00 00 00 	.4byte .Ldebug_info0
 1093 000a 04          	.byte 0x4
 1094 000b 00          	.byte 0x0
 1095 000c 00 00       	.2byte 0x0
 1096 000e 00 00       	.2byte 0x0
 1097 0010 00 00 00 00 	.4byte 0x0
 1098 0014 00 00 00 00 	.4byte 0x0
 1099                 	.section .debug_str,info
 1100                 	.LASF0:
 1101 0000 73 6C 61 76 	.asciz "slave_id"
 1101      65 5F 69 64 
 1101      00 
 1102                 	.section .text,code
 1103              	
 1104              	
 1105              	
 1106              	.section __c30_info,info,bss
 1107                 	__psv_trap_errata:
 1108                 	
 1109                 	.section __c30_signature,info,data
 1110 0000 01 00       	.word 0x0001
 1111 0002 00 00       	.word 0x0000
 1112 0004 00 00       	.word 0x0000
 1113                 	
MPLAB XC16 ASSEMBLY Listing:   			page 26


 1114                 	
 1115                 	
 1116                 	.set ___PA___,0
 1117                 	.end
MPLAB XC16 ASSEMBLY Listing:   			page 27


DEFINED SYMBOLS
                            *ABS*:00000000 /home/user/Documents/FST/Programming/project_sub-zero/lib/can-ids/Devices/BMS_VERBOSE_CAN.c
                            *ABS*:00000001 __33EP256MU806
                            *ABS*:00000001 __dsPIC33E
                            *ABS*:00000001 __HAS_DSP
                            *ABS*:00000001 __HAS_DMAV2
                            *ABS*:00000001 __HAS_CODEGUARD
                            *ABS*:00000001 __HAS_PMP_ENHANCED
                            *ABS*:00000001 __HAS_EDS
                            *ABS*:00000001 __HAS_EP
                            *ABS*:00000011 __TARGET_DIVIDE_CYCLES
                            *ABS*:00000001 __C30ELF
    {standard input}:14     .text:00000000 _parse_can_message_master_verbose
    {standard input}:18     *ABS*:00000000 ___PA___
    {standard input}:44     *ABS*:00000000 ___BP___
    {standard input}:1107   __c30_info:00000000 __psv_trap_errata
    {standard input}:19     .text:00000000 .L0
                            .text:000000a6 .L4
                            .text:0000002e .L7
                            .text:000000dc .L3
                            .text:00000188 .L1
                            .text:0000003c .L5
                            .text:00000112 .L6
                            .text:00000182 .L16
                            .text:0000006c .L9
                            .text:0000007c .L10
                            .text:0000008c .L11
                            .text:0000009c .L12
                            .text:00000184 .L17
                            .text:00000186 .L18
                            .text:00000188 .L19
                    .debug_abbrev:00000000 .Ldebug_abbrev0
                            .text:00000000 .Ltext0
                            .text:00000194 .Letext0
                      .debug_line:00000000 .Ldebug_line0
                       .debug_str:00000000 .LASF0
                            .text:00000000 .LFB0
                            .text:00000194 .LFE0
                     .debug_frame:00000000 .Lframe0
                      .debug_info:00000000 .Ldebug_info0

UNDEFINED SYMBOLS
CORCON

EQUATE SYMBOLS
/home/user/Documents/FST/Programming/project_sub-zero/lib/can-ids/Devices/BMS_VERBOSE_CAN.c = 0x0
                  __33EP256MU806 = 0x1
                      __dsPIC33E = 0x1
                       __HAS_DSP = 0x1
                     __HAS_DMAV2 = 0x1
                 __HAS_CODEGUARD = 0x1
              __HAS_PMP_ENHANCED = 0x1
                       __HAS_EDS = 0x1
                        __HAS_EP = 0x1
          __TARGET_DIVIDE_CYCLES = 0x11
                        __C30ELF = 0x1
                        ___PA___ = 0x0
                        ___BP___ = 0x0
