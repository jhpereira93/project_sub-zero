MPLAB XC16 ASSEMBLY Listing:   			page 1


   1              	.file "/home/user/Documents/FST/Programming/project_sub-zero/lib/can-ids/Devices/ARM_CAN.c"
   2              	.section .debug_abbrev,info
   3                 	.Ldebug_abbrev0:
   4                 	.section .debug_info,info
   5                 	.Ldebug_info0:
   6                 	.section .debug_line,info
   7                 	.Ldebug_line0:
   8 0000 07 01 00 00 	.section .text,code
   8      02 00 A5 00 
   8      00 00 01 01 
   8      FB 0E 0A 00 
   8      01 01 01 01 
   8      00 00 00 01 
   8      6C 69 62 2F 
   8      63 61 6E 2D 
   8      69 64 73 2F 
   9              	.Ltext0:
  10              	.section .text,code
  11              	.align 2
  12              	.global _parse_ARM_torque_message
  13              	.type _parse_ARM_torque_message,@function
  14              	_parse_ARM_torque_message:
  15              	.LFB0:
  16              	.file 1 "lib/can-ids/Devices/ARM_CAN.c"
   1:lib/can-ids/Devices/ARM_CAN.c **** #include <stdint.h>
   2:lib/can-ids/Devices/ARM_CAN.c **** #include "../CAN_IDs.h"
   3:lib/can-ids/Devices/ARM_CAN.c **** #include "ARM_CAN.h"
   4:lib/can-ids/Devices/ARM_CAN.c **** 
   5:lib/can-ids/Devices/ARM_CAN.c **** void parse_ARM_torque_message(uint16_t data[4], ARM_torque *torque){
  17              	.loc 1 5 0
  18              	.set ___PA___,1
  19 000000  04 00 FA 	lnk #4
  20              	.LCFI0:
  21              	.loc 1 5 0
  22 000002  00 0F 78 	mov w0,[w14]
   6:lib/can-ids/Devices/ARM_CAN.c **** 	torque->FL=data[0];
   7:lib/can-ids/Devices/ARM_CAN.c **** 	torque->FR=data[1];
   8:lib/can-ids/Devices/ARM_CAN.c **** 	torque->RL=data[2];
  23              	.loc 1 8 0
  24 000004  1E 01 78 	mov [w14],w2
  25 000006  E4 01 41 	add w2,#4,w3
  26              	.loc 1 6 0
  27 000008  1E 02 78 	mov [w14],w4
  28              	.loc 1 7 0
  29 00000a  1E 80 E8 	inc2 [w14],w0
   9:lib/can-ids/Devices/ARM_CAN.c **** 	torque->RR=data[3];
  30              	.loc 1 9 0
  31 00000c  9E 02 78 	mov [w14],w5
  32 00000e  66 81 42 	add w5,#6,w2
  33              	.loc 1 5 0
  34 000010  11 07 98 	mov w1,[w14+2]
  35              	.loc 1 6 0
  36 000012  94 02 78 	mov [w4],w5
  37 000014  9E 00 90 	mov [w14+2],w1
  38              	.loc 1 7 0
  39 000016  1E 02 90 	mov [w14+2],w4
  40              	.loc 1 6 0
MPLAB XC16 ASSEMBLY Listing:   			page 2


  41 000018  85 08 78 	mov w5,[w1]
  42              	.loc 1 8 0
  43 00001a  9E 00 90 	mov [w14+2],w1
  44              	.loc 1 7 0
  45 00001c  90 02 78 	mov [w0],w5
  46              	.loc 1 9 0
  47 00001e  1E 00 90 	mov [w14+2],w0
  48              	.loc 1 7 0
  49 000020  15 02 98 	mov w5,[w4+2]
  50              	.loc 1 8 0
  51 000022  93 01 78 	mov [w3],w3
  52 000024  A3 00 98 	mov w3,[w1+4]
  53              	.loc 1 9 0
  54 000026  92 00 78 	mov [w2],w1
  55 000028  31 00 98 	mov w1,[w0+6]
  10:lib/can-ids/Devices/ARM_CAN.c **** }
  56              	.loc 1 10 0
  57 00002a  8E 07 78 	mov w14,w15
  58 00002c  4F 07 78 	mov [--w15],w14
  59 00002e  00 40 A9 	bclr CORCON,#2
  60 000030  00 00 06 	return 
  61              	.set ___PA___,0
  62              	.LFE0:
  63              	.size _parse_ARM_torque_message,.-_parse_ARM_torque_message
  64              	.align 2
  65              	.global _parse_ARM_status_message
  66              	.type _parse_ARM_status_message,@function
  67              	_parse_ARM_status_message:
  68              	.LFB1:
  11:lib/can-ids/Devices/ARM_CAN.c **** 
  12:lib/can-ids/Devices/ARM_CAN.c **** void parse_ARM_status_message(uint16_t data[4], ARM_status *status){
  69              	.loc 1 12 0
  70              	.set ___PA___,1
  71 000032  04 00 FA 	lnk #4
  72              	.LCFI1:
  73              	.loc 1 12 0
  74 000034  00 0F 78 	mov w0,[w14]
  75 000036  11 07 98 	mov w1,[w14+2]
  13:lib/can-ids/Devices/ARM_CAN.c **** 	status->ARM_OK=data[0];
  76              	.loc 1 13 0
  77 000038  9E 00 78 	mov [w14],w1
  78 00003a  1E 00 90 	mov [w14+2],w0
  79 00003c  91 00 78 	mov [w1],w1
  80 00003e  81 40 78 	mov.b w1,w1
  81 000040  01 48 78 	mov.b w1,[w0]
  14:lib/can-ids/Devices/ARM_CAN.c **** }
  82              	.loc 1 14 0
  83 000042  8E 07 78 	mov w14,w15
  84 000044  4F 07 78 	mov [--w15],w14
  85 000046  00 40 A9 	bclr CORCON,#2
  86 000048  00 00 06 	return 
  87              	.set ___PA___,0
  88              	.LFE1:
  89              	.size _parse_ARM_status_message,.-_parse_ARM_status_message
  90              	.align 2
  91              	.global _parse_ARM_control_info_message
  92              	.type _parse_ARM_control_info_message,@function
MPLAB XC16 ASSEMBLY Listing:   			page 3


  93              	_parse_ARM_control_info_message:
  94              	.LFB2:
  15:lib/can-ids/Devices/ARM_CAN.c **** 
  16:lib/can-ids/Devices/ARM_CAN.c **** void parse_ARM_control_info_message(uint16_t data[4], ARM_control_info *control_info){
  95              	.loc 1 16 0
  96              	.set ___PA___,1
  97 00004a  04 00 FA 	lnk #4
  98              	.LCFI2:
  99              	.loc 1 16 0
 100 00004c  00 0F 78 	mov w0,[w14]
  17:lib/can-ids/Devices/ARM_CAN.c **** 	control_info->mode = data[0];
  18:lib/can-ids/Devices/ARM_CAN.c **** 	control_info->gain = data[1];
  19:lib/can-ids/Devices/ARM_CAN.c **** 	control_info->tl = data[2];
 101              	.loc 1 19 0
 102 00004e  1E 01 78 	mov [w14],w2
 103 000050  E4 01 41 	add w2,#4,w3
 104              	.loc 1 17 0
 105 000052  1E 02 78 	mov [w14],w4
 106              	.loc 1 18 0
 107 000054  1E 80 E8 	inc2 [w14],w0
  20:lib/can-ids/Devices/ARM_CAN.c **** 	control_info->power = data[3];
 108              	.loc 1 20 0
 109 000056  9E 02 78 	mov [w14],w5
 110 000058  66 81 42 	add w5,#6,w2
 111              	.loc 1 16 0
 112 00005a  11 07 98 	mov w1,[w14+2]
 113              	.loc 1 17 0
 114 00005c  94 02 78 	mov [w4],w5
 115 00005e  9E 00 90 	mov [w14+2],w1
 116              	.loc 1 18 0
 117 000060  1E 02 90 	mov [w14+2],w4
 118              	.loc 1 17 0
 119 000062  85 08 78 	mov w5,[w1]
 120              	.loc 1 19 0
 121 000064  9E 00 90 	mov [w14+2],w1
 122              	.loc 1 18 0
 123 000066  90 02 78 	mov [w0],w5
 124              	.loc 1 20 0
 125 000068  1E 00 90 	mov [w14+2],w0
 126              	.loc 1 18 0
 127 00006a  15 02 98 	mov w5,[w4+2]
 128              	.loc 1 19 0
 129 00006c  93 01 78 	mov [w3],w3
 130 00006e  A3 00 98 	mov w3,[w1+4]
 131              	.loc 1 20 0
 132 000070  92 00 78 	mov [w2],w1
 133 000072  31 00 98 	mov w1,[w0+6]
  21:lib/can-ids/Devices/ARM_CAN.c **** 
  22:lib/can-ids/Devices/ARM_CAN.c **** 	return;
  23:lib/can-ids/Devices/ARM_CAN.c **** }
 134              	.loc 1 23 0
 135 000074  8E 07 78 	mov w14,w15
 136 000076  4F 07 78 	mov [--w15],w14
 137 000078  00 40 A9 	bclr CORCON,#2
 138 00007a  00 00 06 	return 
 139              	.set ___PA___,0
 140              	.LFE2:
MPLAB XC16 ASSEMBLY Listing:   			page 4


 141              	.size _parse_ARM_control_info_message,.-_parse_ARM_control_info_message
 142              	.align 2
 143              	.global _parse_ARM_lims_message
 144              	.type _parse_ARM_lims_message,@function
 145              	_parse_ARM_lims_message:
 146              	.LFB3:
  24:lib/can-ids/Devices/ARM_CAN.c **** 
  25:lib/can-ids/Devices/ARM_CAN.c **** void parse_ARM_lims_message(uint16_t data[4], ARM_lims *lims){
 147              	.loc 1 25 0
 148              	.set ___PA___,1
 149 00007c  04 00 FA 	lnk #4
 150              	.LCFI3:
 151              	.loc 1 25 0
 152 00007e  00 0F 78 	mov w0,[w14]
  26:lib/can-ids/Devices/ARM_CAN.c ****     lims->lim_rpm = data[0];
  27:lib/can-ids/Devices/ARM_CAN.c ****     lims->lim_torque_r = data[1];
  28:lib/can-ids/Devices/ARM_CAN.c ****     lims->lim_torque_f = data[2];
 153              	.loc 1 28 0
 154 000080  9E 01 78 	mov [w14],w3
 155 000082  64 81 41 	add w3,#4,w2
 156              	.loc 1 26 0
 157 000084  1E 00 78 	mov [w14],w0
 158              	.loc 1 27 0
 159 000086  9E 81 E8 	inc2 [w14],w3
 160              	.loc 1 25 0
 161 000088  11 07 98 	mov w1,[w14+2]
 162              	.loc 1 26 0
 163 00008a  10 02 78 	mov [w0],w4
 164 00008c  1E 00 90 	mov [w14+2],w0
 165              	.loc 1 27 0
 166 00008e  9E 00 90 	mov [w14+2],w1
 167              	.loc 1 26 0
 168 000090  04 08 78 	mov w4,[w0]
 169              	.loc 1 28 0
 170 000092  1E 00 90 	mov [w14+2],w0
 171              	.loc 1 27 0
 172 000094  93 01 78 	mov [w3],w3
 173 000096  A3 00 98 	mov w3,[w1+4]
 174              	.loc 1 28 0
 175 000098  92 00 78 	mov [w2],w1
 176 00009a  11 00 98 	mov w1,[w0+2]
  29:lib/can-ids/Devices/ARM_CAN.c ****     
  30:lib/can-ids/Devices/ARM_CAN.c ****     return;
  31:lib/can-ids/Devices/ARM_CAN.c **** }
 177              	.loc 1 31 0
 178 00009c  8E 07 78 	mov w14,w15
 179 00009e  4F 07 78 	mov [--w15],w14
 180 0000a0  00 40 A9 	bclr CORCON,#2
 181 0000a2  00 00 06 	return 
 182              	.set ___PA___,0
 183              	.LFE3:
 184              	.size _parse_ARM_lims_message,.-_parse_ARM_lims_message
 185              	.align 2
 186              	.global _parse_can_ARM
 187              	.type _parse_can_ARM,@function
 188              	_parse_can_ARM:
 189              	.LFB4:
MPLAB XC16 ASSEMBLY Listing:   			page 5


  32:lib/can-ids/Devices/ARM_CAN.c **** 
  33:lib/can-ids/Devices/ARM_CAN.c **** void parse_can_ARM(CANdata message, ARM_CAN_data *data){
 190              	.loc 1 33 0
 191              	.set ___PA___,1
 192 0000a4  0E 00 FA 	lnk #14
 193              	.LCFI4:
 194              	.loc 1 33 0
 195 0000a6  00 0F 78 	mov w0,[w14]
 196 0000a8  11 07 98 	mov w1,[w14+2]
  34:lib/can-ids/Devices/ARM_CAN.c **** 
  35:lib/can-ids/Devices/ARM_CAN.c **** 	switch(message.msg_id){
 197              	.loc 1 35 0
 198 0000aa  1E 00 78 	mov [w14],w0
 199              	.loc 1 33 0
 200 0000ac  22 07 98 	mov w2,[w14+4]
 201              	.loc 1 35 0
 202 0000ae  45 00 DE 	lsr w0,#5,w0
 203              	.loc 1 33 0
 204 0000b0  33 07 98 	mov w3,[w14+6]
 205              	.loc 1 35 0
 206 0000b2  F0 43 B2 	and.b #63,w0
 207              	.loc 1 33 0
 208 0000b4  44 07 98 	mov w4,[w14+8]
 209 0000b6  55 07 98 	mov w5,[w14+10]
 210 0000b8  66 07 98 	mov w6,[w14+12]
 211              	.loc 1 35 0
 212 0000ba  00 80 FB 	ze w0,w0
 213 0000bc  41 02 20 	mov #36,w1
 214 0000be  81 0F 50 	sub w0,w1,[w15]
 215              	.set ___BP___,0
 216 0000c0  00 00 32 	bra z,.L9
 217 0000c2  41 02 20 	mov #36,w1
 218 0000c4  81 0F 50 	sub w0,w1,[w15]
 219              	.set ___BP___,0
 220 0000c6  00 00 3C 	bra gt,.L11
 221 0000c8  E9 0F 50 	sub w0,#9,[w15]
 222              	.set ___BP___,0
 223 0000ca  00 00 32 	bra z,.L7
 224 0000cc  31 02 20 	mov #35,w1
 225 0000ce  81 0F 50 	sub w0,w1,[w15]
 226              	.set ___BP___,0
 227 0000d0  00 00 32 	bra z,.L8
  36:lib/can-ids/Devices/ARM_CAN.c **** 		case MSG_ID_ARM_TORQUE : 
  37:lib/can-ids/Devices/ARM_CAN.c **** 			parse_ARM_torque_message(message.data, &(data->torque));
  38:lib/can-ids/Devices/ARM_CAN.c **** 			return;
  39:lib/can-ids/Devices/ARM_CAN.c **** 		case MSG_ID_ARM_STATUS :
  40:lib/can-ids/Devices/ARM_CAN.c **** 			parse_ARM_status_message(message.data, &(data->status));
  41:lib/can-ids/Devices/ARM_CAN.c **** 			return;
  42:lib/can-ids/Devices/ARM_CAN.c **** 		case MSG_ID_ARM_CONTROL_INFO:
  43:lib/can-ids/Devices/ARM_CAN.c **** 			parse_ARM_control_info_message(message.data, &(data->control_info));
  44:lib/can-ids/Devices/ARM_CAN.c **** 			return;
  45:lib/can-ids/Devices/ARM_CAN.c **** 
  46:lib/can-ids/Devices/ARM_CAN.c **** 		case MSG_ID_ARM_LIMS :
  47:lib/can-ids/Devices/ARM_CAN.c **** 		    parse_ARM_lims_message(message.data, &(data->lims));
  48:lib/can-ids/Devices/ARM_CAN.c **** 		    return;
  49:lib/can-ids/Devices/ARM_CAN.c **** 
  50:lib/can-ids/Devices/ARM_CAN.c **** 		default : return;
MPLAB XC16 ASSEMBLY Listing:   			page 6


 228              	.loc 1 50 0
 229 0000d2  00 00 37 	bra .L5
 230              	.L11:
 231              	.loc 1 35 0
 232 0000d4  51 02 20 	mov #37,w1
 233 0000d6  81 0F 50 	sub w0,w1,[w15]
 234              	.set ___BP___,0
 235 0000d8  00 00 32 	bra z,.L10
 236              	.loc 1 50 0
 237 0000da  00 00 37 	bra .L5
 238              	.L7:
 239              	.loc 1 37 0
 240 0000dc  EE 00 90 	mov [w14+12],w1
 241 0000de  64 00 47 	add w14,#4,w0
 242 0000e0  00 00 07 	rcall _parse_ARM_torque_message
 243              	.loc 1 38 0
 244 0000e2  00 00 37 	bra .L5
 245              	.L8:
 246              	.loc 1 40 0
 247 0000e4  EE 00 90 	mov [w14+12],w1
 248 0000e6  64 00 47 	add w14,#4,w0
 249 0000e8  E8 80 40 	add w1,#8,w1
 250 0000ea  00 00 07 	rcall _parse_ARM_status_message
 251              	.loc 1 41 0
 252 0000ec  00 00 37 	bra .L5
 253              	.L10:
 254              	.loc 1 43 0
 255 0000ee  EE 00 90 	mov [w14+12],w1
 256 0000f0  64 00 47 	add w14,#4,w0
 257 0000f2  EA 80 40 	add w1,#10,w1
 258 0000f4  00 00 07 	rcall _parse_ARM_control_info_message
 259              	.loc 1 44 0
 260 0000f6  00 00 37 	bra .L5
 261              	.L9:
 262              	.loc 1 47 0
 263 0000f8  EE 00 90 	mov [w14+12],w1
 264 0000fa  64 00 47 	add w14,#4,w0
 265 0000fc  F2 80 40 	add w1,#18,w1
 266 0000fe  00 00 07 	rcall _parse_ARM_lims_message
 267              	.L5:
  51:lib/can-ids/Devices/ARM_CAN.c **** 		}
  52:lib/can-ids/Devices/ARM_CAN.c **** }...
 268              	.loc 1 52 0
 269 000100  8E 07 78 	mov w14,w15
 270 000102  4F 07 78 	mov [--w15],w14
 271 000104  00 40 A9 	bclr CORCON,#2
 272 000106  00 00 06 	return 
 273              	.set ___PA___,0
 274              	.LFE4:
 275              	.size _parse_can_ARM,.-_parse_can_ARM
 276              	.section .debug_frame,info
 277                 	.Lframe0:
 278 0000 10 00 00 00 	.4byte .LECIE0-.LSCIE0
 279                 	.LSCIE0:
 280 0004 FF FF FF FF 	.4byte 0xffffffff
 281 0008 01          	.byte 0x1
 282 0009 00          	.byte 0
MPLAB XC16 ASSEMBLY Listing:   			page 7


 283 000a 01          	.uleb128 0x1
 284 000b 02          	.sleb128 2
 285 000c 25          	.byte 0x25
 286 000d 12          	.byte 0x12
 287 000e 0F          	.uleb128 0xf
 288 000f 7E          	.sleb128 -2
 289 0010 09          	.byte 0x9
 290 0011 25          	.uleb128 0x25
 291 0012 0F          	.uleb128 0xf
 292 0013 00          	.align 4
 293                 	.LECIE0:
 294                 	.LSFDE0:
 295 0014 18 00 00 00 	.4byte .LEFDE0-.LASFDE0
 296                 	.LASFDE0:
 297 0018 00 00 00 00 	.4byte .Lframe0
 298 001c 00 00 00 00 	.4byte .LFB0
 299 0020 32 00 00 00 	.4byte .LFE0-.LFB0
 300 0024 04          	.byte 0x4
 301 0025 02 00 00 00 	.4byte .LCFI0-.LFB0
 302 0029 13          	.byte 0x13
 303 002a 7D          	.sleb128 -3
 304 002b 0D          	.byte 0xd
 305 002c 0E          	.uleb128 0xe
 306 002d 8E          	.byte 0x8e
 307 002e 02          	.uleb128 0x2
 308 002f 00          	.align 4
 309                 	.LEFDE0:
 310                 	.LSFDE2:
 311 0030 18 00 00 00 	.4byte .LEFDE2-.LASFDE2
 312                 	.LASFDE2:
 313 0034 00 00 00 00 	.4byte .Lframe0
 314 0038 00 00 00 00 	.4byte .LFB1
 315 003c 18 00 00 00 	.4byte .LFE1-.LFB1
 316 0040 04          	.byte 0x4
 317 0041 02 00 00 00 	.4byte .LCFI1-.LFB1
 318 0045 13          	.byte 0x13
 319 0046 7D          	.sleb128 -3
 320 0047 0D          	.byte 0xd
 321 0048 0E          	.uleb128 0xe
 322 0049 8E          	.byte 0x8e
 323 004a 02          	.uleb128 0x2
 324 004b 00          	.align 4
 325                 	.LEFDE2:
 326                 	.LSFDE4:
 327 004c 18 00 00 00 	.4byte .LEFDE4-.LASFDE4
 328                 	.LASFDE4:
 329 0050 00 00 00 00 	.4byte .Lframe0
 330 0054 00 00 00 00 	.4byte .LFB2
 331 0058 32 00 00 00 	.4byte .LFE2-.LFB2
 332 005c 04          	.byte 0x4
 333 005d 02 00 00 00 	.4byte .LCFI2-.LFB2
 334 0061 13          	.byte 0x13
 335 0062 7D          	.sleb128 -3
 336 0063 0D          	.byte 0xd
 337 0064 0E          	.uleb128 0xe
 338 0065 8E          	.byte 0x8e
 339 0066 02          	.uleb128 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 8


 340 0067 00          	.align 4
 341                 	.LEFDE4:
 342                 	.LSFDE6:
 343 0068 18 00 00 00 	.4byte .LEFDE6-.LASFDE6
 344                 	.LASFDE6:
 345 006c 00 00 00 00 	.4byte .Lframe0
 346 0070 00 00 00 00 	.4byte .LFB3
 347 0074 28 00 00 00 	.4byte .LFE3-.LFB3
 348 0078 04          	.byte 0x4
 349 0079 02 00 00 00 	.4byte .LCFI3-.LFB3
 350 007d 13          	.byte 0x13
 351 007e 7D          	.sleb128 -3
 352 007f 0D          	.byte 0xd
 353 0080 0E          	.uleb128 0xe
 354 0081 8E          	.byte 0x8e
 355 0082 02          	.uleb128 0x2
 356 0083 00          	.align 4
 357                 	.LEFDE6:
 358                 	.LSFDE8:
 359 0084 18 00 00 00 	.4byte .LEFDE8-.LASFDE8
 360                 	.LASFDE8:
 361 0088 00 00 00 00 	.4byte .Lframe0
 362 008c 00 00 00 00 	.4byte .LFB4
 363 0090 64 00 00 00 	.4byte .LFE4-.LFB4
 364 0094 04          	.byte 0x4
 365 0095 02 00 00 00 	.4byte .LCFI4-.LFB4
 366 0099 13          	.byte 0x13
 367 009a 7D          	.sleb128 -3
 368 009b 0D          	.byte 0xd
 369 009c 0E          	.uleb128 0xe
 370 009d 8E          	.byte 0x8e
 371 009e 02          	.uleb128 0x2
 372 009f 00          	.align 4
 373                 	.LEFDE8:
 374                 	.section .text,code
 375              	.Letext0:
 376              	.file 2 "/opt/microchip/xc16/v1.36/bin/bin/../../include/lega-c/stdint.h"
 377              	.file 3 "lib/can-ids/Devices/../CAN_IDs.h"
 378              	.file 4 "lib/can-ids/Devices/ARM_CAN.h"
 379              	.section .debug_info,info
 380 0000 06 05 00 00 	.4byte 0x506
 381 0004 02 00       	.2byte 0x2
 382 0006 00 00 00 00 	.4byte .Ldebug_abbrev0
 383 000a 04          	.byte 0x4
 384 000b 01          	.uleb128 0x1
 385 000c 47 4E 55 20 	.asciz "GNU C 4.5.1 (XC16, Microchip v1.36) (A) Build date: Dec  5 2018"
 385      43 20 34 2E 
 385      35 2E 31 20 
 385      28 58 43 31 
 385      36 2C 20 4D 
 385      69 63 72 6F 
 385      63 68 69 70 
 385      20 76 31 2E 
 385      33 36 29 20 
 386 004c 01          	.byte 0x1
 387 004d 6C 69 62 2F 	.asciz "lib/can-ids/Devices/ARM_CAN.c"
 387      63 61 6E 2D 
MPLAB XC16 ASSEMBLY Listing:   			page 9


 387      69 64 73 2F 
 387      44 65 76 69 
 387      63 65 73 2F 
 387      41 52 4D 5F 
 387      43 41 4E 2E 
 387      63 00 
 388 006b 2F 68 6F 6D 	.asciz "/home/user/Documents/FST/Programming/project_sub-zero"
 388      65 2F 75 73 
 388      65 72 2F 44 
 388      6F 63 75 6D 
 388      65 6E 74 73 
 388      2F 46 53 54 
 388      2F 50 72 6F 
 388      67 72 61 6D 
 388      6D 69 6E 67 
 389 00a1 00 00 00 00 	.4byte .Ltext0
 390 00a5 00 00 00 00 	.4byte .Letext0
 391 00a9 00 00 00 00 	.4byte .Ldebug_line0
 392 00ad 02          	.uleb128 0x2
 393 00ae 01          	.byte 0x1
 394 00af 06          	.byte 0x6
 395 00b0 73 69 67 6E 	.asciz "signed char"
 395      65 64 20 63 
 395      68 61 72 00 
 396 00bc 02          	.uleb128 0x2
 397 00bd 02          	.byte 0x2
 398 00be 05          	.byte 0x5
 399 00bf 69 6E 74 00 	.asciz "int"
 400 00c3 02          	.uleb128 0x2
 401 00c4 04          	.byte 0x4
 402 00c5 05          	.byte 0x5
 403 00c6 6C 6F 6E 67 	.asciz "long int"
 403      20 69 6E 74 
 403      00 
 404 00cf 02          	.uleb128 0x2
 405 00d0 08          	.byte 0x8
 406 00d1 05          	.byte 0x5
 407 00d2 6C 6F 6E 67 	.asciz "long long int"
 407      20 6C 6F 6E 
 407      67 20 69 6E 
 407      74 00 
 408 00e0 03          	.uleb128 0x3
 409 00e1 75 69 6E 74 	.asciz "uint8_t"
 409      38 5F 74 00 
 410 00e9 02          	.byte 0x2
 411 00ea 2B          	.byte 0x2b
 412 00eb EF 00 00 00 	.4byte 0xef
 413 00ef 02          	.uleb128 0x2
 414 00f0 01          	.byte 0x1
 415 00f1 08          	.byte 0x8
 416 00f2 75 6E 73 69 	.asciz "unsigned char"
 416      67 6E 65 64 
 416      20 63 68 61 
 416      72 00 
 417 0100 03          	.uleb128 0x3
 418 0101 75 69 6E 74 	.asciz "uint16_t"
 418      31 36 5F 74 
MPLAB XC16 ASSEMBLY Listing:   			page 10


 418      00 
 419 010a 02          	.byte 0x2
 420 010b 31          	.byte 0x31
 421 010c 10 01 00 00 	.4byte 0x110
 422 0110 02          	.uleb128 0x2
 423 0111 02          	.byte 0x2
 424 0112 07          	.byte 0x7
 425 0113 75 6E 73 69 	.asciz "unsigned int"
 425      67 6E 65 64 
 425      20 69 6E 74 
 425      00 
 426 0120 02          	.uleb128 0x2
 427 0121 04          	.byte 0x4
 428 0122 07          	.byte 0x7
 429 0123 6C 6F 6E 67 	.asciz "long unsigned int"
 429      20 75 6E 73 
 429      69 67 6E 65 
 429      64 20 69 6E 
 429      74 00 
 430 0135 02          	.uleb128 0x2
 431 0136 08          	.byte 0x8
 432 0137 07          	.byte 0x7
 433 0138 6C 6F 6E 67 	.asciz "long long unsigned int"
 433      20 6C 6F 6E 
 433      67 20 75 6E 
 433      73 69 67 6E 
 433      65 64 20 69 
 433      6E 74 00 
 434 014f 04          	.uleb128 0x4
 435 0150 02          	.byte 0x2
 436 0151 03          	.byte 0x3
 437 0152 12          	.byte 0x12
 438 0153 80 01 00 00 	.4byte 0x180
 439 0157 05          	.uleb128 0x5
 440 0158 64 65 76 5F 	.asciz "dev_id"
 440      69 64 00 
 441 015f 03          	.byte 0x3
 442 0160 13          	.byte 0x13
 443 0161 00 01 00 00 	.4byte 0x100
 444 0165 02          	.byte 0x2
 445 0166 05          	.byte 0x5
 446 0167 0B          	.byte 0xb
 447 0168 02          	.byte 0x2
 448 0169 23          	.byte 0x23
 449 016a 00          	.uleb128 0x0
 450 016b 05          	.uleb128 0x5
 451 016c 6D 73 67 5F 	.asciz "msg_id"
 451      69 64 00 
 452 0173 03          	.byte 0x3
 453 0174 16          	.byte 0x16
 454 0175 00 01 00 00 	.4byte 0x100
 455 0179 02          	.byte 0x2
 456 017a 06          	.byte 0x6
 457 017b 05          	.byte 0x5
 458 017c 02          	.byte 0x2
 459 017d 23          	.byte 0x23
 460 017e 00          	.uleb128 0x0
MPLAB XC16 ASSEMBLY Listing:   			page 11


 461 017f 00          	.byte 0x0
 462 0180 06          	.uleb128 0x6
 463 0181 02          	.byte 0x2
 464 0182 03          	.byte 0x3
 465 0183 11          	.byte 0x11
 466 0184 99 01 00 00 	.4byte 0x199
 467 0188 07          	.uleb128 0x7
 468 0189 4F 01 00 00 	.4byte 0x14f
 469 018d 08          	.uleb128 0x8
 470 018e 73 69 64 00 	.asciz "sid"
 471 0192 03          	.byte 0x3
 472 0193 18          	.byte 0x18
 473 0194 00 01 00 00 	.4byte 0x100
 474 0198 00          	.byte 0x0
 475 0199 04          	.uleb128 0x4
 476 019a 0C          	.byte 0xc
 477 019b 03          	.byte 0x3
 478 019c 10          	.byte 0x10
 479 019d C9 01 00 00 	.4byte 0x1c9
 480 01a1 09          	.uleb128 0x9
 481 01a2 80 01 00 00 	.4byte 0x180
 482 01a6 02          	.byte 0x2
 483 01a7 23          	.byte 0x23
 484 01a8 00          	.uleb128 0x0
 485 01a9 05          	.uleb128 0x5
 486 01aa 64 6C 63 00 	.asciz "dlc"
 487 01ae 03          	.byte 0x3
 488 01af 1A          	.byte 0x1a
 489 01b0 00 01 00 00 	.4byte 0x100
 490 01b4 02          	.byte 0x2
 491 01b5 04          	.byte 0x4
 492 01b6 0C          	.byte 0xc
 493 01b7 02          	.byte 0x2
 494 01b8 23          	.byte 0x23
 495 01b9 02          	.uleb128 0x2
 496 01ba 0A          	.uleb128 0xa
 497 01bb 00 00 00 00 	.4byte .LASF0
 498 01bf 03          	.byte 0x3
 499 01c0 1B          	.byte 0x1b
 500 01c1 C9 01 00 00 	.4byte 0x1c9
 501 01c5 02          	.byte 0x2
 502 01c6 23          	.byte 0x23
 503 01c7 04          	.uleb128 0x4
 504 01c8 00          	.byte 0x0
 505 01c9 0B          	.uleb128 0xb
 506 01ca 00 01 00 00 	.4byte 0x100
 507 01ce D9 01 00 00 	.4byte 0x1d9
 508 01d2 0C          	.uleb128 0xc
 509 01d3 10 01 00 00 	.4byte 0x110
 510 01d7 03          	.byte 0x3
 511 01d8 00          	.byte 0x0
 512 01d9 03          	.uleb128 0x3
 513 01da 43 41 4E 64 	.asciz "CANdata"
 513      61 74 61 00 
 514 01e2 03          	.byte 0x3
 515 01e3 1C          	.byte 0x1c
 516 01e4 99 01 00 00 	.4byte 0x199
MPLAB XC16 ASSEMBLY Listing:   			page 12


 517 01e8 04          	.uleb128 0x4
 518 01e9 08          	.byte 0x8
 519 01ea 04          	.byte 0x4
 520 01eb 11          	.byte 0x11
 521 01ec 25 02 00 00 	.4byte 0x225
 522 01f0 0D          	.uleb128 0xd
 523 01f1 46 4C 00    	.asciz "FL"
 524 01f4 04          	.byte 0x4
 525 01f5 12          	.byte 0x12
 526 01f6 00 01 00 00 	.4byte 0x100
 527 01fa 02          	.byte 0x2
 528 01fb 23          	.byte 0x23
 529 01fc 00          	.uleb128 0x0
 530 01fd 0D          	.uleb128 0xd
 531 01fe 46 52 00    	.asciz "FR"
 532 0201 04          	.byte 0x4
 533 0202 13          	.byte 0x13
 534 0203 00 01 00 00 	.4byte 0x100
 535 0207 02          	.byte 0x2
 536 0208 23          	.byte 0x23
 537 0209 02          	.uleb128 0x2
 538 020a 0D          	.uleb128 0xd
 539 020b 52 4C 00    	.asciz "RL"
 540 020e 04          	.byte 0x4
 541 020f 14          	.byte 0x14
 542 0210 00 01 00 00 	.4byte 0x100
 543 0214 02          	.byte 0x2
 544 0215 23          	.byte 0x23
 545 0216 04          	.uleb128 0x4
 546 0217 0D          	.uleb128 0xd
 547 0218 52 52 00    	.asciz "RR"
 548 021b 04          	.byte 0x4
 549 021c 15          	.byte 0x15
 550 021d 00 01 00 00 	.4byte 0x100
 551 0221 02          	.byte 0x2
 552 0222 23          	.byte 0x23
 553 0223 06          	.uleb128 0x6
 554 0224 00          	.byte 0x0
 555 0225 03          	.uleb128 0x3
 556 0226 41 52 4D 5F 	.asciz "ARM_torque"
 556      74 6F 72 71 
 556      75 65 00 
 557 0231 04          	.byte 0x4
 558 0232 16          	.byte 0x16
 559 0233 E8 01 00 00 	.4byte 0x1e8
 560 0237 04          	.uleb128 0x4
 561 0238 01          	.byte 0x1
 562 0239 04          	.byte 0x4
 563 023a 1A          	.byte 0x1a
 564 023b 51 02 00 00 	.4byte 0x251
 565 023f 0D          	.uleb128 0xd
 566 0240 41 52 4D 5F 	.asciz "ARM_OK"
 566      4F 4B 00 
 567 0247 04          	.byte 0x4
 568 0248 1B          	.byte 0x1b
 569 0249 E0 00 00 00 	.4byte 0xe0
 570 024d 02          	.byte 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 13


 571 024e 23          	.byte 0x23
 572 024f 00          	.uleb128 0x0
 573 0250 00          	.byte 0x0
 574 0251 03          	.uleb128 0x3
 575 0252 41 52 4D 5F 	.asciz "ARM_status"
 575      73 74 61 74 
 575      75 73 00 
 576 025d 04          	.byte 0x4
 577 025e 1D          	.byte 0x1d
 578 025f 37 02 00 00 	.4byte 0x237
 579 0263 04          	.uleb128 0x4
 580 0264 06          	.byte 0x6
 581 0265 04          	.byte 0x4
 582 0266 21          	.byte 0x21
 583 0267 AC 02 00 00 	.4byte 0x2ac
 584 026b 0D          	.uleb128 0xd
 585 026c 6C 69 6D 5F 	.asciz "lim_rpm"
 585      72 70 6D 00 
 586 0274 04          	.byte 0x4
 587 0275 22          	.byte 0x22
 588 0276 00 01 00 00 	.4byte 0x100
 589 027a 02          	.byte 0x2
 590 027b 23          	.byte 0x23
 591 027c 00          	.uleb128 0x0
 592 027d 0D          	.uleb128 0xd
 593 027e 6C 69 6D 5F 	.asciz "lim_torque_f"
 593      74 6F 72 71 
 593      75 65 5F 66 
 593      00 
 594 028b 04          	.byte 0x4
 595 028c 23          	.byte 0x23
 596 028d 00 01 00 00 	.4byte 0x100
 597 0291 02          	.byte 0x2
 598 0292 23          	.byte 0x23
 599 0293 02          	.uleb128 0x2
 600 0294 0D          	.uleb128 0xd
 601 0295 6C 69 6D 5F 	.asciz "lim_torque_r"
 601      74 6F 72 71 
 601      75 65 5F 72 
 601      00 
 602 02a2 04          	.byte 0x4
 603 02a3 24          	.byte 0x24
 604 02a4 00 01 00 00 	.4byte 0x100
 605 02a8 02          	.byte 0x2
 606 02a9 23          	.byte 0x23
 607 02aa 04          	.uleb128 0x4
 608 02ab 00          	.byte 0x0
 609 02ac 03          	.uleb128 0x3
 610 02ad 41 52 4D 5F 	.asciz "ARM_lims"
 610      6C 69 6D 73 
 610      00 
 611 02b6 04          	.byte 0x4
 612 02b7 25          	.byte 0x25
 613 02b8 63 02 00 00 	.4byte 0x263
 614 02bc 04          	.uleb128 0x4
 615 02bd 08          	.byte 0x8
 616 02be 04          	.byte 0x4
MPLAB XC16 ASSEMBLY Listing:   			page 14


 617 02bf 29          	.byte 0x29
 618 02c0 00 03 00 00 	.4byte 0x300
 619 02c4 0D          	.uleb128 0xd
 620 02c5 6D 6F 64 65 	.asciz "mode"
 620      00 
 621 02ca 04          	.byte 0x4
 622 02cb 2A          	.byte 0x2a
 623 02cc 00 01 00 00 	.4byte 0x100
 624 02d0 02          	.byte 0x2
 625 02d1 23          	.byte 0x23
 626 02d2 00          	.uleb128 0x0
 627 02d3 0D          	.uleb128 0xd
 628 02d4 67 61 69 6E 	.asciz "gain"
 628      00 
 629 02d9 04          	.byte 0x4
 630 02da 2B          	.byte 0x2b
 631 02db 00 01 00 00 	.4byte 0x100
 632 02df 02          	.byte 0x2
 633 02e0 23          	.byte 0x23
 634 02e1 02          	.uleb128 0x2
 635 02e2 0D          	.uleb128 0xd
 636 02e3 74 6C 00    	.asciz "tl"
 637 02e6 04          	.byte 0x4
 638 02e7 2C          	.byte 0x2c
 639 02e8 00 01 00 00 	.4byte 0x100
 640 02ec 02          	.byte 0x2
 641 02ed 23          	.byte 0x23
 642 02ee 04          	.uleb128 0x4
 643 02ef 0D          	.uleb128 0xd
 644 02f0 70 6F 77 65 	.asciz "power"
 644      72 00 
 645 02f6 04          	.byte 0x4
 646 02f7 2D          	.byte 0x2d
 647 02f8 00 01 00 00 	.4byte 0x100
 648 02fc 02          	.byte 0x2
 649 02fd 23          	.byte 0x23
 650 02fe 06          	.uleb128 0x6
 651 02ff 00          	.byte 0x0
 652 0300 03          	.uleb128 0x3
 653 0301 41 52 4D 5F 	.asciz "ARM_control_info"
 653      63 6F 6E 74 
 653      72 6F 6C 5F 
 653      69 6E 66 6F 
 653      00 
 654 0312 04          	.byte 0x4
 655 0313 2E          	.byte 0x2e
 656 0314 BC 02 00 00 	.4byte 0x2bc
 657 0318 04          	.uleb128 0x4
 658 0319 18          	.byte 0x18
 659 031a 04          	.byte 0x4
 660 031b 36          	.byte 0x36
 661 031c 60 03 00 00 	.4byte 0x360
 662 0320 0D          	.uleb128 0xd
 663 0321 74 6F 72 71 	.asciz "torque"
 663      75 65 00 
 664 0328 04          	.byte 0x4
 665 0329 37          	.byte 0x37
MPLAB XC16 ASSEMBLY Listing:   			page 15


 666 032a 25 02 00 00 	.4byte 0x225
 667 032e 02          	.byte 0x2
 668 032f 23          	.byte 0x23
 669 0330 00          	.uleb128 0x0
 670 0331 0D          	.uleb128 0xd
 671 0332 73 74 61 74 	.asciz "status"
 671      75 73 00 
 672 0339 04          	.byte 0x4
 673 033a 38          	.byte 0x38
 674 033b 51 02 00 00 	.4byte 0x251
 675 033f 02          	.byte 0x2
 676 0340 23          	.byte 0x23
 677 0341 08          	.uleb128 0x8
 678 0342 0A          	.uleb128 0xa
 679 0343 00 00 00 00 	.4byte .LASF1
 680 0347 04          	.byte 0x4
 681 0348 39          	.byte 0x39
 682 0349 00 03 00 00 	.4byte 0x300
 683 034d 02          	.byte 0x2
 684 034e 23          	.byte 0x23
 685 034f 0A          	.uleb128 0xa
 686 0350 0D          	.uleb128 0xd
 687 0351 6C 69 6D 73 	.asciz "lims"
 687      00 
 688 0356 04          	.byte 0x4
 689 0357 3A          	.byte 0x3a
 690 0358 AC 02 00 00 	.4byte 0x2ac
 691 035c 02          	.byte 0x2
 692 035d 23          	.byte 0x23
 693 035e 12          	.uleb128 0x12
 694 035f 00          	.byte 0x0
 695 0360 03          	.uleb128 0x3
 696 0361 41 52 4D 5F 	.asciz "ARM_CAN_data"
 696      43 41 4E 5F 
 696      64 61 74 61 
 696      00 
 697 036e 04          	.byte 0x4
 698 036f 3B          	.byte 0x3b
 699 0370 18 03 00 00 	.4byte 0x318
 700 0374 0E          	.uleb128 0xe
 701 0375 01          	.byte 0x1
 702 0376 70 61 72 73 	.asciz "parse_ARM_torque_message"
 702      65 5F 41 52 
 702      4D 5F 74 6F 
 702      72 71 75 65 
 702      5F 6D 65 73 
 702      73 61 67 65 
 702      00 
 703 038f 01          	.byte 0x1
 704 0390 05          	.byte 0x5
 705 0391 01          	.byte 0x1
 706 0392 00 00 00 00 	.4byte .LFB0
 707 0396 00 00 00 00 	.4byte .LFE0
 708 039a 01          	.byte 0x1
 709 039b 5E          	.byte 0x5e
 710 039c C0 03 00 00 	.4byte 0x3c0
 711 03a0 0F          	.uleb128 0xf
MPLAB XC16 ASSEMBLY Listing:   			page 16


 712 03a1 00 00 00 00 	.4byte .LASF0
 713 03a5 01          	.byte 0x1
 714 03a6 05          	.byte 0x5
 715 03a7 C0 03 00 00 	.4byte 0x3c0
 716 03ab 02          	.byte 0x2
 717 03ac 7E          	.byte 0x7e
 718 03ad 00          	.sleb128 0
 719 03ae 10          	.uleb128 0x10
 720 03af 74 6F 72 71 	.asciz "torque"
 720      75 65 00 
 721 03b6 01          	.byte 0x1
 722 03b7 05          	.byte 0x5
 723 03b8 C6 03 00 00 	.4byte 0x3c6
 724 03bc 02          	.byte 0x2
 725 03bd 7E          	.byte 0x7e
 726 03be 02          	.sleb128 2
 727 03bf 00          	.byte 0x0
 728 03c0 11          	.uleb128 0x11
 729 03c1 02          	.byte 0x2
 730 03c2 00 01 00 00 	.4byte 0x100
 731 03c6 11          	.uleb128 0x11
 732 03c7 02          	.byte 0x2
 733 03c8 25 02 00 00 	.4byte 0x225
 734 03cc 0E          	.uleb128 0xe
 735 03cd 01          	.byte 0x1
 736 03ce 70 61 72 73 	.asciz "parse_ARM_status_message"
 736      65 5F 41 52 
 736      4D 5F 73 74 
 736      61 74 75 73 
 736      5F 6D 65 73 
 736      73 61 67 65 
 736      00 
 737 03e7 01          	.byte 0x1
 738 03e8 0C          	.byte 0xc
 739 03e9 01          	.byte 0x1
 740 03ea 00 00 00 00 	.4byte .LFB1
 741 03ee 00 00 00 00 	.4byte .LFE1
 742 03f2 01          	.byte 0x1
 743 03f3 5E          	.byte 0x5e
 744 03f4 18 04 00 00 	.4byte 0x418
 745 03f8 0F          	.uleb128 0xf
 746 03f9 00 00 00 00 	.4byte .LASF0
 747 03fd 01          	.byte 0x1
 748 03fe 0C          	.byte 0xc
 749 03ff C0 03 00 00 	.4byte 0x3c0
 750 0403 02          	.byte 0x2
 751 0404 7E          	.byte 0x7e
 752 0405 00          	.sleb128 0
 753 0406 10          	.uleb128 0x10
 754 0407 73 74 61 74 	.asciz "status"
 754      75 73 00 
 755 040e 01          	.byte 0x1
 756 040f 0C          	.byte 0xc
 757 0410 18 04 00 00 	.4byte 0x418
 758 0414 02          	.byte 0x2
 759 0415 7E          	.byte 0x7e
 760 0416 02          	.sleb128 2
MPLAB XC16 ASSEMBLY Listing:   			page 17


 761 0417 00          	.byte 0x0
 762 0418 11          	.uleb128 0x11
 763 0419 02          	.byte 0x2
 764 041a 51 02 00 00 	.4byte 0x251
 765 041e 0E          	.uleb128 0xe
 766 041f 01          	.byte 0x1
 767 0420 70 61 72 73 	.asciz "parse_ARM_control_info_message"
 767      65 5F 41 52 
 767      4D 5F 63 6F 
 767      6E 74 72 6F 
 767      6C 5F 69 6E 
 767      66 6F 5F 6D 
 767      65 73 73 61 
 767      67 65 00 
 768 043f 01          	.byte 0x1
 769 0440 10          	.byte 0x10
 770 0441 01          	.byte 0x1
 771 0442 00 00 00 00 	.4byte .LFB2
 772 0446 00 00 00 00 	.4byte .LFE2
 773 044a 01          	.byte 0x1
 774 044b 5E          	.byte 0x5e
 775 044c 6D 04 00 00 	.4byte 0x46d
 776 0450 0F          	.uleb128 0xf
 777 0451 00 00 00 00 	.4byte .LASF0
 778 0455 01          	.byte 0x1
 779 0456 10          	.byte 0x10
 780 0457 C0 03 00 00 	.4byte 0x3c0
 781 045b 02          	.byte 0x2
 782 045c 7E          	.byte 0x7e
 783 045d 00          	.sleb128 0
 784 045e 0F          	.uleb128 0xf
 785 045f 00 00 00 00 	.4byte .LASF1
 786 0463 01          	.byte 0x1
 787 0464 10          	.byte 0x10
 788 0465 6D 04 00 00 	.4byte 0x46d
 789 0469 02          	.byte 0x2
 790 046a 7E          	.byte 0x7e
 791 046b 02          	.sleb128 2
 792 046c 00          	.byte 0x0
 793 046d 11          	.uleb128 0x11
 794 046e 02          	.byte 0x2
 795 046f 00 03 00 00 	.4byte 0x300
 796 0473 0E          	.uleb128 0xe
 797 0474 01          	.byte 0x1
 798 0475 70 61 72 73 	.asciz "parse_ARM_lims_message"
 798      65 5F 41 52 
 798      4D 5F 6C 69 
 798      6D 73 5F 6D 
 798      65 73 73 61 
 798      67 65 00 
 799 048c 01          	.byte 0x1
 800 048d 19          	.byte 0x19
 801 048e 01          	.byte 0x1
 802 048f 00 00 00 00 	.4byte .LFB3
 803 0493 00 00 00 00 	.4byte .LFE3
 804 0497 01          	.byte 0x1
 805 0498 5E          	.byte 0x5e
MPLAB XC16 ASSEMBLY Listing:   			page 18


 806 0499 BB 04 00 00 	.4byte 0x4bb
 807 049d 0F          	.uleb128 0xf
 808 049e 00 00 00 00 	.4byte .LASF0
 809 04a2 01          	.byte 0x1
 810 04a3 19          	.byte 0x19
 811 04a4 C0 03 00 00 	.4byte 0x3c0
 812 04a8 02          	.byte 0x2
 813 04a9 7E          	.byte 0x7e
 814 04aa 00          	.sleb128 0
 815 04ab 10          	.uleb128 0x10
 816 04ac 6C 69 6D 73 	.asciz "lims"
 816      00 
 817 04b1 01          	.byte 0x1
 818 04b2 19          	.byte 0x19
 819 04b3 BB 04 00 00 	.4byte 0x4bb
 820 04b7 02          	.byte 0x2
 821 04b8 7E          	.byte 0x7e
 822 04b9 02          	.sleb128 2
 823 04ba 00          	.byte 0x0
 824 04bb 11          	.uleb128 0x11
 825 04bc 02          	.byte 0x2
 826 04bd AC 02 00 00 	.4byte 0x2ac
 827 04c1 0E          	.uleb128 0xe
 828 04c2 01          	.byte 0x1
 829 04c3 70 61 72 73 	.asciz "parse_can_ARM"
 829      65 5F 63 61 
 829      6E 5F 41 52 
 829      4D 00 
 830 04d1 01          	.byte 0x1
 831 04d2 21          	.byte 0x21
 832 04d3 01          	.byte 0x1
 833 04d4 00 00 00 00 	.4byte .LFB4
 834 04d8 00 00 00 00 	.4byte .LFE4
 835 04dc 01          	.byte 0x1
 836 04dd 5E          	.byte 0x5e
 837 04de 03 05 00 00 	.4byte 0x503
 838 04e2 10          	.uleb128 0x10
 839 04e3 6D 65 73 73 	.asciz "message"
 839      61 67 65 00 
 840 04eb 01          	.byte 0x1
 841 04ec 21          	.byte 0x21
 842 04ed D9 01 00 00 	.4byte 0x1d9
 843 04f1 02          	.byte 0x2
 844 04f2 7E          	.byte 0x7e
 845 04f3 00          	.sleb128 0
 846 04f4 0F          	.uleb128 0xf
 847 04f5 00 00 00 00 	.4byte .LASF0
 848 04f9 01          	.byte 0x1
 849 04fa 21          	.byte 0x21
 850 04fb 03 05 00 00 	.4byte 0x503
 851 04ff 02          	.byte 0x2
 852 0500 7E          	.byte 0x7e
 853 0501 0C          	.sleb128 12
 854 0502 00          	.byte 0x0
 855 0503 11          	.uleb128 0x11
 856 0504 02          	.byte 0x2
 857 0505 60 03 00 00 	.4byte 0x360
MPLAB XC16 ASSEMBLY Listing:   			page 19


 858 0509 00          	.byte 0x0
 859                 	.section .debug_abbrev,info
 860 0000 01          	.uleb128 0x1
 861 0001 11          	.uleb128 0x11
 862 0002 01          	.byte 0x1
 863 0003 25          	.uleb128 0x25
 864 0004 08          	.uleb128 0x8
 865 0005 13          	.uleb128 0x13
 866 0006 0B          	.uleb128 0xb
 867 0007 03          	.uleb128 0x3
 868 0008 08          	.uleb128 0x8
 869 0009 1B          	.uleb128 0x1b
 870 000a 08          	.uleb128 0x8
 871 000b 11          	.uleb128 0x11
 872 000c 01          	.uleb128 0x1
 873 000d 12          	.uleb128 0x12
 874 000e 01          	.uleb128 0x1
 875 000f 10          	.uleb128 0x10
 876 0010 06          	.uleb128 0x6
 877 0011 00          	.byte 0x0
 878 0012 00          	.byte 0x0
 879 0013 02          	.uleb128 0x2
 880 0014 24          	.uleb128 0x24
 881 0015 00          	.byte 0x0
 882 0016 0B          	.uleb128 0xb
 883 0017 0B          	.uleb128 0xb
 884 0018 3E          	.uleb128 0x3e
 885 0019 0B          	.uleb128 0xb
 886 001a 03          	.uleb128 0x3
 887 001b 08          	.uleb128 0x8
 888 001c 00          	.byte 0x0
 889 001d 00          	.byte 0x0
 890 001e 03          	.uleb128 0x3
 891 001f 16          	.uleb128 0x16
 892 0020 00          	.byte 0x0
 893 0021 03          	.uleb128 0x3
 894 0022 08          	.uleb128 0x8
 895 0023 3A          	.uleb128 0x3a
 896 0024 0B          	.uleb128 0xb
 897 0025 3B          	.uleb128 0x3b
 898 0026 0B          	.uleb128 0xb
 899 0027 49          	.uleb128 0x49
 900 0028 13          	.uleb128 0x13
 901 0029 00          	.byte 0x0
 902 002a 00          	.byte 0x0
 903 002b 04          	.uleb128 0x4
 904 002c 13          	.uleb128 0x13
 905 002d 01          	.byte 0x1
 906 002e 0B          	.uleb128 0xb
 907 002f 0B          	.uleb128 0xb
 908 0030 3A          	.uleb128 0x3a
 909 0031 0B          	.uleb128 0xb
 910 0032 3B          	.uleb128 0x3b
 911 0033 0B          	.uleb128 0xb
 912 0034 01          	.uleb128 0x1
 913 0035 13          	.uleb128 0x13
 914 0036 00          	.byte 0x0
MPLAB XC16 ASSEMBLY Listing:   			page 20


 915 0037 00          	.byte 0x0
 916 0038 05          	.uleb128 0x5
 917 0039 0D          	.uleb128 0xd
 918 003a 00          	.byte 0x0
 919 003b 03          	.uleb128 0x3
 920 003c 08          	.uleb128 0x8
 921 003d 3A          	.uleb128 0x3a
 922 003e 0B          	.uleb128 0xb
 923 003f 3B          	.uleb128 0x3b
 924 0040 0B          	.uleb128 0xb
 925 0041 49          	.uleb128 0x49
 926 0042 13          	.uleb128 0x13
 927 0043 0B          	.uleb128 0xb
 928 0044 0B          	.uleb128 0xb
 929 0045 0D          	.uleb128 0xd
 930 0046 0B          	.uleb128 0xb
 931 0047 0C          	.uleb128 0xc
 932 0048 0B          	.uleb128 0xb
 933 0049 38          	.uleb128 0x38
 934 004a 0A          	.uleb128 0xa
 935 004b 00          	.byte 0x0
 936 004c 00          	.byte 0x0
 937 004d 06          	.uleb128 0x6
 938 004e 17          	.uleb128 0x17
 939 004f 01          	.byte 0x1
 940 0050 0B          	.uleb128 0xb
 941 0051 0B          	.uleb128 0xb
 942 0052 3A          	.uleb128 0x3a
 943 0053 0B          	.uleb128 0xb
 944 0054 3B          	.uleb128 0x3b
 945 0055 0B          	.uleb128 0xb
 946 0056 01          	.uleb128 0x1
 947 0057 13          	.uleb128 0x13
 948 0058 00          	.byte 0x0
 949 0059 00          	.byte 0x0
 950 005a 07          	.uleb128 0x7
 951 005b 0D          	.uleb128 0xd
 952 005c 00          	.byte 0x0
 953 005d 49          	.uleb128 0x49
 954 005e 13          	.uleb128 0x13
 955 005f 00          	.byte 0x0
 956 0060 00          	.byte 0x0
 957 0061 08          	.uleb128 0x8
 958 0062 0D          	.uleb128 0xd
 959 0063 00          	.byte 0x0
 960 0064 03          	.uleb128 0x3
 961 0065 08          	.uleb128 0x8
 962 0066 3A          	.uleb128 0x3a
 963 0067 0B          	.uleb128 0xb
 964 0068 3B          	.uleb128 0x3b
 965 0069 0B          	.uleb128 0xb
 966 006a 49          	.uleb128 0x49
 967 006b 13          	.uleb128 0x13
 968 006c 00          	.byte 0x0
 969 006d 00          	.byte 0x0
 970 006e 09          	.uleb128 0x9
 971 006f 0D          	.uleb128 0xd
MPLAB XC16 ASSEMBLY Listing:   			page 21


 972 0070 00          	.byte 0x0
 973 0071 49          	.uleb128 0x49
 974 0072 13          	.uleb128 0x13
 975 0073 38          	.uleb128 0x38
 976 0074 0A          	.uleb128 0xa
 977 0075 00          	.byte 0x0
 978 0076 00          	.byte 0x0
 979 0077 0A          	.uleb128 0xa
 980 0078 0D          	.uleb128 0xd
 981 0079 00          	.byte 0x0
 982 007a 03          	.uleb128 0x3
 983 007b 0E          	.uleb128 0xe
 984 007c 3A          	.uleb128 0x3a
 985 007d 0B          	.uleb128 0xb
 986 007e 3B          	.uleb128 0x3b
 987 007f 0B          	.uleb128 0xb
 988 0080 49          	.uleb128 0x49
 989 0081 13          	.uleb128 0x13
 990 0082 38          	.uleb128 0x38
 991 0083 0A          	.uleb128 0xa
 992 0084 00          	.byte 0x0
 993 0085 00          	.byte 0x0
 994 0086 0B          	.uleb128 0xb
 995 0087 01          	.uleb128 0x1
 996 0088 01          	.byte 0x1
 997 0089 49          	.uleb128 0x49
 998 008a 13          	.uleb128 0x13
 999 008b 01          	.uleb128 0x1
 1000 008c 13          	.uleb128 0x13
 1001 008d 00          	.byte 0x0
 1002 008e 00          	.byte 0x0
 1003 008f 0C          	.uleb128 0xc
 1004 0090 21          	.uleb128 0x21
 1005 0091 00          	.byte 0x0
 1006 0092 49          	.uleb128 0x49
 1007 0093 13          	.uleb128 0x13
 1008 0094 2F          	.uleb128 0x2f
 1009 0095 0B          	.uleb128 0xb
 1010 0096 00          	.byte 0x0
 1011 0097 00          	.byte 0x0
 1012 0098 0D          	.uleb128 0xd
 1013 0099 0D          	.uleb128 0xd
 1014 009a 00          	.byte 0x0
 1015 009b 03          	.uleb128 0x3
 1016 009c 08          	.uleb128 0x8
 1017 009d 3A          	.uleb128 0x3a
 1018 009e 0B          	.uleb128 0xb
 1019 009f 3B          	.uleb128 0x3b
 1020 00a0 0B          	.uleb128 0xb
 1021 00a1 49          	.uleb128 0x49
 1022 00a2 13          	.uleb128 0x13
 1023 00a3 38          	.uleb128 0x38
 1024 00a4 0A          	.uleb128 0xa
 1025 00a5 00          	.byte 0x0
 1026 00a6 00          	.byte 0x0
 1027 00a7 0E          	.uleb128 0xe
 1028 00a8 2E          	.uleb128 0x2e
MPLAB XC16 ASSEMBLY Listing:   			page 22


 1029 00a9 01          	.byte 0x1
 1030 00aa 3F          	.uleb128 0x3f
 1031 00ab 0C          	.uleb128 0xc
 1032 00ac 03          	.uleb128 0x3
 1033 00ad 08          	.uleb128 0x8
 1034 00ae 3A          	.uleb128 0x3a
 1035 00af 0B          	.uleb128 0xb
 1036 00b0 3B          	.uleb128 0x3b
 1037 00b1 0B          	.uleb128 0xb
 1038 00b2 27          	.uleb128 0x27
 1039 00b3 0C          	.uleb128 0xc
 1040 00b4 11          	.uleb128 0x11
 1041 00b5 01          	.uleb128 0x1
 1042 00b6 12          	.uleb128 0x12
 1043 00b7 01          	.uleb128 0x1
 1044 00b8 40          	.uleb128 0x40
 1045 00b9 0A          	.uleb128 0xa
 1046 00ba 01          	.uleb128 0x1
 1047 00bb 13          	.uleb128 0x13
 1048 00bc 00          	.byte 0x0
 1049 00bd 00          	.byte 0x0
 1050 00be 0F          	.uleb128 0xf
 1051 00bf 05          	.uleb128 0x5
 1052 00c0 00          	.byte 0x0
 1053 00c1 03          	.uleb128 0x3
 1054 00c2 0E          	.uleb128 0xe
 1055 00c3 3A          	.uleb128 0x3a
 1056 00c4 0B          	.uleb128 0xb
 1057 00c5 3B          	.uleb128 0x3b
 1058 00c6 0B          	.uleb128 0xb
 1059 00c7 49          	.uleb128 0x49
 1060 00c8 13          	.uleb128 0x13
 1061 00c9 02          	.uleb128 0x2
 1062 00ca 0A          	.uleb128 0xa
 1063 00cb 00          	.byte 0x0
 1064 00cc 00          	.byte 0x0
 1065 00cd 10          	.uleb128 0x10
 1066 00ce 05          	.uleb128 0x5
 1067 00cf 00          	.byte 0x0
 1068 00d0 03          	.uleb128 0x3
 1069 00d1 08          	.uleb128 0x8
 1070 00d2 3A          	.uleb128 0x3a
 1071 00d3 0B          	.uleb128 0xb
 1072 00d4 3B          	.uleb128 0x3b
 1073 00d5 0B          	.uleb128 0xb
 1074 00d6 49          	.uleb128 0x49
 1075 00d7 13          	.uleb128 0x13
 1076 00d8 02          	.uleb128 0x2
 1077 00d9 0A          	.uleb128 0xa
 1078 00da 00          	.byte 0x0
 1079 00db 00          	.byte 0x0
 1080 00dc 11          	.uleb128 0x11
 1081 00dd 0F          	.uleb128 0xf
 1082 00de 00          	.byte 0x0
 1083 00df 0B          	.uleb128 0xb
 1084 00e0 0B          	.uleb128 0xb
 1085 00e1 49          	.uleb128 0x49
MPLAB XC16 ASSEMBLY Listing:   			page 23


 1086 00e2 13          	.uleb128 0x13
 1087 00e3 00          	.byte 0x0
 1088 00e4 00          	.byte 0x0
 1089 00e5 00          	.byte 0x0
 1090                 	.section .debug_pubnames,info
 1091 0000 98 00 00 00 	.4byte 0x98
 1092 0004 02 00       	.2byte 0x2
 1093 0006 00 00 00 00 	.4byte .Ldebug_info0
 1094 000a 0A 05 00 00 	.4byte 0x50a
 1095 000e 74 03 00 00 	.4byte 0x374
 1096 0012 70 61 72 73 	.asciz "parse_ARM_torque_message"
 1096      65 5F 41 52 
 1096      4D 5F 74 6F 
 1096      72 71 75 65 
 1096      5F 6D 65 73 
 1096      73 61 67 65 
 1096      00 
 1097 002b CC 03 00 00 	.4byte 0x3cc
 1098 002f 70 61 72 73 	.asciz "parse_ARM_status_message"
 1098      65 5F 41 52 
 1098      4D 5F 73 74 
 1098      61 74 75 73 
 1098      5F 6D 65 73 
 1098      73 61 67 65 
 1098      00 
 1099 0048 1E 04 00 00 	.4byte 0x41e
 1100 004c 70 61 72 73 	.asciz "parse_ARM_control_info_message"
 1100      65 5F 41 52 
 1100      4D 5F 63 6F 
 1100      6E 74 72 6F 
 1100      6C 5F 69 6E 
 1100      66 6F 5F 6D 
 1100      65 73 73 61 
 1100      67 65 00 
 1101 006b 73 04 00 00 	.4byte 0x473
 1102 006f 70 61 72 73 	.asciz "parse_ARM_lims_message"
 1102      65 5F 41 52 
 1102      4D 5F 6C 69 
 1102      6D 73 5F 6D 
 1102      65 73 73 61 
 1102      67 65 00 
 1103 0086 C1 04 00 00 	.4byte 0x4c1
 1104 008a 70 61 72 73 	.asciz "parse_can_ARM"
 1104      65 5F 63 61 
 1104      6E 5F 41 52 
 1104      4D 00 
 1105 0098 00 00 00 00 	.4byte 0x0
 1106                 	.section .debug_pubtypes,info
 1107 0000 84 00 00 00 	.4byte 0x84
 1108 0004 02 00       	.2byte 0x2
 1109 0006 00 00 00 00 	.4byte .Ldebug_info0
 1110 000a 0A 05 00 00 	.4byte 0x50a
 1111 000e E0 00 00 00 	.4byte 0xe0
 1112 0012 75 69 6E 74 	.asciz "uint8_t"
 1112      38 5F 74 00 
 1113 001a 00 01 00 00 	.4byte 0x100
 1114 001e 75 69 6E 74 	.asciz "uint16_t"
MPLAB XC16 ASSEMBLY Listing:   			page 24


 1114      31 36 5F 74 
 1114      00 
 1115 0027 D9 01 00 00 	.4byte 0x1d9
 1116 002b 43 41 4E 64 	.asciz "CANdata"
 1116      61 74 61 00 
 1117 0033 25 02 00 00 	.4byte 0x225
 1118 0037 41 52 4D 5F 	.asciz "ARM_torque"
 1118      74 6F 72 71 
 1118      75 65 00 
 1119 0042 51 02 00 00 	.4byte 0x251
 1120 0046 41 52 4D 5F 	.asciz "ARM_status"
 1120      73 74 61 74 
 1120      75 73 00 
 1121 0051 AC 02 00 00 	.4byte 0x2ac
 1122 0055 41 52 4D 5F 	.asciz "ARM_lims"
 1122      6C 69 6D 73 
 1122      00 
 1123 005e 00 03 00 00 	.4byte 0x300
 1124 0062 41 52 4D 5F 	.asciz "ARM_control_info"
 1124      63 6F 6E 74 
 1124      72 6F 6C 5F 
 1124      69 6E 66 6F 
 1124      00 
 1125 0073 60 03 00 00 	.4byte 0x360
 1126 0077 41 52 4D 5F 	.asciz "ARM_CAN_data"
 1126      43 41 4E 5F 
 1126      64 61 74 61 
 1126      00 
 1127 0084 00 00 00 00 	.4byte 0x0
 1128                 	.section .debug_aranges,info
 1129 0000 14 00 00 00 	.4byte 0x14
 1130 0004 02 00       	.2byte 0x2
 1131 0006 00 00 00 00 	.4byte .Ldebug_info0
 1132 000a 04          	.byte 0x4
 1133 000b 00          	.byte 0x0
 1134 000c 00 00       	.2byte 0x0
 1135 000e 00 00       	.2byte 0x0
 1136 0010 00 00 00 00 	.4byte 0x0
 1137 0014 00 00 00 00 	.4byte 0x0
 1138                 	.section .debug_str,info
 1139                 	.LASF0:
 1140 0000 64 61 74 61 	.asciz "data"
 1140      00 
 1141                 	.LASF1:
 1142 0005 63 6F 6E 74 	.asciz "control_info"
 1142      72 6F 6C 5F 
 1142      69 6E 66 6F 
 1142      00 
 1143                 	.section .text,code
 1144              	
 1145              	
 1146              	
 1147              	.section __c30_info,info,bss
 1148                 	__psv_trap_errata:
 1149                 	
 1150                 	.section __c30_signature,info,data
 1151 0000 01 00       	.word 0x0001
MPLAB XC16 ASSEMBLY Listing:   			page 25


 1152 0002 00 00       	.word 0x0000
 1153 0004 00 00       	.word 0x0000
 1154                 	
 1155                 	
 1156                 	
 1157                 	.set ___PA___,0
 1158                 	.end
MPLAB XC16 ASSEMBLY Listing:   			page 26


DEFINED SYMBOLS
                            *ABS*:00000000 /home/user/Documents/FST/Programming/project_sub-zero/lib/can-ids/Devices/ARM_CAN.c
                            *ABS*:00000001 __33EP256MU806
                            *ABS*:00000001 __dsPIC33E
                            *ABS*:00000001 __HAS_DSP
                            *ABS*:00000001 __HAS_DMAV2
                            *ABS*:00000001 __HAS_CODEGUARD
                            *ABS*:00000001 __HAS_PMP_ENHANCED
                            *ABS*:00000001 __HAS_EDS
                            *ABS*:00000001 __HAS_EP
                            *ABS*:00000011 __TARGET_DIVIDE_CYCLES
                            *ABS*:00000001 __C30ELF
    {standard input}:14     .text:00000000 _parse_ARM_torque_message
    {standard input}:18     *ABS*:00000000 ___PA___
    {standard input}:67     .text:00000032 _parse_ARM_status_message
    {standard input}:93     .text:0000004a _parse_ARM_control_info_message
    {standard input}:145    .text:0000007c _parse_ARM_lims_message
    {standard input}:188    .text:000000a4 _parse_can_ARM
    {standard input}:215    *ABS*:00000000 ___BP___
    {standard input}:1148   __c30_info:00000000 __psv_trap_errata
    {standard input}:19     .text:00000000 .L0
                            .text:000000f8 .L9
                            .text:000000d4 .L11
                            .text:000000dc .L7
                            .text:000000e4 .L8
                            .text:00000100 .L5
                            .text:000000ee .L10
                    .debug_abbrev:00000000 .Ldebug_abbrev0
                            .text:00000000 .Ltext0
                            .text:00000108 .Letext0
                      .debug_line:00000000 .Ldebug_line0
                       .debug_str:00000000 .LASF0
                       .debug_str:00000005 .LASF1
                            .text:00000000 .LFB0
                            .text:00000032 .LFE0
                            .text:00000032 .LFB1
                            .text:0000004a .LFE1
                            .text:0000004a .LFB2
                            .text:0000007c .LFE2
                            .text:0000007c .LFB3
                            .text:000000a4 .LFE3
                            .text:000000a4 .LFB4
                            .text:00000108 .LFE4
                     .debug_frame:00000000 .Lframe0
                      .debug_info:00000000 .Ldebug_info0

UNDEFINED SYMBOLS
CORCON

EQUATE SYMBOLS
/home/user/Documents/FST/Programming/project_sub-zero/lib/can-ids/Devices/ARM_CAN.c = 0x0
                  __33EP256MU806 = 0x1
                      __dsPIC33E = 0x1
                       __HAS_DSP = 0x1
                     __HAS_DMAV2 = 0x1
                 __HAS_CODEGUARD = 0x1
              __HAS_PMP_ENHANCED = 0x1
                       __HAS_EDS = 0x1
                        __HAS_EP = 0x1
          __TARGET_DIVIDE_CYCLES = 0x11
                        __C30ELF = 0x1
                        ___PA___ = 0x0
                        ___BP___ = 0x0
