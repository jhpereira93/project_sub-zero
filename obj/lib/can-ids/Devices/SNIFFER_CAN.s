MPLAB XC16 ASSEMBLY Listing:   			page 1


   1              	.file "/home/user/Documents/FST/Programming/project_sub-zero/lib/can-ids/Devices/SNIFFER_CAN.c"
   2              	.section .debug_abbrev,info
   3                 	.Ldebug_abbrev0:
   4                 	.section .debug_info,info
   5                 	.Ldebug_info0:
   6                 	.section .debug_line,info
   7                 	.Ldebug_line0:
   8 0000 C1 00 00 00 	.section .text,code
   8      02 00 A2 00 
   8      00 00 01 01 
   8      FB 0E 0A 00 
   8      01 01 01 01 
   8      00 00 00 01 
   8      6C 69 62 2F 
   8      63 61 6E 2D 
   8      69 64 73 2F 
   9              	.Ltext0:
  10              	.section .text,code
  11              	.align 2
  12              	.global _parse_sniffer_message_stats
  13              	.type _parse_sniffer_message_stats,@function
  14              	_parse_sniffer_message_stats:
  15              	.LFB0:
  16              	.file 1 "lib/can-ids/Devices/SNIFFER_CAN.c"
   1:lib/can-ids/Devices/SNIFFER_CAN.c **** #include "SNIFFER_CAN.h"
   2:lib/can-ids/Devices/SNIFFER_CAN.c **** 
   3:lib/can-ids/Devices/SNIFFER_CAN.c **** void parse_sniffer_message_stats(CANdata msg, SNIFFER_MSG_Data *data) {
  17              	.loc 1 3 0
  18              	.set ___PA___,1
  19 000000  0E 00 FA 	lnk #14
  20              	.LCFI0:
  21              	.loc 1 3 0
  22 000002  00 0F 78 	mov w0,[w14]
  23 000004  22 07 98 	mov w2,[w14+4]
  24 000006  66 07 98 	mov w6,[w14+12]
   4:lib/can-ids/Devices/SNIFFER_CAN.c **** 	data->msgs_read = msg.data[0];
  25              	.loc 1 4 0
  26 000008  2E 01 90 	mov [w14+4],w2
  27 00000a  6E 00 90 	mov [w14+12],w0
  28              	.loc 1 3 0
  29 00000c  11 07 98 	mov w1,[w14+2]
  30 00000e  33 07 98 	mov w3,[w14+6]
  31 000010  44 07 98 	mov w4,[w14+8]
  32 000012  55 07 98 	mov w5,[w14+10]
  33              	.loc 1 4 0
  34 000014  02 08 78 	mov w2,[w0]
   5:lib/can-ids/Devices/SNIFFER_CAN.c **** 	return;
   6:lib/can-ids/Devices/SNIFFER_CAN.c **** }
  35              	.loc 1 6 0
  36 000016  8E 07 78 	mov w14,w15
  37 000018  4F 07 78 	mov [--w15],w14
  38 00001a  00 40 A9 	bclr CORCON,#2
  39 00001c  00 00 06 	return 
  40              	.set ___PA___,0
  41              	.LFE0:
  42              	.size _parse_sniffer_message_stats,.-_parse_sniffer_message_stats
  43              	.align 2
MPLAB XC16 ASSEMBLY Listing:   			page 2


  44              	.global _parse_can_sniffer
  45              	.type _parse_can_sniffer,@function
  46              	_parse_can_sniffer:
  47              	.LFB1:
   7:lib/can-ids/Devices/SNIFFER_CAN.c **** 
   8:lib/can-ids/Devices/SNIFFER_CAN.c **** void parse_can_sniffer(CANdata msg, SNIFFER_MSG_Data *data) {
  48              	.loc 1 8 0
  49              	.set ___PA___,1
  50 00001e  0E 00 FA 	lnk #14
  51              	.LCFI1:
  52              	.loc 1 8 0
  53 000020  00 0F 78 	mov w0,[w14]
   9:lib/can-ids/Devices/SNIFFER_CAN.c **** 	if (msg.msg_id == MSG_ID_SNIFFER_STATS) {
  54              	.loc 1 9 0
  55 000022  00 7E 20 	mov #2016,w0
  56 000024  9E 03 78 	mov [w14],w7
  57              	.loc 1 8 0
  58 000026  11 07 98 	mov w1,[w14+2]
  59 000028  22 07 98 	mov w2,[w14+4]
  60 00002a  33 07 98 	mov w3,[w14+6]
  61 00002c  44 07 98 	mov w4,[w14+8]
  62 00002e  55 07 98 	mov w5,[w14+10]
  63 000030  66 07 98 	mov w6,[w14+12]
  64              	.loc 1 9 0
  65 000032  80 80 63 	and w7,w0,w1
  66 000034  00 7E 20 	mov #2016,w0
  67 000036  80 8F 50 	sub w1,w0,[w15]
  68              	.set ___BP___,0
  69 000038  00 00 3A 	bra nz,.L2
  10:lib/can-ids/Devices/SNIFFER_CAN.c **** 		parse_sniffer_message_stats(msg, data);
  70              	.loc 1 10 0
  71 00003a  6E 03 90 	mov [w14+12],w6
  72 00003c  1E 00 78 	mov [w14],w0
  73 00003e  9E 00 90 	mov [w14+2],w1
  74 000040  2E 01 90 	mov [w14+4],w2
  75 000042  BE 01 90 	mov [w14+6],w3
  76 000044  4E 02 90 	mov [w14+8],w4
  77 000046  DE 02 90 	mov [w14+10],w5
  78 000048  00 00 07 	rcall _parse_sniffer_message_stats
  79              	.L2:
  11:lib/can-ids/Devices/SNIFFER_CAN.c **** 	}
  12:lib/can-ids/Devices/SNIFFER_CAN.c **** 	return;
  13:lib/can-ids/Devices/SNIFFER_CAN.c **** }
  80              	.loc 1 13 0
  81 00004a  8E 07 78 	mov w14,w15
  82 00004c  4F 07 78 	mov [--w15],w14
  83 00004e  00 40 A9 	bclr CORCON,#2
  84 000050  00 00 06 	return 
  85              	.set ___PA___,0
  86              	.LFE1:
  87              	.size _parse_can_sniffer,.-_parse_can_sniffer
  88              	.section .debug_frame,info
  89                 	.Lframe0:
  90 0000 10 00 00 00 	.4byte .LECIE0-.LSCIE0
  91                 	.LSCIE0:
  92 0004 FF FF FF FF 	.4byte 0xffffffff
  93 0008 01          	.byte 0x1
MPLAB XC16 ASSEMBLY Listing:   			page 3


  94 0009 00          	.byte 0
  95 000a 01          	.uleb128 0x1
  96 000b 02          	.sleb128 2
  97 000c 25          	.byte 0x25
  98 000d 12          	.byte 0x12
  99 000e 0F          	.uleb128 0xf
 100 000f 7E          	.sleb128 -2
 101 0010 09          	.byte 0x9
 102 0011 25          	.uleb128 0x25
 103 0012 0F          	.uleb128 0xf
 104 0013 00          	.align 4
 105                 	.LECIE0:
 106                 	.LSFDE0:
 107 0014 18 00 00 00 	.4byte .LEFDE0-.LASFDE0
 108                 	.LASFDE0:
 109 0018 00 00 00 00 	.4byte .Lframe0
 110 001c 00 00 00 00 	.4byte .LFB0
 111 0020 1E 00 00 00 	.4byte .LFE0-.LFB0
 112 0024 04          	.byte 0x4
 113 0025 02 00 00 00 	.4byte .LCFI0-.LFB0
 114 0029 13          	.byte 0x13
 115 002a 7D          	.sleb128 -3
 116 002b 0D          	.byte 0xd
 117 002c 0E          	.uleb128 0xe
 118 002d 8E          	.byte 0x8e
 119 002e 02          	.uleb128 0x2
 120 002f 00          	.align 4
 121                 	.LEFDE0:
 122                 	.LSFDE2:
 123 0030 18 00 00 00 	.4byte .LEFDE2-.LASFDE2
 124                 	.LASFDE2:
 125 0034 00 00 00 00 	.4byte .Lframe0
 126 0038 00 00 00 00 	.4byte .LFB1
 127 003c 34 00 00 00 	.4byte .LFE1-.LFB1
 128 0040 04          	.byte 0x4
 129 0041 02 00 00 00 	.4byte .LCFI1-.LFB1
 130 0045 13          	.byte 0x13
 131 0046 7D          	.sleb128 -3
 132 0047 0D          	.byte 0xd
 133 0048 0E          	.uleb128 0xe
 134 0049 8E          	.byte 0x8e
 135 004a 02          	.uleb128 0x2
 136 004b 00          	.align 4
 137                 	.LEFDE2:
 138                 	.section .text,code
 139              	.Letext0:
 140              	.file 2 "lib/can-ids/CAN_IDs.h"
 141              	.file 3 "/opt/microchip/xc16/v1.36/bin/bin/../../include/lega-c/stdint.h"
 142              	.file 4 "lib/can-ids/Devices/SNIFFER_CAN.h"
 143              	.section .debug_info,info
 144 0000 A2 02 00 00 	.4byte 0x2a2
 145 0004 02 00       	.2byte 0x2
 146 0006 00 00 00 00 	.4byte .Ldebug_abbrev0
 147 000a 04          	.byte 0x4
 148 000b 01          	.uleb128 0x1
 149 000c 47 4E 55 20 	.asciz "GNU C 4.5.1 (XC16, Microchip v1.36) (A) Build date: Dec  5 2018"
 149      43 20 34 2E 
MPLAB XC16 ASSEMBLY Listing:   			page 4


 149      35 2E 31 20 
 149      28 58 43 31 
 149      36 2C 20 4D 
 149      69 63 72 6F 
 149      63 68 69 70 
 149      20 76 31 2E 
 149      33 36 29 20 
 150 004c 01          	.byte 0x1
 151 004d 6C 69 62 2F 	.asciz "lib/can-ids/Devices/SNIFFER_CAN.c"
 151      63 61 6E 2D 
 151      69 64 73 2F 
 151      44 65 76 69 
 151      63 65 73 2F 
 151      53 4E 49 46 
 151      46 45 52 5F 
 151      43 41 4E 2E 
 151      63 00 
 152 006f 2F 68 6F 6D 	.asciz "/home/user/Documents/FST/Programming/project_sub-zero"
 152      65 2F 75 73 
 152      65 72 2F 44 
 152      6F 63 75 6D 
 152      65 6E 74 73 
 152      2F 46 53 54 
 152      2F 50 72 6F 
 152      67 72 61 6D 
 152      6D 69 6E 67 
 153 00a5 00 00 00 00 	.4byte .Ltext0
 154 00a9 00 00 00 00 	.4byte .Letext0
 155 00ad 00 00 00 00 	.4byte .Ldebug_line0
 156 00b1 02          	.uleb128 0x2
 157 00b2 01          	.byte 0x1
 158 00b3 06          	.byte 0x6
 159 00b4 73 69 67 6E 	.asciz "signed char"
 159      65 64 20 63 
 159      68 61 72 00 
 160 00c0 02          	.uleb128 0x2
 161 00c1 02          	.byte 0x2
 162 00c2 05          	.byte 0x5
 163 00c3 69 6E 74 00 	.asciz "int"
 164 00c7 02          	.uleb128 0x2
 165 00c8 04          	.byte 0x4
 166 00c9 05          	.byte 0x5
 167 00ca 6C 6F 6E 67 	.asciz "long int"
 167      20 69 6E 74 
 167      00 
 168 00d3 02          	.uleb128 0x2
 169 00d4 08          	.byte 0x8
 170 00d5 05          	.byte 0x5
 171 00d6 6C 6F 6E 67 	.asciz "long long int"
 171      20 6C 6F 6E 
 171      67 20 69 6E 
 171      74 00 
 172 00e4 02          	.uleb128 0x2
 173 00e5 01          	.byte 0x1
 174 00e6 08          	.byte 0x8
 175 00e7 75 6E 73 69 	.asciz "unsigned char"
 175      67 6E 65 64 
MPLAB XC16 ASSEMBLY Listing:   			page 5


 175      20 63 68 61 
 175      72 00 
 176 00f5 03          	.uleb128 0x3
 177 00f6 75 69 6E 74 	.asciz "uint16_t"
 177      31 36 5F 74 
 177      00 
 178 00ff 03          	.byte 0x3
 179 0100 31          	.byte 0x31
 180 0101 05 01 00 00 	.4byte 0x105
 181 0105 02          	.uleb128 0x2
 182 0106 02          	.byte 0x2
 183 0107 07          	.byte 0x7
 184 0108 75 6E 73 69 	.asciz "unsigned int"
 184      67 6E 65 64 
 184      20 69 6E 74 
 184      00 
 185 0115 02          	.uleb128 0x2
 186 0116 04          	.byte 0x4
 187 0117 07          	.byte 0x7
 188 0118 6C 6F 6E 67 	.asciz "long unsigned int"
 188      20 75 6E 73 
 188      69 67 6E 65 
 188      64 20 69 6E 
 188      74 00 
 189 012a 02          	.uleb128 0x2
 190 012b 08          	.byte 0x8
 191 012c 07          	.byte 0x7
 192 012d 6C 6F 6E 67 	.asciz "long long unsigned int"
 192      20 6C 6F 6E 
 192      67 20 75 6E 
 192      73 69 67 6E 
 192      65 64 20 69 
 192      6E 74 00 
 193 0144 04          	.uleb128 0x4
 194 0145 02          	.byte 0x2
 195 0146 02          	.byte 0x2
 196 0147 12          	.byte 0x12
 197 0148 75 01 00 00 	.4byte 0x175
 198 014c 05          	.uleb128 0x5
 199 014d 64 65 76 5F 	.asciz "dev_id"
 199      69 64 00 
 200 0154 02          	.byte 0x2
 201 0155 13          	.byte 0x13
 202 0156 F5 00 00 00 	.4byte 0xf5
 203 015a 02          	.byte 0x2
 204 015b 05          	.byte 0x5
 205 015c 0B          	.byte 0xb
 206 015d 02          	.byte 0x2
 207 015e 23          	.byte 0x23
 208 015f 00          	.uleb128 0x0
 209 0160 05          	.uleb128 0x5
 210 0161 6D 73 67 5F 	.asciz "msg_id"
 210      69 64 00 
 211 0168 02          	.byte 0x2
 212 0169 16          	.byte 0x16
 213 016a F5 00 00 00 	.4byte 0xf5
 214 016e 02          	.byte 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 6


 215 016f 06          	.byte 0x6
 216 0170 05          	.byte 0x5
 217 0171 02          	.byte 0x2
 218 0172 23          	.byte 0x23
 219 0173 00          	.uleb128 0x0
 220 0174 00          	.byte 0x0
 221 0175 06          	.uleb128 0x6
 222 0176 02          	.byte 0x2
 223 0177 02          	.byte 0x2
 224 0178 11          	.byte 0x11
 225 0179 8E 01 00 00 	.4byte 0x18e
 226 017d 07          	.uleb128 0x7
 227 017e 44 01 00 00 	.4byte 0x144
 228 0182 08          	.uleb128 0x8
 229 0183 73 69 64 00 	.asciz "sid"
 230 0187 02          	.byte 0x2
 231 0188 18          	.byte 0x18
 232 0189 F5 00 00 00 	.4byte 0xf5
 233 018d 00          	.byte 0x0
 234 018e 04          	.uleb128 0x4
 235 018f 0C          	.byte 0xc
 236 0190 02          	.byte 0x2
 237 0191 10          	.byte 0x10
 238 0192 BF 01 00 00 	.4byte 0x1bf
 239 0196 09          	.uleb128 0x9
 240 0197 75 01 00 00 	.4byte 0x175
 241 019b 02          	.byte 0x2
 242 019c 23          	.byte 0x23
 243 019d 00          	.uleb128 0x0
 244 019e 05          	.uleb128 0x5
 245 019f 64 6C 63 00 	.asciz "dlc"
 246 01a3 02          	.byte 0x2
 247 01a4 1A          	.byte 0x1a
 248 01a5 F5 00 00 00 	.4byte 0xf5
 249 01a9 02          	.byte 0x2
 250 01aa 04          	.byte 0x4
 251 01ab 0C          	.byte 0xc
 252 01ac 02          	.byte 0x2
 253 01ad 23          	.byte 0x23
 254 01ae 02          	.uleb128 0x2
 255 01af 0A          	.uleb128 0xa
 256 01b0 64 61 74 61 	.asciz "data"
 256      00 
 257 01b5 02          	.byte 0x2
 258 01b6 1B          	.byte 0x1b
 259 01b7 BF 01 00 00 	.4byte 0x1bf
 260 01bb 02          	.byte 0x2
 261 01bc 23          	.byte 0x23
 262 01bd 04          	.uleb128 0x4
 263 01be 00          	.byte 0x0
 264 01bf 0B          	.uleb128 0xb
 265 01c0 F5 00 00 00 	.4byte 0xf5
 266 01c4 CF 01 00 00 	.4byte 0x1cf
 267 01c8 0C          	.uleb128 0xc
 268 01c9 05 01 00 00 	.4byte 0x105
 269 01cd 03          	.byte 0x3
 270 01ce 00          	.byte 0x0
MPLAB XC16 ASSEMBLY Listing:   			page 7


 271 01cf 03          	.uleb128 0x3
 272 01d0 43 41 4E 64 	.asciz "CANdata"
 272      61 74 61 00 
 273 01d8 02          	.byte 0x2
 274 01d9 1C          	.byte 0x1c
 275 01da 8E 01 00 00 	.4byte 0x18e
 276 01de 04          	.uleb128 0x4
 277 01df 02          	.byte 0x2
 278 01e0 04          	.byte 0x4
 279 01e1 08          	.byte 0x8
 280 01e2 FB 01 00 00 	.4byte 0x1fb
 281 01e6 0A          	.uleb128 0xa
 282 01e7 6D 73 67 73 	.asciz "msgs_read"
 282      5F 72 65 61 
 282      64 00 
 283 01f1 04          	.byte 0x4
 284 01f2 09          	.byte 0x9
 285 01f3 F5 00 00 00 	.4byte 0xf5
 286 01f7 02          	.byte 0x2
 287 01f8 23          	.byte 0x23
 288 01f9 00          	.uleb128 0x0
 289 01fa 00          	.byte 0x0
 290 01fb 03          	.uleb128 0x3
 291 01fc 53 4E 49 46 	.asciz "SNIFFER_MSG_Data"
 291      46 45 52 5F 
 291      4D 53 47 5F 
 291      44 61 74 61 
 291      00 
 292 020d 04          	.byte 0x4
 293 020e 0A          	.byte 0xa
 294 020f DE 01 00 00 	.4byte 0x1de
 295 0213 0D          	.uleb128 0xd
 296 0214 01          	.byte 0x1
 297 0215 70 61 72 73 	.asciz "parse_sniffer_message_stats"
 297      65 5F 73 6E 
 297      69 66 66 65 
 297      72 5F 6D 65 
 297      73 73 61 67 
 297      65 5F 73 74 
 297      61 74 73 00 
 298 0231 01          	.byte 0x1
 299 0232 03          	.byte 0x3
 300 0233 01          	.byte 0x1
 301 0234 00 00 00 00 	.4byte .LFB0
 302 0238 00 00 00 00 	.4byte .LFE0
 303 023c 01          	.byte 0x1
 304 023d 5E          	.byte 0x5e
 305 023e 60 02 00 00 	.4byte 0x260
 306 0242 0E          	.uleb128 0xe
 307 0243 6D 73 67 00 	.asciz "msg"
 308 0247 01          	.byte 0x1
 309 0248 03          	.byte 0x3
 310 0249 CF 01 00 00 	.4byte 0x1cf
 311 024d 02          	.byte 0x2
 312 024e 7E          	.byte 0x7e
 313 024f 00          	.sleb128 0
 314 0250 0E          	.uleb128 0xe
MPLAB XC16 ASSEMBLY Listing:   			page 8


 315 0251 64 61 74 61 	.asciz "data"
 315      00 
 316 0256 01          	.byte 0x1
 317 0257 03          	.byte 0x3
 318 0258 60 02 00 00 	.4byte 0x260
 319 025c 02          	.byte 0x2
 320 025d 7E          	.byte 0x7e
 321 025e 0C          	.sleb128 12
 322 025f 00          	.byte 0x0
 323 0260 0F          	.uleb128 0xf
 324 0261 02          	.byte 0x2
 325 0262 FB 01 00 00 	.4byte 0x1fb
 326 0266 10          	.uleb128 0x10
 327 0267 01          	.byte 0x1
 328 0268 70 61 72 73 	.asciz "parse_can_sniffer"
 328      65 5F 63 61 
 328      6E 5F 73 6E 
 328      69 66 66 65 
 328      72 00 
 329 027a 01          	.byte 0x1
 330 027b 08          	.byte 0x8
 331 027c 01          	.byte 0x1
 332 027d 00 00 00 00 	.4byte .LFB1
 333 0281 00 00 00 00 	.4byte .LFE1
 334 0285 01          	.byte 0x1
 335 0286 5E          	.byte 0x5e
 336 0287 0E          	.uleb128 0xe
 337 0288 6D 73 67 00 	.asciz "msg"
 338 028c 01          	.byte 0x1
 339 028d 08          	.byte 0x8
 340 028e CF 01 00 00 	.4byte 0x1cf
 341 0292 02          	.byte 0x2
 342 0293 7E          	.byte 0x7e
 343 0294 00          	.sleb128 0
 344 0295 0E          	.uleb128 0xe
 345 0296 64 61 74 61 	.asciz "data"
 345      00 
 346 029b 01          	.byte 0x1
 347 029c 08          	.byte 0x8
 348 029d 60 02 00 00 	.4byte 0x260
 349 02a1 02          	.byte 0x2
 350 02a2 7E          	.byte 0x7e
 351 02a3 0C          	.sleb128 12
 352 02a4 00          	.byte 0x0
 353 02a5 00          	.byte 0x0
 354                 	.section .debug_abbrev,info
 355 0000 01          	.uleb128 0x1
 356 0001 11          	.uleb128 0x11
 357 0002 01          	.byte 0x1
 358 0003 25          	.uleb128 0x25
 359 0004 08          	.uleb128 0x8
 360 0005 13          	.uleb128 0x13
 361 0006 0B          	.uleb128 0xb
 362 0007 03          	.uleb128 0x3
 363 0008 08          	.uleb128 0x8
 364 0009 1B          	.uleb128 0x1b
 365 000a 08          	.uleb128 0x8
MPLAB XC16 ASSEMBLY Listing:   			page 9


 366 000b 11          	.uleb128 0x11
 367 000c 01          	.uleb128 0x1
 368 000d 12          	.uleb128 0x12
 369 000e 01          	.uleb128 0x1
 370 000f 10          	.uleb128 0x10
 371 0010 06          	.uleb128 0x6
 372 0011 00          	.byte 0x0
 373 0012 00          	.byte 0x0
 374 0013 02          	.uleb128 0x2
 375 0014 24          	.uleb128 0x24
 376 0015 00          	.byte 0x0
 377 0016 0B          	.uleb128 0xb
 378 0017 0B          	.uleb128 0xb
 379 0018 3E          	.uleb128 0x3e
 380 0019 0B          	.uleb128 0xb
 381 001a 03          	.uleb128 0x3
 382 001b 08          	.uleb128 0x8
 383 001c 00          	.byte 0x0
 384 001d 00          	.byte 0x0
 385 001e 03          	.uleb128 0x3
 386 001f 16          	.uleb128 0x16
 387 0020 00          	.byte 0x0
 388 0021 03          	.uleb128 0x3
 389 0022 08          	.uleb128 0x8
 390 0023 3A          	.uleb128 0x3a
 391 0024 0B          	.uleb128 0xb
 392 0025 3B          	.uleb128 0x3b
 393 0026 0B          	.uleb128 0xb
 394 0027 49          	.uleb128 0x49
 395 0028 13          	.uleb128 0x13
 396 0029 00          	.byte 0x0
 397 002a 00          	.byte 0x0
 398 002b 04          	.uleb128 0x4
 399 002c 13          	.uleb128 0x13
 400 002d 01          	.byte 0x1
 401 002e 0B          	.uleb128 0xb
 402 002f 0B          	.uleb128 0xb
 403 0030 3A          	.uleb128 0x3a
 404 0031 0B          	.uleb128 0xb
 405 0032 3B          	.uleb128 0x3b
 406 0033 0B          	.uleb128 0xb
 407 0034 01          	.uleb128 0x1
 408 0035 13          	.uleb128 0x13
 409 0036 00          	.byte 0x0
 410 0037 00          	.byte 0x0
 411 0038 05          	.uleb128 0x5
 412 0039 0D          	.uleb128 0xd
 413 003a 00          	.byte 0x0
 414 003b 03          	.uleb128 0x3
 415 003c 08          	.uleb128 0x8
 416 003d 3A          	.uleb128 0x3a
 417 003e 0B          	.uleb128 0xb
 418 003f 3B          	.uleb128 0x3b
 419 0040 0B          	.uleb128 0xb
 420 0041 49          	.uleb128 0x49
 421 0042 13          	.uleb128 0x13
 422 0043 0B          	.uleb128 0xb
MPLAB XC16 ASSEMBLY Listing:   			page 10


 423 0044 0B          	.uleb128 0xb
 424 0045 0D          	.uleb128 0xd
 425 0046 0B          	.uleb128 0xb
 426 0047 0C          	.uleb128 0xc
 427 0048 0B          	.uleb128 0xb
 428 0049 38          	.uleb128 0x38
 429 004a 0A          	.uleb128 0xa
 430 004b 00          	.byte 0x0
 431 004c 00          	.byte 0x0
 432 004d 06          	.uleb128 0x6
 433 004e 17          	.uleb128 0x17
 434 004f 01          	.byte 0x1
 435 0050 0B          	.uleb128 0xb
 436 0051 0B          	.uleb128 0xb
 437 0052 3A          	.uleb128 0x3a
 438 0053 0B          	.uleb128 0xb
 439 0054 3B          	.uleb128 0x3b
 440 0055 0B          	.uleb128 0xb
 441 0056 01          	.uleb128 0x1
 442 0057 13          	.uleb128 0x13
 443 0058 00          	.byte 0x0
 444 0059 00          	.byte 0x0
 445 005a 07          	.uleb128 0x7
 446 005b 0D          	.uleb128 0xd
 447 005c 00          	.byte 0x0
 448 005d 49          	.uleb128 0x49
 449 005e 13          	.uleb128 0x13
 450 005f 00          	.byte 0x0
 451 0060 00          	.byte 0x0
 452 0061 08          	.uleb128 0x8
 453 0062 0D          	.uleb128 0xd
 454 0063 00          	.byte 0x0
 455 0064 03          	.uleb128 0x3
 456 0065 08          	.uleb128 0x8
 457 0066 3A          	.uleb128 0x3a
 458 0067 0B          	.uleb128 0xb
 459 0068 3B          	.uleb128 0x3b
 460 0069 0B          	.uleb128 0xb
 461 006a 49          	.uleb128 0x49
 462 006b 13          	.uleb128 0x13
 463 006c 00          	.byte 0x0
 464 006d 00          	.byte 0x0
 465 006e 09          	.uleb128 0x9
 466 006f 0D          	.uleb128 0xd
 467 0070 00          	.byte 0x0
 468 0071 49          	.uleb128 0x49
 469 0072 13          	.uleb128 0x13
 470 0073 38          	.uleb128 0x38
 471 0074 0A          	.uleb128 0xa
 472 0075 00          	.byte 0x0
 473 0076 00          	.byte 0x0
 474 0077 0A          	.uleb128 0xa
 475 0078 0D          	.uleb128 0xd
 476 0079 00          	.byte 0x0
 477 007a 03          	.uleb128 0x3
 478 007b 08          	.uleb128 0x8
 479 007c 3A          	.uleb128 0x3a
MPLAB XC16 ASSEMBLY Listing:   			page 11


 480 007d 0B          	.uleb128 0xb
 481 007e 3B          	.uleb128 0x3b
 482 007f 0B          	.uleb128 0xb
 483 0080 49          	.uleb128 0x49
 484 0081 13          	.uleb128 0x13
 485 0082 38          	.uleb128 0x38
 486 0083 0A          	.uleb128 0xa
 487 0084 00          	.byte 0x0
 488 0085 00          	.byte 0x0
 489 0086 0B          	.uleb128 0xb
 490 0087 01          	.uleb128 0x1
 491 0088 01          	.byte 0x1
 492 0089 49          	.uleb128 0x49
 493 008a 13          	.uleb128 0x13
 494 008b 01          	.uleb128 0x1
 495 008c 13          	.uleb128 0x13
 496 008d 00          	.byte 0x0
 497 008e 00          	.byte 0x0
 498 008f 0C          	.uleb128 0xc
 499 0090 21          	.uleb128 0x21
 500 0091 00          	.byte 0x0
 501 0092 49          	.uleb128 0x49
 502 0093 13          	.uleb128 0x13
 503 0094 2F          	.uleb128 0x2f
 504 0095 0B          	.uleb128 0xb
 505 0096 00          	.byte 0x0
 506 0097 00          	.byte 0x0
 507 0098 0D          	.uleb128 0xd
 508 0099 2E          	.uleb128 0x2e
 509 009a 01          	.byte 0x1
 510 009b 3F          	.uleb128 0x3f
 511 009c 0C          	.uleb128 0xc
 512 009d 03          	.uleb128 0x3
 513 009e 08          	.uleb128 0x8
 514 009f 3A          	.uleb128 0x3a
 515 00a0 0B          	.uleb128 0xb
 516 00a1 3B          	.uleb128 0x3b
 517 00a2 0B          	.uleb128 0xb
 518 00a3 27          	.uleb128 0x27
 519 00a4 0C          	.uleb128 0xc
 520 00a5 11          	.uleb128 0x11
 521 00a6 01          	.uleb128 0x1
 522 00a7 12          	.uleb128 0x12
 523 00a8 01          	.uleb128 0x1
 524 00a9 40          	.uleb128 0x40
 525 00aa 0A          	.uleb128 0xa
 526 00ab 01          	.uleb128 0x1
 527 00ac 13          	.uleb128 0x13
 528 00ad 00          	.byte 0x0
 529 00ae 00          	.byte 0x0
 530 00af 0E          	.uleb128 0xe
 531 00b0 05          	.uleb128 0x5
 532 00b1 00          	.byte 0x0
 533 00b2 03          	.uleb128 0x3
 534 00b3 08          	.uleb128 0x8
 535 00b4 3A          	.uleb128 0x3a
 536 00b5 0B          	.uleb128 0xb
MPLAB XC16 ASSEMBLY Listing:   			page 12


 537 00b6 3B          	.uleb128 0x3b
 538 00b7 0B          	.uleb128 0xb
 539 00b8 49          	.uleb128 0x49
 540 00b9 13          	.uleb128 0x13
 541 00ba 02          	.uleb128 0x2
 542 00bb 0A          	.uleb128 0xa
 543 00bc 00          	.byte 0x0
 544 00bd 00          	.byte 0x0
 545 00be 0F          	.uleb128 0xf
 546 00bf 0F          	.uleb128 0xf
 547 00c0 00          	.byte 0x0
 548 00c1 0B          	.uleb128 0xb
 549 00c2 0B          	.uleb128 0xb
 550 00c3 49          	.uleb128 0x49
 551 00c4 13          	.uleb128 0x13
 552 00c5 00          	.byte 0x0
 553 00c6 00          	.byte 0x0
 554 00c7 10          	.uleb128 0x10
 555 00c8 2E          	.uleb128 0x2e
 556 00c9 01          	.byte 0x1
 557 00ca 3F          	.uleb128 0x3f
 558 00cb 0C          	.uleb128 0xc
 559 00cc 03          	.uleb128 0x3
 560 00cd 08          	.uleb128 0x8
 561 00ce 3A          	.uleb128 0x3a
 562 00cf 0B          	.uleb128 0xb
 563 00d0 3B          	.uleb128 0x3b
 564 00d1 0B          	.uleb128 0xb
 565 00d2 27          	.uleb128 0x27
 566 00d3 0C          	.uleb128 0xc
 567 00d4 11          	.uleb128 0x11
 568 00d5 01          	.uleb128 0x1
 569 00d6 12          	.uleb128 0x12
 570 00d7 01          	.uleb128 0x1
 571 00d8 40          	.uleb128 0x40
 572 00d9 0A          	.uleb128 0xa
 573 00da 00          	.byte 0x0
 574 00db 00          	.byte 0x0
 575 00dc 00          	.byte 0x0
 576                 	.section .debug_pubnames,info
 577 0000 44 00 00 00 	.4byte 0x44
 578 0004 02 00       	.2byte 0x2
 579 0006 00 00 00 00 	.4byte .Ldebug_info0
 580 000a A6 02 00 00 	.4byte 0x2a6
 581 000e 13 02 00 00 	.4byte 0x213
 582 0012 70 61 72 73 	.asciz "parse_sniffer_message_stats"
 582      65 5F 73 6E 
 582      69 66 66 65 
 582      72 5F 6D 65 
 582      73 73 61 67 
 582      65 5F 73 74 
 582      61 74 73 00 
 583 002e 66 02 00 00 	.4byte 0x266
 584 0032 70 61 72 73 	.asciz "parse_can_sniffer"
 584      65 5F 63 61 
 584      6E 5F 73 6E 
 584      69 66 66 65 
MPLAB XC16 ASSEMBLY Listing:   			page 13


 584      72 00 
 585 0044 00 00 00 00 	.4byte 0x0
 586                 	.section .debug_pubtypes,info
 587 0000 3C 00 00 00 	.4byte 0x3c
 588 0004 02 00       	.2byte 0x2
 589 0006 00 00 00 00 	.4byte .Ldebug_info0
 590 000a A6 02 00 00 	.4byte 0x2a6
 591 000e F5 00 00 00 	.4byte 0xf5
 592 0012 75 69 6E 74 	.asciz "uint16_t"
 592      31 36 5F 74 
 592      00 
 593 001b CF 01 00 00 	.4byte 0x1cf
 594 001f 43 41 4E 64 	.asciz "CANdata"
 594      61 74 61 00 
 595 0027 FB 01 00 00 	.4byte 0x1fb
 596 002b 53 4E 49 46 	.asciz "SNIFFER_MSG_Data"
 596      46 45 52 5F 
 596      4D 53 47 5F 
 596      44 61 74 61 
 596      00 
 597 003c 00 00 00 00 	.4byte 0x0
 598                 	.section .debug_aranges,info
 599 0000 14 00 00 00 	.4byte 0x14
 600 0004 02 00       	.2byte 0x2
 601 0006 00 00 00 00 	.4byte .Ldebug_info0
 602 000a 04          	.byte 0x4
 603 000b 00          	.byte 0x0
 604 000c 00 00       	.2byte 0x0
 605 000e 00 00       	.2byte 0x0
 606 0010 00 00 00 00 	.4byte 0x0
 607 0014 00 00 00 00 	.4byte 0x0
 608                 	.section .debug_str,info
 609                 	.section .text,code
 610              	
 611              	
 612              	
 613              	.section __c30_info,info,bss
 614                 	__psv_trap_errata:
 615                 	
 616                 	.section __c30_signature,info,data
 617 0000 01 00       	.word 0x0001
 618 0002 00 00       	.word 0x0000
 619 0004 00 00       	.word 0x0000
 620                 	
 621                 	
 622                 	
 623                 	.set ___PA___,0
 624                 	.end
MPLAB XC16 ASSEMBLY Listing:   			page 14


DEFINED SYMBOLS
                            *ABS*:00000000 /home/user/Documents/FST/Programming/project_sub-zero/lib/can-ids/Devices/SNIFFER_CAN.c
                            *ABS*:00000001 __33EP256MU806
                            *ABS*:00000001 __dsPIC33E
                            *ABS*:00000001 __HAS_DSP
                            *ABS*:00000001 __HAS_DMAV2
                            *ABS*:00000001 __HAS_CODEGUARD
                            *ABS*:00000001 __HAS_PMP_ENHANCED
                            *ABS*:00000001 __HAS_EDS
                            *ABS*:00000001 __HAS_EP
                            *ABS*:00000011 __TARGET_DIVIDE_CYCLES
                            *ABS*:00000001 __C30ELF
    {standard input}:14     .text:00000000 _parse_sniffer_message_stats
    {standard input}:18     *ABS*:00000000 ___PA___
    {standard input}:46     .text:0000001e _parse_can_sniffer
    {standard input}:68     *ABS*:00000000 ___BP___
    {standard input}:614    __c30_info:00000000 __psv_trap_errata
    {standard input}:19     .text:00000000 .L0
                            .text:0000004a .L2
                    .debug_abbrev:00000000 .Ldebug_abbrev0
                            .text:00000000 .Ltext0
                            .text:00000052 .Letext0
                      .debug_line:00000000 .Ldebug_line0
                            .text:00000000 .LFB0
                            .text:0000001e .LFE0
                            .text:0000001e .LFB1
                            .text:00000052 .LFE1
                     .debug_frame:00000000 .Lframe0
                      .debug_info:00000000 .Ldebug_info0

UNDEFINED SYMBOLS
CORCON

EQUATE SYMBOLS
/home/user/Documents/FST/Programming/project_sub-zero/lib/can-ids/Devices/SNIFFER_CAN.c = 0x0
                  __33EP256MU806 = 0x1
                      __dsPIC33E = 0x1
                       __HAS_DSP = 0x1
                     __HAS_DMAV2 = 0x1
                 __HAS_CODEGUARD = 0x1
              __HAS_PMP_ENHANCED = 0x1
                       __HAS_EDS = 0x1
                        __HAS_EP = 0x1
          __TARGET_DIVIDE_CYCLES = 0x11
                        __C30ELF = 0x1
                        ___PA___ = 0x0
                        ___BP___ = 0x0
