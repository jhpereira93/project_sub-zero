MPLAB XC16 ASSEMBLY Listing:   			page 1


   1              	.file "/home/user/Documents/FST/Programming/project_sub-zero/lib/can-ids/Devices/GPS_CAN.c"
   2              	.section .debug_abbrev,info
   3                 	.Ldebug_abbrev0:
   4                 	.section .debug_info,info
   5                 	.Ldebug_info0:
   6                 	.section .debug_line,info
   7                 	.Ldebug_line0:
   8 0000 E5 00 00 00 	.section .text,code
   8      02 00 A5 00 
   8      00 00 01 01 
   8      FB 0E 0A 00 
   8      01 01 01 01 
   8      00 00 00 01 
   8      6C 69 62 2F 
   8      63 61 6E 2D 
   8      69 64 73 2F 
   9              	.Ltext0:
  10              	.section .text,code
  11              	.align 2
  12              	.global _parse_can_gps
  13              	.type _parse_can_gps,@function
  14              	_parse_can_gps:
  15              	.LFB0:
  16              	.file 1 "lib/can-ids/Devices/GPS_CAN.c"
   1:lib/can-ids/Devices/GPS_CAN.c **** #include "GPS_CAN.h"
   2:lib/can-ids/Devices/GPS_CAN.c **** 
   3:lib/can-ids/Devices/GPS_CAN.c **** void parse_can_gps (CANdata message, GPS_PARSED_DATA *data){
  17              	.loc 1 3 0
  18              	.set ___PA___,1
  19 000000  0E 00 FA 	lnk #14
  20              	.LCFI0:
  21              	.loc 1 3 0
  22 000002  00 0F 78 	mov w0,[w14]
  23 000004  11 07 98 	mov w1,[w14+2]
  24 000006  22 07 98 	mov w2,[w14+4]
  25 000008  33 07 98 	mov w3,[w14+6]
  26 00000a  44 07 98 	mov w4,[w14+8]
  27 00000c  55 07 98 	mov w5,[w14+10]
  28 00000e  66 07 98 	mov w6,[w14+12]
   4:lib/can-ids/Devices/GPS_CAN.c **** 
   5:lib/can-ids/Devices/GPS_CAN.c ****     parse_gps_message_clock       (message, data);
  29              	.loc 1 5 0
  30 000010  1E 00 78 	mov [w14],w0
  31 000012  6E 03 90 	mov [w14+12],w6
  32 000014  9E 00 90 	mov [w14+2],w1
  33 000016  2E 01 90 	mov [w14+4],w2
  34 000018  BE 01 90 	mov [w14+6],w3
  35 00001a  4E 02 90 	mov [w14+8],w4
  36 00001c  DE 02 90 	mov [w14+10],w5
  37 00001e  00 00 07 	rcall _parse_gps_message_clock
   6:lib/can-ids/Devices/GPS_CAN.c ****     parse_gps_message_velocity    (message, data);
  38              	.loc 1 6 0
  39 000020  6E 03 90 	mov [w14+12],w6
  40 000022  1E 00 78 	mov [w14],w0
  41 000024  9E 00 90 	mov [w14+2],w1
  42 000026  2E 01 90 	mov [w14+4],w2
  43 000028  BE 01 90 	mov [w14+6],w3
MPLAB XC16 ASSEMBLY Listing:   			page 2


  44 00002a  4E 02 90 	mov [w14+8],w4
  45 00002c  DE 02 90 	mov [w14+10],w5
  46 00002e  00 00 07 	rcall _parse_gps_message_velocity
   7:lib/can-ids/Devices/GPS_CAN.c ****     parse_gps_message_coordinates (message, data);
  47              	.loc 1 7 0
  48 000030  6E 03 90 	mov [w14+12],w6
  49 000032  1E 00 78 	mov [w14],w0
  50 000034  9E 00 90 	mov [w14+2],w1
  51 000036  2E 01 90 	mov [w14+4],w2
  52 000038  BE 01 90 	mov [w14+6],w3
  53 00003a  4E 02 90 	mov [w14+8],w4
  54 00003c  DE 02 90 	mov [w14+10],w5
  55 00003e  00 00 07 	rcall _parse_gps_message_coordinates
   8:lib/can-ids/Devices/GPS_CAN.c **** 
   9:lib/can-ids/Devices/GPS_CAN.c ****     return;
  10:lib/can-ids/Devices/GPS_CAN.c **** } 
  56              	.loc 1 10 0
  57 000040  8E 07 78 	mov w14,w15
  58 000042  4F 07 78 	mov [--w15],w14
  59 000044  00 40 A9 	bclr CORCON,#2
  60 000046  00 00 06 	return 
  61              	.set ___PA___,0
  62              	.LFE0:
  63              	.size _parse_can_gps,.-_parse_can_gps
  64              	.align 2
  65              	.global _parse_gps_message_clock
  66              	.type _parse_gps_message_clock,@function
  67              	_parse_gps_message_clock:
  68              	.LFB1:
  11:lib/can-ids/Devices/GPS_CAN.c **** 
  12:lib/can-ids/Devices/GPS_CAN.c **** void parse_gps_message_clock (CANdata message, GPS_PARSED_DATA *data){
  69              	.loc 1 12 0
  70              	.set ___PA___,1
  71 000048  0E 00 FA 	lnk #14
  72              	.LCFI1:
  73              	.loc 1 12 0
  74 00004a  00 0F 78 	mov w0,[w14]
  75 00004c  11 07 98 	mov w1,[w14+2]
  13:lib/can-ids/Devices/GPS_CAN.c **** 
  14:lib/can-ids/Devices/GPS_CAN.c ****     if(message.dev_id != DEVICE_ID_GPS){
  76              	.loc 1 14 0
  77 00004e  1E 00 78 	mov [w14],w0
  78              	.loc 1 12 0
  79 000050  22 07 98 	mov w2,[w14+4]
  80 000052  33 07 98 	mov w3,[w14+6]
  81 000054  44 07 98 	mov w4,[w14+8]
  82 000056  55 07 98 	mov w5,[w14+10]
  83 000058  66 07 98 	mov w6,[w14+12]
  84              	.loc 1 14 0
  85 00005a  7F 00 60 	and w0,#31,w0
  86 00005c  F6 0F 50 	sub w0,#22,[w15]
  87              	.set ___BP___,0
  88 00005e  00 00 3A 	bra nz,.L6
  89              	.L3:
  15:lib/can-ids/Devices/GPS_CAN.c ****         return;
  16:lib/can-ids/Devices/GPS_CAN.c ****     }
  17:lib/can-ids/Devices/GPS_CAN.c ****     else if(message.msg_id == MSG_ID_GPS_CLOCK_SYNC){
MPLAB XC16 ASSEMBLY Listing:   			page 3


  90              	.loc 1 17 0
  91 000060  1E 01 78 	mov [w14],w2
  92 000062  01 7E 20 	mov #2016,w1
  93 000064  00 40 20 	mov #1024,w0
  94 000066  81 00 61 	and w2,w1,w1
  95 000068  80 8F 50 	sub w1,w0,[w15]
  96              	.set ___BP___,0
  97 00006a  00 00 3A 	bra nz,.L7
  18:lib/can-ids/Devices/GPS_CAN.c **** 
  19:lib/can-ids/Devices/GPS_CAN.c ****         data->miliseconds = message.data[1];
  98              	.loc 1 19 0
  99 00006c  BE 00 90 	mov [w14+6],w1
 100 00006e  6E 00 90 	mov [w14+12],w0
 101 000070  01 08 78 	mov w1,[w0]
  20:lib/can-ids/Devices/GPS_CAN.c ****         return;
 102              	.loc 1 20 0
 103 000072  00 00 37 	bra .L2
 104              	.L6:
 105 000074  00 00 37 	bra .L2
 106              	.L7:
 107              	.L2:
  21:lib/can-ids/Devices/GPS_CAN.c ****     }
  22:lib/can-ids/Devices/GPS_CAN.c ****     else    
  23:lib/can-ids/Devices/GPS_CAN.c ****         return;
  24:lib/can-ids/Devices/GPS_CAN.c **** }
 108              	.loc 1 24 0
 109 000076  8E 07 78 	mov w14,w15
 110 000078  4F 07 78 	mov [--w15],w14
 111 00007a  00 40 A9 	bclr CORCON,#2
 112 00007c  00 00 06 	return 
 113              	.set ___PA___,0
 114              	.LFE1:
 115              	.size _parse_gps_message_clock,.-_parse_gps_message_clock
 116              	.align 2
 117              	.global _parse_gps_message_velocity
 118              	.type _parse_gps_message_velocity,@function
 119              	_parse_gps_message_velocity:
 120              	.LFB2:
  25:lib/can-ids/Devices/GPS_CAN.c **** 
  26:lib/can-ids/Devices/GPS_CAN.c **** void parse_gps_message_velocity (CANdata message, GPS_PARSED_DATA *data){
 121              	.loc 1 26 0
 122              	.set ___PA___,1
 123 00007e  0E 00 FA 	lnk #14
 124              	.LCFI2:
 125              	.loc 1 26 0
 126 000080  00 0F 78 	mov w0,[w14]
 127 000082  11 07 98 	mov w1,[w14+2]
  27:lib/can-ids/Devices/GPS_CAN.c **** 
  28:lib/can-ids/Devices/GPS_CAN.c ****     if(message.dev_id != DEVICE_ID_GPS){
 128              	.loc 1 28 0
 129 000084  1E 00 78 	mov [w14],w0
 130              	.loc 1 26 0
 131 000086  22 07 98 	mov w2,[w14+4]
 132 000088  33 07 98 	mov w3,[w14+6]
 133 00008a  44 07 98 	mov w4,[w14+8]
 134 00008c  55 07 98 	mov w5,[w14+10]
 135 00008e  66 07 98 	mov w6,[w14+12]
MPLAB XC16 ASSEMBLY Listing:   			page 4


 136              	.loc 1 28 0
 137 000090  7F 00 60 	and w0,#31,w0
 138 000092  F6 0F 50 	sub w0,#22,[w15]
 139              	.set ___BP___,0
 140 000094  00 00 3A 	bra nz,.L12
 141              	.L9:
  29:lib/can-ids/Devices/GPS_CAN.c ****         return;
  30:lib/can-ids/Devices/GPS_CAN.c ****     }
  31:lib/can-ids/Devices/GPS_CAN.c ****     else if(message.msg_id == MSG_ID_GPS_VELOCITY){
 142              	.loc 1 31 0
 143 000096  1E 01 78 	mov [w14],w2
 144 000098  01 7E 20 	mov #2016,w1
 145 00009a  00 42 20 	mov #1056,w0
 146 00009c  81 00 61 	and w2,w1,w1
 147 00009e  80 8F 50 	sub w1,w0,[w15]
 148              	.set ___BP___,0
 149 0000a0  00 00 3A 	bra nz,.L13
  32:lib/can-ids/Devices/GPS_CAN.c **** 
  33:lib/can-ids/Devices/GPS_CAN.c ****         data->ground_speed = message.data[1];
 150              	.loc 1 33 0
 151 0000a2  BE 00 90 	mov [w14+6],w1
 152 0000a4  6E 00 90 	mov [w14+12],w0
 153 0000a6  11 00 98 	mov w1,[w0+2]
  34:lib/can-ids/Devices/GPS_CAN.c ****         return;
 154              	.loc 1 34 0
 155 0000a8  00 00 37 	bra .L8
 156              	.L12:
 157 0000aa  00 00 37 	bra .L8
 158              	.L13:
 159              	.L8:
  35:lib/can-ids/Devices/GPS_CAN.c ****     }
  36:lib/can-ids/Devices/GPS_CAN.c ****     else
  37:lib/can-ids/Devices/GPS_CAN.c ****         return;
  38:lib/can-ids/Devices/GPS_CAN.c **** }
 160              	.loc 1 38 0
 161 0000ac  8E 07 78 	mov w14,w15
 162 0000ae  4F 07 78 	mov [--w15],w14
 163 0000b0  00 40 A9 	bclr CORCON,#2
 164 0000b2  00 00 06 	return 
 165              	.set ___PA___,0
 166              	.LFE2:
 167              	.size _parse_gps_message_velocity,.-_parse_gps_message_velocity
 168              	.align 2
 169              	.global _parse_gps_message_coordinates
 170              	.type _parse_gps_message_coordinates,@function
 171              	_parse_gps_message_coordinates:
 172              	.LFB3:
  39:lib/can-ids/Devices/GPS_CAN.c **** 
  40:lib/can-ids/Devices/GPS_CAN.c **** void parse_gps_message_coordinates (CANdata message, GPS_PARSED_DATA *data){
 173              	.loc 1 40 0
 174              	.set ___PA___,1
 175 0000b4  0E 00 FA 	lnk #14
 176              	.LCFI3:
 177              	.loc 1 40 0
 178 0000b6  00 0F 78 	mov w0,[w14]
 179 0000b8  11 07 98 	mov w1,[w14+2]
  41:lib/can-ids/Devices/GPS_CAN.c **** 
MPLAB XC16 ASSEMBLY Listing:   			page 5


  42:lib/can-ids/Devices/GPS_CAN.c ****     if(message.dev_id != DEVICE_ID_GPS){
 180              	.loc 1 42 0
 181 0000ba  1E 00 78 	mov [w14],w0
 182              	.loc 1 40 0
 183 0000bc  22 07 98 	mov w2,[w14+4]
 184 0000be  33 07 98 	mov w3,[w14+6]
 185 0000c0  44 07 98 	mov w4,[w14+8]
 186 0000c2  55 07 98 	mov w5,[w14+10]
 187 0000c4  66 07 98 	mov w6,[w14+12]
 188              	.loc 1 42 0
 189 0000c6  7F 00 60 	and w0,#31,w0
 190 0000c8  F6 0F 50 	sub w0,#22,[w15]
 191              	.set ___BP___,0
 192 0000ca  00 00 3A 	bra nz,.L19
 193              	.L15:
  43:lib/can-ids/Devices/GPS_CAN.c ****         return;
  44:lib/can-ids/Devices/GPS_CAN.c ****     }
  45:lib/can-ids/Devices/GPS_CAN.c ****     else if(message.msg_id == MSG_ID_GPS_LATITUDE){
 194              	.loc 1 45 0
 195 0000cc  1E 01 78 	mov [w14],w2
 196 0000ce  01 7E 20 	mov #2016,w1
 197 0000d0  00 44 20 	mov #1088,w0
 198 0000d2  81 00 61 	and w2,w1,w1
 199 0000d4  80 8F 50 	sub w1,w0,[w15]
 200              	.set ___BP___,0
 201 0000d6  00 00 3A 	bra nz,.L17
  46:lib/can-ids/Devices/GPS_CAN.c **** 
  47:lib/can-ids/Devices/GPS_CAN.c ****         data->lat_dg      = message.data[1];   
 202              	.loc 1 47 0
 203 0000d8  3E 00 90 	mov [w14+6],w0
 204 0000da  6E 02 90 	mov [w14+12],w4
 205 0000dc  80 02 78 	mov w0,w5
  48:lib/can-ids/Devices/GPS_CAN.c ****         data->lat_min     = message.data[2];
 206              	.loc 1 48 0
 207 0000de  4E 00 90 	mov [w14+8],w0
 208 0000e0  6E 01 90 	mov [w14+12],w2
 209 0000e2  80 01 78 	mov w0,w3
  49:lib/can-ids/Devices/GPS_CAN.c ****         data->lat_dec_min = message.data[3];
 210              	.loc 1 49 0
 211 0000e4  DE 00 90 	mov [w14+10],w1
 212 0000e6  6E 00 90 	mov [w14+12],w0
 213              	.loc 1 47 0
 214 0000e8  25 02 98 	mov w5,[w4+4]
 215              	.loc 1 48 0
 216 0000ea  33 01 98 	mov w3,[w2+6]
 217              	.loc 1 49 0
 218 0000ec  41 00 98 	mov w1,[w0+8]
  50:lib/can-ids/Devices/GPS_CAN.c **** 
  51:lib/can-ids/Devices/GPS_CAN.c ****         return;
 219              	.loc 1 51 0
 220 0000ee  00 00 37 	bra .L14
 221              	.L17:
  52:lib/can-ids/Devices/GPS_CAN.c ****     }
  53:lib/can-ids/Devices/GPS_CAN.c ****     else if(message.msg_id == MSG_ID_GPS_LONGITUDE){
 222              	.loc 1 53 0
 223 0000f0  00 00 00 	nop 
 224 0000f2  1E 01 78 	mov [w14],w2
MPLAB XC16 ASSEMBLY Listing:   			page 6


 225 0000f4  01 7E 20 	mov #2016,w1
 226 0000f6  00 46 20 	mov #1120,w0
 227 0000f8  81 00 61 	and w2,w1,w1
 228 0000fa  80 8F 50 	sub w1,w0,[w15]
 229              	.set ___BP___,0
 230 0000fc  00 00 3A 	bra nz,.L20
  54:lib/can-ids/Devices/GPS_CAN.c **** 
  55:lib/can-ids/Devices/GPS_CAN.c ****         data->long_dg      = message.data[1];
 231              	.loc 1 55 0
 232 0000fe  3E 00 90 	mov [w14+6],w0
 233 000100  6E 02 90 	mov [w14+12],w4
 234 000102  80 02 78 	mov w0,w5
  56:lib/can-ids/Devices/GPS_CAN.c ****         data->long_min     = message.data[2];
 235              	.loc 1 56 0
 236 000104  4E 00 90 	mov [w14+8],w0
 237 000106  6E 01 90 	mov [w14+12],w2
 238 000108  80 01 78 	mov w0,w3
  57:lib/can-ids/Devices/GPS_CAN.c ****         data->long_dec_min = message.data[3];
 239              	.loc 1 57 0
 240 00010a  DE 00 90 	mov [w14+10],w1
 241 00010c  6E 00 90 	mov [w14+12],w0
 242              	.loc 1 55 0
 243 00010e  55 02 98 	mov w5,[w4+10]
 244              	.loc 1 56 0
 245 000110  63 01 98 	mov w3,[w2+12]
 246              	.loc 1 57 0
 247 000112  71 00 98 	mov w1,[w0+14]
  58:lib/can-ids/Devices/GPS_CAN.c **** 
  59:lib/can-ids/Devices/GPS_CAN.c ****         return;
 248              	.loc 1 59 0
 249 000114  00 00 37 	bra .L14
 250              	.L19:
 251 000116  00 00 37 	bra .L14
 252              	.L20:
 253              	.L14:
  60:lib/can-ids/Devices/GPS_CAN.c ****     }
  61:lib/can-ids/Devices/GPS_CAN.c ****     else
  62:lib/can-ids/Devices/GPS_CAN.c ****         return;
  63:lib/can-ids/Devices/GPS_CAN.c **** }
 254              	.loc 1 63 0
 255 000118  8E 07 78 	mov w14,w15
 256 00011a  4F 07 78 	mov [--w15],w14
 257 00011c  00 40 A9 	bclr CORCON,#2
 258 00011e  00 00 06 	return 
 259              	.set ___PA___,0
 260              	.LFE3:
 261              	.size _parse_gps_message_coordinates,.-_parse_gps_message_coordinates
 262              	.section .debug_frame,info
 263                 	.Lframe0:
 264 0000 10 00 00 00 	.4byte .LECIE0-.LSCIE0
 265                 	.LSCIE0:
 266 0004 FF FF FF FF 	.4byte 0xffffffff
 267 0008 01          	.byte 0x1
 268 0009 00          	.byte 0
 269 000a 01          	.uleb128 0x1
 270 000b 02          	.sleb128 2
 271 000c 25          	.byte 0x25
MPLAB XC16 ASSEMBLY Listing:   			page 7


 272 000d 12          	.byte 0x12
 273 000e 0F          	.uleb128 0xf
 274 000f 7E          	.sleb128 -2
 275 0010 09          	.byte 0x9
 276 0011 25          	.uleb128 0x25
 277 0012 0F          	.uleb128 0xf
 278 0013 00          	.align 4
 279                 	.LECIE0:
 280                 	.LSFDE0:
 281 0014 18 00 00 00 	.4byte .LEFDE0-.LASFDE0
 282                 	.LASFDE0:
 283 0018 00 00 00 00 	.4byte .Lframe0
 284 001c 00 00 00 00 	.4byte .LFB0
 285 0020 48 00 00 00 	.4byte .LFE0-.LFB0
 286 0024 04          	.byte 0x4
 287 0025 02 00 00 00 	.4byte .LCFI0-.LFB0
 288 0029 13          	.byte 0x13
 289 002a 7D          	.sleb128 -3
 290 002b 0D          	.byte 0xd
 291 002c 0E          	.uleb128 0xe
 292 002d 8E          	.byte 0x8e
 293 002e 02          	.uleb128 0x2
 294 002f 00          	.align 4
 295                 	.LEFDE0:
 296                 	.LSFDE2:
 297 0030 18 00 00 00 	.4byte .LEFDE2-.LASFDE2
 298                 	.LASFDE2:
 299 0034 00 00 00 00 	.4byte .Lframe0
 300 0038 00 00 00 00 	.4byte .LFB1
 301 003c 36 00 00 00 	.4byte .LFE1-.LFB1
 302 0040 04          	.byte 0x4
 303 0041 02 00 00 00 	.4byte .LCFI1-.LFB1
 304 0045 13          	.byte 0x13
 305 0046 7D          	.sleb128 -3
 306 0047 0D          	.byte 0xd
 307 0048 0E          	.uleb128 0xe
 308 0049 8E          	.byte 0x8e
 309 004a 02          	.uleb128 0x2
 310 004b 00          	.align 4
 311                 	.LEFDE2:
 312                 	.LSFDE4:
 313 004c 18 00 00 00 	.4byte .LEFDE4-.LASFDE4
 314                 	.LASFDE4:
 315 0050 00 00 00 00 	.4byte .Lframe0
 316 0054 00 00 00 00 	.4byte .LFB2
 317 0058 36 00 00 00 	.4byte .LFE2-.LFB2
 318 005c 04          	.byte 0x4
 319 005d 02 00 00 00 	.4byte .LCFI2-.LFB2
 320 0061 13          	.byte 0x13
 321 0062 7D          	.sleb128 -3
 322 0063 0D          	.byte 0xd
 323 0064 0E          	.uleb128 0xe
 324 0065 8E          	.byte 0x8e
 325 0066 02          	.uleb128 0x2
 326 0067 00          	.align 4
 327                 	.LEFDE4:
 328                 	.LSFDE6:
MPLAB XC16 ASSEMBLY Listing:   			page 8


 329 0068 18 00 00 00 	.4byte .LEFDE6-.LASFDE6
 330                 	.LASFDE6:
 331 006c 00 00 00 00 	.4byte .Lframe0
 332 0070 00 00 00 00 	.4byte .LFB3
 333 0074 6C 00 00 00 	.4byte .LFE3-.LFB3
 334 0078 04          	.byte 0x4
 335 0079 02 00 00 00 	.4byte .LCFI3-.LFB3
 336 007d 13          	.byte 0x13
 337 007e 7D          	.sleb128 -3
 338 007f 0D          	.byte 0xd
 339 0080 0E          	.uleb128 0xe
 340 0081 8E          	.byte 0x8e
 341 0082 02          	.uleb128 0x2
 342 0083 00          	.align 4
 343                 	.LEFDE6:
 344                 	.section .text,code
 345              	.Letext0:
 346              	.file 2 "/opt/microchip/xc16/v1.36/bin/bin/../../include/lega-c/stdint.h"
 347              	.file 3 "lib/can-ids/Devices/../CAN_IDs.h"
 348              	.file 4 "lib/can-ids/Devices/GPS_CAN.h"
 349              	.section .debug_info,info
 350 0000 CD 03 00 00 	.4byte 0x3cd
 351 0004 02 00       	.2byte 0x2
 352 0006 00 00 00 00 	.4byte .Ldebug_abbrev0
 353 000a 04          	.byte 0x4
 354 000b 01          	.uleb128 0x1
 355 000c 47 4E 55 20 	.asciz "GNU C 4.5.1 (XC16, Microchip v1.36) (A) Build date: Dec  5 2018"
 355      43 20 34 2E 
 355      35 2E 31 20 
 355      28 58 43 31 
 355      36 2C 20 4D 
 355      69 63 72 6F 
 355      63 68 69 70 
 355      20 76 31 2E 
 355      33 36 29 20 
 356 004c 01          	.byte 0x1
 357 004d 6C 69 62 2F 	.asciz "lib/can-ids/Devices/GPS_CAN.c"
 357      63 61 6E 2D 
 357      69 64 73 2F 
 357      44 65 76 69 
 357      63 65 73 2F 
 357      47 50 53 5F 
 357      43 41 4E 2E 
 357      63 00 
 358 006b 2F 68 6F 6D 	.asciz "/home/user/Documents/FST/Programming/project_sub-zero"
 358      65 2F 75 73 
 358      65 72 2F 44 
 358      6F 63 75 6D 
 358      65 6E 74 73 
 358      2F 46 53 54 
 358      2F 50 72 6F 
 358      67 72 61 6D 
 358      6D 69 6E 67 
 359 00a1 00 00 00 00 	.4byte .Ltext0
 360 00a5 00 00 00 00 	.4byte .Letext0
 361 00a9 00 00 00 00 	.4byte .Ldebug_line0
 362 00ad 02          	.uleb128 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 9


 363 00ae 01          	.byte 0x1
 364 00af 06          	.byte 0x6
 365 00b0 73 69 67 6E 	.asciz "signed char"
 365      65 64 20 63 
 365      68 61 72 00 
 366 00bc 03          	.uleb128 0x3
 367 00bd 69 6E 74 31 	.asciz "int16_t"
 367      36 5F 74 00 
 368 00c5 02          	.byte 0x2
 369 00c6 14          	.byte 0x14
 370 00c7 CB 00 00 00 	.4byte 0xcb
 371 00cb 02          	.uleb128 0x2
 372 00cc 02          	.byte 0x2
 373 00cd 05          	.byte 0x5
 374 00ce 69 6E 74 00 	.asciz "int"
 375 00d2 02          	.uleb128 0x2
 376 00d3 04          	.byte 0x4
 377 00d4 05          	.byte 0x5
 378 00d5 6C 6F 6E 67 	.asciz "long int"
 378      20 69 6E 74 
 378      00 
 379 00de 02          	.uleb128 0x2
 380 00df 08          	.byte 0x8
 381 00e0 05          	.byte 0x5
 382 00e1 6C 6F 6E 67 	.asciz "long long int"
 382      20 6C 6F 6E 
 382      67 20 69 6E 
 382      74 00 
 383 00ef 02          	.uleb128 0x2
 384 00f0 01          	.byte 0x1
 385 00f1 08          	.byte 0x8
 386 00f2 75 6E 73 69 	.asciz "unsigned char"
 386      67 6E 65 64 
 386      20 63 68 61 
 386      72 00 
 387 0100 03          	.uleb128 0x3
 388 0101 75 69 6E 74 	.asciz "uint16_t"
 388      31 36 5F 74 
 388      00 
 389 010a 02          	.byte 0x2
 390 010b 31          	.byte 0x31
 391 010c 10 01 00 00 	.4byte 0x110
 392 0110 02          	.uleb128 0x2
 393 0111 02          	.byte 0x2
 394 0112 07          	.byte 0x7
 395 0113 75 6E 73 69 	.asciz "unsigned int"
 395      67 6E 65 64 
 395      20 69 6E 74 
 395      00 
 396 0120 02          	.uleb128 0x2
 397 0121 04          	.byte 0x4
 398 0122 07          	.byte 0x7
 399 0123 6C 6F 6E 67 	.asciz "long unsigned int"
 399      20 75 6E 73 
 399      69 67 6E 65 
 399      64 20 69 6E 
 399      74 00 
MPLAB XC16 ASSEMBLY Listing:   			page 10


 400 0135 02          	.uleb128 0x2
 401 0136 08          	.byte 0x8
 402 0137 07          	.byte 0x7
 403 0138 6C 6F 6E 67 	.asciz "long long unsigned int"
 403      20 6C 6F 6E 
 403      67 20 75 6E 
 403      73 69 67 6E 
 403      65 64 20 69 
 403      6E 74 00 
 404 014f 04          	.uleb128 0x4
 405 0150 02          	.byte 0x2
 406 0151 03          	.byte 0x3
 407 0152 12          	.byte 0x12
 408 0153 80 01 00 00 	.4byte 0x180
 409 0157 05          	.uleb128 0x5
 410 0158 64 65 76 5F 	.asciz "dev_id"
 410      69 64 00 
 411 015f 03          	.byte 0x3
 412 0160 13          	.byte 0x13
 413 0161 00 01 00 00 	.4byte 0x100
 414 0165 02          	.byte 0x2
 415 0166 05          	.byte 0x5
 416 0167 0B          	.byte 0xb
 417 0168 02          	.byte 0x2
 418 0169 23          	.byte 0x23
 419 016a 00          	.uleb128 0x0
 420 016b 05          	.uleb128 0x5
 421 016c 6D 73 67 5F 	.asciz "msg_id"
 421      69 64 00 
 422 0173 03          	.byte 0x3
 423 0174 16          	.byte 0x16
 424 0175 00 01 00 00 	.4byte 0x100
 425 0179 02          	.byte 0x2
 426 017a 06          	.byte 0x6
 427 017b 05          	.byte 0x5
 428 017c 02          	.byte 0x2
 429 017d 23          	.byte 0x23
 430 017e 00          	.uleb128 0x0
 431 017f 00          	.byte 0x0
 432 0180 06          	.uleb128 0x6
 433 0181 02          	.byte 0x2
 434 0182 03          	.byte 0x3
 435 0183 11          	.byte 0x11
 436 0184 99 01 00 00 	.4byte 0x199
 437 0188 07          	.uleb128 0x7
 438 0189 4F 01 00 00 	.4byte 0x14f
 439 018d 08          	.uleb128 0x8
 440 018e 73 69 64 00 	.asciz "sid"
 441 0192 03          	.byte 0x3
 442 0193 18          	.byte 0x18
 443 0194 00 01 00 00 	.4byte 0x100
 444 0198 00          	.byte 0x0
 445 0199 04          	.uleb128 0x4
 446 019a 0C          	.byte 0xc
 447 019b 03          	.byte 0x3
 448 019c 10          	.byte 0x10
 449 019d CA 01 00 00 	.4byte 0x1ca
MPLAB XC16 ASSEMBLY Listing:   			page 11


 450 01a1 09          	.uleb128 0x9
 451 01a2 80 01 00 00 	.4byte 0x180
 452 01a6 02          	.byte 0x2
 453 01a7 23          	.byte 0x23
 454 01a8 00          	.uleb128 0x0
 455 01a9 05          	.uleb128 0x5
 456 01aa 64 6C 63 00 	.asciz "dlc"
 457 01ae 03          	.byte 0x3
 458 01af 1A          	.byte 0x1a
 459 01b0 00 01 00 00 	.4byte 0x100
 460 01b4 02          	.byte 0x2
 461 01b5 04          	.byte 0x4
 462 01b6 0C          	.byte 0xc
 463 01b7 02          	.byte 0x2
 464 01b8 23          	.byte 0x23
 465 01b9 02          	.uleb128 0x2
 466 01ba 0A          	.uleb128 0xa
 467 01bb 64 61 74 61 	.asciz "data"
 467      00 
 468 01c0 03          	.byte 0x3
 469 01c1 1B          	.byte 0x1b
 470 01c2 CA 01 00 00 	.4byte 0x1ca
 471 01c6 02          	.byte 0x2
 472 01c7 23          	.byte 0x23
 473 01c8 04          	.uleb128 0x4
 474 01c9 00          	.byte 0x0
 475 01ca 0B          	.uleb128 0xb
 476 01cb 00 01 00 00 	.4byte 0x100
 477 01cf DA 01 00 00 	.4byte 0x1da
 478 01d3 0C          	.uleb128 0xc
 479 01d4 10 01 00 00 	.4byte 0x110
 480 01d8 03          	.byte 0x3
 481 01d9 00          	.byte 0x0
 482 01da 03          	.uleb128 0x3
 483 01db 43 41 4E 64 	.asciz "CANdata"
 483      61 74 61 00 
 484 01e3 03          	.byte 0x3
 485 01e4 1C          	.byte 0x1c
 486 01e5 99 01 00 00 	.4byte 0x199
 487 01e9 04          	.uleb128 0x4
 488 01ea 10          	.byte 0x10
 489 01eb 04          	.byte 0x4
 490 01ec 0B          	.byte 0xb
 491 01ed 94 02 00 00 	.4byte 0x294
 492 01f1 0A          	.uleb128 0xa
 493 01f2 6D 69 6C 69 	.asciz "miliseconds"
 493      73 65 63 6F 
 493      6E 64 73 00 
 494 01fe 04          	.byte 0x4
 495 01ff 0D          	.byte 0xd
 496 0200 00 01 00 00 	.4byte 0x100
 497 0204 02          	.byte 0x2
 498 0205 23          	.byte 0x23
 499 0206 00          	.uleb128 0x0
 500 0207 0A          	.uleb128 0xa
 501 0208 67 72 6F 75 	.asciz "ground_speed"
 501      6E 64 5F 73 
MPLAB XC16 ASSEMBLY Listing:   			page 12


 501      70 65 65 64 
 501      00 
 502 0215 04          	.byte 0x4
 503 0216 0F          	.byte 0xf
 504 0217 00 01 00 00 	.4byte 0x100
 505 021b 02          	.byte 0x2
 506 021c 23          	.byte 0x23
 507 021d 02          	.uleb128 0x2
 508 021e 0A          	.uleb128 0xa
 509 021f 6C 61 74 5F 	.asciz "lat_dg"
 509      64 67 00 
 510 0226 04          	.byte 0x4
 511 0227 11          	.byte 0x11
 512 0228 BC 00 00 00 	.4byte 0xbc
 513 022c 02          	.byte 0x2
 514 022d 23          	.byte 0x23
 515 022e 04          	.uleb128 0x4
 516 022f 0A          	.uleb128 0xa
 517 0230 6C 61 74 5F 	.asciz "lat_min"
 517      6D 69 6E 00 
 518 0238 04          	.byte 0x4
 519 0239 12          	.byte 0x12
 520 023a BC 00 00 00 	.4byte 0xbc
 521 023e 02          	.byte 0x2
 522 023f 23          	.byte 0x23
 523 0240 06          	.uleb128 0x6
 524 0241 0A          	.uleb128 0xa
 525 0242 6C 61 74 5F 	.asciz "lat_dec_min"
 525      64 65 63 5F 
 525      6D 69 6E 00 
 526 024e 04          	.byte 0x4
 527 024f 13          	.byte 0x13
 528 0250 BC 00 00 00 	.4byte 0xbc
 529 0254 02          	.byte 0x2
 530 0255 23          	.byte 0x23
 531 0256 08          	.uleb128 0x8
 532 0257 0A          	.uleb128 0xa
 533 0258 6C 6F 6E 67 	.asciz "long_dg"
 533      5F 64 67 00 
 534 0260 04          	.byte 0x4
 535 0261 15          	.byte 0x15
 536 0262 BC 00 00 00 	.4byte 0xbc
 537 0266 02          	.byte 0x2
 538 0267 23          	.byte 0x23
 539 0268 0A          	.uleb128 0xa
 540 0269 0A          	.uleb128 0xa
 541 026a 6C 6F 6E 67 	.asciz "long_min"
 541      5F 6D 69 6E 
 541      00 
 542 0273 04          	.byte 0x4
 543 0274 16          	.byte 0x16
 544 0275 BC 00 00 00 	.4byte 0xbc
 545 0279 02          	.byte 0x2
 546 027a 23          	.byte 0x23
 547 027b 0C          	.uleb128 0xc
 548 027c 0A          	.uleb128 0xa
 549 027d 6C 6F 6E 67 	.asciz "long_dec_min"
MPLAB XC16 ASSEMBLY Listing:   			page 13


 549      5F 64 65 63 
 549      5F 6D 69 6E 
 549      00 
 550 028a 04          	.byte 0x4
 551 028b 17          	.byte 0x17
 552 028c BC 00 00 00 	.4byte 0xbc
 553 0290 02          	.byte 0x2
 554 0291 23          	.byte 0x23
 555 0292 0E          	.uleb128 0xe
 556 0293 00          	.byte 0x0
 557 0294 03          	.uleb128 0x3
 558 0295 47 50 53 5F 	.asciz "GPS_PARSED_DATA"
 558      50 41 52 53 
 558      45 44 5F 44 
 558      41 54 41 00 
 559 02a5 04          	.byte 0x4
 560 02a6 19          	.byte 0x19
 561 02a7 E9 01 00 00 	.4byte 0x1e9
 562 02ab 0D          	.uleb128 0xd
 563 02ac 01          	.byte 0x1
 564 02ad 70 61 72 73 	.asciz "parse_can_gps"
 564      65 5F 63 61 
 564      6E 5F 67 70 
 564      73 00 
 565 02bb 01          	.byte 0x1
 566 02bc 03          	.byte 0x3
 567 02bd 01          	.byte 0x1
 568 02be 00 00 00 00 	.4byte .LFB0
 569 02c2 00 00 00 00 	.4byte .LFE0
 570 02c6 01          	.byte 0x1
 571 02c7 5E          	.byte 0x5e
 572 02c8 EA 02 00 00 	.4byte 0x2ea
 573 02cc 0E          	.uleb128 0xe
 574 02cd 00 00 00 00 	.4byte .LASF0
 575 02d1 01          	.byte 0x1
 576 02d2 03          	.byte 0x3
 577 02d3 DA 01 00 00 	.4byte 0x1da
 578 02d7 02          	.byte 0x2
 579 02d8 7E          	.byte 0x7e
 580 02d9 00          	.sleb128 0
 581 02da 0F          	.uleb128 0xf
 582 02db 64 61 74 61 	.asciz "data"
 582      00 
 583 02e0 01          	.byte 0x1
 584 02e1 03          	.byte 0x3
 585 02e2 EA 02 00 00 	.4byte 0x2ea
 586 02e6 02          	.byte 0x2
 587 02e7 7E          	.byte 0x7e
 588 02e8 0C          	.sleb128 12
 589 02e9 00          	.byte 0x0
 590 02ea 10          	.uleb128 0x10
 591 02eb 02          	.byte 0x2
 592 02ec 94 02 00 00 	.4byte 0x294
 593 02f0 0D          	.uleb128 0xd
 594 02f1 01          	.byte 0x1
 595 02f2 70 61 72 73 	.asciz "parse_gps_message_clock"
 595      65 5F 67 70 
MPLAB XC16 ASSEMBLY Listing:   			page 14


 595      73 5F 6D 65 
 595      73 73 61 67 
 595      65 5F 63 6C 
 595      6F 63 6B 00 
 596 030a 01          	.byte 0x1
 597 030b 0C          	.byte 0xc
 598 030c 01          	.byte 0x1
 599 030d 00 00 00 00 	.4byte .LFB1
 600 0311 00 00 00 00 	.4byte .LFE1
 601 0315 01          	.byte 0x1
 602 0316 5E          	.byte 0x5e
 603 0317 39 03 00 00 	.4byte 0x339
 604 031b 0E          	.uleb128 0xe
 605 031c 00 00 00 00 	.4byte .LASF0
 606 0320 01          	.byte 0x1
 607 0321 0C          	.byte 0xc
 608 0322 DA 01 00 00 	.4byte 0x1da
 609 0326 02          	.byte 0x2
 610 0327 7E          	.byte 0x7e
 611 0328 00          	.sleb128 0
 612 0329 0F          	.uleb128 0xf
 613 032a 64 61 74 61 	.asciz "data"
 613      00 
 614 032f 01          	.byte 0x1
 615 0330 0C          	.byte 0xc
 616 0331 EA 02 00 00 	.4byte 0x2ea
 617 0335 02          	.byte 0x2
 618 0336 7E          	.byte 0x7e
 619 0337 0C          	.sleb128 12
 620 0338 00          	.byte 0x0
 621 0339 0D          	.uleb128 0xd
 622 033a 01          	.byte 0x1
 623 033b 70 61 72 73 	.asciz "parse_gps_message_velocity"
 623      65 5F 67 70 
 623      73 5F 6D 65 
 623      73 73 61 67 
 623      65 5F 76 65 
 623      6C 6F 63 69 
 623      74 79 00 
 624 0356 01          	.byte 0x1
 625 0357 1A          	.byte 0x1a
 626 0358 01          	.byte 0x1
 627 0359 00 00 00 00 	.4byte .LFB2
 628 035d 00 00 00 00 	.4byte .LFE2
 629 0361 01          	.byte 0x1
 630 0362 5E          	.byte 0x5e
 631 0363 85 03 00 00 	.4byte 0x385
 632 0367 0E          	.uleb128 0xe
 633 0368 00 00 00 00 	.4byte .LASF0
 634 036c 01          	.byte 0x1
 635 036d 1A          	.byte 0x1a
 636 036e DA 01 00 00 	.4byte 0x1da
 637 0372 02          	.byte 0x2
 638 0373 7E          	.byte 0x7e
 639 0374 00          	.sleb128 0
 640 0375 0F          	.uleb128 0xf
 641 0376 64 61 74 61 	.asciz "data"
MPLAB XC16 ASSEMBLY Listing:   			page 15


 641      00 
 642 037b 01          	.byte 0x1
 643 037c 1A          	.byte 0x1a
 644 037d EA 02 00 00 	.4byte 0x2ea
 645 0381 02          	.byte 0x2
 646 0382 7E          	.byte 0x7e
 647 0383 0C          	.sleb128 12
 648 0384 00          	.byte 0x0
 649 0385 11          	.uleb128 0x11
 650 0386 01          	.byte 0x1
 651 0387 70 61 72 73 	.asciz "parse_gps_message_coordinates"
 651      65 5F 67 70 
 651      73 5F 6D 65 
 651      73 73 61 67 
 651      65 5F 63 6F 
 651      6F 72 64 69 
 651      6E 61 74 65 
 651      73 00 
 652 03a5 01          	.byte 0x1
 653 03a6 28          	.byte 0x28
 654 03a7 01          	.byte 0x1
 655 03a8 00 00 00 00 	.4byte .LFB3
 656 03ac 00 00 00 00 	.4byte .LFE3
 657 03b0 01          	.byte 0x1
 658 03b1 5E          	.byte 0x5e
 659 03b2 0E          	.uleb128 0xe
 660 03b3 00 00 00 00 	.4byte .LASF0
 661 03b7 01          	.byte 0x1
 662 03b8 28          	.byte 0x28
 663 03b9 DA 01 00 00 	.4byte 0x1da
 664 03bd 02          	.byte 0x2
 665 03be 7E          	.byte 0x7e
 666 03bf 00          	.sleb128 0
 667 03c0 0F          	.uleb128 0xf
 668 03c1 64 61 74 61 	.asciz "data"
 668      00 
 669 03c6 01          	.byte 0x1
 670 03c7 28          	.byte 0x28
 671 03c8 EA 02 00 00 	.4byte 0x2ea
 672 03cc 02          	.byte 0x2
 673 03cd 7E          	.byte 0x7e
 674 03ce 0C          	.sleb128 12
 675 03cf 00          	.byte 0x0
 676 03d0 00          	.byte 0x0
 677                 	.section .debug_abbrev,info
 678 0000 01          	.uleb128 0x1
 679 0001 11          	.uleb128 0x11
 680 0002 01          	.byte 0x1
 681 0003 25          	.uleb128 0x25
 682 0004 08          	.uleb128 0x8
 683 0005 13          	.uleb128 0x13
 684 0006 0B          	.uleb128 0xb
 685 0007 03          	.uleb128 0x3
 686 0008 08          	.uleb128 0x8
 687 0009 1B          	.uleb128 0x1b
 688 000a 08          	.uleb128 0x8
 689 000b 11          	.uleb128 0x11
MPLAB XC16 ASSEMBLY Listing:   			page 16


 690 000c 01          	.uleb128 0x1
 691 000d 12          	.uleb128 0x12
 692 000e 01          	.uleb128 0x1
 693 000f 10          	.uleb128 0x10
 694 0010 06          	.uleb128 0x6
 695 0011 00          	.byte 0x0
 696 0012 00          	.byte 0x0
 697 0013 02          	.uleb128 0x2
 698 0014 24          	.uleb128 0x24
 699 0015 00          	.byte 0x0
 700 0016 0B          	.uleb128 0xb
 701 0017 0B          	.uleb128 0xb
 702 0018 3E          	.uleb128 0x3e
 703 0019 0B          	.uleb128 0xb
 704 001a 03          	.uleb128 0x3
 705 001b 08          	.uleb128 0x8
 706 001c 00          	.byte 0x0
 707 001d 00          	.byte 0x0
 708 001e 03          	.uleb128 0x3
 709 001f 16          	.uleb128 0x16
 710 0020 00          	.byte 0x0
 711 0021 03          	.uleb128 0x3
 712 0022 08          	.uleb128 0x8
 713 0023 3A          	.uleb128 0x3a
 714 0024 0B          	.uleb128 0xb
 715 0025 3B          	.uleb128 0x3b
 716 0026 0B          	.uleb128 0xb
 717 0027 49          	.uleb128 0x49
 718 0028 13          	.uleb128 0x13
 719 0029 00          	.byte 0x0
 720 002a 00          	.byte 0x0
 721 002b 04          	.uleb128 0x4
 722 002c 13          	.uleb128 0x13
 723 002d 01          	.byte 0x1
 724 002e 0B          	.uleb128 0xb
 725 002f 0B          	.uleb128 0xb
 726 0030 3A          	.uleb128 0x3a
 727 0031 0B          	.uleb128 0xb
 728 0032 3B          	.uleb128 0x3b
 729 0033 0B          	.uleb128 0xb
 730 0034 01          	.uleb128 0x1
 731 0035 13          	.uleb128 0x13
 732 0036 00          	.byte 0x0
 733 0037 00          	.byte 0x0
 734 0038 05          	.uleb128 0x5
 735 0039 0D          	.uleb128 0xd
 736 003a 00          	.byte 0x0
 737 003b 03          	.uleb128 0x3
 738 003c 08          	.uleb128 0x8
 739 003d 3A          	.uleb128 0x3a
 740 003e 0B          	.uleb128 0xb
 741 003f 3B          	.uleb128 0x3b
 742 0040 0B          	.uleb128 0xb
 743 0041 49          	.uleb128 0x49
 744 0042 13          	.uleb128 0x13
 745 0043 0B          	.uleb128 0xb
 746 0044 0B          	.uleb128 0xb
MPLAB XC16 ASSEMBLY Listing:   			page 17


 747 0045 0D          	.uleb128 0xd
 748 0046 0B          	.uleb128 0xb
 749 0047 0C          	.uleb128 0xc
 750 0048 0B          	.uleb128 0xb
 751 0049 38          	.uleb128 0x38
 752 004a 0A          	.uleb128 0xa
 753 004b 00          	.byte 0x0
 754 004c 00          	.byte 0x0
 755 004d 06          	.uleb128 0x6
 756 004e 17          	.uleb128 0x17
 757 004f 01          	.byte 0x1
 758 0050 0B          	.uleb128 0xb
 759 0051 0B          	.uleb128 0xb
 760 0052 3A          	.uleb128 0x3a
 761 0053 0B          	.uleb128 0xb
 762 0054 3B          	.uleb128 0x3b
 763 0055 0B          	.uleb128 0xb
 764 0056 01          	.uleb128 0x1
 765 0057 13          	.uleb128 0x13
 766 0058 00          	.byte 0x0
 767 0059 00          	.byte 0x0
 768 005a 07          	.uleb128 0x7
 769 005b 0D          	.uleb128 0xd
 770 005c 00          	.byte 0x0
 771 005d 49          	.uleb128 0x49
 772 005e 13          	.uleb128 0x13
 773 005f 00          	.byte 0x0
 774 0060 00          	.byte 0x0
 775 0061 08          	.uleb128 0x8
 776 0062 0D          	.uleb128 0xd
 777 0063 00          	.byte 0x0
 778 0064 03          	.uleb128 0x3
 779 0065 08          	.uleb128 0x8
 780 0066 3A          	.uleb128 0x3a
 781 0067 0B          	.uleb128 0xb
 782 0068 3B          	.uleb128 0x3b
 783 0069 0B          	.uleb128 0xb
 784 006a 49          	.uleb128 0x49
 785 006b 13          	.uleb128 0x13
 786 006c 00          	.byte 0x0
 787 006d 00          	.byte 0x0
 788 006e 09          	.uleb128 0x9
 789 006f 0D          	.uleb128 0xd
 790 0070 00          	.byte 0x0
 791 0071 49          	.uleb128 0x49
 792 0072 13          	.uleb128 0x13
 793 0073 38          	.uleb128 0x38
 794 0074 0A          	.uleb128 0xa
 795 0075 00          	.byte 0x0
 796 0076 00          	.byte 0x0
 797 0077 0A          	.uleb128 0xa
 798 0078 0D          	.uleb128 0xd
 799 0079 00          	.byte 0x0
 800 007a 03          	.uleb128 0x3
 801 007b 08          	.uleb128 0x8
 802 007c 3A          	.uleb128 0x3a
 803 007d 0B          	.uleb128 0xb
MPLAB XC16 ASSEMBLY Listing:   			page 18


 804 007e 3B          	.uleb128 0x3b
 805 007f 0B          	.uleb128 0xb
 806 0080 49          	.uleb128 0x49
 807 0081 13          	.uleb128 0x13
 808 0082 38          	.uleb128 0x38
 809 0083 0A          	.uleb128 0xa
 810 0084 00          	.byte 0x0
 811 0085 00          	.byte 0x0
 812 0086 0B          	.uleb128 0xb
 813 0087 01          	.uleb128 0x1
 814 0088 01          	.byte 0x1
 815 0089 49          	.uleb128 0x49
 816 008a 13          	.uleb128 0x13
 817 008b 01          	.uleb128 0x1
 818 008c 13          	.uleb128 0x13
 819 008d 00          	.byte 0x0
 820 008e 00          	.byte 0x0
 821 008f 0C          	.uleb128 0xc
 822 0090 21          	.uleb128 0x21
 823 0091 00          	.byte 0x0
 824 0092 49          	.uleb128 0x49
 825 0093 13          	.uleb128 0x13
 826 0094 2F          	.uleb128 0x2f
 827 0095 0B          	.uleb128 0xb
 828 0096 00          	.byte 0x0
 829 0097 00          	.byte 0x0
 830 0098 0D          	.uleb128 0xd
 831 0099 2E          	.uleb128 0x2e
 832 009a 01          	.byte 0x1
 833 009b 3F          	.uleb128 0x3f
 834 009c 0C          	.uleb128 0xc
 835 009d 03          	.uleb128 0x3
 836 009e 08          	.uleb128 0x8
 837 009f 3A          	.uleb128 0x3a
 838 00a0 0B          	.uleb128 0xb
 839 00a1 3B          	.uleb128 0x3b
 840 00a2 0B          	.uleb128 0xb
 841 00a3 27          	.uleb128 0x27
 842 00a4 0C          	.uleb128 0xc
 843 00a5 11          	.uleb128 0x11
 844 00a6 01          	.uleb128 0x1
 845 00a7 12          	.uleb128 0x12
 846 00a8 01          	.uleb128 0x1
 847 00a9 40          	.uleb128 0x40
 848 00aa 0A          	.uleb128 0xa
 849 00ab 01          	.uleb128 0x1
 850 00ac 13          	.uleb128 0x13
 851 00ad 00          	.byte 0x0
 852 00ae 00          	.byte 0x0
 853 00af 0E          	.uleb128 0xe
 854 00b0 05          	.uleb128 0x5
 855 00b1 00          	.byte 0x0
 856 00b2 03          	.uleb128 0x3
 857 00b3 0E          	.uleb128 0xe
 858 00b4 3A          	.uleb128 0x3a
 859 00b5 0B          	.uleb128 0xb
 860 00b6 3B          	.uleb128 0x3b
MPLAB XC16 ASSEMBLY Listing:   			page 19


 861 00b7 0B          	.uleb128 0xb
 862 00b8 49          	.uleb128 0x49
 863 00b9 13          	.uleb128 0x13
 864 00ba 02          	.uleb128 0x2
 865 00bb 0A          	.uleb128 0xa
 866 00bc 00          	.byte 0x0
 867 00bd 00          	.byte 0x0
 868 00be 0F          	.uleb128 0xf
 869 00bf 05          	.uleb128 0x5
 870 00c0 00          	.byte 0x0
 871 00c1 03          	.uleb128 0x3
 872 00c2 08          	.uleb128 0x8
 873 00c3 3A          	.uleb128 0x3a
 874 00c4 0B          	.uleb128 0xb
 875 00c5 3B          	.uleb128 0x3b
 876 00c6 0B          	.uleb128 0xb
 877 00c7 49          	.uleb128 0x49
 878 00c8 13          	.uleb128 0x13
 879 00c9 02          	.uleb128 0x2
 880 00ca 0A          	.uleb128 0xa
 881 00cb 00          	.byte 0x0
 882 00cc 00          	.byte 0x0
 883 00cd 10          	.uleb128 0x10
 884 00ce 0F          	.uleb128 0xf
 885 00cf 00          	.byte 0x0
 886 00d0 0B          	.uleb128 0xb
 887 00d1 0B          	.uleb128 0xb
 888 00d2 49          	.uleb128 0x49
 889 00d3 13          	.uleb128 0x13
 890 00d4 00          	.byte 0x0
 891 00d5 00          	.byte 0x0
 892 00d6 11          	.uleb128 0x11
 893 00d7 2E          	.uleb128 0x2e
 894 00d8 01          	.byte 0x1
 895 00d9 3F          	.uleb128 0x3f
 896 00da 0C          	.uleb128 0xc
 897 00db 03          	.uleb128 0x3
 898 00dc 08          	.uleb128 0x8
 899 00dd 3A          	.uleb128 0x3a
 900 00de 0B          	.uleb128 0xb
 901 00df 3B          	.uleb128 0x3b
 902 00e0 0B          	.uleb128 0xb
 903 00e1 27          	.uleb128 0x27
 904 00e2 0C          	.uleb128 0xc
 905 00e3 11          	.uleb128 0x11
 906 00e4 01          	.uleb128 0x1
 907 00e5 12          	.uleb128 0x12
 908 00e6 01          	.uleb128 0x1
 909 00e7 40          	.uleb128 0x40
 910 00e8 0A          	.uleb128 0xa
 911 00e9 00          	.byte 0x0
 912 00ea 00          	.byte 0x0
 913 00eb 00          	.byte 0x0
 914                 	.section .debug_pubnames,info
 915 0000 7D 00 00 00 	.4byte 0x7d
 916 0004 02 00       	.2byte 0x2
 917 0006 00 00 00 00 	.4byte .Ldebug_info0
MPLAB XC16 ASSEMBLY Listing:   			page 20


 918 000a D1 03 00 00 	.4byte 0x3d1
 919 000e AB 02 00 00 	.4byte 0x2ab
 920 0012 70 61 72 73 	.asciz "parse_can_gps"
 920      65 5F 63 61 
 920      6E 5F 67 70 
 920      73 00 
 921 0020 F0 02 00 00 	.4byte 0x2f0
 922 0024 70 61 72 73 	.asciz "parse_gps_message_clock"
 922      65 5F 67 70 
 922      73 5F 6D 65 
 922      73 73 61 67 
 922      65 5F 63 6C 
 922      6F 63 6B 00 
 923 003c 39 03 00 00 	.4byte 0x339
 924 0040 70 61 72 73 	.asciz "parse_gps_message_velocity"
 924      65 5F 67 70 
 924      73 5F 6D 65 
 924      73 73 61 67 
 924      65 5F 76 65 
 924      6C 6F 63 69 
 924      74 79 00 
 925 005b 85 03 00 00 	.4byte 0x385
 926 005f 70 61 72 73 	.asciz "parse_gps_message_coordinates"
 926      65 5F 67 70 
 926      73 5F 6D 65 
 926      73 73 61 67 
 926      65 5F 63 6F 
 926      6F 72 64 69 
 926      6E 61 74 65 
 926      73 00 
 927 007d 00 00 00 00 	.4byte 0x0
 928                 	.section .debug_pubtypes,info
 929 0000 47 00 00 00 	.4byte 0x47
 930 0004 02 00       	.2byte 0x2
 931 0006 00 00 00 00 	.4byte .Ldebug_info0
 932 000a D1 03 00 00 	.4byte 0x3d1
 933 000e BC 00 00 00 	.4byte 0xbc
 934 0012 69 6E 74 31 	.asciz "int16_t"
 934      36 5F 74 00 
 935 001a 00 01 00 00 	.4byte 0x100
 936 001e 75 69 6E 74 	.asciz "uint16_t"
 936      31 36 5F 74 
 936      00 
 937 0027 DA 01 00 00 	.4byte 0x1da
 938 002b 43 41 4E 64 	.asciz "CANdata"
 938      61 74 61 00 
 939 0033 94 02 00 00 	.4byte 0x294
 940 0037 47 50 53 5F 	.asciz "GPS_PARSED_DATA"
 940      50 41 52 53 
 940      45 44 5F 44 
 940      41 54 41 00 
 941 0047 00 00 00 00 	.4byte 0x0
 942                 	.section .debug_aranges,info
 943 0000 14 00 00 00 	.4byte 0x14
 944 0004 02 00       	.2byte 0x2
 945 0006 00 00 00 00 	.4byte .Ldebug_info0
 946 000a 04          	.byte 0x4
MPLAB XC16 ASSEMBLY Listing:   			page 21


 947 000b 00          	.byte 0x0
 948 000c 00 00       	.2byte 0x0
 949 000e 00 00       	.2byte 0x0
 950 0010 00 00 00 00 	.4byte 0x0
 951 0014 00 00 00 00 	.4byte 0x0
 952                 	.section .debug_str,info
 953                 	.LASF0:
 954 0000 6D 65 73 73 	.asciz "message"
 954      61 67 65 00 
 955                 	.section .text,code
 956              	
 957              	
 958              	
 959              	.section __c30_info,info,bss
 960                 	__psv_trap_errata:
 961                 	
 962                 	.section __c30_signature,info,data
 963 0000 01 00       	.word 0x0001
 964 0002 00 00       	.word 0x0000
 965 0004 00 00       	.word 0x0000
 966                 	
 967                 	
 968                 	
 969                 	.set ___PA___,0
 970                 	.end
MPLAB XC16 ASSEMBLY Listing:   			page 22


DEFINED SYMBOLS
                            *ABS*:00000000 /home/user/Documents/FST/Programming/project_sub-zero/lib/can-ids/Devices/GPS_CAN.c
                            *ABS*:00000001 __33EP256MU806
                            *ABS*:00000001 __dsPIC33E
                            *ABS*:00000001 __HAS_DSP
                            *ABS*:00000001 __HAS_DMAV2
                            *ABS*:00000001 __HAS_CODEGUARD
                            *ABS*:00000001 __HAS_PMP_ENHANCED
                            *ABS*:00000001 __HAS_EDS
                            *ABS*:00000001 __HAS_EP
                            *ABS*:00000011 __TARGET_DIVIDE_CYCLES
                            *ABS*:00000001 __C30ELF
    {standard input}:14     .text:00000000 _parse_can_gps
    {standard input}:18     *ABS*:00000000 ___PA___
    {standard input}:67     .text:00000048 _parse_gps_message_clock
    {standard input}:119    .text:0000007e _parse_gps_message_velocity
    {standard input}:171    .text:000000b4 _parse_gps_message_coordinates
    {standard input}:87     *ABS*:00000000 ___BP___
    {standard input}:960    __c30_info:00000000 __psv_trap_errata
    {standard input}:19     .text:00000000 .L0
                            .text:00000074 .L6
                            .text:00000076 .L7
                            .text:00000076 .L2
                            .text:000000aa .L12
                            .text:000000ac .L13
                            .text:000000ac .L8
                            .text:00000116 .L19
                            .text:000000f0 .L17
                            .text:00000118 .L14
                            .text:00000118 .L20
                    .debug_abbrev:00000000 .Ldebug_abbrev0
                            .text:00000000 .Ltext0
                            .text:00000120 .Letext0
                      .debug_line:00000000 .Ldebug_line0
                            .text:00000000 .LFB0
                            .text:00000048 .LFE0
                       .debug_str:00000000 .LASF0
                            .text:00000048 .LFB1
                            .text:0000007e .LFE1
                            .text:0000007e .LFB2
                            .text:000000b4 .LFE2
                            .text:000000b4 .LFB3
                            .text:00000120 .LFE3
                     .debug_frame:00000000 .Lframe0
                      .debug_info:00000000 .Ldebug_info0

UNDEFINED SYMBOLS
CORCON

EQUATE SYMBOLS
/home/user/Documents/FST/Programming/project_sub-zero/lib/can-ids/Devices/GPS_CAN.c = 0x0
                  __33EP256MU806 = 0x1
                      __dsPIC33E = 0x1
                       __HAS_DSP = 0x1
                     __HAS_DMAV2 = 0x1
                 __HAS_CODEGUARD = 0x1
              __HAS_PMP_ENHANCED = 0x1
                       __HAS_EDS = 0x1
                        __HAS_EP = 0x1
          __TARGET_DIVIDE_CYCLES = 0x11
                        __C30ELF = 0x1
                        ___PA___ = 0x0
                        ___BP___ = 0x0
