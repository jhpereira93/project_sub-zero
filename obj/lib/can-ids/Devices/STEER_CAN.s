MPLAB XC16 ASSEMBLY Listing:   			page 1


   1              	.file "/home/user/Documents/FST/Programming/project_sub-zero/lib/can-ids/Devices/STEER_CAN.c"
   2              	.section .debug_abbrev,info
   3                 	.Ldebug_abbrev0:
   4                 	.section .debug_info,info
   5                 	.Ldebug_info0:
   6                 	.section .debug_line,info
   7                 	.Ldebug_line0:
   8 0000 BD 00 00 00 	.section .text,code
   8      02 00 9E 00 
   8      00 00 01 01 
   8      FB 0E 0A 00 
   8      01 01 01 01 
   8      00 00 00 01 
   8      6C 69 62 2F 
   8      63 61 6E 2D 
   8      69 64 73 2F 
   9              	.Ltext0:
  10              	.section .text,code
  11              	.align 2
  12              	.global _parse_can_steer
  13              	.type _parse_can_steer,@function
  14              	_parse_can_steer:
  15              	.LFB0:
  16              	.file 1 "lib/can-ids/Devices/STEER_CAN.c"
   1:lib/can-ids/Devices/STEER_CAN.c **** #include "STEER_CAN.h"
   2:lib/can-ids/Devices/STEER_CAN.c **** 
   3:lib/can-ids/Devices/STEER_CAN.c **** void parse_can_steer(CANdata message, STEER_MSG_SIG *steer){
  17              	.loc 1 3 0
  18              	.set ___PA___,1
  19 000000  0E 00 FA 	lnk #14
  20              	.LCFI0:
  21              	.loc 1 3 0
  22 000002  00 0F 78 	mov w0,[w14]
  23 000004  11 07 98 	mov w1,[w14+2]
   4:lib/can-ids/Devices/STEER_CAN.c **** 
   5:lib/can-ids/Devices/STEER_CAN.c **** 	if (message.dev_id != DEVICE_ID_STEER) {
  24              	.loc 1 5 0
  25 000006  1E 00 78 	mov [w14],w0
  26              	.loc 1 3 0
  27 000008  22 07 98 	mov w2,[w14+4]
  28 00000a  33 07 98 	mov w3,[w14+6]
  29 00000c  44 07 98 	mov w4,[w14+8]
  30 00000e  55 07 98 	mov w5,[w14+10]
  31 000010  66 07 98 	mov w6,[w14+12]
  32              	.loc 1 5 0
  33 000012  7F 00 60 	and w0,#31,w0
  34 000014  F9 0F 50 	sub w0,#25,[w15]
  35              	.set ___BP___,0
  36 000016  00 00 3A 	bra nz,.L4
  37              	.L2:
   6:lib/can-ids/Devices/STEER_CAN.c **** 		/*FIXME: send info for error logging*/
   7:lib/can-ids/Devices/STEER_CAN.c ****         return;
   8:lib/can-ids/Devices/STEER_CAN.c **** 	}
   9:lib/can-ids/Devices/STEER_CAN.c **** 	else {
  10:lib/can-ids/Devices/STEER_CAN.c **** 		steer->time_stamp = message.data[0];
  38              	.loc 1 10 0
  39 000018  2E 01 90 	mov [w14+4],w2
MPLAB XC16 ASSEMBLY Listing:   			page 2


  40 00001a  EE 00 90 	mov [w14+12],w1
  11:lib/can-ids/Devices/STEER_CAN.c **** 		steer->value = (int16_t) message.data[1];
  41              	.loc 1 11 0
  42 00001c  3E 00 90 	mov [w14+6],w0
  43              	.loc 1 10 0
  44 00001e  82 08 78 	mov w2,[w1]
  45              	.loc 1 11 0
  46 000020  80 00 78 	mov w0,w1
  47 000022  6E 00 90 	mov [w14+12],w0
  48 000024  11 00 98 	mov w1,[w0+2]
  12:lib/can-ids/Devices/STEER_CAN.c **** 
  13:lib/can-ids/Devices/STEER_CAN.c **** 		if(message.dlc == 6){
  49              	.loc 1 13 0
  50 000026  1E 00 90 	mov [w14+2],w0
  51 000028  6F 00 60 	and w0,#15,w0
  52 00002a  E6 0F 50 	sub w0,#6,[w15]
  53              	.set ___BP___,0
  54 00002c  00 00 3A 	bra nz,.L1
  14:lib/can-ids/Devices/STEER_CAN.c **** 			steer->clear_interface = (bool) message.data[2];
  55              	.loc 1 14 0
  56 00002e  CE 00 90 	mov [w14+8],w1
  57 000030  6E 00 90 	mov [w14+12],w0
  58 000032  01 F0 A7 	btsc w1,#15
  59 000034  81 00 EA 	neg w1,w1
  60 000036  81 00 EA 	neg w1,w1
  61 000038  CF 08 DE 	lsr w1,#15,w1
  62 00003a  81 40 78 	mov.b w1,w1
  63 00003c  41 40 98 	mov.b w1,[w0+4]
  64 00003e  00 00 37 	bra .L1
  65              	.L4:
  66              	.L1:
  15:lib/can-ids/Devices/STEER_CAN.c **** 		}
  16:lib/can-ids/Devices/STEER_CAN.c **** 
  17:lib/can-ids/Devices/STEER_CAN.c **** 	}
  18:lib/can-ids/Devices/STEER_CAN.c **** }
  67              	.loc 1 18 0
  68 000040  8E 07 78 	mov w14,w15
  69 000042  4F 07 78 	mov [--w15],w14
  70 000044  00 40 A9 	bclr CORCON,#2
  71 000046  00 00 06 	return 
  72              	.set ___PA___,0
  73              	.LFE0:
  74              	.size _parse_can_steer,.-_parse_can_steer
  75              	.section .debug_frame,info
  76                 	.Lframe0:
  77 0000 10 00 00 00 	.4byte .LECIE0-.LSCIE0
  78                 	.LSCIE0:
  79 0004 FF FF FF FF 	.4byte 0xffffffff
  80 0008 01          	.byte 0x1
  81 0009 00          	.byte 0
  82 000a 01          	.uleb128 0x1
  83 000b 02          	.sleb128 2
  84 000c 25          	.byte 0x25
  85 000d 12          	.byte 0x12
  86 000e 0F          	.uleb128 0xf
  87 000f 7E          	.sleb128 -2
  88 0010 09          	.byte 0x9
MPLAB XC16 ASSEMBLY Listing:   			page 3


  89 0011 25          	.uleb128 0x25
  90 0012 0F          	.uleb128 0xf
  91 0013 00          	.align 4
  92                 	.LECIE0:
  93                 	.LSFDE0:
  94 0014 18 00 00 00 	.4byte .LEFDE0-.LASFDE0
  95                 	.LASFDE0:
  96 0018 00 00 00 00 	.4byte .Lframe0
  97 001c 00 00 00 00 	.4byte .LFB0
  98 0020 48 00 00 00 	.4byte .LFE0-.LFB0
  99 0024 04          	.byte 0x4
 100 0025 02 00 00 00 	.4byte .LCFI0-.LFB0
 101 0029 13          	.byte 0x13
 102 002a 7D          	.sleb128 -3
 103 002b 0D          	.byte 0xd
 104 002c 0E          	.uleb128 0xe
 105 002d 8E          	.byte 0x8e
 106 002e 02          	.uleb128 0x2
 107 002f 00          	.align 4
 108                 	.LEFDE0:
 109                 	.section .text,code
 110              	.Letext0:
 111              	.file 2 "/opt/microchip/xc16/v1.36/bin/bin/../../include/lega-c/stdint.h"
 112              	.file 3 "lib/can-ids/CAN_IDs.h"
 113              	.file 4 "lib/can-ids/Devices/STEER_CAN.h"
 114              	.section .debug_info,info
 115 0000 9A 02 00 00 	.4byte 0x29a
 116 0004 02 00       	.2byte 0x2
 117 0006 00 00 00 00 	.4byte .Ldebug_abbrev0
 118 000a 04          	.byte 0x4
 119 000b 01          	.uleb128 0x1
 120 000c 47 4E 55 20 	.asciz "GNU C 4.5.1 (XC16, Microchip v1.36) (A) Build date: Dec  5 2018"
 120      43 20 34 2E 
 120      35 2E 31 20 
 120      28 58 43 31 
 120      36 2C 20 4D 
 120      69 63 72 6F 
 120      63 68 69 70 
 120      20 76 31 2E 
 120      33 36 29 20 
 121 004c 01          	.byte 0x1
 122 004d 6C 69 62 2F 	.asciz "lib/can-ids/Devices/STEER_CAN.c"
 122      63 61 6E 2D 
 122      69 64 73 2F 
 122      44 65 76 69 
 122      63 65 73 2F 
 122      53 54 45 45 
 122      52 5F 43 41 
 122      4E 2E 63 00 
 123 006d 2F 68 6F 6D 	.asciz "/home/user/Documents/FST/Programming/project_sub-zero"
 123      65 2F 75 73 
 123      65 72 2F 44 
 123      6F 63 75 6D 
 123      65 6E 74 73 
 123      2F 46 53 54 
 123      2F 50 72 6F 
 123      67 72 61 6D 
MPLAB XC16 ASSEMBLY Listing:   			page 4


 123      6D 69 6E 67 
 124 00a3 00 00 00 00 	.4byte .Ltext0
 125 00a7 00 00 00 00 	.4byte .Letext0
 126 00ab 00 00 00 00 	.4byte .Ldebug_line0
 127 00af 02          	.uleb128 0x2
 128 00b0 01          	.byte 0x1
 129 00b1 06          	.byte 0x6
 130 00b2 73 69 67 6E 	.asciz "signed char"
 130      65 64 20 63 
 130      68 61 72 00 
 131 00be 03          	.uleb128 0x3
 132 00bf 69 6E 74 31 	.asciz "int16_t"
 132      36 5F 74 00 
 133 00c7 02          	.byte 0x2
 134 00c8 14          	.byte 0x14
 135 00c9 CD 00 00 00 	.4byte 0xcd
 136 00cd 02          	.uleb128 0x2
 137 00ce 02          	.byte 0x2
 138 00cf 05          	.byte 0x5
 139 00d0 69 6E 74 00 	.asciz "int"
 140 00d4 02          	.uleb128 0x2
 141 00d5 04          	.byte 0x4
 142 00d6 05          	.byte 0x5
 143 00d7 6C 6F 6E 67 	.asciz "long int"
 143      20 69 6E 74 
 143      00 
 144 00e0 02          	.uleb128 0x2
 145 00e1 08          	.byte 0x8
 146 00e2 05          	.byte 0x5
 147 00e3 6C 6F 6E 67 	.asciz "long long int"
 147      20 6C 6F 6E 
 147      67 20 69 6E 
 147      74 00 
 148 00f1 02          	.uleb128 0x2
 149 00f2 01          	.byte 0x1
 150 00f3 08          	.byte 0x8
 151 00f4 75 6E 73 69 	.asciz "unsigned char"
 151      67 6E 65 64 
 151      20 63 68 61 
 151      72 00 
 152 0102 03          	.uleb128 0x3
 153 0103 75 69 6E 74 	.asciz "uint16_t"
 153      31 36 5F 74 
 153      00 
 154 010c 02          	.byte 0x2
 155 010d 31          	.byte 0x31
 156 010e 12 01 00 00 	.4byte 0x112
 157 0112 02          	.uleb128 0x2
 158 0113 02          	.byte 0x2
 159 0114 07          	.byte 0x7
 160 0115 75 6E 73 69 	.asciz "unsigned int"
 160      67 6E 65 64 
 160      20 69 6E 74 
 160      00 
 161 0122 02          	.uleb128 0x2
 162 0123 04          	.byte 0x4
 163 0124 07          	.byte 0x7
MPLAB XC16 ASSEMBLY Listing:   			page 5


 164 0125 6C 6F 6E 67 	.asciz "long unsigned int"
 164      20 75 6E 73 
 164      69 67 6E 65 
 164      64 20 69 6E 
 164      74 00 
 165 0137 02          	.uleb128 0x2
 166 0138 08          	.byte 0x8
 167 0139 07          	.byte 0x7
 168 013a 6C 6F 6E 67 	.asciz "long long unsigned int"
 168      20 6C 6F 6E 
 168      67 20 75 6E 
 168      73 69 67 6E 
 168      65 64 20 69 
 168      6E 74 00 
 169 0151 04          	.uleb128 0x4
 170 0152 02          	.byte 0x2
 171 0153 03          	.byte 0x3
 172 0154 12          	.byte 0x12
 173 0155 82 01 00 00 	.4byte 0x182
 174 0159 05          	.uleb128 0x5
 175 015a 64 65 76 5F 	.asciz "dev_id"
 175      69 64 00 
 176 0161 03          	.byte 0x3
 177 0162 13          	.byte 0x13
 178 0163 02 01 00 00 	.4byte 0x102
 179 0167 02          	.byte 0x2
 180 0168 05          	.byte 0x5
 181 0169 0B          	.byte 0xb
 182 016a 02          	.byte 0x2
 183 016b 23          	.byte 0x23
 184 016c 00          	.uleb128 0x0
 185 016d 05          	.uleb128 0x5
 186 016e 6D 73 67 5F 	.asciz "msg_id"
 186      69 64 00 
 187 0175 03          	.byte 0x3
 188 0176 16          	.byte 0x16
 189 0177 02 01 00 00 	.4byte 0x102
 190 017b 02          	.byte 0x2
 191 017c 06          	.byte 0x6
 192 017d 05          	.byte 0x5
 193 017e 02          	.byte 0x2
 194 017f 23          	.byte 0x23
 195 0180 00          	.uleb128 0x0
 196 0181 00          	.byte 0x0
 197 0182 06          	.uleb128 0x6
 198 0183 02          	.byte 0x2
 199 0184 03          	.byte 0x3
 200 0185 11          	.byte 0x11
 201 0186 9B 01 00 00 	.4byte 0x19b
 202 018a 07          	.uleb128 0x7
 203 018b 51 01 00 00 	.4byte 0x151
 204 018f 08          	.uleb128 0x8
 205 0190 73 69 64 00 	.asciz "sid"
 206 0194 03          	.byte 0x3
 207 0195 18          	.byte 0x18
 208 0196 02 01 00 00 	.4byte 0x102
 209 019a 00          	.byte 0x0
MPLAB XC16 ASSEMBLY Listing:   			page 6


 210 019b 04          	.uleb128 0x4
 211 019c 0C          	.byte 0xc
 212 019d 03          	.byte 0x3
 213 019e 10          	.byte 0x10
 214 019f CC 01 00 00 	.4byte 0x1cc
 215 01a3 09          	.uleb128 0x9
 216 01a4 82 01 00 00 	.4byte 0x182
 217 01a8 02          	.byte 0x2
 218 01a9 23          	.byte 0x23
 219 01aa 00          	.uleb128 0x0
 220 01ab 05          	.uleb128 0x5
 221 01ac 64 6C 63 00 	.asciz "dlc"
 222 01b0 03          	.byte 0x3
 223 01b1 1A          	.byte 0x1a
 224 01b2 02 01 00 00 	.4byte 0x102
 225 01b6 02          	.byte 0x2
 226 01b7 04          	.byte 0x4
 227 01b8 0C          	.byte 0xc
 228 01b9 02          	.byte 0x2
 229 01ba 23          	.byte 0x23
 230 01bb 02          	.uleb128 0x2
 231 01bc 0A          	.uleb128 0xa
 232 01bd 64 61 74 61 	.asciz "data"
 232      00 
 233 01c2 03          	.byte 0x3
 234 01c3 1B          	.byte 0x1b
 235 01c4 CC 01 00 00 	.4byte 0x1cc
 236 01c8 02          	.byte 0x2
 237 01c9 23          	.byte 0x23
 238 01ca 04          	.uleb128 0x4
 239 01cb 00          	.byte 0x0
 240 01cc 0B          	.uleb128 0xb
 241 01cd 02 01 00 00 	.4byte 0x102
 242 01d1 DC 01 00 00 	.4byte 0x1dc
 243 01d5 0C          	.uleb128 0xc
 244 01d6 12 01 00 00 	.4byte 0x112
 245 01da 03          	.byte 0x3
 246 01db 00          	.byte 0x0
 247 01dc 03          	.uleb128 0x3
 248 01dd 43 41 4E 64 	.asciz "CANdata"
 248      61 74 61 00 
 249 01e5 03          	.byte 0x3
 250 01e6 1C          	.byte 0x1c
 251 01e7 9B 01 00 00 	.4byte 0x19b
 252 01eb 04          	.uleb128 0x4
 253 01ec 06          	.byte 0x6
 254 01ed 04          	.byte 0x4
 255 01ee 12          	.byte 0x12
 256 01ef 33 02 00 00 	.4byte 0x233
 257 01f3 0A          	.uleb128 0xa
 258 01f4 74 69 6D 65 	.asciz "time_stamp"
 258      5F 73 74 61 
 258      6D 70 00 
 259 01ff 04          	.byte 0x4
 260 0200 14          	.byte 0x14
 261 0201 02 01 00 00 	.4byte 0x102
 262 0205 02          	.byte 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 7


 263 0206 23          	.byte 0x23
 264 0207 00          	.uleb128 0x0
 265 0208 0A          	.uleb128 0xa
 266 0209 76 61 6C 75 	.asciz "value"
 266      65 00 
 267 020f 04          	.byte 0x4
 268 0210 15          	.byte 0x15
 269 0211 BE 00 00 00 	.4byte 0xbe
 270 0215 02          	.byte 0x2
 271 0216 23          	.byte 0x23
 272 0217 02          	.uleb128 0x2
 273 0218 0A          	.uleb128 0xa
 274 0219 63 6C 65 61 	.asciz "clear_interface"
 274      72 5F 69 6E 
 274      74 65 72 66 
 274      61 63 65 00 
 275 0229 04          	.byte 0x4
 276 022a 16          	.byte 0x16
 277 022b 33 02 00 00 	.4byte 0x233
 278 022f 02          	.byte 0x2
 279 0230 23          	.byte 0x23
 280 0231 04          	.uleb128 0x4
 281 0232 00          	.byte 0x0
 282 0233 02          	.uleb128 0x2
 283 0234 01          	.byte 0x1
 284 0235 02          	.byte 0x2
 285 0236 5F 42 6F 6F 	.asciz "_Bool"
 285      6C 00 
 286 023c 03          	.uleb128 0x3
 287 023d 53 54 45 45 	.asciz "STEER_MSG_SIG"
 287      52 5F 4D 53 
 287      47 5F 53 49 
 287      47 00 
 288 024b 04          	.byte 0x4
 289 024c 18          	.byte 0x18
 290 024d EB 01 00 00 	.4byte 0x1eb
 291 0251 0D          	.uleb128 0xd
 292 0252 01          	.byte 0x1
 293 0253 70 61 72 73 	.asciz "parse_can_steer"
 293      65 5F 63 61 
 293      6E 5F 73 74 
 293      65 65 72 00 
 294 0263 01          	.byte 0x1
 295 0264 03          	.byte 0x3
 296 0265 01          	.byte 0x1
 297 0266 00 00 00 00 	.4byte .LFB0
 298 026a 00 00 00 00 	.4byte .LFE0
 299 026e 01          	.byte 0x1
 300 026f 5E          	.byte 0x5e
 301 0270 97 02 00 00 	.4byte 0x297
 302 0274 0E          	.uleb128 0xe
 303 0275 6D 65 73 73 	.asciz "message"
 303      61 67 65 00 
 304 027d 01          	.byte 0x1
 305 027e 03          	.byte 0x3
 306 027f DC 01 00 00 	.4byte 0x1dc
 307 0283 02          	.byte 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 8


 308 0284 7E          	.byte 0x7e
 309 0285 00          	.sleb128 0
 310 0286 0E          	.uleb128 0xe
 311 0287 73 74 65 65 	.asciz "steer"
 311      72 00 
 312 028d 01          	.byte 0x1
 313 028e 03          	.byte 0x3
 314 028f 97 02 00 00 	.4byte 0x297
 315 0293 02          	.byte 0x2
 316 0294 7E          	.byte 0x7e
 317 0295 0C          	.sleb128 12
 318 0296 00          	.byte 0x0
 319 0297 0F          	.uleb128 0xf
 320 0298 02          	.byte 0x2
 321 0299 3C 02 00 00 	.4byte 0x23c
 322 029d 00          	.byte 0x0
 323                 	.section .debug_abbrev,info
 324 0000 01          	.uleb128 0x1
 325 0001 11          	.uleb128 0x11
 326 0002 01          	.byte 0x1
 327 0003 25          	.uleb128 0x25
 328 0004 08          	.uleb128 0x8
 329 0005 13          	.uleb128 0x13
 330 0006 0B          	.uleb128 0xb
 331 0007 03          	.uleb128 0x3
 332 0008 08          	.uleb128 0x8
 333 0009 1B          	.uleb128 0x1b
 334 000a 08          	.uleb128 0x8
 335 000b 11          	.uleb128 0x11
 336 000c 01          	.uleb128 0x1
 337 000d 12          	.uleb128 0x12
 338 000e 01          	.uleb128 0x1
 339 000f 10          	.uleb128 0x10
 340 0010 06          	.uleb128 0x6
 341 0011 00          	.byte 0x0
 342 0012 00          	.byte 0x0
 343 0013 02          	.uleb128 0x2
 344 0014 24          	.uleb128 0x24
 345 0015 00          	.byte 0x0
 346 0016 0B          	.uleb128 0xb
 347 0017 0B          	.uleb128 0xb
 348 0018 3E          	.uleb128 0x3e
 349 0019 0B          	.uleb128 0xb
 350 001a 03          	.uleb128 0x3
 351 001b 08          	.uleb128 0x8
 352 001c 00          	.byte 0x0
 353 001d 00          	.byte 0x0
 354 001e 03          	.uleb128 0x3
 355 001f 16          	.uleb128 0x16
 356 0020 00          	.byte 0x0
 357 0021 03          	.uleb128 0x3
 358 0022 08          	.uleb128 0x8
 359 0023 3A          	.uleb128 0x3a
 360 0024 0B          	.uleb128 0xb
 361 0025 3B          	.uleb128 0x3b
 362 0026 0B          	.uleb128 0xb
 363 0027 49          	.uleb128 0x49
MPLAB XC16 ASSEMBLY Listing:   			page 9


 364 0028 13          	.uleb128 0x13
 365 0029 00          	.byte 0x0
 366 002a 00          	.byte 0x0
 367 002b 04          	.uleb128 0x4
 368 002c 13          	.uleb128 0x13
 369 002d 01          	.byte 0x1
 370 002e 0B          	.uleb128 0xb
 371 002f 0B          	.uleb128 0xb
 372 0030 3A          	.uleb128 0x3a
 373 0031 0B          	.uleb128 0xb
 374 0032 3B          	.uleb128 0x3b
 375 0033 0B          	.uleb128 0xb
 376 0034 01          	.uleb128 0x1
 377 0035 13          	.uleb128 0x13
 378 0036 00          	.byte 0x0
 379 0037 00          	.byte 0x0
 380 0038 05          	.uleb128 0x5
 381 0039 0D          	.uleb128 0xd
 382 003a 00          	.byte 0x0
 383 003b 03          	.uleb128 0x3
 384 003c 08          	.uleb128 0x8
 385 003d 3A          	.uleb128 0x3a
 386 003e 0B          	.uleb128 0xb
 387 003f 3B          	.uleb128 0x3b
 388 0040 0B          	.uleb128 0xb
 389 0041 49          	.uleb128 0x49
 390 0042 13          	.uleb128 0x13
 391 0043 0B          	.uleb128 0xb
 392 0044 0B          	.uleb128 0xb
 393 0045 0D          	.uleb128 0xd
 394 0046 0B          	.uleb128 0xb
 395 0047 0C          	.uleb128 0xc
 396 0048 0B          	.uleb128 0xb
 397 0049 38          	.uleb128 0x38
 398 004a 0A          	.uleb128 0xa
 399 004b 00          	.byte 0x0
 400 004c 00          	.byte 0x0
 401 004d 06          	.uleb128 0x6
 402 004e 17          	.uleb128 0x17
 403 004f 01          	.byte 0x1
 404 0050 0B          	.uleb128 0xb
 405 0051 0B          	.uleb128 0xb
 406 0052 3A          	.uleb128 0x3a
 407 0053 0B          	.uleb128 0xb
 408 0054 3B          	.uleb128 0x3b
 409 0055 0B          	.uleb128 0xb
 410 0056 01          	.uleb128 0x1
 411 0057 13          	.uleb128 0x13
 412 0058 00          	.byte 0x0
 413 0059 00          	.byte 0x0
 414 005a 07          	.uleb128 0x7
 415 005b 0D          	.uleb128 0xd
 416 005c 00          	.byte 0x0
 417 005d 49          	.uleb128 0x49
 418 005e 13          	.uleb128 0x13
 419 005f 00          	.byte 0x0
 420 0060 00          	.byte 0x0
MPLAB XC16 ASSEMBLY Listing:   			page 10


 421 0061 08          	.uleb128 0x8
 422 0062 0D          	.uleb128 0xd
 423 0063 00          	.byte 0x0
 424 0064 03          	.uleb128 0x3
 425 0065 08          	.uleb128 0x8
 426 0066 3A          	.uleb128 0x3a
 427 0067 0B          	.uleb128 0xb
 428 0068 3B          	.uleb128 0x3b
 429 0069 0B          	.uleb128 0xb
 430 006a 49          	.uleb128 0x49
 431 006b 13          	.uleb128 0x13
 432 006c 00          	.byte 0x0
 433 006d 00          	.byte 0x0
 434 006e 09          	.uleb128 0x9
 435 006f 0D          	.uleb128 0xd
 436 0070 00          	.byte 0x0
 437 0071 49          	.uleb128 0x49
 438 0072 13          	.uleb128 0x13
 439 0073 38          	.uleb128 0x38
 440 0074 0A          	.uleb128 0xa
 441 0075 00          	.byte 0x0
 442 0076 00          	.byte 0x0
 443 0077 0A          	.uleb128 0xa
 444 0078 0D          	.uleb128 0xd
 445 0079 00          	.byte 0x0
 446 007a 03          	.uleb128 0x3
 447 007b 08          	.uleb128 0x8
 448 007c 3A          	.uleb128 0x3a
 449 007d 0B          	.uleb128 0xb
 450 007e 3B          	.uleb128 0x3b
 451 007f 0B          	.uleb128 0xb
 452 0080 49          	.uleb128 0x49
 453 0081 13          	.uleb128 0x13
 454 0082 38          	.uleb128 0x38
 455 0083 0A          	.uleb128 0xa
 456 0084 00          	.byte 0x0
 457 0085 00          	.byte 0x0
 458 0086 0B          	.uleb128 0xb
 459 0087 01          	.uleb128 0x1
 460 0088 01          	.byte 0x1
 461 0089 49          	.uleb128 0x49
 462 008a 13          	.uleb128 0x13
 463 008b 01          	.uleb128 0x1
 464 008c 13          	.uleb128 0x13
 465 008d 00          	.byte 0x0
 466 008e 00          	.byte 0x0
 467 008f 0C          	.uleb128 0xc
 468 0090 21          	.uleb128 0x21
 469 0091 00          	.byte 0x0
 470 0092 49          	.uleb128 0x49
 471 0093 13          	.uleb128 0x13
 472 0094 2F          	.uleb128 0x2f
 473 0095 0B          	.uleb128 0xb
 474 0096 00          	.byte 0x0
 475 0097 00          	.byte 0x0
 476 0098 0D          	.uleb128 0xd
 477 0099 2E          	.uleb128 0x2e
MPLAB XC16 ASSEMBLY Listing:   			page 11


 478 009a 01          	.byte 0x1
 479 009b 3F          	.uleb128 0x3f
 480 009c 0C          	.uleb128 0xc
 481 009d 03          	.uleb128 0x3
 482 009e 08          	.uleb128 0x8
 483 009f 3A          	.uleb128 0x3a
 484 00a0 0B          	.uleb128 0xb
 485 00a1 3B          	.uleb128 0x3b
 486 00a2 0B          	.uleb128 0xb
 487 00a3 27          	.uleb128 0x27
 488 00a4 0C          	.uleb128 0xc
 489 00a5 11          	.uleb128 0x11
 490 00a6 01          	.uleb128 0x1
 491 00a7 12          	.uleb128 0x12
 492 00a8 01          	.uleb128 0x1
 493 00a9 40          	.uleb128 0x40
 494 00aa 0A          	.uleb128 0xa
 495 00ab 01          	.uleb128 0x1
 496 00ac 13          	.uleb128 0x13
 497 00ad 00          	.byte 0x0
 498 00ae 00          	.byte 0x0
 499 00af 0E          	.uleb128 0xe
 500 00b0 05          	.uleb128 0x5
 501 00b1 00          	.byte 0x0
 502 00b2 03          	.uleb128 0x3
 503 00b3 08          	.uleb128 0x8
 504 00b4 3A          	.uleb128 0x3a
 505 00b5 0B          	.uleb128 0xb
 506 00b6 3B          	.uleb128 0x3b
 507 00b7 0B          	.uleb128 0xb
 508 00b8 49          	.uleb128 0x49
 509 00b9 13          	.uleb128 0x13
 510 00ba 02          	.uleb128 0x2
 511 00bb 0A          	.uleb128 0xa
 512 00bc 00          	.byte 0x0
 513 00bd 00          	.byte 0x0
 514 00be 0F          	.uleb128 0xf
 515 00bf 0F          	.uleb128 0xf
 516 00c0 00          	.byte 0x0
 517 00c1 0B          	.uleb128 0xb
 518 00c2 0B          	.uleb128 0xb
 519 00c3 49          	.uleb128 0x49
 520 00c4 13          	.uleb128 0x13
 521 00c5 00          	.byte 0x0
 522 00c6 00          	.byte 0x0
 523 00c7 00          	.byte 0x0
 524                 	.section .debug_pubnames,info
 525 0000 22 00 00 00 	.4byte 0x22
 526 0004 02 00       	.2byte 0x2
 527 0006 00 00 00 00 	.4byte .Ldebug_info0
 528 000a 9E 02 00 00 	.4byte 0x29e
 529 000e 51 02 00 00 	.4byte 0x251
 530 0012 70 61 72 73 	.asciz "parse_can_steer"
 530      65 5F 63 61 
 530      6E 5F 73 74 
 530      65 65 72 00 
 531 0022 00 00 00 00 	.4byte 0x0
MPLAB XC16 ASSEMBLY Listing:   			page 12


 532                 	.section .debug_pubtypes,info
 533 0000 45 00 00 00 	.4byte 0x45
 534 0004 02 00       	.2byte 0x2
 535 0006 00 00 00 00 	.4byte .Ldebug_info0
 536 000a 9E 02 00 00 	.4byte 0x29e
 537 000e BE 00 00 00 	.4byte 0xbe
 538 0012 69 6E 74 31 	.asciz "int16_t"
 538      36 5F 74 00 
 539 001a 02 01 00 00 	.4byte 0x102
 540 001e 75 69 6E 74 	.asciz "uint16_t"
 540      31 36 5F 74 
 540      00 
 541 0027 DC 01 00 00 	.4byte 0x1dc
 542 002b 43 41 4E 64 	.asciz "CANdata"
 542      61 74 61 00 
 543 0033 3C 02 00 00 	.4byte 0x23c
 544 0037 53 54 45 45 	.asciz "STEER_MSG_SIG"
 544      52 5F 4D 53 
 544      47 5F 53 49 
 544      47 00 
 545 0045 00 00 00 00 	.4byte 0x0
 546                 	.section .debug_aranges,info
 547 0000 14 00 00 00 	.4byte 0x14
 548 0004 02 00       	.2byte 0x2
 549 0006 00 00 00 00 	.4byte .Ldebug_info0
 550 000a 04          	.byte 0x4
 551 000b 00          	.byte 0x0
 552 000c 00 00       	.2byte 0x0
 553 000e 00 00       	.2byte 0x0
 554 0010 00 00 00 00 	.4byte 0x0
 555 0014 00 00 00 00 	.4byte 0x0
 556                 	.section .debug_str,info
 557                 	.section .text,code
 558              	
 559              	
 560              	
 561              	.section __c30_info,info,bss
 562                 	__psv_trap_errata:
 563                 	
 564                 	.section __c30_signature,info,data
 565 0000 01 00       	.word 0x0001
 566 0002 00 00       	.word 0x0000
 567 0004 00 00       	.word 0x0000
 568                 	
 569                 	
 570                 	
 571                 	.set ___PA___,0
 572                 	.end
MPLAB XC16 ASSEMBLY Listing:   			page 13


DEFINED SYMBOLS
                            *ABS*:00000000 /home/user/Documents/FST/Programming/project_sub-zero/lib/can-ids/Devices/STEER_CAN.c
                            *ABS*:00000001 __33EP256MU806
                            *ABS*:00000001 __dsPIC33E
                            *ABS*:00000001 __HAS_DSP
                            *ABS*:00000001 __HAS_DMAV2
                            *ABS*:00000001 __HAS_CODEGUARD
                            *ABS*:00000001 __HAS_PMP_ENHANCED
                            *ABS*:00000001 __HAS_EDS
                            *ABS*:00000001 __HAS_EP
                            *ABS*:00000011 __TARGET_DIVIDE_CYCLES
                            *ABS*:00000001 __C30ELF
    {standard input}:14     .text:00000000 _parse_can_steer
    {standard input}:18     *ABS*:00000000 ___PA___
    {standard input}:35     *ABS*:00000000 ___BP___
    {standard input}:562    __c30_info:00000000 __psv_trap_errata
    {standard input}:19     .text:00000000 .L0
                            .text:00000040 .L4
                            .text:00000040 .L1
                    .debug_abbrev:00000000 .Ldebug_abbrev0
                            .text:00000000 .Ltext0
                            .text:00000048 .Letext0
                      .debug_line:00000000 .Ldebug_line0
                            .text:00000000 .LFB0
                            .text:00000048 .LFE0
                     .debug_frame:00000000 .Lframe0
                      .debug_info:00000000 .Ldebug_info0

UNDEFINED SYMBOLS
CORCON

EQUATE SYMBOLS
/home/user/Documents/FST/Programming/project_sub-zero/lib/can-ids/Devices/STEER_CAN.c = 0x0
                  __33EP256MU806 = 0x1
                      __dsPIC33E = 0x1
                       __HAS_DSP = 0x1
                     __HAS_DMAV2 = 0x1
                 __HAS_CODEGUARD = 0x1
              __HAS_PMP_ENHANCED = 0x1
                       __HAS_EDS = 0x1
                        __HAS_EP = 0x1
          __TARGET_DIVIDE_CYCLES = 0x11
                        __C30ELF = 0x1
                        ___PA___ = 0x0
                        ___BP___ = 0x0
