MPLAB XC16 ASSEMBLY Listing:   			page 1


   1              	.file "/home/user/Documents/FST/Programming/project_sub-zero/lib/can-ids/Devices/SUPOT_CAN.c"
   2              	.section .debug_abbrev,info
   3                 	.Ldebug_abbrev0:
   4                 	.section .debug_info,info
   5                 	.Ldebug_info0:
   6                 	.section .debug_line,info
   7                 	.Ldebug_line0:
   8 0000 BF 00 00 00 	.section .text,code
   8      02 00 9E 00 
   8      00 00 01 01 
   8      FB 0E 0A 00 
   8      01 01 01 01 
   8      00 00 00 01 
   8      6C 69 62 2F 
   8      63 61 6E 2D 
   8      69 64 73 2F 
   9              	.Ltext0:
  10              	.section .text,code
  11              	.align 2
  12              	.global _parse_can_supot_sig
  13              	.type _parse_can_supot_sig,@function
  14              	_parse_can_supot_sig:
  15              	.LFB0:
  16              	.file 1 "lib/can-ids/Devices/SUPOT_CAN.c"
   1:lib/can-ids/Devices/SUPOT_CAN.c **** #include "can-ids/Devices/SUPOT_CAN.h"
   2:lib/can-ids/Devices/SUPOT_CAN.c **** 
   3:lib/can-ids/Devices/SUPOT_CAN.c **** void parse_can_supot_sig (CANdata message, SUPOT_MSG_SIG *supots_values){
  17              	.loc 1 3 0
  18              	.set ___PA___,1
  19 000000  0E 00 FA 	lnk #14
  20              	.LCFI0:
  21              	.loc 1 3 0
  22 000002  00 0F 78 	mov w0,[w14]
  23 000004  11 07 98 	mov w1,[w14+2]
   4:lib/can-ids/Devices/SUPOT_CAN.c **** 
   5:lib/can-ids/Devices/SUPOT_CAN.c ****     if(message.dev_id == DEVICE_ID_SUPOT_FRONT){
  24              	.loc 1 5 0
  25 000006  1E 00 78 	mov [w14],w0
  26              	.loc 1 3 0
  27 000008  22 07 98 	mov w2,[w14+4]
  28 00000a  33 07 98 	mov w3,[w14+6]
  29 00000c  44 07 98 	mov w4,[w14+8]
  30 00000e  55 07 98 	mov w5,[w14+10]
  31 000010  66 07 98 	mov w6,[w14+12]
  32              	.loc 1 5 0
  33 000012  7F 00 60 	and w0,#31,w0
  34 000014  F2 0F 50 	sub w0,#18,[w15]
  35              	.set ___BP___,0
  36 000016  00 00 3A 	bra nz,.L2
   6:lib/can-ids/Devices/SUPOT_CAN.c **** 
   7:lib/can-ids/Devices/SUPOT_CAN.c ****         supots_values->front_right = message.data[0];
  37              	.loc 1 7 0
  38 000018  AE 01 90 	mov [w14+4],w3
  39 00001a  6E 01 90 	mov [w14+12],w2
   8:lib/can-ids/Devices/SUPOT_CAN.c ****         supots_values->front_left  = message.data[1];
  40              	.loc 1 8 0
  41 00001c  BE 00 90 	mov [w14+6],w1
MPLAB XC16 ASSEMBLY Listing:   			page 2


  42 00001e  6E 00 90 	mov [w14+12],w0
  43              	.loc 1 7 0
  44 000020  13 01 98 	mov w3,[w2+2]
  45              	.loc 1 8 0
  46 000022  01 08 78 	mov w1,[w0]
  47 000024  00 00 37 	bra .L1
  48              	.L2:
   9:lib/can-ids/Devices/SUPOT_CAN.c **** 
  10:lib/can-ids/Devices/SUPOT_CAN.c ****     }else if(message.dev_id == DEVICE_ID_SUPOT_REAR){
  49              	.loc 1 10 0
  50 000026  1E 00 78 	mov [w14],w0
  51 000028  7F 00 60 	and w0,#31,w0
  52 00002a  F3 0F 50 	sub w0,#19,[w15]
  53              	.set ___BP___,0
  54 00002c  00 00 3A 	bra nz,.L1
  11:lib/can-ids/Devices/SUPOT_CAN.c **** 
  12:lib/can-ids/Devices/SUPOT_CAN.c ****         supots_values->rear_right = message.data[0];
  55              	.loc 1 12 0
  56 00002e  AE 01 90 	mov [w14+4],w3
  57 000030  6E 01 90 	mov [w14+12],w2
  13:lib/can-ids/Devices/SUPOT_CAN.c ****         supots_values->rear_left  = message.data[1];
  58              	.loc 1 13 0
  59 000032  BE 00 90 	mov [w14+6],w1
  60 000034  6E 00 90 	mov [w14+12],w0
  61              	.loc 1 12 0
  62 000036  33 01 98 	mov w3,[w2+6]
  63              	.loc 1 13 0
  64 000038  21 00 98 	mov w1,[w0+4]
  65              	.L1:
  14:lib/can-ids/Devices/SUPOT_CAN.c **** 
  15:lib/can-ids/Devices/SUPOT_CAN.c ****     }
  16:lib/can-ids/Devices/SUPOT_CAN.c ****     return;
  17:lib/can-ids/Devices/SUPOT_CAN.c **** }...
  66              	.loc 1 17 0
  67 00003a  8E 07 78 	mov w14,w15
  68 00003c  4F 07 78 	mov [--w15],w14
  69 00003e  00 40 A9 	bclr CORCON,#2
  70 000040  00 00 06 	return 
  71              	.set ___PA___,0
  72              	.LFE0:
  73              	.size _parse_can_supot_sig,.-_parse_can_supot_sig
  74              	.section .debug_frame,info
  75                 	.Lframe0:
  76 0000 10 00 00 00 	.4byte .LECIE0-.LSCIE0
  77                 	.LSCIE0:
  78 0004 FF FF FF FF 	.4byte 0xffffffff
  79 0008 01          	.byte 0x1
  80 0009 00          	.byte 0
  81 000a 01          	.uleb128 0x1
  82 000b 02          	.sleb128 2
  83 000c 25          	.byte 0x25
  84 000d 12          	.byte 0x12
  85 000e 0F          	.uleb128 0xf
  86 000f 7E          	.sleb128 -2
  87 0010 09          	.byte 0x9
  88 0011 25          	.uleb128 0x25
  89 0012 0F          	.uleb128 0xf
MPLAB XC16 ASSEMBLY Listing:   			page 3


  90 0013 00          	.align 4
  91                 	.LECIE0:
  92                 	.LSFDE0:
  93 0014 18 00 00 00 	.4byte .LEFDE0-.LASFDE0
  94                 	.LASFDE0:
  95 0018 00 00 00 00 	.4byte .Lframe0
  96 001c 00 00 00 00 	.4byte .LFB0
  97 0020 42 00 00 00 	.4byte .LFE0-.LFB0
  98 0024 04          	.byte 0x4
  99 0025 02 00 00 00 	.4byte .LCFI0-.LFB0
 100 0029 13          	.byte 0x13
 101 002a 7D          	.sleb128 -3
 102 002b 0D          	.byte 0xd
 103 002c 0E          	.uleb128 0xe
 104 002d 8E          	.byte 0x8e
 105 002e 02          	.uleb128 0x2
 106 002f 00          	.align 4
 107                 	.LEFDE0:
 108                 	.section .text,code
 109              	.Letext0:
 110              	.file 2 "lib/can-ids/CAN_IDs.h"
 111              	.file 3 "/opt/microchip/xc16/v1.36/bin/bin/../../include/lega-c/stdint.h"
 112              	.file 4 "lib/can-ids/Devices/SUPOT_CAN.h"
 113              	.section .debug_info,info
 114 0000 A3 02 00 00 	.4byte 0x2a3
 115 0004 02 00       	.2byte 0x2
 116 0006 00 00 00 00 	.4byte .Ldebug_abbrev0
 117 000a 04          	.byte 0x4
 118 000b 01          	.uleb128 0x1
 119 000c 47 4E 55 20 	.asciz "GNU C 4.5.1 (XC16, Microchip v1.36) (A) Build date: Dec  5 2018"
 119      43 20 34 2E 
 119      35 2E 31 20 
 119      28 58 43 31 
 119      36 2C 20 4D 
 119      69 63 72 6F 
 119      63 68 69 70 
 119      20 76 31 2E 
 119      33 36 29 20 
 120 004c 01          	.byte 0x1
 121 004d 6C 69 62 2F 	.asciz "lib/can-ids/Devices/SUPOT_CAN.c"
 121      63 61 6E 2D 
 121      69 64 73 2F 
 121      44 65 76 69 
 121      63 65 73 2F 
 121      53 55 50 4F 
 121      54 5F 43 41 
 121      4E 2E 63 00 
 122 006d 2F 68 6F 6D 	.asciz "/home/user/Documents/FST/Programming/project_sub-zero"
 122      65 2F 75 73 
 122      65 72 2F 44 
 122      6F 63 75 6D 
 122      65 6E 74 73 
 122      2F 46 53 54 
 122      2F 50 72 6F 
 122      67 72 61 6D 
 122      6D 69 6E 67 
 123 00a3 00 00 00 00 	.4byte .Ltext0
MPLAB XC16 ASSEMBLY Listing:   			page 4


 124 00a7 00 00 00 00 	.4byte .Letext0
 125 00ab 00 00 00 00 	.4byte .Ldebug_line0
 126 00af 02          	.uleb128 0x2
 127 00b0 01          	.byte 0x1
 128 00b1 06          	.byte 0x6
 129 00b2 73 69 67 6E 	.asciz "signed char"
 129      65 64 20 63 
 129      68 61 72 00 
 130 00be 02          	.uleb128 0x2
 131 00bf 02          	.byte 0x2
 132 00c0 05          	.byte 0x5
 133 00c1 69 6E 74 00 	.asciz "int"
 134 00c5 02          	.uleb128 0x2
 135 00c6 04          	.byte 0x4
 136 00c7 05          	.byte 0x5
 137 00c8 6C 6F 6E 67 	.asciz "long int"
 137      20 69 6E 74 
 137      00 
 138 00d1 02          	.uleb128 0x2
 139 00d2 08          	.byte 0x8
 140 00d3 05          	.byte 0x5
 141 00d4 6C 6F 6E 67 	.asciz "long long int"
 141      20 6C 6F 6E 
 141      67 20 69 6E 
 141      74 00 
 142 00e2 02          	.uleb128 0x2
 143 00e3 01          	.byte 0x1
 144 00e4 08          	.byte 0x8
 145 00e5 75 6E 73 69 	.asciz "unsigned char"
 145      67 6E 65 64 
 145      20 63 68 61 
 145      72 00 
 146 00f3 03          	.uleb128 0x3
 147 00f4 75 69 6E 74 	.asciz "uint16_t"
 147      31 36 5F 74 
 147      00 
 148 00fd 03          	.byte 0x3
 149 00fe 31          	.byte 0x31
 150 00ff 03 01 00 00 	.4byte 0x103
 151 0103 02          	.uleb128 0x2
 152 0104 02          	.byte 0x2
 153 0105 07          	.byte 0x7
 154 0106 75 6E 73 69 	.asciz "unsigned int"
 154      67 6E 65 64 
 154      20 69 6E 74 
 154      00 
 155 0113 02          	.uleb128 0x2
 156 0114 04          	.byte 0x4
 157 0115 07          	.byte 0x7
 158 0116 6C 6F 6E 67 	.asciz "long unsigned int"
 158      20 75 6E 73 
 158      69 67 6E 65 
 158      64 20 69 6E 
 158      74 00 
 159 0128 02          	.uleb128 0x2
 160 0129 08          	.byte 0x8
 161 012a 07          	.byte 0x7
MPLAB XC16 ASSEMBLY Listing:   			page 5


 162 012b 6C 6F 6E 67 	.asciz "long long unsigned int"
 162      20 6C 6F 6E 
 162      67 20 75 6E 
 162      73 69 67 6E 
 162      65 64 20 69 
 162      6E 74 00 
 163 0142 04          	.uleb128 0x4
 164 0143 02          	.byte 0x2
 165 0144 02          	.byte 0x2
 166 0145 12          	.byte 0x12
 167 0146 73 01 00 00 	.4byte 0x173
 168 014a 05          	.uleb128 0x5
 169 014b 64 65 76 5F 	.asciz "dev_id"
 169      69 64 00 
 170 0152 02          	.byte 0x2
 171 0153 13          	.byte 0x13
 172 0154 F3 00 00 00 	.4byte 0xf3
 173 0158 02          	.byte 0x2
 174 0159 05          	.byte 0x5
 175 015a 0B          	.byte 0xb
 176 015b 02          	.byte 0x2
 177 015c 23          	.byte 0x23
 178 015d 00          	.uleb128 0x0
 179 015e 05          	.uleb128 0x5
 180 015f 6D 73 67 5F 	.asciz "msg_id"
 180      69 64 00 
 181 0166 02          	.byte 0x2
 182 0167 16          	.byte 0x16
 183 0168 F3 00 00 00 	.4byte 0xf3
 184 016c 02          	.byte 0x2
 185 016d 06          	.byte 0x6
 186 016e 05          	.byte 0x5
 187 016f 02          	.byte 0x2
 188 0170 23          	.byte 0x23
 189 0171 00          	.uleb128 0x0
 190 0172 00          	.byte 0x0
 191 0173 06          	.uleb128 0x6
 192 0174 02          	.byte 0x2
 193 0175 02          	.byte 0x2
 194 0176 11          	.byte 0x11
 195 0177 8C 01 00 00 	.4byte 0x18c
 196 017b 07          	.uleb128 0x7
 197 017c 42 01 00 00 	.4byte 0x142
 198 0180 08          	.uleb128 0x8
 199 0181 73 69 64 00 	.asciz "sid"
 200 0185 02          	.byte 0x2
 201 0186 18          	.byte 0x18
 202 0187 F3 00 00 00 	.4byte 0xf3
 203 018b 00          	.byte 0x0
 204 018c 04          	.uleb128 0x4
 205 018d 0C          	.byte 0xc
 206 018e 02          	.byte 0x2
 207 018f 10          	.byte 0x10
 208 0190 BD 01 00 00 	.4byte 0x1bd
 209 0194 09          	.uleb128 0x9
 210 0195 73 01 00 00 	.4byte 0x173
 211 0199 02          	.byte 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 6


 212 019a 23          	.byte 0x23
 213 019b 00          	.uleb128 0x0
 214 019c 05          	.uleb128 0x5
 215 019d 64 6C 63 00 	.asciz "dlc"
 216 01a1 02          	.byte 0x2
 217 01a2 1A          	.byte 0x1a
 218 01a3 F3 00 00 00 	.4byte 0xf3
 219 01a7 02          	.byte 0x2
 220 01a8 04          	.byte 0x4
 221 01a9 0C          	.byte 0xc
 222 01aa 02          	.byte 0x2
 223 01ab 23          	.byte 0x23
 224 01ac 02          	.uleb128 0x2
 225 01ad 0A          	.uleb128 0xa
 226 01ae 64 61 74 61 	.asciz "data"
 226      00 
 227 01b3 02          	.byte 0x2
 228 01b4 1B          	.byte 0x1b
 229 01b5 BD 01 00 00 	.4byte 0x1bd
 230 01b9 02          	.byte 0x2
 231 01ba 23          	.byte 0x23
 232 01bb 04          	.uleb128 0x4
 233 01bc 00          	.byte 0x0
 234 01bd 0B          	.uleb128 0xb
 235 01be F3 00 00 00 	.4byte 0xf3
 236 01c2 CD 01 00 00 	.4byte 0x1cd
 237 01c6 0C          	.uleb128 0xc
 238 01c7 03 01 00 00 	.4byte 0x103
 239 01cb 03          	.byte 0x3
 240 01cc 00          	.byte 0x0
 241 01cd 03          	.uleb128 0x3
 242 01ce 43 41 4E 64 	.asciz "CANdata"
 242      61 74 61 00 
 243 01d6 02          	.byte 0x2
 244 01d7 1C          	.byte 0x1c
 245 01d8 8C 01 00 00 	.4byte 0x18c
 246 01dc 04          	.uleb128 0x4
 247 01dd 08          	.byte 0x8
 248 01de 04          	.byte 0x4
 249 01df 0C          	.byte 0xc
 250 01e0 39 02 00 00 	.4byte 0x239
 251 01e4 0A          	.uleb128 0xa
 252 01e5 66 72 6F 6E 	.asciz "front_left"
 252      74 5F 6C 65 
 252      66 74 00 
 253 01f0 04          	.byte 0x4
 254 01f1 0E          	.byte 0xe
 255 01f2 F3 00 00 00 	.4byte 0xf3
 256 01f6 02          	.byte 0x2
 257 01f7 23          	.byte 0x23
 258 01f8 00          	.uleb128 0x0
 259 01f9 0A          	.uleb128 0xa
 260 01fa 66 72 6F 6E 	.asciz "front_right"
 260      74 5F 72 69 
 260      67 68 74 00 
 261 0206 04          	.byte 0x4
 262 0207 0F          	.byte 0xf
MPLAB XC16 ASSEMBLY Listing:   			page 7


 263 0208 F3 00 00 00 	.4byte 0xf3
 264 020c 02          	.byte 0x2
 265 020d 23          	.byte 0x23
 266 020e 02          	.uleb128 0x2
 267 020f 0A          	.uleb128 0xa
 268 0210 72 65 61 72 	.asciz "rear_left"
 268      5F 6C 65 66 
 268      74 00 
 269 021a 04          	.byte 0x4
 270 021b 10          	.byte 0x10
 271 021c F3 00 00 00 	.4byte 0xf3
 272 0220 02          	.byte 0x2
 273 0221 23          	.byte 0x23
 274 0222 04          	.uleb128 0x4
 275 0223 0A          	.uleb128 0xa
 276 0224 72 65 61 72 	.asciz "rear_right"
 276      5F 72 69 67 
 276      68 74 00 
 277 022f 04          	.byte 0x4
 278 0230 11          	.byte 0x11
 279 0231 F3 00 00 00 	.4byte 0xf3
 280 0235 02          	.byte 0x2
 281 0236 23          	.byte 0x23
 282 0237 06          	.uleb128 0x6
 283 0238 00          	.byte 0x0
 284 0239 03          	.uleb128 0x3
 285 023a 53 55 50 4F 	.asciz "SUPOT_MSG_SIG"
 285      54 5F 4D 53 
 285      47 5F 53 49 
 285      47 00 
 286 0248 04          	.byte 0x4
 287 0249 13          	.byte 0x13
 288 024a DC 01 00 00 	.4byte 0x1dc
 289 024e 0D          	.uleb128 0xd
 290 024f 01          	.byte 0x1
 291 0250 70 61 72 73 	.asciz "parse_can_supot_sig"
 291      65 5F 63 61 
 291      6E 5F 73 75 
 291      70 6F 74 5F 
 291      73 69 67 00 
 292 0264 01          	.byte 0x1
 293 0265 03          	.byte 0x3
 294 0266 01          	.byte 0x1
 295 0267 00 00 00 00 	.4byte .LFB0
 296 026b 00 00 00 00 	.4byte .LFE0
 297 026f 01          	.byte 0x1
 298 0270 5E          	.byte 0x5e
 299 0271 A0 02 00 00 	.4byte 0x2a0
 300 0275 0E          	.uleb128 0xe
 301 0276 6D 65 73 73 	.asciz "message"
 301      61 67 65 00 
 302 027e 01          	.byte 0x1
 303 027f 03          	.byte 0x3
 304 0280 CD 01 00 00 	.4byte 0x1cd
 305 0284 02          	.byte 0x2
 306 0285 7E          	.byte 0x7e
 307 0286 00          	.sleb128 0
MPLAB XC16 ASSEMBLY Listing:   			page 8


 308 0287 0E          	.uleb128 0xe
 309 0288 73 75 70 6F 	.asciz "supots_values"
 309      74 73 5F 76 
 309      61 6C 75 65 
 309      73 00 
 310 0296 01          	.byte 0x1
 311 0297 03          	.byte 0x3
 312 0298 A0 02 00 00 	.4byte 0x2a0
 313 029c 02          	.byte 0x2
 314 029d 7E          	.byte 0x7e
 315 029e 0C          	.sleb128 12
 316 029f 00          	.byte 0x0
 317 02a0 0F          	.uleb128 0xf
 318 02a1 02          	.byte 0x2
 319 02a2 39 02 00 00 	.4byte 0x239
 320 02a6 00          	.byte 0x0
 321                 	.section .debug_abbrev,info
 322 0000 01          	.uleb128 0x1
 323 0001 11          	.uleb128 0x11
 324 0002 01          	.byte 0x1
 325 0003 25          	.uleb128 0x25
 326 0004 08          	.uleb128 0x8
 327 0005 13          	.uleb128 0x13
 328 0006 0B          	.uleb128 0xb
 329 0007 03          	.uleb128 0x3
 330 0008 08          	.uleb128 0x8
 331 0009 1B          	.uleb128 0x1b
 332 000a 08          	.uleb128 0x8
 333 000b 11          	.uleb128 0x11
 334 000c 01          	.uleb128 0x1
 335 000d 12          	.uleb128 0x12
 336 000e 01          	.uleb128 0x1
 337 000f 10          	.uleb128 0x10
 338 0010 06          	.uleb128 0x6
 339 0011 00          	.byte 0x0
 340 0012 00          	.byte 0x0
 341 0013 02          	.uleb128 0x2
 342 0014 24          	.uleb128 0x24
 343 0015 00          	.byte 0x0
 344 0016 0B          	.uleb128 0xb
 345 0017 0B          	.uleb128 0xb
 346 0018 3E          	.uleb128 0x3e
 347 0019 0B          	.uleb128 0xb
 348 001a 03          	.uleb128 0x3
 349 001b 08          	.uleb128 0x8
 350 001c 00          	.byte 0x0
 351 001d 00          	.byte 0x0
 352 001e 03          	.uleb128 0x3
 353 001f 16          	.uleb128 0x16
 354 0020 00          	.byte 0x0
 355 0021 03          	.uleb128 0x3
 356 0022 08          	.uleb128 0x8
 357 0023 3A          	.uleb128 0x3a
 358 0024 0B          	.uleb128 0xb
 359 0025 3B          	.uleb128 0x3b
 360 0026 0B          	.uleb128 0xb
 361 0027 49          	.uleb128 0x49
MPLAB XC16 ASSEMBLY Listing:   			page 9


 362 0028 13          	.uleb128 0x13
 363 0029 00          	.byte 0x0
 364 002a 00          	.byte 0x0
 365 002b 04          	.uleb128 0x4
 366 002c 13          	.uleb128 0x13
 367 002d 01          	.byte 0x1
 368 002e 0B          	.uleb128 0xb
 369 002f 0B          	.uleb128 0xb
 370 0030 3A          	.uleb128 0x3a
 371 0031 0B          	.uleb128 0xb
 372 0032 3B          	.uleb128 0x3b
 373 0033 0B          	.uleb128 0xb
 374 0034 01          	.uleb128 0x1
 375 0035 13          	.uleb128 0x13
 376 0036 00          	.byte 0x0
 377 0037 00          	.byte 0x0
 378 0038 05          	.uleb128 0x5
 379 0039 0D          	.uleb128 0xd
 380 003a 00          	.byte 0x0
 381 003b 03          	.uleb128 0x3
 382 003c 08          	.uleb128 0x8
 383 003d 3A          	.uleb128 0x3a
 384 003e 0B          	.uleb128 0xb
 385 003f 3B          	.uleb128 0x3b
 386 0040 0B          	.uleb128 0xb
 387 0041 49          	.uleb128 0x49
 388 0042 13          	.uleb128 0x13
 389 0043 0B          	.uleb128 0xb
 390 0044 0B          	.uleb128 0xb
 391 0045 0D          	.uleb128 0xd
 392 0046 0B          	.uleb128 0xb
 393 0047 0C          	.uleb128 0xc
 394 0048 0B          	.uleb128 0xb
 395 0049 38          	.uleb128 0x38
 396 004a 0A          	.uleb128 0xa
 397 004b 00          	.byte 0x0
 398 004c 00          	.byte 0x0
 399 004d 06          	.uleb128 0x6
 400 004e 17          	.uleb128 0x17
 401 004f 01          	.byte 0x1
 402 0050 0B          	.uleb128 0xb
 403 0051 0B          	.uleb128 0xb
 404 0052 3A          	.uleb128 0x3a
 405 0053 0B          	.uleb128 0xb
 406 0054 3B          	.uleb128 0x3b
 407 0055 0B          	.uleb128 0xb
 408 0056 01          	.uleb128 0x1
 409 0057 13          	.uleb128 0x13
 410 0058 00          	.byte 0x0
 411 0059 00          	.byte 0x0
 412 005a 07          	.uleb128 0x7
 413 005b 0D          	.uleb128 0xd
 414 005c 00          	.byte 0x0
 415 005d 49          	.uleb128 0x49
 416 005e 13          	.uleb128 0x13
 417 005f 00          	.byte 0x0
 418 0060 00          	.byte 0x0
MPLAB XC16 ASSEMBLY Listing:   			page 10


 419 0061 08          	.uleb128 0x8
 420 0062 0D          	.uleb128 0xd
 421 0063 00          	.byte 0x0
 422 0064 03          	.uleb128 0x3
 423 0065 08          	.uleb128 0x8
 424 0066 3A          	.uleb128 0x3a
 425 0067 0B          	.uleb128 0xb
 426 0068 3B          	.uleb128 0x3b
 427 0069 0B          	.uleb128 0xb
 428 006a 49          	.uleb128 0x49
 429 006b 13          	.uleb128 0x13
 430 006c 00          	.byte 0x0
 431 006d 00          	.byte 0x0
 432 006e 09          	.uleb128 0x9
 433 006f 0D          	.uleb128 0xd
 434 0070 00          	.byte 0x0
 435 0071 49          	.uleb128 0x49
 436 0072 13          	.uleb128 0x13
 437 0073 38          	.uleb128 0x38
 438 0074 0A          	.uleb128 0xa
 439 0075 00          	.byte 0x0
 440 0076 00          	.byte 0x0
 441 0077 0A          	.uleb128 0xa
 442 0078 0D          	.uleb128 0xd
 443 0079 00          	.byte 0x0
 444 007a 03          	.uleb128 0x3
 445 007b 08          	.uleb128 0x8
 446 007c 3A          	.uleb128 0x3a
 447 007d 0B          	.uleb128 0xb
 448 007e 3B          	.uleb128 0x3b
 449 007f 0B          	.uleb128 0xb
 450 0080 49          	.uleb128 0x49
 451 0081 13          	.uleb128 0x13
 452 0082 38          	.uleb128 0x38
 453 0083 0A          	.uleb128 0xa
 454 0084 00          	.byte 0x0
 455 0085 00          	.byte 0x0
 456 0086 0B          	.uleb128 0xb
 457 0087 01          	.uleb128 0x1
 458 0088 01          	.byte 0x1
 459 0089 49          	.uleb128 0x49
 460 008a 13          	.uleb128 0x13
 461 008b 01          	.uleb128 0x1
 462 008c 13          	.uleb128 0x13
 463 008d 00          	.byte 0x0
 464 008e 00          	.byte 0x0
 465 008f 0C          	.uleb128 0xc
 466 0090 21          	.uleb128 0x21
 467 0091 00          	.byte 0x0
 468 0092 49          	.uleb128 0x49
 469 0093 13          	.uleb128 0x13
 470 0094 2F          	.uleb128 0x2f
 471 0095 0B          	.uleb128 0xb
 472 0096 00          	.byte 0x0
 473 0097 00          	.byte 0x0
 474 0098 0D          	.uleb128 0xd
 475 0099 2E          	.uleb128 0x2e
MPLAB XC16 ASSEMBLY Listing:   			page 11


 476 009a 01          	.byte 0x1
 477 009b 3F          	.uleb128 0x3f
 478 009c 0C          	.uleb128 0xc
 479 009d 03          	.uleb128 0x3
 480 009e 08          	.uleb128 0x8
 481 009f 3A          	.uleb128 0x3a
 482 00a0 0B          	.uleb128 0xb
 483 00a1 3B          	.uleb128 0x3b
 484 00a2 0B          	.uleb128 0xb
 485 00a3 27          	.uleb128 0x27
 486 00a4 0C          	.uleb128 0xc
 487 00a5 11          	.uleb128 0x11
 488 00a6 01          	.uleb128 0x1
 489 00a7 12          	.uleb128 0x12
 490 00a8 01          	.uleb128 0x1
 491 00a9 40          	.uleb128 0x40
 492 00aa 0A          	.uleb128 0xa
 493 00ab 01          	.uleb128 0x1
 494 00ac 13          	.uleb128 0x13
 495 00ad 00          	.byte 0x0
 496 00ae 00          	.byte 0x0
 497 00af 0E          	.uleb128 0xe
 498 00b0 05          	.uleb128 0x5
 499 00b1 00          	.byte 0x0
 500 00b2 03          	.uleb128 0x3
 501 00b3 08          	.uleb128 0x8
 502 00b4 3A          	.uleb128 0x3a
 503 00b5 0B          	.uleb128 0xb
 504 00b6 3B          	.uleb128 0x3b
 505 00b7 0B          	.uleb128 0xb
 506 00b8 49          	.uleb128 0x49
 507 00b9 13          	.uleb128 0x13
 508 00ba 02          	.uleb128 0x2
 509 00bb 0A          	.uleb128 0xa
 510 00bc 00          	.byte 0x0
 511 00bd 00          	.byte 0x0
 512 00be 0F          	.uleb128 0xf
 513 00bf 0F          	.uleb128 0xf
 514 00c0 00          	.byte 0x0
 515 00c1 0B          	.uleb128 0xb
 516 00c2 0B          	.uleb128 0xb
 517 00c3 49          	.uleb128 0x49
 518 00c4 13          	.uleb128 0x13
 519 00c5 00          	.byte 0x0
 520 00c6 00          	.byte 0x0
 521 00c7 00          	.byte 0x0
 522                 	.section .debug_pubnames,info
 523 0000 26 00 00 00 	.4byte 0x26
 524 0004 02 00       	.2byte 0x2
 525 0006 00 00 00 00 	.4byte .Ldebug_info0
 526 000a A7 02 00 00 	.4byte 0x2a7
 527 000e 4E 02 00 00 	.4byte 0x24e
 528 0012 70 61 72 73 	.asciz "parse_can_supot_sig"
 528      65 5F 63 61 
 528      6E 5F 73 75 
 528      70 6F 74 5F 
 528      73 69 67 00 
MPLAB XC16 ASSEMBLY Listing:   			page 12


 529 0026 00 00 00 00 	.4byte 0x0
 530                 	.section .debug_pubtypes,info
 531 0000 39 00 00 00 	.4byte 0x39
 532 0004 02 00       	.2byte 0x2
 533 0006 00 00 00 00 	.4byte .Ldebug_info0
 534 000a A7 02 00 00 	.4byte 0x2a7
 535 000e F3 00 00 00 	.4byte 0xf3
 536 0012 75 69 6E 74 	.asciz "uint16_t"
 536      31 36 5F 74 
 536      00 
 537 001b CD 01 00 00 	.4byte 0x1cd
 538 001f 43 41 4E 64 	.asciz "CANdata"
 538      61 74 61 00 
 539 0027 39 02 00 00 	.4byte 0x239
 540 002b 53 55 50 4F 	.asciz "SUPOT_MSG_SIG"
 540      54 5F 4D 53 
 540      47 5F 53 49 
 540      47 00 
 541 0039 00 00 00 00 	.4byte 0x0
 542                 	.section .debug_aranges,info
 543 0000 14 00 00 00 	.4byte 0x14
 544 0004 02 00       	.2byte 0x2
 545 0006 00 00 00 00 	.4byte .Ldebug_info0
 546 000a 04          	.byte 0x4
 547 000b 00          	.byte 0x0
 548 000c 00 00       	.2byte 0x0
 549 000e 00 00       	.2byte 0x0
 550 0010 00 00 00 00 	.4byte 0x0
 551 0014 00 00 00 00 	.4byte 0x0
 552                 	.section .debug_str,info
 553                 	.section .text,code
 554              	
 555              	
 556              	
 557              	.section __c30_info,info,bss
 558                 	__psv_trap_errata:
 559                 	
 560                 	.section __c30_signature,info,data
 561 0000 01 00       	.word 0x0001
 562 0002 00 00       	.word 0x0000
 563 0004 00 00       	.word 0x0000
 564                 	
 565                 	
 566                 	
 567                 	.set ___PA___,0
 568                 	.end
MPLAB XC16 ASSEMBLY Listing:   			page 13


DEFINED SYMBOLS
                            *ABS*:00000000 /home/user/Documents/FST/Programming/project_sub-zero/lib/can-ids/Devices/SUPOT_CAN.c
                            *ABS*:00000001 __33EP256MU806
                            *ABS*:00000001 __dsPIC33E
                            *ABS*:00000001 __HAS_DSP
                            *ABS*:00000001 __HAS_DMAV2
                            *ABS*:00000001 __HAS_CODEGUARD
                            *ABS*:00000001 __HAS_PMP_ENHANCED
                            *ABS*:00000001 __HAS_EDS
                            *ABS*:00000001 __HAS_EP
                            *ABS*:00000011 __TARGET_DIVIDE_CYCLES
                            *ABS*:00000001 __C30ELF
    {standard input}:14     .text:00000000 _parse_can_supot_sig
    {standard input}:18     *ABS*:00000000 ___PA___
    {standard input}:35     *ABS*:00000000 ___BP___
    {standard input}:558    __c30_info:00000000 __psv_trap_errata
    {standard input}:19     .text:00000000 .L0
                            .text:00000026 .L2
                            .text:0000003a .L1
                    .debug_abbrev:00000000 .Ldebug_abbrev0
                            .text:00000000 .Ltext0
                            .text:00000042 .Letext0
                      .debug_line:00000000 .Ldebug_line0
                            .text:00000000 .LFB0
                            .text:00000042 .LFE0
                     .debug_frame:00000000 .Lframe0
                      .debug_info:00000000 .Ldebug_info0

UNDEFINED SYMBOLS
CORCON

EQUATE SYMBOLS
/home/user/Documents/FST/Programming/project_sub-zero/lib/can-ids/Devices/SUPOT_CAN.c = 0x0
                  __33EP256MU806 = 0x1
                      __dsPIC33E = 0x1
                       __HAS_DSP = 0x1
                     __HAS_DMAV2 = 0x1
                 __HAS_CODEGUARD = 0x1
              __HAS_PMP_ENHANCED = 0x1
                       __HAS_EDS = 0x1
                        __HAS_EP = 0x1
          __TARGET_DIVIDE_CYCLES = 0x11
                        __C30ELF = 0x1
                        ___PA___ = 0x0
                        ___BP___ = 0x0
