MPLAB XC16 ASSEMBLY Listing:   			page 1


   1              	.file "/home/user/Documents/FST/Programming/project_sub-zero/lib/can-ids/Devices/DCU_CAN.c"
   2              	.section .debug_abbrev,info
   3                 	.Ldebug_abbrev0:
   4                 	.section .debug_info,info
   5                 	.Ldebug_info0:
   6                 	.section .debug_line,info
   7                 	.Ldebug_line0:
   8 0000 FB 00 00 00 	.section .text,code
   8      02 00 9A 00 
   8      00 00 01 01 
   8      FB 0E 0A 00 
   8      01 01 01 01 
   8      00 00 00 01 
   8      6C 69 62 2F 
   8      63 61 6E 2D 
   8      69 64 73 2F 
   9              	.Ltext0:
  10              	.section .text,code
  11              	.align 2
  12              	.global _parse_dcu_message_status
  13              	.type _parse_dcu_message_status,@function
  14              	_parse_dcu_message_status:
  15              	.LFB0:
  16              	.file 1 "lib/can-ids/Devices/DCU_CAN.c"
   1:lib/can-ids/Devices/DCU_CAN.c **** #include <stdbool.h>
   2:lib/can-ids/Devices/DCU_CAN.c **** #include <stdint.h>
   3:lib/can-ids/Devices/DCU_CAN.c **** #include "can-ids/CAN_IDs.h"
   4:lib/can-ids/Devices/DCU_CAN.c **** #include "DCU_CAN.h"
   5:lib/can-ids/Devices/DCU_CAN.c **** 
   6:lib/can-ids/Devices/DCU_CAN.c **** void parse_dcu_message_status(uint16_t data[4], DCU_MSG_Status *status)
   7:lib/can-ids/Devices/DCU_CAN.c **** {
  17              	.loc 1 7 0
  18              	.set ___PA___,1
  19 000000  04 00 FA 	lnk #4
  20              	.LCFI0:
  21              	.loc 1 7 0
  22 000002  00 0F 78 	mov w0,[w14]
  23 000004  11 07 98 	mov w1,[w14+2]
   8:lib/can-ids/Devices/DCU_CAN.c ****     status->FB_BL            = data[0] >> 0  & 1;
  24              	.loc 1 8 0
  25 000006  9E 00 78 	mov [w14],w1
  26 000008  1E 00 90 	mov [w14+2],w0
  27 00000a  91 00 78 	mov [w1],w1
  28 00000c  10 01 78 	mov [w0],w2
  29 00000e  E1 80 60 	and w1,#1,w1
  30 000010  02 00 A1 	bclr w2,#0
  31 000012  01 F0 A7 	btsc w1,#15
  32 000014  81 00 EA 	neg w1,w1
  33 000016  81 00 EA 	neg w1,w1
  34 000018  CF 08 DE 	lsr w1,#15,w1
  35 00001a  81 40 78 	mov.b w1,w1
  36 00001c  81 80 FB 	ze w1,w1
  37 00001e  E1 80 60 	and w1,#1,w1
  38 000020  82 80 70 	ior w1,w2,w1
  39 000022  01 08 78 	mov w1,[w0]
   9:lib/can-ids/Devices/DCU_CAN.c ****     status->FB_CAN_E         = data[0] >> 1  & 1;
  40              	.loc 1 9 0
MPLAB XC16 ASSEMBLY Listing:   			page 2


  41 000024  9E 00 78 	mov [w14],w1
  42 000026  1E 00 90 	mov [w14+2],w0
  43 000028  91 00 78 	mov [w1],w1
  44 00002a  10 01 78 	mov [w0],w2
  45 00002c  81 00 D1 	lsr w1,w1
  46 00002e  02 10 A1 	bclr w2,#1
  47 000030  E1 80 60 	and w1,#1,w1
  48 000032  01 F0 A7 	btsc w1,#15
  49 000034  81 00 EA 	neg w1,w1
  50 000036  81 00 EA 	neg w1,w1
  51 000038  CF 08 DE 	lsr w1,#15,w1
  52 00003a  81 40 78 	mov.b w1,w1
  53 00003c  81 80 FB 	ze w1,w1
  54 00003e  E1 80 60 	and w1,#1,w1
  55 000040  81 80 40 	add w1,w1,w1
  56 000042  82 80 70 	ior w1,w2,w1
  57 000044  01 08 78 	mov w1,[w0]
  10:lib/can-ids/Devices/DCU_CAN.c ****     status->FB_DRS           = data[0] >> 2  & 1;
  58              	.loc 1 10 0
  59 000046  9E 00 78 	mov [w14],w1
  60 000048  1E 00 90 	mov [w14+2],w0
  61 00004a  91 00 78 	mov [w1],w1
  62 00004c  10 01 78 	mov [w0],w2
  63 00004e  C2 08 DE 	lsr w1,#2,w1
  64 000050  02 20 A1 	bclr w2,#2
  65 000052  E1 80 60 	and w1,#1,w1
  66 000054  01 F0 A7 	btsc w1,#15
  67 000056  81 00 EA 	neg w1,w1
  68 000058  81 00 EA 	neg w1,w1
  69 00005a  CF 08 DE 	lsr w1,#15,w1
  70 00005c  81 40 78 	mov.b w1,w1
  71 00005e  81 80 FB 	ze w1,w1
  72 000060  E1 80 60 	and w1,#1,w1
  73 000062  C2 08 DD 	sl w1,#2,w1
  74 000064  82 80 70 	ior w1,w2,w1
  75 000066  01 08 78 	mov w1,[w0]
  11:lib/can-ids/Devices/DCU_CAN.c ****     status->FB_CUB           = data[0] >> 3  & 1;
  76              	.loc 1 11 0
  77 000068  9E 00 78 	mov [w14],w1
  78 00006a  1E 00 90 	mov [w14+2],w0
  79 00006c  91 00 78 	mov [w1],w1
  80 00006e  10 01 78 	mov [w0],w2
  81 000070  C3 08 DE 	lsr w1,#3,w1
  82 000072  02 30 A1 	bclr w2,#3
  83 000074  E1 80 60 	and w1,#1,w1
  84 000076  01 F0 A7 	btsc w1,#15
  85 000078  81 00 EA 	neg w1,w1
  86 00007a  81 00 EA 	neg w1,w1
  87 00007c  CF 08 DE 	lsr w1,#15,w1
  88 00007e  81 40 78 	mov.b w1,w1
  89 000080  81 80 FB 	ze w1,w1
  90 000082  E1 80 60 	and w1,#1,w1
  91 000084  C3 08 DD 	sl w1,#3,w1
  92 000086  82 80 70 	ior w1,w2,w1
  93 000088  01 08 78 	mov w1,[w0]
  12:lib/can-ids/Devices/DCU_CAN.c ****     status->FB_fans          = data[0] >> 4  & 1;
  94              	.loc 1 12 0
MPLAB XC16 ASSEMBLY Listing:   			page 3


  95 00008a  9E 00 78 	mov [w14],w1
  96 00008c  1E 00 90 	mov [w14+2],w0
  97 00008e  91 00 78 	mov [w1],w1
  98 000090  10 01 78 	mov [w0],w2
  99 000092  C4 08 DE 	lsr w1,#4,w1
 100 000094  02 40 A1 	bclr w2,#4
 101 000096  E1 80 60 	and w1,#1,w1
 102 000098  01 F0 A7 	btsc w1,#15
 103 00009a  81 00 EA 	neg w1,w1
 104 00009c  81 00 EA 	neg w1,w1
 105 00009e  CF 08 DE 	lsr w1,#15,w1
 106 0000a0  81 40 78 	mov.b w1,w1
 107 0000a2  81 80 FB 	ze w1,w1
 108 0000a4  E1 80 60 	and w1,#1,w1
 109 0000a6  C4 08 DD 	sl w1,#4,w1
 110 0000a8  82 80 70 	ior w1,w2,w1
 111 0000aa  01 08 78 	mov w1,[w0]
  13:lib/can-ids/Devices/DCU_CAN.c ****     status->FB_pumps         = data[0] >> 5  & 1;
 112              	.loc 1 13 0
 113 0000ac  9E 00 78 	mov [w14],w1
 114 0000ae  1E 00 90 	mov [w14+2],w0
 115 0000b0  91 00 78 	mov [w1],w1
 116 0000b2  10 01 78 	mov [w0],w2
 117 0000b4  C5 08 DE 	lsr w1,#5,w1
 118 0000b6  02 50 A1 	bclr w2,#5
 119 0000b8  E1 80 60 	and w1,#1,w1
 120 0000ba  01 F0 A7 	btsc w1,#15
 121 0000bc  81 00 EA 	neg w1,w1
 122 0000be  81 00 EA 	neg w1,w1
 123 0000c0  CF 08 DE 	lsr w1,#15,w1
 124 0000c2  81 40 78 	mov.b w1,w1
 125 0000c4  81 80 FB 	ze w1,w1
 126 0000c6  E1 80 60 	and w1,#1,w1
 127 0000c8  C5 08 DD 	sl w1,#5,w1
 128 0000ca  82 80 70 	ior w1,w2,w1
 129 0000cc  01 08 78 	mov w1,[w0]
  14:lib/can-ids/Devices/DCU_CAN.c ****     status->FB_EM            = data[0] >> 6  & 1;
 130              	.loc 1 14 0
 131 0000ce  9E 00 78 	mov [w14],w1
 132 0000d0  1E 00 90 	mov [w14+2],w0
 133 0000d2  91 00 78 	mov [w1],w1
 134 0000d4  10 01 78 	mov [w0],w2
 135 0000d6  C6 08 DE 	lsr w1,#6,w1
 136 0000d8  02 60 A1 	bclr w2,#6
 137 0000da  E1 80 60 	and w1,#1,w1
 138 0000dc  01 F0 A7 	btsc w1,#15
 139 0000de  81 00 EA 	neg w1,w1
 140 0000e0  81 00 EA 	neg w1,w1
 141 0000e2  CF 08 DE 	lsr w1,#15,w1
 142 0000e4  81 40 78 	mov.b w1,w1
 143 0000e6  81 80 FB 	ze w1,w1
 144 0000e8  E1 80 60 	and w1,#1,w1
 145 0000ea  C6 08 DD 	sl w1,#6,w1
 146 0000ec  82 80 70 	ior w1,w2,w1
 147 0000ee  01 08 78 	mov w1,[w0]
  15:lib/can-ids/Devices/DCU_CAN.c ****     status->FB_SC_origin     = data[0] >> 7  & 1;
 148              	.loc 1 15 0
MPLAB XC16 ASSEMBLY Listing:   			page 4


 149 0000f0  9E 00 78 	mov [w14],w1
 150 0000f2  1E 00 90 	mov [w14+2],w0
 151 0000f4  91 00 78 	mov [w1],w1
 152 0000f6  10 01 78 	mov [w0],w2
 153 0000f8  C7 08 DE 	lsr w1,#7,w1
 154 0000fa  02 70 A1 	bclr w2,#7
 155 0000fc  E1 80 60 	and w1,#1,w1
 156 0000fe  01 F0 A7 	btsc w1,#15
 157 000100  81 00 EA 	neg w1,w1
 158 000102  81 00 EA 	neg w1,w1
 159 000104  CF 08 DE 	lsr w1,#15,w1
 160 000106  81 40 78 	mov.b w1,w1
 161 000108  81 80 FB 	ze w1,w1
 162 00010a  E1 80 60 	and w1,#1,w1
 163 00010c  C7 08 DD 	sl w1,#7,w1
 164 00010e  82 80 70 	ior w1,w2,w1
 165 000110  01 08 78 	mov w1,[w0]
  16:lib/can-ids/Devices/DCU_CAN.c ****     status->FB_CUA           = data[0] >> 8  & 1;
 166              	.loc 1 16 0
 167 000112  9E 00 78 	mov [w14],w1
 168 000114  1E 00 90 	mov [w14+2],w0
 169 000116  91 00 78 	mov [w1],w1
 170 000118  10 01 78 	mov [w0],w2
 171 00011a  C8 08 DE 	lsr w1,#8,w1
 172 00011c  02 80 A1 	bclr w2,#8
 173 00011e  E1 80 60 	and w1,#1,w1
 174 000120  01 F0 A7 	btsc w1,#15
 175 000122  81 00 EA 	neg w1,w1
 176 000124  81 00 EA 	neg w1,w1
 177 000126  CF 08 DE 	lsr w1,#15,w1
 178 000128  81 40 78 	mov.b w1,w1
 179 00012a  81 80 FB 	ze w1,w1
 180 00012c  E1 80 60 	and w1,#1,w1
 181 00012e  C8 08 DD 	sl w1,#8,w1
 182 000130  82 80 70 	ior w1,w2,w1
 183 000132  01 08 78 	mov w1,[w0]
  17:lib/can-ids/Devices/DCU_CAN.c ****     status->FB_TSAL          = data[0] >> 9  & 1;
 184              	.loc 1 17 0
 185 000134  9E 00 78 	mov [w14],w1
 186 000136  1E 00 90 	mov [w14+2],w0
 187 000138  91 00 78 	mov [w1],w1
 188 00013a  10 01 78 	mov [w0],w2
 189 00013c  C9 08 DE 	lsr w1,#9,w1
 190 00013e  02 90 A1 	bclr w2,#9
 191 000140  E1 80 60 	and w1,#1,w1
 192 000142  01 F0 A7 	btsc w1,#15
 193 000144  81 00 EA 	neg w1,w1
 194 000146  81 00 EA 	neg w1,w1
 195 000148  CF 08 DE 	lsr w1,#15,w1
 196 00014a  81 40 78 	mov.b w1,w1
 197 00014c  81 80 FB 	ze w1,w1
 198 00014e  E1 80 60 	and w1,#1,w1
 199 000150  C9 08 DD 	sl w1,#9,w1
 200 000152  82 80 70 	ior w1,w2,w1
 201 000154  01 08 78 	mov w1,[w0]
  18:lib/can-ids/Devices/DCU_CAN.c ****     status->FB_fans_AMS      = data[0] >> 10 & 1;
 202              	.loc 1 18 0
MPLAB XC16 ASSEMBLY Listing:   			page 5


 203 000156  9E 00 78 	mov [w14],w1
 204 000158  1E 00 90 	mov [w14+2],w0
 205 00015a  91 00 78 	mov [w1],w1
 206 00015c  10 01 78 	mov [w0],w2
 207 00015e  CA 08 DE 	lsr w1,#10,w1
 208 000160  02 A0 A1 	bclr w2,#10
 209 000162  E1 80 60 	and w1,#1,w1
 210 000164  01 F0 A7 	btsc w1,#15
 211 000166  81 00 EA 	neg w1,w1
 212 000168  81 00 EA 	neg w1,w1
 213 00016a  CF 08 DE 	lsr w1,#15,w1
 214 00016c  81 40 78 	mov.b w1,w1
 215 00016e  81 80 FB 	ze w1,w1
 216 000170  E1 80 60 	and w1,#1,w1
 217 000172  CA 08 DD 	sl w1,#10,w1
 218 000174  82 80 70 	ior w1,w2,w1
 219 000176  01 08 78 	mov w1,[w0]
  19:lib/can-ids/Devices/DCU_CAN.c ****     status->FB_AMS           = data[0] >> 11 & 1;
 220              	.loc 1 19 0
 221 000178  9E 00 78 	mov [w14],w1
 222 00017a  1E 00 90 	mov [w14+2],w0
 223 00017c  91 00 78 	mov [w1],w1
 224 00017e  10 01 78 	mov [w0],w2
 225 000180  CB 08 DE 	lsr w1,#11,w1
 226 000182  02 B0 A1 	bclr w2,#11
 227 000184  E1 80 60 	and w1,#1,w1
 228 000186  01 F0 A7 	btsc w1,#15
 229 000188  81 00 EA 	neg w1,w1
 230 00018a  81 00 EA 	neg w1,w1
 231 00018c  CF 08 DE 	lsr w1,#15,w1
 232 00018e  81 40 78 	mov.b w1,w1
 233 000190  81 80 FB 	ze w1,w1
 234 000192  E1 80 60 	and w1,#1,w1
 235 000194  CB 08 DD 	sl w1,#11,w1
 236 000196  82 80 70 	ior w1,w2,w1
 237 000198  01 08 78 	mov w1,[w0]
  20:lib/can-ids/Devices/DCU_CAN.c ****     status->SC_BSPD_to_bat   = data[0] >> 12 & 1;
 238              	.loc 1 20 0
 239 00019a  9E 00 78 	mov [w14],w1
 240 00019c  1E 00 90 	mov [w14+2],w0
 241 00019e  91 00 78 	mov [w1],w1
 242 0001a0  10 01 78 	mov [w0],w2
 243 0001a2  CC 08 DE 	lsr w1,#12,w1
 244 0001a4  02 C0 A1 	bclr w2,#12
 245 0001a6  E1 80 60 	and w1,#1,w1
 246 0001a8  01 F0 A7 	btsc w1,#15
 247 0001aa  81 00 EA 	neg w1,w1
 248 0001ac  81 00 EA 	neg w1,w1
 249 0001ae  CF 08 DE 	lsr w1,#15,w1
 250 0001b0  81 40 78 	mov.b w1,w1
 251 0001b2  81 80 FB 	ze w1,w1
 252 0001b4  E1 80 60 	and w1,#1,w1
 253 0001b6  CC 08 DD 	sl w1,#12,w1
 254 0001b8  82 80 70 	ior w1,w2,w1
 255 0001ba  01 08 78 	mov w1,[w0]
  21:lib/can-ids/Devices/DCU_CAN.c ****     status->SC_MH_to_front   = data[0] >> 13 & 1;
 256              	.loc 1 21 0
MPLAB XC16 ASSEMBLY Listing:   			page 6


 257 0001bc  9E 00 78 	mov [w14],w1
 258 0001be  1E 00 90 	mov [w14+2],w0
 259 0001c0  91 00 78 	mov [w1],w1
 260 0001c2  10 01 78 	mov [w0],w2
 261 0001c4  CD 08 DE 	lsr w1,#13,w1
 262 0001c6  02 D0 A1 	bclr w2,#13
 263 0001c8  E1 80 60 	and w1,#1,w1
 264 0001ca  01 F0 A7 	btsc w1,#15
 265 0001cc  81 00 EA 	neg w1,w1
 266 0001ce  81 00 EA 	neg w1,w1
 267 0001d0  CF 08 DE 	lsr w1,#15,w1
 268 0001d2  81 40 78 	mov.b w1,w1
 269 0001d4  81 80 FB 	ze w1,w1
 270 0001d6  E1 80 60 	and w1,#1,w1
 271 0001d8  CD 08 DD 	sl w1,#13,w1
 272 0001da  82 80 70 	ior w1,w2,w1
 273 0001dc  01 08 78 	mov w1,[w0]
  22:lib/can-ids/Devices/DCU_CAN.c ****     status->SC_front_to_BSPD = data[0] >> 14 & 1;
 274              	.loc 1 22 0
 275 0001de  9E 00 78 	mov [w14],w1
 276 0001e0  1E 00 90 	mov [w14+2],w0
 277 0001e2  91 00 78 	mov [w1],w1
 278 0001e4  10 01 78 	mov [w0],w2
 279 0001e6  CE 08 DE 	lsr w1,#14,w1
 280 0001e8  02 E0 A1 	bclr w2,#14
 281 0001ea  E1 80 60 	and w1,#1,w1
 282 0001ec  01 F0 A7 	btsc w1,#15
 283 0001ee  81 00 EA 	neg w1,w1
 284 0001f0  81 00 EA 	neg w1,w1
 285 0001f2  CF 08 DE 	lsr w1,#15,w1
 286 0001f4  81 40 78 	mov.b w1,w1
 287 0001f6  81 80 FB 	ze w1,w1
 288 0001f8  E1 80 60 	and w1,#1,w1
 289 0001fa  CE 08 DD 	sl w1,#14,w1
 290 0001fc  82 80 70 	ior w1,w2,w1
 291 0001fe  01 08 78 	mov w1,[w0]
  23:lib/can-ids/Devices/DCU_CAN.c **** 
  24:lib/can-ids/Devices/DCU_CAN.c ****     status->CUA_sig          = data[1] >> 0  & 1;
 292              	.loc 1 24 0
 293 000200  9E 80 E8 	inc2 [w14],w1
 294 000202  1E 00 90 	mov [w14+2],w0
 295 000204  91 00 78 	mov [w1],w1
 296 000206  10 01 90 	mov [w0+2],w2
 297 000208  E1 80 60 	and w1,#1,w1
 298 00020a  02 00 A1 	bclr w2,#0
 299 00020c  01 F0 A7 	btsc w1,#15
 300 00020e  81 00 EA 	neg w1,w1
 301 000210  81 00 EA 	neg w1,w1
 302 000212  CF 08 DE 	lsr w1,#15,w1
 303 000214  81 40 78 	mov.b w1,w1
 304 000216  81 80 FB 	ze w1,w1
 305 000218  E1 80 60 	and w1,#1,w1
 306 00021a  82 80 70 	ior w1,w2,w1
 307 00021c  11 00 98 	mov w1,[w0+2]
  25:lib/can-ids/Devices/DCU_CAN.c ****     status->CUB_sig          = data[1] >> 1  & 1;
 308              	.loc 1 25 0
 309 00021e  9E 80 E8 	inc2 [w14],w1
MPLAB XC16 ASSEMBLY Listing:   			page 7


 310 000220  1E 00 90 	mov [w14+2],w0
 311 000222  91 00 78 	mov [w1],w1
 312 000224  10 01 90 	mov [w0+2],w2
 313 000226  81 00 D1 	lsr w1,w1
 314 000228  02 10 A1 	bclr w2,#1
 315 00022a  E1 80 60 	and w1,#1,w1
 316 00022c  01 F0 A7 	btsc w1,#15
 317 00022e  81 00 EA 	neg w1,w1
 318 000230  81 00 EA 	neg w1,w1
 319 000232  CF 08 DE 	lsr w1,#15,w1
 320 000234  81 40 78 	mov.b w1,w1
 321 000236  81 80 FB 	ze w1,w1
 322 000238  E1 80 60 	and w1,#1,w1
 323 00023a  81 80 40 	add w1,w1,w1
 324 00023c  82 80 70 	ior w1,w2,w1
 325 00023e  11 00 98 	mov w1,[w0+2]
  26:lib/can-ids/Devices/DCU_CAN.c ****     status->fan_sig          = data[1] >> 2  & 1;
 326              	.loc 1 26 0
 327 000240  9E 80 E8 	inc2 [w14],w1
 328 000242  1E 00 90 	mov [w14+2],w0
 329 000244  91 00 78 	mov [w1],w1
 330 000246  10 01 90 	mov [w0+2],w2
 331 000248  C2 08 DE 	lsr w1,#2,w1
 332 00024a  02 20 A1 	bclr w2,#2
 333 00024c  E1 80 60 	and w1,#1,w1
 334 00024e  01 F0 A7 	btsc w1,#15
 335 000250  81 00 EA 	neg w1,w1
 336 000252  81 00 EA 	neg w1,w1
 337 000254  CF 08 DE 	lsr w1,#15,w1
 338 000256  81 40 78 	mov.b w1,w1
 339 000258  81 80 FB 	ze w1,w1
 340 00025a  E1 80 60 	and w1,#1,w1
 341 00025c  C2 08 DD 	sl w1,#2,w1
 342 00025e  82 80 70 	ior w1,w2,w1
 343 000260  11 00 98 	mov w1,[w0+2]
  27:lib/can-ids/Devices/DCU_CAN.c ****     status->SC_switch_sig    = data[1] >> 3  & 1;
 344              	.loc 1 27 0
 345 000262  9E 80 E8 	inc2 [w14],w1
 346 000264  1E 00 90 	mov [w14+2],w0
 347 000266  91 00 78 	mov [w1],w1
 348 000268  10 01 90 	mov [w0+2],w2
 349 00026a  C3 08 DE 	lsr w1,#3,w1
 350 00026c  02 30 A1 	bclr w2,#3
 351 00026e  E1 80 60 	and w1,#1,w1
 352 000270  01 F0 A7 	btsc w1,#15
 353 000272  81 00 EA 	neg w1,w1
 354 000274  81 00 EA 	neg w1,w1
 355 000276  CF 08 DE 	lsr w1,#15,w1
 356 000278  81 40 78 	mov.b w1,w1
 357 00027a  81 80 FB 	ze w1,w1
 358 00027c  E1 80 60 	and w1,#1,w1
 359 00027e  C3 08 DD 	sl w1,#3,w1
 360 000280  82 80 70 	ior w1,w2,w1
 361 000282  11 00 98 	mov w1,[w0+2]
  28:lib/can-ids/Devices/DCU_CAN.c ****     status->pump1_sig        = data[1] >> 4  & 1;
 362              	.loc 1 28 0
 363 000284  9E 80 E8 	inc2 [w14],w1
MPLAB XC16 ASSEMBLY Listing:   			page 8


 364 000286  1E 00 90 	mov [w14+2],w0
 365 000288  91 00 78 	mov [w1],w1
 366 00028a  10 01 90 	mov [w0+2],w2
 367 00028c  C4 08 DE 	lsr w1,#4,w1
 368 00028e  02 40 A1 	bclr w2,#4
 369 000290  E1 80 60 	and w1,#1,w1
 370 000292  01 F0 A7 	btsc w1,#15
 371 000294  81 00 EA 	neg w1,w1
 372 000296  81 00 EA 	neg w1,w1
 373 000298  CF 08 DE 	lsr w1,#15,w1
 374 00029a  81 40 78 	mov.b w1,w1
 375 00029c  81 80 FB 	ze w1,w1
 376 00029e  E1 80 60 	and w1,#1,w1
 377 0002a0  C4 08 DD 	sl w1,#4,w1
 378 0002a2  82 80 70 	ior w1,w2,w1
 379 0002a4  11 00 98 	mov w1,[w0+2]
  29:lib/can-ids/Devices/DCU_CAN.c ****     status->pump2_sig        = data[1] >> 5  & 1;
 380              	.loc 1 29 0
 381 0002a6  9E 80 E8 	inc2 [w14],w1
 382 0002a8  1E 00 90 	mov [w14+2],w0
 383 0002aa  91 00 78 	mov [w1],w1
 384 0002ac  10 01 90 	mov [w0+2],w2
 385 0002ae  C5 08 DE 	lsr w1,#5,w1
 386 0002b0  02 50 A1 	bclr w2,#5
 387 0002b2  E1 80 60 	and w1,#1,w1
 388 0002b4  01 F0 A7 	btsc w1,#15
 389 0002b6  81 00 EA 	neg w1,w1
 390 0002b8  81 00 EA 	neg w1,w1
 391 0002ba  CF 08 DE 	lsr w1,#15,w1
 392 0002bc  81 40 78 	mov.b w1,w1
 393 0002be  81 80 FB 	ze w1,w1
 394 0002c0  E1 80 60 	and w1,#1,w1
 395 0002c2  C5 08 DD 	sl w1,#5,w1
 396 0002c4  82 80 70 	ior w1,w2,w1
 397 0002c6  11 00 98 	mov w1,[w0+2]
  30:lib/can-ids/Devices/DCU_CAN.c ****     status->dcdc_switch_sig  = data[1] >> 6  & 1;
 398              	.loc 1 30 0
 399 0002c8  9E 80 E8 	inc2 [w14],w1
 400 0002ca  1E 00 90 	mov [w14+2],w0
 401 0002cc  91 00 78 	mov [w1],w1
 402 0002ce  10 01 90 	mov [w0+2],w2
 403 0002d0  C6 08 DE 	lsr w1,#6,w1
 404 0002d2  02 60 A1 	bclr w2,#6
 405 0002d4  E1 80 60 	and w1,#1,w1
 406 0002d6  01 F0 A7 	btsc w1,#15
 407 0002d8  81 00 EA 	neg w1,w1
 408 0002da  81 00 EA 	neg w1,w1
 409 0002dc  CF 08 DE 	lsr w1,#15,w1
 410 0002de  81 40 78 	mov.b w1,w1
 411 0002e0  81 80 FB 	ze w1,w1
 412 0002e2  E1 80 60 	and w1,#1,w1
 413 0002e4  C6 08 DD 	sl w1,#6,w1
 414 0002e6  82 80 70 	ior w1,w2,w1
 415 0002e8  11 00 98 	mov w1,[w0+2]
  31:lib/can-ids/Devices/DCU_CAN.c ****     status->BL_sig           = data[1] >> 7  & 1;
 416              	.loc 1 31 0
 417 0002ea  9E 80 E8 	inc2 [w14],w1
MPLAB XC16 ASSEMBLY Listing:   			page 9


 418 0002ec  1E 00 90 	mov [w14+2],w0
 419 0002ee  91 00 78 	mov [w1],w1
 420 0002f0  10 01 90 	mov [w0+2],w2
 421 0002f2  C7 08 DE 	lsr w1,#7,w1
 422 0002f4  02 70 A1 	bclr w2,#7
 423 0002f6  E1 80 60 	and w1,#1,w1
 424 0002f8  01 F0 A7 	btsc w1,#15
 425 0002fa  81 00 EA 	neg w1,w1
 426 0002fc  81 00 EA 	neg w1,w1
 427 0002fe  CF 08 DE 	lsr w1,#15,w1
 428 000300  81 40 78 	mov.b w1,w1
 429 000302  81 80 FB 	ze w1,w1
 430 000304  E1 80 60 	and w1,#1,w1
 431 000306  C7 08 DD 	sl w1,#7,w1
 432 000308  82 80 70 	ior w1,w2,w1
 433 00030a  11 00 98 	mov w1,[w0+2]
  32:lib/can-ids/Devices/DCU_CAN.c ****     status->buzz_sig         = data[1] >> 8  & 1;
 434              	.loc 1 32 0
 435 00030c  9E 80 E8 	inc2 [w14],w1
 436 00030e  1E 00 90 	mov [w14+2],w0
 437 000310  91 00 78 	mov [w1],w1
 438 000312  10 01 90 	mov [w0+2],w2
 439 000314  C8 08 DE 	lsr w1,#8,w1
 440 000316  02 80 A1 	bclr w2,#8
 441 000318  E1 80 60 	and w1,#1,w1
 442 00031a  01 F0 A7 	btsc w1,#15
 443 00031c  81 00 EA 	neg w1,w1
 444 00031e  81 00 EA 	neg w1,w1
 445 000320  CF 08 DE 	lsr w1,#15,w1
 446 000322  81 40 78 	mov.b w1,w1
 447 000324  81 80 FB 	ze w1,w1
 448 000326  E1 80 60 	and w1,#1,w1
 449 000328  C8 08 DD 	sl w1,#8,w1
 450 00032a  82 80 70 	ior w1,w2,w1
 451 00032c  11 00 98 	mov w1,[w0+2]
  33:lib/can-ids/Devices/DCU_CAN.c ****     status->DRS_PWM          = data[1] >> 9  & 1;
 452              	.loc 1 33 0
 453 00032e  1E 80 E8 	inc2 [w14],w0
 454 000330  9E 00 90 	mov [w14+2],w1
 455 000332  10 00 78 	mov [w0],w0
 456 000334  91 01 90 	mov [w1+2],w3
 457 000336  49 00 DE 	lsr w0,#9,w0
 458 000338  F2 1F 20 	mov #511,w2
 459 00033a  00 40 78 	mov.b w0,w0
 460 00033c  02 81 61 	and w3,w2,w2
 461 00033e  00 74 A1 	bclr.b w0,#7
 462 000340  61 40 60 	and.b w0,#1,w0
 463 000342  00 74 A1 	bclr.b w0,#7
 464 000344  00 80 FB 	ze w0,w0
 465 000346  49 00 DD 	sl w0,#9,w0
 466 000348  02 00 70 	ior w0,w2,w0
 467 00034a  90 00 98 	mov w0,[w1+2]
  34:lib/can-ids/Devices/DCU_CAN.c ****     
  35:lib/can-ids/Devices/DCU_CAN.c ****     status->TEMP_PIC = data[2];
 468              	.loc 1 35 0
 469 00034c  9E 00 78 	mov [w14],w1
 470 00034e  64 80 40 	add w1,#4,w0
MPLAB XC16 ASSEMBLY Listing:   			page 10


  36:lib/can-ids/Devices/DCU_CAN.c ****     status->TEMP_BOX = data[3];
 471              	.loc 1 36 0
 472 000350  1E 01 78 	mov [w14],w2
 473 000352  E6 00 41 	add w2,#6,w1
 474              	.loc 1 35 0
 475 000354  90 01 78 	mov [w0],w3
 476 000356  1E 01 90 	mov [w14+2],w2
 477              	.loc 1 36 0
 478 000358  1E 00 90 	mov [w14+2],w0
 479              	.loc 1 35 0
 480 00035a  23 01 98 	mov w3,[w2+4]
 481              	.loc 1 36 0
 482 00035c  91 00 78 	mov [w1],w1
 483 00035e  31 00 98 	mov w1,[w0+6]
  37:lib/can-ids/Devices/DCU_CAN.c **** }
 484              	.loc 1 37 0
 485 000360  8E 07 78 	mov w14,w15
 486 000362  4F 07 78 	mov [--w15],w14
 487 000364  00 40 A9 	bclr CORCON,#2
 488 000366  00 00 06 	return 
 489              	.set ___PA___,0
 490              	.LFE0:
 491              	.size _parse_dcu_message_status,.-_parse_dcu_message_status
 492              	.align 2
 493              	.global _parse_dcu_message_current
 494              	.type _parse_dcu_message_current,@function
 495              	_parse_dcu_message_current:
 496              	.LFB1:
  38:lib/can-ids/Devices/DCU_CAN.c **** 
  39:lib/can-ids/Devices/DCU_CAN.c **** void parse_dcu_message_current(uint16_t data[4], DCU_MSG_Current *current)
  40:lib/can-ids/Devices/DCU_CAN.c **** {
 497              	.loc 1 40 0
 498              	.set ___PA___,1
 499 000368  04 00 FA 	lnk #4
 500              	.LCFI1:
 501              	.loc 1 40 0
 502 00036a  00 0F 78 	mov w0,[w14]
 503 00036c  11 07 98 	mov w1,[w14+2]
  41:lib/can-ids/Devices/DCU_CAN.c ****     current->LV_current = data[0];
 504              	.loc 1 41 0
 505 00036e  1E 00 78 	mov [w14],w0
  42:lib/can-ids/Devices/DCU_CAN.c ****     current->HV_current = data[1];
 506              	.loc 1 42 0
 507 000370  9E 80 E8 	inc2 [w14],w1
 508              	.loc 1 41 0
 509 000372  90 01 78 	mov [w0],w3
 510 000374  1E 01 90 	mov [w14+2],w2
 511              	.loc 1 42 0
 512 000376  1E 00 90 	mov [w14+2],w0
 513              	.loc 1 41 0
 514 000378  03 09 78 	mov w3,[w2]
 515              	.loc 1 42 0
 516 00037a  91 00 78 	mov [w1],w1
 517 00037c  11 00 98 	mov w1,[w0+2]
  43:lib/can-ids/Devices/DCU_CAN.c **** }
 518              	.loc 1 43 0
 519 00037e  8E 07 78 	mov w14,w15
MPLAB XC16 ASSEMBLY Listing:   			page 11


 520 000380  4F 07 78 	mov [--w15],w14
 521 000382  00 40 A9 	bclr CORCON,#2
 522 000384  00 00 06 	return 
 523              	.set ___PA___,0
 524              	.LFE1:
 525              	.size _parse_dcu_message_current,.-_parse_dcu_message_current
 526              	.align 2
 527              	.global _parse_can_dcu
 528              	.type _parse_can_dcu,@function
 529              	_parse_can_dcu:
 530              	.LFB2:
  44:lib/can-ids/Devices/DCU_CAN.c **** 
  45:lib/can-ids/Devices/DCU_CAN.c **** void parse_can_dcu(CANdata message, DCU_CAN_Data *data)
  46:lib/can-ids/Devices/DCU_CAN.c **** {
 531              	.loc 1 46 0
 532              	.set ___PA___,1
 533 000386  0E 00 FA 	lnk #14
 534              	.LCFI2:
 535              	.loc 1 46 0
 536 000388  00 0F 78 	mov w0,[w14]
 537 00038a  11 07 98 	mov w1,[w14+2]
  47:lib/can-ids/Devices/DCU_CAN.c ****     if (message.dev_id != DEVICE_ID_DCU) {
 538              	.loc 1 47 0
 539 00038c  1E 00 78 	mov [w14],w0
 540              	.loc 1 46 0
 541 00038e  22 07 98 	mov w2,[w14+4]
 542 000390  33 07 98 	mov w3,[w14+6]
 543 000392  44 07 98 	mov w4,[w14+8]
 544 000394  55 07 98 	mov w5,[w14+10]
 545 000396  66 07 98 	mov w6,[w14+12]
 546              	.loc 1 47 0
 547 000398  7F 00 60 	and w0,#31,w0
 548 00039a  E8 0F 50 	sub w0,#8,[w15]
 549              	.set ___BP___,0
 550 00039c  00 00 3A 	bra nz,.L8
 551              	.L4:
  48:lib/can-ids/Devices/DCU_CAN.c ****         /*FIXME: send info for error logging*/
  49:lib/can-ids/Devices/DCU_CAN.c ****         return;
  50:lib/can-ids/Devices/DCU_CAN.c ****     }
  51:lib/can-ids/Devices/DCU_CAN.c ****     
  52:lib/can-ids/Devices/DCU_CAN.c ****     switch (message.msg_id) {
 552              	.loc 1 52 0
 553 00039e  1E 00 78 	mov [w14],w0
 554 0003a0  01 02 20 	mov #32,w1
 555 0003a2  45 00 DE 	lsr w0,#5,w0
 556 0003a4  F0 43 B2 	and.b #63,w0
 557 0003a6  00 80 FB 	ze w0,w0
 558 0003a8  81 0F 50 	sub w0,w1,[w15]
 559              	.set ___BP___,0
 560 0003aa  00 00 32 	bra z,.L6
 561 0003ac  11 02 20 	mov #33,w1
 562 0003ae  81 0F 50 	sub w0,w1,[w15]
 563              	.set ___BP___,0
 564 0003b0  00 00 32 	bra z,.L7
 565 0003b2  00 00 37 	bra .L3
 566              	.L6:
  53:lib/can-ids/Devices/DCU_CAN.c ****         case MSG_ID_DCU_STATUS:
MPLAB XC16 ASSEMBLY Listing:   			page 12


  54:lib/can-ids/Devices/DCU_CAN.c ****             parse_dcu_message_status(message.data, &(data->status));
 567              	.loc 1 54 0
 568 0003b4  EE 00 90 	mov [w14+12],w1
 569 0003b6  64 00 47 	add w14,#4,w0
 570 0003b8  00 00 07 	rcall _parse_dcu_message_status
  55:lib/can-ids/Devices/DCU_CAN.c ****             break;
 571              	.loc 1 55 0
 572 0003ba  00 00 37 	bra .L3
 573              	.L7:
  56:lib/can-ids/Devices/DCU_CAN.c ****         case MSG_ID_DCU_CURRENT:
  57:lib/can-ids/Devices/DCU_CAN.c ****             parse_dcu_message_current(message.data, &(data->current));
 574              	.loc 1 57 0
 575 0003bc  EE 00 90 	mov [w14+12],w1
 576 0003be  64 00 47 	add w14,#4,w0
 577 0003c0  E8 80 40 	add w1,#8,w1
 578 0003c2  00 00 07 	rcall _parse_dcu_message_current
 579 0003c4  00 00 37 	bra .L3
 580              	.L8:
 581              	.L3:
  58:lib/can-ids/Devices/DCU_CAN.c ****             break;
  59:lib/can-ids/Devices/DCU_CAN.c ****     }
  60:lib/can-ids/Devices/DCU_CAN.c **** }
 582              	.loc 1 60 0
 583 0003c6  8E 07 78 	mov w14,w15
 584 0003c8  4F 07 78 	mov [--w15],w14
 585 0003ca  00 40 A9 	bclr CORCON,#2
 586 0003cc  00 00 06 	return 
 587              	.set ___PA___,0
 588              	.LFE2:
 589              	.size _parse_can_dcu,.-_parse_can_dcu
 590              	.section .debug_frame,info
 591                 	.Lframe0:
 592 0000 10 00 00 00 	.4byte .LECIE0-.LSCIE0
 593                 	.LSCIE0:
 594 0004 FF FF FF FF 	.4byte 0xffffffff
 595 0008 01          	.byte 0x1
 596 0009 00          	.byte 0
 597 000a 01          	.uleb128 0x1
 598 000b 02          	.sleb128 2
 599 000c 25          	.byte 0x25
 600 000d 12          	.byte 0x12
 601 000e 0F          	.uleb128 0xf
 602 000f 7E          	.sleb128 -2
 603 0010 09          	.byte 0x9
 604 0011 25          	.uleb128 0x25
 605 0012 0F          	.uleb128 0xf
 606 0013 00          	.align 4
 607                 	.LECIE0:
 608                 	.LSFDE0:
 609 0014 18 00 00 00 	.4byte .LEFDE0-.LASFDE0
 610                 	.LASFDE0:
 611 0018 00 00 00 00 	.4byte .Lframe0
 612 001c 00 00 00 00 	.4byte .LFB0
 613 0020 68 03 00 00 	.4byte .LFE0-.LFB0
 614 0024 04          	.byte 0x4
 615 0025 02 00 00 00 	.4byte .LCFI0-.LFB0
 616 0029 13          	.byte 0x13
MPLAB XC16 ASSEMBLY Listing:   			page 13


 617 002a 7D          	.sleb128 -3
 618 002b 0D          	.byte 0xd
 619 002c 0E          	.uleb128 0xe
 620 002d 8E          	.byte 0x8e
 621 002e 02          	.uleb128 0x2
 622 002f 00          	.align 4
 623                 	.LEFDE0:
 624                 	.LSFDE2:
 625 0030 18 00 00 00 	.4byte .LEFDE2-.LASFDE2
 626                 	.LASFDE2:
 627 0034 00 00 00 00 	.4byte .Lframe0
 628 0038 00 00 00 00 	.4byte .LFB1
 629 003c 1E 00 00 00 	.4byte .LFE1-.LFB1
 630 0040 04          	.byte 0x4
 631 0041 02 00 00 00 	.4byte .LCFI1-.LFB1
 632 0045 13          	.byte 0x13
 633 0046 7D          	.sleb128 -3
 634 0047 0D          	.byte 0xd
 635 0048 0E          	.uleb128 0xe
 636 0049 8E          	.byte 0x8e
 637 004a 02          	.uleb128 0x2
 638 004b 00          	.align 4
 639                 	.LEFDE2:
 640                 	.LSFDE4:
 641 004c 18 00 00 00 	.4byte .LEFDE4-.LASFDE4
 642                 	.LASFDE4:
 643 0050 00 00 00 00 	.4byte .Lframe0
 644 0054 00 00 00 00 	.4byte .LFB2
 645 0058 48 00 00 00 	.4byte .LFE2-.LFB2
 646 005c 04          	.byte 0x4
 647 005d 02 00 00 00 	.4byte .LCFI2-.LFB2
 648 0061 13          	.byte 0x13
 649 0062 7D          	.sleb128 -3
 650 0063 0D          	.byte 0xd
 651 0064 0E          	.uleb128 0xe
 652 0065 8E          	.byte 0x8e
 653 0066 02          	.uleb128 0x2
 654 0067 00          	.align 4
 655                 	.LEFDE4:
 656                 	.section .text,code
 657              	.Letext0:
 658              	.file 2 "/opt/microchip/xc16/v1.36/bin/bin/../../include/lega-c/stdint.h"
 659              	.file 3 "lib/can-ids/CAN_IDs.h"
 660              	.file 4 "lib/can-ids/Devices/DCU_CAN.h"
 661              	.section .debug_info,info
 662 0000 4F 06 00 00 	.4byte 0x64f
 663 0004 02 00       	.2byte 0x2
 664 0006 00 00 00 00 	.4byte .Ldebug_abbrev0
 665 000a 04          	.byte 0x4
 666 000b 01          	.uleb128 0x1
 667 000c 47 4E 55 20 	.asciz "GNU C 4.5.1 (XC16, Microchip v1.36) (A) Build date: Dec  5 2018"
 667      43 20 34 2E 
 667      35 2E 31 20 
 667      28 58 43 31 
 667      36 2C 20 4D 
 667      69 63 72 6F 
 667      63 68 69 70 
MPLAB XC16 ASSEMBLY Listing:   			page 14


 667      20 76 31 2E 
 667      33 36 29 20 
 668 004c 01          	.byte 0x1
 669 004d 6C 69 62 2F 	.asciz "lib/can-ids/Devices/DCU_CAN.c"
 669      63 61 6E 2D 
 669      69 64 73 2F 
 669      44 65 76 69 
 669      63 65 73 2F 
 669      44 43 55 5F 
 669      43 41 4E 2E 
 669      63 00 
 670 006b 2F 68 6F 6D 	.asciz "/home/user/Documents/FST/Programming/project_sub-zero"
 670      65 2F 75 73 
 670      65 72 2F 44 
 670      6F 63 75 6D 
 670      65 6E 74 73 
 670      2F 46 53 54 
 670      2F 50 72 6F 
 670      67 72 61 6D 
 670      6D 69 6E 67 
 671 00a1 00 00 00 00 	.4byte .Ltext0
 672 00a5 00 00 00 00 	.4byte .Letext0
 673 00a9 00 00 00 00 	.4byte .Ldebug_line0
 674 00ad 02          	.uleb128 0x2
 675 00ae 01          	.byte 0x1
 676 00af 06          	.byte 0x6
 677 00b0 73 69 67 6E 	.asciz "signed char"
 677      65 64 20 63 
 677      68 61 72 00 
 678 00bc 02          	.uleb128 0x2
 679 00bd 02          	.byte 0x2
 680 00be 05          	.byte 0x5
 681 00bf 69 6E 74 00 	.asciz "int"
 682 00c3 02          	.uleb128 0x2
 683 00c4 04          	.byte 0x4
 684 00c5 05          	.byte 0x5
 685 00c6 6C 6F 6E 67 	.asciz "long int"
 685      20 69 6E 74 
 685      00 
 686 00cf 02          	.uleb128 0x2
 687 00d0 08          	.byte 0x8
 688 00d1 05          	.byte 0x5
 689 00d2 6C 6F 6E 67 	.asciz "long long int"
 689      20 6C 6F 6E 
 689      67 20 69 6E 
 689      74 00 
 690 00e0 03          	.uleb128 0x3
 691 00e1 75 69 6E 74 	.asciz "uint8_t"
 691      38 5F 74 00 
 692 00e9 02          	.byte 0x2
 693 00ea 2B          	.byte 0x2b
 694 00eb EF 00 00 00 	.4byte 0xef
 695 00ef 02          	.uleb128 0x2
 696 00f0 01          	.byte 0x1
 697 00f1 08          	.byte 0x8
 698 00f2 75 6E 73 69 	.asciz "unsigned char"
 698      67 6E 65 64 
MPLAB XC16 ASSEMBLY Listing:   			page 15


 698      20 63 68 61 
 698      72 00 
 699 0100 03          	.uleb128 0x3
 700 0101 75 69 6E 74 	.asciz "uint16_t"
 700      31 36 5F 74 
 700      00 
 701 010a 02          	.byte 0x2
 702 010b 31          	.byte 0x31
 703 010c 10 01 00 00 	.4byte 0x110
 704 0110 02          	.uleb128 0x2
 705 0111 02          	.byte 0x2
 706 0112 07          	.byte 0x7
 707 0113 75 6E 73 69 	.asciz "unsigned int"
 707      67 6E 65 64 
 707      20 69 6E 74 
 707      00 
 708 0120 02          	.uleb128 0x2
 709 0121 04          	.byte 0x4
 710 0122 07          	.byte 0x7
 711 0123 6C 6F 6E 67 	.asciz "long unsigned int"
 711      20 75 6E 73 
 711      69 67 6E 65 
 711      64 20 69 6E 
 711      74 00 
 712 0135 02          	.uleb128 0x2
 713 0136 08          	.byte 0x8
 714 0137 07          	.byte 0x7
 715 0138 6C 6F 6E 67 	.asciz "long long unsigned int"
 715      20 6C 6F 6E 
 715      67 20 75 6E 
 715      73 69 67 6E 
 715      65 64 20 69 
 715      6E 74 00 
 716 014f 04          	.uleb128 0x4
 717 0150 02          	.byte 0x2
 718 0151 03          	.byte 0x3
 719 0152 12          	.byte 0x12
 720 0153 80 01 00 00 	.4byte 0x180
 721 0157 05          	.uleb128 0x5
 722 0158 64 65 76 5F 	.asciz "dev_id"
 722      69 64 00 
 723 015f 03          	.byte 0x3
 724 0160 13          	.byte 0x13
 725 0161 00 01 00 00 	.4byte 0x100
 726 0165 02          	.byte 0x2
 727 0166 05          	.byte 0x5
 728 0167 0B          	.byte 0xb
 729 0168 02          	.byte 0x2
 730 0169 23          	.byte 0x23
 731 016a 00          	.uleb128 0x0
 732 016b 05          	.uleb128 0x5
 733 016c 6D 73 67 5F 	.asciz "msg_id"
 733      69 64 00 
 734 0173 03          	.byte 0x3
 735 0174 16          	.byte 0x16
 736 0175 00 01 00 00 	.4byte 0x100
 737 0179 02          	.byte 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 16


 738 017a 06          	.byte 0x6
 739 017b 05          	.byte 0x5
 740 017c 02          	.byte 0x2
 741 017d 23          	.byte 0x23
 742 017e 00          	.uleb128 0x0
 743 017f 00          	.byte 0x0
 744 0180 06          	.uleb128 0x6
 745 0181 02          	.byte 0x2
 746 0182 03          	.byte 0x3
 747 0183 11          	.byte 0x11
 748 0184 99 01 00 00 	.4byte 0x199
 749 0188 07          	.uleb128 0x7
 750 0189 4F 01 00 00 	.4byte 0x14f
 751 018d 08          	.uleb128 0x8
 752 018e 73 69 64 00 	.asciz "sid"
 753 0192 03          	.byte 0x3
 754 0193 18          	.byte 0x18
 755 0194 00 01 00 00 	.4byte 0x100
 756 0198 00          	.byte 0x0
 757 0199 04          	.uleb128 0x4
 758 019a 0C          	.byte 0xc
 759 019b 03          	.byte 0x3
 760 019c 10          	.byte 0x10
 761 019d CA 01 00 00 	.4byte 0x1ca
 762 01a1 09          	.uleb128 0x9
 763 01a2 80 01 00 00 	.4byte 0x180
 764 01a6 02          	.byte 0x2
 765 01a7 23          	.byte 0x23
 766 01a8 00          	.uleb128 0x0
 767 01a9 05          	.uleb128 0x5
 768 01aa 64 6C 63 00 	.asciz "dlc"
 769 01ae 03          	.byte 0x3
 770 01af 1A          	.byte 0x1a
 771 01b0 00 01 00 00 	.4byte 0x100
 772 01b4 02          	.byte 0x2
 773 01b5 04          	.byte 0x4
 774 01b6 0C          	.byte 0xc
 775 01b7 02          	.byte 0x2
 776 01b8 23          	.byte 0x23
 777 01b9 02          	.uleb128 0x2
 778 01ba 0A          	.uleb128 0xa
 779 01bb 64 61 74 61 	.asciz "data"
 779      00 
 780 01c0 03          	.byte 0x3
 781 01c1 1B          	.byte 0x1b
 782 01c2 CA 01 00 00 	.4byte 0x1ca
 783 01c6 02          	.byte 0x2
 784 01c7 23          	.byte 0x23
 785 01c8 04          	.uleb128 0x4
 786 01c9 00          	.byte 0x0
 787 01ca 0B          	.uleb128 0xb
 788 01cb 00 01 00 00 	.4byte 0x100
 789 01cf DA 01 00 00 	.4byte 0x1da
 790 01d3 0C          	.uleb128 0xc
 791 01d4 10 01 00 00 	.4byte 0x110
 792 01d8 03          	.byte 0x3
 793 01d9 00          	.byte 0x0
MPLAB XC16 ASSEMBLY Listing:   			page 17


 794 01da 03          	.uleb128 0x3
 795 01db 43 41 4E 64 	.asciz "CANdata"
 795      61 74 61 00 
 796 01e3 03          	.byte 0x3
 797 01e4 1C          	.byte 0x1c
 798 01e5 99 01 00 00 	.4byte 0x199
 799 01e9 04          	.uleb128 0x4
 800 01ea 02          	.byte 0x2
 801 01eb 04          	.byte 0x4
 802 01ec 16          	.byte 0x16
 803 01ed 47 03 00 00 	.4byte 0x347
 804 01f1 05          	.uleb128 0x5
 805 01f2 46 42 5F 42 	.asciz "FB_BL"
 805      4C 00 
 806 01f8 04          	.byte 0x4
 807 01f9 17          	.byte 0x17
 808 01fa 47 03 00 00 	.4byte 0x347
 809 01fe 01          	.byte 0x1
 810 01ff 01          	.byte 0x1
 811 0200 07          	.byte 0x7
 812 0201 02          	.byte 0x2
 813 0202 23          	.byte 0x23
 814 0203 00          	.uleb128 0x0
 815 0204 05          	.uleb128 0x5
 816 0205 46 42 5F 43 	.asciz "FB_CAN_E"
 816      41 4E 5F 45 
 816      00 
 817 020e 04          	.byte 0x4
 818 020f 18          	.byte 0x18
 819 0210 47 03 00 00 	.4byte 0x347
 820 0214 01          	.byte 0x1
 821 0215 01          	.byte 0x1
 822 0216 06          	.byte 0x6
 823 0217 02          	.byte 0x2
 824 0218 23          	.byte 0x23
 825 0219 00          	.uleb128 0x0
 826 021a 05          	.uleb128 0x5
 827 021b 46 42 5F 44 	.asciz "FB_DRS"
 827      52 53 00 
 828 0222 04          	.byte 0x4
 829 0223 19          	.byte 0x19
 830 0224 47 03 00 00 	.4byte 0x347
 831 0228 01          	.byte 0x1
 832 0229 01          	.byte 0x1
 833 022a 05          	.byte 0x5
 834 022b 02          	.byte 0x2
 835 022c 23          	.byte 0x23
 836 022d 00          	.uleb128 0x0
 837 022e 05          	.uleb128 0x5
 838 022f 46 42 5F 43 	.asciz "FB_CUB"
 838      55 42 00 
 839 0236 04          	.byte 0x4
 840 0237 1A          	.byte 0x1a
 841 0238 47 03 00 00 	.4byte 0x347
 842 023c 01          	.byte 0x1
 843 023d 01          	.byte 0x1
 844 023e 04          	.byte 0x4
MPLAB XC16 ASSEMBLY Listing:   			page 18


 845 023f 02          	.byte 0x2
 846 0240 23          	.byte 0x23
 847 0241 00          	.uleb128 0x0
 848 0242 05          	.uleb128 0x5
 849 0243 46 42 5F 66 	.asciz "FB_fans"
 849      61 6E 73 00 
 850 024b 04          	.byte 0x4
 851 024c 1B          	.byte 0x1b
 852 024d 47 03 00 00 	.4byte 0x347
 853 0251 01          	.byte 0x1
 854 0252 01          	.byte 0x1
 855 0253 03          	.byte 0x3
 856 0254 02          	.byte 0x2
 857 0255 23          	.byte 0x23
 858 0256 00          	.uleb128 0x0
 859 0257 05          	.uleb128 0x5
 860 0258 46 42 5F 70 	.asciz "FB_pumps"
 860      75 6D 70 73 
 860      00 
 861 0261 04          	.byte 0x4
 862 0262 1C          	.byte 0x1c
 863 0263 47 03 00 00 	.4byte 0x347
 864 0267 01          	.byte 0x1
 865 0268 01          	.byte 0x1
 866 0269 02          	.byte 0x2
 867 026a 02          	.byte 0x2
 868 026b 23          	.byte 0x23
 869 026c 00          	.uleb128 0x0
 870 026d 05          	.uleb128 0x5
 871 026e 46 42 5F 45 	.asciz "FB_EM"
 871      4D 00 
 872 0274 04          	.byte 0x4
 873 0275 1D          	.byte 0x1d
 874 0276 47 03 00 00 	.4byte 0x347
 875 027a 01          	.byte 0x1
 876 027b 01          	.byte 0x1
 877 027c 01          	.byte 0x1
 878 027d 02          	.byte 0x2
 879 027e 23          	.byte 0x23
 880 027f 00          	.uleb128 0x0
 881 0280 05          	.uleb128 0x5
 882 0281 46 42 5F 53 	.asciz "FB_SC_origin"
 882      43 5F 6F 72 
 882      69 67 69 6E 
 882      00 
 883 028e 04          	.byte 0x4
 884 028f 1E          	.byte 0x1e
 885 0290 47 03 00 00 	.4byte 0x347
 886 0294 01          	.byte 0x1
 887 0295 01          	.byte 0x1
 888 0296 08          	.byte 0x8
 889 0297 02          	.byte 0x2
 890 0298 23          	.byte 0x23
 891 0299 00          	.uleb128 0x0
 892 029a 05          	.uleb128 0x5
 893 029b 46 42 5F 43 	.asciz "FB_CUA"
 893      55 41 00 
MPLAB XC16 ASSEMBLY Listing:   			page 19


 894 02a2 04          	.byte 0x4
 895 02a3 1F          	.byte 0x1f
 896 02a4 47 03 00 00 	.4byte 0x347
 897 02a8 01          	.byte 0x1
 898 02a9 01          	.byte 0x1
 899 02aa 07          	.byte 0x7
 900 02ab 02          	.byte 0x2
 901 02ac 23          	.byte 0x23
 902 02ad 01          	.uleb128 0x1
 903 02ae 05          	.uleb128 0x5
 904 02af 46 42 5F 54 	.asciz "FB_TSAL"
 904      53 41 4C 00 
 905 02b7 04          	.byte 0x4
 906 02b8 20          	.byte 0x20
 907 02b9 47 03 00 00 	.4byte 0x347
 908 02bd 01          	.byte 0x1
 909 02be 01          	.byte 0x1
 910 02bf 06          	.byte 0x6
 911 02c0 02          	.byte 0x2
 912 02c1 23          	.byte 0x23
 913 02c2 01          	.uleb128 0x1
 914 02c3 05          	.uleb128 0x5
 915 02c4 46 42 5F 66 	.asciz "FB_fans_AMS"
 915      61 6E 73 5F 
 915      41 4D 53 00 
 916 02d0 04          	.byte 0x4
 917 02d1 21          	.byte 0x21
 918 02d2 47 03 00 00 	.4byte 0x347
 919 02d6 01          	.byte 0x1
 920 02d7 01          	.byte 0x1
 921 02d8 05          	.byte 0x5
 922 02d9 02          	.byte 0x2
 923 02da 23          	.byte 0x23
 924 02db 01          	.uleb128 0x1
 925 02dc 05          	.uleb128 0x5
 926 02dd 46 42 5F 41 	.asciz "FB_AMS"
 926      4D 53 00 
 927 02e4 04          	.byte 0x4
 928 02e5 22          	.byte 0x22
 929 02e6 47 03 00 00 	.4byte 0x347
 930 02ea 01          	.byte 0x1
 931 02eb 01          	.byte 0x1
 932 02ec 04          	.byte 0x4
 933 02ed 02          	.byte 0x2
 934 02ee 23          	.byte 0x23
 935 02ef 01          	.uleb128 0x1
 936 02f0 05          	.uleb128 0x5
 937 02f1 53 43 5F 42 	.asciz "SC_BSPD_to_bat"
 937      53 50 44 5F 
 937      74 6F 5F 62 
 937      61 74 00 
 938 0300 04          	.byte 0x4
 939 0301 24          	.byte 0x24
 940 0302 47 03 00 00 	.4byte 0x347
 941 0306 01          	.byte 0x1
 942 0307 01          	.byte 0x1
 943 0308 03          	.byte 0x3
MPLAB XC16 ASSEMBLY Listing:   			page 20


 944 0309 02          	.byte 0x2
 945 030a 23          	.byte 0x23
 946 030b 01          	.uleb128 0x1
 947 030c 05          	.uleb128 0x5
 948 030d 53 43 5F 4D 	.asciz "SC_MH_to_front"
 948      48 5F 74 6F 
 948      5F 66 72 6F 
 948      6E 74 00 
 949 031c 04          	.byte 0x4
 950 031d 25          	.byte 0x25
 951 031e 47 03 00 00 	.4byte 0x347
 952 0322 01          	.byte 0x1
 953 0323 01          	.byte 0x1
 954 0324 02          	.byte 0x2
 955 0325 02          	.byte 0x2
 956 0326 23          	.byte 0x23
 957 0327 01          	.uleb128 0x1
 958 0328 05          	.uleb128 0x5
 959 0329 53 43 5F 66 	.asciz "SC_front_to_BSPD"
 959      72 6F 6E 74 
 959      5F 74 6F 5F 
 959      42 53 50 44 
 959      00 
 960 033a 04          	.byte 0x4
 961 033b 26          	.byte 0x26
 962 033c 47 03 00 00 	.4byte 0x347
 963 0340 01          	.byte 0x1
 964 0341 01          	.byte 0x1
 965 0342 01          	.byte 0x1
 966 0343 02          	.byte 0x2
 967 0344 23          	.byte 0x23
 968 0345 01          	.uleb128 0x1
 969 0346 00          	.byte 0x0
 970 0347 02          	.uleb128 0x2
 971 0348 01          	.byte 0x1
 972 0349 02          	.byte 0x2
 973 034a 5F 42 6F 6F 	.asciz "_Bool"
 973      6C 00 
 974 0350 06          	.uleb128 0x6
 975 0351 02          	.byte 0x2
 976 0352 04          	.byte 0x4
 977 0353 15          	.byte 0x15
 978 0354 72 03 00 00 	.4byte 0x372
 979 0358 07          	.uleb128 0x7
 980 0359 E9 01 00 00 	.4byte 0x1e9
 981 035d 08          	.uleb128 0x8
 982 035e 46 42 5F 53 	.asciz "FB_SC_Detect"
 982      43 5F 44 65 
 982      74 65 63 74 
 982      00 
 983 036b 04          	.byte 0x4
 984 036c 28          	.byte 0x28
 985 036d 00 01 00 00 	.4byte 0x100
 986 0371 00          	.byte 0x0
 987 0372 04          	.uleb128 0x4
 988 0373 02          	.byte 0x2
 989 0374 04          	.byte 0x4
MPLAB XC16 ASSEMBLY Listing:   			page 21


 990 0375 2D          	.byte 0x2d
 991 0376 5F 04 00 00 	.4byte 0x45f
 992 037a 05          	.uleb128 0x5
 993 037b 43 55 41 5F 	.asciz "CUA_sig"
 993      73 69 67 00 
 994 0383 04          	.byte 0x4
 995 0384 2E          	.byte 0x2e
 996 0385 47 03 00 00 	.4byte 0x347
 997 0389 01          	.byte 0x1
 998 038a 01          	.byte 0x1
 999 038b 07          	.byte 0x7
 1000 038c 02          	.byte 0x2
 1001 038d 23          	.byte 0x23
 1002 038e 00          	.uleb128 0x0
 1003 038f 05          	.uleb128 0x5
 1004 0390 43 55 42 5F 	.asciz "CUB_sig"
 1004      73 69 67 00 
 1005 0398 04          	.byte 0x4
 1006 0399 2F          	.byte 0x2f
 1007 039a 47 03 00 00 	.4byte 0x347
 1008 039e 01          	.byte 0x1
 1009 039f 01          	.byte 0x1
 1010 03a0 06          	.byte 0x6
 1011 03a1 02          	.byte 0x2
 1012 03a2 23          	.byte 0x23
 1013 03a3 00          	.uleb128 0x0
 1014 03a4 05          	.uleb128 0x5
 1015 03a5 66 61 6E 5F 	.asciz "fan_sig"
 1015      73 69 67 00 
 1016 03ad 04          	.byte 0x4
 1017 03ae 30          	.byte 0x30
 1018 03af 47 03 00 00 	.4byte 0x347
 1019 03b3 01          	.byte 0x1
 1020 03b4 01          	.byte 0x1
 1021 03b5 05          	.byte 0x5
 1022 03b6 02          	.byte 0x2
 1023 03b7 23          	.byte 0x23
 1024 03b8 00          	.uleb128 0x0
 1025 03b9 05          	.uleb128 0x5
 1026 03ba 53 43 5F 73 	.asciz "SC_switch_sig"
 1026      77 69 74 63 
 1026      68 5F 73 69 
 1026      67 00 
 1027 03c8 04          	.byte 0x4
 1028 03c9 31          	.byte 0x31
 1029 03ca 47 03 00 00 	.4byte 0x347
 1030 03ce 01          	.byte 0x1
 1031 03cf 01          	.byte 0x1
 1032 03d0 04          	.byte 0x4
 1033 03d1 02          	.byte 0x2
 1034 03d2 23          	.byte 0x23
 1035 03d3 00          	.uleb128 0x0
 1036 03d4 05          	.uleb128 0x5
 1037 03d5 70 75 6D 70 	.asciz "pump1_sig"
 1037      31 5F 73 69 
 1037      67 00 
 1038 03df 04          	.byte 0x4
MPLAB XC16 ASSEMBLY Listing:   			page 22


 1039 03e0 32          	.byte 0x32
 1040 03e1 47 03 00 00 	.4byte 0x347
 1041 03e5 01          	.byte 0x1
 1042 03e6 01          	.byte 0x1
 1043 03e7 03          	.byte 0x3
 1044 03e8 02          	.byte 0x2
 1045 03e9 23          	.byte 0x23
 1046 03ea 00          	.uleb128 0x0
 1047 03eb 05          	.uleb128 0x5
 1048 03ec 70 75 6D 70 	.asciz "pump2_sig"
 1048      32 5F 73 69 
 1048      67 00 
 1049 03f6 04          	.byte 0x4
 1050 03f7 33          	.byte 0x33
 1051 03f8 47 03 00 00 	.4byte 0x347
 1052 03fc 01          	.byte 0x1
 1053 03fd 01          	.byte 0x1
 1054 03fe 02          	.byte 0x2
 1055 03ff 02          	.byte 0x2
 1056 0400 23          	.byte 0x23
 1057 0401 00          	.uleb128 0x0
 1058 0402 05          	.uleb128 0x5
 1059 0403 64 63 64 63 	.asciz "dcdc_switch_sig"
 1059      5F 73 77 69 
 1059      74 63 68 5F 
 1059      73 69 67 00 
 1060 0413 04          	.byte 0x4
 1061 0414 34          	.byte 0x34
 1062 0415 47 03 00 00 	.4byte 0x347
 1063 0419 01          	.byte 0x1
 1064 041a 01          	.byte 0x1
 1065 041b 01          	.byte 0x1
 1066 041c 02          	.byte 0x2
 1067 041d 23          	.byte 0x23
 1068 041e 00          	.uleb128 0x0
 1069 041f 05          	.uleb128 0x5
 1070 0420 42 4C 5F 73 	.asciz "BL_sig"
 1070      69 67 00 
 1071 0427 04          	.byte 0x4
 1072 0428 35          	.byte 0x35
 1073 0429 47 03 00 00 	.4byte 0x347
 1074 042d 01          	.byte 0x1
 1075 042e 01          	.byte 0x1
 1076 042f 08          	.byte 0x8
 1077 0430 02          	.byte 0x2
 1078 0431 23          	.byte 0x23
 1079 0432 00          	.uleb128 0x0
 1080 0433 05          	.uleb128 0x5
 1081 0434 62 75 7A 7A 	.asciz "buzz_sig"
 1081      5F 73 69 67 
 1081      00 
 1082 043d 04          	.byte 0x4
 1083 043e 36          	.byte 0x36
 1084 043f 47 03 00 00 	.4byte 0x347
 1085 0443 01          	.byte 0x1
 1086 0444 01          	.byte 0x1
 1087 0445 07          	.byte 0x7
MPLAB XC16 ASSEMBLY Listing:   			page 23


 1088 0446 02          	.byte 0x2
 1089 0447 23          	.byte 0x23
 1090 0448 01          	.uleb128 0x1
 1091 0449 05          	.uleb128 0x5
 1092 044a 44 52 53 5F 	.asciz "DRS_PWM"
 1092      50 57 4D 00 
 1093 0452 04          	.byte 0x4
 1094 0453 37          	.byte 0x37
 1095 0454 E0 00 00 00 	.4byte 0xe0
 1096 0458 01          	.byte 0x1
 1097 0459 07          	.byte 0x7
 1098 045a 08          	.byte 0x8
 1099 045b 02          	.byte 0x2
 1100 045c 23          	.byte 0x23
 1101 045d 01          	.uleb128 0x1
 1102 045e 00          	.byte 0x0
 1103 045f 06          	.uleb128 0x6
 1104 0460 02          	.byte 0x2
 1105 0461 04          	.byte 0x4
 1106 0462 2C          	.byte 0x2c
 1107 0463 7C 04 00 00 	.4byte 0x47c
 1108 0467 07          	.uleb128 0x7
 1109 0468 72 03 00 00 	.4byte 0x372
 1110 046c 08          	.uleb128 0x8
 1111 046d 53 69 67 6E 	.asciz "Signals"
 1111      61 6C 73 00 
 1112 0475 04          	.byte 0x4
 1113 0476 39          	.byte 0x39
 1114 0477 00 01 00 00 	.4byte 0x100
 1115 047b 00          	.byte 0x0
 1116 047c 04          	.uleb128 0x4
 1117 047d 08          	.byte 0x8
 1118 047e 04          	.byte 0x4
 1119 047f 13          	.byte 0x13
 1120 0480 BB 04 00 00 	.4byte 0x4bb
 1121 0484 09          	.uleb128 0x9
 1122 0485 50 03 00 00 	.4byte 0x350
 1123 0489 02          	.byte 0x2
 1124 048a 23          	.byte 0x23
 1125 048b 00          	.uleb128 0x0
 1126 048c 09          	.uleb128 0x9
 1127 048d 5F 04 00 00 	.4byte 0x45f
 1128 0491 02          	.byte 0x2
 1129 0492 23          	.byte 0x23
 1130 0493 02          	.uleb128 0x2
 1131 0494 0A          	.uleb128 0xa
 1132 0495 54 45 4D 50 	.asciz "TEMP_PIC"
 1132      5F 50 49 43 
 1132      00 
 1133 049e 04          	.byte 0x4
 1134 049f 3D          	.byte 0x3d
 1135 04a0 00 01 00 00 	.4byte 0x100
 1136 04a4 02          	.byte 0x2
 1137 04a5 23          	.byte 0x23
 1138 04a6 04          	.uleb128 0x4
 1139 04a7 0A          	.uleb128 0xa
 1140 04a8 54 45 4D 50 	.asciz "TEMP_BOX"
MPLAB XC16 ASSEMBLY Listing:   			page 24


 1140      5F 42 4F 58 
 1140      00 
 1141 04b1 04          	.byte 0x4
 1142 04b2 40          	.byte 0x40
 1143 04b3 00 01 00 00 	.4byte 0x100
 1144 04b7 02          	.byte 0x2
 1145 04b8 23          	.byte 0x23
 1146 04b9 06          	.uleb128 0x6
 1147 04ba 00          	.byte 0x0
 1148 04bb 03          	.uleb128 0x3
 1149 04bc 44 43 55 5F 	.asciz "DCU_MSG_Status"
 1149      4D 53 47 5F 
 1149      53 74 61 74 
 1149      75 73 00 
 1150 04cb 04          	.byte 0x4
 1151 04cc 41          	.byte 0x41
 1152 04cd 7C 04 00 00 	.4byte 0x47c
 1153 04d1 04          	.uleb128 0x4
 1154 04d2 04          	.byte 0x4
 1155 04d3 04          	.byte 0x4
 1156 04d4 44          	.byte 0x44
 1157 04d5 04 05 00 00 	.4byte 0x504
 1158 04d9 0A          	.uleb128 0xa
 1159 04da 4C 56 5F 63 	.asciz "LV_current"
 1159      75 72 72 65 
 1159      6E 74 00 
 1160 04e5 04          	.byte 0x4
 1161 04e6 46          	.byte 0x46
 1162 04e7 00 01 00 00 	.4byte 0x100
 1163 04eb 02          	.byte 0x2
 1164 04ec 23          	.byte 0x23
 1165 04ed 00          	.uleb128 0x0
 1166 04ee 0A          	.uleb128 0xa
 1167 04ef 48 56 5F 63 	.asciz "HV_current"
 1167      75 72 72 65 
 1167      6E 74 00 
 1168 04fa 04          	.byte 0x4
 1169 04fb 49          	.byte 0x49
 1170 04fc 00 01 00 00 	.4byte 0x100
 1171 0500 02          	.byte 0x2
 1172 0501 23          	.byte 0x23
 1173 0502 02          	.uleb128 0x2
 1174 0503 00          	.byte 0x0
 1175 0504 03          	.uleb128 0x3
 1176 0505 44 43 55 5F 	.asciz "DCU_MSG_Current"
 1176      4D 53 47 5F 
 1176      43 75 72 72 
 1176      65 6E 74 00 
 1177 0515 04          	.byte 0x4
 1178 0516 4A          	.byte 0x4a
 1179 0517 D1 04 00 00 	.4byte 0x4d1
 1180 051b 04          	.uleb128 0x4
 1181 051c 0C          	.byte 0xc
 1182 051d 04          	.byte 0x4
 1183 051e 4D          	.byte 0x4d
 1184 051f 47 05 00 00 	.4byte 0x547
 1185 0523 0A          	.uleb128 0xa
MPLAB XC16 ASSEMBLY Listing:   			page 25


 1186 0524 73 74 61 74 	.asciz "status"
 1186      75 73 00 
 1187 052b 04          	.byte 0x4
 1188 052c 4E          	.byte 0x4e
 1189 052d BB 04 00 00 	.4byte 0x4bb
 1190 0531 02          	.byte 0x2
 1191 0532 23          	.byte 0x23
 1192 0533 00          	.uleb128 0x0
 1193 0534 0A          	.uleb128 0xa
 1194 0535 63 75 72 72 	.asciz "current"
 1194      65 6E 74 00 
 1195 053d 04          	.byte 0x4
 1196 053e 4F          	.byte 0x4f
 1197 053f 04 05 00 00 	.4byte 0x504
 1198 0543 02          	.byte 0x2
 1199 0544 23          	.byte 0x23
 1200 0545 08          	.uleb128 0x8
 1201 0546 00          	.byte 0x0
 1202 0547 03          	.uleb128 0x3
 1203 0548 44 43 55 5F 	.asciz "DCU_CAN_Data"
 1203      43 41 4E 5F 
 1203      44 61 74 61 
 1203      00 
 1204 0555 04          	.byte 0x4
 1205 0556 50          	.byte 0x50
 1206 0557 1B 05 00 00 	.4byte 0x51b
 1207 055b 0D          	.uleb128 0xd
 1208 055c 01          	.byte 0x1
 1209 055d 70 61 72 73 	.asciz "parse_dcu_message_status"
 1209      65 5F 64 63 
 1209      75 5F 6D 65 
 1209      73 73 61 67 
 1209      65 5F 73 74 
 1209      61 74 75 73 
 1209      00 
 1210 0576 01          	.byte 0x1
 1211 0577 06          	.byte 0x6
 1212 0578 01          	.byte 0x1
 1213 0579 00 00 00 00 	.4byte .LFB0
 1214 057d 00 00 00 00 	.4byte .LFE0
 1215 0581 01          	.byte 0x1
 1216 0582 5E          	.byte 0x5e
 1217 0583 A8 05 00 00 	.4byte 0x5a8
 1218 0587 0E          	.uleb128 0xe
 1219 0588 64 61 74 61 	.asciz "data"
 1219      00 
 1220 058d 01          	.byte 0x1
 1221 058e 06          	.byte 0x6
 1222 058f A8 05 00 00 	.4byte 0x5a8
 1223 0593 02          	.byte 0x2
 1224 0594 7E          	.byte 0x7e
 1225 0595 00          	.sleb128 0
 1226 0596 0E          	.uleb128 0xe
 1227 0597 73 74 61 74 	.asciz "status"
 1227      75 73 00 
 1228 059e 01          	.byte 0x1
 1229 059f 06          	.byte 0x6
MPLAB XC16 ASSEMBLY Listing:   			page 26


 1230 05a0 AE 05 00 00 	.4byte 0x5ae
 1231 05a4 02          	.byte 0x2
 1232 05a5 7E          	.byte 0x7e
 1233 05a6 02          	.sleb128 2
 1234 05a7 00          	.byte 0x0
 1235 05a8 0F          	.uleb128 0xf
 1236 05a9 02          	.byte 0x2
 1237 05aa 00 01 00 00 	.4byte 0x100
 1238 05ae 0F          	.uleb128 0xf
 1239 05af 02          	.byte 0x2
 1240 05b0 BB 04 00 00 	.4byte 0x4bb
 1241 05b4 0D          	.uleb128 0xd
 1242 05b5 01          	.byte 0x1
 1243 05b6 70 61 72 73 	.asciz "parse_dcu_message_current"
 1243      65 5F 64 63 
 1243      75 5F 6D 65 
 1243      73 73 61 67 
 1243      65 5F 63 75 
 1243      72 72 65 6E 
 1243      74 00 
 1244 05d0 01          	.byte 0x1
 1245 05d1 27          	.byte 0x27
 1246 05d2 01          	.byte 0x1
 1247 05d3 00 00 00 00 	.4byte .LFB1
 1248 05d7 00 00 00 00 	.4byte .LFE1
 1249 05db 01          	.byte 0x1
 1250 05dc 5E          	.byte 0x5e
 1251 05dd 03 06 00 00 	.4byte 0x603
 1252 05e1 0E          	.uleb128 0xe
 1253 05e2 64 61 74 61 	.asciz "data"
 1253      00 
 1254 05e7 01          	.byte 0x1
 1255 05e8 27          	.byte 0x27
 1256 05e9 A8 05 00 00 	.4byte 0x5a8
 1257 05ed 02          	.byte 0x2
 1258 05ee 7E          	.byte 0x7e
 1259 05ef 00          	.sleb128 0
 1260 05f0 0E          	.uleb128 0xe
 1261 05f1 63 75 72 72 	.asciz "current"
 1261      65 6E 74 00 
 1262 05f9 01          	.byte 0x1
 1263 05fa 27          	.byte 0x27
 1264 05fb 03 06 00 00 	.4byte 0x603
 1265 05ff 02          	.byte 0x2
 1266 0600 7E          	.byte 0x7e
 1267 0601 02          	.sleb128 2
 1268 0602 00          	.byte 0x0
 1269 0603 0F          	.uleb128 0xf
 1270 0604 02          	.byte 0x2
 1271 0605 04 05 00 00 	.4byte 0x504
 1272 0609 0D          	.uleb128 0xd
 1273 060a 01          	.byte 0x1
 1274 060b 70 61 72 73 	.asciz "parse_can_dcu"
 1274      65 5F 63 61 
 1274      6E 5F 64 63 
 1274      75 00 
 1275 0619 01          	.byte 0x1
MPLAB XC16 ASSEMBLY Listing:   			page 27


 1276 061a 2D          	.byte 0x2d
 1277 061b 01          	.byte 0x1
 1278 061c 00 00 00 00 	.4byte .LFB2
 1279 0620 00 00 00 00 	.4byte .LFE2
 1280 0624 01          	.byte 0x1
 1281 0625 5E          	.byte 0x5e
 1282 0626 4C 06 00 00 	.4byte 0x64c
 1283 062a 0E          	.uleb128 0xe
 1284 062b 6D 65 73 73 	.asciz "message"
 1284      61 67 65 00 
 1285 0633 01          	.byte 0x1
 1286 0634 2D          	.byte 0x2d
 1287 0635 DA 01 00 00 	.4byte 0x1da
 1288 0639 02          	.byte 0x2
 1289 063a 7E          	.byte 0x7e
 1290 063b 00          	.sleb128 0
 1291 063c 0E          	.uleb128 0xe
 1292 063d 64 61 74 61 	.asciz "data"
 1292      00 
 1293 0642 01          	.byte 0x1
 1294 0643 2D          	.byte 0x2d
 1295 0644 4C 06 00 00 	.4byte 0x64c
 1296 0648 02          	.byte 0x2
 1297 0649 7E          	.byte 0x7e
 1298 064a 0C          	.sleb128 12
 1299 064b 00          	.byte 0x0
 1300 064c 0F          	.uleb128 0xf
 1301 064d 02          	.byte 0x2
 1302 064e 47 05 00 00 	.4byte 0x547
 1303 0652 00          	.byte 0x0
 1304                 	.section .debug_abbrev,info
 1305 0000 01          	.uleb128 0x1
 1306 0001 11          	.uleb128 0x11
 1307 0002 01          	.byte 0x1
 1308 0003 25          	.uleb128 0x25
 1309 0004 08          	.uleb128 0x8
 1310 0005 13          	.uleb128 0x13
 1311 0006 0B          	.uleb128 0xb
 1312 0007 03          	.uleb128 0x3
 1313 0008 08          	.uleb128 0x8
 1314 0009 1B          	.uleb128 0x1b
 1315 000a 08          	.uleb128 0x8
 1316 000b 11          	.uleb128 0x11
 1317 000c 01          	.uleb128 0x1
 1318 000d 12          	.uleb128 0x12
 1319 000e 01          	.uleb128 0x1
 1320 000f 10          	.uleb128 0x10
 1321 0010 06          	.uleb128 0x6
 1322 0011 00          	.byte 0x0
 1323 0012 00          	.byte 0x0
 1324 0013 02          	.uleb128 0x2
 1325 0014 24          	.uleb128 0x24
 1326 0015 00          	.byte 0x0
 1327 0016 0B          	.uleb128 0xb
 1328 0017 0B          	.uleb128 0xb
 1329 0018 3E          	.uleb128 0x3e
 1330 0019 0B          	.uleb128 0xb
MPLAB XC16 ASSEMBLY Listing:   			page 28


 1331 001a 03          	.uleb128 0x3
 1332 001b 08          	.uleb128 0x8
 1333 001c 00          	.byte 0x0
 1334 001d 00          	.byte 0x0
 1335 001e 03          	.uleb128 0x3
 1336 001f 16          	.uleb128 0x16
 1337 0020 00          	.byte 0x0
 1338 0021 03          	.uleb128 0x3
 1339 0022 08          	.uleb128 0x8
 1340 0023 3A          	.uleb128 0x3a
 1341 0024 0B          	.uleb128 0xb
 1342 0025 3B          	.uleb128 0x3b
 1343 0026 0B          	.uleb128 0xb
 1344 0027 49          	.uleb128 0x49
 1345 0028 13          	.uleb128 0x13
 1346 0029 00          	.byte 0x0
 1347 002a 00          	.byte 0x0
 1348 002b 04          	.uleb128 0x4
 1349 002c 13          	.uleb128 0x13
 1350 002d 01          	.byte 0x1
 1351 002e 0B          	.uleb128 0xb
 1352 002f 0B          	.uleb128 0xb
 1353 0030 3A          	.uleb128 0x3a
 1354 0031 0B          	.uleb128 0xb
 1355 0032 3B          	.uleb128 0x3b
 1356 0033 0B          	.uleb128 0xb
 1357 0034 01          	.uleb128 0x1
 1358 0035 13          	.uleb128 0x13
 1359 0036 00          	.byte 0x0
 1360 0037 00          	.byte 0x0
 1361 0038 05          	.uleb128 0x5
 1362 0039 0D          	.uleb128 0xd
 1363 003a 00          	.byte 0x0
 1364 003b 03          	.uleb128 0x3
 1365 003c 08          	.uleb128 0x8
 1366 003d 3A          	.uleb128 0x3a
 1367 003e 0B          	.uleb128 0xb
 1368 003f 3B          	.uleb128 0x3b
 1369 0040 0B          	.uleb128 0xb
 1370 0041 49          	.uleb128 0x49
 1371 0042 13          	.uleb128 0x13
 1372 0043 0B          	.uleb128 0xb
 1373 0044 0B          	.uleb128 0xb
 1374 0045 0D          	.uleb128 0xd
 1375 0046 0B          	.uleb128 0xb
 1376 0047 0C          	.uleb128 0xc
 1377 0048 0B          	.uleb128 0xb
 1378 0049 38          	.uleb128 0x38
 1379 004a 0A          	.uleb128 0xa
 1380 004b 00          	.byte 0x0
 1381 004c 00          	.byte 0x0
 1382 004d 06          	.uleb128 0x6
 1383 004e 17          	.uleb128 0x17
 1384 004f 01          	.byte 0x1
 1385 0050 0B          	.uleb128 0xb
 1386 0051 0B          	.uleb128 0xb
 1387 0052 3A          	.uleb128 0x3a
MPLAB XC16 ASSEMBLY Listing:   			page 29


 1388 0053 0B          	.uleb128 0xb
 1389 0054 3B          	.uleb128 0x3b
 1390 0055 0B          	.uleb128 0xb
 1391 0056 01          	.uleb128 0x1
 1392 0057 13          	.uleb128 0x13
 1393 0058 00          	.byte 0x0
 1394 0059 00          	.byte 0x0
 1395 005a 07          	.uleb128 0x7
 1396 005b 0D          	.uleb128 0xd
 1397 005c 00          	.byte 0x0
 1398 005d 49          	.uleb128 0x49
 1399 005e 13          	.uleb128 0x13
 1400 005f 00          	.byte 0x0
 1401 0060 00          	.byte 0x0
 1402 0061 08          	.uleb128 0x8
 1403 0062 0D          	.uleb128 0xd
 1404 0063 00          	.byte 0x0
 1405 0064 03          	.uleb128 0x3
 1406 0065 08          	.uleb128 0x8
 1407 0066 3A          	.uleb128 0x3a
 1408 0067 0B          	.uleb128 0xb
 1409 0068 3B          	.uleb128 0x3b
 1410 0069 0B          	.uleb128 0xb
 1411 006a 49          	.uleb128 0x49
 1412 006b 13          	.uleb128 0x13
 1413 006c 00          	.byte 0x0
 1414 006d 00          	.byte 0x0
 1415 006e 09          	.uleb128 0x9
 1416 006f 0D          	.uleb128 0xd
 1417 0070 00          	.byte 0x0
 1418 0071 49          	.uleb128 0x49
 1419 0072 13          	.uleb128 0x13
 1420 0073 38          	.uleb128 0x38
 1421 0074 0A          	.uleb128 0xa
 1422 0075 00          	.byte 0x0
 1423 0076 00          	.byte 0x0
 1424 0077 0A          	.uleb128 0xa
 1425 0078 0D          	.uleb128 0xd
 1426 0079 00          	.byte 0x0
 1427 007a 03          	.uleb128 0x3
 1428 007b 08          	.uleb128 0x8
 1429 007c 3A          	.uleb128 0x3a
 1430 007d 0B          	.uleb128 0xb
 1431 007e 3B          	.uleb128 0x3b
 1432 007f 0B          	.uleb128 0xb
 1433 0080 49          	.uleb128 0x49
 1434 0081 13          	.uleb128 0x13
 1435 0082 38          	.uleb128 0x38
 1436 0083 0A          	.uleb128 0xa
 1437 0084 00          	.byte 0x0
 1438 0085 00          	.byte 0x0
 1439 0086 0B          	.uleb128 0xb
 1440 0087 01          	.uleb128 0x1
 1441 0088 01          	.byte 0x1
 1442 0089 49          	.uleb128 0x49
 1443 008a 13          	.uleb128 0x13
 1444 008b 01          	.uleb128 0x1
MPLAB XC16 ASSEMBLY Listing:   			page 30


 1445 008c 13          	.uleb128 0x13
 1446 008d 00          	.byte 0x0
 1447 008e 00          	.byte 0x0
 1448 008f 0C          	.uleb128 0xc
 1449 0090 21          	.uleb128 0x21
 1450 0091 00          	.byte 0x0
 1451 0092 49          	.uleb128 0x49
 1452 0093 13          	.uleb128 0x13
 1453 0094 2F          	.uleb128 0x2f
 1454 0095 0B          	.uleb128 0xb
 1455 0096 00          	.byte 0x0
 1456 0097 00          	.byte 0x0
 1457 0098 0D          	.uleb128 0xd
 1458 0099 2E          	.uleb128 0x2e
 1459 009a 01          	.byte 0x1
 1460 009b 3F          	.uleb128 0x3f
 1461 009c 0C          	.uleb128 0xc
 1462 009d 03          	.uleb128 0x3
 1463 009e 08          	.uleb128 0x8
 1464 009f 3A          	.uleb128 0x3a
 1465 00a0 0B          	.uleb128 0xb
 1466 00a1 3B          	.uleb128 0x3b
 1467 00a2 0B          	.uleb128 0xb
 1468 00a3 27          	.uleb128 0x27
 1469 00a4 0C          	.uleb128 0xc
 1470 00a5 11          	.uleb128 0x11
 1471 00a6 01          	.uleb128 0x1
 1472 00a7 12          	.uleb128 0x12
 1473 00a8 01          	.uleb128 0x1
 1474 00a9 40          	.uleb128 0x40
 1475 00aa 0A          	.uleb128 0xa
 1476 00ab 01          	.uleb128 0x1
 1477 00ac 13          	.uleb128 0x13
 1478 00ad 00          	.byte 0x0
 1479 00ae 00          	.byte 0x0
 1480 00af 0E          	.uleb128 0xe
 1481 00b0 05          	.uleb128 0x5
 1482 00b1 00          	.byte 0x0
 1483 00b2 03          	.uleb128 0x3
 1484 00b3 08          	.uleb128 0x8
 1485 00b4 3A          	.uleb128 0x3a
 1486 00b5 0B          	.uleb128 0xb
 1487 00b6 3B          	.uleb128 0x3b
 1488 00b7 0B          	.uleb128 0xb
 1489 00b8 49          	.uleb128 0x49
 1490 00b9 13          	.uleb128 0x13
 1491 00ba 02          	.uleb128 0x2
 1492 00bb 0A          	.uleb128 0xa
 1493 00bc 00          	.byte 0x0
 1494 00bd 00          	.byte 0x0
 1495 00be 0F          	.uleb128 0xf
 1496 00bf 0F          	.uleb128 0xf
 1497 00c0 00          	.byte 0x0
 1498 00c1 0B          	.uleb128 0xb
 1499 00c2 0B          	.uleb128 0xb
 1500 00c3 49          	.uleb128 0x49
 1501 00c4 13          	.uleb128 0x13
MPLAB XC16 ASSEMBLY Listing:   			page 31


 1502 00c5 00          	.byte 0x0
 1503 00c6 00          	.byte 0x0
 1504 00c7 00          	.byte 0x0
 1505                 	.section .debug_pubnames,info
 1506 0000 5B 00 00 00 	.4byte 0x5b
 1507 0004 02 00       	.2byte 0x2
 1508 0006 00 00 00 00 	.4byte .Ldebug_info0
 1509 000a 53 06 00 00 	.4byte 0x653
 1510 000e 5B 05 00 00 	.4byte 0x55b
 1511 0012 70 61 72 73 	.asciz "parse_dcu_message_status"
 1511      65 5F 64 63 
 1511      75 5F 6D 65 
 1511      73 73 61 67 
 1511      65 5F 73 74 
 1511      61 74 75 73 
 1511      00 
 1512 002b B4 05 00 00 	.4byte 0x5b4
 1513 002f 70 61 72 73 	.asciz "parse_dcu_message_current"
 1513      65 5F 64 63 
 1513      75 5F 6D 65 
 1513      73 73 61 67 
 1513      65 5F 63 75 
 1513      72 72 65 6E 
 1513      74 00 
 1514 0049 09 06 00 00 	.4byte 0x609
 1515 004d 70 61 72 73 	.asciz "parse_can_dcu"
 1515      65 5F 63 61 
 1515      6E 5F 64 63 
 1515      75 00 
 1516 005b 00 00 00 00 	.4byte 0x0
 1517                 	.section .debug_pubtypes,info
 1518 0000 6B 00 00 00 	.4byte 0x6b
 1519 0004 02 00       	.2byte 0x2
 1520 0006 00 00 00 00 	.4byte .Ldebug_info0
 1521 000a 53 06 00 00 	.4byte 0x653
 1522 000e E0 00 00 00 	.4byte 0xe0
 1523 0012 75 69 6E 74 	.asciz "uint8_t"
 1523      38 5F 74 00 
 1524 001a 00 01 00 00 	.4byte 0x100
 1525 001e 75 69 6E 74 	.asciz "uint16_t"
 1525      31 36 5F 74 
 1525      00 
 1526 0027 DA 01 00 00 	.4byte 0x1da
 1527 002b 43 41 4E 64 	.asciz "CANdata"
 1527      61 74 61 00 
 1528 0033 BB 04 00 00 	.4byte 0x4bb
 1529 0037 44 43 55 5F 	.asciz "DCU_MSG_Status"
 1529      4D 53 47 5F 
 1529      53 74 61 74 
 1529      75 73 00 
 1530 0046 04 05 00 00 	.4byte 0x504
 1531 004a 44 43 55 5F 	.asciz "DCU_MSG_Current"
 1531      4D 53 47 5F 
 1531      43 75 72 72 
 1531      65 6E 74 00 
 1532 005a 47 05 00 00 	.4byte 0x547
 1533 005e 44 43 55 5F 	.asciz "DCU_CAN_Data"
MPLAB XC16 ASSEMBLY Listing:   			page 32


 1533      43 41 4E 5F 
 1533      44 61 74 61 
 1533      00 
 1534 006b 00 00 00 00 	.4byte 0x0
 1535                 	.section .debug_aranges,info
 1536 0000 14 00 00 00 	.4byte 0x14
 1537 0004 02 00       	.2byte 0x2
 1538 0006 00 00 00 00 	.4byte .Ldebug_info0
 1539 000a 04          	.byte 0x4
 1540 000b 00          	.byte 0x0
 1541 000c 00 00       	.2byte 0x0
 1542 000e 00 00       	.2byte 0x0
 1543 0010 00 00 00 00 	.4byte 0x0
 1544 0014 00 00 00 00 	.4byte 0x0
 1545                 	.section .debug_str,info
 1546                 	.section .text,code
 1547              	
 1548              	
 1549              	
 1550              	.section __c30_info,info,bss
 1551                 	__psv_trap_errata:
 1552                 	
 1553                 	.section __c30_signature,info,data
 1554 0000 01 00       	.word 0x0001
 1555 0002 00 00       	.word 0x0000
 1556 0004 00 00       	.word 0x0000
 1557                 	
 1558                 	
 1559                 	
 1560                 	.set ___PA___,0
 1561                 	.end
MPLAB XC16 ASSEMBLY Listing:   			page 33


DEFINED SYMBOLS
                            *ABS*:00000000 /home/user/Documents/FST/Programming/project_sub-zero/lib/can-ids/Devices/DCU_CAN.c
                            *ABS*:00000001 __33EP256MU806
                            *ABS*:00000001 __dsPIC33E
                            *ABS*:00000001 __HAS_DSP
                            *ABS*:00000001 __HAS_DMAV2
                            *ABS*:00000001 __HAS_CODEGUARD
                            *ABS*:00000001 __HAS_PMP_ENHANCED
                            *ABS*:00000001 __HAS_EDS
                            *ABS*:00000001 __HAS_EP
                            *ABS*:00000011 __TARGET_DIVIDE_CYCLES
                            *ABS*:00000001 __C30ELF
    {standard input}:14     .text:00000000 _parse_dcu_message_status
    {standard input}:18     *ABS*:00000000 ___PA___
    {standard input}:495    .text:00000368 _parse_dcu_message_current
    {standard input}:529    .text:00000386 _parse_can_dcu
    {standard input}:549    *ABS*:00000000 ___BP___
    {standard input}:1551   __c30_info:00000000 __psv_trap_errata
    {standard input}:19     .text:00000000 .L0
                            .text:000003c6 .L8
                            .text:000003b4 .L6
                            .text:000003bc .L7
                            .text:000003c6 .L3
                    .debug_abbrev:00000000 .Ldebug_abbrev0
                            .text:00000000 .Ltext0
                            .text:000003ce .Letext0
                      .debug_line:00000000 .Ldebug_line0
                            .text:00000000 .LFB0
                            .text:00000368 .LFE0
                            .text:00000368 .LFB1
                            .text:00000386 .LFE1
                            .text:00000386 .LFB2
                            .text:000003ce .LFE2
                     .debug_frame:00000000 .Lframe0
                      .debug_info:00000000 .Ldebug_info0

UNDEFINED SYMBOLS
CORCON

EQUATE SYMBOLS
/home/user/Documents/FST/Programming/project_sub-zero/lib/can-ids/Devices/DCU_CAN.c = 0x0
                  __33EP256MU806 = 0x1
                      __dsPIC33E = 0x1
                       __HAS_DSP = 0x1
                     __HAS_DMAV2 = 0x1
                 __HAS_CODEGUARD = 0x1
              __HAS_PMP_ENHANCED = 0x1
                       __HAS_EDS = 0x1
                        __HAS_EP = 0x1
          __TARGET_DIVIDE_CYCLES = 0x11
                        __C30ELF = 0x1
                        ___PA___ = 0x0
                        ___BP___ = 0x0
