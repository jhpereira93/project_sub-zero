MPLAB XC16 ASSEMBLY Listing:   			page 1


   1              	.file "/home/user/Documents/FST/Programming/project_sub-zero/lib/can-ids/Devices/IIB_CAN.c"
   2              	.section .debug_abbrev,info
   3                 	.Ldebug_abbrev0:
   4                 	.section .debug_info,info
   5                 	.Ldebug_info0:
   6                 	.section .debug_line,info
   7                 	.Ldebug_line0:
   8 0000 55 01 00 00 	.section .text,code
   8      02 00 A5 00 
   8      00 00 01 01 
   8      FB 0E 0A 00 
   8      01 01 01 01 
   8      00 00 00 01 
   8      6C 69 62 2F 
   8      63 61 6E 2D 
   8      69 64 73 2F 
   9              	.Ltext0:
  10              	.section .text,code
  11              	.align 2
  12              	.global _parse_IIB_speed_message
  13              	.type _parse_IIB_speed_message,@function
  14              	_parse_IIB_speed_message:
  15              	.LFB0:
  16              	.file 1 "lib/can-ids/Devices/IIB_CAN.c"
   1:lib/can-ids/Devices/IIB_CAN.c **** #include <stdbool.h>
   2:lib/can-ids/Devices/IIB_CAN.c **** #include <stdint.h>
   3:lib/can-ids/Devices/IIB_CAN.c **** #include "../CAN_IDs.h"
   4:lib/can-ids/Devices/IIB_CAN.c **** #include "IIB_CAN.h"
   5:lib/can-ids/Devices/IIB_CAN.c **** 
   6:lib/can-ids/Devices/IIB_CAN.c **** void parse_IIB_speed_message(uint16_t data[4], IIB_speed *speed){
  17              	.loc 1 6 0
  18              	.set ___PA___,1
  19 000000  04 00 FA 	lnk #4
  20              	.LCFI0:
  21              	.loc 1 6 0
  22 000002  00 0F 78 	mov w0,[w14]
   7:lib/can-ids/Devices/IIB_CAN.c **** 	speed->FL = data[0];
   8:lib/can-ids/Devices/IIB_CAN.c **** 	speed->FR = data[1];
   9:lib/can-ids/Devices/IIB_CAN.c **** 	speed->RL = data[2];
  23              	.loc 1 9 0
  24 000004  1E 01 78 	mov [w14],w2
  25 000006  E4 01 41 	add w2,#4,w3
  26              	.loc 1 7 0
  27 000008  1E 02 78 	mov [w14],w4
  28              	.loc 1 8 0
  29 00000a  1E 80 E8 	inc2 [w14],w0
  10:lib/can-ids/Devices/IIB_CAN.c **** 	speed->RR = data[3];
  30              	.loc 1 10 0
  31 00000c  9E 02 78 	mov [w14],w5
  32 00000e  66 81 42 	add w5,#6,w2
  33              	.loc 1 6 0
  34 000010  11 07 98 	mov w1,[w14+2]
  35              	.loc 1 7 0
  36 000012  94 02 78 	mov [w4],w5
  37 000014  9E 00 90 	mov [w14+2],w1
  38              	.loc 1 8 0
  39 000016  1E 02 90 	mov [w14+2],w4
MPLAB XC16 ASSEMBLY Listing:   			page 2


  40              	.loc 1 7 0
  41 000018  85 08 78 	mov w5,[w1]
  42              	.loc 1 9 0
  43 00001a  9E 00 90 	mov [w14+2],w1
  44              	.loc 1 8 0
  45 00001c  90 02 78 	mov [w0],w5
  46              	.loc 1 10 0
  47 00001e  1E 00 90 	mov [w14+2],w0
  48              	.loc 1 8 0
  49 000020  15 02 98 	mov w5,[w4+2]
  50              	.loc 1 9 0
  51 000022  93 01 78 	mov [w3],w3
  52 000024  A3 00 98 	mov w3,[w1+4]
  53              	.loc 1 10 0
  54 000026  92 00 78 	mov [w2],w1
  55 000028  31 00 98 	mov w1,[w0+6]
  11:lib/can-ids/Devices/IIB_CAN.c **** }
  56              	.loc 1 11 0
  57 00002a  8E 07 78 	mov w14,w15
  58 00002c  4F 07 78 	mov [--w15],w14
  59 00002e  00 40 A9 	bclr CORCON,#2
  60 000030  00 00 06 	return 
  61              	.set ___PA___,0
  62              	.LFE0:
  63              	.size _parse_IIB_speed_message,.-_parse_IIB_speed_message
  64              	.align 2
  65              	.global _parse_IIB_error_message
  66              	.type _parse_IIB_error_message,@function
  67              	_parse_IIB_error_message:
  68              	.LFB1:
  12:lib/can-ids/Devices/IIB_CAN.c **** 
  13:lib/can-ids/Devices/IIB_CAN.c **** void parse_IIB_error_message(uint16_t data[4], IIB_error *error){
  69              	.loc 1 13 0
  70              	.set ___PA___,1
  71 000032  04 00 FA 	lnk #4
  72              	.LCFI1:
  73              	.loc 1 13 0
  74 000034  00 0F 78 	mov w0,[w14]
  14:lib/can-ids/Devices/IIB_CAN.c **** 	error->FL = data[0];
  15:lib/can-ids/Devices/IIB_CAN.c **** 	error->FR = data[1];
  16:lib/can-ids/Devices/IIB_CAN.c **** 	error->RL = data[2];
  75              	.loc 1 16 0
  76 000036  1E 01 78 	mov [w14],w2
  77 000038  E4 01 41 	add w2,#4,w3
  78              	.loc 1 14 0
  79 00003a  1E 02 78 	mov [w14],w4
  80              	.loc 1 15 0
  81 00003c  1E 80 E8 	inc2 [w14],w0
  17:lib/can-ids/Devices/IIB_CAN.c **** 	error->RR = data[3];
  82              	.loc 1 17 0
  83 00003e  9E 02 78 	mov [w14],w5
  84 000040  66 81 42 	add w5,#6,w2
  85              	.loc 1 13 0
  86 000042  11 07 98 	mov w1,[w14+2]
  87              	.loc 1 14 0
  88 000044  94 02 78 	mov [w4],w5
  89 000046  9E 00 90 	mov [w14+2],w1
MPLAB XC16 ASSEMBLY Listing:   			page 3


  90              	.loc 1 15 0
  91 000048  1E 02 90 	mov [w14+2],w4
  92              	.loc 1 14 0
  93 00004a  85 08 78 	mov w5,[w1]
  94              	.loc 1 16 0
  95 00004c  9E 00 90 	mov [w14+2],w1
  96              	.loc 1 15 0
  97 00004e  90 02 78 	mov [w0],w5
  98              	.loc 1 17 0
  99 000050  1E 00 90 	mov [w14+2],w0
 100              	.loc 1 15 0
 101 000052  15 02 98 	mov w5,[w4+2]
 102              	.loc 1 16 0
 103 000054  93 01 78 	mov [w3],w3
 104 000056  A3 00 98 	mov w3,[w1+4]
 105              	.loc 1 17 0
 106 000058  92 00 78 	mov [w2],w1
 107 00005a  31 00 98 	mov w1,[w0+6]
  18:lib/can-ids/Devices/IIB_CAN.c **** }
 108              	.loc 1 18 0
 109 00005c  8E 07 78 	mov w14,w15
 110 00005e  4F 07 78 	mov [--w15],w14
 111 000060  00 40 A9 	bclr CORCON,#2
 112 000062  00 00 06 	return 
 113              	.set ___PA___,0
 114              	.LFE1:
 115              	.size _parse_IIB_error_message,.-_parse_IIB_error_message
 116              	.align 2
 117              	.global _parse_IIB_values_message
 118              	.type _parse_IIB_values_message,@function
 119              	_parse_IIB_values_message:
 120              	.LFB2:
  19:lib/can-ids/Devices/IIB_CAN.c **** 
  20:lib/can-ids/Devices/IIB_CAN.c **** void parse_IIB_values_message(uint16_t data[4], IIB_values *values){
 121              	.loc 1 20 0
 122              	.set ___PA___,1
 123 000064  04 00 FA 	lnk #4
 124              	.LCFI2:
 125 000066  88 9F BE 	mov.d w8,[w15++]
 126              	.LCFI3:
 127 000068  8A 1F 78 	mov w10,[w15++]
 128              	.LCFI4:
 129              	.loc 1 20 0
 130 00006a  00 0F 78 	mov w0,[w14]
  21:lib/can-ids/Devices/IIB_CAN.c **** 	values->node                = (data[0] & 0b01100000000000000) >> 14;
  22:lib/can-ids/Devices/IIB_CAN.c **** 	values->AMK_temp_motor      = (data[0] & 0b00011111111110000) >>4; 
  23:lib/can-ids/Devices/IIB_CAN.c **** 	values->AMK_temp_inverter   = ((data[0] & 0b000000000001111) << 6) | ((data[1] & 0b011111100000000
  24:lib/can-ids/Devices/IIB_CAN.c **** 	values->AMK_temp_IGBT       = data[1] & 0b00000001111111111;
  25:lib/can-ids/Devices/IIB_CAN.c **** 	values->torque              = data[2];
 131              	.loc 1 25 0
 132 00006c  1E 01 78 	mov [w14],w2
 133 00006e  E4 01 41 	add w2,#4,w3
 134              	.loc 1 21 0
 135 000070  1E 00 78 	mov [w14],w0
 136              	.loc 1 22 0
 137 000072  1E 02 78 	mov [w14],w4
 138              	.loc 1 23 0
MPLAB XC16 ASSEMBLY Listing:   			page 4


 139 000074  9E 04 78 	mov [w14],w9
 140 000076  1E 84 E8 	inc2 [w14],w8
 141              	.loc 1 24 0
 142 000078  1E 83 E8 	inc2 [w14],w6
  26:lib/can-ids/Devices/IIB_CAN.c **** 	values->magnetizing_current = data[3];
 143              	.loc 1 26 0
 144 00007a  9E 02 78 	mov [w14],w5
 145 00007c  66 81 42 	add w5,#6,w2
 146              	.loc 1 20 0
 147 00007e  11 07 98 	mov w1,[w14+2]
 148              	.loc 1 22 0
 149 000080  01 FF 23 	mov #16368,w1
 150              	.loc 1 21 0
 151 000082  10 00 78 	mov [w0],w0
 152 000084  9E 02 90 	mov [w14+2],w5
 153 000086  CE 03 DE 	lsr w0,#14,w7
 154              	.loc 1 22 0
 155 000088  1E 00 90 	mov [w14+2],w0
 156              	.loc 1 21 0
 157 00008a  87 0A 78 	mov w7,[w5]
 158              	.loc 1 23 0
 159 00008c  9E 03 90 	mov [w14+2],w7
 160              	.loc 1 22 0
 161 00008e  14 02 78 	mov [w4],w4
 162              	.loc 1 24 0
 163 000090  F5 3F 20 	mov #1023,w5
 164              	.loc 1 22 0
 165 000092  81 00 62 	and w4,w1,w1
 166              	.loc 1 24 0
 167 000094  1E 02 90 	mov [w14+2],w4
 168              	.loc 1 22 0
 169 000096  44 0D DE 	lsr w1,#4,w10
 170              	.loc 1 25 0
 171 000098  9E 00 90 	mov [w14+2],w1
 172              	.loc 1 22 0
 173 00009a  1A 00 98 	mov w10,[w0+2]
 174              	.loc 1 26 0
 175 00009c  1E 00 90 	mov [w14+2],w0
 176              	.loc 1 23 0
 177 00009e  99 04 78 	mov [w9],w9
 178 0000a0  18 04 78 	mov [w8],w8
 179 0000a2  EF 84 64 	and w9,#15,w9
 180 0000a4  4A 44 DE 	lsr w8,#10,w8
 181 0000a6  C6 4C DD 	sl w9,#6,w9
 182 0000a8  09 04 74 	ior w8,w9,w8
 183 0000aa  A8 03 98 	mov w8,[w7+4]
 184              	.loc 1 24 0
 185 0000ac  16 03 78 	mov [w6],w6
 186 0000ae  85 02 63 	and w6,w5,w5
 187 0000b0  35 02 98 	mov w5,[w4+6]
 188              	.loc 1 25 0
 189 0000b2  93 01 78 	mov [w3],w3
 190 0000b4  C3 00 98 	mov w3,[w1+8]
 191              	.loc 1 26 0
 192 0000b6  92 00 78 	mov [w2],w1
 193 0000b8  51 00 98 	mov w1,[w0+10]
  27:lib/can-ids/Devices/IIB_CAN.c **** }
MPLAB XC16 ASSEMBLY Listing:   			page 5


 194              	.loc 1 27 0
 195 0000ba  4F 05 78 	mov [--w15],w10
 196 0000bc  4F 04 BE 	mov.d [--w15],w8
 197 0000be  8E 07 78 	mov w14,w15
 198 0000c0  4F 07 78 	mov [--w15],w14
 199 0000c2  00 40 A9 	bclr CORCON,#2
 200 0000c4  00 00 06 	return 
 201              	.set ___PA___,0
 202              	.LFE2:
 203              	.size _parse_IIB_values_message,.-_parse_IIB_values_message
 204              	.align 2
 205              	.global _parse_IIB_status_message
 206              	.type _parse_IIB_status_message,@function
 207              	_parse_IIB_status_message:
 208              	.LFB3:
  28:lib/can-ids/Devices/IIB_CAN.c **** 
  29:lib/can-ids/Devices/IIB_CAN.c **** void parse_IIB_status_message(uint16_t data[2], IIB_status *status){
 209              	.loc 1 29 0
 210              	.set ___PA___,1
 211 0000c6  04 00 FA 	lnk #4
 212              	.LCFI5:
 213              	.loc 1 29 0
 214 0000c8  00 0F 78 	mov w0,[w14]
 215 0000ca  11 07 98 	mov w1,[w14+2]
  30:lib/can-ids/Devices/IIB_CAN.c **** 
  31:lib/can-ids/Devices/IIB_CAN.c **** 	status->INV_RTR = data[0] & 0b01; 
 216              	.loc 1 31 0
 217 0000cc  1E 00 78 	mov [w14],w0
 218 0000ce  9E 00 90 	mov [w14+2],w1
 219 0000d0  10 00 78 	mov [w0],w0
 220 0000d2  11 01 78 	mov [w1],w2
 221 0000d4  00 40 78 	mov.b w0,w0
 222 0000d6  02 00 A1 	bclr w2,#0
 223 0000d8  61 40 60 	and.b w0,#1,w0
 224 0000da  61 40 60 	and.b w0,#1,w0
 225 0000dc  61 40 60 	and.b w0,#1,w0
 226 0000de  00 80 FB 	ze w0,w0
 227 0000e0  61 00 60 	and w0,#1,w0
 228 0000e2  02 00 70 	ior w0,w2,w0
 229 0000e4  80 08 78 	mov w0,[w1]
  32:lib/can-ids/Devices/IIB_CAN.c ****         status->CAR_RTR = (data[0] & 0b010) >> 1;
 230              	.loc 1 32 0
 231 0000e6  9E 00 78 	mov [w14],w1
 232 0000e8  1E 00 90 	mov [w14+2],w0
 233 0000ea  91 00 78 	mov [w1],w1
 234 0000ec  10 01 78 	mov [w0],w2
 235 0000ee  E2 80 60 	and w1,#2,w1
 236 0000f0  02 10 A1 	bclr w2,#1
 237 0000f2  81 00 D1 	lsr w1,w1
 238 0000f4  81 40 78 	mov.b w1,w1
 239 0000f6  E1 C0 60 	and.b w1,#1,w1
 240 0000f8  81 80 FB 	ze w1,w1
 241 0000fa  E1 80 60 	and w1,#1,w1
 242 0000fc  81 80 40 	add w1,w1,w1
 243 0000fe  82 80 70 	ior w1,w2,w1
 244 000100  01 08 78 	mov w1,[w0]
  33:lib/can-ids/Devices/IIB_CAN.c ****     	status->INV_ERROR_1 = (data[0] & 0b0100) >> 2;
MPLAB XC16 ASSEMBLY Listing:   			page 6


 245              	.loc 1 33 0
 246 000102  9E 00 78 	mov [w14],w1
 247 000104  1E 00 90 	mov [w14+2],w0
 248 000106  91 00 78 	mov [w1],w1
 249 000108  10 01 78 	mov [w0],w2
 250 00010a  E4 80 60 	and w1,#4,w1
 251 00010c  02 20 A1 	bclr w2,#2
 252 00010e  C2 08 DE 	lsr w1,#2,w1
 253 000110  81 40 78 	mov.b w1,w1
 254 000112  E1 C0 60 	and.b w1,#1,w1
 255 000114  81 80 FB 	ze w1,w1
 256 000116  E1 80 60 	and w1,#1,w1
 257 000118  C2 08 DD 	sl w1,#2,w1
 258 00011a  82 80 70 	ior w1,w2,w1
 259 00011c  01 08 78 	mov w1,[w0]
  34:lib/can-ids/Devices/IIB_CAN.c ****     	status->INV_ERROR_2 = (data[0] & 0b01000) >> 3;
 260              	.loc 1 34 0
 261 00011e  9E 00 78 	mov [w14],w1
 262 000120  1E 00 90 	mov [w14+2],w0
 263 000122  91 00 78 	mov [w1],w1
 264 000124  10 01 78 	mov [w0],w2
 265 000126  E8 80 60 	and w1,#8,w1
 266 000128  02 30 A1 	bclr w2,#3
 267 00012a  C3 08 DE 	lsr w1,#3,w1
 268 00012c  81 40 78 	mov.b w1,w1
 269 00012e  E1 C0 60 	and.b w1,#1,w1
 270 000130  81 80 FB 	ze w1,w1
 271 000132  E1 80 60 	and w1,#1,w1
 272 000134  C3 08 DD 	sl w1,#3,w1
 273 000136  82 80 70 	ior w1,w2,w1
 274 000138  01 08 78 	mov w1,[w0]
  35:lib/can-ids/Devices/IIB_CAN.c ****     	status->INV_ERROR_3 = (data[0] & 0b010000) >> 4;
 275              	.loc 1 35 0
 276 00013a  9E 00 78 	mov [w14],w1
 277 00013c  1E 00 90 	mov [w14+2],w0
 278 00013e  91 00 78 	mov [w1],w1
 279 000140  10 01 78 	mov [w0],w2
 280 000142  F0 80 60 	and w1,#16,w1
 281 000144  02 40 A1 	bclr w2,#4
 282 000146  C4 08 DE 	lsr w1,#4,w1
 283 000148  81 40 78 	mov.b w1,w1
 284 00014a  E1 C0 60 	and.b w1,#1,w1
 285 00014c  81 80 FB 	ze w1,w1
 286 00014e  E1 80 60 	and w1,#1,w1
 287 000150  C4 08 DD 	sl w1,#4,w1
 288 000152  82 80 70 	ior w1,w2,w1
 289 000154  01 08 78 	mov w1,[w0]
  36:lib/can-ids/Devices/IIB_CAN.c ****     	status->INV_ERROR_4 = (data[0] & 0b0100000) >> 5;
 290              	.loc 1 36 0
 291 000156  9E 00 78 	mov [w14],w1
 292 000158  1E 00 90 	mov [w14+2],w0
 293 00015a  91 01 78 	mov [w1],w3
 294 00015c  10 01 78 	mov [w0],w2
 295 00015e  01 02 20 	mov #32,w1
 296 000160  02 50 A1 	bclr w2,#5
 297 000162  81 80 61 	and w3,w1,w1
 298 000164  C5 08 DE 	lsr w1,#5,w1
MPLAB XC16 ASSEMBLY Listing:   			page 7


 299 000166  81 40 78 	mov.b w1,w1
 300 000168  E1 C0 60 	and.b w1,#1,w1
 301 00016a  81 80 FB 	ze w1,w1
 302 00016c  E1 80 60 	and w1,#1,w1
 303 00016e  C5 08 DD 	sl w1,#5,w1
 304 000170  82 80 70 	ior w1,w2,w1
 305 000172  01 08 78 	mov w1,[w0]
  37:lib/can-ids/Devices/IIB_CAN.c ****     	status->INV_P_ERROR_1 = (data[0] & 0b01000000) >> 6;
 306              	.loc 1 37 0
 307 000174  9E 00 78 	mov [w14],w1
 308 000176  1E 00 90 	mov [w14+2],w0
 309 000178  91 01 78 	mov [w1],w3
 310 00017a  10 01 78 	mov [w0],w2
 311 00017c  01 04 20 	mov #64,w1
 312 00017e  02 60 A1 	bclr w2,#6
 313 000180  81 80 61 	and w3,w1,w1
 314 000182  C6 08 DE 	lsr w1,#6,w1
 315 000184  81 40 78 	mov.b w1,w1
 316 000186  E1 C0 60 	and.b w1,#1,w1
 317 000188  81 80 FB 	ze w1,w1
 318 00018a  E1 80 60 	and w1,#1,w1
 319 00018c  C6 08 DD 	sl w1,#6,w1
 320 00018e  82 80 70 	ior w1,w2,w1
 321 000190  01 08 78 	mov w1,[w0]
  38:lib/can-ids/Devices/IIB_CAN.c ****     	status->INV_P_ERROR_2 = (data[0] & 0b010000000) >> 7;
 322              	.loc 1 38 0
 323 000192  9E 00 78 	mov [w14],w1
 324 000194  1E 00 90 	mov [w14+2],w0
 325 000196  91 01 78 	mov [w1],w3
 326 000198  10 01 78 	mov [w0],w2
 327 00019a  01 08 20 	mov #128,w1
 328 00019c  02 70 A1 	bclr w2,#7
 329 00019e  81 80 61 	and w3,w1,w1
 330 0001a0  C7 08 DE 	lsr w1,#7,w1
 331 0001a2  81 40 78 	mov.b w1,w1
 332 0001a4  E1 C0 60 	and.b w1,#1,w1
 333 0001a6  81 80 FB 	ze w1,w1
 334 0001a8  E1 80 60 	and w1,#1,w1
 335 0001aa  C7 08 DD 	sl w1,#7,w1
 336 0001ac  82 80 70 	ior w1,w2,w1
 337 0001ae  01 08 78 	mov w1,[w0]
  39:lib/can-ids/Devices/IIB_CAN.c ****     	status->INV_P_ERROR_3 = (data[0] & 0b0100000000) >> 8;
 338              	.loc 1 39 0
 339 0001b0  9E 00 78 	mov [w14],w1
 340 0001b2  1E 00 90 	mov [w14+2],w0
 341 0001b4  91 01 78 	mov [w1],w3
 342 0001b6  10 01 78 	mov [w0],w2
 343 0001b8  01 10 20 	mov #256,w1
 344 0001ba  02 80 A1 	bclr w2,#8
 345 0001bc  81 80 61 	and w3,w1,w1
 346 0001be  C8 08 DE 	lsr w1,#8,w1
 347 0001c0  81 40 78 	mov.b w1,w1
 348 0001c2  E1 C0 60 	and.b w1,#1,w1
 349 0001c4  81 80 FB 	ze w1,w1
 350 0001c6  E1 80 60 	and w1,#1,w1
 351 0001c8  C8 08 DD 	sl w1,#8,w1
 352 0001ca  82 80 70 	ior w1,w2,w1
MPLAB XC16 ASSEMBLY Listing:   			page 8


 353 0001cc  01 08 78 	mov w1,[w0]
  40:lib/can-ids/Devices/IIB_CAN.c ****     	status->INV_P_ERROR_4 = (data[0] & 0b01000000000) >> 9;
 354              	.loc 1 40 0
 355 0001ce  9E 00 78 	mov [w14],w1
 356 0001d0  1E 00 90 	mov [w14+2],w0
 357 0001d2  91 01 78 	mov [w1],w3
 358 0001d4  10 01 78 	mov [w0],w2
 359 0001d6  01 20 20 	mov #512,w1
 360 0001d8  02 90 A1 	bclr w2,#9
 361 0001da  81 80 61 	and w3,w1,w1
 362 0001dc  C9 08 DE 	lsr w1,#9,w1
 363 0001de  81 40 78 	mov.b w1,w1
 364 0001e0  E1 C0 60 	and.b w1,#1,w1
 365 0001e2  81 80 FB 	ze w1,w1
 366 0001e4  E1 80 60 	and w1,#1,w1
 367 0001e6  C9 08 DD 	sl w1,#9,w1
 368 0001e8  82 80 70 	ior w1,w2,w1
 369 0001ea  01 08 78 	mov w1,[w0]
  41:lib/can-ids/Devices/IIB_CAN.c ****     	status->SEND_ERROR = (data[0] & 0b010000000000) >> 10;
 370              	.loc 1 41 0
 371 0001ec  9E 00 78 	mov [w14],w1
 372 0001ee  1E 00 90 	mov [w14+2],w0
 373 0001f0  91 01 78 	mov [w1],w3
 374 0001f2  10 01 78 	mov [w0],w2
 375 0001f4  01 40 20 	mov #1024,w1
 376 0001f6  02 A0 A1 	bclr w2,#10
 377 0001f8  81 80 61 	and w3,w1,w1
 378 0001fa  CA 08 DE 	lsr w1,#10,w1
 379 0001fc  81 40 78 	mov.b w1,w1
 380 0001fe  E1 C0 60 	and.b w1,#1,w1
 381 000200  81 80 FB 	ze w1,w1
 382 000202  E1 80 60 	and w1,#1,w1
 383 000204  CA 08 DD 	sl w1,#10,w1
 384 000206  82 80 70 	ior w1,w2,w1
 385 000208  01 08 78 	mov w1,[w0]
  42:lib/can-ids/Devices/IIB_CAN.c ****     	status->REGEN_OK = (data[0] & 0b0100000000000) >> 11;
 386              	.loc 1 42 0
 387 00020a  9E 00 78 	mov [w14],w1
 388 00020c  1E 00 90 	mov [w14+2],w0
 389 00020e  91 01 78 	mov [w1],w3
 390 000210  10 01 78 	mov [w0],w2
 391 000212  01 80 20 	mov #2048,w1
 392 000214  02 B0 A1 	bclr w2,#11
 393 000216  81 80 61 	and w3,w1,w1
 394 000218  CB 08 DE 	lsr w1,#11,w1
 395 00021a  81 40 78 	mov.b w1,w1
 396 00021c  E1 C0 60 	and.b w1,#1,w1
 397 00021e  81 80 FB 	ze w1,w1
 398 000220  E1 80 60 	and w1,#1,w1
 399 000222  CB 08 DD 	sl w1,#11,w1
 400 000224  82 80 70 	ior w1,w2,w1
 401 000226  01 08 78 	mov w1,[w0]
  43:lib/can-ids/Devices/IIB_CAN.c ****     	status->TURNING_OFF = (data[0] & 0b01000000000000) >> 12;
 402              	.loc 1 43 0
 403 000228  9E 00 78 	mov [w14],w1
 404 00022a  1E 00 90 	mov [w14+2],w0
 405 00022c  91 01 78 	mov [w1],w3
MPLAB XC16 ASSEMBLY Listing:   			page 9


 406 00022e  10 01 78 	mov [w0],w2
 407 000230  01 00 21 	mov #4096,w1
 408 000232  02 C0 A1 	bclr w2,#12
 409 000234  81 80 61 	and w3,w1,w1
 410 000236  CC 08 DE 	lsr w1,#12,w1
 411 000238  81 40 78 	mov.b w1,w1
 412 00023a  E1 C0 60 	and.b w1,#1,w1
 413 00023c  81 80 FB 	ze w1,w1
 414 00023e  E1 80 60 	and w1,#1,w1
 415 000240  CC 08 DD 	sl w1,#12,w1
 416 000242  82 80 70 	ior w1,w2,w1
 417 000244  01 08 78 	mov w1,[w0]
  44:lib/can-ids/Devices/IIB_CAN.c ****     	status->TURNING_ON = (data[0] & 0b010000000000000) >> 13;
 418              	.loc 1 44 0
 419 000246  9E 00 78 	mov [w14],w1
 420 000248  1E 00 90 	mov [w14+2],w0
 421 00024a  91 01 78 	mov [w1],w3
 422 00024c  10 01 78 	mov [w0],w2
 423 00024e  01 00 22 	mov #8192,w1
 424 000250  02 D0 A1 	bclr w2,#13
 425 000252  81 80 61 	and w3,w1,w1
 426 000254  CD 08 DE 	lsr w1,#13,w1
 427 000256  81 40 78 	mov.b w1,w1
 428 000258  E1 C0 60 	and.b w1,#1,w1
 429 00025a  81 80 FB 	ze w1,w1
 430 00025c  E1 80 60 	and w1,#1,w1
 431 00025e  CD 08 DD 	sl w1,#13,w1
 432 000260  82 80 70 	ior w1,w2,w1
 433 000262  01 08 78 	mov w1,[w0]
  45:lib/can-ids/Devices/IIB_CAN.c ****     	status->INV_HV = (data[0] & 0b0100000000000000) >> 14;
 434              	.loc 1 45 0
 435 000264  9E 00 78 	mov [w14],w1
 436 000266  1E 00 90 	mov [w14+2],w0
 437 000268  91 01 78 	mov [w1],w3
 438 00026a  10 01 78 	mov [w0],w2
 439 00026c  01 00 24 	mov #16384,w1
 440 00026e  02 E0 A1 	bclr w2,#14
 441 000270  81 80 61 	and w3,w1,w1
 442 000272  CE 08 DE 	lsr w1,#14,w1
 443 000274  81 40 78 	mov.b w1,w1
 444 000276  E1 C0 60 	and.b w1,#1,w1
 445 000278  81 80 FB 	ze w1,w1
 446 00027a  E1 80 60 	and w1,#1,w1
 447 00027c  CE 08 DD 	sl w1,#14,w1
 448 00027e  82 80 70 	ior w1,w2,w1
 449 000280  01 08 78 	mov w1,[w0]
  46:lib/can-ids/Devices/IIB_CAN.c ****     	status->SDC1 = data[1] & 0b01;
 450              	.loc 1 46 0
 451 000282  1E 80 E8 	inc2 [w14],w0
 452 000284  9E 00 90 	mov [w14+2],w1
 453 000286  10 00 78 	mov [w0],w0
 454 000288  11 01 78 	mov [w1],w2
 455 00028a  00 40 78 	mov.b w0,w0
 456 00028c  02 F0 A1 	bclr w2,#15
 457 00028e  61 40 60 	and.b w0,#1,w0
 458 000290  61 40 60 	and.b w0,#1,w0
 459 000292  61 40 60 	and.b w0,#1,w0
MPLAB XC16 ASSEMBLY Listing:   			page 10


 460 000294  00 80 FB 	ze w0,w0
 461 000296  4F 00 DD 	sl w0,#15,w0
 462 000298  02 00 70 	ior w0,w2,w0
 463 00029a  80 08 78 	mov w0,[w1]
  47:lib/can-ids/Devices/IIB_CAN.c ****     	status->SDC2 = (data[1] & 0b010) >> 1;
 464              	.loc 1 47 0
 465 00029c  9E 80 E8 	inc2 [w14],w1
 466 00029e  1E 00 90 	mov [w14+2],w0
 467 0002a0  91 00 78 	mov [w1],w1
 468 0002a2  10 01 90 	mov [w0+2],w2
 469 0002a4  E2 80 60 	and w1,#2,w1
 470 0002a6  02 00 A1 	bclr w2,#0
 471 0002a8  81 00 D1 	lsr w1,w1
 472 0002aa  81 40 78 	mov.b w1,w1
 473 0002ac  E1 C0 60 	and.b w1,#1,w1
 474 0002ae  81 80 FB 	ze w1,w1
 475 0002b0  E1 80 60 	and w1,#1,w1
 476 0002b2  82 80 70 	ior w1,w2,w1
 477 0002b4  11 00 98 	mov w1,[w0+2]
  48:lib/can-ids/Devices/IIB_CAN.c ****     	status->CONTROLLER = (data[1] &0b0100) >> 2;
 478              	.loc 1 48 0
 479 0002b6  9E 80 E8 	inc2 [w14],w1
 480 0002b8  1E 00 90 	mov [w14+2],w0
 481 0002ba  91 00 78 	mov [w1],w1
 482 0002bc  10 01 90 	mov [w0+2],w2
 483 0002be  E4 80 60 	and w1,#4,w1
 484 0002c0  02 10 A1 	bclr w2,#1
 485 0002c2  C2 08 DE 	lsr w1,#2,w1
 486 0002c4  81 40 78 	mov.b w1,w1
 487 0002c6  E1 C0 60 	and.b w1,#1,w1
 488 0002c8  81 80 FB 	ze w1,w1
 489 0002ca  E1 80 60 	and w1,#1,w1
 490 0002cc  81 80 40 	add w1,w1,w1
 491 0002ce  82 80 70 	ior w1,w2,w1
 492 0002d0  11 00 98 	mov w1,[w0+2]
  49:lib/can-ids/Devices/IIB_CAN.c ****     	status->I1 = (data[1] & 0b01000) >> 3;
 493              	.loc 1 49 0
 494 0002d2  9E 80 E8 	inc2 [w14],w1
 495 0002d4  1E 00 90 	mov [w14+2],w0
 496 0002d6  91 00 78 	mov [w1],w1
 497 0002d8  10 01 90 	mov [w0+2],w2
 498 0002da  E8 80 60 	and w1,#8,w1
 499 0002dc  02 20 A1 	bclr w2,#2
 500 0002de  C3 08 DE 	lsr w1,#3,w1
 501 0002e0  81 40 78 	mov.b w1,w1
 502 0002e2  E1 C0 60 	and.b w1,#1,w1
 503 0002e4  81 80 FB 	ze w1,w1
 504 0002e6  E1 80 60 	and w1,#1,w1
 505 0002e8  C2 08 DD 	sl w1,#2,w1
 506 0002ea  82 80 70 	ior w1,w2,w1
 507 0002ec  11 00 98 	mov w1,[w0+2]
  50:lib/can-ids/Devices/IIB_CAN.c ****     	status->I2 = (data[1] & 0b010000) >> 4;
 508              	.loc 1 50 0
 509 0002ee  9E 80 E8 	inc2 [w14],w1
 510 0002f0  1E 00 90 	mov [w14+2],w0
 511 0002f2  91 00 78 	mov [w1],w1
 512 0002f4  10 01 90 	mov [w0+2],w2
MPLAB XC16 ASSEMBLY Listing:   			page 11


 513 0002f6  F0 80 60 	and w1,#16,w1
 514 0002f8  02 30 A1 	bclr w2,#3
 515 0002fa  C4 08 DE 	lsr w1,#4,w1
 516 0002fc  81 40 78 	mov.b w1,w1
 517 0002fe  E1 C0 60 	and.b w1,#1,w1
 518 000300  81 80 FB 	ze w1,w1
 519 000302  E1 80 60 	and w1,#1,w1
 520 000304  C3 08 DD 	sl w1,#3,w1
 521 000306  82 80 70 	ior w1,w2,w1
 522 000308  11 00 98 	mov w1,[w0+2]
  51:lib/can-ids/Devices/IIB_CAN.c ****     	status->I3 = (data[1] & 0b0100000) >> 5;
 523              	.loc 1 51 0
 524 00030a  9E 80 E8 	inc2 [w14],w1
 525 00030c  1E 00 90 	mov [w14+2],w0
 526 00030e  91 01 78 	mov [w1],w3
 527 000310  10 01 90 	mov [w0+2],w2
 528 000312  01 02 20 	mov #32,w1
 529 000314  02 40 A1 	bclr w2,#4
 530 000316  81 80 61 	and w3,w1,w1
 531 000318  C5 08 DE 	lsr w1,#5,w1
 532 00031a  81 40 78 	mov.b w1,w1
 533 00031c  E1 C0 60 	and.b w1,#1,w1
 534 00031e  81 80 FB 	ze w1,w1
 535 000320  E1 80 60 	and w1,#1,w1
 536 000322  C4 08 DD 	sl w1,#4,w1
 537 000324  82 80 70 	ior w1,w2,w1
 538 000326  11 00 98 	mov w1,[w0+2]
  52:lib/can-ids/Devices/IIB_CAN.c ****     	status->I4 = (data[1] & 0b01000000) >> 6;
 539              	.loc 1 52 0
 540 000328  9E 80 E8 	inc2 [w14],w1
 541 00032a  1E 00 90 	mov [w14+2],w0
 542 00032c  91 01 78 	mov [w1],w3
 543 00032e  10 01 90 	mov [w0+2],w2
 544 000330  01 04 20 	mov #64,w1
 545 000332  02 50 A1 	bclr w2,#5
 546 000334  81 80 61 	and w3,w1,w1
 547 000336  C6 08 DE 	lsr w1,#6,w1
 548 000338  81 40 78 	mov.b w1,w1
 549 00033a  E1 C0 60 	and.b w1,#1,w1
 550 00033c  81 80 FB 	ze w1,w1
 551 00033e  E1 80 60 	and w1,#1,w1
 552 000340  C5 08 DD 	sl w1,#5,w1
 553 000342  82 80 70 	ior w1,w2,w1
 554 000344  11 00 98 	mov w1,[w0+2]
  53:lib/can-ids/Devices/IIB_CAN.c **** 		status->FRONT_OFF = (data[1] & 0b010000000) >> 7;
 555              	.loc 1 53 0
 556 000346  9E 80 E8 	inc2 [w14],w1
 557 000348  1E 00 90 	mov [w14+2],w0
 558 00034a  91 01 78 	mov [w1],w3
 559 00034c  10 01 90 	mov [w0+2],w2
 560 00034e  01 08 20 	mov #128,w1
 561 000350  02 70 A1 	bclr w2,#7
 562 000352  81 80 61 	and w3,w1,w1
 563 000354  C7 08 DE 	lsr w1,#7,w1
 564 000356  81 40 78 	mov.b w1,w1
 565 000358  E1 C0 60 	and.b w1,#1,w1
 566 00035a  81 80 FB 	ze w1,w1
MPLAB XC16 ASSEMBLY Listing:   			page 12


 567 00035c  E1 80 60 	and w1,#1,w1
 568 00035e  C7 08 DD 	sl w1,#7,w1
 569 000360  82 80 70 	ior w1,w2,w1
 570 000362  11 00 98 	mov w1,[w0+2]
  54:lib/can-ids/Devices/IIB_CAN.c **** 		status->REAR_OFF = (data[1] & 0b0100000000) >> 8;
 571              	.loc 1 54 0
 572 000364  9E 80 E8 	inc2 [w14],w1
 573 000366  1E 00 90 	mov [w14+2],w0
 574 000368  91 01 78 	mov [w1],w3
 575 00036a  10 01 90 	mov [w0+2],w2
 576 00036c  01 10 20 	mov #256,w1
 577 00036e  02 60 A1 	bclr w2,#6
 578 000370  81 80 61 	and w3,w1,w1
 579 000372  C8 08 DE 	lsr w1,#8,w1
 580 000374  81 40 78 	mov.b w1,w1
 581 000376  E1 C0 60 	and.b w1,#1,w1
 582 000378  81 80 FB 	ze w1,w1
 583 00037a  E1 80 60 	and w1,#1,w1
 584 00037c  C6 08 DD 	sl w1,#6,w1
 585 00037e  82 80 70 	ior w1,w2,w1
 586 000380  11 00 98 	mov w1,[w0+2]
  55:lib/can-ids/Devices/IIB_CAN.c **** }
 587              	.loc 1 55 0
 588 000382  8E 07 78 	mov w14,w15
 589 000384  4F 07 78 	mov [--w15],w14
 590 000386  00 40 A9 	bclr CORCON,#2
 591 000388  00 00 06 	return 
 592              	.set ___PA___,0
 593              	.LFE3:
 594              	.size _parse_IIB_status_message,.-_parse_IIB_status_message
 595              	.align 2
 596              	.global _parse_IIB_lims_message
 597              	.type _parse_IIB_lims_message,@function
 598              	_parse_IIB_lims_message:
 599              	.LFB4:
  56:lib/can-ids/Devices/IIB_CAN.c **** 
  57:lib/can-ids/Devices/IIB_CAN.c **** void parse_IIB_lims_message(uint16_t data[4], IIB_lims *lims){
 600              	.loc 1 57 0
 601              	.set ___PA___,1
 602 00038a  04 00 FA 	lnk #4
 603              	.LCFI6:
 604              	.loc 1 57 0
 605 00038c  00 0F 78 	mov w0,[w14]
  58:lib/can-ids/Devices/IIB_CAN.c ****     lims->lim_rpm = data[0];
  59:lib/can-ids/Devices/IIB_CAN.c ****     lims->lim_torque_r = data[1];
  60:lib/can-ids/Devices/IIB_CAN.c ****     lims->lim_torque_f = data[2];
 606              	.loc 1 60 0
 607 00038e  9E 01 78 	mov [w14],w3
 608 000390  64 81 41 	add w3,#4,w2
 609              	.loc 1 58 0
 610 000392  1E 00 78 	mov [w14],w0
 611              	.loc 1 59 0
 612 000394  9E 81 E8 	inc2 [w14],w3
 613              	.loc 1 57 0
 614 000396  11 07 98 	mov w1,[w14+2]
 615              	.loc 1 58 0
 616 000398  10 02 78 	mov [w0],w4
MPLAB XC16 ASSEMBLY Listing:   			page 13


 617 00039a  1E 00 90 	mov [w14+2],w0
 618              	.loc 1 59 0
 619 00039c  9E 00 90 	mov [w14+2],w1
 620              	.loc 1 58 0
 621 00039e  04 08 78 	mov w4,[w0]
 622              	.loc 1 60 0
 623 0003a0  1E 00 90 	mov [w14+2],w0
 624              	.loc 1 59 0
 625 0003a2  93 01 78 	mov [w3],w3
 626 0003a4  A3 00 98 	mov w3,[w1+4]
 627              	.loc 1 60 0
 628 0003a6  92 00 78 	mov [w2],w1
 629 0003a8  11 00 98 	mov w1,[w0+2]
  61:lib/can-ids/Devices/IIB_CAN.c ****     
  62:lib/can-ids/Devices/IIB_CAN.c ****     return;
  63:lib/can-ids/Devices/IIB_CAN.c **** }
 630              	.loc 1 63 0
 631 0003aa  8E 07 78 	mov w14,w15
 632 0003ac  4F 07 78 	mov [--w15],w14
 633 0003ae  00 40 A9 	bclr CORCON,#2
 634 0003b0  00 00 06 	return 
 635              	.set ___PA___,0
 636              	.LFE4:
 637              	.size _parse_IIB_lims_message,.-_parse_IIB_lims_message
 638              	.align 2
 639              	.global _parse_can_IIB
 640              	.type _parse_can_IIB,@function
 641              	_parse_can_IIB:
 642              	.LFB5:
  64:lib/can-ids/Devices/IIB_CAN.c **** 
  65:lib/can-ids/Devices/IIB_CAN.c **** void parse_can_IIB(CANdata message, IIB_CAN_data *data){
 643              	.loc 1 65 0
 644              	.set ___PA___,1
 645 0003b2  0E 00 FA 	lnk #14
 646              	.LCFI7:
 647              	.loc 1 65 0
 648 0003b4  00 0F 78 	mov w0,[w14]
 649 0003b6  11 07 98 	mov w1,[w14+2]
  66:lib/can-ids/Devices/IIB_CAN.c **** 
  67:lib/can-ids/Devices/IIB_CAN.c **** 	switch(message.msg_id){
 650              	.loc 1 67 0
 651 0003b8  1E 00 78 	mov [w14],w0
 652              	.loc 1 65 0
 653 0003ba  22 07 98 	mov w2,[w14+4]
 654              	.loc 1 67 0
 655 0003bc  45 00 DE 	lsr w0,#5,w0
 656              	.loc 1 65 0
 657 0003be  33 07 98 	mov w3,[w14+6]
 658              	.loc 1 67 0
 659 0003c0  F0 43 B2 	and.b #63,w0
 660              	.loc 1 65 0
 661 0003c2  44 07 98 	mov w4,[w14+8]
 662 0003c4  55 07 98 	mov w5,[w14+10]
 663 0003c6  66 07 98 	mov w6,[w14+12]
 664              	.loc 1 67 0
 665 0003c8  00 80 FB 	ze w0,w0
 666 0003ca  21 02 20 	mov #34,w1
MPLAB XC16 ASSEMBLY Listing:   			page 14


 667 0003cc  81 0F 50 	sub w0,w1,[w15]
 668              	.set ___BP___,0
 669 0003ce  00 00 32 	bra z,.L10
 670 0003d0  21 02 20 	mov #34,w1
 671 0003d2  81 0F 50 	sub w0,w1,[w15]
 672              	.set ___BP___,0
 673 0003d4  00 00 3C 	bra gt,.L13
 674 0003d6  EB 0F 50 	sub w0,#11,[w15]
 675              	.set ___BP___,0
 676 0003d8  00 00 32 	bra z,.L8
 677 0003da  11 02 20 	mov #33,w1
 678 0003dc  81 0F 50 	sub w0,w1,[w15]
 679              	.set ___BP___,0
 680 0003de  00 00 32 	bra z,.L9
  68:lib/can-ids/Devices/IIB_CAN.c **** 		case MSG_ID_IIB_SPEED : 
  69:lib/can-ids/Devices/IIB_CAN.c **** 			parse_IIB_speed_message(message.data, &(data->speed));
  70:lib/can-ids/Devices/IIB_CAN.c **** 			return;
  71:lib/can-ids/Devices/IIB_CAN.c **** 		case MSG_ID_IIB_ERROR :
  72:lib/can-ids/Devices/IIB_CAN.c **** 			parse_IIB_error_message(message.data, &(data->error));
  73:lib/can-ids/Devices/IIB_CAN.c **** 			return;
  74:lib/can-ids/Devices/IIB_CAN.c **** 		case MSG_ID_IIB_VALUES :
  75:lib/can-ids/Devices/IIB_CAN.c **** 			parse_IIB_values_message(message.data, &(data->values));
  76:lib/can-ids/Devices/IIB_CAN.c **** 			return; 
  77:lib/can-ids/Devices/IIB_CAN.c **** 	 	case MSG_ID_IIB_STATUS :
  78:lib/can-ids/Devices/IIB_CAN.c **** 	 		parse_IIB_status_message(message.data, &(data->status));
  79:lib/can-ids/Devices/IIB_CAN.c **** 			return;
  80:lib/can-ids/Devices/IIB_CAN.c **** 		case MSG_ID_IIB_LIMS :
  81:lib/can-ids/Devices/IIB_CAN.c **** 		    parse_IIB_lims_message(message.data, &(data->lims));
  82:lib/can-ids/Devices/IIB_CAN.c **** 		    return;
  83:lib/can-ids/Devices/IIB_CAN.c **** 
  84:lib/can-ids/Devices/IIB_CAN.c **** 		default : return;
 681              	.loc 1 84 0
 682 0003e0  00 00 37 	bra .L6
 683              	.L13:
 684              	.loc 1 67 0
 685 0003e2  31 02 20 	mov #35,w1
 686 0003e4  81 0F 50 	sub w0,w1,[w15]
 687              	.set ___BP___,0
 688 0003e6  00 00 32 	bra z,.L11
 689 0003e8  41 02 20 	mov #36,w1
 690 0003ea  81 0F 50 	sub w0,w1,[w15]
 691              	.set ___BP___,0
 692 0003ec  00 00 32 	bra z,.L12
 693              	.loc 1 84 0
 694 0003ee  00 00 37 	bra .L6
 695              	.L8:
 696              	.loc 1 69 0
 697 0003f0  EE 00 90 	mov [w14+12],w1
 698 0003f2  64 00 47 	add w14,#4,w0
 699 0003f4  00 00 07 	rcall _parse_IIB_speed_message
 700              	.loc 1 70 0
 701 0003f6  00 00 37 	bra .L6
 702              	.L9:
 703              	.loc 1 72 0
 704 0003f8  EE 00 90 	mov [w14+12],w1
 705 0003fa  64 00 47 	add w14,#4,w0
 706 0003fc  E8 80 40 	add w1,#8,w1
MPLAB XC16 ASSEMBLY Listing:   			page 15


 707 0003fe  00 00 07 	rcall _parse_IIB_error_message
 708              	.loc 1 73 0
 709 000400  00 00 37 	bra .L6
 710              	.L10:
 711              	.loc 1 75 0
 712 000402  EE 00 90 	mov [w14+12],w1
 713 000404  64 00 47 	add w14,#4,w0
 714 000406  F0 80 40 	add w1,#16,w1
 715 000408  00 00 07 	rcall _parse_IIB_values_message
 716              	.loc 1 76 0
 717 00040a  00 00 37 	bra .L6
 718              	.L11:
 719              	.loc 1 78 0
 720 00040c  EE 00 90 	mov [w14+12],w1
 721 00040e  64 00 47 	add w14,#4,w0
 722 000410  FC 80 40 	add w1,#28,w1
 723 000412  00 00 07 	rcall _parse_IIB_status_message
 724              	.loc 1 79 0
 725 000414  00 00 37 	bra .L6
 726              	.L12:
 727              	.loc 1 81 0
 728 000416  EE 00 90 	mov [w14+12],w1
 729 000418  64 00 47 	add w14,#4,w0
 730 00041a  01 02 B0 	add #32,w1
 731 00041c  00 00 07 	rcall _parse_IIB_lims_message
 732              	.L6:
  85:lib/can-ids/Devices/IIB_CAN.c **** 	}
  86:lib/can-ids/Devices/IIB_CAN.c **** }
 733              	.loc 1 86 0
 734 00041e  8E 07 78 	mov w14,w15
 735 000420  4F 07 78 	mov [--w15],w14
 736 000422  00 40 A9 	bclr CORCON,#2
 737 000424  00 00 06 	return 
 738              	.set ___PA___,0
 739              	.LFE5:
 740              	.size _parse_can_IIB,.-_parse_can_IIB
 741              	.section .debug_frame,info
 742                 	.Lframe0:
 743 0000 10 00 00 00 	.4byte .LECIE0-.LSCIE0
 744                 	.LSCIE0:
 745 0004 FF FF FF FF 	.4byte 0xffffffff
 746 0008 01          	.byte 0x1
 747 0009 00          	.byte 0
 748 000a 01          	.uleb128 0x1
 749 000b 02          	.sleb128 2
 750 000c 25          	.byte 0x25
 751 000d 12          	.byte 0x12
 752 000e 0F          	.uleb128 0xf
 753 000f 7E          	.sleb128 -2
 754 0010 09          	.byte 0x9
 755 0011 25          	.uleb128 0x25
 756 0012 0F          	.uleb128 0xf
 757 0013 00          	.align 4
 758                 	.LECIE0:
 759                 	.LSFDE0:
 760 0014 18 00 00 00 	.4byte .LEFDE0-.LASFDE0
 761                 	.LASFDE0:
MPLAB XC16 ASSEMBLY Listing:   			page 16


 762 0018 00 00 00 00 	.4byte .Lframe0
 763 001c 00 00 00 00 	.4byte .LFB0
 764 0020 32 00 00 00 	.4byte .LFE0-.LFB0
 765 0024 04          	.byte 0x4
 766 0025 02 00 00 00 	.4byte .LCFI0-.LFB0
 767 0029 13          	.byte 0x13
 768 002a 7D          	.sleb128 -3
 769 002b 0D          	.byte 0xd
 770 002c 0E          	.uleb128 0xe
 771 002d 8E          	.byte 0x8e
 772 002e 02          	.uleb128 0x2
 773 002f 00          	.align 4
 774                 	.LEFDE0:
 775                 	.LSFDE2:
 776 0030 18 00 00 00 	.4byte .LEFDE2-.LASFDE2
 777                 	.LASFDE2:
 778 0034 00 00 00 00 	.4byte .Lframe0
 779 0038 00 00 00 00 	.4byte .LFB1
 780 003c 32 00 00 00 	.4byte .LFE1-.LFB1
 781 0040 04          	.byte 0x4
 782 0041 02 00 00 00 	.4byte .LCFI1-.LFB1
 783 0045 13          	.byte 0x13
 784 0046 7D          	.sleb128 -3
 785 0047 0D          	.byte 0xd
 786 0048 0E          	.uleb128 0xe
 787 0049 8E          	.byte 0x8e
 788 004a 02          	.uleb128 0x2
 789 004b 00          	.align 4
 790                 	.LEFDE2:
 791                 	.LSFDE4:
 792 004c 20 00 00 00 	.4byte .LEFDE4-.LASFDE4
 793                 	.LASFDE4:
 794 0050 00 00 00 00 	.4byte .Lframe0
 795 0054 00 00 00 00 	.4byte .LFB2
 796 0058 62 00 00 00 	.4byte .LFE2-.LFB2
 797 005c 04          	.byte 0x4
 798 005d 02 00 00 00 	.4byte .LCFI2-.LFB2
 799 0061 13          	.byte 0x13
 800 0062 7D          	.sleb128 -3
 801 0063 0D          	.byte 0xd
 802 0064 0E          	.uleb128 0xe
 803 0065 8E          	.byte 0x8e
 804 0066 02          	.uleb128 0x2
 805 0067 04          	.byte 0x4
 806 0068 04 00 00 00 	.4byte .LCFI4-.LCFI2
 807 006c 8A          	.byte 0x8a
 808 006d 07          	.uleb128 0x7
 809 006e 88          	.byte 0x88
 810 006f 05          	.uleb128 0x5
 811                 	.align 4
 812                 	.LEFDE4:
 813                 	.LSFDE6:
 814 0070 18 00 00 00 	.4byte .LEFDE6-.LASFDE6
 815                 	.LASFDE6:
 816 0074 00 00 00 00 	.4byte .Lframe0
 817 0078 00 00 00 00 	.4byte .LFB3
 818 007c C4 02 00 00 	.4byte .LFE3-.LFB3
MPLAB XC16 ASSEMBLY Listing:   			page 17


 819 0080 04          	.byte 0x4
 820 0081 02 00 00 00 	.4byte .LCFI5-.LFB3
 821 0085 13          	.byte 0x13
 822 0086 7D          	.sleb128 -3
 823 0087 0D          	.byte 0xd
 824 0088 0E          	.uleb128 0xe
 825 0089 8E          	.byte 0x8e
 826 008a 02          	.uleb128 0x2
 827 008b 00          	.align 4
 828                 	.LEFDE6:
 829                 	.LSFDE8:
 830 008c 18 00 00 00 	.4byte .LEFDE8-.LASFDE8
 831                 	.LASFDE8:
 832 0090 00 00 00 00 	.4byte .Lframe0
 833 0094 00 00 00 00 	.4byte .LFB4
 834 0098 28 00 00 00 	.4byte .LFE4-.LFB4
 835 009c 04          	.byte 0x4
 836 009d 02 00 00 00 	.4byte .LCFI6-.LFB4
 837 00a1 13          	.byte 0x13
 838 00a2 7D          	.sleb128 -3
 839 00a3 0D          	.byte 0xd
 840 00a4 0E          	.uleb128 0xe
 841 00a5 8E          	.byte 0x8e
 842 00a6 02          	.uleb128 0x2
 843 00a7 00          	.align 4
 844                 	.LEFDE8:
 845                 	.LSFDE10:
 846 00a8 18 00 00 00 	.4byte .LEFDE10-.LASFDE10
 847                 	.LASFDE10:
 848 00ac 00 00 00 00 	.4byte .Lframe0
 849 00b0 00 00 00 00 	.4byte .LFB5
 850 00b4 74 00 00 00 	.4byte .LFE5-.LFB5
 851 00b8 04          	.byte 0x4
 852 00b9 02 00 00 00 	.4byte .LCFI7-.LFB5
 853 00bd 13          	.byte 0x13
 854 00be 7D          	.sleb128 -3
 855 00bf 0D          	.byte 0xd
 856 00c0 0E          	.uleb128 0xe
 857 00c1 8E          	.byte 0x8e
 858 00c2 02          	.uleb128 0x2
 859 00c3 00          	.align 4
 860                 	.LEFDE10:
 861                 	.section .text,code
 862              	.Letext0:
 863              	.file 2 "lib/can-ids/Devices/../CAN_IDs.h"
 864              	.file 3 "/opt/microchip/xc16/v1.36/bin/bin/../../include/lega-c/stdint.h"
 865              	.file 4 "lib/can-ids/Devices/IIB_CAN.h"
 866              	.section .debug_info,info
 867 0000 F0 07 00 00 	.4byte 0x7f0
 868 0004 02 00       	.2byte 0x2
 869 0006 00 00 00 00 	.4byte .Ldebug_abbrev0
 870 000a 04          	.byte 0x4
 871 000b 01          	.uleb128 0x1
 872 000c 47 4E 55 20 	.asciz "GNU C 4.5.1 (XC16, Microchip v1.36) (A) Build date: Dec  5 2018"
 872      43 20 34 2E 
 872      35 2E 31 20 
 872      28 58 43 31 
MPLAB XC16 ASSEMBLY Listing:   			page 18


 872      36 2C 20 4D 
 872      69 63 72 6F 
 872      63 68 69 70 
 872      20 76 31 2E 
 872      33 36 29 20 
 873 004c 01          	.byte 0x1
 874 004d 6C 69 62 2F 	.asciz "lib/can-ids/Devices/IIB_CAN.c"
 874      63 61 6E 2D 
 874      69 64 73 2F 
 874      44 65 76 69 
 874      63 65 73 2F 
 874      49 49 42 5F 
 874      43 41 4E 2E 
 874      63 00 
 875 006b 2F 68 6F 6D 	.asciz "/home/user/Documents/FST/Programming/project_sub-zero"
 875      65 2F 75 73 
 875      65 72 2F 44 
 875      6F 63 75 6D 
 875      65 6E 74 73 
 875      2F 46 53 54 
 875      2F 50 72 6F 
 875      67 72 61 6D 
 875      6D 69 6E 67 
 876 00a1 00 00 00 00 	.4byte .Ltext0
 877 00a5 00 00 00 00 	.4byte .Letext0
 878 00a9 00 00 00 00 	.4byte .Ldebug_line0
 879 00ad 02          	.uleb128 0x2
 880 00ae 01          	.byte 0x1
 881 00af 06          	.byte 0x6
 882 00b0 73 69 67 6E 	.asciz "signed char"
 882      65 64 20 63 
 882      68 61 72 00 
 883 00bc 02          	.uleb128 0x2
 884 00bd 02          	.byte 0x2
 885 00be 05          	.byte 0x5
 886 00bf 69 6E 74 00 	.asciz "int"
 887 00c3 02          	.uleb128 0x2
 888 00c4 04          	.byte 0x4
 889 00c5 05          	.byte 0x5
 890 00c6 6C 6F 6E 67 	.asciz "long int"
 890      20 69 6E 74 
 890      00 
 891 00cf 02          	.uleb128 0x2
 892 00d0 08          	.byte 0x8
 893 00d1 05          	.byte 0x5
 894 00d2 6C 6F 6E 67 	.asciz "long long int"
 894      20 6C 6F 6E 
 894      67 20 69 6E 
 894      74 00 
 895 00e0 02          	.uleb128 0x2
 896 00e1 01          	.byte 0x1
 897 00e2 08          	.byte 0x8
 898 00e3 75 6E 73 69 	.asciz "unsigned char"
 898      67 6E 65 64 
 898      20 63 68 61 
 898      72 00 
 899 00f1 03          	.uleb128 0x3
MPLAB XC16 ASSEMBLY Listing:   			page 19


 900 00f2 75 69 6E 74 	.asciz "uint16_t"
 900      31 36 5F 74 
 900      00 
 901 00fb 03          	.byte 0x3
 902 00fc 31          	.byte 0x31
 903 00fd 01 01 00 00 	.4byte 0x101
 904 0101 02          	.uleb128 0x2
 905 0102 02          	.byte 0x2
 906 0103 07          	.byte 0x7
 907 0104 75 6E 73 69 	.asciz "unsigned int"
 907      67 6E 65 64 
 907      20 69 6E 74 
 907      00 
 908 0111 02          	.uleb128 0x2
 909 0112 04          	.byte 0x4
 910 0113 07          	.byte 0x7
 911 0114 6C 6F 6E 67 	.asciz "long unsigned int"
 911      20 75 6E 73 
 911      69 67 6E 65 
 911      64 20 69 6E 
 911      74 00 
 912 0126 02          	.uleb128 0x2
 913 0127 08          	.byte 0x8
 914 0128 07          	.byte 0x7
 915 0129 6C 6F 6E 67 	.asciz "long long unsigned int"
 915      20 6C 6F 6E 
 915      67 20 75 6E 
 915      73 69 67 6E 
 915      65 64 20 69 
 915      6E 74 00 
 916 0140 04          	.uleb128 0x4
 917 0141 02          	.byte 0x2
 918 0142 02          	.byte 0x2
 919 0143 12          	.byte 0x12
 920 0144 71 01 00 00 	.4byte 0x171
 921 0148 05          	.uleb128 0x5
 922 0149 64 65 76 5F 	.asciz "dev_id"
 922      69 64 00 
 923 0150 02          	.byte 0x2
 924 0151 13          	.byte 0x13
 925 0152 F1 00 00 00 	.4byte 0xf1
 926 0156 02          	.byte 0x2
 927 0157 05          	.byte 0x5
 928 0158 0B          	.byte 0xb
 929 0159 02          	.byte 0x2
 930 015a 23          	.byte 0x23
 931 015b 00          	.uleb128 0x0
 932 015c 05          	.uleb128 0x5
 933 015d 6D 73 67 5F 	.asciz "msg_id"
 933      69 64 00 
 934 0164 02          	.byte 0x2
 935 0165 16          	.byte 0x16
 936 0166 F1 00 00 00 	.4byte 0xf1
 937 016a 02          	.byte 0x2
 938 016b 06          	.byte 0x6
 939 016c 05          	.byte 0x5
 940 016d 02          	.byte 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 20


 941 016e 23          	.byte 0x23
 942 016f 00          	.uleb128 0x0
 943 0170 00          	.byte 0x0
 944 0171 06          	.uleb128 0x6
 945 0172 02          	.byte 0x2
 946 0173 02          	.byte 0x2
 947 0174 11          	.byte 0x11
 948 0175 8A 01 00 00 	.4byte 0x18a
 949 0179 07          	.uleb128 0x7
 950 017a 40 01 00 00 	.4byte 0x140
 951 017e 08          	.uleb128 0x8
 952 017f 73 69 64 00 	.asciz "sid"
 953 0183 02          	.byte 0x2
 954 0184 18          	.byte 0x18
 955 0185 F1 00 00 00 	.4byte 0xf1
 956 0189 00          	.byte 0x0
 957 018a 04          	.uleb128 0x4
 958 018b 0C          	.byte 0xc
 959 018c 02          	.byte 0x2
 960 018d 10          	.byte 0x10
 961 018e BA 01 00 00 	.4byte 0x1ba
 962 0192 09          	.uleb128 0x9
 963 0193 71 01 00 00 	.4byte 0x171
 964 0197 02          	.byte 0x2
 965 0198 23          	.byte 0x23
 966 0199 00          	.uleb128 0x0
 967 019a 05          	.uleb128 0x5
 968 019b 64 6C 63 00 	.asciz "dlc"
 969 019f 02          	.byte 0x2
 970 01a0 1A          	.byte 0x1a
 971 01a1 F1 00 00 00 	.4byte 0xf1
 972 01a5 02          	.byte 0x2
 973 01a6 04          	.byte 0x4
 974 01a7 0C          	.byte 0xc
 975 01a8 02          	.byte 0x2
 976 01a9 23          	.byte 0x23
 977 01aa 02          	.uleb128 0x2
 978 01ab 0A          	.uleb128 0xa
 979 01ac 00 00 00 00 	.4byte .LASF0
 980 01b0 02          	.byte 0x2
 981 01b1 1B          	.byte 0x1b
 982 01b2 BA 01 00 00 	.4byte 0x1ba
 983 01b6 02          	.byte 0x2
 984 01b7 23          	.byte 0x23
 985 01b8 04          	.uleb128 0x4
 986 01b9 00          	.byte 0x0
 987 01ba 0B          	.uleb128 0xb
 988 01bb F1 00 00 00 	.4byte 0xf1
 989 01bf CA 01 00 00 	.4byte 0x1ca
 990 01c3 0C          	.uleb128 0xc
 991 01c4 01 01 00 00 	.4byte 0x101
 992 01c8 03          	.byte 0x3
 993 01c9 00          	.byte 0x0
 994 01ca 03          	.uleb128 0x3
 995 01cb 43 41 4E 64 	.asciz "CANdata"
 995      61 74 61 00 
 996 01d3 02          	.byte 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 21


 997 01d4 1C          	.byte 0x1c
 998 01d5 8A 01 00 00 	.4byte 0x18a
 999 01d9 04          	.uleb128 0x4
 1000 01da 08          	.byte 0x8
 1001 01db 04          	.byte 0x4
 1002 01dc 0D          	.byte 0xd
 1003 01dd 16 02 00 00 	.4byte 0x216
 1004 01e1 0D          	.uleb128 0xd
 1005 01e2 46 4C 00    	.asciz "FL"
 1006 01e5 04          	.byte 0x4
 1007 01e6 0E          	.byte 0xe
 1008 01e7 F1 00 00 00 	.4byte 0xf1
 1009 01eb 02          	.byte 0x2
 1010 01ec 23          	.byte 0x23
 1011 01ed 00          	.uleb128 0x0
 1012 01ee 0D          	.uleb128 0xd
 1013 01ef 46 52 00    	.asciz "FR"
 1014 01f2 04          	.byte 0x4
 1015 01f3 0F          	.byte 0xf
 1016 01f4 F1 00 00 00 	.4byte 0xf1
 1017 01f8 02          	.byte 0x2
 1018 01f9 23          	.byte 0x23
 1019 01fa 02          	.uleb128 0x2
 1020 01fb 0D          	.uleb128 0xd
 1021 01fc 52 4C 00    	.asciz "RL"
 1022 01ff 04          	.byte 0x4
 1023 0200 10          	.byte 0x10
 1024 0201 F1 00 00 00 	.4byte 0xf1
 1025 0205 02          	.byte 0x2
 1026 0206 23          	.byte 0x23
 1027 0207 04          	.uleb128 0x4
 1028 0208 0D          	.uleb128 0xd
 1029 0209 52 52 00    	.asciz "RR"
 1030 020c 04          	.byte 0x4
 1031 020d 11          	.byte 0x11
 1032 020e F1 00 00 00 	.4byte 0xf1
 1033 0212 02          	.byte 0x2
 1034 0213 23          	.byte 0x23
 1035 0214 06          	.uleb128 0x6
 1036 0215 00          	.byte 0x0
 1037 0216 03          	.uleb128 0x3
 1038 0217 49 49 42 5F 	.asciz "IIB_speed"
 1038      73 70 65 65 
 1038      64 00 
 1039 0221 04          	.byte 0x4
 1040 0222 12          	.byte 0x12
 1041 0223 D9 01 00 00 	.4byte 0x1d9
 1042 0227 04          	.uleb128 0x4
 1043 0228 08          	.byte 0x8
 1044 0229 04          	.byte 0x4
 1045 022a 16          	.byte 0x16
 1046 022b 64 02 00 00 	.4byte 0x264
 1047 022f 0D          	.uleb128 0xd
 1048 0230 46 4C 00    	.asciz "FL"
 1049 0233 04          	.byte 0x4
 1050 0234 17          	.byte 0x17
 1051 0235 F1 00 00 00 	.4byte 0xf1
MPLAB XC16 ASSEMBLY Listing:   			page 22


 1052 0239 02          	.byte 0x2
 1053 023a 23          	.byte 0x23
 1054 023b 00          	.uleb128 0x0
 1055 023c 0D          	.uleb128 0xd
 1056 023d 46 52 00    	.asciz "FR"
 1057 0240 04          	.byte 0x4
 1058 0241 18          	.byte 0x18
 1059 0242 F1 00 00 00 	.4byte 0xf1
 1060 0246 02          	.byte 0x2
 1061 0247 23          	.byte 0x23
 1062 0248 02          	.uleb128 0x2
 1063 0249 0D          	.uleb128 0xd
 1064 024a 52 4C 00    	.asciz "RL"
 1065 024d 04          	.byte 0x4
 1066 024e 19          	.byte 0x19
 1067 024f F1 00 00 00 	.4byte 0xf1
 1068 0253 02          	.byte 0x2
 1069 0254 23          	.byte 0x23
 1070 0255 04          	.uleb128 0x4
 1071 0256 0D          	.uleb128 0xd
 1072 0257 52 52 00    	.asciz "RR"
 1073 025a 04          	.byte 0x4
 1074 025b 1A          	.byte 0x1a
 1075 025c F1 00 00 00 	.4byte 0xf1
 1076 0260 02          	.byte 0x2
 1077 0261 23          	.byte 0x23
 1078 0262 06          	.uleb128 0x6
 1079 0263 00          	.byte 0x0
 1080 0264 03          	.uleb128 0x3
 1081 0265 49 49 42 5F 	.asciz "IIB_error"
 1081      65 72 72 6F 
 1081      72 00 
 1082 026f 04          	.byte 0x4
 1083 0270 1B          	.byte 0x1b
 1084 0271 27 02 00 00 	.4byte 0x227
 1085 0275 04          	.uleb128 0x4
 1086 0276 0C          	.byte 0xc
 1087 0277 04          	.byte 0x4
 1088 0278 1F          	.byte 0x1f
 1089 0279 09 03 00 00 	.4byte 0x309
 1090 027d 0D          	.uleb128 0xd
 1091 027e 6E 6F 64 65 	.asciz "node"
 1091      00 
 1092 0283 04          	.byte 0x4
 1093 0284 20          	.byte 0x20
 1094 0285 F1 00 00 00 	.4byte 0xf1
 1095 0289 02          	.byte 0x2
 1096 028a 23          	.byte 0x23
 1097 028b 00          	.uleb128 0x0
 1098 028c 0D          	.uleb128 0xd
 1099 028d 41 4D 4B 5F 	.asciz "AMK_temp_motor"
 1099      74 65 6D 70 
 1099      5F 6D 6F 74 
 1099      6F 72 00 
 1100 029c 04          	.byte 0x4
 1101 029d 21          	.byte 0x21
 1102 029e F1 00 00 00 	.4byte 0xf1
MPLAB XC16 ASSEMBLY Listing:   			page 23


 1103 02a2 02          	.byte 0x2
 1104 02a3 23          	.byte 0x23
 1105 02a4 02          	.uleb128 0x2
 1106 02a5 0D          	.uleb128 0xd
 1107 02a6 41 4D 4B 5F 	.asciz "AMK_temp_inverter"
 1107      74 65 6D 70 
 1107      5F 69 6E 76 
 1107      65 72 74 65 
 1107      72 00 
 1108 02b8 04          	.byte 0x4
 1109 02b9 22          	.byte 0x22
 1110 02ba F1 00 00 00 	.4byte 0xf1
 1111 02be 02          	.byte 0x2
 1112 02bf 23          	.byte 0x23
 1113 02c0 04          	.uleb128 0x4
 1114 02c1 0D          	.uleb128 0xd
 1115 02c2 41 4D 4B 5F 	.asciz "AMK_temp_IGBT"
 1115      74 65 6D 70 
 1115      5F 49 47 42 
 1115      54 00 
 1116 02d0 04          	.byte 0x4
 1117 02d1 23          	.byte 0x23
 1118 02d2 F1 00 00 00 	.4byte 0xf1
 1119 02d6 02          	.byte 0x2
 1120 02d7 23          	.byte 0x23
 1121 02d8 06          	.uleb128 0x6
 1122 02d9 0D          	.uleb128 0xd
 1123 02da 74 6F 72 71 	.asciz "torque"
 1123      75 65 00 
 1124 02e1 04          	.byte 0x4
 1125 02e2 24          	.byte 0x24
 1126 02e3 F1 00 00 00 	.4byte 0xf1
 1127 02e7 02          	.byte 0x2
 1128 02e8 23          	.byte 0x23
 1129 02e9 08          	.uleb128 0x8
 1130 02ea 0D          	.uleb128 0xd
 1131 02eb 6D 61 67 6E 	.asciz "magnetizing_current"
 1131      65 74 69 7A 
 1131      69 6E 67 5F 
 1131      63 75 72 72 
 1131      65 6E 74 00 
 1132 02ff 04          	.byte 0x4
 1133 0300 25          	.byte 0x25
 1134 0301 F1 00 00 00 	.4byte 0xf1
 1135 0305 02          	.byte 0x2
 1136 0306 23          	.byte 0x23
 1137 0307 0A          	.uleb128 0xa
 1138 0308 00          	.byte 0x0
 1139 0309 03          	.uleb128 0x3
 1140 030a 49 49 42 5F 	.asciz "IIB_values"
 1140      76 61 6C 75 
 1140      65 73 00 
 1141 0315 04          	.byte 0x4
 1142 0316 26          	.byte 0x26
 1143 0317 75 02 00 00 	.4byte 0x275
 1144 031b 04          	.uleb128 0x4
 1145 031c 04          	.byte 0x4
MPLAB XC16 ASSEMBLY Listing:   			page 24


 1146 031d 04          	.byte 0x4
 1147 031e 2A          	.byte 0x2a
 1148 031f 3A 05 00 00 	.4byte 0x53a
 1149 0323 05          	.uleb128 0x5
 1150 0324 49 4E 56 5F 	.asciz "INV_RTR"
 1150      52 54 52 00 
 1151 032c 04          	.byte 0x4
 1152 032d 2B          	.byte 0x2b
 1153 032e 01 01 00 00 	.4byte 0x101
 1154 0332 02          	.byte 0x2
 1155 0333 01          	.byte 0x1
 1156 0334 0F          	.byte 0xf
 1157 0335 02          	.byte 0x2
 1158 0336 23          	.byte 0x23
 1159 0337 00          	.uleb128 0x0
 1160 0338 05          	.uleb128 0x5
 1161 0339 43 41 52 5F 	.asciz "CAR_RTR"
 1161      52 54 52 00 
 1162 0341 04          	.byte 0x4
 1163 0342 2C          	.byte 0x2c
 1164 0343 01 01 00 00 	.4byte 0x101
 1165 0347 02          	.byte 0x2
 1166 0348 01          	.byte 0x1
 1167 0349 0E          	.byte 0xe
 1168 034a 02          	.byte 0x2
 1169 034b 23          	.byte 0x23
 1170 034c 00          	.uleb128 0x0
 1171 034d 05          	.uleb128 0x5
 1172 034e 49 4E 56 5F 	.asciz "INV_ERROR_1"
 1172      45 52 52 4F 
 1172      52 5F 31 00 
 1173 035a 04          	.byte 0x4
 1174 035b 2D          	.byte 0x2d
 1175 035c 01 01 00 00 	.4byte 0x101
 1176 0360 02          	.byte 0x2
 1177 0361 01          	.byte 0x1
 1178 0362 0D          	.byte 0xd
 1179 0363 02          	.byte 0x2
 1180 0364 23          	.byte 0x23
 1181 0365 00          	.uleb128 0x0
 1182 0366 05          	.uleb128 0x5
 1183 0367 49 4E 56 5F 	.asciz "INV_ERROR_2"
 1183      45 52 52 4F 
 1183      52 5F 32 00 
 1184 0373 04          	.byte 0x4
 1185 0374 2E          	.byte 0x2e
 1186 0375 01 01 00 00 	.4byte 0x101
 1187 0379 02          	.byte 0x2
 1188 037a 01          	.byte 0x1
 1189 037b 0C          	.byte 0xc
 1190 037c 02          	.byte 0x2
 1191 037d 23          	.byte 0x23
 1192 037e 00          	.uleb128 0x0
 1193 037f 05          	.uleb128 0x5
 1194 0380 49 4E 56 5F 	.asciz "INV_ERROR_3"
 1194      45 52 52 4F 
 1194      52 5F 33 00 
MPLAB XC16 ASSEMBLY Listing:   			page 25


 1195 038c 04          	.byte 0x4
 1196 038d 2F          	.byte 0x2f
 1197 038e 01 01 00 00 	.4byte 0x101
 1198 0392 02          	.byte 0x2
 1199 0393 01          	.byte 0x1
 1200 0394 0B          	.byte 0xb
 1201 0395 02          	.byte 0x2
 1202 0396 23          	.byte 0x23
 1203 0397 00          	.uleb128 0x0
 1204 0398 05          	.uleb128 0x5
 1205 0399 49 4E 56 5F 	.asciz "INV_ERROR_4"
 1205      45 52 52 4F 
 1205      52 5F 34 00 
 1206 03a5 04          	.byte 0x4
 1207 03a6 30          	.byte 0x30
 1208 03a7 01 01 00 00 	.4byte 0x101
 1209 03ab 02          	.byte 0x2
 1210 03ac 01          	.byte 0x1
 1211 03ad 0A          	.byte 0xa
 1212 03ae 02          	.byte 0x2
 1213 03af 23          	.byte 0x23
 1214 03b0 00          	.uleb128 0x0
 1215 03b1 05          	.uleb128 0x5
 1216 03b2 49 4E 56 5F 	.asciz "INV_P_ERROR_1"
 1216      50 5F 45 52 
 1216      52 4F 52 5F 
 1216      31 00 
 1217 03c0 04          	.byte 0x4
 1218 03c1 31          	.byte 0x31
 1219 03c2 01 01 00 00 	.4byte 0x101
 1220 03c6 02          	.byte 0x2
 1221 03c7 01          	.byte 0x1
 1222 03c8 09          	.byte 0x9
 1223 03c9 02          	.byte 0x2
 1224 03ca 23          	.byte 0x23
 1225 03cb 00          	.uleb128 0x0
 1226 03cc 05          	.uleb128 0x5
 1227 03cd 49 4E 56 5F 	.asciz "INV_P_ERROR_2"
 1227      50 5F 45 52 
 1227      52 4F 52 5F 
 1227      32 00 
 1228 03db 04          	.byte 0x4
 1229 03dc 32          	.byte 0x32
 1230 03dd 01 01 00 00 	.4byte 0x101
 1231 03e1 02          	.byte 0x2
 1232 03e2 01          	.byte 0x1
 1233 03e3 08          	.byte 0x8
 1234 03e4 02          	.byte 0x2
 1235 03e5 23          	.byte 0x23
 1236 03e6 00          	.uleb128 0x0
 1237 03e7 05          	.uleb128 0x5
 1238 03e8 49 4E 56 5F 	.asciz "INV_P_ERROR_3"
 1238      50 5F 45 52 
 1238      52 4F 52 5F 
 1238      33 00 
 1239 03f6 04          	.byte 0x4
 1240 03f7 33          	.byte 0x33
MPLAB XC16 ASSEMBLY Listing:   			page 26


 1241 03f8 01 01 00 00 	.4byte 0x101
 1242 03fc 02          	.byte 0x2
 1243 03fd 01          	.byte 0x1
 1244 03fe 07          	.byte 0x7
 1245 03ff 02          	.byte 0x2
 1246 0400 23          	.byte 0x23
 1247 0401 00          	.uleb128 0x0
 1248 0402 05          	.uleb128 0x5
 1249 0403 49 4E 56 5F 	.asciz "INV_P_ERROR_4"
 1249      50 5F 45 52 
 1249      52 4F 52 5F 
 1249      34 00 
 1250 0411 04          	.byte 0x4
 1251 0412 34          	.byte 0x34
 1252 0413 01 01 00 00 	.4byte 0x101
 1253 0417 02          	.byte 0x2
 1254 0418 01          	.byte 0x1
 1255 0419 06          	.byte 0x6
 1256 041a 02          	.byte 0x2
 1257 041b 23          	.byte 0x23
 1258 041c 00          	.uleb128 0x0
 1259 041d 05          	.uleb128 0x5
 1260 041e 53 45 4E 44 	.asciz "SEND_ERROR"
 1260      5F 45 52 52 
 1260      4F 52 00 
 1261 0429 04          	.byte 0x4
 1262 042a 35          	.byte 0x35
 1263 042b 01 01 00 00 	.4byte 0x101
 1264 042f 02          	.byte 0x2
 1265 0430 01          	.byte 0x1
 1266 0431 05          	.byte 0x5
 1267 0432 02          	.byte 0x2
 1268 0433 23          	.byte 0x23
 1269 0434 00          	.uleb128 0x0
 1270 0435 05          	.uleb128 0x5
 1271 0436 52 45 47 45 	.asciz "REGEN_OK"
 1271      4E 5F 4F 4B 
 1271      00 
 1272 043f 04          	.byte 0x4
 1273 0440 36          	.byte 0x36
 1274 0441 01 01 00 00 	.4byte 0x101
 1275 0445 02          	.byte 0x2
 1276 0446 01          	.byte 0x1
 1277 0447 04          	.byte 0x4
 1278 0448 02          	.byte 0x2
 1279 0449 23          	.byte 0x23
 1280 044a 00          	.uleb128 0x0
 1281 044b 05          	.uleb128 0x5
 1282 044c 54 55 52 4E 	.asciz "TURNING_OFF"
 1282      49 4E 47 5F 
 1282      4F 46 46 00 
 1283 0458 04          	.byte 0x4
 1284 0459 37          	.byte 0x37
 1285 045a 01 01 00 00 	.4byte 0x101
 1286 045e 02          	.byte 0x2
 1287 045f 01          	.byte 0x1
 1288 0460 03          	.byte 0x3
MPLAB XC16 ASSEMBLY Listing:   			page 27


 1289 0461 02          	.byte 0x2
 1290 0462 23          	.byte 0x23
 1291 0463 00          	.uleb128 0x0
 1292 0464 05          	.uleb128 0x5
 1293 0465 54 55 52 4E 	.asciz "TURNING_ON"
 1293      49 4E 47 5F 
 1293      4F 4E 00 
 1294 0470 04          	.byte 0x4
 1295 0471 38          	.byte 0x38
 1296 0472 01 01 00 00 	.4byte 0x101
 1297 0476 02          	.byte 0x2
 1298 0477 01          	.byte 0x1
 1299 0478 02          	.byte 0x2
 1300 0479 02          	.byte 0x2
 1301 047a 23          	.byte 0x23
 1302 047b 00          	.uleb128 0x0
 1303 047c 05          	.uleb128 0x5
 1304 047d 49 4E 56 5F 	.asciz "INV_HV"
 1304      48 56 00 
 1305 0484 04          	.byte 0x4
 1306 0485 39          	.byte 0x39
 1307 0486 01 01 00 00 	.4byte 0x101
 1308 048a 02          	.byte 0x2
 1309 048b 01          	.byte 0x1
 1310 048c 01          	.byte 0x1
 1311 048d 02          	.byte 0x2
 1312 048e 23          	.byte 0x23
 1313 048f 00          	.uleb128 0x0
 1314 0490 05          	.uleb128 0x5
 1315 0491 53 44 43 31 	.asciz "SDC1"
 1315      00 
 1316 0496 04          	.byte 0x4
 1317 0497 3A          	.byte 0x3a
 1318 0498 01 01 00 00 	.4byte 0x101
 1319 049c 02          	.byte 0x2
 1320 049d 01          	.byte 0x1
 1321 049e 10          	.byte 0x10
 1322 049f 02          	.byte 0x2
 1323 04a0 23          	.byte 0x23
 1324 04a1 00          	.uleb128 0x0
 1325 04a2 05          	.uleb128 0x5
 1326 04a3 53 44 43 32 	.asciz "SDC2"
 1326      00 
 1327 04a8 04          	.byte 0x4
 1328 04a9 3B          	.byte 0x3b
 1329 04aa 01 01 00 00 	.4byte 0x101
 1330 04ae 02          	.byte 0x2
 1331 04af 01          	.byte 0x1
 1332 04b0 0F          	.byte 0xf
 1333 04b1 02          	.byte 0x2
 1334 04b2 23          	.byte 0x23
 1335 04b3 02          	.uleb128 0x2
 1336 04b4 05          	.uleb128 0x5
 1337 04b5 43 4F 4E 54 	.asciz "CONTROLLER"
 1337      52 4F 4C 4C 
 1337      45 52 00 
 1338 04c0 04          	.byte 0x4
MPLAB XC16 ASSEMBLY Listing:   			page 28


 1339 04c1 3C          	.byte 0x3c
 1340 04c2 01 01 00 00 	.4byte 0x101
 1341 04c6 02          	.byte 0x2
 1342 04c7 01          	.byte 0x1
 1343 04c8 0E          	.byte 0xe
 1344 04c9 02          	.byte 0x2
 1345 04ca 23          	.byte 0x23
 1346 04cb 02          	.uleb128 0x2
 1347 04cc 05          	.uleb128 0x5
 1348 04cd 49 31 00    	.asciz "I1"
 1349 04d0 04          	.byte 0x4
 1350 04d1 3D          	.byte 0x3d
 1351 04d2 01 01 00 00 	.4byte 0x101
 1352 04d6 02          	.byte 0x2
 1353 04d7 01          	.byte 0x1
 1354 04d8 0D          	.byte 0xd
 1355 04d9 02          	.byte 0x2
 1356 04da 23          	.byte 0x23
 1357 04db 02          	.uleb128 0x2
 1358 04dc 05          	.uleb128 0x5
 1359 04dd 49 32 00    	.asciz "I2"
 1360 04e0 04          	.byte 0x4
 1361 04e1 3E          	.byte 0x3e
 1362 04e2 01 01 00 00 	.4byte 0x101
 1363 04e6 02          	.byte 0x2
 1364 04e7 01          	.byte 0x1
 1365 04e8 0C          	.byte 0xc
 1366 04e9 02          	.byte 0x2
 1367 04ea 23          	.byte 0x23
 1368 04eb 02          	.uleb128 0x2
 1369 04ec 05          	.uleb128 0x5
 1370 04ed 49 33 00    	.asciz "I3"
 1371 04f0 04          	.byte 0x4
 1372 04f1 3F          	.byte 0x3f
 1373 04f2 01 01 00 00 	.4byte 0x101
 1374 04f6 02          	.byte 0x2
 1375 04f7 01          	.byte 0x1
 1376 04f8 0B          	.byte 0xb
 1377 04f9 02          	.byte 0x2
 1378 04fa 23          	.byte 0x23
 1379 04fb 02          	.uleb128 0x2
 1380 04fc 05          	.uleb128 0x5
 1381 04fd 49 34 00    	.asciz "I4"
 1382 0500 04          	.byte 0x4
 1383 0501 40          	.byte 0x40
 1384 0502 01 01 00 00 	.4byte 0x101
 1385 0506 02          	.byte 0x2
 1386 0507 01          	.byte 0x1
 1387 0508 0A          	.byte 0xa
 1388 0509 02          	.byte 0x2
 1389 050a 23          	.byte 0x23
 1390 050b 02          	.uleb128 0x2
 1391 050c 05          	.uleb128 0x5
 1392 050d 52 45 41 52 	.asciz "REAR_OFF"
 1392      5F 4F 46 46 
 1392      00 
 1393 0516 04          	.byte 0x4
MPLAB XC16 ASSEMBLY Listing:   			page 29


 1394 0517 41          	.byte 0x41
 1395 0518 01 01 00 00 	.4byte 0x101
 1396 051c 02          	.byte 0x2
 1397 051d 01          	.byte 0x1
 1398 051e 09          	.byte 0x9
 1399 051f 02          	.byte 0x2
 1400 0520 23          	.byte 0x23
 1401 0521 02          	.uleb128 0x2
 1402 0522 05          	.uleb128 0x5
 1403 0523 46 52 4F 4E 	.asciz "FRONT_OFF"
 1403      54 5F 4F 46 
 1403      46 00 
 1404 052d 04          	.byte 0x4
 1405 052e 42          	.byte 0x42
 1406 052f 01 01 00 00 	.4byte 0x101
 1407 0533 02          	.byte 0x2
 1408 0534 01          	.byte 0x1
 1409 0535 08          	.byte 0x8
 1410 0536 02          	.byte 0x2
 1411 0537 23          	.byte 0x23
 1412 0538 02          	.uleb128 0x2
 1413 0539 00          	.byte 0x0
 1414 053a 03          	.uleb128 0x3
 1415 053b 49 49 42 5F 	.asciz "IIB_status"
 1415      73 74 61 74 
 1415      75 73 00 
 1416 0546 04          	.byte 0x4
 1417 0547 43          	.byte 0x43
 1418 0548 1B 03 00 00 	.4byte 0x31b
 1419 054c 04          	.uleb128 0x4
 1420 054d 06          	.byte 0x6
 1421 054e 04          	.byte 0x4
 1422 054f 49          	.byte 0x49
 1423 0550 95 05 00 00 	.4byte 0x595
 1424 0554 0D          	.uleb128 0xd
 1425 0555 6C 69 6D 5F 	.asciz "lim_rpm"
 1425      72 70 6D 00 
 1426 055d 04          	.byte 0x4
 1427 055e 4A          	.byte 0x4a
 1428 055f F1 00 00 00 	.4byte 0xf1
 1429 0563 02          	.byte 0x2
 1430 0564 23          	.byte 0x23
 1431 0565 00          	.uleb128 0x0
 1432 0566 0D          	.uleb128 0xd
 1433 0567 6C 69 6D 5F 	.asciz "lim_torque_f"
 1433      74 6F 72 71 
 1433      75 65 5F 66 
 1433      00 
 1434 0574 04          	.byte 0x4
 1435 0575 4B          	.byte 0x4b
 1436 0576 F1 00 00 00 	.4byte 0xf1
 1437 057a 02          	.byte 0x2
 1438 057b 23          	.byte 0x23
 1439 057c 02          	.uleb128 0x2
 1440 057d 0D          	.uleb128 0xd
 1441 057e 6C 69 6D 5F 	.asciz "lim_torque_r"
 1441      74 6F 72 71 
MPLAB XC16 ASSEMBLY Listing:   			page 30


 1441      75 65 5F 72 
 1441      00 
 1442 058b 04          	.byte 0x4
 1443 058c 4C          	.byte 0x4c
 1444 058d F1 00 00 00 	.4byte 0xf1
 1445 0591 02          	.byte 0x2
 1446 0592 23          	.byte 0x23
 1447 0593 04          	.uleb128 0x4
 1448 0594 00          	.byte 0x0
 1449 0595 03          	.uleb128 0x3
 1450 0596 49 49 42 5F 	.asciz "IIB_lims"
 1450      6C 69 6D 73 
 1450      00 
 1451 059f 04          	.byte 0x4
 1452 05a0 4D          	.byte 0x4d
 1453 05a1 4C 05 00 00 	.4byte 0x54c
 1454 05a5 04          	.uleb128 0x4
 1455 05a6 26          	.byte 0x26
 1456 05a7 04          	.byte 0x4
 1457 05a8 4F          	.byte 0x4f
 1458 05a9 FF 05 00 00 	.4byte 0x5ff
 1459 05ad 0D          	.uleb128 0xd
 1460 05ae 73 70 65 65 	.asciz "speed"
 1460      64 00 
 1461 05b4 04          	.byte 0x4
 1462 05b5 50          	.byte 0x50
 1463 05b6 16 02 00 00 	.4byte 0x216
 1464 05ba 02          	.byte 0x2
 1465 05bb 23          	.byte 0x23
 1466 05bc 00          	.uleb128 0x0
 1467 05bd 0D          	.uleb128 0xd
 1468 05be 65 72 72 6F 	.asciz "error"
 1468      72 00 
 1469 05c4 04          	.byte 0x4
 1470 05c5 51          	.byte 0x51
 1471 05c6 64 02 00 00 	.4byte 0x264
 1472 05ca 02          	.byte 0x2
 1473 05cb 23          	.byte 0x23
 1474 05cc 08          	.uleb128 0x8
 1475 05cd 0D          	.uleb128 0xd
 1476 05ce 76 61 6C 75 	.asciz "values"
 1476      65 73 00 
 1477 05d5 04          	.byte 0x4
 1478 05d6 52          	.byte 0x52
 1479 05d7 09 03 00 00 	.4byte 0x309
 1480 05db 02          	.byte 0x2
 1481 05dc 23          	.byte 0x23
 1482 05dd 10          	.uleb128 0x10
 1483 05de 0D          	.uleb128 0xd
 1484 05df 73 74 61 74 	.asciz "status"
 1484      75 73 00 
 1485 05e6 04          	.byte 0x4
 1486 05e7 53          	.byte 0x53
 1487 05e8 3A 05 00 00 	.4byte 0x53a
 1488 05ec 02          	.byte 0x2
 1489 05ed 23          	.byte 0x23
 1490 05ee 1C          	.uleb128 0x1c
MPLAB XC16 ASSEMBLY Listing:   			page 31


 1491 05ef 0D          	.uleb128 0xd
 1492 05f0 6C 69 6D 73 	.asciz "lims"
 1492      00 
 1493 05f5 04          	.byte 0x4
 1494 05f6 54          	.byte 0x54
 1495 05f7 95 05 00 00 	.4byte 0x595
 1496 05fb 02          	.byte 0x2
 1497 05fc 23          	.byte 0x23
 1498 05fd 20          	.uleb128 0x20
 1499 05fe 00          	.byte 0x0
 1500 05ff 03          	.uleb128 0x3
 1501 0600 49 49 42 5F 	.asciz "IIB_CAN_data"
 1501      43 41 4E 5F 
 1501      64 61 74 61 
 1501      00 
 1502 060d 04          	.byte 0x4
 1503 060e 55          	.byte 0x55
 1504 060f A5 05 00 00 	.4byte 0x5a5
 1505 0613 0E          	.uleb128 0xe
 1506 0614 01          	.byte 0x1
 1507 0615 70 61 72 73 	.asciz "parse_IIB_speed_message"
 1507      65 5F 49 49 
 1507      42 5F 73 70 
 1507      65 65 64 5F 
 1507      6D 65 73 73 
 1507      61 67 65 00 
 1508 062d 01          	.byte 0x1
 1509 062e 06          	.byte 0x6
 1510 062f 01          	.byte 0x1
 1511 0630 00 00 00 00 	.4byte .LFB0
 1512 0634 00 00 00 00 	.4byte .LFE0
 1513 0638 01          	.byte 0x1
 1514 0639 5E          	.byte 0x5e
 1515 063a 5D 06 00 00 	.4byte 0x65d
 1516 063e 0F          	.uleb128 0xf
 1517 063f 00 00 00 00 	.4byte .LASF0
 1518 0643 01          	.byte 0x1
 1519 0644 06          	.byte 0x6
 1520 0645 5D 06 00 00 	.4byte 0x65d
 1521 0649 02          	.byte 0x2
 1522 064a 7E          	.byte 0x7e
 1523 064b 00          	.sleb128 0
 1524 064c 10          	.uleb128 0x10
 1525 064d 73 70 65 65 	.asciz "speed"
 1525      64 00 
 1526 0653 01          	.byte 0x1
 1527 0654 06          	.byte 0x6
 1528 0655 63 06 00 00 	.4byte 0x663
 1529 0659 02          	.byte 0x2
 1530 065a 7E          	.byte 0x7e
 1531 065b 02          	.sleb128 2
 1532 065c 00          	.byte 0x0
 1533 065d 11          	.uleb128 0x11
 1534 065e 02          	.byte 0x2
 1535 065f F1 00 00 00 	.4byte 0xf1
 1536 0663 11          	.uleb128 0x11
 1537 0664 02          	.byte 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 32


 1538 0665 16 02 00 00 	.4byte 0x216
 1539 0669 0E          	.uleb128 0xe
 1540 066a 01          	.byte 0x1
 1541 066b 70 61 72 73 	.asciz "parse_IIB_error_message"
 1541      65 5F 49 49 
 1541      42 5F 65 72 
 1541      72 6F 72 5F 
 1541      6D 65 73 73 
 1541      61 67 65 00 
 1542 0683 01          	.byte 0x1
 1543 0684 0D          	.byte 0xd
 1544 0685 01          	.byte 0x1
 1545 0686 00 00 00 00 	.4byte .LFB1
 1546 068a 00 00 00 00 	.4byte .LFE1
 1547 068e 01          	.byte 0x1
 1548 068f 5E          	.byte 0x5e
 1549 0690 B3 06 00 00 	.4byte 0x6b3
 1550 0694 0F          	.uleb128 0xf
 1551 0695 00 00 00 00 	.4byte .LASF0
 1552 0699 01          	.byte 0x1
 1553 069a 0D          	.byte 0xd
 1554 069b 5D 06 00 00 	.4byte 0x65d
 1555 069f 02          	.byte 0x2
 1556 06a0 7E          	.byte 0x7e
 1557 06a1 00          	.sleb128 0
 1558 06a2 10          	.uleb128 0x10
 1559 06a3 65 72 72 6F 	.asciz "error"
 1559      72 00 
 1560 06a9 01          	.byte 0x1
 1561 06aa 0D          	.byte 0xd
 1562 06ab B3 06 00 00 	.4byte 0x6b3
 1563 06af 02          	.byte 0x2
 1564 06b0 7E          	.byte 0x7e
 1565 06b1 02          	.sleb128 2
 1566 06b2 00          	.byte 0x0
 1567 06b3 11          	.uleb128 0x11
 1568 06b4 02          	.byte 0x2
 1569 06b5 64 02 00 00 	.4byte 0x264
 1570 06b9 0E          	.uleb128 0xe
 1571 06ba 01          	.byte 0x1
 1572 06bb 70 61 72 73 	.asciz "parse_IIB_values_message"
 1572      65 5F 49 49 
 1572      42 5F 76 61 
 1572      6C 75 65 73 
 1572      5F 6D 65 73 
 1572      73 61 67 65 
 1572      00 
 1573 06d4 01          	.byte 0x1
 1574 06d5 14          	.byte 0x14
 1575 06d6 01          	.byte 0x1
 1576 06d7 00 00 00 00 	.4byte .LFB2
 1577 06db 00 00 00 00 	.4byte .LFE2
 1578 06df 01          	.byte 0x1
 1579 06e0 5E          	.byte 0x5e
 1580 06e1 05 07 00 00 	.4byte 0x705
 1581 06e5 0F          	.uleb128 0xf
 1582 06e6 00 00 00 00 	.4byte .LASF0
MPLAB XC16 ASSEMBLY Listing:   			page 33


 1583 06ea 01          	.byte 0x1
 1584 06eb 14          	.byte 0x14
 1585 06ec 5D 06 00 00 	.4byte 0x65d
 1586 06f0 02          	.byte 0x2
 1587 06f1 7E          	.byte 0x7e
 1588 06f2 00          	.sleb128 0
 1589 06f3 10          	.uleb128 0x10
 1590 06f4 76 61 6C 75 	.asciz "values"
 1590      65 73 00 
 1591 06fb 01          	.byte 0x1
 1592 06fc 14          	.byte 0x14
 1593 06fd 05 07 00 00 	.4byte 0x705
 1594 0701 02          	.byte 0x2
 1595 0702 7E          	.byte 0x7e
 1596 0703 02          	.sleb128 2
 1597 0704 00          	.byte 0x0
 1598 0705 11          	.uleb128 0x11
 1599 0706 02          	.byte 0x2
 1600 0707 09 03 00 00 	.4byte 0x309
 1601 070b 0E          	.uleb128 0xe
 1602 070c 01          	.byte 0x1
 1603 070d 70 61 72 73 	.asciz "parse_IIB_status_message"
 1603      65 5F 49 49 
 1603      42 5F 73 74 
 1603      61 74 75 73 
 1603      5F 6D 65 73 
 1603      73 61 67 65 
 1603      00 
 1604 0726 01          	.byte 0x1
 1605 0727 1D          	.byte 0x1d
 1606 0728 01          	.byte 0x1
 1607 0729 00 00 00 00 	.4byte .LFB3
 1608 072d 00 00 00 00 	.4byte .LFE3
 1609 0731 01          	.byte 0x1
 1610 0732 5E          	.byte 0x5e
 1611 0733 57 07 00 00 	.4byte 0x757
 1612 0737 0F          	.uleb128 0xf
 1613 0738 00 00 00 00 	.4byte .LASF0
 1614 073c 01          	.byte 0x1
 1615 073d 1D          	.byte 0x1d
 1616 073e 5D 06 00 00 	.4byte 0x65d
 1617 0742 02          	.byte 0x2
 1618 0743 7E          	.byte 0x7e
 1619 0744 00          	.sleb128 0
 1620 0745 10          	.uleb128 0x10
 1621 0746 73 74 61 74 	.asciz "status"
 1621      75 73 00 
 1622 074d 01          	.byte 0x1
 1623 074e 1D          	.byte 0x1d
 1624 074f 57 07 00 00 	.4byte 0x757
 1625 0753 02          	.byte 0x2
 1626 0754 7E          	.byte 0x7e
 1627 0755 02          	.sleb128 2
 1628 0756 00          	.byte 0x0
 1629 0757 11          	.uleb128 0x11
 1630 0758 02          	.byte 0x2
 1631 0759 3A 05 00 00 	.4byte 0x53a
MPLAB XC16 ASSEMBLY Listing:   			page 34


 1632 075d 0E          	.uleb128 0xe
 1633 075e 01          	.byte 0x1
 1634 075f 70 61 72 73 	.asciz "parse_IIB_lims_message"
 1634      65 5F 49 49 
 1634      42 5F 6C 69 
 1634      6D 73 5F 6D 
 1634      65 73 73 61 
 1634      67 65 00 
 1635 0776 01          	.byte 0x1
 1636 0777 39          	.byte 0x39
 1637 0778 01          	.byte 0x1
 1638 0779 00 00 00 00 	.4byte .LFB4
 1639 077d 00 00 00 00 	.4byte .LFE4
 1640 0781 01          	.byte 0x1
 1641 0782 5E          	.byte 0x5e
 1642 0783 A5 07 00 00 	.4byte 0x7a5
 1643 0787 0F          	.uleb128 0xf
 1644 0788 00 00 00 00 	.4byte .LASF0
 1645 078c 01          	.byte 0x1
 1646 078d 39          	.byte 0x39
 1647 078e 5D 06 00 00 	.4byte 0x65d
 1648 0792 02          	.byte 0x2
 1649 0793 7E          	.byte 0x7e
 1650 0794 00          	.sleb128 0
 1651 0795 10          	.uleb128 0x10
 1652 0796 6C 69 6D 73 	.asciz "lims"
 1652      00 
 1653 079b 01          	.byte 0x1
 1654 079c 39          	.byte 0x39
 1655 079d A5 07 00 00 	.4byte 0x7a5
 1656 07a1 02          	.byte 0x2
 1657 07a2 7E          	.byte 0x7e
 1658 07a3 02          	.sleb128 2
 1659 07a4 00          	.byte 0x0
 1660 07a5 11          	.uleb128 0x11
 1661 07a6 02          	.byte 0x2
 1662 07a7 95 05 00 00 	.4byte 0x595
 1663 07ab 0E          	.uleb128 0xe
 1664 07ac 01          	.byte 0x1
 1665 07ad 70 61 72 73 	.asciz "parse_can_IIB"
 1665      65 5F 63 61 
 1665      6E 5F 49 49 
 1665      42 00 
 1666 07bb 01          	.byte 0x1
 1667 07bc 41          	.byte 0x41
 1668 07bd 01          	.byte 0x1
 1669 07be 00 00 00 00 	.4byte .LFB5
 1670 07c2 00 00 00 00 	.4byte .LFE5
 1671 07c6 01          	.byte 0x1
 1672 07c7 5E          	.byte 0x5e
 1673 07c8 ED 07 00 00 	.4byte 0x7ed
 1674 07cc 10          	.uleb128 0x10
 1675 07cd 6D 65 73 73 	.asciz "message"
 1675      61 67 65 00 
 1676 07d5 01          	.byte 0x1
 1677 07d6 41          	.byte 0x41
 1678 07d7 CA 01 00 00 	.4byte 0x1ca
MPLAB XC16 ASSEMBLY Listing:   			page 35


 1679 07db 02          	.byte 0x2
 1680 07dc 7E          	.byte 0x7e
 1681 07dd 00          	.sleb128 0
 1682 07de 0F          	.uleb128 0xf
 1683 07df 00 00 00 00 	.4byte .LASF0
 1684 07e3 01          	.byte 0x1
 1685 07e4 41          	.byte 0x41
 1686 07e5 ED 07 00 00 	.4byte 0x7ed
 1687 07e9 02          	.byte 0x2
 1688 07ea 7E          	.byte 0x7e
 1689 07eb 0C          	.sleb128 12
 1690 07ec 00          	.byte 0x0
 1691 07ed 11          	.uleb128 0x11
 1692 07ee 02          	.byte 0x2
 1693 07ef FF 05 00 00 	.4byte 0x5ff
 1694 07f3 00          	.byte 0x0
 1695                 	.section .debug_abbrev,info
 1696 0000 01          	.uleb128 0x1
 1697 0001 11          	.uleb128 0x11
 1698 0002 01          	.byte 0x1
 1699 0003 25          	.uleb128 0x25
 1700 0004 08          	.uleb128 0x8
 1701 0005 13          	.uleb128 0x13
 1702 0006 0B          	.uleb128 0xb
 1703 0007 03          	.uleb128 0x3
 1704 0008 08          	.uleb128 0x8
 1705 0009 1B          	.uleb128 0x1b
 1706 000a 08          	.uleb128 0x8
 1707 000b 11          	.uleb128 0x11
 1708 000c 01          	.uleb128 0x1
 1709 000d 12          	.uleb128 0x12
 1710 000e 01          	.uleb128 0x1
 1711 000f 10          	.uleb128 0x10
 1712 0010 06          	.uleb128 0x6
 1713 0011 00          	.byte 0x0
 1714 0012 00          	.byte 0x0
 1715 0013 02          	.uleb128 0x2
 1716 0014 24          	.uleb128 0x24
 1717 0015 00          	.byte 0x0
 1718 0016 0B          	.uleb128 0xb
 1719 0017 0B          	.uleb128 0xb
 1720 0018 3E          	.uleb128 0x3e
 1721 0019 0B          	.uleb128 0xb
 1722 001a 03          	.uleb128 0x3
 1723 001b 08          	.uleb128 0x8
 1724 001c 00          	.byte 0x0
 1725 001d 00          	.byte 0x0
 1726 001e 03          	.uleb128 0x3
 1727 001f 16          	.uleb128 0x16
 1728 0020 00          	.byte 0x0
 1729 0021 03          	.uleb128 0x3
 1730 0022 08          	.uleb128 0x8
 1731 0023 3A          	.uleb128 0x3a
 1732 0024 0B          	.uleb128 0xb
 1733 0025 3B          	.uleb128 0x3b
 1734 0026 0B          	.uleb128 0xb
 1735 0027 49          	.uleb128 0x49
MPLAB XC16 ASSEMBLY Listing:   			page 36


 1736 0028 13          	.uleb128 0x13
 1737 0029 00          	.byte 0x0
 1738 002a 00          	.byte 0x0
 1739 002b 04          	.uleb128 0x4
 1740 002c 13          	.uleb128 0x13
 1741 002d 01          	.byte 0x1
 1742 002e 0B          	.uleb128 0xb
 1743 002f 0B          	.uleb128 0xb
 1744 0030 3A          	.uleb128 0x3a
 1745 0031 0B          	.uleb128 0xb
 1746 0032 3B          	.uleb128 0x3b
 1747 0033 0B          	.uleb128 0xb
 1748 0034 01          	.uleb128 0x1
 1749 0035 13          	.uleb128 0x13
 1750 0036 00          	.byte 0x0
 1751 0037 00          	.byte 0x0
 1752 0038 05          	.uleb128 0x5
 1753 0039 0D          	.uleb128 0xd
 1754 003a 00          	.byte 0x0
 1755 003b 03          	.uleb128 0x3
 1756 003c 08          	.uleb128 0x8
 1757 003d 3A          	.uleb128 0x3a
 1758 003e 0B          	.uleb128 0xb
 1759 003f 3B          	.uleb128 0x3b
 1760 0040 0B          	.uleb128 0xb
 1761 0041 49          	.uleb128 0x49
 1762 0042 13          	.uleb128 0x13
 1763 0043 0B          	.uleb128 0xb
 1764 0044 0B          	.uleb128 0xb
 1765 0045 0D          	.uleb128 0xd
 1766 0046 0B          	.uleb128 0xb
 1767 0047 0C          	.uleb128 0xc
 1768 0048 0B          	.uleb128 0xb
 1769 0049 38          	.uleb128 0x38
 1770 004a 0A          	.uleb128 0xa
 1771 004b 00          	.byte 0x0
 1772 004c 00          	.byte 0x0
 1773 004d 06          	.uleb128 0x6
 1774 004e 17          	.uleb128 0x17
 1775 004f 01          	.byte 0x1
 1776 0050 0B          	.uleb128 0xb
 1777 0051 0B          	.uleb128 0xb
 1778 0052 3A          	.uleb128 0x3a
 1779 0053 0B          	.uleb128 0xb
 1780 0054 3B          	.uleb128 0x3b
 1781 0055 0B          	.uleb128 0xb
 1782 0056 01          	.uleb128 0x1
 1783 0057 13          	.uleb128 0x13
 1784 0058 00          	.byte 0x0
 1785 0059 00          	.byte 0x0
 1786 005a 07          	.uleb128 0x7
 1787 005b 0D          	.uleb128 0xd
 1788 005c 00          	.byte 0x0
 1789 005d 49          	.uleb128 0x49
 1790 005e 13          	.uleb128 0x13
 1791 005f 00          	.byte 0x0
 1792 0060 00          	.byte 0x0
MPLAB XC16 ASSEMBLY Listing:   			page 37


 1793 0061 08          	.uleb128 0x8
 1794 0062 0D          	.uleb128 0xd
 1795 0063 00          	.byte 0x0
 1796 0064 03          	.uleb128 0x3
 1797 0065 08          	.uleb128 0x8
 1798 0066 3A          	.uleb128 0x3a
 1799 0067 0B          	.uleb128 0xb
 1800 0068 3B          	.uleb128 0x3b
 1801 0069 0B          	.uleb128 0xb
 1802 006a 49          	.uleb128 0x49
 1803 006b 13          	.uleb128 0x13
 1804 006c 00          	.byte 0x0
 1805 006d 00          	.byte 0x0
 1806 006e 09          	.uleb128 0x9
 1807 006f 0D          	.uleb128 0xd
 1808 0070 00          	.byte 0x0
 1809 0071 49          	.uleb128 0x49
 1810 0072 13          	.uleb128 0x13
 1811 0073 38          	.uleb128 0x38
 1812 0074 0A          	.uleb128 0xa
 1813 0075 00          	.byte 0x0
 1814 0076 00          	.byte 0x0
 1815 0077 0A          	.uleb128 0xa
 1816 0078 0D          	.uleb128 0xd
 1817 0079 00          	.byte 0x0
 1818 007a 03          	.uleb128 0x3
 1819 007b 0E          	.uleb128 0xe
 1820 007c 3A          	.uleb128 0x3a
 1821 007d 0B          	.uleb128 0xb
 1822 007e 3B          	.uleb128 0x3b
 1823 007f 0B          	.uleb128 0xb
 1824 0080 49          	.uleb128 0x49
 1825 0081 13          	.uleb128 0x13
 1826 0082 38          	.uleb128 0x38
 1827 0083 0A          	.uleb128 0xa
 1828 0084 00          	.byte 0x0
 1829 0085 00          	.byte 0x0
 1830 0086 0B          	.uleb128 0xb
 1831 0087 01          	.uleb128 0x1
 1832 0088 01          	.byte 0x1
 1833 0089 49          	.uleb128 0x49
 1834 008a 13          	.uleb128 0x13
 1835 008b 01          	.uleb128 0x1
 1836 008c 13          	.uleb128 0x13
 1837 008d 00          	.byte 0x0
 1838 008e 00          	.byte 0x0
 1839 008f 0C          	.uleb128 0xc
 1840 0090 21          	.uleb128 0x21
 1841 0091 00          	.byte 0x0
 1842 0092 49          	.uleb128 0x49
 1843 0093 13          	.uleb128 0x13
 1844 0094 2F          	.uleb128 0x2f
 1845 0095 0B          	.uleb128 0xb
 1846 0096 00          	.byte 0x0
 1847 0097 00          	.byte 0x0
 1848 0098 0D          	.uleb128 0xd
 1849 0099 0D          	.uleb128 0xd
MPLAB XC16 ASSEMBLY Listing:   			page 38


 1850 009a 00          	.byte 0x0
 1851 009b 03          	.uleb128 0x3
 1852 009c 08          	.uleb128 0x8
 1853 009d 3A          	.uleb128 0x3a
 1854 009e 0B          	.uleb128 0xb
 1855 009f 3B          	.uleb128 0x3b
 1856 00a0 0B          	.uleb128 0xb
 1857 00a1 49          	.uleb128 0x49
 1858 00a2 13          	.uleb128 0x13
 1859 00a3 38          	.uleb128 0x38
 1860 00a4 0A          	.uleb128 0xa
 1861 00a5 00          	.byte 0x0
 1862 00a6 00          	.byte 0x0
 1863 00a7 0E          	.uleb128 0xe
 1864 00a8 2E          	.uleb128 0x2e
 1865 00a9 01          	.byte 0x1
 1866 00aa 3F          	.uleb128 0x3f
 1867 00ab 0C          	.uleb128 0xc
 1868 00ac 03          	.uleb128 0x3
 1869 00ad 08          	.uleb128 0x8
 1870 00ae 3A          	.uleb128 0x3a
 1871 00af 0B          	.uleb128 0xb
 1872 00b0 3B          	.uleb128 0x3b
 1873 00b1 0B          	.uleb128 0xb
 1874 00b2 27          	.uleb128 0x27
 1875 00b3 0C          	.uleb128 0xc
 1876 00b4 11          	.uleb128 0x11
 1877 00b5 01          	.uleb128 0x1
 1878 00b6 12          	.uleb128 0x12
 1879 00b7 01          	.uleb128 0x1
 1880 00b8 40          	.uleb128 0x40
 1881 00b9 0A          	.uleb128 0xa
 1882 00ba 01          	.uleb128 0x1
 1883 00bb 13          	.uleb128 0x13
 1884 00bc 00          	.byte 0x0
 1885 00bd 00          	.byte 0x0
 1886 00be 0F          	.uleb128 0xf
 1887 00bf 05          	.uleb128 0x5
 1888 00c0 00          	.byte 0x0
 1889 00c1 03          	.uleb128 0x3
 1890 00c2 0E          	.uleb128 0xe
 1891 00c3 3A          	.uleb128 0x3a
 1892 00c4 0B          	.uleb128 0xb
 1893 00c5 3B          	.uleb128 0x3b
 1894 00c6 0B          	.uleb128 0xb
 1895 00c7 49          	.uleb128 0x49
 1896 00c8 13          	.uleb128 0x13
 1897 00c9 02          	.uleb128 0x2
 1898 00ca 0A          	.uleb128 0xa
 1899 00cb 00          	.byte 0x0
 1900 00cc 00          	.byte 0x0
 1901 00cd 10          	.uleb128 0x10
 1902 00ce 05          	.uleb128 0x5
 1903 00cf 00          	.byte 0x0
 1904 00d0 03          	.uleb128 0x3
 1905 00d1 08          	.uleb128 0x8
 1906 00d2 3A          	.uleb128 0x3a
MPLAB XC16 ASSEMBLY Listing:   			page 39


 1907 00d3 0B          	.uleb128 0xb
 1908 00d4 3B          	.uleb128 0x3b
 1909 00d5 0B          	.uleb128 0xb
 1910 00d6 49          	.uleb128 0x49
 1911 00d7 13          	.uleb128 0x13
 1912 00d8 02          	.uleb128 0x2
 1913 00d9 0A          	.uleb128 0xa
 1914 00da 00          	.byte 0x0
 1915 00db 00          	.byte 0x0
 1916 00dc 11          	.uleb128 0x11
 1917 00dd 0F          	.uleb128 0xf
 1918 00de 00          	.byte 0x0
 1919 00df 0B          	.uleb128 0xb
 1920 00e0 0B          	.uleb128 0xb
 1921 00e1 49          	.uleb128 0x49
 1922 00e2 13          	.uleb128 0x13
 1923 00e3 00          	.byte 0x0
 1924 00e4 00          	.byte 0x0
 1925 00e5 00          	.byte 0x0
 1926                 	.section .debug_pubnames,info
 1927 0000 AD 00 00 00 	.4byte 0xad
 1928 0004 02 00       	.2byte 0x2
 1929 0006 00 00 00 00 	.4byte .Ldebug_info0
 1930 000a F4 07 00 00 	.4byte 0x7f4
 1931 000e 13 06 00 00 	.4byte 0x613
 1932 0012 70 61 72 73 	.asciz "parse_IIB_speed_message"
 1932      65 5F 49 49 
 1932      42 5F 73 70 
 1932      65 65 64 5F 
 1932      6D 65 73 73 
 1932      61 67 65 00 
 1933 002a 69 06 00 00 	.4byte 0x669
 1934 002e 70 61 72 73 	.asciz "parse_IIB_error_message"
 1934      65 5F 49 49 
 1934      42 5F 65 72 
 1934      72 6F 72 5F 
 1934      6D 65 73 73 
 1934      61 67 65 00 
 1935 0046 B9 06 00 00 	.4byte 0x6b9
 1936 004a 70 61 72 73 	.asciz "parse_IIB_values_message"
 1936      65 5F 49 49 
 1936      42 5F 76 61 
 1936      6C 75 65 73 
 1936      5F 6D 65 73 
 1936      73 61 67 65 
 1936      00 
 1937 0063 0B 07 00 00 	.4byte 0x70b
 1938 0067 70 61 72 73 	.asciz "parse_IIB_status_message"
 1938      65 5F 49 49 
 1938      42 5F 73 74 
 1938      61 74 75 73 
 1938      5F 6D 65 73 
 1938      73 61 67 65 
 1938      00 
 1939 0080 5D 07 00 00 	.4byte 0x75d
 1940 0084 70 61 72 73 	.asciz "parse_IIB_lims_message"
 1940      65 5F 49 49 
MPLAB XC16 ASSEMBLY Listing:   			page 40


 1940      42 5F 6C 69 
 1940      6D 73 5F 6D 
 1940      65 73 73 61 
 1940      67 65 00 
 1941 009b AB 07 00 00 	.4byte 0x7ab
 1942 009f 70 61 72 73 	.asciz "parse_can_IIB"
 1942      65 5F 63 61 
 1942      6E 5F 49 49 
 1942      42 00 
 1943 00ad 00 00 00 00 	.4byte 0x0
 1944                 	.section .debug_pubtypes,info
 1945 0000 7F 00 00 00 	.4byte 0x7f
 1946 0004 02 00       	.2byte 0x2
 1947 0006 00 00 00 00 	.4byte .Ldebug_info0
 1948 000a F4 07 00 00 	.4byte 0x7f4
 1949 000e F1 00 00 00 	.4byte 0xf1
 1950 0012 75 69 6E 74 	.asciz "uint16_t"
 1950      31 36 5F 74 
 1950      00 
 1951 001b CA 01 00 00 	.4byte 0x1ca
 1952 001f 43 41 4E 64 	.asciz "CANdata"
 1952      61 74 61 00 
 1953 0027 16 02 00 00 	.4byte 0x216
 1954 002b 49 49 42 5F 	.asciz "IIB_speed"
 1954      73 70 65 65 
 1954      64 00 
 1955 0035 64 02 00 00 	.4byte 0x264
 1956 0039 49 49 42 5F 	.asciz "IIB_error"
 1956      65 72 72 6F 
 1956      72 00 
 1957 0043 09 03 00 00 	.4byte 0x309
 1958 0047 49 49 42 5F 	.asciz "IIB_values"
 1958      76 61 6C 75 
 1958      65 73 00 
 1959 0052 3A 05 00 00 	.4byte 0x53a
 1960 0056 49 49 42 5F 	.asciz "IIB_status"
 1960      73 74 61 74 
 1960      75 73 00 
 1961 0061 95 05 00 00 	.4byte 0x595
 1962 0065 49 49 42 5F 	.asciz "IIB_lims"
 1962      6C 69 6D 73 
 1962      00 
 1963 006e FF 05 00 00 	.4byte 0x5ff
 1964 0072 49 49 42 5F 	.asciz "IIB_CAN_data"
 1964      43 41 4E 5F 
 1964      64 61 74 61 
 1964      00 
 1965 007f 00 00 00 00 	.4byte 0x0
 1966                 	.section .debug_aranges,info
 1967 0000 14 00 00 00 	.4byte 0x14
 1968 0004 02 00       	.2byte 0x2
 1969 0006 00 00 00 00 	.4byte .Ldebug_info0
 1970 000a 04          	.byte 0x4
 1971 000b 00          	.byte 0x0
 1972 000c 00 00       	.2byte 0x0
 1973 000e 00 00       	.2byte 0x0
 1974 0010 00 00 00 00 	.4byte 0x0
MPLAB XC16 ASSEMBLY Listing:   			page 41


 1975 0014 00 00 00 00 	.4byte 0x0
 1976                 	.section .debug_str,info
 1977                 	.LASF0:
 1978 0000 64 61 74 61 	.asciz "data"
 1978      00 
 1979                 	.section .text,code
 1980              	
 1981              	
 1982              	
 1983              	.section __c30_info,info,bss
 1984                 	__psv_trap_errata:
 1985                 	
 1986                 	.section __c30_signature,info,data
 1987 0000 01 00       	.word 0x0001
 1988 0002 00 00       	.word 0x0000
 1989 0004 00 00       	.word 0x0000
 1990                 	
 1991                 	
 1992                 	
 1993                 	.set ___PA___,0
 1994                 	.end
MPLAB XC16 ASSEMBLY Listing:   			page 42


DEFINED SYMBOLS
                            *ABS*:00000000 /home/user/Documents/FST/Programming/project_sub-zero/lib/can-ids/Devices/IIB_CAN.c
                            *ABS*:00000001 __33EP256MU806
                            *ABS*:00000001 __dsPIC33E
                            *ABS*:00000001 __HAS_DSP
                            *ABS*:00000001 __HAS_DMAV2
                            *ABS*:00000001 __HAS_CODEGUARD
                            *ABS*:00000001 __HAS_PMP_ENHANCED
                            *ABS*:00000001 __HAS_EDS
                            *ABS*:00000001 __HAS_EP
                            *ABS*:00000011 __TARGET_DIVIDE_CYCLES
                            *ABS*:00000001 __C30ELF
    {standard input}:14     .text:00000000 _parse_IIB_speed_message
    {standard input}:18     *ABS*:00000000 ___PA___
    {standard input}:67     .text:00000032 _parse_IIB_error_message
    {standard input}:119    .text:00000064 _parse_IIB_values_message
    {standard input}:207    .text:000000c6 _parse_IIB_status_message
    {standard input}:598    .text:0000038a _parse_IIB_lims_message
    {standard input}:641    .text:000003b2 _parse_can_IIB
    {standard input}:668    *ABS*:00000000 ___BP___
    {standard input}:1984   __c30_info:00000000 __psv_trap_errata
    {standard input}:19     .text:00000000 .L0
                            .text:00000402 .L10
                            .text:000003e2 .L13
                            .text:000003f0 .L8
                            .text:000003f8 .L9
                            .text:0000041e .L6
                            .text:0000040c .L11
                            .text:00000416 .L12
                    .debug_abbrev:00000000 .Ldebug_abbrev0
                            .text:00000000 .Ltext0
                            .text:00000426 .Letext0
                      .debug_line:00000000 .Ldebug_line0
                       .debug_str:00000000 .LASF0
                            .text:00000000 .LFB0
                            .text:00000032 .LFE0
                            .text:00000032 .LFB1
                            .text:00000064 .LFE1
                            .text:00000064 .LFB2
                            .text:000000c6 .LFE2
                            .text:000000c6 .LFB3
                            .text:0000038a .LFE3
                            .text:0000038a .LFB4
                            .text:000003b2 .LFE4
                            .text:000003b2 .LFB5
                            .text:00000426 .LFE5
                     .debug_frame:00000000 .Lframe0
                      .debug_info:00000000 .Ldebug_info0

UNDEFINED SYMBOLS
CORCON

EQUATE SYMBOLS
/home/user/Documents/FST/Programming/project_sub-zero/lib/can-ids/Devices/IIB_CAN.c = 0x0
                  __33EP256MU806 = 0x1
                      __dsPIC33E = 0x1
                       __HAS_DSP = 0x1
                     __HAS_DMAV2 = 0x1
                 __HAS_CODEGUARD = 0x1
              __HAS_PMP_ENHANCED = 0x1
                       __HAS_EDS = 0x1
                        __HAS_EP = 0x1
          __TARGET_DIVIDE_CYCLES = 0x11
                        __C30ELF = 0x1
                        ___PA___ = 0x0
                        ___BP___ = 0x0
