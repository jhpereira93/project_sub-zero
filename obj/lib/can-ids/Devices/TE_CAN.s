MPLAB XC16 ASSEMBLY Listing:   			page 1


   1              	.file "/home/user/Documents/FST/Programming/project_sub-zero/lib/can-ids/Devices/TE_CAN.c"
   2              	.section .debug_abbrev,info
   3                 	.Ldebug_abbrev0:
   4                 	.section .debug_info,info
   5                 	.Ldebug_info0:
   6                 	.section .debug_line,info
   7                 	.Ldebug_line0:
   8 0000 01 01 00 00 	.section .text,code
   8      02 00 98 00 
   8      00 00 01 01 
   8      FB 0E 0A 00 
   8      01 01 01 01 
   8      00 00 00 01 
   8      6C 69 62 2F 
   8      63 61 6E 2D 
   8      69 64 73 2F 
   9              	.Ltext0:
  10              	.section .text,code
  11              	.align 2
  12              	.global _parse_te_main_message
  13              	.type _parse_te_main_message,@function
  14              	_parse_te_main_message:
  15              	.LFB0:
  16              	.file 1 "lib/can-ids/Devices/TE_CAN.c"
   1:lib/can-ids/Devices/TE_CAN.c **** #include <stdbool.h>
   2:lib/can-ids/Devices/TE_CAN.c **** #include <stdint.h>
   3:lib/can-ids/Devices/TE_CAN.c **** #include "can-ids/CAN_IDs.h"
   4:lib/can-ids/Devices/TE_CAN.c **** #include "TE_CAN.h"
   5:lib/can-ids/Devices/TE_CAN.c **** 
   6:lib/can-ids/Devices/TE_CAN.c **** void parse_te_main_message(uint16_t data[4], TE_MESSAGE *main_message)
   7:lib/can-ids/Devices/TE_CAN.c **** {
  17              	.loc 1 7 0
  18              	.set ___PA___,1
  19 000000  04 00 FA 	lnk #4
  20              	.LCFI0:
  21              	.loc 1 7 0
  22 000002  00 0F 78 	mov w0,[w14]
   8:lib/can-ids/Devices/TE_CAN.c **** 	main_message->status.TE_status	= data [0];
   9:lib/can-ids/Devices/TE_CAN.c **** 	main_message->APPS				= data [1];
  10:lib/can-ids/Devices/TE_CAN.c **** 	main_message->BPS_pressure		= data [2];
  23              	.loc 1 10 0
  24 000004  1E 01 78 	mov [w14],w2
  25 000006  E4 01 41 	add w2,#4,w3
  26              	.loc 1 8 0
  27 000008  1E 02 78 	mov [w14],w4
  28              	.loc 1 9 0
  29 00000a  1E 80 E8 	inc2 [w14],w0
  11:lib/can-ids/Devices/TE_CAN.c **** 	main_message->BPS_electric		= data [3];
  30              	.loc 1 11 0
  31 00000c  9E 02 78 	mov [w14],w5
  32 00000e  66 81 42 	add w5,#6,w2
  33              	.loc 1 7 0
  34 000010  11 07 98 	mov w1,[w14+2]
  35              	.loc 1 8 0
  36 000012  94 02 78 	mov [w4],w5
  37 000014  9E 00 90 	mov [w14+2],w1
  38              	.loc 1 9 0
MPLAB XC16 ASSEMBLY Listing:   			page 2


  39 000016  1E 02 90 	mov [w14+2],w4
  40              	.loc 1 8 0
  41 000018  85 08 78 	mov w5,[w1]
  42              	.loc 1 10 0
  43 00001a  9E 00 90 	mov [w14+2],w1
  44              	.loc 1 9 0
  45 00001c  90 02 78 	mov [w0],w5
  46              	.loc 1 11 0
  47 00001e  1E 00 90 	mov [w14+2],w0
  48              	.loc 1 9 0
  49 000020  25 02 98 	mov w5,[w4+4]
  50              	.loc 1 10 0
  51 000022  93 01 78 	mov [w3],w3
  52 000024  B3 00 98 	mov w3,[w1+6]
  53              	.loc 1 11 0
  54 000026  92 00 78 	mov [w2],w1
  55 000028  41 00 98 	mov w1,[w0+8]
  12:lib/can-ids/Devices/TE_CAN.c **** 
  13:lib/can-ids/Devices/TE_CAN.c **** 	return;
  14:lib/can-ids/Devices/TE_CAN.c **** }
  56              	.loc 1 14 0
  57 00002a  8E 07 78 	mov w14,w15
  58 00002c  4F 07 78 	mov [--w15],w14
  59 00002e  00 40 A9 	bclr CORCON,#2
  60 000030  00 00 06 	return 
  61              	.set ___PA___,0
  62              	.LFE0:
  63              	.size _parse_te_main_message,.-_parse_te_main_message
  64              	.align 2
  65              	.global _parse_te_verbose_1_message
  66              	.type _parse_te_verbose_1_message,@function
  67              	_parse_te_verbose_1_message:
  68              	.LFB1:
  15:lib/can-ids/Devices/TE_CAN.c **** 
  16:lib/can-ids/Devices/TE_CAN.c **** 
  17:lib/can-ids/Devices/TE_CAN.c **** /*
  18:lib/can-ids/Devices/TE_CAN.c **** void parse_te_debug_message(uint16_t data[4], TE_DEBUG_MESSAGE *debug_message)
  19:lib/can-ids/Devices/TE_CAN.c **** {
  20:lib/can-ids/Devices/TE_CAN.c **** 	debug_message->APPS_0			= data [0];
  21:lib/can-ids/Devices/TE_CAN.c **** 	debug_message->APPS_1			= data [1];
  22:lib/can-ids/Devices/TE_CAN.c **** 	debug_message->BPS_pressure_0	= data [2];
  23:lib/can-ids/Devices/TE_CAN.c **** 	debug_message->BPS_pressure_1	= data [3];
  24:lib/can-ids/Devices/TE_CAN.c **** 
  25:lib/can-ids/Devices/TE_CAN.c **** 	return;
  26:lib/can-ids/Devices/TE_CAN.c **** }
  27:lib/can-ids/Devices/TE_CAN.c **** */
  28:lib/can-ids/Devices/TE_CAN.c **** 
  29:lib/can-ids/Devices/TE_CAN.c **** void parse_te_verbose_1_message(uint16_t data[4], TE_VERBOSE_MESSAGE *verbose_message){
  69              	.loc 1 29 0
  70              	.set ___PA___,1
  71 000032  04 00 FA 	lnk #4
  72              	.LCFI1:
  73              	.loc 1 29 0
  74 000034  00 0F 78 	mov w0,[w14]
  30:lib/can-ids/Devices/TE_CAN.c **** 
  31:lib/can-ids/Devices/TE_CAN.c **** 	verbose_message->apps_0 	= data[0];
  32:lib/can-ids/Devices/TE_CAN.c **** 	verbose_message->apps_1 	= data[1];
MPLAB XC16 ASSEMBLY Listing:   			page 3


  33:lib/can-ids/Devices/TE_CAN.c **** 	verbose_message->pressure_0 = data[2];
  75              	.loc 1 33 0
  76 000036  1E 01 78 	mov [w14],w2
  77 000038  E4 01 41 	add w2,#4,w3
  78              	.loc 1 31 0
  79 00003a  1E 02 78 	mov [w14],w4
  80              	.loc 1 32 0
  81 00003c  1E 80 E8 	inc2 [w14],w0
  34:lib/can-ids/Devices/TE_CAN.c **** 	verbose_message->pressure_1 = data[3];
  82              	.loc 1 34 0
  83 00003e  9E 02 78 	mov [w14],w5
  84 000040  66 81 42 	add w5,#6,w2
  85              	.loc 1 29 0
  86 000042  11 07 98 	mov w1,[w14+2]
  87              	.loc 1 31 0
  88 000044  94 02 78 	mov [w4],w5
  89 000046  9E 00 90 	mov [w14+2],w1
  90              	.loc 1 32 0
  91 000048  1E 02 90 	mov [w14+2],w4
  92              	.loc 1 31 0
  93 00004a  85 08 78 	mov w5,[w1]
  94              	.loc 1 33 0
  95 00004c  9E 00 90 	mov [w14+2],w1
  96              	.loc 1 32 0
  97 00004e  90 02 78 	mov [w0],w5
  98              	.loc 1 34 0
  99 000050  1E 00 90 	mov [w14+2],w0
 100              	.loc 1 32 0
 101 000052  15 02 98 	mov w5,[w4+2]
 102              	.loc 1 33 0
 103 000054  93 01 78 	mov [w3],w3
 104 000056  B3 00 98 	mov w3,[w1+6]
 105              	.loc 1 34 0
 106 000058  92 00 78 	mov [w2],w1
 107 00005a  41 00 98 	mov w1,[w0+8]
  35:lib/can-ids/Devices/TE_CAN.c **** 
  36:lib/can-ids/Devices/TE_CAN.c **** 	return;
  37:lib/can-ids/Devices/TE_CAN.c **** }
 108              	.loc 1 37 0
 109 00005c  8E 07 78 	mov w14,w15
 110 00005e  4F 07 78 	mov [--w15],w14
 111 000060  00 40 A9 	bclr CORCON,#2
 112 000062  00 00 06 	return 
 113              	.set ___PA___,0
 114              	.LFE1:
 115              	.size _parse_te_verbose_1_message,.-_parse_te_verbose_1_message
 116              	.align 2
 117              	.global _parse_te_verbose_2_message
 118              	.type _parse_te_verbose_2_message,@function
 119              	_parse_te_verbose_2_message:
 120              	.LFB2:
  38:lib/can-ids/Devices/TE_CAN.c **** 
  39:lib/can-ids/Devices/TE_CAN.c **** 
  40:lib/can-ids/Devices/TE_CAN.c **** 
  41:lib/can-ids/Devices/TE_CAN.c **** void parse_te_verbose_2_message(uint16_t data[4], TE_VERBOSE_MESSAGE *verbose_message){
 121              	.loc 1 41 0
 122              	.set ___PA___,1
MPLAB XC16 ASSEMBLY Listing:   			page 4


 123 000064  04 00 FA 	lnk #4
 124              	.LCFI2:
 125              	.loc 1 41 0
 126 000066  00 0F 78 	mov w0,[w14]
  42:lib/can-ids/Devices/TE_CAN.c **** 
  43:lib/can-ids/Devices/TE_CAN.c **** 	verbose_message->electric = data[0];
  44:lib/can-ids/Devices/TE_CAN.c **** 	verbose_message->time_imp = data[1];
  45:lib/can-ids/Devices/TE_CAN.c **** 	verbose_message->apps_imp_flag = data[2];
 127              	.loc 1 45 0
 128 000068  9E 01 78 	mov [w14],w3
 129 00006a  64 81 41 	add w3,#4,w2
 130              	.loc 1 43 0
 131 00006c  1E 00 78 	mov [w14],w0
 132              	.loc 1 44 0
 133 00006e  9E 81 E8 	inc2 [w14],w3
 134              	.loc 1 41 0
 135 000070  11 07 98 	mov w1,[w14+2]
 136              	.loc 1 43 0
 137 000072  10 02 78 	mov [w0],w4
 138 000074  1E 00 90 	mov [w14+2],w0
 139              	.loc 1 44 0
 140 000076  9E 00 90 	mov [w14+2],w1
 141              	.loc 1 43 0
 142 000078  24 00 98 	mov w4,[w0+4]
 143              	.loc 1 45 0
 144 00007a  1E 00 90 	mov [w14+2],w0
 145              	.loc 1 44 0
 146 00007c  93 01 78 	mov [w3],w3
 147 00007e  D3 00 98 	mov w3,[w1+10]
 148              	.loc 1 45 0
 149 000080  92 00 78 	mov [w2],w1
 150 000082  01 F0 A7 	btsc w1,#15
 151 000084  81 00 EA 	neg w1,w1
 152 000086  81 00 EA 	neg w1,w1
 153 000088  CF 08 DE 	lsr w1,#15,w1
 154 00008a  81 40 78 	mov.b w1,w1
 155 00008c  41 48 98 	mov.b w1,[w0+12]
  46:lib/can-ids/Devices/TE_CAN.c **** 	verbose_message->apps_timer_exceed_flag=data[3];
 156              	.loc 1 46 0
 157 00008e  1E 01 78 	mov [w14],w2
 158 000090  E6 00 41 	add w2,#6,w1
 159 000092  1E 00 90 	mov [w14+2],w0
 160 000094  00 00 00 	nop 
 161 000096  91 00 78 	mov [w1],w1
 162 000098  01 F0 A7 	btsc w1,#15
 163 00009a  81 00 EA 	neg w1,w1
 164 00009c  81 00 EA 	neg w1,w1
 165 00009e  CF 08 DE 	lsr w1,#15,w1
 166 0000a0  00 00 00 	nop 
 167 0000a2  81 40 78 	mov.b w1,w1
 168 0000a4  00 00 00 	nop 
 169 0000a6  51 48 98 	mov.b w1,[w0+13]
  47:lib/can-ids/Devices/TE_CAN.c **** 
  48:lib/can-ids/Devices/TE_CAN.c **** 	return;
  49:lib/can-ids/Devices/TE_CAN.c **** }
 170              	.loc 1 49 0
 171 0000a8  8E 07 78 	mov w14,w15
MPLAB XC16 ASSEMBLY Listing:   			page 5


 172 0000aa  4F 07 78 	mov [--w15],w14
 173 0000ac  00 40 A9 	bclr CORCON,#2
 174 0000ae  00 00 06 	return 
 175              	.set ___PA___,0
 176              	.LFE2:
 177              	.size _parse_te_verbose_2_message,.-_parse_te_verbose_2_message
 178              	.align 2
 179              	.global _parse_can_te_limit
 180              	.type _parse_can_te_limit,@function
 181              	_parse_can_te_limit:
 182              	.LFB3:
  50:lib/can-ids/Devices/TE_CAN.c **** 
  51:lib/can-ids/Devices/TE_CAN.c **** 
  52:lib/can-ids/Devices/TE_CAN.c **** void parse_can_te_limit(CANdata message, TE_LIMIT *limit_message){
 183              	.loc 1 52 0
 184              	.set ___PA___,1
 185 0000b0  0E 00 FA 	lnk #14
 186              	.LCFI3:
 187              	.loc 1 52 0
 188 0000b2  00 0F 78 	mov w0,[w14]
 189 0000b4  66 07 98 	mov w6,[w14+12]
 190 0000b6  22 07 98 	mov w2,[w14+4]
 191 0000b8  33 07 98 	mov w3,[w14+6]
  53:lib/can-ids/Devices/TE_CAN.c **** 	limit_message->header = message.data[0];
 192              	.loc 1 53 0
 193 0000ba  2E 03 90 	mov [w14+4],w6
 194 0000bc  EE 01 90 	mov [w14+12],w3
  54:lib/can-ids/Devices/TE_CAN.c **** 	limit_message->data_limit= message.data[1];
 195              	.loc 1 54 0
 196 0000be  3E 01 90 	mov [w14+6],w2
 197 0000c0  6E 00 90 	mov [w14+12],w0
 198              	.loc 1 52 0
 199 0000c2  11 07 98 	mov w1,[w14+2]
 200 0000c4  44 07 98 	mov w4,[w14+8]
 201 0000c6  55 07 98 	mov w5,[w14+10]
 202              	.loc 1 53 0
 203 0000c8  86 09 78 	mov w6,[w3]
 204              	.loc 1 54 0
 205 0000ca  12 00 98 	mov w2,[w0+2]
  55:lib/can-ids/Devices/TE_CAN.c **** }
 206              	.loc 1 55 0
 207 0000cc  8E 07 78 	mov w14,w15
 208 0000ce  4F 07 78 	mov [--w15],w14
 209 0000d0  00 40 A9 	bclr CORCON,#2
 210 0000d2  00 00 06 	return 
 211              	.set ___PA___,0
 212              	.LFE3:
 213              	.size _parse_can_te_limit,.-_parse_can_te_limit
 214              	.align 2
 215              	.global _parse_can_te
 216              	.type _parse_can_te,@function
 217              	_parse_can_te:
 218              	.LFB4:
  56:lib/can-ids/Devices/TE_CAN.c **** 
  57:lib/can-ids/Devices/TE_CAN.c **** 
  58:lib/can-ids/Devices/TE_CAN.c **** 
  59:lib/can-ids/Devices/TE_CAN.c **** void parse_can_te(CANdata message, TE_CAN_Data *data)
MPLAB XC16 ASSEMBLY Listing:   			page 6


  60:lib/can-ids/Devices/TE_CAN.c **** {
 219              	.loc 1 60 0
 220              	.set ___PA___,1
 221 0000d4  0E 00 FA 	lnk #14
 222              	.LCFI4:
 223              	.loc 1 60 0
 224 0000d6  00 0F 78 	mov w0,[w14]
 225 0000d8  11 07 98 	mov w1,[w14+2]
  61:lib/can-ids/Devices/TE_CAN.c **** 	if (message.dev_id != DEVICE_ID_TE) {
 226              	.loc 1 61 0
 227 0000da  1E 00 78 	mov [w14],w0
 228              	.loc 1 60 0
 229 0000dc  22 07 98 	mov w2,[w14+4]
 230 0000de  33 07 98 	mov w3,[w14+6]
 231 0000e0  44 07 98 	mov w4,[w14+8]
 232 0000e2  55 07 98 	mov w5,[w14+10]
 233 0000e4  66 07 98 	mov w6,[w14+12]
 234              	.loc 1 61 0
 235 0000e6  7F 00 60 	and w0,#31,w0
 236 0000e8  E9 0F 50 	sub w0,#9,[w15]
 237              	.set ___BP___,0
 238 0000ea  00 00 3A 	bra nz,.L14
 239              	.L6:
  62:lib/can-ids/Devices/TE_CAN.c **** 		return;
  63:lib/can-ids/Devices/TE_CAN.c **** 	}
  64:lib/can-ids/Devices/TE_CAN.c **** 
  65:lib/can-ids/Devices/TE_CAN.c **** 	switch (message.msg_id) {
 240              	.loc 1 65 0
 241 0000ec  1E 00 78 	mov [w14],w0
 242 0000ee  21 02 20 	mov #34,w1
 243 0000f0  45 00 DE 	lsr w0,#5,w0
 244 0000f2  F0 43 B2 	and.b #63,w0
 245 0000f4  00 80 FB 	ze w0,w0
 246 0000f6  81 0F 50 	sub w0,w1,[w15]
 247              	.set ___BP___,0
 248 0000f8  00 00 32 	bra z,.L11
 249 0000fa  21 02 20 	mov #34,w1
 250 0000fc  81 0F 50 	sub w0,w1,[w15]
 251              	.set ___BP___,0
 252 0000fe  00 00 3C 	bra gt,.L13
 253 000100  EA 0F 50 	sub w0,#10,[w15]
 254              	.set ___BP___,0
 255 000102  00 00 32 	bra z,.L9
 256 000104  11 02 20 	mov #33,w1
 257 000106  81 0F 50 	sub w0,w1,[w15]
 258              	.set ___BP___,0
 259 000108  00 00 32 	bra z,.L10
  66:lib/can-ids/Devices/TE_CAN.c **** 		case MSG_ID_TE_MAIN:
  67:lib/can-ids/Devices/TE_CAN.c **** 			parse_te_main_message(message.data, &(data->main_message));
  68:lib/can-ids/Devices/TE_CAN.c **** 			break;
  69:lib/can-ids/Devices/TE_CAN.c **** 		//case MSG_ID_TE_DEBUG:
  70:lib/can-ids/Devices/TE_CAN.c **** 			// parse_te_debug_message(message.data, &(data->debug_message));
  71:lib/can-ids/Devices/TE_CAN.c **** 			// break;
  72:lib/can-ids/Devices/TE_CAN.c **** 		case MSG_ID_TE_VERBOSE_1:
  73:lib/can-ids/Devices/TE_CAN.c **** 			parse_te_verbose_1_message(message.data, &data->verbose_message);
  74:lib/can-ids/Devices/TE_CAN.c **** 			break;
  75:lib/can-ids/Devices/TE_CAN.c **** 		case MSG_ID_TE_VERBOSE_2:
MPLAB XC16 ASSEMBLY Listing:   			page 7


  76:lib/can-ids/Devices/TE_CAN.c **** 			parse_te_verbose_2_message(message.data, &data->verbose_message);
  77:lib/can-ids/Devices/TE_CAN.c **** 			break;
  78:lib/can-ids/Devices/TE_CAN.c ****         case MSG_ID_TE_LIMIT_ANSWER:
  79:lib/can-ids/Devices/TE_CAN.c **** 			parse_can_te_limit(message, &(data->limit_message));
  80:lib/can-ids/Devices/TE_CAN.c **** 	}
  81:lib/can-ids/Devices/TE_CAN.c **** 	return;
 260              	.loc 1 81 0
 261 00010a  00 00 37 	bra .L5
 262              	.L13:
 263              	.loc 1 65 0
 264 00010c  31 02 20 	mov #35,w1
 265 00010e  81 0F 50 	sub w0,w1,[w15]
 266              	.set ___BP___,0
 267 000110  00 00 32 	bra z,.L12
 268              	.loc 1 81 0
 269 000112  00 00 37 	bra .L5
 270              	.L9:
 271              	.loc 1 67 0
 272 000114  EE 00 90 	mov [w14+12],w1
 273 000116  64 00 47 	add w14,#4,w0
 274 000118  00 00 07 	rcall _parse_te_main_message
 275              	.loc 1 68 0
 276 00011a  00 00 37 	bra .L8
 277              	.L10:
 278              	.loc 1 73 0
 279 00011c  EE 00 90 	mov [w14+12],w1
 280 00011e  64 00 47 	add w14,#4,w0
 281 000120  EA 80 40 	add w1,#10,w1
 282 000122  00 00 07 	rcall _parse_te_verbose_1_message
 283              	.loc 1 74 0
 284 000124  00 00 37 	bra .L8
 285              	.L11:
 286              	.loc 1 76 0
 287 000126  EE 00 90 	mov [w14+12],w1
 288 000128  64 00 47 	add w14,#4,w0
 289 00012a  EA 80 40 	add w1,#10,w1
 290 00012c  00 00 07 	rcall _parse_te_verbose_2_message
 291              	.loc 1 77 0
 292 00012e  00 00 37 	bra .L8
 293              	.L12:
 294              	.loc 1 79 0
 295 000130  6E 00 90 	mov [w14+12],w0
 296 000132  9E 00 90 	mov [w14+2],w1
 297 000134  78 01 40 	add w0,#24,w2
 298 000136  1E 00 78 	mov [w14],w0
 299 000138  02 03 78 	mov w2,w6
 300 00013a  2E 01 90 	mov [w14+4],w2
 301 00013c  BE 01 90 	mov [w14+6],w3
 302 00013e  4E 02 90 	mov [w14+8],w4
 303 000140  DE 02 90 	mov [w14+10],w5
 304 000142  00 00 07 	rcall _parse_can_te_limit
 305              	.loc 1 81 0
 306 000144  00 00 37 	bra .L5
 307              	.L8:
 308 000146  00 00 37 	bra .L5
 309              	.L14:
 310              	.L5:
MPLAB XC16 ASSEMBLY Listing:   			page 8


  82:lib/can-ids/Devices/TE_CAN.c **** }
 311              	.loc 1 82 0
 312 000148  8E 07 78 	mov w14,w15
 313 00014a  4F 07 78 	mov [--w15],w14
 314 00014c  00 40 A9 	bclr CORCON,#2
 315 00014e  00 00 06 	return 
 316              	.set ___PA___,0
 317              	.LFE4:
 318              	.size _parse_can_te,.-_parse_can_te
 319              	.section .debug_frame,info
 320                 	.Lframe0:
 321 0000 10 00 00 00 	.4byte .LECIE0-.LSCIE0
 322                 	.LSCIE0:
 323 0004 FF FF FF FF 	.4byte 0xffffffff
 324 0008 01          	.byte 0x1
 325 0009 00          	.byte 0
 326 000a 01          	.uleb128 0x1
 327 000b 02          	.sleb128 2
 328 000c 25          	.byte 0x25
 329 000d 12          	.byte 0x12
 330 000e 0F          	.uleb128 0xf
 331 000f 7E          	.sleb128 -2
 332 0010 09          	.byte 0x9
 333 0011 25          	.uleb128 0x25
 334 0012 0F          	.uleb128 0xf
 335 0013 00          	.align 4
 336                 	.LECIE0:
 337                 	.LSFDE0:
 338 0014 18 00 00 00 	.4byte .LEFDE0-.LASFDE0
 339                 	.LASFDE0:
 340 0018 00 00 00 00 	.4byte .Lframe0
 341 001c 00 00 00 00 	.4byte .LFB0
 342 0020 32 00 00 00 	.4byte .LFE0-.LFB0
 343 0024 04          	.byte 0x4
 344 0025 02 00 00 00 	.4byte .LCFI0-.LFB0
 345 0029 13          	.byte 0x13
 346 002a 7D          	.sleb128 -3
 347 002b 0D          	.byte 0xd
 348 002c 0E          	.uleb128 0xe
 349 002d 8E          	.byte 0x8e
 350 002e 02          	.uleb128 0x2
 351 002f 00          	.align 4
 352                 	.LEFDE0:
 353                 	.LSFDE2:
 354 0030 18 00 00 00 	.4byte .LEFDE2-.LASFDE2
 355                 	.LASFDE2:
 356 0034 00 00 00 00 	.4byte .Lframe0
 357 0038 00 00 00 00 	.4byte .LFB1
 358 003c 32 00 00 00 	.4byte .LFE1-.LFB1
 359 0040 04          	.byte 0x4
 360 0041 02 00 00 00 	.4byte .LCFI1-.LFB1
 361 0045 13          	.byte 0x13
 362 0046 7D          	.sleb128 -3
 363 0047 0D          	.byte 0xd
 364 0048 0E          	.uleb128 0xe
 365 0049 8E          	.byte 0x8e
 366 004a 02          	.uleb128 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 9


 367 004b 00          	.align 4
 368                 	.LEFDE2:
 369                 	.LSFDE4:
 370 004c 18 00 00 00 	.4byte .LEFDE4-.LASFDE4
 371                 	.LASFDE4:
 372 0050 00 00 00 00 	.4byte .Lframe0
 373 0054 00 00 00 00 	.4byte .LFB2
 374 0058 4C 00 00 00 	.4byte .LFE2-.LFB2
 375 005c 04          	.byte 0x4
 376 005d 02 00 00 00 	.4byte .LCFI2-.LFB2
 377 0061 13          	.byte 0x13
 378 0062 7D          	.sleb128 -3
 379 0063 0D          	.byte 0xd
 380 0064 0E          	.uleb128 0xe
 381 0065 8E          	.byte 0x8e
 382 0066 02          	.uleb128 0x2
 383 0067 00          	.align 4
 384                 	.LEFDE4:
 385                 	.LSFDE6:
 386 0068 18 00 00 00 	.4byte .LEFDE6-.LASFDE6
 387                 	.LASFDE6:
 388 006c 00 00 00 00 	.4byte .Lframe0
 389 0070 00 00 00 00 	.4byte .LFB3
 390 0074 24 00 00 00 	.4byte .LFE3-.LFB3
 391 0078 04          	.byte 0x4
 392 0079 02 00 00 00 	.4byte .LCFI3-.LFB3
 393 007d 13          	.byte 0x13
 394 007e 7D          	.sleb128 -3
 395 007f 0D          	.byte 0xd
 396 0080 0E          	.uleb128 0xe
 397 0081 8E          	.byte 0x8e
 398 0082 02          	.uleb128 0x2
 399 0083 00          	.align 4
 400                 	.LEFDE6:
 401                 	.LSFDE8:
 402 0084 18 00 00 00 	.4byte .LEFDE8-.LASFDE8
 403                 	.LASFDE8:
 404 0088 00 00 00 00 	.4byte .Lframe0
 405 008c 00 00 00 00 	.4byte .LFB4
 406 0090 7C 00 00 00 	.4byte .LFE4-.LFB4
 407 0094 04          	.byte 0x4
 408 0095 02 00 00 00 	.4byte .LCFI4-.LFB4
 409 0099 13          	.byte 0x13
 410 009a 7D          	.sleb128 -3
 411 009b 0D          	.byte 0xd
 412 009c 0E          	.uleb128 0xe
 413 009d 8E          	.byte 0x8e
 414 009e 02          	.uleb128 0x2
 415 009f 00          	.align 4
 416                 	.LEFDE8:
 417                 	.section .text,code
 418              	.Letext0:
 419              	.file 2 "lib/can-ids/CAN_IDs.h"
 420              	.file 3 "/opt/microchip/xc16/v1.36/bin/bin/../../include/lega-c/stdint.h"
 421              	.file 4 "lib/can-ids/Devices/TE_CAN.h"
 422              	.section .debug_info,info
 423 0000 69 07 00 00 	.4byte 0x769
MPLAB XC16 ASSEMBLY Listing:   			page 10


 424 0004 02 00       	.2byte 0x2
 425 0006 00 00 00 00 	.4byte .Ldebug_abbrev0
 426 000a 04          	.byte 0x4
 427 000b 01          	.uleb128 0x1
 428 000c 47 4E 55 20 	.asciz "GNU C 4.5.1 (XC16, Microchip v1.36) (A) Build date: Dec  5 2018"
 428      43 20 34 2E 
 428      35 2E 31 20 
 428      28 58 43 31 
 428      36 2C 20 4D 
 428      69 63 72 6F 
 428      63 68 69 70 
 428      20 76 31 2E 
 428      33 36 29 20 
 429 004c 01          	.byte 0x1
 430 004d 6C 69 62 2F 	.asciz "lib/can-ids/Devices/TE_CAN.c"
 430      63 61 6E 2D 
 430      69 64 73 2F 
 430      44 65 76 69 
 430      63 65 73 2F 
 430      54 45 5F 43 
 430      41 4E 2E 63 
 430      00 
 431 006a 2F 68 6F 6D 	.asciz "/home/user/Documents/FST/Programming/project_sub-zero"
 431      65 2F 75 73 
 431      65 72 2F 44 
 431      6F 63 75 6D 
 431      65 6E 74 73 
 431      2F 46 53 54 
 431      2F 50 72 6F 
 431      67 72 61 6D 
 431      6D 69 6E 67 
 432 00a0 00 00 00 00 	.4byte .Ltext0
 433 00a4 00 00 00 00 	.4byte .Letext0
 434 00a8 00 00 00 00 	.4byte .Ldebug_line0
 435 00ac 02          	.uleb128 0x2
 436 00ad 01          	.byte 0x1
 437 00ae 06          	.byte 0x6
 438 00af 73 69 67 6E 	.asciz "signed char"
 438      65 64 20 63 
 438      68 61 72 00 
 439 00bb 02          	.uleb128 0x2
 440 00bc 02          	.byte 0x2
 441 00bd 05          	.byte 0x5
 442 00be 69 6E 74 00 	.asciz "int"
 443 00c2 02          	.uleb128 0x2
 444 00c3 04          	.byte 0x4
 445 00c4 05          	.byte 0x5
 446 00c5 6C 6F 6E 67 	.asciz "long int"
 446      20 69 6E 74 
 446      00 
 447 00ce 02          	.uleb128 0x2
 448 00cf 08          	.byte 0x8
 449 00d0 05          	.byte 0x5
 450 00d1 6C 6F 6E 67 	.asciz "long long int"
 450      20 6C 6F 6E 
 450      67 20 69 6E 
 450      74 00 
MPLAB XC16 ASSEMBLY Listing:   			page 11


 451 00df 02          	.uleb128 0x2
 452 00e0 01          	.byte 0x1
 453 00e1 08          	.byte 0x8
 454 00e2 75 6E 73 69 	.asciz "unsigned char"
 454      67 6E 65 64 
 454      20 63 68 61 
 454      72 00 
 455 00f0 03          	.uleb128 0x3
 456 00f1 75 69 6E 74 	.asciz "uint16_t"
 456      31 36 5F 74 
 456      00 
 457 00fa 03          	.byte 0x3
 458 00fb 31          	.byte 0x31
 459 00fc 00 01 00 00 	.4byte 0x100
 460 0100 02          	.uleb128 0x2
 461 0101 02          	.byte 0x2
 462 0102 07          	.byte 0x7
 463 0103 75 6E 73 69 	.asciz "unsigned int"
 463      67 6E 65 64 
 463      20 69 6E 74 
 463      00 
 464 0110 02          	.uleb128 0x2
 465 0111 04          	.byte 0x4
 466 0112 07          	.byte 0x7
 467 0113 6C 6F 6E 67 	.asciz "long unsigned int"
 467      20 75 6E 73 
 467      69 67 6E 65 
 467      64 20 69 6E 
 467      74 00 
 468 0125 02          	.uleb128 0x2
 469 0126 08          	.byte 0x8
 470 0127 07          	.byte 0x7
 471 0128 6C 6F 6E 67 	.asciz "long long unsigned int"
 471      20 6C 6F 6E 
 471      67 20 75 6E 
 471      73 69 67 6E 
 471      65 64 20 69 
 471      6E 74 00 
 472 013f 04          	.uleb128 0x4
 473 0140 02          	.byte 0x2
 474 0141 02          	.byte 0x2
 475 0142 12          	.byte 0x12
 476 0143 70 01 00 00 	.4byte 0x170
 477 0147 05          	.uleb128 0x5
 478 0148 64 65 76 5F 	.asciz "dev_id"
 478      69 64 00 
 479 014f 02          	.byte 0x2
 480 0150 13          	.byte 0x13
 481 0151 F0 00 00 00 	.4byte 0xf0
 482 0155 02          	.byte 0x2
 483 0156 05          	.byte 0x5
 484 0157 0B          	.byte 0xb
 485 0158 02          	.byte 0x2
 486 0159 23          	.byte 0x23
 487 015a 00          	.uleb128 0x0
 488 015b 05          	.uleb128 0x5
 489 015c 6D 73 67 5F 	.asciz "msg_id"
MPLAB XC16 ASSEMBLY Listing:   			page 12


 489      69 64 00 
 490 0163 02          	.byte 0x2
 491 0164 16          	.byte 0x16
 492 0165 F0 00 00 00 	.4byte 0xf0
 493 0169 02          	.byte 0x2
 494 016a 06          	.byte 0x6
 495 016b 05          	.byte 0x5
 496 016c 02          	.byte 0x2
 497 016d 23          	.byte 0x23
 498 016e 00          	.uleb128 0x0
 499 016f 00          	.byte 0x0
 500 0170 06          	.uleb128 0x6
 501 0171 02          	.byte 0x2
 502 0172 02          	.byte 0x2
 503 0173 11          	.byte 0x11
 504 0174 89 01 00 00 	.4byte 0x189
 505 0178 07          	.uleb128 0x7
 506 0179 3F 01 00 00 	.4byte 0x13f
 507 017d 08          	.uleb128 0x8
 508 017e 73 69 64 00 	.asciz "sid"
 509 0182 02          	.byte 0x2
 510 0183 18          	.byte 0x18
 511 0184 F0 00 00 00 	.4byte 0xf0
 512 0188 00          	.byte 0x0
 513 0189 04          	.uleb128 0x4
 514 018a 0C          	.byte 0xc
 515 018b 02          	.byte 0x2
 516 018c 10          	.byte 0x10
 517 018d BA 01 00 00 	.4byte 0x1ba
 518 0191 09          	.uleb128 0x9
 519 0192 70 01 00 00 	.4byte 0x170
 520 0196 02          	.byte 0x2
 521 0197 23          	.byte 0x23
 522 0198 00          	.uleb128 0x0
 523 0199 05          	.uleb128 0x5
 524 019a 64 6C 63 00 	.asciz "dlc"
 525 019e 02          	.byte 0x2
 526 019f 1A          	.byte 0x1a
 527 01a0 F0 00 00 00 	.4byte 0xf0
 528 01a4 02          	.byte 0x2
 529 01a5 04          	.byte 0x4
 530 01a6 0C          	.byte 0xc
 531 01a7 02          	.byte 0x2
 532 01a8 23          	.byte 0x23
 533 01a9 02          	.uleb128 0x2
 534 01aa 0A          	.uleb128 0xa
 535 01ab 64 61 74 61 	.asciz "data"
 535      00 
 536 01b0 02          	.byte 0x2
 537 01b1 1B          	.byte 0x1b
 538 01b2 BA 01 00 00 	.4byte 0x1ba
 539 01b6 02          	.byte 0x2
 540 01b7 23          	.byte 0x23
 541 01b8 04          	.uleb128 0x4
 542 01b9 00          	.byte 0x0
 543 01ba 0B          	.uleb128 0xb
 544 01bb F0 00 00 00 	.4byte 0xf0
MPLAB XC16 ASSEMBLY Listing:   			page 13


 545 01bf CA 01 00 00 	.4byte 0x1ca
 546 01c3 0C          	.uleb128 0xc
 547 01c4 00 01 00 00 	.4byte 0x100
 548 01c8 03          	.byte 0x3
 549 01c9 00          	.byte 0x0
 550 01ca 03          	.uleb128 0x3
 551 01cb 43 41 4E 64 	.asciz "CANdata"
 551      61 74 61 00 
 552 01d3 02          	.byte 0x2
 553 01d4 1C          	.byte 0x1c
 554 01d5 89 01 00 00 	.4byte 0x189
 555 01d9 04          	.uleb128 0x4
 556 01da 02          	.byte 0x2
 557 01db 04          	.byte 0x4
 558 01dc 1C          	.byte 0x1c
 559 01dd 8B 03 00 00 	.4byte 0x38b
 560 01e1 05          	.uleb128 0x5
 561 01e2 43 43 5F 41 	.asciz "CC_APPS_0"
 561      50 50 53 5F 
 561      30 00 
 562 01ec 04          	.byte 0x4
 563 01ed 1D          	.byte 0x1d
 564 01ee 8B 03 00 00 	.4byte 0x38b
 565 01f2 01          	.byte 0x1
 566 01f3 01          	.byte 0x1
 567 01f4 07          	.byte 0x7
 568 01f5 02          	.byte 0x2
 569 01f6 23          	.byte 0x23
 570 01f7 00          	.uleb128 0x0
 571 01f8 05          	.uleb128 0x5
 572 01f9 43 43 5F 41 	.asciz "CC_APPS_1"
 572      50 50 53 5F 
 572      31 00 
 573 0203 04          	.byte 0x4
 574 0204 1E          	.byte 0x1e
 575 0205 8B 03 00 00 	.4byte 0x38b
 576 0209 01          	.byte 0x1
 577 020a 01          	.byte 0x1
 578 020b 06          	.byte 0x6
 579 020c 02          	.byte 0x2
 580 020d 23          	.byte 0x23
 581 020e 00          	.uleb128 0x0
 582 020f 05          	.uleb128 0x5
 583 0210 43 43 5F 42 	.asciz "CC_BPS_electric_0"
 583      50 53 5F 65 
 583      6C 65 63 74 
 583      72 69 63 5F 
 583      30 00 
 584 0222 04          	.byte 0x4
 585 0223 1F          	.byte 0x1f
 586 0224 8B 03 00 00 	.4byte 0x38b
 587 0228 01          	.byte 0x1
 588 0229 01          	.byte 0x1
 589 022a 05          	.byte 0x5
 590 022b 02          	.byte 0x2
 591 022c 23          	.byte 0x23
 592 022d 00          	.uleb128 0x0
MPLAB XC16 ASSEMBLY Listing:   			page 14


 593 022e 05          	.uleb128 0x5
 594 022f 43 43 5F 42 	.asciz "CC_BPS_pressure_0"
 594      50 53 5F 70 
 594      72 65 73 73 
 594      75 72 65 5F 
 594      30 00 
 595 0241 04          	.byte 0x4
 596 0242 20          	.byte 0x20
 597 0243 8B 03 00 00 	.4byte 0x38b
 598 0247 01          	.byte 0x1
 599 0248 01          	.byte 0x1
 600 0249 04          	.byte 0x4
 601 024a 02          	.byte 0x2
 602 024b 23          	.byte 0x23
 603 024c 00          	.uleb128 0x0
 604 024d 05          	.uleb128 0x5
 605 024e 43 43 5F 42 	.asciz "CC_BPS_pressure_1"
 605      50 53 5F 70 
 605      72 65 73 73 
 605      75 72 65 5F 
 605      31 00 
 606 0260 04          	.byte 0x4
 607 0261 21          	.byte 0x21
 608 0262 8B 03 00 00 	.4byte 0x38b
 609 0266 01          	.byte 0x1
 610 0267 01          	.byte 0x1
 611 0268 03          	.byte 0x3
 612 0269 02          	.byte 0x2
 613 026a 23          	.byte 0x23
 614 026b 00          	.uleb128 0x0
 615 026c 05          	.uleb128 0x5
 616 026d 4F 76 65 72 	.asciz "Overshoot_APPS_0"
 616      73 68 6F 6F 
 616      74 5F 41 50 
 616      50 53 5F 30 
 616      00 
 617 027e 04          	.byte 0x4
 618 027f 22          	.byte 0x22
 619 0280 8B 03 00 00 	.4byte 0x38b
 620 0284 01          	.byte 0x1
 621 0285 01          	.byte 0x1
 622 0286 02          	.byte 0x2
 623 0287 02          	.byte 0x2
 624 0288 23          	.byte 0x23
 625 0289 00          	.uleb128 0x0
 626 028a 05          	.uleb128 0x5
 627 028b 4F 76 65 72 	.asciz "Overshoot_APPS_1"
 627      73 68 6F 6F 
 627      74 5F 41 50 
 627      50 53 5F 31 
 627      00 
 628 029c 04          	.byte 0x4
 629 029d 23          	.byte 0x23
 630 029e 8B 03 00 00 	.4byte 0x38b
 631 02a2 01          	.byte 0x1
 632 02a3 01          	.byte 0x1
 633 02a4 01          	.byte 0x1
MPLAB XC16 ASSEMBLY Listing:   			page 15


 634 02a5 02          	.byte 0x2
 635 02a6 23          	.byte 0x23
 636 02a7 00          	.uleb128 0x0
 637 02a8 05          	.uleb128 0x5
 638 02a9 4F 76 65 72 	.asciz "Overshoot_BPS_electric"
 638      73 68 6F 6F 
 638      74 5F 42 50 
 638      53 5F 65 6C 
 638      65 63 74 72 
 638      69 63 00 
 639 02c0 04          	.byte 0x4
 640 02c1 24          	.byte 0x24
 641 02c2 8B 03 00 00 	.4byte 0x38b
 642 02c6 01          	.byte 0x1
 643 02c7 01          	.byte 0x1
 644 02c8 08          	.byte 0x8
 645 02c9 02          	.byte 0x2
 646 02ca 23          	.byte 0x23
 647 02cb 00          	.uleb128 0x0
 648 02cc 05          	.uleb128 0x5
 649 02cd 49 6D 70 6C 	.asciz "Implausibility_APPS_Timer_Exceeded"
 649      61 75 73 69 
 649      62 69 6C 69 
 649      74 79 5F 41 
 649      50 50 53 5F 
 649      54 69 6D 65 
 649      72 5F 45 78 
 649      63 65 65 64 
 649      65 64 00 
 650 02f0 04          	.byte 0x4
 651 02f1 28          	.byte 0x28
 652 02f2 8B 03 00 00 	.4byte 0x38b
 653 02f6 01          	.byte 0x1
 654 02f7 01          	.byte 0x1
 655 02f8 07          	.byte 0x7
 656 02f9 02          	.byte 0x2
 657 02fa 23          	.byte 0x23
 658 02fb 01          	.uleb128 0x1
 659 02fc 05          	.uleb128 0x5
 660 02fd 49 6D 70 6C 	.asciz "Implausibility_APPS_BPS_Timer_Exceeded"
 660      61 75 73 69 
 660      62 69 6C 69 
 660      74 79 5F 41 
 660      50 50 53 5F 
 660      42 50 53 5F 
 660      54 69 6D 65 
 660      72 5F 45 78 
 660      63 65 65 64 
 661 0324 04          	.byte 0x4
 662 0325 29          	.byte 0x29
 663 0326 8B 03 00 00 	.4byte 0x38b
 664 032a 01          	.byte 0x1
 665 032b 01          	.byte 0x1
 666 032c 06          	.byte 0x6
 667 032d 02          	.byte 0x2
 668 032e 23          	.byte 0x23
 669 032f 01          	.uleb128 0x1
MPLAB XC16 ASSEMBLY Listing:   			page 16


 670 0330 05          	.uleb128 0x5
 671 0331 48 61 72 64 	.asciz "Hard_braking"
 671      5F 62 72 61 
 671      6B 69 6E 67 
 671      00 
 672 033e 04          	.byte 0x4
 673 033f 2A          	.byte 0x2a
 674 0340 8B 03 00 00 	.4byte 0x38b
 675 0344 01          	.byte 0x1
 676 0345 01          	.byte 0x1
 677 0346 05          	.byte 0x5
 678 0347 02          	.byte 0x2
 679 0348 23          	.byte 0x23
 680 0349 01          	.uleb128 0x1
 681 034a 05          	.uleb128 0x5
 682 034b 53 43 5F 4D 	.asciz "SC_Motor_Interlock_Front"
 682      6F 74 6F 72 
 682      5F 49 6E 74 
 682      65 72 6C 6F 
 682      63 6B 5F 46 
 682      72 6F 6E 74 
 682      00 
 683 0364 04          	.byte 0x4
 684 0365 2C          	.byte 0x2c
 685 0366 8B 03 00 00 	.4byte 0x38b
 686 036a 01          	.byte 0x1
 687 036b 01          	.byte 0x1
 688 036c 04          	.byte 0x4
 689 036d 02          	.byte 0x2
 690 036e 23          	.byte 0x23
 691 036f 01          	.uleb128 0x1
 692 0370 05          	.uleb128 0x5
 693 0371 56 65 72 62 	.asciz "Verbose_Mode"
 693      6F 73 65 5F 
 693      4D 6F 64 65 
 693      00 
 694 037e 04          	.byte 0x4
 695 037f 2E          	.byte 0x2e
 696 0380 8B 03 00 00 	.4byte 0x38b
 697 0384 01          	.byte 0x1
 698 0385 01          	.byte 0x1
 699 0386 03          	.byte 0x3
 700 0387 02          	.byte 0x2
 701 0388 23          	.byte 0x23
 702 0389 01          	.uleb128 0x1
 703 038a 00          	.byte 0x0
 704 038b 02          	.uleb128 0x2
 705 038c 01          	.byte 0x1
 706 038d 02          	.byte 0x2
 707 038e 5F 42 6F 6F 	.asciz "_Bool"
 707      6C 00 
 708 0394 06          	.uleb128 0x6
 709 0395 02          	.byte 0x2
 710 0396 04          	.byte 0x4
 711 0397 1B          	.byte 0x1b
 712 0398 B3 03 00 00 	.4byte 0x3b3
 713 039c 07          	.uleb128 0x7
MPLAB XC16 ASSEMBLY Listing:   			page 17


 714 039d D9 01 00 00 	.4byte 0x1d9
 715 03a1 08          	.uleb128 0x8
 716 03a2 54 45 5F 73 	.asciz "TE_status"
 716      74 61 74 75 
 716      73 00 
 717 03ac 04          	.byte 0x4
 718 03ad 31          	.byte 0x31
 719 03ae F0 00 00 00 	.4byte 0xf0
 720 03b2 00          	.byte 0x0
 721 03b3 04          	.uleb128 0x4
 722 03b4 01          	.byte 0x1
 723 03b5 04          	.byte 0x4
 724 03b6 33          	.byte 0x33
 725 03b7 02 04 00 00 	.4byte 0x402
 726 03bb 05          	.uleb128 0x5
 727 03bc 49 6D 70 6C 	.asciz "Implausibility_APPS"
 727      61 75 73 69 
 727      62 69 6C 69 
 727      74 79 5F 41 
 727      50 50 53 00 
 728 03d0 04          	.byte 0x4
 729 03d1 35          	.byte 0x35
 730 03d2 8B 03 00 00 	.4byte 0x38b
 731 03d6 01          	.byte 0x1
 732 03d7 01          	.byte 0x1
 733 03d8 07          	.byte 0x7
 734 03d9 02          	.byte 0x2
 735 03da 23          	.byte 0x23
 736 03db 00          	.uleb128 0x0
 737 03dc 05          	.uleb128 0x5
 738 03dd 49 6D 70 6C 	.asciz "Implausibility_APPS_BPS"
 738      61 75 73 69 
 738      62 69 6C 69 
 738      74 79 5F 41 
 738      50 50 53 5F 
 738      42 50 53 00 
 739 03f5 04          	.byte 0x4
 740 03f6 36          	.byte 0x36
 741 03f7 8B 03 00 00 	.4byte 0x38b
 742 03fb 01          	.byte 0x1
 743 03fc 01          	.byte 0x1
 744 03fd 06          	.byte 0x6
 745 03fe 02          	.byte 0x2
 746 03ff 23          	.byte 0x23
 747 0400 00          	.uleb128 0x0
 748 0401 00          	.byte 0x0
 749 0402 04          	.uleb128 0x4
 750 0403 04          	.byte 0x4
 751 0404 04          	.byte 0x4
 752 0405 1A          	.byte 0x1a
 753 0406 1B 04 00 00 	.4byte 0x41b
 754 040a 09          	.uleb128 0x9
 755 040b 94 03 00 00 	.4byte 0x394
 756 040f 02          	.byte 0x2
 757 0410 23          	.byte 0x23
 758 0411 00          	.uleb128 0x0
 759 0412 09          	.uleb128 0x9
MPLAB XC16 ASSEMBLY Listing:   			page 18


 760 0413 B3 03 00 00 	.4byte 0x3b3
 761 0417 02          	.byte 0x2
 762 0418 23          	.byte 0x23
 763 0419 02          	.uleb128 0x2
 764 041a 00          	.byte 0x0
 765 041b 03          	.uleb128 0x3
 766 041c 54 45 5F 43 	.asciz "TE_CORE"
 766      4F 52 45 00 
 767 0424 04          	.byte 0x4
 768 0425 39          	.byte 0x39
 769 0426 02 04 00 00 	.4byte 0x402
 770 042a 04          	.uleb128 0x4
 771 042b 0A          	.byte 0xa
 772 042c 04          	.byte 0x4
 773 042d 46          	.byte 0x46
 774 042e 81 04 00 00 	.4byte 0x481
 775 0432 0A          	.uleb128 0xa
 776 0433 73 74 61 74 	.asciz "status"
 776      75 73 00 
 777 043a 04          	.byte 0x4
 778 043b 48          	.byte 0x48
 779 043c 1B 04 00 00 	.4byte 0x41b
 780 0440 02          	.byte 0x2
 781 0441 23          	.byte 0x23
 782 0442 00          	.uleb128 0x0
 783 0443 0A          	.uleb128 0xa
 784 0444 41 50 50 53 	.asciz "APPS"
 784      00 
 785 0449 04          	.byte 0x4
 786 044a 4A          	.byte 0x4a
 787 044b F0 00 00 00 	.4byte 0xf0
 788 044f 02          	.byte 0x2
 789 0450 23          	.byte 0x23
 790 0451 04          	.uleb128 0x4
 791 0452 0A          	.uleb128 0xa
 792 0453 42 50 53 5F 	.asciz "BPS_pressure"
 792      70 72 65 73 
 792      73 75 72 65 
 792      00 
 793 0460 04          	.byte 0x4
 794 0461 4B          	.byte 0x4b
 795 0462 F0 00 00 00 	.4byte 0xf0
 796 0466 02          	.byte 0x2
 797 0467 23          	.byte 0x23
 798 0468 06          	.uleb128 0x6
 799 0469 0A          	.uleb128 0xa
 800 046a 42 50 53 5F 	.asciz "BPS_electric"
 800      65 6C 65 63 
 800      74 72 69 63 
 800      00 
 801 0477 04          	.byte 0x4
 802 0478 4C          	.byte 0x4c
 803 0479 F0 00 00 00 	.4byte 0xf0
 804 047d 02          	.byte 0x2
 805 047e 23          	.byte 0x23
 806 047f 08          	.uleb128 0x8
 807 0480 00          	.byte 0x0
MPLAB XC16 ASSEMBLY Listing:   			page 19


 808 0481 03          	.uleb128 0x3
 809 0482 54 45 5F 4D 	.asciz "TE_MESSAGE"
 809      45 53 53 41 
 809      47 45 00 
 810 048d 04          	.byte 0x4
 811 048e 4E          	.byte 0x4e
 812 048f 2A 04 00 00 	.4byte 0x42a
 813 0493 04          	.uleb128 0x4
 814 0494 0E          	.byte 0xe
 815 0495 04          	.byte 0x4
 816 0496 5D          	.byte 0x5d
 817 0497 47 05 00 00 	.4byte 0x547
 818 049b 0A          	.uleb128 0xa
 819 049c 61 70 70 73 	.asciz "apps_0"
 819      5F 30 00 
 820 04a3 04          	.byte 0x4
 821 04a4 5F          	.byte 0x5f
 822 04a5 F0 00 00 00 	.4byte 0xf0
 823 04a9 02          	.byte 0x2
 824 04aa 23          	.byte 0x23
 825 04ab 00          	.uleb128 0x0
 826 04ac 0A          	.uleb128 0xa
 827 04ad 61 70 70 73 	.asciz "apps_1"
 827      5F 31 00 
 828 04b4 04          	.byte 0x4
 829 04b5 60          	.byte 0x60
 830 04b6 F0 00 00 00 	.4byte 0xf0
 831 04ba 02          	.byte 0x2
 832 04bb 23          	.byte 0x23
 833 04bc 02          	.uleb128 0x2
 834 04bd 0A          	.uleb128 0xa
 835 04be 65 6C 65 63 	.asciz "electric"
 835      74 72 69 63 
 835      00 
 836 04c7 04          	.byte 0x4
 837 04c8 61          	.byte 0x61
 838 04c9 F0 00 00 00 	.4byte 0xf0
 839 04cd 02          	.byte 0x2
 840 04ce 23          	.byte 0x23
 841 04cf 04          	.uleb128 0x4
 842 04d0 0A          	.uleb128 0xa
 843 04d1 70 72 65 73 	.asciz "pressure_0"
 843      73 75 72 65 
 843      5F 30 00 
 844 04dc 04          	.byte 0x4
 845 04dd 62          	.byte 0x62
 846 04de F0 00 00 00 	.4byte 0xf0
 847 04e2 02          	.byte 0x2
 848 04e3 23          	.byte 0x23
 849 04e4 06          	.uleb128 0x6
 850 04e5 0A          	.uleb128 0xa
 851 04e6 70 72 65 73 	.asciz "pressure_1"
 851      73 75 72 65 
 851      5F 31 00 
 852 04f1 04          	.byte 0x4
 853 04f2 63          	.byte 0x63
 854 04f3 F0 00 00 00 	.4byte 0xf0
MPLAB XC16 ASSEMBLY Listing:   			page 20


 855 04f7 02          	.byte 0x2
 856 04f8 23          	.byte 0x23
 857 04f9 08          	.uleb128 0x8
 858 04fa 0A          	.uleb128 0xa
 859 04fb 74 69 6D 65 	.asciz "time_imp"
 859      5F 69 6D 70 
 859      00 
 860 0504 04          	.byte 0x4
 861 0505 64          	.byte 0x64
 862 0506 F0 00 00 00 	.4byte 0xf0
 863 050a 02          	.byte 0x2
 864 050b 23          	.byte 0x23
 865 050c 0A          	.uleb128 0xa
 866 050d 0A          	.uleb128 0xa
 867 050e 61 70 70 73 	.asciz "apps_imp_flag"
 867      5F 69 6D 70 
 867      5F 66 6C 61 
 867      67 00 
 868 051c 04          	.byte 0x4
 869 051d 65          	.byte 0x65
 870 051e 8B 03 00 00 	.4byte 0x38b
 871 0522 02          	.byte 0x2
 872 0523 23          	.byte 0x23
 873 0524 0C          	.uleb128 0xc
 874 0525 0A          	.uleb128 0xa
 875 0526 61 70 70 73 	.asciz "apps_timer_exceed_flag"
 875      5F 74 69 6D 
 875      65 72 5F 65 
 875      78 63 65 65 
 875      64 5F 66 6C 
 875      61 67 00 
 876 053d 04          	.byte 0x4
 877 053e 66          	.byte 0x66
 878 053f 8B 03 00 00 	.4byte 0x38b
 879 0543 02          	.byte 0x2
 880 0544 23          	.byte 0x23
 881 0545 0D          	.uleb128 0xd
 882 0546 00          	.byte 0x0
 883 0547 03          	.uleb128 0x3
 884 0548 54 45 5F 56 	.asciz "TE_VERBOSE_MESSAGE"
 884      45 52 42 4F 
 884      53 45 5F 4D 
 884      45 53 53 41 
 884      47 45 00 
 885 055b 04          	.byte 0x4
 886 055c 68          	.byte 0x68
 887 055d 93 04 00 00 	.4byte 0x493
 888 0561 04          	.uleb128 0x4
 889 0562 04          	.byte 0x4
 890 0563 04          	.byte 0x4
 891 0564 6B          	.byte 0x6b
 892 0565 90 05 00 00 	.4byte 0x590
 893 0569 0A          	.uleb128 0xa
 894 056a 68 65 61 64 	.asciz "header"
 894      65 72 00 
 895 0571 04          	.byte 0x4
 896 0572 6C          	.byte 0x6c
MPLAB XC16 ASSEMBLY Listing:   			page 21


 897 0573 F0 00 00 00 	.4byte 0xf0
 898 0577 02          	.byte 0x2
 899 0578 23          	.byte 0x23
 900 0579 00          	.uleb128 0x0
 901 057a 0A          	.uleb128 0xa
 902 057b 64 61 74 61 	.asciz "data_limit"
 902      5F 6C 69 6D 
 902      69 74 00 
 903 0586 04          	.byte 0x4
 904 0587 6D          	.byte 0x6d
 905 0588 F0 00 00 00 	.4byte 0xf0
 906 058c 02          	.byte 0x2
 907 058d 23          	.byte 0x23
 908 058e 02          	.uleb128 0x2
 909 058f 00          	.byte 0x0
 910 0590 03          	.uleb128 0x3
 911 0591 54 45 5F 4C 	.asciz "TE_LIMIT"
 911      49 4D 49 54 
 911      00 
 912 059a 04          	.byte 0x4
 913 059b 6E          	.byte 0x6e
 914 059c 61 05 00 00 	.4byte 0x561
 915 05a0 04          	.uleb128 0x4
 916 05a1 1C          	.byte 0x1c
 917 05a2 04          	.byte 0x4
 918 05a3 71          	.byte 0x71
 919 05a4 D3 05 00 00 	.4byte 0x5d3
 920 05a8 0D          	.uleb128 0xd
 921 05a9 00 00 00 00 	.4byte .LASF0
 922 05ad 04          	.byte 0x4
 923 05ae 73          	.byte 0x73
 924 05af 81 04 00 00 	.4byte 0x481
 925 05b3 02          	.byte 0x2
 926 05b4 23          	.byte 0x23
 927 05b5 00          	.uleb128 0x0
 928 05b6 0D          	.uleb128 0xd
 929 05b7 00 00 00 00 	.4byte .LASF1
 930 05bb 04          	.byte 0x4
 931 05bc 75          	.byte 0x75
 932 05bd 47 05 00 00 	.4byte 0x547
 933 05c1 02          	.byte 0x2
 934 05c2 23          	.byte 0x23
 935 05c3 0A          	.uleb128 0xa
 936 05c4 0D          	.uleb128 0xd
 937 05c5 00 00 00 00 	.4byte .LASF2
 938 05c9 04          	.byte 0x4
 939 05ca 76          	.byte 0x76
 940 05cb 90 05 00 00 	.4byte 0x590
 941 05cf 02          	.byte 0x2
 942 05d0 23          	.byte 0x23
 943 05d1 18          	.uleb128 0x18
 944 05d2 00          	.byte 0x0
 945 05d3 03          	.uleb128 0x3
 946 05d4 54 45 5F 43 	.asciz "TE_CAN_Data"
 946      41 4E 5F 44 
 946      61 74 61 00 
 947 05e0 04          	.byte 0x4
MPLAB XC16 ASSEMBLY Listing:   			page 22


 948 05e1 78          	.byte 0x78
 949 05e2 A0 05 00 00 	.4byte 0x5a0
 950 05e6 0E          	.uleb128 0xe
 951 05e7 01          	.byte 0x1
 952 05e8 70 61 72 73 	.asciz "parse_te_main_message"
 952      65 5F 74 65 
 952      5F 6D 61 69 
 952      6E 5F 6D 65 
 952      73 73 61 67 
 952      65 00 
 953 05fe 01          	.byte 0x1
 954 05ff 06          	.byte 0x6
 955 0600 01          	.byte 0x1
 956 0601 00 00 00 00 	.4byte .LFB0
 957 0605 00 00 00 00 	.4byte .LFE0
 958 0609 01          	.byte 0x1
 959 060a 5E          	.byte 0x5e
 960 060b 2D 06 00 00 	.4byte 0x62d
 961 060f 0F          	.uleb128 0xf
 962 0610 64 61 74 61 	.asciz "data"
 962      00 
 963 0615 01          	.byte 0x1
 964 0616 06          	.byte 0x6
 965 0617 2D 06 00 00 	.4byte 0x62d
 966 061b 02          	.byte 0x2
 967 061c 7E          	.byte 0x7e
 968 061d 00          	.sleb128 0
 969 061e 10          	.uleb128 0x10
 970 061f 00 00 00 00 	.4byte .LASF0
 971 0623 01          	.byte 0x1
 972 0624 06          	.byte 0x6
 973 0625 33 06 00 00 	.4byte 0x633
 974 0629 02          	.byte 0x2
 975 062a 7E          	.byte 0x7e
 976 062b 02          	.sleb128 2
 977 062c 00          	.byte 0x0
 978 062d 11          	.uleb128 0x11
 979 062e 02          	.byte 0x2
 980 062f F0 00 00 00 	.4byte 0xf0
 981 0633 11          	.uleb128 0x11
 982 0634 02          	.byte 0x2
 983 0635 81 04 00 00 	.4byte 0x481
 984 0639 0E          	.uleb128 0xe
 985 063a 01          	.byte 0x1
 986 063b 70 61 72 73 	.asciz "parse_te_verbose_1_message"
 986      65 5F 74 65 
 986      5F 76 65 72 
 986      62 6F 73 65 
 986      5F 31 5F 6D 
 986      65 73 73 61 
 986      67 65 00 
 987 0656 01          	.byte 0x1
 988 0657 1D          	.byte 0x1d
 989 0658 01          	.byte 0x1
 990 0659 00 00 00 00 	.4byte .LFB1
 991 065d 00 00 00 00 	.4byte .LFE1
 992 0661 01          	.byte 0x1
MPLAB XC16 ASSEMBLY Listing:   			page 23


 993 0662 5E          	.byte 0x5e
 994 0663 85 06 00 00 	.4byte 0x685
 995 0667 0F          	.uleb128 0xf
 996 0668 64 61 74 61 	.asciz "data"
 996      00 
 997 066d 01          	.byte 0x1
 998 066e 1D          	.byte 0x1d
 999 066f 2D 06 00 00 	.4byte 0x62d
 1000 0673 02          	.byte 0x2
 1001 0674 7E          	.byte 0x7e
 1002 0675 00          	.sleb128 0
 1003 0676 10          	.uleb128 0x10
 1004 0677 00 00 00 00 	.4byte .LASF1
 1005 067b 01          	.byte 0x1
 1006 067c 1D          	.byte 0x1d
 1007 067d 85 06 00 00 	.4byte 0x685
 1008 0681 02          	.byte 0x2
 1009 0682 7E          	.byte 0x7e
 1010 0683 02          	.sleb128 2
 1011 0684 00          	.byte 0x0
 1012 0685 11          	.uleb128 0x11
 1013 0686 02          	.byte 0x2
 1014 0687 47 05 00 00 	.4byte 0x547
 1015 068b 0E          	.uleb128 0xe
 1016 068c 01          	.byte 0x1
 1017 068d 70 61 72 73 	.asciz "parse_te_verbose_2_message"
 1017      65 5F 74 65 
 1017      5F 76 65 72 
 1017      62 6F 73 65 
 1017      5F 32 5F 6D 
 1017      65 73 73 61 
 1017      67 65 00 
 1018 06a8 01          	.byte 0x1
 1019 06a9 29          	.byte 0x29
 1020 06aa 01          	.byte 0x1
 1021 06ab 00 00 00 00 	.4byte .LFB2
 1022 06af 00 00 00 00 	.4byte .LFE2
 1023 06b3 01          	.byte 0x1
 1024 06b4 5E          	.byte 0x5e
 1025 06b5 D7 06 00 00 	.4byte 0x6d7
 1026 06b9 0F          	.uleb128 0xf
 1027 06ba 64 61 74 61 	.asciz "data"
 1027      00 
 1028 06bf 01          	.byte 0x1
 1029 06c0 29          	.byte 0x29
 1030 06c1 2D 06 00 00 	.4byte 0x62d
 1031 06c5 02          	.byte 0x2
 1032 06c6 7E          	.byte 0x7e
 1033 06c7 00          	.sleb128 0
 1034 06c8 10          	.uleb128 0x10
 1035 06c9 00 00 00 00 	.4byte .LASF1
 1036 06cd 01          	.byte 0x1
 1037 06ce 29          	.byte 0x29
 1038 06cf 85 06 00 00 	.4byte 0x685
 1039 06d3 02          	.byte 0x2
 1040 06d4 7E          	.byte 0x7e
 1041 06d5 02          	.sleb128 2
MPLAB XC16 ASSEMBLY Listing:   			page 24


 1042 06d6 00          	.byte 0x0
 1043 06d7 0E          	.uleb128 0xe
 1044 06d8 01          	.byte 0x1
 1045 06d9 70 61 72 73 	.asciz "parse_can_te_limit"
 1045      65 5F 63 61 
 1045      6E 5F 74 65 
 1045      5F 6C 69 6D 
 1045      69 74 00 
 1046 06ec 01          	.byte 0x1
 1047 06ed 34          	.byte 0x34
 1048 06ee 01          	.byte 0x1
 1049 06ef 00 00 00 00 	.4byte .LFB3
 1050 06f3 00 00 00 00 	.4byte .LFE3
 1051 06f7 01          	.byte 0x1
 1052 06f8 5E          	.byte 0x5e
 1053 06f9 1E 07 00 00 	.4byte 0x71e
 1054 06fd 0F          	.uleb128 0xf
 1055 06fe 6D 65 73 73 	.asciz "message"
 1055      61 67 65 00 
 1056 0706 01          	.byte 0x1
 1057 0707 34          	.byte 0x34
 1058 0708 CA 01 00 00 	.4byte 0x1ca
 1059 070c 02          	.byte 0x2
 1060 070d 7E          	.byte 0x7e
 1061 070e 00          	.sleb128 0
 1062 070f 10          	.uleb128 0x10
 1063 0710 00 00 00 00 	.4byte .LASF2
 1064 0714 01          	.byte 0x1
 1065 0715 34          	.byte 0x34
 1066 0716 1E 07 00 00 	.4byte 0x71e
 1067 071a 02          	.byte 0x2
 1068 071b 7E          	.byte 0x7e
 1069 071c 0C          	.sleb128 12
 1070 071d 00          	.byte 0x0
 1071 071e 11          	.uleb128 0x11
 1072 071f 02          	.byte 0x2
 1073 0720 90 05 00 00 	.4byte 0x590
 1074 0724 0E          	.uleb128 0xe
 1075 0725 01          	.byte 0x1
 1076 0726 70 61 72 73 	.asciz "parse_can_te"
 1076      65 5F 63 61 
 1076      6E 5F 74 65 
 1076      00 
 1077 0733 01          	.byte 0x1
 1078 0734 3B          	.byte 0x3b
 1079 0735 01          	.byte 0x1
 1080 0736 00 00 00 00 	.4byte .LFB4
 1081 073a 00 00 00 00 	.4byte .LFE4
 1082 073e 01          	.byte 0x1
 1083 073f 5E          	.byte 0x5e
 1084 0740 66 07 00 00 	.4byte 0x766
 1085 0744 0F          	.uleb128 0xf
 1086 0745 6D 65 73 73 	.asciz "message"
 1086      61 67 65 00 
 1087 074d 01          	.byte 0x1
 1088 074e 3B          	.byte 0x3b
 1089 074f CA 01 00 00 	.4byte 0x1ca
MPLAB XC16 ASSEMBLY Listing:   			page 25


 1090 0753 02          	.byte 0x2
 1091 0754 7E          	.byte 0x7e
 1092 0755 00          	.sleb128 0
 1093 0756 0F          	.uleb128 0xf
 1094 0757 64 61 74 61 	.asciz "data"
 1094      00 
 1095 075c 01          	.byte 0x1
 1096 075d 3B          	.byte 0x3b
 1097 075e 66 07 00 00 	.4byte 0x766
 1098 0762 02          	.byte 0x2
 1099 0763 7E          	.byte 0x7e
 1100 0764 0C          	.sleb128 12
 1101 0765 00          	.byte 0x0
 1102 0766 11          	.uleb128 0x11
 1103 0767 02          	.byte 0x2
 1104 0768 D3 05 00 00 	.4byte 0x5d3
 1105 076c 00          	.byte 0x0
 1106                 	.section .debug_abbrev,info
 1107 0000 01          	.uleb128 0x1
 1108 0001 11          	.uleb128 0x11
 1109 0002 01          	.byte 0x1
 1110 0003 25          	.uleb128 0x25
 1111 0004 08          	.uleb128 0x8
 1112 0005 13          	.uleb128 0x13
 1113 0006 0B          	.uleb128 0xb
 1114 0007 03          	.uleb128 0x3
 1115 0008 08          	.uleb128 0x8
 1116 0009 1B          	.uleb128 0x1b
 1117 000a 08          	.uleb128 0x8
 1118 000b 11          	.uleb128 0x11
 1119 000c 01          	.uleb128 0x1
 1120 000d 12          	.uleb128 0x12
 1121 000e 01          	.uleb128 0x1
 1122 000f 10          	.uleb128 0x10
 1123 0010 06          	.uleb128 0x6
 1124 0011 00          	.byte 0x0
 1125 0012 00          	.byte 0x0
 1126 0013 02          	.uleb128 0x2
 1127 0014 24          	.uleb128 0x24
 1128 0015 00          	.byte 0x0
 1129 0016 0B          	.uleb128 0xb
 1130 0017 0B          	.uleb128 0xb
 1131 0018 3E          	.uleb128 0x3e
 1132 0019 0B          	.uleb128 0xb
 1133 001a 03          	.uleb128 0x3
 1134 001b 08          	.uleb128 0x8
 1135 001c 00          	.byte 0x0
 1136 001d 00          	.byte 0x0
 1137 001e 03          	.uleb128 0x3
 1138 001f 16          	.uleb128 0x16
 1139 0020 00          	.byte 0x0
 1140 0021 03          	.uleb128 0x3
 1141 0022 08          	.uleb128 0x8
 1142 0023 3A          	.uleb128 0x3a
 1143 0024 0B          	.uleb128 0xb
 1144 0025 3B          	.uleb128 0x3b
 1145 0026 0B          	.uleb128 0xb
MPLAB XC16 ASSEMBLY Listing:   			page 26


 1146 0027 49          	.uleb128 0x49
 1147 0028 13          	.uleb128 0x13
 1148 0029 00          	.byte 0x0
 1149 002a 00          	.byte 0x0
 1150 002b 04          	.uleb128 0x4
 1151 002c 13          	.uleb128 0x13
 1152 002d 01          	.byte 0x1
 1153 002e 0B          	.uleb128 0xb
 1154 002f 0B          	.uleb128 0xb
 1155 0030 3A          	.uleb128 0x3a
 1156 0031 0B          	.uleb128 0xb
 1157 0032 3B          	.uleb128 0x3b
 1158 0033 0B          	.uleb128 0xb
 1159 0034 01          	.uleb128 0x1
 1160 0035 13          	.uleb128 0x13
 1161 0036 00          	.byte 0x0
 1162 0037 00          	.byte 0x0
 1163 0038 05          	.uleb128 0x5
 1164 0039 0D          	.uleb128 0xd
 1165 003a 00          	.byte 0x0
 1166 003b 03          	.uleb128 0x3
 1167 003c 08          	.uleb128 0x8
 1168 003d 3A          	.uleb128 0x3a
 1169 003e 0B          	.uleb128 0xb
 1170 003f 3B          	.uleb128 0x3b
 1171 0040 0B          	.uleb128 0xb
 1172 0041 49          	.uleb128 0x49
 1173 0042 13          	.uleb128 0x13
 1174 0043 0B          	.uleb128 0xb
 1175 0044 0B          	.uleb128 0xb
 1176 0045 0D          	.uleb128 0xd
 1177 0046 0B          	.uleb128 0xb
 1178 0047 0C          	.uleb128 0xc
 1179 0048 0B          	.uleb128 0xb
 1180 0049 38          	.uleb128 0x38
 1181 004a 0A          	.uleb128 0xa
 1182 004b 00          	.byte 0x0
 1183 004c 00          	.byte 0x0
 1184 004d 06          	.uleb128 0x6
 1185 004e 17          	.uleb128 0x17
 1186 004f 01          	.byte 0x1
 1187 0050 0B          	.uleb128 0xb
 1188 0051 0B          	.uleb128 0xb
 1189 0052 3A          	.uleb128 0x3a
 1190 0053 0B          	.uleb128 0xb
 1191 0054 3B          	.uleb128 0x3b
 1192 0055 0B          	.uleb128 0xb
 1193 0056 01          	.uleb128 0x1
 1194 0057 13          	.uleb128 0x13
 1195 0058 00          	.byte 0x0
 1196 0059 00          	.byte 0x0
 1197 005a 07          	.uleb128 0x7
 1198 005b 0D          	.uleb128 0xd
 1199 005c 00          	.byte 0x0
 1200 005d 49          	.uleb128 0x49
 1201 005e 13          	.uleb128 0x13
 1202 005f 00          	.byte 0x0
MPLAB XC16 ASSEMBLY Listing:   			page 27


 1203 0060 00          	.byte 0x0
 1204 0061 08          	.uleb128 0x8
 1205 0062 0D          	.uleb128 0xd
 1206 0063 00          	.byte 0x0
 1207 0064 03          	.uleb128 0x3
 1208 0065 08          	.uleb128 0x8
 1209 0066 3A          	.uleb128 0x3a
 1210 0067 0B          	.uleb128 0xb
 1211 0068 3B          	.uleb128 0x3b
 1212 0069 0B          	.uleb128 0xb
 1213 006a 49          	.uleb128 0x49
 1214 006b 13          	.uleb128 0x13
 1215 006c 00          	.byte 0x0
 1216 006d 00          	.byte 0x0
 1217 006e 09          	.uleb128 0x9
 1218 006f 0D          	.uleb128 0xd
 1219 0070 00          	.byte 0x0
 1220 0071 49          	.uleb128 0x49
 1221 0072 13          	.uleb128 0x13
 1222 0073 38          	.uleb128 0x38
 1223 0074 0A          	.uleb128 0xa
 1224 0075 00          	.byte 0x0
 1225 0076 00          	.byte 0x0
 1226 0077 0A          	.uleb128 0xa
 1227 0078 0D          	.uleb128 0xd
 1228 0079 00          	.byte 0x0
 1229 007a 03          	.uleb128 0x3
 1230 007b 08          	.uleb128 0x8
 1231 007c 3A          	.uleb128 0x3a
 1232 007d 0B          	.uleb128 0xb
 1233 007e 3B          	.uleb128 0x3b
 1234 007f 0B          	.uleb128 0xb
 1235 0080 49          	.uleb128 0x49
 1236 0081 13          	.uleb128 0x13
 1237 0082 38          	.uleb128 0x38
 1238 0083 0A          	.uleb128 0xa
 1239 0084 00          	.byte 0x0
 1240 0085 00          	.byte 0x0
 1241 0086 0B          	.uleb128 0xb
 1242 0087 01          	.uleb128 0x1
 1243 0088 01          	.byte 0x1
 1244 0089 49          	.uleb128 0x49
 1245 008a 13          	.uleb128 0x13
 1246 008b 01          	.uleb128 0x1
 1247 008c 13          	.uleb128 0x13
 1248 008d 00          	.byte 0x0
 1249 008e 00          	.byte 0x0
 1250 008f 0C          	.uleb128 0xc
 1251 0090 21          	.uleb128 0x21
 1252 0091 00          	.byte 0x0
 1253 0092 49          	.uleb128 0x49
 1254 0093 13          	.uleb128 0x13
 1255 0094 2F          	.uleb128 0x2f
 1256 0095 0B          	.uleb128 0xb
 1257 0096 00          	.byte 0x0
 1258 0097 00          	.byte 0x0
 1259 0098 0D          	.uleb128 0xd
MPLAB XC16 ASSEMBLY Listing:   			page 28


 1260 0099 0D          	.uleb128 0xd
 1261 009a 00          	.byte 0x0
 1262 009b 03          	.uleb128 0x3
 1263 009c 0E          	.uleb128 0xe
 1264 009d 3A          	.uleb128 0x3a
 1265 009e 0B          	.uleb128 0xb
 1266 009f 3B          	.uleb128 0x3b
 1267 00a0 0B          	.uleb128 0xb
 1268 00a1 49          	.uleb128 0x49
 1269 00a2 13          	.uleb128 0x13
 1270 00a3 38          	.uleb128 0x38
 1271 00a4 0A          	.uleb128 0xa
 1272 00a5 00          	.byte 0x0
 1273 00a6 00          	.byte 0x0
 1274 00a7 0E          	.uleb128 0xe
 1275 00a8 2E          	.uleb128 0x2e
 1276 00a9 01          	.byte 0x1
 1277 00aa 3F          	.uleb128 0x3f
 1278 00ab 0C          	.uleb128 0xc
 1279 00ac 03          	.uleb128 0x3
 1280 00ad 08          	.uleb128 0x8
 1281 00ae 3A          	.uleb128 0x3a
 1282 00af 0B          	.uleb128 0xb
 1283 00b0 3B          	.uleb128 0x3b
 1284 00b1 0B          	.uleb128 0xb
 1285 00b2 27          	.uleb128 0x27
 1286 00b3 0C          	.uleb128 0xc
 1287 00b4 11          	.uleb128 0x11
 1288 00b5 01          	.uleb128 0x1
 1289 00b6 12          	.uleb128 0x12
 1290 00b7 01          	.uleb128 0x1
 1291 00b8 40          	.uleb128 0x40
 1292 00b9 0A          	.uleb128 0xa
 1293 00ba 01          	.uleb128 0x1
 1294 00bb 13          	.uleb128 0x13
 1295 00bc 00          	.byte 0x0
 1296 00bd 00          	.byte 0x0
 1297 00be 0F          	.uleb128 0xf
 1298 00bf 05          	.uleb128 0x5
 1299 00c0 00          	.byte 0x0
 1300 00c1 03          	.uleb128 0x3
 1301 00c2 08          	.uleb128 0x8
 1302 00c3 3A          	.uleb128 0x3a
 1303 00c4 0B          	.uleb128 0xb
 1304 00c5 3B          	.uleb128 0x3b
 1305 00c6 0B          	.uleb128 0xb
 1306 00c7 49          	.uleb128 0x49
 1307 00c8 13          	.uleb128 0x13
 1308 00c9 02          	.uleb128 0x2
 1309 00ca 0A          	.uleb128 0xa
 1310 00cb 00          	.byte 0x0
 1311 00cc 00          	.byte 0x0
 1312 00cd 10          	.uleb128 0x10
 1313 00ce 05          	.uleb128 0x5
 1314 00cf 00          	.byte 0x0
 1315 00d0 03          	.uleb128 0x3
 1316 00d1 0E          	.uleb128 0xe
MPLAB XC16 ASSEMBLY Listing:   			page 29


 1317 00d2 3A          	.uleb128 0x3a
 1318 00d3 0B          	.uleb128 0xb
 1319 00d4 3B          	.uleb128 0x3b
 1320 00d5 0B          	.uleb128 0xb
 1321 00d6 49          	.uleb128 0x49
 1322 00d7 13          	.uleb128 0x13
 1323 00d8 02          	.uleb128 0x2
 1324 00d9 0A          	.uleb128 0xa
 1325 00da 00          	.byte 0x0
 1326 00db 00          	.byte 0x0
 1327 00dc 11          	.uleb128 0x11
 1328 00dd 0F          	.uleb128 0xf
 1329 00de 00          	.byte 0x0
 1330 00df 0B          	.uleb128 0xb
 1331 00e0 0B          	.uleb128 0xb
 1332 00e1 49          	.uleb128 0x49
 1333 00e2 13          	.uleb128 0x13
 1334 00e3 00          	.byte 0x0
 1335 00e4 00          	.byte 0x0
 1336 00e5 00          	.byte 0x0
 1337                 	.section .debug_pubnames,info
 1338 0000 8E 00 00 00 	.4byte 0x8e
 1339 0004 02 00       	.2byte 0x2
 1340 0006 00 00 00 00 	.4byte .Ldebug_info0
 1341 000a 6D 07 00 00 	.4byte 0x76d
 1342 000e E6 05 00 00 	.4byte 0x5e6
 1343 0012 70 61 72 73 	.asciz "parse_te_main_message"
 1343      65 5F 74 65 
 1343      5F 6D 61 69 
 1343      6E 5F 6D 65 
 1343      73 73 61 67 
 1343      65 00 
 1344 0028 39 06 00 00 	.4byte 0x639
 1345 002c 70 61 72 73 	.asciz "parse_te_verbose_1_message"
 1345      65 5F 74 65 
 1345      5F 76 65 72 
 1345      62 6F 73 65 
 1345      5F 31 5F 6D 
 1345      65 73 73 61 
 1345      67 65 00 
 1346 0047 8B 06 00 00 	.4byte 0x68b
 1347 004b 70 61 72 73 	.asciz "parse_te_verbose_2_message"
 1347      65 5F 74 65 
 1347      5F 76 65 72 
 1347      62 6F 73 65 
 1347      5F 32 5F 6D 
 1347      65 73 73 61 
 1347      67 65 00 
 1348 0066 D7 06 00 00 	.4byte 0x6d7
 1349 006a 70 61 72 73 	.asciz "parse_can_te_limit"
 1349      65 5F 63 61 
 1349      6E 5F 74 65 
 1349      5F 6C 69 6D 
 1349      69 74 00 
 1350 007d 24 07 00 00 	.4byte 0x724
 1351 0081 70 61 72 73 	.asciz "parse_can_te"
 1351      65 5F 63 61 
MPLAB XC16 ASSEMBLY Listing:   			page 30


 1351      6E 5F 74 65 
 1351      00 
 1352 008e 00 00 00 00 	.4byte 0x0
 1353                 	.section .debug_pubtypes,info
 1354 0000 76 00 00 00 	.4byte 0x76
 1355 0004 02 00       	.2byte 0x2
 1356 0006 00 00 00 00 	.4byte .Ldebug_info0
 1357 000a 6D 07 00 00 	.4byte 0x76d
 1358 000e F0 00 00 00 	.4byte 0xf0
 1359 0012 75 69 6E 74 	.asciz "uint16_t"
 1359      31 36 5F 74 
 1359      00 
 1360 001b CA 01 00 00 	.4byte 0x1ca
 1361 001f 43 41 4E 64 	.asciz "CANdata"
 1361      61 74 61 00 
 1362 0027 1B 04 00 00 	.4byte 0x41b
 1363 002b 54 45 5F 43 	.asciz "TE_CORE"
 1363      4F 52 45 00 
 1364 0033 81 04 00 00 	.4byte 0x481
 1365 0037 54 45 5F 4D 	.asciz "TE_MESSAGE"
 1365      45 53 53 41 
 1365      47 45 00 
 1366 0042 47 05 00 00 	.4byte 0x547
 1367 0046 54 45 5F 56 	.asciz "TE_VERBOSE_MESSAGE"
 1367      45 52 42 4F 
 1367      53 45 5F 4D 
 1367      45 53 53 41 
 1367      47 45 00 
 1368 0059 90 05 00 00 	.4byte 0x590
 1369 005d 54 45 5F 4C 	.asciz "TE_LIMIT"
 1369      49 4D 49 54 
 1369      00 
 1370 0066 D3 05 00 00 	.4byte 0x5d3
 1371 006a 54 45 5F 43 	.asciz "TE_CAN_Data"
 1371      41 4E 5F 44 
 1371      61 74 61 00 
 1372 0076 00 00 00 00 	.4byte 0x0
 1373                 	.section .debug_aranges,info
 1374 0000 14 00 00 00 	.4byte 0x14
 1375 0004 02 00       	.2byte 0x2
 1376 0006 00 00 00 00 	.4byte .Ldebug_info0
 1377 000a 04          	.byte 0x4
 1378 000b 00          	.byte 0x0
 1379 000c 00 00       	.2byte 0x0
 1380 000e 00 00       	.2byte 0x0
 1381 0010 00 00 00 00 	.4byte 0x0
 1382 0014 00 00 00 00 	.4byte 0x0
 1383                 	.section .debug_str,info
 1384                 	.LASF2:
 1385 0000 6C 69 6D 69 	.asciz "limit_message"
 1385      74 5F 6D 65 
 1385      73 73 61 67 
 1385      65 00 
 1386                 	.LASF1:
 1387 000e 76 65 72 62 	.asciz "verbose_message"
 1387      6F 73 65 5F 
 1387      6D 65 73 73 
MPLAB XC16 ASSEMBLY Listing:   			page 31


 1387      61 67 65 00 
 1388                 	.LASF0:
 1389 001e 6D 61 69 6E 	.asciz "main_message"
 1389      5F 6D 65 73 
 1389      73 61 67 65 
 1389      00 
 1390                 	.section .text,code
 1391              	
 1392              	
 1393              	
 1394              	.section __c30_info,info,bss
 1395                 	__psv_trap_errata:
 1396                 	
 1397                 	.section __c30_signature,info,data
 1398 0000 01 00       	.word 0x0001
 1399 0002 00 00       	.word 0x0000
 1400 0004 00 00       	.word 0x0000
 1401                 	
 1402                 	
 1403                 	
 1404                 	.set ___PA___,0
 1405                 	.end
MPLAB XC16 ASSEMBLY Listing:   			page 32


DEFINED SYMBOLS
                            *ABS*:00000000 /home/user/Documents/FST/Programming/project_sub-zero/lib/can-ids/Devices/TE_CAN.c
                            *ABS*:00000001 __33EP256MU806
                            *ABS*:00000001 __dsPIC33E
                            *ABS*:00000001 __HAS_DSP
                            *ABS*:00000001 __HAS_DMAV2
                            *ABS*:00000001 __HAS_CODEGUARD
                            *ABS*:00000001 __HAS_PMP_ENHANCED
                            *ABS*:00000001 __HAS_EDS
                            *ABS*:00000001 __HAS_EP
                            *ABS*:00000011 __TARGET_DIVIDE_CYCLES
                            *ABS*:00000001 __C30ELF
    {standard input}:14     .text:00000000 _parse_te_main_message
    {standard input}:18     *ABS*:00000000 ___PA___
    {standard input}:67     .text:00000032 _parse_te_verbose_1_message
    {standard input}:119    .text:00000064 _parse_te_verbose_2_message
    {standard input}:181    .text:000000b0 _parse_can_te_limit
    {standard input}:217    .text:000000d4 _parse_can_te
    {standard input}:237    *ABS*:00000000 ___BP___
    {standard input}:1395   __c30_info:00000000 __psv_trap_errata
    {standard input}:19     .text:00000000 .L0
                            .text:00000148 .L14
                            .text:00000126 .L11
                            .text:0000010c .L13
                            .text:00000114 .L9
                            .text:0000011c .L10
                            .text:00000148 .L5
                            .text:00000130 .L12
                            .text:00000146 .L8
                    .debug_abbrev:00000000 .Ldebug_abbrev0
                            .text:00000000 .Ltext0
                            .text:00000150 .Letext0
                      .debug_line:00000000 .Ldebug_line0
                       .debug_str:0000001e .LASF0
                       .debug_str:0000000e .LASF1
                       .debug_str:00000000 .LASF2
                            .text:00000000 .LFB0
                            .text:00000032 .LFE0
                            .text:00000032 .LFB1
                            .text:00000064 .LFE1
                            .text:00000064 .LFB2
                            .text:000000b0 .LFE2
                            .text:000000b0 .LFB3
                            .text:000000d4 .LFE3
                            .text:000000d4 .LFB4
                            .text:00000150 .LFE4
                     .debug_frame:00000000 .Lframe0
                      .debug_info:00000000 .Ldebug_info0

UNDEFINED SYMBOLS
CORCON

EQUATE SYMBOLS
/home/user/Documents/FST/Programming/project_sub-zero/lib/can-ids/Devices/TE_CAN.c = 0x0
                  __33EP256MU806 = 0x1
                      __dsPIC33E = 0x1
                       __HAS_DSP = 0x1
                     __HAS_DMAV2 = 0x1
                 __HAS_CODEGUARD = 0x1
              __HAS_PMP_ENHANCED = 0x1
                       __HAS_EDS = 0x1
                        __HAS_EP = 0x1
          __TARGET_DIVIDE_CYCLES = 0x11
                        __C30ELF = 0x1
                        ___PA___ = 0x0
                        ___BP___ = 0x0
