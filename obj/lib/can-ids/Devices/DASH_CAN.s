MPLAB XC16 ASSEMBLY Listing:   			page 1


   1              	.file "/home/user/Documents/FST/Programming/project_sub-zero/lib/can-ids/Devices/DASH_CAN.c"
   2              	.section .debug_abbrev,info
   3                 	.Ldebug_abbrev0:
   4                 	.section .debug_info,info
   5                 	.Ldebug_info0:
   6                 	.section .debug_line,info
   7                 	.Ldebug_line0:
   8 0000 C0 00 00 00 	.section .text,code
   8      02 00 9C 00 
   8      00 00 01 01 
   8      FB 0E 0A 00 
   8      01 01 01 01 
   8      00 00 00 01 
   8      6C 69 62 2F 
   8      63 61 6E 2D 
   8      69 64 73 2F 
   9              	.Ltext0:
  10              	.section .text,code
  11              	.align 2
  12              	.global _parse_can_message_status
  13              	.type _parse_can_message_status,@function
  14              	_parse_can_message_status:
  15              	.LFB0:
  16              	.file 1 "lib/can-ids/Devices/DASH_CAN.c"
   1:lib/can-ids/Devices/DASH_CAN.c **** #include <stdbool.h>
   2:lib/can-ids/Devices/DASH_CAN.c **** #include <stdint.h>
   3:lib/can-ids/Devices/DASH_CAN.c **** #include <stdio.h>
   4:lib/can-ids/Devices/DASH_CAN.c **** 
   5:lib/can-ids/Devices/DASH_CAN.c **** #include "can-ids/CAN_IDs.h"
   6:lib/can-ids/Devices/DASH_CAN.c **** #include "DASH_CAN.h"
   7:lib/can-ids/Devices/DASH_CAN.c **** 
   8:lib/can-ids/Devices/DASH_CAN.c **** 
   9:lib/can-ids/Devices/DASH_CAN.c **** void parse_can_message_status(uint16_t data[4], DASH_MSG_status *status) {
  17              	.loc 1 9 0
  18              	.set ___PA___,1
  19 000000  04 00 FA 	lnk #4
  20              	.LCFI0:
  21              	.loc 1 9 0
  22 000002  00 0F 78 	mov w0,[w14]
  23 000004  11 07 98 	mov w1,[w14+2]
  10:lib/can-ids/Devices/DASH_CAN.c **** 	status->board_temp = data[0];
  24              	.loc 1 10 0
  25 000006  9E 00 78 	mov [w14],w1
  26 000008  1E 00 90 	mov [w14+2],w0
  27 00000a  91 00 78 	mov [w1],w1
  28 00000c  81 40 78 	mov.b w1,w1
  29 00000e  01 48 78 	mov.b w1,[w0]
  11:lib/can-ids/Devices/DASH_CAN.c **** 	status->LEDs       = data[1] >> 2;
  30              	.loc 1 11 0
  31 000010  9E 80 E8 	inc2 [w14],w1
  32 000012  1E 00 90 	mov [w14+2],w0
  33 000014  91 00 78 	mov [w1],w1
  34 000016  90 41 90 	mov.b [w0+1],w3
  35 000018  C2 08 DE 	lsr w1,#2,w1
  36 00001a  02 CC B3 	mov.b #-64,w2
  37 00001c  81 40 78 	mov.b w1,w1
  38 00001e  02 C1 61 	and.b w3,w2,w2
MPLAB XC16 ASSEMBLY Listing:   			page 2


  39 000020  F1 43 B2 	and.b #63,w1
  40 000022  F1 43 B2 	and.b #63,w1
  41 000024  81 40 71 	ior.b w2,w1,w1
  42 000026  11 40 98 	mov.b w1,[w0+1]
  12:lib/can-ids/Devices/DASH_CAN.c ****     status->SC         = (data[1] >> 1) & 1;
  43              	.loc 1 12 0
  44 000028  9E 80 E8 	inc2 [w14],w1
  45 00002a  1E 00 90 	mov [w14+2],w0
  46 00002c  00 00 00 	nop 
  47 00002e  91 00 78 	mov [w1],w1
  48 000030  81 00 D1 	lsr w1,w1
  49 000032  00 00 00 	nop 
  50 000034  E1 80 60 	and w1,#1,w1
  51 000036  01 F0 A7 	btsc w1,#15
  52 000038  81 00 EA 	neg w1,w1
  53 00003a  81 00 EA 	neg w1,w1
  54 00003c  CF 08 DE 	lsr w1,#15,w1
  55 00003e  00 00 00 	nop 
  56 000040  81 40 78 	mov.b w1,w1
  57 000042  00 00 00 	nop 
  58 000044  21 40 98 	mov.b w1,[w0+2]
  13:lib/can-ids/Devices/DASH_CAN.c **** 	status->debug_mode = data[1];
  59              	.loc 1 13 0
  60 000046  00 00 00 	nop 
  61 000048  9E 80 E8 	inc2 [w14],w1
  62 00004a  1E 00 90 	mov [w14+2],w0
  63 00004c  00 00 00 	nop 
  64 00004e  91 00 78 	mov [w1],w1
  65 000050  01 F0 A7 	btsc w1,#15
  66 000052  81 00 EA 	neg w1,w1
  67 000054  81 00 EA 	neg w1,w1
  68 000056  CF 08 DE 	lsr w1,#15,w1
  69 000058  00 00 00 	nop 
  70 00005a  81 40 78 	mov.b w1,w1
  71 00005c  00 00 00 	nop 
  72 00005e  31 40 98 	mov.b w1,[w0+3]
  14:lib/can-ids/Devices/DASH_CAN.c **** }
  73              	.loc 1 14 0
  74 000060  8E 07 78 	mov w14,w15
  75 000062  4F 07 78 	mov [--w15],w14
  76 000064  00 40 A9 	bclr CORCON,#2
  77 000066  00 00 06 	return 
  78              	.set ___PA___,0
  79              	.LFE0:
  80              	.size _parse_can_message_status,.-_parse_can_message_status
  81              	.align 2
  82              	.global _parse_can_dash
  83              	.type _parse_can_dash,@function
  84              	_parse_can_dash:
  85              	.LFB1:
  15:lib/can-ids/Devices/DASH_CAN.c **** 
  16:lib/can-ids/Devices/DASH_CAN.c **** void parse_can_dash(CANdata message, DASH_CAN_Data *data) {
  86              	.loc 1 16 0
  87              	.set ___PA___,1
  88 000068  0E 00 FA 	lnk #14
  89              	.LCFI1:
  90              	.loc 1 16 0
MPLAB XC16 ASSEMBLY Listing:   			page 3


  91 00006a  00 0F 78 	mov w0,[w14]
  92 00006c  11 07 98 	mov w1,[w14+2]
  17:lib/can-ids/Devices/DASH_CAN.c **** 	if (message.dev_id != DEVICE_ID_DASH) {
  93              	.loc 1 17 0
  94 00006e  1E 00 78 	mov [w14],w0
  95              	.loc 1 16 0
  96 000070  22 07 98 	mov w2,[w14+4]
  97 000072  33 07 98 	mov w3,[w14+6]
  98 000074  44 07 98 	mov w4,[w14+8]
  99 000076  55 07 98 	mov w5,[w14+10]
 100 000078  66 07 98 	mov w6,[w14+12]
 101              	.loc 1 17 0
 102 00007a  7F 00 60 	and w0,#31,w0
 103 00007c  EA 0F 50 	sub w0,#10,[w15]
 104              	.set ___BP___,0
 105 00007e  00 00 3A 	bra nz,.L6
 106              	.L3:
  18:lib/can-ids/Devices/DASH_CAN.c **** 		/*FIXME: send info for error logging*/
  19:lib/can-ids/Devices/DASH_CAN.c ****         return;
  20:lib/can-ids/Devices/DASH_CAN.c **** 	}
  21:lib/can-ids/Devices/DASH_CAN.c **** 
  22:lib/can-ids/Devices/DASH_CAN.c **** 	switch (message.msg_id){
 107              	.loc 1 22 0
 108 000080  9E 00 78 	mov [w14],w1
 109 000082  00 02 20 	mov #32,w0
 110 000084  C5 08 DE 	lsr w1,#5,w1
 111 000086  F1 43 B2 	and.b #63,w1
 112 000088  81 80 FB 	ze w1,w1
 113 00008a  80 8F 50 	sub w1,w0,[w15]
 114              	.set ___BP___,0
 115 00008c  00 00 3A 	bra nz,.L2
 116              	.L5:
  23:lib/can-ids/Devices/DASH_CAN.c **** 		case MSG_ID_DASH_STATUS:
  24:lib/can-ids/Devices/DASH_CAN.c **** 			parse_can_message_status(message.data, &(data->status));
 117              	.loc 1 24 0
 118 00008e  EE 00 90 	mov [w14+12],w1
 119 000090  64 00 47 	add w14,#4,w0
 120 000092  00 00 07 	rcall _parse_can_message_status
 121 000094  00 00 37 	bra .L2
 122              	.L6:
 123              	.L2:
  25:lib/can-ids/Devices/DASH_CAN.c **** 			break;
  26:lib/can-ids/Devices/DASH_CAN.c **** 	}
  27:lib/can-ids/Devices/DASH_CAN.c **** }
 124              	.loc 1 27 0
 125 000096  8E 07 78 	mov w14,w15
 126 000098  4F 07 78 	mov [--w15],w14
 127 00009a  00 40 A9 	bclr CORCON,#2
 128 00009c  00 00 06 	return 
 129              	.set ___PA___,0
 130              	.LFE1:
 131              	.size _parse_can_dash,.-_parse_can_dash
 132              	.section .debug_frame,info
 133                 	.Lframe0:
 134 0000 10 00 00 00 	.4byte .LECIE0-.LSCIE0
 135                 	.LSCIE0:
 136 0004 FF FF FF FF 	.4byte 0xffffffff
MPLAB XC16 ASSEMBLY Listing:   			page 4


 137 0008 01          	.byte 0x1
 138 0009 00          	.byte 0
 139 000a 01          	.uleb128 0x1
 140 000b 02          	.sleb128 2
 141 000c 25          	.byte 0x25
 142 000d 12          	.byte 0x12
 143 000e 0F          	.uleb128 0xf
 144 000f 7E          	.sleb128 -2
 145 0010 09          	.byte 0x9
 146 0011 25          	.uleb128 0x25
 147 0012 0F          	.uleb128 0xf
 148 0013 00          	.align 4
 149                 	.LECIE0:
 150                 	.LSFDE0:
 151 0014 18 00 00 00 	.4byte .LEFDE0-.LASFDE0
 152                 	.LASFDE0:
 153 0018 00 00 00 00 	.4byte .Lframe0
 154 001c 00 00 00 00 	.4byte .LFB0
 155 0020 68 00 00 00 	.4byte .LFE0-.LFB0
 156 0024 04          	.byte 0x4
 157 0025 02 00 00 00 	.4byte .LCFI0-.LFB0
 158 0029 13          	.byte 0x13
 159 002a 7D          	.sleb128 -3
 160 002b 0D          	.byte 0xd
 161 002c 0E          	.uleb128 0xe
 162 002d 8E          	.byte 0x8e
 163 002e 02          	.uleb128 0x2
 164 002f 00          	.align 4
 165                 	.LEFDE0:
 166                 	.LSFDE2:
 167 0030 18 00 00 00 	.4byte .LEFDE2-.LASFDE2
 168                 	.LASFDE2:
 169 0034 00 00 00 00 	.4byte .Lframe0
 170 0038 00 00 00 00 	.4byte .LFB1
 171 003c 36 00 00 00 	.4byte .LFE1-.LFB1
 172 0040 04          	.byte 0x4
 173 0041 02 00 00 00 	.4byte .LCFI1-.LFB1
 174 0045 13          	.byte 0x13
 175 0046 7D          	.sleb128 -3
 176 0047 0D          	.byte 0xd
 177 0048 0E          	.uleb128 0xe
 178 0049 8E          	.byte 0x8e
 179 004a 02          	.uleb128 0x2
 180 004b 00          	.align 4
 181                 	.LEFDE2:
 182                 	.section .text,code
 183              	.Letext0:
 184              	.file 2 "/opt/microchip/xc16/v1.36/bin/bin/../../include/lega-c/stdint.h"
 185              	.file 3 "lib/can-ids/CAN_IDs.h"
 186              	.file 4 "lib/can-ids/Devices/DASH_CAN.h"
 187              	.section .debug_info,info
 188 0000 E1 03 00 00 	.4byte 0x3e1
 189 0004 02 00       	.2byte 0x2
 190 0006 00 00 00 00 	.4byte .Ldebug_abbrev0
 191 000a 04          	.byte 0x4
 192 000b 01          	.uleb128 0x1
 193 000c 47 4E 55 20 	.asciz "GNU C 4.5.1 (XC16, Microchip v1.36) (A) Build date: Dec  5 2018"
MPLAB XC16 ASSEMBLY Listing:   			page 5


 193      43 20 34 2E 
 193      35 2E 31 20 
 193      28 58 43 31 
 193      36 2C 20 4D 
 193      69 63 72 6F 
 193      63 68 69 70 
 193      20 76 31 2E 
 193      33 36 29 20 
 194 004c 01          	.byte 0x1
 195 004d 6C 69 62 2F 	.asciz "lib/can-ids/Devices/DASH_CAN.c"
 195      63 61 6E 2D 
 195      69 64 73 2F 
 195      44 65 76 69 
 195      63 65 73 2F 
 195      44 41 53 48 
 195      5F 43 41 4E 
 195      2E 63 00 
 196 006c 2F 68 6F 6D 	.asciz "/home/user/Documents/FST/Programming/project_sub-zero"
 196      65 2F 75 73 
 196      65 72 2F 44 
 196      6F 63 75 6D 
 196      65 6E 74 73 
 196      2F 46 53 54 
 196      2F 50 72 6F 
 196      67 72 61 6D 
 196      6D 69 6E 67 
 197 00a2 00 00 00 00 	.4byte .Ltext0
 198 00a6 00 00 00 00 	.4byte .Letext0
 199 00aa 00 00 00 00 	.4byte .Ldebug_line0
 200 00ae 02          	.uleb128 0x2
 201 00af 01          	.byte 0x1
 202 00b0 06          	.byte 0x6
 203 00b1 73 69 67 6E 	.asciz "signed char"
 203      65 64 20 63 
 203      68 61 72 00 
 204 00bd 02          	.uleb128 0x2
 205 00be 02          	.byte 0x2
 206 00bf 05          	.byte 0x5
 207 00c0 69 6E 74 00 	.asciz "int"
 208 00c4 02          	.uleb128 0x2
 209 00c5 04          	.byte 0x4
 210 00c6 05          	.byte 0x5
 211 00c7 6C 6F 6E 67 	.asciz "long int"
 211      20 69 6E 74 
 211      00 
 212 00d0 02          	.uleb128 0x2
 213 00d1 08          	.byte 0x8
 214 00d2 05          	.byte 0x5
 215 00d3 6C 6F 6E 67 	.asciz "long long int"
 215      20 6C 6F 6E 
 215      67 20 69 6E 
 215      74 00 
 216 00e1 03          	.uleb128 0x3
 217 00e2 75 69 6E 74 	.asciz "uint8_t"
 217      38 5F 74 00 
 218 00ea 02          	.byte 0x2
 219 00eb 2B          	.byte 0x2b
MPLAB XC16 ASSEMBLY Listing:   			page 6


 220 00ec F0 00 00 00 	.4byte 0xf0
 221 00f0 02          	.uleb128 0x2
 222 00f1 01          	.byte 0x1
 223 00f2 08          	.byte 0x8
 224 00f3 75 6E 73 69 	.asciz "unsigned char"
 224      67 6E 65 64 
 224      20 63 68 61 
 224      72 00 
 225 0101 03          	.uleb128 0x3
 226 0102 75 69 6E 74 	.asciz "uint16_t"
 226      31 36 5F 74 
 226      00 
 227 010b 02          	.byte 0x2
 228 010c 31          	.byte 0x31
 229 010d 11 01 00 00 	.4byte 0x111
 230 0111 02          	.uleb128 0x2
 231 0112 02          	.byte 0x2
 232 0113 07          	.byte 0x7
 233 0114 75 6E 73 69 	.asciz "unsigned int"
 233      67 6E 65 64 
 233      20 69 6E 74 
 233      00 
 234 0121 02          	.uleb128 0x2
 235 0122 04          	.byte 0x4
 236 0123 07          	.byte 0x7
 237 0124 6C 6F 6E 67 	.asciz "long unsigned int"
 237      20 75 6E 73 
 237      69 67 6E 65 
 237      64 20 69 6E 
 237      74 00 
 238 0136 02          	.uleb128 0x2
 239 0137 08          	.byte 0x8
 240 0138 07          	.byte 0x7
 241 0139 6C 6F 6E 67 	.asciz "long long unsigned int"
 241      20 6C 6F 6E 
 241      67 20 75 6E 
 241      73 69 67 6E 
 241      65 64 20 69 
 241      6E 74 00 
 242 0150 02          	.uleb128 0x2
 243 0151 02          	.byte 0x2
 244 0152 07          	.byte 0x7
 245 0153 73 68 6F 72 	.asciz "short unsigned int"
 245      74 20 75 6E 
 245      73 69 67 6E 
 245      65 64 20 69 
 245      6E 74 00 
 246 0166 02          	.uleb128 0x2
 247 0167 01          	.byte 0x1
 248 0168 06          	.byte 0x6
 249 0169 63 68 61 72 	.asciz "char"
 249      00 
 250 016e 04          	.uleb128 0x4
 251 016f 02          	.byte 0x2
 252 0170 03          	.byte 0x3
 253 0171 12          	.byte 0x12
 254 0172 9F 01 00 00 	.4byte 0x19f
MPLAB XC16 ASSEMBLY Listing:   			page 7


 255 0176 05          	.uleb128 0x5
 256 0177 64 65 76 5F 	.asciz "dev_id"
 256      69 64 00 
 257 017e 03          	.byte 0x3
 258 017f 13          	.byte 0x13
 259 0180 01 01 00 00 	.4byte 0x101
 260 0184 02          	.byte 0x2
 261 0185 05          	.byte 0x5
 262 0186 0B          	.byte 0xb
 263 0187 02          	.byte 0x2
 264 0188 23          	.byte 0x23
 265 0189 00          	.uleb128 0x0
 266 018a 05          	.uleb128 0x5
 267 018b 6D 73 67 5F 	.asciz "msg_id"
 267      69 64 00 
 268 0192 03          	.byte 0x3
 269 0193 16          	.byte 0x16
 270 0194 01 01 00 00 	.4byte 0x101
 271 0198 02          	.byte 0x2
 272 0199 06          	.byte 0x6
 273 019a 05          	.byte 0x5
 274 019b 02          	.byte 0x2
 275 019c 23          	.byte 0x23
 276 019d 00          	.uleb128 0x0
 277 019e 00          	.byte 0x0
 278 019f 06          	.uleb128 0x6
 279 01a0 02          	.byte 0x2
 280 01a1 03          	.byte 0x3
 281 01a2 11          	.byte 0x11
 282 01a3 B8 01 00 00 	.4byte 0x1b8
 283 01a7 07          	.uleb128 0x7
 284 01a8 6E 01 00 00 	.4byte 0x16e
 285 01ac 08          	.uleb128 0x8
 286 01ad 73 69 64 00 	.asciz "sid"
 287 01b1 03          	.byte 0x3
 288 01b2 18          	.byte 0x18
 289 01b3 01 01 00 00 	.4byte 0x101
 290 01b7 00          	.byte 0x0
 291 01b8 04          	.uleb128 0x4
 292 01b9 0C          	.byte 0xc
 293 01ba 03          	.byte 0x3
 294 01bb 10          	.byte 0x10
 295 01bc E9 01 00 00 	.4byte 0x1e9
 296 01c0 09          	.uleb128 0x9
 297 01c1 9F 01 00 00 	.4byte 0x19f
 298 01c5 02          	.byte 0x2
 299 01c6 23          	.byte 0x23
 300 01c7 00          	.uleb128 0x0
 301 01c8 05          	.uleb128 0x5
 302 01c9 64 6C 63 00 	.asciz "dlc"
 303 01cd 03          	.byte 0x3
 304 01ce 1A          	.byte 0x1a
 305 01cf 01 01 00 00 	.4byte 0x101
 306 01d3 02          	.byte 0x2
 307 01d4 04          	.byte 0x4
 308 01d5 0C          	.byte 0xc
 309 01d6 02          	.byte 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 8


 310 01d7 23          	.byte 0x23
 311 01d8 02          	.uleb128 0x2
 312 01d9 0A          	.uleb128 0xa
 313 01da 64 61 74 61 	.asciz "data"
 313      00 
 314 01df 03          	.byte 0x3
 315 01e0 1B          	.byte 0x1b
 316 01e1 E9 01 00 00 	.4byte 0x1e9
 317 01e5 02          	.byte 0x2
 318 01e6 23          	.byte 0x23
 319 01e7 04          	.uleb128 0x4
 320 01e8 00          	.byte 0x0
 321 01e9 0B          	.uleb128 0xb
 322 01ea 01 01 00 00 	.4byte 0x101
 323 01ee F9 01 00 00 	.4byte 0x1f9
 324 01f2 0C          	.uleb128 0xc
 325 01f3 11 01 00 00 	.4byte 0x111
 326 01f7 03          	.byte 0x3
 327 01f8 00          	.byte 0x0
 328 01f9 03          	.uleb128 0x3
 329 01fa 43 41 4E 64 	.asciz "CANdata"
 329      61 74 61 00 
 330 0202 03          	.byte 0x3
 331 0203 1C          	.byte 0x1c
 332 0204 B8 01 00 00 	.4byte 0x1b8
 333 0208 04          	.uleb128 0x4
 334 0209 01          	.byte 0x1
 335 020a 04          	.byte 0x4
 336 020b 0F          	.byte 0xf
 337 020c 8D 02 00 00 	.4byte 0x28d
 338 0210 05          	.uleb128 0x5
 339 0211 41 49 52 5F 	.asciz "AIR_plus"
 339      70 6C 75 73 
 339      00 
 340 021a 04          	.byte 0x4
 341 021b 10          	.byte 0x10
 342 021c 8D 02 00 00 	.4byte 0x28d
 343 0220 01          	.byte 0x1
 344 0221 01          	.byte 0x1
 345 0222 07          	.byte 0x7
 346 0223 02          	.byte 0x2
 347 0224 23          	.byte 0x23
 348 0225 00          	.uleb128 0x0
 349 0226 05          	.uleb128 0x5
 350 0227 41 49 52 5F 	.asciz "AIR_minus"
 350      6D 69 6E 75 
 350      73 00 
 351 0231 04          	.byte 0x4
 352 0232 11          	.byte 0x11
 353 0233 8D 02 00 00 	.4byte 0x28d
 354 0237 01          	.byte 0x1
 355 0238 01          	.byte 0x1
 356 0239 06          	.byte 0x6
 357 023a 02          	.byte 0x2
 358 023b 23          	.byte 0x23
 359 023c 00          	.uleb128 0x0
 360 023d 05          	.uleb128 0x5
MPLAB XC16 ASSEMBLY Listing:   			page 9


 361 023e 49 4D 44 00 	.asciz "IMD"
 362 0242 04          	.byte 0x4
 363 0243 12          	.byte 0x12
 364 0244 8D 02 00 00 	.4byte 0x28d
 365 0248 01          	.byte 0x1
 366 0249 01          	.byte 0x1
 367 024a 05          	.byte 0x5
 368 024b 02          	.byte 0x2
 369 024c 23          	.byte 0x23
 370 024d 00          	.uleb128 0x0
 371 024e 05          	.uleb128 0x5
 372 024f 49 4D 44 5F 	.asciz "IMD_latch"
 372      6C 61 74 63 
 372      68 00 
 373 0259 04          	.byte 0x4
 374 025a 13          	.byte 0x13
 375 025b 8D 02 00 00 	.4byte 0x28d
 376 025f 01          	.byte 0x1
 377 0260 01          	.byte 0x1
 378 0261 04          	.byte 0x4
 379 0262 02          	.byte 0x2
 380 0263 23          	.byte 0x23
 381 0264 00          	.uleb128 0x0
 382 0265 05          	.uleb128 0x5
 383 0266 41 4D 53 00 	.asciz "AMS"
 384 026a 04          	.byte 0x4
 385 026b 14          	.byte 0x14
 386 026c 8D 02 00 00 	.4byte 0x28d
 387 0270 01          	.byte 0x1
 388 0271 01          	.byte 0x1
 389 0272 03          	.byte 0x3
 390 0273 02          	.byte 0x2
 391 0274 23          	.byte 0x23
 392 0275 00          	.uleb128 0x0
 393 0276 05          	.uleb128 0x5
 394 0277 52 54 44 5F 	.asciz "RTD_mode"
 394      6D 6F 64 65 
 394      00 
 395 0280 04          	.byte 0x4
 396 0281 15          	.byte 0x15
 397 0282 8D 02 00 00 	.4byte 0x28d
 398 0286 01          	.byte 0x1
 399 0287 01          	.byte 0x1
 400 0288 02          	.byte 0x2
 401 0289 02          	.byte 0x2
 402 028a 23          	.byte 0x23
 403 028b 00          	.uleb128 0x0
 404 028c 00          	.byte 0x0
 405 028d 02          	.uleb128 0x2
 406 028e 01          	.byte 0x1
 407 028f 02          	.byte 0x2
 408 0290 5F 42 6F 6F 	.asciz "_Bool"
 408      6C 00 
 409 0296 06          	.uleb128 0x6
 410 0297 01          	.byte 0x1
 411 0298 04          	.byte 0x4
 412 0299 0E          	.byte 0xe
MPLAB XC16 ASSEMBLY Listing:   			page 10


 413 029a B3 02 00 00 	.4byte 0x2b3
 414 029e 07          	.uleb128 0x7
 415 029f 08 02 00 00 	.4byte 0x208
 416 02a3 0D          	.uleb128 0xd
 417 02a4 4C 45 44 73 	.asciz "LEDs"
 417      00 
 418 02a9 04          	.byte 0x4
 419 02aa 17          	.byte 0x17
 420 02ab E1 00 00 00 	.4byte 0xe1
 421 02af 01          	.byte 0x1
 422 02b0 06          	.byte 0x6
 423 02b1 02          	.byte 0x2
 424 02b2 00          	.byte 0x0
 425 02b3 04          	.uleb128 0x4
 426 02b4 04          	.byte 0x4
 427 02b5 04          	.byte 0x4
 428 02b6 0A          	.byte 0xa
 429 02b7 FB 02 00 00 	.4byte 0x2fb
 430 02bb 0A          	.uleb128 0xa
 431 02bc 62 6F 61 72 	.asciz "board_temp"
 431      64 5F 74 65 
 431      6D 70 00 
 432 02c7 04          	.byte 0x4
 433 02c8 0C          	.byte 0xc
 434 02c9 E1 00 00 00 	.4byte 0xe1
 435 02cd 02          	.byte 0x2
 436 02ce 23          	.byte 0x23
 437 02cf 00          	.uleb128 0x0
 438 02d0 09          	.uleb128 0x9
 439 02d1 96 02 00 00 	.4byte 0x296
 440 02d5 02          	.byte 0x2
 441 02d6 23          	.byte 0x23
 442 02d7 01          	.uleb128 0x1
 443 02d8 0A          	.uleb128 0xa
 444 02d9 53 43 00    	.asciz "SC"
 445 02dc 04          	.byte 0x4
 446 02dd 19          	.byte 0x19
 447 02de 8D 02 00 00 	.4byte 0x28d
 448 02e2 02          	.byte 0x2
 449 02e3 23          	.byte 0x23
 450 02e4 02          	.uleb128 0x2
 451 02e5 0A          	.uleb128 0xa
 452 02e6 64 65 62 75 	.asciz "debug_mode"
 452      67 5F 6D 6F 
 452      64 65 00 
 453 02f1 04          	.byte 0x4
 454 02f2 1A          	.byte 0x1a
 455 02f3 8D 02 00 00 	.4byte 0x28d
 456 02f7 02          	.byte 0x2
 457 02f8 23          	.byte 0x23
 458 02f9 03          	.uleb128 0x3
 459 02fa 00          	.byte 0x0
 460 02fb 03          	.uleb128 0x3
 461 02fc 44 41 53 48 	.asciz "DASH_MSG_status"
 461      5F 4D 53 47 
 461      5F 73 74 61 
 461      74 75 73 00 
MPLAB XC16 ASSEMBLY Listing:   			page 11


 462 030c 04          	.byte 0x4
 463 030d 1B          	.byte 0x1b
 464 030e B3 02 00 00 	.4byte 0x2b3
 465 0312 04          	.uleb128 0x4
 466 0313 04          	.byte 0x4
 467 0314 04          	.byte 0x4
 468 0315 1F          	.byte 0x1f
 469 0316 2C 03 00 00 	.4byte 0x32c
 470 031a 0A          	.uleb128 0xa
 471 031b 73 74 61 74 	.asciz "status"
 471      75 73 00 
 472 0322 04          	.byte 0x4
 473 0323 20          	.byte 0x20
 474 0324 FB 02 00 00 	.4byte 0x2fb
 475 0328 02          	.byte 0x2
 476 0329 23          	.byte 0x23
 477 032a 00          	.uleb128 0x0
 478 032b 00          	.byte 0x0
 479 032c 03          	.uleb128 0x3
 480 032d 44 41 53 48 	.asciz "DASH_CAN_Data"
 480      5F 43 41 4E 
 480      5F 44 61 74 
 480      61 00 
 481 033b 04          	.byte 0x4
 482 033c 21          	.byte 0x21
 483 033d 12 03 00 00 	.4byte 0x312
 484 0341 0E          	.uleb128 0xe
 485 0342 01          	.byte 0x1
 486 0343 70 61 72 73 	.asciz "parse_can_message_status"
 486      65 5F 63 61 
 486      6E 5F 6D 65 
 486      73 73 61 67 
 486      65 5F 73 74 
 486      61 74 75 73 
 486      00 
 487 035c 01          	.byte 0x1
 488 035d 09          	.byte 0x9
 489 035e 01          	.byte 0x1
 490 035f 00 00 00 00 	.4byte .LFB0
 491 0363 00 00 00 00 	.4byte .LFE0
 492 0367 01          	.byte 0x1
 493 0368 5E          	.byte 0x5e
 494 0369 8E 03 00 00 	.4byte 0x38e
 495 036d 0F          	.uleb128 0xf
 496 036e 64 61 74 61 	.asciz "data"
 496      00 
 497 0373 01          	.byte 0x1
 498 0374 09          	.byte 0x9
 499 0375 8E 03 00 00 	.4byte 0x38e
 500 0379 02          	.byte 0x2
 501 037a 7E          	.byte 0x7e
 502 037b 00          	.sleb128 0
 503 037c 0F          	.uleb128 0xf
 504 037d 73 74 61 74 	.asciz "status"
 504      75 73 00 
 505 0384 01          	.byte 0x1
 506 0385 09          	.byte 0x9
MPLAB XC16 ASSEMBLY Listing:   			page 12


 507 0386 94 03 00 00 	.4byte 0x394
 508 038a 02          	.byte 0x2
 509 038b 7E          	.byte 0x7e
 510 038c 02          	.sleb128 2
 511 038d 00          	.byte 0x0
 512 038e 10          	.uleb128 0x10
 513 038f 02          	.byte 0x2
 514 0390 01 01 00 00 	.4byte 0x101
 515 0394 10          	.uleb128 0x10
 516 0395 02          	.byte 0x2
 517 0396 FB 02 00 00 	.4byte 0x2fb
 518 039a 0E          	.uleb128 0xe
 519 039b 01          	.byte 0x1
 520 039c 70 61 72 73 	.asciz "parse_can_dash"
 520      65 5F 63 61 
 520      6E 5F 64 61 
 520      73 68 00 
 521 03ab 01          	.byte 0x1
 522 03ac 10          	.byte 0x10
 523 03ad 01          	.byte 0x1
 524 03ae 00 00 00 00 	.4byte .LFB1
 525 03b2 00 00 00 00 	.4byte .LFE1
 526 03b6 01          	.byte 0x1
 527 03b7 5E          	.byte 0x5e
 528 03b8 DE 03 00 00 	.4byte 0x3de
 529 03bc 0F          	.uleb128 0xf
 530 03bd 6D 65 73 73 	.asciz "message"
 530      61 67 65 00 
 531 03c5 01          	.byte 0x1
 532 03c6 10          	.byte 0x10
 533 03c7 F9 01 00 00 	.4byte 0x1f9
 534 03cb 02          	.byte 0x2
 535 03cc 7E          	.byte 0x7e
 536 03cd 00          	.sleb128 0
 537 03ce 0F          	.uleb128 0xf
 538 03cf 64 61 74 61 	.asciz "data"
 538      00 
 539 03d4 01          	.byte 0x1
 540 03d5 10          	.byte 0x10
 541 03d6 DE 03 00 00 	.4byte 0x3de
 542 03da 02          	.byte 0x2
 543 03db 7E          	.byte 0x7e
 544 03dc 0C          	.sleb128 12
 545 03dd 00          	.byte 0x0
 546 03de 10          	.uleb128 0x10
 547 03df 02          	.byte 0x2
 548 03e0 2C 03 00 00 	.4byte 0x32c
 549 03e4 00          	.byte 0x0
 550                 	.section .debug_abbrev,info
 551 0000 01          	.uleb128 0x1
 552 0001 11          	.uleb128 0x11
 553 0002 01          	.byte 0x1
 554 0003 25          	.uleb128 0x25
 555 0004 08          	.uleb128 0x8
 556 0005 13          	.uleb128 0x13
 557 0006 0B          	.uleb128 0xb
 558 0007 03          	.uleb128 0x3
MPLAB XC16 ASSEMBLY Listing:   			page 13


 559 0008 08          	.uleb128 0x8
 560 0009 1B          	.uleb128 0x1b
 561 000a 08          	.uleb128 0x8
 562 000b 11          	.uleb128 0x11
 563 000c 01          	.uleb128 0x1
 564 000d 12          	.uleb128 0x12
 565 000e 01          	.uleb128 0x1
 566 000f 10          	.uleb128 0x10
 567 0010 06          	.uleb128 0x6
 568 0011 00          	.byte 0x0
 569 0012 00          	.byte 0x0
 570 0013 02          	.uleb128 0x2
 571 0014 24          	.uleb128 0x24
 572 0015 00          	.byte 0x0
 573 0016 0B          	.uleb128 0xb
 574 0017 0B          	.uleb128 0xb
 575 0018 3E          	.uleb128 0x3e
 576 0019 0B          	.uleb128 0xb
 577 001a 03          	.uleb128 0x3
 578 001b 08          	.uleb128 0x8
 579 001c 00          	.byte 0x0
 580 001d 00          	.byte 0x0
 581 001e 03          	.uleb128 0x3
 582 001f 16          	.uleb128 0x16
 583 0020 00          	.byte 0x0
 584 0021 03          	.uleb128 0x3
 585 0022 08          	.uleb128 0x8
 586 0023 3A          	.uleb128 0x3a
 587 0024 0B          	.uleb128 0xb
 588 0025 3B          	.uleb128 0x3b
 589 0026 0B          	.uleb128 0xb
 590 0027 49          	.uleb128 0x49
 591 0028 13          	.uleb128 0x13
 592 0029 00          	.byte 0x0
 593 002a 00          	.byte 0x0
 594 002b 04          	.uleb128 0x4
 595 002c 13          	.uleb128 0x13
 596 002d 01          	.byte 0x1
 597 002e 0B          	.uleb128 0xb
 598 002f 0B          	.uleb128 0xb
 599 0030 3A          	.uleb128 0x3a
 600 0031 0B          	.uleb128 0xb
 601 0032 3B          	.uleb128 0x3b
 602 0033 0B          	.uleb128 0xb
 603 0034 01          	.uleb128 0x1
 604 0035 13          	.uleb128 0x13
 605 0036 00          	.byte 0x0
 606 0037 00          	.byte 0x0
 607 0038 05          	.uleb128 0x5
 608 0039 0D          	.uleb128 0xd
 609 003a 00          	.byte 0x0
 610 003b 03          	.uleb128 0x3
 611 003c 08          	.uleb128 0x8
 612 003d 3A          	.uleb128 0x3a
 613 003e 0B          	.uleb128 0xb
 614 003f 3B          	.uleb128 0x3b
 615 0040 0B          	.uleb128 0xb
MPLAB XC16 ASSEMBLY Listing:   			page 14


 616 0041 49          	.uleb128 0x49
 617 0042 13          	.uleb128 0x13
 618 0043 0B          	.uleb128 0xb
 619 0044 0B          	.uleb128 0xb
 620 0045 0D          	.uleb128 0xd
 621 0046 0B          	.uleb128 0xb
 622 0047 0C          	.uleb128 0xc
 623 0048 0B          	.uleb128 0xb
 624 0049 38          	.uleb128 0x38
 625 004a 0A          	.uleb128 0xa
 626 004b 00          	.byte 0x0
 627 004c 00          	.byte 0x0
 628 004d 06          	.uleb128 0x6
 629 004e 17          	.uleb128 0x17
 630 004f 01          	.byte 0x1
 631 0050 0B          	.uleb128 0xb
 632 0051 0B          	.uleb128 0xb
 633 0052 3A          	.uleb128 0x3a
 634 0053 0B          	.uleb128 0xb
 635 0054 3B          	.uleb128 0x3b
 636 0055 0B          	.uleb128 0xb
 637 0056 01          	.uleb128 0x1
 638 0057 13          	.uleb128 0x13
 639 0058 00          	.byte 0x0
 640 0059 00          	.byte 0x0
 641 005a 07          	.uleb128 0x7
 642 005b 0D          	.uleb128 0xd
 643 005c 00          	.byte 0x0
 644 005d 49          	.uleb128 0x49
 645 005e 13          	.uleb128 0x13
 646 005f 00          	.byte 0x0
 647 0060 00          	.byte 0x0
 648 0061 08          	.uleb128 0x8
 649 0062 0D          	.uleb128 0xd
 650 0063 00          	.byte 0x0
 651 0064 03          	.uleb128 0x3
 652 0065 08          	.uleb128 0x8
 653 0066 3A          	.uleb128 0x3a
 654 0067 0B          	.uleb128 0xb
 655 0068 3B          	.uleb128 0x3b
 656 0069 0B          	.uleb128 0xb
 657 006a 49          	.uleb128 0x49
 658 006b 13          	.uleb128 0x13
 659 006c 00          	.byte 0x0
 660 006d 00          	.byte 0x0
 661 006e 09          	.uleb128 0x9
 662 006f 0D          	.uleb128 0xd
 663 0070 00          	.byte 0x0
 664 0071 49          	.uleb128 0x49
 665 0072 13          	.uleb128 0x13
 666 0073 38          	.uleb128 0x38
 667 0074 0A          	.uleb128 0xa
 668 0075 00          	.byte 0x0
 669 0076 00          	.byte 0x0
 670 0077 0A          	.uleb128 0xa
 671 0078 0D          	.uleb128 0xd
 672 0079 00          	.byte 0x0
MPLAB XC16 ASSEMBLY Listing:   			page 15


 673 007a 03          	.uleb128 0x3
 674 007b 08          	.uleb128 0x8
 675 007c 3A          	.uleb128 0x3a
 676 007d 0B          	.uleb128 0xb
 677 007e 3B          	.uleb128 0x3b
 678 007f 0B          	.uleb128 0xb
 679 0080 49          	.uleb128 0x49
 680 0081 13          	.uleb128 0x13
 681 0082 38          	.uleb128 0x38
 682 0083 0A          	.uleb128 0xa
 683 0084 00          	.byte 0x0
 684 0085 00          	.byte 0x0
 685 0086 0B          	.uleb128 0xb
 686 0087 01          	.uleb128 0x1
 687 0088 01          	.byte 0x1
 688 0089 49          	.uleb128 0x49
 689 008a 13          	.uleb128 0x13
 690 008b 01          	.uleb128 0x1
 691 008c 13          	.uleb128 0x13
 692 008d 00          	.byte 0x0
 693 008e 00          	.byte 0x0
 694 008f 0C          	.uleb128 0xc
 695 0090 21          	.uleb128 0x21
 696 0091 00          	.byte 0x0
 697 0092 49          	.uleb128 0x49
 698 0093 13          	.uleb128 0x13
 699 0094 2F          	.uleb128 0x2f
 700 0095 0B          	.uleb128 0xb
 701 0096 00          	.byte 0x0
 702 0097 00          	.byte 0x0
 703 0098 0D          	.uleb128 0xd
 704 0099 0D          	.uleb128 0xd
 705 009a 00          	.byte 0x0
 706 009b 03          	.uleb128 0x3
 707 009c 08          	.uleb128 0x8
 708 009d 3A          	.uleb128 0x3a
 709 009e 0B          	.uleb128 0xb
 710 009f 3B          	.uleb128 0x3b
 711 00a0 0B          	.uleb128 0xb
 712 00a1 49          	.uleb128 0x49
 713 00a2 13          	.uleb128 0x13
 714 00a3 0B          	.uleb128 0xb
 715 00a4 0B          	.uleb128 0xb
 716 00a5 0D          	.uleb128 0xd
 717 00a6 0B          	.uleb128 0xb
 718 00a7 0C          	.uleb128 0xc
 719 00a8 0B          	.uleb128 0xb
 720 00a9 00          	.byte 0x0
 721 00aa 00          	.byte 0x0
 722 00ab 0E          	.uleb128 0xe
 723 00ac 2E          	.uleb128 0x2e
 724 00ad 01          	.byte 0x1
 725 00ae 3F          	.uleb128 0x3f
 726 00af 0C          	.uleb128 0xc
 727 00b0 03          	.uleb128 0x3
 728 00b1 08          	.uleb128 0x8
 729 00b2 3A          	.uleb128 0x3a
MPLAB XC16 ASSEMBLY Listing:   			page 16


 730 00b3 0B          	.uleb128 0xb
 731 00b4 3B          	.uleb128 0x3b
 732 00b5 0B          	.uleb128 0xb
 733 00b6 27          	.uleb128 0x27
 734 00b7 0C          	.uleb128 0xc
 735 00b8 11          	.uleb128 0x11
 736 00b9 01          	.uleb128 0x1
 737 00ba 12          	.uleb128 0x12
 738 00bb 01          	.uleb128 0x1
 739 00bc 40          	.uleb128 0x40
 740 00bd 0A          	.uleb128 0xa
 741 00be 01          	.uleb128 0x1
 742 00bf 13          	.uleb128 0x13
 743 00c0 00          	.byte 0x0
 744 00c1 00          	.byte 0x0
 745 00c2 0F          	.uleb128 0xf
 746 00c3 05          	.uleb128 0x5
 747 00c4 00          	.byte 0x0
 748 00c5 03          	.uleb128 0x3
 749 00c6 08          	.uleb128 0x8
 750 00c7 3A          	.uleb128 0x3a
 751 00c8 0B          	.uleb128 0xb
 752 00c9 3B          	.uleb128 0x3b
 753 00ca 0B          	.uleb128 0xb
 754 00cb 49          	.uleb128 0x49
 755 00cc 13          	.uleb128 0x13
 756 00cd 02          	.uleb128 0x2
 757 00ce 0A          	.uleb128 0xa
 758 00cf 00          	.byte 0x0
 759 00d0 00          	.byte 0x0
 760 00d1 10          	.uleb128 0x10
 761 00d2 0F          	.uleb128 0xf
 762 00d3 00          	.byte 0x0
 763 00d4 0B          	.uleb128 0xb
 764 00d5 0B          	.uleb128 0xb
 765 00d6 49          	.uleb128 0x49
 766 00d7 13          	.uleb128 0x13
 767 00d8 00          	.byte 0x0
 768 00d9 00          	.byte 0x0
 769 00da 00          	.byte 0x0
 770                 	.section .debug_pubnames,info
 771 0000 3E 00 00 00 	.4byte 0x3e
 772 0004 02 00       	.2byte 0x2
 773 0006 00 00 00 00 	.4byte .Ldebug_info0
 774 000a E5 03 00 00 	.4byte 0x3e5
 775 000e 41 03 00 00 	.4byte 0x341
 776 0012 70 61 72 73 	.asciz "parse_can_message_status"
 776      65 5F 63 61 
 776      6E 5F 6D 65 
 776      73 73 61 67 
 776      65 5F 73 74 
 776      61 74 75 73 
 776      00 
 777 002b 9A 03 00 00 	.4byte 0x39a
 778 002f 70 61 72 73 	.asciz "parse_can_dash"
 778      65 5F 63 61 
 778      6E 5F 64 61 
MPLAB XC16 ASSEMBLY Listing:   			page 17


 778      73 68 00 
 779 003e 00 00 00 00 	.4byte 0x0
 780                 	.section .debug_pubtypes,info
 781 0000 59 00 00 00 	.4byte 0x59
 782 0004 02 00       	.2byte 0x2
 783 0006 00 00 00 00 	.4byte .Ldebug_info0
 784 000a E5 03 00 00 	.4byte 0x3e5
 785 000e E1 00 00 00 	.4byte 0xe1
 786 0012 75 69 6E 74 	.asciz "uint8_t"
 786      38 5F 74 00 
 787 001a 01 01 00 00 	.4byte 0x101
 788 001e 75 69 6E 74 	.asciz "uint16_t"
 788      31 36 5F 74 
 788      00 
 789 0027 F9 01 00 00 	.4byte 0x1f9
 790 002b 43 41 4E 64 	.asciz "CANdata"
 790      61 74 61 00 
 791 0033 FB 02 00 00 	.4byte 0x2fb
 792 0037 44 41 53 48 	.asciz "DASH_MSG_status"
 792      5F 4D 53 47 
 792      5F 73 74 61 
 792      74 75 73 00 
 793 0047 2C 03 00 00 	.4byte 0x32c
 794 004b 44 41 53 48 	.asciz "DASH_CAN_Data"
 794      5F 43 41 4E 
 794      5F 44 61 74 
 794      61 00 
 795 0059 00 00 00 00 	.4byte 0x0
 796                 	.section .debug_aranges,info
 797 0000 14 00 00 00 	.4byte 0x14
 798 0004 02 00       	.2byte 0x2
 799 0006 00 00 00 00 	.4byte .Ldebug_info0
 800 000a 04          	.byte 0x4
 801 000b 00          	.byte 0x0
 802 000c 00 00       	.2byte 0x0
 803 000e 00 00       	.2byte 0x0
 804 0010 00 00 00 00 	.4byte 0x0
 805 0014 00 00 00 00 	.4byte 0x0
 806                 	.section .debug_str,info
 807                 	.section .text,code
 808              	
 809              	
 810              	
 811              	.section __c30_info,info,bss
 812                 	__psv_trap_errata:
 813                 	
 814                 	.section __c30_signature,info,data
 815 0000 01 00       	.word 0x0001
 816 0002 00 00       	.word 0x0000
 817 0004 00 00       	.word 0x0000
 818                 	
 819                 	
 820                 	
 821                 	.set ___PA___,0
 822                 	.end
MPLAB XC16 ASSEMBLY Listing:   			page 18


DEFINED SYMBOLS
                            *ABS*:00000000 /home/user/Documents/FST/Programming/project_sub-zero/lib/can-ids/Devices/DASH_CAN.c
                            *ABS*:00000001 __33EP256MU806
                            *ABS*:00000001 __dsPIC33E
                            *ABS*:00000001 __HAS_DSP
                            *ABS*:00000001 __HAS_DMAV2
                            *ABS*:00000001 __HAS_CODEGUARD
                            *ABS*:00000001 __HAS_PMP_ENHANCED
                            *ABS*:00000001 __HAS_EDS
                            *ABS*:00000001 __HAS_EP
                            *ABS*:00000011 __TARGET_DIVIDE_CYCLES
                            *ABS*:00000001 __C30ELF
    {standard input}:14     .text:00000000 _parse_can_message_status
    {standard input}:18     *ABS*:00000000 ___PA___
    {standard input}:84     .text:00000068 _parse_can_dash
    {standard input}:104    *ABS*:00000000 ___BP___
    {standard input}:812    __c30_info:00000000 __psv_trap_errata
    {standard input}:19     .text:00000000 .L0
                            .text:00000096 .L6
                            .text:00000096 .L2
                    .debug_abbrev:00000000 .Ldebug_abbrev0
                            .text:00000000 .Ltext0
                            .text:0000009e .Letext0
                      .debug_line:00000000 .Ldebug_line0
                            .text:00000000 .LFB0
                            .text:00000068 .LFE0
                            .text:00000068 .LFB1
                            .text:0000009e .LFE1
                     .debug_frame:00000000 .Lframe0
                      .debug_info:00000000 .Ldebug_info0

UNDEFINED SYMBOLS
CORCON

EQUATE SYMBOLS
/home/user/Documents/FST/Programming/project_sub-zero/lib/can-ids/Devices/DASH_CAN.c = 0x0
                  __33EP256MU806 = 0x1
                      __dsPIC33E = 0x1
                       __HAS_DSP = 0x1
                     __HAS_DMAV2 = 0x1
                 __HAS_CODEGUARD = 0x1
              __HAS_PMP_ENHANCED = 0x1
                       __HAS_EDS = 0x1
                        __HAS_EP = 0x1
          __TARGET_DIVIDE_CYCLES = 0x11
                        __C30ELF = 0x1
                        ___PA___ = 0x0
                        ___BP___ = 0x0
