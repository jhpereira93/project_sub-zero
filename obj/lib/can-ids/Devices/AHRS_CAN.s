MPLAB XC16 ASSEMBLY Listing:   			page 1


   1              	.file "/home/user/Documents/FST/Programming/project_sub-zero/lib/can-ids/Devices/AHRS_CAN.c"
   2              	.section .debug_abbrev,info
   3                 	.Ldebug_abbrev0:
   4                 	.section .debug_info,info
   5                 	.Ldebug_info0:
   6                 	.section .debug_line,info
   7                 	.Ldebug_line0:
   8 0000 CA 01 00 00 	.section .text,code
   8      02 00 9C 00 
   8      00 00 01 01 
   8      FB 0E 0A 00 
   8      01 01 01 01 
   8      00 00 00 01 
   8      6C 69 62 2F 
   8      63 61 6E 2D 
   8      69 64 73 2F 
   9              	.Ltext0:
  10              	.section .text,code
  11              	.align 2
  12              	.global _parse_can_message_health_temperature
  13              	.type _parse_can_message_health_temperature,@function
  14              	_parse_can_message_health_temperature:
  15              	.LFB0:
  16              	.file 1 "lib/can-ids/Devices/AHRS_CAN.c"
   1:lib/can-ids/Devices/AHRS_CAN.c **** #include "AHRS_CAN.h"
   2:lib/can-ids/Devices/AHRS_CAN.c **** 
   3:lib/can-ids/Devices/AHRS_CAN.c **** /* parse fuctions */
   4:lib/can-ids/Devices/AHRS_CAN.c **** 
   5:lib/can-ids/Devices/AHRS_CAN.c **** void parse_can_message_health_temperature(uint16_t data[4], AHRS_MSG_HEALTH_TEMPERATURE *health_tem
  17              	.loc 1 5 0
  18              	.set ___PA___,1
  19 000000  06 00 FA 	lnk #6
  20              	.LCFI0:
  21              	.loc 1 5 0
  22 000002  10 07 98 	mov w0,[w14+2]
   6:lib/can-ids/Devices/AHRS_CAN.c **** 
   7:lib/can-ids/Devices/AHRS_CAN.c ****     int16_t temperature;
   8:lib/can-ids/Devices/AHRS_CAN.c **** 
   9:lib/can-ids/Devices/AHRS_CAN.c ****     health_temperature->health = 0b00111111 & data[0];
  23              	.loc 1 9 0
  24 000004  1E 00 90 	mov [w14+2],w0
  25              	.loc 1 5 0
  26 000006  21 07 98 	mov w1,[w14+4]
  27              	.loc 1 9 0
  28 000008  90 00 78 	mov [w0],w1
  29 00000a  2E 00 90 	mov [w14+4],w0
  30 00000c  81 40 78 	mov.b w1,w1
  31 00000e  F1 43 B2 	and.b #63,w1
  32 000010  01 48 78 	mov.b w1,[w0]
  10:lib/can-ids/Devices/AHRS_CAN.c ****     temperature  = (int16_t) data[1] ;
  33              	.loc 1 10 0
  34 000012  1E 00 90 	mov [w14+2],w0
  35 000014  80 80 E8 	inc2 w0,w1
  11:lib/can-ids/Devices/AHRS_CAN.c ****     health_temperature->temperature_time = data[2];
  36              	.loc 1 11 0
  37 000016  1E 00 90 	mov [w14+2],w0
  38              	.loc 1 10 0
MPLAB XC16 ASSEMBLY Listing:   			page 2


  39 000018  00 00 00 	nop 
  40 00001a  91 00 78 	mov [w1],w1
  41              	.loc 1 11 0
  42 00001c  64 00 40 	add w0,#4,w0
  43              	.loc 1 10 0
  44 00001e  01 0F 78 	mov w1,[w14]
  45              	.loc 1 11 0
  46 000020  AE 00 90 	mov [w14+4],w1
  47 000022  10 01 78 	mov [w0],w2
  12:lib/can-ids/Devices/AHRS_CAN.c **** 
  13:lib/can-ids/Devices/AHRS_CAN.c ****     health_temperature->temperature = temperature /100;
  48              	.loc 1 13 0
  49 000024  1E 00 78 	mov [w14],w0
  50              	.loc 1 11 0
  51 000026  92 00 98 	mov w2,[w1+2]
  52              	.loc 1 13 0
  53 000028  4F 81 DE 	asr w0,#15,w2
  54 00002a  B1 47 21 	mov #5243,w1
  55 00002c  01 80 B9 	mul.ss w0,w1,w0
  56 00002e  43 88 DE 	asr w1,#3,w0
  57 000030  02 00 50 	sub w0,w2,w0
  58 000032  CF 80 DE 	asr w0,#15,w1
  59 000034  00 00 07 	rcall ___floatsisf
  60 000036  2E 01 90 	mov [w14+4],w2
  61 000038  20 01 98 	mov w0,[w2+4]
  62 00003a  31 01 98 	mov w1,[w2+6]
  14:lib/can-ids/Devices/AHRS_CAN.c **** 
  15:lib/can-ids/Devices/AHRS_CAN.c ****     return;
  16:lib/can-ids/Devices/AHRS_CAN.c **** 
  17:lib/can-ids/Devices/AHRS_CAN.c **** }
  63              	.loc 1 17 0
  64 00003c  8E 07 78 	mov w14,w15
  65 00003e  4F 07 78 	mov [--w15],w14
  66 000040  00 40 A9 	bclr CORCON,#2
  67 000042  00 00 06 	return 
  68              	.set ___PA___,0
  69              	.LFE0:
  70              	.size _parse_can_message_health_temperature,.-_parse_can_message_health_temperature
  71              	.align 2
  72              	.global _parse_can_message_gyro_z_accel_x
  73              	.type _parse_can_message_gyro_z_accel_x,@function
  74              	_parse_can_message_gyro_z_accel_x:
  75              	.LFB1:
  18:lib/can-ids/Devices/AHRS_CAN.c **** void parse_can_message_gyro_z_accel_x(uint16_t data[4], AHRS_MSG_GYRO *gyro, AHRS_MSG_ACCEL *accel)
  76              	.loc 1 18 0
  77              	.set ___PA___,1
  78 000044  0A 00 FA 	lnk #10
  79              	.LCFI1:
  80              	.loc 1 18 0
  81 000046  20 07 98 	mov w0,[w14+4]
  19:lib/can-ids/Devices/AHRS_CAN.c **** 
  20:lib/can-ids/Devices/AHRS_CAN.c ****     int16_t gyro_z = 0;
  21:lib/can-ids/Devices/AHRS_CAN.c ****     int16_t accel_x = 0;
  22:lib/can-ids/Devices/AHRS_CAN.c **** 
  23:lib/can-ids/Devices/AHRS_CAN.c ****     gyro_z = (int16_t) data[0];
  82              	.loc 1 23 0
  83 000048  AE 01 90 	mov [w14+4],w3
MPLAB XC16 ASSEMBLY Listing:   			page 3


  84              	.loc 1 18 0
  85 00004a  31 07 98 	mov w1,[w14+6]
  86 00004c  42 07 98 	mov w2,[w14+8]
  24:lib/can-ids/Devices/AHRS_CAN.c ****     gyro->time_z  = data[1];
  87              	.loc 1 24 0
  88 00004e  2E 00 90 	mov [w14+4],w0
  25:lib/can-ids/Devices/AHRS_CAN.c ****     accel_x = (int16_t) data[2];
  89              	.loc 1 25 0
  90 000050  AE 00 90 	mov [w14+4],w1
  91              	.loc 1 24 0
  92 000052  00 80 E8 	inc2 w0,w0
  93              	.loc 1 25 0
  94 000054  E4 80 40 	add w1,#4,w1
  26:lib/can-ids/Devices/AHRS_CAN.c ****     accel->time_x = data[3];
  95              	.loc 1 26 0
  96 000056  2E 02 90 	mov [w14+4],w4
  97              	.loc 1 20 0
  98 000058  00 01 EB 	clr w2
  99              	.loc 1 26 0
 100 00005a  66 02 42 	add w4,#6,w4
 101              	.loc 1 20 0
 102 00005c  02 0F 78 	mov w2,[w14]
 103              	.loc 1 21 0
 104 00005e  00 01 EB 	clr w2
 105 000060  12 07 98 	mov w2,[w14+2]
 106              	.loc 1 23 0
 107 000062  13 01 78 	mov [w3],w2
 108 000064  02 0F 78 	mov w2,[w14]
 109              	.loc 1 24 0
 110 000066  3E 01 90 	mov [w14+6],w2
 111 000068  90 01 78 	mov [w0],w3
  27:lib/can-ids/Devices/AHRS_CAN.c **** 
  28:lib/can-ids/Devices/AHRS_CAN.c ****     gyro->z = gyro_z/100;
 112              	.loc 1 28 0
 113 00006a  1E 00 78 	mov [w14],w0
 114              	.loc 1 24 0
 115 00006c  63 01 98 	mov w3,[w2+12]
 116              	.loc 1 26 0
 117 00006e  CE 01 90 	mov [w14+8],w3
 118              	.loc 1 25 0
 119 000070  91 00 78 	mov [w1],w1
 120              	.loc 1 28 0
 121 000072  4F 81 DE 	asr w0,#15,w2
 122              	.loc 1 25 0
 123 000074  11 07 98 	mov w1,[w14+2]
 124              	.loc 1 28 0
 125 000076  B1 47 21 	mov #5243,w1
 126              	.loc 1 26 0
 127 000078  14 02 78 	mov [w4],w4
 128              	.loc 1 28 0
 129 00007a  01 80 B9 	mul.ss w0,w1,w0
 130              	.loc 1 26 0
 131 00007c  E4 01 98 	mov w4,[w3+12]
 132              	.loc 1 28 0
 133 00007e  43 88 DE 	asr w1,#3,w0
 134 000080  02 00 50 	sub w0,w2,w0
 135 000082  CF 80 DE 	asr w0,#15,w1
MPLAB XC16 ASSEMBLY Listing:   			page 4


 136 000084  00 00 07 	rcall ___floatsisf
  29:lib/can-ids/Devices/AHRS_CAN.c ****     accel->x = accel_x/10000;
 137              	.loc 1 29 0
 138 000086  72 A3 21 	mov #6711,w2
 139 000088  1E 02 90 	mov [w14+2],w4
 140              	.loc 1 28 0
 141 00008a  BE 02 90 	mov [w14+6],w5
 142              	.loc 1 29 0
 143 00008c  02 A1 B9 	mul.ss w4,w2,w2
 144 00008e  4F A2 DE 	asr w4,#15,w4
 145 000090  4A 99 DE 	asr w3,#10,w2
 146              	.loc 1 28 0
 147 000092  C0 02 98 	mov w0,[w5+8]
 148 000094  D1 02 98 	mov w1,[w5+10]
 149              	.loc 1 29 0
 150 000096  04 00 51 	sub w2,w4,w0
 151 000098  CF 80 DE 	asr w0,#15,w1
 152 00009a  00 00 07 	rcall ___floatsisf
 153 00009c  4E 01 90 	mov [w14+8],w2
 154 00009e  00 89 BE 	mov.d w0,[w2]
  30:lib/can-ids/Devices/AHRS_CAN.c **** 
  31:lib/can-ids/Devices/AHRS_CAN.c ****     return;
  32:lib/can-ids/Devices/AHRS_CAN.c **** }
 155              	.loc 1 32 0
 156 0000a0  8E 07 78 	mov w14,w15
 157 0000a2  4F 07 78 	mov [--w15],w14
 158 0000a4  00 40 A9 	bclr CORCON,#2
 159 0000a6  00 00 06 	return 
 160              	.set ___PA___,0
 161              	.LFE1:
 162              	.size _parse_can_message_gyro_z_accel_x,.-_parse_can_message_gyro_z_accel_x
 163              	.align 2
 164              	.global _parse_can_message_gyro_x_y
 165              	.type _parse_can_message_gyro_x_y,@function
 166              	_parse_can_message_gyro_x_y:
 167              	.LFB2:
  33:lib/can-ids/Devices/AHRS_CAN.c **** 
  34:lib/can-ids/Devices/AHRS_CAN.c **** void parse_can_message_gyro_x_y(uint16_t data[4], AHRS_MSG_GYRO *gyro){
 168              	.loc 1 34 0
 169              	.set ___PA___,1
 170 0000a8  0A 00 FA 	lnk #10
 171              	.LCFI2:
 172              	.loc 1 34 0
 173 0000aa  20 07 98 	mov w0,[w14+4]
  35:lib/can-ids/Devices/AHRS_CAN.c **** 
  36:lib/can-ids/Devices/AHRS_CAN.c ****     int16_t gyro_x = 0;
  37:lib/can-ids/Devices/AHRS_CAN.c ****     int16_t gyro_y = 0;
  38:lib/can-ids/Devices/AHRS_CAN.c **** 
  39:lib/can-ids/Devices/AHRS_CAN.c ****     gyro_x = (int16_t) data[0];
 174              	.loc 1 39 0
 175 0000ac  AE 01 90 	mov [w14+4],w3
 176              	.loc 1 34 0
 177 0000ae  31 07 98 	mov w1,[w14+6]
  40:lib/can-ids/Devices/AHRS_CAN.c ****     gyro_y = (int16_t) data[1];
 178              	.loc 1 40 0
 179 0000b0  2E 00 90 	mov [w14+4],w0
  41:lib/can-ids/Devices/AHRS_CAN.c ****     gyro->time_x_y = data[2];
MPLAB XC16 ASSEMBLY Listing:   			page 5


 180              	.loc 1 41 0
 181 0000b2  AE 00 90 	mov [w14+4],w1
 182              	.loc 1 40 0
 183 0000b4  00 80 E8 	inc2 w0,w0
 184              	.loc 1 41 0
 185 0000b6  64 81 40 	add w1,#4,w2
 186              	.loc 1 36 0
 187 0000b8  00 02 EB 	clr w4
 188              	.loc 1 37 0
 189 0000ba  80 00 EB 	clr w1
 190              	.loc 1 36 0
 191 0000bc  04 0F 78 	mov w4,[w14]
 192              	.loc 1 37 0
 193 0000be  11 07 98 	mov w1,[w14+2]
 194              	.loc 1 39 0
 195 0000c0  93 00 78 	mov [w3],w1
 196 0000c2  01 0F 78 	mov w1,[w14]
 197              	.loc 1 41 0
 198 0000c4  BE 00 90 	mov [w14+6],w1
 199 0000c6  41 07 98 	mov w1,[w14+8]
 200              	.loc 1 40 0
 201 0000c8  10 00 78 	mov [w0],w0
  42:lib/can-ids/Devices/AHRS_CAN.c **** 
  43:lib/can-ids/Devices/AHRS_CAN.c ****     gyro->x = gyro_x/1000;
 202              	.loc 1 43 0
 203 0000ca  9E 01 78 	mov [w14],w3
 204              	.loc 1 40 0
 205 0000cc  10 07 98 	mov w0,[w14+2]
 206              	.loc 1 43 0
 207 0000ce  80 3E 20 	mov #1000,w0
 208              	.loc 1 41 0
 209 0000d0  12 01 78 	mov [w2],w2
 210              	.loc 1 43 0
 211 0000d2  00 02 78 	mov w0,w4
 212 0000d4  11 00 09 	repeat #__TARGET_DIVIDE_CYCLES
 213 0000d6  84 01 D8 	div.sw w3,w4
 214              	.loc 1 41 0
 215 0000d8  CE 00 90 	mov [w14+8],w1
 216 0000da  F2 00 98 	mov w2,[w1+14]
 217              	.loc 1 43 0
 218 0000dc  CF 80 DE 	asr w0,#15,w1
 219 0000de  00 00 07 	rcall ___floatsisf
  44:lib/can-ids/Devices/AHRS_CAN.c ****     gyro->y = gyro_y/1000;
 220              	.loc 1 44 0
 221 0000e0  82 3E 20 	mov #1000,w2
 222              	.loc 1 43 0
 223 0000e2  3E 02 90 	mov [w14+6],w4
 224              	.loc 1 44 0
 225 0000e4  9E 01 90 	mov [w14+2],w3
 226              	.loc 1 43 0
 227 0000e6  00 8A BE 	mov.d w0,[w4]
 228              	.loc 1 44 0
 229 0000e8  11 00 09 	repeat #__TARGET_DIVIDE_CYCLES
 230 0000ea  82 01 D8 	div.sw w3,w2
 231 0000ec  CF 80 DE 	asr w0,#15,w1
 232 0000ee  00 00 07 	rcall ___floatsisf
 233 0000f0  3E 01 90 	mov [w14+6],w2
MPLAB XC16 ASSEMBLY Listing:   			page 6


 234 0000f2  20 01 98 	mov w0,[w2+4]
 235 0000f4  31 01 98 	mov w1,[w2+6]
  45:lib/can-ids/Devices/AHRS_CAN.c **** 
  46:lib/can-ids/Devices/AHRS_CAN.c ****     return;
  47:lib/can-ids/Devices/AHRS_CAN.c **** 
  48:lib/can-ids/Devices/AHRS_CAN.c **** }
 236              	.loc 1 48 0
 237 0000f6  8E 07 78 	mov w14,w15
 238 0000f8  4F 07 78 	mov [--w15],w14
 239 0000fa  00 40 A9 	bclr CORCON,#2
 240 0000fc  00 00 06 	return 
 241              	.set ___PA___,0
 242              	.LFE2:
 243              	.size _parse_can_message_gyro_x_y,.-_parse_can_message_gyro_x_y
 244              	.align 2
 245              	.global _parse_can_message_accel_y_z
 246              	.type _parse_can_message_accel_y_z,@function
 247              	_parse_can_message_accel_y_z:
 248              	.LFB3:
  49:lib/can-ids/Devices/AHRS_CAN.c **** 
  50:lib/can-ids/Devices/AHRS_CAN.c **** void parse_can_message_accel_y_z(uint16_t data[4], AHRS_MSG_ACCEL *accel){
 249              	.loc 1 50 0
 250              	.set ___PA___,1
 251 0000fe  0A 00 FA 	lnk #10
 252              	.LCFI3:
 253              	.loc 1 50 0
 254 000100  20 07 98 	mov w0,[w14+4]
  51:lib/can-ids/Devices/AHRS_CAN.c **** 
  52:lib/can-ids/Devices/AHRS_CAN.c ****     int16_t accel_y = 0;
  53:lib/can-ids/Devices/AHRS_CAN.c ****     int16_t accel_z = 0;
  54:lib/can-ids/Devices/AHRS_CAN.c **** 
  55:lib/can-ids/Devices/AHRS_CAN.c ****     accel_y = (int16_t) data[0];
 255              	.loc 1 55 0
 256 000102  AE 01 90 	mov [w14+4],w3
 257              	.loc 1 50 0
 258 000104  31 07 98 	mov w1,[w14+6]
  56:lib/can-ids/Devices/AHRS_CAN.c ****     accel_z = (int16_t) data[1];
 259              	.loc 1 56 0
 260 000106  2E 00 90 	mov [w14+4],w0
  57:lib/can-ids/Devices/AHRS_CAN.c ****     accel->time_y_z = data[2];
 261              	.loc 1 57 0
 262 000108  AE 00 90 	mov [w14+4],w1
 263              	.loc 1 56 0
 264 00010a  00 80 E8 	inc2 w0,w0
 265              	.loc 1 57 0
 266 00010c  64 81 40 	add w1,#4,w2
 267              	.loc 1 52 0
 268 00010e  00 02 EB 	clr w4
 269              	.loc 1 53 0
 270 000110  80 00 EB 	clr w1
 271              	.loc 1 52 0
 272 000112  04 0F 78 	mov w4,[w14]
 273              	.loc 1 53 0
 274 000114  11 07 98 	mov w1,[w14+2]
 275              	.loc 1 55 0
 276 000116  93 00 78 	mov [w3],w1
 277 000118  01 0F 78 	mov w1,[w14]
MPLAB XC16 ASSEMBLY Listing:   			page 7


 278              	.loc 1 57 0
 279 00011a  BE 00 90 	mov [w14+6],w1
 280 00011c  41 07 98 	mov w1,[w14+8]
 281              	.loc 1 56 0
 282 00011e  10 00 78 	mov [w0],w0
  58:lib/can-ids/Devices/AHRS_CAN.c **** 
  59:lib/can-ids/Devices/AHRS_CAN.c ****     accel->y = accel_y/1000;
 283              	.loc 1 59 0
 284 000120  9E 01 78 	mov [w14],w3
 285              	.loc 1 56 0
 286 000122  10 07 98 	mov w0,[w14+2]
 287              	.loc 1 59 0
 288 000124  80 3E 20 	mov #1000,w0
 289              	.loc 1 57 0
 290 000126  12 01 78 	mov [w2],w2
 291              	.loc 1 59 0
 292 000128  00 02 78 	mov w0,w4
 293 00012a  11 00 09 	repeat #__TARGET_DIVIDE_CYCLES
 294 00012c  84 01 D8 	div.sw w3,w4
 295              	.loc 1 57 0
 296 00012e  CE 00 90 	mov [w14+8],w1
 297 000130  F2 00 98 	mov w2,[w1+14]
 298              	.loc 1 59 0
 299 000132  CF 80 DE 	asr w0,#15,w1
 300 000134  00 00 07 	rcall ___floatsisf
  60:lib/can-ids/Devices/AHRS_CAN.c ****     accel->z = accel_z/1000;
 301              	.loc 1 60 0
 302 000136  82 3E 20 	mov #1000,w2
 303              	.loc 1 59 0
 304 000138  3E 02 90 	mov [w14+6],w4
 305              	.loc 1 60 0
 306 00013a  9E 01 90 	mov [w14+2],w3
 307              	.loc 1 59 0
 308 00013c  20 02 98 	mov w0,[w4+4]
 309 00013e  31 02 98 	mov w1,[w4+6]
 310              	.loc 1 60 0
 311 000140  11 00 09 	repeat #__TARGET_DIVIDE_CYCLES
 312 000142  82 01 D8 	div.sw w3,w2
 313 000144  CF 80 DE 	asr w0,#15,w1
 314 000146  00 00 07 	rcall ___floatsisf
 315 000148  3E 01 90 	mov [w14+6],w2
 316 00014a  40 01 98 	mov w0,[w2+8]
 317 00014c  51 01 98 	mov w1,[w2+10]
  61:lib/can-ids/Devices/AHRS_CAN.c **** 
  62:lib/can-ids/Devices/AHRS_CAN.c ****     return;
  63:lib/can-ids/Devices/AHRS_CAN.c **** 
  64:lib/can-ids/Devices/AHRS_CAN.c **** }
 318              	.loc 1 64 0
 319 00014e  8E 07 78 	mov w14,w15
 320 000150  4F 07 78 	mov [--w15],w14
 321 000152  00 40 A9 	bclr CORCON,#2
 322 000154  00 00 06 	return 
 323              	.set ___PA___,0
 324              	.LFE3:
 325              	.size _parse_can_message_accel_y_z,.-_parse_can_message_accel_y_z
 326              	.align 2
 327              	.global _parse_can_message_mag
MPLAB XC16 ASSEMBLY Listing:   			page 8


 328              	.type _parse_can_message_mag,@function
 329              	_parse_can_message_mag:
 330              	.LFB4:
  65:lib/can-ids/Devices/AHRS_CAN.c **** void parse_can_message_mag(uint16_t data[4], AHRS_MSG_MAG *mag){
 331              	.loc 1 65 0
 332              	.set ___PA___,1
 333 000156  0A 00 FA 	lnk #10
 334              	.LCFI4:
 335              	.loc 1 65 0
 336 000158  30 07 98 	mov w0,[w14+6]
  66:lib/can-ids/Devices/AHRS_CAN.c **** 
  67:lib/can-ids/Devices/AHRS_CAN.c ****     int16_t mag_x;
  68:lib/can-ids/Devices/AHRS_CAN.c ****     int16_t mag_y;
  69:lib/can-ids/Devices/AHRS_CAN.c ****     int16_t mag_z;
  70:lib/can-ids/Devices/AHRS_CAN.c **** 
  71:lib/can-ids/Devices/AHRS_CAN.c ****     mag_x = (int16_t) data[0];
 337              	.loc 1 71 0
 338 00015a  BE 01 90 	mov [w14+6],w3
  72:lib/can-ids/Devices/AHRS_CAN.c ****     mag_x = (int16_t) data[1];
 339              	.loc 1 72 0
 340 00015c  3E 00 90 	mov [w14+6],w0
 341 00015e  00 81 E8 	inc2 w0,w2
 342              	.loc 1 65 0
 343 000160  41 07 98 	mov w1,[w14+8]
  73:lib/can-ids/Devices/AHRS_CAN.c ****     mag_z = (int16_t) data[2];
 344              	.loc 1 73 0
 345 000162  3E 00 90 	mov [w14+6],w0
 346              	.loc 1 71 0
 347 000164  93 00 78 	mov [w3],w1
 348              	.loc 1 73 0
 349 000166  64 00 40 	add w0,#4,w0
 350              	.loc 1 71 0
 351 000168  01 0F 78 	mov w1,[w14]
  74:lib/can-ids/Devices/AHRS_CAN.c ****     mag->time = data[3];
 352              	.loc 1 74 0
 353 00016a  BE 00 90 	mov [w14+6],w1
 354              	.loc 1 72 0
 355 00016c  12 01 78 	mov [w2],w2
 356              	.loc 1 74 0
 357 00016e  E6 80 40 	add w1,#6,w1
 358              	.loc 1 72 0
 359 000170  02 0F 78 	mov w2,[w14]
 360              	.loc 1 74 0
 361 000172  CE 01 90 	mov [w14+8],w3
 362              	.loc 1 73 0
 363 000174  10 01 78 	mov [w0],w2
  75:lib/can-ids/Devices/AHRS_CAN.c **** 
  76:lib/can-ids/Devices/AHRS_CAN.c ****     mag->x = mag_x/10;
 364              	.loc 1 76 0
 365 000176  1E 00 78 	mov [w14],w0
 366              	.loc 1 73 0
 367 000178  12 07 98 	mov w2,[w14+2]
 368              	.loc 1 76 0
 369 00017a  4F 81 DE 	asr w0,#15,w2
 370              	.loc 1 74 0
 371 00017c  11 02 78 	mov [w1],w4
 372              	.loc 1 76 0
MPLAB XC16 ASSEMBLY Listing:   			page 9


 373 00017e  71 66 26 	mov #26215,w1
 374              	.loc 1 74 0
 375 000180  E4 01 98 	mov w4,[w3+12]
 376              	.loc 1 76 0
 377 000182  01 80 B9 	mul.ss w0,w1,w0
 378 000184  42 88 DE 	asr w1,#2,w0
 379 000186  02 00 50 	sub w0,w2,w0
 380 000188  CF 80 DE 	asr w0,#15,w1
 381 00018a  00 00 07 	rcall ___floatsisf
  77:lib/can-ids/Devices/AHRS_CAN.c ****     mag->y = mag_y/10;
 382              	.loc 1 77 0
 383 00018c  72 66 26 	mov #26215,w2
 384 00018e  2E 02 90 	mov [w14+4],w4
 385              	.loc 1 76 0
 386 000190  CE 02 90 	mov [w14+8],w5
 387              	.loc 1 77 0
 388 000192  02 A1 B9 	mul.ss w4,w2,w2
 389 000194  4F A2 DE 	asr w4,#15,w4
 390 000196  42 99 DE 	asr w3,#2,w2
 391              	.loc 1 76 0
 392 000198  80 8A BE 	mov.d w0,[w5]
 393              	.loc 1 77 0
 394 00019a  04 00 51 	sub w2,w4,w0
 395 00019c  CF 80 DE 	asr w0,#15,w1
 396 00019e  00 00 07 	rcall ___floatsisf
  78:lib/can-ids/Devices/AHRS_CAN.c ****     mag->z = mag_z/10;
 397              	.loc 1 78 0
 398 0001a0  72 66 26 	mov #26215,w2
 399 0001a2  1E 02 90 	mov [w14+2],w4
 400              	.loc 1 77 0
 401 0001a4  CE 02 90 	mov [w14+8],w5
 402              	.loc 1 78 0
 403 0001a6  02 A1 B9 	mul.ss w4,w2,w2
 404 0001a8  4F A2 DE 	asr w4,#15,w4
 405 0001aa  42 99 DE 	asr w3,#2,w2
 406              	.loc 1 77 0
 407 0001ac  A0 02 98 	mov w0,[w5+4]
 408 0001ae  B1 02 98 	mov w1,[w5+6]
 409              	.loc 1 78 0
 410 0001b0  04 00 51 	sub w2,w4,w0
 411 0001b2  CF 80 DE 	asr w0,#15,w1
 412 0001b4  00 00 07 	rcall ___floatsisf
 413 0001b6  4E 01 90 	mov [w14+8],w2
 414 0001b8  40 01 98 	mov w0,[w2+8]
 415 0001ba  51 01 98 	mov w1,[w2+10]
  79:lib/can-ids/Devices/AHRS_CAN.c **** 
  80:lib/can-ids/Devices/AHRS_CAN.c ****     return;
  81:lib/can-ids/Devices/AHRS_CAN.c **** 
  82:lib/can-ids/Devices/AHRS_CAN.c **** }
 416              	.loc 1 82 0
 417 0001bc  8E 07 78 	mov w14,w15
 418 0001be  4F 07 78 	mov [--w15],w14
 419 0001c0  00 40 A9 	bclr CORCON,#2
 420 0001c2  00 00 06 	return 
 421              	.set ___PA___,0
 422              	.LFE4:
 423              	.size _parse_can_message_mag,.-_parse_can_message_mag
MPLAB XC16 ASSEMBLY Listing:   			page 10


 424              	.align 2
 425              	.global _parse_can_message_quat
 426              	.type _parse_can_message_quat,@function
 427              	_parse_can_message_quat:
 428              	.LFB5:
  83:lib/can-ids/Devices/AHRS_CAN.c **** void parse_can_message_quat(uint16_t data[4], AHRS_MSG_QUAT *quat){
 429              	.loc 1 83 0
 430              	.set ___PA___,1
 431 0001c4  0C 00 FA 	lnk #12
 432              	.LCFI5:
 433              	.loc 1 83 0
 434 0001c6  40 07 98 	mov w0,[w14+8]
  84:lib/can-ids/Devices/AHRS_CAN.c **** 
  85:lib/can-ids/Devices/AHRS_CAN.c ****     int16_t quat_A_B_aux;
  86:lib/can-ids/Devices/AHRS_CAN.c ****     int16_t quat_C_D_aux;
  87:lib/can-ids/Devices/AHRS_CAN.c ****     int8_t quat_A;
  88:lib/can-ids/Devices/AHRS_CAN.c ****     int8_t quat_B;
  89:lib/can-ids/Devices/AHRS_CAN.c ****     int8_t quat_C;
  90:lib/can-ids/Devices/AHRS_CAN.c ****     int8_t quat_D;
  91:lib/can-ids/Devices/AHRS_CAN.c **** 
  92:lib/can-ids/Devices/AHRS_CAN.c ****     quat_A_B_aux = (int16_t) data[0];
 435              	.loc 1 92 0
 436 0001c8  4E 01 90 	mov [w14+8],w2
 437              	.loc 1 83 0
 438 0001ca  51 07 98 	mov w1,[w14+10]
  93:lib/can-ids/Devices/AHRS_CAN.c ****     quat_C_D_aux = (int16_t) data[1];
 439              	.loc 1 93 0
 440 0001cc  4E 00 90 	mov [w14+8],w0
 441              	.loc 1 92 0
 442 0001ce  92 00 78 	mov [w2],w1
 443              	.loc 1 93 0
 444 0001d0  00 80 E8 	inc2 w0,w0
 445              	.loc 1 92 0
 446 0001d2  01 0F 78 	mov w1,[w14]
  94:lib/can-ids/Devices/AHRS_CAN.c ****     quat->time = data[2];
 447              	.loc 1 94 0
 448 0001d4  CE 00 90 	mov [w14+8],w1
 449 0001d6  64 81 40 	add w1,#4,w2
 450              	.loc 1 93 0
 451 0001d8  10 00 78 	mov [w0],w0
 452              	.loc 1 94 0
 453 0001da  DE 00 90 	mov [w14+10],w1
 454              	.loc 1 93 0
 455 0001dc  10 07 98 	mov w0,[w14+2]
  95:lib/can-ids/Devices/AHRS_CAN.c **** 
  96:lib/can-ids/Devices/AHRS_CAN.c ****     quat_A = (quat_A_B_aux >>8) & 0x00FF;
 456              	.loc 1 96 0
 457 0001de  1E 00 78 	mov [w14],w0
 458              	.loc 1 94 0
 459 0001e0  12 01 78 	mov [w2],w2
 460              	.loc 1 96 0
 461 0001e2  C8 01 DE 	lsr w0,#8,w3
  97:lib/can-ids/Devices/AHRS_CAN.c ****     quat_B = quat_A_B_aux & 0x00FF;
 462              	.loc 1 97 0
 463 0001e4  1E 00 78 	mov [w14],w0
 464              	.loc 1 96 0
 465 0001e6  43 47 98 	mov.b w3,[w14+4]
MPLAB XC16 ASSEMBLY Listing:   			page 11


 466              	.loc 1 97 0
 467 0001e8  50 47 98 	mov.b w0,[w14+5]
  98:lib/can-ids/Devices/AHRS_CAN.c ****     quat_C = (quat_C_D_aux >>8) & 0x00FF;
 468              	.loc 1 98 0
 469 0001ea  1E 00 90 	mov [w14+2],w0
  99:lib/can-ids/Devices/AHRS_CAN.c ****     quat_D = quat_C_D_aux & 0x00FF;
 470              	.loc 1 99 0
 471 0001ec  9E 01 90 	mov [w14+2],w3
 472              	.loc 1 98 0
 473 0001ee  48 00 DE 	lsr w0,#8,w0
 474              	.loc 1 99 0
 475 0001f0  73 47 98 	mov.b w3,[w14+7]
 476              	.loc 1 98 0
 477 0001f2  60 47 98 	mov.b w0,[w14+6]
 100:lib/can-ids/Devices/AHRS_CAN.c **** 
 101:lib/can-ids/Devices/AHRS_CAN.c **** //for now stays like this... in the even of the number being too small arrangements will need to be
 102:lib/can-ids/Devices/AHRS_CAN.c ****     quat->A = (float)quat_A;
 478              	.loc 1 102 0
 479 0001f4  4E 40 90 	mov.b [w14+4],w0
 480              	.loc 1 94 0
 481 0001f6  82 08 98 	mov w2,[w1+16]
 482              	.loc 1 102 0
 483 0001f8  00 00 FB 	se w0,w0
 484 0001fa  CF 80 DE 	asr w0,#15,w1
 485 0001fc  00 00 07 	rcall ___floatsisf
 486 0001fe  DE 01 90 	mov [w14+10],w3
 103:lib/can-ids/Devices/AHRS_CAN.c ****     quat->B = (float)quat_B;
 487              	.loc 1 103 0
 488 000200  5E 41 90 	mov.b [w14+5],w2
 489              	.loc 1 102 0
 490 000202  80 89 BE 	mov.d w0,[w3]
 491              	.loc 1 103 0
 492 000204  02 00 FB 	se w2,w0
 493 000206  CF 80 DE 	asr w0,#15,w1
 494 000208  00 00 07 	rcall ___floatsisf
 495 00020a  DE 01 90 	mov [w14+10],w3
 104:lib/can-ids/Devices/AHRS_CAN.c ****     quat->C = (float)quat_C;
 496              	.loc 1 104 0
 497 00020c  6E 41 90 	mov.b [w14+6],w2
 498              	.loc 1 103 0
 499 00020e  A0 01 98 	mov w0,[w3+4]
 500 000210  B1 01 98 	mov w1,[w3+6]
 501              	.loc 1 104 0
 502 000212  02 00 FB 	se w2,w0
 503 000214  CF 80 DE 	asr w0,#15,w1
 504 000216  00 00 07 	rcall ___floatsisf
 505 000218  DE 01 90 	mov [w14+10],w3
 105:lib/can-ids/Devices/AHRS_CAN.c ****     quat->D = (float)quat_D;
 506              	.loc 1 105 0
 507 00021a  7E 41 90 	mov.b [w14+7],w2
 508              	.loc 1 104 0
 509 00021c  C0 01 98 	mov w0,[w3+8]
 510 00021e  D1 01 98 	mov w1,[w3+10]
 511              	.loc 1 105 0
 512 000220  02 00 FB 	se w2,w0
 513 000222  CF 80 DE 	asr w0,#15,w1
 514 000224  00 00 07 	rcall ___floatsisf
MPLAB XC16 ASSEMBLY Listing:   			page 12


 515 000226  5E 01 90 	mov [w14+10],w2
 516 000228  60 01 98 	mov w0,[w2+12]
 517 00022a  71 01 98 	mov w1,[w2+14]
 106:lib/can-ids/Devices/AHRS_CAN.c **** 
 107:lib/can-ids/Devices/AHRS_CAN.c ****     return;
 108:lib/can-ids/Devices/AHRS_CAN.c **** 
 109:lib/can-ids/Devices/AHRS_CAN.c **** }
 518              	.loc 1 109 0
 519 00022c  8E 07 78 	mov w14,w15
 520 00022e  4F 07 78 	mov [--w15],w14
 521 000230  00 40 A9 	bclr CORCON,#2
 522 000232  00 00 06 	return 
 523              	.set ___PA___,0
 524              	.LFE5:
 525              	.size _parse_can_message_quat,.-_parse_can_message_quat
 526              	.align 2
 527              	.global _parse_can_message_euler_angle
 528              	.type _parse_can_message_euler_angle,@function
 529              	_parse_can_message_euler_angle:
 530              	.LFB6:
 110:lib/can-ids/Devices/AHRS_CAN.c **** void parse_can_message_euler_angle(uint16_t data[4], AHRS_MSG_EULER *euler){
 531              	.loc 1 110 0
 532              	.set ___PA___,1
 533 000234  0A 00 FA 	lnk #10
 534              	.LCFI6:
 535              	.loc 1 110 0
 536 000236  30 07 98 	mov w0,[w14+6]
 111:lib/can-ids/Devices/AHRS_CAN.c **** 
 112:lib/can-ids/Devices/AHRS_CAN.c ****     int16_t roll;
 113:lib/can-ids/Devices/AHRS_CAN.c ****     int16_t pitch;
 114:lib/can-ids/Devices/AHRS_CAN.c ****     int16_t yaw;
 115:lib/can-ids/Devices/AHRS_CAN.c **** 
 116:lib/can-ids/Devices/AHRS_CAN.c ****     roll = (int16_t) data[0];
 537              	.loc 1 116 0
 538 000238  3E 01 90 	mov [w14+6],w2
 539              	.loc 1 110 0
 540 00023a  41 07 98 	mov w1,[w14+8]
 117:lib/can-ids/Devices/AHRS_CAN.c ****     pitch = (int16_t) data[1];
 541              	.loc 1 117 0
 542 00023c  3E 00 90 	mov [w14+6],w0
 543              	.loc 1 116 0
 544 00023e  92 00 78 	mov [w2],w1
 545              	.loc 1 117 0
 546 000240  00 80 E8 	inc2 w0,w0
 547              	.loc 1 116 0
 548 000242  01 0F 78 	mov w1,[w14]
 118:lib/can-ids/Devices/AHRS_CAN.c ****     yaw = (int16_t) data [2];
 549              	.loc 1 118 0
 550 000244  BE 00 90 	mov [w14+6],w1
 119:lib/can-ids/Devices/AHRS_CAN.c ****     euler->time_angle = data[3];
 551              	.loc 1 119 0
 552 000246  3E 01 90 	mov [w14+6],w2
 553              	.loc 1 118 0
 554 000248  E4 80 40 	add w1,#4,w1
 555              	.loc 1 119 0
 556 00024a  66 02 41 	add w2,#6,w4
 557              	.loc 1 117 0
MPLAB XC16 ASSEMBLY Listing:   			page 13


 558 00024c  10 01 78 	mov [w0],w2
 120:lib/can-ids/Devices/AHRS_CAN.c **** 
 121:lib/can-ids/Devices/AHRS_CAN.c ****     euler->roll = roll  /100;
 559              	.loc 1 121 0
 560 00024e  1E 00 78 	mov [w14],w0
 561              	.loc 1 117 0
 562 000250  12 07 98 	mov w2,[w14+2]
 563              	.loc 1 119 0
 564 000252  CE 01 90 	mov [w14+8],w3
 565              	.loc 1 118 0
 566 000254  91 00 78 	mov [w1],w1
 567              	.loc 1 121 0
 568 000256  4F 81 DE 	asr w0,#15,w2
 569              	.loc 1 118 0
 570 000258  21 07 98 	mov w1,[w14+4]
 571              	.loc 1 121 0
 572 00025a  B1 47 21 	mov #5243,w1
 573              	.loc 1 119 0
 574 00025c  14 02 78 	mov [w4],w4
 575              	.loc 1 121 0
 576 00025e  01 80 B9 	mul.ss w0,w1,w0
 577              	.loc 1 119 0
 578 000260  C4 09 98 	mov w4,[w3+24]
 579              	.loc 1 121 0
 580 000262  43 88 DE 	asr w1,#3,w0
 581 000264  02 00 50 	sub w0,w2,w0
 582 000266  CF 80 DE 	asr w0,#15,w1
 583 000268  00 00 07 	rcall ___floatsisf
 122:lib/can-ids/Devices/AHRS_CAN.c ****     euler->pitch = pitch /100;
 584              	.loc 1 122 0
 585 00026a  B2 47 21 	mov #5243,w2
 586 00026c  1E 02 90 	mov [w14+2],w4
 587              	.loc 1 121 0
 588 00026e  CE 02 90 	mov [w14+8],w5
 589              	.loc 1 122 0
 590 000270  02 A1 B9 	mul.ss w4,w2,w2
 591 000272  4F A2 DE 	asr w4,#15,w4
 592 000274  43 99 DE 	asr w3,#3,w2
 593              	.loc 1 121 0
 594 000276  80 8A BE 	mov.d w0,[w5]
 595              	.loc 1 122 0
 596 000278  04 00 51 	sub w2,w4,w0
 597 00027a  CF 80 DE 	asr w0,#15,w1
 598 00027c  00 00 07 	rcall ___floatsisf
 123:lib/can-ids/Devices/AHRS_CAN.c ****     euler->yaw = yaw /100;
 599              	.loc 1 123 0
 600 00027e  B2 47 21 	mov #5243,w2
 601 000280  2E 02 90 	mov [w14+4],w4
 602              	.loc 1 122 0
 603 000282  CE 02 90 	mov [w14+8],w5
 604              	.loc 1 123 0
 605 000284  02 A1 B9 	mul.ss w4,w2,w2
 606 000286  4F A2 DE 	asr w4,#15,w4
 607 000288  43 99 DE 	asr w3,#3,w2
 608              	.loc 1 122 0
 609 00028a  A0 02 98 	mov w0,[w5+4]
 610 00028c  B1 02 98 	mov w1,[w5+6]
MPLAB XC16 ASSEMBLY Listing:   			page 14


 611              	.loc 1 123 0
 612 00028e  04 00 51 	sub w2,w4,w0
 613 000290  CF 80 DE 	asr w0,#15,w1
 614 000292  00 00 07 	rcall ___floatsisf
 615 000294  4E 01 90 	mov [w14+8],w2
 616 000296  40 01 98 	mov w0,[w2+8]
 617 000298  51 01 98 	mov w1,[w2+10]
 124:lib/can-ids/Devices/AHRS_CAN.c **** 
 125:lib/can-ids/Devices/AHRS_CAN.c ****     return;
 126:lib/can-ids/Devices/AHRS_CAN.c **** }
 618              	.loc 1 126 0
 619 00029a  8E 07 78 	mov w14,w15
 620 00029c  4F 07 78 	mov [--w15],w14
 621 00029e  00 40 A9 	bclr CORCON,#2
 622 0002a0  00 00 06 	return 
 623              	.set ___PA___,0
 624              	.LFE6:
 625              	.size _parse_can_message_euler_angle,.-_parse_can_message_euler_angle
 626              	.align 2
 627              	.global _parse_can_message_euler_rate
 628              	.type _parse_can_message_euler_rate,@function
 629              	_parse_can_message_euler_rate:
 630              	.LFB7:
 127:lib/can-ids/Devices/AHRS_CAN.c **** 
 128:lib/can-ids/Devices/AHRS_CAN.c **** void parse_can_message_euler_rate(uint16_t data[4], AHRS_MSG_EULER *euler){
 631              	.loc 1 128 0
 632              	.set ___PA___,1
 633 0002a2  0A 00 FA 	lnk #10
 634              	.LCFI7:
 635              	.loc 1 128 0
 636 0002a4  30 07 98 	mov w0,[w14+6]
 129:lib/can-ids/Devices/AHRS_CAN.c **** 
 130:lib/can-ids/Devices/AHRS_CAN.c ****     int16_t roll_rate;
 131:lib/can-ids/Devices/AHRS_CAN.c ****     int16_t pitch_rate;
 132:lib/can-ids/Devices/AHRS_CAN.c ****     int16_t yaw_rate;
 133:lib/can-ids/Devices/AHRS_CAN.c **** 
 134:lib/can-ids/Devices/AHRS_CAN.c ****     roll_rate = (int16_t) data[0];
 637              	.loc 1 134 0
 638 0002a6  3E 01 90 	mov [w14+6],w2
 639              	.loc 1 128 0
 640 0002a8  41 07 98 	mov w1,[w14+8]
 135:lib/can-ids/Devices/AHRS_CAN.c ****     pitch_rate = (int16_t) data[1];
 641              	.loc 1 135 0
 642 0002aa  3E 00 90 	mov [w14+6],w0
 643              	.loc 1 134 0
 644 0002ac  92 00 78 	mov [w2],w1
 645              	.loc 1 135 0
 646 0002ae  00 80 E8 	inc2 w0,w0
 647              	.loc 1 134 0
 648 0002b0  01 0F 78 	mov w1,[w14]
 136:lib/can-ids/Devices/AHRS_CAN.c ****     yaw_rate = (int16_t) data[2];
 649              	.loc 1 136 0
 650 0002b2  BE 00 90 	mov [w14+6],w1
 137:lib/can-ids/Devices/AHRS_CAN.c ****     euler->time_rate = data[3];
 651              	.loc 1 137 0
 652 0002b4  3E 01 90 	mov [w14+6],w2
 653              	.loc 1 136 0
MPLAB XC16 ASSEMBLY Listing:   			page 15


 654 0002b6  E4 80 40 	add w1,#4,w1
 655              	.loc 1 137 0
 656 0002b8  66 02 41 	add w2,#6,w4
 657              	.loc 1 135 0
 658 0002ba  10 01 78 	mov [w0],w2
 138:lib/can-ids/Devices/AHRS_CAN.c **** 
 139:lib/can-ids/Devices/AHRS_CAN.c ****     euler->roll_rate = roll_rate /10000;
 659              	.loc 1 139 0
 660 0002bc  1E 00 78 	mov [w14],w0
 661              	.loc 1 135 0
 662 0002be  12 07 98 	mov w2,[w14+2]
 663              	.loc 1 137 0
 664 0002c0  CE 01 90 	mov [w14+8],w3
 665              	.loc 1 136 0
 666 0002c2  91 00 78 	mov [w1],w1
 667              	.loc 1 139 0
 668 0002c4  4F 81 DE 	asr w0,#15,w2
 669              	.loc 1 136 0
 670 0002c6  21 07 98 	mov w1,[w14+4]
 671              	.loc 1 139 0
 672 0002c8  71 A3 21 	mov #6711,w1
 673              	.loc 1 137 0
 674 0002ca  14 02 78 	mov [w4],w4
 675              	.loc 1 139 0
 676 0002cc  01 80 B9 	mul.ss w0,w1,w0
 677              	.loc 1 137 0
 678 0002ce  D4 09 98 	mov w4,[w3+26]
 679              	.loc 1 139 0
 680 0002d0  4A 88 DE 	asr w1,#10,w0
 681 0002d2  02 00 50 	sub w0,w2,w0
 682 0002d4  CF 80 DE 	asr w0,#15,w1
 683 0002d6  00 00 07 	rcall ___floatsisf
 140:lib/can-ids/Devices/AHRS_CAN.c ****     euler->pitch_rate = pitch_rate /10000;
 684              	.loc 1 140 0
 685 0002d8  72 A3 21 	mov #6711,w2
 686 0002da  1E 02 90 	mov [w14+2],w4
 687              	.loc 1 139 0
 688 0002dc  CE 02 90 	mov [w14+8],w5
 689              	.loc 1 140 0
 690 0002de  02 A1 B9 	mul.ss w4,w2,w2
 691 0002e0  4F A2 DE 	asr w4,#15,w4
 692 0002e2  4A 99 DE 	asr w3,#10,w2
 693              	.loc 1 139 0
 694 0002e4  E0 02 98 	mov w0,[w5+12]
 695 0002e6  F1 02 98 	mov w1,[w5+14]
 696              	.loc 1 140 0
 697 0002e8  04 00 51 	sub w2,w4,w0
 698 0002ea  CF 80 DE 	asr w0,#15,w1
 699 0002ec  00 00 07 	rcall ___floatsisf
 141:lib/can-ids/Devices/AHRS_CAN.c ****     euler->yaw_rate = yaw_rate /100;
 700              	.loc 1 141 0
 701 0002ee  B2 47 21 	mov #5243,w2
 702 0002f0  2E 02 90 	mov [w14+4],w4
 703              	.loc 1 140 0
 704 0002f2  CE 02 90 	mov [w14+8],w5
 705              	.loc 1 141 0
 706 0002f4  02 A1 B9 	mul.ss w4,w2,w2
MPLAB XC16 ASSEMBLY Listing:   			page 16


 707 0002f6  4F A2 DE 	asr w4,#15,w4
 708 0002f8  43 99 DE 	asr w3,#3,w2
 709              	.loc 1 140 0
 710 0002fa  80 0A 98 	mov w0,[w5+16]
 711 0002fc  91 0A 98 	mov w1,[w5+18]
 712              	.loc 1 141 0
 713 0002fe  04 00 51 	sub w2,w4,w0
 714 000300  CF 80 DE 	asr w0,#15,w1
 715 000302  00 00 07 	rcall ___floatsisf
 716 000304  4E 01 90 	mov [w14+8],w2
 717 000306  20 09 98 	mov w0,[w2+20]
 718 000308  31 09 98 	mov w1,[w2+22]
 142:lib/can-ids/Devices/AHRS_CAN.c **** 
 143:lib/can-ids/Devices/AHRS_CAN.c ****     return;
 144:lib/can-ids/Devices/AHRS_CAN.c ****     
 145:lib/can-ids/Devices/AHRS_CAN.c **** }
 719              	.loc 1 145 0
 720 00030a  8E 07 78 	mov w14,w15
 721 00030c  4F 07 78 	mov [--w15],w14
 722 00030e  00 40 A9 	bclr CORCON,#2
 723 000310  00 00 06 	return 
 724              	.set ___PA___,0
 725              	.LFE7:
 726              	.size _parse_can_message_euler_rate,.-_parse_can_message_euler_rate
 727              	.align 2
 728              	.global _parse_can_ahrs
 729              	.type _parse_can_ahrs,@function
 730              	_parse_can_ahrs:
 731              	.LFB8:
 146:lib/can-ids/Devices/AHRS_CAN.c **** 
 147:lib/can-ids/Devices/AHRS_CAN.c **** 
 148:lib/can-ids/Devices/AHRS_CAN.c **** void parse_can_ahrs(CANdata message, AHRS_CAN_Data *data){
 732              	.loc 1 148 0
 733              	.set ___PA___,1
 734 000312  0E 00 FA 	lnk #14
 735              	.LCFI8:
 736              	.loc 1 148 0
 737 000314  00 0F 78 	mov w0,[w14]
 738 000316  11 07 98 	mov w1,[w14+2]
 149:lib/can-ids/Devices/AHRS_CAN.c ****     if (message.dev_id != DEVICE_ID_AHRS) {
 739              	.loc 1 149 0
 740 000318  1E 00 78 	mov [w14],w0
 741              	.loc 1 148 0
 742 00031a  22 07 98 	mov w2,[w14+4]
 743 00031c  33 07 98 	mov w3,[w14+6]
 744 00031e  44 07 98 	mov w4,[w14+8]
 745 000320  55 07 98 	mov w5,[w14+10]
 746 000322  66 07 98 	mov w6,[w14+12]
 747              	.loc 1 149 0
 748 000324  7F 00 60 	and w0,#31,w0
 749 000326  F4 0F 50 	sub w0,#20,[w15]
 750              	.set ___BP___,0
 751 000328  00 00 3A 	bra nz,.L21
 752              	.L10:
 150:lib/can-ids/Devices/AHRS_CAN.c **** 		/*FIXME: send info for error logging*/
 151:lib/can-ids/Devices/AHRS_CAN.c ****         return;
 152:lib/can-ids/Devices/AHRS_CAN.c **** 	}  
MPLAB XC16 ASSEMBLY Listing:   			page 17


 153:lib/can-ids/Devices/AHRS_CAN.c **** 
 154:lib/can-ids/Devices/AHRS_CAN.c ****     switch (message.msg_id){
 753              	.loc 1 154 0
 754 00032a  1E 00 78 	mov [w14],w0
 755 00032c  31 02 20 	mov #35,w1
 756 00032e  45 00 DE 	lsr w0,#5,w0
 757 000330  F0 43 B2 	and.b #63,w0
 758 000332  00 80 FB 	ze w0,w0
 759 000334  81 0F 50 	sub w0,w1,[w15]
 760              	.set ___BP___,0
 761 000336  00 00 32 	bra z,.L15
 762 000338  31 02 20 	mov #35,w1
 763 00033a  81 0F 50 	sub w0,w1,[w15]
 764              	.set ___BP___,0
 765 00033c  00 00 3C 	bra gt,.L20
 766 00033e  11 02 20 	mov #33,w1
 767 000340  81 0F 50 	sub w0,w1,[w15]
 768              	.set ___BP___,0
 769 000342  00 00 32 	bra z,.L13
 770 000344  11 02 20 	mov #33,w1
 771 000346  81 0F 50 	sub w0,w1,[w15]
 772              	.set ___BP___,0
 773 000348  00 00 3C 	bra gt,.L14
 774 00034a  01 02 20 	mov #32,w1
 775 00034c  81 0F 50 	sub w0,w1,[w15]
 776              	.set ___BP___,0
 777 00034e  00 00 32 	bra z,.L12
 778 000350  00 00 37 	bra .L9
 779              	.L20:
 780 000352  51 02 20 	mov #37,w1
 781 000354  81 0F 50 	sub w0,w1,[w15]
 782              	.set ___BP___,0
 783 000356  00 00 32 	bra z,.L17
 784 000358  51 02 20 	mov #37,w1
 785 00035a  81 0F 50 	sub w0,w1,[w15]
 786              	.set ___BP___,0
 787 00035c  00 00 35 	bra lt,.L16
 788 00035e  61 02 20 	mov #38,w1
 789 000360  81 0F 50 	sub w0,w1,[w15]
 790              	.set ___BP___,0
 791 000362  00 00 32 	bra z,.L18
 792 000364  71 02 20 	mov #39,w1
 793 000366  81 0F 50 	sub w0,w1,[w15]
 794              	.set ___BP___,0
 795 000368  00 00 32 	bra z,.L19
 796 00036a  00 00 37 	bra .L9
 797              	.L12:
 155:lib/can-ids/Devices/AHRS_CAN.c **** 		case MSG_ID_AHRS_HEALTH_TEMPERATURE:
 156:lib/can-ids/Devices/AHRS_CAN.c ****             parse_can_message_health_temperature(message.data,&(data->health_temperature));
 798              	.loc 1 156 0
 799 00036c  EE 00 90 	mov [w14+12],w1
 800 00036e  64 00 47 	add w14,#4,w0
 801 000370  00 00 07 	rcall _parse_can_message_health_temperature
 157:lib/can-ids/Devices/AHRS_CAN.c ****             break;
 802              	.loc 1 157 0
 803 000372  00 00 37 	bra .L9
 804              	.L13:
MPLAB XC16 ASSEMBLY Listing:   			page 18


 158:lib/can-ids/Devices/AHRS_CAN.c ****         case MSG_ID_AHRS_GYRO_Z_ACCEL_X:
 159:lib/can-ids/Devices/AHRS_CAN.c ****             parse_can_message_gyro_z_accel_x(message.data, &(data->gyro), &(data->accel));
 805              	.loc 1 159 0
 806 000374  EE 00 90 	mov [w14+12],w1
 807 000376  6E 00 90 	mov [w14+12],w0
 808 000378  78 81 40 	add w1,#24,w2
 809 00037a  E8 00 40 	add w0,#8,w1
 810 00037c  64 00 47 	add w14,#4,w0
 811 00037e  00 00 07 	rcall _parse_can_message_gyro_z_accel_x
 160:lib/can-ids/Devices/AHRS_CAN.c ****             break;
 812              	.loc 1 160 0
 813 000380  00 00 37 	bra .L9
 814              	.L14:
 161:lib/can-ids/Devices/AHRS_CAN.c ****         case MSG_ID_AHRS_GYRO_X_Y:
 162:lib/can-ids/Devices/AHRS_CAN.c ****             parse_can_message_gyro_x_y(message.data, &(data->gyro));
 815              	.loc 1 162 0
 816 000382  EE 00 90 	mov [w14+12],w1
 817 000384  64 00 47 	add w14,#4,w0
 818 000386  E8 80 40 	add w1,#8,w1
 819 000388  00 00 07 	rcall _parse_can_message_gyro_x_y
 820              	.L15:
 163:lib/can-ids/Devices/AHRS_CAN.c ****         case MSG_ID_AHRS_ACCEL_Y_Z:
 164:lib/can-ids/Devices/AHRS_CAN.c ****             parse_can_message_accel_y_z(message.data, &(data->accel));
 821              	.loc 1 164 0
 822 00038a  EE 00 90 	mov [w14+12],w1
 823 00038c  64 00 47 	add w14,#4,w0
 824 00038e  F8 80 40 	add w1,#24,w1
 825 000390  00 00 07 	rcall _parse_can_message_accel_y_z
 165:lib/can-ids/Devices/AHRS_CAN.c ****             break;
 826              	.loc 1 165 0
 827 000392  00 00 37 	bra .L9
 828              	.L16:
 166:lib/can-ids/Devices/AHRS_CAN.c ****         case MSG_ID_AHRS_MAG:
 167:lib/can-ids/Devices/AHRS_CAN.c ****             parse_can_message_mag(message.data, &(data->mag));
 829              	.loc 1 167 0
 830 000394  EE 00 90 	mov [w14+12],w1
 831 000396  64 00 47 	add w14,#4,w0
 832 000398  81 02 B0 	add #40,w1
 833 00039a  00 00 07 	rcall _parse_can_message_mag
 168:lib/can-ids/Devices/AHRS_CAN.c ****             break;
 834              	.loc 1 168 0
 835 00039c  00 00 37 	bra .L9
 836              	.L17:
 169:lib/can-ids/Devices/AHRS_CAN.c ****         case MSG_ID_AHRS_QUAT: 
 170:lib/can-ids/Devices/AHRS_CAN.c ****             parse_can_message_quat(message.data, &(data->quat));
 837              	.loc 1 170 0
 838 00039e  EE 00 90 	mov [w14+12],w1
 839 0003a0  64 00 47 	add w14,#4,w0
 840 0003a2  61 03 B0 	add #54,w1
 841 0003a4  00 00 07 	rcall _parse_can_message_quat
 171:lib/can-ids/Devices/AHRS_CAN.c ****             break;
 842              	.loc 1 171 0
 843 0003a6  00 00 37 	bra .L9
 844              	.L18:
 172:lib/can-ids/Devices/AHRS_CAN.c ****         case MSG_ID_AHRS_EULER_ANGLE:
 173:lib/can-ids/Devices/AHRS_CAN.c ****             parse_can_message_euler_angle(message.data, &(data->euler));
 845              	.loc 1 173 0
MPLAB XC16 ASSEMBLY Listing:   			page 19


 846 0003a8  EE 00 90 	mov [w14+12],w1
 847 0003aa  64 00 47 	add w14,#4,w0
 848 0003ac  81 04 B0 	add #72,w1
 849 0003ae  00 00 07 	rcall _parse_can_message_euler_angle
 174:lib/can-ids/Devices/AHRS_CAN.c ****             break;
 850              	.loc 1 174 0
 851 0003b0  00 00 37 	bra .L9
 852              	.L19:
 175:lib/can-ids/Devices/AHRS_CAN.c ****         case MSG_ID_AHRS_EULER_RATE:
 176:lib/can-ids/Devices/AHRS_CAN.c ****             parse_can_message_euler_rate(message.data, &(data->euler));
 853              	.loc 1 176 0
 854 0003b2  EE 00 90 	mov [w14+12],w1
 855 0003b4  64 00 47 	add w14,#4,w0
 856 0003b6  81 04 B0 	add #72,w1
 857 0003b8  00 00 07 	rcall _parse_can_message_euler_rate
 858 0003ba  00 00 37 	bra .L9
 859              	.L21:
 860              	.L9:
 177:lib/can-ids/Devices/AHRS_CAN.c ****             break;
 178:lib/can-ids/Devices/AHRS_CAN.c **** 	}  
 179:lib/can-ids/Devices/AHRS_CAN.c **** }...
 861              	.loc 1 179 0
 862 0003bc  8E 07 78 	mov w14,w15
 863 0003be  4F 07 78 	mov [--w15],w14
 864 0003c0  00 40 A9 	bclr CORCON,#2
 865 0003c2  00 00 06 	return 
 866              	.set ___PA___,0
 867              	.LFE8:
 868              	.size _parse_can_ahrs,.-_parse_can_ahrs
 869              	.section .debug_frame,info
 870                 	.Lframe0:
 871 0000 10 00 00 00 	.4byte .LECIE0-.LSCIE0
 872                 	.LSCIE0:
 873 0004 FF FF FF FF 	.4byte 0xffffffff
 874 0008 01          	.byte 0x1
 875 0009 00          	.byte 0
 876 000a 01          	.uleb128 0x1
 877 000b 02          	.sleb128 2
 878 000c 25          	.byte 0x25
 879 000d 12          	.byte 0x12
 880 000e 0F          	.uleb128 0xf
 881 000f 7E          	.sleb128 -2
 882 0010 09          	.byte 0x9
 883 0011 25          	.uleb128 0x25
 884 0012 0F          	.uleb128 0xf
 885 0013 00          	.align 4
 886                 	.LECIE0:
 887                 	.LSFDE0:
 888 0014 18 00 00 00 	.4byte .LEFDE0-.LASFDE0
 889                 	.LASFDE0:
 890 0018 00 00 00 00 	.4byte .Lframe0
 891 001c 00 00 00 00 	.4byte .LFB0
 892 0020 44 00 00 00 	.4byte .LFE0-.LFB0
 893 0024 04          	.byte 0x4
 894 0025 02 00 00 00 	.4byte .LCFI0-.LFB0
 895 0029 13          	.byte 0x13
 896 002a 7D          	.sleb128 -3
MPLAB XC16 ASSEMBLY Listing:   			page 20


 897 002b 0D          	.byte 0xd
 898 002c 0E          	.uleb128 0xe
 899 002d 8E          	.byte 0x8e
 900 002e 02          	.uleb128 0x2
 901 002f 00          	.align 4
 902                 	.LEFDE0:
 903                 	.LSFDE2:
 904 0030 18 00 00 00 	.4byte .LEFDE2-.LASFDE2
 905                 	.LASFDE2:
 906 0034 00 00 00 00 	.4byte .Lframe0
 907 0038 00 00 00 00 	.4byte .LFB1
 908 003c 64 00 00 00 	.4byte .LFE1-.LFB1
 909 0040 04          	.byte 0x4
 910 0041 02 00 00 00 	.4byte .LCFI1-.LFB1
 911 0045 13          	.byte 0x13
 912 0046 7D          	.sleb128 -3
 913 0047 0D          	.byte 0xd
 914 0048 0E          	.uleb128 0xe
 915 0049 8E          	.byte 0x8e
 916 004a 02          	.uleb128 0x2
 917 004b 00          	.align 4
 918                 	.LEFDE2:
 919                 	.LSFDE4:
 920 004c 18 00 00 00 	.4byte .LEFDE4-.LASFDE4
 921                 	.LASFDE4:
 922 0050 00 00 00 00 	.4byte .Lframe0
 923 0054 00 00 00 00 	.4byte .LFB2
 924 0058 56 00 00 00 	.4byte .LFE2-.LFB2
 925 005c 04          	.byte 0x4
 926 005d 02 00 00 00 	.4byte .LCFI2-.LFB2
 927 0061 13          	.byte 0x13
 928 0062 7D          	.sleb128 -3
 929 0063 0D          	.byte 0xd
 930 0064 0E          	.uleb128 0xe
 931 0065 8E          	.byte 0x8e
 932 0066 02          	.uleb128 0x2
 933 0067 00          	.align 4
 934                 	.LEFDE4:
 935                 	.LSFDE6:
 936 0068 18 00 00 00 	.4byte .LEFDE6-.LASFDE6
 937                 	.LASFDE6:
 938 006c 00 00 00 00 	.4byte .Lframe0
 939 0070 00 00 00 00 	.4byte .LFB3
 940 0074 58 00 00 00 	.4byte .LFE3-.LFB3
 941 0078 04          	.byte 0x4
 942 0079 02 00 00 00 	.4byte .LCFI3-.LFB3
 943 007d 13          	.byte 0x13
 944 007e 7D          	.sleb128 -3
 945 007f 0D          	.byte 0xd
 946 0080 0E          	.uleb128 0xe
 947 0081 8E          	.byte 0x8e
 948 0082 02          	.uleb128 0x2
 949 0083 00          	.align 4
 950                 	.LEFDE6:
 951                 	.LSFDE8:
 952 0084 18 00 00 00 	.4byte .LEFDE8-.LASFDE8
 953                 	.LASFDE8:
MPLAB XC16 ASSEMBLY Listing:   			page 21


 954 0088 00 00 00 00 	.4byte .Lframe0
 955 008c 00 00 00 00 	.4byte .LFB4
 956 0090 6E 00 00 00 	.4byte .LFE4-.LFB4
 957 0094 04          	.byte 0x4
 958 0095 02 00 00 00 	.4byte .LCFI4-.LFB4
 959 0099 13          	.byte 0x13
 960 009a 7D          	.sleb128 -3
 961 009b 0D          	.byte 0xd
 962 009c 0E          	.uleb128 0xe
 963 009d 8E          	.byte 0x8e
 964 009e 02          	.uleb128 0x2
 965 009f 00          	.align 4
 966                 	.LEFDE8:
 967                 	.LSFDE10:
 968 00a0 18 00 00 00 	.4byte .LEFDE10-.LASFDE10
 969                 	.LASFDE10:
 970 00a4 00 00 00 00 	.4byte .Lframe0
 971 00a8 00 00 00 00 	.4byte .LFB5
 972 00ac 70 00 00 00 	.4byte .LFE5-.LFB5
 973 00b0 04          	.byte 0x4
 974 00b1 02 00 00 00 	.4byte .LCFI5-.LFB5
 975 00b5 13          	.byte 0x13
 976 00b6 7D          	.sleb128 -3
 977 00b7 0D          	.byte 0xd
 978 00b8 0E          	.uleb128 0xe
 979 00b9 8E          	.byte 0x8e
 980 00ba 02          	.uleb128 0x2
 981 00bb 00          	.align 4
 982                 	.LEFDE10:
 983                 	.LSFDE12:
 984 00bc 18 00 00 00 	.4byte .LEFDE12-.LASFDE12
 985                 	.LASFDE12:
 986 00c0 00 00 00 00 	.4byte .Lframe0
 987 00c4 00 00 00 00 	.4byte .LFB6
 988 00c8 6E 00 00 00 	.4byte .LFE6-.LFB6
 989 00cc 04          	.byte 0x4
 990 00cd 02 00 00 00 	.4byte .LCFI6-.LFB6
 991 00d1 13          	.byte 0x13
 992 00d2 7D          	.sleb128 -3
 993 00d3 0D          	.byte 0xd
 994 00d4 0E          	.uleb128 0xe
 995 00d5 8E          	.byte 0x8e
 996 00d6 02          	.uleb128 0x2
 997 00d7 00          	.align 4
 998                 	.LEFDE12:
 999                 	.LSFDE14:
 1000 00d8 18 00 00 00 	.4byte .LEFDE14-.LASFDE14
 1001                 	.LASFDE14:
 1002 00dc 00 00 00 00 	.4byte .Lframe0
 1003 00e0 00 00 00 00 	.4byte .LFB7
 1004 00e4 70 00 00 00 	.4byte .LFE7-.LFB7
 1005 00e8 04          	.byte 0x4
 1006 00e9 02 00 00 00 	.4byte .LCFI7-.LFB7
 1007 00ed 13          	.byte 0x13
 1008 00ee 7D          	.sleb128 -3
 1009 00ef 0D          	.byte 0xd
 1010 00f0 0E          	.uleb128 0xe
MPLAB XC16 ASSEMBLY Listing:   			page 22


 1011 00f1 8E          	.byte 0x8e
 1012 00f2 02          	.uleb128 0x2
 1013 00f3 00          	.align 4
 1014                 	.LEFDE14:
 1015                 	.LSFDE16:
 1016 00f4 18 00 00 00 	.4byte .LEFDE16-.LASFDE16
 1017                 	.LASFDE16:
 1018 00f8 00 00 00 00 	.4byte .Lframe0
 1019 00fc 00 00 00 00 	.4byte .LFB8
 1020 0100 B2 00 00 00 	.4byte .LFE8-.LFB8
 1021 0104 04          	.byte 0x4
 1022 0105 02 00 00 00 	.4byte .LCFI8-.LFB8
 1023 0109 13          	.byte 0x13
 1024 010a 7D          	.sleb128 -3
 1025 010b 0D          	.byte 0xd
 1026 010c 0E          	.uleb128 0xe
 1027 010d 8E          	.byte 0x8e
 1028 010e 02          	.uleb128 0x2
 1029 010f 00          	.align 4
 1030                 	.LEFDE16:
 1031                 	.section .text,code
 1032              	.Letext0:
 1033              	.file 2 "/opt/microchip/xc16/v1.36/bin/bin/../../include/lega-c/stdint.h"
 1034              	.file 3 "lib/can-ids/CAN_IDs.h"
 1035              	.file 4 "lib/can-ids/Devices/AHRS_CAN.h"
 1036              	.section .debug_info,info
 1037 0000 EC 09 00 00 	.4byte 0x9ec
 1038 0004 02 00       	.2byte 0x2
 1039 0006 00 00 00 00 	.4byte .Ldebug_abbrev0
 1040 000a 04          	.byte 0x4
 1041 000b 01          	.uleb128 0x1
 1042 000c 47 4E 55 20 	.asciz "GNU C 4.5.1 (XC16, Microchip v1.36) (A) Build date: Dec  5 2018"
 1042      43 20 34 2E 
 1042      35 2E 31 20 
 1042      28 58 43 31 
 1042      36 2C 20 4D 
 1042      69 63 72 6F 
 1042      63 68 69 70 
 1042      20 76 31 2E 
 1042      33 36 29 20 
 1043 004c 01          	.byte 0x1
 1044 004d 6C 69 62 2F 	.asciz "lib/can-ids/Devices/AHRS_CAN.c"
 1044      63 61 6E 2D 
 1044      69 64 73 2F 
 1044      44 65 76 69 
 1044      63 65 73 2F 
 1044      41 48 52 53 
 1044      5F 43 41 4E 
 1044      2E 63 00 
 1045 006c 2F 68 6F 6D 	.asciz "/home/user/Documents/FST/Programming/project_sub-zero"
 1045      65 2F 75 73 
 1045      65 72 2F 44 
 1045      6F 63 75 6D 
 1045      65 6E 74 73 
 1045      2F 46 53 54 
 1045      2F 50 72 6F 
 1045      67 72 61 6D 
MPLAB XC16 ASSEMBLY Listing:   			page 23


 1045      6D 69 6E 67 
 1046 00a2 00 00 00 00 	.4byte .Ltext0
 1047 00a6 00 00 00 00 	.4byte .Letext0
 1048 00aa 00 00 00 00 	.4byte .Ldebug_line0
 1049 00ae 02          	.uleb128 0x2
 1050 00af 69 6E 74 38 	.asciz "int8_t"
 1050      5F 74 00 
 1051 00b6 02          	.byte 0x2
 1052 00b7 0D          	.byte 0xd
 1053 00b8 BC 00 00 00 	.4byte 0xbc
 1054 00bc 03          	.uleb128 0x3
 1055 00bd 01          	.byte 0x1
 1056 00be 06          	.byte 0x6
 1057 00bf 73 69 67 6E 	.asciz "signed char"
 1057      65 64 20 63 
 1057      68 61 72 00 
 1058 00cb 02          	.uleb128 0x2
 1059 00cc 69 6E 74 31 	.asciz "int16_t"
 1059      36 5F 74 00 
 1060 00d4 02          	.byte 0x2
 1061 00d5 14          	.byte 0x14
 1062 00d6 DA 00 00 00 	.4byte 0xda
 1063 00da 03          	.uleb128 0x3
 1064 00db 02          	.byte 0x2
 1065 00dc 05          	.byte 0x5
 1066 00dd 69 6E 74 00 	.asciz "int"
 1067 00e1 03          	.uleb128 0x3
 1068 00e2 04          	.byte 0x4
 1069 00e3 05          	.byte 0x5
 1070 00e4 6C 6F 6E 67 	.asciz "long int"
 1070      20 69 6E 74 
 1070      00 
 1071 00ed 03          	.uleb128 0x3
 1072 00ee 08          	.byte 0x8
 1073 00ef 05          	.byte 0x5
 1074 00f0 6C 6F 6E 67 	.asciz "long long int"
 1074      20 6C 6F 6E 
 1074      67 20 69 6E 
 1074      74 00 
 1075 00fe 02          	.uleb128 0x2
 1076 00ff 75 69 6E 74 	.asciz "uint8_t"
 1076      38 5F 74 00 
 1077 0107 02          	.byte 0x2
 1078 0108 2B          	.byte 0x2b
 1079 0109 0D 01 00 00 	.4byte 0x10d
 1080 010d 03          	.uleb128 0x3
 1081 010e 01          	.byte 0x1
 1082 010f 08          	.byte 0x8
 1083 0110 75 6E 73 69 	.asciz "unsigned char"
 1083      67 6E 65 64 
 1083      20 63 68 61 
 1083      72 00 
 1084 011e 02          	.uleb128 0x2
 1085 011f 75 69 6E 74 	.asciz "uint16_t"
 1085      31 36 5F 74 
 1085      00 
 1086 0128 02          	.byte 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 24


 1087 0129 31          	.byte 0x31
 1088 012a 2E 01 00 00 	.4byte 0x12e
 1089 012e 03          	.uleb128 0x3
 1090 012f 02          	.byte 0x2
 1091 0130 07          	.byte 0x7
 1092 0131 75 6E 73 69 	.asciz "unsigned int"
 1092      67 6E 65 64 
 1092      20 69 6E 74 
 1092      00 
 1093 013e 03          	.uleb128 0x3
 1094 013f 04          	.byte 0x4
 1095 0140 07          	.byte 0x7
 1096 0141 6C 6F 6E 67 	.asciz "long unsigned int"
 1096      20 75 6E 73 
 1096      69 67 6E 65 
 1096      64 20 69 6E 
 1096      74 00 
 1097 0153 03          	.uleb128 0x3
 1098 0154 08          	.byte 0x8
 1099 0155 07          	.byte 0x7
 1100 0156 6C 6F 6E 67 	.asciz "long long unsigned int"
 1100      20 6C 6F 6E 
 1100      67 20 75 6E 
 1100      73 69 67 6E 
 1100      65 64 20 69 
 1100      6E 74 00 
 1101 016d 04          	.uleb128 0x4
 1102 016e 02          	.byte 0x2
 1103 016f 03          	.byte 0x3
 1104 0170 12          	.byte 0x12
 1105 0171 9E 01 00 00 	.4byte 0x19e
 1106 0175 05          	.uleb128 0x5
 1107 0176 64 65 76 5F 	.asciz "dev_id"
 1107      69 64 00 
 1108 017d 03          	.byte 0x3
 1109 017e 13          	.byte 0x13
 1110 017f 1E 01 00 00 	.4byte 0x11e
 1111 0183 02          	.byte 0x2
 1112 0184 05          	.byte 0x5
 1113 0185 0B          	.byte 0xb
 1114 0186 02          	.byte 0x2
 1115 0187 23          	.byte 0x23
 1116 0188 00          	.uleb128 0x0
 1117 0189 05          	.uleb128 0x5
 1118 018a 6D 73 67 5F 	.asciz "msg_id"
 1118      69 64 00 
 1119 0191 03          	.byte 0x3
 1120 0192 16          	.byte 0x16
 1121 0193 1E 01 00 00 	.4byte 0x11e
 1122 0197 02          	.byte 0x2
 1123 0198 06          	.byte 0x6
 1124 0199 05          	.byte 0x5
 1125 019a 02          	.byte 0x2
 1126 019b 23          	.byte 0x23
 1127 019c 00          	.uleb128 0x0
 1128 019d 00          	.byte 0x0
 1129 019e 06          	.uleb128 0x6
MPLAB XC16 ASSEMBLY Listing:   			page 25


 1130 019f 02          	.byte 0x2
 1131 01a0 03          	.byte 0x3
 1132 01a1 11          	.byte 0x11
 1133 01a2 B7 01 00 00 	.4byte 0x1b7
 1134 01a6 07          	.uleb128 0x7
 1135 01a7 6D 01 00 00 	.4byte 0x16d
 1136 01ab 08          	.uleb128 0x8
 1137 01ac 73 69 64 00 	.asciz "sid"
 1138 01b0 03          	.byte 0x3
 1139 01b1 18          	.byte 0x18
 1140 01b2 1E 01 00 00 	.4byte 0x11e
 1141 01b6 00          	.byte 0x0
 1142 01b7 04          	.uleb128 0x4
 1143 01b8 0C          	.byte 0xc
 1144 01b9 03          	.byte 0x3
 1145 01ba 10          	.byte 0x10
 1146 01bb E7 01 00 00 	.4byte 0x1e7
 1147 01bf 09          	.uleb128 0x9
 1148 01c0 9E 01 00 00 	.4byte 0x19e
 1149 01c4 02          	.byte 0x2
 1150 01c5 23          	.byte 0x23
 1151 01c6 00          	.uleb128 0x0
 1152 01c7 05          	.uleb128 0x5
 1153 01c8 64 6C 63 00 	.asciz "dlc"
 1154 01cc 03          	.byte 0x3
 1155 01cd 1A          	.byte 0x1a
 1156 01ce 1E 01 00 00 	.4byte 0x11e
 1157 01d2 02          	.byte 0x2
 1158 01d3 04          	.byte 0x4
 1159 01d4 0C          	.byte 0xc
 1160 01d5 02          	.byte 0x2
 1161 01d6 23          	.byte 0x23
 1162 01d7 02          	.uleb128 0x2
 1163 01d8 0A          	.uleb128 0xa
 1164 01d9 00 00 00 00 	.4byte .LASF0
 1165 01dd 03          	.byte 0x3
 1166 01de 1B          	.byte 0x1b
 1167 01df E7 01 00 00 	.4byte 0x1e7
 1168 01e3 02          	.byte 0x2
 1169 01e4 23          	.byte 0x23
 1170 01e5 04          	.uleb128 0x4
 1171 01e6 00          	.byte 0x0
 1172 01e7 0B          	.uleb128 0xb
 1173 01e8 1E 01 00 00 	.4byte 0x11e
 1174 01ec F7 01 00 00 	.4byte 0x1f7
 1175 01f0 0C          	.uleb128 0xc
 1176 01f1 2E 01 00 00 	.4byte 0x12e
 1177 01f5 03          	.byte 0x3
 1178 01f6 00          	.byte 0x0
 1179 01f7 02          	.uleb128 0x2
 1180 01f8 43 41 4E 64 	.asciz "CANdata"
 1180      61 74 61 00 
 1181 0200 03          	.byte 0x3
 1182 0201 1C          	.byte 0x1c
 1183 0202 B7 01 00 00 	.4byte 0x1b7
 1184 0206 04          	.uleb128 0x4
 1185 0207 01          	.byte 0x1
MPLAB XC16 ASSEMBLY Listing:   			page 26


 1186 0208 04          	.byte 0x4
 1187 0209 17          	.byte 0x17
 1188 020a 7B 02 00 00 	.4byte 0x27b
 1189 020e 05          	.uleb128 0x5
 1190 020f 4F 56 46 00 	.asciz "OVF"
 1191 0213 04          	.byte 0x4
 1192 0214 18          	.byte 0x18
 1193 0215 FE 00 00 00 	.4byte 0xfe
 1194 0219 01          	.byte 0x1
 1195 021a 01          	.byte 0x1
 1196 021b 07          	.byte 0x7
 1197 021c 02          	.byte 0x2
 1198 021d 23          	.byte 0x23
 1199 021e 00          	.uleb128 0x0
 1200 021f 05          	.uleb128 0x5
 1201 0220 4D 47 5F 4E 	.asciz "MG_N"
 1201      00 
 1202 0225 04          	.byte 0x4
 1203 0226 1A          	.byte 0x1a
 1204 0227 FE 00 00 00 	.4byte 0xfe
 1205 022b 01          	.byte 0x1
 1206 022c 01          	.byte 0x1
 1207 022d 06          	.byte 0x6
 1208 022e 02          	.byte 0x2
 1209 022f 23          	.byte 0x23
 1210 0230 00          	.uleb128 0x0
 1211 0231 05          	.uleb128 0x5
 1212 0232 41 43 43 5F 	.asciz "ACC_N"
 1212      4E 00 
 1213 0238 04          	.byte 0x4
 1214 0239 1E          	.byte 0x1e
 1215 023a FE 00 00 00 	.4byte 0xfe
 1216 023e 01          	.byte 0x1
 1217 023f 01          	.byte 0x1
 1218 0240 05          	.byte 0x5
 1219 0241 02          	.byte 0x2
 1220 0242 23          	.byte 0x23
 1221 0243 00          	.uleb128 0x0
 1222 0244 05          	.uleb128 0x5
 1223 0245 41 43 43 45 	.asciz "ACCEL"
 1223      4C 00 
 1224 024b 04          	.byte 0x4
 1225 024c 21          	.byte 0x21
 1226 024d FE 00 00 00 	.4byte 0xfe
 1227 0251 01          	.byte 0x1
 1228 0252 01          	.byte 0x1
 1229 0253 04          	.byte 0x4
 1230 0254 02          	.byte 0x2
 1231 0255 23          	.byte 0x23
 1232 0256 00          	.uleb128 0x0
 1233 0257 05          	.uleb128 0x5
 1234 0258 47 59 52 4F 	.asciz "GYRO"
 1234      00 
 1235 025d 04          	.byte 0x4
 1236 025e 22          	.byte 0x22
 1237 025f FE 00 00 00 	.4byte 0xfe
 1238 0263 01          	.byte 0x1
MPLAB XC16 ASSEMBLY Listing:   			page 27


 1239 0264 01          	.byte 0x1
 1240 0265 03          	.byte 0x3
 1241 0266 02          	.byte 0x2
 1242 0267 23          	.byte 0x23
 1243 0268 00          	.uleb128 0x0
 1244 0269 05          	.uleb128 0x5
 1245 026a 4D 41 47 00 	.asciz "MAG"
 1246 026e 04          	.byte 0x4
 1247 026f 23          	.byte 0x23
 1248 0270 FE 00 00 00 	.4byte 0xfe
 1249 0274 01          	.byte 0x1
 1250 0275 01          	.byte 0x1
 1251 0276 02          	.byte 0x2
 1252 0277 02          	.byte 0x2
 1253 0278 23          	.byte 0x23
 1254 0279 00          	.uleb128 0x0
 1255 027a 00          	.byte 0x0
 1256 027b 06          	.uleb128 0x6
 1257 027c 01          	.byte 0x1
 1258 027d 04          	.byte 0x4
 1259 027e 16          	.byte 0x16
 1260 027f 97 02 00 00 	.4byte 0x297
 1261 0283 07          	.uleb128 0x7
 1262 0284 06 02 00 00 	.4byte 0x206
 1263 0288 08          	.uleb128 0x8
 1264 0289 68 65 61 6C 	.asciz "health"
 1264      74 68 00 
 1265 0290 04          	.byte 0x4
 1266 0291 25          	.byte 0x25
 1267 0292 FE 00 00 00 	.4byte 0xfe
 1268 0296 00          	.byte 0x0
 1269 0297 04          	.uleb128 0x4
 1270 0298 08          	.byte 0x8
 1271 0299 04          	.byte 0x4
 1272 029a 15          	.byte 0x15
 1273 029b D1 02 00 00 	.4byte 0x2d1
 1274 029f 09          	.uleb128 0x9
 1275 02a0 7B 02 00 00 	.4byte 0x27b
 1276 02a4 02          	.byte 0x2
 1277 02a5 23          	.byte 0x23
 1278 02a6 00          	.uleb128 0x0
 1279 02a7 0D          	.uleb128 0xd
 1280 02a8 74 65 6D 70 	.asciz "temperature_time"
 1280      65 72 61 74 
 1280      75 72 65 5F 
 1280      74 69 6D 65 
 1280      00 
 1281 02b9 04          	.byte 0x4
 1282 02ba 27          	.byte 0x27
 1283 02bb 1E 01 00 00 	.4byte 0x11e
 1284 02bf 02          	.byte 0x2
 1285 02c0 23          	.byte 0x23
 1286 02c1 02          	.uleb128 0x2
 1287 02c2 0A          	.uleb128 0xa
 1288 02c3 00 00 00 00 	.4byte .LASF1
 1289 02c7 04          	.byte 0x4
 1290 02c8 28          	.byte 0x28
MPLAB XC16 ASSEMBLY Listing:   			page 28


 1291 02c9 D1 02 00 00 	.4byte 0x2d1
 1292 02cd 02          	.byte 0x2
 1293 02ce 23          	.byte 0x23
 1294 02cf 04          	.uleb128 0x4
 1295 02d0 00          	.byte 0x0
 1296 02d1 03          	.uleb128 0x3
 1297 02d2 04          	.byte 0x4
 1298 02d3 04          	.byte 0x4
 1299 02d4 66 6C 6F 61 	.asciz "float"
 1299      74 00 
 1300 02da 02          	.uleb128 0x2
 1301 02db 41 48 52 53 	.asciz "AHRS_MSG_HEALTH_TEMPERATURE"
 1301      5F 4D 53 47 
 1301      5F 48 45 41 
 1301      4C 54 48 5F 
 1301      54 45 4D 50 
 1301      45 52 41 54 
 1301      55 52 45 00 
 1302 02f7 04          	.byte 0x4
 1303 02f8 2A          	.byte 0x2a
 1304 02f9 97 02 00 00 	.4byte 0x297
 1305 02fd 04          	.uleb128 0x4
 1306 02fe 10          	.byte 0x10
 1307 02ff 04          	.byte 0x4
 1308 0300 2E          	.byte 0x2e
 1309 0301 4E 03 00 00 	.4byte 0x34e
 1310 0305 0D          	.uleb128 0xd
 1311 0306 78 00       	.asciz "x"
 1312 0308 04          	.byte 0x4
 1313 0309 2F          	.byte 0x2f
 1314 030a D1 02 00 00 	.4byte 0x2d1
 1315 030e 02          	.byte 0x2
 1316 030f 23          	.byte 0x23
 1317 0310 00          	.uleb128 0x0
 1318 0311 0D          	.uleb128 0xd
 1319 0312 79 00       	.asciz "y"
 1320 0314 04          	.byte 0x4
 1321 0315 30          	.byte 0x30
 1322 0316 D1 02 00 00 	.4byte 0x2d1
 1323 031a 02          	.byte 0x2
 1324 031b 23          	.byte 0x23
 1325 031c 04          	.uleb128 0x4
 1326 031d 0D          	.uleb128 0xd
 1327 031e 7A 00       	.asciz "z"
 1328 0320 04          	.byte 0x4
 1329 0321 31          	.byte 0x31
 1330 0322 D1 02 00 00 	.4byte 0x2d1
 1331 0326 02          	.byte 0x2
 1332 0327 23          	.byte 0x23
 1333 0328 08          	.uleb128 0x8
 1334 0329 0D          	.uleb128 0xd
 1335 032a 74 69 6D 65 	.asciz "time_z"
 1335      5F 7A 00 
 1336 0331 04          	.byte 0x4
 1337 0332 32          	.byte 0x32
 1338 0333 1E 01 00 00 	.4byte 0x11e
 1339 0337 02          	.byte 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 29


 1340 0338 23          	.byte 0x23
 1341 0339 0C          	.uleb128 0xc
 1342 033a 0D          	.uleb128 0xd
 1343 033b 74 69 6D 65 	.asciz "time_x_y"
 1343      5F 78 5F 79 
 1343      00 
 1344 0344 04          	.byte 0x4
 1345 0345 33          	.byte 0x33
 1346 0346 1E 01 00 00 	.4byte 0x11e
 1347 034a 02          	.byte 0x2
 1348 034b 23          	.byte 0x23
 1349 034c 0E          	.uleb128 0xe
 1350 034d 00          	.byte 0x0
 1351 034e 02          	.uleb128 0x2
 1352 034f 41 48 52 53 	.asciz "AHRS_MSG_GYRO"
 1352      5F 4D 53 47 
 1352      5F 47 59 52 
 1352      4F 00 
 1353 035d 04          	.byte 0x4
 1354 035e 35          	.byte 0x35
 1355 035f FD 02 00 00 	.4byte 0x2fd
 1356 0363 04          	.uleb128 0x4
 1357 0364 10          	.byte 0x10
 1358 0365 04          	.byte 0x4
 1359 0366 39          	.byte 0x39
 1360 0367 B4 03 00 00 	.4byte 0x3b4
 1361 036b 0D          	.uleb128 0xd
 1362 036c 78 00       	.asciz "x"
 1363 036e 04          	.byte 0x4
 1364 036f 3A          	.byte 0x3a
 1365 0370 D1 02 00 00 	.4byte 0x2d1
 1366 0374 02          	.byte 0x2
 1367 0375 23          	.byte 0x23
 1368 0376 00          	.uleb128 0x0
 1369 0377 0D          	.uleb128 0xd
 1370 0378 79 00       	.asciz "y"
 1371 037a 04          	.byte 0x4
 1372 037b 3B          	.byte 0x3b
 1373 037c D1 02 00 00 	.4byte 0x2d1
 1374 0380 02          	.byte 0x2
 1375 0381 23          	.byte 0x23
 1376 0382 04          	.uleb128 0x4
 1377 0383 0D          	.uleb128 0xd
 1378 0384 7A 00       	.asciz "z"
 1379 0386 04          	.byte 0x4
 1380 0387 3C          	.byte 0x3c
 1381 0388 D1 02 00 00 	.4byte 0x2d1
 1382 038c 02          	.byte 0x2
 1383 038d 23          	.byte 0x23
 1384 038e 08          	.uleb128 0x8
 1385 038f 0D          	.uleb128 0xd
 1386 0390 74 69 6D 65 	.asciz "time_x"
 1386      5F 78 00 
 1387 0397 04          	.byte 0x4
 1388 0398 3D          	.byte 0x3d
 1389 0399 1E 01 00 00 	.4byte 0x11e
 1390 039d 02          	.byte 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 30


 1391 039e 23          	.byte 0x23
 1392 039f 0C          	.uleb128 0xc
 1393 03a0 0D          	.uleb128 0xd
 1394 03a1 74 69 6D 65 	.asciz "time_y_z"
 1394      5F 79 5F 7A 
 1394      00 
 1395 03aa 04          	.byte 0x4
 1396 03ab 3E          	.byte 0x3e
 1397 03ac 1E 01 00 00 	.4byte 0x11e
 1398 03b0 02          	.byte 0x2
 1399 03b1 23          	.byte 0x23
 1400 03b2 0E          	.uleb128 0xe
 1401 03b3 00          	.byte 0x0
 1402 03b4 02          	.uleb128 0x2
 1403 03b5 41 48 52 53 	.asciz "AHRS_MSG_ACCEL"
 1403      5F 4D 53 47 
 1403      5F 41 43 43 
 1403      45 4C 00 
 1404 03c4 04          	.byte 0x4
 1405 03c5 40          	.byte 0x40
 1406 03c6 63 03 00 00 	.4byte 0x363
 1407 03ca 04          	.uleb128 0x4
 1408 03cb 0E          	.byte 0xe
 1409 03cc 04          	.byte 0x4
 1410 03cd 44          	.byte 0x44
 1411 03ce 06 04 00 00 	.4byte 0x406
 1412 03d2 0D          	.uleb128 0xd
 1413 03d3 78 00       	.asciz "x"
 1414 03d5 04          	.byte 0x4
 1415 03d6 45          	.byte 0x45
 1416 03d7 D1 02 00 00 	.4byte 0x2d1
 1417 03db 02          	.byte 0x2
 1418 03dc 23          	.byte 0x23
 1419 03dd 00          	.uleb128 0x0
 1420 03de 0D          	.uleb128 0xd
 1421 03df 79 00       	.asciz "y"
 1422 03e1 04          	.byte 0x4
 1423 03e2 46          	.byte 0x46
 1424 03e3 D1 02 00 00 	.4byte 0x2d1
 1425 03e7 02          	.byte 0x2
 1426 03e8 23          	.byte 0x23
 1427 03e9 04          	.uleb128 0x4
 1428 03ea 0D          	.uleb128 0xd
 1429 03eb 7A 00       	.asciz "z"
 1430 03ed 04          	.byte 0x4
 1431 03ee 47          	.byte 0x47
 1432 03ef D1 02 00 00 	.4byte 0x2d1
 1433 03f3 02          	.byte 0x2
 1434 03f4 23          	.byte 0x23
 1435 03f5 08          	.uleb128 0x8
 1436 03f6 0D          	.uleb128 0xd
 1437 03f7 74 69 6D 65 	.asciz "time"
 1437      00 
 1438 03fc 04          	.byte 0x4
 1439 03fd 48          	.byte 0x48
 1440 03fe 1E 01 00 00 	.4byte 0x11e
 1441 0402 02          	.byte 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 31


 1442 0403 23          	.byte 0x23
 1443 0404 0C          	.uleb128 0xc
 1444 0405 00          	.byte 0x0
 1445 0406 02          	.uleb128 0x2
 1446 0407 41 48 52 53 	.asciz "AHRS_MSG_MAG"
 1446      5F 4D 53 47 
 1446      5F 4D 41 47 
 1446      00 
 1447 0414 04          	.byte 0x4
 1448 0415 4A          	.byte 0x4a
 1449 0416 CA 03 00 00 	.4byte 0x3ca
 1450 041a 04          	.uleb128 0x4
 1451 041b 12          	.byte 0x12
 1452 041c 04          	.byte 0x4
 1453 041d 4D          	.byte 0x4d
 1454 041e 62 04 00 00 	.4byte 0x462
 1455 0422 0D          	.uleb128 0xd
 1456 0423 41 00       	.asciz "A"
 1457 0425 04          	.byte 0x4
 1458 0426 4E          	.byte 0x4e
 1459 0427 D1 02 00 00 	.4byte 0x2d1
 1460 042b 02          	.byte 0x2
 1461 042c 23          	.byte 0x23
 1462 042d 00          	.uleb128 0x0
 1463 042e 0D          	.uleb128 0xd
 1464 042f 42 00       	.asciz "B"
 1465 0431 04          	.byte 0x4
 1466 0432 4F          	.byte 0x4f
 1467 0433 D1 02 00 00 	.4byte 0x2d1
 1468 0437 02          	.byte 0x2
 1469 0438 23          	.byte 0x23
 1470 0439 04          	.uleb128 0x4
 1471 043a 0D          	.uleb128 0xd
 1472 043b 43 00       	.asciz "C"
 1473 043d 04          	.byte 0x4
 1474 043e 50          	.byte 0x50
 1475 043f D1 02 00 00 	.4byte 0x2d1
 1476 0443 02          	.byte 0x2
 1477 0444 23          	.byte 0x23
 1478 0445 08          	.uleb128 0x8
 1479 0446 0D          	.uleb128 0xd
 1480 0447 44 00       	.asciz "D"
 1481 0449 04          	.byte 0x4
 1482 044a 51          	.byte 0x51
 1483 044b D1 02 00 00 	.4byte 0x2d1
 1484 044f 02          	.byte 0x2
 1485 0450 23          	.byte 0x23
 1486 0451 0C          	.uleb128 0xc
 1487 0452 0D          	.uleb128 0xd
 1488 0453 74 69 6D 65 	.asciz "time"
 1488      00 
 1489 0458 04          	.byte 0x4
 1490 0459 52          	.byte 0x52
 1491 045a 1E 01 00 00 	.4byte 0x11e
 1492 045e 02          	.byte 0x2
 1493 045f 23          	.byte 0x23
 1494 0460 10          	.uleb128 0x10
MPLAB XC16 ASSEMBLY Listing:   			page 32


 1495 0461 00          	.byte 0x0
 1496 0462 02          	.uleb128 0x2
 1497 0463 41 48 52 53 	.asciz "AHRS_MSG_QUAT"
 1497      5F 4D 53 47 
 1497      5F 51 55 41 
 1497      54 00 
 1498 0471 04          	.byte 0x4
 1499 0472 54          	.byte 0x54
 1500 0473 1A 04 00 00 	.4byte 0x41a
 1501 0477 04          	.uleb128 0x4
 1502 0478 1C          	.byte 0x1c
 1503 0479 04          	.byte 0x4
 1504 047a 57          	.byte 0x57
 1505 047b 00 05 00 00 	.4byte 0x500
 1506 047f 0D          	.uleb128 0xd
 1507 0480 72 6F 6C 6C 	.asciz "roll"
 1507      00 
 1508 0485 04          	.byte 0x4
 1509 0486 58          	.byte 0x58
 1510 0487 D1 02 00 00 	.4byte 0x2d1
 1511 048b 02          	.byte 0x2
 1512 048c 23          	.byte 0x23
 1513 048d 00          	.uleb128 0x0
 1514 048e 0D          	.uleb128 0xd
 1515 048f 70 69 74 63 	.asciz "pitch"
 1515      68 00 
 1516 0495 04          	.byte 0x4
 1517 0496 59          	.byte 0x59
 1518 0497 D1 02 00 00 	.4byte 0x2d1
 1519 049b 02          	.byte 0x2
 1520 049c 23          	.byte 0x23
 1521 049d 04          	.uleb128 0x4
 1522 049e 0D          	.uleb128 0xd
 1523 049f 79 61 77 00 	.asciz "yaw"
 1524 04a3 04          	.byte 0x4
 1525 04a4 5A          	.byte 0x5a
 1526 04a5 D1 02 00 00 	.4byte 0x2d1
 1527 04a9 02          	.byte 0x2
 1528 04aa 23          	.byte 0x23
 1529 04ab 08          	.uleb128 0x8
 1530 04ac 0A          	.uleb128 0xa
 1531 04ad 00 00 00 00 	.4byte .LASF2
 1532 04b1 04          	.byte 0x4
 1533 04b2 5B          	.byte 0x5b
 1534 04b3 D1 02 00 00 	.4byte 0x2d1
 1535 04b7 02          	.byte 0x2
 1536 04b8 23          	.byte 0x23
 1537 04b9 0C          	.uleb128 0xc
 1538 04ba 0A          	.uleb128 0xa
 1539 04bb 00 00 00 00 	.4byte .LASF3
 1540 04bf 04          	.byte 0x4
 1541 04c0 5C          	.byte 0x5c
 1542 04c1 D1 02 00 00 	.4byte 0x2d1
 1543 04c5 02          	.byte 0x2
 1544 04c6 23          	.byte 0x23
 1545 04c7 10          	.uleb128 0x10
 1546 04c8 0A          	.uleb128 0xa
MPLAB XC16 ASSEMBLY Listing:   			page 33


 1547 04c9 00 00 00 00 	.4byte .LASF4
 1548 04cd 04          	.byte 0x4
 1549 04ce 5D          	.byte 0x5d
 1550 04cf D1 02 00 00 	.4byte 0x2d1
 1551 04d3 02          	.byte 0x2
 1552 04d4 23          	.byte 0x23
 1553 04d5 14          	.uleb128 0x14
 1554 04d6 0D          	.uleb128 0xd
 1555 04d7 74 69 6D 65 	.asciz "time_angle"
 1555      5F 61 6E 67 
 1555      6C 65 00 
 1556 04e2 04          	.byte 0x4
 1557 04e3 5E          	.byte 0x5e
 1558 04e4 1E 01 00 00 	.4byte 0x11e
 1559 04e8 02          	.byte 0x2
 1560 04e9 23          	.byte 0x23
 1561 04ea 18          	.uleb128 0x18
 1562 04eb 0D          	.uleb128 0xd
 1563 04ec 74 69 6D 65 	.asciz "time_rate"
 1563      5F 72 61 74 
 1563      65 00 
 1564 04f6 04          	.byte 0x4
 1565 04f7 5F          	.byte 0x5f
 1566 04f8 1E 01 00 00 	.4byte 0x11e
 1567 04fc 02          	.byte 0x2
 1568 04fd 23          	.byte 0x23
 1569 04fe 1A          	.uleb128 0x1a
 1570 04ff 00          	.byte 0x0
 1571 0500 02          	.uleb128 0x2
 1572 0501 41 48 52 53 	.asciz "AHRS_MSG_EULER"
 1572      5F 4D 53 47 
 1572      5F 45 55 4C 
 1572      45 52 00 
 1573 0510 04          	.byte 0x4
 1574 0511 61          	.byte 0x61
 1575 0512 77 04 00 00 	.4byte 0x477
 1576 0516 04          	.uleb128 0x4
 1577 0517 64          	.byte 0x64
 1578 0518 04          	.byte 0x4
 1579 0519 65          	.byte 0x65
 1580 051a 79 05 00 00 	.4byte 0x579
 1581 051e 0A          	.uleb128 0xa
 1582 051f 00 00 00 00 	.4byte .LASF5
 1583 0523 04          	.byte 0x4
 1584 0524 66          	.byte 0x66
 1585 0525 DA 02 00 00 	.4byte 0x2da
 1586 0529 02          	.byte 0x2
 1587 052a 23          	.byte 0x23
 1588 052b 00          	.uleb128 0x0
 1589 052c 0D          	.uleb128 0xd
 1590 052d 67 79 72 6F 	.asciz "gyro"
 1590      00 
 1591 0532 04          	.byte 0x4
 1592 0533 67          	.byte 0x67
 1593 0534 4E 03 00 00 	.4byte 0x34e
 1594 0538 02          	.byte 0x2
 1595 0539 23          	.byte 0x23
MPLAB XC16 ASSEMBLY Listing:   			page 34


 1596 053a 08          	.uleb128 0x8
 1597 053b 0D          	.uleb128 0xd
 1598 053c 61 63 63 65 	.asciz "accel"
 1598      6C 00 
 1599 0542 04          	.byte 0x4
 1600 0543 68          	.byte 0x68
 1601 0544 B4 03 00 00 	.4byte 0x3b4
 1602 0548 02          	.byte 0x2
 1603 0549 23          	.byte 0x23
 1604 054a 18          	.uleb128 0x18
 1605 054b 0D          	.uleb128 0xd
 1606 054c 6D 61 67 00 	.asciz "mag"
 1607 0550 04          	.byte 0x4
 1608 0551 69          	.byte 0x69
 1609 0552 06 04 00 00 	.4byte 0x406
 1610 0556 02          	.byte 0x2
 1611 0557 23          	.byte 0x23
 1612 0558 28          	.uleb128 0x28
 1613 0559 0D          	.uleb128 0xd
 1614 055a 71 75 61 74 	.asciz "quat"
 1614      00 
 1615 055f 04          	.byte 0x4
 1616 0560 6A          	.byte 0x6a
 1617 0561 62 04 00 00 	.4byte 0x462
 1618 0565 02          	.byte 0x2
 1619 0566 23          	.byte 0x23
 1620 0567 36          	.uleb128 0x36
 1621 0568 0D          	.uleb128 0xd
 1622 0569 65 75 6C 65 	.asciz "euler"
 1622      72 00 
 1623 056f 04          	.byte 0x4
 1624 0570 6B          	.byte 0x6b
 1625 0571 00 05 00 00 	.4byte 0x500
 1626 0575 02          	.byte 0x2
 1627 0576 23          	.byte 0x23
 1628 0577 48          	.uleb128 0x48
 1629 0578 00          	.byte 0x0
 1630 0579 02          	.uleb128 0x2
 1631 057a 41 48 52 53 	.asciz "AHRS_CAN_Data"
 1631      5F 43 41 4E 
 1631      5F 44 61 74 
 1631      61 00 
 1632 0588 04          	.byte 0x4
 1633 0589 6C          	.byte 0x6c
 1634 058a 16 05 00 00 	.4byte 0x516
 1635 058e 0E          	.uleb128 0xe
 1636 058f 01          	.byte 0x1
 1637 0590 70 61 72 73 	.asciz "parse_can_message_health_temperature"
 1637      65 5F 63 61 
 1637      6E 5F 6D 65 
 1637      73 73 61 67 
 1637      65 5F 68 65 
 1637      61 6C 74 68 
 1637      5F 74 65 6D 
 1637      70 65 72 61 
 1637      74 75 72 65 
 1638 05b5 01          	.byte 0x1
MPLAB XC16 ASSEMBLY Listing:   			page 35


 1639 05b6 05          	.byte 0x5
 1640 05b7 01          	.byte 0x1
 1641 05b8 00 00 00 00 	.4byte .LFB0
 1642 05bc 00 00 00 00 	.4byte .LFE0
 1643 05c0 01          	.byte 0x1
 1644 05c1 5E          	.byte 0x5e
 1645 05c2 F1 05 00 00 	.4byte 0x5f1
 1646 05c6 0F          	.uleb128 0xf
 1647 05c7 00 00 00 00 	.4byte .LASF0
 1648 05cb 01          	.byte 0x1
 1649 05cc 05          	.byte 0x5
 1650 05cd F1 05 00 00 	.4byte 0x5f1
 1651 05d1 02          	.byte 0x2
 1652 05d2 7E          	.byte 0x7e
 1653 05d3 02          	.sleb128 2
 1654 05d4 0F          	.uleb128 0xf
 1655 05d5 00 00 00 00 	.4byte .LASF5
 1656 05d9 01          	.byte 0x1
 1657 05da 05          	.byte 0x5
 1658 05db F7 05 00 00 	.4byte 0x5f7
 1659 05df 02          	.byte 0x2
 1660 05e0 7E          	.byte 0x7e
 1661 05e1 04          	.sleb128 4
 1662 05e2 10          	.uleb128 0x10
 1663 05e3 00 00 00 00 	.4byte .LASF1
 1664 05e7 01          	.byte 0x1
 1665 05e8 07          	.byte 0x7
 1666 05e9 CB 00 00 00 	.4byte 0xcb
 1667 05ed 02          	.byte 0x2
 1668 05ee 7E          	.byte 0x7e
 1669 05ef 00          	.sleb128 0
 1670 05f0 00          	.byte 0x0
 1671 05f1 11          	.uleb128 0x11
 1672 05f2 02          	.byte 0x2
 1673 05f3 1E 01 00 00 	.4byte 0x11e
 1674 05f7 11          	.uleb128 0x11
 1675 05f8 02          	.byte 0x2
 1676 05f9 DA 02 00 00 	.4byte 0x2da
 1677 05fd 0E          	.uleb128 0xe
 1678 05fe 01          	.byte 0x1
 1679 05ff 70 61 72 73 	.asciz "parse_can_message_gyro_z_accel_x"
 1679      65 5F 63 61 
 1679      6E 5F 6D 65 
 1679      73 73 61 67 
 1679      65 5F 67 79 
 1679      72 6F 5F 7A 
 1679      5F 61 63 63 
 1679      65 6C 5F 78 
 1679      00 
 1680 0620 01          	.byte 0x1
 1681 0621 12          	.byte 0x12
 1682 0622 01          	.byte 0x1
 1683 0623 00 00 00 00 	.4byte .LFB1
 1684 0627 00 00 00 00 	.4byte .LFE1
 1685 062b 01          	.byte 0x1
 1686 062c 5E          	.byte 0x5e
 1687 062d 82 06 00 00 	.4byte 0x682
MPLAB XC16 ASSEMBLY Listing:   			page 36


 1688 0631 0F          	.uleb128 0xf
 1689 0632 00 00 00 00 	.4byte .LASF0
 1690 0636 01          	.byte 0x1
 1691 0637 12          	.byte 0x12
 1692 0638 F1 05 00 00 	.4byte 0x5f1
 1693 063c 02          	.byte 0x2
 1694 063d 7E          	.byte 0x7e
 1695 063e 04          	.sleb128 4
 1696 063f 12          	.uleb128 0x12
 1697 0640 67 79 72 6F 	.asciz "gyro"
 1697      00 
 1698 0645 01          	.byte 0x1
 1699 0646 12          	.byte 0x12
 1700 0647 82 06 00 00 	.4byte 0x682
 1701 064b 02          	.byte 0x2
 1702 064c 7E          	.byte 0x7e
 1703 064d 06          	.sleb128 6
 1704 064e 12          	.uleb128 0x12
 1705 064f 61 63 63 65 	.asciz "accel"
 1705      6C 00 
 1706 0655 01          	.byte 0x1
 1707 0656 12          	.byte 0x12
 1708 0657 88 06 00 00 	.4byte 0x688
 1709 065b 02          	.byte 0x2
 1710 065c 7E          	.byte 0x7e
 1711 065d 08          	.sleb128 8
 1712 065e 13          	.uleb128 0x13
 1713 065f 67 79 72 6F 	.asciz "gyro_z"
 1713      5F 7A 00 
 1714 0666 01          	.byte 0x1
 1715 0667 14          	.byte 0x14
 1716 0668 CB 00 00 00 	.4byte 0xcb
 1717 066c 02          	.byte 0x2
 1718 066d 7E          	.byte 0x7e
 1719 066e 00          	.sleb128 0
 1720 066f 13          	.uleb128 0x13
 1721 0670 61 63 63 65 	.asciz "accel_x"
 1721      6C 5F 78 00 
 1722 0678 01          	.byte 0x1
 1723 0679 15          	.byte 0x15
 1724 067a CB 00 00 00 	.4byte 0xcb
 1725 067e 02          	.byte 0x2
 1726 067f 7E          	.byte 0x7e
 1727 0680 02          	.sleb128 2
 1728 0681 00          	.byte 0x0
 1729 0682 11          	.uleb128 0x11
 1730 0683 02          	.byte 0x2
 1731 0684 4E 03 00 00 	.4byte 0x34e
 1732 0688 11          	.uleb128 0x11
 1733 0689 02          	.byte 0x2
 1734 068a B4 03 00 00 	.4byte 0x3b4
 1735 068e 0E          	.uleb128 0xe
 1736 068f 01          	.byte 0x1
 1737 0690 70 61 72 73 	.asciz "parse_can_message_gyro_x_y"
 1737      65 5F 63 61 
 1737      6E 5F 6D 65 
 1737      73 73 61 67 
MPLAB XC16 ASSEMBLY Listing:   			page 37


 1737      65 5F 67 79 
 1737      72 6F 5F 78 
 1737      5F 79 00 
 1738 06ab 01          	.byte 0x1
 1739 06ac 22          	.byte 0x22
 1740 06ad 01          	.byte 0x1
 1741 06ae 00 00 00 00 	.4byte .LFB2
 1742 06b2 00 00 00 00 	.4byte .LFE2
 1743 06b6 01          	.byte 0x1
 1744 06b7 5E          	.byte 0x5e
 1745 06b8 FC 06 00 00 	.4byte 0x6fc
 1746 06bc 0F          	.uleb128 0xf
 1747 06bd 00 00 00 00 	.4byte .LASF0
 1748 06c1 01          	.byte 0x1
 1749 06c2 22          	.byte 0x22
 1750 06c3 F1 05 00 00 	.4byte 0x5f1
 1751 06c7 02          	.byte 0x2
 1752 06c8 7E          	.byte 0x7e
 1753 06c9 04          	.sleb128 4
 1754 06ca 12          	.uleb128 0x12
 1755 06cb 67 79 72 6F 	.asciz "gyro"
 1755      00 
 1756 06d0 01          	.byte 0x1
 1757 06d1 22          	.byte 0x22
 1758 06d2 82 06 00 00 	.4byte 0x682
 1759 06d6 02          	.byte 0x2
 1760 06d7 7E          	.byte 0x7e
 1761 06d8 06          	.sleb128 6
 1762 06d9 13          	.uleb128 0x13
 1763 06da 67 79 72 6F 	.asciz "gyro_x"
 1763      5F 78 00 
 1764 06e1 01          	.byte 0x1
 1765 06e2 24          	.byte 0x24
 1766 06e3 CB 00 00 00 	.4byte 0xcb
 1767 06e7 02          	.byte 0x2
 1768 06e8 7E          	.byte 0x7e
 1769 06e9 00          	.sleb128 0
 1770 06ea 13          	.uleb128 0x13
 1771 06eb 67 79 72 6F 	.asciz "gyro_y"
 1771      5F 79 00 
 1772 06f2 01          	.byte 0x1
 1773 06f3 25          	.byte 0x25
 1774 06f4 CB 00 00 00 	.4byte 0xcb
 1775 06f8 02          	.byte 0x2
 1776 06f9 7E          	.byte 0x7e
 1777 06fa 02          	.sleb128 2
 1778 06fb 00          	.byte 0x0
 1779 06fc 0E          	.uleb128 0xe
 1780 06fd 01          	.byte 0x1
 1781 06fe 70 61 72 73 	.asciz "parse_can_message_accel_y_z"
 1781      65 5F 63 61 
 1781      6E 5F 6D 65 
 1781      73 73 61 67 
 1781      65 5F 61 63 
 1781      63 65 6C 5F 
 1781      79 5F 7A 00 
 1782 071a 01          	.byte 0x1
MPLAB XC16 ASSEMBLY Listing:   			page 38


 1783 071b 32          	.byte 0x32
 1784 071c 01          	.byte 0x1
 1785 071d 00 00 00 00 	.4byte .LFB3
 1786 0721 00 00 00 00 	.4byte .LFE3
 1787 0725 01          	.byte 0x1
 1788 0726 5E          	.byte 0x5e
 1789 0727 6E 07 00 00 	.4byte 0x76e
 1790 072b 0F          	.uleb128 0xf
 1791 072c 00 00 00 00 	.4byte .LASF0
 1792 0730 01          	.byte 0x1
 1793 0731 32          	.byte 0x32
 1794 0732 F1 05 00 00 	.4byte 0x5f1
 1795 0736 02          	.byte 0x2
 1796 0737 7E          	.byte 0x7e
 1797 0738 04          	.sleb128 4
 1798 0739 12          	.uleb128 0x12
 1799 073a 61 63 63 65 	.asciz "accel"
 1799      6C 00 
 1800 0740 01          	.byte 0x1
 1801 0741 32          	.byte 0x32
 1802 0742 88 06 00 00 	.4byte 0x688
 1803 0746 02          	.byte 0x2
 1804 0747 7E          	.byte 0x7e
 1805 0748 06          	.sleb128 6
 1806 0749 13          	.uleb128 0x13
 1807 074a 61 63 63 65 	.asciz "accel_y"
 1807      6C 5F 79 00 
 1808 0752 01          	.byte 0x1
 1809 0753 34          	.byte 0x34
 1810 0754 CB 00 00 00 	.4byte 0xcb
 1811 0758 02          	.byte 0x2
 1812 0759 7E          	.byte 0x7e
 1813 075a 00          	.sleb128 0
 1814 075b 13          	.uleb128 0x13
 1815 075c 61 63 63 65 	.asciz "accel_z"
 1815      6C 5F 7A 00 
 1816 0764 01          	.byte 0x1
 1817 0765 35          	.byte 0x35
 1818 0766 CB 00 00 00 	.4byte 0xcb
 1819 076a 02          	.byte 0x2
 1820 076b 7E          	.byte 0x7e
 1821 076c 02          	.sleb128 2
 1822 076d 00          	.byte 0x0
 1823 076e 0E          	.uleb128 0xe
 1824 076f 01          	.byte 0x1
 1825 0770 70 61 72 73 	.asciz "parse_can_message_mag"
 1825      65 5F 63 61 
 1825      6E 5F 6D 65 
 1825      73 73 61 67 
 1825      65 5F 6D 61 
 1825      67 00 
 1826 0786 01          	.byte 0x1
 1827 0787 41          	.byte 0x41
 1828 0788 01          	.byte 0x1
 1829 0789 00 00 00 00 	.4byte .LFB4
 1830 078d 00 00 00 00 	.4byte .LFE4
 1831 0791 01          	.byte 0x1
MPLAB XC16 ASSEMBLY Listing:   			page 39


 1832 0792 5E          	.byte 0x5e
 1833 0793 E4 07 00 00 	.4byte 0x7e4
 1834 0797 0F          	.uleb128 0xf
 1835 0798 00 00 00 00 	.4byte .LASF0
 1836 079c 01          	.byte 0x1
 1837 079d 41          	.byte 0x41
 1838 079e F1 05 00 00 	.4byte 0x5f1
 1839 07a2 02          	.byte 0x2
 1840 07a3 7E          	.byte 0x7e
 1841 07a4 06          	.sleb128 6
 1842 07a5 12          	.uleb128 0x12
 1843 07a6 6D 61 67 00 	.asciz "mag"
 1844 07aa 01          	.byte 0x1
 1845 07ab 41          	.byte 0x41
 1846 07ac E4 07 00 00 	.4byte 0x7e4
 1847 07b0 02          	.byte 0x2
 1848 07b1 7E          	.byte 0x7e
 1849 07b2 08          	.sleb128 8
 1850 07b3 13          	.uleb128 0x13
 1851 07b4 6D 61 67 5F 	.asciz "mag_x"
 1851      78 00 
 1852 07ba 01          	.byte 0x1
 1853 07bb 43          	.byte 0x43
 1854 07bc CB 00 00 00 	.4byte 0xcb
 1855 07c0 02          	.byte 0x2
 1856 07c1 7E          	.byte 0x7e
 1857 07c2 00          	.sleb128 0
 1858 07c3 13          	.uleb128 0x13
 1859 07c4 6D 61 67 5F 	.asciz "mag_y"
 1859      79 00 
 1860 07ca 01          	.byte 0x1
 1861 07cb 44          	.byte 0x44
 1862 07cc CB 00 00 00 	.4byte 0xcb
 1863 07d0 02          	.byte 0x2
 1864 07d1 7E          	.byte 0x7e
 1865 07d2 04          	.sleb128 4
 1866 07d3 13          	.uleb128 0x13
 1867 07d4 6D 61 67 5F 	.asciz "mag_z"
 1867      7A 00 
 1868 07da 01          	.byte 0x1
 1869 07db 45          	.byte 0x45
 1870 07dc CB 00 00 00 	.4byte 0xcb
 1871 07e0 02          	.byte 0x2
 1872 07e1 7E          	.byte 0x7e
 1873 07e2 02          	.sleb128 2
 1874 07e3 00          	.byte 0x0
 1875 07e4 11          	.uleb128 0x11
 1876 07e5 02          	.byte 0x2
 1877 07e6 06 04 00 00 	.4byte 0x406
 1878 07ea 0E          	.uleb128 0xe
 1879 07eb 01          	.byte 0x1
 1880 07ec 70 61 72 73 	.asciz "parse_can_message_quat"
 1880      65 5F 63 61 
 1880      6E 5F 6D 65 
 1880      73 73 61 67 
 1880      65 5F 71 75 
 1880      61 74 00 
MPLAB XC16 ASSEMBLY Listing:   			page 40


 1881 0803 01          	.byte 0x1
 1882 0804 53          	.byte 0x53
 1883 0805 01          	.byte 0x1
 1884 0806 00 00 00 00 	.4byte .LFB5
 1885 080a 00 00 00 00 	.4byte .LFE5
 1886 080e 01          	.byte 0x1
 1887 080f 5E          	.byte 0x5e
 1888 0810 A4 08 00 00 	.4byte 0x8a4
 1889 0814 0F          	.uleb128 0xf
 1890 0815 00 00 00 00 	.4byte .LASF0
 1891 0819 01          	.byte 0x1
 1892 081a 53          	.byte 0x53
 1893 081b F1 05 00 00 	.4byte 0x5f1
 1894 081f 02          	.byte 0x2
 1895 0820 7E          	.byte 0x7e
 1896 0821 08          	.sleb128 8
 1897 0822 12          	.uleb128 0x12
 1898 0823 71 75 61 74 	.asciz "quat"
 1898      00 
 1899 0828 01          	.byte 0x1
 1900 0829 53          	.byte 0x53
 1901 082a A4 08 00 00 	.4byte 0x8a4
 1902 082e 02          	.byte 0x2
 1903 082f 7E          	.byte 0x7e
 1904 0830 0A          	.sleb128 10
 1905 0831 13          	.uleb128 0x13
 1906 0832 71 75 61 74 	.asciz "quat_A_B_aux"
 1906      5F 41 5F 42 
 1906      5F 61 75 78 
 1906      00 
 1907 083f 01          	.byte 0x1
 1908 0840 55          	.byte 0x55
 1909 0841 CB 00 00 00 	.4byte 0xcb
 1910 0845 02          	.byte 0x2
 1911 0846 7E          	.byte 0x7e
 1912 0847 00          	.sleb128 0
 1913 0848 13          	.uleb128 0x13
 1914 0849 71 75 61 74 	.asciz "quat_C_D_aux"
 1914      5F 43 5F 44 
 1914      5F 61 75 78 
 1914      00 
 1915 0856 01          	.byte 0x1
 1916 0857 56          	.byte 0x56
 1917 0858 CB 00 00 00 	.4byte 0xcb
 1918 085c 02          	.byte 0x2
 1919 085d 7E          	.byte 0x7e
 1920 085e 02          	.sleb128 2
 1921 085f 13          	.uleb128 0x13
 1922 0860 71 75 61 74 	.asciz "quat_A"
 1922      5F 41 00 
 1923 0867 01          	.byte 0x1
 1924 0868 57          	.byte 0x57
 1925 0869 AE 00 00 00 	.4byte 0xae
 1926 086d 02          	.byte 0x2
 1927 086e 7E          	.byte 0x7e
 1928 086f 04          	.sleb128 4
 1929 0870 13          	.uleb128 0x13
MPLAB XC16 ASSEMBLY Listing:   			page 41


 1930 0871 71 75 61 74 	.asciz "quat_B"
 1930      5F 42 00 
 1931 0878 01          	.byte 0x1
 1932 0879 58          	.byte 0x58
 1933 087a AE 00 00 00 	.4byte 0xae
 1934 087e 02          	.byte 0x2
 1935 087f 7E          	.byte 0x7e
 1936 0880 05          	.sleb128 5
 1937 0881 13          	.uleb128 0x13
 1938 0882 71 75 61 74 	.asciz "quat_C"
 1938      5F 43 00 
 1939 0889 01          	.byte 0x1
 1940 088a 59          	.byte 0x59
 1941 088b AE 00 00 00 	.4byte 0xae
 1942 088f 02          	.byte 0x2
 1943 0890 7E          	.byte 0x7e
 1944 0891 06          	.sleb128 6
 1945 0892 13          	.uleb128 0x13
 1946 0893 71 75 61 74 	.asciz "quat_D"
 1946      5F 44 00 
 1947 089a 01          	.byte 0x1
 1948 089b 5A          	.byte 0x5a
 1949 089c AE 00 00 00 	.4byte 0xae
 1950 08a0 02          	.byte 0x2
 1951 08a1 7E          	.byte 0x7e
 1952 08a2 07          	.sleb128 7
 1953 08a3 00          	.byte 0x0
 1954 08a4 11          	.uleb128 0x11
 1955 08a5 02          	.byte 0x2
 1956 08a6 62 04 00 00 	.4byte 0x462
 1957 08aa 0E          	.uleb128 0xe
 1958 08ab 01          	.byte 0x1
 1959 08ac 70 61 72 73 	.asciz "parse_can_message_euler_angle"
 1959      65 5F 63 61 
 1959      6E 5F 6D 65 
 1959      73 73 61 67 
 1959      65 5F 65 75 
 1959      6C 65 72 5F 
 1959      61 6E 67 6C 
 1959      65 00 
 1960 08ca 01          	.byte 0x1
 1961 08cb 6E          	.byte 0x6e
 1962 08cc 01          	.byte 0x1
 1963 08cd 00 00 00 00 	.4byte .LFB6
 1964 08d1 00 00 00 00 	.4byte .LFE6
 1965 08d5 01          	.byte 0x1
 1966 08d6 5E          	.byte 0x5e
 1967 08d7 27 09 00 00 	.4byte 0x927
 1968 08db 0F          	.uleb128 0xf
 1969 08dc 00 00 00 00 	.4byte .LASF0
 1970 08e0 01          	.byte 0x1
 1971 08e1 6E          	.byte 0x6e
 1972 08e2 F1 05 00 00 	.4byte 0x5f1
 1973 08e6 02          	.byte 0x2
 1974 08e7 7E          	.byte 0x7e
 1975 08e8 06          	.sleb128 6
 1976 08e9 12          	.uleb128 0x12
MPLAB XC16 ASSEMBLY Listing:   			page 42


 1977 08ea 65 75 6C 65 	.asciz "euler"
 1977      72 00 
 1978 08f0 01          	.byte 0x1
 1979 08f1 6E          	.byte 0x6e
 1980 08f2 27 09 00 00 	.4byte 0x927
 1981 08f6 02          	.byte 0x2
 1982 08f7 7E          	.byte 0x7e
 1983 08f8 08          	.sleb128 8
 1984 08f9 13          	.uleb128 0x13
 1985 08fa 72 6F 6C 6C 	.asciz "roll"
 1985      00 
 1986 08ff 01          	.byte 0x1
 1987 0900 70          	.byte 0x70
 1988 0901 CB 00 00 00 	.4byte 0xcb
 1989 0905 02          	.byte 0x2
 1990 0906 7E          	.byte 0x7e
 1991 0907 00          	.sleb128 0
 1992 0908 13          	.uleb128 0x13
 1993 0909 70 69 74 63 	.asciz "pitch"
 1993      68 00 
 1994 090f 01          	.byte 0x1
 1995 0910 71          	.byte 0x71
 1996 0911 CB 00 00 00 	.4byte 0xcb
 1997 0915 02          	.byte 0x2
 1998 0916 7E          	.byte 0x7e
 1999 0917 02          	.sleb128 2
 2000 0918 13          	.uleb128 0x13
 2001 0919 79 61 77 00 	.asciz "yaw"
 2002 091d 01          	.byte 0x1
 2003 091e 72          	.byte 0x72
 2004 091f CB 00 00 00 	.4byte 0xcb
 2005 0923 02          	.byte 0x2
 2006 0924 7E          	.byte 0x7e
 2007 0925 04          	.sleb128 4
 2008 0926 00          	.byte 0x0
 2009 0927 11          	.uleb128 0x11
 2010 0928 02          	.byte 0x2
 2011 0929 00 05 00 00 	.4byte 0x500
 2012 092d 0E          	.uleb128 0xe
 2013 092e 01          	.byte 0x1
 2014 092f 70 61 72 73 	.asciz "parse_can_message_euler_rate"
 2014      65 5F 63 61 
 2014      6E 5F 6D 65 
 2014      73 73 61 67 
 2014      65 5F 65 75 
 2014      6C 65 72 5F 
 2014      72 61 74 65 
 2014      00 
 2015 094c 01          	.byte 0x1
 2016 094d 80          	.byte 0x80
 2017 094e 01          	.byte 0x1
 2018 094f 00 00 00 00 	.4byte .LFB7
 2019 0953 00 00 00 00 	.4byte .LFE7
 2020 0957 01          	.byte 0x1
 2021 0958 5E          	.byte 0x5e
 2022 0959 A6 09 00 00 	.4byte 0x9a6
 2023 095d 0F          	.uleb128 0xf
MPLAB XC16 ASSEMBLY Listing:   			page 43


 2024 095e 00 00 00 00 	.4byte .LASF0
 2025 0962 01          	.byte 0x1
 2026 0963 80          	.byte 0x80
 2027 0964 F1 05 00 00 	.4byte 0x5f1
 2028 0968 02          	.byte 0x2
 2029 0969 7E          	.byte 0x7e
 2030 096a 06          	.sleb128 6
 2031 096b 12          	.uleb128 0x12
 2032 096c 65 75 6C 65 	.asciz "euler"
 2032      72 00 
 2033 0972 01          	.byte 0x1
 2034 0973 80          	.byte 0x80
 2035 0974 27 09 00 00 	.4byte 0x927
 2036 0978 02          	.byte 0x2
 2037 0979 7E          	.byte 0x7e
 2038 097a 08          	.sleb128 8
 2039 097b 10          	.uleb128 0x10
 2040 097c 00 00 00 00 	.4byte .LASF2
 2041 0980 01          	.byte 0x1
 2042 0981 82          	.byte 0x82
 2043 0982 CB 00 00 00 	.4byte 0xcb
 2044 0986 02          	.byte 0x2
 2045 0987 7E          	.byte 0x7e
 2046 0988 00          	.sleb128 0
 2047 0989 10          	.uleb128 0x10
 2048 098a 00 00 00 00 	.4byte .LASF3
 2049 098e 01          	.byte 0x1
 2050 098f 83          	.byte 0x83
 2051 0990 CB 00 00 00 	.4byte 0xcb
 2052 0994 02          	.byte 0x2
 2053 0995 7E          	.byte 0x7e
 2054 0996 02          	.sleb128 2
 2055 0997 10          	.uleb128 0x10
 2056 0998 00 00 00 00 	.4byte .LASF4
 2057 099c 01          	.byte 0x1
 2058 099d 84          	.byte 0x84
 2059 099e CB 00 00 00 	.4byte 0xcb
 2060 09a2 02          	.byte 0x2
 2061 09a3 7E          	.byte 0x7e
 2062 09a4 04          	.sleb128 4
 2063 09a5 00          	.byte 0x0
 2064 09a6 0E          	.uleb128 0xe
 2065 09a7 01          	.byte 0x1
 2066 09a8 70 61 72 73 	.asciz "parse_can_ahrs"
 2066      65 5F 63 61 
 2066      6E 5F 61 68 
 2066      72 73 00 
 2067 09b7 01          	.byte 0x1
 2068 09b8 94          	.byte 0x94
 2069 09b9 01          	.byte 0x1
 2070 09ba 00 00 00 00 	.4byte .LFB8
 2071 09be 00 00 00 00 	.4byte .LFE8
 2072 09c2 01          	.byte 0x1
 2073 09c3 5E          	.byte 0x5e
 2074 09c4 E9 09 00 00 	.4byte 0x9e9
 2075 09c8 12          	.uleb128 0x12
 2076 09c9 6D 65 73 73 	.asciz "message"
MPLAB XC16 ASSEMBLY Listing:   			page 44


 2076      61 67 65 00 
 2077 09d1 01          	.byte 0x1
 2078 09d2 94          	.byte 0x94
 2079 09d3 F7 01 00 00 	.4byte 0x1f7
 2080 09d7 02          	.byte 0x2
 2081 09d8 7E          	.byte 0x7e
 2082 09d9 00          	.sleb128 0
 2083 09da 0F          	.uleb128 0xf
 2084 09db 00 00 00 00 	.4byte .LASF0
 2085 09df 01          	.byte 0x1
 2086 09e0 94          	.byte 0x94
 2087 09e1 E9 09 00 00 	.4byte 0x9e9
 2088 09e5 02          	.byte 0x2
 2089 09e6 7E          	.byte 0x7e
 2090 09e7 0C          	.sleb128 12
 2091 09e8 00          	.byte 0x0
 2092 09e9 11          	.uleb128 0x11
 2093 09ea 02          	.byte 0x2
 2094 09eb 79 05 00 00 	.4byte 0x579
 2095 09ef 00          	.byte 0x0
 2096                 	.section .debug_abbrev,info
 2097 0000 01          	.uleb128 0x1
 2098 0001 11          	.uleb128 0x11
 2099 0002 01          	.byte 0x1
 2100 0003 25          	.uleb128 0x25
 2101 0004 08          	.uleb128 0x8
 2102 0005 13          	.uleb128 0x13
 2103 0006 0B          	.uleb128 0xb
 2104 0007 03          	.uleb128 0x3
 2105 0008 08          	.uleb128 0x8
 2106 0009 1B          	.uleb128 0x1b
 2107 000a 08          	.uleb128 0x8
 2108 000b 11          	.uleb128 0x11
 2109 000c 01          	.uleb128 0x1
 2110 000d 12          	.uleb128 0x12
 2111 000e 01          	.uleb128 0x1
 2112 000f 10          	.uleb128 0x10
 2113 0010 06          	.uleb128 0x6
 2114 0011 00          	.byte 0x0
 2115 0012 00          	.byte 0x0
 2116 0013 02          	.uleb128 0x2
 2117 0014 16          	.uleb128 0x16
 2118 0015 00          	.byte 0x0
 2119 0016 03          	.uleb128 0x3
 2120 0017 08          	.uleb128 0x8
 2121 0018 3A          	.uleb128 0x3a
 2122 0019 0B          	.uleb128 0xb
 2123 001a 3B          	.uleb128 0x3b
 2124 001b 0B          	.uleb128 0xb
 2125 001c 49          	.uleb128 0x49
 2126 001d 13          	.uleb128 0x13
 2127 001e 00          	.byte 0x0
 2128 001f 00          	.byte 0x0
 2129 0020 03          	.uleb128 0x3
 2130 0021 24          	.uleb128 0x24
 2131 0022 00          	.byte 0x0
 2132 0023 0B          	.uleb128 0xb
MPLAB XC16 ASSEMBLY Listing:   			page 45


 2133 0024 0B          	.uleb128 0xb
 2134 0025 3E          	.uleb128 0x3e
 2135 0026 0B          	.uleb128 0xb
 2136 0027 03          	.uleb128 0x3
 2137 0028 08          	.uleb128 0x8
 2138 0029 00          	.byte 0x0
 2139 002a 00          	.byte 0x0
 2140 002b 04          	.uleb128 0x4
 2141 002c 13          	.uleb128 0x13
 2142 002d 01          	.byte 0x1
 2143 002e 0B          	.uleb128 0xb
 2144 002f 0B          	.uleb128 0xb
 2145 0030 3A          	.uleb128 0x3a
 2146 0031 0B          	.uleb128 0xb
 2147 0032 3B          	.uleb128 0x3b
 2148 0033 0B          	.uleb128 0xb
 2149 0034 01          	.uleb128 0x1
 2150 0035 13          	.uleb128 0x13
 2151 0036 00          	.byte 0x0
 2152 0037 00          	.byte 0x0
 2153 0038 05          	.uleb128 0x5
 2154 0039 0D          	.uleb128 0xd
 2155 003a 00          	.byte 0x0
 2156 003b 03          	.uleb128 0x3
 2157 003c 08          	.uleb128 0x8
 2158 003d 3A          	.uleb128 0x3a
 2159 003e 0B          	.uleb128 0xb
 2160 003f 3B          	.uleb128 0x3b
 2161 0040 0B          	.uleb128 0xb
 2162 0041 49          	.uleb128 0x49
 2163 0042 13          	.uleb128 0x13
 2164 0043 0B          	.uleb128 0xb
 2165 0044 0B          	.uleb128 0xb
 2166 0045 0D          	.uleb128 0xd
 2167 0046 0B          	.uleb128 0xb
 2168 0047 0C          	.uleb128 0xc
 2169 0048 0B          	.uleb128 0xb
 2170 0049 38          	.uleb128 0x38
 2171 004a 0A          	.uleb128 0xa
 2172 004b 00          	.byte 0x0
 2173 004c 00          	.byte 0x0
 2174 004d 06          	.uleb128 0x6
 2175 004e 17          	.uleb128 0x17
 2176 004f 01          	.byte 0x1
 2177 0050 0B          	.uleb128 0xb
 2178 0051 0B          	.uleb128 0xb
 2179 0052 3A          	.uleb128 0x3a
 2180 0053 0B          	.uleb128 0xb
 2181 0054 3B          	.uleb128 0x3b
 2182 0055 0B          	.uleb128 0xb
 2183 0056 01          	.uleb128 0x1
 2184 0057 13          	.uleb128 0x13
 2185 0058 00          	.byte 0x0
 2186 0059 00          	.byte 0x0
 2187 005a 07          	.uleb128 0x7
 2188 005b 0D          	.uleb128 0xd
 2189 005c 00          	.byte 0x0
MPLAB XC16 ASSEMBLY Listing:   			page 46


 2190 005d 49          	.uleb128 0x49
 2191 005e 13          	.uleb128 0x13
 2192 005f 00          	.byte 0x0
 2193 0060 00          	.byte 0x0
 2194 0061 08          	.uleb128 0x8
 2195 0062 0D          	.uleb128 0xd
 2196 0063 00          	.byte 0x0
 2197 0064 03          	.uleb128 0x3
 2198 0065 08          	.uleb128 0x8
 2199 0066 3A          	.uleb128 0x3a
 2200 0067 0B          	.uleb128 0xb
 2201 0068 3B          	.uleb128 0x3b
 2202 0069 0B          	.uleb128 0xb
 2203 006a 49          	.uleb128 0x49
 2204 006b 13          	.uleb128 0x13
 2205 006c 00          	.byte 0x0
 2206 006d 00          	.byte 0x0
 2207 006e 09          	.uleb128 0x9
 2208 006f 0D          	.uleb128 0xd
 2209 0070 00          	.byte 0x0
 2210 0071 49          	.uleb128 0x49
 2211 0072 13          	.uleb128 0x13
 2212 0073 38          	.uleb128 0x38
 2213 0074 0A          	.uleb128 0xa
 2214 0075 00          	.byte 0x0
 2215 0076 00          	.byte 0x0
 2216 0077 0A          	.uleb128 0xa
 2217 0078 0D          	.uleb128 0xd
 2218 0079 00          	.byte 0x0
 2219 007a 03          	.uleb128 0x3
 2220 007b 0E          	.uleb128 0xe
 2221 007c 3A          	.uleb128 0x3a
 2222 007d 0B          	.uleb128 0xb
 2223 007e 3B          	.uleb128 0x3b
 2224 007f 0B          	.uleb128 0xb
 2225 0080 49          	.uleb128 0x49
 2226 0081 13          	.uleb128 0x13
 2227 0082 38          	.uleb128 0x38
 2228 0083 0A          	.uleb128 0xa
 2229 0084 00          	.byte 0x0
 2230 0085 00          	.byte 0x0
 2231 0086 0B          	.uleb128 0xb
 2232 0087 01          	.uleb128 0x1
 2233 0088 01          	.byte 0x1
 2234 0089 49          	.uleb128 0x49
 2235 008a 13          	.uleb128 0x13
 2236 008b 01          	.uleb128 0x1
 2237 008c 13          	.uleb128 0x13
 2238 008d 00          	.byte 0x0
 2239 008e 00          	.byte 0x0
 2240 008f 0C          	.uleb128 0xc
 2241 0090 21          	.uleb128 0x21
 2242 0091 00          	.byte 0x0
 2243 0092 49          	.uleb128 0x49
 2244 0093 13          	.uleb128 0x13
 2245 0094 2F          	.uleb128 0x2f
 2246 0095 0B          	.uleb128 0xb
MPLAB XC16 ASSEMBLY Listing:   			page 47


 2247 0096 00          	.byte 0x0
 2248 0097 00          	.byte 0x0
 2249 0098 0D          	.uleb128 0xd
 2250 0099 0D          	.uleb128 0xd
 2251 009a 00          	.byte 0x0
 2252 009b 03          	.uleb128 0x3
 2253 009c 08          	.uleb128 0x8
 2254 009d 3A          	.uleb128 0x3a
 2255 009e 0B          	.uleb128 0xb
 2256 009f 3B          	.uleb128 0x3b
 2257 00a0 0B          	.uleb128 0xb
 2258 00a1 49          	.uleb128 0x49
 2259 00a2 13          	.uleb128 0x13
 2260 00a3 38          	.uleb128 0x38
 2261 00a4 0A          	.uleb128 0xa
 2262 00a5 00          	.byte 0x0
 2263 00a6 00          	.byte 0x0
 2264 00a7 0E          	.uleb128 0xe
 2265 00a8 2E          	.uleb128 0x2e
 2266 00a9 01          	.byte 0x1
 2267 00aa 3F          	.uleb128 0x3f
 2268 00ab 0C          	.uleb128 0xc
 2269 00ac 03          	.uleb128 0x3
 2270 00ad 08          	.uleb128 0x8
 2271 00ae 3A          	.uleb128 0x3a
 2272 00af 0B          	.uleb128 0xb
 2273 00b0 3B          	.uleb128 0x3b
 2274 00b1 0B          	.uleb128 0xb
 2275 00b2 27          	.uleb128 0x27
 2276 00b3 0C          	.uleb128 0xc
 2277 00b4 11          	.uleb128 0x11
 2278 00b5 01          	.uleb128 0x1
 2279 00b6 12          	.uleb128 0x12
 2280 00b7 01          	.uleb128 0x1
 2281 00b8 40          	.uleb128 0x40
 2282 00b9 0A          	.uleb128 0xa
 2283 00ba 01          	.uleb128 0x1
 2284 00bb 13          	.uleb128 0x13
 2285 00bc 00          	.byte 0x0
 2286 00bd 00          	.byte 0x0
 2287 00be 0F          	.uleb128 0xf
 2288 00bf 05          	.uleb128 0x5
 2289 00c0 00          	.byte 0x0
 2290 00c1 03          	.uleb128 0x3
 2291 00c2 0E          	.uleb128 0xe
 2292 00c3 3A          	.uleb128 0x3a
 2293 00c4 0B          	.uleb128 0xb
 2294 00c5 3B          	.uleb128 0x3b
 2295 00c6 0B          	.uleb128 0xb
 2296 00c7 49          	.uleb128 0x49
 2297 00c8 13          	.uleb128 0x13
 2298 00c9 02          	.uleb128 0x2
 2299 00ca 0A          	.uleb128 0xa
 2300 00cb 00          	.byte 0x0
 2301 00cc 00          	.byte 0x0
 2302 00cd 10          	.uleb128 0x10
 2303 00ce 34          	.uleb128 0x34
MPLAB XC16 ASSEMBLY Listing:   			page 48


 2304 00cf 00          	.byte 0x0
 2305 00d0 03          	.uleb128 0x3
 2306 00d1 0E          	.uleb128 0xe
 2307 00d2 3A          	.uleb128 0x3a
 2308 00d3 0B          	.uleb128 0xb
 2309 00d4 3B          	.uleb128 0x3b
 2310 00d5 0B          	.uleb128 0xb
 2311 00d6 49          	.uleb128 0x49
 2312 00d7 13          	.uleb128 0x13
 2313 00d8 02          	.uleb128 0x2
 2314 00d9 0A          	.uleb128 0xa
 2315 00da 00          	.byte 0x0
 2316 00db 00          	.byte 0x0
 2317 00dc 11          	.uleb128 0x11
 2318 00dd 0F          	.uleb128 0xf
 2319 00de 00          	.byte 0x0
 2320 00df 0B          	.uleb128 0xb
 2321 00e0 0B          	.uleb128 0xb
 2322 00e1 49          	.uleb128 0x49
 2323 00e2 13          	.uleb128 0x13
 2324 00e3 00          	.byte 0x0
 2325 00e4 00          	.byte 0x0
 2326 00e5 12          	.uleb128 0x12
 2327 00e6 05          	.uleb128 0x5
 2328 00e7 00          	.byte 0x0
 2329 00e8 03          	.uleb128 0x3
 2330 00e9 08          	.uleb128 0x8
 2331 00ea 3A          	.uleb128 0x3a
 2332 00eb 0B          	.uleb128 0xb
 2333 00ec 3B          	.uleb128 0x3b
 2334 00ed 0B          	.uleb128 0xb
 2335 00ee 49          	.uleb128 0x49
 2336 00ef 13          	.uleb128 0x13
 2337 00f0 02          	.uleb128 0x2
 2338 00f1 0A          	.uleb128 0xa
 2339 00f2 00          	.byte 0x0
 2340 00f3 00          	.byte 0x0
 2341 00f4 13          	.uleb128 0x13
 2342 00f5 34          	.uleb128 0x34
 2343 00f6 00          	.byte 0x0
 2344 00f7 03          	.uleb128 0x3
 2345 00f8 08          	.uleb128 0x8
 2346 00f9 3A          	.uleb128 0x3a
 2347 00fa 0B          	.uleb128 0xb
 2348 00fb 3B          	.uleb128 0x3b
 2349 00fc 0B          	.uleb128 0xb
 2350 00fd 49          	.uleb128 0x49
 2351 00fe 13          	.uleb128 0x13
 2352 00ff 02          	.uleb128 0x2
 2353 0100 0A          	.uleb128 0xa
 2354 0101 00          	.byte 0x0
 2355 0102 00          	.byte 0x0
 2356 0103 00          	.byte 0x0
 2357                 	.section .debug_pubnames,info
 2358 0000 26 01 00 00 	.4byte 0x126
 2359 0004 02 00       	.2byte 0x2
 2360 0006 00 00 00 00 	.4byte .Ldebug_info0
MPLAB XC16 ASSEMBLY Listing:   			page 49


 2361 000a F0 09 00 00 	.4byte 0x9f0
 2362 000e 8E 05 00 00 	.4byte 0x58e
 2363 0012 70 61 72 73 	.asciz "parse_can_message_health_temperature"
 2363      65 5F 63 61 
 2363      6E 5F 6D 65 
 2363      73 73 61 67 
 2363      65 5F 68 65 
 2363      61 6C 74 68 
 2363      5F 74 65 6D 
 2363      70 65 72 61 
 2363      74 75 72 65 
 2364 0037 FD 05 00 00 	.4byte 0x5fd
 2365 003b 70 61 72 73 	.asciz "parse_can_message_gyro_z_accel_x"
 2365      65 5F 63 61 
 2365      6E 5F 6D 65 
 2365      73 73 61 67 
 2365      65 5F 67 79 
 2365      72 6F 5F 7A 
 2365      5F 61 63 63 
 2365      65 6C 5F 78 
 2365      00 
 2366 005c 8E 06 00 00 	.4byte 0x68e
 2367 0060 70 61 72 73 	.asciz "parse_can_message_gyro_x_y"
 2367      65 5F 63 61 
 2367      6E 5F 6D 65 
 2367      73 73 61 67 
 2367      65 5F 67 79 
 2367      72 6F 5F 78 
 2367      5F 79 00 
 2368 007b FC 06 00 00 	.4byte 0x6fc
 2369 007f 70 61 72 73 	.asciz "parse_can_message_accel_y_z"
 2369      65 5F 63 61 
 2369      6E 5F 6D 65 
 2369      73 73 61 67 
 2369      65 5F 61 63 
 2369      63 65 6C 5F 
 2369      79 5F 7A 00 
 2370 009b 6E 07 00 00 	.4byte 0x76e
 2371 009f 70 61 72 73 	.asciz "parse_can_message_mag"
 2371      65 5F 63 61 
 2371      6E 5F 6D 65 
 2371      73 73 61 67 
 2371      65 5F 6D 61 
 2371      67 00 
 2372 00b5 EA 07 00 00 	.4byte 0x7ea
 2373 00b9 70 61 72 73 	.asciz "parse_can_message_quat"
 2373      65 5F 63 61 
 2373      6E 5F 6D 65 
 2373      73 73 61 67 
 2373      65 5F 71 75 
 2373      61 74 00 
 2374 00d0 AA 08 00 00 	.4byte 0x8aa
 2375 00d4 70 61 72 73 	.asciz "parse_can_message_euler_angle"
 2375      65 5F 63 61 
 2375      6E 5F 6D 65 
 2375      73 73 61 67 
 2375      65 5F 65 75 
MPLAB XC16 ASSEMBLY Listing:   			page 50


 2375      6C 65 72 5F 
 2375      61 6E 67 6C 
 2375      65 00 
 2376 00f2 2D 09 00 00 	.4byte 0x92d
 2377 00f6 70 61 72 73 	.asciz "parse_can_message_euler_rate"
 2377      65 5F 63 61 
 2377      6E 5F 6D 65 
 2377      73 73 61 67 
 2377      65 5F 65 75 
 2377      6C 65 72 5F 
 2377      72 61 74 65 
 2377      00 
 2378 0113 A6 09 00 00 	.4byte 0x9a6
 2379 0117 70 61 72 73 	.asciz "parse_can_ahrs"
 2379      65 5F 63 61 
 2379      6E 5F 61 68 
 2379      72 73 00 
 2380 0126 00 00 00 00 	.4byte 0x0
 2381                 	.section .debug_pubtypes,info
 2382 0000 D7 00 00 00 	.4byte 0xd7
 2383 0004 02 00       	.2byte 0x2
 2384 0006 00 00 00 00 	.4byte .Ldebug_info0
 2385 000a F0 09 00 00 	.4byte 0x9f0
 2386 000e AE 00 00 00 	.4byte 0xae
 2387 0012 69 6E 74 38 	.asciz "int8_t"
 2387      5F 74 00 
 2388 0019 CB 00 00 00 	.4byte 0xcb
 2389 001d 69 6E 74 31 	.asciz "int16_t"
 2389      36 5F 74 00 
 2390 0025 FE 00 00 00 	.4byte 0xfe
 2391 0029 75 69 6E 74 	.asciz "uint8_t"
 2391      38 5F 74 00 
 2392 0031 1E 01 00 00 	.4byte 0x11e
 2393 0035 75 69 6E 74 	.asciz "uint16_t"
 2393      31 36 5F 74 
 2393      00 
 2394 003e F7 01 00 00 	.4byte 0x1f7
 2395 0042 43 41 4E 64 	.asciz "CANdata"
 2395      61 74 61 00 
 2396 004a DA 02 00 00 	.4byte 0x2da
 2397 004e 41 48 52 53 	.asciz "AHRS_MSG_HEALTH_TEMPERATURE"
 2397      5F 4D 53 47 
 2397      5F 48 45 41 
 2397      4C 54 48 5F 
 2397      54 45 4D 50 
 2397      45 52 41 54 
 2397      55 52 45 00 
 2398 006a 4E 03 00 00 	.4byte 0x34e
 2399 006e 41 48 52 53 	.asciz "AHRS_MSG_GYRO"
 2399      5F 4D 53 47 
 2399      5F 47 59 52 
 2399      4F 00 
 2400 007c B4 03 00 00 	.4byte 0x3b4
 2401 0080 41 48 52 53 	.asciz "AHRS_MSG_ACCEL"
 2401      5F 4D 53 47 
 2401      5F 41 43 43 
 2401      45 4C 00 
MPLAB XC16 ASSEMBLY Listing:   			page 51


 2402 008f 06 04 00 00 	.4byte 0x406
 2403 0093 41 48 52 53 	.asciz "AHRS_MSG_MAG"
 2403      5F 4D 53 47 
 2403      5F 4D 41 47 
 2403      00 
 2404 00a0 62 04 00 00 	.4byte 0x462
 2405 00a4 41 48 52 53 	.asciz "AHRS_MSG_QUAT"
 2405      5F 4D 53 47 
 2405      5F 51 55 41 
 2405      54 00 
 2406 00b2 00 05 00 00 	.4byte 0x500
 2407 00b6 41 48 52 53 	.asciz "AHRS_MSG_EULER"
 2407      5F 4D 53 47 
 2407      5F 45 55 4C 
 2407      45 52 00 
 2408 00c5 79 05 00 00 	.4byte 0x579
 2409 00c9 41 48 52 53 	.asciz "AHRS_CAN_Data"
 2409      5F 43 41 4E 
 2409      5F 44 61 74 
 2409      61 00 
 2410 00d7 00 00 00 00 	.4byte 0x0
 2411                 	.section .debug_aranges,info
 2412 0000 14 00 00 00 	.4byte 0x14
 2413 0004 02 00       	.2byte 0x2
 2414 0006 00 00 00 00 	.4byte .Ldebug_info0
 2415 000a 04          	.byte 0x4
 2416 000b 00          	.byte 0x0
 2417 000c 00 00       	.2byte 0x0
 2418 000e 00 00       	.2byte 0x0
 2419 0010 00 00 00 00 	.4byte 0x0
 2420 0014 00 00 00 00 	.4byte 0x0
 2421                 	.section .debug_str,info
 2422                 	.LASF4:
 2423 0000 79 61 77 5F 	.asciz "yaw_rate"
 2423      72 61 74 65 
 2423      00 
 2424                 	.LASF1:
 2425 0009 74 65 6D 70 	.asciz "temperature"
 2425      65 72 61 74 
 2425      75 72 65 00 
 2426                 	.LASF5:
 2427 0015 68 65 61 6C 	.asciz "health_temperature"
 2427      74 68 5F 74 
 2427      65 6D 70 65 
 2427      72 61 74 75 
 2427      72 65 00 
 2428                 	.LASF2:
 2429 0028 72 6F 6C 6C 	.asciz "roll_rate"
 2429      5F 72 61 74 
 2429      65 00 
 2430                 	.LASF3:
 2431 0032 70 69 74 63 	.asciz "pitch_rate"
 2431      68 5F 72 61 
 2431      74 65 00 
 2432                 	.LASF0:
 2433 003d 64 61 74 61 	.asciz "data"
 2433      00 
MPLAB XC16 ASSEMBLY Listing:   			page 52


 2434                 	.section .text,code
 2435              	
 2436              	
 2437              	
 2438              	.section __c30_info,info,bss
 2439                 	__psv_trap_errata:
 2440                 	
 2441                 	.section __c30_signature,info,data
 2442 0000 01 00       	.word 0x0001
 2443 0002 00 00       	.word 0x0000
 2444 0004 00 00       	.word 0x0000
 2445                 	
 2446                 	
 2447                 	
 2448                 	.set ___PA___,0
 2449                 	.end
MPLAB XC16 ASSEMBLY Listing:   			page 53


DEFINED SYMBOLS
                            *ABS*:00000000 /home/user/Documents/FST/Programming/project_sub-zero/lib/can-ids/Devices/AHRS_CAN.c
                            *ABS*:00000001 __33EP256MU806
                            *ABS*:00000001 __dsPIC33E
                            *ABS*:00000001 __HAS_DSP
                            *ABS*:00000001 __HAS_DMAV2
                            *ABS*:00000001 __HAS_CODEGUARD
                            *ABS*:00000001 __HAS_PMP_ENHANCED
                            *ABS*:00000001 __HAS_EDS
                            *ABS*:00000001 __HAS_EP
                            *ABS*:00000011 __TARGET_DIVIDE_CYCLES
                            *ABS*:00000001 __C30ELF
    {standard input}:14     .text:00000000 _parse_can_message_health_temperature
    {standard input}:18     *ABS*:00000000 ___PA___
    {standard input}:74     .text:00000044 _parse_can_message_gyro_z_accel_x
    {standard input}:166    .text:000000a8 _parse_can_message_gyro_x_y
    {standard input}:247    .text:000000fe _parse_can_message_accel_y_z
    {standard input}:329    .text:00000156 _parse_can_message_mag
    {standard input}:427    .text:000001c4 _parse_can_message_quat
    {standard input}:529    .text:00000234 _parse_can_message_euler_angle
    {standard input}:629    .text:000002a2 _parse_can_message_euler_rate
    {standard input}:730    .text:00000312 _parse_can_ahrs
    {standard input}:750    *ABS*:00000000 ___BP___
    {standard input}:2439   __c30_info:00000000 __psv_trap_errata
    {standard input}:19     .text:00000000 .L0
                            .text:000003bc .L21
                            .text:0000038a .L15
                            .text:00000352 .L20
                            .text:00000374 .L13
                            .text:00000382 .L14
                            .text:0000036c .L12
                            .text:000003bc .L9
                            .text:0000039e .L17
                            .text:00000394 .L16
                            .text:000003a8 .L18
                            .text:000003b2 .L19
                    .debug_abbrev:00000000 .Ldebug_abbrev0
                            .text:00000000 .Ltext0
                            .text:000003c4 .Letext0
                      .debug_line:00000000 .Ldebug_line0
                       .debug_str:0000003d .LASF0
                       .debug_str:00000009 .LASF1
                       .debug_str:00000028 .LASF2
                       .debug_str:00000032 .LASF3
                       .debug_str:00000000 .LASF4
                       .debug_str:00000015 .LASF5
                            .text:00000000 .LFB0
                            .text:00000044 .LFE0
                            .text:00000044 .LFB1
                            .text:000000a8 .LFE1
                            .text:000000a8 .LFB2
                            .text:000000fe .LFE2
                            .text:000000fe .LFB3
                            .text:00000156 .LFE3
                            .text:00000156 .LFB4
                            .text:000001c4 .LFE4
                            .text:000001c4 .LFB5
MPLAB XC16 ASSEMBLY Listing:   			page 54


                            .text:00000234 .LFE5
                            .text:00000234 .LFB6
                            .text:000002a2 .LFE6
                            .text:000002a2 .LFB7
                            .text:00000312 .LFE7
                            .text:00000312 .LFB8
                            .text:000003c4 .LFE8
                     .debug_frame:00000000 .Lframe0
                      .debug_info:00000000 .Ldebug_info0

UNDEFINED SYMBOLS
___floatsisf
CORCON

EQUATE SYMBOLS
/home/user/Documents/FST/Programming/project_sub-zero/lib/can-ids/Devices/AHRS_CAN.c = 0x0
                  __33EP256MU806 = 0x1
                      __dsPIC33E = 0x1
                       __HAS_DSP = 0x1
                     __HAS_DMAV2 = 0x1
                 __HAS_CODEGUARD = 0x1
              __HAS_PMP_ENHANCED = 0x1
                       __HAS_EDS = 0x1
                        __HAS_EP = 0x1
          __TARGET_DIVIDE_CYCLES = 0x11
                        __C30ELF = 0x1
                        ___PA___ = 0x0
                        ___BP___ = 0x0
