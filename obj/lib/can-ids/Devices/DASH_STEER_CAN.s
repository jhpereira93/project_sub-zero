MPLAB XC16 ASSEMBLY Listing:   			page 1


   1              	.file "/home/user/Documents/FST/Programming/project_sub-zero/lib/can-ids/Devices/DASH_STEER_CAN.c"
   2              	.section .debug_abbrev,info
   3                 	.Ldebug_abbrev0:
   4                 	.section .debug_info,info
   5                 	.Ldebug_info0:
   6                 	.section .debug_line,info
   7                 	.Ldebug_line0:
   8 0000 C1 00 00 00 	.section .text,code
   8      02 00 8F 00 
   8      00 00 01 01 
   8      FB 0E 0A 00 
   8      01 01 01 01 
   8      00 00 00 01 
   8      6C 69 62 2F 
   8      63 61 6E 2D 
   8      69 64 73 2F 
   9              	.Ltext0:
  10              	.section .text,code
  11              	.align 2
  12              	.global _parse_can_message_motor_off
  13              	.type _parse_can_message_motor_off,@function
  14              	_parse_can_message_motor_off:
  15              	.LFB0:
  16              	.file 1 "lib/can-ids/Devices/DASH_STEER_CAN.c"
   1:lib/can-ids/Devices/DASH_STEER_CAN.c **** #include <stdbool.h>
   2:lib/can-ids/Devices/DASH_STEER_CAN.c **** #include <stdint.h>
   3:lib/can-ids/Devices/DASH_STEER_CAN.c **** #include <stdio.h>
   4:lib/can-ids/Devices/DASH_STEER_CAN.c **** 
   5:lib/can-ids/Devices/DASH_STEER_CAN.c **** #include "can-ids/CAN_IDs.h"
   6:lib/can-ids/Devices/DASH_STEER_CAN.c **** #include "DASH_STEER_CAN.h"
   7:lib/can-ids/Devices/DASH_STEER_CAN.c **** 
   8:lib/can-ids/Devices/DASH_STEER_CAN.c **** void parse_can_message_motor_off(uint16_t data[4],DASH_MSG_Motor *motor){
  17              	.loc 1 8 0
  18              	.set ___PA___,1
  19 000000  04 00 FA 	lnk #4
  20              	.LCFI0:
  21              	.loc 1 8 0
  22 000002  00 0F 78 	mov w0,[w14]
  23 000004  11 07 98 	mov w1,[w14+2]
   9:lib/can-ids/Devices/DASH_STEER_CAN.c ****         motor->motor = (MOTOR)data[0];
  24              	.loc 1 9 0
  25 000006  9E 00 78 	mov [w14],w1
  26 000008  1E 00 90 	mov [w14+2],w0
  27 00000a  91 00 78 	mov [w1],w1
  28 00000c  01 08 78 	mov w1,[w0]
  10:lib/can-ids/Devices/DASH_STEER_CAN.c **** }
  29              	.loc 1 10 0
  30 00000e  8E 07 78 	mov w14,w15
  31 000010  4F 07 78 	mov [--w15],w14
  32 000012  00 40 A9 	bclr CORCON,#2
  33 000014  00 00 06 	return 
  34              	.set ___PA___,0
  35              	.LFE0:
  36              	.size _parse_can_message_motor_off,.-_parse_can_message_motor_off
  37              	.align 2
  38              	.global _parse_can_message_accumulator
  39              	.type _parse_can_message_accumulator,@function
MPLAB XC16 ASSEMBLY Listing:   			page 2


  40              	_parse_can_message_accumulator:
  41              	.LFB1:
  11:lib/can-ids/Devices/DASH_STEER_CAN.c **** 
  12:lib/can-ids/Devices/DASH_STEER_CAN.c **** void parse_can_message_accumulator(uint16_t data[4], DASH_MSG_Accumulator *info) {
  42              	.loc 1 12 0
  43              	.set ___PA___,1
  44 000016  04 00 FA 	lnk #4
  45              	.LCFI1:
  46              	.loc 1 12 0
  47 000018  00 0F 78 	mov w0,[w14]
  48 00001a  11 07 98 	mov w1,[w14+2]
  13:lib/can-ids/Devices/DASH_STEER_CAN.c **** 	info->max_temp = (data[0] >> 8) & 0xFF;
  49              	.loc 1 13 0
  50 00001c  9E 00 78 	mov [w14],w1
  51 00001e  1E 00 90 	mov [w14+2],w0
  52 000020  91 00 78 	mov [w1],w1
  53 000022  C8 08 DE 	lsr w1,#8,w1
  54 000024  81 40 78 	mov.b w1,w1
  55 000026  01 48 78 	mov.b w1,[w0]
  14:lib/can-ids/Devices/DASH_STEER_CAN.c **** 	info->mean_temp = data[0] & 0xFF;
  56              	.loc 1 14 0
  57 000028  9E 00 78 	mov [w14],w1
  58 00002a  1E 00 90 	mov [w14+2],w0
  59 00002c  91 00 78 	mov [w1],w1
  60 00002e  81 40 78 	mov.b w1,w1
  61 000030  11 40 98 	mov.b w1,[w0+1]
  15:lib/can-ids/Devices/DASH_STEER_CAN.c ****     info->min_voltage = data[1];
  62              	.loc 1 15 0
  63 000032  1E 80 E8 	inc2 [w14],w0
  16:lib/can-ids/Devices/DASH_STEER_CAN.c **** 	info->percentage = data[2];
  64              	.loc 1 16 0
  65 000034  1E 01 78 	mov [w14],w2
  66 000036  E4 00 41 	add w2,#4,w1
  67              	.loc 1 15 0
  68 000038  90 01 78 	mov [w0],w3
  69 00003a  1E 01 90 	mov [w14+2],w2
  70              	.loc 1 16 0
  71 00003c  1E 00 90 	mov [w14+2],w0
  72              	.loc 1 15 0
  73 00003e  13 01 98 	mov w3,[w2+2]
  74              	.loc 1 16 0
  75 000040  91 00 78 	mov [w1],w1
  76 000042  81 40 78 	mov.b w1,w1
  77 000044  41 40 98 	mov.b w1,[w0+4]
  17:lib/can-ids/Devices/DASH_STEER_CAN.c **** }
  78              	.loc 1 17 0
  79 000046  8E 07 78 	mov w14,w15
  80 000048  4F 07 78 	mov [--w15],w14
  81 00004a  00 40 A9 	bclr CORCON,#2
  82 00004c  00 00 06 	return 
  83              	.set ___PA___,0
  84              	.LFE1:
  85              	.size _parse_can_message_accumulator,.-_parse_can_message_accumulator
  86              	.align 2
  87              	.global _parse_can_message_peripherals
  88              	.type _parse_can_message_peripherals,@function
  89              	_parse_can_message_peripherals:
MPLAB XC16 ASSEMBLY Listing:   			page 3


  90              	.LFB2:
  18:lib/can-ids/Devices/DASH_STEER_CAN.c **** 
  19:lib/can-ids/Devices/DASH_STEER_CAN.c **** void parse_can_message_peripherals(uint16_t data[4], DASH_MSG_Peripherals *peripherals){
  91              	.loc 1 19 0
  92              	.set ___PA___,1
  93 00004e  04 00 FA 	lnk #4
  94              	.LCFI2:
  95              	.loc 1 19 0
  96 000050  00 0F 78 	mov w0,[w14]
  97 000052  11 07 98 	mov w1,[w14+2]
  20:lib/can-ids/Devices/DASH_STEER_CAN.c **** 	peripherals->pump2 = data[0] & 0b0001;
  98              	.loc 1 20 0
  99 000054  9E 00 78 	mov [w14],w1
 100 000056  1E 00 90 	mov [w14+2],w0
 101 000058  91 00 78 	mov [w1],w1
 102 00005a  10 41 78 	mov.b [w0],w2
 103 00005c  E1 80 60 	and w1,#1,w1
 104 00005e  02 04 A1 	bclr.b w2,#0
 105 000060  01 F0 A7 	btsc w1,#15
 106 000062  81 00 EA 	neg w1,w1
 107 000064  81 00 EA 	neg w1,w1
 108 000066  CF 08 DE 	lsr w1,#15,w1
 109 000068  81 40 78 	mov.b w1,w1
 110 00006a  E1 C0 60 	and.b w1,#1,w1
 111 00006c  81 40 71 	ior.b w2,w1,w1
 112 00006e  01 48 78 	mov.b w1,[w0]
  21:lib/can-ids/Devices/DASH_STEER_CAN.c **** 	peripherals->pump1 = (data[0] >>1) & 0b0001;
 113              	.loc 1 21 0
 114 000070  9E 00 78 	mov [w14],w1
 115 000072  1E 00 90 	mov [w14+2],w0
 116 000074  91 00 78 	mov [w1],w1
 117 000076  10 41 78 	mov.b [w0],w2
 118 000078  81 00 D1 	lsr w1,w1
 119 00007a  02 14 A1 	bclr.b w2,#1
 120 00007c  E1 80 60 	and w1,#1,w1
 121 00007e  01 F0 A7 	btsc w1,#15
 122 000080  81 00 EA 	neg w1,w1
 123 000082  81 00 EA 	neg w1,w1
 124 000084  CF 08 DE 	lsr w1,#15,w1
 125 000086  81 40 78 	mov.b w1,w1
 126 000088  E1 C0 60 	and.b w1,#1,w1
 127 00008a  81 C0 40 	add.b w1,w1,w1
 128 00008c  81 40 71 	ior.b w2,w1,w1
 129 00008e  01 48 78 	mov.b w1,[w0]
  22:lib/can-ids/Devices/DASH_STEER_CAN.c **** 	peripherals->fans = (data[0] >>2) & 0b0001;
 130              	.loc 1 22 0
 131 000090  9E 00 78 	mov [w14],w1
 132 000092  1E 00 90 	mov [w14+2],w0
 133 000094  91 00 78 	mov [w1],w1
 134 000096  10 41 78 	mov.b [w0],w2
 135 000098  C2 09 DE 	lsr w1,#2,w3
 136 00009a  21 00 20 	mov #2,w1
 137 00009c  E1 81 61 	and w3,#1,w3
 138 00009e  02 24 A1 	bclr.b w2,#2
 139 0000a0  03 F0 A7 	btsc w3,#15
 140 0000a2  83 01 EA 	neg w3,w3
 141 0000a4  83 01 EA 	neg w3,w3
MPLAB XC16 ASSEMBLY Listing:   			page 4


 142 0000a6  CF 19 DE 	lsr w3,#15,w3
 143 0000a8  83 41 78 	mov.b w3,w3
 144 0000aa  E1 C1 61 	and.b w3,#1,w3
 145 0000ac  81 18 DD 	sl w3,w1,w1
 146 0000ae  81 40 71 	ior.b w2,w1,w1
 147 0000b0  01 48 78 	mov.b w1,[w0]
  23:lib/can-ids/Devices/DASH_STEER_CAN.c **** }
 148              	.loc 1 23 0
 149 0000b2  8E 07 78 	mov w14,w15
 150 0000b4  4F 07 78 	mov [--w15],w14
 151 0000b6  00 40 A9 	bclr CORCON,#2
 152 0000b8  00 00 06 	return 
 153              	.set ___PA___,0
 154              	.LFE2:
 155              	.size _parse_can_message_peripherals,.-_parse_can_message_peripherals
 156              	.align 2
 157              	.global _parse_can_message_mode
 158              	.type _parse_can_message_mode,@function
 159              	_parse_can_message_mode:
 160              	.LFB3:
  24:lib/can-ids/Devices/DASH_STEER_CAN.c **** 
  25:lib/can-ids/Devices/DASH_STEER_CAN.c **** void parse_can_message_mode(uint16_t data[4], _MODES *mode){
 161              	.loc 1 25 0
 162              	.set ___PA___,1
 163 0000ba  04 00 FA 	lnk #4
 164              	.LCFI3:
 165              	.loc 1 25 0
 166 0000bc  00 0F 78 	mov w0,[w14]
 167 0000be  11 07 98 	mov w1,[w14+2]
  26:lib/can-ids/Devices/DASH_STEER_CAN.c ****     *mode = (_MODES)data[0];
 168              	.loc 1 26 0
 169 0000c0  9E 00 78 	mov [w14],w1
 170 0000c2  1E 00 90 	mov [w14+2],w0
 171 0000c4  91 00 78 	mov [w1],w1
 172 0000c6  01 08 78 	mov w1,[w0]
  27:lib/can-ids/Devices/DASH_STEER_CAN.c **** }
 173              	.loc 1 27 0
 174 0000c8  8E 07 78 	mov w14,w15
 175 0000ca  4F 07 78 	mov [--w15],w14
 176 0000cc  00 40 A9 	bclr CORCON,#2
 177 0000ce  00 00 06 	return 
 178              	.set ___PA___,0
 179              	.LFE3:
 180              	.size _parse_can_message_mode,.-_parse_can_message_mode
 181              	.align 2
 182              	.global _parse_can_message_tq_mode
 183              	.type _parse_can_message_tq_mode,@function
 184              	_parse_can_message_tq_mode:
 185              	.LFB4:
  28:lib/can-ids/Devices/DASH_STEER_CAN.c **** 
  29:lib/can-ids/Devices/DASH_STEER_CAN.c **** void parse_can_message_tq_mode(uint16_t data[4], uint8_t *tq_mode){
 186              	.loc 1 29 0
 187              	.set ___PA___,1
 188 0000d0  04 00 FA 	lnk #4
 189              	.LCFI4:
 190              	.loc 1 29 0
 191 0000d2  00 0F 78 	mov w0,[w14]
MPLAB XC16 ASSEMBLY Listing:   			page 5


 192 0000d4  11 07 98 	mov w1,[w14+2]
  30:lib/can-ids/Devices/DASH_STEER_CAN.c **** 	*tq_mode = data[0];
 193              	.loc 1 30 0
 194 0000d6  9E 00 78 	mov [w14],w1
 195 0000d8  1E 00 90 	mov [w14+2],w0
 196 0000da  91 00 78 	mov [w1],w1
 197 0000dc  81 40 78 	mov.b w1,w1
 198 0000de  01 48 78 	mov.b w1,[w0]
  31:lib/can-ids/Devices/DASH_STEER_CAN.c **** }
 199              	.loc 1 31 0
 200 0000e0  8E 07 78 	mov w14,w15
 201 0000e2  4F 07 78 	mov [--w15],w14
 202 0000e4  00 40 A9 	bclr CORCON,#2
 203 0000e6  00 00 06 	return 
 204              	.set ___PA___,0
 205              	.LFE4:
 206              	.size _parse_can_message_tq_mode,.-_parse_can_message_tq_mode
 207              	.section .debug_frame,info
 208                 	.Lframe0:
 209 0000 10 00 00 00 	.4byte .LECIE0-.LSCIE0
 210                 	.LSCIE0:
 211 0004 FF FF FF FF 	.4byte 0xffffffff
 212 0008 01          	.byte 0x1
 213 0009 00          	.byte 0
 214 000a 01          	.uleb128 0x1
 215 000b 02          	.sleb128 2
 216 000c 25          	.byte 0x25
 217 000d 12          	.byte 0x12
 218 000e 0F          	.uleb128 0xf
 219 000f 7E          	.sleb128 -2
 220 0010 09          	.byte 0x9
 221 0011 25          	.uleb128 0x25
 222 0012 0F          	.uleb128 0xf
 223 0013 00          	.align 4
 224                 	.LECIE0:
 225                 	.LSFDE0:
 226 0014 18 00 00 00 	.4byte .LEFDE0-.LASFDE0
 227                 	.LASFDE0:
 228 0018 00 00 00 00 	.4byte .Lframe0
 229 001c 00 00 00 00 	.4byte .LFB0
 230 0020 16 00 00 00 	.4byte .LFE0-.LFB0
 231 0024 04          	.byte 0x4
 232 0025 02 00 00 00 	.4byte .LCFI0-.LFB0
 233 0029 13          	.byte 0x13
 234 002a 7D          	.sleb128 -3
 235 002b 0D          	.byte 0xd
 236 002c 0E          	.uleb128 0xe
 237 002d 8E          	.byte 0x8e
 238 002e 02          	.uleb128 0x2
 239 002f 00          	.align 4
 240                 	.LEFDE0:
 241                 	.LSFDE2:
 242 0030 18 00 00 00 	.4byte .LEFDE2-.LASFDE2
 243                 	.LASFDE2:
 244 0034 00 00 00 00 	.4byte .Lframe0
 245 0038 00 00 00 00 	.4byte .LFB1
 246 003c 38 00 00 00 	.4byte .LFE1-.LFB1
MPLAB XC16 ASSEMBLY Listing:   			page 6


 247 0040 04          	.byte 0x4
 248 0041 02 00 00 00 	.4byte .LCFI1-.LFB1
 249 0045 13          	.byte 0x13
 250 0046 7D          	.sleb128 -3
 251 0047 0D          	.byte 0xd
 252 0048 0E          	.uleb128 0xe
 253 0049 8E          	.byte 0x8e
 254 004a 02          	.uleb128 0x2
 255 004b 00          	.align 4
 256                 	.LEFDE2:
 257                 	.LSFDE4:
 258 004c 18 00 00 00 	.4byte .LEFDE4-.LASFDE4
 259                 	.LASFDE4:
 260 0050 00 00 00 00 	.4byte .Lframe0
 261 0054 00 00 00 00 	.4byte .LFB2
 262 0058 6C 00 00 00 	.4byte .LFE2-.LFB2
 263 005c 04          	.byte 0x4
 264 005d 02 00 00 00 	.4byte .LCFI2-.LFB2
 265 0061 13          	.byte 0x13
 266 0062 7D          	.sleb128 -3
 267 0063 0D          	.byte 0xd
 268 0064 0E          	.uleb128 0xe
 269 0065 8E          	.byte 0x8e
 270 0066 02          	.uleb128 0x2
 271 0067 00          	.align 4
 272                 	.LEFDE4:
 273                 	.LSFDE6:
 274 0068 18 00 00 00 	.4byte .LEFDE6-.LASFDE6
 275                 	.LASFDE6:
 276 006c 00 00 00 00 	.4byte .Lframe0
 277 0070 00 00 00 00 	.4byte .LFB3
 278 0074 16 00 00 00 	.4byte .LFE3-.LFB3
 279 0078 04          	.byte 0x4
 280 0079 02 00 00 00 	.4byte .LCFI3-.LFB3
 281 007d 13          	.byte 0x13
 282 007e 7D          	.sleb128 -3
 283 007f 0D          	.byte 0xd
 284 0080 0E          	.uleb128 0xe
 285 0081 8E          	.byte 0x8e
 286 0082 02          	.uleb128 0x2
 287 0083 00          	.align 4
 288                 	.LEFDE6:
 289                 	.LSFDE8:
 290 0084 18 00 00 00 	.4byte .LEFDE8-.LASFDE8
 291                 	.LASFDE8:
 292 0088 00 00 00 00 	.4byte .Lframe0
 293 008c 00 00 00 00 	.4byte .LFB4
 294 0090 18 00 00 00 	.4byte .LFE4-.LFB4
 295 0094 04          	.byte 0x4
 296 0095 02 00 00 00 	.4byte .LCFI4-.LFB4
 297 0099 13          	.byte 0x13
 298 009a 7D          	.sleb128 -3
 299 009b 0D          	.byte 0xd
 300 009c 0E          	.uleb128 0xe
 301 009d 8E          	.byte 0x8e
 302 009e 02          	.uleb128 0x2
 303 009f 00          	.align 4
MPLAB XC16 ASSEMBLY Listing:   			page 7


 304                 	.LEFDE8:
 305                 	.section .text,code
 306              	.Letext0:
 307              	.file 2 "/opt/microchip/xc16/v1.36/bin/bin/../../include/lega-c/stdint.h"
 308              	.file 3 "lib/can-ids/Devices/DASH_STEER_CAN.h"
 309              	.section .debug_info,info
 310 0000 92 04 00 00 	.4byte 0x492
 311 0004 02 00       	.2byte 0x2
 312 0006 00 00 00 00 	.4byte .Ldebug_abbrev0
 313 000a 04          	.byte 0x4
 314 000b 01          	.uleb128 0x1
 315 000c 47 4E 55 20 	.asciz "GNU C 4.5.1 (XC16, Microchip v1.36) (A) Build date: Dec  5 2018"
 315      43 20 34 2E 
 315      35 2E 31 20 
 315      28 58 43 31 
 315      36 2C 20 4D 
 315      69 63 72 6F 
 315      63 68 69 70 
 315      20 76 31 2E 
 315      33 36 29 20 
 316 004c 01          	.byte 0x1
 317 004d 6C 69 62 2F 	.asciz "lib/can-ids/Devices/DASH_STEER_CAN.c"
 317      63 61 6E 2D 
 317      69 64 73 2F 
 317      44 65 76 69 
 317      63 65 73 2F 
 317      44 41 53 48 
 317      5F 53 54 45 
 317      45 52 5F 43 
 317      41 4E 2E 63 
 318 0072 2F 68 6F 6D 	.asciz "/home/user/Documents/FST/Programming/project_sub-zero"
 318      65 2F 75 73 
 318      65 72 2F 44 
 318      6F 63 75 6D 
 318      65 6E 74 73 
 318      2F 46 53 54 
 318      2F 50 72 6F 
 318      67 72 61 6D 
 318      6D 69 6E 67 
 319 00a8 00 00 00 00 	.4byte .Ltext0
 320 00ac 00 00 00 00 	.4byte .Letext0
 321 00b0 00 00 00 00 	.4byte .Ldebug_line0
 322 00b4 02          	.uleb128 0x2
 323 00b5 01          	.byte 0x1
 324 00b6 06          	.byte 0x6
 325 00b7 73 69 67 6E 	.asciz "signed char"
 325      65 64 20 63 
 325      68 61 72 00 
 326 00c3 02          	.uleb128 0x2
 327 00c4 02          	.byte 0x2
 328 00c5 05          	.byte 0x5
 329 00c6 69 6E 74 00 	.asciz "int"
 330 00ca 02          	.uleb128 0x2
 331 00cb 04          	.byte 0x4
 332 00cc 05          	.byte 0x5
 333 00cd 6C 6F 6E 67 	.asciz "long int"
 333      20 69 6E 74 
MPLAB XC16 ASSEMBLY Listing:   			page 8


 333      00 
 334 00d6 02          	.uleb128 0x2
 335 00d7 08          	.byte 0x8
 336 00d8 05          	.byte 0x5
 337 00d9 6C 6F 6E 67 	.asciz "long long int"
 337      20 6C 6F 6E 
 337      67 20 69 6E 
 337      74 00 
 338 00e7 03          	.uleb128 0x3
 339 00e8 75 69 6E 74 	.asciz "uint8_t"
 339      38 5F 74 00 
 340 00f0 02          	.byte 0x2
 341 00f1 2B          	.byte 0x2b
 342 00f2 F6 00 00 00 	.4byte 0xf6
 343 00f6 02          	.uleb128 0x2
 344 00f7 01          	.byte 0x1
 345 00f8 08          	.byte 0x8
 346 00f9 75 6E 73 69 	.asciz "unsigned char"
 346      67 6E 65 64 
 346      20 63 68 61 
 346      72 00 
 347 0107 03          	.uleb128 0x3
 348 0108 75 69 6E 74 	.asciz "uint16_t"
 348      31 36 5F 74 
 348      00 
 349 0111 02          	.byte 0x2
 350 0112 31          	.byte 0x31
 351 0113 17 01 00 00 	.4byte 0x117
 352 0117 02          	.uleb128 0x2
 353 0118 02          	.byte 0x2
 354 0119 07          	.byte 0x7
 355 011a 75 6E 73 69 	.asciz "unsigned int"
 355      67 6E 65 64 
 355      20 69 6E 74 
 355      00 
 356 0127 02          	.uleb128 0x2
 357 0128 04          	.byte 0x4
 358 0129 07          	.byte 0x7
 359 012a 6C 6F 6E 67 	.asciz "long unsigned int"
 359      20 75 6E 73 
 359      69 67 6E 65 
 359      64 20 69 6E 
 359      74 00 
 360 013c 02          	.uleb128 0x2
 361 013d 08          	.byte 0x8
 362 013e 07          	.byte 0x7
 363 013f 6C 6F 6E 67 	.asciz "long long unsigned int"
 363      20 6C 6F 6E 
 363      67 20 75 6E 
 363      73 69 67 6E 
 363      65 64 20 69 
 363      6E 74 00 
 364 0156 02          	.uleb128 0x2
 365 0157 02          	.byte 0x2
 366 0158 07          	.byte 0x7
 367 0159 73 68 6F 72 	.asciz "short unsigned int"
 367      74 20 75 6E 
MPLAB XC16 ASSEMBLY Listing:   			page 9


 367      73 69 67 6E 
 367      65 64 20 69 
 367      6E 74 00 
 368 016c 02          	.uleb128 0x2
 369 016d 01          	.byte 0x1
 370 016e 06          	.byte 0x6
 371 016f 63 68 61 72 	.asciz "char"
 371      00 
 372 0174 04          	.uleb128 0x4
 373 0175 5F 6D 6F 74 	.asciz "_motor"
 373      6F 72 00 
 374 017c 02          	.byte 0x2
 375 017d 03          	.byte 0x3
 376 017e 12          	.byte 0x12
 377 017f 93 01 00 00 	.4byte 0x193
 378 0183 05          	.uleb128 0x5
 379 0184 52 45 41 52 	.asciz "REAR"
 379      00 
 380 0189 00          	.sleb128 0
 381 018a 05          	.uleb128 0x5
 382 018b 46 52 4F 4E 	.asciz "FRONT"
 382      54 00 
 383 0191 01          	.sleb128 1
 384 0192 00          	.byte 0x0
 385 0193 03          	.uleb128 0x3
 386 0194 4D 4F 54 4F 	.asciz "MOTOR"
 386      52 00 
 387 019a 03          	.byte 0x3
 388 019b 15          	.byte 0x15
 389 019c 74 01 00 00 	.4byte 0x174
 390 01a0 04          	.uleb128 0x4
 391 01a1 5F 6D 6F 64 	.asciz "_modes"
 391      65 73 00 
 392 01a8 02          	.byte 0x2
 393 01a9 03          	.byte 0x3
 394 01aa 17          	.byte 0x17
 395 01ab C9 01 00 00 	.4byte 0x1c9
 396 01af 05          	.uleb128 0x5
 397 01b0 5F 41 43 43 	.asciz "_ACCEL_MODE"
 397      45 4C 5F 4D 
 397      4F 44 45 00 
 398 01bc 00          	.sleb128 0
 399 01bd 05          	.uleb128 0x5
 400 01be 5F 53 50 5F 	.asciz "_SP_MODE"
 400      4D 4F 44 45 
 400      00 
 401 01c7 01          	.sleb128 1
 402 01c8 00          	.byte 0x0
 403 01c9 03          	.uleb128 0x3
 404 01ca 5F 4D 4F 44 	.asciz "_MODES"
 404      45 53 00 
 405 01d1 03          	.byte 0x3
 406 01d2 1A          	.byte 0x1a
 407 01d3 A0 01 00 00 	.4byte 0x1a0
 408 01d7 06          	.uleb128 0x6
 409 01d8 02          	.byte 0x2
 410 01d9 03          	.byte 0x3
MPLAB XC16 ASSEMBLY Listing:   			page 10


 411 01da 1C          	.byte 0x1c
 412 01db F0 01 00 00 	.4byte 0x1f0
 413 01df 07          	.uleb128 0x7
 414 01e0 6D 6F 74 6F 	.asciz "motor"
 414      72 00 
 415 01e6 03          	.byte 0x3
 416 01e7 1D          	.byte 0x1d
 417 01e8 93 01 00 00 	.4byte 0x193
 418 01ec 02          	.byte 0x2
 419 01ed 23          	.byte 0x23
 420 01ee 00          	.uleb128 0x0
 421 01ef 00          	.byte 0x0
 422 01f0 03          	.uleb128 0x3
 423 01f1 44 41 53 48 	.asciz "DASH_MSG_Motor"
 423      5F 4D 53 47 
 423      5F 4D 6F 74 
 423      6F 72 00 
 424 0200 03          	.byte 0x3
 425 0201 1E          	.byte 0x1e
 426 0202 D7 01 00 00 	.4byte 0x1d7
 427 0206 06          	.uleb128 0x6
 428 0207 06          	.byte 0x6
 429 0208 03          	.byte 0x3
 430 0209 20          	.byte 0x20
 431 020a 61 02 00 00 	.4byte 0x261
 432 020e 07          	.uleb128 0x7
 433 020f 6D 61 78 5F 	.asciz "max_temp"
 433      74 65 6D 70 
 433      00 
 434 0218 03          	.byte 0x3
 435 0219 21          	.byte 0x21
 436 021a E7 00 00 00 	.4byte 0xe7
 437 021e 02          	.byte 0x2
 438 021f 23          	.byte 0x23
 439 0220 00          	.uleb128 0x0
 440 0221 07          	.uleb128 0x7
 441 0222 6D 65 61 6E 	.asciz "mean_temp"
 441      5F 74 65 6D 
 441      70 00 
 442 022c 03          	.byte 0x3
 443 022d 22          	.byte 0x22
 444 022e E7 00 00 00 	.4byte 0xe7
 445 0232 02          	.byte 0x2
 446 0233 23          	.byte 0x23
 447 0234 01          	.uleb128 0x1
 448 0235 07          	.uleb128 0x7
 449 0236 6D 69 6E 5F 	.asciz "min_voltage"
 449      76 6F 6C 74 
 449      61 67 65 00 
 450 0242 03          	.byte 0x3
 451 0243 23          	.byte 0x23
 452 0244 07 01 00 00 	.4byte 0x107
 453 0248 02          	.byte 0x2
 454 0249 23          	.byte 0x23
 455 024a 02          	.uleb128 0x2
 456 024b 07          	.uleb128 0x7
 457 024c 70 65 72 63 	.asciz "percentage"
MPLAB XC16 ASSEMBLY Listing:   			page 11


 457      65 6E 74 61 
 457      67 65 00 
 458 0257 03          	.byte 0x3
 459 0258 24          	.byte 0x24
 460 0259 E7 00 00 00 	.4byte 0xe7
 461 025d 02          	.byte 0x2
 462 025e 23          	.byte 0x23
 463 025f 04          	.uleb128 0x4
 464 0260 00          	.byte 0x0
 465 0261 03          	.uleb128 0x3
 466 0262 44 41 53 48 	.asciz "DASH_MSG_Accumulator"
 466      5F 4D 53 47 
 466      5F 41 63 63 
 466      75 6D 75 6C 
 466      61 74 6F 72 
 466      00 
 467 0277 03          	.byte 0x3
 468 0278 25          	.byte 0x25
 469 0279 06 02 00 00 	.4byte 0x206
 470 027d 06          	.uleb128 0x6
 471 027e 01          	.byte 0x1
 472 027f 03          	.byte 0x3
 473 0280 27          	.byte 0x27
 474 0281 BE 02 00 00 	.4byte 0x2be
 475 0285 08          	.uleb128 0x8
 476 0286 70 75 6D 70 	.asciz "pump2"
 476      32 00 
 477 028c 03          	.byte 0x3
 478 028d 29          	.byte 0x29
 479 028e BE 02 00 00 	.4byte 0x2be
 480 0292 01          	.byte 0x1
 481 0293 01          	.byte 0x1
 482 0294 07          	.byte 0x7
 483 0295 02          	.byte 0x2
 484 0296 23          	.byte 0x23
 485 0297 00          	.uleb128 0x0
 486 0298 08          	.uleb128 0x8
 487 0299 70 75 6D 70 	.asciz "pump1"
 487      31 00 
 488 029f 03          	.byte 0x3
 489 02a0 2A          	.byte 0x2a
 490 02a1 BE 02 00 00 	.4byte 0x2be
 491 02a5 01          	.byte 0x1
 492 02a6 01          	.byte 0x1
 493 02a7 06          	.byte 0x6
 494 02a8 02          	.byte 0x2
 495 02a9 23          	.byte 0x23
 496 02aa 00          	.uleb128 0x0
 497 02ab 08          	.uleb128 0x8
 498 02ac 66 61 6E 73 	.asciz "fans"
 498      00 
 499 02b1 03          	.byte 0x3
 500 02b2 2B          	.byte 0x2b
 501 02b3 BE 02 00 00 	.4byte 0x2be
 502 02b7 01          	.byte 0x1
 503 02b8 01          	.byte 0x1
 504 02b9 05          	.byte 0x5
MPLAB XC16 ASSEMBLY Listing:   			page 12


 505 02ba 02          	.byte 0x2
 506 02bb 23          	.byte 0x23
 507 02bc 00          	.uleb128 0x0
 508 02bd 00          	.byte 0x0
 509 02be 02          	.uleb128 0x2
 510 02bf 01          	.byte 0x1
 511 02c0 02          	.byte 0x2
 512 02c1 5F 42 6F 6F 	.asciz "_Bool"
 512      6C 00 
 513 02c7 03          	.uleb128 0x3
 514 02c8 44 41 53 48 	.asciz "DASH_MSG_Peripherals"
 514      5F 4D 53 47 
 514      5F 50 65 72 
 514      69 70 68 65 
 514      72 61 6C 73 
 514      00 
 515 02dd 03          	.byte 0x3
 516 02de 2D          	.byte 0x2d
 517 02df 7D 02 00 00 	.4byte 0x27d
 518 02e3 09          	.uleb128 0x9
 519 02e4 01          	.byte 0x1
 520 02e5 70 61 72 73 	.asciz "parse_can_message_motor_off"
 520      65 5F 63 61 
 520      6E 5F 6D 65 
 520      73 73 61 67 
 520      65 5F 6D 6F 
 520      74 6F 72 5F 
 520      6F 66 66 00 
 521 0301 01          	.byte 0x1
 522 0302 08          	.byte 0x8
 523 0303 01          	.byte 0x1
 524 0304 00 00 00 00 	.4byte .LFB0
 525 0308 00 00 00 00 	.4byte .LFE0
 526 030c 01          	.byte 0x1
 527 030d 5E          	.byte 0x5e
 528 030e 32 03 00 00 	.4byte 0x332
 529 0312 0A          	.uleb128 0xa
 530 0313 64 61 74 61 	.asciz "data"
 530      00 
 531 0318 01          	.byte 0x1
 532 0319 08          	.byte 0x8
 533 031a 32 03 00 00 	.4byte 0x332
 534 031e 02          	.byte 0x2
 535 031f 7E          	.byte 0x7e
 536 0320 00          	.sleb128 0
 537 0321 0A          	.uleb128 0xa
 538 0322 6D 6F 74 6F 	.asciz "motor"
 538      72 00 
 539 0328 01          	.byte 0x1
 540 0329 08          	.byte 0x8
 541 032a 38 03 00 00 	.4byte 0x338
 542 032e 02          	.byte 0x2
 543 032f 7E          	.byte 0x7e
 544 0330 02          	.sleb128 2
 545 0331 00          	.byte 0x0
 546 0332 0B          	.uleb128 0xb
 547 0333 02          	.byte 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 13


 548 0334 07 01 00 00 	.4byte 0x107
 549 0338 0B          	.uleb128 0xb
 550 0339 02          	.byte 0x2
 551 033a F0 01 00 00 	.4byte 0x1f0
 552 033e 09          	.uleb128 0x9
 553 033f 01          	.byte 0x1
 554 0340 70 61 72 73 	.asciz "parse_can_message_accumulator"
 554      65 5F 63 61 
 554      6E 5F 6D 65 
 554      73 73 61 67 
 554      65 5F 61 63 
 554      63 75 6D 75 
 554      6C 61 74 6F 
 554      72 00 
 555 035e 01          	.byte 0x1
 556 035f 0C          	.byte 0xc
 557 0360 01          	.byte 0x1
 558 0361 00 00 00 00 	.4byte .LFB1
 559 0365 00 00 00 00 	.4byte .LFE1
 560 0369 01          	.byte 0x1
 561 036a 5E          	.byte 0x5e
 562 036b 8E 03 00 00 	.4byte 0x38e
 563 036f 0A          	.uleb128 0xa
 564 0370 64 61 74 61 	.asciz "data"
 564      00 
 565 0375 01          	.byte 0x1
 566 0376 0C          	.byte 0xc
 567 0377 32 03 00 00 	.4byte 0x332
 568 037b 02          	.byte 0x2
 569 037c 7E          	.byte 0x7e
 570 037d 00          	.sleb128 0
 571 037e 0A          	.uleb128 0xa
 572 037f 69 6E 66 6F 	.asciz "info"
 572      00 
 573 0384 01          	.byte 0x1
 574 0385 0C          	.byte 0xc
 575 0386 8E 03 00 00 	.4byte 0x38e
 576 038a 02          	.byte 0x2
 577 038b 7E          	.byte 0x7e
 578 038c 02          	.sleb128 2
 579 038d 00          	.byte 0x0
 580 038e 0B          	.uleb128 0xb
 581 038f 02          	.byte 0x2
 582 0390 61 02 00 00 	.4byte 0x261
 583 0394 09          	.uleb128 0x9
 584 0395 01          	.byte 0x1
 585 0396 70 61 72 73 	.asciz "parse_can_message_peripherals"
 585      65 5F 63 61 
 585      6E 5F 6D 65 
 585      73 73 61 67 
 585      65 5F 70 65 
 585      72 69 70 68 
 585      65 72 61 6C 
 585      73 00 
 586 03b4 01          	.byte 0x1
 587 03b5 13          	.byte 0x13
 588 03b6 01          	.byte 0x1
MPLAB XC16 ASSEMBLY Listing:   			page 14


 589 03b7 00 00 00 00 	.4byte .LFB2
 590 03bb 00 00 00 00 	.4byte .LFE2
 591 03bf 01          	.byte 0x1
 592 03c0 5E          	.byte 0x5e
 593 03c1 EB 03 00 00 	.4byte 0x3eb
 594 03c5 0A          	.uleb128 0xa
 595 03c6 64 61 74 61 	.asciz "data"
 595      00 
 596 03cb 01          	.byte 0x1
 597 03cc 13          	.byte 0x13
 598 03cd 32 03 00 00 	.4byte 0x332
 599 03d1 02          	.byte 0x2
 600 03d2 7E          	.byte 0x7e
 601 03d3 00          	.sleb128 0
 602 03d4 0A          	.uleb128 0xa
 603 03d5 70 65 72 69 	.asciz "peripherals"
 603      70 68 65 72 
 603      61 6C 73 00 
 604 03e1 01          	.byte 0x1
 605 03e2 13          	.byte 0x13
 606 03e3 EB 03 00 00 	.4byte 0x3eb
 607 03e7 02          	.byte 0x2
 608 03e8 7E          	.byte 0x7e
 609 03e9 02          	.sleb128 2
 610 03ea 00          	.byte 0x0
 611 03eb 0B          	.uleb128 0xb
 612 03ec 02          	.byte 0x2
 613 03ed C7 02 00 00 	.4byte 0x2c7
 614 03f1 09          	.uleb128 0x9
 615 03f2 01          	.byte 0x1
 616 03f3 70 61 72 73 	.asciz "parse_can_message_mode"
 616      65 5F 63 61 
 616      6E 5F 6D 65 
 616      73 73 61 67 
 616      65 5F 6D 6F 
 616      64 65 00 
 617 040a 01          	.byte 0x1
 618 040b 19          	.byte 0x19
 619 040c 01          	.byte 0x1
 620 040d 00 00 00 00 	.4byte .LFB3
 621 0411 00 00 00 00 	.4byte .LFE3
 622 0415 01          	.byte 0x1
 623 0416 5E          	.byte 0x5e
 624 0417 3A 04 00 00 	.4byte 0x43a
 625 041b 0A          	.uleb128 0xa
 626 041c 64 61 74 61 	.asciz "data"
 626      00 
 627 0421 01          	.byte 0x1
 628 0422 19          	.byte 0x19
 629 0423 32 03 00 00 	.4byte 0x332
 630 0427 02          	.byte 0x2
 631 0428 7E          	.byte 0x7e
 632 0429 00          	.sleb128 0
 633 042a 0A          	.uleb128 0xa
 634 042b 6D 6F 64 65 	.asciz "mode"
 634      00 
 635 0430 01          	.byte 0x1
MPLAB XC16 ASSEMBLY Listing:   			page 15


 636 0431 19          	.byte 0x19
 637 0432 3A 04 00 00 	.4byte 0x43a
 638 0436 02          	.byte 0x2
 639 0437 7E          	.byte 0x7e
 640 0438 02          	.sleb128 2
 641 0439 00          	.byte 0x0
 642 043a 0B          	.uleb128 0xb
 643 043b 02          	.byte 0x2
 644 043c C9 01 00 00 	.4byte 0x1c9
 645 0440 09          	.uleb128 0x9
 646 0441 01          	.byte 0x1
 647 0442 70 61 72 73 	.asciz "parse_can_message_tq_mode"
 647      65 5F 63 61 
 647      6E 5F 6D 65 
 647      73 73 61 67 
 647      65 5F 74 71 
 647      5F 6D 6F 64 
 647      65 00 
 648 045c 01          	.byte 0x1
 649 045d 1D          	.byte 0x1d
 650 045e 01          	.byte 0x1
 651 045f 00 00 00 00 	.4byte .LFB4
 652 0463 00 00 00 00 	.4byte .LFE4
 653 0467 01          	.byte 0x1
 654 0468 5E          	.byte 0x5e
 655 0469 8F 04 00 00 	.4byte 0x48f
 656 046d 0A          	.uleb128 0xa
 657 046e 64 61 74 61 	.asciz "data"
 657      00 
 658 0473 01          	.byte 0x1
 659 0474 1D          	.byte 0x1d
 660 0475 32 03 00 00 	.4byte 0x332
 661 0479 02          	.byte 0x2
 662 047a 7E          	.byte 0x7e
 663 047b 00          	.sleb128 0
 664 047c 0A          	.uleb128 0xa
 665 047d 74 71 5F 6D 	.asciz "tq_mode"
 665      6F 64 65 00 
 666 0485 01          	.byte 0x1
 667 0486 1D          	.byte 0x1d
 668 0487 8F 04 00 00 	.4byte 0x48f
 669 048b 02          	.byte 0x2
 670 048c 7E          	.byte 0x7e
 671 048d 02          	.sleb128 2
 672 048e 00          	.byte 0x0
 673 048f 0B          	.uleb128 0xb
 674 0490 02          	.byte 0x2
 675 0491 E7 00 00 00 	.4byte 0xe7
 676 0495 00          	.byte 0x0
 677                 	.section .debug_abbrev,info
 678 0000 01          	.uleb128 0x1
 679 0001 11          	.uleb128 0x11
 680 0002 01          	.byte 0x1
 681 0003 25          	.uleb128 0x25
 682 0004 08          	.uleb128 0x8
 683 0005 13          	.uleb128 0x13
 684 0006 0B          	.uleb128 0xb
MPLAB XC16 ASSEMBLY Listing:   			page 16


 685 0007 03          	.uleb128 0x3
 686 0008 08          	.uleb128 0x8
 687 0009 1B          	.uleb128 0x1b
 688 000a 08          	.uleb128 0x8
 689 000b 11          	.uleb128 0x11
 690 000c 01          	.uleb128 0x1
 691 000d 12          	.uleb128 0x12
 692 000e 01          	.uleb128 0x1
 693 000f 10          	.uleb128 0x10
 694 0010 06          	.uleb128 0x6
 695 0011 00          	.byte 0x0
 696 0012 00          	.byte 0x0
 697 0013 02          	.uleb128 0x2
 698 0014 24          	.uleb128 0x24
 699 0015 00          	.byte 0x0
 700 0016 0B          	.uleb128 0xb
 701 0017 0B          	.uleb128 0xb
 702 0018 3E          	.uleb128 0x3e
 703 0019 0B          	.uleb128 0xb
 704 001a 03          	.uleb128 0x3
 705 001b 08          	.uleb128 0x8
 706 001c 00          	.byte 0x0
 707 001d 00          	.byte 0x0
 708 001e 03          	.uleb128 0x3
 709 001f 16          	.uleb128 0x16
 710 0020 00          	.byte 0x0
 711 0021 03          	.uleb128 0x3
 712 0022 08          	.uleb128 0x8
 713 0023 3A          	.uleb128 0x3a
 714 0024 0B          	.uleb128 0xb
 715 0025 3B          	.uleb128 0x3b
 716 0026 0B          	.uleb128 0xb
 717 0027 49          	.uleb128 0x49
 718 0028 13          	.uleb128 0x13
 719 0029 00          	.byte 0x0
 720 002a 00          	.byte 0x0
 721 002b 04          	.uleb128 0x4
 722 002c 04          	.uleb128 0x4
 723 002d 01          	.byte 0x1
 724 002e 03          	.uleb128 0x3
 725 002f 08          	.uleb128 0x8
 726 0030 0B          	.uleb128 0xb
 727 0031 0B          	.uleb128 0xb
 728 0032 3A          	.uleb128 0x3a
 729 0033 0B          	.uleb128 0xb
 730 0034 3B          	.uleb128 0x3b
 731 0035 0B          	.uleb128 0xb
 732 0036 01          	.uleb128 0x1
 733 0037 13          	.uleb128 0x13
 734 0038 00          	.byte 0x0
 735 0039 00          	.byte 0x0
 736 003a 05          	.uleb128 0x5
 737 003b 28          	.uleb128 0x28
 738 003c 00          	.byte 0x0
 739 003d 03          	.uleb128 0x3
 740 003e 08          	.uleb128 0x8
 741 003f 1C          	.uleb128 0x1c
MPLAB XC16 ASSEMBLY Listing:   			page 17


 742 0040 0D          	.uleb128 0xd
 743 0041 00          	.byte 0x0
 744 0042 00          	.byte 0x0
 745 0043 06          	.uleb128 0x6
 746 0044 13          	.uleb128 0x13
 747 0045 01          	.byte 0x1
 748 0046 0B          	.uleb128 0xb
 749 0047 0B          	.uleb128 0xb
 750 0048 3A          	.uleb128 0x3a
 751 0049 0B          	.uleb128 0xb
 752 004a 3B          	.uleb128 0x3b
 753 004b 0B          	.uleb128 0xb
 754 004c 01          	.uleb128 0x1
 755 004d 13          	.uleb128 0x13
 756 004e 00          	.byte 0x0
 757 004f 00          	.byte 0x0
 758 0050 07          	.uleb128 0x7
 759 0051 0D          	.uleb128 0xd
 760 0052 00          	.byte 0x0
 761 0053 03          	.uleb128 0x3
 762 0054 08          	.uleb128 0x8
 763 0055 3A          	.uleb128 0x3a
 764 0056 0B          	.uleb128 0xb
 765 0057 3B          	.uleb128 0x3b
 766 0058 0B          	.uleb128 0xb
 767 0059 49          	.uleb128 0x49
 768 005a 13          	.uleb128 0x13
 769 005b 38          	.uleb128 0x38
 770 005c 0A          	.uleb128 0xa
 771 005d 00          	.byte 0x0
 772 005e 00          	.byte 0x0
 773 005f 08          	.uleb128 0x8
 774 0060 0D          	.uleb128 0xd
 775 0061 00          	.byte 0x0
 776 0062 03          	.uleb128 0x3
 777 0063 08          	.uleb128 0x8
 778 0064 3A          	.uleb128 0x3a
 779 0065 0B          	.uleb128 0xb
 780 0066 3B          	.uleb128 0x3b
 781 0067 0B          	.uleb128 0xb
 782 0068 49          	.uleb128 0x49
 783 0069 13          	.uleb128 0x13
 784 006a 0B          	.uleb128 0xb
 785 006b 0B          	.uleb128 0xb
 786 006c 0D          	.uleb128 0xd
 787 006d 0B          	.uleb128 0xb
 788 006e 0C          	.uleb128 0xc
 789 006f 0B          	.uleb128 0xb
 790 0070 38          	.uleb128 0x38
 791 0071 0A          	.uleb128 0xa
 792 0072 00          	.byte 0x0
 793 0073 00          	.byte 0x0
 794 0074 09          	.uleb128 0x9
 795 0075 2E          	.uleb128 0x2e
 796 0076 01          	.byte 0x1
 797 0077 3F          	.uleb128 0x3f
 798 0078 0C          	.uleb128 0xc
MPLAB XC16 ASSEMBLY Listing:   			page 18


 799 0079 03          	.uleb128 0x3
 800 007a 08          	.uleb128 0x8
 801 007b 3A          	.uleb128 0x3a
 802 007c 0B          	.uleb128 0xb
 803 007d 3B          	.uleb128 0x3b
 804 007e 0B          	.uleb128 0xb
 805 007f 27          	.uleb128 0x27
 806 0080 0C          	.uleb128 0xc
 807 0081 11          	.uleb128 0x11
 808 0082 01          	.uleb128 0x1
 809 0083 12          	.uleb128 0x12
 810 0084 01          	.uleb128 0x1
 811 0085 40          	.uleb128 0x40
 812 0086 0A          	.uleb128 0xa
 813 0087 01          	.uleb128 0x1
 814 0088 13          	.uleb128 0x13
 815 0089 00          	.byte 0x0
 816 008a 00          	.byte 0x0
 817 008b 0A          	.uleb128 0xa
 818 008c 05          	.uleb128 0x5
 819 008d 00          	.byte 0x0
 820 008e 03          	.uleb128 0x3
 821 008f 08          	.uleb128 0x8
 822 0090 3A          	.uleb128 0x3a
 823 0091 0B          	.uleb128 0xb
 824 0092 3B          	.uleb128 0x3b
 825 0093 0B          	.uleb128 0xb
 826 0094 49          	.uleb128 0x49
 827 0095 13          	.uleb128 0x13
 828 0096 02          	.uleb128 0x2
 829 0097 0A          	.uleb128 0xa
 830 0098 00          	.byte 0x0
 831 0099 00          	.byte 0x0
 832 009a 0B          	.uleb128 0xb
 833 009b 0F          	.uleb128 0xf
 834 009c 00          	.byte 0x0
 835 009d 0B          	.uleb128 0xb
 836 009e 0B          	.uleb128 0xb
 837 009f 49          	.uleb128 0x49
 838 00a0 13          	.uleb128 0x13
 839 00a1 00          	.byte 0x0
 840 00a2 00          	.byte 0x0
 841 00a3 00          	.byte 0x0
 842                 	.section .debug_pubnames,info
 843 0000 AB 00 00 00 	.4byte 0xab
 844 0004 02 00       	.2byte 0x2
 845 0006 00 00 00 00 	.4byte .Ldebug_info0
 846 000a 96 04 00 00 	.4byte 0x496
 847 000e E3 02 00 00 	.4byte 0x2e3
 848 0012 70 61 72 73 	.asciz "parse_can_message_motor_off"
 848      65 5F 63 61 
 848      6E 5F 6D 65 
 848      73 73 61 67 
 848      65 5F 6D 6F 
 848      74 6F 72 5F 
 848      6F 66 66 00 
 849 002e 3E 03 00 00 	.4byte 0x33e
MPLAB XC16 ASSEMBLY Listing:   			page 19


 850 0032 70 61 72 73 	.asciz "parse_can_message_accumulator"
 850      65 5F 63 61 
 850      6E 5F 6D 65 
 850      73 73 61 67 
 850      65 5F 61 63 
 850      63 75 6D 75 
 850      6C 61 74 6F 
 850      72 00 
 851 0050 94 03 00 00 	.4byte 0x394
 852 0054 70 61 72 73 	.asciz "parse_can_message_peripherals"
 852      65 5F 63 61 
 852      6E 5F 6D 65 
 852      73 73 61 67 
 852      65 5F 70 65 
 852      72 69 70 68 
 852      65 72 61 6C 
 852      73 00 
 853 0072 F1 03 00 00 	.4byte 0x3f1
 854 0076 70 61 72 73 	.asciz "parse_can_message_mode"
 854      65 5F 63 61 
 854      6E 5F 6D 65 
 854      73 73 61 67 
 854      65 5F 6D 6F 
 854      64 65 00 
 855 008d 40 04 00 00 	.4byte 0x440
 856 0091 70 61 72 73 	.asciz "parse_can_message_tq_mode"
 856      65 5F 63 61 
 856      6E 5F 6D 65 
 856      73 73 61 67 
 856      65 5F 74 71 
 856      5F 6D 6F 64 
 856      65 00 
 857 00ab 00 00 00 00 	.4byte 0x0
 858                 	.section .debug_pubtypes,info
 859 0000 97 00 00 00 	.4byte 0x97
 860 0004 02 00       	.2byte 0x2
 861 0006 00 00 00 00 	.4byte .Ldebug_info0
 862 000a 96 04 00 00 	.4byte 0x496
 863 000e E7 00 00 00 	.4byte 0xe7
 864 0012 75 69 6E 74 	.asciz "uint8_t"
 864      38 5F 74 00 
 865 001a 07 01 00 00 	.4byte 0x107
 866 001e 75 69 6E 74 	.asciz "uint16_t"
 866      31 36 5F 74 
 866      00 
 867 0027 74 01 00 00 	.4byte 0x174
 868 002b 5F 6D 6F 74 	.asciz "_motor"
 868      6F 72 00 
 869 0032 93 01 00 00 	.4byte 0x193
 870 0036 4D 4F 54 4F 	.asciz "MOTOR"
 870      52 00 
 871 003c A0 01 00 00 	.4byte 0x1a0
 872 0040 5F 6D 6F 64 	.asciz "_modes"
 872      65 73 00 
 873 0047 C9 01 00 00 	.4byte 0x1c9
 874 004b 5F 4D 4F 44 	.asciz "_MODES"
 874      45 53 00 
MPLAB XC16 ASSEMBLY Listing:   			page 20


 875 0052 F0 01 00 00 	.4byte 0x1f0
 876 0056 44 41 53 48 	.asciz "DASH_MSG_Motor"
 876      5F 4D 53 47 
 876      5F 4D 6F 74 
 876      6F 72 00 
 877 0065 61 02 00 00 	.4byte 0x261
 878 0069 44 41 53 48 	.asciz "DASH_MSG_Accumulator"
 878      5F 4D 53 47 
 878      5F 41 63 63 
 878      75 6D 75 6C 
 878      61 74 6F 72 
 878      00 
 879 007e C7 02 00 00 	.4byte 0x2c7
 880 0082 44 41 53 48 	.asciz "DASH_MSG_Peripherals"
 880      5F 4D 53 47 
 880      5F 50 65 72 
 880      69 70 68 65 
 880      72 61 6C 73 
 880      00 
 881 0097 00 00 00 00 	.4byte 0x0
 882                 	.section .debug_aranges,info
 883 0000 14 00 00 00 	.4byte 0x14
 884 0004 02 00       	.2byte 0x2
 885 0006 00 00 00 00 	.4byte .Ldebug_info0
 886 000a 04          	.byte 0x4
 887 000b 00          	.byte 0x0
 888 000c 00 00       	.2byte 0x0
 889 000e 00 00       	.2byte 0x0
 890 0010 00 00 00 00 	.4byte 0x0
 891 0014 00 00 00 00 	.4byte 0x0
 892                 	.section .debug_str,info
 893                 	.section .text,code
 894              	
 895              	
 896              	
 897              	.section __c30_info,info,bss
 898                 	__psv_trap_errata:
 899                 	
 900                 	.section __c30_signature,info,data
 901 0000 01 00       	.word 0x0001
 902 0002 00 00       	.word 0x0000
 903 0004 00 00       	.word 0x0000
 904                 	
 905                 	
 906                 	
 907                 	.set ___PA___,0
 908                 	.end
MPLAB XC16 ASSEMBLY Listing:   			page 21


DEFINED SYMBOLS
                            *ABS*:00000000 /home/user/Documents/FST/Programming/project_sub-zero/lib/can-ids/Devices/DASH_STEER_CAN.c
                            *ABS*:00000001 __33EP256MU806
                            *ABS*:00000001 __dsPIC33E
                            *ABS*:00000001 __HAS_DSP
                            *ABS*:00000001 __HAS_DMAV2
                            *ABS*:00000001 __HAS_CODEGUARD
                            *ABS*:00000001 __HAS_PMP_ENHANCED
                            *ABS*:00000001 __HAS_EDS
                            *ABS*:00000001 __HAS_EP
                            *ABS*:00000011 __TARGET_DIVIDE_CYCLES
                            *ABS*:00000001 __C30ELF
    {standard input}:14     .text:00000000 _parse_can_message_motor_off
    {standard input}:18     *ABS*:00000000 ___PA___
    {standard input}:40     .text:00000016 _parse_can_message_accumulator
    {standard input}:89     .text:0000004e _parse_can_message_peripherals
    {standard input}:159    .text:000000ba _parse_can_message_mode
    {standard input}:184    .text:000000d0 _parse_can_message_tq_mode
    {standard input}:898    __c30_info:00000000 __psv_trap_errata
    {standard input}:19     .text:00000000 .L0
                    .debug_abbrev:00000000 .Ldebug_abbrev0
                            .text:00000000 .Ltext0
                            .text:000000e8 .Letext0
                      .debug_line:00000000 .Ldebug_line0
                            .text:00000000 .LFB0
                            .text:00000016 .LFE0
                            .text:00000016 .LFB1
                            .text:0000004e .LFE1
                            .text:0000004e .LFB2
                            .text:000000ba .LFE2
                            .text:000000ba .LFB3
                            .text:000000d0 .LFE3
                            .text:000000d0 .LFB4
                            .text:000000e8 .LFE4
                     .debug_frame:00000000 .Lframe0
                      .debug_info:00000000 .Ldebug_info0

UNDEFINED SYMBOLS
CORCON

EQUATE SYMBOLS
/home/user/Documents/FST/Programming/project_sub-zero/lib/can-ids/Devices/DASH_STEER_CAN.c = 0x0
                  __33EP256MU806 = 0x1
                      __dsPIC33E = 0x1
                       __HAS_DSP = 0x1
                     __HAS_DMAV2 = 0x1
                 __HAS_CODEGUARD = 0x1
              __HAS_PMP_ENHANCED = 0x1
                       __HAS_EDS = 0x1
                        __HAS_EP = 0x1
          __TARGET_DIVIDE_CYCLES = 0x11
                        __C30ELF = 0x1
                        ___PA___ = 0x0
