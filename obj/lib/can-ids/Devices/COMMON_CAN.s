MPLAB XC16 ASSEMBLY Listing:   			page 1


   1              	.file "/home/user/Documents/FST/Programming/project_sub-zero/lib/can-ids/Devices/COMMON_CAN.c"
   2              	.section .debug_abbrev,info
   3                 	.Ldebug_abbrev0:
   4                 	.section .debug_info,info
   5                 	.Ldebug_info0:
   6                 	.section .debug_line,info
   7                 	.Ldebug_line0:
   8 0000 74 01 00 00 	.section .text,code
   8      02 00 A0 00 
   8      00 00 01 01 
   8      FB 0E 0A 00 
   8      01 01 01 01 
   8      00 00 00 01 
   8      6C 69 62 2F 
   8      63 61 6E 2D 
   8      69 64 73 2F 
   9              	.Ltext0:
  10              	.section .text,code
  11              	.align 2
  12              	.global _parse_can_common_RTD
  13              	.type _parse_can_common_RTD,@function
  14              	_parse_can_common_RTD:
  15              	.LFB0:
  16              	.file 1 "lib/can-ids/Devices/COMMON_CAN.c"
   1:lib/can-ids/Devices/COMMON_CAN.c **** #include <stdbool.h>
   2:lib/can-ids/Devices/COMMON_CAN.c **** #include <stdint.h>
   3:lib/can-ids/Devices/COMMON_CAN.c **** #include "can-ids/CAN_IDs.h"
   4:lib/can-ids/Devices/COMMON_CAN.c **** #include "COMMON_CAN.h"
   5:lib/can-ids/Devices/COMMON_CAN.c **** 
   6:lib/can-ids/Devices/COMMON_CAN.c **** void parse_can_common_RTD(CANdata message, COMMON_MSG_RTD_ON *RTD){
  17              	.loc 1 6 0
  18              	.set ___PA___,1
  19 000000  0E 00 FA 	lnk #14
  20              	.LCFI0:
  21              	.loc 1 6 0
  22 000002  00 0F 78 	mov w0,[w14]
  23 000004  22 07 98 	mov w2,[w14+4]
  24 000006  66 07 98 	mov w6,[w14+12]
   7:lib/can-ids/Devices/COMMON_CAN.c ****     
   8:lib/can-ids/Devices/COMMON_CAN.c ****     RTD->step = (RTD_ON) ((message.data[0]) & 0b1111);
  25              	.loc 1 8 0
  26 000008  2E 01 90 	mov [w14+4],w2
  27 00000a  6E 00 90 	mov [w14+12],w0
  28 00000c  6F 01 61 	and w2,#15,w2
  29              	.loc 1 6 0
  30 00000e  11 07 98 	mov w1,[w14+2]
  31 000010  33 07 98 	mov w3,[w14+6]
  32 000012  44 07 98 	mov w4,[w14+8]
  33 000014  55 07 98 	mov w5,[w14+10]
  34              	.loc 1 8 0
  35 000016  02 08 78 	mov w2,[w0]
   9:lib/can-ids/Devices/COMMON_CAN.c **** 
  10:lib/can-ids/Devices/COMMON_CAN.c ****     return;
  11:lib/can-ids/Devices/COMMON_CAN.c **** }
  36              	.loc 1 11 0
  37 000018  8E 07 78 	mov w14,w15
  38 00001a  4F 07 78 	mov [--w15],w14
MPLAB XC16 ASSEMBLY Listing:   			page 2


  39 00001c  00 40 A9 	bclr CORCON,#2
  40 00001e  00 00 06 	return 
  41              	.set ___PA___,0
  42              	.LFE0:
  43              	.size _parse_can_common_RTD,.-_parse_can_common_RTD
  44              	.align 2
  45              	.global _parse_can_common_trap
  46              	.type _parse_can_common_trap,@function
  47              	_parse_can_common_trap:
  48              	.LFB1:
  12:lib/can-ids/Devices/COMMON_CAN.c **** 
  13:lib/can-ids/Devices/COMMON_CAN.c **** void parse_can_common_trap(CANdata message, COMMON_MSG_TRAP *trap){
  49              	.loc 1 13 0
  50              	.set ___PA___,1
  51 000020  0E 00 FA 	lnk #14
  52              	.LCFI1:
  53              	.loc 1 13 0
  54 000022  00 0F 78 	mov w0,[w14]
  55 000024  22 07 98 	mov w2,[w14+4]
  56 000026  66 07 98 	mov w6,[w14+12]
  14:lib/can-ids/Devices/COMMON_CAN.c ****     
  15:lib/can-ids/Devices/COMMON_CAN.c ****     trap->type = message.data[0];
  57              	.loc 1 15 0
  58 000028  2E 01 90 	mov [w14+4],w2
  59 00002a  6E 00 90 	mov [w14+12],w0
  60 00002c  02 41 78 	mov.b w2,w2
  61              	.loc 1 13 0
  62 00002e  11 07 98 	mov w1,[w14+2]
  63 000030  33 07 98 	mov w3,[w14+6]
  64 000032  44 07 98 	mov w4,[w14+8]
  65 000034  55 07 98 	mov w5,[w14+10]
  66              	.loc 1 15 0
  67 000036  02 48 78 	mov.b w2,[w0]
  16:lib/can-ids/Devices/COMMON_CAN.c **** 
  17:lib/can-ids/Devices/COMMON_CAN.c ****     return;
  18:lib/can-ids/Devices/COMMON_CAN.c **** }
  68              	.loc 1 18 0
  69 000038  8E 07 78 	mov w14,w15
  70 00003a  4F 07 78 	mov [--w15],w14
  71 00003c  00 40 A9 	bclr CORCON,#2
  72 00003e  00 00 06 	return 
  73              	.set ___PA___,0
  74              	.LFE1:
  75              	.size _parse_can_common_trap,.-_parse_can_common_trap
  76              	.align 2
  77              	.global _parse_can_common_reset
  78              	.type _parse_can_common_reset,@function
  79              	_parse_can_common_reset:
  80              	.LFB2:
  19:lib/can-ids/Devices/COMMON_CAN.c **** 
  20:lib/can-ids/Devices/COMMON_CAN.c **** void parse_can_common_reset(CANdata message, COMMON_MSG_RESET *RESET){
  81              	.loc 1 20 0
  82              	.set ___PA___,1
  83 000040  0E 00 FA 	lnk #14
  84              	.LCFI2:
  85              	.loc 1 20 0
  86 000042  00 0F 78 	mov w0,[w14]
MPLAB XC16 ASSEMBLY Listing:   			page 3


  87 000044  22 07 98 	mov w2,[w14+4]
  88 000046  66 07 98 	mov w6,[w14+12]
  21:lib/can-ids/Devices/COMMON_CAN.c **** 
  22:lib/can-ids/Devices/COMMON_CAN.c ****     RESET->RCON = message.data[0];
  89              	.loc 1 22 0
  90 000048  2E 01 90 	mov [w14+4],w2
  91 00004a  6E 00 90 	mov [w14+12],w0
  92 00004c  02 41 78 	mov.b w2,w2
  93              	.loc 1 20 0
  94 00004e  11 07 98 	mov w1,[w14+2]
  95 000050  33 07 98 	mov w3,[w14+6]
  96 000052  44 07 98 	mov w4,[w14+8]
  97 000054  55 07 98 	mov w5,[w14+10]
  98              	.loc 1 22 0
  99 000056  02 48 78 	mov.b w2,[w0]
  23:lib/can-ids/Devices/COMMON_CAN.c **** 
  24:lib/can-ids/Devices/COMMON_CAN.c ****     return;
  25:lib/can-ids/Devices/COMMON_CAN.c **** }
 100              	.loc 1 25 0
 101 000058  8E 07 78 	mov w14,w15
 102 00005a  4F 07 78 	mov [--w15],w14
 103 00005c  00 40 A9 	bclr CORCON,#2
 104 00005e  00 00 06 	return 
 105              	.set ___PA___,0
 106              	.LFE2:
 107              	.size _parse_can_common_reset,.-_parse_can_common_reset
 108              	.align 2
 109              	.global _parse_can_common_parse_error
 110              	.type _parse_can_common_parse_error,@function
 111              	_parse_can_common_parse_error:
 112              	.LFB3:
  26:lib/can-ids/Devices/COMMON_CAN.c **** 
  27:lib/can-ids/Devices/COMMON_CAN.c **** void parse_can_common_parse_error(CANdata message, COMMON_MSG_PARSE_ERROR *PARSE){
 113              	.loc 1 27 0
 114              	.set ___PA___,1
 115 000060  0E 00 FA 	lnk #14
 116              	.LCFI3:
 117              	.loc 1 27 0
 118 000062  00 0F 78 	mov w0,[w14]
 119 000064  22 07 98 	mov w2,[w14+4]
 120 000066  66 07 98 	mov w6,[w14+12]
  28:lib/can-ids/Devices/COMMON_CAN.c **** 
  29:lib/can-ids/Devices/COMMON_CAN.c ****     PARSE->wrong_dev_id = message.data[0] & 0b11111;
 121              	.loc 1 29 0
 122 000068  2E 01 90 	mov [w14+4],w2
 123 00006a  6E 00 90 	mov [w14+12],w0
 124 00006c  02 41 78 	mov.b w2,w2
 125 00006e  7F 41 61 	and.b w2,#31,w2
 126 000070  02 48 78 	mov.b w2,[w0]
  30:lib/can-ids/Devices/COMMON_CAN.c ****     PARSE->right_dev_id = (message.data [0] >> 5) & 0b11111;
 127              	.loc 1 30 0
 128 000072  2E 01 90 	mov [w14+4],w2
 129 000074  6E 00 90 	mov [w14+12],w0
 130 000076  45 11 DE 	lsr w2,#5,w2
 131 000078  02 41 78 	mov.b w2,w2
 132 00007a  7F 41 61 	and.b w2,#31,w2
 133 00007c  12 40 98 	mov.b w2,[w0+1]
MPLAB XC16 ASSEMBLY Listing:   			page 4


 134              	.loc 1 27 0
 135 00007e  11 07 98 	mov w1,[w14+2]
 136 000080  33 07 98 	mov w3,[w14+6]
 137 000082  44 07 98 	mov w4,[w14+8]
 138 000084  55 07 98 	mov w5,[w14+10]
  31:lib/can-ids/Devices/COMMON_CAN.c **** 
  32:lib/can-ids/Devices/COMMON_CAN.c ****     return;
  33:lib/can-ids/Devices/COMMON_CAN.c **** }
 139              	.loc 1 33 0
 140 000086  8E 07 78 	mov w14,w15
 141 000088  4F 07 78 	mov [--w15],w14
 142 00008a  00 40 A9 	bclr CORCON,#2
 143 00008c  00 00 06 	return 
 144              	.set ___PA___,0
 145              	.LFE3:
 146              	.size _parse_can_common_parse_error,.-_parse_can_common_parse_error
 147              	.align 2
 148              	.global _parse_can_common_TS
 149              	.type _parse_can_common_TS,@function
 150              	_parse_can_common_TS:
 151              	.LFB4:
  34:lib/can-ids/Devices/COMMON_CAN.c **** 
  35:lib/can-ids/Devices/COMMON_CAN.c **** void parse_can_common_TS(CANdata message, COMMON_MSG_TS *TS){
 152              	.loc 1 35 0
 153              	.set ___PA___,1
 154 00008e  0E 00 FA 	lnk #14
 155              	.LCFI4:
 156              	.loc 1 35 0
 157 000090  00 0F 78 	mov w0,[w14]
 158 000092  22 07 98 	mov w2,[w14+4]
 159 000094  66 07 98 	mov w6,[w14+12]
  36:lib/can-ids/Devices/COMMON_CAN.c ****     
  37:lib/can-ids/Devices/COMMON_CAN.c ****     TS->step = (TS_STEP) message.data[0];
 160              	.loc 1 37 0
 161 000096  2E 01 90 	mov [w14+4],w2
 162 000098  6E 00 90 	mov [w14+12],w0
 163              	.loc 1 35 0
 164 00009a  11 07 98 	mov w1,[w14+2]
 165 00009c  33 07 98 	mov w3,[w14+6]
 166 00009e  44 07 98 	mov w4,[w14+8]
 167 0000a0  55 07 98 	mov w5,[w14+10]
 168              	.loc 1 37 0
 169 0000a2  02 08 78 	mov w2,[w0]
  38:lib/can-ids/Devices/COMMON_CAN.c ****     return;
  39:lib/can-ids/Devices/COMMON_CAN.c **** }
 170              	.loc 1 39 0
 171 0000a4  8E 07 78 	mov w14,w15
 172 0000a6  4F 07 78 	mov [--w15],w14
 173 0000a8  00 40 A9 	bclr CORCON,#2
 174 0000aa  00 00 06 	return 
 175              	.set ___PA___,0
 176              	.LFE4:
 177              	.size _parse_can_common_TS,.-_parse_can_common_TS
 178              	.align 2
 179              	.global _parse_can_common_RTD_OFF
 180              	.type _parse_can_common_RTD_OFF,@function
 181              	_parse_can_common_RTD_OFF:
MPLAB XC16 ASSEMBLY Listing:   			page 5


 182              	.LFB5:
  40:lib/can-ids/Devices/COMMON_CAN.c **** 
  41:lib/can-ids/Devices/COMMON_CAN.c **** void parse_can_common_RTD_OFF(CANdata message, COMMON_MSG_RTD_OFF *rtd_off_msg){
 183              	.loc 1 41 0
 184              	.set ___PA___,1
 185 0000ac  0E 00 FA 	lnk #14
 186              	.LCFI5:
 187              	.loc 1 41 0
 188 0000ae  00 0F 78 	mov w0,[w14]
 189 0000b0  22 07 98 	mov w2,[w14+4]
 190 0000b2  66 07 98 	mov w6,[w14+12]
  42:lib/can-ids/Devices/COMMON_CAN.c ****     rtd_off_msg->step = (RTD_OFF) message.data[0];
 191              	.loc 1 42 0
 192 0000b4  2E 01 90 	mov [w14+4],w2
 193 0000b6  6E 00 90 	mov [w14+12],w0
 194              	.loc 1 41 0
 195 0000b8  11 07 98 	mov w1,[w14+2]
 196 0000ba  33 07 98 	mov w3,[w14+6]
 197 0000bc  44 07 98 	mov w4,[w14+8]
 198 0000be  55 07 98 	mov w5,[w14+10]
 199              	.loc 1 42 0
 200 0000c0  02 08 78 	mov w2,[w0]
  43:lib/can-ids/Devices/COMMON_CAN.c **** 
  44:lib/can-ids/Devices/COMMON_CAN.c ****     return;
  45:lib/can-ids/Devices/COMMON_CAN.c **** }
 201              	.loc 1 45 0
 202 0000c2  8E 07 78 	mov w14,w15
 203 0000c4  4F 07 78 	mov [--w15],w14
 204 0000c6  00 40 A9 	bclr CORCON,#2
 205 0000c8  00 00 06 	return 
 206              	.set ___PA___,0
 207              	.LFE5:
 208              	.size _parse_can_common_RTD_OFF,.-_parse_can_common_RTD_OFF
 209              	.align 2
 210              	.global _parse_can_power_inverters
 211              	.type _parse_can_power_inverters,@function
 212              	_parse_can_power_inverters:
 213              	.LFB6:
  46:lib/can-ids/Devices/COMMON_CAN.c **** 
  47:lib/can-ids/Devices/COMMON_CAN.c **** void parse_can_power_inverters(CANdata message, INVERTERS_POWER *inv_power){
 214              	.loc 1 47 0
 215              	.set ___PA___,1
 216 0000ca  0E 00 FA 	lnk #14
 217              	.LCFI6:
 218              	.loc 1 47 0
 219 0000cc  00 0F 78 	mov w0,[w14]
 220 0000ce  22 07 98 	mov w2,[w14+4]
 221 0000d0  66 07 98 	mov w6,[w14+12]
  48:lib/can-ids/Devices/COMMON_CAN.c ****     inv_power->inverters_power = message.data[0];
 222              	.loc 1 48 0
 223 0000d2  2E 01 90 	mov [w14+4],w2
 224 0000d4  6E 00 90 	mov [w14+12],w0
 225              	.loc 1 47 0
 226 0000d6  11 07 98 	mov w1,[w14+2]
 227 0000d8  33 07 98 	mov w3,[w14+6]
 228 0000da  44 07 98 	mov w4,[w14+8]
 229 0000dc  55 07 98 	mov w5,[w14+10]
MPLAB XC16 ASSEMBLY Listing:   			page 6


 230              	.loc 1 48 0
 231 0000de  02 08 78 	mov w2,[w0]
  49:lib/can-ids/Devices/COMMON_CAN.c **** }
 232              	.loc 1 49 0
 233 0000e0  8E 07 78 	mov w14,w15
 234 0000e2  4F 07 78 	mov [--w15],w14
 235 0000e4  00 40 A9 	bclr CORCON,#2
 236 0000e6  00 00 06 	return 
 237              	.set ___PA___,0
 238              	.LFE6:
 239              	.size _parse_can_power_inverters,.-_parse_can_power_inverters
 240              	.align 2
 241              	.global _make_reset_msg
 242              	.type _make_reset_msg,@function
 243              	_make_reset_msg:
 244              	.LFB7:
  50:lib/can-ids/Devices/COMMON_CAN.c **** 
  51:lib/can-ids/Devices/COMMON_CAN.c **** CANdata make_reset_msg(uint8_t dev_id, uint8_t RCON){
 245              	.loc 1 51 0
 246              	.set ___PA___,1
 247 0000e8  0E 00 FA 	lnk #14
 248              	.LCFI7:
 249              	.loc 1 51 0
 250 0000ea  00 02 78 	mov w0,w4
  52:lib/can-ids/Devices/COMMON_CAN.c **** 
  53:lib/can-ids/Devices/COMMON_CAN.c ****     CANdata message;
  54:lib/can-ids/Devices/COMMON_CAN.c **** 
  55:lib/can-ids/Devices/COMMON_CAN.c ****     message.dev_id = dev_id;
  56:lib/can-ids/Devices/COMMON_CAN.c ****     message.msg_id = MSG_ID_COMMON_RESET;
  57:lib/can-ids/Devices/COMMON_CAN.c **** 
  58:lib/can-ids/Devices/COMMON_CAN.c ****     message.dlc=2;
  59:lib/can-ids/Devices/COMMON_CAN.c **** 
  60:lib/can-ids/Devices/COMMON_CAN.c ****     message.data[0] = RCON;
  61:lib/can-ids/Devices/COMMON_CAN.c **** 
  62:lib/can-ids/Devices/COMMON_CAN.c ****     return message;
 251              	.loc 1 62 0
 252 0000ec  8E 01 78 	mov w14,w3
 253              	.loc 1 55 0
 254 0000ee  9E 02 78 	mov [w14],w5
 255              	.loc 1 51 0
 256 0000f0  41 4F 98 	mov.b w1,[w14+12]
 257              	.loc 1 55 0
 258 0000f2  01 FE 2F 	mov #-32,w1
 259 0000f4  4E 48 90 	mov.b [w14+12],w0
 260 0000f6  81 80 62 	and w5,w1,w1
 261 0000f8  7F 40 60 	and.b w0,#31,w0
 262 0000fa  00 80 FB 	ze w0,w0
 263 0000fc  7F 00 60 	and w0,#31,w0
 264 0000fe  01 00 70 	ior w0,w1,w0
 265 000100  00 0F 78 	mov w0,[w14]
 266              	.loc 1 58 0
 267 000102  1E 03 90 	mov [w14+2],w6
 268              	.loc 1 56 0
 269 000104  9E 03 78 	mov [w14],w7
 270              	.loc 1 62 0
 271 000106  04 00 78 	mov w4,w0
 272              	.loc 1 56 0
MPLAB XC16 ASSEMBLY Listing:   			page 7


 273 000108  F1 81 2F 	mov #-2017,w1
 274 00010a  05 3A 20 	mov #928,w5
 275 00010c  81 83 63 	and w7,w1,w7
 276              	.loc 1 58 0
 277 00010e  01 FF 2F 	mov #-16,w1
 278              	.loc 1 56 0
 279 000110  87 82 72 	ior w5,w7,w5
 280              	.loc 1 58 0
 281 000112  81 00 63 	and w6,w1,w1
 282              	.loc 1 56 0
 283 000114  05 0F 78 	mov w5,[w14]
 284              	.loc 1 58 0
 285 000116  01 10 A0 	bset w1,#1
 286 000118  11 07 98 	mov w1,[w14+2]
 287              	.loc 1 62 0
 288 00011a  33 18 78 	mov [w3++],[w0++]
 289 00011c  23 10 78 	mov [w3--],[w0--]
 290 00011e  E4 81 41 	add w3,#4,w3
 291 000120  64 00 40 	add w0,#4,w0
 292              	.loc 1 51 0
 293 000122  52 4F 98 	mov.b w2,[w14+13]
 294              	.loc 1 60 0
 295 000124  DE 48 90 	mov.b [w14+13],w1
 296 000126  81 80 FB 	ze w1,w1
 297 000128  21 07 98 	mov w1,[w14+4]
 298              	.loc 1 62 0
 299 00012a  33 18 78 	mov [w3++],[w0++]
 300 00012c  23 10 78 	mov [w3--],[w0--]
 301 00012e  E4 81 41 	add w3,#4,w3
 302 000130  64 00 40 	add w0,#4,w0
 303 000132  33 18 78 	mov [w3++],[w0++]
 304 000134  23 10 78 	mov [w3--],[w0--]
  63:lib/can-ids/Devices/COMMON_CAN.c **** }
 305              	.loc 1 63 0
 306 000136  04 00 78 	mov w4,w0
 307 000138  8E 07 78 	mov w14,w15
 308 00013a  4F 07 78 	mov [--w15],w14
 309 00013c  00 40 A9 	bclr CORCON,#2
 310 00013e  00 00 06 	return 
 311              	.set ___PA___,0
 312              	.LFE7:
 313              	.size _make_reset_msg,.-_make_reset_msg
 314              	.align 2
 315              	.global _make_parse_error_msg
 316              	.type _make_parse_error_msg,@function
 317              	_make_parse_error_msg:
 318              	.LFB8:
  64:lib/can-ids/Devices/COMMON_CAN.c **** 
  65:lib/can-ids/Devices/COMMON_CAN.c **** CANdata make_parse_error_msg(uint8_t dev_id, uint8_t wrong_id, uint8_t right_id){
 319              	.loc 1 65 0
 320              	.set ___PA___,1
 321 000140  10 00 FA 	lnk #16
 322              	.LCFI8:
 323 000142  88 1F 78 	mov w8,[w15++]
 324              	.LCFI9:
 325              	.loc 1 65 0
 326 000144  80 02 78 	mov w0,w5
MPLAB XC16 ASSEMBLY Listing:   			page 8


  66:lib/can-ids/Devices/COMMON_CAN.c **** 
  67:lib/can-ids/Devices/COMMON_CAN.c ****     CANdata message;
  68:lib/can-ids/Devices/COMMON_CAN.c **** 
  69:lib/can-ids/Devices/COMMON_CAN.c ****     message.dev_id = dev_id;
  70:lib/can-ids/Devices/COMMON_CAN.c ****     message.msg_id=MSG_ID_COMMON_PARSE_ERROR;
  71:lib/can-ids/Devices/COMMON_CAN.c **** 
  72:lib/can-ids/Devices/COMMON_CAN.c ****     message.dlc=2;
  73:lib/can-ids/Devices/COMMON_CAN.c **** 
  74:lib/can-ids/Devices/COMMON_CAN.c ****     message.data[0] = wrong_id | (right_id << 5);
  75:lib/can-ids/Devices/COMMON_CAN.c **** 
  76:lib/can-ids/Devices/COMMON_CAN.c ****     return message;
 327              	.loc 1 76 0
 328 000146  0E 02 78 	mov w14,w4
 329              	.loc 1 69 0
 330 000148  1E 03 78 	mov [w14],w6
 331              	.loc 1 65 0
 332 00014a  41 4F 98 	mov.b w1,[w14+12]
 333              	.loc 1 69 0
 334 00014c  01 FE 2F 	mov #-32,w1
 335 00014e  4E 48 90 	mov.b [w14+12],w0
 336 000150  81 00 63 	and w6,w1,w1
 337 000152  7F 40 60 	and.b w0,#31,w0
 338 000154  00 80 FB 	ze w0,w0
 339 000156  7F 00 60 	and w0,#31,w0
 340 000158  01 00 70 	ior w0,w1,w0
 341 00015a  00 0F 78 	mov w0,[w14]
 342              	.loc 1 72 0
 343 00015c  9E 03 90 	mov [w14+2],w7
 344              	.loc 1 70 0
 345 00015e  1E 04 78 	mov [w14],w8
 346              	.loc 1 76 0
 347 000160  05 00 78 	mov w5,w0
 348              	.loc 1 70 0
 349 000162  F1 81 2F 	mov #-2017,w1
 350 000164  06 3E 20 	mov #992,w6
 351 000166  01 04 64 	and w8,w1,w8
 352              	.loc 1 72 0
 353 000168  01 FF 2F 	mov #-16,w1
 354              	.loc 1 70 0
 355 00016a  08 03 73 	ior w6,w8,w6
 356              	.loc 1 72 0
 357 00016c  81 80 63 	and w7,w1,w1
 358              	.loc 1 70 0
 359 00016e  06 0F 78 	mov w6,[w14]
 360              	.loc 1 72 0
 361 000170  01 10 A0 	bset w1,#1
 362 000172  11 07 98 	mov w1,[w14+2]
 363              	.loc 1 76 0
 364 000174  34 18 78 	mov [w4++],[w0++]
 365 000176  24 10 78 	mov [w4--],[w0--]
 366 000178  64 02 42 	add w4,#4,w4
 367 00017a  64 00 40 	add w0,#4,w0
 368              	.loc 1 65 0
 369 00017c  52 4F 98 	mov.b w2,[w14+13]
 370 00017e  63 4F 98 	mov.b w3,[w14+14]
 371              	.loc 1 74 0
 372 000180  5E 49 90 	mov.b [w14+13],w2
MPLAB XC16 ASSEMBLY Listing:   			page 9


 373 000182  EE 48 90 	mov.b [w14+14],w1
 374 000184  02 81 FB 	ze w2,w2
 375 000186  81 80 FB 	ze w1,w1
 376 000188  C5 08 DD 	sl w1,#5,w1
 377 00018a  82 80 70 	ior w1,w2,w1
 378 00018c  21 07 98 	mov w1,[w14+4]
 379              	.loc 1 76 0
 380 00018e  34 18 78 	mov [w4++],[w0++]
 381 000190  24 10 78 	mov [w4--],[w0--]
 382 000192  64 02 42 	add w4,#4,w4
 383 000194  64 00 40 	add w0,#4,w0
 384 000196  34 18 78 	mov [w4++],[w0++]
 385 000198  24 10 78 	mov [w4--],[w0--]
  77:lib/can-ids/Devices/COMMON_CAN.c **** }
 386              	.loc 1 77 0
 387 00019a  05 00 78 	mov w5,w0
 388 00019c  4F 04 78 	mov [--w15],w8
 389 00019e  8E 07 78 	mov w14,w15
 390 0001a0  4F 07 78 	mov [--w15],w14
 391 0001a2  00 40 A9 	bclr CORCON,#2
 392 0001a4  00 00 06 	return 
 393              	.set ___PA___,0
 394              	.LFE8:
 395              	.size _make_parse_error_msg,.-_make_parse_error_msg
 396              	.align 2
 397              	.global _make_rtd_on_msg
 398              	.type _make_rtd_on_msg,@function
 399              	_make_rtd_on_msg:
 400              	.LFB9:
  78:lib/can-ids/Devices/COMMON_CAN.c **** 
  79:lib/can-ids/Devices/COMMON_CAN.c **** CANdata make_rtd_on_msg(uint8_t dev_id, RTD_ON step){
 401              	.loc 1 79 0
 402              	.set ___PA___,1
 403 0001a6  10 00 FA 	lnk #16
 404              	.LCFI10:
 405              	.loc 1 79 0
 406 0001a8  00 02 78 	mov w0,w4
  80:lib/can-ids/Devices/COMMON_CAN.c **** 
  81:lib/can-ids/Devices/COMMON_CAN.c ****     CANdata message;
  82:lib/can-ids/Devices/COMMON_CAN.c **** 
  83:lib/can-ids/Devices/COMMON_CAN.c ****     message.dev_id = dev_id;
  84:lib/can-ids/Devices/COMMON_CAN.c ****     message.msg_id = CMD_ID_COMMON_RTD_ON;
  85:lib/can-ids/Devices/COMMON_CAN.c **** 
  86:lib/can-ids/Devices/COMMON_CAN.c ****     message.dlc=2;
  87:lib/can-ids/Devices/COMMON_CAN.c **** 
  88:lib/can-ids/Devices/COMMON_CAN.c ****     message.data[0] = step;
  89:lib/can-ids/Devices/COMMON_CAN.c **** 
  90:lib/can-ids/Devices/COMMON_CAN.c ****     return message;
 407              	.loc 1 90 0
 408 0001aa  8E 01 78 	mov w14,w3
 409              	.loc 1 83 0
 410 0001ac  9E 02 78 	mov [w14],w5
 411              	.loc 1 79 0
 412 0001ae  41 4F 98 	mov.b w1,[w14+12]
 413              	.loc 1 83 0
 414 0001b0  01 FE 2F 	mov #-32,w1
 415 0001b2  4E 48 90 	mov.b [w14+12],w0
MPLAB XC16 ASSEMBLY Listing:   			page 10


 416 0001b4  81 80 62 	and w5,w1,w1
 417 0001b6  7F 40 60 	and.b w0,#31,w0
 418 0001b8  00 80 FB 	ze w0,w0
 419 0001ba  7F 00 60 	and w0,#31,w0
 420 0001bc  01 00 70 	ior w0,w1,w0
 421 0001be  00 0F 78 	mov w0,[w14]
 422              	.loc 1 86 0
 423 0001c0  1E 03 90 	mov [w14+2],w6
 424              	.loc 1 84 0
 425 0001c2  9E 03 78 	mov [w14],w7
 426              	.loc 1 90 0
 427 0001c4  04 00 78 	mov w4,w0
 428              	.loc 1 84 0
 429 0001c6  F5 81 2F 	mov #-2017,w5
 430              	.loc 1 86 0
 431 0001c8  01 FF 2F 	mov #-16,w1
 432              	.loc 1 84 0
 433 0001ca  85 82 63 	and w7,w5,w5
 434              	.loc 1 86 0
 435 0001cc  81 00 63 	and w6,w1,w1
 436              	.loc 1 84 0
 437 0001ce  05 90 A0 	bset w5,#9
 438              	.loc 1 86 0
 439 0001d0  01 10 A0 	bset w1,#1
 440              	.loc 1 84 0
 441 0001d2  05 0F 78 	mov w5,[w14]
 442              	.loc 1 86 0
 443 0001d4  11 07 98 	mov w1,[w14+2]
 444              	.loc 1 90 0
 445 0001d6  33 18 78 	mov [w3++],[w0++]
 446 0001d8  23 10 78 	mov [w3--],[w0--]
 447 0001da  E4 81 41 	add w3,#4,w3
 448 0001dc  64 00 40 	add w0,#4,w0
 449              	.loc 1 79 0
 450 0001de  72 07 98 	mov w2,[w14+14]
 451              	.loc 1 88 0
 452 0001e0  FE 00 90 	mov [w14+14],w1
 453 0001e2  21 07 98 	mov w1,[w14+4]
 454              	.loc 1 90 0
 455 0001e4  33 18 78 	mov [w3++],[w0++]
 456 0001e6  23 10 78 	mov [w3--],[w0--]
 457 0001e8  E4 81 41 	add w3,#4,w3
 458 0001ea  64 00 40 	add w0,#4,w0
 459 0001ec  33 18 78 	mov [w3++],[w0++]
 460 0001ee  23 10 78 	mov [w3--],[w0--]
  91:lib/can-ids/Devices/COMMON_CAN.c **** }
 461              	.loc 1 91 0
 462 0001f0  04 00 78 	mov w4,w0
 463 0001f2  8E 07 78 	mov w14,w15
 464 0001f4  4F 07 78 	mov [--w15],w14
 465 0001f6  00 40 A9 	bclr CORCON,#2
 466 0001f8  00 00 06 	return 
 467              	.set ___PA___,0
 468              	.LFE9:
 469              	.size _make_rtd_on_msg,.-_make_rtd_on_msg
 470              	.align 2
 471              	.global _make_rtd_off_msg
MPLAB XC16 ASSEMBLY Listing:   			page 11


 472              	.type _make_rtd_off_msg,@function
 473              	_make_rtd_off_msg:
 474              	.LFB10:
  92:lib/can-ids/Devices/COMMON_CAN.c **** 
  93:lib/can-ids/Devices/COMMON_CAN.c **** //reason only applicable to IIB
  94:lib/can-ids/Devices/COMMON_CAN.c **** CANdata make_rtd_off_msg(uint8_t dev_id, RTD_OFF step){
 475              	.loc 1 94 0
 476              	.set ___PA___,1
 477 0001fa  10 00 FA 	lnk #16
 478              	.LCFI11:
 479              	.loc 1 94 0
 480 0001fc  00 02 78 	mov w0,w4
  95:lib/can-ids/Devices/COMMON_CAN.c **** 
  96:lib/can-ids/Devices/COMMON_CAN.c ****     CANdata message;
  97:lib/can-ids/Devices/COMMON_CAN.c **** 
  98:lib/can-ids/Devices/COMMON_CAN.c ****     message.dev_id=dev_id;
  99:lib/can-ids/Devices/COMMON_CAN.c ****     message.msg_id = CMD_ID_COMMON_RTD_OFF;
 100:lib/can-ids/Devices/COMMON_CAN.c **** 
 101:lib/can-ids/Devices/COMMON_CAN.c ****     message.dlc=2;
 102:lib/can-ids/Devices/COMMON_CAN.c **** 
 103:lib/can-ids/Devices/COMMON_CAN.c ****     message.data[0] = step;
 104:lib/can-ids/Devices/COMMON_CAN.c **** 
 105:lib/can-ids/Devices/COMMON_CAN.c ****     return message;
 481              	.loc 1 105 0
 482 0001fe  8E 01 78 	mov w14,w3
 483              	.loc 1 98 0
 484 000200  9E 02 78 	mov [w14],w5
 485              	.loc 1 94 0
 486 000202  41 4F 98 	mov.b w1,[w14+12]
 487              	.loc 1 98 0
 488 000204  01 FE 2F 	mov #-32,w1
 489 000206  4E 48 90 	mov.b [w14+12],w0
 490 000208  81 80 62 	and w5,w1,w1
 491 00020a  7F 40 60 	and.b w0,#31,w0
 492 00020c  00 80 FB 	ze w0,w0
 493 00020e  7F 00 60 	and w0,#31,w0
 494 000210  01 00 70 	ior w0,w1,w0
 495 000212  00 0F 78 	mov w0,[w14]
 496              	.loc 1 101 0
 497 000214  1E 03 90 	mov [w14+2],w6
 498              	.loc 1 99 0
 499 000216  9E 03 78 	mov [w14],w7
 500              	.loc 1 105 0
 501 000218  04 00 78 	mov w4,w0
 502              	.loc 1 99 0
 503 00021a  F1 81 2F 	mov #-2017,w1
 504 00021c  05 22 20 	mov #544,w5
 505 00021e  81 83 63 	and w7,w1,w7
 506              	.loc 1 101 0
 507 000220  01 FF 2F 	mov #-16,w1
 508              	.loc 1 99 0
 509 000222  87 82 72 	ior w5,w7,w5
 510              	.loc 1 101 0
 511 000224  81 00 63 	and w6,w1,w1
 512              	.loc 1 99 0
 513 000226  05 0F 78 	mov w5,[w14]
 514              	.loc 1 101 0
MPLAB XC16 ASSEMBLY Listing:   			page 12


 515 000228  01 10 A0 	bset w1,#1
 516 00022a  11 07 98 	mov w1,[w14+2]
 517              	.loc 1 105 0
 518 00022c  33 18 78 	mov [w3++],[w0++]
 519 00022e  23 10 78 	mov [w3--],[w0--]
 520 000230  E4 81 41 	add w3,#4,w3
 521 000232  64 00 40 	add w0,#4,w0
 522              	.loc 1 94 0
 523 000234  72 07 98 	mov w2,[w14+14]
 524              	.loc 1 103 0
 525 000236  FE 00 90 	mov [w14+14],w1
 526 000238  21 07 98 	mov w1,[w14+4]
 527              	.loc 1 105 0
 528 00023a  33 18 78 	mov [w3++],[w0++]
 529 00023c  23 10 78 	mov [w3--],[w0--]
 530 00023e  E4 81 41 	add w3,#4,w3
 531 000240  64 00 40 	add w0,#4,w0
 532 000242  33 18 78 	mov [w3++],[w0++]
 533 000244  23 10 78 	mov [w3--],[w0--]
 106:lib/can-ids/Devices/COMMON_CAN.c **** }
 534              	.loc 1 106 0
 535 000246  04 00 78 	mov w4,w0
 536 000248  8E 07 78 	mov w14,w15
 537 00024a  4F 07 78 	mov [--w15],w14
 538 00024c  00 40 A9 	bclr CORCON,#2
 539 00024e  00 00 06 	return 
 540              	.set ___PA___,0
 541              	.LFE10:
 542              	.size _make_rtd_off_msg,.-_make_rtd_off_msg
 543              	.align 2
 544              	.global _make_ts_step_msg
 545              	.type _make_ts_step_msg,@function
 546              	_make_ts_step_msg:
 547              	.LFB11:
 107:lib/can-ids/Devices/COMMON_CAN.c **** 
 108:lib/can-ids/Devices/COMMON_CAN.c **** CANdata make_ts_step_msg(uint8_t dev_id, TS_STEP step){
 548              	.loc 1 108 0
 549              	.set ___PA___,1
 550 000250  10 00 FA 	lnk #16
 551              	.LCFI12:
 552              	.loc 1 108 0
 553 000252  00 02 78 	mov w0,w4
 109:lib/can-ids/Devices/COMMON_CAN.c **** 
 110:lib/can-ids/Devices/COMMON_CAN.c ****     CANdata message;
 111:lib/can-ids/Devices/COMMON_CAN.c **** 
 112:lib/can-ids/Devices/COMMON_CAN.c ****     message.dev_id = dev_id;
 113:lib/can-ids/Devices/COMMON_CAN.c ****     message.msg_id = CMD_ID_COMMON_TS;
 114:lib/can-ids/Devices/COMMON_CAN.c **** 
 115:lib/can-ids/Devices/COMMON_CAN.c ****     message.dlc=2;
 116:lib/can-ids/Devices/COMMON_CAN.c **** 
 117:lib/can-ids/Devices/COMMON_CAN.c ****     message.data[0] = step;
 118:lib/can-ids/Devices/COMMON_CAN.c **** 
 119:lib/can-ids/Devices/COMMON_CAN.c ****     return message;
 554              	.loc 1 119 0
 555 000254  8E 01 78 	mov w14,w3
 556              	.loc 1 112 0
 557 000256  9E 02 78 	mov [w14],w5
MPLAB XC16 ASSEMBLY Listing:   			page 13


 558              	.loc 1 108 0
 559 000258  41 4F 98 	mov.b w1,[w14+12]
 560              	.loc 1 112 0
 561 00025a  01 FE 2F 	mov #-32,w1
 562 00025c  4E 48 90 	mov.b [w14+12],w0
 563 00025e  81 80 62 	and w5,w1,w1
 564 000260  7F 40 60 	and.b w0,#31,w0
 565 000262  00 80 FB 	ze w0,w0
 566 000264  7F 00 60 	and w0,#31,w0
 567 000266  01 00 70 	ior w0,w1,w0
 568 000268  00 0F 78 	mov w0,[w14]
 569              	.loc 1 115 0
 570 00026a  1E 03 90 	mov [w14+2],w6
 571              	.loc 1 113 0
 572 00026c  9E 03 78 	mov [w14],w7
 573              	.loc 1 119 0
 574 00026e  04 00 78 	mov w4,w0
 575              	.loc 1 113 0
 576 000270  F1 81 2F 	mov #-2017,w1
 577 000272  05 24 20 	mov #576,w5
 578 000274  81 83 63 	and w7,w1,w7
 579              	.loc 1 115 0
 580 000276  01 FF 2F 	mov #-16,w1
 581              	.loc 1 113 0
 582 000278  87 82 72 	ior w5,w7,w5
 583              	.loc 1 115 0
 584 00027a  81 00 63 	and w6,w1,w1
 585              	.loc 1 113 0
 586 00027c  05 0F 78 	mov w5,[w14]
 587              	.loc 1 115 0
 588 00027e  01 10 A0 	bset w1,#1
 589 000280  11 07 98 	mov w1,[w14+2]
 590              	.loc 1 119 0
 591 000282  33 18 78 	mov [w3++],[w0++]
 592 000284  23 10 78 	mov [w3--],[w0--]
 593 000286  E4 81 41 	add w3,#4,w3
 594 000288  64 00 40 	add w0,#4,w0
 595              	.loc 1 108 0
 596 00028a  72 07 98 	mov w2,[w14+14]
 597              	.loc 1 117 0
 598 00028c  FE 00 90 	mov [w14+14],w1
 599 00028e  21 07 98 	mov w1,[w14+4]
 600              	.loc 1 119 0
 601 000290  33 18 78 	mov [w3++],[w0++]
 602 000292  23 10 78 	mov [w3--],[w0--]
 603 000294  E4 81 41 	add w3,#4,w3
 604 000296  64 00 40 	add w0,#4,w0
 605 000298  33 18 78 	mov [w3++],[w0++]
 606 00029a  23 10 78 	mov [w3--],[w0--]
 120:lib/can-ids/Devices/COMMON_CAN.c **** }
 607              	.loc 1 120 0
 608 00029c  04 00 78 	mov w4,w0
 609 00029e  8E 07 78 	mov w14,w15
 610 0002a0  4F 07 78 	mov [--w15],w14
 611 0002a2  00 40 A9 	bclr CORCON,#2
 612 0002a4  00 00 06 	return 
 613              	.set ___PA___,0
MPLAB XC16 ASSEMBLY Listing:   			page 14


 614              	.LFE11:
 615              	.size _make_ts_step_msg,.-_make_ts_step_msg
 616              	.section .debug_frame,info
 617                 	.Lframe0:
 618 0000 10 00 00 00 	.4byte .LECIE0-.LSCIE0
 619                 	.LSCIE0:
 620 0004 FF FF FF FF 	.4byte 0xffffffff
 621 0008 01          	.byte 0x1
 622 0009 00          	.byte 0
 623 000a 01          	.uleb128 0x1
 624 000b 02          	.sleb128 2
 625 000c 25          	.byte 0x25
 626 000d 12          	.byte 0x12
 627 000e 0F          	.uleb128 0xf
 628 000f 7E          	.sleb128 -2
 629 0010 09          	.byte 0x9
 630 0011 25          	.uleb128 0x25
 631 0012 0F          	.uleb128 0xf
 632 0013 00          	.align 4
 633                 	.LECIE0:
 634                 	.LSFDE0:
 635 0014 18 00 00 00 	.4byte .LEFDE0-.LASFDE0
 636                 	.LASFDE0:
 637 0018 00 00 00 00 	.4byte .Lframe0
 638 001c 00 00 00 00 	.4byte .LFB0
 639 0020 20 00 00 00 	.4byte .LFE0-.LFB0
 640 0024 04          	.byte 0x4
 641 0025 02 00 00 00 	.4byte .LCFI0-.LFB0
 642 0029 13          	.byte 0x13
 643 002a 7D          	.sleb128 -3
 644 002b 0D          	.byte 0xd
 645 002c 0E          	.uleb128 0xe
 646 002d 8E          	.byte 0x8e
 647 002e 02          	.uleb128 0x2
 648 002f 00          	.align 4
 649                 	.LEFDE0:
 650                 	.LSFDE2:
 651 0030 18 00 00 00 	.4byte .LEFDE2-.LASFDE2
 652                 	.LASFDE2:
 653 0034 00 00 00 00 	.4byte .Lframe0
 654 0038 00 00 00 00 	.4byte .LFB1
 655 003c 20 00 00 00 	.4byte .LFE1-.LFB1
 656 0040 04          	.byte 0x4
 657 0041 02 00 00 00 	.4byte .LCFI1-.LFB1
 658 0045 13          	.byte 0x13
 659 0046 7D          	.sleb128 -3
 660 0047 0D          	.byte 0xd
 661 0048 0E          	.uleb128 0xe
 662 0049 8E          	.byte 0x8e
 663 004a 02          	.uleb128 0x2
 664 004b 00          	.align 4
 665                 	.LEFDE2:
 666                 	.LSFDE4:
 667 004c 18 00 00 00 	.4byte .LEFDE4-.LASFDE4
 668                 	.LASFDE4:
 669 0050 00 00 00 00 	.4byte .Lframe0
 670 0054 00 00 00 00 	.4byte .LFB2
MPLAB XC16 ASSEMBLY Listing:   			page 15


 671 0058 20 00 00 00 	.4byte .LFE2-.LFB2
 672 005c 04          	.byte 0x4
 673 005d 02 00 00 00 	.4byte .LCFI2-.LFB2
 674 0061 13          	.byte 0x13
 675 0062 7D          	.sleb128 -3
 676 0063 0D          	.byte 0xd
 677 0064 0E          	.uleb128 0xe
 678 0065 8E          	.byte 0x8e
 679 0066 02          	.uleb128 0x2
 680 0067 00          	.align 4
 681                 	.LEFDE4:
 682                 	.LSFDE6:
 683 0068 18 00 00 00 	.4byte .LEFDE6-.LASFDE6
 684                 	.LASFDE6:
 685 006c 00 00 00 00 	.4byte .Lframe0
 686 0070 00 00 00 00 	.4byte .LFB3
 687 0074 2E 00 00 00 	.4byte .LFE3-.LFB3
 688 0078 04          	.byte 0x4
 689 0079 02 00 00 00 	.4byte .LCFI3-.LFB3
 690 007d 13          	.byte 0x13
 691 007e 7D          	.sleb128 -3
 692 007f 0D          	.byte 0xd
 693 0080 0E          	.uleb128 0xe
 694 0081 8E          	.byte 0x8e
 695 0082 02          	.uleb128 0x2
 696 0083 00          	.align 4
 697                 	.LEFDE6:
 698                 	.LSFDE8:
 699 0084 18 00 00 00 	.4byte .LEFDE8-.LASFDE8
 700                 	.LASFDE8:
 701 0088 00 00 00 00 	.4byte .Lframe0
 702 008c 00 00 00 00 	.4byte .LFB4
 703 0090 1E 00 00 00 	.4byte .LFE4-.LFB4
 704 0094 04          	.byte 0x4
 705 0095 02 00 00 00 	.4byte .LCFI4-.LFB4
 706 0099 13          	.byte 0x13
 707 009a 7D          	.sleb128 -3
 708 009b 0D          	.byte 0xd
 709 009c 0E          	.uleb128 0xe
 710 009d 8E          	.byte 0x8e
 711 009e 02          	.uleb128 0x2
 712 009f 00          	.align 4
 713                 	.LEFDE8:
 714                 	.LSFDE10:
 715 00a0 18 00 00 00 	.4byte .LEFDE10-.LASFDE10
 716                 	.LASFDE10:
 717 00a4 00 00 00 00 	.4byte .Lframe0
 718 00a8 00 00 00 00 	.4byte .LFB5
 719 00ac 1E 00 00 00 	.4byte .LFE5-.LFB5
 720 00b0 04          	.byte 0x4
 721 00b1 02 00 00 00 	.4byte .LCFI5-.LFB5
 722 00b5 13          	.byte 0x13
 723 00b6 7D          	.sleb128 -3
 724 00b7 0D          	.byte 0xd
 725 00b8 0E          	.uleb128 0xe
 726 00b9 8E          	.byte 0x8e
 727 00ba 02          	.uleb128 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 16


 728 00bb 00          	.align 4
 729                 	.LEFDE10:
 730                 	.LSFDE12:
 731 00bc 18 00 00 00 	.4byte .LEFDE12-.LASFDE12
 732                 	.LASFDE12:
 733 00c0 00 00 00 00 	.4byte .Lframe0
 734 00c4 00 00 00 00 	.4byte .LFB6
 735 00c8 1E 00 00 00 	.4byte .LFE6-.LFB6
 736 00cc 04          	.byte 0x4
 737 00cd 02 00 00 00 	.4byte .LCFI6-.LFB6
 738 00d1 13          	.byte 0x13
 739 00d2 7D          	.sleb128 -3
 740 00d3 0D          	.byte 0xd
 741 00d4 0E          	.uleb128 0xe
 742 00d5 8E          	.byte 0x8e
 743 00d6 02          	.uleb128 0x2
 744 00d7 00          	.align 4
 745                 	.LEFDE12:
 746                 	.LSFDE14:
 747 00d8 18 00 00 00 	.4byte .LEFDE14-.LASFDE14
 748                 	.LASFDE14:
 749 00dc 00 00 00 00 	.4byte .Lframe0
 750 00e0 00 00 00 00 	.4byte .LFB7
 751 00e4 58 00 00 00 	.4byte .LFE7-.LFB7
 752 00e8 04          	.byte 0x4
 753 00e9 02 00 00 00 	.4byte .LCFI7-.LFB7
 754 00ed 13          	.byte 0x13
 755 00ee 7D          	.sleb128 -3
 756 00ef 0D          	.byte 0xd
 757 00f0 0E          	.uleb128 0xe
 758 00f1 8E          	.byte 0x8e
 759 00f2 02          	.uleb128 0x2
 760 00f3 00          	.align 4
 761                 	.LEFDE14:
 762                 	.LSFDE16:
 763 00f4 1E 00 00 00 	.4byte .LEFDE16-.LASFDE16
 764                 	.LASFDE16:
 765 00f8 00 00 00 00 	.4byte .Lframe0
 766 00fc 00 00 00 00 	.4byte .LFB8
 767 0100 66 00 00 00 	.4byte .LFE8-.LFB8
 768 0104 04          	.byte 0x4
 769 0105 02 00 00 00 	.4byte .LCFI8-.LFB8
 770 0109 13          	.byte 0x13
 771 010a 7D          	.sleb128 -3
 772 010b 0D          	.byte 0xd
 773 010c 0E          	.uleb128 0xe
 774 010d 8E          	.byte 0x8e
 775 010e 02          	.uleb128 0x2
 776 010f 04          	.byte 0x4
 777 0110 02 00 00 00 	.4byte .LCFI9-.LCFI8
 778 0114 88          	.byte 0x88
 779 0115 0B          	.uleb128 0xb
 780                 	.align 4
 781                 	.LEFDE16:
 782                 	.LSFDE18:
 783 0116 18 00 00 00 	.4byte .LEFDE18-.LASFDE18
 784                 	.LASFDE18:
MPLAB XC16 ASSEMBLY Listing:   			page 17


 785 011a 00 00 00 00 	.4byte .Lframe0
 786 011e 00 00 00 00 	.4byte .LFB9
 787 0122 54 00 00 00 	.4byte .LFE9-.LFB9
 788 0126 04          	.byte 0x4
 789 0127 02 00 00 00 	.4byte .LCFI10-.LFB9
 790 012b 13          	.byte 0x13
 791 012c 7D          	.sleb128 -3
 792 012d 0D          	.byte 0xd
 793 012e 0E          	.uleb128 0xe
 794 012f 8E          	.byte 0x8e
 795 0130 02          	.uleb128 0x2
 796 0131 00          	.align 4
 797                 	.LEFDE18:
 798                 	.LSFDE20:
 799 0132 18 00 00 00 	.4byte .LEFDE20-.LASFDE20
 800                 	.LASFDE20:
 801 0136 00 00 00 00 	.4byte .Lframe0
 802 013a 00 00 00 00 	.4byte .LFB10
 803 013e 56 00 00 00 	.4byte .LFE10-.LFB10
 804 0142 04          	.byte 0x4
 805 0143 02 00 00 00 	.4byte .LCFI11-.LFB10
 806 0147 13          	.byte 0x13
 807 0148 7D          	.sleb128 -3
 808 0149 0D          	.byte 0xd
 809 014a 0E          	.uleb128 0xe
 810 014b 8E          	.byte 0x8e
 811 014c 02          	.uleb128 0x2
 812 014d 00          	.align 4
 813                 	.LEFDE20:
 814                 	.LSFDE22:
 815 014e 18 00 00 00 	.4byte .LEFDE22-.LASFDE22
 816                 	.LASFDE22:
 817 0152 00 00 00 00 	.4byte .Lframe0
 818 0156 00 00 00 00 	.4byte .LFB11
 819 015a 56 00 00 00 	.4byte .LFE11-.LFB11
 820 015e 04          	.byte 0x4
 821 015f 02 00 00 00 	.4byte .LCFI12-.LFB11
 822 0163 13          	.byte 0x13
 823 0164 7D          	.sleb128 -3
 824 0165 0D          	.byte 0xd
 825 0166 0E          	.uleb128 0xe
 826 0167 8E          	.byte 0x8e
 827 0168 02          	.uleb128 0x2
 828 0169 00          	.align 4
 829                 	.LEFDE22:
 830                 	.section .text,code
 831              	.Letext0:
 832              	.file 2 "/opt/microchip/xc16/v1.36/bin/bin/../../include/lega-c/stdint.h"
 833              	.file 3 "lib/can-ids/CAN_IDs.h"
 834              	.file 4 "lib/can-ids/Devices/COMMON_CAN.h"
 835              	.section .debug_info,info
 836 0000 82 08 00 00 	.4byte 0x882
 837 0004 02 00       	.2byte 0x2
 838 0006 00 00 00 00 	.4byte .Ldebug_abbrev0
 839 000a 04          	.byte 0x4
 840 000b 01          	.uleb128 0x1
 841 000c 47 4E 55 20 	.asciz "GNU C 4.5.1 (XC16, Microchip v1.36) (A) Build date: Dec  5 2018"
MPLAB XC16 ASSEMBLY Listing:   			page 18


 841      43 20 34 2E 
 841      35 2E 31 20 
 841      28 58 43 31 
 841      36 2C 20 4D 
 841      69 63 72 6F 
 841      63 68 69 70 
 841      20 76 31 2E 
 841      33 36 29 20 
 842 004c 01          	.byte 0x1
 843 004d 6C 69 62 2F 	.asciz "lib/can-ids/Devices/COMMON_CAN.c"
 843      63 61 6E 2D 
 843      69 64 73 2F 
 843      44 65 76 69 
 843      63 65 73 2F 
 843      43 4F 4D 4D 
 843      4F 4E 5F 43 
 843      41 4E 2E 63 
 843      00 
 844 006e 2F 68 6F 6D 	.asciz "/home/user/Documents/FST/Programming/project_sub-zero"
 844      65 2F 75 73 
 844      65 72 2F 44 
 844      6F 63 75 6D 
 844      65 6E 74 73 
 844      2F 46 53 54 
 844      2F 50 72 6F 
 844      67 72 61 6D 
 844      6D 69 6E 67 
 845 00a4 00 00 00 00 	.4byte .Ltext0
 846 00a8 00 00 00 00 	.4byte .Letext0
 847 00ac 00 00 00 00 	.4byte .Ldebug_line0
 848 00b0 02          	.uleb128 0x2
 849 00b1 01          	.byte 0x1
 850 00b2 06          	.byte 0x6
 851 00b3 73 69 67 6E 	.asciz "signed char"
 851      65 64 20 63 
 851      68 61 72 00 
 852 00bf 02          	.uleb128 0x2
 853 00c0 02          	.byte 0x2
 854 00c1 05          	.byte 0x5
 855 00c2 69 6E 74 00 	.asciz "int"
 856 00c6 02          	.uleb128 0x2
 857 00c7 04          	.byte 0x4
 858 00c8 05          	.byte 0x5
 859 00c9 6C 6F 6E 67 	.asciz "long int"
 859      20 69 6E 74 
 859      00 
 860 00d2 02          	.uleb128 0x2
 861 00d3 08          	.byte 0x8
 862 00d4 05          	.byte 0x5
 863 00d5 6C 6F 6E 67 	.asciz "long long int"
 863      20 6C 6F 6E 
 863      67 20 69 6E 
 863      74 00 
 864 00e3 03          	.uleb128 0x3
 865 00e4 75 69 6E 74 	.asciz "uint8_t"
 865      38 5F 74 00 
 866 00ec 02          	.byte 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 19


 867 00ed 2B          	.byte 0x2b
 868 00ee F2 00 00 00 	.4byte 0xf2
 869 00f2 02          	.uleb128 0x2
 870 00f3 01          	.byte 0x1
 871 00f4 08          	.byte 0x8
 872 00f5 75 6E 73 69 	.asciz "unsigned char"
 872      67 6E 65 64 
 872      20 63 68 61 
 872      72 00 
 873 0103 03          	.uleb128 0x3
 874 0104 75 69 6E 74 	.asciz "uint16_t"
 874      31 36 5F 74 
 874      00 
 875 010d 02          	.byte 0x2
 876 010e 31          	.byte 0x31
 877 010f 13 01 00 00 	.4byte 0x113
 878 0113 02          	.uleb128 0x2
 879 0114 02          	.byte 0x2
 880 0115 07          	.byte 0x7
 881 0116 75 6E 73 69 	.asciz "unsigned int"
 881      67 6E 65 64 
 881      20 69 6E 74 
 881      00 
 882 0123 02          	.uleb128 0x2
 883 0124 04          	.byte 0x4
 884 0125 07          	.byte 0x7
 885 0126 6C 6F 6E 67 	.asciz "long unsigned int"
 885      20 75 6E 73 
 885      69 67 6E 65 
 885      64 20 69 6E 
 885      74 00 
 886 0138 02          	.uleb128 0x2
 887 0139 08          	.byte 0x8
 888 013a 07          	.byte 0x7
 889 013b 6C 6F 6E 67 	.asciz "long long unsigned int"
 889      20 6C 6F 6E 
 889      67 20 75 6E 
 889      73 69 67 6E 
 889      65 64 20 69 
 889      6E 74 00 
 890 0152 04          	.uleb128 0x4
 891 0153 02          	.byte 0x2
 892 0154 03          	.byte 0x3
 893 0155 12          	.byte 0x12
 894 0156 80 01 00 00 	.4byte 0x180
 895 015a 05          	.uleb128 0x5
 896 015b 00 00 00 00 	.4byte .LASF0
 897 015f 03          	.byte 0x3
 898 0160 13          	.byte 0x13
 899 0161 03 01 00 00 	.4byte 0x103
 900 0165 02          	.byte 0x2
 901 0166 05          	.byte 0x5
 902 0167 0B          	.byte 0xb
 903 0168 02          	.byte 0x2
 904 0169 23          	.byte 0x23
 905 016a 00          	.uleb128 0x0
 906 016b 06          	.uleb128 0x6
MPLAB XC16 ASSEMBLY Listing:   			page 20


 907 016c 6D 73 67 5F 	.asciz "msg_id"
 907      69 64 00 
 908 0173 03          	.byte 0x3
 909 0174 16          	.byte 0x16
 910 0175 03 01 00 00 	.4byte 0x103
 911 0179 02          	.byte 0x2
 912 017a 06          	.byte 0x6
 913 017b 05          	.byte 0x5
 914 017c 02          	.byte 0x2
 915 017d 23          	.byte 0x23
 916 017e 00          	.uleb128 0x0
 917 017f 00          	.byte 0x0
 918 0180 07          	.uleb128 0x7
 919 0181 02          	.byte 0x2
 920 0182 03          	.byte 0x3
 921 0183 11          	.byte 0x11
 922 0184 99 01 00 00 	.4byte 0x199
 923 0188 08          	.uleb128 0x8
 924 0189 52 01 00 00 	.4byte 0x152
 925 018d 09          	.uleb128 0x9
 926 018e 73 69 64 00 	.asciz "sid"
 927 0192 03          	.byte 0x3
 928 0193 18          	.byte 0x18
 929 0194 03 01 00 00 	.4byte 0x103
 930 0198 00          	.byte 0x0
 931 0199 04          	.uleb128 0x4
 932 019a 0C          	.byte 0xc
 933 019b 03          	.byte 0x3
 934 019c 10          	.byte 0x10
 935 019d CA 01 00 00 	.4byte 0x1ca
 936 01a1 0A          	.uleb128 0xa
 937 01a2 80 01 00 00 	.4byte 0x180
 938 01a6 02          	.byte 0x2
 939 01a7 23          	.byte 0x23
 940 01a8 00          	.uleb128 0x0
 941 01a9 06          	.uleb128 0x6
 942 01aa 64 6C 63 00 	.asciz "dlc"
 943 01ae 03          	.byte 0x3
 944 01af 1A          	.byte 0x1a
 945 01b0 03 01 00 00 	.4byte 0x103
 946 01b4 02          	.byte 0x2
 947 01b5 04          	.byte 0x4
 948 01b6 0C          	.byte 0xc
 949 01b7 02          	.byte 0x2
 950 01b8 23          	.byte 0x23
 951 01b9 02          	.uleb128 0x2
 952 01ba 0B          	.uleb128 0xb
 953 01bb 64 61 74 61 	.asciz "data"
 953      00 
 954 01c0 03          	.byte 0x3
 955 01c1 1B          	.byte 0x1b
 956 01c2 CA 01 00 00 	.4byte 0x1ca
 957 01c6 02          	.byte 0x2
 958 01c7 23          	.byte 0x23
 959 01c8 04          	.uleb128 0x4
 960 01c9 00          	.byte 0x0
 961 01ca 0C          	.uleb128 0xc
MPLAB XC16 ASSEMBLY Listing:   			page 21


 962 01cb 03 01 00 00 	.4byte 0x103
 963 01cf DA 01 00 00 	.4byte 0x1da
 964 01d3 0D          	.uleb128 0xd
 965 01d4 13 01 00 00 	.4byte 0x113
 966 01d8 03          	.byte 0x3
 967 01d9 00          	.byte 0x0
 968 01da 03          	.uleb128 0x3
 969 01db 43 41 4E 64 	.asciz "CANdata"
 969      61 74 61 00 
 970 01e3 03          	.byte 0x3
 971 01e4 1C          	.byte 0x1c
 972 01e5 99 01 00 00 	.4byte 0x199
 973 01e9 0E          	.uleb128 0xe
 974 01ea 02          	.byte 0x2
 975 01eb 04          	.byte 0x4
 976 01ec 0C          	.byte 0xc
 977 01ed 86 02 00 00 	.4byte 0x286
 978 01f1 0F          	.uleb128 0xf
 979 01f2 52 54 44 5F 	.asciz "RTD_STEP_DASH_BUTTON"
 979      53 54 45 50 
 979      5F 44 41 53 
 979      48 5F 42 55 
 979      54 54 4F 4E 
 979      00 
 980 0207 00          	.sleb128 0
 981 0208 0F          	.uleb128 0xf
 982 0209 52 54 44 5F 	.asciz "RTD_STEP_TE_OK"
 982      53 54 45 50 
 982      5F 54 45 5F 
 982      4F 4B 00 
 983 0218 01          	.sleb128 1
 984 0219 0F          	.uleb128 0xf
 985 021a 52 54 44 5F 	.asciz "RTD_STEP_TE_NOK"
 985      53 54 45 50 
 985      5F 54 45 5F 
 985      4E 4F 4B 00 
 986 022a 09          	.sleb128 9
 987 022b 0F          	.uleb128 0xf
 988 022c 52 54 44 5F 	.asciz "RTD_STEP_INV_OK"
 988      53 54 45 50 
 988      5F 49 4E 56 
 988      5F 4F 4B 00 
 989 023c 02          	.sleb128 2
 990 023d 0F          	.uleb128 0xf
 991 023e 52 54 44 5F 	.asciz "RTD_STEP_INV_NOK"
 991      53 54 45 50 
 991      5F 49 4E 56 
 991      5F 4E 4F 4B 
 991      00 
 992 024f 0A          	.sleb128 10
 993 0250 0F          	.uleb128 0xf
 994 0251 52 54 44 5F 	.asciz "RTD_STEP_DCU"
 994      53 54 45 50 
 994      5F 44 43 55 
 994      00 
 995 025e 03          	.sleb128 3
 996 025f 0F          	.uleb128 0xf
MPLAB XC16 ASSEMBLY Listing:   			page 22


 997 0260 52 54 44 5F 	.asciz "RTD_STEP_INV_GO"
 997      53 54 45 50 
 997      5F 49 4E 56 
 997      5F 47 4F 00 
 998 0270 04          	.sleb128 4
 999 0271 0F          	.uleb128 0xf
 1000 0272 52 54 44 5F 	.asciz "RTD_STEP_DASH_LED"
 1000      53 54 45 50 
 1000      5F 44 41 53 
 1000      48 5F 4C 45 
 1000      44 00 
 1001 0284 05          	.sleb128 5
 1002 0285 00          	.byte 0x0
 1003 0286 03          	.uleb128 0x3
 1004 0287 52 54 44 5F 	.asciz "RTD_ON"
 1004      4F 4E 00 
 1005 028e 04          	.byte 0x4
 1006 028f 15          	.byte 0x15
 1007 0290 E9 01 00 00 	.4byte 0x1e9
 1008 0294 0E          	.uleb128 0xe
 1009 0295 02          	.byte 0x2
 1010 0296 04          	.byte 0x4
 1011 0297 17          	.byte 0x17
 1012 0298 CC 02 00 00 	.4byte 0x2cc
 1013 029c 0F          	.uleb128 0xf
 1014 029d 52 54 44 5F 	.asciz "RTD_DASH_OFF"
 1014      44 41 53 48 
 1014      5F 4F 46 46 
 1014      00 
 1015 02aa 00          	.sleb128 0
 1016 02ab 0F          	.uleb128 0xf
 1017 02ac 52 54 44 5F 	.asciz "RTD_INV_CONFIRM"
 1017      49 4E 56 5F 
 1017      43 4F 4E 46 
 1017      49 52 4D 00 
 1018 02bc 01          	.sleb128 1
 1019 02bd 0F          	.uleb128 0xf
 1020 02be 52 54 44 5F 	.asciz "RTD_INV_OFF"
 1020      49 4E 56 5F 
 1020      4F 46 46 00 
 1021 02ca 02          	.sleb128 2
 1022 02cb 00          	.byte 0x0
 1023 02cc 03          	.uleb128 0x3
 1024 02cd 52 54 44 5F 	.asciz "RTD_OFF"
 1024      4F 46 46 00 
 1025 02d5 04          	.byte 0x4
 1026 02d6 1B          	.byte 0x1b
 1027 02d7 94 02 00 00 	.4byte 0x294
 1028 02db 0E          	.uleb128 0xe
 1029 02dc 02          	.byte 0x2
 1030 02dd 04          	.byte 0x4
 1031 02de 1D          	.byte 0x1d
 1032 02df 14 03 00 00 	.4byte 0x314
 1033 02e3 0F          	.uleb128 0xf
 1034 02e4 54 53 5F 44 	.asciz "TS_DASH_BUTTON"
 1034      41 53 48 5F 
 1034      42 55 54 54 
MPLAB XC16 ASSEMBLY Listing:   			page 23


 1034      4F 4E 00 
 1035 02f3 00          	.sleb128 0
 1036 02f4 0F          	.uleb128 0xf
 1037 02f5 54 53 5F 4D 	.asciz "TS_MASTER_ON"
 1037      41 53 54 45 
 1037      52 5F 4F 4E 
 1037      00 
 1038 0302 01          	.sleb128 1
 1039 0303 0F          	.uleb128 0xf
 1040 0304 54 53 5F 4D 	.asciz "TS_MASTER_OFF"
 1040      41 53 54 45 
 1040      52 5F 4F 46 
 1040      46 00 
 1041 0312 02          	.sleb128 2
 1042 0313 00          	.byte 0x0
 1043 0314 03          	.uleb128 0x3
 1044 0315 54 53 5F 53 	.asciz "TS_STEP"
 1044      54 45 50 00 
 1045 031d 04          	.byte 0x4
 1046 031e 21          	.byte 0x21
 1047 031f DB 02 00 00 	.4byte 0x2db
 1048 0323 04          	.uleb128 0x4
 1049 0324 01          	.byte 0x1
 1050 0325 04          	.byte 0x4
 1051 0326 26          	.byte 0x26
 1052 0327 3B 03 00 00 	.4byte 0x33b
 1053 032b 0B          	.uleb128 0xb
 1054 032c 52 43 4F 4E 	.asciz "RCON"
 1054      00 
 1055 0331 04          	.byte 0x4
 1056 0332 27          	.byte 0x27
 1057 0333 E3 00 00 00 	.4byte 0xe3
 1058 0337 02          	.byte 0x2
 1059 0338 23          	.byte 0x23
 1060 0339 00          	.uleb128 0x0
 1061 033a 00          	.byte 0x0
 1062 033b 03          	.uleb128 0x3
 1063 033c 43 4F 4D 4D 	.asciz "COMMON_MSG_RESET"
 1063      4F 4E 5F 4D 
 1063      53 47 5F 52 
 1063      45 53 45 54 
 1063      00 
 1064 034d 04          	.byte 0x4
 1065 034e 29          	.byte 0x29
 1066 034f 23 03 00 00 	.4byte 0x323
 1067 0353 04          	.uleb128 0x4
 1068 0354 01          	.byte 0x1
 1069 0355 04          	.byte 0x4
 1070 0356 2B          	.byte 0x2b
 1071 0357 6B 03 00 00 	.4byte 0x36b
 1072 035b 0B          	.uleb128 0xb
 1073 035c 74 79 70 65 	.asciz "type"
 1073      00 
 1074 0361 04          	.byte 0x4
 1075 0362 2C          	.byte 0x2c
 1076 0363 E3 00 00 00 	.4byte 0xe3
 1077 0367 02          	.byte 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 24


 1078 0368 23          	.byte 0x23
 1079 0369 00          	.uleb128 0x0
 1080 036a 00          	.byte 0x0
 1081 036b 03          	.uleb128 0x3
 1082 036c 43 4F 4D 4D 	.asciz "COMMON_MSG_TRAP"
 1082      4F 4E 5F 4D 
 1082      53 47 5F 54 
 1082      52 41 50 00 
 1083 037c 04          	.byte 0x4
 1084 037d 2D          	.byte 0x2d
 1085 037e 53 03 00 00 	.4byte 0x353
 1086 0382 04          	.uleb128 0x4
 1087 0383 02          	.byte 0x2
 1088 0384 04          	.byte 0x4
 1089 0385 30          	.byte 0x30
 1090 0386 99 03 00 00 	.4byte 0x399
 1091 038a 10          	.uleb128 0x10
 1092 038b 00 00 00 00 	.4byte .LASF1
 1093 038f 04          	.byte 0x4
 1094 0390 31          	.byte 0x31
 1095 0391 86 02 00 00 	.4byte 0x286
 1096 0395 02          	.byte 0x2
 1097 0396 23          	.byte 0x23
 1098 0397 00          	.uleb128 0x0
 1099 0398 00          	.byte 0x0
 1100 0399 03          	.uleb128 0x3
 1101 039a 43 4F 4D 4D 	.asciz "COMMON_MSG_RTD_ON"
 1101      4F 4E 5F 4D 
 1101      53 47 5F 52 
 1101      54 44 5F 4F 
 1101      4E 00 
 1102 03ac 04          	.byte 0x4
 1103 03ad 33          	.byte 0x33
 1104 03ae 82 03 00 00 	.4byte 0x382
 1105 03b2 04          	.uleb128 0x4
 1106 03b3 02          	.byte 0x2
 1107 03b4 04          	.byte 0x4
 1108 03b5 35          	.byte 0x35
 1109 03b6 E9 03 00 00 	.4byte 0x3e9
 1110 03ba 0B          	.uleb128 0xb
 1111 03bb 77 72 6F 6E 	.asciz "wrong_dev_id"
 1111      67 5F 64 65 
 1111      76 5F 69 64 
 1111      00 
 1112 03c8 04          	.byte 0x4
 1113 03c9 37          	.byte 0x37
 1114 03ca E3 00 00 00 	.4byte 0xe3
 1115 03ce 02          	.byte 0x2
 1116 03cf 23          	.byte 0x23
 1117 03d0 00          	.uleb128 0x0
 1118 03d1 0B          	.uleb128 0xb
 1119 03d2 72 69 67 68 	.asciz "right_dev_id"
 1119      74 5F 64 65 
 1119      76 5F 69 64 
 1119      00 
 1120 03df 04          	.byte 0x4
 1121 03e0 38          	.byte 0x38
MPLAB XC16 ASSEMBLY Listing:   			page 25


 1122 03e1 E3 00 00 00 	.4byte 0xe3
 1123 03e5 02          	.byte 0x2
 1124 03e6 23          	.byte 0x23
 1125 03e7 01          	.uleb128 0x1
 1126 03e8 00          	.byte 0x0
 1127 03e9 03          	.uleb128 0x3
 1128 03ea 43 4F 4D 4D 	.asciz "COMMON_MSG_PARSE_ERROR"
 1128      4F 4E 5F 4D 
 1128      53 47 5F 50 
 1128      41 52 53 45 
 1128      5F 45 52 52 
 1128      4F 52 00 
 1129 0401 04          	.byte 0x4
 1130 0402 3A          	.byte 0x3a
 1131 0403 B2 03 00 00 	.4byte 0x3b2
 1132 0407 04          	.uleb128 0x4
 1133 0408 02          	.byte 0x2
 1134 0409 04          	.byte 0x4
 1135 040a 3C          	.byte 0x3c
 1136 040b 1E 04 00 00 	.4byte 0x41e
 1137 040f 10          	.uleb128 0x10
 1138 0410 00 00 00 00 	.4byte .LASF1
 1139 0414 04          	.byte 0x4
 1140 0415 3D          	.byte 0x3d
 1141 0416 CC 02 00 00 	.4byte 0x2cc
 1142 041a 02          	.byte 0x2
 1143 041b 23          	.byte 0x23
 1144 041c 00          	.uleb128 0x0
 1145 041d 00          	.byte 0x0
 1146 041e 03          	.uleb128 0x3
 1147 041f 43 4F 4D 4D 	.asciz "COMMON_MSG_RTD_OFF"
 1147      4F 4E 5F 4D 
 1147      53 47 5F 52 
 1147      54 44 5F 4F 
 1147      46 46 00 
 1148 0432 04          	.byte 0x4
 1149 0433 3F          	.byte 0x3f
 1150 0434 07 04 00 00 	.4byte 0x407
 1151 0438 04          	.uleb128 0x4
 1152 0439 02          	.byte 0x2
 1153 043a 04          	.byte 0x4
 1154 043b 41          	.byte 0x41
 1155 043c 4F 04 00 00 	.4byte 0x44f
 1156 0440 10          	.uleb128 0x10
 1157 0441 00 00 00 00 	.4byte .LASF1
 1158 0445 04          	.byte 0x4
 1159 0446 42          	.byte 0x42
 1160 0447 14 03 00 00 	.4byte 0x314
 1161 044b 02          	.byte 0x2
 1162 044c 23          	.byte 0x23
 1163 044d 00          	.uleb128 0x0
 1164 044e 00          	.byte 0x0
 1165 044f 03          	.uleb128 0x3
 1166 0450 43 4F 4D 4D 	.asciz "COMMON_MSG_TS"
 1166      4F 4E 5F 4D 
 1166      53 47 5F 54 
 1166      53 00 
MPLAB XC16 ASSEMBLY Listing:   			page 26


 1167 045e 04          	.byte 0x4
 1168 045f 43          	.byte 0x43
 1169 0460 38 04 00 00 	.4byte 0x438
 1170 0464 04          	.uleb128 0x4
 1171 0465 02          	.byte 0x2
 1172 0466 04          	.byte 0x4
 1173 0467 4A          	.byte 0x4a
 1174 0468 87 04 00 00 	.4byte 0x487
 1175 046c 0B          	.uleb128 0xb
 1176 046d 69 6E 76 65 	.asciz "inverters_power"
 1176      72 74 65 72 
 1176      73 5F 70 6F 
 1176      77 65 72 00 
 1177 047d 04          	.byte 0x4
 1178 047e 4B          	.byte 0x4b
 1179 047f 03 01 00 00 	.4byte 0x103
 1180 0483 02          	.byte 0x2
 1181 0484 23          	.byte 0x23
 1182 0485 00          	.uleb128 0x0
 1183 0486 00          	.byte 0x0
 1184 0487 03          	.uleb128 0x3
 1185 0488 49 4E 56 45 	.asciz "INVERTERS_POWER"
 1185      52 54 45 52 
 1185      53 5F 50 4F 
 1185      57 45 52 00 
 1186 0498 04          	.byte 0x4
 1187 0499 4C          	.byte 0x4c
 1188 049a 64 04 00 00 	.4byte 0x464
 1189 049e 11          	.uleb128 0x11
 1190 049f 01          	.byte 0x1
 1191 04a0 70 61 72 73 	.asciz "parse_can_common_RTD"
 1191      65 5F 63 61 
 1191      6E 5F 63 6F 
 1191      6D 6D 6F 6E 
 1191      5F 52 54 44 
 1191      00 
 1192 04b5 01          	.byte 0x1
 1193 04b6 06          	.byte 0x6
 1194 04b7 01          	.byte 0x1
 1195 04b8 00 00 00 00 	.4byte .LFB0
 1196 04bc 00 00 00 00 	.4byte .LFE0
 1197 04c0 01          	.byte 0x1
 1198 04c1 5E          	.byte 0x5e
 1199 04c2 E3 04 00 00 	.4byte 0x4e3
 1200 04c6 12          	.uleb128 0x12
 1201 04c7 00 00 00 00 	.4byte .LASF2
 1202 04cb 01          	.byte 0x1
 1203 04cc 06          	.byte 0x6
 1204 04cd DA 01 00 00 	.4byte 0x1da
 1205 04d1 02          	.byte 0x2
 1206 04d2 7E          	.byte 0x7e
 1207 04d3 00          	.sleb128 0
 1208 04d4 13          	.uleb128 0x13
 1209 04d5 52 54 44 00 	.asciz "RTD"
 1210 04d9 01          	.byte 0x1
 1211 04da 06          	.byte 0x6
 1212 04db E3 04 00 00 	.4byte 0x4e3
MPLAB XC16 ASSEMBLY Listing:   			page 27


 1213 04df 02          	.byte 0x2
 1214 04e0 7E          	.byte 0x7e
 1215 04e1 0C          	.sleb128 12
 1216 04e2 00          	.byte 0x0
 1217 04e3 14          	.uleb128 0x14
 1218 04e4 02          	.byte 0x2
 1219 04e5 99 03 00 00 	.4byte 0x399
 1220 04e9 11          	.uleb128 0x11
 1221 04ea 01          	.byte 0x1
 1222 04eb 70 61 72 73 	.asciz "parse_can_common_trap"
 1222      65 5F 63 61 
 1222      6E 5F 63 6F 
 1222      6D 6D 6F 6E 
 1222      5F 74 72 61 
 1222      70 00 
 1223 0501 01          	.byte 0x1
 1224 0502 0D          	.byte 0xd
 1225 0503 01          	.byte 0x1
 1226 0504 00 00 00 00 	.4byte .LFB1
 1227 0508 00 00 00 00 	.4byte .LFE1
 1228 050c 01          	.byte 0x1
 1229 050d 5E          	.byte 0x5e
 1230 050e 30 05 00 00 	.4byte 0x530
 1231 0512 12          	.uleb128 0x12
 1232 0513 00 00 00 00 	.4byte .LASF2
 1233 0517 01          	.byte 0x1
 1234 0518 0D          	.byte 0xd
 1235 0519 DA 01 00 00 	.4byte 0x1da
 1236 051d 02          	.byte 0x2
 1237 051e 7E          	.byte 0x7e
 1238 051f 00          	.sleb128 0
 1239 0520 13          	.uleb128 0x13
 1240 0521 74 72 61 70 	.asciz "trap"
 1240      00 
 1241 0526 01          	.byte 0x1
 1242 0527 0D          	.byte 0xd
 1243 0528 30 05 00 00 	.4byte 0x530
 1244 052c 02          	.byte 0x2
 1245 052d 7E          	.byte 0x7e
 1246 052e 0C          	.sleb128 12
 1247 052f 00          	.byte 0x0
 1248 0530 14          	.uleb128 0x14
 1249 0531 02          	.byte 0x2
 1250 0532 6B 03 00 00 	.4byte 0x36b
 1251 0536 11          	.uleb128 0x11
 1252 0537 01          	.byte 0x1
 1253 0538 70 61 72 73 	.asciz "parse_can_common_reset"
 1253      65 5F 63 61 
 1253      6E 5F 63 6F 
 1253      6D 6D 6F 6E 
 1253      5F 72 65 73 
 1253      65 74 00 
 1254 054f 01          	.byte 0x1
 1255 0550 14          	.byte 0x14
 1256 0551 01          	.byte 0x1
 1257 0552 00 00 00 00 	.4byte .LFB2
 1258 0556 00 00 00 00 	.4byte .LFE2
MPLAB XC16 ASSEMBLY Listing:   			page 28


 1259 055a 01          	.byte 0x1
 1260 055b 5E          	.byte 0x5e
 1261 055c 7F 05 00 00 	.4byte 0x57f
 1262 0560 12          	.uleb128 0x12
 1263 0561 00 00 00 00 	.4byte .LASF2
 1264 0565 01          	.byte 0x1
 1265 0566 14          	.byte 0x14
 1266 0567 DA 01 00 00 	.4byte 0x1da
 1267 056b 02          	.byte 0x2
 1268 056c 7E          	.byte 0x7e
 1269 056d 00          	.sleb128 0
 1270 056e 13          	.uleb128 0x13
 1271 056f 52 45 53 45 	.asciz "RESET"
 1271      54 00 
 1272 0575 01          	.byte 0x1
 1273 0576 14          	.byte 0x14
 1274 0577 7F 05 00 00 	.4byte 0x57f
 1275 057b 02          	.byte 0x2
 1276 057c 7E          	.byte 0x7e
 1277 057d 0C          	.sleb128 12
 1278 057e 00          	.byte 0x0
 1279 057f 14          	.uleb128 0x14
 1280 0580 02          	.byte 0x2
 1281 0581 3B 03 00 00 	.4byte 0x33b
 1282 0585 11          	.uleb128 0x11
 1283 0586 01          	.byte 0x1
 1284 0587 70 61 72 73 	.asciz "parse_can_common_parse_error"
 1284      65 5F 63 61 
 1284      6E 5F 63 6F 
 1284      6D 6D 6F 6E 
 1284      5F 70 61 72 
 1284      73 65 5F 65 
 1284      72 72 6F 72 
 1284      00 
 1285 05a4 01          	.byte 0x1
 1286 05a5 1B          	.byte 0x1b
 1287 05a6 01          	.byte 0x1
 1288 05a7 00 00 00 00 	.4byte .LFB3
 1289 05ab 00 00 00 00 	.4byte .LFE3
 1290 05af 01          	.byte 0x1
 1291 05b0 5E          	.byte 0x5e
 1292 05b1 D4 05 00 00 	.4byte 0x5d4
 1293 05b5 12          	.uleb128 0x12
 1294 05b6 00 00 00 00 	.4byte .LASF2
 1295 05ba 01          	.byte 0x1
 1296 05bb 1B          	.byte 0x1b
 1297 05bc DA 01 00 00 	.4byte 0x1da
 1298 05c0 02          	.byte 0x2
 1299 05c1 7E          	.byte 0x7e
 1300 05c2 00          	.sleb128 0
 1301 05c3 13          	.uleb128 0x13
 1302 05c4 50 41 52 53 	.asciz "PARSE"
 1302      45 00 
 1303 05ca 01          	.byte 0x1
 1304 05cb 1B          	.byte 0x1b
 1305 05cc D4 05 00 00 	.4byte 0x5d4
 1306 05d0 02          	.byte 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 29


 1307 05d1 7E          	.byte 0x7e
 1308 05d2 0C          	.sleb128 12
 1309 05d3 00          	.byte 0x0
 1310 05d4 14          	.uleb128 0x14
 1311 05d5 02          	.byte 0x2
 1312 05d6 E9 03 00 00 	.4byte 0x3e9
 1313 05da 11          	.uleb128 0x11
 1314 05db 01          	.byte 0x1
 1315 05dc 70 61 72 73 	.asciz "parse_can_common_TS"
 1315      65 5F 63 61 
 1315      6E 5F 63 6F 
 1315      6D 6D 6F 6E 
 1315      5F 54 53 00 
 1316 05f0 01          	.byte 0x1
 1317 05f1 23          	.byte 0x23
 1318 05f2 01          	.byte 0x1
 1319 05f3 00 00 00 00 	.4byte .LFB4
 1320 05f7 00 00 00 00 	.4byte .LFE4
 1321 05fb 01          	.byte 0x1
 1322 05fc 5E          	.byte 0x5e
 1323 05fd 1D 06 00 00 	.4byte 0x61d
 1324 0601 12          	.uleb128 0x12
 1325 0602 00 00 00 00 	.4byte .LASF2
 1326 0606 01          	.byte 0x1
 1327 0607 23          	.byte 0x23
 1328 0608 DA 01 00 00 	.4byte 0x1da
 1329 060c 02          	.byte 0x2
 1330 060d 7E          	.byte 0x7e
 1331 060e 00          	.sleb128 0
 1332 060f 13          	.uleb128 0x13
 1333 0610 54 53 00    	.asciz "TS"
 1334 0613 01          	.byte 0x1
 1335 0614 23          	.byte 0x23
 1336 0615 1D 06 00 00 	.4byte 0x61d
 1337 0619 02          	.byte 0x2
 1338 061a 7E          	.byte 0x7e
 1339 061b 0C          	.sleb128 12
 1340 061c 00          	.byte 0x0
 1341 061d 14          	.uleb128 0x14
 1342 061e 02          	.byte 0x2
 1343 061f 4F 04 00 00 	.4byte 0x44f
 1344 0623 11          	.uleb128 0x11
 1345 0624 01          	.byte 0x1
 1346 0625 70 61 72 73 	.asciz "parse_can_common_RTD_OFF"
 1346      65 5F 63 61 
 1346      6E 5F 63 6F 
 1346      6D 6D 6F 6E 
 1346      5F 52 54 44 
 1346      5F 4F 46 46 
 1346      00 
 1347 063e 01          	.byte 0x1
 1348 063f 29          	.byte 0x29
 1349 0640 01          	.byte 0x1
 1350 0641 00 00 00 00 	.4byte .LFB5
 1351 0645 00 00 00 00 	.4byte .LFE5
 1352 0649 01          	.byte 0x1
 1353 064a 5E          	.byte 0x5e
MPLAB XC16 ASSEMBLY Listing:   			page 30


 1354 064b 74 06 00 00 	.4byte 0x674
 1355 064f 12          	.uleb128 0x12
 1356 0650 00 00 00 00 	.4byte .LASF2
 1357 0654 01          	.byte 0x1
 1358 0655 29          	.byte 0x29
 1359 0656 DA 01 00 00 	.4byte 0x1da
 1360 065a 02          	.byte 0x2
 1361 065b 7E          	.byte 0x7e
 1362 065c 00          	.sleb128 0
 1363 065d 13          	.uleb128 0x13
 1364 065e 72 74 64 5F 	.asciz "rtd_off_msg"
 1364      6F 66 66 5F 
 1364      6D 73 67 00 
 1365 066a 01          	.byte 0x1
 1366 066b 29          	.byte 0x29
 1367 066c 74 06 00 00 	.4byte 0x674
 1368 0670 02          	.byte 0x2
 1369 0671 7E          	.byte 0x7e
 1370 0672 0C          	.sleb128 12
 1371 0673 00          	.byte 0x0
 1372 0674 14          	.uleb128 0x14
 1373 0675 02          	.byte 0x2
 1374 0676 1E 04 00 00 	.4byte 0x41e
 1375 067a 11          	.uleb128 0x11
 1376 067b 01          	.byte 0x1
 1377 067c 70 61 72 73 	.asciz "parse_can_power_inverters"
 1377      65 5F 63 61 
 1377      6E 5F 70 6F 
 1377      77 65 72 5F 
 1377      69 6E 76 65 
 1377      72 74 65 72 
 1377      73 00 
 1378 0696 01          	.byte 0x1
 1379 0697 2F          	.byte 0x2f
 1380 0698 01          	.byte 0x1
 1381 0699 00 00 00 00 	.4byte .LFB6
 1382 069d 00 00 00 00 	.4byte .LFE6
 1383 06a1 01          	.byte 0x1
 1384 06a2 5E          	.byte 0x5e
 1385 06a3 CA 06 00 00 	.4byte 0x6ca
 1386 06a7 12          	.uleb128 0x12
 1387 06a8 00 00 00 00 	.4byte .LASF2
 1388 06ac 01          	.byte 0x1
 1389 06ad 2F          	.byte 0x2f
 1390 06ae DA 01 00 00 	.4byte 0x1da
 1391 06b2 02          	.byte 0x2
 1392 06b3 7E          	.byte 0x7e
 1393 06b4 00          	.sleb128 0
 1394 06b5 13          	.uleb128 0x13
 1395 06b6 69 6E 76 5F 	.asciz "inv_power"
 1395      70 6F 77 65 
 1395      72 00 
 1396 06c0 01          	.byte 0x1
 1397 06c1 2F          	.byte 0x2f
 1398 06c2 CA 06 00 00 	.4byte 0x6ca
 1399 06c6 02          	.byte 0x2
 1400 06c7 7E          	.byte 0x7e
MPLAB XC16 ASSEMBLY Listing:   			page 31


 1401 06c8 0C          	.sleb128 12
 1402 06c9 00          	.byte 0x0
 1403 06ca 14          	.uleb128 0x14
 1404 06cb 02          	.byte 0x2
 1405 06cc 87 04 00 00 	.4byte 0x487
 1406 06d0 15          	.uleb128 0x15
 1407 06d1 01          	.byte 0x1
 1408 06d2 6D 61 6B 65 	.asciz "make_reset_msg"
 1408      5F 72 65 73 
 1408      65 74 5F 6D 
 1408      73 67 00 
 1409 06e1 01          	.byte 0x1
 1410 06e2 33          	.byte 0x33
 1411 06e3 01          	.byte 0x1
 1412 06e4 DA 01 00 00 	.4byte 0x1da
 1413 06e8 00 00 00 00 	.4byte .LFB7
 1414 06ec 00 00 00 00 	.4byte .LFE7
 1415 06f0 01          	.byte 0x1
 1416 06f1 5E          	.byte 0x5e
 1417 06f2 22 07 00 00 	.4byte 0x722
 1418 06f6 12          	.uleb128 0x12
 1419 06f7 00 00 00 00 	.4byte .LASF0
 1420 06fb 01          	.byte 0x1
 1421 06fc 33          	.byte 0x33
 1422 06fd E3 00 00 00 	.4byte 0xe3
 1423 0701 02          	.byte 0x2
 1424 0702 7E          	.byte 0x7e
 1425 0703 0C          	.sleb128 12
 1426 0704 13          	.uleb128 0x13
 1427 0705 52 43 4F 4E 	.asciz "RCON"
 1427      00 
 1428 070a 01          	.byte 0x1
 1429 070b 33          	.byte 0x33
 1430 070c E3 00 00 00 	.4byte 0xe3
 1431 0710 02          	.byte 0x2
 1432 0711 7E          	.byte 0x7e
 1433 0712 0D          	.sleb128 13
 1434 0713 16          	.uleb128 0x16
 1435 0714 00 00 00 00 	.4byte .LASF2
 1436 0718 01          	.byte 0x1
 1437 0719 35          	.byte 0x35
 1438 071a DA 01 00 00 	.4byte 0x1da
 1439 071e 02          	.byte 0x2
 1440 071f 7E          	.byte 0x7e
 1441 0720 00          	.sleb128 0
 1442 0721 00          	.byte 0x0
 1443 0722 15          	.uleb128 0x15
 1444 0723 01          	.byte 0x1
 1445 0724 6D 61 6B 65 	.asciz "make_parse_error_msg"
 1445      5F 70 61 72 
 1445      73 65 5F 65 
 1445      72 72 6F 72 
 1445      5F 6D 73 67 
 1445      00 
 1446 0739 01          	.byte 0x1
 1447 073a 41          	.byte 0x41
 1448 073b 01          	.byte 0x1
MPLAB XC16 ASSEMBLY Listing:   			page 32


 1449 073c DA 01 00 00 	.4byte 0x1da
 1450 0740 00 00 00 00 	.4byte .LFB8
 1451 0744 00 00 00 00 	.4byte .LFE8
 1452 0748 01          	.byte 0x1
 1453 0749 5E          	.byte 0x5e
 1454 074a 91 07 00 00 	.4byte 0x791
 1455 074e 12          	.uleb128 0x12
 1456 074f 00 00 00 00 	.4byte .LASF0
 1457 0753 01          	.byte 0x1
 1458 0754 41          	.byte 0x41
 1459 0755 E3 00 00 00 	.4byte 0xe3
 1460 0759 02          	.byte 0x2
 1461 075a 7E          	.byte 0x7e
 1462 075b 0C          	.sleb128 12
 1463 075c 13          	.uleb128 0x13
 1464 075d 77 72 6F 6E 	.asciz "wrong_id"
 1464      67 5F 69 64 
 1464      00 
 1465 0766 01          	.byte 0x1
 1466 0767 41          	.byte 0x41
 1467 0768 E3 00 00 00 	.4byte 0xe3
 1468 076c 02          	.byte 0x2
 1469 076d 7E          	.byte 0x7e
 1470 076e 0D          	.sleb128 13
 1471 076f 13          	.uleb128 0x13
 1472 0770 72 69 67 68 	.asciz "right_id"
 1472      74 5F 69 64 
 1472      00 
 1473 0779 01          	.byte 0x1
 1474 077a 41          	.byte 0x41
 1475 077b E3 00 00 00 	.4byte 0xe3
 1476 077f 02          	.byte 0x2
 1477 0780 7E          	.byte 0x7e
 1478 0781 0E          	.sleb128 14
 1479 0782 16          	.uleb128 0x16
 1480 0783 00 00 00 00 	.4byte .LASF2
 1481 0787 01          	.byte 0x1
 1482 0788 43          	.byte 0x43
 1483 0789 DA 01 00 00 	.4byte 0x1da
 1484 078d 02          	.byte 0x2
 1485 078e 7E          	.byte 0x7e
 1486 078f 00          	.sleb128 0
 1487 0790 00          	.byte 0x0
 1488 0791 15          	.uleb128 0x15
 1489 0792 01          	.byte 0x1
 1490 0793 6D 61 6B 65 	.asciz "make_rtd_on_msg"
 1490      5F 72 74 64 
 1490      5F 6F 6E 5F 
 1490      6D 73 67 00 
 1491 07a3 01          	.byte 0x1
 1492 07a4 4F          	.byte 0x4f
 1493 07a5 01          	.byte 0x1
 1494 07a6 DA 01 00 00 	.4byte 0x1da
 1495 07aa 00 00 00 00 	.4byte .LFB9
 1496 07ae 00 00 00 00 	.4byte .LFE9
 1497 07b2 01          	.byte 0x1
 1498 07b3 5E          	.byte 0x5e
MPLAB XC16 ASSEMBLY Listing:   			page 33


 1499 07b4 E3 07 00 00 	.4byte 0x7e3
 1500 07b8 12          	.uleb128 0x12
 1501 07b9 00 00 00 00 	.4byte .LASF0
 1502 07bd 01          	.byte 0x1
 1503 07be 4F          	.byte 0x4f
 1504 07bf E3 00 00 00 	.4byte 0xe3
 1505 07c3 02          	.byte 0x2
 1506 07c4 7E          	.byte 0x7e
 1507 07c5 0C          	.sleb128 12
 1508 07c6 12          	.uleb128 0x12
 1509 07c7 00 00 00 00 	.4byte .LASF1
 1510 07cb 01          	.byte 0x1
 1511 07cc 4F          	.byte 0x4f
 1512 07cd 86 02 00 00 	.4byte 0x286
 1513 07d1 02          	.byte 0x2
 1514 07d2 7E          	.byte 0x7e
 1515 07d3 0E          	.sleb128 14
 1516 07d4 16          	.uleb128 0x16
 1517 07d5 00 00 00 00 	.4byte .LASF2
 1518 07d9 01          	.byte 0x1
 1519 07da 51          	.byte 0x51
 1520 07db DA 01 00 00 	.4byte 0x1da
 1521 07df 02          	.byte 0x2
 1522 07e0 7E          	.byte 0x7e
 1523 07e1 00          	.sleb128 0
 1524 07e2 00          	.byte 0x0
 1525 07e3 15          	.uleb128 0x15
 1526 07e4 01          	.byte 0x1
 1527 07e5 6D 61 6B 65 	.asciz "make_rtd_off_msg"
 1527      5F 72 74 64 
 1527      5F 6F 66 66 
 1527      5F 6D 73 67 
 1527      00 
 1528 07f6 01          	.byte 0x1
 1529 07f7 5E          	.byte 0x5e
 1530 07f8 01          	.byte 0x1
 1531 07f9 DA 01 00 00 	.4byte 0x1da
 1532 07fd 00 00 00 00 	.4byte .LFB10
 1533 0801 00 00 00 00 	.4byte .LFE10
 1534 0805 01          	.byte 0x1
 1535 0806 5E          	.byte 0x5e
 1536 0807 36 08 00 00 	.4byte 0x836
 1537 080b 12          	.uleb128 0x12
 1538 080c 00 00 00 00 	.4byte .LASF0
 1539 0810 01          	.byte 0x1
 1540 0811 5E          	.byte 0x5e
 1541 0812 E3 00 00 00 	.4byte 0xe3
 1542 0816 02          	.byte 0x2
 1543 0817 7E          	.byte 0x7e
 1544 0818 0C          	.sleb128 12
 1545 0819 12          	.uleb128 0x12
 1546 081a 00 00 00 00 	.4byte .LASF1
 1547 081e 01          	.byte 0x1
 1548 081f 5E          	.byte 0x5e
 1549 0820 CC 02 00 00 	.4byte 0x2cc
 1550 0824 02          	.byte 0x2
 1551 0825 7E          	.byte 0x7e
MPLAB XC16 ASSEMBLY Listing:   			page 34


 1552 0826 0E          	.sleb128 14
 1553 0827 16          	.uleb128 0x16
 1554 0828 00 00 00 00 	.4byte .LASF2
 1555 082c 01          	.byte 0x1
 1556 082d 60          	.byte 0x60
 1557 082e DA 01 00 00 	.4byte 0x1da
 1558 0832 02          	.byte 0x2
 1559 0833 7E          	.byte 0x7e
 1560 0834 00          	.sleb128 0
 1561 0835 00          	.byte 0x0
 1562 0836 17          	.uleb128 0x17
 1563 0837 01          	.byte 0x1
 1564 0838 6D 61 6B 65 	.asciz "make_ts_step_msg"
 1564      5F 74 73 5F 
 1564      73 74 65 70 
 1564      5F 6D 73 67 
 1564      00 
 1565 0849 01          	.byte 0x1
 1566 084a 6C          	.byte 0x6c
 1567 084b 01          	.byte 0x1
 1568 084c DA 01 00 00 	.4byte 0x1da
 1569 0850 00 00 00 00 	.4byte .LFB11
 1570 0854 00 00 00 00 	.4byte .LFE11
 1571 0858 01          	.byte 0x1
 1572 0859 5E          	.byte 0x5e
 1573 085a 12          	.uleb128 0x12
 1574 085b 00 00 00 00 	.4byte .LASF0
 1575 085f 01          	.byte 0x1
 1576 0860 6C          	.byte 0x6c
 1577 0861 E3 00 00 00 	.4byte 0xe3
 1578 0865 02          	.byte 0x2
 1579 0866 7E          	.byte 0x7e
 1580 0867 0C          	.sleb128 12
 1581 0868 12          	.uleb128 0x12
 1582 0869 00 00 00 00 	.4byte .LASF1
 1583 086d 01          	.byte 0x1
 1584 086e 6C          	.byte 0x6c
 1585 086f 14 03 00 00 	.4byte 0x314
 1586 0873 02          	.byte 0x2
 1587 0874 7E          	.byte 0x7e
 1588 0875 0E          	.sleb128 14
 1589 0876 16          	.uleb128 0x16
 1590 0877 00 00 00 00 	.4byte .LASF2
 1591 087b 01          	.byte 0x1
 1592 087c 6E          	.byte 0x6e
 1593 087d DA 01 00 00 	.4byte 0x1da
 1594 0881 02          	.byte 0x2
 1595 0882 7E          	.byte 0x7e
 1596 0883 00          	.sleb128 0
 1597 0884 00          	.byte 0x0
 1598 0885 00          	.byte 0x0
 1599                 	.section .debug_abbrev,info
 1600 0000 01          	.uleb128 0x1
 1601 0001 11          	.uleb128 0x11
 1602 0002 01          	.byte 0x1
 1603 0003 25          	.uleb128 0x25
 1604 0004 08          	.uleb128 0x8
MPLAB XC16 ASSEMBLY Listing:   			page 35


 1605 0005 13          	.uleb128 0x13
 1606 0006 0B          	.uleb128 0xb
 1607 0007 03          	.uleb128 0x3
 1608 0008 08          	.uleb128 0x8
 1609 0009 1B          	.uleb128 0x1b
 1610 000a 08          	.uleb128 0x8
 1611 000b 11          	.uleb128 0x11
 1612 000c 01          	.uleb128 0x1
 1613 000d 12          	.uleb128 0x12
 1614 000e 01          	.uleb128 0x1
 1615 000f 10          	.uleb128 0x10
 1616 0010 06          	.uleb128 0x6
 1617 0011 00          	.byte 0x0
 1618 0012 00          	.byte 0x0
 1619 0013 02          	.uleb128 0x2
 1620 0014 24          	.uleb128 0x24
 1621 0015 00          	.byte 0x0
 1622 0016 0B          	.uleb128 0xb
 1623 0017 0B          	.uleb128 0xb
 1624 0018 3E          	.uleb128 0x3e
 1625 0019 0B          	.uleb128 0xb
 1626 001a 03          	.uleb128 0x3
 1627 001b 08          	.uleb128 0x8
 1628 001c 00          	.byte 0x0
 1629 001d 00          	.byte 0x0
 1630 001e 03          	.uleb128 0x3
 1631 001f 16          	.uleb128 0x16
 1632 0020 00          	.byte 0x0
 1633 0021 03          	.uleb128 0x3
 1634 0022 08          	.uleb128 0x8
 1635 0023 3A          	.uleb128 0x3a
 1636 0024 0B          	.uleb128 0xb
 1637 0025 3B          	.uleb128 0x3b
 1638 0026 0B          	.uleb128 0xb
 1639 0027 49          	.uleb128 0x49
 1640 0028 13          	.uleb128 0x13
 1641 0029 00          	.byte 0x0
 1642 002a 00          	.byte 0x0
 1643 002b 04          	.uleb128 0x4
 1644 002c 13          	.uleb128 0x13
 1645 002d 01          	.byte 0x1
 1646 002e 0B          	.uleb128 0xb
 1647 002f 0B          	.uleb128 0xb
 1648 0030 3A          	.uleb128 0x3a
 1649 0031 0B          	.uleb128 0xb
 1650 0032 3B          	.uleb128 0x3b
 1651 0033 0B          	.uleb128 0xb
 1652 0034 01          	.uleb128 0x1
 1653 0035 13          	.uleb128 0x13
 1654 0036 00          	.byte 0x0
 1655 0037 00          	.byte 0x0
 1656 0038 05          	.uleb128 0x5
 1657 0039 0D          	.uleb128 0xd
 1658 003a 00          	.byte 0x0
 1659 003b 03          	.uleb128 0x3
 1660 003c 0E          	.uleb128 0xe
 1661 003d 3A          	.uleb128 0x3a
MPLAB XC16 ASSEMBLY Listing:   			page 36


 1662 003e 0B          	.uleb128 0xb
 1663 003f 3B          	.uleb128 0x3b
 1664 0040 0B          	.uleb128 0xb
 1665 0041 49          	.uleb128 0x49
 1666 0042 13          	.uleb128 0x13
 1667 0043 0B          	.uleb128 0xb
 1668 0044 0B          	.uleb128 0xb
 1669 0045 0D          	.uleb128 0xd
 1670 0046 0B          	.uleb128 0xb
 1671 0047 0C          	.uleb128 0xc
 1672 0048 0B          	.uleb128 0xb
 1673 0049 38          	.uleb128 0x38
 1674 004a 0A          	.uleb128 0xa
 1675 004b 00          	.byte 0x0
 1676 004c 00          	.byte 0x0
 1677 004d 06          	.uleb128 0x6
 1678 004e 0D          	.uleb128 0xd
 1679 004f 00          	.byte 0x0
 1680 0050 03          	.uleb128 0x3
 1681 0051 08          	.uleb128 0x8
 1682 0052 3A          	.uleb128 0x3a
 1683 0053 0B          	.uleb128 0xb
 1684 0054 3B          	.uleb128 0x3b
 1685 0055 0B          	.uleb128 0xb
 1686 0056 49          	.uleb128 0x49
 1687 0057 13          	.uleb128 0x13
 1688 0058 0B          	.uleb128 0xb
 1689 0059 0B          	.uleb128 0xb
 1690 005a 0D          	.uleb128 0xd
 1691 005b 0B          	.uleb128 0xb
 1692 005c 0C          	.uleb128 0xc
 1693 005d 0B          	.uleb128 0xb
 1694 005e 38          	.uleb128 0x38
 1695 005f 0A          	.uleb128 0xa
 1696 0060 00          	.byte 0x0
 1697 0061 00          	.byte 0x0
 1698 0062 07          	.uleb128 0x7
 1699 0063 17          	.uleb128 0x17
 1700 0064 01          	.byte 0x1
 1701 0065 0B          	.uleb128 0xb
 1702 0066 0B          	.uleb128 0xb
 1703 0067 3A          	.uleb128 0x3a
 1704 0068 0B          	.uleb128 0xb
 1705 0069 3B          	.uleb128 0x3b
 1706 006a 0B          	.uleb128 0xb
 1707 006b 01          	.uleb128 0x1
 1708 006c 13          	.uleb128 0x13
 1709 006d 00          	.byte 0x0
 1710 006e 00          	.byte 0x0
 1711 006f 08          	.uleb128 0x8
 1712 0070 0D          	.uleb128 0xd
 1713 0071 00          	.byte 0x0
 1714 0072 49          	.uleb128 0x49
 1715 0073 13          	.uleb128 0x13
 1716 0074 00          	.byte 0x0
 1717 0075 00          	.byte 0x0
 1718 0076 09          	.uleb128 0x9
MPLAB XC16 ASSEMBLY Listing:   			page 37


 1719 0077 0D          	.uleb128 0xd
 1720 0078 00          	.byte 0x0
 1721 0079 03          	.uleb128 0x3
 1722 007a 08          	.uleb128 0x8
 1723 007b 3A          	.uleb128 0x3a
 1724 007c 0B          	.uleb128 0xb
 1725 007d 3B          	.uleb128 0x3b
 1726 007e 0B          	.uleb128 0xb
 1727 007f 49          	.uleb128 0x49
 1728 0080 13          	.uleb128 0x13
 1729 0081 00          	.byte 0x0
 1730 0082 00          	.byte 0x0
 1731 0083 0A          	.uleb128 0xa
 1732 0084 0D          	.uleb128 0xd
 1733 0085 00          	.byte 0x0
 1734 0086 49          	.uleb128 0x49
 1735 0087 13          	.uleb128 0x13
 1736 0088 38          	.uleb128 0x38
 1737 0089 0A          	.uleb128 0xa
 1738 008a 00          	.byte 0x0
 1739 008b 00          	.byte 0x0
 1740 008c 0B          	.uleb128 0xb
 1741 008d 0D          	.uleb128 0xd
 1742 008e 00          	.byte 0x0
 1743 008f 03          	.uleb128 0x3
 1744 0090 08          	.uleb128 0x8
 1745 0091 3A          	.uleb128 0x3a
 1746 0092 0B          	.uleb128 0xb
 1747 0093 3B          	.uleb128 0x3b
 1748 0094 0B          	.uleb128 0xb
 1749 0095 49          	.uleb128 0x49
 1750 0096 13          	.uleb128 0x13
 1751 0097 38          	.uleb128 0x38
 1752 0098 0A          	.uleb128 0xa
 1753 0099 00          	.byte 0x0
 1754 009a 00          	.byte 0x0
 1755 009b 0C          	.uleb128 0xc
 1756 009c 01          	.uleb128 0x1
 1757 009d 01          	.byte 0x1
 1758 009e 49          	.uleb128 0x49
 1759 009f 13          	.uleb128 0x13
 1760 00a0 01          	.uleb128 0x1
 1761 00a1 13          	.uleb128 0x13
 1762 00a2 00          	.byte 0x0
 1763 00a3 00          	.byte 0x0
 1764 00a4 0D          	.uleb128 0xd
 1765 00a5 21          	.uleb128 0x21
 1766 00a6 00          	.byte 0x0
 1767 00a7 49          	.uleb128 0x49
 1768 00a8 13          	.uleb128 0x13
 1769 00a9 2F          	.uleb128 0x2f
 1770 00aa 0B          	.uleb128 0xb
 1771 00ab 00          	.byte 0x0
 1772 00ac 00          	.byte 0x0
 1773 00ad 0E          	.uleb128 0xe
 1774 00ae 04          	.uleb128 0x4
 1775 00af 01          	.byte 0x1
MPLAB XC16 ASSEMBLY Listing:   			page 38


 1776 00b0 0B          	.uleb128 0xb
 1777 00b1 0B          	.uleb128 0xb
 1778 00b2 3A          	.uleb128 0x3a
 1779 00b3 0B          	.uleb128 0xb
 1780 00b4 3B          	.uleb128 0x3b
 1781 00b5 0B          	.uleb128 0xb
 1782 00b6 01          	.uleb128 0x1
 1783 00b7 13          	.uleb128 0x13
 1784 00b8 00          	.byte 0x0
 1785 00b9 00          	.byte 0x0
 1786 00ba 0F          	.uleb128 0xf
 1787 00bb 28          	.uleb128 0x28
 1788 00bc 00          	.byte 0x0
 1789 00bd 03          	.uleb128 0x3
 1790 00be 08          	.uleb128 0x8
 1791 00bf 1C          	.uleb128 0x1c
 1792 00c0 0D          	.uleb128 0xd
 1793 00c1 00          	.byte 0x0
 1794 00c2 00          	.byte 0x0
 1795 00c3 10          	.uleb128 0x10
 1796 00c4 0D          	.uleb128 0xd
 1797 00c5 00          	.byte 0x0
 1798 00c6 03          	.uleb128 0x3
 1799 00c7 0E          	.uleb128 0xe
 1800 00c8 3A          	.uleb128 0x3a
 1801 00c9 0B          	.uleb128 0xb
 1802 00ca 3B          	.uleb128 0x3b
 1803 00cb 0B          	.uleb128 0xb
 1804 00cc 49          	.uleb128 0x49
 1805 00cd 13          	.uleb128 0x13
 1806 00ce 38          	.uleb128 0x38
 1807 00cf 0A          	.uleb128 0xa
 1808 00d0 00          	.byte 0x0
 1809 00d1 00          	.byte 0x0
 1810 00d2 11          	.uleb128 0x11
 1811 00d3 2E          	.uleb128 0x2e
 1812 00d4 01          	.byte 0x1
 1813 00d5 3F          	.uleb128 0x3f
 1814 00d6 0C          	.uleb128 0xc
 1815 00d7 03          	.uleb128 0x3
 1816 00d8 08          	.uleb128 0x8
 1817 00d9 3A          	.uleb128 0x3a
 1818 00da 0B          	.uleb128 0xb
 1819 00db 3B          	.uleb128 0x3b
 1820 00dc 0B          	.uleb128 0xb
 1821 00dd 27          	.uleb128 0x27
 1822 00de 0C          	.uleb128 0xc
 1823 00df 11          	.uleb128 0x11
 1824 00e0 01          	.uleb128 0x1
 1825 00e1 12          	.uleb128 0x12
 1826 00e2 01          	.uleb128 0x1
 1827 00e3 40          	.uleb128 0x40
 1828 00e4 0A          	.uleb128 0xa
 1829 00e5 01          	.uleb128 0x1
 1830 00e6 13          	.uleb128 0x13
 1831 00e7 00          	.byte 0x0
 1832 00e8 00          	.byte 0x0
MPLAB XC16 ASSEMBLY Listing:   			page 39


 1833 00e9 12          	.uleb128 0x12
 1834 00ea 05          	.uleb128 0x5
 1835 00eb 00          	.byte 0x0
 1836 00ec 03          	.uleb128 0x3
 1837 00ed 0E          	.uleb128 0xe
 1838 00ee 3A          	.uleb128 0x3a
 1839 00ef 0B          	.uleb128 0xb
 1840 00f0 3B          	.uleb128 0x3b
 1841 00f1 0B          	.uleb128 0xb
 1842 00f2 49          	.uleb128 0x49
 1843 00f3 13          	.uleb128 0x13
 1844 00f4 02          	.uleb128 0x2
 1845 00f5 0A          	.uleb128 0xa
 1846 00f6 00          	.byte 0x0
 1847 00f7 00          	.byte 0x0
 1848 00f8 13          	.uleb128 0x13
 1849 00f9 05          	.uleb128 0x5
 1850 00fa 00          	.byte 0x0
 1851 00fb 03          	.uleb128 0x3
 1852 00fc 08          	.uleb128 0x8
 1853 00fd 3A          	.uleb128 0x3a
 1854 00fe 0B          	.uleb128 0xb
 1855 00ff 3B          	.uleb128 0x3b
 1856 0100 0B          	.uleb128 0xb
 1857 0101 49          	.uleb128 0x49
 1858 0102 13          	.uleb128 0x13
 1859 0103 02          	.uleb128 0x2
 1860 0104 0A          	.uleb128 0xa
 1861 0105 00          	.byte 0x0
 1862 0106 00          	.byte 0x0
 1863 0107 14          	.uleb128 0x14
 1864 0108 0F          	.uleb128 0xf
 1865 0109 00          	.byte 0x0
 1866 010a 0B          	.uleb128 0xb
 1867 010b 0B          	.uleb128 0xb
 1868 010c 49          	.uleb128 0x49
 1869 010d 13          	.uleb128 0x13
 1870 010e 00          	.byte 0x0
 1871 010f 00          	.byte 0x0
 1872 0110 15          	.uleb128 0x15
 1873 0111 2E          	.uleb128 0x2e
 1874 0112 01          	.byte 0x1
 1875 0113 3F          	.uleb128 0x3f
 1876 0114 0C          	.uleb128 0xc
 1877 0115 03          	.uleb128 0x3
 1878 0116 08          	.uleb128 0x8
 1879 0117 3A          	.uleb128 0x3a
 1880 0118 0B          	.uleb128 0xb
 1881 0119 3B          	.uleb128 0x3b
 1882 011a 0B          	.uleb128 0xb
 1883 011b 27          	.uleb128 0x27
 1884 011c 0C          	.uleb128 0xc
 1885 011d 49          	.uleb128 0x49
 1886 011e 13          	.uleb128 0x13
 1887 011f 11          	.uleb128 0x11
 1888 0120 01          	.uleb128 0x1
 1889 0121 12          	.uleb128 0x12
MPLAB XC16 ASSEMBLY Listing:   			page 40


 1890 0122 01          	.uleb128 0x1
 1891 0123 40          	.uleb128 0x40
 1892 0124 0A          	.uleb128 0xa
 1893 0125 01          	.uleb128 0x1
 1894 0126 13          	.uleb128 0x13
 1895 0127 00          	.byte 0x0
 1896 0128 00          	.byte 0x0
 1897 0129 16          	.uleb128 0x16
 1898 012a 34          	.uleb128 0x34
 1899 012b 00          	.byte 0x0
 1900 012c 03          	.uleb128 0x3
 1901 012d 0E          	.uleb128 0xe
 1902 012e 3A          	.uleb128 0x3a
 1903 012f 0B          	.uleb128 0xb
 1904 0130 3B          	.uleb128 0x3b
 1905 0131 0B          	.uleb128 0xb
 1906 0132 49          	.uleb128 0x49
 1907 0133 13          	.uleb128 0x13
 1908 0134 02          	.uleb128 0x2
 1909 0135 0A          	.uleb128 0xa
 1910 0136 00          	.byte 0x0
 1911 0137 00          	.byte 0x0
 1912 0138 17          	.uleb128 0x17
 1913 0139 2E          	.uleb128 0x2e
 1914 013a 01          	.byte 0x1
 1915 013b 3F          	.uleb128 0x3f
 1916 013c 0C          	.uleb128 0xc
 1917 013d 03          	.uleb128 0x3
 1918 013e 08          	.uleb128 0x8
 1919 013f 3A          	.uleb128 0x3a
 1920 0140 0B          	.uleb128 0xb
 1921 0141 3B          	.uleb128 0x3b
 1922 0142 0B          	.uleb128 0xb
 1923 0143 27          	.uleb128 0x27
 1924 0144 0C          	.uleb128 0xc
 1925 0145 49          	.uleb128 0x49
 1926 0146 13          	.uleb128 0x13
 1927 0147 11          	.uleb128 0x11
 1928 0148 01          	.uleb128 0x1
 1929 0149 12          	.uleb128 0x12
 1930 014a 01          	.uleb128 0x1
 1931 014b 40          	.uleb128 0x40
 1932 014c 0A          	.uleb128 0xa
 1933 014d 00          	.byte 0x0
 1934 014e 00          	.byte 0x0
 1935 014f 00          	.byte 0x0
 1936                 	.section .debug_pubnames,info
 1937 0000 3A 01 00 00 	.4byte 0x13a
 1938 0004 02 00       	.2byte 0x2
 1939 0006 00 00 00 00 	.4byte .Ldebug_info0
 1940 000a 86 08 00 00 	.4byte 0x886
 1941 000e 9E 04 00 00 	.4byte 0x49e
 1942 0012 70 61 72 73 	.asciz "parse_can_common_RTD"
 1942      65 5F 63 61 
 1942      6E 5F 63 6F 
 1942      6D 6D 6F 6E 
 1942      5F 52 54 44 
MPLAB XC16 ASSEMBLY Listing:   			page 41


 1942      00 
 1943 0027 E9 04 00 00 	.4byte 0x4e9
 1944 002b 70 61 72 73 	.asciz "parse_can_common_trap"
 1944      65 5F 63 61 
 1944      6E 5F 63 6F 
 1944      6D 6D 6F 6E 
 1944      5F 74 72 61 
 1944      70 00 
 1945 0041 36 05 00 00 	.4byte 0x536
 1946 0045 70 61 72 73 	.asciz "parse_can_common_reset"
 1946      65 5F 63 61 
 1946      6E 5F 63 6F 
 1946      6D 6D 6F 6E 
 1946      5F 72 65 73 
 1946      65 74 00 
 1947 005c 85 05 00 00 	.4byte 0x585
 1948 0060 70 61 72 73 	.asciz "parse_can_common_parse_error"
 1948      65 5F 63 61 
 1948      6E 5F 63 6F 
 1948      6D 6D 6F 6E 
 1948      5F 70 61 72 
 1948      73 65 5F 65 
 1948      72 72 6F 72 
 1948      00 
 1949 007d DA 05 00 00 	.4byte 0x5da
 1950 0081 70 61 72 73 	.asciz "parse_can_common_TS"
 1950      65 5F 63 61 
 1950      6E 5F 63 6F 
 1950      6D 6D 6F 6E 
 1950      5F 54 53 00 
 1951 0095 23 06 00 00 	.4byte 0x623
 1952 0099 70 61 72 73 	.asciz "parse_can_common_RTD_OFF"
 1952      65 5F 63 61 
 1952      6E 5F 63 6F 
 1952      6D 6D 6F 6E 
 1952      5F 52 54 44 
 1952      5F 4F 46 46 
 1952      00 
 1953 00b2 7A 06 00 00 	.4byte 0x67a
 1954 00b6 70 61 72 73 	.asciz "parse_can_power_inverters"
 1954      65 5F 63 61 
 1954      6E 5F 70 6F 
 1954      77 65 72 5F 
 1954      69 6E 76 65 
 1954      72 74 65 72 
 1954      73 00 
 1955 00d0 D0 06 00 00 	.4byte 0x6d0
 1956 00d4 6D 61 6B 65 	.asciz "make_reset_msg"
 1956      5F 72 65 73 
 1956      65 74 5F 6D 
 1956      73 67 00 
 1957 00e3 22 07 00 00 	.4byte 0x722
 1958 00e7 6D 61 6B 65 	.asciz "make_parse_error_msg"
 1958      5F 70 61 72 
 1958      73 65 5F 65 
 1958      72 72 6F 72 
 1958      5F 6D 73 67 
MPLAB XC16 ASSEMBLY Listing:   			page 42


 1958      00 
 1959 00fc 91 07 00 00 	.4byte 0x791
 1960 0100 6D 61 6B 65 	.asciz "make_rtd_on_msg"
 1960      5F 72 74 64 
 1960      5F 6F 6E 5F 
 1960      6D 73 67 00 
 1961 0110 E3 07 00 00 	.4byte 0x7e3
 1962 0114 6D 61 6B 65 	.asciz "make_rtd_off_msg"
 1962      5F 72 74 64 
 1962      5F 6F 66 66 
 1962      5F 6D 73 67 
 1962      00 
 1963 0125 36 08 00 00 	.4byte 0x836
 1964 0129 6D 61 6B 65 	.asciz "make_ts_step_msg"
 1964      5F 74 73 5F 
 1964      73 74 65 70 
 1964      5F 6D 73 67 
 1964      00 
 1965 013a 00 00 00 00 	.4byte 0x0
 1966                 	.section .debug_pubtypes,info
 1967 0000 ED 00 00 00 	.4byte 0xed
 1968 0004 02 00       	.2byte 0x2
 1969 0006 00 00 00 00 	.4byte .Ldebug_info0
 1970 000a 86 08 00 00 	.4byte 0x886
 1971 000e E3 00 00 00 	.4byte 0xe3
 1972 0012 75 69 6E 74 	.asciz "uint8_t"
 1972      38 5F 74 00 
 1973 001a 03 01 00 00 	.4byte 0x103
 1974 001e 75 69 6E 74 	.asciz "uint16_t"
 1974      31 36 5F 74 
 1974      00 
 1975 0027 DA 01 00 00 	.4byte 0x1da
 1976 002b 43 41 4E 64 	.asciz "CANdata"
 1976      61 74 61 00 
 1977 0033 86 02 00 00 	.4byte 0x286
 1978 0037 52 54 44 5F 	.asciz "RTD_ON"
 1978      4F 4E 00 
 1979 003e CC 02 00 00 	.4byte 0x2cc
 1980 0042 52 54 44 5F 	.asciz "RTD_OFF"
 1980      4F 46 46 00 
 1981 004a 14 03 00 00 	.4byte 0x314
 1982 004e 54 53 5F 53 	.asciz "TS_STEP"
 1982      54 45 50 00 
 1983 0056 3B 03 00 00 	.4byte 0x33b
 1984 005a 43 4F 4D 4D 	.asciz "COMMON_MSG_RESET"
 1984      4F 4E 5F 4D 
 1984      53 47 5F 52 
 1984      45 53 45 54 
 1984      00 
 1985 006b 6B 03 00 00 	.4byte 0x36b
 1986 006f 43 4F 4D 4D 	.asciz "COMMON_MSG_TRAP"
 1986      4F 4E 5F 4D 
 1986      53 47 5F 54 
 1986      52 41 50 00 
 1987 007f 99 03 00 00 	.4byte 0x399
 1988 0083 43 4F 4D 4D 	.asciz "COMMON_MSG_RTD_ON"
 1988      4F 4E 5F 4D 
MPLAB XC16 ASSEMBLY Listing:   			page 43


 1988      53 47 5F 52 
 1988      54 44 5F 4F 
 1988      4E 00 
 1989 0095 E9 03 00 00 	.4byte 0x3e9
 1990 0099 43 4F 4D 4D 	.asciz "COMMON_MSG_PARSE_ERROR"
 1990      4F 4E 5F 4D 
 1990      53 47 5F 50 
 1990      41 52 53 45 
 1990      5F 45 52 52 
 1990      4F 52 00 
 1991 00b0 1E 04 00 00 	.4byte 0x41e
 1992 00b4 43 4F 4D 4D 	.asciz "COMMON_MSG_RTD_OFF"
 1992      4F 4E 5F 4D 
 1992      53 47 5F 52 
 1992      54 44 5F 4F 
 1992      46 46 00 
 1993 00c7 4F 04 00 00 	.4byte 0x44f
 1994 00cb 43 4F 4D 4D 	.asciz "COMMON_MSG_TS"
 1994      4F 4E 5F 4D 
 1994      53 47 5F 54 
 1994      53 00 
 1995 00d9 87 04 00 00 	.4byte 0x487
 1996 00dd 49 4E 56 45 	.asciz "INVERTERS_POWER"
 1996      52 54 45 52 
 1996      53 5F 50 4F 
 1996      57 45 52 00 
 1997 00ed 00 00 00 00 	.4byte 0x0
 1998                 	.section .debug_aranges,info
 1999 0000 14 00 00 00 	.4byte 0x14
 2000 0004 02 00       	.2byte 0x2
 2001 0006 00 00 00 00 	.4byte .Ldebug_info0
 2002 000a 04          	.byte 0x4
 2003 000b 00          	.byte 0x0
 2004 000c 00 00       	.2byte 0x0
 2005 000e 00 00       	.2byte 0x0
 2006 0010 00 00 00 00 	.4byte 0x0
 2007 0014 00 00 00 00 	.4byte 0x0
 2008                 	.section .debug_str,info
 2009                 	.LASF0:
 2010 0000 64 65 76 5F 	.asciz "dev_id"
 2010      69 64 00 
 2011                 	.LASF1:
 2012 0007 73 74 65 70 	.asciz "step"
 2012      00 
 2013                 	.LASF2:
 2014 000c 6D 65 73 73 	.asciz "message"
 2014      61 67 65 00 
 2015                 	.section .text,code
 2016              	
 2017              	
 2018              	
 2019              	.section __c30_info,info,bss
 2020                 	__psv_trap_errata:
 2021                 	
 2022                 	.section __c30_signature,info,data
 2023 0000 01 00       	.word 0x0001
 2024 0002 00 00       	.word 0x0000
MPLAB XC16 ASSEMBLY Listing:   			page 44


 2025 0004 00 00       	.word 0x0000
 2026                 	
 2027                 	
 2028                 	
 2029                 	.set ___PA___,0
 2030                 	.end
MPLAB XC16 ASSEMBLY Listing:   			page 45


DEFINED SYMBOLS
                            *ABS*:00000000 /home/user/Documents/FST/Programming/project_sub-zero/lib/can-ids/Devices/COMMON_CAN.c
                            *ABS*:00000001 __33EP256MU806
                            *ABS*:00000001 __dsPIC33E
                            *ABS*:00000001 __HAS_DSP
                            *ABS*:00000001 __HAS_DMAV2
                            *ABS*:00000001 __HAS_CODEGUARD
                            *ABS*:00000001 __HAS_PMP_ENHANCED
                            *ABS*:00000001 __HAS_EDS
                            *ABS*:00000001 __HAS_EP
                            *ABS*:00000011 __TARGET_DIVIDE_CYCLES
                            *ABS*:00000001 __C30ELF
    {standard input}:14     .text:00000000 _parse_can_common_RTD
    {standard input}:18     *ABS*:00000000 ___PA___
    {standard input}:47     .text:00000020 _parse_can_common_trap
    {standard input}:79     .text:00000040 _parse_can_common_reset
    {standard input}:111    .text:00000060 _parse_can_common_parse_error
    {standard input}:150    .text:0000008e _parse_can_common_TS
    {standard input}:181    .text:000000ac _parse_can_common_RTD_OFF
    {standard input}:212    .text:000000ca _parse_can_power_inverters
    {standard input}:243    .text:000000e8 _make_reset_msg
    {standard input}:317    .text:00000140 _make_parse_error_msg
    {standard input}:399    .text:000001a6 _make_rtd_on_msg
    {standard input}:473    .text:000001fa _make_rtd_off_msg
    {standard input}:546    .text:00000250 _make_ts_step_msg
    {standard input}:2020   __c30_info:00000000 __psv_trap_errata
    {standard input}:19     .text:00000000 .L0
                    .debug_abbrev:00000000 .Ldebug_abbrev0
                            .text:00000000 .Ltext0
                            .text:000002a6 .Letext0
                      .debug_line:00000000 .Ldebug_line0
                       .debug_str:00000000 .LASF0
                       .debug_str:00000007 .LASF1
                            .text:00000000 .LFB0
                            .text:00000020 .LFE0
                       .debug_str:0000000c .LASF2
                            .text:00000020 .LFB1
                            .text:00000040 .LFE1
                            .text:00000040 .LFB2
                            .text:00000060 .LFE2
                            .text:00000060 .LFB3
                            .text:0000008e .LFE3
                            .text:0000008e .LFB4
                            .text:000000ac .LFE4
                            .text:000000ac .LFB5
                            .text:000000ca .LFE5
                            .text:000000ca .LFB6
                            .text:000000e8 .LFE6
                            .text:000000e8 .LFB7
                            .text:00000140 .LFE7
                            .text:00000140 .LFB8
                            .text:000001a6 .LFE8
                            .text:000001a6 .LFB9
                            .text:000001fa .LFE9
                            .text:000001fa .LFB10
                            .text:00000250 .LFE10
                            .text:00000250 .LFB11
MPLAB XC16 ASSEMBLY Listing:   			page 46


                            .text:000002a6 .LFE11
                     .debug_frame:00000000 .Lframe0
                      .debug_info:00000000 .Ldebug_info0

UNDEFINED SYMBOLS
CORCON

EQUATE SYMBOLS
/home/user/Documents/FST/Programming/project_sub-zero/lib/can-ids/Devices/COMMON_CAN.c = 0x0
                  __33EP256MU806 = 0x1
                      __dsPIC33E = 0x1
                       __HAS_DSP = 0x1
                     __HAS_DMAV2 = 0x1
                 __HAS_CODEGUARD = 0x1
              __HAS_PMP_ENHANCED = 0x1
                       __HAS_EDS = 0x1
                        __HAS_EP = 0x1
          __TARGET_DIVIDE_CYCLES = 0x11
                        __C30ELF = 0x1
                        ___PA___ = 0x0
