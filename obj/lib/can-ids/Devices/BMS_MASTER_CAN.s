MPLAB XC16 ASSEMBLY Listing:   			page 1


   1              	.file "/home/user/Documents/FST/Programming/project_sub-zero/lib/can-ids/Devices/BMS_MASTER_CAN.c"
   2              	.section .debug_abbrev,info
   3                 	.Ldebug_abbrev0:
   4                 	.section .debug_info,info
   5                 	.Ldebug_info0:
   6                 	.section .debug_line,info
   7                 	.Ldebug_line0:
   8 0000 11 01 00 00 	.section .text,code
   8      02 00 A8 00 
   8      00 00 01 01 
   8      FB 0E 0A 00 
   8      01 01 01 01 
   8      00 00 00 01 
   8      6C 69 62 2F 
   8      63 61 6E 2D 
   8      69 64 73 2F 
   9              	.Ltext0:
  10              	.section .text,code
  11              	.align 2
  12              	.global _parse_can_message_master_ts
  13              	.type _parse_can_message_master_ts,@function
  14              	_parse_can_message_master_ts:
  15              	.LFB0:
  16              	.file 1 "lib/can-ids/Devices/BMS_MASTER_CAN.c"
   1:lib/can-ids/Devices/BMS_MASTER_CAN.c **** #include <stdint.h>
   2:lib/can-ids/Devices/BMS_MASTER_CAN.c **** 
   3:lib/can-ids/Devices/BMS_MASTER_CAN.c **** #include "can-ids/CAN_IDs.h"
   4:lib/can-ids/Devices/BMS_MASTER_CAN.c **** #include "can-ids/Devices/BMS_MASTER_CAN.h"
   5:lib/can-ids/Devices/BMS_MASTER_CAN.c **** 
   6:lib/can-ids/Devices/BMS_MASTER_CAN.c **** 
   7:lib/can-ids/Devices/BMS_MASTER_CAN.c **** 
   8:lib/can-ids/Devices/BMS_MASTER_CAN.c **** void parse_can_message_master_ts(CANdata msg, MASTER_MSG_TS *ts) {
  17              	.loc 1 8 0
  18              	.set ___PA___,1
  19 000000  0E 00 FA 	lnk #14
  20              	.LCFI0:
  21              	.loc 1 8 0
  22 000002  00 0F 78 	mov w0,[w14]
  23 000004  66 07 98 	mov w6,[w14+12]
   9:lib/can-ids/Devices/BMS_MASTER_CAN.c **** 	ts->state = msg.data[0] & 1;
  24              	.loc 1 9 0
  25 000006  6E 00 90 	mov [w14+12],w0
  26              	.loc 1 8 0
  27 000008  22 07 98 	mov w2,[w14+4]
  28 00000a  11 07 98 	mov w1,[w14+2]
  29 00000c  33 07 98 	mov w3,[w14+6]
  30 00000e  44 07 98 	mov w4,[w14+8]
  31 000010  55 07 98 	mov w5,[w14+10]
  32              	.loc 1 9 0
  33 000012  AE 00 90 	mov [w14+4],w1
  34 000014  10 01 78 	mov [w0],w2
  35 000016  E1 80 60 	and w1,#1,w1
  36 000018  02 00 A1 	bclr w2,#0
  37 00001a  01 F0 A7 	btsc w1,#15
  38 00001c  81 00 EA 	neg w1,w1
  39 00001e  81 00 EA 	neg w1,w1
  40 000020  CF 08 DE 	lsr w1,#15,w1
MPLAB XC16 ASSEMBLY Listing:   			page 2


  41 000022  81 40 78 	mov.b w1,w1
  42 000024  81 80 FB 	ze w1,w1
  43 000026  E1 80 60 	and w1,#1,w1
  44 000028  82 80 70 	ior w1,w2,w1
  45 00002a  01 08 78 	mov w1,[w0]
  10:lib/can-ids/Devices/BMS_MASTER_CAN.c **** 	ts->reason = (MASTER_MSG_TS_OFF_Reason) (msg.data[0] >> 1);
  46              	.loc 1 10 0
  47 00002c  6E 00 90 	mov [w14+12],w0
  48 00002e  AE 00 90 	mov [w14+4],w1
  49 000030  10 02 78 	mov [w0],w4
  50 000032  01 01 D1 	lsr w1,w2
  51 000034  F1 07 20 	mov #127,w1
  52 000036  82 41 78 	mov.b w2,w3
  53 000038  12 F0 2F 	mov #-255,w2
  54 00003a  03 74 A1 	bclr.b w3,#7
  55 00003c  02 01 62 	and w4,w2,w2
  56 00003e  83 81 FB 	ze w3,w3
  57 000040  81 80 61 	and w3,w1,w1
  58 000042  81 80 40 	add w1,w1,w1
  59 000044  82 80 70 	ior w1,w2,w1
  60 000046  01 08 78 	mov w1,[w0]
  11:lib/can-ids/Devices/BMS_MASTER_CAN.c **** }
  61              	.loc 1 11 0
  62 000048  8E 07 78 	mov w14,w15
  63 00004a  4F 07 78 	mov [--w15],w14
  64 00004c  00 40 A9 	bclr CORCON,#2
  65 00004e  00 00 06 	return 
  66              	.set ___PA___,0
  67              	.LFE0:
  68              	.size _parse_can_message_master_ts,.-_parse_can_message_master_ts
  69              	.align 2
  70              	.global _parse_can_message_master_energy_meter
  71              	.type _parse_can_message_master_energy_meter,@function
  72              	_parse_can_message_master_energy_meter:
  73              	.LFB1:
  12:lib/can-ids/Devices/BMS_MASTER_CAN.c **** 
  13:lib/can-ids/Devices/BMS_MASTER_CAN.c **** void parse_can_message_master_energy_meter(CANdata msg, MASTER_MSG_Energy_Meter
  14:lib/can-ids/Devices/BMS_MASTER_CAN.c **** 		*energy_meter) {
  74              	.loc 1 14 0
  75              	.set ___PA___,1
  76 000050  0E 00 FA 	lnk #14
  77              	.LCFI1:
  78 000052  88 1F 78 	mov w8,[w15++]
  79              	.LCFI2:
  80              	.loc 1 14 0
  81 000054  00 0F 78 	mov w0,[w14]
  82 000056  66 07 98 	mov w6,[w14+12]
  83 000058  22 07 98 	mov w2,[w14+4]
  84 00005a  33 07 98 	mov w3,[w14+6]
  85 00005c  44 07 98 	mov w4,[w14+8]
  86 00005e  55 07 98 	mov w5,[w14+10]
  15:lib/can-ids/Devices/BMS_MASTER_CAN.c **** 	energy_meter->voltage = msg.data[0];
  87              	.loc 1 15 0
  88 000060  2E 04 90 	mov [w14+4],w8
  89 000062  EE 03 90 	mov [w14+12],w7
  16:lib/can-ids/Devices/BMS_MASTER_CAN.c **** 	energy_meter->current = (int16_t)msg.data[1];
  90              	.loc 1 16 0
MPLAB XC16 ASSEMBLY Listing:   			page 3


  91 000064  3E 00 90 	mov [w14+6],w0
  92 000066  EE 02 90 	mov [w14+12],w5
  93 000068  00 03 78 	mov w0,w6
  17:lib/can-ids/Devices/BMS_MASTER_CAN.c **** 	energy_meter->power   = (int16_t)msg.data[2];
  94              	.loc 1 17 0
  95 00006a  4E 00 90 	mov [w14+8],w0
  96 00006c  EE 01 90 	mov [w14+12],w3
  97 00006e  00 02 78 	mov w0,w4
  18:lib/can-ids/Devices/BMS_MASTER_CAN.c **** 	energy_meter->SoC     = msg.data[3];
  98              	.loc 1 18 0
  99 000070  5E 01 90 	mov [w14+10],w2
 100 000072  6E 00 90 	mov [w14+12],w0
 101              	.loc 1 14 0
 102 000074  11 07 98 	mov w1,[w14+2]
 103              	.loc 1 15 0
 104 000076  88 0B 78 	mov w8,[w7]
 105              	.loc 1 16 0
 106 000078  96 02 98 	mov w6,[w5+2]
 107              	.loc 1 17 0
 108 00007a  A4 01 98 	mov w4,[w3+4]
 109              	.loc 1 18 0
 110 00007c  32 00 98 	mov w2,[w0+6]
  19:lib/can-ids/Devices/BMS_MASTER_CAN.c **** }
 111              	.loc 1 19 0
 112 00007e  4F 04 78 	mov [--w15],w8
 113 000080  8E 07 78 	mov w14,w15
 114 000082  4F 07 78 	mov [--w15],w14
 115 000084  00 40 A9 	bclr CORCON,#2
 116 000086  00 00 06 	return 
 117              	.set ___PA___,0
 118              	.LFE1:
 119              	.size _parse_can_message_master_energy_meter,.-_parse_can_message_master_energy_meter
 120              	.align 2
 121              	.global _parse_can_message_master_cell_voltage_info
 122              	.type _parse_can_message_master_cell_voltage_info,@function
 123              	_parse_can_message_master_cell_voltage_info:
 124              	.LFB2:
  20:lib/can-ids/Devices/BMS_MASTER_CAN.c **** 
  21:lib/can-ids/Devices/BMS_MASTER_CAN.c **** void parse_can_message_master_cell_voltage_info(CANdata msg,
  22:lib/can-ids/Devices/BMS_MASTER_CAN.c **** 		MASTER_MSG_Battery_Voltage_Info *cell_voltage_info) {
 125              	.loc 1 22 0
 126              	.set ___PA___,1
 127 000088  0E 00 FA 	lnk #14
 128              	.LCFI3:
 129              	.loc 1 22 0
 130 00008a  00 0F 78 	mov w0,[w14]
 131 00008c  66 07 98 	mov w6,[w14+12]
 132 00008e  55 07 98 	mov w5,[w14+10]
  23:lib/can-ids/Devices/BMS_MASTER_CAN.c **** 
  24:lib/can-ids/Devices/BMS_MASTER_CAN.c **** 	cell_voltage_info->min_voltage = msg.data[0];
  25:lib/can-ids/Devices/BMS_MASTER_CAN.c **** 	cell_voltage_info->mean_voltage = msg.data[1];
  26:lib/can-ids/Devices/BMS_MASTER_CAN.c **** 	cell_voltage_info->max_voltage = msg.data[2];
  27:lib/can-ids/Devices/BMS_MASTER_CAN.c **** 	cell_voltage_info->balance_target[0] = (msg.data[3] & 0xFF);
 133              	.loc 1 27 0
 134 000090  6E 00 90 	mov [w14+12],w0
 135 000092  DE 02 90 	mov [w14+10],w5
 136 000094  85 42 78 	mov.b w5,w5
MPLAB XC16 ASSEMBLY Listing:   			page 4


 137 000096  65 40 98 	mov.b w5,[w0+6]
  28:lib/can-ids/Devices/BMS_MASTER_CAN.c **** 	cell_voltage_info->balance_target[1] = ((msg.data[3]>>8) & 0xFF);
 138              	.loc 1 28 0
 139 000098  DE 02 90 	mov [w14+10],w5
 140 00009a  6E 00 90 	mov [w14+12],w0
 141 00009c  C8 2A DE 	lsr w5,#8,w5
 142 00009e  85 42 78 	mov.b w5,w5
 143 0000a0  75 40 98 	mov.b w5,[w0+7]
 144              	.loc 1 22 0
 145 0000a2  22 07 98 	mov w2,[w14+4]
 146 0000a4  33 07 98 	mov w3,[w14+6]
 147 0000a6  44 07 98 	mov w4,[w14+8]
 148              	.loc 1 24 0
 149 0000a8  2E 03 90 	mov [w14+4],w6
 150 0000aa  EE 02 90 	mov [w14+12],w5
 151              	.loc 1 25 0
 152 0000ac  3E 02 90 	mov [w14+6],w4
 153 0000ae  EE 01 90 	mov [w14+12],w3
 154              	.loc 1 26 0
 155 0000b0  4E 01 90 	mov [w14+8],w2
 156 0000b2  6E 00 90 	mov [w14+12],w0
 157              	.loc 1 22 0
 158 0000b4  11 07 98 	mov w1,[w14+2]
 159              	.loc 1 24 0
 160 0000b6  86 0A 78 	mov w6,[w5]
 161              	.loc 1 25 0
 162 0000b8  94 01 98 	mov w4,[w3+2]
 163              	.loc 1 26 0
 164 0000ba  22 00 98 	mov w2,[w0+4]
  29:lib/can-ids/Devices/BMS_MASTER_CAN.c **** }
 165              	.loc 1 29 0
 166 0000bc  8E 07 78 	mov w14,w15
 167 0000be  4F 07 78 	mov [--w15],w14
 168 0000c0  00 40 A9 	bclr CORCON,#2
 169 0000c2  00 00 06 	return 
 170              	.set ___PA___,0
 171              	.LFE2:
 172              	.size _parse_can_message_master_cell_voltage_info,.-_parse_can_message_master_cell_voltage_info
 173              	.align 2
 174              	.global _parse_can_message_master_cell_temperature_info
 175              	.type _parse_can_message_master_cell_temperature_info,@function
 176              	_parse_can_message_master_cell_temperature_info:
 177              	.LFB3:
  30:lib/can-ids/Devices/BMS_MASTER_CAN.c **** 
  31:lib/can-ids/Devices/BMS_MASTER_CAN.c **** void parse_can_message_master_cell_temperature_info(CANdata msg,
  32:lib/can-ids/Devices/BMS_MASTER_CAN.c **** 		MASTER_MSG_Battery_Temp_Info *cell_temp_info) {
 178              	.loc 1 32 0
 179              	.set ___PA___,1
 180 0000c4  0E 00 FA 	lnk #14
 181              	.LCFI4:
 182              	.loc 1 32 0
 183 0000c6  00 0F 78 	mov w0,[w14]
 184 0000c8  66 07 98 	mov w6,[w14+12]
 185 0000ca  55 07 98 	mov w5,[w14+10]
  33:lib/can-ids/Devices/BMS_MASTER_CAN.c **** 	cell_temp_info->min_temperature = msg.data[0];
  34:lib/can-ids/Devices/BMS_MASTER_CAN.c **** 	cell_temp_info->mean_temperature = msg.data[1];
  35:lib/can-ids/Devices/BMS_MASTER_CAN.c **** 	cell_temp_info->max_temperature = msg.data[2];
MPLAB XC16 ASSEMBLY Listing:   			page 5


  36:lib/can-ids/Devices/BMS_MASTER_CAN.c ****     cell_temp_info->hottest_cell[0] = (msg.data[3] & 0xFF);
 186              	.loc 1 36 0
 187 0000cc  6E 00 90 	mov [w14+12],w0
 188 0000ce  DE 02 90 	mov [w14+10],w5
 189 0000d0  85 42 78 	mov.b w5,w5
 190 0000d2  65 40 98 	mov.b w5,[w0+6]
  37:lib/can-ids/Devices/BMS_MASTER_CAN.c ****     cell_temp_info->hottest_cell[1] = ((msg.data[3]>>8) & 0xFF);
 191              	.loc 1 37 0
 192 0000d4  DE 02 90 	mov [w14+10],w5
 193 0000d6  6E 00 90 	mov [w14+12],w0
 194 0000d8  C8 2A DE 	lsr w5,#8,w5
 195 0000da  85 42 78 	mov.b w5,w5
 196 0000dc  75 40 98 	mov.b w5,[w0+7]
 197              	.loc 1 32 0
 198 0000de  22 07 98 	mov w2,[w14+4]
 199 0000e0  33 07 98 	mov w3,[w14+6]
 200 0000e2  44 07 98 	mov w4,[w14+8]
 201              	.loc 1 33 0
 202 0000e4  2E 03 90 	mov [w14+4],w6
 203 0000e6  EE 02 90 	mov [w14+12],w5
 204              	.loc 1 34 0
 205 0000e8  3E 02 90 	mov [w14+6],w4
 206 0000ea  EE 01 90 	mov [w14+12],w3
 207              	.loc 1 35 0
 208 0000ec  4E 01 90 	mov [w14+8],w2
 209 0000ee  6E 00 90 	mov [w14+12],w0
 210              	.loc 1 32 0
 211 0000f0  11 07 98 	mov w1,[w14+2]
 212              	.loc 1 33 0
 213 0000f2  86 0A 78 	mov w6,[w5]
 214              	.loc 1 34 0
 215 0000f4  94 01 98 	mov w4,[w3+2]
 216              	.loc 1 35 0
 217 0000f6  22 00 98 	mov w2,[w0+4]
  38:lib/can-ids/Devices/BMS_MASTER_CAN.c **** }
 218              	.loc 1 38 0
 219 0000f8  8E 07 78 	mov w14,w15
 220 0000fa  4F 07 78 	mov [--w15],w14
 221 0000fc  00 40 A9 	bclr CORCON,#2
 222 0000fe  00 00 06 	return 
 223              	.set ___PA___,0
 224              	.LFE3:
 225              	.size _parse_can_message_master_cell_temperature_info,.-_parse_can_message_master_cell_temperature_info
 226              	.align 2
 227              	.global _parse_can_message_master_status
 228              	.type _parse_can_message_master_status,@function
 229              	_parse_can_message_master_status:
 230              	.LFB4:
  39:lib/can-ids/Devices/BMS_MASTER_CAN.c **** 
  40:lib/can-ids/Devices/BMS_MASTER_CAN.c **** 
  41:lib/can-ids/Devices/BMS_MASTER_CAN.c **** void parse_can_message_master_status(CANdata msg, MASTER_MSG_Status *status)
  42:lib/can-ids/Devices/BMS_MASTER_CAN.c **** {
 231              	.loc 1 42 0
 232              	.set ___PA___,1
 233 000100  0E 00 FA 	lnk #14
 234              	.LCFI5:
 235              	.loc 1 42 0
MPLAB XC16 ASSEMBLY Listing:   			page 6


 236 000102  00 0F 78 	mov w0,[w14]
 237 000104  22 07 98 	mov w2,[w14+4]
 238 000106  66 07 98 	mov w6,[w14+12]
  43:lib/can-ids/Devices/BMS_MASTER_CAN.c **** 	status->status = msg.data[0];
 239              	.loc 1 43 0
 240 000108  2E 01 90 	mov [w14+4],w2
 241 00010a  6E 00 90 	mov [w14+12],w0
 242              	.loc 1 42 0
 243 00010c  11 07 98 	mov w1,[w14+2]
 244 00010e  33 07 98 	mov w3,[w14+6]
 245 000110  44 07 98 	mov w4,[w14+8]
 246 000112  55 07 98 	mov w5,[w14+10]
 247              	.loc 1 43 0
 248 000114  02 08 78 	mov w2,[w0]
  44:lib/can-ids/Devices/BMS_MASTER_CAN.c **** 	return;
  45:lib/can-ids/Devices/BMS_MASTER_CAN.c **** }
 249              	.loc 1 45 0
 250 000116  8E 07 78 	mov w14,w15
 251 000118  4F 07 78 	mov [--w15],w14
 252 00011a  00 40 A9 	bclr CORCON,#2
 253 00011c  00 00 06 	return 
 254              	.set ___PA___,0
 255              	.LFE4:
 256              	.size _parse_can_message_master_status,.-_parse_can_message_master_status
 257              	.align 2
 258              	.global _parse_can_message_master
 259              	.type _parse_can_message_master,@function
 260              	_parse_can_message_master:
 261              	.LFB5:
  46:lib/can-ids/Devices/BMS_MASTER_CAN.c **** 
  47:lib/can-ids/Devices/BMS_MASTER_CAN.c **** void parse_can_message_master(CANdata msg, MASTER_MSG_Data *data) {
 262              	.loc 1 47 0
 263              	.set ___PA___,1
 264 00011e  0E 00 FA 	lnk #14
 265              	.LCFI6:
 266              	.loc 1 47 0
 267 000120  00 0F 78 	mov w0,[w14]
 268 000122  11 07 98 	mov w1,[w14+2]
  48:lib/can-ids/Devices/BMS_MASTER_CAN.c **** 	switch(msg.msg_id) {
 269              	.loc 1 48 0
 270 000124  1E 00 78 	mov [w14],w0
 271              	.loc 1 47 0
 272 000126  22 07 98 	mov w2,[w14+4]
 273              	.loc 1 48 0
 274 000128  45 00 DE 	lsr w0,#5,w0
 275              	.loc 1 47 0
 276 00012a  33 07 98 	mov w3,[w14+6]
 277              	.loc 1 48 0
 278 00012c  F0 43 B2 	and.b #63,w0
 279              	.loc 1 47 0
 280 00012e  44 07 98 	mov w4,[w14+8]
 281 000130  55 07 98 	mov w5,[w14+10]
 282 000132  66 07 98 	mov w6,[w14+12]
 283              	.loc 1 48 0
 284 000134  00 80 FB 	ze w0,w0
 285 000136  EF 0F 50 	sub w0,#15,[w15]
 286              	.set ___BP___,0
MPLAB XC16 ASSEMBLY Listing:   			page 7


 287 000138  00 00 32 	bra z,.L11
 288 00013a  EF 0F 50 	sub w0,#15,[w15]
 289              	.set ___BP___,0
 290 00013c  00 00 3C 	bra gt,.L16
 291 00013e  ED 0F 50 	sub w0,#13,[w15]
 292              	.set ___BP___,0
 293 000140  00 00 32 	bra z,.L9
 294 000142  ED 0F 50 	sub w0,#13,[w15]
 295              	.set ___BP___,0
 296 000144  00 00 3C 	bra gt,.L10
 297 000146  EC 0F 50 	sub w0,#12,[w15]
 298              	.set ___BP___,0
 299 000148  00 00 32 	bra z,.L8
 300 00014a  00 00 37 	bra .L6
 301              	.L16:
 302 00014c  91 03 20 	mov #57,w1
 303 00014e  81 0F 50 	sub w0,w1,[w15]
 304              	.set ___BP___,0
 305 000150  00 00 32 	bra z,.L13
 306 000152  91 03 20 	mov #57,w1
 307 000154  81 0F 50 	sub w0,w1,[w15]
 308              	.set ___BP___,0
 309 000156  00 00 3C 	bra gt,.L17
 310 000158  81 03 20 	mov #56,w1
 311 00015a  81 0F 50 	sub w0,w1,[w15]
 312              	.set ___BP___,0
 313 00015c  00 00 32 	bra z,.L12
 314 00015e  00 00 37 	bra .L6
 315              	.L17:
 316 000160  D1 03 20 	mov #61,w1
 317 000162  81 0F 50 	sub w0,w1,[w15]
 318              	.set ___BP___,0
 319 000164  00 00 32 	bra z,.L14
 320 000166  E1 03 20 	mov #62,w1
 321 000168  81 0F 50 	sub w0,w1,[w15]
 322              	.set ___BP___,0
 323 00016a  00 00 32 	bra z,.L15
 324 00016c  00 00 37 	bra .L6
 325              	.L12:
  49:lib/can-ids/Devices/BMS_MASTER_CAN.c **** 		case MSG_ID_MASTER_CELL_TEMPERATURE_INFO:
  50:lib/can-ids/Devices/BMS_MASTER_CAN.c **** 			parse_can_message_master_cell_temperature_info(msg,
 326              	.loc 1 50 0
 327 00016e  6E 00 90 	mov [w14+12],w0
 328 000170  9E 00 90 	mov [w14+2],w1
 329 000172  78 01 40 	add w0,#24,w2
 330 000174  1E 00 78 	mov [w14],w0
 331 000176  02 03 78 	mov w2,w6
 332 000178  2E 01 90 	mov [w14+4],w2
 333 00017a  BE 01 90 	mov [w14+6],w3
 334 00017c  4E 02 90 	mov [w14+8],w4
 335 00017e  DE 02 90 	mov [w14+10],w5
 336 000180  00 00 07 	rcall _parse_can_message_master_cell_temperature_info
  51:lib/can-ids/Devices/BMS_MASTER_CAN.c **** 					&(data->cell_temp_info));
  52:lib/can-ids/Devices/BMS_MASTER_CAN.c **** 			break;
 337              	.loc 1 52 0
 338 000182  00 00 37 	bra .L6
 339              	.L13:
MPLAB XC16 ASSEMBLY Listing:   			page 8


  53:lib/can-ids/Devices/BMS_MASTER_CAN.c **** 		case MSG_ID_MASTER_CELL_VOLTAGE_INFO:
  54:lib/can-ids/Devices/BMS_MASTER_CAN.c **** 			parse_can_message_master_cell_voltage_info(msg,
 340              	.loc 1 54 0
 341 000184  00 00 00 	nop 
 342 000186  6E 00 90 	mov [w14+12],w0
 343 000188  00 00 00 	nop 
 344 00018a  9E 00 90 	mov [w14+2],w1
 345 00018c  70 01 40 	add w0,#16,w2
 346 00018e  1E 00 78 	mov [w14],w0
 347 000190  02 03 78 	mov w2,w6
 348 000192  2E 01 90 	mov [w14+4],w2
 349 000194  BE 01 90 	mov [w14+6],w3
 350 000196  4E 02 90 	mov [w14+8],w4
 351 000198  DE 02 90 	mov [w14+10],w5
 352 00019a  00 00 07 	rcall _parse_can_message_master_cell_voltage_info
  55:lib/can-ids/Devices/BMS_MASTER_CAN.c **** 					&(data->cell_voltage_info));
  56:lib/can-ids/Devices/BMS_MASTER_CAN.c **** 			break;
 353              	.loc 1 56 0
 354 00019c  00 00 37 	bra .L6
 355              	.L8:
  57:lib/can-ids/Devices/BMS_MASTER_CAN.c **** 		case MSG_ID_MASTER_TS_STATE:
  58:lib/can-ids/Devices/BMS_MASTER_CAN.c **** 			parse_can_message_master_ts(msg, &(data->TS));
 356              	.loc 1 58 0
 357 00019e  00 00 00 	nop 
 358 0001a0  6E 00 90 	mov [w14+12],w0
 359 0001a2  00 00 00 	nop 
 360 0001a4  9E 00 90 	mov [w14+2],w1
 361 0001a6  00 81 E8 	inc2 w0,w2
 362 0001a8  1E 00 78 	mov [w14],w0
 363 0001aa  02 03 78 	mov w2,w6
 364 0001ac  2E 01 90 	mov [w14+4],w2
 365 0001ae  BE 01 90 	mov [w14+6],w3
 366 0001b0  4E 02 90 	mov [w14+8],w4
 367 0001b2  DE 02 90 	mov [w14+10],w5
 368 0001b4  00 00 07 	rcall _parse_can_message_master_ts
  59:lib/can-ids/Devices/BMS_MASTER_CAN.c **** 			break;
 369              	.loc 1 59 0
 370 0001b6  00 00 37 	bra .L6
 371              	.L9:
  60:lib/can-ids/Devices/BMS_MASTER_CAN.c **** 		case MSG_ID_MASTER_IMD_ERROR:
  61:lib/can-ids/Devices/BMS_MASTER_CAN.c **** 			data->imd_error = 1;
 372              	.loc 1 61 0
 373 0001b8  00 00 00 	nop 
 374 0001ba  6E 00 90 	mov [w14+12],w0
 375 0001bc  11 C0 B3 	mov.b #1,w1
 376 0001be  41 48 98 	mov.b w1,[w0+12]
  62:lib/can-ids/Devices/BMS_MASTER_CAN.c **** 			break;
 377              	.loc 1 62 0
 378 0001c0  00 00 37 	bra .L6
 379              	.L10:
  63:lib/can-ids/Devices/BMS_MASTER_CAN.c **** 		case MSG_ID_MASTER_AMS_ERROR:
  64:lib/can-ids/Devices/BMS_MASTER_CAN.c **** 			data->ams_error = 1;
 380              	.loc 1 64 0
 381 0001c2  00 00 00 	nop 
 382 0001c4  6E 00 90 	mov [w14+12],w0
 383 0001c6  11 C0 B3 	mov.b #1,w1
 384 0001c8  51 48 98 	mov.b w1,[w0+13]
MPLAB XC16 ASSEMBLY Listing:   			page 9


  65:lib/can-ids/Devices/BMS_MASTER_CAN.c **** 			break;
 385              	.loc 1 65 0
 386 0001ca  00 00 37 	bra .L6
 387              	.L14:
  66:lib/can-ids/Devices/BMS_MASTER_CAN.c **** 		case MSG_ID_MASTER_ENERGY_METER:
  67:lib/can-ids/Devices/BMS_MASTER_CAN.c **** 			parse_can_message_master_energy_meter(msg, &(data->energy_meter));
 388              	.loc 1 67 0
 389 0001cc  00 00 00 	nop 
 390 0001ce  6E 00 90 	mov [w14+12],w0
 391 0001d0  00 00 00 	nop 
 392 0001d2  9E 00 90 	mov [w14+2],w1
 393 0001d4  64 01 40 	add w0,#4,w2
 394 0001d6  1E 00 78 	mov [w14],w0
 395 0001d8  02 03 78 	mov w2,w6
 396 0001da  2E 01 90 	mov [w14+4],w2
 397 0001dc  BE 01 90 	mov [w14+6],w3
 398 0001de  4E 02 90 	mov [w14+8],w4
 399 0001e0  DE 02 90 	mov [w14+10],w5
 400 0001e2  00 00 07 	rcall _parse_can_message_master_energy_meter
  68:lib/can-ids/Devices/BMS_MASTER_CAN.c **** 			break;
 401              	.loc 1 68 0
 402 0001e4  00 00 37 	bra .L6
 403              	.L15:
  69:lib/can-ids/Devices/BMS_MASTER_CAN.c **** 		case MSG_ID_MASTER_SC_OPEN:
  70:lib/can-ids/Devices/BMS_MASTER_CAN.c **** 			data->sc_open = 1;
 404              	.loc 1 70 0
 405 0001e6  00 00 00 	nop 
 406 0001e8  6E 00 90 	mov [w14+12],w0
 407 0001ea  11 C0 B3 	mov.b #1,w1
 408 0001ec  61 48 98 	mov.b w1,[w0+14]
  71:lib/can-ids/Devices/BMS_MASTER_CAN.c **** 			break;
 409              	.loc 1 71 0
 410 0001ee  00 00 37 	bra .L6
 411              	.L11:
  72:lib/can-ids/Devices/BMS_MASTER_CAN.c **** 		case MSG_ID_MASTER_STATUS:
  73:lib/can-ids/Devices/BMS_MASTER_CAN.c **** 			parse_can_message_master_status(msg, &(data->status));
 412              	.loc 1 73 0
 413 0001f0  00 00 00 	nop 
 414 0001f2  6E 00 90 	mov [w14+12],w0
 415 0001f4  00 00 00 	nop 
 416 0001f6  9E 00 90 	mov [w14+2],w1
 417 0001f8  00 03 78 	mov w0,w6
 418 0001fa  1E 00 78 	mov [w14],w0
 419 0001fc  2E 01 90 	mov [w14+4],w2
 420 0001fe  BE 01 90 	mov [w14+6],w3
 421 000200  4E 02 90 	mov [w14+8],w4
 422 000202  DE 02 90 	mov [w14+10],w5
 423 000204  00 00 07 	rcall _parse_can_message_master_status
 424              	.L6:
  74:lib/can-ids/Devices/BMS_MASTER_CAN.c **** 			break;
  75:lib/can-ids/Devices/BMS_MASTER_CAN.c **** 	}
  76:lib/can-ids/Devices/BMS_MASTER_CAN.c **** }
 425              	.loc 1 76 0
 426 000206  8E 07 78 	mov w14,w15
 427 000208  4F 07 78 	mov [--w15],w14
 428 00020a  00 40 A9 	bclr CORCON,#2
 429 00020c  00 00 06 	return 
MPLAB XC16 ASSEMBLY Listing:   			page 10


 430              	.set ___PA___,0
 431              	.LFE5:
 432              	.size _parse_can_message_master,.-_parse_can_message_master
 433              	.section .debug_frame,info
 434                 	.Lframe0:
 435 0000 10 00 00 00 	.4byte .LECIE0-.LSCIE0
 436                 	.LSCIE0:
 437 0004 FF FF FF FF 	.4byte 0xffffffff
 438 0008 01          	.byte 0x1
 439 0009 00          	.byte 0
 440 000a 01          	.uleb128 0x1
 441 000b 02          	.sleb128 2
 442 000c 25          	.byte 0x25
 443 000d 12          	.byte 0x12
 444 000e 0F          	.uleb128 0xf
 445 000f 7E          	.sleb128 -2
 446 0010 09          	.byte 0x9
 447 0011 25          	.uleb128 0x25
 448 0012 0F          	.uleb128 0xf
 449 0013 00          	.align 4
 450                 	.LECIE0:
 451                 	.LSFDE0:
 452 0014 18 00 00 00 	.4byte .LEFDE0-.LASFDE0
 453                 	.LASFDE0:
 454 0018 00 00 00 00 	.4byte .Lframe0
 455 001c 00 00 00 00 	.4byte .LFB0
 456 0020 50 00 00 00 	.4byte .LFE0-.LFB0
 457 0024 04          	.byte 0x4
 458 0025 02 00 00 00 	.4byte .LCFI0-.LFB0
 459 0029 13          	.byte 0x13
 460 002a 7D          	.sleb128 -3
 461 002b 0D          	.byte 0xd
 462 002c 0E          	.uleb128 0xe
 463 002d 8E          	.byte 0x8e
 464 002e 02          	.uleb128 0x2
 465 002f 00          	.align 4
 466                 	.LEFDE0:
 467                 	.LSFDE2:
 468 0030 1E 00 00 00 	.4byte .LEFDE2-.LASFDE2
 469                 	.LASFDE2:
 470 0034 00 00 00 00 	.4byte .Lframe0
 471 0038 00 00 00 00 	.4byte .LFB1
 472 003c 38 00 00 00 	.4byte .LFE1-.LFB1
 473 0040 04          	.byte 0x4
 474 0041 02 00 00 00 	.4byte .LCFI1-.LFB1
 475 0045 13          	.byte 0x13
 476 0046 7D          	.sleb128 -3
 477 0047 0D          	.byte 0xd
 478 0048 0E          	.uleb128 0xe
 479 0049 8E          	.byte 0x8e
 480 004a 02          	.uleb128 0x2
 481 004b 04          	.byte 0x4
 482 004c 02 00 00 00 	.4byte .LCFI2-.LCFI1
 483 0050 88          	.byte 0x88
 484 0051 0A          	.uleb128 0xa
 485                 	.align 4
 486                 	.LEFDE2:
MPLAB XC16 ASSEMBLY Listing:   			page 11


 487                 	.LSFDE4:
 488 0052 18 00 00 00 	.4byte .LEFDE4-.LASFDE4
 489                 	.LASFDE4:
 490 0056 00 00 00 00 	.4byte .Lframe0
 491 005a 00 00 00 00 	.4byte .LFB2
 492 005e 3C 00 00 00 	.4byte .LFE2-.LFB2
 493 0062 04          	.byte 0x4
 494 0063 02 00 00 00 	.4byte .LCFI3-.LFB2
 495 0067 13          	.byte 0x13
 496 0068 7D          	.sleb128 -3
 497 0069 0D          	.byte 0xd
 498 006a 0E          	.uleb128 0xe
 499 006b 8E          	.byte 0x8e
 500 006c 02          	.uleb128 0x2
 501 006d 00          	.align 4
 502                 	.LEFDE4:
 503                 	.LSFDE6:
 504 006e 18 00 00 00 	.4byte .LEFDE6-.LASFDE6
 505                 	.LASFDE6:
 506 0072 00 00 00 00 	.4byte .Lframe0
 507 0076 00 00 00 00 	.4byte .LFB3
 508 007a 3C 00 00 00 	.4byte .LFE3-.LFB3
 509 007e 04          	.byte 0x4
 510 007f 02 00 00 00 	.4byte .LCFI4-.LFB3
 511 0083 13          	.byte 0x13
 512 0084 7D          	.sleb128 -3
 513 0085 0D          	.byte 0xd
 514 0086 0E          	.uleb128 0xe
 515 0087 8E          	.byte 0x8e
 516 0088 02          	.uleb128 0x2
 517 0089 00          	.align 4
 518                 	.LEFDE6:
 519                 	.LSFDE8:
 520 008a 18 00 00 00 	.4byte .LEFDE8-.LASFDE8
 521                 	.LASFDE8:
 522 008e 00 00 00 00 	.4byte .Lframe0
 523 0092 00 00 00 00 	.4byte .LFB4
 524 0096 1E 00 00 00 	.4byte .LFE4-.LFB4
 525 009a 04          	.byte 0x4
 526 009b 02 00 00 00 	.4byte .LCFI5-.LFB4
 527 009f 13          	.byte 0x13
 528 00a0 7D          	.sleb128 -3
 529 00a1 0D          	.byte 0xd
 530 00a2 0E          	.uleb128 0xe
 531 00a3 8E          	.byte 0x8e
 532 00a4 02          	.uleb128 0x2
 533 00a5 00          	.align 4
 534                 	.LEFDE8:
 535                 	.LSFDE10:
 536 00a6 18 00 00 00 	.4byte .LEFDE10-.LASFDE10
 537                 	.LASFDE10:
 538 00aa 00 00 00 00 	.4byte .Lframe0
 539 00ae 00 00 00 00 	.4byte .LFB5
 540 00b2 F0 00 00 00 	.4byte .LFE5-.LFB5
 541 00b6 04          	.byte 0x4
 542 00b7 02 00 00 00 	.4byte .LCFI6-.LFB5
 543 00bb 13          	.byte 0x13
MPLAB XC16 ASSEMBLY Listing:   			page 12


 544 00bc 7D          	.sleb128 -3
 545 00bd 0D          	.byte 0xd
 546 00be 0E          	.uleb128 0xe
 547 00bf 8E          	.byte 0x8e
 548 00c0 02          	.uleb128 0x2
 549 00c1 00          	.align 4
 550                 	.LEFDE10:
 551                 	.section .text,code
 552              	.Letext0:
 553              	.file 2 "/opt/microchip/xc16/v1.36/bin/bin/../../include/lega-c/stdint.h"
 554              	.file 3 "lib/can-ids/CAN_IDs.h"
 555              	.file 4 "lib/can-ids/Devices/BMS_MASTER_CAN.h"
 556              	.section .debug_info,info
 557 0000 DD 09 00 00 	.4byte 0x9dd
 558 0004 02 00       	.2byte 0x2
 559 0006 00 00 00 00 	.4byte .Ldebug_abbrev0
 560 000a 04          	.byte 0x4
 561 000b 01          	.uleb128 0x1
 562 000c 47 4E 55 20 	.asciz "GNU C 4.5.1 (XC16, Microchip v1.36) (A) Build date: Dec  5 2018"
 562      43 20 34 2E 
 562      35 2E 31 20 
 562      28 58 43 31 
 562      36 2C 20 4D 
 562      69 63 72 6F 
 562      63 68 69 70 
 562      20 76 31 2E 
 562      33 36 29 20 
 563 004c 01          	.byte 0x1
 564 004d 6C 69 62 2F 	.asciz "lib/can-ids/Devices/BMS_MASTER_CAN.c"
 564      63 61 6E 2D 
 564      69 64 73 2F 
 564      44 65 76 69 
 564      63 65 73 2F 
 564      42 4D 53 5F 
 564      4D 41 53 54 
 564      45 52 5F 43 
 564      41 4E 2E 63 
 565 0072 2F 68 6F 6D 	.asciz "/home/user/Documents/FST/Programming/project_sub-zero"
 565      65 2F 75 73 
 565      65 72 2F 44 
 565      6F 63 75 6D 
 565      65 6E 74 73 
 565      2F 46 53 54 
 565      2F 50 72 6F 
 565      67 72 61 6D 
 565      6D 69 6E 67 
 566 00a8 00 00 00 00 	.4byte .Ltext0
 567 00ac 00 00 00 00 	.4byte .Letext0
 568 00b0 00 00 00 00 	.4byte .Ldebug_line0
 569 00b4 02          	.uleb128 0x2
 570 00b5 01          	.byte 0x1
 571 00b6 06          	.byte 0x6
 572 00b7 73 69 67 6E 	.asciz "signed char"
 572      65 64 20 63 
 572      68 61 72 00 
 573 00c3 03          	.uleb128 0x3
 574 00c4 69 6E 74 31 	.asciz "int16_t"
MPLAB XC16 ASSEMBLY Listing:   			page 13


 574      36 5F 74 00 
 575 00cc 02          	.byte 0x2
 576 00cd 14          	.byte 0x14
 577 00ce D2 00 00 00 	.4byte 0xd2
 578 00d2 02          	.uleb128 0x2
 579 00d3 02          	.byte 0x2
 580 00d4 05          	.byte 0x5
 581 00d5 69 6E 74 00 	.asciz "int"
 582 00d9 02          	.uleb128 0x2
 583 00da 04          	.byte 0x4
 584 00db 05          	.byte 0x5
 585 00dc 6C 6F 6E 67 	.asciz "long int"
 585      20 69 6E 74 
 585      00 
 586 00e5 02          	.uleb128 0x2
 587 00e6 08          	.byte 0x8
 588 00e7 05          	.byte 0x5
 589 00e8 6C 6F 6E 67 	.asciz "long long int"
 589      20 6C 6F 6E 
 589      67 20 69 6E 
 589      74 00 
 590 00f6 03          	.uleb128 0x3
 591 00f7 75 69 6E 74 	.asciz "uint8_t"
 591      38 5F 74 00 
 592 00ff 02          	.byte 0x2
 593 0100 2B          	.byte 0x2b
 594 0101 05 01 00 00 	.4byte 0x105
 595 0105 02          	.uleb128 0x2
 596 0106 01          	.byte 0x1
 597 0107 08          	.byte 0x8
 598 0108 75 6E 73 69 	.asciz "unsigned char"
 598      67 6E 65 64 
 598      20 63 68 61 
 598      72 00 
 599 0116 03          	.uleb128 0x3
 600 0117 75 69 6E 74 	.asciz "uint16_t"
 600      31 36 5F 74 
 600      00 
 601 0120 02          	.byte 0x2
 602 0121 31          	.byte 0x31
 603 0122 26 01 00 00 	.4byte 0x126
 604 0126 02          	.uleb128 0x2
 605 0127 02          	.byte 0x2
 606 0128 07          	.byte 0x7
 607 0129 75 6E 73 69 	.asciz "unsigned int"
 607      67 6E 65 64 
 607      20 69 6E 74 
 607      00 
 608 0136 02          	.uleb128 0x2
 609 0137 04          	.byte 0x4
 610 0138 07          	.byte 0x7
 611 0139 6C 6F 6E 67 	.asciz "long unsigned int"
 611      20 75 6E 73 
 611      69 67 6E 65 
 611      64 20 69 6E 
 611      74 00 
 612 014b 02          	.uleb128 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 14


 613 014c 08          	.byte 0x8
 614 014d 07          	.byte 0x7
 615 014e 6C 6F 6E 67 	.asciz "long long unsigned int"
 615      20 6C 6F 6E 
 615      67 20 75 6E 
 615      73 69 67 6E 
 615      65 64 20 69 
 615      6E 74 00 
 616 0165 04          	.uleb128 0x4
 617 0166 02          	.byte 0x2
 618 0167 03          	.byte 0x3
 619 0168 12          	.byte 0x12
 620 0169 96 01 00 00 	.4byte 0x196
 621 016d 05          	.uleb128 0x5
 622 016e 64 65 76 5F 	.asciz "dev_id"
 622      69 64 00 
 623 0175 03          	.byte 0x3
 624 0176 13          	.byte 0x13
 625 0177 16 01 00 00 	.4byte 0x116
 626 017b 02          	.byte 0x2
 627 017c 05          	.byte 0x5
 628 017d 0B          	.byte 0xb
 629 017e 02          	.byte 0x2
 630 017f 23          	.byte 0x23
 631 0180 00          	.uleb128 0x0
 632 0181 05          	.uleb128 0x5
 633 0182 6D 73 67 5F 	.asciz "msg_id"
 633      69 64 00 
 634 0189 03          	.byte 0x3
 635 018a 16          	.byte 0x16
 636 018b 16 01 00 00 	.4byte 0x116
 637 018f 02          	.byte 0x2
 638 0190 06          	.byte 0x6
 639 0191 05          	.byte 0x5
 640 0192 02          	.byte 0x2
 641 0193 23          	.byte 0x23
 642 0194 00          	.uleb128 0x0
 643 0195 00          	.byte 0x0
 644 0196 06          	.uleb128 0x6
 645 0197 02          	.byte 0x2
 646 0198 03          	.byte 0x3
 647 0199 11          	.byte 0x11
 648 019a AF 01 00 00 	.4byte 0x1af
 649 019e 07          	.uleb128 0x7
 650 019f 65 01 00 00 	.4byte 0x165
 651 01a3 08          	.uleb128 0x8
 652 01a4 73 69 64 00 	.asciz "sid"
 653 01a8 03          	.byte 0x3
 654 01a9 18          	.byte 0x18
 655 01aa 16 01 00 00 	.4byte 0x116
 656 01ae 00          	.byte 0x0
 657 01af 04          	.uleb128 0x4
 658 01b0 0C          	.byte 0xc
 659 01b1 03          	.byte 0x3
 660 01b2 10          	.byte 0x10
 661 01b3 E0 01 00 00 	.4byte 0x1e0
 662 01b7 09          	.uleb128 0x9
MPLAB XC16 ASSEMBLY Listing:   			page 15


 663 01b8 96 01 00 00 	.4byte 0x196
 664 01bc 02          	.byte 0x2
 665 01bd 23          	.byte 0x23
 666 01be 00          	.uleb128 0x0
 667 01bf 05          	.uleb128 0x5
 668 01c0 64 6C 63 00 	.asciz "dlc"
 669 01c4 03          	.byte 0x3
 670 01c5 1A          	.byte 0x1a
 671 01c6 16 01 00 00 	.4byte 0x116
 672 01ca 02          	.byte 0x2
 673 01cb 04          	.byte 0x4
 674 01cc 0C          	.byte 0xc
 675 01cd 02          	.byte 0x2
 676 01ce 23          	.byte 0x23
 677 01cf 02          	.uleb128 0x2
 678 01d0 0A          	.uleb128 0xa
 679 01d1 64 61 74 61 	.asciz "data"
 679      00 
 680 01d6 03          	.byte 0x3
 681 01d7 1B          	.byte 0x1b
 682 01d8 E0 01 00 00 	.4byte 0x1e0
 683 01dc 02          	.byte 0x2
 684 01dd 23          	.byte 0x23
 685 01de 04          	.uleb128 0x4
 686 01df 00          	.byte 0x0
 687 01e0 0B          	.uleb128 0xb
 688 01e1 16 01 00 00 	.4byte 0x116
 689 01e5 F0 01 00 00 	.4byte 0x1f0
 690 01e9 0C          	.uleb128 0xc
 691 01ea 26 01 00 00 	.4byte 0x126
 692 01ee 03          	.byte 0x3
 693 01ef 00          	.byte 0x0
 694 01f0 03          	.uleb128 0x3
 695 01f1 43 41 4E 64 	.asciz "CANdata"
 695      61 74 61 00 
 696 01f9 03          	.byte 0x3
 697 01fa 1C          	.byte 0x1c
 698 01fb AF 01 00 00 	.4byte 0x1af
 699 01ff 0D          	.uleb128 0xd
 700 0200 02          	.byte 0x2
 701 0201 04          	.byte 0x4
 702 0202 20          	.byte 0x20
 703 0203 58 03 00 00 	.4byte 0x358
 704 0207 0E          	.uleb128 0xe
 705 0208 54 53 5F 4F 	.asciz "TS_OFF_REASON_DASH_BUTTON"
 705      46 46 5F 52 
 705      45 41 53 4F 
 705      4E 5F 44 41 
 705      53 48 5F 42 
 705      55 54 54 4F 
 705      4E 00 
 706 0222 00          	.sleb128 0
 707 0223 0E          	.uleb128 0xe
 708 0224 54 53 5F 4F 	.asciz "TS_OFF_REASON_OVERVOLTAGE"
 708      46 46 5F 52 
 708      45 41 53 4F 
 708      4E 5F 4F 56 
MPLAB XC16 ASSEMBLY Listing:   			page 16


 708      45 52 56 4F 
 708      4C 54 41 47 
 708      45 00 
 709 023e 01          	.sleb128 1
 710 023f 0E          	.uleb128 0xe
 711 0240 54 53 5F 4F 	.asciz "TS_OFF_REASON_UNDERVOLTAGE"
 711      46 46 5F 52 
 711      45 41 53 4F 
 711      4E 5F 55 4E 
 711      44 45 52 56 
 711      4F 4C 54 41 
 711      47 45 00 
 712 025b 02          	.sleb128 2
 713 025c 0E          	.uleb128 0xe
 714 025d 54 53 5F 4F 	.asciz "TS_OFF_REASON_OVERCURRENT_IN"
 714      46 46 5F 52 
 714      45 41 53 4F 
 714      4E 5F 4F 56 
 714      45 52 43 55 
 714      52 52 45 4E 
 714      54 5F 49 4E 
 714      00 
 715 027a 03          	.sleb128 3
 716 027b 0E          	.uleb128 0xe
 717 027c 54 53 5F 4F 	.asciz "TS_OFF_REASON_OVERCURRENT_OUT"
 717      46 46 5F 52 
 717      45 41 53 4F 
 717      4E 5F 4F 56 
 717      45 52 43 55 
 717      52 52 45 4E 
 717      54 5F 4F 55 
 717      54 00 
 718 029a 04          	.sleb128 4
 719 029b 0E          	.uleb128 0xe
 720 029c 54 53 5F 4F 	.asciz "TS_OFF_REASON_OVERTEMPERATURE"
 720      46 46 5F 52 
 720      45 41 53 4F 
 720      4E 5F 4F 56 
 720      45 52 54 45 
 720      4D 50 45 52 
 720      41 54 55 52 
 720      45 00 
 721 02ba 05          	.sleb128 5
 722 02bb 0E          	.uleb128 0xe
 723 02bc 54 53 5F 4F 	.asciz "TS_OFF_REASON_UNDERTEMPERATURE"
 723      46 46 5F 52 
 723      45 41 53 4F 
 723      4E 5F 55 4E 
 723      44 45 52 54 
 723      45 4D 50 45 
 723      52 41 54 55 
 723      52 45 00 
 724 02db 06          	.sleb128 6
 725 02dc 0E          	.uleb128 0xe
 726 02dd 54 53 5F 4F 	.asciz "TS_OFF_REASON_MIN_CELL_LTC"
 726      46 46 5F 52 
 726      45 41 53 4F 
MPLAB XC16 ASSEMBLY Listing:   			page 17


 726      4E 5F 4D 49 
 726      4E 5F 43 45 
 726      4C 4C 5F 4C 
 726      54 43 00 
 727 02f8 07          	.sleb128 7
 728 02f9 0E          	.uleb128 0xe
 729 02fa 54 53 5F 4F 	.asciz "TS_OFF_REASON_IMD"
 729      46 46 5F 52 
 729      45 41 53 4F 
 729      4E 5F 49 4D 
 729      44 00 
 730 030c 08          	.sleb128 8
 731 030d 0E          	.uleb128 0xe
 732 030e 54 53 5F 4F 	.asciz "TS_OFF_REASON_FAKE_ERROR"
 732      46 46 5F 52 
 732      45 41 53 4F 
 732      4E 5F 46 41 
 732      4B 45 5F 45 
 732      52 52 4F 52 
 732      00 
 733 0327 09          	.sleb128 9
 734 0328 0E          	.uleb128 0xe
 735 0329 54 53 5F 4F 	.asciz "TS_OFF_REASON_OTHER"
 735      46 46 5F 52 
 735      45 41 53 4F 
 735      4E 5F 4F 54 
 735      48 45 52 00 
 736 033d 0A          	.sleb128 10
 737 033e 0E          	.uleb128 0xe
 738 033f 54 53 5F 4F 	.asciz "TS_OFF_REASON_NO_ERROR"
 738      46 46 5F 52 
 738      45 41 53 4F 
 738      4E 5F 4E 4F 
 738      5F 45 52 52 
 738      4F 52 00 
 739 0356 0B          	.sleb128 11
 740 0357 00          	.byte 0x0
 741 0358 03          	.uleb128 0x3
 742 0359 4D 41 53 54 	.asciz "MASTER_MSG_TS_OFF_Reason"
 742      45 52 5F 4D 
 742      53 47 5F 54 
 742      53 5F 4F 46 
 742      46 5F 52 65 
 742      61 73 6F 6E 
 742      00 
 743 0372 04          	.byte 0x4
 744 0373 2D          	.byte 0x2d
 745 0374 FF 01 00 00 	.4byte 0x1ff
 746 0378 04          	.uleb128 0x4
 747 0379 02          	.byte 0x2
 748 037a 04          	.byte 0x4
 749 037b 35          	.byte 0x35
 750 037c A8 03 00 00 	.4byte 0x3a8
 751 0380 05          	.uleb128 0x5
 752 0381 73 74 61 74 	.asciz "state"
 752      65 00 
 753 0387 04          	.byte 0x4
MPLAB XC16 ASSEMBLY Listing:   			page 18


 754 0388 36          	.byte 0x36
 755 0389 A8 03 00 00 	.4byte 0x3a8
 756 038d 01          	.byte 0x1
 757 038e 01          	.byte 0x1
 758 038f 07          	.byte 0x7
 759 0390 02          	.byte 0x2
 760 0391 23          	.byte 0x23
 761 0392 00          	.uleb128 0x0
 762 0393 05          	.uleb128 0x5
 763 0394 72 65 61 73 	.asciz "reason"
 763      6F 6E 00 
 764 039b 04          	.byte 0x4
 765 039c 37          	.byte 0x37
 766 039d 58 03 00 00 	.4byte 0x358
 767 03a1 02          	.byte 0x2
 768 03a2 07          	.byte 0x7
 769 03a3 08          	.byte 0x8
 770 03a4 02          	.byte 0x2
 771 03a5 23          	.byte 0x23
 772 03a6 00          	.uleb128 0x0
 773 03a7 00          	.byte 0x0
 774 03a8 02          	.uleb128 0x2
 775 03a9 01          	.byte 0x1
 776 03aa 02          	.byte 0x2
 777 03ab 5F 42 6F 6F 	.asciz "_Bool"
 777      6C 00 
 778 03b1 03          	.uleb128 0x3
 779 03b2 4D 41 53 54 	.asciz "MASTER_MSG_TS"
 779      45 52 5F 4D 
 779      53 47 5F 54 
 779      53 00 
 780 03c0 04          	.byte 0x4
 781 03c1 38          	.byte 0x38
 782 03c2 78 03 00 00 	.4byte 0x378
 783 03c6 04          	.uleb128 0x4
 784 03c7 08          	.byte 0x8
 785 03c8 04          	.byte 0x4
 786 03c9 3F          	.byte 0x3f
 787 03ca 11 04 00 00 	.4byte 0x411
 788 03ce 0A          	.uleb128 0xa
 789 03cf 76 6F 6C 74 	.asciz "voltage"
 789      61 67 65 00 
 790 03d7 04          	.byte 0x4
 791 03d8 40          	.byte 0x40
 792 03d9 16 01 00 00 	.4byte 0x116
 793 03dd 02          	.byte 0x2
 794 03de 23          	.byte 0x23
 795 03df 00          	.uleb128 0x0
 796 03e0 0A          	.uleb128 0xa
 797 03e1 63 75 72 72 	.asciz "current"
 797      65 6E 74 00 
 798 03e9 04          	.byte 0x4
 799 03ea 48          	.byte 0x48
 800 03eb C3 00 00 00 	.4byte 0xc3
 801 03ef 02          	.byte 0x2
 802 03f0 23          	.byte 0x23
 803 03f1 02          	.uleb128 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 19


 804 03f2 0A          	.uleb128 0xa
 805 03f3 70 6F 77 65 	.asciz "power"
 805      72 00 
 806 03f9 04          	.byte 0x4
 807 03fa 51          	.byte 0x51
 808 03fb C3 00 00 00 	.4byte 0xc3
 809 03ff 02          	.byte 0x2
 810 0400 23          	.byte 0x23
 811 0401 04          	.uleb128 0x4
 812 0402 0A          	.uleb128 0xa
 813 0403 53 6F 43 00 	.asciz "SoC"
 814 0407 04          	.byte 0x4
 815 0408 53          	.byte 0x53
 816 0409 16 01 00 00 	.4byte 0x116
 817 040d 02          	.byte 0x2
 818 040e 23          	.byte 0x23
 819 040f 06          	.uleb128 0x6
 820 0410 00          	.byte 0x0
 821 0411 03          	.uleb128 0x3
 822 0412 4D 41 53 54 	.asciz "MASTER_MSG_Energy_Meter"
 822      45 52 5F 4D 
 822      53 47 5F 45 
 822      6E 65 72 67 
 822      79 5F 4D 65 
 822      74 65 72 00 
 823 042a 04          	.byte 0x4
 824 042b 54          	.byte 0x54
 825 042c C6 03 00 00 	.4byte 0x3c6
 826 0430 04          	.uleb128 0x4
 827 0431 02          	.byte 0x2
 828 0432 04          	.byte 0x4
 829 0433 5B          	.byte 0x5b
 830 0434 B6 05 00 00 	.4byte 0x5b6
 831 0438 05          	.uleb128 0x5
 832 0439 41 4D 53 5F 	.asciz "AMS_OK"
 832      4F 4B 00 
 833 0440 04          	.byte 0x4
 834 0441 60          	.byte 0x60
 835 0442 A8 03 00 00 	.4byte 0x3a8
 836 0446 01          	.byte 0x1
 837 0447 01          	.byte 0x1
 838 0448 07          	.byte 0x7
 839 0449 02          	.byte 0x2
 840 044a 23          	.byte 0x23
 841 044b 00          	.uleb128 0x0
 842 044c 05          	.uleb128 0x5
 843 044d 49 4D 44 5F 	.asciz "IMD_OK"
 843      4F 4B 00 
 844 0454 04          	.byte 0x4
 845 0455 61          	.byte 0x61
 846 0456 A8 03 00 00 	.4byte 0x3a8
 847 045a 01          	.byte 0x1
 848 045b 01          	.byte 0x1
 849 045c 06          	.byte 0x6
 850 045d 02          	.byte 0x2
 851 045e 23          	.byte 0x23
 852 045f 00          	.uleb128 0x0
MPLAB XC16 ASSEMBLY Listing:   			page 20


 853 0460 05          	.uleb128 0x5
 854 0461 49 4D 44 5F 	.asciz "IMD_LATCH"
 854      4C 41 54 43 
 854      48 00 
 855 046b 04          	.byte 0x4
 856 046c 68          	.byte 0x68
 857 046d A8 03 00 00 	.4byte 0x3a8
 858 0471 01          	.byte 0x1
 859 0472 01          	.byte 0x1
 860 0473 05          	.byte 0x5
 861 0474 02          	.byte 0x2
 862 0475 23          	.byte 0x23
 863 0476 00          	.uleb128 0x0
 864 0477 05          	.uleb128 0x5
 865 0478 41 4D 53 5F 	.asciz "AMS_LATCH"
 865      4C 41 54 43 
 865      48 00 
 866 0482 04          	.byte 0x4
 867 0483 69          	.byte 0x69
 868 0484 A8 03 00 00 	.4byte 0x3a8
 869 0488 01          	.byte 0x1
 870 0489 01          	.byte 0x1
 871 048a 04          	.byte 0x4
 872 048b 02          	.byte 0x2
 873 048c 23          	.byte 0x23
 874 048d 00          	.uleb128 0x0
 875 048e 05          	.uleb128 0x5
 876 048f 41 49 52 5F 	.asciz "AIR_positive"
 876      70 6F 73 69 
 876      74 69 76 65 
 876      00 
 877 049c 04          	.byte 0x4
 878 049d 70          	.byte 0x70
 879 049e A8 03 00 00 	.4byte 0x3a8
 880 04a2 01          	.byte 0x1
 881 04a3 01          	.byte 0x1
 882 04a4 03          	.byte 0x3
 883 04a5 02          	.byte 0x2
 884 04a6 23          	.byte 0x23
 885 04a7 00          	.uleb128 0x0
 886 04a8 05          	.uleb128 0x5
 887 04a9 41 49 52 5F 	.asciz "AIR_negative"
 887      6E 65 67 61 
 887      74 69 76 65 
 887      00 
 888 04b6 04          	.byte 0x4
 889 04b7 71          	.byte 0x71
 890 04b8 A8 03 00 00 	.4byte 0x3a8
 891 04bc 01          	.byte 0x1
 892 04bd 01          	.byte 0x1
 893 04be 02          	.byte 0x2
 894 04bf 02          	.byte 0x2
 895 04c0 23          	.byte 0x23
 896 04c1 00          	.uleb128 0x0
 897 04c2 05          	.uleb128 0x5
 898 04c3 50 72 65 43 	.asciz "PreChK"
 898      68 4B 00 
MPLAB XC16 ASSEMBLY Listing:   			page 21


 899 04ca 04          	.byte 0x4
 900 04cb 72          	.byte 0x72
 901 04cc A8 03 00 00 	.4byte 0x3a8
 902 04d0 01          	.byte 0x1
 903 04d1 01          	.byte 0x1
 904 04d2 01          	.byte 0x1
 905 04d3 02          	.byte 0x2
 906 04d4 23          	.byte 0x23
 907 04d5 00          	.uleb128 0x0
 908 04d6 05          	.uleb128 0x5
 909 04d7 44 69 73 43 	.asciz "DisChK"
 909      68 4B 00 
 910 04de 04          	.byte 0x4
 911 04df 73          	.byte 0x73
 912 04e0 A8 03 00 00 	.4byte 0x3a8
 913 04e4 01          	.byte 0x1
 914 04e5 01          	.byte 0x1
 915 04e6 08          	.byte 0x8
 916 04e7 02          	.byte 0x2
 917 04e8 23          	.byte 0x23
 918 04e9 00          	.uleb128 0x0
 919 04ea 05          	.uleb128 0x5
 920 04eb 53 43 5F 44 	.asciz "SC_DCU_IMD"
 920      43 55 5F 49 
 920      4D 44 00 
 921 04f6 04          	.byte 0x4
 922 04f7 7A          	.byte 0x7a
 923 04f8 A8 03 00 00 	.4byte 0x3a8
 924 04fc 01          	.byte 0x1
 925 04fd 01          	.byte 0x1
 926 04fe 07          	.byte 0x7
 927 04ff 02          	.byte 0x2
 928 0500 23          	.byte 0x23
 929 0501 01          	.uleb128 0x1
 930 0502 05          	.uleb128 0x5
 931 0503 53 43 5F 49 	.asciz "SC_IMD_AMS"
 931      4D 44 5F 41 
 931      4D 53 00 
 932 050e 04          	.byte 0x4
 933 050f 7B          	.byte 0x7b
 934 0510 A8 03 00 00 	.4byte 0x3a8
 935 0514 01          	.byte 0x1
 936 0515 01          	.byte 0x1
 937 0516 06          	.byte 0x6
 938 0517 02          	.byte 0x2
 939 0518 23          	.byte 0x23
 940 0519 01          	.uleb128 0x1
 941 051a 05          	.uleb128 0x5
 942 051b 53 43 5F 41 	.asciz "SC_AMS_DCU"
 942      4D 53 5F 44 
 942      43 55 00 
 943 0526 04          	.byte 0x4
 944 0527 7C          	.byte 0x7c
 945 0528 A8 03 00 00 	.4byte 0x3a8
 946 052c 01          	.byte 0x1
 947 052d 01          	.byte 0x1
 948 052e 05          	.byte 0x5
MPLAB XC16 ASSEMBLY Listing:   			page 22


 949 052f 02          	.byte 0x2
 950 0530 23          	.byte 0x23
 951 0531 01          	.uleb128 0x1
 952 0532 05          	.uleb128 0x5
 953 0533 53 43 5F 54 	.asciz "SC_TSMS_Relays"
 953      53 4D 53 5F 
 953      52 65 6C 61 
 953      79 73 00 
 954 0542 04          	.byte 0x4
 955 0543 7D          	.byte 0x7d
 956 0544 A8 03 00 00 	.4byte 0x3a8
 957 0548 01          	.byte 0x1
 958 0549 01          	.byte 0x1
 959 054a 04          	.byte 0x4
 960 054b 02          	.byte 0x2
 961 054c 23          	.byte 0x23
 962 054d 01          	.uleb128 0x1
 963 054e 05          	.uleb128 0x5
 964 054f 73 68 75 74 	.asciz "shutdown_above_minimum"
 964      64 6F 77 6E 
 964      5F 61 62 6F 
 964      76 65 5F 6D 
 964      69 6E 69 6D 
 964      75 6D 00 
 965 0566 04          	.byte 0x4
 966 0567 84          	.byte 0x84
 967 0568 A8 03 00 00 	.4byte 0x3a8
 968 056c 01          	.byte 0x1
 969 056d 01          	.byte 0x1
 970 056e 03          	.byte 0x3
 971 056f 02          	.byte 0x2
 972 0570 23          	.byte 0x23
 973 0571 01          	.uleb128 0x1
 974 0572 05          	.uleb128 0x5
 975 0573 73 68 75 74 	.asciz "shutdown_open"
 975      64 6F 77 6E 
 975      5F 6F 70 65 
 975      6E 00 
 976 0581 04          	.byte 0x4
 977 0582 85          	.byte 0x85
 978 0583 A8 03 00 00 	.4byte 0x3a8
 979 0587 01          	.byte 0x1
 980 0588 01          	.byte 0x1
 981 0589 02          	.byte 0x2
 982 058a 02          	.byte 0x2
 983 058b 23          	.byte 0x23
 984 058c 01          	.uleb128 0x1
 985 058d 05          	.uleb128 0x5
 986 058e 76 65 72 62 	.asciz "verbose"
 986      6F 73 65 00 
 987 0596 04          	.byte 0x4
 988 0597 88          	.byte 0x88
 989 0598 A8 03 00 00 	.4byte 0x3a8
 990 059c 01          	.byte 0x1
 991 059d 01          	.byte 0x1
 992 059e 01          	.byte 0x1
 993 059f 02          	.byte 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 23


 994 05a0 23          	.byte 0x23
 995 05a1 01          	.uleb128 0x1
 996 05a2 05          	.uleb128 0x5
 997 05a3 74 73 5F 6F 	.asciz "ts_on"
 997      6E 00 
 998 05a9 04          	.byte 0x4
 999 05aa 89          	.byte 0x89
 1000 05ab A8 03 00 00 	.4byte 0x3a8
 1001 05af 01          	.byte 0x1
 1002 05b0 01          	.byte 0x1
 1003 05b1 08          	.byte 0x8
 1004 05b2 02          	.byte 0x2
 1005 05b3 23          	.byte 0x23
 1006 05b4 01          	.uleb128 0x1
 1007 05b5 00          	.byte 0x0
 1008 05b6 06          	.uleb128 0x6
 1009 05b7 02          	.byte 0x2
 1010 05b8 04          	.byte 0x4
 1011 05b9 5A          	.byte 0x5a
 1012 05ba CF 05 00 00 	.4byte 0x5cf
 1013 05be 07          	.uleb128 0x7
 1014 05bf 30 04 00 00 	.4byte 0x430
 1015 05c3 0F          	.uleb128 0xf
 1016 05c4 00 00 00 00 	.4byte .LASF0
 1017 05c8 04          	.byte 0x4
 1018 05c9 8B          	.byte 0x8b
 1019 05ca 16 01 00 00 	.4byte 0x116
 1020 05ce 00          	.byte 0x0
 1021 05cf 04          	.uleb128 0x4
 1022 05d0 02          	.byte 0x2
 1023 05d1 04          	.byte 0x4
 1024 05d2 59          	.byte 0x59
 1025 05d3 E0 05 00 00 	.4byte 0x5e0
 1026 05d7 09          	.uleb128 0x9
 1027 05d8 B6 05 00 00 	.4byte 0x5b6
 1028 05dc 02          	.byte 0x2
 1029 05dd 23          	.byte 0x23
 1030 05de 00          	.uleb128 0x0
 1031 05df 00          	.byte 0x0
 1032 05e0 03          	.uleb128 0x3
 1033 05e1 4D 41 53 54 	.asciz "MASTER_MSG_Status"
 1033      45 52 5F 4D 
 1033      53 47 5F 53 
 1033      74 61 74 75 
 1033      73 00 
 1034 05f3 04          	.byte 0x4
 1035 05f4 8D          	.byte 0x8d
 1036 05f5 CF 05 00 00 	.4byte 0x5cf
 1037 05f9 04          	.uleb128 0x4
 1038 05fa 08          	.byte 0x8
 1039 05fb 04          	.byte 0x4
 1040 05fc 94          	.byte 0x94
 1041 05fd 5E 06 00 00 	.4byte 0x65e
 1042 0601 0A          	.uleb128 0xa
 1043 0602 6D 69 6E 5F 	.asciz "min_voltage"
 1043      76 6F 6C 74 
 1043      61 67 65 00 
MPLAB XC16 ASSEMBLY Listing:   			page 24


 1044 060e 04          	.byte 0x4
 1045 060f 95          	.byte 0x95
 1046 0610 16 01 00 00 	.4byte 0x116
 1047 0614 02          	.byte 0x2
 1048 0615 23          	.byte 0x23
 1049 0616 00          	.uleb128 0x0
 1050 0617 0A          	.uleb128 0xa
 1051 0618 6D 65 61 6E 	.asciz "mean_voltage"
 1051      5F 76 6F 6C 
 1051      74 61 67 65 
 1051      00 
 1052 0625 04          	.byte 0x4
 1053 0626 96          	.byte 0x96
 1054 0627 16 01 00 00 	.4byte 0x116
 1055 062b 02          	.byte 0x2
 1056 062c 23          	.byte 0x23
 1057 062d 02          	.uleb128 0x2
 1058 062e 0A          	.uleb128 0xa
 1059 062f 6D 61 78 5F 	.asciz "max_voltage"
 1059      76 6F 6C 74 
 1059      61 67 65 00 
 1060 063b 04          	.byte 0x4
 1061 063c 97          	.byte 0x97
 1062 063d 16 01 00 00 	.4byte 0x116
 1063 0641 02          	.byte 0x2
 1064 0642 23          	.byte 0x23
 1065 0643 04          	.uleb128 0x4
 1066 0644 0A          	.uleb128 0xa
 1067 0645 62 61 6C 61 	.asciz "balance_target"
 1067      6E 63 65 5F 
 1067      74 61 72 67 
 1067      65 74 00 
 1068 0654 04          	.byte 0x4
 1069 0655 98          	.byte 0x98
 1070 0656 5E 06 00 00 	.4byte 0x65e
 1071 065a 02          	.byte 0x2
 1072 065b 23          	.byte 0x23
 1073 065c 06          	.uleb128 0x6
 1074 065d 00          	.byte 0x0
 1075 065e 0B          	.uleb128 0xb
 1076 065f F6 00 00 00 	.4byte 0xf6
 1077 0663 6E 06 00 00 	.4byte 0x66e
 1078 0667 0C          	.uleb128 0xc
 1079 0668 26 01 00 00 	.4byte 0x126
 1080 066c 01          	.byte 0x1
 1081 066d 00          	.byte 0x0
 1082 066e 03          	.uleb128 0x3
 1083 066f 4D 41 53 54 	.asciz "MASTER_MSG_Battery_Voltage_Info"
 1083      45 52 5F 4D 
 1083      53 47 5F 42 
 1083      61 74 74 65 
 1083      72 79 5F 56 
 1083      6F 6C 74 61 
 1083      67 65 5F 49 
 1083      6E 66 6F 00 
 1084 068f 04          	.byte 0x4
 1085 0690 99          	.byte 0x99
MPLAB XC16 ASSEMBLY Listing:   			page 25


 1086 0691 F9 05 00 00 	.4byte 0x5f9
 1087 0695 04          	.uleb128 0x4
 1088 0696 08          	.byte 0x8
 1089 0697 04          	.byte 0x4
 1090 0698 A1          	.byte 0xa1
 1091 0699 04 07 00 00 	.4byte 0x704
 1092 069d 0A          	.uleb128 0xa
 1093 069e 6D 69 6E 5F 	.asciz "min_temperature"
 1093      74 65 6D 70 
 1093      65 72 61 74 
 1093      75 72 65 00 
 1094 06ae 04          	.byte 0x4
 1095 06af A2          	.byte 0xa2
 1096 06b0 16 01 00 00 	.4byte 0x116
 1097 06b4 02          	.byte 0x2
 1098 06b5 23          	.byte 0x23
 1099 06b6 00          	.uleb128 0x0
 1100 06b7 0A          	.uleb128 0xa
 1101 06b8 6D 65 61 6E 	.asciz "mean_temperature"
 1101      5F 74 65 6D 
 1101      70 65 72 61 
 1101      74 75 72 65 
 1101      00 
 1102 06c9 04          	.byte 0x4
 1103 06ca A3          	.byte 0xa3
 1104 06cb 16 01 00 00 	.4byte 0x116
 1105 06cf 02          	.byte 0x2
 1106 06d0 23          	.byte 0x23
 1107 06d1 02          	.uleb128 0x2
 1108 06d2 0A          	.uleb128 0xa
 1109 06d3 6D 61 78 5F 	.asciz "max_temperature"
 1109      74 65 6D 70 
 1109      65 72 61 74 
 1109      75 72 65 00 
 1110 06e3 04          	.byte 0x4
 1111 06e4 A4          	.byte 0xa4
 1112 06e5 16 01 00 00 	.4byte 0x116
 1113 06e9 02          	.byte 0x2
 1114 06ea 23          	.byte 0x23
 1115 06eb 04          	.uleb128 0x4
 1116 06ec 0A          	.uleb128 0xa
 1117 06ed 68 6F 74 74 	.asciz "hottest_cell"
 1117      65 73 74 5F 
 1117      63 65 6C 6C 
 1117      00 
 1118 06fa 04          	.byte 0x4
 1119 06fb A5          	.byte 0xa5
 1120 06fc 5E 06 00 00 	.4byte 0x65e
 1121 0700 02          	.byte 0x2
 1122 0701 23          	.byte 0x23
 1123 0702 06          	.uleb128 0x6
 1124 0703 00          	.byte 0x0
 1125 0704 03          	.uleb128 0x3
 1126 0705 4D 41 53 54 	.asciz "MASTER_MSG_Battery_Temp_Info"
 1126      45 52 5F 4D 
 1126      53 47 5F 42 
 1126      61 74 74 65 
MPLAB XC16 ASSEMBLY Listing:   			page 26


 1126      72 79 5F 54 
 1126      65 6D 70 5F 
 1126      49 6E 66 6F 
 1126      00 
 1127 0722 04          	.byte 0x4
 1128 0723 A6          	.byte 0xa6
 1129 0724 95 06 00 00 	.4byte 0x695
 1130 0728 04          	.uleb128 0x4
 1131 0729 20          	.byte 0x20
 1132 072a 04          	.byte 0x4
 1133 072b AB          	.byte 0xab
 1134 072c B0 07 00 00 	.4byte 0x7b0
 1135 0730 10          	.uleb128 0x10
 1136 0731 00 00 00 00 	.4byte .LASF0
 1137 0735 04          	.byte 0x4
 1138 0736 AC          	.byte 0xac
 1139 0737 E0 05 00 00 	.4byte 0x5e0
 1140 073b 02          	.byte 0x2
 1141 073c 23          	.byte 0x23
 1142 073d 00          	.uleb128 0x0
 1143 073e 0A          	.uleb128 0xa
 1144 073f 54 53 00    	.asciz "TS"
 1145 0742 04          	.byte 0x4
 1146 0743 AD          	.byte 0xad
 1147 0744 B1 03 00 00 	.4byte 0x3b1
 1148 0748 02          	.byte 0x2
 1149 0749 23          	.byte 0x23
 1150 074a 02          	.uleb128 0x2
 1151 074b 10          	.uleb128 0x10
 1152 074c 00 00 00 00 	.4byte .LASF1
 1153 0750 04          	.byte 0x4
 1154 0751 AE          	.byte 0xae
 1155 0752 11 04 00 00 	.4byte 0x411
 1156 0756 02          	.byte 0x2
 1157 0757 23          	.byte 0x23
 1158 0758 04          	.uleb128 0x4
 1159 0759 0A          	.uleb128 0xa
 1160 075a 69 6D 64 5F 	.asciz "imd_error"
 1160      65 72 72 6F 
 1160      72 00 
 1161 0764 04          	.byte 0x4
 1162 0765 AF          	.byte 0xaf
 1163 0766 A8 03 00 00 	.4byte 0x3a8
 1164 076a 02          	.byte 0x2
 1165 076b 23          	.byte 0x23
 1166 076c 0C          	.uleb128 0xc
 1167 076d 0A          	.uleb128 0xa
 1168 076e 61 6D 73 5F 	.asciz "ams_error"
 1168      65 72 72 6F 
 1168      72 00 
 1169 0778 04          	.byte 0x4
 1170 0779 B0          	.byte 0xb0
 1171 077a A8 03 00 00 	.4byte 0x3a8
 1172 077e 02          	.byte 0x2
 1173 077f 23          	.byte 0x23
 1174 0780 0D          	.uleb128 0xd
 1175 0781 0A          	.uleb128 0xa
MPLAB XC16 ASSEMBLY Listing:   			page 27


 1176 0782 73 63 5F 6F 	.asciz "sc_open"
 1176      70 65 6E 00 
 1177 078a 04          	.byte 0x4
 1178 078b B1          	.byte 0xb1
 1179 078c A8 03 00 00 	.4byte 0x3a8
 1180 0790 02          	.byte 0x2
 1181 0791 23          	.byte 0x23
 1182 0792 0E          	.uleb128 0xe
 1183 0793 10          	.uleb128 0x10
 1184 0794 00 00 00 00 	.4byte .LASF2
 1185 0798 04          	.byte 0x4
 1186 0799 B2          	.byte 0xb2
 1187 079a 6E 06 00 00 	.4byte 0x66e
 1188 079e 02          	.byte 0x2
 1189 079f 23          	.byte 0x23
 1190 07a0 10          	.uleb128 0x10
 1191 07a1 10          	.uleb128 0x10
 1192 07a2 00 00 00 00 	.4byte .LASF3
 1193 07a6 04          	.byte 0x4
 1194 07a7 B3          	.byte 0xb3
 1195 07a8 04 07 00 00 	.4byte 0x704
 1196 07ac 02          	.byte 0x2
 1197 07ad 23          	.byte 0x23
 1198 07ae 18          	.uleb128 0x18
 1199 07af 00          	.byte 0x0
 1200 07b0 03          	.uleb128 0x3
 1201 07b1 4D 41 53 54 	.asciz "MASTER_MSG_Data"
 1201      45 52 5F 4D 
 1201      53 47 5F 44 
 1201      61 74 61 00 
 1202 07c1 04          	.byte 0x4
 1203 07c2 B4          	.byte 0xb4
 1204 07c3 28 07 00 00 	.4byte 0x728
 1205 07c7 11          	.uleb128 0x11
 1206 07c8 01          	.byte 0x1
 1207 07c9 70 61 72 73 	.asciz "parse_can_message_master_ts"
 1207      65 5F 63 61 
 1207      6E 5F 6D 65 
 1207      73 73 61 67 
 1207      65 5F 6D 61 
 1207      73 74 65 72 
 1207      5F 74 73 00 
 1208 07e5 01          	.byte 0x1
 1209 07e6 08          	.byte 0x8
 1210 07e7 01          	.byte 0x1
 1211 07e8 00 00 00 00 	.4byte .LFB0
 1212 07ec 00 00 00 00 	.4byte .LFE0
 1213 07f0 01          	.byte 0x1
 1214 07f1 5E          	.byte 0x5e
 1215 07f2 12 08 00 00 	.4byte 0x812
 1216 07f6 12          	.uleb128 0x12
 1217 07f7 6D 73 67 00 	.asciz "msg"
 1218 07fb 01          	.byte 0x1
 1219 07fc 08          	.byte 0x8
 1220 07fd F0 01 00 00 	.4byte 0x1f0
 1221 0801 02          	.byte 0x2
 1222 0802 7E          	.byte 0x7e
MPLAB XC16 ASSEMBLY Listing:   			page 28


 1223 0803 00          	.sleb128 0
 1224 0804 12          	.uleb128 0x12
 1225 0805 74 73 00    	.asciz "ts"
 1226 0808 01          	.byte 0x1
 1227 0809 08          	.byte 0x8
 1228 080a 12 08 00 00 	.4byte 0x812
 1229 080e 02          	.byte 0x2
 1230 080f 7E          	.byte 0x7e
 1231 0810 0C          	.sleb128 12
 1232 0811 00          	.byte 0x0
 1233 0812 13          	.uleb128 0x13
 1234 0813 02          	.byte 0x2
 1235 0814 B1 03 00 00 	.4byte 0x3b1
 1236 0818 11          	.uleb128 0x11
 1237 0819 01          	.byte 0x1
 1238 081a 70 61 72 73 	.asciz "parse_can_message_master_energy_meter"
 1238      65 5F 63 61 
 1238      6E 5F 6D 65 
 1238      73 73 61 67 
 1238      65 5F 6D 61 
 1238      73 74 65 72 
 1238      5F 65 6E 65 
 1238      72 67 79 5F 
 1238      6D 65 74 65 
 1239 0840 01          	.byte 0x1
 1240 0841 0D          	.byte 0xd
 1241 0842 01          	.byte 0x1
 1242 0843 00 00 00 00 	.4byte .LFB1
 1243 0847 00 00 00 00 	.4byte .LFE1
 1244 084b 01          	.byte 0x1
 1245 084c 5E          	.byte 0x5e
 1246 084d 6E 08 00 00 	.4byte 0x86e
 1247 0851 12          	.uleb128 0x12
 1248 0852 6D 73 67 00 	.asciz "msg"
 1249 0856 01          	.byte 0x1
 1250 0857 0D          	.byte 0xd
 1251 0858 F0 01 00 00 	.4byte 0x1f0
 1252 085c 02          	.byte 0x2
 1253 085d 7E          	.byte 0x7e
 1254 085e 00          	.sleb128 0
 1255 085f 14          	.uleb128 0x14
 1256 0860 00 00 00 00 	.4byte .LASF1
 1257 0864 01          	.byte 0x1
 1258 0865 0E          	.byte 0xe
 1259 0866 6E 08 00 00 	.4byte 0x86e
 1260 086a 02          	.byte 0x2
 1261 086b 7E          	.byte 0x7e
 1262 086c 0C          	.sleb128 12
 1263 086d 00          	.byte 0x0
 1264 086e 13          	.uleb128 0x13
 1265 086f 02          	.byte 0x2
 1266 0870 11 04 00 00 	.4byte 0x411
 1267 0874 11          	.uleb128 0x11
 1268 0875 01          	.byte 0x1
 1269 0876 70 61 72 73 	.asciz "parse_can_message_master_cell_voltage_info"
 1269      65 5F 63 61 
 1269      6E 5F 6D 65 
MPLAB XC16 ASSEMBLY Listing:   			page 29


 1269      73 73 61 67 
 1269      65 5F 6D 61 
 1269      73 74 65 72 
 1269      5F 63 65 6C 
 1269      6C 5F 76 6F 
 1269      6C 74 61 67 
 1270 08a1 01          	.byte 0x1
 1271 08a2 15          	.byte 0x15
 1272 08a3 01          	.byte 0x1
 1273 08a4 00 00 00 00 	.4byte .LFB2
 1274 08a8 00 00 00 00 	.4byte .LFE2
 1275 08ac 01          	.byte 0x1
 1276 08ad 5E          	.byte 0x5e
 1277 08ae CF 08 00 00 	.4byte 0x8cf
 1278 08b2 12          	.uleb128 0x12
 1279 08b3 6D 73 67 00 	.asciz "msg"
 1280 08b7 01          	.byte 0x1
 1281 08b8 15          	.byte 0x15
 1282 08b9 F0 01 00 00 	.4byte 0x1f0
 1283 08bd 02          	.byte 0x2
 1284 08be 7E          	.byte 0x7e
 1285 08bf 00          	.sleb128 0
 1286 08c0 14          	.uleb128 0x14
 1287 08c1 00 00 00 00 	.4byte .LASF2
 1288 08c5 01          	.byte 0x1
 1289 08c6 16          	.byte 0x16
 1290 08c7 CF 08 00 00 	.4byte 0x8cf
 1291 08cb 02          	.byte 0x2
 1292 08cc 7E          	.byte 0x7e
 1293 08cd 0C          	.sleb128 12
 1294 08ce 00          	.byte 0x0
 1295 08cf 13          	.uleb128 0x13
 1296 08d0 02          	.byte 0x2
 1297 08d1 6E 06 00 00 	.4byte 0x66e
 1298 08d5 11          	.uleb128 0x11
 1299 08d6 01          	.byte 0x1
 1300 08d7 70 61 72 73 	.asciz "parse_can_message_master_cell_temperature_info"
 1300      65 5F 63 61 
 1300      6E 5F 6D 65 
 1300      73 73 61 67 
 1300      65 5F 6D 61 
 1300      73 74 65 72 
 1300      5F 63 65 6C 
 1300      6C 5F 74 65 
 1300      6D 70 65 72 
 1301 0906 01          	.byte 0x1
 1302 0907 1F          	.byte 0x1f
 1303 0908 01          	.byte 0x1
 1304 0909 00 00 00 00 	.4byte .LFB3
 1305 090d 00 00 00 00 	.4byte .LFE3
 1306 0911 01          	.byte 0x1
 1307 0912 5E          	.byte 0x5e
 1308 0913 34 09 00 00 	.4byte 0x934
 1309 0917 12          	.uleb128 0x12
 1310 0918 6D 73 67 00 	.asciz "msg"
 1311 091c 01          	.byte 0x1
 1312 091d 1F          	.byte 0x1f
MPLAB XC16 ASSEMBLY Listing:   			page 30


 1313 091e F0 01 00 00 	.4byte 0x1f0
 1314 0922 02          	.byte 0x2
 1315 0923 7E          	.byte 0x7e
 1316 0924 00          	.sleb128 0
 1317 0925 14          	.uleb128 0x14
 1318 0926 00 00 00 00 	.4byte .LASF3
 1319 092a 01          	.byte 0x1
 1320 092b 20          	.byte 0x20
 1321 092c 34 09 00 00 	.4byte 0x934
 1322 0930 02          	.byte 0x2
 1323 0931 7E          	.byte 0x7e
 1324 0932 0C          	.sleb128 12
 1325 0933 00          	.byte 0x0
 1326 0934 13          	.uleb128 0x13
 1327 0935 02          	.byte 0x2
 1328 0936 04 07 00 00 	.4byte 0x704
 1329 093a 11          	.uleb128 0x11
 1330 093b 01          	.byte 0x1
 1331 093c 70 61 72 73 	.asciz "parse_can_message_master_status"
 1331      65 5F 63 61 
 1331      6E 5F 6D 65 
 1331      73 73 61 67 
 1331      65 5F 6D 61 
 1331      73 74 65 72 
 1331      5F 73 74 61 
 1331      74 75 73 00 
 1332 095c 01          	.byte 0x1
 1333 095d 29          	.byte 0x29
 1334 095e 01          	.byte 0x1
 1335 095f 00 00 00 00 	.4byte .LFB4
 1336 0963 00 00 00 00 	.4byte .LFE4
 1337 0967 01          	.byte 0x1
 1338 0968 5E          	.byte 0x5e
 1339 0969 8A 09 00 00 	.4byte 0x98a
 1340 096d 12          	.uleb128 0x12
 1341 096e 6D 73 67 00 	.asciz "msg"
 1342 0972 01          	.byte 0x1
 1343 0973 29          	.byte 0x29
 1344 0974 F0 01 00 00 	.4byte 0x1f0
 1345 0978 02          	.byte 0x2
 1346 0979 7E          	.byte 0x7e
 1347 097a 00          	.sleb128 0
 1348 097b 14          	.uleb128 0x14
 1349 097c 00 00 00 00 	.4byte .LASF0
 1350 0980 01          	.byte 0x1
 1351 0981 29          	.byte 0x29
 1352 0982 8A 09 00 00 	.4byte 0x98a
 1353 0986 02          	.byte 0x2
 1354 0987 7E          	.byte 0x7e
 1355 0988 0C          	.sleb128 12
 1356 0989 00          	.byte 0x0
 1357 098a 13          	.uleb128 0x13
 1358 098b 02          	.byte 0x2
 1359 098c E0 05 00 00 	.4byte 0x5e0
 1360 0990 11          	.uleb128 0x11
 1361 0991 01          	.byte 0x1
 1362 0992 70 61 72 73 	.asciz "parse_can_message_master"
MPLAB XC16 ASSEMBLY Listing:   			page 31


 1362      65 5F 63 61 
 1362      6E 5F 6D 65 
 1362      73 73 61 67 
 1362      65 5F 6D 61 
 1362      73 74 65 72 
 1362      00 
 1363 09ab 01          	.byte 0x1
 1364 09ac 2F          	.byte 0x2f
 1365 09ad 01          	.byte 0x1
 1366 09ae 00 00 00 00 	.4byte .LFB5
 1367 09b2 00 00 00 00 	.4byte .LFE5
 1368 09b6 01          	.byte 0x1
 1369 09b7 5E          	.byte 0x5e
 1370 09b8 DA 09 00 00 	.4byte 0x9da
 1371 09bc 12          	.uleb128 0x12
 1372 09bd 6D 73 67 00 	.asciz "msg"
 1373 09c1 01          	.byte 0x1
 1374 09c2 2F          	.byte 0x2f
 1375 09c3 F0 01 00 00 	.4byte 0x1f0
 1376 09c7 02          	.byte 0x2
 1377 09c8 7E          	.byte 0x7e
 1378 09c9 00          	.sleb128 0
 1379 09ca 12          	.uleb128 0x12
 1380 09cb 64 61 74 61 	.asciz "data"
 1380      00 
 1381 09d0 01          	.byte 0x1
 1382 09d1 2F          	.byte 0x2f
 1383 09d2 DA 09 00 00 	.4byte 0x9da
 1384 09d6 02          	.byte 0x2
 1385 09d7 7E          	.byte 0x7e
 1386 09d8 0C          	.sleb128 12
 1387 09d9 00          	.byte 0x0
 1388 09da 13          	.uleb128 0x13
 1389 09db 02          	.byte 0x2
 1390 09dc B0 07 00 00 	.4byte 0x7b0
 1391 09e0 00          	.byte 0x0
 1392                 	.section .debug_abbrev,info
 1393 0000 01          	.uleb128 0x1
 1394 0001 11          	.uleb128 0x11
 1395 0002 01          	.byte 0x1
 1396 0003 25          	.uleb128 0x25
 1397 0004 08          	.uleb128 0x8
 1398 0005 13          	.uleb128 0x13
 1399 0006 0B          	.uleb128 0xb
 1400 0007 03          	.uleb128 0x3
 1401 0008 08          	.uleb128 0x8
 1402 0009 1B          	.uleb128 0x1b
 1403 000a 08          	.uleb128 0x8
 1404 000b 11          	.uleb128 0x11
 1405 000c 01          	.uleb128 0x1
 1406 000d 12          	.uleb128 0x12
 1407 000e 01          	.uleb128 0x1
 1408 000f 10          	.uleb128 0x10
 1409 0010 06          	.uleb128 0x6
 1410 0011 00          	.byte 0x0
 1411 0012 00          	.byte 0x0
 1412 0013 02          	.uleb128 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 32


 1413 0014 24          	.uleb128 0x24
 1414 0015 00          	.byte 0x0
 1415 0016 0B          	.uleb128 0xb
 1416 0017 0B          	.uleb128 0xb
 1417 0018 3E          	.uleb128 0x3e
 1418 0019 0B          	.uleb128 0xb
 1419 001a 03          	.uleb128 0x3
 1420 001b 08          	.uleb128 0x8
 1421 001c 00          	.byte 0x0
 1422 001d 00          	.byte 0x0
 1423 001e 03          	.uleb128 0x3
 1424 001f 16          	.uleb128 0x16
 1425 0020 00          	.byte 0x0
 1426 0021 03          	.uleb128 0x3
 1427 0022 08          	.uleb128 0x8
 1428 0023 3A          	.uleb128 0x3a
 1429 0024 0B          	.uleb128 0xb
 1430 0025 3B          	.uleb128 0x3b
 1431 0026 0B          	.uleb128 0xb
 1432 0027 49          	.uleb128 0x49
 1433 0028 13          	.uleb128 0x13
 1434 0029 00          	.byte 0x0
 1435 002a 00          	.byte 0x0
 1436 002b 04          	.uleb128 0x4
 1437 002c 13          	.uleb128 0x13
 1438 002d 01          	.byte 0x1
 1439 002e 0B          	.uleb128 0xb
 1440 002f 0B          	.uleb128 0xb
 1441 0030 3A          	.uleb128 0x3a
 1442 0031 0B          	.uleb128 0xb
 1443 0032 3B          	.uleb128 0x3b
 1444 0033 0B          	.uleb128 0xb
 1445 0034 01          	.uleb128 0x1
 1446 0035 13          	.uleb128 0x13
 1447 0036 00          	.byte 0x0
 1448 0037 00          	.byte 0x0
 1449 0038 05          	.uleb128 0x5
 1450 0039 0D          	.uleb128 0xd
 1451 003a 00          	.byte 0x0
 1452 003b 03          	.uleb128 0x3
 1453 003c 08          	.uleb128 0x8
 1454 003d 3A          	.uleb128 0x3a
 1455 003e 0B          	.uleb128 0xb
 1456 003f 3B          	.uleb128 0x3b
 1457 0040 0B          	.uleb128 0xb
 1458 0041 49          	.uleb128 0x49
 1459 0042 13          	.uleb128 0x13
 1460 0043 0B          	.uleb128 0xb
 1461 0044 0B          	.uleb128 0xb
 1462 0045 0D          	.uleb128 0xd
 1463 0046 0B          	.uleb128 0xb
 1464 0047 0C          	.uleb128 0xc
 1465 0048 0B          	.uleb128 0xb
 1466 0049 38          	.uleb128 0x38
 1467 004a 0A          	.uleb128 0xa
 1468 004b 00          	.byte 0x0
 1469 004c 00          	.byte 0x0
MPLAB XC16 ASSEMBLY Listing:   			page 33


 1470 004d 06          	.uleb128 0x6
 1471 004e 17          	.uleb128 0x17
 1472 004f 01          	.byte 0x1
 1473 0050 0B          	.uleb128 0xb
 1474 0051 0B          	.uleb128 0xb
 1475 0052 3A          	.uleb128 0x3a
 1476 0053 0B          	.uleb128 0xb
 1477 0054 3B          	.uleb128 0x3b
 1478 0055 0B          	.uleb128 0xb
 1479 0056 01          	.uleb128 0x1
 1480 0057 13          	.uleb128 0x13
 1481 0058 00          	.byte 0x0
 1482 0059 00          	.byte 0x0
 1483 005a 07          	.uleb128 0x7
 1484 005b 0D          	.uleb128 0xd
 1485 005c 00          	.byte 0x0
 1486 005d 49          	.uleb128 0x49
 1487 005e 13          	.uleb128 0x13
 1488 005f 00          	.byte 0x0
 1489 0060 00          	.byte 0x0
 1490 0061 08          	.uleb128 0x8
 1491 0062 0D          	.uleb128 0xd
 1492 0063 00          	.byte 0x0
 1493 0064 03          	.uleb128 0x3
 1494 0065 08          	.uleb128 0x8
 1495 0066 3A          	.uleb128 0x3a
 1496 0067 0B          	.uleb128 0xb
 1497 0068 3B          	.uleb128 0x3b
 1498 0069 0B          	.uleb128 0xb
 1499 006a 49          	.uleb128 0x49
 1500 006b 13          	.uleb128 0x13
 1501 006c 00          	.byte 0x0
 1502 006d 00          	.byte 0x0
 1503 006e 09          	.uleb128 0x9
 1504 006f 0D          	.uleb128 0xd
 1505 0070 00          	.byte 0x0
 1506 0071 49          	.uleb128 0x49
 1507 0072 13          	.uleb128 0x13
 1508 0073 38          	.uleb128 0x38
 1509 0074 0A          	.uleb128 0xa
 1510 0075 00          	.byte 0x0
 1511 0076 00          	.byte 0x0
 1512 0077 0A          	.uleb128 0xa
 1513 0078 0D          	.uleb128 0xd
 1514 0079 00          	.byte 0x0
 1515 007a 03          	.uleb128 0x3
 1516 007b 08          	.uleb128 0x8
 1517 007c 3A          	.uleb128 0x3a
 1518 007d 0B          	.uleb128 0xb
 1519 007e 3B          	.uleb128 0x3b
 1520 007f 0B          	.uleb128 0xb
 1521 0080 49          	.uleb128 0x49
 1522 0081 13          	.uleb128 0x13
 1523 0082 38          	.uleb128 0x38
 1524 0083 0A          	.uleb128 0xa
 1525 0084 00          	.byte 0x0
 1526 0085 00          	.byte 0x0
MPLAB XC16 ASSEMBLY Listing:   			page 34


 1527 0086 0B          	.uleb128 0xb
 1528 0087 01          	.uleb128 0x1
 1529 0088 01          	.byte 0x1
 1530 0089 49          	.uleb128 0x49
 1531 008a 13          	.uleb128 0x13
 1532 008b 01          	.uleb128 0x1
 1533 008c 13          	.uleb128 0x13
 1534 008d 00          	.byte 0x0
 1535 008e 00          	.byte 0x0
 1536 008f 0C          	.uleb128 0xc
 1537 0090 21          	.uleb128 0x21
 1538 0091 00          	.byte 0x0
 1539 0092 49          	.uleb128 0x49
 1540 0093 13          	.uleb128 0x13
 1541 0094 2F          	.uleb128 0x2f
 1542 0095 0B          	.uleb128 0xb
 1543 0096 00          	.byte 0x0
 1544 0097 00          	.byte 0x0
 1545 0098 0D          	.uleb128 0xd
 1546 0099 04          	.uleb128 0x4
 1547 009a 01          	.byte 0x1
 1548 009b 0B          	.uleb128 0xb
 1549 009c 0B          	.uleb128 0xb
 1550 009d 3A          	.uleb128 0x3a
 1551 009e 0B          	.uleb128 0xb
 1552 009f 3B          	.uleb128 0x3b
 1553 00a0 0B          	.uleb128 0xb
 1554 00a1 01          	.uleb128 0x1
 1555 00a2 13          	.uleb128 0x13
 1556 00a3 00          	.byte 0x0
 1557 00a4 00          	.byte 0x0
 1558 00a5 0E          	.uleb128 0xe
 1559 00a6 28          	.uleb128 0x28
 1560 00a7 00          	.byte 0x0
 1561 00a8 03          	.uleb128 0x3
 1562 00a9 08          	.uleb128 0x8
 1563 00aa 1C          	.uleb128 0x1c
 1564 00ab 0D          	.uleb128 0xd
 1565 00ac 00          	.byte 0x0
 1566 00ad 00          	.byte 0x0
 1567 00ae 0F          	.uleb128 0xf
 1568 00af 0D          	.uleb128 0xd
 1569 00b0 00          	.byte 0x0
 1570 00b1 03          	.uleb128 0x3
 1571 00b2 0E          	.uleb128 0xe
 1572 00b3 3A          	.uleb128 0x3a
 1573 00b4 0B          	.uleb128 0xb
 1574 00b5 3B          	.uleb128 0x3b
 1575 00b6 0B          	.uleb128 0xb
 1576 00b7 49          	.uleb128 0x49
 1577 00b8 13          	.uleb128 0x13
 1578 00b9 00          	.byte 0x0
 1579 00ba 00          	.byte 0x0
 1580 00bb 10          	.uleb128 0x10
 1581 00bc 0D          	.uleb128 0xd
 1582 00bd 00          	.byte 0x0
 1583 00be 03          	.uleb128 0x3
MPLAB XC16 ASSEMBLY Listing:   			page 35


 1584 00bf 0E          	.uleb128 0xe
 1585 00c0 3A          	.uleb128 0x3a
 1586 00c1 0B          	.uleb128 0xb
 1587 00c2 3B          	.uleb128 0x3b
 1588 00c3 0B          	.uleb128 0xb
 1589 00c4 49          	.uleb128 0x49
 1590 00c5 13          	.uleb128 0x13
 1591 00c6 38          	.uleb128 0x38
 1592 00c7 0A          	.uleb128 0xa
 1593 00c8 00          	.byte 0x0
 1594 00c9 00          	.byte 0x0
 1595 00ca 11          	.uleb128 0x11
 1596 00cb 2E          	.uleb128 0x2e
 1597 00cc 01          	.byte 0x1
 1598 00cd 3F          	.uleb128 0x3f
 1599 00ce 0C          	.uleb128 0xc
 1600 00cf 03          	.uleb128 0x3
 1601 00d0 08          	.uleb128 0x8
 1602 00d1 3A          	.uleb128 0x3a
 1603 00d2 0B          	.uleb128 0xb
 1604 00d3 3B          	.uleb128 0x3b
 1605 00d4 0B          	.uleb128 0xb
 1606 00d5 27          	.uleb128 0x27
 1607 00d6 0C          	.uleb128 0xc
 1608 00d7 11          	.uleb128 0x11
 1609 00d8 01          	.uleb128 0x1
 1610 00d9 12          	.uleb128 0x12
 1611 00da 01          	.uleb128 0x1
 1612 00db 40          	.uleb128 0x40
 1613 00dc 0A          	.uleb128 0xa
 1614 00dd 01          	.uleb128 0x1
 1615 00de 13          	.uleb128 0x13
 1616 00df 00          	.byte 0x0
 1617 00e0 00          	.byte 0x0
 1618 00e1 12          	.uleb128 0x12
 1619 00e2 05          	.uleb128 0x5
 1620 00e3 00          	.byte 0x0
 1621 00e4 03          	.uleb128 0x3
 1622 00e5 08          	.uleb128 0x8
 1623 00e6 3A          	.uleb128 0x3a
 1624 00e7 0B          	.uleb128 0xb
 1625 00e8 3B          	.uleb128 0x3b
 1626 00e9 0B          	.uleb128 0xb
 1627 00ea 49          	.uleb128 0x49
 1628 00eb 13          	.uleb128 0x13
 1629 00ec 02          	.uleb128 0x2
 1630 00ed 0A          	.uleb128 0xa
 1631 00ee 00          	.byte 0x0
 1632 00ef 00          	.byte 0x0
 1633 00f0 13          	.uleb128 0x13
 1634 00f1 0F          	.uleb128 0xf
 1635 00f2 00          	.byte 0x0
 1636 00f3 0B          	.uleb128 0xb
 1637 00f4 0B          	.uleb128 0xb
 1638 00f5 49          	.uleb128 0x49
 1639 00f6 13          	.uleb128 0x13
 1640 00f7 00          	.byte 0x0
MPLAB XC16 ASSEMBLY Listing:   			page 36


 1641 00f8 00          	.byte 0x0
 1642 00f9 14          	.uleb128 0x14
 1643 00fa 05          	.uleb128 0x5
 1644 00fb 00          	.byte 0x0
 1645 00fc 03          	.uleb128 0x3
 1646 00fd 0E          	.uleb128 0xe
 1647 00fe 3A          	.uleb128 0x3a
 1648 00ff 0B          	.uleb128 0xb
 1649 0100 3B          	.uleb128 0x3b
 1650 0101 0B          	.uleb128 0xb
 1651 0102 49          	.uleb128 0x49
 1652 0103 13          	.uleb128 0x13
 1653 0104 02          	.uleb128 0x2
 1654 0105 0A          	.uleb128 0xa
 1655 0106 00          	.byte 0x0
 1656 0107 00          	.byte 0x0
 1657 0108 00          	.byte 0x0
 1658                 	.section .debug_pubnames,info
 1659 0000 FB 00 00 00 	.4byte 0xfb
 1660 0004 02 00       	.2byte 0x2
 1661 0006 00 00 00 00 	.4byte .Ldebug_info0
 1662 000a E1 09 00 00 	.4byte 0x9e1
 1663 000e C7 07 00 00 	.4byte 0x7c7
 1664 0012 70 61 72 73 	.asciz "parse_can_message_master_ts"
 1664      65 5F 63 61 
 1664      6E 5F 6D 65 
 1664      73 73 61 67 
 1664      65 5F 6D 61 
 1664      73 74 65 72 
 1664      5F 74 73 00 
 1665 002e 18 08 00 00 	.4byte 0x818
 1666 0032 70 61 72 73 	.asciz "parse_can_message_master_energy_meter"
 1666      65 5F 63 61 
 1666      6E 5F 6D 65 
 1666      73 73 61 67 
 1666      65 5F 6D 61 
 1666      73 74 65 72 
 1666      5F 65 6E 65 
 1666      72 67 79 5F 
 1666      6D 65 74 65 
 1667 0058 74 08 00 00 	.4byte 0x874
 1668 005c 70 61 72 73 	.asciz "parse_can_message_master_cell_voltage_info"
 1668      65 5F 63 61 
 1668      6E 5F 6D 65 
 1668      73 73 61 67 
 1668      65 5F 6D 61 
 1668      73 74 65 72 
 1668      5F 63 65 6C 
 1668      6C 5F 76 6F 
 1668      6C 74 61 67 
 1669 0087 D5 08 00 00 	.4byte 0x8d5
 1670 008b 70 61 72 73 	.asciz "parse_can_message_master_cell_temperature_info"
 1670      65 5F 63 61 
 1670      6E 5F 6D 65 
 1670      73 73 61 67 
 1670      65 5F 6D 61 
 1670      73 74 65 72 
MPLAB XC16 ASSEMBLY Listing:   			page 37


 1670      5F 63 65 6C 
 1670      6C 5F 74 65 
 1670      6D 70 65 72 
 1671 00ba 3A 09 00 00 	.4byte 0x93a
 1672 00be 70 61 72 73 	.asciz "parse_can_message_master_status"
 1672      65 5F 63 61 
 1672      6E 5F 6D 65 
 1672      73 73 61 67 
 1672      65 5F 6D 61 
 1672      73 74 65 72 
 1672      5F 73 74 61 
 1672      74 75 73 00 
 1673 00de 90 09 00 00 	.4byte 0x990
 1674 00e2 70 61 72 73 	.asciz "parse_can_message_master"
 1674      65 5F 63 61 
 1674      6E 5F 6D 65 
 1674      73 73 61 67 
 1674      65 5F 6D 61 
 1674      73 74 65 72 
 1674      00 
 1675 00fb 00 00 00 00 	.4byte 0x0
 1676                 	.section .debug_pubtypes,info
 1677 0000 F9 00 00 00 	.4byte 0xf9
 1678 0004 02 00       	.2byte 0x2
 1679 0006 00 00 00 00 	.4byte .Ldebug_info0
 1680 000a E1 09 00 00 	.4byte 0x9e1
 1681 000e C3 00 00 00 	.4byte 0xc3
 1682 0012 69 6E 74 31 	.asciz "int16_t"
 1682      36 5F 74 00 
 1683 001a F6 00 00 00 	.4byte 0xf6
 1684 001e 75 69 6E 74 	.asciz "uint8_t"
 1684      38 5F 74 00 
 1685 0026 16 01 00 00 	.4byte 0x116
 1686 002a 75 69 6E 74 	.asciz "uint16_t"
 1686      31 36 5F 74 
 1686      00 
 1687 0033 F0 01 00 00 	.4byte 0x1f0
 1688 0037 43 41 4E 64 	.asciz "CANdata"
 1688      61 74 61 00 
 1689 003f 58 03 00 00 	.4byte 0x358
 1690 0043 4D 41 53 54 	.asciz "MASTER_MSG_TS_OFF_Reason"
 1690      45 52 5F 4D 
 1690      53 47 5F 54 
 1690      53 5F 4F 46 
 1690      46 5F 52 65 
 1690      61 73 6F 6E 
 1690      00 
 1691 005c B1 03 00 00 	.4byte 0x3b1
 1692 0060 4D 41 53 54 	.asciz "MASTER_MSG_TS"
 1692      45 52 5F 4D 
 1692      53 47 5F 54 
 1692      53 00 
 1693 006e 11 04 00 00 	.4byte 0x411
 1694 0072 4D 41 53 54 	.asciz "MASTER_MSG_Energy_Meter"
 1694      45 52 5F 4D 
 1694      53 47 5F 45 
 1694      6E 65 72 67 
MPLAB XC16 ASSEMBLY Listing:   			page 38


 1694      79 5F 4D 65 
 1694      74 65 72 00 
 1695 008a E0 05 00 00 	.4byte 0x5e0
 1696 008e 4D 41 53 54 	.asciz "MASTER_MSG_Status"
 1696      45 52 5F 4D 
 1696      53 47 5F 53 
 1696      74 61 74 75 
 1696      73 00 
 1697 00a0 6E 06 00 00 	.4byte 0x66e
 1698 00a4 4D 41 53 54 	.asciz "MASTER_MSG_Battery_Voltage_Info"
 1698      45 52 5F 4D 
 1698      53 47 5F 42 
 1698      61 74 74 65 
 1698      72 79 5F 56 
 1698      6F 6C 74 61 
 1698      67 65 5F 49 
 1698      6E 66 6F 00 
 1699 00c4 04 07 00 00 	.4byte 0x704
 1700 00c8 4D 41 53 54 	.asciz "MASTER_MSG_Battery_Temp_Info"
 1700      45 52 5F 4D 
 1700      53 47 5F 42 
 1700      61 74 74 65 
 1700      72 79 5F 54 
 1700      65 6D 70 5F 
 1700      49 6E 66 6F 
 1700      00 
 1701 00e5 B0 07 00 00 	.4byte 0x7b0
 1702 00e9 4D 41 53 54 	.asciz "MASTER_MSG_Data"
 1702      45 52 5F 4D 
 1702      53 47 5F 44 
 1702      61 74 61 00 
 1703 00f9 00 00 00 00 	.4byte 0x0
 1704                 	.section .debug_aranges,info
 1705 0000 14 00 00 00 	.4byte 0x14
 1706 0004 02 00       	.2byte 0x2
 1707 0006 00 00 00 00 	.4byte .Ldebug_info0
 1708 000a 04          	.byte 0x4
 1709 000b 00          	.byte 0x0
 1710 000c 00 00       	.2byte 0x0
 1711 000e 00 00       	.2byte 0x0
 1712 0010 00 00 00 00 	.4byte 0x0
 1713 0014 00 00 00 00 	.4byte 0x0
 1714                 	.section .debug_str,info
 1715                 	.LASF1:
 1716 0000 65 6E 65 72 	.asciz "energy_meter"
 1716      67 79 5F 6D 
 1716      65 74 65 72 
 1716      00 
 1717                 	.LASF2:
 1718 000d 63 65 6C 6C 	.asciz "cell_voltage_info"
 1718      5F 76 6F 6C 
 1718      74 61 67 65 
 1718      5F 69 6E 66 
 1718      6F 00 
 1719                 	.LASF3:
 1720 001f 63 65 6C 6C 	.asciz "cell_temp_info"
 1720      5F 74 65 6D 
MPLAB XC16 ASSEMBLY Listing:   			page 39


 1720      70 5F 69 6E 
 1720      66 6F 00 
 1721                 	.LASF0:
 1722 002e 73 74 61 74 	.asciz "status"
 1722      75 73 00 
 1723                 	.section .text,code
 1724              	
 1725              	
 1726              	
 1727              	.section __c30_info,info,bss
 1728                 	__psv_trap_errata:
 1729                 	
 1730                 	.section __c30_signature,info,data
 1731 0000 01 00       	.word 0x0001
 1732 0002 00 00       	.word 0x0000
 1733 0004 00 00       	.word 0x0000
 1734                 	
 1735                 	
 1736                 	
 1737                 	.set ___PA___,0
 1738                 	.end
MPLAB XC16 ASSEMBLY Listing:   			page 40


DEFINED SYMBOLS
                            *ABS*:00000000 /home/user/Documents/FST/Programming/project_sub-zero/lib/can-ids/Devices/BMS_MASTER_CAN.c
                            *ABS*:00000001 __33EP256MU806
                            *ABS*:00000001 __dsPIC33E
                            *ABS*:00000001 __HAS_DSP
                            *ABS*:00000001 __HAS_DMAV2
                            *ABS*:00000001 __HAS_CODEGUARD
                            *ABS*:00000001 __HAS_PMP_ENHANCED
                            *ABS*:00000001 __HAS_EDS
                            *ABS*:00000001 __HAS_EP
                            *ABS*:00000011 __TARGET_DIVIDE_CYCLES
                            *ABS*:00000001 __C30ELF
    {standard input}:14     .text:00000000 _parse_can_message_master_ts
    {standard input}:18     *ABS*:00000000 ___PA___
    {standard input}:72     .text:00000050 _parse_can_message_master_energy_meter
    {standard input}:123    .text:00000088 _parse_can_message_master_cell_voltage_info
    {standard input}:176    .text:000000c4 _parse_can_message_master_cell_temperature_info
    {standard input}:229    .text:00000100 _parse_can_message_master_status
    {standard input}:260    .text:0000011e _parse_can_message_master
    {standard input}:286    *ABS*:00000000 ___BP___
    {standard input}:1728   __c30_info:00000000 __psv_trap_errata
    {standard input}:19     .text:00000000 .L0
                            .text:000001f0 .L11
                            .text:0000014c .L16
                            .text:000001b8 .L9
                            .text:000001c2 .L10
                            .text:0000019e .L8
                            .text:00000206 .L6
                            .text:00000184 .L13
                            .text:00000160 .L17
                            .text:0000016e .L12
                            .text:000001cc .L14
                            .text:000001e6 .L15
                    .debug_abbrev:00000000 .Ldebug_abbrev0
                            .text:00000000 .Ltext0
                            .text:0000020e .Letext0
                      .debug_line:00000000 .Ldebug_line0
                       .debug_str:0000002e .LASF0
                       .debug_str:00000000 .LASF1
                       .debug_str:0000000d .LASF2
                       .debug_str:0000001f .LASF3
                            .text:00000000 .LFB0
                            .text:00000050 .LFE0
                            .text:00000050 .LFB1
                            .text:00000088 .LFE1
                            .text:00000088 .LFB2
                            .text:000000c4 .LFE2
                            .text:000000c4 .LFB3
                            .text:00000100 .LFE3
                            .text:00000100 .LFB4
                            .text:0000011e .LFE4
                            .text:0000011e .LFB5
                            .text:0000020e .LFE5
                     .debug_frame:00000000 .Lframe0
                      .debug_info:00000000 .Ldebug_info0

UNDEFINED SYMBOLS
MPLAB XC16 ASSEMBLY Listing:   			page 41


CORCON

EQUATE SYMBOLS
/home/user/Documents/FST/Programming/project_sub-zero/lib/can-ids/Devices/BMS_MASTER_CAN.c = 0x0
                  __33EP256MU806 = 0x1
                      __dsPIC33E = 0x1
                       __HAS_DSP = 0x1
                     __HAS_DMAV2 = 0x1
                 __HAS_CODEGUARD = 0x1
              __HAS_PMP_ENHANCED = 0x1
                       __HAS_EDS = 0x1
                        __HAS_EP = 0x1
          __TARGET_DIVIDE_CYCLES = 0x11
                        __C30ELF = 0x1
                        ___PA___ = 0x0
                        ___BP___ = 0x0
