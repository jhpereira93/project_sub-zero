MPLAB XC16 ASSEMBLY Listing:   			page 1


   1              	.file "/home/user/Documents/FST/Programming/project_sub-zero/lib/can-ids/CAN_IDs.c"
   2              	.section .debug_abbrev,info
   3                 	.Ldebug_abbrev0:
   4                 	.section .debug_info,info
   5                 	.Ldebug_info0:
   6                 	.section .debug_line,info
   7                 	.Ldebug_line0:
   8 0000 97 00 00 00 	.section .text,code
   8      02 00 79 00 
   8      00 00 01 01 
   8      FB 0E 0A 00 
   8      01 01 01 01 
   8      00 00 00 01 
   8      6C 69 62 2F 
   8      63 61 6E 2D 
   8      69 64 73 00 
   9              	.Ltext0:
  10              	.section .text,code
  11              	.align 2
  12              	.global _can_data_to_can_message
  13              	.type _can_data_to_can_message,@function
  14              	_can_data_to_can_message:
  15              	.LFB0:
  16              	.file 1 "lib/can-ids/CAN_IDs.c"
   1:lib/can-ids/CAN_IDs.c **** #include "can-ids/CAN_IDs.h"
   2:lib/can-ids/CAN_IDs.c **** 
   3:lib/can-ids/CAN_IDs.c **** CANmessage can_data_to_can_message(CANdata msg, uint32_t timestamp) {
  17              	.loc 1 3 0
  18              	.set ___PA___,1
  19 000000  1C 00 FA 	lnk #28
  20              	.LCFI0:
  21 000002  88 1F 78 	mov w8,[w15++]
  22              	.LCFI1:
   4:lib/can-ids/CAN_IDs.c **** 	CANmessage aux;
   5:lib/can-ids/CAN_IDs.c **** 	aux.candata = msg;
  23              	.loc 1 5 0
  24 000004  F0 03 47 	add w14,#16,w7
  25              	.loc 1 3 0
  26 000006  01 0F 98 	mov w1,[w14+16]
  27 000008  12 0F 98 	mov w2,[w14+18]
  28              	.loc 1 5 0
  29 00000a  8E 00 78 	mov w14,w1
  30 00000c  B7 18 78 	mov [w7++],[w1++]
  31 00000e  A7 10 78 	mov [w7--],[w1--]
  32 000010  E4 83 43 	add w7,#4,w7
  33 000012  E4 80 40 	add w1,#4,w1
  34              	.loc 1 3 0
  35 000014  23 0F 98 	mov w3,[w14+20]
  36 000016  34 0F 98 	mov w4,[w14+22]
  37              	.loc 1 5 0
  38 000018  B7 18 78 	mov [w7++],[w1++]
  39 00001a  A7 10 78 	mov [w7--],[w1--]
  40 00001c  E4 83 43 	add w7,#4,w7
  41              	.loc 1 3 0
  42 00001e  00 04 78 	mov w0,w8
  43 000020  45 0F 98 	mov w5,[w14+24]
  44 000022  56 0F 98 	mov w6,[w14+26]
MPLAB XC16 ASSEMBLY Listing:   			page 2


  45              	.loc 1 5 0
  46 000024  E4 80 40 	add w1,#4,w1
   6:lib/can-ids/CAN_IDs.c **** 	aux.timestamp = timestamp;
  47              	.loc 1 6 0
  48 000026  3E B9 97 	mov [w14-10],w2
  49 000028  CE B9 97 	mov [w14-8],w3
  50 00002a  62 07 98 	mov w2,[w14+12]
  51 00002c  73 07 98 	mov w3,[w14+14]
  52              	.loc 1 5 0
  53 00002e  B7 18 78 	mov [w7++],[w1++]
  54 000030  A7 10 78 	mov [w7--],[w1--]
   7:lib/can-ids/CAN_IDs.c **** 
   8:lib/can-ids/CAN_IDs.c **** 	return aux;
  55              	.loc 1 8 0
  56 000032  08 00 78 	mov w8,w0
  57 000034  8E 00 78 	mov w14,w1
  58 000036  02 01 20 	mov #16,w2
  59 000038  00 00 07 	rcall _memcpy
   9:lib/can-ids/CAN_IDs.c **** }
  60              	.loc 1 9 0
  61 00003a  08 00 78 	mov w8,w0
  62 00003c  4F 04 78 	mov [--w15],w8
  63 00003e  8E 07 78 	mov w14,w15
  64 000040  4F 07 78 	mov [--w15],w14
  65 000042  00 40 A9 	bclr CORCON,#2
  66 000044  00 00 06 	return 
  67              	.set ___PA___,0
  68              	.LFE0:
  69              	.size _can_data_to_can_message,.-_can_data_to_can_message
  70              	.section .debug_frame,info
  71                 	.Lframe0:
  72 0000 10 00 00 00 	.4byte .LECIE0-.LSCIE0
  73                 	.LSCIE0:
  74 0004 FF FF FF FF 	.4byte 0xffffffff
  75 0008 01          	.byte 0x1
  76 0009 00          	.byte 0
  77 000a 01          	.uleb128 0x1
  78 000b 02          	.sleb128 2
  79 000c 25          	.byte 0x25
  80 000d 12          	.byte 0x12
  81 000e 0F          	.uleb128 0xf
  82 000f 7E          	.sleb128 -2
  83 0010 09          	.byte 0x9
  84 0011 25          	.uleb128 0x25
  85 0012 0F          	.uleb128 0xf
  86 0013 00          	.align 4
  87                 	.LECIE0:
  88                 	.LSFDE0:
  89 0014 1E 00 00 00 	.4byte .LEFDE0-.LASFDE0
  90                 	.LASFDE0:
  91 0018 00 00 00 00 	.4byte .Lframe0
  92 001c 00 00 00 00 	.4byte .LFB0
  93 0020 46 00 00 00 	.4byte .LFE0-.LFB0
  94 0024 04          	.byte 0x4
  95 0025 02 00 00 00 	.4byte .LCFI0-.LFB0
  96 0029 13          	.byte 0x13
  97 002a 7D          	.sleb128 -3
MPLAB XC16 ASSEMBLY Listing:   			page 3


  98 002b 0D          	.byte 0xd
  99 002c 0E          	.uleb128 0xe
 100 002d 8E          	.byte 0x8e
 101 002e 02          	.uleb128 0x2
 102 002f 04          	.byte 0x4
 103 0030 02 00 00 00 	.4byte .LCFI1-.LCFI0
 104 0034 88          	.byte 0x88
 105 0035 11          	.uleb128 0x11
 106                 	.align 4
 107                 	.LEFDE0:
 108                 	.section .text,code
 109              	.Letext0:
 110              	.file 2 "/opt/microchip/xc16/v1.36/bin/bin/../../include/lega-c/stdint.h"
 111              	.file 3 "lib/can-ids/CAN_IDs.h"
 112              	.section .debug_info,info
 113 0000 70 02 00 00 	.4byte 0x270
 114 0004 02 00       	.2byte 0x2
 115 0006 00 00 00 00 	.4byte .Ldebug_abbrev0
 116 000a 04          	.byte 0x4
 117 000b 01          	.uleb128 0x1
 118 000c 47 4E 55 20 	.asciz "GNU C 4.5.1 (XC16, Microchip v1.36) (A) Build date: Dec  5 2018"
 118      43 20 34 2E 
 118      35 2E 31 20 
 118      28 58 43 31 
 118      36 2C 20 4D 
 118      69 63 72 6F 
 118      63 68 69 70 
 118      20 76 31 2E 
 118      33 36 29 20 
 119 004c 01          	.byte 0x1
 120 004d 6C 69 62 2F 	.asciz "lib/can-ids/CAN_IDs.c"
 120      63 61 6E 2D 
 120      69 64 73 2F 
 120      43 41 4E 5F 
 120      49 44 73 2E 
 120      63 00 
 121 0063 2F 68 6F 6D 	.asciz "/home/user/Documents/FST/Programming/project_sub-zero"
 121      65 2F 75 73 
 121      65 72 2F 44 
 121      6F 63 75 6D 
 121      65 6E 74 73 
 121      2F 46 53 54 
 121      2F 50 72 6F 
 121      67 72 61 6D 
 121      6D 69 6E 67 
 122 0099 00 00 00 00 	.4byte .Ltext0
 123 009d 00 00 00 00 	.4byte .Letext0
 124 00a1 00 00 00 00 	.4byte .Ldebug_line0
 125 00a5 02          	.uleb128 0x2
 126 00a6 01          	.byte 0x1
 127 00a7 06          	.byte 0x6
 128 00a8 73 69 67 6E 	.asciz "signed char"
 128      65 64 20 63 
 128      68 61 72 00 
 129 00b4 02          	.uleb128 0x2
 130 00b5 02          	.byte 0x2
 131 00b6 05          	.byte 0x5
MPLAB XC16 ASSEMBLY Listing:   			page 4


 132 00b7 69 6E 74 00 	.asciz "int"
 133 00bb 02          	.uleb128 0x2
 134 00bc 04          	.byte 0x4
 135 00bd 05          	.byte 0x5
 136 00be 6C 6F 6E 67 	.asciz "long int"
 136      20 69 6E 74 
 136      00 
 137 00c7 02          	.uleb128 0x2
 138 00c8 08          	.byte 0x8
 139 00c9 05          	.byte 0x5
 140 00ca 6C 6F 6E 67 	.asciz "long long int"
 140      20 6C 6F 6E 
 140      67 20 69 6E 
 140      74 00 
 141 00d8 02          	.uleb128 0x2
 142 00d9 01          	.byte 0x1
 143 00da 08          	.byte 0x8
 144 00db 75 6E 73 69 	.asciz "unsigned char"
 144      67 6E 65 64 
 144      20 63 68 61 
 144      72 00 
 145 00e9 03          	.uleb128 0x3
 146 00ea 75 69 6E 74 	.asciz "uint16_t"
 146      31 36 5F 74 
 146      00 
 147 00f3 02          	.byte 0x2
 148 00f4 31          	.byte 0x31
 149 00f5 F9 00 00 00 	.4byte 0xf9
 150 00f9 02          	.uleb128 0x2
 151 00fa 02          	.byte 0x2
 152 00fb 07          	.byte 0x7
 153 00fc 75 6E 73 69 	.asciz "unsigned int"
 153      67 6E 65 64 
 153      20 69 6E 74 
 153      00 
 154 0109 03          	.uleb128 0x3
 155 010a 75 69 6E 74 	.asciz "uint32_t"
 155      33 32 5F 74 
 155      00 
 156 0113 02          	.byte 0x2
 157 0114 37          	.byte 0x37
 158 0115 19 01 00 00 	.4byte 0x119
 159 0119 02          	.uleb128 0x2
 160 011a 04          	.byte 0x4
 161 011b 07          	.byte 0x7
 162 011c 6C 6F 6E 67 	.asciz "long unsigned int"
 162      20 75 6E 73 
 162      69 67 6E 65 
 162      64 20 69 6E 
 162      74 00 
 163 012e 02          	.uleb128 0x2
 164 012f 08          	.byte 0x8
 165 0130 07          	.byte 0x7
 166 0131 6C 6F 6E 67 	.asciz "long long unsigned int"
 166      20 6C 6F 6E 
 166      67 20 75 6E 
 166      73 69 67 6E 
MPLAB XC16 ASSEMBLY Listing:   			page 5


 166      65 64 20 69 
 166      6E 74 00 
 167 0148 04          	.uleb128 0x4
 168 0149 02          	.byte 0x2
 169 014a 03          	.byte 0x3
 170 014b 12          	.byte 0x12
 171 014c 79 01 00 00 	.4byte 0x179
 172 0150 05          	.uleb128 0x5
 173 0151 64 65 76 5F 	.asciz "dev_id"
 173      69 64 00 
 174 0158 03          	.byte 0x3
 175 0159 13          	.byte 0x13
 176 015a E9 00 00 00 	.4byte 0xe9
 177 015e 02          	.byte 0x2
 178 015f 05          	.byte 0x5
 179 0160 0B          	.byte 0xb
 180 0161 02          	.byte 0x2
 181 0162 23          	.byte 0x23
 182 0163 00          	.uleb128 0x0
 183 0164 05          	.uleb128 0x5
 184 0165 6D 73 67 5F 	.asciz "msg_id"
 184      69 64 00 
 185 016c 03          	.byte 0x3
 186 016d 16          	.byte 0x16
 187 016e E9 00 00 00 	.4byte 0xe9
 188 0172 02          	.byte 0x2
 189 0173 06          	.byte 0x6
 190 0174 05          	.byte 0x5
 191 0175 02          	.byte 0x2
 192 0176 23          	.byte 0x23
 193 0177 00          	.uleb128 0x0
 194 0178 00          	.byte 0x0
 195 0179 06          	.uleb128 0x6
 196 017a 02          	.byte 0x2
 197 017b 03          	.byte 0x3
 198 017c 11          	.byte 0x11
 199 017d 92 01 00 00 	.4byte 0x192
 200 0181 07          	.uleb128 0x7
 201 0182 48 01 00 00 	.4byte 0x148
 202 0186 08          	.uleb128 0x8
 203 0187 73 69 64 00 	.asciz "sid"
 204 018b 03          	.byte 0x3
 205 018c 18          	.byte 0x18
 206 018d E9 00 00 00 	.4byte 0xe9
 207 0191 00          	.byte 0x0
 208 0192 04          	.uleb128 0x4
 209 0193 0C          	.byte 0xc
 210 0194 03          	.byte 0x3
 211 0195 10          	.byte 0x10
 212 0196 C3 01 00 00 	.4byte 0x1c3
 213 019a 09          	.uleb128 0x9
 214 019b 79 01 00 00 	.4byte 0x179
 215 019f 02          	.byte 0x2
 216 01a0 23          	.byte 0x23
 217 01a1 00          	.uleb128 0x0
 218 01a2 05          	.uleb128 0x5
 219 01a3 64 6C 63 00 	.asciz "dlc"
MPLAB XC16 ASSEMBLY Listing:   			page 6


 220 01a7 03          	.byte 0x3
 221 01a8 1A          	.byte 0x1a
 222 01a9 E9 00 00 00 	.4byte 0xe9
 223 01ad 02          	.byte 0x2
 224 01ae 04          	.byte 0x4
 225 01af 0C          	.byte 0xc
 226 01b0 02          	.byte 0x2
 227 01b1 23          	.byte 0x23
 228 01b2 02          	.uleb128 0x2
 229 01b3 0A          	.uleb128 0xa
 230 01b4 64 61 74 61 	.asciz "data"
 230      00 
 231 01b9 03          	.byte 0x3
 232 01ba 1B          	.byte 0x1b
 233 01bb C3 01 00 00 	.4byte 0x1c3
 234 01bf 02          	.byte 0x2
 235 01c0 23          	.byte 0x23
 236 01c1 04          	.uleb128 0x4
 237 01c2 00          	.byte 0x0
 238 01c3 0B          	.uleb128 0xb
 239 01c4 E9 00 00 00 	.4byte 0xe9
 240 01c8 D3 01 00 00 	.4byte 0x1d3
 241 01cc 0C          	.uleb128 0xc
 242 01cd F9 00 00 00 	.4byte 0xf9
 243 01d1 03          	.byte 0x3
 244 01d2 00          	.byte 0x0
 245 01d3 03          	.uleb128 0x3
 246 01d4 43 41 4E 64 	.asciz "CANdata"
 246      61 74 61 00 
 247 01dc 03          	.byte 0x3
 248 01dd 1C          	.byte 0x1c
 249 01de 92 01 00 00 	.4byte 0x192
 250 01e2 04          	.uleb128 0x4
 251 01e3 10          	.byte 0x10
 252 01e4 03          	.byte 0x3
 253 01e5 1F          	.byte 0x1f
 254 01e6 0B 02 00 00 	.4byte 0x20b
 255 01ea 0A          	.uleb128 0xa
 256 01eb 63 61 6E 64 	.asciz "candata"
 256      61 74 61 00 
 257 01f3 03          	.byte 0x3
 258 01f4 20          	.byte 0x20
 259 01f5 D3 01 00 00 	.4byte 0x1d3
 260 01f9 02          	.byte 0x2
 261 01fa 23          	.byte 0x23
 262 01fb 00          	.uleb128 0x0
 263 01fc 0D          	.uleb128 0xd
 264 01fd 00 00 00 00 	.4byte .LASF0
 265 0201 03          	.byte 0x3
 266 0202 21          	.byte 0x21
 267 0203 09 01 00 00 	.4byte 0x109
 268 0207 02          	.byte 0x2
 269 0208 23          	.byte 0x23
 270 0209 0C          	.uleb128 0xc
 271 020a 00          	.byte 0x0
 272 020b 03          	.uleb128 0x3
 273 020c 43 41 4E 6D 	.asciz "CANmessage"
MPLAB XC16 ASSEMBLY Listing:   			page 7


 273      65 73 73 61 
 273      67 65 00 
 274 0217 03          	.byte 0x3
 275 0218 22          	.byte 0x22
 276 0219 E2 01 00 00 	.4byte 0x1e2
 277 021d 0E          	.uleb128 0xe
 278 021e 01          	.byte 0x1
 279 021f 63 61 6E 5F 	.asciz "can_data_to_can_message"
 279      64 61 74 61 
 279      5F 74 6F 5F 
 279      63 61 6E 5F 
 279      6D 65 73 73 
 279      61 67 65 00 
 280 0237 01          	.byte 0x1
 281 0238 03          	.byte 0x3
 282 0239 01          	.byte 0x1
 283 023a 0B 02 00 00 	.4byte 0x20b
 284 023e 00 00 00 00 	.4byte .LFB0
 285 0242 00 00 00 00 	.4byte .LFE0
 286 0246 01          	.byte 0x1
 287 0247 5E          	.byte 0x5e
 288 0248 0F          	.uleb128 0xf
 289 0249 6D 73 67 00 	.asciz "msg"
 290 024d 01          	.byte 0x1
 291 024e 03          	.byte 0x3
 292 024f D3 01 00 00 	.4byte 0x1d3
 293 0253 02          	.byte 0x2
 294 0254 7E          	.byte 0x7e
 295 0255 10          	.sleb128 16
 296 0256 10          	.uleb128 0x10
 297 0257 00 00 00 00 	.4byte .LASF0
 298 025b 01          	.byte 0x1
 299 025c 03          	.byte 0x3
 300 025d 09 01 00 00 	.4byte 0x109
 301 0261 02          	.byte 0x2
 302 0262 7E          	.byte 0x7e
 303 0263 76          	.sleb128 -10
 304 0264 11          	.uleb128 0x11
 305 0265 61 75 78 00 	.asciz "aux"
 306 0269 01          	.byte 0x1
 307 026a 04          	.byte 0x4
 308 026b 0B 02 00 00 	.4byte 0x20b
 309 026f 02          	.byte 0x2
 310 0270 7E          	.byte 0x7e
 311 0271 00          	.sleb128 0
 312 0272 00          	.byte 0x0
 313 0273 00          	.byte 0x0
 314                 	.section .debug_abbrev,info
 315 0000 01          	.uleb128 0x1
 316 0001 11          	.uleb128 0x11
 317 0002 01          	.byte 0x1
 318 0003 25          	.uleb128 0x25
 319 0004 08          	.uleb128 0x8
 320 0005 13          	.uleb128 0x13
 321 0006 0B          	.uleb128 0xb
 322 0007 03          	.uleb128 0x3
 323 0008 08          	.uleb128 0x8
MPLAB XC16 ASSEMBLY Listing:   			page 8


 324 0009 1B          	.uleb128 0x1b
 325 000a 08          	.uleb128 0x8
 326 000b 11          	.uleb128 0x11
 327 000c 01          	.uleb128 0x1
 328 000d 12          	.uleb128 0x12
 329 000e 01          	.uleb128 0x1
 330 000f 10          	.uleb128 0x10
 331 0010 06          	.uleb128 0x6
 332 0011 00          	.byte 0x0
 333 0012 00          	.byte 0x0
 334 0013 02          	.uleb128 0x2
 335 0014 24          	.uleb128 0x24
 336 0015 00          	.byte 0x0
 337 0016 0B          	.uleb128 0xb
 338 0017 0B          	.uleb128 0xb
 339 0018 3E          	.uleb128 0x3e
 340 0019 0B          	.uleb128 0xb
 341 001a 03          	.uleb128 0x3
 342 001b 08          	.uleb128 0x8
 343 001c 00          	.byte 0x0
 344 001d 00          	.byte 0x0
 345 001e 03          	.uleb128 0x3
 346 001f 16          	.uleb128 0x16
 347 0020 00          	.byte 0x0
 348 0021 03          	.uleb128 0x3
 349 0022 08          	.uleb128 0x8
 350 0023 3A          	.uleb128 0x3a
 351 0024 0B          	.uleb128 0xb
 352 0025 3B          	.uleb128 0x3b
 353 0026 0B          	.uleb128 0xb
 354 0027 49          	.uleb128 0x49
 355 0028 13          	.uleb128 0x13
 356 0029 00          	.byte 0x0
 357 002a 00          	.byte 0x0
 358 002b 04          	.uleb128 0x4
 359 002c 13          	.uleb128 0x13
 360 002d 01          	.byte 0x1
 361 002e 0B          	.uleb128 0xb
 362 002f 0B          	.uleb128 0xb
 363 0030 3A          	.uleb128 0x3a
 364 0031 0B          	.uleb128 0xb
 365 0032 3B          	.uleb128 0x3b
 366 0033 0B          	.uleb128 0xb
 367 0034 01          	.uleb128 0x1
 368 0035 13          	.uleb128 0x13
 369 0036 00          	.byte 0x0
 370 0037 00          	.byte 0x0
 371 0038 05          	.uleb128 0x5
 372 0039 0D          	.uleb128 0xd
 373 003a 00          	.byte 0x0
 374 003b 03          	.uleb128 0x3
 375 003c 08          	.uleb128 0x8
 376 003d 3A          	.uleb128 0x3a
 377 003e 0B          	.uleb128 0xb
 378 003f 3B          	.uleb128 0x3b
 379 0040 0B          	.uleb128 0xb
 380 0041 49          	.uleb128 0x49
MPLAB XC16 ASSEMBLY Listing:   			page 9


 381 0042 13          	.uleb128 0x13
 382 0043 0B          	.uleb128 0xb
 383 0044 0B          	.uleb128 0xb
 384 0045 0D          	.uleb128 0xd
 385 0046 0B          	.uleb128 0xb
 386 0047 0C          	.uleb128 0xc
 387 0048 0B          	.uleb128 0xb
 388 0049 38          	.uleb128 0x38
 389 004a 0A          	.uleb128 0xa
 390 004b 00          	.byte 0x0
 391 004c 00          	.byte 0x0
 392 004d 06          	.uleb128 0x6
 393 004e 17          	.uleb128 0x17
 394 004f 01          	.byte 0x1
 395 0050 0B          	.uleb128 0xb
 396 0051 0B          	.uleb128 0xb
 397 0052 3A          	.uleb128 0x3a
 398 0053 0B          	.uleb128 0xb
 399 0054 3B          	.uleb128 0x3b
 400 0055 0B          	.uleb128 0xb
 401 0056 01          	.uleb128 0x1
 402 0057 13          	.uleb128 0x13
 403 0058 00          	.byte 0x0
 404 0059 00          	.byte 0x0
 405 005a 07          	.uleb128 0x7
 406 005b 0D          	.uleb128 0xd
 407 005c 00          	.byte 0x0
 408 005d 49          	.uleb128 0x49
 409 005e 13          	.uleb128 0x13
 410 005f 00          	.byte 0x0
 411 0060 00          	.byte 0x0
 412 0061 08          	.uleb128 0x8
 413 0062 0D          	.uleb128 0xd
 414 0063 00          	.byte 0x0
 415 0064 03          	.uleb128 0x3
 416 0065 08          	.uleb128 0x8
 417 0066 3A          	.uleb128 0x3a
 418 0067 0B          	.uleb128 0xb
 419 0068 3B          	.uleb128 0x3b
 420 0069 0B          	.uleb128 0xb
 421 006a 49          	.uleb128 0x49
 422 006b 13          	.uleb128 0x13
 423 006c 00          	.byte 0x0
 424 006d 00          	.byte 0x0
 425 006e 09          	.uleb128 0x9
 426 006f 0D          	.uleb128 0xd
 427 0070 00          	.byte 0x0
 428 0071 49          	.uleb128 0x49
 429 0072 13          	.uleb128 0x13
 430 0073 38          	.uleb128 0x38
 431 0074 0A          	.uleb128 0xa
 432 0075 00          	.byte 0x0
 433 0076 00          	.byte 0x0
 434 0077 0A          	.uleb128 0xa
 435 0078 0D          	.uleb128 0xd
 436 0079 00          	.byte 0x0
 437 007a 03          	.uleb128 0x3
MPLAB XC16 ASSEMBLY Listing:   			page 10


 438 007b 08          	.uleb128 0x8
 439 007c 3A          	.uleb128 0x3a
 440 007d 0B          	.uleb128 0xb
 441 007e 3B          	.uleb128 0x3b
 442 007f 0B          	.uleb128 0xb
 443 0080 49          	.uleb128 0x49
 444 0081 13          	.uleb128 0x13
 445 0082 38          	.uleb128 0x38
 446 0083 0A          	.uleb128 0xa
 447 0084 00          	.byte 0x0
 448 0085 00          	.byte 0x0
 449 0086 0B          	.uleb128 0xb
 450 0087 01          	.uleb128 0x1
 451 0088 01          	.byte 0x1
 452 0089 49          	.uleb128 0x49
 453 008a 13          	.uleb128 0x13
 454 008b 01          	.uleb128 0x1
 455 008c 13          	.uleb128 0x13
 456 008d 00          	.byte 0x0
 457 008e 00          	.byte 0x0
 458 008f 0C          	.uleb128 0xc
 459 0090 21          	.uleb128 0x21
 460 0091 00          	.byte 0x0
 461 0092 49          	.uleb128 0x49
 462 0093 13          	.uleb128 0x13
 463 0094 2F          	.uleb128 0x2f
 464 0095 0B          	.uleb128 0xb
 465 0096 00          	.byte 0x0
 466 0097 00          	.byte 0x0
 467 0098 0D          	.uleb128 0xd
 468 0099 0D          	.uleb128 0xd
 469 009a 00          	.byte 0x0
 470 009b 03          	.uleb128 0x3
 471 009c 0E          	.uleb128 0xe
 472 009d 3A          	.uleb128 0x3a
 473 009e 0B          	.uleb128 0xb
 474 009f 3B          	.uleb128 0x3b
 475 00a0 0B          	.uleb128 0xb
 476 00a1 49          	.uleb128 0x49
 477 00a2 13          	.uleb128 0x13
 478 00a3 38          	.uleb128 0x38
 479 00a4 0A          	.uleb128 0xa
 480 00a5 00          	.byte 0x0
 481 00a6 00          	.byte 0x0
 482 00a7 0E          	.uleb128 0xe
 483 00a8 2E          	.uleb128 0x2e
 484 00a9 01          	.byte 0x1
 485 00aa 3F          	.uleb128 0x3f
 486 00ab 0C          	.uleb128 0xc
 487 00ac 03          	.uleb128 0x3
 488 00ad 08          	.uleb128 0x8
 489 00ae 3A          	.uleb128 0x3a
 490 00af 0B          	.uleb128 0xb
 491 00b0 3B          	.uleb128 0x3b
 492 00b1 0B          	.uleb128 0xb
 493 00b2 27          	.uleb128 0x27
 494 00b3 0C          	.uleb128 0xc
MPLAB XC16 ASSEMBLY Listing:   			page 11


 495 00b4 49          	.uleb128 0x49
 496 00b5 13          	.uleb128 0x13
 497 00b6 11          	.uleb128 0x11
 498 00b7 01          	.uleb128 0x1
 499 00b8 12          	.uleb128 0x12
 500 00b9 01          	.uleb128 0x1
 501 00ba 40          	.uleb128 0x40
 502 00bb 0A          	.uleb128 0xa
 503 00bc 00          	.byte 0x0
 504 00bd 00          	.byte 0x0
 505 00be 0F          	.uleb128 0xf
 506 00bf 05          	.uleb128 0x5
 507 00c0 00          	.byte 0x0
 508 00c1 03          	.uleb128 0x3
 509 00c2 08          	.uleb128 0x8
 510 00c3 3A          	.uleb128 0x3a
 511 00c4 0B          	.uleb128 0xb
 512 00c5 3B          	.uleb128 0x3b
 513 00c6 0B          	.uleb128 0xb
 514 00c7 49          	.uleb128 0x49
 515 00c8 13          	.uleb128 0x13
 516 00c9 02          	.uleb128 0x2
 517 00ca 0A          	.uleb128 0xa
 518 00cb 00          	.byte 0x0
 519 00cc 00          	.byte 0x0
 520 00cd 10          	.uleb128 0x10
 521 00ce 05          	.uleb128 0x5
 522 00cf 00          	.byte 0x0
 523 00d0 03          	.uleb128 0x3
 524 00d1 0E          	.uleb128 0xe
 525 00d2 3A          	.uleb128 0x3a
 526 00d3 0B          	.uleb128 0xb
 527 00d4 3B          	.uleb128 0x3b
 528 00d5 0B          	.uleb128 0xb
 529 00d6 49          	.uleb128 0x49
 530 00d7 13          	.uleb128 0x13
 531 00d8 02          	.uleb128 0x2
 532 00d9 0A          	.uleb128 0xa
 533 00da 00          	.byte 0x0
 534 00db 00          	.byte 0x0
 535 00dc 11          	.uleb128 0x11
 536 00dd 34          	.uleb128 0x34
 537 00de 00          	.byte 0x0
 538 00df 03          	.uleb128 0x3
 539 00e0 08          	.uleb128 0x8
 540 00e1 3A          	.uleb128 0x3a
 541 00e2 0B          	.uleb128 0xb
 542 00e3 3B          	.uleb128 0x3b
 543 00e4 0B          	.uleb128 0xb
 544 00e5 49          	.uleb128 0x49
 545 00e6 13          	.uleb128 0x13
 546 00e7 02          	.uleb128 0x2
 547 00e8 0A          	.uleb128 0xa
 548 00e9 00          	.byte 0x0
 549 00ea 00          	.byte 0x0
 550 00eb 00          	.byte 0x0
 551                 	.section .debug_pubnames,info
MPLAB XC16 ASSEMBLY Listing:   			page 12


 552 0000 2A 00 00 00 	.4byte 0x2a
 553 0004 02 00       	.2byte 0x2
 554 0006 00 00 00 00 	.4byte .Ldebug_info0
 555 000a 74 02 00 00 	.4byte 0x274
 556 000e 1D 02 00 00 	.4byte 0x21d
 557 0012 63 61 6E 5F 	.asciz "can_data_to_can_message"
 557      64 61 74 61 
 557      5F 74 6F 5F 
 557      63 61 6E 5F 
 557      6D 65 73 73 
 557      61 67 65 00 
 558 002a 00 00 00 00 	.4byte 0x0
 559                 	.section .debug_pubtypes,info
 560 0000 43 00 00 00 	.4byte 0x43
 561 0004 02 00       	.2byte 0x2
 562 0006 00 00 00 00 	.4byte .Ldebug_info0
 563 000a 74 02 00 00 	.4byte 0x274
 564 000e E9 00 00 00 	.4byte 0xe9
 565 0012 75 69 6E 74 	.asciz "uint16_t"
 565      31 36 5F 74 
 565      00 
 566 001b 09 01 00 00 	.4byte 0x109
 567 001f 75 69 6E 74 	.asciz "uint32_t"
 567      33 32 5F 74 
 567      00 
 568 0028 D3 01 00 00 	.4byte 0x1d3
 569 002c 43 41 4E 64 	.asciz "CANdata"
 569      61 74 61 00 
 570 0034 0B 02 00 00 	.4byte 0x20b
 571 0038 43 41 4E 6D 	.asciz "CANmessage"
 571      65 73 73 61 
 571      67 65 00 
 572 0043 00 00 00 00 	.4byte 0x0
 573                 	.section .debug_aranges,info
 574 0000 14 00 00 00 	.4byte 0x14
 575 0004 02 00       	.2byte 0x2
 576 0006 00 00 00 00 	.4byte .Ldebug_info0
 577 000a 04          	.byte 0x4
 578 000b 00          	.byte 0x0
 579 000c 00 00       	.2byte 0x0
 580 000e 00 00       	.2byte 0x0
 581 0010 00 00 00 00 	.4byte 0x0
 582 0014 00 00 00 00 	.4byte 0x0
 583                 	.section .debug_str,info
 584                 	.LASF0:
 585 0000 74 69 6D 65 	.asciz "timestamp"
 585      73 74 61 6D 
 585      70 00 
 586                 	.section .text,code
 587              	
 588              	
 589              	
 590              	.section __c30_info,info,bss
 591                 	__psv_trap_errata:
 592                 	
 593                 	.section __c30_signature,info,data
 594 0000 01 00       	.word 0x0001
MPLAB XC16 ASSEMBLY Listing:   			page 13


 595 0002 00 00       	.word 0x0000
 596 0004 00 00       	.word 0x0000
 597                 	
 598                 	
 599                 	
 600                 	.set ___PA___,0
 601                 	.end
MPLAB XC16 ASSEMBLY Listing:   			page 14


DEFINED SYMBOLS
                            *ABS*:00000000 /home/user/Documents/FST/Programming/project_sub-zero/lib/can-ids/CAN_IDs.c
                            *ABS*:00000001 __33EP256MU806
                            *ABS*:00000001 __dsPIC33E
                            *ABS*:00000001 __HAS_DSP
                            *ABS*:00000001 __HAS_DMAV2
                            *ABS*:00000001 __HAS_CODEGUARD
                            *ABS*:00000001 __HAS_PMP_ENHANCED
                            *ABS*:00000001 __HAS_EDS
                            *ABS*:00000001 __HAS_EP
                            *ABS*:00000011 __TARGET_DIVIDE_CYCLES
                            *ABS*:00000001 __C30ELF
    {standard input}:14     .text:00000000 _can_data_to_can_message
    {standard input}:18     *ABS*:00000000 ___PA___
    {standard input}:591    __c30_info:00000000 __psv_trap_errata
    {standard input}:19     .text:00000000 .L0
                    .debug_abbrev:00000000 .Ldebug_abbrev0
                            .text:00000000 .Ltext0
                            .text:00000046 .Letext0
                      .debug_line:00000000 .Ldebug_line0
                       .debug_str:00000000 .LASF0
                            .text:00000000 .LFB0
                            .text:00000046 .LFE0
                     .debug_frame:00000000 .Lframe0
                      .debug_info:00000000 .Ldebug_info0

UNDEFINED SYMBOLS
_memcpy
CORCON

EQUATE SYMBOLS
/home/user/Documents/FST/Programming/project_sub-zero/lib/can-ids/CAN_IDs.c = 0x0
                  __33EP256MU806 = 0x1
                      __dsPIC33E = 0x1
                       __HAS_DSP = 0x1
                     __HAS_DMAV2 = 0x1
                 __HAS_CODEGUARD = 0x1
              __HAS_PMP_ENHANCED = 0x1
                       __HAS_EDS = 0x1
                        __HAS_EP = 0x1
          __TARGET_DIVIDE_CYCLES = 0x11
                        __C30ELF = 0x1
                        ___PA___ = 0x0
