MPLAB XC16 ASSEMBLY Listing:   			page 1


   1              	.file "/home/user/Documents/FST/Programming/project_sub-zero/lib/can-ids/table.c"
   2              	.section .debug_abbrev,info
   3                 	.Ldebug_abbrev0:
   4                 	.section .debug_info,info
   5                 	.Ldebug_info0:
   6                 	.section .debug_line,info
   7                 	.Ldebug_line0:
   8 0000 2D 00 00 00 	.section .text,code
   8      02 00 27 00 
   8      00 00 01 01 
   8      FB 0E 0A 00 
   8      01 01 01 01 
   8      00 00 00 01 
   8      6C 69 62 2F 
   8      63 61 6E 2D 
   8      69 64 73 00 
   9              	.Ltext0:
  10              	.global _nomes_placas
  11              	.section .const,psv,page
  12                 	.LC0:
  13 0000 30 00       	.asciz "0"
  14                 	.LC1:
  15 0002 31 00       	.asciz "1"
  16                 	.LC2:
  17 0004 32 00       	.asciz "2"
  18                 	.LC3:
  19 0006 33 00       	.asciz "3"
  20                 	.LC4:
  21 0008 34 00       	.asciz "4"
  22                 	.LC5:
  23 000a 35 00       	.asciz "5"
  24                 	.LC6:
  25 000c 36 00       	.asciz "6"
  26                 	.LC7:
  27 000e 37 00       	.asciz "7"
  28                 	.LC8:
  29 0010 44 43 55 00 	.asciz "DCU"
  30                 	.LC9:
  31 0014 54 45 00    	.asciz "TE"
  32                 	.LC10:
  33 0017 44 41 53 48 	.asciz "DASH"
  33      00 
  34                 	.LC11:
  35 001c 4C 4F 47 47 	.asciz "LOGGER"
  35      45 52 00 
  36                 	.LC12:
  37 0023 53 54 45 45 	.asciz "STEERING_WHEEL"
  37      52 49 4E 47 
  37      5F 57 48 45 
  37      45 4C 00 
  38                 	.LC13:
  39 0032 41 52 4D 00 	.asciz "ARM"
  40                 	.LC14:
  41 0036 42 4D 53 5F 	.asciz "BMS_MASTER"
  41      4D 41 53 54 
  41      45 52 00 
  42                 	.LC15:
MPLAB XC16 ASSEMBLY Listing:   			page 2


  43 0041 42 4D 53 5F 	.asciz "BMS_VERBOSE"
  43      56 45 52 42 
  43      4F 53 45 00 
  44                 	.LC16:
  45 004d 49 49 42 00 	.asciz "IIB"
  46                 	.LC17:
  47 0051 54 45 4C 45 	.asciz "TELEMETRY"
  47      4D 45 54 52 
  47      59 00 
  48                 	.LC18:
  49 005b 53 55 50 4F 	.asciz "SUPOT_FRONT"
  49      54 5F 46 52 
  49      4F 4E 54 00 
  50                 	.LC19:
  51 0067 53 55 50 4F 	.asciz "SUPOT_REAR"
  51      54 5F 52 45 
  51      41 52 00 
  52                 	.LC20:
  53 0072 41 48 52 53 	.asciz "AHRS"
  53      00 
  54                 	.LC21:
  55 0077 49 4D 55 00 	.asciz "IMU"
  56                 	.LC22:
  57 007b 47 50 53 00 	.asciz "GPS"
  58                 	.LC23:
  59 007f 43 43 55 00 	.asciz "CCU"
  60                 	.LC24:
  61 0083 54 49 52 45 	.asciz "TIRETEMP"
  61      54 45 4D 50 
  61      00 
  62                 	.LC25:
  63 008c 53 54 45 45 	.asciz "STEER"
  63      52 00 
  64                 	.LC26:
  65 0092 32 36 00    	.asciz "26"
  66                 	.LC27:
  67 0095 32 37 00    	.asciz "27"
  68                 	.LC28:
  69 0098 32 38 00    	.asciz "28"
  70                 	.LC29:
  71 009b 32 39 00    	.asciz "29"
  72                 	.LC30:
  73 009e 57 41 54 45 	.asciz "WATER_TEMP"
  73      52 5F 54 45 
  73      4D 50 00 
  74                 	.LC31:
  75 00a9 49 4E 54 45 	.asciz "INTERFACE"
  75      52 46 41 43 
  75      45 00 
  76 00b3 00          	.section .data,data
  77                 	.align 2
  78                 	.type _nomes_placas,@object
  79                 	.size _nomes_placas,64
  80                 	_nomes_placas:
  81 0000 00 00       	.word .LC0
  82 0002 00 00       	.word .LC1
  83 0004 00 00       	.word .LC2
MPLAB XC16 ASSEMBLY Listing:   			page 3


  84 0006 00 00       	.word .LC3
  85 0008 00 00       	.word .LC4
  86 000a 00 00       	.word .LC5
  87 000c 00 00       	.word .LC6
  88 000e 00 00       	.word .LC7
  89 0010 00 00       	.word .LC8
  90 0012 00 00       	.word .LC9
  91 0014 00 00       	.word .LC10
  92 0016 00 00       	.word .LC11
  93 0018 00 00       	.word .LC12
  94 001a 00 00       	.word .LC13
  95 001c 00 00       	.word .LC14
  96 001e 00 00       	.word .LC15
  97 0020 00 00       	.word .LC16
  98 0022 00 00       	.word .LC17
  99 0024 00 00       	.word .LC18
 100 0026 00 00       	.word .LC19
 101 0028 00 00       	.word .LC20
 102 002a 00 00       	.word .LC21
 103 002c 00 00       	.word .LC22
 104 002e 00 00       	.word .LC23
 105 0030 00 00       	.word .LC24
 106 0032 00 00       	.word .LC25
 107 0034 00 00       	.word .LC26
 108 0036 00 00       	.word .LC27
 109 0038 00 00       	.word .LC28
 110 003a 00 00       	.word .LC29
 111 003c 00 00       	.word .LC30
 112 003e 00 00       	.word .LC31
 113                 	.section .text,code
 114              	.Letext0:
 115              	.file 1 "lib/can-ids/table.c"
 116              	.section .debug_info,info
 117 0000 EE 00 00 00 	.4byte 0xee
 118 0004 02 00       	.2byte 0x2
 119 0006 00 00 00 00 	.4byte .Ldebug_abbrev0
 120 000a 04          	.byte 0x4
 121 000b 01          	.uleb128 0x1
 122 000c 47 4E 55 20 	.asciz "GNU C 4.5.1 (XC16, Microchip v1.36) (A) Build date: Dec  5 2018"
 122      43 20 34 2E 
 122      35 2E 31 20 
 122      28 58 43 31 
 122      36 2C 20 4D 
 122      69 63 72 6F 
 122      63 68 69 70 
 122      20 76 31 2E 
 122      33 36 29 20 
 123 004c 01          	.byte 0x1
 124 004d 6C 69 62 2F 	.asciz "lib/can-ids/table.c"
 124      63 61 6E 2D 
 124      69 64 73 2F 
 124      74 61 62 6C 
 124      65 2E 63 00 
 125 0061 2F 68 6F 6D 	.asciz "/home/user/Documents/FST/Programming/project_sub-zero"
 125      65 2F 75 73 
 125      65 72 2F 44 
 125      6F 63 75 6D 
MPLAB XC16 ASSEMBLY Listing:   			page 4


 125      65 6E 74 73 
 125      2F 46 53 54 
 125      2F 50 72 6F 
 125      67 72 61 6D 
 125      6D 69 6E 67 
 126 0097 00 00 00 00 	.4byte .Ltext0
 127 009b 00 00 00 00 	.4byte .Letext0
 128 009f 00 00 00 00 	.4byte .Ldebug_line0
 129 00a3 02          	.uleb128 0x2
 130 00a4 C3 00 00 00 	.4byte 0xc3
 131 00a8 B3 00 00 00 	.4byte 0xb3
 132 00ac 03          	.uleb128 0x3
 133 00ad B3 00 00 00 	.4byte 0xb3
 134 00b1 1F          	.byte 0x1f
 135 00b2 00          	.byte 0x0
 136 00b3 04          	.uleb128 0x4
 137 00b4 02          	.byte 0x2
 138 00b5 07          	.byte 0x7
 139 00b6 75 6E 73 69 	.asciz "unsigned int"
 139      67 6E 65 64 
 139      20 69 6E 74 
 139      00 
 140 00c3 05          	.uleb128 0x5
 141 00c4 02          	.byte 0x2
 142 00c5 C9 00 00 00 	.4byte 0xc9
 143 00c9 06          	.uleb128 0x6
 144 00ca CE 00 00 00 	.4byte 0xce
 145 00ce 04          	.uleb128 0x4
 146 00cf 01          	.byte 0x1
 147 00d0 06          	.byte 0x6
 148 00d1 63 68 61 72 	.asciz "char"
 148      00 
 149 00d6 07          	.uleb128 0x7
 150 00d7 6E 6F 6D 65 	.asciz "nomes_placas"
 150      73 5F 70 6C 
 150      61 63 61 73 
 150      00 
 151 00e4 01          	.byte 0x1
 152 00e5 03          	.byte 0x3
 153 00e6 A3 00 00 00 	.4byte 0xa3
 154 00ea 01          	.byte 0x1
 155 00eb 05          	.byte 0x5
 156 00ec 03          	.byte 0x3
 157 00ed 00 00 00 00 	.4byte _nomes_placas
 158 00f1 00          	.byte 0x0
 159                 	.section .debug_abbrev,info
 160 0000 01          	.uleb128 0x1
 161 0001 11          	.uleb128 0x11
 162 0002 01          	.byte 0x1
 163 0003 25          	.uleb128 0x25
 164 0004 08          	.uleb128 0x8
 165 0005 13          	.uleb128 0x13
 166 0006 0B          	.uleb128 0xb
 167 0007 03          	.uleb128 0x3
 168 0008 08          	.uleb128 0x8
 169 0009 1B          	.uleb128 0x1b
 170 000a 08          	.uleb128 0x8
MPLAB XC16 ASSEMBLY Listing:   			page 5


 171 000b 11          	.uleb128 0x11
 172 000c 01          	.uleb128 0x1
 173 000d 12          	.uleb128 0x12
 174 000e 01          	.uleb128 0x1
 175 000f 10          	.uleb128 0x10
 176 0010 06          	.uleb128 0x6
 177 0011 00          	.byte 0x0
 178 0012 00          	.byte 0x0
 179 0013 02          	.uleb128 0x2
 180 0014 01          	.uleb128 0x1
 181 0015 01          	.byte 0x1
 182 0016 49          	.uleb128 0x49
 183 0017 13          	.uleb128 0x13
 184 0018 01          	.uleb128 0x1
 185 0019 13          	.uleb128 0x13
 186 001a 00          	.byte 0x0
 187 001b 00          	.byte 0x0
 188 001c 03          	.uleb128 0x3
 189 001d 21          	.uleb128 0x21
 190 001e 00          	.byte 0x0
 191 001f 49          	.uleb128 0x49
 192 0020 13          	.uleb128 0x13
 193 0021 2F          	.uleb128 0x2f
 194 0022 0B          	.uleb128 0xb
 195 0023 00          	.byte 0x0
 196 0024 00          	.byte 0x0
 197 0025 04          	.uleb128 0x4
 198 0026 24          	.uleb128 0x24
 199 0027 00          	.byte 0x0
 200 0028 0B          	.uleb128 0xb
 201 0029 0B          	.uleb128 0xb
 202 002a 3E          	.uleb128 0x3e
 203 002b 0B          	.uleb128 0xb
 204 002c 03          	.uleb128 0x3
 205 002d 08          	.uleb128 0x8
 206 002e 00          	.byte 0x0
 207 002f 00          	.byte 0x0
 208 0030 05          	.uleb128 0x5
 209 0031 0F          	.uleb128 0xf
 210 0032 00          	.byte 0x0
 211 0033 0B          	.uleb128 0xb
 212 0034 0B          	.uleb128 0xb
 213 0035 49          	.uleb128 0x49
 214 0036 13          	.uleb128 0x13
 215 0037 00          	.byte 0x0
 216 0038 00          	.byte 0x0
 217 0039 06          	.uleb128 0x6
 218 003a 26          	.uleb128 0x26
 219 003b 00          	.byte 0x0
 220 003c 49          	.uleb128 0x49
 221 003d 13          	.uleb128 0x13
 222 003e 00          	.byte 0x0
 223 003f 00          	.byte 0x0
 224 0040 07          	.uleb128 0x7
 225 0041 34          	.uleb128 0x34
 226 0042 00          	.byte 0x0
 227 0043 03          	.uleb128 0x3
MPLAB XC16 ASSEMBLY Listing:   			page 6


 228 0044 08          	.uleb128 0x8
 229 0045 3A          	.uleb128 0x3a
 230 0046 0B          	.uleb128 0xb
 231 0047 3B          	.uleb128 0x3b
 232 0048 0B          	.uleb128 0xb
 233 0049 49          	.uleb128 0x49
 234 004a 13          	.uleb128 0x13
 235 004b 3F          	.uleb128 0x3f
 236 004c 0C          	.uleb128 0xc
 237 004d 02          	.uleb128 0x2
 238 004e 0A          	.uleb128 0xa
 239 004f 00          	.byte 0x0
 240 0050 00          	.byte 0x0
 241 0051 00          	.byte 0x0
 242                 	.section .debug_pubnames,info
 243 0000 1F 00 00 00 	.4byte 0x1f
 244 0004 02 00       	.2byte 0x2
 245 0006 00 00 00 00 	.4byte .Ldebug_info0
 246 000a F2 00 00 00 	.4byte 0xf2
 247 000e D6 00 00 00 	.4byte 0xd6
 248 0012 6E 6F 6D 65 	.asciz "nomes_placas"
 248      73 5F 70 6C 
 248      61 63 61 73 
 248      00 
 249 001f 00 00 00 00 	.4byte 0x0
 250                 	.section .debug_str,info
 251                 	.section .text,code
 252              	
 253              	
 254              	
 255              	.section __c30_info,info,bss
 256                 	__psv_trap_errata:
 257                 	
 258                 	.section __c30_signature,info,data
 259 0000 01 00       	.word 0x0001
 260 0002 00 00       	.word 0x0000
 261 0004 00 00       	.word 0x0000
 262                 	
 263                 	
 264                 	
 265                 	.set ___PA___,0
 266                 	.end
MPLAB XC16 ASSEMBLY Listing:   			page 7


DEFINED SYMBOLS
                            *ABS*:00000000 /home/user/Documents/FST/Programming/project_sub-zero/lib/can-ids/table.c
                            *ABS*:00000001 __33EP256MU806
                            *ABS*:00000001 __dsPIC33E
                            *ABS*:00000001 __HAS_DSP
                            *ABS*:00000001 __HAS_DMAV2
                            *ABS*:00000001 __HAS_CODEGUARD
                            *ABS*:00000001 __HAS_PMP_ENHANCED
                            *ABS*:00000001 __HAS_EDS
                            *ABS*:00000001 __HAS_EP
                            *ABS*:00000011 __TARGET_DIVIDE_CYCLES
                            *ABS*:00000001 __C30ELF
    {standard input}:80     .data:00000000 _nomes_placas
                            *ABS*:00000008 __ext_attr_.const
    {standard input}:256    __c30_info:00000000 __psv_trap_errata
    {standard input}:265    *ABS*:00000000 ___PA___
                           .const:00000000 .LC0
                           .const:00000002 .LC1
                           .const:00000004 .LC2
                           .const:00000006 .LC3
                           .const:00000008 .LC4
                           .const:0000000a .LC5
                           .const:0000000c .LC6
                           .const:0000000e .LC7
                           .const:00000010 .LC8
                           .const:00000014 .LC9
                           .const:00000017 .LC10
                           .const:0000001c .LC11
                           .const:00000023 .LC12
                           .const:00000032 .LC13
                           .const:00000036 .LC14
                           .const:00000041 .LC15
                           .const:0000004d .LC16
                           .const:00000051 .LC17
                           .const:0000005b .LC18
                           .const:00000067 .LC19
                           .const:00000072 .LC20
                           .const:00000077 .LC21
                           .const:0000007b .LC22
                           .const:0000007f .LC23
                           .const:00000083 .LC24
                           .const:0000008c .LC25
                           .const:00000092 .LC26
                           .const:00000095 .LC27
                           .const:00000098 .LC28
                           .const:0000009b .LC29
                           .const:0000009e .LC30
                           .const:000000a9 .LC31
                    .debug_abbrev:00000000 .Ldebug_abbrev0
                            .text:00000000 .Ltext0
                            .text:00000000 .Letext0
                      .debug_line:00000000 .Ldebug_line0
                      .debug_info:00000000 .Ldebug_info0

NO UNDEFINED SYMBOLS

EQUATE SYMBOLS
/home/user/Documents/FST/Programming/project_sub-zero/lib/can-ids/table.c = 0x0
                  __33EP256MU806 = 0x1
                      __dsPIC33E = 0x1
                       __HAS_DSP = 0x1
                     __HAS_DMAV2 = 0x1
                 __HAS_CODEGUARD = 0x1
              __HAS_PMP_ENHANCED = 0x1
                       __HAS_EDS = 0x1
                        __HAS_EP = 0x1
          __TARGET_DIVIDE_CYCLES = 0x11
                        __C30ELF = 0x1
               __ext_attr_.const = 0x8
                        ___PA___ = 0x0
