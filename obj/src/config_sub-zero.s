MPLAB XC16 ASSEMBLY Listing:   			page 1


   1              	.file "/home/user/Documents/FST/Programming/project_sub-zero/src/config_sub-zero.c"
   2              	.section .debug_abbrev,info
   3                 	.Ldebug_abbrev0:
   4                 	.section .debug_info,info
   5                 	.Ldebug_info0:
   6                 	.section .debug_line,info
   7                 	.Ldebug_line0:
   8 0000 0D 01 00 00 	.section .text,code
   8      02 00 D2 00 
   8      00 00 01 01 
   8      FB 0E 0A 00 
   8      01 01 01 01 
   8      00 00 00 01 
   8      73 72 63 00 
   8      2F 6F 70 74 
   8      2F 6D 69 63 
   9              	.Ltext0:
  10              	.section .text,code
  11              	.align 2
  12              	.global _config_IO
  13              	.type _config_IO,@function
  14              	_config_IO:
  15              	.LFB0:
  16              	.file 1 "src/config_sub-zero.c"
   1:src/config_sub-zero.c **** #include <xc.h>
   2:src/config_sub-zero.c **** #include "config_sub-zero.h"
   3:src/config_sub-zero.c **** #include "lib_pic33e/pps.h"
   4:src/config_sub-zero.c **** #include "lib_pic33e/ADC.h"
   5:src/config_sub-zero.c **** #include "lib_pic33e/external.h"
   6:src/config_sub-zero.c **** 
   7:src/config_sub-zero.c **** void config_IO()
   8:src/config_sub-zero.c **** {
  17              	.loc 1 8 0
  18              	.set ___PA___,1
  19 000000  00 00 FA 	lnk #0
  20              	.LCFI0:
   9:src/config_sub-zero.c **** 
  10:src/config_sub-zero.c ****     //Peripheral Pin Select:
  11:src/config_sub-zero.c ****     PPSUnLock;
  21              	.loc 1 11 0
  22 000002  02 00 80 	mov _OSCCON,w2
  23 000004  F1 0B 20 	mov #191,w1
  24 000006  60 04 20 	mov #70,w0
  25 000008  01 01 61 	and w2,w1,w2
  26 00000a  71 05 20 	mov #87,w1
  27 00000c  03 00 20 	mov #_OSCCON,w3
  28 00000e  80 49 78 	mov.b w0,[w3]
  29 000010  81 49 78 	mov.b w1,[w3]
  30 000012  82 49 78 	mov.b w2,[w3]
  12:src/config_sub-zero.c ****     	//CAN
  13:src/config_sub-zero.c **** 	PPSOutput(OUT_FN_PPS_C1TX, OUT_PIN_PPS_RP96);     //CAN FOR THE DEVBOARD
  31              	.loc 1 13 0
  32 000014  01 00 80 	mov _RPOR7bits,w1
  33 000016  00 FC 2F 	mov #-64,w0
  14:src/config_sub-zero.c **** 	PPSInput(IN_FN_PPS_C1RX, IN_PIN_PPS_RP97);
  34              	.loc 1 14 0
  35 000018  03 F8 2F 	mov #-128,w3
MPLAB XC16 ASSEMBLY Listing:   			page 2


  36              	.loc 1 13 0
  37 00001a  00 80 60 	and w1,w0,w0
  38              	.loc 1 14 0
  39 00001c  12 06 20 	mov #97,w2
  40              	.loc 1 13 0
  41 00001e  80 00 78 	mov w0,w1
  42 000020  E1 00 B3 	ior #14,w1
  15:src/config_sub-zero.c **** 		//External Interrupts:
  16:src/config_sub-zero.c **** 	//PPSInput(IN_FN_PPS_INT1, IN_PIN_PPS_RPI34);
  17:src/config_sub-zero.c **** 	//PPSInput(IN_FN_PPS_INT2, IN_PIN_PPS_RPI37);
  18:src/config_sub-zero.c **** 	//PPSInput(IN_FN_PPS_INT3, IN_PIN_PPS_RPI86);
  19:src/config_sub-zero.c **** 		//Output Compare PWM:
  20:src/config_sub-zero.c **** 			//OC1 PUMP Control
  21:src/config_sub-zero.c **** 	//PPSOutput(OUT_FN_PPS_OC1, OUT_PIN_PPS_RP85);
  22:src/config_sub-zero.c **** 			//OC2 FAN Control
  23:src/config_sub-zero.c **** 	//PPSOutput(OUT_FN_PPS_OC2, OUT_PIN_PPS_RP87);
  24:src/config_sub-zero.c ****     PPSLock;
  43              	.loc 1 24 0
  44 000022  60 04 20 	mov #70,w0
  45              	.loc 1 13 0
  46 000024  01 00 88 	mov w1,_RPOR7bits
  47              	.loc 1 24 0
  48 000026  71 05 20 	mov #87,w1
  49              	.loc 1 14 0
  50 000028  04 00 80 	mov _RPINR26bits,w4
  51 00002a  83 01 62 	and w4,w3,w3
  52 00002c  03 01 71 	ior w2,w3,w2
  53 00002e  02 00 88 	mov w2,_RPINR26bits
  54              	.loc 1 24 0
  55 000030  02 00 80 	mov _OSCCON,w2
  56 000032  02 60 A0 	bset w2,#6
  57 000034  03 00 20 	mov #_OSCCON,w3
  58 000036  80 49 78 	mov.b w0,[w3]
  59 000038  81 49 78 	mov.b w1,[w3]
  60 00003a  82 49 78 	mov.b w2,[w3]
  25:src/config_sub-zero.c **** }
  61              	.loc 1 25 0
  62 00003c  8E 07 78 	mov w14,w15
  63 00003e  4F 07 78 	mov [--w15],w14
  64 000040  00 40 A9 	bclr CORCON,#2
  65 000042  00 00 06 	return 
  66              	.set ___PA___,0
  67              	.LFE0:
  68              	.size _config_IO,.-_config_IO
  69              	.align 2
  70              	.global _config_adc
  71              	.type _config_adc,@function
  72              	_config_adc:
  73              	.LFB1:
  26:src/config_sub-zero.c **** 
  27:src/config_sub-zero.c **** /*void config_timer3_for_PWM()
  28:src/config_sub-zero.c **** {
  29:src/config_sub-zero.c ****     //Timer3 configuration to be used for PWM cycle period.
  30:src/config_sub-zero.c ****     T3CON = 0;
  31:src/config_sub-zero.c ****     T3CONbits.TCKPS = 0b11;    //1:256 prescaler;
  32:src/config_sub-zero.c ****     TMR3 = 0;
  33:src/config_sub-zero.c ****     //PR3 é ignorado no modo de PWM
MPLAB XC16 ASSEMBLY Listing:   			page 3


  34:src/config_sub-zero.c ****     IFS0bits.T3IF = 0;      //Clear Timer interrupt flag;
  35:src/config_sub-zero.c ****     IEC0bits.T3IE = 0;      //Disable Timer interrupt;
  36:src/config_sub-zero.c ****     //Start timer
  37:src/config_sub-zero.c ****     T3CONbits.TON = 1;
  38:src/config_sub-zero.c **** }
  39:src/config_sub-zero.c **** 
  40:src/config_sub-zero.c **** void config_OC1()
  41:src/config_sub-zero.c **** {
  42:src/config_sub-zero.c ****     //Output Compare configuration;
  43:src/config_sub-zero.c ****     OC1CON1 = 0;
  44:src/config_sub-zero.c ****     OC1CON1bits.OCSIDL = 0;     //No idle mode;
  45:src/config_sub-zero.c ****     OC1CON1bits.OCTSEL = 0b001; //Timer3 as the clock;
  46:src/config_sub-zero.c ****     OC1CON1bits.OCM = 0b110;    //Edge aligned PWM mode;
  47:src/config_sub-zero.c ****     OC1CON2 = 0;
  48:src/config_sub-zero.c ****     OC1CON2bits.SYNCSEL = 31;   //Sync using OC1RS
  49:src/config_sub-zero.c ****     OC1RS = 32000;
  50:src/config_sub-zero.c ****     OC1R = 16000;
  51:src/config_sub-zero.c ****     //Interrupts
  52:src/config_sub-zero.c ****     IFS0bits.OC1IF = 0;
  53:src/config_sub-zero.c ****     IEC0bits.OC1IE = 0;
  54:src/config_sub-zero.c **** }*/
  55:src/config_sub-zero.c **** 
  56:src/config_sub-zero.c **** void config_adc()
  57:src/config_sub-zero.c **** {
  74              	.loc 1 57 0
  75              	.set ___PA___,1
  76 000044  0A 00 FA 	lnk #10
  77              	.LCFI1:
  58:src/config_sub-zero.c **** 
  59:src/config_sub-zero.c ****       //config IO
  60:src/config_sub-zero.c ****       ANSELBbits.ANSB9 = 1;
  61:src/config_sub-zero.c ****       TRISBbits.TRISB9 = 1;
  62:src/config_sub-zero.c **** 
  63:src/config_sub-zero.c ****       ADC_parameters adc_config;
  64:src/config_sub-zero.c **** 
  65:src/config_sub-zero.c ****       config_pin_cycling(AN9 | AN5, 250000UL, 64, 4, &adc_config);
  78              	.loc 1 65 0
  79 000046  02 09 2D 	mov #53392,w2
  80 000048  33 00 20 	mov #3,w3
  81              	.loc 1 60 0
  82 00004a  01 20 A8 	bset.b _ANSELBbits+1,#1
  83              	.loc 1 65 0
  84 00004c  41 00 20 	mov #4,w1
  85 00004e  04 04 20 	mov #64,w4
  86 000050  05 00 20 	mov #0,w5
  87              	.loc 1 61 0
  88 000052  01 20 A8 	bset.b _TRISBbits+1,#1
  89              	.loc 1 65 0
  90 000054  0E 03 78 	mov w14,w6
  91 000056  00 22 20 	mov #544,w0
  92 000058  00 00 07 	rcall _config_pin_cycling
  66:src/config_sub-zero.c ****       config_adc1(adc_config);
  93              	.loc 1 66 0
  94 00005a  1E 00 78 	mov [w14],w0
  95 00005c  9E 00 90 	mov [w14+2],w1
  96 00005e  2E 01 90 	mov [w14+4],w2
  97 000060  BE 01 90 	mov [w14+6],w3
MPLAB XC16 ASSEMBLY Listing:   			page 4


  98 000062  4E 02 90 	mov [w14+8],w4
  99 000064  00 00 07 	rcall _config_adc1
  67:src/config_sub-zero.c **** }
 100              	.loc 1 67 0
 101 000066  8E 07 78 	mov w14,w15
 102 000068  4F 07 78 	mov [--w15],w14
 103 00006a  00 40 A9 	bclr CORCON,#2
 104 00006c  00 00 06 	return 
 105              	.set ___PA___,0
 106              	.LFE1:
 107              	.size _config_adc,.-_config_adc
 108              	.align 2
 109              	.global _external1_callback
 110              	.type _external1_callback,@function
 111              	_external1_callback:
 112              	.LFB2:
  68:src/config_sub-zero.c **** 
  69:src/config_sub-zero.c **** void external1_callback(void){
 113              	.loc 1 69 0
 114              	.set ___PA___,1
 115 00006e  00 00 FA 	lnk #0
 116              	.LCFI2:
  70:src/config_sub-zero.c ****       LATEbits.LATE7 ^= 1;
 117              	.loc 1 70 0
 118 000070  00 00 80 	mov _LATEbits,w0
 119 000072  01 00 80 	mov _LATEbits,w1
 120 000074  47 00 DE 	lsr w0,#7,w0
 121 000076  01 70 A1 	bclr w1,#7
 122 000078  61 40 60 	and.b w0,#1,w0
 123 00007a  00 04 A2 	btg.b w0,#0
 124 00007c  61 40 60 	and.b w0,#1,w0
 125 00007e  00 80 FB 	ze w0,w0
 126 000080  61 00 60 	and w0,#1,w0
 127 000082  47 00 DD 	sl w0,#7,w0
 128 000084  01 00 70 	ior w0,w1,w0
 129 000086  00 00 88 	mov w0,_LATEbits
  71:src/config_sub-zero.c **** }
 130              	.loc 1 71 0
 131 000088  8E 07 78 	mov w14,w15
 132 00008a  4F 07 78 	mov [--w15],w14
 133 00008c  00 40 A9 	bclr CORCON,#2
 134 00008e  00 00 06 	return 
 135              	.set ___PA___,0
 136              	.LFE2:
 137              	.size _external1_callback,.-_external1_callback
 138              	.align 2
 139              	.global _tmr2_int
 140              	.type _tmr2_int,@function
 141              	_tmr2_int:
 142              	.LFB3:
  72:src/config_sub-zero.c **** 
  73:src/config_sub-zero.c **** void tmr2_int(){
 143              	.loc 1 73 0
 144              	.set ___PA___,1
 145 000090  00 00 FA 	lnk #0
 146              	.LCFI3:
  74:src/config_sub-zero.c ****       send_adc_value();
MPLAB XC16 ASSEMBLY Listing:   			page 5


 147              	.loc 1 74 0
 148 000092  00 00 07 	rcall _send_adc_value
  75:src/config_sub-zero.c **** }
 149              	.loc 1 75 0
 150 000094  8E 07 78 	mov w14,w15
 151 000096  4F 07 78 	mov [--w15],w14
 152 000098  00 40 A9 	bclr CORCON,#2
 153 00009a  00 00 06 	return 
 154              	.set ___PA___,0
 155              	.LFE3:
 156              	.size _tmr2_int,.-_tmr2_int
 157              	.section .debug_frame,info
 158                 	.Lframe0:
 159 0000 10 00 00 00 	.4byte .LECIE0-.LSCIE0
 160                 	.LSCIE0:
 161 0004 FF FF FF FF 	.4byte 0xffffffff
 162 0008 01          	.byte 0x1
 163 0009 00          	.byte 0
 164 000a 01          	.uleb128 0x1
 165 000b 02          	.sleb128 2
 166 000c 25          	.byte 0x25
 167 000d 12          	.byte 0x12
 168 000e 0F          	.uleb128 0xf
 169 000f 7E          	.sleb128 -2
 170 0010 09          	.byte 0x9
 171 0011 25          	.uleb128 0x25
 172 0012 0F          	.uleb128 0xf
 173 0013 00          	.align 4
 174                 	.LECIE0:
 175                 	.LSFDE0:
 176 0014 18 00 00 00 	.4byte .LEFDE0-.LASFDE0
 177                 	.LASFDE0:
 178 0018 00 00 00 00 	.4byte .Lframe0
 179 001c 00 00 00 00 	.4byte .LFB0
 180 0020 44 00 00 00 	.4byte .LFE0-.LFB0
 181 0024 04          	.byte 0x4
 182 0025 02 00 00 00 	.4byte .LCFI0-.LFB0
 183 0029 13          	.byte 0x13
 184 002a 7D          	.sleb128 -3
 185 002b 0D          	.byte 0xd
 186 002c 0E          	.uleb128 0xe
 187 002d 8E          	.byte 0x8e
 188 002e 02          	.uleb128 0x2
 189 002f 00          	.align 4
 190                 	.LEFDE0:
 191                 	.LSFDE2:
 192 0030 18 00 00 00 	.4byte .LEFDE2-.LASFDE2
 193                 	.LASFDE2:
 194 0034 00 00 00 00 	.4byte .Lframe0
 195 0038 00 00 00 00 	.4byte .LFB1
 196 003c 2A 00 00 00 	.4byte .LFE1-.LFB1
 197 0040 04          	.byte 0x4
 198 0041 02 00 00 00 	.4byte .LCFI1-.LFB1
 199 0045 13          	.byte 0x13
 200 0046 7D          	.sleb128 -3
 201 0047 0D          	.byte 0xd
 202 0048 0E          	.uleb128 0xe
MPLAB XC16 ASSEMBLY Listing:   			page 6


 203 0049 8E          	.byte 0x8e
 204 004a 02          	.uleb128 0x2
 205 004b 00          	.align 4
 206                 	.LEFDE2:
 207                 	.LSFDE4:
 208 004c 18 00 00 00 	.4byte .LEFDE4-.LASFDE4
 209                 	.LASFDE4:
 210 0050 00 00 00 00 	.4byte .Lframe0
 211 0054 00 00 00 00 	.4byte .LFB2
 212 0058 22 00 00 00 	.4byte .LFE2-.LFB2
 213 005c 04          	.byte 0x4
 214 005d 02 00 00 00 	.4byte .LCFI2-.LFB2
 215 0061 13          	.byte 0x13
 216 0062 7D          	.sleb128 -3
 217 0063 0D          	.byte 0xd
 218 0064 0E          	.uleb128 0xe
 219 0065 8E          	.byte 0x8e
 220 0066 02          	.uleb128 0x2
 221 0067 00          	.align 4
 222                 	.LEFDE4:
 223                 	.LSFDE6:
 224 0068 18 00 00 00 	.4byte .LEFDE6-.LASFDE6
 225                 	.LASFDE6:
 226 006c 00 00 00 00 	.4byte .Lframe0
 227 0070 00 00 00 00 	.4byte .LFB3
 228 0074 0C 00 00 00 	.4byte .LFE3-.LFB3
 229 0078 04          	.byte 0x4
 230 0079 02 00 00 00 	.4byte .LCFI3-.LFB3
 231 007d 13          	.byte 0x13
 232 007e 7D          	.sleb128 -3
 233 007f 0D          	.byte 0xd
 234 0080 0E          	.uleb128 0xe
 235 0081 8E          	.byte 0x8e
 236 0082 02          	.uleb128 0x2
 237 0083 00          	.align 4
 238                 	.LEFDE6:
 239                 	.section .text,code
 240              	.Letext0:
 241              	.file 2 "/opt/microchip/xc16/v1.36/bin/bin/../../support/dsPIC33E/h/p33EP256MU806.h"
 242              	.file 3 "/opt/microchip/xc16/v1.36/bin/bin/../../include/lega-c/stdint.h"
 243              	.file 4 "lib/lib_pic33e/ADC.h"
 244              	.section .debug_info,info
 245 0000 26 0C 00 00 	.4byte 0xc26
 246 0004 02 00       	.2byte 0x2
 247 0006 00 00 00 00 	.4byte .Ldebug_abbrev0
 248 000a 04          	.byte 0x4
 249 000b 01          	.uleb128 0x1
 250 000c 47 4E 55 20 	.asciz "GNU C 4.5.1 (XC16, Microchip v1.36) (A) Build date: Dec  5 2018"
 250      43 20 34 2E 
 250      35 2E 31 20 
 250      28 58 43 31 
 250      36 2C 20 4D 
 250      69 63 72 6F 
 250      63 68 69 70 
 250      20 76 31 2E 
 250      33 36 29 20 
 251 004c 01          	.byte 0x1
MPLAB XC16 ASSEMBLY Listing:   			page 7


 252 004d 73 72 63 2F 	.asciz "src/config_sub-zero.c"
 252      63 6F 6E 66 
 252      69 67 5F 73 
 252      75 62 2D 7A 
 252      65 72 6F 2E 
 252      63 00 
 253 0063 2F 68 6F 6D 	.asciz "/home/user/Documents/FST/Programming/project_sub-zero"
 253      65 2F 75 73 
 253      65 72 2F 44 
 253      6F 63 75 6D 
 253      65 6E 74 73 
 253      2F 46 53 54 
 253      2F 50 72 6F 
 253      67 72 61 6D 
 253      6D 69 6E 67 
 254 0099 00 00 00 00 	.4byte .Ltext0
 255 009d 00 00 00 00 	.4byte .Letext0
 256 00a1 00 00 00 00 	.4byte .Ldebug_line0
 257 00a5 02          	.uleb128 0x2
 258 00a6 01          	.byte 0x1
 259 00a7 06          	.byte 0x6
 260 00a8 73 69 67 6E 	.asciz "signed char"
 260      65 64 20 63 
 260      68 61 72 00 
 261 00b4 02          	.uleb128 0x2
 262 00b5 02          	.byte 0x2
 263 00b6 05          	.byte 0x5
 264 00b7 69 6E 74 00 	.asciz "int"
 265 00bb 02          	.uleb128 0x2
 266 00bc 04          	.byte 0x4
 267 00bd 05          	.byte 0x5
 268 00be 6C 6F 6E 67 	.asciz "long int"
 268      20 69 6E 74 
 268      00 
 269 00c7 02          	.uleb128 0x2
 270 00c8 08          	.byte 0x8
 271 00c9 05          	.byte 0x5
 272 00ca 6C 6F 6E 67 	.asciz "long long int"
 272      20 6C 6F 6E 
 272      67 20 69 6E 
 272      74 00 
 273 00d8 02          	.uleb128 0x2
 274 00d9 01          	.byte 0x1
 275 00da 08          	.byte 0x8
 276 00db 75 6E 73 69 	.asciz "unsigned char"
 276      67 6E 65 64 
 276      20 63 68 61 
 276      72 00 
 277 00e9 03          	.uleb128 0x3
 278 00ea 75 69 6E 74 	.asciz "uint16_t"
 278      31 36 5F 74 
 278      00 
 279 00f3 03          	.byte 0x3
 280 00f4 31          	.byte 0x31
 281 00f5 F9 00 00 00 	.4byte 0xf9
 282 00f9 02          	.uleb128 0x2
 283 00fa 02          	.byte 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 8


 284 00fb 07          	.byte 0x7
 285 00fc 75 6E 73 69 	.asciz "unsigned int"
 285      67 6E 65 64 
 285      20 69 6E 74 
 285      00 
 286 0109 02          	.uleb128 0x2
 287 010a 04          	.byte 0x4
 288 010b 07          	.byte 0x7
 289 010c 6C 6F 6E 67 	.asciz "long unsigned int"
 289      20 75 6E 73 
 289      69 67 6E 65 
 289      64 20 69 6E 
 289      74 00 
 290 011e 02          	.uleb128 0x2
 291 011f 08          	.byte 0x8
 292 0120 07          	.byte 0x7
 293 0121 6C 6F 6E 67 	.asciz "long long unsigned int"
 293      20 6C 6F 6E 
 293      67 20 75 6E 
 293      73 69 67 6E 
 293      65 64 20 69 
 293      6E 74 00 
 294 0138 04          	.uleb128 0x4
 295 0139 02          	.byte 0x2
 296 013a 02          	.byte 0x2
 297 013b 79 1E       	.2byte 0x1e79
 298 013d 6A 01 00 00 	.4byte 0x16a
 299 0141 05          	.uleb128 0x5
 300 0142 52 50 39 36 	.asciz "RP96R"
 300      52 00 
 301 0148 02          	.byte 0x2
 302 0149 7A 1E       	.2byte 0x1e7a
 303 014b E9 00 00 00 	.4byte 0xe9
 304 014f 02          	.byte 0x2
 305 0150 06          	.byte 0x6
 306 0151 0A          	.byte 0xa
 307 0152 02          	.byte 0x2
 308 0153 23          	.byte 0x23
 309 0154 00          	.uleb128 0x0
 310 0155 05          	.uleb128 0x5
 311 0156 52 50 39 37 	.asciz "RP97R"
 311      52 00 
 312 015c 02          	.byte 0x2
 313 015d 7C 1E       	.2byte 0x1e7c
 314 015f E9 00 00 00 	.4byte 0xe9
 315 0163 02          	.byte 0x2
 316 0164 06          	.byte 0x6
 317 0165 02          	.byte 0x2
 318 0166 02          	.byte 0x2
 319 0167 23          	.byte 0x23
 320 0168 00          	.uleb128 0x0
 321 0169 00          	.byte 0x0
 322 016a 04          	.uleb128 0x4
 323 016b 02          	.byte 0x2
 324 016c 02          	.byte 0x2
 325 016d 7E 1E       	.2byte 0x1e7e
 326 016f 70 02 00 00 	.4byte 0x270
MPLAB XC16 ASSEMBLY Listing:   			page 9


 327 0173 05          	.uleb128 0x5
 328 0174 52 50 39 36 	.asciz "RP96R0"
 328      52 30 00 
 329 017b 02          	.byte 0x2
 330 017c 7F 1E       	.2byte 0x1e7f
 331 017e E9 00 00 00 	.4byte 0xe9
 332 0182 02          	.byte 0x2
 333 0183 01          	.byte 0x1
 334 0184 0F          	.byte 0xf
 335 0185 02          	.byte 0x2
 336 0186 23          	.byte 0x23
 337 0187 00          	.uleb128 0x0
 338 0188 05          	.uleb128 0x5
 339 0189 52 50 39 36 	.asciz "RP96R1"
 339      52 31 00 
 340 0190 02          	.byte 0x2
 341 0191 80 1E       	.2byte 0x1e80
 342 0193 E9 00 00 00 	.4byte 0xe9
 343 0197 02          	.byte 0x2
 344 0198 01          	.byte 0x1
 345 0199 0E          	.byte 0xe
 346 019a 02          	.byte 0x2
 347 019b 23          	.byte 0x23
 348 019c 00          	.uleb128 0x0
 349 019d 05          	.uleb128 0x5
 350 019e 52 50 39 36 	.asciz "RP96R2"
 350      52 32 00 
 351 01a5 02          	.byte 0x2
 352 01a6 81 1E       	.2byte 0x1e81
 353 01a8 E9 00 00 00 	.4byte 0xe9
 354 01ac 02          	.byte 0x2
 355 01ad 01          	.byte 0x1
 356 01ae 0D          	.byte 0xd
 357 01af 02          	.byte 0x2
 358 01b0 23          	.byte 0x23
 359 01b1 00          	.uleb128 0x0
 360 01b2 05          	.uleb128 0x5
 361 01b3 52 50 39 36 	.asciz "RP96R3"
 361      52 33 00 
 362 01ba 02          	.byte 0x2
 363 01bb 82 1E       	.2byte 0x1e82
 364 01bd E9 00 00 00 	.4byte 0xe9
 365 01c1 02          	.byte 0x2
 366 01c2 01          	.byte 0x1
 367 01c3 0C          	.byte 0xc
 368 01c4 02          	.byte 0x2
 369 01c5 23          	.byte 0x23
 370 01c6 00          	.uleb128 0x0
 371 01c7 05          	.uleb128 0x5
 372 01c8 52 50 39 36 	.asciz "RP96R4"
 372      52 34 00 
 373 01cf 02          	.byte 0x2
 374 01d0 83 1E       	.2byte 0x1e83
 375 01d2 E9 00 00 00 	.4byte 0xe9
 376 01d6 02          	.byte 0x2
 377 01d7 01          	.byte 0x1
 378 01d8 0B          	.byte 0xb
MPLAB XC16 ASSEMBLY Listing:   			page 10


 379 01d9 02          	.byte 0x2
 380 01da 23          	.byte 0x23
 381 01db 00          	.uleb128 0x0
 382 01dc 05          	.uleb128 0x5
 383 01dd 52 50 39 36 	.asciz "RP96R5"
 383      52 35 00 
 384 01e4 02          	.byte 0x2
 385 01e5 84 1E       	.2byte 0x1e84
 386 01e7 E9 00 00 00 	.4byte 0xe9
 387 01eb 02          	.byte 0x2
 388 01ec 01          	.byte 0x1
 389 01ed 0A          	.byte 0xa
 390 01ee 02          	.byte 0x2
 391 01ef 23          	.byte 0x23
 392 01f0 00          	.uleb128 0x0
 393 01f1 05          	.uleb128 0x5
 394 01f2 52 50 39 37 	.asciz "RP97R0"
 394      52 30 00 
 395 01f9 02          	.byte 0x2
 396 01fa 86 1E       	.2byte 0x1e86
 397 01fc E9 00 00 00 	.4byte 0xe9
 398 0200 02          	.byte 0x2
 399 0201 01          	.byte 0x1
 400 0202 07          	.byte 0x7
 401 0203 02          	.byte 0x2
 402 0204 23          	.byte 0x23
 403 0205 00          	.uleb128 0x0
 404 0206 05          	.uleb128 0x5
 405 0207 52 50 39 37 	.asciz "RP97R1"
 405      52 31 00 
 406 020e 02          	.byte 0x2
 407 020f 87 1E       	.2byte 0x1e87
 408 0211 E9 00 00 00 	.4byte 0xe9
 409 0215 02          	.byte 0x2
 410 0216 01          	.byte 0x1
 411 0217 06          	.byte 0x6
 412 0218 02          	.byte 0x2
 413 0219 23          	.byte 0x23
 414 021a 00          	.uleb128 0x0
 415 021b 05          	.uleb128 0x5
 416 021c 52 50 39 37 	.asciz "RP97R2"
 416      52 32 00 
 417 0223 02          	.byte 0x2
 418 0224 88 1E       	.2byte 0x1e88
 419 0226 E9 00 00 00 	.4byte 0xe9
 420 022a 02          	.byte 0x2
 421 022b 01          	.byte 0x1
 422 022c 05          	.byte 0x5
 423 022d 02          	.byte 0x2
 424 022e 23          	.byte 0x23
 425 022f 00          	.uleb128 0x0
 426 0230 05          	.uleb128 0x5
 427 0231 52 50 39 37 	.asciz "RP97R3"
 427      52 33 00 
 428 0238 02          	.byte 0x2
 429 0239 89 1E       	.2byte 0x1e89
 430 023b E9 00 00 00 	.4byte 0xe9
MPLAB XC16 ASSEMBLY Listing:   			page 11


 431 023f 02          	.byte 0x2
 432 0240 01          	.byte 0x1
 433 0241 04          	.byte 0x4
 434 0242 02          	.byte 0x2
 435 0243 23          	.byte 0x23
 436 0244 00          	.uleb128 0x0
 437 0245 05          	.uleb128 0x5
 438 0246 52 50 39 37 	.asciz "RP97R4"
 438      52 34 00 
 439 024d 02          	.byte 0x2
 440 024e 8A 1E       	.2byte 0x1e8a
 441 0250 E9 00 00 00 	.4byte 0xe9
 442 0254 02          	.byte 0x2
 443 0255 01          	.byte 0x1
 444 0256 03          	.byte 0x3
 445 0257 02          	.byte 0x2
 446 0258 23          	.byte 0x23
 447 0259 00          	.uleb128 0x0
 448 025a 05          	.uleb128 0x5
 449 025b 52 50 39 37 	.asciz "RP97R5"
 449      52 35 00 
 450 0262 02          	.byte 0x2
 451 0263 8B 1E       	.2byte 0x1e8b
 452 0265 E9 00 00 00 	.4byte 0xe9
 453 0269 02          	.byte 0x2
 454 026a 01          	.byte 0x1
 455 026b 02          	.byte 0x2
 456 026c 02          	.byte 0x2
 457 026d 23          	.byte 0x23
 458 026e 00          	.uleb128 0x0
 459 026f 00          	.byte 0x0
 460 0270 06          	.uleb128 0x6
 461 0271 02          	.byte 0x2
 462 0272 02          	.byte 0x2
 463 0273 78 1E       	.2byte 0x1e78
 464 0275 84 02 00 00 	.4byte 0x284
 465 0279 07          	.uleb128 0x7
 466 027a 38 01 00 00 	.4byte 0x138
 467 027e 07          	.uleb128 0x7
 468 027f 6A 01 00 00 	.4byte 0x16a
 469 0283 00          	.byte 0x0
 470 0284 08          	.uleb128 0x8
 471 0285 74 61 67 52 	.asciz "tagRPOR7BITS"
 471      50 4F 52 37 
 471      42 49 54 53 
 471      00 
 472 0292 02          	.byte 0x2
 473 0293 02          	.byte 0x2
 474 0294 77 1E       	.2byte 0x1e77
 475 0296 A3 02 00 00 	.4byte 0x2a3
 476 029a 09          	.uleb128 0x9
 477 029b 70 02 00 00 	.4byte 0x270
 478 029f 02          	.byte 0x2
 479 02a0 23          	.byte 0x23
 480 02a1 00          	.uleb128 0x0
 481 02a2 00          	.byte 0x0
 482 02a3 0A          	.uleb128 0xa
MPLAB XC16 ASSEMBLY Listing:   			page 12


 483 02a4 52 50 4F 52 	.asciz "RPOR7BITS"
 483      37 42 49 54 
 483      53 00 
 484 02ae 02          	.byte 0x2
 485 02af 8E 1E       	.2byte 0x1e8e
 486 02b1 84 02 00 00 	.4byte 0x284
 487 02b5 04          	.uleb128 0x4
 488 02b6 02          	.byte 0x2
 489 02b7 02          	.byte 0x2
 490 02b8 B6 21       	.2byte 0x21b6
 491 02ba E7 02 00 00 	.4byte 0x2e7
 492 02be 05          	.uleb128 0x5
 493 02bf 43 31 52 58 	.asciz "C1RXR"
 493      52 00 
 494 02c5 02          	.byte 0x2
 495 02c6 B7 21       	.2byte 0x21b7
 496 02c8 E9 00 00 00 	.4byte 0xe9
 497 02cc 02          	.byte 0x2
 498 02cd 07          	.byte 0x7
 499 02ce 09          	.byte 0x9
 500 02cf 02          	.byte 0x2
 501 02d0 23          	.byte 0x23
 502 02d1 00          	.uleb128 0x0
 503 02d2 05          	.uleb128 0x5
 504 02d3 43 32 52 58 	.asciz "C2RXR"
 504      52 00 
 505 02d9 02          	.byte 0x2
 506 02da B9 21       	.2byte 0x21b9
 507 02dc E9 00 00 00 	.4byte 0xe9
 508 02e0 02          	.byte 0x2
 509 02e1 07          	.byte 0x7
 510 02e2 01          	.byte 0x1
 511 02e3 02          	.byte 0x2
 512 02e4 23          	.byte 0x23
 513 02e5 00          	.uleb128 0x0
 514 02e6 00          	.byte 0x0
 515 02e7 04          	.uleb128 0x4
 516 02e8 02          	.byte 0x2
 517 02e9 02          	.byte 0x2
 518 02ea BB 21       	.2byte 0x21bb
 519 02ec 17 04 00 00 	.4byte 0x417
 520 02f0 05          	.uleb128 0x5
 521 02f1 43 31 52 58 	.asciz "C1RXR0"
 521      52 30 00 
 522 02f8 02          	.byte 0x2
 523 02f9 BC 21       	.2byte 0x21bc
 524 02fb E9 00 00 00 	.4byte 0xe9
 525 02ff 02          	.byte 0x2
 526 0300 01          	.byte 0x1
 527 0301 0F          	.byte 0xf
 528 0302 02          	.byte 0x2
 529 0303 23          	.byte 0x23
 530 0304 00          	.uleb128 0x0
 531 0305 05          	.uleb128 0x5
 532 0306 43 31 52 58 	.asciz "C1RXR1"
 532      52 31 00 
 533 030d 02          	.byte 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 13


 534 030e BD 21       	.2byte 0x21bd
 535 0310 E9 00 00 00 	.4byte 0xe9
 536 0314 02          	.byte 0x2
 537 0315 01          	.byte 0x1
 538 0316 0E          	.byte 0xe
 539 0317 02          	.byte 0x2
 540 0318 23          	.byte 0x23
 541 0319 00          	.uleb128 0x0
 542 031a 05          	.uleb128 0x5
 543 031b 43 31 52 58 	.asciz "C1RXR2"
 543      52 32 00 
 544 0322 02          	.byte 0x2
 545 0323 BE 21       	.2byte 0x21be
 546 0325 E9 00 00 00 	.4byte 0xe9
 547 0329 02          	.byte 0x2
 548 032a 01          	.byte 0x1
 549 032b 0D          	.byte 0xd
 550 032c 02          	.byte 0x2
 551 032d 23          	.byte 0x23
 552 032e 00          	.uleb128 0x0
 553 032f 05          	.uleb128 0x5
 554 0330 43 31 52 58 	.asciz "C1RXR3"
 554      52 33 00 
 555 0337 02          	.byte 0x2
 556 0338 BF 21       	.2byte 0x21bf
 557 033a E9 00 00 00 	.4byte 0xe9
 558 033e 02          	.byte 0x2
 559 033f 01          	.byte 0x1
 560 0340 0C          	.byte 0xc
 561 0341 02          	.byte 0x2
 562 0342 23          	.byte 0x23
 563 0343 00          	.uleb128 0x0
 564 0344 05          	.uleb128 0x5
 565 0345 43 31 52 58 	.asciz "C1RXR4"
 565      52 34 00 
 566 034c 02          	.byte 0x2
 567 034d C0 21       	.2byte 0x21c0
 568 034f E9 00 00 00 	.4byte 0xe9
 569 0353 02          	.byte 0x2
 570 0354 01          	.byte 0x1
 571 0355 0B          	.byte 0xb
 572 0356 02          	.byte 0x2
 573 0357 23          	.byte 0x23
 574 0358 00          	.uleb128 0x0
 575 0359 05          	.uleb128 0x5
 576 035a 43 31 52 58 	.asciz "C1RXR5"
 576      52 35 00 
 577 0361 02          	.byte 0x2
 578 0362 C1 21       	.2byte 0x21c1
 579 0364 E9 00 00 00 	.4byte 0xe9
 580 0368 02          	.byte 0x2
 581 0369 01          	.byte 0x1
 582 036a 0A          	.byte 0xa
 583 036b 02          	.byte 0x2
 584 036c 23          	.byte 0x23
 585 036d 00          	.uleb128 0x0
 586 036e 05          	.uleb128 0x5
MPLAB XC16 ASSEMBLY Listing:   			page 14


 587 036f 43 31 52 58 	.asciz "C1RXR6"
 587      52 36 00 
 588 0376 02          	.byte 0x2
 589 0377 C2 21       	.2byte 0x21c2
 590 0379 E9 00 00 00 	.4byte 0xe9
 591 037d 02          	.byte 0x2
 592 037e 01          	.byte 0x1
 593 037f 09          	.byte 0x9
 594 0380 02          	.byte 0x2
 595 0381 23          	.byte 0x23
 596 0382 00          	.uleb128 0x0
 597 0383 05          	.uleb128 0x5
 598 0384 43 32 52 58 	.asciz "C2RXR0"
 598      52 30 00 
 599 038b 02          	.byte 0x2
 600 038c C4 21       	.2byte 0x21c4
 601 038e E9 00 00 00 	.4byte 0xe9
 602 0392 02          	.byte 0x2
 603 0393 01          	.byte 0x1
 604 0394 07          	.byte 0x7
 605 0395 02          	.byte 0x2
 606 0396 23          	.byte 0x23
 607 0397 00          	.uleb128 0x0
 608 0398 05          	.uleb128 0x5
 609 0399 43 32 52 58 	.asciz "C2RXR1"
 609      52 31 00 
 610 03a0 02          	.byte 0x2
 611 03a1 C5 21       	.2byte 0x21c5
 612 03a3 E9 00 00 00 	.4byte 0xe9
 613 03a7 02          	.byte 0x2
 614 03a8 01          	.byte 0x1
 615 03a9 06          	.byte 0x6
 616 03aa 02          	.byte 0x2
 617 03ab 23          	.byte 0x23
 618 03ac 00          	.uleb128 0x0
 619 03ad 05          	.uleb128 0x5
 620 03ae 43 32 52 58 	.asciz "C2RXR2"
 620      52 32 00 
 621 03b5 02          	.byte 0x2
 622 03b6 C6 21       	.2byte 0x21c6
 623 03b8 E9 00 00 00 	.4byte 0xe9
 624 03bc 02          	.byte 0x2
 625 03bd 01          	.byte 0x1
 626 03be 05          	.byte 0x5
 627 03bf 02          	.byte 0x2
 628 03c0 23          	.byte 0x23
 629 03c1 00          	.uleb128 0x0
 630 03c2 05          	.uleb128 0x5
 631 03c3 43 32 52 58 	.asciz "C2RXR3"
 631      52 33 00 
 632 03ca 02          	.byte 0x2
 633 03cb C7 21       	.2byte 0x21c7
 634 03cd E9 00 00 00 	.4byte 0xe9
 635 03d1 02          	.byte 0x2
 636 03d2 01          	.byte 0x1
 637 03d3 04          	.byte 0x4
 638 03d4 02          	.byte 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 15


 639 03d5 23          	.byte 0x23
 640 03d6 00          	.uleb128 0x0
 641 03d7 05          	.uleb128 0x5
 642 03d8 43 32 52 58 	.asciz "C2RXR4"
 642      52 34 00 
 643 03df 02          	.byte 0x2
 644 03e0 C8 21       	.2byte 0x21c8
 645 03e2 E9 00 00 00 	.4byte 0xe9
 646 03e6 02          	.byte 0x2
 647 03e7 01          	.byte 0x1
 648 03e8 03          	.byte 0x3
 649 03e9 02          	.byte 0x2
 650 03ea 23          	.byte 0x23
 651 03eb 00          	.uleb128 0x0
 652 03ec 05          	.uleb128 0x5
 653 03ed 43 32 52 58 	.asciz "C2RXR5"
 653      52 35 00 
 654 03f4 02          	.byte 0x2
 655 03f5 C9 21       	.2byte 0x21c9
 656 03f7 E9 00 00 00 	.4byte 0xe9
 657 03fb 02          	.byte 0x2
 658 03fc 01          	.byte 0x1
 659 03fd 02          	.byte 0x2
 660 03fe 02          	.byte 0x2
 661 03ff 23          	.byte 0x23
 662 0400 00          	.uleb128 0x0
 663 0401 05          	.uleb128 0x5
 664 0402 43 32 52 58 	.asciz "C2RXR6"
 664      52 36 00 
 665 0409 02          	.byte 0x2
 666 040a CA 21       	.2byte 0x21ca
 667 040c E9 00 00 00 	.4byte 0xe9
 668 0410 02          	.byte 0x2
 669 0411 01          	.byte 0x1
 670 0412 01          	.byte 0x1
 671 0413 02          	.byte 0x2
 672 0414 23          	.byte 0x23
 673 0415 00          	.uleb128 0x0
 674 0416 00          	.byte 0x0
 675 0417 06          	.uleb128 0x6
 676 0418 02          	.byte 0x2
 677 0419 02          	.byte 0x2
 678 041a B5 21       	.2byte 0x21b5
 679 041c 2B 04 00 00 	.4byte 0x42b
 680 0420 07          	.uleb128 0x7
 681 0421 B5 02 00 00 	.4byte 0x2b5
 682 0425 07          	.uleb128 0x7
 683 0426 E7 02 00 00 	.4byte 0x2e7
 684 042a 00          	.byte 0x0
 685 042b 08          	.uleb128 0x8
 686 042c 74 61 67 52 	.asciz "tagRPINR26BITS"
 686      50 49 4E 52 
 686      32 36 42 49 
 686      54 53 00 
 687 043b 02          	.byte 0x2
 688 043c 02          	.byte 0x2
 689 043d B4 21       	.2byte 0x21b4
MPLAB XC16 ASSEMBLY Listing:   			page 16


 690 043f 4C 04 00 00 	.4byte 0x44c
 691 0443 09          	.uleb128 0x9
 692 0444 17 04 00 00 	.4byte 0x417
 693 0448 02          	.byte 0x2
 694 0449 23          	.byte 0x23
 695 044a 00          	.uleb128 0x0
 696 044b 00          	.byte 0x0
 697 044c 0A          	.uleb128 0xa
 698 044d 52 50 49 4E 	.asciz "RPINR26BITS"
 698      52 32 36 42 
 698      49 54 53 00 
 699 0459 02          	.byte 0x2
 700 045a CD 21       	.2byte 0x21cd
 701 045c 2B 04 00 00 	.4byte 0x42b
 702 0460 08          	.uleb128 0x8
 703 0461 74 61 67 54 	.asciz "tagTRISBBITS"
 703      52 49 53 42 
 703      42 49 54 53 
 703      00 
 704 046e 02          	.byte 0x2
 705 046f 02          	.byte 0x2
 706 0470 39 3A       	.2byte 0x3a39
 707 0472 CD 05 00 00 	.4byte 0x5cd
 708 0476 05          	.uleb128 0x5
 709 0477 54 52 49 53 	.asciz "TRISB0"
 709      42 30 00 
 710 047e 02          	.byte 0x2
 711 047f 3A 3A       	.2byte 0x3a3a
 712 0481 E9 00 00 00 	.4byte 0xe9
 713 0485 02          	.byte 0x2
 714 0486 01          	.byte 0x1
 715 0487 0F          	.byte 0xf
 716 0488 02          	.byte 0x2
 717 0489 23          	.byte 0x23
 718 048a 00          	.uleb128 0x0
 719 048b 05          	.uleb128 0x5
 720 048c 54 52 49 53 	.asciz "TRISB1"
 720      42 31 00 
 721 0493 02          	.byte 0x2
 722 0494 3B 3A       	.2byte 0x3a3b
 723 0496 E9 00 00 00 	.4byte 0xe9
 724 049a 02          	.byte 0x2
 725 049b 01          	.byte 0x1
 726 049c 0E          	.byte 0xe
 727 049d 02          	.byte 0x2
 728 049e 23          	.byte 0x23
 729 049f 00          	.uleb128 0x0
 730 04a0 05          	.uleb128 0x5
 731 04a1 54 52 49 53 	.asciz "TRISB2"
 731      42 32 00 
 732 04a8 02          	.byte 0x2
 733 04a9 3C 3A       	.2byte 0x3a3c
 734 04ab E9 00 00 00 	.4byte 0xe9
 735 04af 02          	.byte 0x2
 736 04b0 01          	.byte 0x1
 737 04b1 0D          	.byte 0xd
 738 04b2 02          	.byte 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 17


 739 04b3 23          	.byte 0x23
 740 04b4 00          	.uleb128 0x0
 741 04b5 05          	.uleb128 0x5
 742 04b6 54 52 49 53 	.asciz "TRISB3"
 742      42 33 00 
 743 04bd 02          	.byte 0x2
 744 04be 3D 3A       	.2byte 0x3a3d
 745 04c0 E9 00 00 00 	.4byte 0xe9
 746 04c4 02          	.byte 0x2
 747 04c5 01          	.byte 0x1
 748 04c6 0C          	.byte 0xc
 749 04c7 02          	.byte 0x2
 750 04c8 23          	.byte 0x23
 751 04c9 00          	.uleb128 0x0
 752 04ca 05          	.uleb128 0x5
 753 04cb 54 52 49 53 	.asciz "TRISB4"
 753      42 34 00 
 754 04d2 02          	.byte 0x2
 755 04d3 3E 3A       	.2byte 0x3a3e
 756 04d5 E9 00 00 00 	.4byte 0xe9
 757 04d9 02          	.byte 0x2
 758 04da 01          	.byte 0x1
 759 04db 0B          	.byte 0xb
 760 04dc 02          	.byte 0x2
 761 04dd 23          	.byte 0x23
 762 04de 00          	.uleb128 0x0
 763 04df 05          	.uleb128 0x5
 764 04e0 54 52 49 53 	.asciz "TRISB5"
 764      42 35 00 
 765 04e7 02          	.byte 0x2
 766 04e8 3F 3A       	.2byte 0x3a3f
 767 04ea E9 00 00 00 	.4byte 0xe9
 768 04ee 02          	.byte 0x2
 769 04ef 01          	.byte 0x1
 770 04f0 0A          	.byte 0xa
 771 04f1 02          	.byte 0x2
 772 04f2 23          	.byte 0x23
 773 04f3 00          	.uleb128 0x0
 774 04f4 05          	.uleb128 0x5
 775 04f5 54 52 49 53 	.asciz "TRISB6"
 775      42 36 00 
 776 04fc 02          	.byte 0x2
 777 04fd 40 3A       	.2byte 0x3a40
 778 04ff E9 00 00 00 	.4byte 0xe9
 779 0503 02          	.byte 0x2
 780 0504 01          	.byte 0x1
 781 0505 09          	.byte 0x9
 782 0506 02          	.byte 0x2
 783 0507 23          	.byte 0x23
 784 0508 00          	.uleb128 0x0
 785 0509 05          	.uleb128 0x5
 786 050a 54 52 49 53 	.asciz "TRISB7"
 786      42 37 00 
 787 0511 02          	.byte 0x2
 788 0512 41 3A       	.2byte 0x3a41
 789 0514 E9 00 00 00 	.4byte 0xe9
 790 0518 02          	.byte 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 18


 791 0519 01          	.byte 0x1
 792 051a 08          	.byte 0x8
 793 051b 02          	.byte 0x2
 794 051c 23          	.byte 0x23
 795 051d 00          	.uleb128 0x0
 796 051e 05          	.uleb128 0x5
 797 051f 54 52 49 53 	.asciz "TRISB8"
 797      42 38 00 
 798 0526 02          	.byte 0x2
 799 0527 42 3A       	.2byte 0x3a42
 800 0529 E9 00 00 00 	.4byte 0xe9
 801 052d 02          	.byte 0x2
 802 052e 01          	.byte 0x1
 803 052f 07          	.byte 0x7
 804 0530 02          	.byte 0x2
 805 0531 23          	.byte 0x23
 806 0532 00          	.uleb128 0x0
 807 0533 05          	.uleb128 0x5
 808 0534 54 52 49 53 	.asciz "TRISB9"
 808      42 39 00 
 809 053b 02          	.byte 0x2
 810 053c 43 3A       	.2byte 0x3a43
 811 053e E9 00 00 00 	.4byte 0xe9
 812 0542 02          	.byte 0x2
 813 0543 01          	.byte 0x1
 814 0544 06          	.byte 0x6
 815 0545 02          	.byte 0x2
 816 0546 23          	.byte 0x23
 817 0547 00          	.uleb128 0x0
 818 0548 05          	.uleb128 0x5
 819 0549 54 52 49 53 	.asciz "TRISB10"
 819      42 31 30 00 
 820 0551 02          	.byte 0x2
 821 0552 44 3A       	.2byte 0x3a44
 822 0554 E9 00 00 00 	.4byte 0xe9
 823 0558 02          	.byte 0x2
 824 0559 01          	.byte 0x1
 825 055a 05          	.byte 0x5
 826 055b 02          	.byte 0x2
 827 055c 23          	.byte 0x23
 828 055d 00          	.uleb128 0x0
 829 055e 05          	.uleb128 0x5
 830 055f 54 52 49 53 	.asciz "TRISB11"
 830      42 31 31 00 
 831 0567 02          	.byte 0x2
 832 0568 45 3A       	.2byte 0x3a45
 833 056a E9 00 00 00 	.4byte 0xe9
 834 056e 02          	.byte 0x2
 835 056f 01          	.byte 0x1
 836 0570 04          	.byte 0x4
 837 0571 02          	.byte 0x2
 838 0572 23          	.byte 0x23
 839 0573 00          	.uleb128 0x0
 840 0574 05          	.uleb128 0x5
 841 0575 54 52 49 53 	.asciz "TRISB12"
 841      42 31 32 00 
 842 057d 02          	.byte 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 19


 843 057e 46 3A       	.2byte 0x3a46
 844 0580 E9 00 00 00 	.4byte 0xe9
 845 0584 02          	.byte 0x2
 846 0585 01          	.byte 0x1
 847 0586 03          	.byte 0x3
 848 0587 02          	.byte 0x2
 849 0588 23          	.byte 0x23
 850 0589 00          	.uleb128 0x0
 851 058a 05          	.uleb128 0x5
 852 058b 54 52 49 53 	.asciz "TRISB13"
 852      42 31 33 00 
 853 0593 02          	.byte 0x2
 854 0594 47 3A       	.2byte 0x3a47
 855 0596 E9 00 00 00 	.4byte 0xe9
 856 059a 02          	.byte 0x2
 857 059b 01          	.byte 0x1
 858 059c 02          	.byte 0x2
 859 059d 02          	.byte 0x2
 860 059e 23          	.byte 0x23
 861 059f 00          	.uleb128 0x0
 862 05a0 05          	.uleb128 0x5
 863 05a1 54 52 49 53 	.asciz "TRISB14"
 863      42 31 34 00 
 864 05a9 02          	.byte 0x2
 865 05aa 48 3A       	.2byte 0x3a48
 866 05ac E9 00 00 00 	.4byte 0xe9
 867 05b0 02          	.byte 0x2
 868 05b1 01          	.byte 0x1
 869 05b2 01          	.byte 0x1
 870 05b3 02          	.byte 0x2
 871 05b4 23          	.byte 0x23
 872 05b5 00          	.uleb128 0x0
 873 05b6 05          	.uleb128 0x5
 874 05b7 54 52 49 53 	.asciz "TRISB15"
 874      42 31 35 00 
 875 05bf 02          	.byte 0x2
 876 05c0 49 3A       	.2byte 0x3a49
 877 05c2 E9 00 00 00 	.4byte 0xe9
 878 05c6 02          	.byte 0x2
 879 05c7 01          	.byte 0x1
 880 05c8 10          	.byte 0x10
 881 05c9 02          	.byte 0x2
 882 05ca 23          	.byte 0x23
 883 05cb 00          	.uleb128 0x0
 884 05cc 00          	.byte 0x0
 885 05cd 0A          	.uleb128 0xa
 886 05ce 54 52 49 53 	.asciz "TRISBBITS"
 886      42 42 49 54 
 886      53 00 
 887 05d8 02          	.byte 0x2
 888 05d9 4A 3A       	.2byte 0x3a4a
 889 05db 60 04 00 00 	.4byte 0x460
 890 05df 08          	.uleb128 0x8
 891 05e0 74 61 67 41 	.asciz "tagANSELBBITS"
 891      4E 53 45 4C 
 891      42 42 49 54 
 891      53 00 
MPLAB XC16 ASSEMBLY Listing:   			page 20


 892 05ee 02          	.byte 0x2
 893 05ef 02          	.byte 0x2
 894 05f0 BD 3A       	.2byte 0x3abd
 895 05f2 3D 07 00 00 	.4byte 0x73d
 896 05f6 05          	.uleb128 0x5
 897 05f7 41 4E 53 42 	.asciz "ANSB0"
 897      30 00 
 898 05fd 02          	.byte 0x2
 899 05fe BE 3A       	.2byte 0x3abe
 900 0600 E9 00 00 00 	.4byte 0xe9
 901 0604 02          	.byte 0x2
 902 0605 01          	.byte 0x1
 903 0606 0F          	.byte 0xf
 904 0607 02          	.byte 0x2
 905 0608 23          	.byte 0x23
 906 0609 00          	.uleb128 0x0
 907 060a 05          	.uleb128 0x5
 908 060b 41 4E 53 42 	.asciz "ANSB1"
 908      31 00 
 909 0611 02          	.byte 0x2
 910 0612 BF 3A       	.2byte 0x3abf
 911 0614 E9 00 00 00 	.4byte 0xe9
 912 0618 02          	.byte 0x2
 913 0619 01          	.byte 0x1
 914 061a 0E          	.byte 0xe
 915 061b 02          	.byte 0x2
 916 061c 23          	.byte 0x23
 917 061d 00          	.uleb128 0x0
 918 061e 05          	.uleb128 0x5
 919 061f 41 4E 53 42 	.asciz "ANSB2"
 919      32 00 
 920 0625 02          	.byte 0x2
 921 0626 C0 3A       	.2byte 0x3ac0
 922 0628 E9 00 00 00 	.4byte 0xe9
 923 062c 02          	.byte 0x2
 924 062d 01          	.byte 0x1
 925 062e 0D          	.byte 0xd
 926 062f 02          	.byte 0x2
 927 0630 23          	.byte 0x23
 928 0631 00          	.uleb128 0x0
 929 0632 05          	.uleb128 0x5
 930 0633 41 4E 53 42 	.asciz "ANSB3"
 930      33 00 
 931 0639 02          	.byte 0x2
 932 063a C1 3A       	.2byte 0x3ac1
 933 063c E9 00 00 00 	.4byte 0xe9
 934 0640 02          	.byte 0x2
 935 0641 01          	.byte 0x1
 936 0642 0C          	.byte 0xc
 937 0643 02          	.byte 0x2
 938 0644 23          	.byte 0x23
 939 0645 00          	.uleb128 0x0
 940 0646 05          	.uleb128 0x5
 941 0647 41 4E 53 42 	.asciz "ANSB4"
 941      34 00 
 942 064d 02          	.byte 0x2
 943 064e C2 3A       	.2byte 0x3ac2
MPLAB XC16 ASSEMBLY Listing:   			page 21


 944 0650 E9 00 00 00 	.4byte 0xe9
 945 0654 02          	.byte 0x2
 946 0655 01          	.byte 0x1
 947 0656 0B          	.byte 0xb
 948 0657 02          	.byte 0x2
 949 0658 23          	.byte 0x23
 950 0659 00          	.uleb128 0x0
 951 065a 05          	.uleb128 0x5
 952 065b 41 4E 53 42 	.asciz "ANSB5"
 952      35 00 
 953 0661 02          	.byte 0x2
 954 0662 C3 3A       	.2byte 0x3ac3
 955 0664 E9 00 00 00 	.4byte 0xe9
 956 0668 02          	.byte 0x2
 957 0669 01          	.byte 0x1
 958 066a 0A          	.byte 0xa
 959 066b 02          	.byte 0x2
 960 066c 23          	.byte 0x23
 961 066d 00          	.uleb128 0x0
 962 066e 05          	.uleb128 0x5
 963 066f 41 4E 53 42 	.asciz "ANSB6"
 963      36 00 
 964 0675 02          	.byte 0x2
 965 0676 C4 3A       	.2byte 0x3ac4
 966 0678 E9 00 00 00 	.4byte 0xe9
 967 067c 02          	.byte 0x2
 968 067d 01          	.byte 0x1
 969 067e 09          	.byte 0x9
 970 067f 02          	.byte 0x2
 971 0680 23          	.byte 0x23
 972 0681 00          	.uleb128 0x0
 973 0682 05          	.uleb128 0x5
 974 0683 41 4E 53 42 	.asciz "ANSB7"
 974      37 00 
 975 0689 02          	.byte 0x2
 976 068a C5 3A       	.2byte 0x3ac5
 977 068c E9 00 00 00 	.4byte 0xe9
 978 0690 02          	.byte 0x2
 979 0691 01          	.byte 0x1
 980 0692 08          	.byte 0x8
 981 0693 02          	.byte 0x2
 982 0694 23          	.byte 0x23
 983 0695 00          	.uleb128 0x0
 984 0696 05          	.uleb128 0x5
 985 0697 41 4E 53 42 	.asciz "ANSB8"
 985      38 00 
 986 069d 02          	.byte 0x2
 987 069e C6 3A       	.2byte 0x3ac6
 988 06a0 E9 00 00 00 	.4byte 0xe9
 989 06a4 02          	.byte 0x2
 990 06a5 01          	.byte 0x1
 991 06a6 07          	.byte 0x7
 992 06a7 02          	.byte 0x2
 993 06a8 23          	.byte 0x23
 994 06a9 00          	.uleb128 0x0
 995 06aa 05          	.uleb128 0x5
 996 06ab 41 4E 53 42 	.asciz "ANSB9"
MPLAB XC16 ASSEMBLY Listing:   			page 22


 996      39 00 
 997 06b1 02          	.byte 0x2
 998 06b2 C7 3A       	.2byte 0x3ac7
 999 06b4 E9 00 00 00 	.4byte 0xe9
 1000 06b8 02          	.byte 0x2
 1001 06b9 01          	.byte 0x1
 1002 06ba 06          	.byte 0x6
 1003 06bb 02          	.byte 0x2
 1004 06bc 23          	.byte 0x23
 1005 06bd 00          	.uleb128 0x0
 1006 06be 05          	.uleb128 0x5
 1007 06bf 41 4E 53 42 	.asciz "ANSB10"
 1007      31 30 00 
 1008 06c6 02          	.byte 0x2
 1009 06c7 C8 3A       	.2byte 0x3ac8
 1010 06c9 E9 00 00 00 	.4byte 0xe9
 1011 06cd 02          	.byte 0x2
 1012 06ce 01          	.byte 0x1
 1013 06cf 05          	.byte 0x5
 1014 06d0 02          	.byte 0x2
 1015 06d1 23          	.byte 0x23
 1016 06d2 00          	.uleb128 0x0
 1017 06d3 05          	.uleb128 0x5
 1018 06d4 41 4E 53 42 	.asciz "ANSB11"
 1018      31 31 00 
 1019 06db 02          	.byte 0x2
 1020 06dc C9 3A       	.2byte 0x3ac9
 1021 06de E9 00 00 00 	.4byte 0xe9
 1022 06e2 02          	.byte 0x2
 1023 06e3 01          	.byte 0x1
 1024 06e4 04          	.byte 0x4
 1025 06e5 02          	.byte 0x2
 1026 06e6 23          	.byte 0x23
 1027 06e7 00          	.uleb128 0x0
 1028 06e8 05          	.uleb128 0x5
 1029 06e9 41 4E 53 42 	.asciz "ANSB12"
 1029      31 32 00 
 1030 06f0 02          	.byte 0x2
 1031 06f1 CA 3A       	.2byte 0x3aca
 1032 06f3 E9 00 00 00 	.4byte 0xe9
 1033 06f7 02          	.byte 0x2
 1034 06f8 01          	.byte 0x1
 1035 06f9 03          	.byte 0x3
 1036 06fa 02          	.byte 0x2
 1037 06fb 23          	.byte 0x23
 1038 06fc 00          	.uleb128 0x0
 1039 06fd 05          	.uleb128 0x5
 1040 06fe 41 4E 53 42 	.asciz "ANSB13"
 1040      31 33 00 
 1041 0705 02          	.byte 0x2
 1042 0706 CB 3A       	.2byte 0x3acb
 1043 0708 E9 00 00 00 	.4byte 0xe9
 1044 070c 02          	.byte 0x2
 1045 070d 01          	.byte 0x1
 1046 070e 02          	.byte 0x2
 1047 070f 02          	.byte 0x2
 1048 0710 23          	.byte 0x23
MPLAB XC16 ASSEMBLY Listing:   			page 23


 1049 0711 00          	.uleb128 0x0
 1050 0712 05          	.uleb128 0x5
 1051 0713 41 4E 53 42 	.asciz "ANSB14"
 1051      31 34 00 
 1052 071a 02          	.byte 0x2
 1053 071b CC 3A       	.2byte 0x3acc
 1054 071d E9 00 00 00 	.4byte 0xe9
 1055 0721 02          	.byte 0x2
 1056 0722 01          	.byte 0x1
 1057 0723 01          	.byte 0x1
 1058 0724 02          	.byte 0x2
 1059 0725 23          	.byte 0x23
 1060 0726 00          	.uleb128 0x0
 1061 0727 05          	.uleb128 0x5
 1062 0728 41 4E 53 42 	.asciz "ANSB15"
 1062      31 35 00 
 1063 072f 02          	.byte 0x2
 1064 0730 CD 3A       	.2byte 0x3acd
 1065 0732 E9 00 00 00 	.4byte 0xe9
 1066 0736 02          	.byte 0x2
 1067 0737 01          	.byte 0x1
 1068 0738 10          	.byte 0x10
 1069 0739 02          	.byte 0x2
 1070 073a 23          	.byte 0x23
 1071 073b 00          	.uleb128 0x0
 1072 073c 00          	.byte 0x0
 1073 073d 0A          	.uleb128 0xa
 1074 073e 41 4E 53 45 	.asciz "ANSELBBITS"
 1074      4C 42 42 49 
 1074      54 53 00 
 1075 0749 02          	.byte 0x2
 1076 074a CE 3A       	.2byte 0x3ace
 1077 074c DF 05 00 00 	.4byte 0x5df
 1078 0750 08          	.uleb128 0x8
 1079 0751 74 61 67 4C 	.asciz "tagLATEBITS"
 1079      41 54 45 42 
 1079      49 54 53 00 
 1080 075d 02          	.byte 0x2
 1081 075e 02          	.byte 0x2
 1082 075f C0 3B       	.2byte 0x3bc0
 1083 0761 06 08 00 00 	.4byte 0x806
 1084 0765 05          	.uleb128 0x5
 1085 0766 4C 41 54 45 	.asciz "LATE0"
 1085      30 00 
 1086 076c 02          	.byte 0x2
 1087 076d C1 3B       	.2byte 0x3bc1
 1088 076f E9 00 00 00 	.4byte 0xe9
 1089 0773 02          	.byte 0x2
 1090 0774 01          	.byte 0x1
 1091 0775 0F          	.byte 0xf
 1092 0776 02          	.byte 0x2
 1093 0777 23          	.byte 0x23
 1094 0778 00          	.uleb128 0x0
 1095 0779 05          	.uleb128 0x5
 1096 077a 4C 41 54 45 	.asciz "LATE1"
 1096      31 00 
 1097 0780 02          	.byte 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 24


 1098 0781 C2 3B       	.2byte 0x3bc2
 1099 0783 E9 00 00 00 	.4byte 0xe9
 1100 0787 02          	.byte 0x2
 1101 0788 01          	.byte 0x1
 1102 0789 0E          	.byte 0xe
 1103 078a 02          	.byte 0x2
 1104 078b 23          	.byte 0x23
 1105 078c 00          	.uleb128 0x0
 1106 078d 05          	.uleb128 0x5
 1107 078e 4C 41 54 45 	.asciz "LATE2"
 1107      32 00 
 1108 0794 02          	.byte 0x2
 1109 0795 C3 3B       	.2byte 0x3bc3
 1110 0797 E9 00 00 00 	.4byte 0xe9
 1111 079b 02          	.byte 0x2
 1112 079c 01          	.byte 0x1
 1113 079d 0D          	.byte 0xd
 1114 079e 02          	.byte 0x2
 1115 079f 23          	.byte 0x23
 1116 07a0 00          	.uleb128 0x0
 1117 07a1 05          	.uleb128 0x5
 1118 07a2 4C 41 54 45 	.asciz "LATE3"
 1118      33 00 
 1119 07a8 02          	.byte 0x2
 1120 07a9 C4 3B       	.2byte 0x3bc4
 1121 07ab E9 00 00 00 	.4byte 0xe9
 1122 07af 02          	.byte 0x2
 1123 07b0 01          	.byte 0x1
 1124 07b1 0C          	.byte 0xc
 1125 07b2 02          	.byte 0x2
 1126 07b3 23          	.byte 0x23
 1127 07b4 00          	.uleb128 0x0
 1128 07b5 05          	.uleb128 0x5
 1129 07b6 4C 41 54 45 	.asciz "LATE4"
 1129      34 00 
 1130 07bc 02          	.byte 0x2
 1131 07bd C5 3B       	.2byte 0x3bc5
 1132 07bf E9 00 00 00 	.4byte 0xe9
 1133 07c3 02          	.byte 0x2
 1134 07c4 01          	.byte 0x1
 1135 07c5 0B          	.byte 0xb
 1136 07c6 02          	.byte 0x2
 1137 07c7 23          	.byte 0x23
 1138 07c8 00          	.uleb128 0x0
 1139 07c9 05          	.uleb128 0x5
 1140 07ca 4C 41 54 45 	.asciz "LATE5"
 1140      35 00 
 1141 07d0 02          	.byte 0x2
 1142 07d1 C6 3B       	.2byte 0x3bc6
 1143 07d3 E9 00 00 00 	.4byte 0xe9
 1144 07d7 02          	.byte 0x2
 1145 07d8 01          	.byte 0x1
 1146 07d9 0A          	.byte 0xa
 1147 07da 02          	.byte 0x2
 1148 07db 23          	.byte 0x23
 1149 07dc 00          	.uleb128 0x0
 1150 07dd 05          	.uleb128 0x5
MPLAB XC16 ASSEMBLY Listing:   			page 25


 1151 07de 4C 41 54 45 	.asciz "LATE6"
 1151      36 00 
 1152 07e4 02          	.byte 0x2
 1153 07e5 C7 3B       	.2byte 0x3bc7
 1154 07e7 E9 00 00 00 	.4byte 0xe9
 1155 07eb 02          	.byte 0x2
 1156 07ec 01          	.byte 0x1
 1157 07ed 09          	.byte 0x9
 1158 07ee 02          	.byte 0x2
 1159 07ef 23          	.byte 0x23
 1160 07f0 00          	.uleb128 0x0
 1161 07f1 05          	.uleb128 0x5
 1162 07f2 4C 41 54 45 	.asciz "LATE7"
 1162      37 00 
 1163 07f8 02          	.byte 0x2
 1164 07f9 C8 3B       	.2byte 0x3bc8
 1165 07fb E9 00 00 00 	.4byte 0xe9
 1166 07ff 02          	.byte 0x2
 1167 0800 01          	.byte 0x1
 1168 0801 08          	.byte 0x8
 1169 0802 02          	.byte 0x2
 1170 0803 23          	.byte 0x23
 1171 0804 00          	.uleb128 0x0
 1172 0805 00          	.byte 0x0
 1173 0806 0A          	.uleb128 0xa
 1174 0807 4C 41 54 45 	.asciz "LATEBITS"
 1174      42 49 54 53 
 1174      00 
 1175 0810 02          	.byte 0x2
 1176 0811 C9 3B       	.2byte 0x3bc9
 1177 0813 50 07 00 00 	.4byte 0x750
 1178 0817 0B          	.uleb128 0xb
 1179 0818 02          	.byte 0x2
 1180 0819 04          	.byte 0x4
 1181 081a 1F          	.byte 0x1f
 1182 081b 30 08 00 00 	.4byte 0x830
 1183 081f 0C          	.uleb128 0xc
 1184 0820 4D 41 4E 55 	.asciz "MANUAL"
 1184      41 4C 00 
 1185 0827 00          	.sleb128 0
 1186 0828 0C          	.uleb128 0xc
 1187 0829 41 55 54 4F 	.asciz "AUTO"
 1187      00 
 1188 082e 01          	.sleb128 1
 1189 082f 00          	.byte 0x0
 1190 0830 03          	.uleb128 0x3
 1191 0831 53 61 6D 70 	.asciz "Sampling"
 1191      6C 69 6E 67 
 1191      00 
 1192 083a 04          	.byte 0x4
 1193 083b 1F          	.byte 0x1f
 1194 083c 17 08 00 00 	.4byte 0x817
 1195 0840 0B          	.uleb128 0xb
 1196 0841 02          	.byte 0x2
 1197 0842 04          	.byte 0x4
 1198 0843 24          	.byte 0x24
 1199 0844 85 08 00 00 	.4byte 0x885
MPLAB XC16 ASSEMBLY Listing:   			page 26


 1200 0848 0C          	.uleb128 0xc
 1201 0849 53 41 4D 50 	.asciz "SAMP"
 1201      00 
 1202 084e 00          	.sleb128 0
 1203 084f 0C          	.uleb128 0xc
 1204 0850 49 4E 54 30 	.asciz "INT0"
 1204      00 
 1205 0855 01          	.sleb128 1
 1206 0856 0C          	.uleb128 0xc
 1207 0857 54 49 4D 45 	.asciz "TIMER3"
 1207      52 33 00 
 1208 085e 02          	.sleb128 2
 1209 085f 0C          	.uleb128 0xc
 1210 0860 50 57 4D 5F 	.asciz "PWM_P"
 1210      50 00 
 1211 0866 03          	.sleb128 3
 1212 0867 0C          	.uleb128 0xc
 1213 0868 54 49 4D 45 	.asciz "TIMER5"
 1213      52 35 00 
 1214 086f 04          	.sleb128 4
 1215 0870 0C          	.uleb128 0xc
 1216 0871 50 57 4D 5F 	.asciz "PWM_S"
 1216      53 00 
 1217 0877 05          	.sleb128 5
 1218 0878 0C          	.uleb128 0xc
 1219 0879 41 55 54 4F 	.asciz "AUTO_CONV"
 1219      5F 43 4F 4E 
 1219      56 00 
 1220 0883 07          	.sleb128 7
 1221 0884 00          	.byte 0x0
 1222 0885 03          	.uleb128 0x3
 1223 0886 53 53 52 43 	.asciz "SSRC"
 1223      00 
 1224 088b 04          	.byte 0x4
 1225 088c 24          	.byte 0x24
 1226 088d 40 08 00 00 	.4byte 0x840
 1227 0891 0B          	.uleb128 0xb
 1228 0892 02          	.byte 0x2
 1229 0893 04          	.byte 0x4
 1230 0894 2A          	.byte 0x2a
 1231 0895 D8 08 00 00 	.4byte 0x8d8
 1232 0899 0C          	.uleb128 0xc
 1233 089a 49 4E 54 45 	.asciz "INTERNAL"
 1233      52 4E 41 4C 
 1233      00 
 1234 08a3 00          	.sleb128 0
 1235 08a4 0C          	.uleb128 0xc
 1236 08a5 50 4F 53 49 	.asciz "POSITIVE_EXTERNAL"
 1236      54 49 56 45 
 1236      5F 45 58 54 
 1236      45 52 4E 41 
 1236      4C 00 
 1237 08b7 01          	.sleb128 1
 1238 08b8 0C          	.uleb128 0xc
 1239 08b9 4E 45 47 41 	.asciz "NEGATIVE_EXTERNAL"
 1239      54 49 56 45 
 1239      5F 45 58 54 
MPLAB XC16 ASSEMBLY Listing:   			page 27


 1239      45 52 4E 41 
 1239      4C 00 
 1240 08cb 02          	.sleb128 2
 1241 08cc 0C          	.uleb128 0xc
 1242 08cd 45 58 54 45 	.asciz "EXTERNAL"
 1242      52 4E 41 4C 
 1242      00 
 1243 08d6 03          	.sleb128 3
 1244 08d7 00          	.byte 0x0
 1245 08d8 03          	.uleb128 0x3
 1246 08d9 56 6F 6C 74 	.asciz "Voltage"
 1246      61 67 65 00 
 1247 08e1 04          	.byte 0x4
 1248 08e2 2F          	.byte 0x2f
 1249 08e3 91 08 00 00 	.4byte 0x891
 1250 08e7 0B          	.uleb128 0xb
 1251 08e8 02          	.byte 0x2
 1252 08e9 04          	.byte 0x4
 1253 08ea 36          	.byte 0x36
 1254 08eb 6B 09 00 00 	.4byte 0x96b
 1255 08ef 0C          	.uleb128 0xc
 1256 08f0 41 4E 30 00 	.asciz "AN0"
 1257 08f4 01          	.sleb128 1
 1258 08f5 0C          	.uleb128 0xc
 1259 08f6 41 4E 31 00 	.asciz "AN1"
 1260 08fa 02          	.sleb128 2
 1261 08fb 0C          	.uleb128 0xc
 1262 08fc 41 4E 32 00 	.asciz "AN2"
 1263 0900 04          	.sleb128 4
 1264 0901 0C          	.uleb128 0xc
 1265 0902 41 4E 33 00 	.asciz "AN3"
 1266 0906 08          	.sleb128 8
 1267 0907 0C          	.uleb128 0xc
 1268 0908 41 4E 34 00 	.asciz "AN4"
 1269 090c 10          	.sleb128 16
 1270 090d 0C          	.uleb128 0xc
 1271 090e 41 4E 35 00 	.asciz "AN5"
 1272 0912 20          	.sleb128 32
 1273 0913 0C          	.uleb128 0xc
 1274 0914 41 4E 36 00 	.asciz "AN6"
 1275 0918 C0 00       	.sleb128 64
 1276 091a 0C          	.uleb128 0xc
 1277 091b 41 4E 37 00 	.asciz "AN7"
 1278 091f 80 01       	.sleb128 128
 1279 0921 0C          	.uleb128 0xc
 1280 0922 41 4E 38 00 	.asciz "AN8"
 1281 0926 80 02       	.sleb128 256
 1282 0928 0C          	.uleb128 0xc
 1283 0929 41 4E 39 00 	.asciz "AN9"
 1284 092d 80 04       	.sleb128 512
 1285 092f 0C          	.uleb128 0xc
 1286 0930 41 4E 31 30 	.asciz "AN10"
 1286      00 
 1287 0935 80 08       	.sleb128 1024
 1288 0937 0C          	.uleb128 0xc
 1289 0938 41 4E 31 31 	.asciz "AN11"
 1289      00 
MPLAB XC16 ASSEMBLY Listing:   			page 28


 1290 093d 80 10       	.sleb128 2048
 1291 093f 0C          	.uleb128 0xc
 1292 0940 41 4E 31 32 	.asciz "AN12"
 1292      00 
 1293 0945 80 20       	.sleb128 4096
 1294 0947 0C          	.uleb128 0xc
 1295 0948 41 4E 31 33 	.asciz "AN13"
 1295      00 
 1296 094d 80 C0 00    	.sleb128 8192
 1297 0950 0C          	.uleb128 0xc
 1298 0951 41 4E 31 34 	.asciz "AN14"
 1298      00 
 1299 0956 80 80 01    	.sleb128 16384
 1300 0959 0C          	.uleb128 0xc
 1301 095a 41 4E 31 35 	.asciz "AN15"
 1301      00 
 1302 095f 80 80 02    	.sleb128 32768
 1303 0962 0C          	.uleb128 0xc
 1304 0963 41 4C 4C 00 	.asciz "ALL"
 1305 0967 FF FF 03    	.sleb128 65535
 1306 096a 00          	.byte 0x0
 1307 096b 0D          	.uleb128 0xd
 1308 096c 0A          	.byte 0xa
 1309 096d 04          	.byte 0x4
 1310 096e 58          	.byte 0x58
 1311 096f A4 0A 00 00 	.4byte 0xaa4
 1312 0973 0E          	.uleb128 0xe
 1313 0974 69 64 6C 65 	.asciz "idle"
 1313      00 
 1314 0979 04          	.byte 0x4
 1315 097a 5A          	.byte 0x5a
 1316 097b F9 00 00 00 	.4byte 0xf9
 1317 097f 02          	.byte 0x2
 1318 0980 01          	.byte 0x1
 1319 0981 0F          	.byte 0xf
 1320 0982 02          	.byte 0x2
 1321 0983 23          	.byte 0x23
 1322 0984 00          	.uleb128 0x0
 1323 0985 0E          	.uleb128 0xe
 1324 0986 66 6F 72 6D 	.asciz "form"
 1324      00 
 1325 098b 04          	.byte 0x4
 1326 098c 5B          	.byte 0x5b
 1327 098d F9 00 00 00 	.4byte 0xf9
 1328 0991 02          	.byte 0x2
 1329 0992 02          	.byte 0x2
 1330 0993 0D          	.byte 0xd
 1331 0994 02          	.byte 0x2
 1332 0995 23          	.byte 0x23
 1333 0996 00          	.uleb128 0x0
 1334 0997 0E          	.uleb128 0xe
 1335 0998 63 6F 6E 76 	.asciz "conversion_trigger"
 1335      65 72 73 69 
 1335      6F 6E 5F 74 
 1335      72 69 67 67 
 1335      65 72 00 
 1336 09ab 04          	.byte 0x4
MPLAB XC16 ASSEMBLY Listing:   			page 29


 1337 09ac 5E          	.byte 0x5e
 1338 09ad 85 08 00 00 	.4byte 0x885
 1339 09b1 02          	.byte 0x2
 1340 09b2 03          	.byte 0x3
 1341 09b3 0A          	.byte 0xa
 1342 09b4 02          	.byte 0x2
 1343 09b5 23          	.byte 0x23
 1344 09b6 00          	.uleb128 0x0
 1345 09b7 0E          	.uleb128 0xe
 1346 09b8 73 61 6D 70 	.asciz "sampling_type"
 1346      6C 69 6E 67 
 1346      5F 74 79 70 
 1346      65 00 
 1347 09c6 04          	.byte 0x4
 1348 09c7 62          	.byte 0x62
 1349 09c8 30 08 00 00 	.4byte 0x830
 1350 09cc 02          	.byte 0x2
 1351 09cd 01          	.byte 0x1
 1352 09ce 09          	.byte 0x9
 1353 09cf 02          	.byte 0x2
 1354 09d0 23          	.byte 0x23
 1355 09d1 00          	.uleb128 0x0
 1356 09d2 0E          	.uleb128 0xe
 1357 09d3 76 6F 6C 74 	.asciz "voltage_ref"
 1357      61 67 65 5F 
 1357      72 65 66 00 
 1358 09df 04          	.byte 0x4
 1359 09e0 63          	.byte 0x63
 1360 09e1 D8 08 00 00 	.4byte 0x8d8
 1361 09e5 02          	.byte 0x2
 1362 09e6 02          	.byte 0x2
 1363 09e7 07          	.byte 0x7
 1364 09e8 02          	.byte 0x2
 1365 09e9 23          	.byte 0x23
 1366 09ea 00          	.uleb128 0x0
 1367 09eb 0E          	.uleb128 0xe
 1368 09ec 73 61 6D 70 	.asciz "sample_pin_select_type"
 1368      6C 65 5F 70 
 1368      69 6E 5F 73 
 1368      65 6C 65 63 
 1368      74 5F 74 79 
 1368      70 65 00 
 1369 0a03 04          	.byte 0x4
 1370 0a04 65          	.byte 0x65
 1371 0a05 30 08 00 00 	.4byte 0x830
 1372 0a09 02          	.byte 0x2
 1373 0a0a 01          	.byte 0x1
 1374 0a0b 06          	.byte 0x6
 1375 0a0c 02          	.byte 0x2
 1376 0a0d 23          	.byte 0x23
 1377 0a0e 00          	.uleb128 0x0
 1378 0a0f 0E          	.uleb128 0xe
 1379 0a10 73 6D 70 69 	.asciz "smpi"
 1379      00 
 1380 0a15 04          	.byte 0x4
 1381 0a16 67          	.byte 0x67
 1382 0a17 F9 00 00 00 	.4byte 0xf9
MPLAB XC16 ASSEMBLY Listing:   			page 30


 1383 0a1b 02          	.byte 0x2
 1384 0a1c 04          	.byte 0x4
 1385 0a1d 02          	.byte 0x2
 1386 0a1e 02          	.byte 0x2
 1387 0a1f 23          	.byte 0x23
 1388 0a20 00          	.uleb128 0x0
 1389 0a21 0E          	.uleb128 0xe
 1390 0a22 70 69 6E 5F 	.asciz "pin_select"
 1390      73 65 6C 65 
 1390      63 74 00 
 1391 0a2d 04          	.byte 0x4
 1392 0a2e 68          	.byte 0x68
 1393 0a2f F9 00 00 00 	.4byte 0xf9
 1394 0a33 02          	.byte 0x2
 1395 0a34 10          	.byte 0x10
 1396 0a35 10          	.byte 0x10
 1397 0a36 02          	.byte 0x2
 1398 0a37 23          	.byte 0x23
 1399 0a38 02          	.uleb128 0x2
 1400 0a39 0F          	.uleb128 0xf
 1401 0a3a 61 75 74 6F 	.asciz "auto_sample_time"
 1401      5F 73 61 6D 
 1401      70 6C 65 5F 
 1401      74 69 6D 65 
 1401      00 
 1402 0a4b 04          	.byte 0x4
 1403 0a4c 6A          	.byte 0x6a
 1404 0a4d F9 00 00 00 	.4byte 0xf9
 1405 0a51 02          	.byte 0x2
 1406 0a52 23          	.byte 0x23
 1407 0a53 04          	.uleb128 0x4
 1408 0a54 0F          	.uleb128 0xf
 1409 0a55 63 6F 6E 76 	.asciz "conversion_time"
 1409      65 72 73 69 
 1409      6F 6E 5F 74 
 1409      69 6D 65 00 
 1410 0a65 04          	.byte 0x4
 1411 0a66 6B          	.byte 0x6b
 1412 0a67 F9 00 00 00 	.4byte 0xf9
 1413 0a6b 02          	.byte 0x2
 1414 0a6c 23          	.byte 0x23
 1415 0a6d 06          	.uleb128 0x6
 1416 0a6e 0E          	.uleb128 0xe
 1417 0a6f 69 6E 74 65 	.asciz "interrupt_priority"
 1417      72 72 75 70 
 1417      74 5F 70 72 
 1417      69 6F 72 69 
 1417      74 79 00 
 1418 0a82 04          	.byte 0x4
 1419 0a83 6C          	.byte 0x6c
 1420 0a84 F9 00 00 00 	.4byte 0xf9
 1421 0a88 02          	.byte 0x2
 1422 0a89 03          	.byte 0x3
 1423 0a8a 0D          	.byte 0xd
 1424 0a8b 02          	.byte 0x2
 1425 0a8c 23          	.byte 0x23
 1426 0a8d 08          	.uleb128 0x8
MPLAB XC16 ASSEMBLY Listing:   			page 31


 1427 0a8e 0E          	.uleb128 0xe
 1428 0a8f 74 75 72 6E 	.asciz "turn_on"
 1428      5F 6F 6E 00 
 1429 0a97 04          	.byte 0x4
 1430 0a98 6D          	.byte 0x6d
 1431 0a99 F9 00 00 00 	.4byte 0xf9
 1432 0a9d 02          	.byte 0x2
 1433 0a9e 01          	.byte 0x1
 1434 0a9f 0C          	.byte 0xc
 1435 0aa0 02          	.byte 0x2
 1436 0aa1 23          	.byte 0x23
 1437 0aa2 08          	.uleb128 0x8
 1438 0aa3 00          	.byte 0x0
 1439 0aa4 03          	.uleb128 0x3
 1440 0aa5 41 44 43 5F 	.asciz "ADC_parameters"
 1440      70 61 72 61 
 1440      6D 65 74 65 
 1440      72 73 00 
 1441 0ab4 04          	.byte 0x4
 1442 0ab5 70          	.byte 0x70
 1443 0ab6 6B 09 00 00 	.4byte 0x96b
 1444 0aba 10          	.uleb128 0x10
 1445 0abb 01          	.byte 0x1
 1446 0abc 63 6F 6E 66 	.asciz "config_IO"
 1446      69 67 5F 49 
 1446      4F 00 
 1447 0ac6 01          	.byte 0x1
 1448 0ac7 07          	.byte 0x7
 1449 0ac8 00 00 00 00 	.4byte .LFB0
 1450 0acc 00 00 00 00 	.4byte .LFE0
 1451 0ad0 01          	.byte 0x1
 1452 0ad1 5E          	.byte 0x5e
 1453 0ad2 11          	.uleb128 0x11
 1454 0ad3 01          	.byte 0x1
 1455 0ad4 63 6F 6E 66 	.asciz "config_adc"
 1455      69 67 5F 61 
 1455      64 63 00 
 1456 0adf 01          	.byte 0x1
 1457 0ae0 38          	.byte 0x38
 1458 0ae1 00 00 00 00 	.4byte .LFB1
 1459 0ae5 00 00 00 00 	.4byte .LFE1
 1460 0ae9 01          	.byte 0x1
 1461 0aea 5E          	.byte 0x5e
 1462 0aeb 05 0B 00 00 	.4byte 0xb05
 1463 0aef 12          	.uleb128 0x12
 1464 0af0 61 64 63 5F 	.asciz "adc_config"
 1464      63 6F 6E 66 
 1464      69 67 00 
 1465 0afb 01          	.byte 0x1
 1466 0afc 3F          	.byte 0x3f
 1467 0afd A4 0A 00 00 	.4byte 0xaa4
 1468 0b01 02          	.byte 0x2
 1469 0b02 7E          	.byte 0x7e
 1470 0b03 00          	.sleb128 0
 1471 0b04 00          	.byte 0x0
 1472 0b05 13          	.uleb128 0x13
 1473 0b06 01          	.byte 0x1
MPLAB XC16 ASSEMBLY Listing:   			page 32


 1474 0b07 65 78 74 65 	.asciz "external1_callback"
 1474      72 6E 61 6C 
 1474      31 5F 63 61 
 1474      6C 6C 62 61 
 1474      63 6B 00 
 1475 0b1a 01          	.byte 0x1
 1476 0b1b 45          	.byte 0x45
 1477 0b1c 01          	.byte 0x1
 1478 0b1d 00 00 00 00 	.4byte .LFB2
 1479 0b21 00 00 00 00 	.4byte .LFE2
 1480 0b25 01          	.byte 0x1
 1481 0b26 5E          	.byte 0x5e
 1482 0b27 11          	.uleb128 0x11
 1483 0b28 01          	.byte 0x1
 1484 0b29 74 6D 72 32 	.asciz "tmr2_int"
 1484      5F 69 6E 74 
 1484      00 
 1485 0b32 01          	.byte 0x1
 1486 0b33 49          	.byte 0x49
 1487 0b34 00 00 00 00 	.4byte .LFB3
 1488 0b38 00 00 00 00 	.4byte .LFE3
 1489 0b3c 01          	.byte 0x1
 1490 0b3d 5E          	.byte 0x5e
 1491 0b3e 5D 0B 00 00 	.4byte 0xb5d
 1492 0b42 14          	.uleb128 0x14
 1493 0b43 01          	.byte 0x1
 1494 0b44 73 65 6E 64 	.asciz "send_adc_value"
 1494      5F 61 64 63 
 1494      5F 76 61 6C 
 1494      75 65 00 
 1495 0b53 01          	.byte 0x1
 1496 0b54 4A          	.byte 0x4a
 1497 0b55 B4 00 00 00 	.4byte 0xb4
 1498 0b59 01          	.byte 0x1
 1499 0b5a 15          	.uleb128 0x15
 1500 0b5b 00          	.byte 0x0
 1501 0b5c 00          	.byte 0x0
 1502 0b5d 16          	.uleb128 0x16
 1503 0b5e 00 00 00 00 	.4byte .LASF0
 1504 0b62 02          	.byte 0x2
 1505 0b63 8F 1E       	.2byte 0x1e8f
 1506 0b65 6B 0B 00 00 	.4byte 0xb6b
 1507 0b69 01          	.byte 0x1
 1508 0b6a 01          	.byte 0x1
 1509 0b6b 17          	.uleb128 0x17
 1510 0b6c A3 02 00 00 	.4byte 0x2a3
 1511 0b70 16          	.uleb128 0x16
 1512 0b71 00 00 00 00 	.4byte .LASF1
 1513 0b75 02          	.byte 0x2
 1514 0b76 CE 21       	.2byte 0x21ce
 1515 0b78 7E 0B 00 00 	.4byte 0xb7e
 1516 0b7c 01          	.byte 0x1
 1517 0b7d 01          	.byte 0x1
 1518 0b7e 17          	.uleb128 0x17
 1519 0b7f 4C 04 00 00 	.4byte 0x44c
 1520 0b83 18          	.uleb128 0x18
 1521 0b84 4F 53 43 43 	.asciz "OSCCON"
MPLAB XC16 ASSEMBLY Listing:   			page 33


 1521      4F 4E 00 
 1522 0b8b 02          	.byte 0x2
 1523 0b8c BF 23       	.2byte 0x23bf
 1524 0b8e 94 0B 00 00 	.4byte 0xb94
 1525 0b92 01          	.byte 0x1
 1526 0b93 01          	.byte 0x1
 1527 0b94 17          	.uleb128 0x17
 1528 0b95 E9 00 00 00 	.4byte 0xe9
 1529 0b99 16          	.uleb128 0x16
 1530 0b9a 00 00 00 00 	.4byte .LASF2
 1531 0b9e 02          	.byte 0x2
 1532 0b9f 4B 3A       	.2byte 0x3a4b
 1533 0ba1 A7 0B 00 00 	.4byte 0xba7
 1534 0ba5 01          	.byte 0x1
 1535 0ba6 01          	.byte 0x1
 1536 0ba7 17          	.uleb128 0x17
 1537 0ba8 CD 05 00 00 	.4byte 0x5cd
 1538 0bac 16          	.uleb128 0x16
 1539 0bad 00 00 00 00 	.4byte .LASF3
 1540 0bb1 02          	.byte 0x2
 1541 0bb2 CF 3A       	.2byte 0x3acf
 1542 0bb4 BA 0B 00 00 	.4byte 0xbba
 1543 0bb8 01          	.byte 0x1
 1544 0bb9 01          	.byte 0x1
 1545 0bba 17          	.uleb128 0x17
 1546 0bbb 3D 07 00 00 	.4byte 0x73d
 1547 0bbf 16          	.uleb128 0x16
 1548 0bc0 00 00 00 00 	.4byte .LASF4
 1549 0bc4 02          	.byte 0x2
 1550 0bc5 CA 3B       	.2byte 0x3bca
 1551 0bc7 CD 0B 00 00 	.4byte 0xbcd
 1552 0bcb 01          	.byte 0x1
 1553 0bcc 01          	.byte 0x1
 1554 0bcd 17          	.uleb128 0x17
 1555 0bce 06 08 00 00 	.4byte 0x806
 1556 0bd2 16          	.uleb128 0x16
 1557 0bd3 00 00 00 00 	.4byte .LASF0
 1558 0bd7 02          	.byte 0x2
 1559 0bd8 8F 1E       	.2byte 0x1e8f
 1560 0bda 6B 0B 00 00 	.4byte 0xb6b
 1561 0bde 01          	.byte 0x1
 1562 0bdf 01          	.byte 0x1
 1563 0be0 16          	.uleb128 0x16
 1564 0be1 00 00 00 00 	.4byte .LASF1
 1565 0be5 02          	.byte 0x2
 1566 0be6 CE 21       	.2byte 0x21ce
 1567 0be8 7E 0B 00 00 	.4byte 0xb7e
 1568 0bec 01          	.byte 0x1
 1569 0bed 01          	.byte 0x1
 1570 0bee 18          	.uleb128 0x18
 1571 0bef 4F 53 43 43 	.asciz "OSCCON"
 1571      4F 4E 00 
 1572 0bf6 02          	.byte 0x2
 1573 0bf7 BF 23       	.2byte 0x23bf
 1574 0bf9 94 0B 00 00 	.4byte 0xb94
 1575 0bfd 01          	.byte 0x1
 1576 0bfe 01          	.byte 0x1
MPLAB XC16 ASSEMBLY Listing:   			page 34


 1577 0bff 16          	.uleb128 0x16
 1578 0c00 00 00 00 00 	.4byte .LASF2
 1579 0c04 02          	.byte 0x2
 1580 0c05 4B 3A       	.2byte 0x3a4b
 1581 0c07 A7 0B 00 00 	.4byte 0xba7
 1582 0c0b 01          	.byte 0x1
 1583 0c0c 01          	.byte 0x1
 1584 0c0d 16          	.uleb128 0x16
 1585 0c0e 00 00 00 00 	.4byte .LASF3
 1586 0c12 02          	.byte 0x2
 1587 0c13 CF 3A       	.2byte 0x3acf
 1588 0c15 BA 0B 00 00 	.4byte 0xbba
 1589 0c19 01          	.byte 0x1
 1590 0c1a 01          	.byte 0x1
 1591 0c1b 16          	.uleb128 0x16
 1592 0c1c 00 00 00 00 	.4byte .LASF4
 1593 0c20 02          	.byte 0x2
 1594 0c21 CA 3B       	.2byte 0x3bca
 1595 0c23 CD 0B 00 00 	.4byte 0xbcd
 1596 0c27 01          	.byte 0x1
 1597 0c28 01          	.byte 0x1
 1598 0c29 00          	.byte 0x0
 1599                 	.section .debug_abbrev,info
 1600 0000 01          	.uleb128 0x1
 1601 0001 11          	.uleb128 0x11
 1602 0002 01          	.byte 0x1
 1603 0003 25          	.uleb128 0x25
 1604 0004 08          	.uleb128 0x8
 1605 0005 13          	.uleb128 0x13
 1606 0006 0B          	.uleb128 0xb
 1607 0007 03          	.uleb128 0x3
 1608 0008 08          	.uleb128 0x8
 1609 0009 1B          	.uleb128 0x1b
 1610 000a 08          	.uleb128 0x8
 1611 000b 11          	.uleb128 0x11
 1612 000c 01          	.uleb128 0x1
 1613 000d 12          	.uleb128 0x12
 1614 000e 01          	.uleb128 0x1
 1615 000f 10          	.uleb128 0x10
 1616 0010 06          	.uleb128 0x6
 1617 0011 00          	.byte 0x0
 1618 0012 00          	.byte 0x0
 1619 0013 02          	.uleb128 0x2
 1620 0014 24          	.uleb128 0x24
 1621 0015 00          	.byte 0x0
 1622 0016 0B          	.uleb128 0xb
 1623 0017 0B          	.uleb128 0xb
 1624 0018 3E          	.uleb128 0x3e
 1625 0019 0B          	.uleb128 0xb
 1626 001a 03          	.uleb128 0x3
 1627 001b 08          	.uleb128 0x8
 1628 001c 00          	.byte 0x0
 1629 001d 00          	.byte 0x0
 1630 001e 03          	.uleb128 0x3
 1631 001f 16          	.uleb128 0x16
 1632 0020 00          	.byte 0x0
 1633 0021 03          	.uleb128 0x3
MPLAB XC16 ASSEMBLY Listing:   			page 35


 1634 0022 08          	.uleb128 0x8
 1635 0023 3A          	.uleb128 0x3a
 1636 0024 0B          	.uleb128 0xb
 1637 0025 3B          	.uleb128 0x3b
 1638 0026 0B          	.uleb128 0xb
 1639 0027 49          	.uleb128 0x49
 1640 0028 13          	.uleb128 0x13
 1641 0029 00          	.byte 0x0
 1642 002a 00          	.byte 0x0
 1643 002b 04          	.uleb128 0x4
 1644 002c 13          	.uleb128 0x13
 1645 002d 01          	.byte 0x1
 1646 002e 0B          	.uleb128 0xb
 1647 002f 0B          	.uleb128 0xb
 1648 0030 3A          	.uleb128 0x3a
 1649 0031 0B          	.uleb128 0xb
 1650 0032 3B          	.uleb128 0x3b
 1651 0033 05          	.uleb128 0x5
 1652 0034 01          	.uleb128 0x1
 1653 0035 13          	.uleb128 0x13
 1654 0036 00          	.byte 0x0
 1655 0037 00          	.byte 0x0
 1656 0038 05          	.uleb128 0x5
 1657 0039 0D          	.uleb128 0xd
 1658 003a 00          	.byte 0x0
 1659 003b 03          	.uleb128 0x3
 1660 003c 08          	.uleb128 0x8
 1661 003d 3A          	.uleb128 0x3a
 1662 003e 0B          	.uleb128 0xb
 1663 003f 3B          	.uleb128 0x3b
 1664 0040 05          	.uleb128 0x5
 1665 0041 49          	.uleb128 0x49
 1666 0042 13          	.uleb128 0x13
 1667 0043 0B          	.uleb128 0xb
 1668 0044 0B          	.uleb128 0xb
 1669 0045 0D          	.uleb128 0xd
 1670 0046 0B          	.uleb128 0xb
 1671 0047 0C          	.uleb128 0xc
 1672 0048 0B          	.uleb128 0xb
 1673 0049 38          	.uleb128 0x38
 1674 004a 0A          	.uleb128 0xa
 1675 004b 00          	.byte 0x0
 1676 004c 00          	.byte 0x0
 1677 004d 06          	.uleb128 0x6
 1678 004e 17          	.uleb128 0x17
 1679 004f 01          	.byte 0x1
 1680 0050 0B          	.uleb128 0xb
 1681 0051 0B          	.uleb128 0xb
 1682 0052 3A          	.uleb128 0x3a
 1683 0053 0B          	.uleb128 0xb
 1684 0054 3B          	.uleb128 0x3b
 1685 0055 05          	.uleb128 0x5
 1686 0056 01          	.uleb128 0x1
 1687 0057 13          	.uleb128 0x13
 1688 0058 00          	.byte 0x0
 1689 0059 00          	.byte 0x0
 1690 005a 07          	.uleb128 0x7
MPLAB XC16 ASSEMBLY Listing:   			page 36


 1691 005b 0D          	.uleb128 0xd
 1692 005c 00          	.byte 0x0
 1693 005d 49          	.uleb128 0x49
 1694 005e 13          	.uleb128 0x13
 1695 005f 00          	.byte 0x0
 1696 0060 00          	.byte 0x0
 1697 0061 08          	.uleb128 0x8
 1698 0062 13          	.uleb128 0x13
 1699 0063 01          	.byte 0x1
 1700 0064 03          	.uleb128 0x3
 1701 0065 08          	.uleb128 0x8
 1702 0066 0B          	.uleb128 0xb
 1703 0067 0B          	.uleb128 0xb
 1704 0068 3A          	.uleb128 0x3a
 1705 0069 0B          	.uleb128 0xb
 1706 006a 3B          	.uleb128 0x3b
 1707 006b 05          	.uleb128 0x5
 1708 006c 01          	.uleb128 0x1
 1709 006d 13          	.uleb128 0x13
 1710 006e 00          	.byte 0x0
 1711 006f 00          	.byte 0x0
 1712 0070 09          	.uleb128 0x9
 1713 0071 0D          	.uleb128 0xd
 1714 0072 00          	.byte 0x0
 1715 0073 49          	.uleb128 0x49
 1716 0074 13          	.uleb128 0x13
 1717 0075 38          	.uleb128 0x38
 1718 0076 0A          	.uleb128 0xa
 1719 0077 00          	.byte 0x0
 1720 0078 00          	.byte 0x0
 1721 0079 0A          	.uleb128 0xa
 1722 007a 16          	.uleb128 0x16
 1723 007b 00          	.byte 0x0
 1724 007c 03          	.uleb128 0x3
 1725 007d 08          	.uleb128 0x8
 1726 007e 3A          	.uleb128 0x3a
 1727 007f 0B          	.uleb128 0xb
 1728 0080 3B          	.uleb128 0x3b
 1729 0081 05          	.uleb128 0x5
 1730 0082 49          	.uleb128 0x49
 1731 0083 13          	.uleb128 0x13
 1732 0084 00          	.byte 0x0
 1733 0085 00          	.byte 0x0
 1734 0086 0B          	.uleb128 0xb
 1735 0087 04          	.uleb128 0x4
 1736 0088 01          	.byte 0x1
 1737 0089 0B          	.uleb128 0xb
 1738 008a 0B          	.uleb128 0xb
 1739 008b 3A          	.uleb128 0x3a
 1740 008c 0B          	.uleb128 0xb
 1741 008d 3B          	.uleb128 0x3b
 1742 008e 0B          	.uleb128 0xb
 1743 008f 01          	.uleb128 0x1
 1744 0090 13          	.uleb128 0x13
 1745 0091 00          	.byte 0x0
 1746 0092 00          	.byte 0x0
 1747 0093 0C          	.uleb128 0xc
MPLAB XC16 ASSEMBLY Listing:   			page 37


 1748 0094 28          	.uleb128 0x28
 1749 0095 00          	.byte 0x0
 1750 0096 03          	.uleb128 0x3
 1751 0097 08          	.uleb128 0x8
 1752 0098 1C          	.uleb128 0x1c
 1753 0099 0D          	.uleb128 0xd
 1754 009a 00          	.byte 0x0
 1755 009b 00          	.byte 0x0
 1756 009c 0D          	.uleb128 0xd
 1757 009d 13          	.uleb128 0x13
 1758 009e 01          	.byte 0x1
 1759 009f 0B          	.uleb128 0xb
 1760 00a0 0B          	.uleb128 0xb
 1761 00a1 3A          	.uleb128 0x3a
 1762 00a2 0B          	.uleb128 0xb
 1763 00a3 3B          	.uleb128 0x3b
 1764 00a4 0B          	.uleb128 0xb
 1765 00a5 01          	.uleb128 0x1
 1766 00a6 13          	.uleb128 0x13
 1767 00a7 00          	.byte 0x0
 1768 00a8 00          	.byte 0x0
 1769 00a9 0E          	.uleb128 0xe
 1770 00aa 0D          	.uleb128 0xd
 1771 00ab 00          	.byte 0x0
 1772 00ac 03          	.uleb128 0x3
 1773 00ad 08          	.uleb128 0x8
 1774 00ae 3A          	.uleb128 0x3a
 1775 00af 0B          	.uleb128 0xb
 1776 00b0 3B          	.uleb128 0x3b
 1777 00b1 0B          	.uleb128 0xb
 1778 00b2 49          	.uleb128 0x49
 1779 00b3 13          	.uleb128 0x13
 1780 00b4 0B          	.uleb128 0xb
 1781 00b5 0B          	.uleb128 0xb
 1782 00b6 0D          	.uleb128 0xd
 1783 00b7 0B          	.uleb128 0xb
 1784 00b8 0C          	.uleb128 0xc
 1785 00b9 0B          	.uleb128 0xb
 1786 00ba 38          	.uleb128 0x38
 1787 00bb 0A          	.uleb128 0xa
 1788 00bc 00          	.byte 0x0
 1789 00bd 00          	.byte 0x0
 1790 00be 0F          	.uleb128 0xf
 1791 00bf 0D          	.uleb128 0xd
 1792 00c0 00          	.byte 0x0
 1793 00c1 03          	.uleb128 0x3
 1794 00c2 08          	.uleb128 0x8
 1795 00c3 3A          	.uleb128 0x3a
 1796 00c4 0B          	.uleb128 0xb
 1797 00c5 3B          	.uleb128 0x3b
 1798 00c6 0B          	.uleb128 0xb
 1799 00c7 49          	.uleb128 0x49
 1800 00c8 13          	.uleb128 0x13
 1801 00c9 38          	.uleb128 0x38
 1802 00ca 0A          	.uleb128 0xa
 1803 00cb 00          	.byte 0x0
 1804 00cc 00          	.byte 0x0
MPLAB XC16 ASSEMBLY Listing:   			page 38


 1805 00cd 10          	.uleb128 0x10
 1806 00ce 2E          	.uleb128 0x2e
 1807 00cf 00          	.byte 0x0
 1808 00d0 3F          	.uleb128 0x3f
 1809 00d1 0C          	.uleb128 0xc
 1810 00d2 03          	.uleb128 0x3
 1811 00d3 08          	.uleb128 0x8
 1812 00d4 3A          	.uleb128 0x3a
 1813 00d5 0B          	.uleb128 0xb
 1814 00d6 3B          	.uleb128 0x3b
 1815 00d7 0B          	.uleb128 0xb
 1816 00d8 11          	.uleb128 0x11
 1817 00d9 01          	.uleb128 0x1
 1818 00da 12          	.uleb128 0x12
 1819 00db 01          	.uleb128 0x1
 1820 00dc 40          	.uleb128 0x40
 1821 00dd 0A          	.uleb128 0xa
 1822 00de 00          	.byte 0x0
 1823 00df 00          	.byte 0x0
 1824 00e0 11          	.uleb128 0x11
 1825 00e1 2E          	.uleb128 0x2e
 1826 00e2 01          	.byte 0x1
 1827 00e3 3F          	.uleb128 0x3f
 1828 00e4 0C          	.uleb128 0xc
 1829 00e5 03          	.uleb128 0x3
 1830 00e6 08          	.uleb128 0x8
 1831 00e7 3A          	.uleb128 0x3a
 1832 00e8 0B          	.uleb128 0xb
 1833 00e9 3B          	.uleb128 0x3b
 1834 00ea 0B          	.uleb128 0xb
 1835 00eb 11          	.uleb128 0x11
 1836 00ec 01          	.uleb128 0x1
 1837 00ed 12          	.uleb128 0x12
 1838 00ee 01          	.uleb128 0x1
 1839 00ef 40          	.uleb128 0x40
 1840 00f0 0A          	.uleb128 0xa
 1841 00f1 01          	.uleb128 0x1
 1842 00f2 13          	.uleb128 0x13
 1843 00f3 00          	.byte 0x0
 1844 00f4 00          	.byte 0x0
 1845 00f5 12          	.uleb128 0x12
 1846 00f6 34          	.uleb128 0x34
 1847 00f7 00          	.byte 0x0
 1848 00f8 03          	.uleb128 0x3
 1849 00f9 08          	.uleb128 0x8
 1850 00fa 3A          	.uleb128 0x3a
 1851 00fb 0B          	.uleb128 0xb
 1852 00fc 3B          	.uleb128 0x3b
 1853 00fd 0B          	.uleb128 0xb
 1854 00fe 49          	.uleb128 0x49
 1855 00ff 13          	.uleb128 0x13
 1856 0100 02          	.uleb128 0x2
 1857 0101 0A          	.uleb128 0xa
 1858 0102 00          	.byte 0x0
 1859 0103 00          	.byte 0x0
 1860 0104 13          	.uleb128 0x13
 1861 0105 2E          	.uleb128 0x2e
MPLAB XC16 ASSEMBLY Listing:   			page 39


 1862 0106 00          	.byte 0x0
 1863 0107 3F          	.uleb128 0x3f
 1864 0108 0C          	.uleb128 0xc
 1865 0109 03          	.uleb128 0x3
 1866 010a 08          	.uleb128 0x8
 1867 010b 3A          	.uleb128 0x3a
 1868 010c 0B          	.uleb128 0xb
 1869 010d 3B          	.uleb128 0x3b
 1870 010e 0B          	.uleb128 0xb
 1871 010f 27          	.uleb128 0x27
 1872 0110 0C          	.uleb128 0xc
 1873 0111 11          	.uleb128 0x11
 1874 0112 01          	.uleb128 0x1
 1875 0113 12          	.uleb128 0x12
 1876 0114 01          	.uleb128 0x1
 1877 0115 40          	.uleb128 0x40
 1878 0116 0A          	.uleb128 0xa
 1879 0117 00          	.byte 0x0
 1880 0118 00          	.byte 0x0
 1881 0119 14          	.uleb128 0x14
 1882 011a 2E          	.uleb128 0x2e
 1883 011b 01          	.byte 0x1
 1884 011c 3F          	.uleb128 0x3f
 1885 011d 0C          	.uleb128 0xc
 1886 011e 03          	.uleb128 0x3
 1887 011f 08          	.uleb128 0x8
 1888 0120 3A          	.uleb128 0x3a
 1889 0121 0B          	.uleb128 0xb
 1890 0122 3B          	.uleb128 0x3b
 1891 0123 0B          	.uleb128 0xb
 1892 0124 49          	.uleb128 0x49
 1893 0125 13          	.uleb128 0x13
 1894 0126 3C          	.uleb128 0x3c
 1895 0127 0C          	.uleb128 0xc
 1896 0128 00          	.byte 0x0
 1897 0129 00          	.byte 0x0
 1898 012a 15          	.uleb128 0x15
 1899 012b 18          	.uleb128 0x18
 1900 012c 00          	.byte 0x0
 1901 012d 00          	.byte 0x0
 1902 012e 00          	.byte 0x0
 1903 012f 16          	.uleb128 0x16
 1904 0130 34          	.uleb128 0x34
 1905 0131 00          	.byte 0x0
 1906 0132 03          	.uleb128 0x3
 1907 0133 0E          	.uleb128 0xe
 1908 0134 3A          	.uleb128 0x3a
 1909 0135 0B          	.uleb128 0xb
 1910 0136 3B          	.uleb128 0x3b
 1911 0137 05          	.uleb128 0x5
 1912 0138 49          	.uleb128 0x49
 1913 0139 13          	.uleb128 0x13
 1914 013a 3F          	.uleb128 0x3f
 1915 013b 0C          	.uleb128 0xc
 1916 013c 3C          	.uleb128 0x3c
 1917 013d 0C          	.uleb128 0xc
 1918 013e 00          	.byte 0x0
MPLAB XC16 ASSEMBLY Listing:   			page 40


 1919 013f 00          	.byte 0x0
 1920 0140 17          	.uleb128 0x17
 1921 0141 35          	.uleb128 0x35
 1922 0142 00          	.byte 0x0
 1923 0143 49          	.uleb128 0x49
 1924 0144 13          	.uleb128 0x13
 1925 0145 00          	.byte 0x0
 1926 0146 00          	.byte 0x0
 1927 0147 18          	.uleb128 0x18
 1928 0148 34          	.uleb128 0x34
 1929 0149 00          	.byte 0x0
 1930 014a 03          	.uleb128 0x3
 1931 014b 08          	.uleb128 0x8
 1932 014c 3A          	.uleb128 0x3a
 1933 014d 0B          	.uleb128 0xb
 1934 014e 3B          	.uleb128 0x3b
 1935 014f 05          	.uleb128 0x5
 1936 0150 49          	.uleb128 0x49
 1937 0151 13          	.uleb128 0x13
 1938 0152 3F          	.uleb128 0x3f
 1939 0153 0C          	.uleb128 0xc
 1940 0154 3C          	.uleb128 0x3c
 1941 0155 0C          	.uleb128 0xc
 1942 0156 00          	.byte 0x0
 1943 0157 00          	.byte 0x0
 1944 0158 00          	.byte 0x0
 1945                 	.section .debug_pubnames,info
 1946 0000 4F 00 00 00 	.4byte 0x4f
 1947 0004 02 00       	.2byte 0x2
 1948 0006 00 00 00 00 	.4byte .Ldebug_info0
 1949 000a 2A 0C 00 00 	.4byte 0xc2a
 1950 000e BA 0A 00 00 	.4byte 0xaba
 1951 0012 63 6F 6E 66 	.asciz "config_IO"
 1951      69 67 5F 49 
 1951      4F 00 
 1952 001c D2 0A 00 00 	.4byte 0xad2
 1953 0020 63 6F 6E 66 	.asciz "config_adc"
 1953      69 67 5F 61 
 1953      64 63 00 
 1954 002b 05 0B 00 00 	.4byte 0xb05
 1955 002f 65 78 74 65 	.asciz "external1_callback"
 1955      72 6E 61 6C 
 1955      31 5F 63 61 
 1955      6C 6C 62 61 
 1955      63 6B 00 
 1956 0042 27 0B 00 00 	.4byte 0xb27
 1957 0046 74 6D 72 32 	.asciz "tmr2_int"
 1957      5F 69 6E 74 
 1957      00 
 1958 004f 00 00 00 00 	.4byte 0x0
 1959                 	.section .debug_pubtypes,info
 1960 0000 EF 00 00 00 	.4byte 0xef
 1961 0004 02 00       	.2byte 0x2
 1962 0006 00 00 00 00 	.4byte .Ldebug_info0
 1963 000a 2A 0C 00 00 	.4byte 0xc2a
 1964 000e E9 00 00 00 	.4byte 0xe9
 1965 0012 75 69 6E 74 	.asciz "uint16_t"
MPLAB XC16 ASSEMBLY Listing:   			page 41


 1965      31 36 5F 74 
 1965      00 
 1966 001b 84 02 00 00 	.4byte 0x284
 1967 001f 74 61 67 52 	.asciz "tagRPOR7BITS"
 1967      50 4F 52 37 
 1967      42 49 54 53 
 1967      00 
 1968 002c A3 02 00 00 	.4byte 0x2a3
 1969 0030 52 50 4F 52 	.asciz "RPOR7BITS"
 1969      37 42 49 54 
 1969      53 00 
 1970 003a 2B 04 00 00 	.4byte 0x42b
 1971 003e 74 61 67 52 	.asciz "tagRPINR26BITS"
 1971      50 49 4E 52 
 1971      32 36 42 49 
 1971      54 53 00 
 1972 004d 4C 04 00 00 	.4byte 0x44c
 1973 0051 52 50 49 4E 	.asciz "RPINR26BITS"
 1973      52 32 36 42 
 1973      49 54 53 00 
 1974 005d 60 04 00 00 	.4byte 0x460
 1975 0061 74 61 67 54 	.asciz "tagTRISBBITS"
 1975      52 49 53 42 
 1975      42 49 54 53 
 1975      00 
 1976 006e CD 05 00 00 	.4byte 0x5cd
 1977 0072 54 52 49 53 	.asciz "TRISBBITS"
 1977      42 42 49 54 
 1977      53 00 
 1978 007c DF 05 00 00 	.4byte 0x5df
 1979 0080 74 61 67 41 	.asciz "tagANSELBBITS"
 1979      4E 53 45 4C 
 1979      42 42 49 54 
 1979      53 00 
 1980 008e 3D 07 00 00 	.4byte 0x73d
 1981 0092 41 4E 53 45 	.asciz "ANSELBBITS"
 1981      4C 42 42 49 
 1981      54 53 00 
 1982 009d 50 07 00 00 	.4byte 0x750
 1983 00a1 74 61 67 4C 	.asciz "tagLATEBITS"
 1983      41 54 45 42 
 1983      49 54 53 00 
 1984 00ad 06 08 00 00 	.4byte 0x806
 1985 00b1 4C 41 54 45 	.asciz "LATEBITS"
 1985      42 49 54 53 
 1985      00 
 1986 00ba 30 08 00 00 	.4byte 0x830
 1987 00be 53 61 6D 70 	.asciz "Sampling"
 1987      6C 69 6E 67 
 1987      00 
 1988 00c7 85 08 00 00 	.4byte 0x885
 1989 00cb 53 53 52 43 	.asciz "SSRC"
 1989      00 
 1990 00d0 D8 08 00 00 	.4byte 0x8d8
 1991 00d4 56 6F 6C 74 	.asciz "Voltage"
 1991      61 67 65 00 
 1992 00dc A4 0A 00 00 	.4byte 0xaa4
MPLAB XC16 ASSEMBLY Listing:   			page 42


 1993 00e0 41 44 43 5F 	.asciz "ADC_parameters"
 1993      70 61 72 61 
 1993      6D 65 74 65 
 1993      72 73 00 
 1994 00ef 00 00 00 00 	.4byte 0x0
 1995                 	.section .debug_aranges,info
 1996 0000 14 00 00 00 	.4byte 0x14
 1997 0004 02 00       	.2byte 0x2
 1998 0006 00 00 00 00 	.4byte .Ldebug_info0
 1999 000a 04          	.byte 0x4
 2000 000b 00          	.byte 0x0
 2001 000c 00 00       	.2byte 0x0
 2002 000e 00 00       	.2byte 0x0
 2003 0010 00 00 00 00 	.4byte 0x0
 2004 0014 00 00 00 00 	.4byte 0x0
 2005                 	.section .debug_str,info
 2006                 	.LASF3:
 2007 0000 41 4E 53 45 	.asciz "ANSELBbits"
 2007      4C 42 62 69 
 2007      74 73 00 
 2008                 	.LASF1:
 2009 000b 52 50 49 4E 	.asciz "RPINR26bits"
 2009      52 32 36 62 
 2009      69 74 73 00 
 2010                 	.LASF2:
 2011 0017 54 52 49 53 	.asciz "TRISBbits"
 2011      42 62 69 74 
 2011      73 00 
 2012                 	.LASF4:
 2013 0021 4C 41 54 45 	.asciz "LATEbits"
 2013      62 69 74 73 
 2013      00 
 2014                 	.LASF0:
 2015 002a 52 50 4F 52 	.asciz "RPOR7bits"
 2015      37 62 69 74 
 2015      73 00 
 2016                 	.section .text,code
 2017              	
 2018              	
 2019              	
 2020              	.section __c30_info,info,bss
 2021                 	__psv_trap_errata:
 2022                 	
 2023                 	.section __c30_signature,info,data
 2024 0000 01 00       	.word 0x0001
 2025 0002 00 00       	.word 0x0000
 2026 0004 00 00       	.word 0x0000
 2027                 	
 2028                 	
 2029                 	
 2030                 	.set ___PA___,0
 2031                 	.end
MPLAB XC16 ASSEMBLY Listing:   			page 43


DEFINED SYMBOLS
                            *ABS*:00000000 /home/user/Documents/FST/Programming/project_sub-zero/src/config_sub-zero.c
                            *ABS*:00000001 __33EP256MU806
                            *ABS*:00000001 __dsPIC33E
                            *ABS*:00000001 __HAS_DSP
                            *ABS*:00000001 __HAS_DMAV2
                            *ABS*:00000001 __HAS_CODEGUARD
                            *ABS*:00000001 __HAS_PMP_ENHANCED
                            *ABS*:00000001 __HAS_EDS
                            *ABS*:00000001 __HAS_EP
                            *ABS*:00000011 __TARGET_DIVIDE_CYCLES
                            *ABS*:00000001 __C30ELF
    {standard input}:14     .text:00000000 _config_IO
    {standard input}:18     *ABS*:00000000 ___PA___
    {standard input}:72     .text:00000044 _config_adc
    {standard input}:111    .text:0000006e _external1_callback
    {standard input}:141    .text:00000090 _tmr2_int
    {standard input}:2021   __c30_info:00000000 __psv_trap_errata
    {standard input}:19     .text:00000000 .L0
                    .debug_abbrev:00000000 .Ldebug_abbrev0
                            .text:00000000 .Ltext0
                            .text:0000009c .Letext0
                      .debug_line:00000000 .Ldebug_line0
                            .text:00000000 .LFB0
                            .text:00000044 .LFE0
                            .text:00000044 .LFB1
                            .text:0000006e .LFE1
                            .text:0000006e .LFB2
                            .text:00000090 .LFE2
                            .text:00000090 .LFB3
                            .text:0000009c .LFE3
                       .debug_str:0000002a .LASF0
                       .debug_str:0000000b .LASF1
                       .debug_str:00000017 .LASF2
                       .debug_str:00000000 .LASF3
                       .debug_str:00000021 .LASF4
                     .debug_frame:00000000 .Lframe0
                      .debug_info:00000000 .Ldebug_info0

UNDEFINED SYMBOLS
_OSCCON
_RPOR7bits
_RPINR26bits
CORCON
_ANSELBbits
_TRISBbits
_config_pin_cycling
_config_adc1
_LATEbits
_send_adc_value

EQUATE SYMBOLS
/home/user/Documents/FST/Programming/project_sub-zero/src/config_sub-zero.c = 0x0
                  __33EP256MU806 = 0x1
                      __dsPIC33E = 0x1
                       __HAS_DSP = 0x1
                     __HAS_DMAV2 = 0x1
                 __HAS_CODEGUARD = 0x1
              __HAS_PMP_ENHANCED = 0x1
                       __HAS_EDS = 0x1
                        __HAS_EP = 0x1
          __TARGET_DIVIDE_CYCLES = 0x11
                        __C30ELF = 0x1
                        ___PA___ = 0x0
