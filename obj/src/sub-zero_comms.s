MPLAB XC16 ASSEMBLY Listing:   			page 1


   1              	.file "/home/user/Documents/FST/Programming/project_sub-zero/src/sub-zero_comms.c"
   2              	.section .debug_abbrev,info
   3                 	.Ldebug_abbrev0:
   4                 	.section .debug_info,info
   5                 	.Ldebug_info0:
   6                 	.section .debug_line,info
   7                 	.Ldebug_line0:
   8 0000 FF 00 00 00 	.section .text,code
   8      02 00 D1 00 
   8      00 00 01 01 
   8      FB 0E 0A 00 
   8      01 01 01 01 
   8      00 00 00 01 
   8      73 72 63 00 
   8      6C 69 62 2F 
   8      6C 69 62 5F 
   9              	.Ltext0:
  10              	.section .text,code
  11              	.align 2
  12              	.global _send_reset_message
  13              	.type _send_reset_message,@function
  14              	_send_reset_message:
  15              	.LFB0:
  16              	.file 1 "src/sub-zero_comms.c"
   1:src/sub-zero_comms.c **** #include <stdlib.h>
   2:src/sub-zero_comms.c **** #include "sub-zero_comms.h"
   3:src/sub-zero_comms.c **** #include "config_sub-zero.h"
   4:src/sub-zero_comms.c **** #include "lib_pic33e/can.h"
   5:src/sub-zero_comms.c **** #include "can-ids/Devices/SUBZERO_CAN.h"
   6:src/sub-zero_comms.c **** #include "lib_pic33e/ADC.h"
   7:src/sub-zero_comms.c **** 
   8:src/sub-zero_comms.c **** void send_reset_message(){
  17              	.loc 1 8 0
  18              	.set ___PA___,1
  19 000000  0C 00 FA 	lnk #12
  20              	.LCFI0:
   9:src/sub-zero_comms.c **** 	CANdata reset_message;
  10:src/sub-zero_comms.c **** 
  11:src/sub-zero_comms.c **** 	reset_message.dev_id = DEVICE_ID_SUBZERO;
  21              	.loc 1 11 0
  22 000002  9E 00 78 	mov [w14],w1
  23 000004  00 FE 2F 	mov #-32,w0
  24 000006  00 80 60 	and w1,w0,w0
  25 000008  20 01 B3 	ior #18,w0
  26 00000a  00 0F 78 	mov w0,[w14]
  12:src/sub-zero_comms.c **** 	reset_message.msg_id = MSG_ID_SUBZERO_RESET;
  27              	.loc 1 12 0
  28 00000c  F1 81 2F 	mov #-2017,w1
  29 00000e  1E 01 78 	mov [w14],w2
  30 000010  00 50 20 	mov #1280,w0
  31 000012  81 00 61 	and w2,w1,w1
  32 000014  01 00 70 	ior w0,w1,w0
  33 000016  00 0F 78 	mov w0,[w14]
  13:src/sub-zero_comms.c **** 	reset_message.dlc = 0;
  34              	.loc 1 13 0
  35 000018  1E 01 90 	mov [w14+2],w2
  36 00001a  01 FF 2F 	mov #-16,w1
MPLAB XC16 ASSEMBLY Listing:   			page 2


  14:src/sub-zero_comms.c **** 	send_can1(reset_message);
  37              	.loc 1 14 0
  38 00001c  1E 00 78 	mov [w14],w0
  39              	.loc 1 13 0
  40 00001e  81 00 61 	and w2,w1,w1
  41              	.loc 1 14 0
  42 000020  2E 01 90 	mov [w14+4],w2
  43              	.loc 1 13 0
  44 000022  11 07 98 	mov w1,[w14+2]
  45              	.loc 1 14 0
  46 000024  BE 01 90 	mov [w14+6],w3
  47 000026  9E 00 90 	mov [w14+2],w1
  48 000028  4E 02 90 	mov [w14+8],w4
  49 00002a  DE 02 90 	mov [w14+10],w5
  50 00002c  00 00 07 	rcall _send_can1
  15:src/sub-zero_comms.c **** }
  51              	.loc 1 15 0
  52 00002e  8E 07 78 	mov w14,w15
  53 000030  4F 07 78 	mov [--w15],w14
  54 000032  00 40 A9 	bclr CORCON,#2
  55 000034  00 00 06 	return 
  56              	.set ___PA___,0
  57              	.LFE0:
  58              	.size _send_reset_message,.-_send_reset_message
  59              	.align 2
  60              	.global _send_adc_value
  61              	.type _send_adc_value,@function
  62              	_send_adc_value:
  63              	.LFB1:
  16:src/sub-zero_comms.c **** 
  17:src/sub-zero_comms.c **** void send_adc_value(){
  64              	.loc 1 17 0
  65              	.set ___PA___,1
  66 000036  0C 00 FA 	lnk #12
  67              	.LCFI1:
  18:src/sub-zero_comms.c **** 	CANdata adc_value;
  19:src/sub-zero_comms.c **** 
  20:src/sub-zero_comms.c **** 	//uint16_t adc_results[1] = {0};
  21:src/sub-zero_comms.c **** 
  22:src/sub-zero_comms.c **** 	//get_pin_cycling_values(&adc_results, 0, 4);
  23:src/sub-zero_comms.c **** 
  24:src/sub-zero_comms.c **** 	adc_value.dev_id = DEVICE_ID_SUBZERO;
  68              	.loc 1 24 0
  69 000038  9E 00 78 	mov [w14],w1
  70 00003a  00 FE 2F 	mov #-32,w0
  71 00003c  00 80 60 	and w1,w0,w0
  72 00003e  20 01 B3 	ior #18,w0
  73 000040  00 0F 78 	mov w0,[w14]
  25:src/sub-zero_comms.c **** 	adc_value.msg_id = MSG_ID_SUBZERO_ADC;
  74              	.loc 1 25 0
  75 000042  F1 81 2F 	mov #-2017,w1
  76 000044  1E 01 78 	mov [w14],w2
  77 000046  00 52 20 	mov #1312,w0
  78 000048  81 00 61 	and w2,w1,w1
  79 00004a  01 00 70 	ior w0,w1,w0
  80 00004c  00 0F 78 	mov w0,[w14]
  26:src/sub-zero_comms.c **** 	adc_value.dlc = 8;
MPLAB XC16 ASSEMBLY Listing:   			page 3


  27:src/sub-zero_comms.c **** 	adc_value.data[0] = ADC1BUF0;
  81              	.loc 1 27 0
  82 00004e  04 00 80 	mov _ADC1BUF0,w4
  83              	.loc 1 26 0
  84 000050  9E 01 90 	mov [w14+2],w3
  28:src/sub-zero_comms.c **** 	adc_value.data[1] = ADC1BUF1;
  85              	.loc 1 28 0
  86 000052  02 00 80 	mov _ADC1BUF1,w2
  87              	.loc 1 26 0
  88 000054  00 FF 2F 	mov #-16,w0
  29:src/sub-zero_comms.c **** 	adc_value.data[2] = ADC1BUF2;
  89              	.loc 1 29 0
  90 000056  01 00 80 	mov _ADC1BUF2,w1
  91              	.loc 1 26 0
  92 000058  80 81 61 	and w3,w0,w3
  30:src/sub-zero_comms.c **** 	adc_value.data[3] = ADC1BUF3;
  93              	.loc 1 30 0
  94 00005a  00 00 80 	mov _ADC1BUF3,w0
  95              	.loc 1 26 0
  96 00005c  03 30 A0 	bset w3,#3
  97              	.loc 1 27 0
  98 00005e  24 07 98 	mov w4,[w14+4]
  99              	.loc 1 26 0
 100 000060  13 07 98 	mov w3,[w14+2]
 101              	.loc 1 28 0
 102 000062  32 07 98 	mov w2,[w14+6]
 103              	.loc 1 29 0
 104 000064  41 07 98 	mov w1,[w14+8]
 105              	.loc 1 30 0
 106 000066  50 07 98 	mov w0,[w14+10]
  31:src/sub-zero_comms.c **** 
  32:src/sub-zero_comms.c **** 	send_can1(adc_value);
 107              	.loc 1 32 0
 108 000068  1E 00 78 	mov [w14],w0
 109 00006a  9E 00 90 	mov [w14+2],w1
 110 00006c  2E 01 90 	mov [w14+4],w2
 111 00006e  BE 01 90 	mov [w14+6],w3
 112 000070  4E 02 90 	mov [w14+8],w4
 113 000072  DE 02 90 	mov [w14+10],w5
 114 000074  00 00 07 	rcall _send_can1
  33:src/sub-zero_comms.c **** 
  34:src/sub-zero_comms.c **** }
 115              	.loc 1 34 0
 116 000076  8E 07 78 	mov w14,w15
 117 000078  4F 07 78 	mov [--w15],w14
 118 00007a  00 40 A9 	bclr CORCON,#2
 119 00007c  00 00 06 	return 
 120              	.set ___PA___,0
 121              	.LFE1:
 122              	.size _send_adc_value,.-_send_adc_value
 123              	.section .debug_frame,info
 124                 	.Lframe0:
 125 0000 10 00 00 00 	.4byte .LECIE0-.LSCIE0
 126                 	.LSCIE0:
 127 0004 FF FF FF FF 	.4byte 0xffffffff
 128 0008 01          	.byte 0x1
 129 0009 00          	.byte 0
MPLAB XC16 ASSEMBLY Listing:   			page 4


 130 000a 01          	.uleb128 0x1
 131 000b 02          	.sleb128 2
 132 000c 25          	.byte 0x25
 133 000d 12          	.byte 0x12
 134 000e 0F          	.uleb128 0xf
 135 000f 7E          	.sleb128 -2
 136 0010 09          	.byte 0x9
 137 0011 25          	.uleb128 0x25
 138 0012 0F          	.uleb128 0xf
 139 0013 00          	.align 4
 140                 	.LECIE0:
 141                 	.LSFDE0:
 142 0014 18 00 00 00 	.4byte .LEFDE0-.LASFDE0
 143                 	.LASFDE0:
 144 0018 00 00 00 00 	.4byte .Lframe0
 145 001c 00 00 00 00 	.4byte .LFB0
 146 0020 36 00 00 00 	.4byte .LFE0-.LFB0
 147 0024 04          	.byte 0x4
 148 0025 02 00 00 00 	.4byte .LCFI0-.LFB0
 149 0029 13          	.byte 0x13
 150 002a 7D          	.sleb128 -3
 151 002b 0D          	.byte 0xd
 152 002c 0E          	.uleb128 0xe
 153 002d 8E          	.byte 0x8e
 154 002e 02          	.uleb128 0x2
 155 002f 00          	.align 4
 156                 	.LEFDE0:
 157                 	.LSFDE2:
 158 0030 18 00 00 00 	.4byte .LEFDE2-.LASFDE2
 159                 	.LASFDE2:
 160 0034 00 00 00 00 	.4byte .Lframe0
 161 0038 00 00 00 00 	.4byte .LFB1
 162 003c 48 00 00 00 	.4byte .LFE1-.LFB1
 163 0040 04          	.byte 0x4
 164 0041 02 00 00 00 	.4byte .LCFI1-.LFB1
 165 0045 13          	.byte 0x13
 166 0046 7D          	.sleb128 -3
 167 0047 0D          	.byte 0xd
 168 0048 0E          	.uleb128 0xe
 169 0049 8E          	.byte 0x8e
 170 004a 02          	.uleb128 0x2
 171 004b 00          	.align 4
 172                 	.LEFDE2:
 173                 	.section .text,code
 174              	.Letext0:
 175              	.file 2 "lib/lib_pic33e/can.h"
 176              	.file 3 "/opt/microchip/xc16/v1.36/bin/bin/../../include/lega-c/stdint.h"
 177              	.file 4 "/opt/microchip/xc16/v1.36/bin/bin/../../support/dsPIC33E/h/p33EP256MU806.h"
 178              	.section .debug_info,info
 179 0000 CD 02 00 00 	.4byte 0x2cd
 180 0004 02 00       	.2byte 0x2
 181 0006 00 00 00 00 	.4byte .Ldebug_abbrev0
 182 000a 04          	.byte 0x4
 183 000b 01          	.uleb128 0x1
 184 000c 47 4E 55 20 	.asciz "GNU C 4.5.1 (XC16, Microchip v1.36) (A) Build date: Dec  5 2018"
 184      43 20 34 2E 
 184      35 2E 31 20 
MPLAB XC16 ASSEMBLY Listing:   			page 5


 184      28 58 43 31 
 184      36 2C 20 4D 
 184      69 63 72 6F 
 184      63 68 69 70 
 184      20 76 31 2E 
 184      33 36 29 20 
 185 004c 01          	.byte 0x1
 186 004d 73 72 63 2F 	.asciz "src/sub-zero_comms.c"
 186      73 75 62 2D 
 186      7A 65 72 6F 
 186      5F 63 6F 6D 
 186      6D 73 2E 63 
 186      00 
 187 0062 2F 68 6F 6D 	.asciz "/home/user/Documents/FST/Programming/project_sub-zero"
 187      65 2F 75 73 
 187      65 72 2F 44 
 187      6F 63 75 6D 
 187      65 6E 74 73 
 187      2F 46 53 54 
 187      2F 50 72 6F 
 187      67 72 61 6D 
 187      6D 69 6E 67 
 188 0098 00 00 00 00 	.4byte .Ltext0
 189 009c 00 00 00 00 	.4byte .Letext0
 190 00a0 00 00 00 00 	.4byte .Ldebug_line0
 191 00a4 02          	.uleb128 0x2
 192 00a5 08          	.byte 0x8
 193 00a6 05          	.byte 0x5
 194 00a7 6C 6F 6E 67 	.asciz "long long int"
 194      20 6C 6F 6E 
 194      67 20 69 6E 
 194      74 00 
 195 00b5 02          	.uleb128 0x2
 196 00b6 08          	.byte 0x8
 197 00b7 07          	.byte 0x7
 198 00b8 6C 6F 6E 67 	.asciz "long long unsigned int"
 198      20 6C 6F 6E 
 198      67 20 75 6E 
 198      73 69 67 6E 
 198      65 64 20 69 
 198      6E 74 00 
 199 00cf 02          	.uleb128 0x2
 200 00d0 02          	.byte 0x2
 201 00d1 07          	.byte 0x7
 202 00d2 73 68 6F 72 	.asciz "short unsigned int"
 202      74 20 75 6E 
 202      73 69 67 6E 
 202      65 64 20 69 
 202      6E 74 00 
 203 00e5 02          	.uleb128 0x2
 204 00e6 02          	.byte 0x2
 205 00e7 07          	.byte 0x7
 206 00e8 75 6E 73 69 	.asciz "unsigned int"
 206      67 6E 65 64 
 206      20 69 6E 74 
 206      00 
 207 00f5 02          	.uleb128 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 6


 208 00f6 02          	.byte 0x2
 209 00f7 05          	.byte 0x5
 210 00f8 69 6E 74 00 	.asciz "int"
 211 00fc 02          	.uleb128 0x2
 212 00fd 04          	.byte 0x4
 213 00fe 05          	.byte 0x5
 214 00ff 6C 6F 6E 67 	.asciz "long int"
 214      20 69 6E 74 
 214      00 
 215 0108 02          	.uleb128 0x2
 216 0109 01          	.byte 0x1
 217 010a 06          	.byte 0x6
 218 010b 73 69 67 6E 	.asciz "signed char"
 218      65 64 20 63 
 218      68 61 72 00 
 219 0117 02          	.uleb128 0x2
 220 0118 01          	.byte 0x1
 221 0119 08          	.byte 0x8
 222 011a 75 6E 73 69 	.asciz "unsigned char"
 222      67 6E 65 64 
 222      20 63 68 61 
 222      72 00 
 223 0128 03          	.uleb128 0x3
 224 0129 75 69 6E 74 	.asciz "uint16_t"
 224      31 36 5F 74 
 224      00 
 225 0132 03          	.byte 0x3
 226 0133 31          	.byte 0x31
 227 0134 E5 00 00 00 	.4byte 0xe5
 228 0138 02          	.uleb128 0x2
 229 0139 04          	.byte 0x4
 230 013a 07          	.byte 0x7
 231 013b 6C 6F 6E 67 	.asciz "long unsigned int"
 231      20 75 6E 73 
 231      69 67 6E 65 
 231      64 20 69 6E 
 231      74 00 
 232 014d 04          	.uleb128 0x4
 233 014e 02          	.byte 0x2
 234 014f 02          	.byte 0x2
 235 0150 16          	.byte 0x16
 236 0151 7E 01 00 00 	.4byte 0x17e
 237 0155 05          	.uleb128 0x5
 238 0156 64 65 76 5F 	.asciz "dev_id"
 238      69 64 00 
 239 015d 02          	.byte 0x2
 240 015e 17          	.byte 0x17
 241 015f 28 01 00 00 	.4byte 0x128
 242 0163 02          	.byte 0x2
 243 0164 05          	.byte 0x5
 244 0165 0B          	.byte 0xb
 245 0166 02          	.byte 0x2
 246 0167 23          	.byte 0x23
 247 0168 00          	.uleb128 0x0
 248 0169 05          	.uleb128 0x5
 249 016a 6D 73 67 5F 	.asciz "msg_id"
 249      69 64 00 
MPLAB XC16 ASSEMBLY Listing:   			page 7


 250 0171 02          	.byte 0x2
 251 0172 18          	.byte 0x18
 252 0173 28 01 00 00 	.4byte 0x128
 253 0177 02          	.byte 0x2
 254 0178 06          	.byte 0x6
 255 0179 05          	.byte 0x5
 256 017a 02          	.byte 0x2
 257 017b 23          	.byte 0x23
 258 017c 00          	.uleb128 0x0
 259 017d 00          	.byte 0x0
 260 017e 06          	.uleb128 0x6
 261 017f 02          	.byte 0x2
 262 0180 02          	.byte 0x2
 263 0181 15          	.byte 0x15
 264 0182 97 01 00 00 	.4byte 0x197
 265 0186 07          	.uleb128 0x7
 266 0187 4D 01 00 00 	.4byte 0x14d
 267 018b 08          	.uleb128 0x8
 268 018c 73 69 64 00 	.asciz "sid"
 269 0190 02          	.byte 0x2
 270 0191 1A          	.byte 0x1a
 271 0192 28 01 00 00 	.4byte 0x128
 272 0196 00          	.byte 0x0
 273 0197 04          	.uleb128 0x4
 274 0198 0C          	.byte 0xc
 275 0199 02          	.byte 0x2
 276 019a 14          	.byte 0x14
 277 019b C8 01 00 00 	.4byte 0x1c8
 278 019f 09          	.uleb128 0x9
 279 01a0 7E 01 00 00 	.4byte 0x17e
 280 01a4 02          	.byte 0x2
 281 01a5 23          	.byte 0x23
 282 01a6 00          	.uleb128 0x0
 283 01a7 05          	.uleb128 0x5
 284 01a8 64 6C 63 00 	.asciz "dlc"
 285 01ac 02          	.byte 0x2
 286 01ad 1C          	.byte 0x1c
 287 01ae 28 01 00 00 	.4byte 0x128
 288 01b2 02          	.byte 0x2
 289 01b3 04          	.byte 0x4
 290 01b4 0C          	.byte 0xc
 291 01b5 02          	.byte 0x2
 292 01b6 23          	.byte 0x23
 293 01b7 02          	.uleb128 0x2
 294 01b8 0A          	.uleb128 0xa
 295 01b9 64 61 74 61 	.asciz "data"
 295      00 
 296 01be 02          	.byte 0x2
 297 01bf 1D          	.byte 0x1d
 298 01c0 C8 01 00 00 	.4byte 0x1c8
 299 01c4 02          	.byte 0x2
 300 01c5 23          	.byte 0x23
 301 01c6 04          	.uleb128 0x4
 302 01c7 00          	.byte 0x0
 303 01c8 0B          	.uleb128 0xb
 304 01c9 28 01 00 00 	.4byte 0x128
 305 01cd D8 01 00 00 	.4byte 0x1d8
MPLAB XC16 ASSEMBLY Listing:   			page 8


 306 01d1 0C          	.uleb128 0xc
 307 01d2 E5 00 00 00 	.4byte 0xe5
 308 01d6 03          	.byte 0x3
 309 01d7 00          	.byte 0x0
 310 01d8 03          	.uleb128 0x3
 311 01d9 43 41 4E 64 	.asciz "CANdata"
 311      61 74 61 00 
 312 01e1 02          	.byte 0x2
 313 01e2 1E          	.byte 0x1e
 314 01e3 97 01 00 00 	.4byte 0x197
 315 01e7 0D          	.uleb128 0xd
 316 01e8 01          	.byte 0x1
 317 01e9 73 65 6E 64 	.asciz "send_reset_message"
 317      5F 72 65 73 
 317      65 74 5F 6D 
 317      65 73 73 61 
 317      67 65 00 
 318 01fc 01          	.byte 0x1
 319 01fd 08          	.byte 0x8
 320 01fe 00 00 00 00 	.4byte .LFB0
 321 0202 00 00 00 00 	.4byte .LFE0
 322 0206 01          	.byte 0x1
 323 0207 5E          	.byte 0x5e
 324 0208 25 02 00 00 	.4byte 0x225
 325 020c 0E          	.uleb128 0xe
 326 020d 72 65 73 65 	.asciz "reset_message"
 326      74 5F 6D 65 
 326      73 73 61 67 
 326      65 00 
 327 021b 01          	.byte 0x1
 328 021c 09          	.byte 0x9
 329 021d D8 01 00 00 	.4byte 0x1d8
 330 0221 02          	.byte 0x2
 331 0222 7E          	.byte 0x7e
 332 0223 00          	.sleb128 0
 333 0224 00          	.byte 0x0
 334 0225 0D          	.uleb128 0xd
 335 0226 01          	.byte 0x1
 336 0227 73 65 6E 64 	.asciz "send_adc_value"
 336      5F 61 64 63 
 336      5F 76 61 6C 
 336      75 65 00 
 337 0236 01          	.byte 0x1
 338 0237 11          	.byte 0x11
 339 0238 00 00 00 00 	.4byte .LFB1
 340 023c 00 00 00 00 	.4byte .LFE1
 341 0240 01          	.byte 0x1
 342 0241 5E          	.byte 0x5e
 343 0242 5B 02 00 00 	.4byte 0x25b
 344 0246 0E          	.uleb128 0xe
 345 0247 61 64 63 5F 	.asciz "adc_value"
 345      76 61 6C 75 
 345      65 00 
 346 0251 01          	.byte 0x1
 347 0252 12          	.byte 0x12
 348 0253 D8 01 00 00 	.4byte 0x1d8
 349 0257 02          	.byte 0x2
MPLAB XC16 ASSEMBLY Listing:   			page 9


 350 0258 7E          	.byte 0x7e
 351 0259 00          	.sleb128 0
 352 025a 00          	.byte 0x0
 353 025b 0F          	.uleb128 0xf
 354 025c 00 00 00 00 	.4byte .LASF0
 355 0260 04          	.byte 0x4
 356 0261 80 09       	.2byte 0x980
 357 0263 69 02 00 00 	.4byte 0x269
 358 0267 01          	.byte 0x1
 359 0268 01          	.byte 0x1
 360 0269 10          	.uleb128 0x10
 361 026a 28 01 00 00 	.4byte 0x128
 362 026e 0F          	.uleb128 0xf
 363 026f 00 00 00 00 	.4byte .LASF1
 364 0273 04          	.byte 0x4
 365 0274 82 09       	.2byte 0x982
 366 0276 69 02 00 00 	.4byte 0x269
 367 027a 01          	.byte 0x1
 368 027b 01          	.byte 0x1
 369 027c 0F          	.uleb128 0xf
 370 027d 00 00 00 00 	.4byte .LASF2
 371 0281 04          	.byte 0x4
 372 0282 84 09       	.2byte 0x984
 373 0284 69 02 00 00 	.4byte 0x269
 374 0288 01          	.byte 0x1
 375 0289 01          	.byte 0x1
 376 028a 0F          	.uleb128 0xf
 377 028b 00 00 00 00 	.4byte .LASF3
 378 028f 04          	.byte 0x4
 379 0290 86 09       	.2byte 0x986
 380 0292 69 02 00 00 	.4byte 0x269
 381 0296 01          	.byte 0x1
 382 0297 01          	.byte 0x1
 383 0298 0F          	.uleb128 0xf
 384 0299 00 00 00 00 	.4byte .LASF0
 385 029d 04          	.byte 0x4
 386 029e 80 09       	.2byte 0x980
 387 02a0 69 02 00 00 	.4byte 0x269
 388 02a4 01          	.byte 0x1
 389 02a5 01          	.byte 0x1
 390 02a6 0F          	.uleb128 0xf
 391 02a7 00 00 00 00 	.4byte .LASF1
 392 02ab 04          	.byte 0x4
 393 02ac 82 09       	.2byte 0x982
 394 02ae 69 02 00 00 	.4byte 0x269
 395 02b2 01          	.byte 0x1
 396 02b3 01          	.byte 0x1
 397 02b4 0F          	.uleb128 0xf
 398 02b5 00 00 00 00 	.4byte .LASF2
 399 02b9 04          	.byte 0x4
 400 02ba 84 09       	.2byte 0x984
 401 02bc 69 02 00 00 	.4byte 0x269
 402 02c0 01          	.byte 0x1
 403 02c1 01          	.byte 0x1
 404 02c2 0F          	.uleb128 0xf
 405 02c3 00 00 00 00 	.4byte .LASF3
 406 02c7 04          	.byte 0x4
MPLAB XC16 ASSEMBLY Listing:   			page 10


 407 02c8 86 09       	.2byte 0x986
 408 02ca 69 02 00 00 	.4byte 0x269
 409 02ce 01          	.byte 0x1
 410 02cf 01          	.byte 0x1
 411 02d0 00          	.byte 0x0
 412                 	.section .debug_abbrev,info
 413 0000 01          	.uleb128 0x1
 414 0001 11          	.uleb128 0x11
 415 0002 01          	.byte 0x1
 416 0003 25          	.uleb128 0x25
 417 0004 08          	.uleb128 0x8
 418 0005 13          	.uleb128 0x13
 419 0006 0B          	.uleb128 0xb
 420 0007 03          	.uleb128 0x3
 421 0008 08          	.uleb128 0x8
 422 0009 1B          	.uleb128 0x1b
 423 000a 08          	.uleb128 0x8
 424 000b 11          	.uleb128 0x11
 425 000c 01          	.uleb128 0x1
 426 000d 12          	.uleb128 0x12
 427 000e 01          	.uleb128 0x1
 428 000f 10          	.uleb128 0x10
 429 0010 06          	.uleb128 0x6
 430 0011 00          	.byte 0x0
 431 0012 00          	.byte 0x0
 432 0013 02          	.uleb128 0x2
 433 0014 24          	.uleb128 0x24
 434 0015 00          	.byte 0x0
 435 0016 0B          	.uleb128 0xb
 436 0017 0B          	.uleb128 0xb
 437 0018 3E          	.uleb128 0x3e
 438 0019 0B          	.uleb128 0xb
 439 001a 03          	.uleb128 0x3
 440 001b 08          	.uleb128 0x8
 441 001c 00          	.byte 0x0
 442 001d 00          	.byte 0x0
 443 001e 03          	.uleb128 0x3
 444 001f 16          	.uleb128 0x16
 445 0020 00          	.byte 0x0
 446 0021 03          	.uleb128 0x3
 447 0022 08          	.uleb128 0x8
 448 0023 3A          	.uleb128 0x3a
 449 0024 0B          	.uleb128 0xb
 450 0025 3B          	.uleb128 0x3b
 451 0026 0B          	.uleb128 0xb
 452 0027 49          	.uleb128 0x49
 453 0028 13          	.uleb128 0x13
 454 0029 00          	.byte 0x0
 455 002a 00          	.byte 0x0
 456 002b 04          	.uleb128 0x4
 457 002c 13          	.uleb128 0x13
 458 002d 01          	.byte 0x1
 459 002e 0B          	.uleb128 0xb
 460 002f 0B          	.uleb128 0xb
 461 0030 3A          	.uleb128 0x3a
 462 0031 0B          	.uleb128 0xb
 463 0032 3B          	.uleb128 0x3b
MPLAB XC16 ASSEMBLY Listing:   			page 11


 464 0033 0B          	.uleb128 0xb
 465 0034 01          	.uleb128 0x1
 466 0035 13          	.uleb128 0x13
 467 0036 00          	.byte 0x0
 468 0037 00          	.byte 0x0
 469 0038 05          	.uleb128 0x5
 470 0039 0D          	.uleb128 0xd
 471 003a 00          	.byte 0x0
 472 003b 03          	.uleb128 0x3
 473 003c 08          	.uleb128 0x8
 474 003d 3A          	.uleb128 0x3a
 475 003e 0B          	.uleb128 0xb
 476 003f 3B          	.uleb128 0x3b
 477 0040 0B          	.uleb128 0xb
 478 0041 49          	.uleb128 0x49
 479 0042 13          	.uleb128 0x13
 480 0043 0B          	.uleb128 0xb
 481 0044 0B          	.uleb128 0xb
 482 0045 0D          	.uleb128 0xd
 483 0046 0B          	.uleb128 0xb
 484 0047 0C          	.uleb128 0xc
 485 0048 0B          	.uleb128 0xb
 486 0049 38          	.uleb128 0x38
 487 004a 0A          	.uleb128 0xa
 488 004b 00          	.byte 0x0
 489 004c 00          	.byte 0x0
 490 004d 06          	.uleb128 0x6
 491 004e 17          	.uleb128 0x17
 492 004f 01          	.byte 0x1
 493 0050 0B          	.uleb128 0xb
 494 0051 0B          	.uleb128 0xb
 495 0052 3A          	.uleb128 0x3a
 496 0053 0B          	.uleb128 0xb
 497 0054 3B          	.uleb128 0x3b
 498 0055 0B          	.uleb128 0xb
 499 0056 01          	.uleb128 0x1
 500 0057 13          	.uleb128 0x13
 501 0058 00          	.byte 0x0
 502 0059 00          	.byte 0x0
 503 005a 07          	.uleb128 0x7
 504 005b 0D          	.uleb128 0xd
 505 005c 00          	.byte 0x0
 506 005d 49          	.uleb128 0x49
 507 005e 13          	.uleb128 0x13
 508 005f 00          	.byte 0x0
 509 0060 00          	.byte 0x0
 510 0061 08          	.uleb128 0x8
 511 0062 0D          	.uleb128 0xd
 512 0063 00          	.byte 0x0
 513 0064 03          	.uleb128 0x3
 514 0065 08          	.uleb128 0x8
 515 0066 3A          	.uleb128 0x3a
 516 0067 0B          	.uleb128 0xb
 517 0068 3B          	.uleb128 0x3b
 518 0069 0B          	.uleb128 0xb
 519 006a 49          	.uleb128 0x49
 520 006b 13          	.uleb128 0x13
MPLAB XC16 ASSEMBLY Listing:   			page 12


 521 006c 00          	.byte 0x0
 522 006d 00          	.byte 0x0
 523 006e 09          	.uleb128 0x9
 524 006f 0D          	.uleb128 0xd
 525 0070 00          	.byte 0x0
 526 0071 49          	.uleb128 0x49
 527 0072 13          	.uleb128 0x13
 528 0073 38          	.uleb128 0x38
 529 0074 0A          	.uleb128 0xa
 530 0075 00          	.byte 0x0
 531 0076 00          	.byte 0x0
 532 0077 0A          	.uleb128 0xa
 533 0078 0D          	.uleb128 0xd
 534 0079 00          	.byte 0x0
 535 007a 03          	.uleb128 0x3
 536 007b 08          	.uleb128 0x8
 537 007c 3A          	.uleb128 0x3a
 538 007d 0B          	.uleb128 0xb
 539 007e 3B          	.uleb128 0x3b
 540 007f 0B          	.uleb128 0xb
 541 0080 49          	.uleb128 0x49
 542 0081 13          	.uleb128 0x13
 543 0082 38          	.uleb128 0x38
 544 0083 0A          	.uleb128 0xa
 545 0084 00          	.byte 0x0
 546 0085 00          	.byte 0x0
 547 0086 0B          	.uleb128 0xb
 548 0087 01          	.uleb128 0x1
 549 0088 01          	.byte 0x1
 550 0089 49          	.uleb128 0x49
 551 008a 13          	.uleb128 0x13
 552 008b 01          	.uleb128 0x1
 553 008c 13          	.uleb128 0x13
 554 008d 00          	.byte 0x0
 555 008e 00          	.byte 0x0
 556 008f 0C          	.uleb128 0xc
 557 0090 21          	.uleb128 0x21
 558 0091 00          	.byte 0x0
 559 0092 49          	.uleb128 0x49
 560 0093 13          	.uleb128 0x13
 561 0094 2F          	.uleb128 0x2f
 562 0095 0B          	.uleb128 0xb
 563 0096 00          	.byte 0x0
 564 0097 00          	.byte 0x0
 565 0098 0D          	.uleb128 0xd
 566 0099 2E          	.uleb128 0x2e
 567 009a 01          	.byte 0x1
 568 009b 3F          	.uleb128 0x3f
 569 009c 0C          	.uleb128 0xc
 570 009d 03          	.uleb128 0x3
 571 009e 08          	.uleb128 0x8
 572 009f 3A          	.uleb128 0x3a
 573 00a0 0B          	.uleb128 0xb
 574 00a1 3B          	.uleb128 0x3b
 575 00a2 0B          	.uleb128 0xb
 576 00a3 11          	.uleb128 0x11
 577 00a4 01          	.uleb128 0x1
MPLAB XC16 ASSEMBLY Listing:   			page 13


 578 00a5 12          	.uleb128 0x12
 579 00a6 01          	.uleb128 0x1
 580 00a7 40          	.uleb128 0x40
 581 00a8 0A          	.uleb128 0xa
 582 00a9 01          	.uleb128 0x1
 583 00aa 13          	.uleb128 0x13
 584 00ab 00          	.byte 0x0
 585 00ac 00          	.byte 0x0
 586 00ad 0E          	.uleb128 0xe
 587 00ae 34          	.uleb128 0x34
 588 00af 00          	.byte 0x0
 589 00b0 03          	.uleb128 0x3
 590 00b1 08          	.uleb128 0x8
 591 00b2 3A          	.uleb128 0x3a
 592 00b3 0B          	.uleb128 0xb
 593 00b4 3B          	.uleb128 0x3b
 594 00b5 0B          	.uleb128 0xb
 595 00b6 49          	.uleb128 0x49
 596 00b7 13          	.uleb128 0x13
 597 00b8 02          	.uleb128 0x2
 598 00b9 0A          	.uleb128 0xa
 599 00ba 00          	.byte 0x0
 600 00bb 00          	.byte 0x0
 601 00bc 0F          	.uleb128 0xf
 602 00bd 34          	.uleb128 0x34
 603 00be 00          	.byte 0x0
 604 00bf 03          	.uleb128 0x3
 605 00c0 0E          	.uleb128 0xe
 606 00c1 3A          	.uleb128 0x3a
 607 00c2 0B          	.uleb128 0xb
 608 00c3 3B          	.uleb128 0x3b
 609 00c4 05          	.uleb128 0x5
 610 00c5 49          	.uleb128 0x49
 611 00c6 13          	.uleb128 0x13
 612 00c7 3F          	.uleb128 0x3f
 613 00c8 0C          	.uleb128 0xc
 614 00c9 3C          	.uleb128 0x3c
 615 00ca 0C          	.uleb128 0xc
 616 00cb 00          	.byte 0x0
 617 00cc 00          	.byte 0x0
 618 00cd 10          	.uleb128 0x10
 619 00ce 35          	.uleb128 0x35
 620 00cf 00          	.byte 0x0
 621 00d0 49          	.uleb128 0x49
 622 00d1 13          	.uleb128 0x13
 623 00d2 00          	.byte 0x0
 624 00d3 00          	.byte 0x0
 625 00d4 00          	.byte 0x0
 626                 	.section .debug_pubnames,info
 627 0000 38 00 00 00 	.4byte 0x38
 628 0004 02 00       	.2byte 0x2
 629 0006 00 00 00 00 	.4byte .Ldebug_info0
 630 000a D1 02 00 00 	.4byte 0x2d1
 631 000e E7 01 00 00 	.4byte 0x1e7
 632 0012 73 65 6E 64 	.asciz "send_reset_message"
 632      5F 72 65 73 
 632      65 74 5F 6D 
MPLAB XC16 ASSEMBLY Listing:   			page 14


 632      65 73 73 61 
 632      67 65 00 
 633 0025 25 02 00 00 	.4byte 0x225
 634 0029 73 65 6E 64 	.asciz "send_adc_value"
 634      5F 61 64 63 
 634      5F 76 61 6C 
 634      75 65 00 
 635 0038 00 00 00 00 	.4byte 0x0
 636                 	.section .debug_pubtypes,info
 637 0000 27 00 00 00 	.4byte 0x27
 638 0004 02 00       	.2byte 0x2
 639 0006 00 00 00 00 	.4byte .Ldebug_info0
 640 000a D1 02 00 00 	.4byte 0x2d1
 641 000e 28 01 00 00 	.4byte 0x128
 642 0012 75 69 6E 74 	.asciz "uint16_t"
 642      31 36 5F 74 
 642      00 
 643 001b D8 01 00 00 	.4byte 0x1d8
 644 001f 43 41 4E 64 	.asciz "CANdata"
 644      61 74 61 00 
 645 0027 00 00 00 00 	.4byte 0x0
 646                 	.section .debug_aranges,info
 647 0000 14 00 00 00 	.4byte 0x14
 648 0004 02 00       	.2byte 0x2
 649 0006 00 00 00 00 	.4byte .Ldebug_info0
 650 000a 04          	.byte 0x4
 651 000b 00          	.byte 0x0
 652 000c 00 00       	.2byte 0x0
 653 000e 00 00       	.2byte 0x0
 654 0010 00 00 00 00 	.4byte 0x0
 655 0014 00 00 00 00 	.4byte 0x0
 656                 	.section .debug_str,info
 657                 	.LASF0:
 658 0000 41 44 43 31 	.asciz "ADC1BUF0"
 658      42 55 46 30 
 658      00 
 659                 	.LASF1:
 660 0009 41 44 43 31 	.asciz "ADC1BUF1"
 660      42 55 46 31 
 660      00 
 661                 	.LASF2:
 662 0012 41 44 43 31 	.asciz "ADC1BUF2"
 662      42 55 46 32 
 662      00 
 663                 	.LASF3:
 664 001b 41 44 43 31 	.asciz "ADC1BUF3"
 664      42 55 46 33 
 664      00 
 665                 	.section .text,code
 666              	
 667              	
 668              	
 669              	.section __c30_info,info,bss
 670                 	__psv_trap_errata:
 671                 	
 672                 	.section __c30_signature,info,data
 673 0000 01 00       	.word 0x0001
MPLAB XC16 ASSEMBLY Listing:   			page 15


 674 0002 00 00       	.word 0x0000
 675 0004 00 00       	.word 0x0000
 676                 	
 677                 	
 678                 	
 679                 	.set ___PA___,0
 680                 	.end
MPLAB XC16 ASSEMBLY Listing:   			page 16


DEFINED SYMBOLS
                            *ABS*:00000000 /home/user/Documents/FST/Programming/project_sub-zero/src/sub-zero_comms.c
                            *ABS*:00000001 __33EP256MU806
                            *ABS*:00000001 __dsPIC33E
                            *ABS*:00000001 __HAS_DSP
                            *ABS*:00000001 __HAS_DMAV2
                            *ABS*:00000001 __HAS_CODEGUARD
                            *ABS*:00000001 __HAS_PMP_ENHANCED
                            *ABS*:00000001 __HAS_EDS
                            *ABS*:00000001 __HAS_EP
                            *ABS*:00000011 __TARGET_DIVIDE_CYCLES
                            *ABS*:00000001 __C30ELF
    {standard input}:14     .text:00000000 _send_reset_message
    {standard input}:18     *ABS*:00000000 ___PA___
    {standard input}:62     .text:00000036 _send_adc_value
    {standard input}:670    __c30_info:00000000 __psv_trap_errata
    {standard input}:19     .text:00000000 .L0
                    .debug_abbrev:00000000 .Ldebug_abbrev0
                            .text:00000000 .Ltext0
                            .text:0000007e .Letext0
                      .debug_line:00000000 .Ldebug_line0
                            .text:00000000 .LFB0
                            .text:00000036 .LFE0
                            .text:00000036 .LFB1
                            .text:0000007e .LFE1
                       .debug_str:00000000 .LASF0
                       .debug_str:00000009 .LASF1
                       .debug_str:00000012 .LASF2
                       .debug_str:0000001b .LASF3
                     .debug_frame:00000000 .Lframe0
                      .debug_info:00000000 .Ldebug_info0

UNDEFINED SYMBOLS
_send_can1
CORCON
_ADC1BUF0
_ADC1BUF1
_ADC1BUF2
_ADC1BUF3

EQUATE SYMBOLS
/home/user/Documents/FST/Programming/project_sub-zero/src/sub-zero_comms.c = 0x0
                  __33EP256MU806 = 0x1
                      __dsPIC33E = 0x1
                       __HAS_DSP = 0x1
                     __HAS_DMAV2 = 0x1
                 __HAS_CODEGUARD = 0x1
              __HAS_PMP_ENHANCED = 0x1
                       __HAS_EDS = 0x1
                        __HAS_EP = 0x1
          __TARGET_DIVIDE_CYCLES = 0x11
                        __C30ELF = 0x1
                        ___PA___ = 0x0
