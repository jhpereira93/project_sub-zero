MPLAB XC16 ASSEMBLY Listing:   			page 1


   1              	.file "/home/user/Documents/FST/Programming/project_sub-zero/src/main.c"
   2              	.section .debug_abbrev,info
   3                 	.Ldebug_abbrev0:
   4                 	.section .debug_info,info
   5                 	.Ldebug_info0:
   6                 	.section .debug_line,info
   7                 	.Ldebug_line0:
   8 0000 58 00 00 00 	.section .text,code
   8      02 00 37 00 
   8      00 00 01 01 
   8      FB 0E 0A 00 
   8      01 01 01 01 
   8      00 00 00 01 
   8      73 72 63 00 
   8      6C 69 62 2F 
   8      6C 69 62 5F 
   9              	.Ltext0:
  10              	.section .text,code
  11              	.align 2
  12              	.global _main
  13              	.type _main,@function
  14              	_main:
  15              	.LFB0:
  16              	.file 1 "src/main.c"
   1:src/main.c    **** // FST Lisboa
   2:src/main.c    **** // Project Template
   3:src/main.c    **** 
   4:src/main.c    **** // DSPIC33EP256MU806 Configuration Bit Settings
   5:src/main.c    **** 
   6:src/main.c    **** // 'C' source line config statements
   7:src/main.c    **** 
   8:src/main.c    **** // FGS
   9:src/main.c    **** #pragma config GWRP = OFF				// General Segment Write-Protect bit (General Segment may be written)
  10:src/main.c    **** #pragma config GSS = OFF				// General Segment Code-Protect bit (General Segment Code protect is di
  11:src/main.c    **** #pragma config GSSK = OFF				// General Segment Key bits (General Segment Write Protection and Code
  12:src/main.c    **** 
  13:src/main.c    **** // FOSCSEL
  14:src/main.c    **** #pragma config FNOSC = FRC				// Initial Oscillator Source Selection bits (Internal Fast RC (FRC))
  15:src/main.c    **** #pragma config IESO = OFF				// Two-speed Oscillator Start-up Enable bit (Start up with user-select
  16:src/main.c    **** 
  17:src/main.c    **** // FOSC
  18:src/main.c    **** #pragma config POSCMD = HS				// Primary Oscillator Mode Select bits (XT Crystal Oscillator Mode)
  19:src/main.c    **** #pragma config OSCIOFNC = OFF			// OSC2 Pin Function bit (OSC2 is clock output)
  20:src/main.c    **** #pragma config IOL1WAY = OFF			// Peripheral pin select configuration (Allow multiple reconfigurati
  21:src/main.c    **** #pragma config FCKSM = CSECMD			// Clock Switching Mode bits (Clock switching is enabled,Fail-safe 
  22:src/main.c    **** 
  23:src/main.c    **** // FWDT
  24:src/main.c    **** #pragma config WDTPOST = PS32768		// Watchdog Timer Postscaler bits (1:32,768)
  25:src/main.c    **** #pragma config WDTPRE = PR128			// Watchdog Timer Prescaler bit (1:128)
  26:src/main.c    **** #pragma config PLLKEN = ON				// PLL Lock Wait Enable bit (Clock switch to PLL source will wait unt
  27:src/main.c    **** #pragma config WINDIS = OFF				// Watchdog Timer Window Enable bit (Watchdog Timer in Non-Window mo
  28:src/main.c    **** #pragma config FWDTEN = ON				// Watchdog Timer Enable bit (Watchdog timer always enabled)
  29:src/main.c    **** 
  30:src/main.c    **** // FPOR
  31:src/main.c    **** #pragma config FPWRT = PWR4		        // Power-on Reset Timer Value Select bits (4s)
  32:src/main.c    **** #pragma config BOREN = ON				// Brown-out Reset (BOR) Detection Enable bit (BOR is enabled)
  33:src/main.c    **** #pragma config ALTI2C1 = OFF			// Alternate I2C pins for I2C1 (SDA1/SCK1 pins are selected as the I
MPLAB XC16 ASSEMBLY Listing:   			page 2


  34:src/main.c    **** 
  35:src/main.c    **** // FICD
  36:src/main.c    **** #pragma config ICS = NONE				// ICD Communication Channel Select bits (Reserved, do not use)
  37:src/main.c    **** #pragma config RSTPRI = PF				// Reset Target Vector Select bit (Device will obtain reset instructi
  38:src/main.c    **** #pragma config JTAGEN = OFF				// JTAG Enable bit (JTAG is disabled)
  39:src/main.c    **** 
  40:src/main.c    **** // FAS
  41:src/main.c    **** #pragma config AWRP = OFF				// Auxiliary Segment Write-protect bit (Aux Flash may be written)
  42:src/main.c    **** #pragma config APL = OFF				// Auxiliary Segment Code-protect bit (Aux Flash Code protect is disabl
  43:src/main.c    **** #pragma config APLK = OFF				// Auxiliary Segment Key bits (Aux Flash Write Protection and Code Pro
  44:src/main.c    **** 
  45:src/main.c    **** // #pragma config statements should precede project file includes.
  46:src/main.c    **** // Use project enums instead of #define for ON and OFF.
  47:src/main.c    **** 
  48:src/main.c    **** #include <xc.h>
  49:src/main.c    **** // NOTE: Always include timing.h
  50:src/main.c    **** #include "lib_pic33e/timing.h"
  51:src/main.c    **** 
  52:src/main.c    **** //Comment this line if USB is not needed
  53:src/main.c    **** #include "lib_pic33e/usb_lib.h"
  54:src/main.c    **** 
  55:src/main.c    **** // Trap handling
  56:src/main.c    **** #include "lib_pic33e/trap.h"
  57:src/main.c    **** 
  58:src/main.c    **** //Specific sub-zero filesupstreamupstream
  59:src/main.c    **** #include "config_sub-zero.h"
  60:src/main.c    **** #include "sub-zero_comms.h"
  61:src/main.c    **** #include "lib_pic33e/timer.h"
  62:src/main.c    **** 
  63:src/main.c    **** int main(){
  17              	.loc 1 63 0
  18              	.set ___PA___,1
  19 000000  00 00 FA 	lnk #0
  20              	.LCFI0:
  64:src/main.c    **** 
  65:src/main.c    **** 
  66:src/main.c    ****     USBInit();
  21              	.loc 1 66 0
  22 000002  00 00 07 	rcall _USBInit
  67:src/main.c    **** 
  68:src/main.c    **** 		config_IO();
  23              	.loc 1 68 0
  24 000004  00 00 07 	rcall _config_IO
  69:src/main.c    **** 		//config_external1(0, 1);
  70:src/main.c    ****     //config_timer3_for_PWM();
  71:src/main.c    ****     //config_OC1();
  72:src/main.c    ****     //config_timer5_for_CAN();
  73:src/main.c    ****     config_timer2(1000, 5, tmr2_int);
  25              	.loc 1 73 0
  26 000006  00 00 20 	mov #handle(_tmr2_int),w0
  27 000008  00 01 78 	mov w0,w2
  28 00000a  51 00 20 	mov #5,w1
  29 00000c  80 3E 20 	mov #1000,w0
  30 00000e  00 00 07 	rcall _config_timer2
  74:src/main.c    ****     config_adc();
  31              	.loc 1 74 0
  32 000010  00 00 07 	rcall _config_adc
MPLAB XC16 ASSEMBLY Listing:   			page 3


  75:src/main.c    ****     config_can1(NULL);
  33              	.loc 1 75 0
  34 000012  00 00 EB 	clr w0
  35 000014  00 00 07 	rcall _config_can1
  76:src/main.c    **** 
  77:src/main.c    ****     send_reset_message();
  36              	.loc 1 77 0
  37 000016  00 00 07 	rcall _send_reset_message
  38              	.L2:
  78:src/main.c    **** 
  79:src/main.c    ****     while(1)
  80:src/main.c    ****     {
  81:src/main.c    ****        //Clear watchdog timer
  82:src/main.c    ****        ClrWdt();
  39              	.loc 1 82 0
  40 000018  00 60 FE 	clrwdt 
  83:src/main.c    ****     }//end while
  41              	.loc 1 83 0
  42 00001a  00 00 37 	bra .L2
  43              	.LFE0:
  44              	.size _main,.-_main
  45              	.align 2
  46              	.global _trap_handler
  47              	.type _trap_handler,@function
  48              	_trap_handler:
  49              	.LFB1:
  84:src/main.c    **** 
  85:src/main.c    ****     return 0;
  86:src/main.c    **** }//end main
  87:src/main.c    **** 
  88:src/main.c    **** 
  89:src/main.c    **** void trap_handler(TrapType type){
  50              	.loc 1 89 0
  51              	.set ___PA___,1
  52 00001c  02 00 FA 	lnk #2
  53              	.LCFI1:
  54 00001e  00 0F 78 	mov w0,[w14]
  90:src/main.c    **** 
  91:src/main.c    ****     switch(type){
  92:src/main.c    ****         case HARD_TRAP_OSCILATOR_FAIL:
  93:src/main.c    ****             break;
  94:src/main.c    ****         case HARD_TRAP_ADDRESS_ERROR:
  95:src/main.c    ****             break;
  96:src/main.c    ****         case HARD_TRAP_STACK_ERROR:
  97:src/main.c    ****             break;
  98:src/main.c    ****         case HARD_TRAP_MATH_ERROR:
  99:src/main.c    ****             break;
 100:src/main.c    ****         case CUSTOM_TRAP_PARSE_ERROR:
 101:src/main.c    ****             break;
 102:src/main.c    ****         default:
 103:src/main.c    ****             break;
 104:src/main.c    ****     }
 105:src/main.c    **** 
 106:src/main.c    **** 
 107:src/main.c    ****     return;
 108:src/main.c    **** }
  55              	.loc 1 108 0
MPLAB XC16 ASSEMBLY Listing:   			page 4


  56 000020  8E 07 78 	mov w14,w15
  57 000022  4F 07 78 	mov [--w15],w14
  58 000024  00 40 A9 	bclr CORCON,#2
  59 000026  00 00 06 	return 
  60              	.set ___PA___,0
  61              	.LFE1:
  62              	.size _trap_handler,.-_trap_handler
  63              	.section .debug_frame,info
  64                 	.Lframe0:
  65 0000 10 00 00 00 	.4byte .LECIE0-.LSCIE0
  66                 	.LSCIE0:
  67 0004 FF FF FF FF 	.4byte 0xffffffff
  68 0008 01          	.byte 0x1
  69 0009 00          	.byte 0
  70 000a 01          	.uleb128 0x1
  71 000b 02          	.sleb128 2
  72 000c 25          	.byte 0x25
  73 000d 12          	.byte 0x12
  74 000e 0F          	.uleb128 0xf
  75 000f 7E          	.sleb128 -2
  76 0010 09          	.byte 0x9
  77 0011 25          	.uleb128 0x25
  78 0012 0F          	.uleb128 0xf
  79 0013 00          	.align 4
  80                 	.LECIE0:
  81                 	.LSFDE0:
  82 0014 18 00 00 00 	.4byte .LEFDE0-.LASFDE0
  83                 	.LASFDE0:
  84 0018 00 00 00 00 	.4byte .Lframe0
  85 001c 00 00 00 00 	.4byte .LFB0
  86 0020 1C 00 00 00 	.4byte .LFE0-.LFB0
  87 0024 04          	.byte 0x4
  88 0025 02 00 00 00 	.4byte .LCFI0-.LFB0
  89 0029 13          	.byte 0x13
  90 002a 7D          	.sleb128 -3
  91 002b 0D          	.byte 0xd
  92 002c 0E          	.uleb128 0xe
  93 002d 8E          	.byte 0x8e
  94 002e 02          	.uleb128 0x2
  95 002f 00          	.align 4
  96                 	.LEFDE0:
  97                 	.LSFDE2:
  98 0030 18 00 00 00 	.4byte .LEFDE2-.LASFDE2
  99                 	.LASFDE2:
 100 0034 00 00 00 00 	.4byte .Lframe0
 101 0038 00 00 00 00 	.4byte .LFB1
 102 003c 0C 00 00 00 	.4byte .LFE1-.LFB1
 103 0040 04          	.byte 0x4
 104 0041 02 00 00 00 	.4byte .LCFI1-.LFB1
 105 0045 13          	.byte 0x13
 106 0046 7D          	.sleb128 -3
 107 0047 0D          	.byte 0xd
 108 0048 0E          	.uleb128 0xe
 109 0049 8E          	.byte 0x8e
 110 004a 02          	.uleb128 0x2
 111 004b 00          	.align 4
 112                 	.LEFDE2:
MPLAB XC16 ASSEMBLY Listing:   			page 5


 113                 	.section .text,code
 114              	.Letext0:
 115              	.file 2 "lib/lib_pic33e/trap.h"
 116              	.section .debug_info,info
 117 0000 39 02 00 00 	.4byte 0x239
 118 0004 02 00       	.2byte 0x2
 119 0006 00 00 00 00 	.4byte .Ldebug_abbrev0
 120 000a 04          	.byte 0x4
 121 000b 01          	.uleb128 0x1
 122 000c 47 4E 55 20 	.asciz "GNU C 4.5.1 (XC16, Microchip v1.36) (A) Build date: Dec  5 2018"
 122      43 20 34 2E 
 122      35 2E 31 20 
 122      28 58 43 31 
 122      36 2C 20 4D 
 122      69 63 72 6F 
 122      63 68 69 70 
 122      20 76 31 2E 
 122      33 36 29 20 
 123 004c 01          	.byte 0x1
 124 004d 73 72 63 2F 	.asciz "src/main.c"
 124      6D 61 69 6E 
 124      2E 63 00 
 125 0058 2F 68 6F 6D 	.asciz "/home/user/Documents/FST/Programming/project_sub-zero"
 125      65 2F 75 73 
 125      65 72 2F 44 
 125      6F 63 75 6D 
 125      65 6E 74 73 
 125      2F 46 53 54 
 125      2F 50 72 6F 
 125      67 72 61 6D 
 125      6D 69 6E 67 
 126 008e 00 00 00 00 	.4byte .Ltext0
 127 0092 00 00 00 00 	.4byte .Letext0
 128 0096 00 00 00 00 	.4byte .Ldebug_line0
 129 009a 02          	.uleb128 0x2
 130 009b 01          	.byte 0x1
 131 009c 06          	.byte 0x6
 132 009d 73 69 67 6E 	.asciz "signed char"
 132      65 64 20 63 
 132      68 61 72 00 
 133 00a9 02          	.uleb128 0x2
 134 00aa 02          	.byte 0x2
 135 00ab 05          	.byte 0x5
 136 00ac 69 6E 74 00 	.asciz "int"
 137 00b0 02          	.uleb128 0x2
 138 00b1 04          	.byte 0x4
 139 00b2 05          	.byte 0x5
 140 00b3 6C 6F 6E 67 	.asciz "long int"
 140      20 69 6E 74 
 140      00 
 141 00bc 02          	.uleb128 0x2
 142 00bd 08          	.byte 0x8
 143 00be 05          	.byte 0x5
 144 00bf 6C 6F 6E 67 	.asciz "long long int"
 144      20 6C 6F 6E 
 144      67 20 69 6E 
 144      74 00 
MPLAB XC16 ASSEMBLY Listing:   			page 6


 145 00cd 02          	.uleb128 0x2
 146 00ce 01          	.byte 0x1
 147 00cf 08          	.byte 0x8
 148 00d0 75 6E 73 69 	.asciz "unsigned char"
 148      67 6E 65 64 
 148      20 63 68 61 
 148      72 00 
 149 00de 02          	.uleb128 0x2
 150 00df 02          	.byte 0x2
 151 00e0 07          	.byte 0x7
 152 00e1 75 6E 73 69 	.asciz "unsigned int"
 152      67 6E 65 64 
 152      20 69 6E 74 
 152      00 
 153 00ee 02          	.uleb128 0x2
 154 00ef 04          	.byte 0x4
 155 00f0 07          	.byte 0x7
 156 00f1 6C 6F 6E 67 	.asciz "long unsigned int"
 156      20 75 6E 73 
 156      69 67 6E 65 
 156      64 20 69 6E 
 156      74 00 
 157 0103 02          	.uleb128 0x2
 158 0104 08          	.byte 0x8
 159 0105 07          	.byte 0x7
 160 0106 6C 6F 6E 67 	.asciz "long long unsigned int"
 160      20 6C 6F 6E 
 160      67 20 75 6E 
 160      73 69 67 6E 
 160      65 64 20 69 
 160      6E 74 00 
 161 011d 02          	.uleb128 0x2
 162 011e 02          	.byte 0x2
 163 011f 07          	.byte 0x7
 164 0120 73 68 6F 72 	.asciz "short unsigned int"
 164      74 20 75 6E 
 164      73 69 67 6E 
 164      65 64 20 69 
 164      6E 74 00 
 165 0133 02          	.uleb128 0x2
 166 0134 01          	.byte 0x1
 167 0135 02          	.byte 0x2
 168 0136 5F 42 6F 6F 	.asciz "_Bool"
 168      6C 00 
 169 013c 03          	.uleb128 0x3
 170 013d 74 72 61 70 	.asciz "trap_types"
 170      5F 74 79 70 
 170      65 73 00 
 171 0148 02          	.byte 0x2
 172 0149 02          	.byte 0x2
 173 014a 08          	.byte 0x8
 174 014b CE 01 00 00 	.4byte 0x1ce
 175 014f 04          	.uleb128 0x4
 176 0150 48 41 52 44 	.asciz "HARD_TRAP_OSCILATOR_FAIL"
 176      5F 54 52 41 
 176      50 5F 4F 53 
 176      43 49 4C 41 
MPLAB XC16 ASSEMBLY Listing:   			page 7


 176      54 4F 52 5F 
 176      46 41 49 4C 
 176      00 
 177 0169 00          	.sleb128 0
 178 016a 04          	.uleb128 0x4
 179 016b 48 41 52 44 	.asciz "HARD_TRAP_ADDRESS_ERROR"
 179      5F 54 52 41 
 179      50 5F 41 44 
 179      44 52 45 53 
 179      53 5F 45 52 
 179      52 4F 52 00 
 180 0183 01          	.sleb128 1
 181 0184 04          	.uleb128 0x4
 182 0185 48 41 52 44 	.asciz "HARD_TRAP_STACK_ERROR"
 182      5F 54 52 41 
 182      50 5F 53 54 
 182      41 43 4B 5F 
 182      45 52 52 4F 
 182      52 00 
 183 019b 02          	.sleb128 2
 184 019c 04          	.uleb128 0x4
 185 019d 48 41 52 44 	.asciz "HARD_TRAP_MATH_ERROR"
 185      5F 54 52 41 
 185      50 5F 4D 41 
 185      54 48 5F 45 
 185      52 52 4F 52 
 185      00 
 186 01b2 03          	.sleb128 3
 187 01b3 04          	.uleb128 0x4
 188 01b4 43 55 53 54 	.asciz "CUSTOM_TRAP_PARSE_ERROR"
 188      4F 4D 5F 54 
 188      52 41 50 5F 
 188      50 41 52 53 
 188      45 5F 45 52 
 188      52 4F 52 00 
 189 01cc 04          	.sleb128 4
 190 01cd 00          	.byte 0x0
 191 01ce 05          	.uleb128 0x5
 192 01cf 54 72 61 70 	.asciz "TrapType"
 192      54 79 70 65 
 192      00 
 193 01d8 02          	.byte 0x2
 194 01d9 0E          	.byte 0xe
 195 01da 3C 01 00 00 	.4byte 0x13c
 196 01de 06          	.uleb128 0x6
 197 01df 01          	.byte 0x1
 198 01e0 6D 61 69 6E 	.asciz "main"
 198      00 
 199 01e5 01          	.byte 0x1
 200 01e6 3F          	.byte 0x3f
 201 01e7 A9 00 00 00 	.4byte 0xa9
 202 01eb 00 00 00 00 	.4byte .LFB0
 203 01ef 00 00 00 00 	.4byte .LFE0
 204 01f3 01          	.byte 0x1
 205 01f4 5E          	.byte 0x5e
 206 01f5 10 02 00 00 	.4byte 0x210
 207 01f9 07          	.uleb128 0x7
MPLAB XC16 ASSEMBLY Listing:   			page 8


 208 01fa 01          	.byte 0x1
 209 01fb 63 6F 6E 66 	.asciz "config_adc"
 209      69 67 5F 61 
 209      64 63 00 
 210 0206 01          	.byte 0x1
 211 0207 4A          	.byte 0x4a
 212 0208 A9 00 00 00 	.4byte 0xa9
 213 020c 01          	.byte 0x1
 214 020d 08          	.uleb128 0x8
 215 020e 00          	.byte 0x0
 216 020f 00          	.byte 0x0
 217 0210 09          	.uleb128 0x9
 218 0211 01          	.byte 0x1
 219 0212 74 72 61 70 	.asciz "trap_handler"
 219      5F 68 61 6E 
 219      64 6C 65 72 
 219      00 
 220 021f 01          	.byte 0x1
 221 0220 59          	.byte 0x59
 222 0221 01          	.byte 0x1
 223 0222 00 00 00 00 	.4byte .LFB1
 224 0226 00 00 00 00 	.4byte .LFE1
 225 022a 01          	.byte 0x1
 226 022b 5E          	.byte 0x5e
 227 022c 0A          	.uleb128 0xa
 228 022d 74 79 70 65 	.asciz "type"
 228      00 
 229 0232 01          	.byte 0x1
 230 0233 59          	.byte 0x59
 231 0234 CE 01 00 00 	.4byte 0x1ce
 232 0238 02          	.byte 0x2
 233 0239 7E          	.byte 0x7e
 234 023a 00          	.sleb128 0
 235 023b 00          	.byte 0x0
 236 023c 00          	.byte 0x0
 237                 	.section .debug_abbrev,info
 238 0000 01          	.uleb128 0x1
 239 0001 11          	.uleb128 0x11
 240 0002 01          	.byte 0x1
 241 0003 25          	.uleb128 0x25
 242 0004 08          	.uleb128 0x8
 243 0005 13          	.uleb128 0x13
 244 0006 0B          	.uleb128 0xb
 245 0007 03          	.uleb128 0x3
 246 0008 08          	.uleb128 0x8
 247 0009 1B          	.uleb128 0x1b
 248 000a 08          	.uleb128 0x8
 249 000b 11          	.uleb128 0x11
 250 000c 01          	.uleb128 0x1
 251 000d 12          	.uleb128 0x12
 252 000e 01          	.uleb128 0x1
 253 000f 10          	.uleb128 0x10
 254 0010 06          	.uleb128 0x6
 255 0011 00          	.byte 0x0
 256 0012 00          	.byte 0x0
 257 0013 02          	.uleb128 0x2
 258 0014 24          	.uleb128 0x24
MPLAB XC16 ASSEMBLY Listing:   			page 9


 259 0015 00          	.byte 0x0
 260 0016 0B          	.uleb128 0xb
 261 0017 0B          	.uleb128 0xb
 262 0018 3E          	.uleb128 0x3e
 263 0019 0B          	.uleb128 0xb
 264 001a 03          	.uleb128 0x3
 265 001b 08          	.uleb128 0x8
 266 001c 00          	.byte 0x0
 267 001d 00          	.byte 0x0
 268 001e 03          	.uleb128 0x3
 269 001f 04          	.uleb128 0x4
 270 0020 01          	.byte 0x1
 271 0021 03          	.uleb128 0x3
 272 0022 08          	.uleb128 0x8
 273 0023 0B          	.uleb128 0xb
 274 0024 0B          	.uleb128 0xb
 275 0025 3A          	.uleb128 0x3a
 276 0026 0B          	.uleb128 0xb
 277 0027 3B          	.uleb128 0x3b
 278 0028 0B          	.uleb128 0xb
 279 0029 01          	.uleb128 0x1
 280 002a 13          	.uleb128 0x13
 281 002b 00          	.byte 0x0
 282 002c 00          	.byte 0x0
 283 002d 04          	.uleb128 0x4
 284 002e 28          	.uleb128 0x28
 285 002f 00          	.byte 0x0
 286 0030 03          	.uleb128 0x3
 287 0031 08          	.uleb128 0x8
 288 0032 1C          	.uleb128 0x1c
 289 0033 0D          	.uleb128 0xd
 290 0034 00          	.byte 0x0
 291 0035 00          	.byte 0x0
 292 0036 05          	.uleb128 0x5
 293 0037 16          	.uleb128 0x16
 294 0038 00          	.byte 0x0
 295 0039 03          	.uleb128 0x3
 296 003a 08          	.uleb128 0x8
 297 003b 3A          	.uleb128 0x3a
 298 003c 0B          	.uleb128 0xb
 299 003d 3B          	.uleb128 0x3b
 300 003e 0B          	.uleb128 0xb
 301 003f 49          	.uleb128 0x49
 302 0040 13          	.uleb128 0x13
 303 0041 00          	.byte 0x0
 304 0042 00          	.byte 0x0
 305 0043 06          	.uleb128 0x6
 306 0044 2E          	.uleb128 0x2e
 307 0045 01          	.byte 0x1
 308 0046 3F          	.uleb128 0x3f
 309 0047 0C          	.uleb128 0xc
 310 0048 03          	.uleb128 0x3
 311 0049 08          	.uleb128 0x8
 312 004a 3A          	.uleb128 0x3a
 313 004b 0B          	.uleb128 0xb
 314 004c 3B          	.uleb128 0x3b
 315 004d 0B          	.uleb128 0xb
MPLAB XC16 ASSEMBLY Listing:   			page 10


 316 004e 49          	.uleb128 0x49
 317 004f 13          	.uleb128 0x13
 318 0050 11          	.uleb128 0x11
 319 0051 01          	.uleb128 0x1
 320 0052 12          	.uleb128 0x12
 321 0053 01          	.uleb128 0x1
 322 0054 40          	.uleb128 0x40
 323 0055 0A          	.uleb128 0xa
 324 0056 01          	.uleb128 0x1
 325 0057 13          	.uleb128 0x13
 326 0058 00          	.byte 0x0
 327 0059 00          	.byte 0x0
 328 005a 07          	.uleb128 0x7
 329 005b 2E          	.uleb128 0x2e
 330 005c 01          	.byte 0x1
 331 005d 3F          	.uleb128 0x3f
 332 005e 0C          	.uleb128 0xc
 333 005f 03          	.uleb128 0x3
 334 0060 08          	.uleb128 0x8
 335 0061 3A          	.uleb128 0x3a
 336 0062 0B          	.uleb128 0xb
 337 0063 3B          	.uleb128 0x3b
 338 0064 0B          	.uleb128 0xb
 339 0065 49          	.uleb128 0x49
 340 0066 13          	.uleb128 0x13
 341 0067 3C          	.uleb128 0x3c
 342 0068 0C          	.uleb128 0xc
 343 0069 00          	.byte 0x0
 344 006a 00          	.byte 0x0
 345 006b 08          	.uleb128 0x8
 346 006c 18          	.uleb128 0x18
 347 006d 00          	.byte 0x0
 348 006e 00          	.byte 0x0
 349 006f 00          	.byte 0x0
 350 0070 09          	.uleb128 0x9
 351 0071 2E          	.uleb128 0x2e
 352 0072 01          	.byte 0x1
 353 0073 3F          	.uleb128 0x3f
 354 0074 0C          	.uleb128 0xc
 355 0075 03          	.uleb128 0x3
 356 0076 08          	.uleb128 0x8
 357 0077 3A          	.uleb128 0x3a
 358 0078 0B          	.uleb128 0xb
 359 0079 3B          	.uleb128 0x3b
 360 007a 0B          	.uleb128 0xb
 361 007b 27          	.uleb128 0x27
 362 007c 0C          	.uleb128 0xc
 363 007d 11          	.uleb128 0x11
 364 007e 01          	.uleb128 0x1
 365 007f 12          	.uleb128 0x12
 366 0080 01          	.uleb128 0x1
 367 0081 40          	.uleb128 0x40
 368 0082 0A          	.uleb128 0xa
 369 0083 00          	.byte 0x0
 370 0084 00          	.byte 0x0
 371 0085 0A          	.uleb128 0xa
 372 0086 05          	.uleb128 0x5
MPLAB XC16 ASSEMBLY Listing:   			page 11


 373 0087 00          	.byte 0x0
 374 0088 03          	.uleb128 0x3
 375 0089 08          	.uleb128 0x8
 376 008a 3A          	.uleb128 0x3a
 377 008b 0B          	.uleb128 0xb
 378 008c 3B          	.uleb128 0x3b
 379 008d 0B          	.uleb128 0xb
 380 008e 49          	.uleb128 0x49
 381 008f 13          	.uleb128 0x13
 382 0090 02          	.uleb128 0x2
 383 0091 0A          	.uleb128 0xa
 384 0092 00          	.byte 0x0
 385 0093 00          	.byte 0x0
 386 0094 00          	.byte 0x0
 387                 	.section .debug_pubnames,info
 388 0000 28 00 00 00 	.4byte 0x28
 389 0004 02 00       	.2byte 0x2
 390 0006 00 00 00 00 	.4byte .Ldebug_info0
 391 000a 3D 02 00 00 	.4byte 0x23d
 392 000e DE 01 00 00 	.4byte 0x1de
 393 0012 6D 61 69 6E 	.asciz "main"
 393      00 
 394 0017 10 02 00 00 	.4byte 0x210
 395 001b 74 72 61 70 	.asciz "trap_handler"
 395      5F 68 61 6E 
 395      64 6C 65 72 
 395      00 
 396 0028 00 00 00 00 	.4byte 0x0
 397                 	.section .debug_pubtypes,info
 398 0000 2A 00 00 00 	.4byte 0x2a
 399 0004 02 00       	.2byte 0x2
 400 0006 00 00 00 00 	.4byte .Ldebug_info0
 401 000a 3D 02 00 00 	.4byte 0x23d
 402 000e 3C 01 00 00 	.4byte 0x13c
 403 0012 74 72 61 70 	.asciz "trap_types"
 403      5F 74 79 70 
 403      65 73 00 
 404 001d CE 01 00 00 	.4byte 0x1ce
 405 0021 54 72 61 70 	.asciz "TrapType"
 405      54 79 70 65 
 405      00 
 406 002a 00 00 00 00 	.4byte 0x0
 407                 	.section .debug_aranges,info
 408 0000 14 00 00 00 	.4byte 0x14
 409 0004 02 00       	.2byte 0x2
 410 0006 00 00 00 00 	.4byte .Ldebug_info0
 411 000a 04          	.byte 0x4
 412 000b 00          	.byte 0x0
 413 000c 00 00       	.2byte 0x0
 414 000e 00 00       	.2byte 0x0
 415 0010 00 00 00 00 	.4byte 0x0
 416 0014 00 00 00 00 	.4byte 0x0
 417                 	.section .debug_str,info
 418                 	.section .text,code
 419              	
 420              	
 421              	
MPLAB XC16 ASSEMBLY Listing:   			page 12


 422              	.section __c30_info,info,bss
 423                 	__psv_trap_errata:
 424                 	
 425                 	.section __c30_signature,info,data
 426 0000 01 00       	.word 0x0001
 427 0002 00 00       	.word 0x0000
 428 0004 00 00       	.word 0x0000
 429                 	
 430                 	
 431                 	
 432                 	.section .config_APLK,code,address(0xf80010),keep
 433              	__config_APLK:
 434 000000  CF FF FF 	.pword 16777167
 435              	
 436              	.section .config_JTAGEN,code,address(0xf8000e),keep
 437              	__config_JTAGEN:
 438 000000  DC FF FF 	.pword 16777180
 439              	
 440              	.section .config_ALTI2C1,code,address(0xf8000c),keep
 441              	__config_ALTI2C1:
 442 000000  FA FF FF 	.pword 16777210
 443              	
 444              	.section .config_FWDTEN,code,address(0xf8000a),keep
 445              	__config_FWDTEN:
 446 000000  FF FF FF 	.pword 16777215
 447              	
 448              	.section .config_FCKSM,code,address(0xf80008),keep
 449              	__config_FCKSM:
 450 000000  5E FF FF 	.pword 16777054
 451              	
 452              	.section .config_IESO,code,address(0xf80006),keep
 453              	__config_IESO:
 454 000000  78 FF FF 	.pword 16777080
 455              	
 456              	.section .config_GSSK,code,address(0xf80004),keep
 457              	__config_GSSK:
 458 000000  CF FF FF 	.pword 16777167
 459              	
 460              	.set ___PA___,0
 461              	.end
MPLAB XC16 ASSEMBLY Listing:   			page 13


DEFINED SYMBOLS
                            *ABS*:00000000 /home/user/Documents/FST/Programming/project_sub-zero/src/main.c
                            *ABS*:00000001 __33EP256MU806
                            *ABS*:00000001 __dsPIC33E
                            *ABS*:00000001 __HAS_DSP
                            *ABS*:00000001 __HAS_DMAV2
                            *ABS*:00000001 __HAS_CODEGUARD
                            *ABS*:00000001 __HAS_PMP_ENHANCED
                            *ABS*:00000001 __HAS_EDS
                            *ABS*:00000001 __HAS_EP
                            *ABS*:00000011 __TARGET_DIVIDE_CYCLES
                            *ABS*:00000001 __C30ELF
    {standard input}:14     .text:00000000 _main
    {standard input}:18     *ABS*:00000000 ___PA___
    {standard input}:48     .text:0000001c _trap_handler
    {standard input}:423    __c30_info:00000000 __psv_trap_errata
                            *ABS*:00000040 __ext_attr_.config_APLK
    {standard input}:433    .config_APLK:00000000 __config_APLK
                            *ABS*:00000040 __ext_attr_.config_JTAGEN
    {standard input}:437    .config_JTAGEN:00000000 __config_JTAGEN
                            *ABS*:00000040 __ext_attr_.config_ALTI2C1
    {standard input}:441    .config_ALTI2C1:00000000 __config_ALTI2C1
                            *ABS*:00000040 __ext_attr_.config_FWDTEN
    {standard input}:445    .config_FWDTEN:00000000 __config_FWDTEN
                            *ABS*:00000040 __ext_attr_.config_FCKSM
    {standard input}:449    .config_FCKSM:00000000 __config_FCKSM
                            *ABS*:00000040 __ext_attr_.config_IESO
    {standard input}:453    .config_IESO:00000000 __config_IESO
                            *ABS*:00000040 __ext_attr_.config_GSSK
    {standard input}:457    .config_GSSK:00000000 __config_GSSK
    {standard input}:19     .text:00000000 .L0
                            .text:00000018 .L2
                    .debug_abbrev:00000000 .Ldebug_abbrev0
                            .text:00000000 .Ltext0
                            .text:00000028 .Letext0
                      .debug_line:00000000 .Ldebug_line0
                            .text:00000000 .LFB0
                            .text:0000001c .LFE0
                            .text:0000001c .LFB1
                            .text:00000028 .LFE1
                     .debug_frame:00000000 .Lframe0
                      .debug_info:00000000 .Ldebug_info0

UNDEFINED SYMBOLS
_USBInit
_config_IO
_tmr2_int
_config_timer2
_config_adc
_config_can1
_send_reset_message
CORCON

EQUATE SYMBOLS
/home/user/Documents/FST/Programming/project_sub-zero/src/main.c = 0x0
                  __33EP256MU806 = 0x1
                      __dsPIC33E = 0x1
                       __HAS_DSP = 0x1
                     __HAS_DMAV2 = 0x1
                 __HAS_CODEGUARD = 0x1
              __HAS_PMP_ENHANCED = 0x1
                       __HAS_EDS = 0x1
                        __HAS_EP = 0x1
          __TARGET_DIVIDE_CYCLES = 0x11
                        __C30ELF = 0x1
                        ___PA___ = 0x0
         __ext_attr_.config_APLK = 0x40
       __ext_attr_.config_JTAGEN = 0x40
      __ext_attr_.config_ALTI2C1 = 0x40
       __ext_attr_.config_FWDTEN = 0x40
        __ext_attr_.config_FCKSM = 0x40
         __ext_attr_.config_IESO = 0x40
         __ext_attr_.config_GSSK = 0x40
