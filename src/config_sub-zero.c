#include <xc.h>
#include "config_sub-zero.h"
#include "lib_pic33e/pps.h"
#include "lib_pic33e/ADC.h"
#include "lib_pic33e/external.h"

void config_IO()
{

    //Peripheral Pin Select:
    PPSUnLock;
    	//CAN
	PPSOutput(OUT_FN_PPS_C1TX, OUT_PIN_PPS_RP96);     //CAN FOR THE DEVBOARD
	PPSInput(IN_FN_PPS_C1RX, IN_PIN_PPS_RP97);
		//External Interrupts:
	//PPSInput(IN_FN_PPS_INT1, IN_PIN_PPS_RPI34);
	//PPSInput(IN_FN_PPS_INT2, IN_PIN_PPS_RPI37);
	//PPSInput(IN_FN_PPS_INT3, IN_PIN_PPS_RPI86);
		//Output Compare PWM:
			//OC1 PUMP Control
	//PPSOutput(OUT_FN_PPS_OC1, OUT_PIN_PPS_RP85);
			//OC2 FAN Control
	//PPSOutput(OUT_FN_PPS_OC2, OUT_PIN_PPS_RP87);
    PPSLock;
}

/*void config_timer3_for_PWM()
{
    //Timer3 configuration to be used for PWM cycle period.
    T3CON = 0;
    T3CONbits.TCKPS = 0b11;    //1:256 prescaler;
    TMR3 = 0;
    //PR3 é ignorado no modo de PWM
    IFS0bits.T3IF = 0;      //Clear Timer interrupt flag;
    IEC0bits.T3IE = 0;      //Disable Timer interrupt;
    //Start timer
    T3CONbits.TON = 1;
}

void config_OC1()
{
    //Output Compare configuration;
    OC1CON1 = 0;
    OC1CON1bits.OCSIDL = 0;     //No idle mode;
    OC1CON1bits.OCTSEL = 0b001; //Timer3 as the clock;
    OC1CON1bits.OCM = 0b110;    //Edge aligned PWM mode;
    OC1CON2 = 0;
    OC1CON2bits.SYNCSEL = 31;   //Sync using OC1RS
    OC1RS = 32000;
    OC1R = 16000;
    //Interrupts
    IFS0bits.OC1IF = 0;
    IEC0bits.OC1IE = 0;
}*/

void config_adc()
{

      //config IO
      ANSELBbits.ANSB9 = 1;
      TRISBbits.TRISB9 = 1;

      ADC_parameters adc_config;

      config_pin_cycling(AN9 | AN5, 250000UL, 64, 4, &adc_config);
      config_adc1(adc_config);
}

void external1_callback(void){
      LATEbits.LATE7 ^= 1;
}

void tmr2_int(){
      send_adc_value();
}
