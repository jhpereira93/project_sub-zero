#ifndef __CONFIG_SUBZERO_H__
#define __CONFIG_SUBZERO_H__

#include "can-ids/CAN_IDs.h"

void config_IO();
void config_timer3_for_PWM();
void config_OC1();
void tmr2_int();

#endif
