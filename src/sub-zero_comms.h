#ifndef __SUBZERO_COMMS_H__
#define __SUBZERO_COMMS_H__

#include "lib_pic33e/can.h"

void send_reset_message();
void send_adc_value();

#endif
