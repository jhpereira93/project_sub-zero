#include <stdlib.h>
#include "sub-zero_comms.h"
#include "config_sub-zero.h"
#include "lib_pic33e/can.h"
#include "can-ids/Devices/SUBZERO_CAN.h"
#include "lib_pic33e/ADC.h"

void send_reset_message(){
	CANdata reset_message;

	reset_message.dev_id = DEVICE_ID_SUBZERO;
	reset_message.msg_id = MSG_ID_SUBZERO_RESET;
	reset_message.dlc = 0;
	send_can1(reset_message);
}

void send_adc_value(){
	CANdata adc_value;

	//uint16_t adc_results[1] = {0};

	//get_pin_cycling_values(&adc_results, 0, 4);

	adc_value.dev_id = DEVICE_ID_SUBZERO;
	adc_value.msg_id = MSG_ID_SUBZERO_ADC;
	adc_value.dlc = 8;
	adc_value.data[0] = ADC1BUF0;
	adc_value.data[1] = ADC1BUF1;
	adc_value.data[2] = ADC1BUF2;
	adc_value.data[3] = ADC1BUF3;

	send_can1(adc_value);

}
